﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRightClick
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblHeading = New System.Windows.Forms.Label
        Me.gridMenuItem = New System.Windows.Forms.DataGridView
        Me.gridMenuItemPostingline = New System.Windows.Forms.DataGridView
        Me.lblFieldSource = New System.Windows.Forms.Label
        Me.lblFieldName = New System.Windows.Forms.Label
        Me.lblFieldValue = New System.Windows.Forms.Label
        Me.cmbFieldSource1 = New System.Windows.Forms.ComboBox
        Me.lbl1 = New System.Windows.Forms.Label
        Me.lbl2 = New System.Windows.Forms.Label
        Me.cmbFieldSource2 = New System.Windows.Forms.ComboBox
        Me.lbl3 = New System.Windows.Forms.Label
        Me.cmbFieldSource3 = New System.Windows.Forms.ComboBox
        Me.lbl4 = New System.Windows.Forms.Label
        Me.cmbFieldSource4 = New System.Windows.Forms.ComboBox
        Me.lbl5 = New System.Windows.Forms.Label
        Me.cmbFieldSource5 = New System.Windows.Forms.ComboBox
        Me.lbl6 = New System.Windows.Forms.Label
        Me.cmbFieldSource6 = New System.Windows.Forms.ComboBox
        Me.lbl7 = New System.Windows.Forms.Label
        Me.cmbFieldSource7 = New System.Windows.Forms.ComboBox
        Me.lbl8 = New System.Windows.Forms.Label
        Me.cmbFieldSource8 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName8 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName7 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName6 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName5 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName4 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName3 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName2 = New System.Windows.Forms.ComboBox
        Me.cmbFieldName1 = New System.Windows.Forms.ComboBox
        Me.txtValue1 = New System.Windows.Forms.TextBox
        Me.txtValue2 = New System.Windows.Forms.TextBox
        Me.txtValue3 = New System.Windows.Forms.TextBox
        Me.txtValue4 = New System.Windows.Forms.TextBox
        Me.txtValue5 = New System.Windows.Forms.TextBox
        Me.txtValue6 = New System.Windows.Forms.TextBox
        Me.txtValue7 = New System.Windows.Forms.TextBox
        Me.txtValue8 = New System.Windows.Forms.TextBox
        Me.cmdDeleteRightClick = New System.Windows.Forms.Button
        Me.cmdDeleteLine = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.gridMenuItem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridMenuItemPostingline, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(1160, 536)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 27
        Me.cmdOK.Text = "50110-End"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(689, 9)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(105, 33)
        Me.lblHeading.TabIndex = 32
        Me.lblHeading.Text = "55049"
        '
        'gridMenuItem
        '
        Me.gridMenuItem.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMenuItem.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.gridMenuItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridMenuItem.DefaultCellStyle = DataGridViewCellStyle8
        Me.gridMenuItem.Location = New System.Drawing.Point(239, 85)
        Me.gridMenuItem.Name = "gridMenuItem"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMenuItem.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.gridMenuItem.Size = New System.Drawing.Size(918, 130)
        Me.gridMenuItem.TabIndex = 60
        '
        'gridMenuItemPostingline
        '
        Me.gridMenuItemPostingline.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMenuItemPostingline.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.gridMenuItemPostingline.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridMenuItemPostingline.DefaultCellStyle = DataGridViewCellStyle11
        Me.gridMenuItemPostingline.Location = New System.Drawing.Point(239, 231)
        Me.gridMenuItemPostingline.Name = "gridMenuItemPostingline"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMenuItemPostingline.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.gridMenuItemPostingline.Size = New System.Drawing.Size(906, 158)
        Me.gridMenuItemPostingline.TabIndex = 1
        '
        'lblFieldSource
        '
        Me.lblFieldSource.AutoSize = True
        Me.lblFieldSource.Location = New System.Drawing.Point(60, 459)
        Me.lblFieldSource.Name = "lblFieldSource"
        Me.lblFieldSource.Size = New System.Drawing.Size(103, 13)
        Me.lblFieldSource.TabIndex = 63
        Me.lblFieldSource.Text = "60417 - Field soruce"
        '
        'lblFieldName
        '
        Me.lblFieldName.AutoSize = True
        Me.lblFieldName.Location = New System.Drawing.Point(60, 432)
        Me.lblFieldName.Name = "lblFieldName"
        Me.lblFieldName.Size = New System.Drawing.Size(97, 13)
        Me.lblFieldName.TabIndex = 64
        Me.lblFieldName.Text = "60415 - Field name"
        '
        'lblFieldValue
        '
        Me.lblFieldValue.AutoSize = True
        Me.lblFieldValue.Location = New System.Drawing.Point(60, 485)
        Me.lblFieldValue.Name = "lblFieldValue"
        Me.lblFieldValue.Size = New System.Drawing.Size(73, 13)
        Me.lblFieldValue.TabIndex = 65
        Me.lblFieldValue.Text = "60416 - Value"
        '
        'cmbFieldSource1
        '
        Me.cmbFieldSource1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource1.FormattingEnabled = True
        Me.cmbFieldSource1.Location = New System.Drawing.Point(160, 456)
        Me.cmbFieldSource1.Name = "cmbFieldSource1"
        Me.cmbFieldSource1.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource1.TabIndex = 3
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.Location = New System.Drawing.Point(206, 404)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(21, 24)
        Me.lbl1.TabIndex = 67
        Me.lbl1.Text = "1"
        '
        'lbl2
        '
        Me.lbl2.AutoSize = True
        Me.lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl2.Location = New System.Drawing.Point(342, 404)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(21, 24)
        Me.lbl2.TabIndex = 69
        Me.lbl2.Text = "2"
        '
        'cmbFieldSource2
        '
        Me.cmbFieldSource2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource2.FormattingEnabled = True
        Me.cmbFieldSource2.Location = New System.Drawing.Point(296, 456)
        Me.cmbFieldSource2.Name = "cmbFieldSource2"
        Me.cmbFieldSource2.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource2.TabIndex = 6
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl3.Location = New System.Drawing.Point(478, 404)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(21, 24)
        Me.lbl3.TabIndex = 71
        Me.lbl3.Text = "3"
        '
        'cmbFieldSource3
        '
        Me.cmbFieldSource3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource3.FormattingEnabled = True
        Me.cmbFieldSource3.Location = New System.Drawing.Point(432, 456)
        Me.cmbFieldSource3.Name = "cmbFieldSource3"
        Me.cmbFieldSource3.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource3.TabIndex = 9
        '
        'lbl4
        '
        Me.lbl4.AutoSize = True
        Me.lbl4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl4.Location = New System.Drawing.Point(614, 404)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(21, 24)
        Me.lbl4.TabIndex = 73
        Me.lbl4.Text = "4"
        '
        'cmbFieldSource4
        '
        Me.cmbFieldSource4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource4.FormattingEnabled = True
        Me.cmbFieldSource4.Location = New System.Drawing.Point(568, 456)
        Me.cmbFieldSource4.Name = "cmbFieldSource4"
        Me.cmbFieldSource4.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource4.TabIndex = 12
        '
        'lbl5
        '
        Me.lbl5.AutoSize = True
        Me.lbl5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5.Location = New System.Drawing.Point(750, 404)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(21, 24)
        Me.lbl5.TabIndex = 75
        Me.lbl5.Text = "5"
        '
        'cmbFieldSource5
        '
        Me.cmbFieldSource5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource5.FormattingEnabled = True
        Me.cmbFieldSource5.Location = New System.Drawing.Point(704, 456)
        Me.cmbFieldSource5.Name = "cmbFieldSource5"
        Me.cmbFieldSource5.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource5.TabIndex = 15
        '
        'lbl6
        '
        Me.lbl6.AutoSize = True
        Me.lbl6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl6.Location = New System.Drawing.Point(886, 404)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(21, 24)
        Me.lbl6.TabIndex = 77
        Me.lbl6.Text = "6"
        '
        'cmbFieldSource6
        '
        Me.cmbFieldSource6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource6.FormattingEnabled = True
        Me.cmbFieldSource6.Location = New System.Drawing.Point(840, 456)
        Me.cmbFieldSource6.Name = "cmbFieldSource6"
        Me.cmbFieldSource6.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource6.TabIndex = 18
        '
        'lbl7
        '
        Me.lbl7.AutoSize = True
        Me.lbl7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl7.Location = New System.Drawing.Point(1022, 404)
        Me.lbl7.Name = "lbl7"
        Me.lbl7.Size = New System.Drawing.Size(21, 24)
        Me.lbl7.TabIndex = 79
        Me.lbl7.Text = "7"
        '
        'cmbFieldSource7
        '
        Me.cmbFieldSource7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource7.FormattingEnabled = True
        Me.cmbFieldSource7.Location = New System.Drawing.Point(976, 456)
        Me.cmbFieldSource7.Name = "cmbFieldSource7"
        Me.cmbFieldSource7.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource7.TabIndex = 21
        '
        'lbl8
        '
        Me.lbl8.AutoSize = True
        Me.lbl8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl8.Location = New System.Drawing.Point(1157, 404)
        Me.lbl8.Name = "lbl8"
        Me.lbl8.Size = New System.Drawing.Size(21, 24)
        Me.lbl8.TabIndex = 81
        Me.lbl8.Text = "8"
        '
        'cmbFieldSource8
        '
        Me.cmbFieldSource8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldSource8.FormattingEnabled = True
        Me.cmbFieldSource8.Location = New System.Drawing.Point(1111, 456)
        Me.cmbFieldSource8.Name = "cmbFieldSource8"
        Me.cmbFieldSource8.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldSource8.TabIndex = 25
        '
        'cmbFieldName8
        '
        Me.cmbFieldName8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName8.FormattingEnabled = True
        Me.cmbFieldName8.Location = New System.Drawing.Point(1111, 429)
        Me.cmbFieldName8.Name = "cmbFieldName8"
        Me.cmbFieldName8.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName8.TabIndex = 23
        '
        'cmbFieldName7
        '
        Me.cmbFieldName7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName7.FormattingEnabled = True
        Me.cmbFieldName7.Location = New System.Drawing.Point(976, 429)
        Me.cmbFieldName7.Name = "cmbFieldName7"
        Me.cmbFieldName7.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName7.TabIndex = 20
        '
        'cmbFieldName6
        '
        Me.cmbFieldName6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName6.FormattingEnabled = True
        Me.cmbFieldName6.Location = New System.Drawing.Point(840, 429)
        Me.cmbFieldName6.Name = "cmbFieldName6"
        Me.cmbFieldName6.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName6.TabIndex = 17
        '
        'cmbFieldName5
        '
        Me.cmbFieldName5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName5.FormattingEnabled = True
        Me.cmbFieldName5.Location = New System.Drawing.Point(704, 429)
        Me.cmbFieldName5.Name = "cmbFieldName5"
        Me.cmbFieldName5.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName5.TabIndex = 14
        '
        'cmbFieldName4
        '
        Me.cmbFieldName4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName4.FormattingEnabled = True
        Me.cmbFieldName4.Location = New System.Drawing.Point(568, 429)
        Me.cmbFieldName4.Name = "cmbFieldName4"
        Me.cmbFieldName4.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName4.TabIndex = 11
        '
        'cmbFieldName3
        '
        Me.cmbFieldName3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName3.FormattingEnabled = True
        Me.cmbFieldName3.Location = New System.Drawing.Point(432, 429)
        Me.cmbFieldName3.Name = "cmbFieldName3"
        Me.cmbFieldName3.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName3.TabIndex = 8
        '
        'cmbFieldName2
        '
        Me.cmbFieldName2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName2.FormattingEnabled = True
        Me.cmbFieldName2.Location = New System.Drawing.Point(296, 429)
        Me.cmbFieldName2.Name = "cmbFieldName2"
        Me.cmbFieldName2.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName2.TabIndex = 5
        '
        'cmbFieldName1
        '
        Me.cmbFieldName1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFieldName1.FormattingEnabled = True
        Me.cmbFieldName1.Location = New System.Drawing.Point(160, 429)
        Me.cmbFieldName1.Name = "cmbFieldName1"
        Me.cmbFieldName1.Size = New System.Drawing.Size(121, 21)
        Me.cmbFieldName1.TabIndex = 2
        '
        'txtValue1
        '
        Me.txtValue1.Location = New System.Drawing.Point(160, 485)
        Me.txtValue1.Name = "txtValue1"
        Me.txtValue1.Size = New System.Drawing.Size(121, 20)
        Me.txtValue1.TabIndex = 4
        '
        'txtValue2
        '
        Me.txtValue2.Location = New System.Drawing.Point(296, 485)
        Me.txtValue2.Name = "txtValue2"
        Me.txtValue2.Size = New System.Drawing.Size(121, 20)
        Me.txtValue2.TabIndex = 7
        '
        'txtValue3
        '
        Me.txtValue3.Location = New System.Drawing.Point(432, 485)
        Me.txtValue3.Name = "txtValue3"
        Me.txtValue3.Size = New System.Drawing.Size(121, 20)
        Me.txtValue3.TabIndex = 10
        '
        'txtValue4
        '
        Me.txtValue4.Location = New System.Drawing.Point(568, 485)
        Me.txtValue4.Name = "txtValue4"
        Me.txtValue4.Size = New System.Drawing.Size(121, 20)
        Me.txtValue4.TabIndex = 13
        '
        'txtValue5
        '
        Me.txtValue5.Location = New System.Drawing.Point(704, 485)
        Me.txtValue5.Name = "txtValue5"
        Me.txtValue5.Size = New System.Drawing.Size(121, 20)
        Me.txtValue5.TabIndex = 16
        '
        'txtValue6
        '
        Me.txtValue6.Location = New System.Drawing.Point(840, 485)
        Me.txtValue6.Name = "txtValue6"
        Me.txtValue6.Size = New System.Drawing.Size(121, 20)
        Me.txtValue6.TabIndex = 19
        '
        'txtValue7
        '
        Me.txtValue7.Location = New System.Drawing.Point(976, 485)
        Me.txtValue7.Name = "txtValue7"
        Me.txtValue7.Size = New System.Drawing.Size(121, 20)
        Me.txtValue7.TabIndex = 22
        '
        'txtValue8
        '
        Me.txtValue8.Location = New System.Drawing.Point(1111, 485)
        Me.txtValue8.Name = "txtValue8"
        Me.txtValue8.Size = New System.Drawing.Size(121, 20)
        Me.txtValue8.TabIndex = 26
        '
        'cmdDeleteRightClick
        '
        Me.cmdDeleteRightClick.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDeleteRightClick.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDeleteRightClick.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDeleteRightClick.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDeleteRightClick.Location = New System.Drawing.Point(1160, 177)
        Me.cmdDeleteRightClick.Name = "cmdDeleteRightClick"
        Me.cmdDeleteRightClick.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDeleteRightClick.Size = New System.Drawing.Size(73, 21)
        Me.cmdDeleteRightClick.TabIndex = 82
        Me.cmdDeleteRightClick.Text = "50413-Slett"
        Me.cmdDeleteRightClick.UseVisualStyleBackColor = False
        '
        'cmdDeleteLine
        '
        Me.cmdDeleteLine.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDeleteLine.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDeleteLine.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDeleteLine.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDeleteLine.Location = New System.Drawing.Point(1160, 351)
        Me.cmdDeleteLine.Name = "cmdDeleteLine"
        Me.cmdDeleteLine.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDeleteLine.Size = New System.Drawing.Size(73, 21)
        Me.cmdDeleteLine.TabIndex = 83
        Me.cmdDeleteLine.Text = "50413-Slett"
        Me.cmdDeleteLine.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(1080, 536)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 84
        Me.cmdCancel.Text = "55006-Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(1001, 536)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(73, 21)
        Me.cmdSave.TabIndex = 85
        Me.cmdSave.Text = "55034-Save"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(241, 225)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(920, 1)
        Me.lblLine1.TabIndex = 86
        Me.lblLine1.Text = "Label1"
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(241, 398)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(920, 1)
        Me.lblLine2.TabIndex = 87
        Me.lblLine2.Text = "Label1"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(10, 526)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1230, 1)
        Me.Label1.TabIndex = 88
        Me.Label1.Text = "Label1"
        '
        'frmRightClick
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1248, 574)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdDeleteLine)
        Me.Controls.Add(Me.cmdDeleteRightClick)
        Me.Controls.Add(Me.txtValue8)
        Me.Controls.Add(Me.txtValue7)
        Me.Controls.Add(Me.txtValue6)
        Me.Controls.Add(Me.txtValue5)
        Me.Controls.Add(Me.txtValue4)
        Me.Controls.Add(Me.txtValue3)
        Me.Controls.Add(Me.txtValue2)
        Me.Controls.Add(Me.txtValue1)
        Me.Controls.Add(Me.cmbFieldName8)
        Me.Controls.Add(Me.cmbFieldName7)
        Me.Controls.Add(Me.cmbFieldName6)
        Me.Controls.Add(Me.cmbFieldName5)
        Me.Controls.Add(Me.cmbFieldName4)
        Me.Controls.Add(Me.cmbFieldName3)
        Me.Controls.Add(Me.cmbFieldName2)
        Me.Controls.Add(Me.cmbFieldName1)
        Me.Controls.Add(Me.lbl8)
        Me.Controls.Add(Me.cmbFieldSource8)
        Me.Controls.Add(Me.lbl7)
        Me.Controls.Add(Me.cmbFieldSource7)
        Me.Controls.Add(Me.lbl6)
        Me.Controls.Add(Me.cmbFieldSource6)
        Me.Controls.Add(Me.lbl5)
        Me.Controls.Add(Me.cmbFieldSource5)
        Me.Controls.Add(Me.lbl4)
        Me.Controls.Add(Me.cmbFieldSource4)
        Me.Controls.Add(Me.lbl3)
        Me.Controls.Add(Me.cmbFieldSource3)
        Me.Controls.Add(Me.lbl2)
        Me.Controls.Add(Me.cmbFieldSource2)
        Me.Controls.Add(Me.lbl1)
        Me.Controls.Add(Me.cmbFieldSource1)
        Me.Controls.Add(Me.lblFieldValue)
        Me.Controls.Add(Me.lblFieldName)
        Me.Controls.Add(Me.lblFieldSource)
        Me.Controls.Add(Me.gridMenuItemPostingline)
        Me.Controls.Add(Me.gridMenuItem)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.cmdOK)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRightClick"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "55049"
        CType(Me.gridMenuItem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridMenuItemPostingline, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents lblHeading As System.Windows.Forms.Label
    Friend WithEvents gridMenuItem As System.Windows.Forms.DataGridView
    Friend WithEvents gridMenuItemPostingline As System.Windows.Forms.DataGridView
    Friend WithEvents lblFieldSource As System.Windows.Forms.Label
    Friend WithEvents lblFieldName As System.Windows.Forms.Label
    Friend WithEvents lblFieldValue As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource1 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource2 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource3 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource4 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource5 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl6 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource6 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl7 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource7 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl8 As System.Windows.Forms.Label
    Friend WithEvents cmbFieldSource8 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName8 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName7 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName6 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName5 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName4 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName3 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFieldName1 As System.Windows.Forms.ComboBox
    Friend WithEvents txtValue1 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue2 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue3 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue4 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue5 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue6 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue7 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue8 As System.Windows.Forms.TextBox
    Public WithEvents cmdDeleteRightClick As System.Windows.Forms.Button
    Public WithEvents cmdDeleteLine As System.Windows.Forms.Button
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
