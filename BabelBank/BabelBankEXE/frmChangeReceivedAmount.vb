Option Strict Off
Option Explicit On

Friend Class frmChangeReceivedAmount
	Inherits System.Windows.Forms.Form
	Public bStoreInfo As Boolean
	
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		bStoreInfo = False
		Me.Hide()
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		bStoreInfo = True
		Me.Hide()
		
	End Sub
	
	Private Sub frmChangeReceivedAmount_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormvbStyle(Me, "dollar.jpg", 1)
		FormLRSCaptions(Me)
		
		bStoreInfo = False
		
	End Sub
	
	Private Sub txtAmount_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAmount.Enter
		
		txtAmount.SelectionStart = 0
		txtAmount.SelectionLength = Len(Trim(txtAmount.Text))
		
	End Sub
	
	Private Sub txtCurrency_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCurrency.Enter
		
		txtCurrency.SelectionStart = 0
		txtCurrency.SelectionLength = Len(Trim(txtCurrency.Text))
		
	End Sub
End Class
