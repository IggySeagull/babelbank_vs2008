<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmChooseFileformat
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents picPane As System.Windows.Forms.PictureBox
	Public WithEvents lstFileformats As System.Windows.Forms.ListBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmChooseFileformat))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdOK = New System.Windows.Forms.Button
		Me.picPane = New System.Windows.Forms.PictureBox
		Me.lstFileformats = New System.Windows.Forms.ListBox
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Form1"
		Me.ClientSize = New System.Drawing.Size(312, 245)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmChooseFileformat"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.cmdOK.Size = New System.Drawing.Size(97, 33)
		Me.cmdOK.Location = New System.Drawing.Point(112, 201)
		Me.cmdOK.TabIndex = 2
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.picPane.Size = New System.Drawing.Size(41, 41)
		Me.picPane.Location = New System.Drawing.Point(8, 41)
		Me.picPane.Image = CType(resources.GetObject("picPane.Image"), System.Drawing.Image)
		Me.picPane.TabIndex = 1
		Me.picPane.Dock = System.Windows.Forms.DockStyle.None
		Me.picPane.BackColor = System.Drawing.SystemColors.Control
		Me.picPane.CausesValidation = True
		Me.picPane.Enabled = True
		Me.picPane.ForeColor = System.Drawing.SystemColors.ControlText
		Me.picPane.Cursor = System.Windows.Forms.Cursors.Default
		Me.picPane.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picPane.TabStop = True
		Me.picPane.Visible = True
		Me.picPane.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.picPane.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.picPane.Name = "picPane"
		Me.lstFileformats.Size = New System.Drawing.Size(193, 150)
		Me.lstFileformats.Location = New System.Drawing.Point(64, 41)
		Me.lstFileformats.TabIndex = 0
		Me.lstFileformats.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstFileformats.BackColor = System.Drawing.SystemColors.Window
		Me.lstFileformats.CausesValidation = True
		Me.lstFileformats.Enabled = True
		Me.lstFileformats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstFileformats.IntegralHeight = True
		Me.lstFileformats.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstFileformats.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstFileformats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstFileformats.Sorted = False
		Me.lstFileformats.TabStop = True
		Me.lstFileformats.Visible = True
		Me.lstFileformats.MultiColumn = False
		Me.lstFileformats.Name = "lstFileformats"
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(picPane)
		Me.Controls.Add(lstFileformats)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
