'Option Strict Off
'Option Explicit On
'Module RegDLL

'	' Test if BabelBanks .dlls have been registered
'	' If not registrered with correct version, then this module will do so!
'	Public Declare Function RegvbBabelDLL Lib "vbBabel.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegbbSetupDLL Lib "bbSetup.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegEDI2XMLDLL Lib "EDI2XML.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegBabelBank414DLL Lib "BabelBank414.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegBabelBank809DLL Lib "BabelBank809.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegvbBabel414DLL Lib "vbBabel414.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegvbBabel809DLL Lib "vbBabel809.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegSetup414DLL Lib "Setup414.dll"  Alias "DllRegisterServer"() As Integer
'	Public Declare Function RegSetup809DLL Lib "Setup809.dll"  Alias "DllRegisterServer"() As Integer

'    Public Declare Function GetFileVersionInfoSize Lib "version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, ByRef lpdwHandle As Integer) As Integer

'	'Public Declare Sub CopyMemory Lib "kernel32" _
'	''   Alias "RtlMoveMemory" _
'	''  (Destination As Any, _
'	''   Source As Any, _
'	''   ByVal Length As Long)


'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    Public Declare Function VerQueryValue Lib "version.dll" Alias "VerQueryValueA" (ByRef pBlock As Byte, ByVal lpSubBlock As String, ByRef lplpBuffer As Integer, ByRef nVerSize As Integer) As Integer


'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    Public Declare Function GetFileVersionInfo Lib "version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwHandle As Integer, ByVal dwLen As Integer, ByRef lpData As Byte) As Integer
'	Public Function CheckVersion_vbBabel(ByRef sBabelVersion As String) As Boolean
'		' Test if correct version of vbBabel.dll is registered. If not, do so ..
'		On Error GoTo Err_DLL_Not_Registered
'		Dim bRegMyDLLAttempted As Boolean
'		Dim oBabel As New vbBabel.BabelFileHandling
'		Dim response As Short
'		Dim sVersion As String

'		CheckVersion_vbBabel = True

'		' sBabelVersion is like 1.00.31, DLL-version info is like 1.00.00.31
'		' Insert into sBabelVersion to make them equal;
'		sBabelVersion = Left(sBabelVersion, 5) & "00." & Right(sBabelVersion, 2)

'		'The following statement will fail at run-time if vbBabel is not registered.
'		'FIX FIX
'		' remove next line ???? Check version only?????
'		' find another test to check if  Babelbank is installed at all!
'		' check f.ex. Programsmenu!
'		oBabel.BackupPath = "\test"
'		'UPGRADE_NOTE: Object oBabel may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
'		oBabel = Nothing
'		'---

'		If CheckVersion_vbBabel Then
'			' dll is registered, but is it the correct version ?
'			sVersion = GetFileVersion("vbbabel.dll")
'			' 28.12.2007 Update only for newer versions!!!!
'			If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then
'				MsgBox("Oppdaterer registry-informasjon for vbBabel.dll. Fra versjon " & sVersion & " til " & sBabelVersion, MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'				bRegMyDLLAttempted = True
'				' Runs into err_DLL_Registeres if error here
'				RegvbBabelDLL() ' Declared in General-part, see top of module
'			End If
'		End If
'		Exit Function

'Err_DLL_Not_Registered: 
'		' Check to see if error 429 occurs
'		If Err.Number = 429 Then
'			' Run clientinstall ?
'			' A full installset of BabelBank should reside in \ClientInstall
'			' underneath AppPath.

'			' Try to install from here

'			' FIX: Must first modify SETUP.LST, to install to correct location
'			' Correct location is App.path
'			' Setup.lst has a line "DefaultDir=$(ProgramFiles)\BabelBank"
'			' which must be modified to DefaultDir=App.Path, where App.Path
'			' is BabelBank.exe's App.Path
'			' First rename Setup.lst

'			' Then Open the file

'			' Change the conent after DefaultDir

'			' Save to Setup.lst in App.path\ClientInstall

'			' changed 23.08.2007
'			' when using InstallShield, we have no clientinstall folder

'			'    response = MsgBox("BABELBANK M� INSTALLERES F�R BRUK." & vbCrLf & vbCrLf & _
'			''    "Du m� ha Administratorrettigheter for � utf�re dette." & vbCrLf & vbCrLf & _
'			''    "Er du usikker, s� svar Nei, og kontakt din Systemadministrator!" & vbCrLf & vbCrLf & _
'			''    "�nsker du � fortsette med installasjon ?", vbYesNo + vbCritical, "BabelBank installasjon")
'			'    If response = vbYes Then
'			'        MsgBox "Starter installasjon." & vbCrLf & _
'			''        "BabelBank avsluttes, og m� startes igjen manuelt." & vbCrLf & vbCrLf & _
'			''        "Hvis PC-en automatisk stenges, og restartes, er dette" & vbCrLf & _
'			''        "fordi nye systemfiler gj�r en omstart n�dvendig." & vbCrLf & _
'			''        "I s�fall m� du starte installasjonen p� ny, ved enten" & vbCrLf & _
'			''        "� starte BabelBank igjen, eller ved � kj�re SETUP.EXE" & vbCrLf & _
'			''        "fra katalogen 'ClientInstall'", vbInformation
'			'        Shell App.path & "\clientinstall\setup.exe -s install.log", vbNormalFocus
'			'    End If

'			response = MsgBox("BABELBANK M� INSTALLERES F�R BRUK." & vbCrLf & vbCrLf & "Installer BabelBank med det installasjonssett du benyttet forrige gang " & "- Versjon " & sBabelVersion & vbCrLf & vbCrLf & "Programmet avsluttes", MsgBoxResult.OK + MsgBoxStyle.Critical, "BabelBank installasjon")

'			End

'		Else
'			MsgBox("En feil oppsto pga. problemer med vbBabel.dll " & Str(Err.Number) & " " & Err.Description, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
'			CheckVersion_vbBabel = False
'		End If

'	End Function
'	Public Function CheckVersion_bbSetup(ByRef sBabelVersion As String) As Boolean
'		' Test if correct version of bbSetup.dll is registered. If not, do so ..
'		On Error GoTo Err_DLL_Not_Registered
'		Dim bRegMyDLLAttempted As Boolean
'		Dim oSetup As New BabelSetup.Sequence
'		Dim sVersion As String

'		' sBabelVersion is like 1.00.31, DLL-version info is like 1.00.00.31
'		' Insert into sBabelVersion to make them equal;
'		sBabelVersion = Left(sBabelVersion, 5) & "00." & Right(sBabelVersion, 2)

'		CheckVersion_bbSetup = True
'		'The following statement will fail at run-time if bbSetup is not registered.
'		oSetup.Filesetup_ID = 1

'		If CheckVersion_bbSetup Then
'			' dll is registered, but is it the correct version ?
'			sVersion = GetFileVersion("bbSetup.dll")
'			'If sBabelVersion <> sVersion Then
'			' 28.12.2007 Update only for newer versions!!!!
'			If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'				MsgBox("Oppdaterer registry-informasjon for bbSetup.dll", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'				bRegMyDLLAttempted = True
'				' Runs into err_DLL_Registeres if error here
'				RegbbSetupDLL() ' Declared in General-part, see top of module
'			End If
'		End If
'		Exit Function

'		'UPGRADE_NOTE: Object oSetup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
'		oSetup = Nothing

'		Exit Function

'Err_DLL_Not_Registered: 
'		' Check to see if error 429 occurs
'		If Err.Number = 429 Then

'			MsgBox("Pr�ver � registrere bbSetup.dll", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)

'			' bRegMyDLLAttempted is used to determine whether an attempt to register the
'			' ActiveX DLL has already been attempted. This helps to avoid getting stuck
'			' in a loop if the ActiveX DLL cannot be registered for some reason.
'			If bRegMyDLLAttempted Then
'				MsgBox("Greier ikke � registrere bbSetup.dll." & vbCrLf & "Kontakt din Systemadministrator." & vbCrLf & "Du trenger rettigheter til � skrive til Windows Registry!", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
'				CheckVersion_bbSetup = False
'				Resume Next
'			Else
'				RegbbSetupDLL() ' Declared in General-part, see top of module
'				bRegMyDLLAttempted = True
'				'MsgBox "Registration of bbSetup attempted."
'				Resume 
'			End If
'		Else
'			MsgBox("En feil oppsto pga. problemer med bbSetup.dll " & Str(Err.Number) & " " & Err.Description, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)

'			CheckVersion_bbSetup = False
'		End If

'	End Function
'	Public Function CheckVersion_EDI2XML(ByRef sBabelVersion As String) As Boolean
'		' Test if correct version of EDI2XML.dll is registered. If not, do so ..
'		On Error GoTo Err_DLL_Not_Registered
'		Dim bRegMyDLLAttempted As Boolean
'		Dim sVersion As String

'		' sBabelVersion is like 1.00.31, DLL-version info is like 1.00.00.31
'		' Insert into sBabelVersion to make them equal;
'		sBabelVersion = Left(sBabelVersion, 5) & "00." & Right(sBabelVersion, 2)

'		CheckVersion_EDI2XML = True

'		sVersion = GetFileVersion("EDI2XML.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for EDI2XML.dll", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegEDI2XMLDLL() ' Declared in General-part, see top of module
'		End If
'		Exit Function

'Err_DLL_Not_Registered: 
'		' Check to see if error 429 occurs
'		If Err.Number = 429 Then

'			MsgBox("Pr�ver � registrere EDI2XML.dll", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)

'			' bRegMyDLLAttempted is used to determine whether an attempt to register the
'			' ActiveX DLL has already been attempted. This helps to avoid getting stuck
'			' in a loop if the ActiveX DLL cannot be registered for some reason.
'			If bRegMyDLLAttempted Then
'				MsgBox("Greier ikke � registrere EDI2XML.dll." & vbCrLf & "Kontakt din Systemadministrator." & vbCrLf & "Du trenger rettigheter til � skrive til Windows Registry!", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
'				CheckVersion_EDI2XML = False
'				Resume Next
'			Else
'				RegbbSetupDLL() ' Declared in General-part, see top of module
'				bRegMyDLLAttempted = True
'				'MsgBox "Registration of EDI2XML attempted."
'				Resume 
'			End If
'		Else
'			MsgBox("En feil oppsto pga. problemer med EDI2XML.dll " & Str(Err.Number) & " " & Err.Description, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
'			CheckVersion_EDI2XML = False
'		End If

'	End Function

'	Public Function CheckVersion_LanguageDLLs(ByRef sBabelVersion As String) As Boolean
'		' Test if correct version of LanguageDLL's are registered. If not, do so ..
'		On Error GoTo Err_DLL_Not_Registered
'		Dim bRegMyDLLAttempted As Boolean
'		Dim sVersion As String

'		' sBabelVersion is like 1.00.31, DLL-version info is like 1.00.00.31
'		' Insert into sBabelVersion to make them equal;
'		sBabelVersion = Left(sBabelVersion, 5) & "00." & Right(sBabelVersion, 2)

'		CheckVersion_LanguageDLLs = True
'		sVersion = GetFileVersion("Setup414.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for Setup414.DLL", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegSetup414DLL() ' Declared in General-part, see top of module
'		End If
'		sVersion = GetFileVersion("Setup809.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for Setup809.DLL", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegSetup809DLL() ' Declared in General-part, see top of module
'		End If
'		sVersion = GetFileVersion("BabelBank414.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for BabelBank414.DLL", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegBabelBank414DLL() ' Declared in General-part, see top of module
'		End If
'		sVersion = GetFileVersion("BabelBank809.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for BabelBank809.DLL", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegBabelBank809DLL() ' Declared in General-part, see top of module
'		End If
'		sVersion = GetFileVersion("vbBabel414.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for vbBabel414.DLL", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegvbBabel414DLL() ' Declared in General-part, see top of module
'		End If
'		sVersion = GetFileVersion("vbBabel809.dll")
'		'If sBabelVersion <> sVersion Then
'		' 28.12.2007 Update only for newer versions!!!!
'		If Val(Replace(sBabelVersion, ".", "")) > Val(Replace(sVersion, ".", "")) Then

'			MsgBox("Oppdaterer registry-informasjon for vbBabel809.DLL", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
'			bRegMyDLLAttempted = True
'			' Runs into err_DLL_Registeres if error here
'			RegvbBabel809DLL() ' Declared in General-part, see top of module
'		End If
'		Exit Function

'Err_DLL_Not_Registered: 
'		' Check to see if error 429 occurs
'		If Err.Number = 429 Then

'			MsgBox("Pr�ver � registrere spr�k DLL-er", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)

'			' bRegMyDLLAttempted is used to determine whether an attempt to register the
'			' ActiveX DLL has already been attempted. This helps to avoid getting stuck
'			' in a loop if the ActiveX DLL cannot be registered for some reason.
'			If bRegMyDLLAttempted Then
'				MsgBox("Greier ikke � registrere spr�k DLL-er." & vbCrLf & "Kontakt din Systemadministrator." & vbCrLf & "Du trenger rettigheter til � skrive til Windows Registry!", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
'				CheckVersion_LanguageDLLs = False
'				Resume Next
'			End If
'		Else
'			MsgBox("En feil oppsto pga. problemer med spr�k DLL-er " & Str(Err.Number) & " " & Err.Description, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
'			CheckVersion_LanguageDLLs = False
'		End If

'	End Function

'	Public Function GetFileVersion(ByRef sDriverFile As String) As String

'		Dim FI As VS_FIXEDFILEINFO
'		Dim sBuffer() As Byte
'		Dim nBufferSize As Integer
'		Dim lpBuffer As Integer
'		Dim nVerSize As Integer
'		Dim nUnused As Integer
'		Dim tmpVer As String

'		'GetFileVersionInfoSize determines whether the operating
'		'system can obtain version information about a specified
'		'file. If version information is available, it returns
'		'the size in bytes of that information. As with other
'		'file installation functions, GetFileVersionInfoSize
'		'works only with Win32 file images.
'		'
'		'A empty variable must be passed as the second
'		'parameter, which the call returns 0 in.
'		nBufferSize = GetFileVersionInfoSize(sDriverFile, nUnused)

'		If nBufferSize > 0 Then

'			'create a buffer to receive file-version
'			'(FI) information.
'			ReDim sBuffer(nBufferSize)
'			Call GetFileVersionInfo(sDriverFile, 0, nBufferSize, sBuffer(0))

'			'VerQueryValue function returns selected version info
'			'from the specified version-information resource. Grab
'			'the file info and copy it into the  VS_FIXEDFILEINFO structure.
'			Call VerQueryValue(sBuffer(0), "\", lpBuffer, nVerSize)
'			'UPGRADE_WARNING: Couldn't resolve default property of object FI. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
'			Call CopyMemory(FI, lpBuffer, Len(FI))

'			'extract the file version from the FI structure
'            tmpVer = VB6.Format(HiWord(FI.dwFileVersionMS)) & "." & VB6.Format(LoWord(FI.dwFileVersionMS), "00") & "."

'            If FI.dwFileVersionLS > 0 Then
'                tmpVer = tmpVer & VB6.Format(HiWord(FI.dwFileVersionLS), "00") & "." & VB6.Format(LoWord(FI.dwFileVersionLS), "00")
'            Else
'                tmpVer = tmpVer & VB6.Format(FI.dwFileVersionLS, "0000")
'            End If

'		End If

'		GetFileVersion = tmpVer

'	End Function
'	Public Function HiWord(ByRef dw As Integer) As Integer

'		If dw And &H80000000 Then
'			HiWord = (dw \ 65535) - 1
'		Else : HiWord = dw \ 65535
'		End If

'	End Function
'	Public Function LoWord(ByRef dw As Integer) As Integer

'		If dw And &H8000 Then
'			LoWord = &H8000 Or (dw And &H7FFF)
'		Else : LoWord = dw And &HFFFF
'		End If

'	End Function

'--end block--'
'End Module
