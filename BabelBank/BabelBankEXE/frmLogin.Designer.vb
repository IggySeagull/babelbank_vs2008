<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLogin
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtUID As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtPWD As System.Windows.Forms.TextBox
	Public WithEvents _lblUID_0 As System.Windows.Forms.Label
	Public WithEvents _lblPWD_1 As System.Windows.Forms.Label
	Public WithEvents lblPWD As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents lblUID As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLogin))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtUID = New System.Windows.Forms.TextBox
		Me.cmdOK = New System.Windows.Forms.Button
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.txtPWD = New System.Windows.Forms.TextBox
		Me._lblUID_0 = New System.Windows.Forms.Label
		Me._lblPWD_1 = New System.Windows.Forms.Label
		Me.lblPWD = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.lblUID = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.lblPWD, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.lblUID, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Login"
		Me.ClientSize = New System.Drawing.Size(250, 103)
		Me.Location = New System.Drawing.Point(189, 232)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLogin"
		Me.txtUID.AutoSize = False
		Me.txtUID.Size = New System.Drawing.Size(155, 23)
		Me.txtUID.Location = New System.Drawing.Point(86, 9)
		Me.txtUID.TabIndex = 0
		Me.txtUID.AcceptsReturn = True
		Me.txtUID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUID.BackColor = System.Drawing.SystemColors.Window
		Me.txtUID.CausesValidation = True
		Me.txtUID.Enabled = True
		Me.txtUID.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUID.HideSelection = True
		Me.txtUID.ReadOnly = False
		Me.txtUID.Maxlength = 0
		Me.txtUID.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUID.MultiLine = False
		Me.txtUID.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUID.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUID.TabStop = True
		Me.txtUID.Visible = True
		Me.txtUID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtUID.Name = "txtUID"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(76, 26)
		Me.cmdOK.Location = New System.Drawing.Point(33, 68)
		Me.cmdOK.TabIndex = 4
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(76, 26)
		Me.cmdCancel.Location = New System.Drawing.Point(140, 68)
		Me.cmdCancel.TabIndex = 5
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.txtPWD.AutoSize = False
		Me.txtPWD.Size = New System.Drawing.Size(155, 23)
		Me.txtPWD.IMEMode = System.Windows.Forms.ImeMode.Disable
		Me.txtPWD.Location = New System.Drawing.Point(86, 35)
		Me.txtPWD.PasswordChar = ChrW(42)
		Me.txtPWD.TabIndex = 1
		Me.txtPWD.AcceptsReturn = True
		Me.txtPWD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPWD.BackColor = System.Drawing.SystemColors.Window
		Me.txtPWD.CausesValidation = True
		Me.txtPWD.Enabled = True
		Me.txtPWD.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPWD.HideSelection = True
		Me.txtPWD.ReadOnly = False
		Me.txtPWD.Maxlength = 0
		Me.txtPWD.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPWD.MultiLine = False
		Me.txtPWD.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPWD.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPWD.TabStop = True
		Me.txtPWD.Visible = True
		Me.txtPWD.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPWD.Name = "txtPWD"
		Me._lblUID_0.Text = "&User Name:"
		Me._lblUID_0.Size = New System.Drawing.Size(72, 18)
		Me._lblUID_0.Location = New System.Drawing.Point(7, 10)
		Me._lblUID_0.TabIndex = 2
		Me._lblUID_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblUID_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblUID_0.Enabled = True
		Me._lblUID_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblUID_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblUID_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblUID_0.UseMnemonic = True
		Me._lblUID_0.Visible = True
		Me._lblUID_0.AutoSize = False
		Me._lblUID_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblUID_0.Name = "_lblUID_0"
		Me._lblPWD_1.Text = "&Password:"
		Me._lblPWD_1.Size = New System.Drawing.Size(72, 18)
		Me._lblPWD_1.Location = New System.Drawing.Point(7, 36)
		Me._lblPWD_1.TabIndex = 3
		Me._lblPWD_1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblPWD_1.BackColor = System.Drawing.SystemColors.Control
		Me._lblPWD_1.Enabled = True
		Me._lblPWD_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblPWD_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblPWD_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblPWD_1.UseMnemonic = True
		Me._lblPWD_1.Visible = True
		Me._lblPWD_1.AutoSize = False
		Me._lblPWD_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblPWD_1.Name = "_lblPWD_1"
		Me.Controls.Add(txtUID)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(txtPWD)
		Me.Controls.Add(_lblUID_0)
		Me.Controls.Add(_lblPWD_1)
		Me.lblPWD.SetIndex(_lblPWD_1, CType(1, Short))
		Me.lblUID.SetIndex(_lblUID_0, CType(0, Short))
		CType(Me.lblUID, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.lblPWD, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
