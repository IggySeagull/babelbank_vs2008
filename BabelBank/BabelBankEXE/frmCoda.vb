Option Strict Off
Option Explicit On

Friend Class frmCoda
	Inherits System.Windows.Forms.Form
	
    Private oCodaDAL As vbBabel.DAL = Nothing
    Private sMySQL As String
	Private sUsrCode As String
	Private scmpCode As String
	Private spcmCode As String
	Private sprlcode As String
	Public bContinue As Boolean
	Public Function GetUsrCode() As String
		GetUsrCode = sUsrCode
	End Function
	Public Function GetcmpCode() As String
		GetcmpCode = scmpCode
	End Function
	Public Function GetpcmCode() As String
		GetpcmCode = spcmCode
	End Function
	Public Function GetprlCode() As String
		GetprlCode = sprlcode
	End Function
	'UPGRADE_WARNING: Event cmbUser.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbUser_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbUser.SelectedIndexChanged
		
        Try
            If Not EmptyString((Me.cmbUser.Text)) Then
                'sUsrCode = VB6.GetItemString(Me.cmbUser, Me.cmbUser.SelectedIndex)
                '17.07.2019
                sUsrCode = Me.cmbUser.Text

                sMySQL = "SELECT DISTINCT cmpcode FROM oas_dttheader WHERE usrcode='" & sUsrCode & "'"
                oCodaDAL.SQL = sMySQL
                If oCodaDAL.Reader_Execute() Then

                    Me.cmbCompany.Items.Clear()

                    Do While oCodaDAL.Reader_ReadRecord

                        Me.cmbCompany.Items.Add(oCodaDAL.Reader_GetString("cmpcode"))

                    Loop

                Else

                    Throw New Exception(LRSCommon(45002) & oCodaDAL.ErrorMessage)

                End If

            End If

        Catch ex As Exception

            MsgBox("Function: cmbUser_SelectedIndexChanged" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)

        End Try
    End Sub
	'UPGRADE_WARNING: Event cmbCompany.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbCompany_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCompany.SelectedIndexChanged
		
        Try
            If Not EmptyString((Me.cmbCompany.Text)) Then
                'scmpCode = VB6.GetItemString(Me.cmbCompany, Me.cmbCompany.SelectedIndex)
                ' 17.07.2019
                scmpCode = Me.cmbCompany.Text

                sMySQL = "SELECT DISTINCT pcmcode FROM oas_dttheader WHERE usrcode='"
                sMySQL = sMySQL & sUsrCode & "' AND cmpcode = '" & scmpCode & "'"
                oCodaDAL.SQL = sMySQL
                If oCodaDAL.Reader_Execute() Then

                    Me.cmbType.Items.Clear()

                    Do While oCodaDAL.Reader_ReadRecord

                        Me.cmbType.Items.Add(oCodaDAL.Reader_GetString("pcmcode"))

                    Loop

                Else

                    Throw New Exception(LRSCommon(45002) & oCodaDAL.ErrorMessage)

                End If

            End If

        Catch ex As Exception

            MsgBox("Function: cmbCompany_SelectedIndexChanged" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)

        End Try

    End Sub
	'UPGRADE_WARNING: Event cmbType.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbType_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbType.SelectedIndexChanged
		
        Try
            If Not EmptyString((Me.cmbType.Text)) Then
                'spcmCode = VB6.GetItemString(Me.cmbType, Me.cmbType.SelectedIndex)
                ' 17.07.2019
                spcmCode = Me.cmbType.Text

                sMySQL = "SELECT prlcode FROM oas_dttheader WHERE usrcode='"
                sMySQL = sMySQL & sUsrCode & "' AND cmpcode = '" & scmpCode
                sMySQL = sMySQL & "' AND pcmcode = '" & spcmCode & "'"
                oCodaDAL.SQL = sMySQL
                If oCodaDAL.Reader_Execute() Then

                    Me.cmbDataset.Items.Clear()

                    Do While oCodaDAL.Reader_ReadRecord

                        Me.cmbDataset.Items.Add(oCodaDAL.Reader_GetString("prlcode"))

                    Loop

                Else

                    Throw New Exception(LRSCommon(45002) & oCodaDAL.ErrorMessage)

                End If

            End If

        Catch ex As Exception

            MsgBox("Function: cmbType_SelectedIndexChanged" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)

        End Try

    End Sub
	'UPGRADE_WARNING: Event cmbDataset.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbDataset_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbDataset.SelectedIndexChanged
		
		If Not EmptyString((Me.cmbDataset.Text)) Then
            'sprlcode = VB6.GetItemString(Me.cmbDataset, Me.cmbDataset.SelectedIndex)
            ' 17.07.2019
            sprlcode = Me.cmbDataset.Text
		End If
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bContinue = False
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		bContinue = True
		Me.Hide()
	End Sub
	
	Private Sub frmCoda_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		'FormvbStyle Me, "dollar.jpg"
		FormLRSCaptions(Me)
		
	End Sub
    Public Function SetConnection(ByRef DAL As vbBabel.DAL)
        'We get an already opened connection from the calling procedure
        oCodaDAL = DAL
    End Function
End Class
