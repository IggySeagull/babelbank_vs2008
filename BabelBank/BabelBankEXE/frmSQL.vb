﻿Imports System.Data.SqlClient
Public Class frmSQL

    'Private Sub BindGrid() Handles cmdSELECT.Click
  
    'End Sub
    Private Sub cmdSaveCallstack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSELECT.Click

        ''Dim constring As String = "Data Source=.\SQL2005;Initial Catalog=Northwind;User id = sa;password=pass@123"
        'Dim constring As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\projects\babelbank\profile.mdb"
        'Using con As New SqlConnection()
        '    con.ConnectionString = FindBBConnectionString()

        '    Using cmd As New SqlCommand("SELECT * FROM Format", con)
        '        cmd.CommandType = CommandType.Text
        '        Using sda As New SqlDataAdapter(cmd)
        '            Using dt As New DataTable()
        '                sda.Fill(dt)
        '                gridResult.DataSource = dt
        '            End Using
        '        End Using
        '    End Using
        'End Using

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iNoOfItems As Integer = 0
        Dim lRecord As Long = 0
        Dim lItem As Long
        Dim aResultArray(,) As String
        Dim sHeadings As String
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim iCounter As Integer
        Dim iSeperator As Integer
        Dim sHeading As String
        Dim iColCounter As Integer
        Dim iRowCounter As Integer
        'Dim iRightJustifyColumn As Integer



        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If
        oMyDal.ShowErrorMessage = True

        oMyDal.SQL = Me.txtQuery.Text
        oMyDal.ShowErrorMessage = True

        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then

                iNoOfItems = oMyDal.Reader_FieldCount
                lRecord = -1
                Do While oMyDal.Reader_ReadRecord
                    lRecord = lRecord + 1
                    ReDim Preserve aResultArray(iNoOfItems, lRecord)
                    For lItem = 0 To iNoOfItems
                        aResultArray(lItem, lRecord) = oMyDal.Reader_GetField(lItem)
                        'aResultArray(lItem, lRecord) = rsQuery.Fields.Item(lItem).Value
                        If lItem = 0 Then
                            sHeadings = oMyDal.Reader_GetFieldName(lItem)
                        Else
                            sHeadings = sHeadings & "," & oMyDal.Reader_GetFieldName(lItem)
                        End If
                    Next lItem
                Loop
                sHeadings = sHeadings & ","

                With Me.gridResult
                    .SuspendLayout()
                    .Columns.Clear()
                    .ScrollBars = ScrollBars.Both
                    .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                    ' not possible to add rows manually!
                    .AllowUserToAddRows = False
                    .RowHeadersVisible = False
                    .BorderStyle = BorderStyle.None
                    .RowTemplate.Height = 16
                    .BackgroundColor = Color.White
                    .CellBorderStyle = DataGridViewCellBorderStyle.None
                    ' 22.05.2017
                    '.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                    .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
                    .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None

                    For iCounter = 1 To iNoOfItems + 1
                        ' add columns
                        iSeperator = InStr(1, sHeadings, ",")
                        If iSeperator > 0 Then
                            sHeading = sHeadings.Substring(0, iSeperator - 1).Trim '  Trim(Left(sHeadings, iSeperator - 1))
                            sHeadings = sHeadings.Substring(iSeperator) 'Mid(sHeadings, iSeperator + 1)
                            '.Col = lCounter
                            '.Text = sHeading
                            txtColumn = New DataGridViewTextBoxColumn
                            txtColumn.HeaderText = sHeading
                            txtColumn.DisplayIndex = iCounter - 1
                            .Columns.Add(txtColumn)
                        Else
                            Exit For
                        End If
                    Next iCounter

                    Dim myRow As DataGridViewRow
                    ' Fill up from the array
                    For iRowCounter = 0 To aResultArray.GetUpperBound(1)  ' UBound(aResultArray, 2) 'lArrayLength

                        .Rows.Add()
                        myRow = .Rows(.RowCount - 1)

                        For iColCounter = 0 To aResultArray.GetUpperBound(0)  'UBound(aResultArray, 1)
                            myRow.Cells(iColCounter).Value = aResultArray(iColCounter, iRowCounter)
                        Next iColCounter

                    Next iRowCounter
                    .ResumeLayout()
                    .Refresh()

                    '22.05.2017
                    .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                End With
                Me.gridResult.Visible = True

            Else
                MsgBox("No records found", MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
            End If
            'Else
            ' No need to add an extra errormsg
            '    Throw New Exception(LRSCommon(45002) & oMyERPDal.ErrorMessage)
        End If

        oMyDal.Close()
        oMyDal = Nothing

    End Sub

    Private Sub cmdOthers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOthers.Click
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If
            oMyDal.ShowErrorMessage = True

            sMySQL = Me.txtQuery.Text
            If Not EmptyString(sMySQL) Then
                oMyDal.SQL = sMySQL
                oMyDal.ExecuteNonQuery()
                If oMyDal.RecordsAffected > 0 Then
                    MsgBox(LRS(15039, oMyDal.RecordsAffected), MsgBoxStyle.OkOnly)
                Else
                    MsgBox(LRS(15040), MsgBoxStyle.OkOnly)
                End If
            End If

            oMyDal.Close()
            oMyDal = Nothing

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)

        End Try

    End Sub

    Private Sub frmSQL_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormLRSCaptions(Me)
        FormvbStyle(Me, "dollar.jpg", 500)

    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub
End Class