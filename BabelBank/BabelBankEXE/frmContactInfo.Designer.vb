<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmContactInfo
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblAdress As System.Windows.Forms.Label
	Public WithEvents imgMail As System.Windows.Forms.PictureBox
	Public WithEvents lblMail As System.Windows.Forms.Label
	Public WithEvents lblText2 As System.Windows.Forms.Label
	Public WithEvents lblPhone As System.Windows.Forms.Label
	Public WithEvents lblText As System.Windows.Forms.Label
	Public WithEvents imgPhone As System.Windows.Forms.PictureBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmContactInfo))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblAdress = New System.Windows.Forms.Label
        Me.imgMail = New System.Windows.Forms.PictureBox
        Me.lblMail = New System.Windows.Forms.Label
        Me.lblText2 = New System.Windows.Forms.Label
        Me.lblPhone = New System.Windows.Forms.Label
        Me.lblText = New System.Windows.Forms.Label
        Me.imgPhone = New System.Windows.Forms.PictureBox
        CType(Me.imgMail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPhone, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(172, 274)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblAdress
        '
        Me.lblAdress.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdress.Location = New System.Drawing.Point(91, 234)
        Me.lblAdress.Name = "lblAdress"
        Me.lblAdress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdress.Size = New System.Drawing.Size(321, 25)
        Me.lblAdress.TabIndex = 5
        Me.lblAdress.Text = "Visual Banking AS, Stockholmgata 12, 0566 Oslo, Norway"
        '
        'imgMail
        '
        Me.imgMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgMail.Image = CType(resources.GetObject("imgMail.Image"), System.Drawing.Image)
        Me.imgMail.Location = New System.Drawing.Point(32, 154)
        Me.imgMail.Name = "imgMail"
        Me.imgMail.Size = New System.Drawing.Size(32, 32)
        Me.imgMail.TabIndex = 6
        Me.imgMail.TabStop = False
        '
        'lblMail
        '
        Me.lblMail.BackColor = System.Drawing.SystemColors.Control
        Me.lblMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMail.Location = New System.Drawing.Point(91, 194)
        Me.lblMail.Name = "lblMail"
        Me.lblMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMail.Size = New System.Drawing.Size(249, 33)
        Me.lblMail.TabIndex = 4
        Me.lblMail.Text = "support@visualbanking.net"
        '
        'lblText2
        '
        Me.lblText2.BackColor = System.Drawing.SystemColors.Control
        Me.lblText2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblText2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblText2.Location = New System.Drawing.Point(91, 157)
        Me.lblText2.Name = "lblText2"
        Me.lblText2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblText2.Size = New System.Drawing.Size(297, 33)
        Me.lblText2.TabIndex = 3
        Me.lblText2.Text = "Supportmail:"
        '
        'lblPhone
        '
        Me.lblPhone.BackColor = System.Drawing.SystemColors.Control
        Me.lblPhone.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPhone.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPhone.Location = New System.Drawing.Point(92, 119)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPhone.Size = New System.Drawing.Size(153, 33)
        Me.lblPhone.TabIndex = 2
        Me.lblPhone.Text = "(+47) 23 18 36 00"
        '
        'lblText
        '
        Me.lblText.BackColor = System.Drawing.SystemColors.Control
        Me.lblText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblText.Location = New System.Drawing.Point(91, 82)
        Me.lblText.Name = "lblText"
        Me.lblText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblText.Size = New System.Drawing.Size(297, 33)
        Me.lblText.TabIndex = 1
        Me.lblText.Text = "Supporttelefon Visual Banking AS"
        '
        'imgPhone
        '
        Me.imgPhone.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPhone.Image = CType(resources.GetObject("imgPhone.Image"), System.Drawing.Image)
        Me.imgPhone.Location = New System.Drawing.Point(32, 82)
        Me.imgPhone.Name = "imgPhone"
        Me.imgPhone.Size = New System.Drawing.Size(32, 32)
        Me.imgPhone.TabIndex = 7
        Me.imgPhone.TabStop = False
        '
        'frmContactInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(423, 314)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblAdress)
        Me.Controls.Add(Me.imgMail)
        Me.Controls.Add(Me.lblMail)
        Me.Controls.Add(Me.lblText2)
        Me.Controls.Add(Me.lblPhone)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.imgPhone)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmContactInfo"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Support"
        CType(Me.imgMail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPhone, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
