﻿Imports System.Windows.Forms
Imports System.IO
Public Class frmBB_Error
    Private sMessage As String
    Private aFilesArray As String(,)
    Private bMailSent As Boolean = False
    Private sSystemInfo As String = ""

    Public Sub PassFileArray(ByVal aF As String(,))
        aFilesArray = aF
    End Sub
    Public Sub PassMsg(ByVal sMsg As String)
        sMessage = sMsg
    End Sub
    Public Sub PassSystemInfo(ByVal sMsg As String)
        sSystemInfo = sMsg
    End Sub
    Private Sub frmBB_Error_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        ' must manually resize lblErrorContent based on its size

        'Added code using the variable bCheckIfVisma
        'If an error is raised in the function LicRead the call to IsThisVismaIntegrator
        ' once again creates an instance of vbBabel.License and thereby running LicRead and
        ' therefore raiseing the same error once again and there you go around and around.
        'Therefore don't call the function IsThisVismaIntegrator if the initial error was raised
        ' in the function LicRead.
        Dim bCheckIfVisma As Boolean = True

        FormLRSCaptions(Me)

        If Not EmptyString(Me.txtErrorMsgContent.Text) Then
            If Me.txtErrorMsgContent.Text.Length > 17 Then
                If Me.txtErrorMsgContent.Text.Substring(0, 18) = "FunctionX: LicRead" Then
                    bCheckIfVisma = False
                End If
            End If
        End If
        ' for VismaPaymentBabelBank - do not show button Supportmail
        If bCheckIfVisma Then
            If IsThisVismaIntegrator() Then
				Me.cmdEMail.Visible = False
				' 22.09.2021 - do not send mail in Visma cases !!!!
				bMailSent = True
			End If
        End If

    End Sub
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK

        '21.08.2018 - Added the question regarding sending of supportmail to within the errorhandling
        If Not bMailSent Then
            If MsgBox(LRS(60070), MsgBoxStyle.YesNo + MsgBoxStyle.Question, LRS(60040)) = MsgBoxResult.Yes Then
                'HelpSupportmail_Click(HelpSupportmail, New System.EventArgs(), aF, sMsg)
                SendSupportMail(aFilesArray, sMessage, Me.lblCallstack.Text)
            End If
        End If

        Me.Close()
    End Sub

    Private Sub cmdEMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEMail.Click
        'frmStart.SupportMail(aErrorArray, sMessage)

        'Systeminfo is added as standard in SendSupportMail
        SendSupportMail(aFilesArray, sMessage, Me.lblCallstack.Text)
        bMailSent = True
    End Sub
    Private Sub cmdSaveCallstack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveCallstack.Click
        ' Save callstack to a textfile, with filesave dialog
        Dim myStream As Stream
        Dim saveFileDialog1 As New SaveFileDialog()

        saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
        saveFileDialog1.FilterIndex = 2
        saveFileDialog1.RestoreDirectory = True

        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
            myStream = saveFileDialog1.OpenFile()

            Using sw As StreamWriter = New StreamWriter(myStream)
                ' save callstack to the fil
                sw.WriteLine(DateTime.Now)
                sw.WriteLine(Me.lblSourceContent.Text)
                If Not Me.lblErrorNoContent.Text = "0" Then
                    sw.WriteLine(Me.lblErrorNoContent.Text)
                End If
                sw.WriteLine(Me.txtErrorMsgContent.Text)
                If Not EmptyString(Me.lblCallstack.Text) Then
                    If Me.lblCallstack.Text.Length > 20 Then
                        sw.WriteLine("")
                        sw.WriteLine("Callstack:")
                        sw.Write(Me.lblCallstack.Text)
                    End If
                End If

                sw.Close()
            End Using

            If (myStream IsNot Nothing) Then
                ' Code to write the stream goes here.
                myStream.Close()
            End If
        End If

    End Sub

End Class
