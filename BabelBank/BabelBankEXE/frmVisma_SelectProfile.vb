Option Strict Off
Option Explicit On
Friend Class frmVisma_SelectProfile
	Inherits System.Windows.Forms.Form
    Dim sClientNumber As String
	Dim sBankPartnerID As String
	Dim sAccountNumber As String
	Dim iSelected As Short
	Private Sub frmVisma_SelectProfile_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormLRSCaptions(Me)
		FormVismaStyle(Me)
		iSelected = 1
    End Sub
	Public Sub SetClientNumber(ByRef sCN As String)
		Dim sCompanyNumber As Object
        sCompanyNumber = sCN
		Me.txtClientNumber.Text = sCN
	End Sub
	Public Sub SetBankPartnerID(ByRef sBP As String)
		sBankPartnerID = sBP
		Me.txtBankPartner.Text = sBP
	End Sub
	Public Sub SetAccountNumber(ByRef sAC As String)
		sAccountNumber = sAC
		Me.txtAccountNumber.Text = sAC
	End Sub
	Public Function GetSelected() As Short
		' return selected listboxvalue to calling procedure
		GetSelected = iSelected
	End Function
    Public Function GetSelectedProfileGroupName() As String
        ' return selected listboxvalue to calling procedure
        GetSelectedProfileGroupName = Me.lstProfiles.Text
    End Function
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        iSelected = -1
        Me.Hide()
    End Sub
    Private Sub cmdSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelect.Click
        iSelected = lstProfiles.SelectedIndex
        Me.Hide()
    End Sub
End Class