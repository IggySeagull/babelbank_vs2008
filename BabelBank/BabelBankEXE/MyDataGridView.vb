﻿Public Class MyDataGridView
    Inherits DataGridView

    Protected Overloads Overrides Function ProcessDialogKey(ByVal keyData As Keys) As Boolean
        If keyData = Keys.Enter Then
            MyBase.ProcessTabKey(Keys.Tab)
            Return True
        End If
        Return MyBase.ProcessDialogKey(keyData)
    End Function

    Protected Overloads Overrides Function ProcessDataGridViewKey(ByVal e As KeyEventArgs) As Boolean
        If e.KeyCode = Keys.Enter Then
            MyBase.ProcessTabKey(Keys.Tab)
            Return True
        End If
        Return MyBase.ProcessDataGridViewKey(e)
    End Function

End Class
Public Class gridMatchedSort

    ' Usage:
    '
    '     Dim oGridMatchedSort As New gridMatchedSort
    '        oGridMatchedSort.SetSortOrder = <SortOrder.Ascending> or <SortOrder.Descending>
    '        oGridMatchedSort.SortCol1 = <first sort column> . f.ex. lColMatchedMyField2
    '        oGridMatchedSort.SortCol2 = <second sort column> . f.ex. lColMatchedExternalClientNo
    '        oGridMatchedSort.SortCol3 = <third sort column> . f.ex. lColMatchedCustomerNo
    '        Me.gridMatched.Sort(oGridMatchedSort)

    Implements System.Collections.IComparer
    Private myGrid As DataGridView
    Private sortOrderModifier As Integer = 1
    ' 05.10.2017 added an extra column 37, for sorting only, to keep 1. and 2 last rows correct
    Private lSortCol37, lSortCol1, lSortCol2, lSortCol3 As Integer
    Private lMaxRow As Long = 0
    Dim sInitialSort1ValueRow0 As String = ""
    Dim sInitialSort1ValueRowMax2 As String = ""
    Dim sInitialSort1ValueRowMax1 As String = ""

    Public Sub New()
        lSortCol1 = -1
        lSortCol2 = -1
        lSortCol3 = -1
        lSortCol37 = 37

    End Sub
    Public WriteOnly Property SetSortOrder() As SortOrder
        Set(ByVal Value As SortOrder)
            Dim l As Long = 0
            If Value = SortOrder.Descending Then
                sortOrderModifier = -1
                ' set initial values for sortingcolumn 37
                myGrid.Rows(0).Cells(lSortCol37).Value = "9"
                myGrid.Rows(myGrid.RowCount - 2).Cells(lSortCol37).Value = "1"
                myGrid.Rows(myGrid.RowCount - 1).Cells(lSortCol37).Value = "0"
            ElseIf Value = SortOrder.Ascending Then
                sortOrderModifier = 1
                ' set initial values for sortingcolumn 37
                myGrid.Rows(0).Cells(lSortCol37).Value = "0"
                myGrid.Rows(myGrid.RowCount - 2).Cells(lSortCol37).Value = "8"
                myGrid.Rows(myGrid.RowCount - 1).Cells(lSortCol37).Value = "9"
            End If
            ' 29.11.2018 - and fill all rows "in between" with "2"
            For l = 1 To myGrid.RowCount - 3
                myGrid.Rows(l).Cells(lSortCol37).Value = "2"
            Next
        End Set
    End Property
    Public WriteOnly Property SetmyGrid() As DataGridView
        Set(ByVal Value As DataGridView)
            myGrid = Value
            lMaxRow = myGrid.RowCount

        End Set
    End Property
    Public Property SortCol1() As Integer
        Get
            SortCol1 = lSortCol1
        End Get
        Set(ByVal Value As Integer)
            lSortCol1 = Value
        End Set
    End Property
    Public Property SortCol2() As Integer
        Get
            SortCol2 = lSortCol2
        End Get
        Set(ByVal Value As Integer)
            lSortCol2 = Value
        End Set
    End Property
    Public Property SortCol3() As Integer
        Get
            SortCol3 = lSortCol3
        End Get
        Set(ByVal Value As Integer)
            lSortCol3 = Value
        End Set
    End Property
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer _
            Implements System.Collections.IComparer.Compare

        '- gjenstår:
        '    - sørg for at rad 0 alltid kommer øverst
        '    - sørg for at de to nederste radene alltid kommer nederst, uten å fylle verdier i MyField2

        Dim DataGridViewRow1 As DataGridViewRow = CType(x, DataGridViewRow)
        Dim DataGridViewRow2 As DataGridViewRow = CType(y, DataGridViewRow)
        Dim CompareResult As Integer = 0

        ' only 1.st and 2 last rows are filled with values initially in col 37
        ' 29.11.2018 - all rows col 37 are filled initially
        'If DataGridViewRow1.Cells(lSortCol37).Value Is Nothing Then
        '    DataGridViewRow1.Cells(lSortCol37).Value = "2"
        'End If
        'If DataGridViewRow2.Cells(lSortCol37).Value Is Nothing Then
        '    DataGridViewRow2.Cells(lSortCol37).Value = "2"
        'End If

        ' always sort first on column 37 - to keep 1.st and 2 last rows intact
        CompareResult = System.String.Compare( _
       DataGridViewRow1.Cells(lSortCol37).Value.ToString(), _
       DataGridViewRow2.Cells(lSortCol37).Value.ToString())


        ' Then, sort on first column 
        If CompareResult = 0 And lSortCol1 > -1 Then
            'CompareResult = System.String.Compare( _
            '   IIf(DataGridViewRow1.Cells(lSortCol1).Value Is Nothing, "0", DataGridViewRow1.Cells(lSortCol1).Value.ToString()), _
            '  IIf(DataGridViewRow2.Cells(lSortCol1).Value Is Nothing, "0", DataGridViewRow2.Cells(lSortCol1).Value.ToString()))
            ' problems with Nothing values;
            If DataGridViewRow1.Cells(lSortCol1).Value Is Nothing And DataGridViewRow2.Cells(lSortCol1).Value Is Nothing Then
                CompareResult = System.String.Compare("", "")
            ElseIf DataGridViewRow1.Cells(lSortCol1).Value Is Nothing Then
                CompareResult = System.String.Compare("", DataGridViewRow2.Cells(lSortCol1).Value.ToString())
            ElseIf DataGridViewRow2.Cells(lSortCol1).Value Is Nothing Then
                CompareResult = System.String.Compare(DataGridViewRow1.Cells(lSortCol1).Value.ToString(), "")
            Else
                'CompareResult = System.String.Compare(DataGridViewRow1.Cells(lSortCol1).Value.ToString(), DataGridViewRow2.Cells(lSortCol1).Value.ToString())
                CompareResult = System.String.Compare(PadLeft(DataGridViewRow1.Cells(lSortCol1).Value.ToString(), 30, "0"), PadLeft(DataGridViewRow2.Cells(lSortCol1).Value.ToString(), 30, "0"))
            End If

            ' then sort on second column
            If CompareResult = 0 And lSortCol2 > -1 Then
                ' problems with Nothing values;
                If DataGridViewRow1.Cells(lSortCol2).Value Is Nothing And DataGridViewRow2.Cells(lSortCol2).Value Is Nothing Then
                    CompareResult = System.String.Compare("", "")
                ElseIf DataGridViewRow1.Cells(lSortCol2).Value Is Nothing Then
                    CompareResult = System.String.Compare("", DataGridViewRow2.Cells(lSortCol2).Value.ToString())
                ElseIf DataGridViewRow2.Cells(lSortCol2).Value Is Nothing Then
                    CompareResult = System.String.Compare(DataGridViewRow1.Cells(lSortCol2).Value.ToString(), "")
                Else
                    CompareResult = System.String.Compare(PadLeft(DataGridViewRow1.Cells(lSortCol2).Value.ToString(), 30, "0"), PadLeft(DataGridViewRow2.Cells(lSortCol2).Value.ToString(), 30, "0"))
                End If

                'CompareResult = System.String.Compare( _
                '    IIf(DataGridViewRow1.Cells(lSortCol2).Value Is Nothing, "0", DataGridViewRow1.Cells(lSortCol2).Value.ToString()), _
                '    IIf(DataGridViewRow2.Cells(lSortCol2).Value Is Nothing, "0", DataGridViewRow2.Cells(lSortCol2).Value.ToString()))
            End If

                ' then sort on third column
                If CompareResult = 0 And lSortCol3 > -1 Then
                'CompareResult = System.String.Compare( _
                '   IIf(DataGridViewRow1.Cells(lSortCol3).Value Is Nothing, "2", DataGridViewRow1.Cells(lSortCol3).Value.ToString()), _
                '  IIf(DataGridViewRow2.Cells(lSortCol3).Value Is Nothing, "2", DataGridViewRow2.Cells(lSortCol3).Value.ToString()))
                ' problems with Nothing values;
                If DataGridViewRow1.Cells(lSortCol3).Value Is Nothing And DataGridViewRow2.Cells(lSortCol3).Value Is Nothing Then
                    CompareResult = System.String.Compare("", "")
                ElseIf DataGridViewRow1.Cells(lSortCol3).Value Is Nothing Then
                    CompareResult = System.String.Compare("", DataGridViewRow2.Cells(lSortCol3).Value.ToString())
                ElseIf DataGridViewRow2.Cells(lSortCol3).Value Is Nothing Then
                    CompareResult = System.String.Compare(DataGridViewRow1.Cells(lSortCol3).Value.ToString(), "")
                Else
                    CompareResult = System.String.Compare(PadLeft(DataGridViewRow1.Cells(lSortCol3).Value.ToString(), 30, "0"), PadLeft(DataGridViewRow2.Cells(lSortCol3).Value.ToString(), 30, "0"))
                End If

                End If
            End If

            Return CompareResult * sortOrderModifier
    End Function
    
End Class
