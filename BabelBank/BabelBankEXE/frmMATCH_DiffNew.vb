Option Strict Off
Option Explicit On

Friend Class frmMATCH_DiffNew
	Inherits System.Windows.Forms.Form
	Dim nRestAmount As Double
	Dim lAction As Integer
	Dim sDiscrepancyAccount As String
	Dim sObservationAccount As String
	Dim sChargesAccount As String
	Dim aCustomers() As String
    Dim aQuickPostings(,) As String
	Dim sSpreadText, sAccount, sAccountType As String
	Private Sub frmMATCH_DiffNew_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
        FormvbStyle(Me, "dollar.jpg", 300, 50)
		FormLRSCaptions(Me)
        sSpreadText = ""
        If Me.lstQuickPostings.Items.Count > 0 Then
            Me.lstQuickPostings.SelectedIndex = 0
            Me.lstQuickPostings.Focus()
        End If

    End Sub
    Public Function Action() As Object
        ' Returnvalue to calling sub
        Action = lAction
    End Function
    Public Function Account() As Object
        ' Returnvalue to calling sub
        Account = sAccount
    End Function
    Public Function SpreadText() As Object
        ' Returnvalue to calling sub
        SpreadText = sSpreadText
    End Function
    Public Function AccountType() As Object
        ' Returnvalue to calling sub
        AccountType = sAccountType
    End Function
    Public Sub RestAmount(ByRef nRest As Double)
        ' Send rest (diff) amount from calling sub
        nRestAmount = nRest
        lblDiff.Text = LRS(60129) & " " & VB6.Format(nRestAmount, "##,##0.00#")
    End Sub
    'Public Sub DiscrepancyAccount(sAcc As String)
    '' Send �rediff-account from calling sub
    'sDiscrepancyAccount = sAcc
    'optDiscrepancyAccount.Caption = LRS(60126) & " " & sDiscrepancyAccount
    'End Sub
    'Public Sub ObservationAccount(sAcc As String)
    '' Send ObservationAccount (feilkonto/avvikskonto) from calling sub
    'sObservationAccount = sAcc
    'optObservationAccount.Caption = LRS(60128) & " " & sObservationAccount
    'End Sub
    'Public Sub ChargesAccount(sAcc As String)
    '' Send ObservationAccount (feilkonto/avvikskonto) from calling sub
    'sChargesAccount = sAcc
    'optChargesAccount.Caption = LRS(60153) & " " & sChargesAccount
    'End Sub
    Public Sub Customers(ByRef aCust() As String)
        ' Send list of customeraccounts for this payment from calling sub
        Dim i As Integer
        Dim iNoOfAccounts As Short
        Dim iNoOfCustomers As Short
        Dim lLineHeight As Integer

        ReDim aCustomers(0)
        If aCust(0) = "" Then
            lstToAccount.Visible = False
            'optToAccount(1).Visible = False
        Else
            'optToAccount(1).Visible = True
            If Not Array_IsEmpty(aCust) Then
                For i = 0 To UBound(aCust)
                    If i > 0 Then
                        ReDim Preserve aCustomers(i)
                    End If
                    ' added 03.12.2015 toi avoid error when aCust(i) = nothing
                    If aCust(i) <> Nothing Then
                        lstToAccount.Items.Add(aCust(i)) 'Add accountno in listbox
                        aCustomers(i) = aCust(i)
                    End If
                Next i
            Else
                lstToAccount.Visible = False
            End If
        End If

        'New code with Listbox
        ' Set new locations of the controls

        iNoOfAccounts = lstQuickPostings.Items.Count
        iNoOfCustomers = lstToAccount.Items.Count
        lLineHeight = 195


        If iNoOfAccounts < 0 Then
            If iNoOfCustomers < 0 Then
                'Nothing to do
            Else
                If iNoOfCustomers > 12 Then
                    iNoOfCustomers = 12
                End If
                ' Must resize form, and move line and buttons down
                Me.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Height) + (iNoOfCustomers - 2) * lLineHeight)
                lstToAccount.Height = VB6.TwipsToPixelsY(iNoOfCustomers * lLineHeight + 60)
                'lblLine1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine1.Top) + (iNoOfCustomers - 2) * lLineHeight)
                'cmdOK.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdOK.Top) + (iNoOfCustomers - 2) * lLineHeight)
                'cmdCancel.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdCancel.Top) + (iNoOfCustomers - 2) * lLineHeight)
            End If
        ElseIf iNoOfAccounts > 12 Then
            If iNoOfCustomers < 0 Then
                If iNoOfAccounts > 23 Then
                    iNoOfAccounts = 23
                End If
                ' Must resize form, and move line and buttons down
                Me.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Height) + (iNoOfAccounts - 1) * lLineHeight)
                lstQuickPostings.Height = VB6.TwipsToPixelsY(iNoOfAccounts * lLineHeight + 60)
                'lblLine2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine2.Top) + (iNoOfAccounts - 1) * lLineHeight)

                lblToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                lstToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lstToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                'lblLine1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine1.Top) + (iNoOfAccounts - 2) * lLineHeight)
                'cmdOK.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdOK.Top) + (iNoOfAccounts - 2) * lLineHeight)
                'cmdCancel.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdCancel.Top) + (iNoOfAccounts - 2) * lLineHeight)
            ElseIf iNoOfCustomers > 12 Then
                ' Must resize form, and move line and buttons down
                Me.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Height) + 22 * lLineHeight)
                lstQuickPostings.Height = VB6.TwipsToPixelsY(12 * lLineHeight + 60)
                'lblLine2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine2.Top) + 11 * lLineHeight)

                lblToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblToAccount.Top) + 11 * lLineHeight)
                lstToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lstToAccount.Top) + 11 * lLineHeight)
                lstToAccount.Height = VB6.TwipsToPixelsY(12 * lLineHeight + 60)
                'lblLine1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine1.Top) + 22 * lLineHeight)
                'cmdOK.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdOK.Top) + 22 * lLineHeight)
                'cmdCancel.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdCancel.Top) + 22 * lLineHeight)

            Else
                'Use the free space in the customerlist to fill up with accounts
                If iNoOfAccounts > 12 + 12 - iNoOfCustomers Then
                    iNoOfAccounts = 12 + 12 - iNoOfCustomers
                End If
                ' Must resize form, and move line and buttons down
                Me.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Height) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
                lstQuickPostings.Height = VB6.TwipsToPixelsY(iNoOfAccounts * lLineHeight + 60)
                'lblLine2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine2.Top) + (iNoOfAccounts - 1) * lLineHeight)

                lblToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                lstToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lstToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                lstToAccount.Height = VB6.TwipsToPixelsY(iNoOfCustomers * lLineHeight + 60)
                'lblLine1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine1.Top) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
                'cmdOK.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdOK.Top) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
                'cmdCancel.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdCancel.Top) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
            End If
        Else
            If iNoOfCustomers < 0 Then
                ' Must resize form, and move line and buttons down
                Me.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Height) + (iNoOfAccounts - 1) * lLineHeight)
                lstQuickPostings.Height = VB6.TwipsToPixelsY(iNoOfAccounts * lLineHeight + 60)
                'lblLine2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine2.Top) + (iNoOfAccounts - 1) * lLineHeight)

                lblToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                'lstToAccount.Top = lstToAccount.Top + iNoOfAccounts * lLineHeight
                'lblLine1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine1.Top) + (iNoOfAccounts - 2) * lLineHeight)
                'cmdOK.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdOK.Top) + (iNoOfAccounts - 2) * lLineHeight)
                'cmdCancel.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdCancel.Top) + (iNoOfAccounts - 2) * lLineHeight)
            Else
                If iNoOfCustomers > 12 + 12 - iNoOfAccounts Then
                    'Use the free space in the customerlist to fill up with accounts
                    iNoOfCustomers = 12 + 12 - iNoOfAccounts
                End If
                ' Must resize form, and move line and buttons down
                Me.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Height) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
                lstQuickPostings.Height = VB6.TwipsToPixelsY(iNoOfAccounts * lLineHeight + 60)
                lstToAccount.Height = VB6.TwipsToPixelsY(iNoOfCustomers * lLineHeight + 60)
                'lblLine2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine2.Top) + (iNoOfAccounts - 1) * lLineHeight)
                lblToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                lstToAccount.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lstToAccount.Top) + (iNoOfAccounts - 1) * lLineHeight)
                'lblLine1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblLine1.Top) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
                'cmdOK.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdOK.Top) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)
                'cmdCancel.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(cmdCancel.Top) + (iNoOfAccounts + iNoOfCustomers - 2) * lLineHeight)

            End If
        End If

    End Sub
    Public Sub QuickPostingAccounts(ByRef aAccountsInfo(,) As String)
        ' Send list of customeraccounts for this payment from calling sub
        Dim i As Integer
        Dim j As Integer
        Dim iNoOfActiveDimensions As Integer
        Dim sListing As String

        'ReDim aQuickPostings(0)
        aQuickPostings = VB6.CopyArray(aAccountsInfo)

        If aAccountsInfo(1, 0) = "" Then
            lstQuickPostings.Visible = False
            'optQuickPosting(1).Visible = False
        Else
            'optQuickPosting(1).Visible = True
            If Not Array_IsEmpty(aAccountsInfo) Then
                For i = 0 To UBound(aAccountsInfo, 2)
                    'lstQuickPostings.Items.Add(aAccountsInfo(1, i) & " - " & aAccountsInfo(0, i))
                    ' Check no of active dimensions
                    If Not EmptyString(aQuickPostings(15, i)) Then
                        iNoOfActiveDimensions = 10
                    ElseIf Not EmptyString(aQuickPostings(14, i)) Then
                        iNoOfActiveDimensions = 9
                    ElseIf Not EmptyString(aQuickPostings(13, i)) Then
                        iNoOfActiveDimensions = 8
                    ElseIf Not EmptyString(aQuickPostings(12, i)) Then
                        iNoOfActiveDimensions = 7
                    ElseIf Not EmptyString(aQuickPostings(11, i)) Then
                        iNoOfActiveDimensions = 6
                    ElseIf Not EmptyString(aQuickPostings(10, i)) Then
                        iNoOfActiveDimensions = 5
                    ElseIf Not EmptyString(aQuickPostings(9, i)) Then
                        iNoOfActiveDimensions = 4
                    ElseIf Not EmptyString(aQuickPostings(8, i)) Then
                        iNoOfActiveDimensions = 3
                    ElseIf Not EmptyString(aQuickPostings(7, i)) Then
                        iNoOfActiveDimensions = 2
                    ElseIf Not EmptyString(aQuickPostings(6, i)) Then
                        iNoOfActiveDimensions = 1
                    Else
                        iNoOfActiveDimensions = 0

                    End If
                    sListing = aAccountsInfo(1, i)
                    ' add as many dimension as stated
                    For j = 1 To iNoOfActiveDimensions
                        sListing = sListing & "/" & aQuickPostings(5 + j, i)
                    Next j
                    '03.05.2016 - Changed the order. For SISMO the accountnumber (sListing) with dimensions is so long 
                    '  that they coudln't see the name of the quickposting account aAccountsInfo(0, i)
                    'Old code - 
                    'sListing = sListing & " - " & aAccountsInfo(0, i)
                    'New code
                    sListing = aAccountsInfo(0, i) & " - " & sListing
                    lstQuickPostings.Items.Add(sListing)
                Next i
            Else
                lstQuickPostings.Visible = False
            End If
        End If

    End Sub
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		lAction = 0
        'Me.Hide()  ' commented 15.05.2018
		Me.Close()
	End Sub
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		TheChoiceIsMade()
		
	End Sub
	
	Private Sub frmMATCH_DiffNew_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		lstQuickPostings.Items.Clear()
		lstToAccount.Items.Clear()
		
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Function TheChoiceIsMade() As Object
        Dim bButtonFound As Boolean
        Dim iNoOfDimensions As Integer = 0
        Dim i As Integer
        Dim stmp As String = ""
		lAction = 0
		sAccount = ""
		bButtonFound = False
		
		If lstQuickPostings.SelectedIndex > -1 Then
            sAccount = aQuickPostings(1, lstQuickPostings.SelectedIndex)
            ' 29.07.2015 Introduced dimensions;
            '11.07.2017 - check if we have any dimensions in use;
            For i = 1 To 10
                If aQuickPostings(i + 5, lstQuickPostings.SelectedIndex) <> "" Then
                    stmp = stmp + aQuickPostings(i + 5, lstQuickPostings.SelectedIndex)
                End If
            Next
            For i = 1 To 10
                ' 11.07.2017 added test not to have accounts like 50000////////// returned
                If stmp <> "" Then
                    sAccount = sAccount & "/" & aQuickPostings(i + 5, lstQuickPostings.SelectedIndex)
                End If
                If Not EmptyString(aQuickPostings(i + 5, lstQuickPostings.SelectedIndex)) Then
                    iNoOfDimensions = i
                End If
            Next i
            ' return string with correct no of dimensions;
            '24.04.2017 - Why do we do this???
            '  The sAccount see correct after the For .. Next above, and here we add some of it once more?????
            ' 11.07.2017 removed the below for next
            'stmp = sAccount
            'For i = 1 To iNoOfDimensions + 1
            '    sAccount = sAccount & "/" & xDelim(stmp, "/", i)
            'Next i

			If aQuickPostings(4, lstQuickPostings.SelectedIndex) = "NOMATCH" Then
				lAction = 3
			Else
				sAccountType = aQuickPostings(2, lstQuickPostings.SelectedIndex)
				lAction = 2
			End If
			sSpreadText = aQuickPostings(3, lstQuickPostings.SelectedIndex)
			bButtonFound = True
		End If
		
		If lstToAccount.SelectedIndex > -1 Then
			sAccount = aCustomers(lstToAccount.SelectedIndex)
			lAction = 4
			bButtonFound = True
		End If
		
		' Must check if user have made a choise
		If Not bButtonFound Then
			' JanP 20.01.04
			' FIX LRS(
			MsgBox("Du m� gj�re et valg f�r du trykker OK!", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
		Else
            'Me.Hide()  
            Me.Close()   ' added 15.05.2018
		End If
		
		
    End Function

    Private Sub lstQuickPostings_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstQuickPostings.KeyUp
        If e.KeyCode = Keys.Enter Then
            lstToAccount.SelectedIndex = -1
            TheChoiceIsMade()
        End If
    End Sub
    Private Sub lstQuickPostings_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstQuickPostings.SelectedIndexChanged

        'Reset lstToAccount only when an item is choosen in lstQuickPostings
        If lstQuickPostings.SelectedIndex > -1 Then
            lstToAccount.SelectedIndex = -1
        End If

    End Sub
	
	Private Sub lstQuickPostings_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstQuickPostings.DoubleClick
		
		lstToAccount.SelectedIndex = -1
		TheChoiceIsMade()
		
	End Sub
    Private Sub lstToAccount_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstToAccount.SelectedIndexChanged

        'Reset lstQuickPostings only when an item is choosen in lstToAccount
        If lstToAccount.SelectedIndex > -1 Then
            lstQuickPostings.SelectedIndex = -1
        End If

    End Sub
	
	Private Sub lstToAccount_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstToAccount.DoubleClick
		
		lstQuickPostings.SelectedIndex = -1
		TheChoiceIsMade()
		
	End Sub
End Class
