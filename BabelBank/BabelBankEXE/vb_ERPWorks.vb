Option Strict Off
Option Explicit On
Module vb_ERPWorks
	Private conERPConnection As ADODB.Connection
    Private ERPConnection As System.Data.Common.DbConnection 'Ding
    Private sLocalConnString As String
	Private sLocalUID As String
	Private sLocalPWD As String
	
	Private lERP_CursorType As ADODB.CursorTypeEnum
	Private lERP_LockType As ADODB.LockTypeEnum
	Private lERP_CursorLocation As ADODB.CursorLocationEnum
	Private bERP_CreateRSWithOpen As Boolean
	Private bERP_ConnectionIsInitiated As Boolean
	
	
	Public Function ERP_CloseConnection() As Object
		
		If Not conERPConnection Is Nothing Then
			If conERPConnection.State = ADODB.ObjectStateEnum.adStateOpen Then
				conERPConnection.Close()
			End If
			'UPGRADE_NOTE: Object conERPConnection may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			conERPConnection = Nothing
		End If
		
		
	End Function
	Public Function ERP_MakeConnection(ByRef sConnString As String, Optional ByRef sUID As String = "", Optional ByRef sPWD As String = "", Optional ByRef sCreateRSWithOpen As String = "True", Optional ByRef lCursorType As Integer = ADODB.CursorTypeEnum.adOpenStatic, Optional ByRef lLockType As Integer = ADODB.LockTypeEnum.adLockOptimistic, Optional ByRef lCursorLocation As Integer = ADODB.CursorLocationEnum.adUseClient) As Boolean
		Dim sErrorString As String
		Dim bx As Boolean
		Dim iLoginAttempts As Short
		
		
		'Default values
		'3 = adopenstatic
		'3 = adLockOptimistic
		'3 = adUseClient
		
		'Close the connection if it's open.
		On Error GoTo ERR_ERP_MakeConnection
		
		If Not conERPConnection Is Nothing Then
			If conERPConnection.State = ADODB.ObjectStateEnum.adStateOpen Then
				conERPConnection.Close()
			End If
			'UPGRADE_NOTE: Object conERPConnection may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			conERPConnection = Nothing
		End If
		
		'iLoginAttempts = 1
		''Hvis sconnstring inneholder =UID og/eller =PWD, og henholdsvis
		''sUID og sLocalUID eller sPwd sLocalPWD er tomme s� vis p�loggingsbilde
		'Do Until iLoginAttempts > 3
		If Not EmptyString(sUID) Then
			sLocalUID = sUID
		End If
		If Not EmptyString(sPWD) Then
			sLocalPWD = sPWD
		End If
		''
		''    bShowLoginForm = False
		''    If InStr(1, sConnString, "=UID", vbTextCompare) > 0 Then
		''        If EmptyString(sLocalUID) Then
		''            bShowLoginForm = True
		''        End If
		''    End If
		''    If InStr(1, sConnString, "=PWD", vbTextCompare) > 0 Then
		''        If EmptyString(sLocalPWD) Then
		''            bShowLoginForm = True
		''        End If
		''    End If
		'
		'    If bShowLoginForm Then
		'        iLoginAttempts = iLoginAttempts + 1
		'        If EmptyString(sLocalUID) Then
		'            frmLogin.txtUID = Environ$("Username")
		'        Else
		'            frmLogin.txtUID = sLocalUID
		'        End If
        '        frmLogin.txtPWD = ""
        '        Screen.MousePointer = vbNormal
        '        frmLogin.Show 1
        '        Screen.MousePointer = vbHourglass
        '
        '        If frmLogin.bPasswordEntered Then
        '            sLocalUID = Trim$(frmLogin.txtUID)
        '            sLocalPWD = Trim$(frmLogin.txtPWD)
        '        Else
        '            Err.Raise 27001, "ERP_MakeConnection", LRS(27001)
        '            'You have chosen to cancel BabelBank
        '            iLoginAttempts = 4
        '        End If
        '
        '    Else
        '        iLoginAttempts = 4
        '    End If

        sLocalConnString = sConnString
        'MsgBox "Bruker ID: " & sLocalUID & "Passord:" & sLocalPWD
        conERPConnection = ConnectToERPDB(sConnString, sLocalUID, sLocalPWD, False, sErrorString, True)

        If Len(sErrorString) > 0 Then
            If iLoginAttempts = 4 Then
                Err.Raise(Err.Number, "vb_ERPWorks", sErrorString)
            Else
                MsgBox(Err.Description, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "Login")
            End If
        End If



        If lCursorType < 0 Or lCursorType > 3 Then
            Err.Raise(19504, "vb_ERPWorks", LRS(19504, "cursor type"))
            'Invalid CursorType specified."
        Else
            lERP_CursorType = lCursorType
        End If
        If lLockType < 1 Or lLockType > 4 Then
            Err.Raise(19504, "vb_ERPWorks", LRS(19504, "lock type"))
            'Invalid LockType specified."
        Else
            lERP_LockType = lLockType
        End If
        If lCursorLocation < 2 Or lCursorLocation > 3 Then
            Err.Raise(19504, "vb_ERPWorks", LRS(19504, "cursor location"))
            'Invalid CursorLocation specified."
        Else
            lERP_CursorLocation = lCursorLocation
        End If
        If sCreateRSWithOpen = "True" Then
            bERP_CreateRSWithOpen = True
        Else
            bERP_CreateRSWithOpen = False
        End If

        bERP_ConnectionIsInitiated = True

        ERP_MakeConnection = True

        On Error GoTo 0
        Exit Function

ERR_ERP_MakeConnection:

        sLocalUID = ""
        sLocalPWD = ""
        iLoginAttempts = 1
        Err.Raise(Err.Number, "ERP_MakeConnection", Err.Description)

    End Function
    Public Function ERP_MakeConnectionNew(ByRef sConnString As String, Optional ByRef sUID As String = "", Optional ByRef sPWD As String = "", Optional ByRef sCreateRSWithOpen As String = "True", Optional ByRef lCursorType As Integer = ADODB.CursorTypeEnum.adOpenStatic, Optional ByRef lLockType As Integer = ADODB.LockTypeEnum.adLockOptimistic, Optional ByRef lCursorLocation As Integer = ADODB.CursorLocationEnum.adUseClient) As Boolean
        Dim sErrorString As String
        Dim bx As Boolean
        Dim iLoginAttempts As Short


        'Default values
        '3 = adopenstatic
        '3 = adLockOptimistic
        '3 = adUseClient

        'Close the connection if it's open.
        On Error GoTo ERR_ERP_MakeConnectionNew

        If Not ERPConnection Is Nothing Then
            If ERPConnection.State <> ConnectionState.Closed Then
                ERPConnection.Close()
            End If
            ERPConnection.Dispose()
        End If

        ''Hvis sconnstring inneholder =UID og/eller =PWD, og henholdsvis
        ''sUID og sLocalUID eller sPwd sLocalPWD er tomme s� vis p�loggingsbilde
        If Not EmptyString(sUID) Then
            sLocalUID = sUID
        End If
        If Not EmptyString(sPWD) Then
            sLocalPWD = sPWD
        End If

        sLocalConnString = sConnString
        'MsgBox "Bruker ID: " & sLocalUID & "Passord:" & sLocalPWD

        ERPConnection = ConnectToERPDBNew("System.Data.OleDb", sConnString, sLocalUID, sLocalPWD, False, sErrorString, True)

        If Len(sErrorString) > 0 Then
            If iLoginAttempts = 4 Then
                Err.Raise(Err.Number, "vb_ERPWorks", sErrorString)
            Else
                MsgBox(Err.Description, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Login")
            End If
        End If


        'Not implemented the use of cursor etc. yet
        If lCursorType < 0 Or lCursorType > 3 Then
            Err.Raise(19504, "vb_ERPWorks", LRS(19504, "cursor type"))
            'Invalid CursorType specified."
        Else
            lERP_CursorType = lCursorType
        End If
        If lLockType < 1 Or lLockType > 4 Then
            Err.Raise(19504, "vb_ERPWorks", LRS(19504, "lock type"))
            'Invalid LockType specified."
        Else
            lERP_LockType = lLockType
        End If
        If lCursorLocation < 2 Or lCursorLocation > 3 Then
            Err.Raise(19504, "vb_ERPWorks", LRS(19504, "cursor location"))
            'Invalid CursorLocation specified."
        Else
            lERP_CursorLocation = lCursorLocation
        End If
        If sCreateRSWithOpen = "True" Then
            bERP_CreateRSWithOpen = True
        Else
            bERP_CreateRSWithOpen = False
        End If

        bERP_ConnectionIsInitiated = True

        ERP_MakeConnectionNew = True

        On Error GoTo 0
        Exit Function

ERR_ERP_MakeConnectionNew:

        sLocalUID = ""
        sLocalPWD = ""
        iLoginAttempts = 1
        Err.Raise(Err.Number, "ERP_MakeConnection", Err.Description)

    End Function

    Public Function HaveToShowLoginForm(ByRef sConnString As String, ByRef sUID As String, ByRef sPWD As String) As Boolean
        Dim bShowLoginForm As Boolean

        If Not EmptyString(sUID) Then
            sLocalUID = sUID
        End If
        If Not EmptyString(sPWD) Then
            sLocalPWD = sPWD
        End If

        bShowLoginForm = False
        If InStr(1, sConnString, "=UID", CompareMethod.Text) > 0 Then
            If EmptyString(sLocalUID) Then
                bShowLoginForm = True
            End If
        End If
        If InStr(1, sConnString, "=PWD", CompareMethod.Text) > 0 Then
            If EmptyString(sLocalPWD) Then
                bShowLoginForm = True
            End If
        End If

        HaveToShowLoginForm = bShowLoginForm

    End Function
    Public Function GetUID() As String

        GetUID = sLocalUID

    End Function

    Public Function GetPWD() As String

        GetPWD = sLocalPWD

    End Function
    Public Function SetUID(ByRef s As String) As String

        sLocalUID = s

    End Function

    Public Function SetPWD(ByRef s As String) As String

        sLocalPWD = s

    End Function

    Public Function ERP_GetRecordset(ByRef sMySQL As String, ByRef oPayment As vbBabel.Payment, ByRef sClientNo As String, Optional ByRef sAmount As String = "", Optional ByRef sCustomerNo As String = "", Optional ByRef bUseoriginalAmountInMatching As Boolean = False, Optional ByRef sClientName As String = "") As ADODB.Recordset
        Dim rsERP As ADODB.Recordset
        Dim sMyNewSQL As String
        Dim iNoOfAttempts As Short
        iNoOfAttempts = 0

        '15.10.2007 Added the use of bUseoriginalAmountInMatching

        On Error GoTo ERRORERP_GetRecordset

        'If Not bERP_ConnectionIsInitiated Or conERPConnection.State = adStateClosed Then
        '    Err.Raise 1, "vb_ERPWorks", "Babelbank tries to run an SQL, when the connection isn't open."
        'Else
        'BB_Amount
        sMyNewSQL = Replace(sMySQL, "BB_Amount", Trim(sAmount), , , CompareMethod.Text)
        'BB_ClientNo
        sMyNewSQL = Replace(sMyNewSQL, "BB_ClientNo", sClientNo, , , CompareMethod.Text)
        'BB_Currency
        sMyNewSQL = Replace(sMyNewSQL, "BB_Currency", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
        'BB_InvoiceIdentifier
        sMyNewSQL = Replace(sMyNewSQL, "BB_InvoiceIdentifier", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)
        'BB_CustomerIdentifier
        sMyNewSQL = Replace(sMyNewSQL, "BB_CustomerIdentifier", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)

        'BB_AccountNo
        sMyNewSQL = Replace(sMyNewSQL, "BB_AccountNo", oPayment.E_Account, , , CompareMethod.Text)
        'BB_Name
        sMyNewSQL = Replace(sMyNewSQL, "BB_Name", Replace(oPayment.E_Name, "'", ""), , , CompareMethod.Text)
        'BB_CustomerNo
        sMyNewSQL = Replace(sMyNewSQL, "BB_CustomerNo", sCustomerNo, , , CompareMethod.Text)
        'BB_Zip
        sMyNewSQL = Replace(sMyNewSQL, "BB_Zip", oPayment.E_Zip, , , CompareMethod.Text)
        'BB_Adr1
        sMyNewSQL = Replace(sMyNewSQL, "BB_Adr1", oPayment.E_Adr1, , , CompareMethod.Text)
        'BB_Adr2
        sMyNewSQL = Replace(sMyNewSQL, "BB_Adr2", oPayment.E_Adr2, , , CompareMethod.Text)
        'BB_ValueDate
        sMyNewSQL = Replace(sMyNewSQL, "BB_ValueDate", oPayment.DATE_Value, , , CompareMethod.Text)
        'BB_ClientName, added 19.11.2007
        sMyNewSQL = Replace(sMyNewSQL, "BB_ClientName", Trim(sClientName), , , CompareMethod.Text)
        'BB_ExtraD1, added 15.01.2009
        sMyNewSQL = Replace(sMyNewSQL, "BB_ExtraD1", Trim(oPayment.ExtraD1), , , CompareMethod.Text)

        If bERP_CreateRSWithOpen Then
            'Use recordset - open to retrieve the recordset

            rsERP = CreateObject("ADODB.RecordSet")
            rsERP.CursorLocation = lERP_CursorLocation

            If conERPConnection.State = ADODB.ObjectStateEnum.adStateClosed Then
                ERP_MakeConnection(sLocalConnString, sLocalUID, sLocalPWD)
            End If
            'sMyNewSQL = "SELECT R.Ref AS BBRET_InvoiceNo, R.InvoNo AS FakturaNr, R.RAm*100 as BBRET_AMOUNT, R.CustNo AS BBRET_CustomerNo, K.Nm AS Navn, R.DueDt AS Forfall, R.Ref AS BBRET_MatchID, '311#1' AS BBRET_MyField FROM F0002..OpCustTr R, F0002..Actor K WHERE R.CustNo = K.CustNo AND R.Ram=57907/100 AND R.RAm<>0 AND St<3  ORDER BY R.DueDt"

            'rsERP.CursorLocation = adUseServer
            'rsERP.CursorType = adOpenForwardOnly 'Default adOpenForwardOnly
            'rsERP.LockType = adLockReadOnly 'Default adLockReadOnly

            'sMyNewSQL = "SELECT AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.CurrencyCode as BBRET_Currency, LI.DueDate AS FFDato, PA.PartName AS BBRET_Name, PA.PartyAddressLine1 AS Adresse1, PA.PartyAddressLine2 AS Adresse2, PA.PartyAddressLine3 AS Adresse3, PA.PartyPostCode AS Postnr, PA.PartyPostDesc AS Poststed, PA.PartyCityName as Sted, AD.CompanyRegNumber AS Orgnr, AD.ClientsDebtorReference AS Debitornr, PAD.PartName AS Debitornavn, ltrim(str(LI.LedgerItemId)) as BBRET_MatchID, AD.AgreementNumber + '-' + AD.ClientASCNumber + '-' + AD.AccountNumber + '-88888' AS BBRET_MyField, LI.Openitem AS BBRET_Filter, CASE WHEN DI.DiscountDate >= CONVERT(datetime, '20091005') THEN DI.DiscountAmount Else 0 END AS BBRET_Discount FROM LedgerItems LI inner JOIN AgreementDetails AD ON AD.AgreementID = LI.DebtorAgreementID inner JOIN AgreementDetails AC on AC.AgreementID = LI.ClientAgreementID AND AC.AgreementType='CA'"
            'sMyNewSQL = sMyNewSQL & "Inner JOIN Part PA ON PA.PartID = LI.DebtorPartID and PA.Parttype='D' Inner JOIN Part PAD ON PAD.PartID = LI.ClientPartID and PAD.Parttype='C' left outer join Discounts DI ON DI.LedgerItemId=LI.LedgerItemId AND DI.DebtorAgreementID=LI.DebtorAgreementID AND DI.DebtorPartId=LI.DebtorPartId WHERE LI.DocumentNumber = '907529'  AND AC.CurrencyCode='NOK' ORDER BY AD.ClientNumber ASC, LI.DueDate ASC"
            'sMyNewSQL = "SELECT LI.DocumentNumber, ltrim(str(LI.LedgerItemId)) as BBRET_CustomerNo FROM LedgerItems LI WHERE LI.DocumentNumber = '907529'"

            'conERPConnection.CommandTimeout = 120

            rsERP.Open(sMyNewSQL, conERPConnection, lERP_CursorType, lERP_LockType)
            If conERPConnection.Errors.Count > 0 Then
                'Try to make a new connection
                ERP_MakeConnection(sLocalConnString, sLocalUID, sLocalPWD)
                rsERP.Open(sMyNewSQL, conERPConnection, lERP_CursorType, lERP_LockType)
            End If
        Else
            'Use Connection - Execute to retrieve the recordset

            rsERP.CursorLocation = lERP_CursorLocation
            rsERP.CursorType = lERP_CursorType
            rsERP.LockType = lERP_LockType
            rsERP = conERPConnection.Execute(sMyNewSQL)

        End If

        'End If

        ERP_GetRecordset = rsERP

        Exit Function

ERRORERP_GetRecordset:
        'If iNoOfAttempts < 3 Then
        '    iNoOfAttempts = iNoOfAttempts + 1
        '    Pause 2
        '    ' try to reconnect, may be timeout
        '    ERP_MakeConnection sLocalConnString, sLocalUID, sLocalPWD
        '    Resume
        'Else

        'UPGRADE_NOTE: Object rsERP may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        rsERP = Nothing

        ERP_GetRecordset = rsERP

        Err.Raise(Err.Number, Err.Source, Err.Description)
        'End If
    End Function
    Public Function ERP_GetReader(ByRef sMySQL As String, ByRef oPayment As vbBabel.Payment, ByRef sClientNo As String, Optional ByRef sAmount As String = "", Optional ByRef sCustomerNo As String = "", Optional ByRef bUseoriginalAmountInMatching As Boolean = False, Optional ByRef sClientName As String = "") As System.Data.Common.DbDataReader
        Dim readerCommand As System.Data.Common.DbCommand
        Dim readerERP As System.Data.Common.DbDataReader
        Dim sMyNewSQL As String
        Dim iNoOfAttempts As Short
        iNoOfAttempts = 0

        '15.10.2007 Added the use of bUseoriginalAmountInMatching

        'BB_Amount
        sMyNewSQL = Replace(sMySQL, "BB_Amount", Trim(sAmount), , , CompareMethod.Text)
        'BB_ClientNo
        sMyNewSQL = Replace(sMyNewSQL, "BB_ClientNo", sClientNo, , , CompareMethod.Text)
        'BB_Currency
        sMyNewSQL = Replace(sMyNewSQL, "BB_Currency", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
        'BB_InvoiceIdentifier
        sMyNewSQL = Replace(sMyNewSQL, "BB_InvoiceIdentifier", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)
        'BB_CustomerIdentifier
        sMyNewSQL = Replace(sMyNewSQL, "BB_CustomerIdentifier", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)

        'BB_AccountNo
        sMyNewSQL = Replace(sMyNewSQL, "BB_AccountNo", oPayment.E_Account, , , CompareMethod.Text)
        'BB_Name
        sMyNewSQL = Replace(sMyNewSQL, "BB_Name", Replace(oPayment.E_Name, "'", ""), , , CompareMethod.Text)
        'BB_CustomerNo
        sMyNewSQL = Replace(sMyNewSQL, "BB_CustomerNo", sCustomerNo, , , CompareMethod.Text)
        'BB_Zip
        sMyNewSQL = Replace(sMyNewSQL, "BB_Zip", oPayment.E_Zip, , , CompareMethod.Text)
        'BB_Adr1
        sMyNewSQL = Replace(sMyNewSQL, "BB_Adr1", oPayment.E_Adr1, , , CompareMethod.Text)
        'BB_Adr2
        sMyNewSQL = Replace(sMyNewSQL, "BB_Adr2", oPayment.E_Adr2, , , CompareMethod.Text)
        'BB_ValueDate
        sMyNewSQL = Replace(sMyNewSQL, "BB_ValueDate", oPayment.DATE_Value, , , CompareMethod.Text)
        'BB_ClientName, added 19.11.2007
        sMyNewSQL = Replace(sMyNewSQL, "BB_ClientName", Trim(sClientName), , , CompareMethod.Text)
        'BB_ExtraD1, added 15.01.2009
        sMyNewSQL = Replace(sMyNewSQL, "BB_ExtraD1", Trim(oPayment.ExtraD1), , , CompareMethod.Text)

        If bERP_CreateRSWithOpen Then
            'Use recordset - open to retrieve the recordset

            'Not implemented the use of cursors yet
            'rsERP.CursorLocation = lERP_CursorLocation

            If ERPConnection.State = ConnectionState.Closed Then
                ERP_MakeConnectionNew(sLocalConnString, sLocalUID, sLocalPWD)
            End If

            'Using ERPConnection
            Try
                ' Create the command.
                'Dim command As System.Data.Common.DbCommand = myConnection.CreateCommand()
                readerCommand = ERPConnection.CreateCommand()
                readerCommand.CommandText = sMySQL
                readerCommand.CommandType = CommandType.Text
                readerCommand.Connection = ERPConnection

                'Andre parametre som kan v�re interessante
                'myCommand.CommandType = CommandType.StoredProcedure
                'myCommand.Cancel() - Pr�ver � stoppe ekskveringen av en DbCommand (SQL-en)
                'myCommand.CreateParameter - ParameterDirection til StoredProcedure?
                'myCommand.ExecuteNonQuery - Kj�rer SQL som ikke returnerer felter, 'INSERT/UPDATE, CHANGE/ADD TABLES etc...
                'myCommand.ExecuteScalar - Typisk SELECT COUNT(*) ....! Executes the query and returns the first column of the first row in the result set returned by the query.

                ' Retrieve the data.
                'Dim reader As System.Data.Common.DbDataReader = command.ExecuteReader()
                readerERP = readerCommand.ExecuteReader

                'If readerERP.HasRows Then
                '    Do While readerERP.Read()
                '        If HasColumnAndValue(readerERP, "BBRET_CustomerNo") Then
                '            sMyNewSQL = readerERP.GetString(3)
                '        End If
                '    Loop
                'End If

                'readerERP = readerCommand.ExecuteReader
            Catch ex As Exception
                Console.WriteLine("Exception.Message: {0}", ex.Message)
            End Try
            'End Using

        Else
            'Use Connection - Execute to retrieve the recordset

            'rsERP.CursorLocation = lERP_CursorLocation
            'rsERP.CursorType = lERP_CursorType
            'rsERP.LockType = lERP_LockType
            'rsERP = conERPConnection.Execute(sMyNewSQL)

        End If

        ERP_GetReader = readerERP

        Exit Function

    End Function

    Public Function ERP_GetRecordsetFixedSQL(ByRef sMySQL As String, Optional ByRef lCursorLocation As Integer = -1) As ADODB.Recordset
        Dim rsERP As ADODB.Recordset
        Dim iNoOfAttempts As Short
        iNoOfAttempts = 0

        On Error GoTo ERRORERP_GetRecordsetFixedSQL

        If bERP_CreateRSWithOpen Then
            'Use recordset - open to retrieve the recordset
            'sMySQL = "INSERT INTO Kundens_bankkonto (Kundenr_, Kode, Navn, Bankregistreringsnr_, Bankkontonr_) VALUES('99998', 'BB', 'Kjellemann', '9246', '90461032795')"
            rsERP = CreateObject("ADODB.RecordSet")
            If lCursorLocation > 0 Then
                rsERP.CursorLocation = lCursorLocation
            Else
                rsERP.CursorLocation = lERP_CursorLocation
            End If

            'rsERP.CursorLocation = adUseServer
            'rsERP.Close
            'rsERP.Open sMySQL, conERPConnection, adOpenStatic, adLockReadOnly
            'rsERP.Open sMySQL, conERPConnection, adOpenStatic, adLockReadOnly
            'Myklebust
            rsERP.Open(sMySQL, conERPConnection, lERP_CursorType, lERP_LockType)
            If conERPConnection.Errors.Count > 0 Then
                'Try to make a new connection
                ERP_MakeConnection(sLocalConnString, sLocalUID, sLocalPWD)
                rsERP.Open(sMySQL, conERPConnection, lERP_CursorType, lERP_LockType)
            End If

        Else
            'Use Connection - Execute to retrieve the recordset

            rsERP.CursorLocation = lERP_CursorLocation
            rsERP.CursorType = lERP_CursorType
            rsERP.LockType = lERP_LockType
            rsERP = conERPConnection.Execute(sMySQL)

        End If

        ERP_GetRecordsetFixedSQL = rsERP

        Exit Function

ERRORERP_GetRecordsetFixedSQL:
        'If iNoOfAttempts < 3 Then
        '    iNoOfAttempts = iNoOfAttempts + 1
        '    Pause 2
        '    ' try to reconnect, may be timeout
        '    ERP_MakeConnection sLocalConnString, sLocalUID, sLocalPWD
        '    Resume
        'Else

        'UPGRADE_NOTE: Object rsERP may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        rsERP = Nothing

        ERP_GetRecordsetFixedSQL = rsERP

        Err.Raise(Err.Number, Err.Source, Err.Description)
        'End If

    End Function
    '----------------------------------------------------

    'To validate different use of the recordset

    '    If rsCustomer.Supports(adAddNew) Then
    '        Debug.Print "Opened cursor supports insert"
    '    End If
    '    If rsCustomer.Supports(adUpdate) Then
    '        Debug.Print "Opened cursor supports update"
    '    End If
    '    If rsCustomer.Supports(adDelete) Then
    '        Debug.Print "Opened cursor supports delete"
    '    End If
    '    Debug.Print "Number of records: " & rsCustomer.RecordCount
    '

    'adAddNew You can use the AddNew method to add new records.
    'adApproxPosition You can read and set the AbsolutePosition and AbsolutePage properties.
    'adBookmark You can use the Bookmark property to gain access to specific records.
    'adDelete You can use the Delete method to delete records.
    'adHoldRecords You can retrieve more records or change the next retrieve position without committing all pending changes.
    'adMovePrevious You can use the MoveFirst and MovePrevious methods, and Move or GetRows methods to move the current record position backward without requiring bookmarks.
    'adResync You can update the cursor with the data visible in the underlying database, using the Resync method.
    'adUpdate You can use the Update method to modify existing data.
    'adUpdateBatch You can use batch updating (UpdateBatch and CancelBatch methods) to transmit groups of changes to the provider.
    'adIndex You can use the Index property to name an index.
    'adSeek You can use the Seek method to locate a row in a Recordset.


    '---------------------------------------------------------------------


    'Public Function ERP_CheckIfNewClient(sNewAccountNo As String) As Boolean
    'Dim bDBProfileFound As Boolean
    'Dim iCounter As Integer
    '
    ''Check if it is a new account
    'If sAccountNo <> sNewAccountNo Then
    '    sAccountNo = sNewAccountNo
    '    Set oClient = FindClient(oBabelFile.VB_Profile, iFileSetup_ID, sNewAccountNo, oBabelFile.FilenameIn, iFilenameInNo)
    '    'Check if it is a new client
    '    If sClientNo <> oClient.ClientNo Then
    '        sClientNo = oClient.ClientNo
    '        'If new ERP DB, create a new connection
    '        If iDBProfileID <> oClient.DBProfile_ID Then
    '            iDBProfileID = oClient.DBProfile_ID
    '            If conERPConnection.State = adStateOpen Then
    '                conERPConnection.Close
    '                Set conERPConnection = Nothing
    '            End If
    '            bDBProfileFound = False
    '            For iCounter = 0 To UBound(aQueryArray(), 1)
    '                If oClient.DBProfile_ID = Val(aQueryArray(lCounter, 3, 0)) Then
    '                    bDBProfileFound = True
    '                    'iERPArrayindex keeps the value of the first dimension in the array
    '                    iERPArrayIndex = iCounter
    '                    Exit For
    '                End If
    '            Next iCounter
    '            If bDBProfileFound Then
    '                'An error will be raised if the connection fails.
    '                'FIX: Maybe we should do it another way. Return the errortext and .......
    '                Set conERPConnection = ConnectToERPDB(aQueryArray(lCounter, 0, 0), aQueryArray(lCounter, 1, 0), aQueryArray(lCounter, 2, 0), False, sErrorString, True)
    '            Else
    '                'Make a description on how to do this
    '                Err.Raise 1, "vb_ERPWorks", "You try to match the payments on client %1, but no ERP-connection are specified for the client"
    '            End If
    '        End If
    '        'Retrieve description
    '        '  that uniqly identify the invoice
    '        aInvDesc() = FindDescription(lCompanyNo, oClient.Client_ID)
    '    End If
    'End If
    '
    'End Function

    Public Function ERP_ExecuteCommand(ByRef sMySQL As String) As Integer
        Dim cmdUpdate As New ADODB.Command
        Dim lRecordsAffected As Integer

        On Error GoTo ErrorERP_ExecuteCommand

        'sMySQL = "UPDATE u_typeresk set u_banknr = convert(int,substring('16441565121',1,4)), u_bankkto = convert(int,right('16441565121',7)) WHERE u_selskap = 10 AND u_region = 0 AND u_resktype <> 'L' AND u_resknr = 902020 and ((right(rtrim('0000'+convert(char(4),u_banknr)),4)+right(rtrim('0000000'+convert(char(7),u_bankkto)),7) <> '16441565123'))"
        'sMySQL = "UPDATE u_typeresk set u_banknr = convert(int,substring('16441565120',1,4)), u_bankkto = convert(int,right('16441565120',7)) WHERE u_selskap = 10 AND u_region = 0 AND u_resktype <> 'L' AND u_resknr = 902020 and right(rtrim('0000'+ltrim(str(u_banknr,4))),4)+right(rtrim('0000000'+ltrim(str(u_bankkto,7))),7) <> '16441565122'"
        lRecordsAffected = 0

        cmdUpdate.CommandText = sMySQL
        cmdUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
        cmdUpdate.let_ActiveConnection(conERPConnection)
        cmdUpdate.Execute(lRecordsAffected)

        ERP_ExecuteCommand = lRecordsAffected

        Exit Function

ErrorERP_ExecuteCommand:
        If InStr(Err.Description, "Record already exists") Then
            Err.Clear()
            ERP_ExecuteCommand = 0
        Else
            Err.Raise(Err.Number, "ERP_Execute_Command", Err.Description)
        End If

    End Function
    'Public Function ERP_ExecuteProcedure(sMySQL As String) As Long
    Public Function ERP_ExecuteProcedure(ByRef sMySQL As String, ByRef oPayment As vbBabel.Payment, ByRef oInvoice As vbBabel.Invoice, ByRef sClientNo As String, Optional ByRef sAmount As String = "", Optional ByRef sCustomerNo As String = "", Optional ByRef bUseoriginalAmountInMatching As Boolean = False, Optional ByRef sClientName As String = "") As Integer

        Dim cmdUpdate As ADODB.Command
        Dim lRecordsAffected As Integer
        Dim res As Short

        On Error GoTo ErrorERP_ExecuteCommand

        'Exec Conecto_BBAccountsKids 150, '123456', '12951065178', '20100630', '123456789101115', 560.00
        cmdUpdate = New ADODB.Command
        cmdUpdate.let_ActiveConnection(conERPConnection)
        cmdUpdate.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc
        cmdUpdate.CommandText = sMySQL
        cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_ClientNo", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 15, sClientNo))
        cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_CustomerNo", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 30, oInvoice.CustomerNo))
        cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_AccountNo", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 11, oPayment.E_Account))
        cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_ValueDate", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 8, oPayment.DATE_Value))
        If IsOCR((oPayment.PayCode)) Or IsAutogiro((oPayment.PayCode)) Then
            cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_UniqueID", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 25, oInvoice.Unique_Id))
        Else
            cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_UniqueID", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 25, oInvoice.MATCH_ID))
        End If
        cmdUpdate.Parameters.Append(cmdUpdate.CreateParameter("BB_Amount", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInput, 15, ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), , ".")))

        'cmdUpdate.CommandText = "Conecto_BBAccountsKIDs"
        'cmdUpdate.Parameters.Append cmdUpdate.CreateParameter("BB_ClientNo", adVarChar, adParamInput, 15, "150")
        'cmdUpdate.Parameters.Append cmdUpdate.CreateParameter("BB_CustomerNo", adVarChar, adParamInput, 30, "123456")
        'cmdUpdate.Parameters.Append cmdUpdate.CreateParameter("BB_AccountNo", adVarChar, adParamInput, 11, "12951065178")
        'cmdUpdate.Parameters.Append cmdUpdate.CreateParameter("BB_ValueDate", adVarChar, adParamInput, 8, "20100630")
        'cmdUpdate.Parameters.Append cmdUpdate.CreateParameter("BB_UniqueID", adVarChar, adParamInput, 25, "123456789101116")
        'cmdUpdate.Parameters.Append cmdUpdate.CreateParameter("BB_Amount", adVarChar, adParamInput, 15, "560.00")
        'cmd.Parameters.Append cmd.CreateParameter("result", adInteger, adParamOutput)

        cmdUpdate.Execute()
        'res = cmd("result")
        'If (res = 1) Then
        ' MsgBox "Updated Successfully"
        'End If
        'UPGRADE_NOTE: Object cmdUpdate.ActiveConnection may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        cmdUpdate.ActiveConnection = Nothing

        lRecordsAffected = 0

        Exit Function

ErrorERP_ExecuteCommand:
        If InStr(Err.Description, "Record already exists") Then
            Err.Clear()
            ERP_ExecuteProcedure = 0
        Else
            Err.Raise(Err.Number, "ERP_Execute_Command", Err.Description)
        End If

    End Function
End Module
