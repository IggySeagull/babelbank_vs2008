<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_Setup
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
		Form_Initialize_renamed()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cmdHelp As System.Windows.Forms.Button
    Public WithEvents cmbProfileGroup As System.Windows.Forms.ComboBox
    Public WithEvents lblBBVersion As System.Windows.Forms.Label
    'Public WithEvents sprExports As AxFPSpread.AxvaSpread
    'Public WithEvents sprImports As AxFPSpread.AxvaSpread
	Public WithEvents prgActionProgress As System.Windows.Forms.ProgressBar
	Public WithEvents imgPlus As System.Windows.Forms.PictureBox
	Public WithEvents lblProfileGroup As System.Windows.Forms.Label
    Public WithEvents lblExp As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisma_Setup))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdHelp = New System.Windows.Forms.Button
        Me.cmbProfileGroup = New System.Windows.Forms.ComboBox
        Me.lblBBVersion = New System.Windows.Forms.Label
        Me.prgActionProgress = New System.Windows.Forms.ProgressBar
        Me.imgPlus = New System.Windows.Forms.PictureBox
        Me.lblProfileGroup = New System.Windows.Forms.Label
        Me.lblExp = New System.Windows.Forms.Label
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.HjemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportFiles = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportFiles = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem
        Me.OppsettToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuProfileGroup = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClients = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReportSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem
        Me.DanskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EnglishToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NorskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SvenskaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuConvertXToBabelBank = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpgradeViews = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuLog = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.gridExp = New System.Windows.Forms.DataGridView
        Me.gridImp = New System.Windows.Forms.DataGridView
        Me.lblImp = New System.Windows.Forms.Label
        CType(Me.imgPlus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.gridExp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridImp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdHelp
        '
        Me.cmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdHelp.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdHelp.Location = New System.Drawing.Point(620, 51)
        Me.cmdHelp.Name = "cmdHelp"
        Me.cmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdHelp.Size = New System.Drawing.Size(100, 24)
        Me.cmdHelp.TabIndex = 25
        Me.cmdHelp.Text = "50900-Hjelp"
        Me.cmdHelp.UseVisualStyleBackColor = False
        '
        'cmbProfileGroup
        '
        Me.cmbProfileGroup.BackColor = System.Drawing.SystemColors.Window
        Me.cmbProfileGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbProfileGroup.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbProfileGroup.Location = New System.Drawing.Point(120, 51)
        Me.cmbProfileGroup.Name = "cmbProfileGroup"
        Me.cmbProfileGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbProfileGroup.Size = New System.Drawing.Size(241, 21)
        Me.cmbProfileGroup.TabIndex = 16
        '
        'lblBBVersion
        '
        Me.lblBBVersion.BackColor = System.Drawing.SystemColors.Control
        Me.lblBBVersion.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBBVersion.ForeColor = System.Drawing.Color.Black
        Me.lblBBVersion.Location = New System.Drawing.Point(634, 33)
        Me.lblBBVersion.Name = "lblBBVersion"
        Me.lblBBVersion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBBVersion.Size = New System.Drawing.Size(97, 16)
        Me.lblBBVersion.TabIndex = 24
        Me.lblBBVersion.Text = "lblBBVersion"
        '
        'prgActionProgress
        '
        Me.prgActionProgress.Location = New System.Drawing.Point(16, 84)
        Me.prgActionProgress.Name = "prgActionProgress"
        Me.prgActionProgress.Size = New System.Drawing.Size(704, 17)
        Me.prgActionProgress.TabIndex = 26
        Me.prgActionProgress.Visible = False
        '
        'imgPlus
        '
        Me.imgPlus.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPlus.Location = New System.Drawing.Point(384, 108)
        Me.imgPlus.Name = "imgPlus"
        Me.imgPlus.Size = New System.Drawing.Size(16, 16)
        Me.imgPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgPlus.TabIndex = 27
        Me.imgPlus.TabStop = False
        Me.imgPlus.Visible = False
        '
        'lblProfileGroup
        '
        Me.lblProfileGroup.BackColor = System.Drawing.SystemColors.Control
        Me.lblProfileGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProfileGroup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProfileGroup.Location = New System.Drawing.Point(16, 51)
        Me.lblProfileGroup.Name = "lblProfileGroup"
        Me.lblProfileGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProfileGroup.Size = New System.Drawing.Size(97, 17)
        Me.lblProfileGroup.TabIndex = 17
        Me.lblProfileGroup.Text = "35017-Profilgruppe"
        '
        'lblExp
        '
        Me.lblExp.BackColor = System.Drawing.SystemColors.Control
        Me.lblExp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExp.Location = New System.Drawing.Point(16, 107)
        Me.lblExp.Name = "lblExp"
        Me.lblExp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExp.Size = New System.Drawing.Size(313, 16)
        Me.lblExp.TabIndex = 3
        Me.lblExp.Text = "60008 - Files TO bank"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HjemToolStripMenuItem, Me.OppsettToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(733, 24)
        Me.MenuStrip1.TabIndex = 32
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HjemToolStripMenuItem
        '
        Me.HjemToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExportFiles, Me.mnuImportFiles, Me.mnuExit})
        Me.HjemToolStripMenuItem.Name = "HjemToolStripMenuItem"
        Me.HjemToolStripMenuItem.Size = New System.Drawing.Size(49, 20)
        Me.HjemToolStripMenuItem.Text = "50100"
        '
        'mnuExportFiles
        '
        Me.mnuExportFiles.Name = "mnuExportFiles"
        Me.mnuExportFiles.Size = New System.Drawing.Size(104, 22)
        Me.mnuExportFiles.Text = "60008"
        '
        'mnuImportFiles
        '
        Me.mnuImportFiles.Name = "mnuImportFiles"
        Me.mnuImportFiles.Size = New System.Drawing.Size(104, 22)
        Me.mnuImportFiles.Text = "60009"
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(104, 22)
        Me.mnuExit.Text = "50110"
        '
        'OppsettToolStripMenuItem
        '
        Me.OppsettToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProfileGroup, Me.mnuClients, Me.mnuReportSetup, Me.ToolStripMenuItem7, Me.mnuConvertXToBabelBank, Me.mnuUpgradeViews, Me.mnuLog, Me.mnuAbout})
        Me.OppsettToolStripMenuItem.Name = "OppsettToolStripMenuItem"
        Me.OppsettToolStripMenuItem.Size = New System.Drawing.Size(49, 20)
        Me.OppsettToolStripMenuItem.Text = "35005"
        '
        'mnuProfileGroup
        '
        Me.mnuProfileGroup.Name = "mnuProfileGroup"
        Me.mnuProfileGroup.Size = New System.Drawing.Size(191, 22)
        Me.mnuProfileGroup.Text = "35017"
        '
        'mnuClients
        '
        Me.mnuClients.Name = "mnuClients"
        Me.mnuClients.Size = New System.Drawing.Size(191, 22)
        Me.mnuClients.Text = "50801"
        '
        'mnuReportSetup
        '
        Me.mnuReportSetup.Name = "mnuReportSetup"
        Me.mnuReportSetup.Size = New System.Drawing.Size(191, 22)
        Me.mnuReportSetup.Text = "35213"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DanskToolStripMenuItem, Me.EnglishToolStripMenuItem, Me.NorskToolStripMenuItem, Me.SvenskaToolStripMenuItem})
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(191, 22)
        Me.ToolStripMenuItem7.Text = "35120"
        '
        'DanskToolStripMenuItem
        '
        Me.DanskToolStripMenuItem.Name = "DanskToolStripMenuItem"
        Me.DanskToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.DanskToolStripMenuItem.Text = "Dansk"
        '
        'EnglishToolStripMenuItem
        '
        Me.EnglishToolStripMenuItem.Name = "EnglishToolStripMenuItem"
        Me.EnglishToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.EnglishToolStripMenuItem.Text = "English"
        '
        'NorskToolStripMenuItem
        '
        Me.NorskToolStripMenuItem.Name = "NorskToolStripMenuItem"
        Me.NorskToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.NorskToolStripMenuItem.Text = "Norsk"
        '
        'SvenskaToolStripMenuItem
        '
        Me.SvenskaToolStripMenuItem.Name = "SvenskaToolStripMenuItem"
        Me.SvenskaToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.SvenskaToolStripMenuItem.Text = "Svenska"
        '
        'mnuConvertXToBabelBank
        '
        Me.mnuConvertXToBabelBank.Name = "mnuConvertXToBabelBank"
        Me.mnuConvertXToBabelBank.Size = New System.Drawing.Size(191, 22)
        Me.mnuConvertXToBabelBank.Text = "ConvertX->BabelBank"
        '
        'mnuUpgradeViews
        '
        Me.mnuUpgradeViews.Name = "mnuUpgradeViews"
        Me.mnuUpgradeViews.Size = New System.Drawing.Size(191, 22)
        Me.mnuUpgradeViews.Text = "35182"
        '
        'mnuLog
        '
        Me.mnuLog.Name = "mnuLog"
        Me.mnuLog.Size = New System.Drawing.Size(191, 22)
        Me.mnuLog.Text = "35121"
        '
        'mnuAbout
        '
        Me.mnuAbout.Name = "mnuAbout"
        Me.mnuAbout.Size = New System.Drawing.Size(191, 22)
        Me.mnuAbout.Text = "50905"
        '
        'gridExp
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridExp.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridExp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridExp.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridExp.Location = New System.Drawing.Point(12, 127)
        Me.gridExp.Name = "gridExp"
        Me.gridExp.Size = New System.Drawing.Size(709, 150)
        Me.gridExp.TabIndex = 34
        '
        'gridImp
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridImp.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridImp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridImp.DefaultCellStyle = DataGridViewCellStyle4
        Me.gridImp.Location = New System.Drawing.Point(12, 310)
        Me.gridImp.Name = "gridImp"
        Me.gridImp.Size = New System.Drawing.Size(709, 150)
        Me.gridImp.TabIndex = 35
        '
        'lblImp
        '
        Me.lblImp.BackColor = System.Drawing.SystemColors.Control
        Me.lblImp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblImp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblImp.Location = New System.Drawing.Point(16, 288)
        Me.lblImp.Name = "lblImp"
        Me.lblImp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblImp.Size = New System.Drawing.Size(313, 16)
        Me.lblImp.TabIndex = 36
        Me.lblImp.Text = "60009 - Files FROM bank"
        '
        'frmVisma_Setup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(733, 500)
        Me.Controls.Add(Me.lblImp)
        Me.Controls.Add(Me.gridImp)
        Me.Controls.Add(Me.gridExp)
        Me.Controls.Add(Me.lblBBVersion)
        Me.Controls.Add(Me.cmdHelp)
        Me.Controls.Add(Me.cmbProfileGroup)
        Me.Controls.Add(Me.prgActionProgress)
        Me.Controls.Add(Me.imgPlus)
        Me.Controls.Add(Me.lblProfileGroup)
        Me.Controls.Add(Me.lblExp)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(10, 45)
        Me.Name = "frmVisma_Setup"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visma Payment (BabelBank)"
        CType(Me.imgPlus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.gridExp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridImp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents HjemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OppsettToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProfileGroup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuConvertXToBabelBank As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DanskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnglishToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NorskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SvenskaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpgradeViews As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gridExp As System.Windows.Forms.DataGridView
    Friend WithEvents gridImp As System.Windows.Forms.DataGridView
    Public WithEvents lblImp As System.Windows.Forms.Label
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
#End Region
End Class