Option Strict Off
Option Explicit On
Friend Class frmAbout
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		Me.Close()
		
	End Sub
    Private Sub frmAbout_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg", 850, 100)
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

    End Sub
    Private Sub UpgradeToAccess2016()
        ' UPGRADE from Access 2000 to Access 2016 (.accdb format)
        ' NB! Works for one-database solution only !!!!

        Dim myConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        Dim s1997File As String
        Dim s2016File As String
        Dim sErrorCode As String = ""

        'Dim myConfigSettings = System.Configuration.ConfigurationManager.AppSettings

        Try
            sErrorCode = "0001 - Start"
            'If MsgBox("For � oppgradere databasen, m� du ha skrivetilgang til BabelBanks programomr�de." & vbNewLine & My.Application.Info.DirectoryPath & vbNewLine & "Har du ikke dette, kontakt din IT support, eller Visual Banking." & vbNewLine & vbNewLine & "�nsker du � fortsette?", vbYesNo + MsgBoxStyle.Question) = vbYes Then
            If MsgBox(LRS(60441) & vbNewLine & My.Application.Info.DirectoryPath & vbNewLine & vbNewLine & LRS(60442) & vbNewLine & vbNewLine & LRS(60293), vbYesNo + MsgBoxStyle.Question) = vbYes Then
                's1997file = "C:\Projects\Babelbank\Profiles\profile.mdb"
                s1997File = BB_DatabasePath()
                ' test if databasename already is .accdb
                If InStr(s1997File.ToLower, ".accdb") > 0 Then
                    'MsgBox("Database has already been converted to Access 2016", MsgBoxStyle.Exclamation)
                    MsgBox(LRS(60437), MsgBoxStyle.Exclamation)
                Else

                    'If MsgBox("Er du sikker p� at du vil oppgradere BabelBanks database til Access 2016?", vbYesNo + MsgBoxStyle.Question) = vbYes Then
                    If MsgBox(LRS(60438), vbYesNo + MsgBoxStyle.Question) = vbYes Then
                        's2016File = "C:\Projects\Babelbank\Profiles\profile_2007.accdb"
                        ' redo to same path, but with accdb
                        s2016File = Replace(s1997File.ToLower, ".mdb", ".accdb")

                        ' Check if file already present - if so, do not proceed
                        If System.IO.File.Exists(s2016File) Then
                            'MsgBox("The database file already exists!" & vbNewLine & s2016File, MsgBoxStyle.Exclamation)
                            MsgBox(LRS(60439) & vbNewLine & s2016File, MsgBoxStyle.Exclamation)
                        Else
                            sErrorCode = "0010 - Before ConvertAccessProject"
                            Dim x As New Microsoft.Office.Interop.Access.Application
                            x.ConvertAccessProject(s1997File, s2016File, Microsoft.Office.Interop.Access.AcFileFormat.acFileFormatAccess2007)
                            x.Quit()
                            sErrorCode = "0011 - After ConvertAccessProject"


                            ' and change in BabelBankExe.config
                            ' update config file.
                            ' Must have values like this:
                            '<add key="COMPANYNAME1" value="Visual Banking" />
                            'If myConfig.("COMPANYNAME1") = "" Then
                            ' need to have a COMPANYNAME1 in BabelBankExe.config
                            ' find name from licensefile
                            Dim oLicense As New vbBabel.License
                            myConfig.AppSettings.Settings.Remove("COMPANYNAME1")
                            myConfig.AppSettings.Settings.Add("COMPANYNAME1", oLicense.LicName)
                            oLicense = Nothing
                            sErrorCode = "0101 - COMPANYNAME1 set"

                            'End If
                            '<add key="SQL_SERVER1" value="FALSE" />
                            'myConfigSettings("SQL_SERVER1") = "False"
                            myConfig.AppSettings.Settings.Remove("SQL_SERVER1")
                            myConfig.AppSettings.Settings.Add("SQL_SERVER1", "FALSE")
                            sErrorCode = "0102 - SQL_SERVER1 set"

                            '<add key="ACCESS20161" value="TRUE" />
                            'myConfigSettings("ACCESS20161") = "True"
                            myConfig.AppSettings.Settings.Remove("ACCESS20161")
                            myConfig.AppSettings.Settings.Add("ACCESS20161", "TRUE")
                            sErrorCode = "0103 - ACCESS20161 set"

                            '<add key="DATABASEPATH1" value="c:\projects\babelbank\profiles\profile.accdb" />
                            'myConfigSettings("DATABASEPATH1") = s2016File
                            myConfig.AppSettings.Settings.Remove("DATABASEPATH1")
                            myConfig.AppSettings.Settings.Add("DATABASEPATH1", s2016File)
                            sErrorCode = "0104 - DATABASEPATH1 set"

                            myConfig.AppSettings.Settings.Remove("LICENSEPATH1")
                            myConfig.AppSettings.Settings.Add("LICENSEPATH1", BB_LicensePath())
                            sErrorCode = "0105 - LICENSEPATH1 set"

                            '<add key="BB_CONNECTIONSTRING1" value="C:\Projects\Babelbank\Profiles\profile.accdb" />
                            'myConfigSettings("BB_CONNECTIONSTRING1") = s2016File
                            myConfig.AppSettings.Settings.Remove("BB_CONNECTIONSTRING1")
                            myConfig.AppSettings.Settings.Add("BB_CONNECTIONSTRING1", s2016File)
                            sErrorCode = "0106 - BB_CONNECTIOSTRING1 set"

                            'Save the configuration file.
                            myConfig.Save(ConfigurationSaveMode.Modified)
                            sErrorCode = "0110 - Configuration modified"
                            'Force a reload of a changed section.
                            ConfigurationManager.RefreshSection("appSettings")
                            sErrorCode = "0111 - Configuration saved"

                            '
                            ' exit BabelBank?
                            ' 
                            'MsgBox("Vellykket konvertering av database." & vbNewLine & "BabelBank avsluttes.", MsgBoxStyle.Exclamation)
                            MsgBox(LRS(60440) & vbNewLine & LRS(15006) & ".", MsgBoxStyle.Exclamation)
                            End
                        End If
                    End If
                End If
            End If
        Catch
            ' todo: what to put here?
            ' Error when converting the Access database! Contact Visual Banking
            MsgBox(LRS(60443) & vbNewLine & vbNewLine & LRS(60444) & vbNewLine & vbNewLine & "Error:" & sErrorCode, MsgBoxStyle.Critical)

        End Try
    End Sub

    Private Sub TeamViewer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles frmTeamViewer.Click
        ' call TeamViewer Quick support
        Dim sURL As String
        
        sURL = "https://download.teamviewer.com/download/TeamViewerQS.exe"

        ShellExecute(Me.Handle.ToInt32, "open", sURL, "", "", SW_SHOW)
    End Sub

    Private Sub cmdAccess2016_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAccess2016.Click
        UpgradeToAccess2016()
    End Sub
End Class
