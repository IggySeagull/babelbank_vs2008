﻿Imports System.Windows.Forms
Imports VB = Microsoft.VisualBasic
' Mangler
' Flytt opp/ned

Public Class frmRightClick
    Private bSave As Boolean
    Private sErrorString As String
    Private bUpdateDone As Boolean
    Private sCompany_ID As String
    Private iManualMenuItemID As Integer = -1
    Private iManualMenuItemPostingLineID As Integer = -1
    Private iPostingLine As Integer
    Private bInitMode As Boolean = True
    Private iLevel3UsedItems As Integer = 1 ' how many "level3", postinglinelevel, are in use 
    Private bChangesDone As Boolean

    Private lManualLine_CurrentCell As Integer
    Private lManualLine_CurrentRow As Integer
    Private lManualPosting_CurrentRow As Integer
    Private lManualPosting_CurrentCell As Integer
    Private myRow As DataGridViewRow

    Private Sub frmRightClick_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SaveRightClickRow(-9, -9)  ' make sure last posting is saved
        e.Cancel = True
        Me.Hide()
    End Sub
    Private Sub frmRightClick_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        Dim oDal As vbBabel.DAL

        Try
            FormvbStyle(Me, "dollar.jpg")
            FormLRSCaptions(Me)

            For Each ctl In Me.Controls
                If TypeName(ctl) = "PictureBox" Then
                    If ctl.Left = 0 And ctl.Top = 0 Then
                        ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                        Me.lblHeading.Location = New Point(550, 18)
                        Me.lblHeading.BackColor = Color.Transparent
                        Me.lblHeading.Parent = ctl
                    End If
                End If
            Next

            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

            bInitMode = True
            gridMenuItem_Construct()
            ControlsConstruct()  ' Fill in values in controls other than grids
            gridmenuItem_Fill()

            bInitMode = False
            ' position to gridMenuItem, cell 0,3
            Me.gridMenuItem_RowEnter(Me.gridMenuItem, New DataGridViewCellEventArgs(3, 0))
            Me.gridMenuItem.CurrentCell = Me.gridMenuItem.Rows(0).Cells(3)

            Me.gridMenuItem.Focus()
            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            bUpdateDone = False
            bChangesDone = False

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            Throw New Exception("Function: frmRightClick.Load" & vbCrLf & ex.Message)

        End Try

    End Sub
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        SaveRightClickRow(-9, -9)  ' make sure last posting is saved
        Me.Close()
    End Sub
    Friend Sub SetCompany_ID(ByVal s As String)
        sCompany_ID = s
    End Sub
    Private Sub HideLevel3()
        ' Show only as many level 3 as needed
        Select Case iLevel3UsedItems

            Case 0
                Me.cmbFieldName2.Visible = False
                Me.lbl2.Visible = False
                Me.cmbFieldSource2.Visible = False
                Me.txtValue2.Visible = False

                Me.cmbFieldName3.Visible = False
                Me.lbl3.Visible = False
                Me.cmbFieldSource3.Visible = False
                Me.txtValue3.Visible = False

                Me.cmbFieldName4.Visible = False
                Me.lbl4.Visible = False
                Me.cmbFieldSource4.Visible = False
                Me.txtValue4.Visible = False

                Me.cmbFieldName5.Visible = False
                Me.lbl5.Visible = False
                Me.cmbFieldSource5.Visible = False
                Me.txtValue5.Visible = False

                Me.cmbFieldName6.Visible = False
                Me.lbl6.Visible = False
                Me.cmbFieldSource6.Visible = False
                Me.txtValue6.Visible = False

                Me.cmbFieldName7.Visible = False
                Me.lbl7.Visible = False
                Me.cmbFieldSource7.Visible = False
                Me.txtValue7.Visible = False

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 1
                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = False
                Me.lbl3.Visible = False
                Me.cmbFieldSource3.Visible = False
                Me.txtValue3.Visible = False

                Me.cmbFieldName4.Visible = False
                Me.lbl4.Visible = False
                Me.cmbFieldSource4.Visible = False
                Me.txtValue4.Visible = False

                Me.cmbFieldName5.Visible = False
                Me.lbl5.Visible = False
                Me.cmbFieldSource5.Visible = False
                Me.txtValue5.Visible = False

                Me.cmbFieldName6.Visible = False
                Me.lbl6.Visible = False
                Me.cmbFieldSource6.Visible = False
                Me.txtValue6.Visible = False

                Me.cmbFieldName7.Visible = False
                Me.lbl7.Visible = False
                Me.cmbFieldSource7.Visible = False
                Me.txtValue7.Visible = False

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 2
                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = True
                Me.lbl3.Visible = True
                Me.cmbFieldSource3.Visible = True
                Me.txtValue3.Visible = True

                Me.cmbFieldName4.Visible = False
                Me.lbl4.Visible = False
                Me.cmbFieldSource4.Visible = False
                Me.txtValue4.Visible = False

                Me.cmbFieldName5.Visible = False
                Me.lbl5.Visible = False
                Me.cmbFieldSource5.Visible = False
                Me.txtValue5.Visible = False

                Me.cmbFieldName6.Visible = False
                Me.lbl6.Visible = False
                Me.cmbFieldSource6.Visible = False
                Me.txtValue6.Visible = False

                Me.cmbFieldName7.Visible = False
                Me.lbl7.Visible = False
                Me.cmbFieldSource7.Visible = False
                Me.txtValue7.Visible = False

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 3

                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = True
                Me.lbl3.Visible = True
                Me.cmbFieldSource3.Visible = True
                Me.txtValue3.Visible = True

                Me.cmbFieldName4.Visible = True
                Me.lbl4.Visible = True
                Me.cmbFieldSource4.Visible = True
                Me.txtValue4.Visible = True

                Me.cmbFieldName5.Visible = False
                Me.lbl5.Visible = False
                Me.cmbFieldSource5.Visible = False
                Me.txtValue5.Visible = False

                Me.cmbFieldName6.Visible = False
                Me.lbl6.Visible = False
                Me.cmbFieldSource6.Visible = False
                Me.txtValue6.Visible = False

                Me.cmbFieldName7.Visible = False
                Me.lbl7.Visible = False
                Me.cmbFieldSource7.Visible = False
                Me.txtValue7.Visible = False

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 4

                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = True
                Me.lbl3.Visible = True
                Me.cmbFieldSource3.Visible = True
                Me.txtValue3.Visible = True

                Me.cmbFieldName4.Visible = True
                Me.lbl4.Visible = True
                Me.cmbFieldSource4.Visible = True
                Me.txtValue4.Visible = True

                Me.cmbFieldName5.Visible = True
                Me.lbl5.Visible = True
                Me.cmbFieldSource5.Visible = True
                Me.txtValue5.Visible = True

                Me.cmbFieldName6.Visible = False
                Me.lbl6.Visible = False
                Me.cmbFieldSource6.Visible = False
                Me.txtValue6.Visible = False

                Me.cmbFieldName7.Visible = False
                Me.lbl7.Visible = False
                Me.cmbFieldSource7.Visible = False
                Me.txtValue7.Visible = False

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 5

                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = True
                Me.lbl3.Visible = True
                Me.cmbFieldSource3.Visible = True
                Me.txtValue3.Visible = True

                Me.cmbFieldName4.Visible = True
                Me.lbl4.Visible = True
                Me.cmbFieldSource4.Visible = True
                Me.txtValue4.Visible = True

                Me.cmbFieldName5.Visible = True
                Me.lbl5.Visible = True
                Me.cmbFieldSource5.Visible = True
                Me.txtValue5.Visible = True

                Me.cmbFieldName6.Visible = True
                Me.lbl6.Visible = True
                Me.cmbFieldSource6.Visible = True
                Me.txtValue6.Visible = True

                Me.cmbFieldName7.Visible = False
                Me.lbl7.Visible = False
                Me.cmbFieldSource7.Visible = False
                Me.txtValue7.Visible = False

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 6

                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = True
                Me.lbl3.Visible = True
                Me.cmbFieldSource3.Visible = True
                Me.txtValue3.Visible = True

                Me.cmbFieldName4.Visible = True
                Me.lbl4.Visible = True
                Me.cmbFieldSource4.Visible = True
                Me.txtValue4.Visible = True

                Me.cmbFieldName5.Visible = True
                Me.lbl5.Visible = True
                Me.cmbFieldSource5.Visible = True
                Me.txtValue5.Visible = True

                Me.cmbFieldName6.Visible = True
                Me.lbl6.Visible = True
                Me.cmbFieldSource6.Visible = True
                Me.txtValue6.Visible = True

                Me.cmbFieldName7.Visible = True
                Me.lbl7.Visible = True
                Me.cmbFieldSource7.Visible = True
                Me.txtValue7.Visible = True

                Me.cmbFieldName8.Visible = False
                Me.lbl8.Visible = False
                Me.cmbFieldSource8.Visible = False
                Me.txtValue8.Visible = False

            Case 7
                Me.cmbFieldName2.Visible = True
                Me.lbl2.Visible = True
                Me.cmbFieldSource2.Visible = True
                Me.txtValue2.Visible = True

                Me.cmbFieldName3.Visible = True
                Me.lbl3.Visible = True
                Me.cmbFieldSource3.Visible = True
                Me.txtValue3.Visible = True

                Me.cmbFieldName4.Visible = True
                Me.lbl4.Visible = True
                Me.cmbFieldSource4.Visible = True
                Me.txtValue4.Visible = True

                Me.cmbFieldName5.Visible = True
                Me.lbl5.Visible = True
                Me.cmbFieldSource5.Visible = True
                Me.txtValue5.Visible = True

                Me.cmbFieldName6.Visible = True
                Me.lbl6.Visible = True
                Me.cmbFieldSource6.Visible = True
                Me.txtValue6.Visible = True

                Me.cmbFieldName7.Visible = True
                Me.lbl7.Visible = True
                Me.cmbFieldSource7.Visible = True
                Me.txtValue7.Visible = True

                Me.cmbFieldName8.Visible = True
                Me.lbl8.Visible = True
                Me.cmbFieldSource8.Visible = True
                Me.txtValue8.Visible = True

        End Select

    End Sub
    Private Sub gridMenuItem_Construct()
        ' Instantiate gridmenuItem, the topmost grid in RighClick setup
        ' Set headers etc
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim ComboColumn As DataGridViewComboBoxColumn
        Dim checkBoxColumn As DataGridViewCheckBoxColumn

        With Me.gridMenuItem
            .Rows.Clear()  ' empty content of gridMenuItem

            .ReadOnly = False
            .Columns.Clear()

            .ScrollBars = ScrollBars.Both
            ' possible to add rows manually!
            .AllowUserToAddRows = True
            .RowHeadersVisible = True
            .BorderStyle = BorderStyle.None
            .BackgroundColor = Color.White
            .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
            .RowsDefaultCellStyle.SelectionForeColor = Color.Black
            .RowTemplate.Height = 18
            .CellBorderStyle = DataGridViewCellBorderStyle.None
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect

            ' Include all fields
            ' - 0 Company_ID        (Hidden)
            ' - 1 ManualMenuItemID  (Hidden)
            ' - 2 QOrder            (Hidden)
            ' - 3 Name
            ' - 4 SQL name
            ' - 5 Special
            ' - 6 AskFor (field) ComboBox
            ' - 7 AskForDefaultValue
            ' - 8 RefundPayment


            ' 0 Company_ID (Hidden)
            ' --------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "CompanyID"
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 1 ManualmenuItemID (Hidden)
            ' --------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "ManualMenuItemID"
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 2 QOrder (Hidden)
            ' --------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "QOrder"
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 3 Label for menuitem
            ' --------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60411)
            txtColumn.Visible = True
            txtColumn.Width = 250
            txtColumn.MinimumWidth = 250
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 4 SQL name
            ' -----------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(62039)
            txtColumn.Width = 200
            txtColumn.MinimumWidth = 200
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 5 Special
            ' -----------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60357)
            txtColumn.Width = 120
            txtColumn.MinimumWidth = 120
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 6 Fieldname (AskFor)
            ' ---------------------
            ComboColumn = New DataGridViewComboBoxColumn
            ComboColumn.HeaderText = LRS(60412)
            ComboColumn.Width = 150
            ComboColumn.MinimumWidth = 100
            ComboColumn.Items.Add("BB_Amount")

            .Columns.Add(ComboColumn)

            ' 7 AskForDefaultValue
            ' -----------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60413)
            txtColumn.Width = 120
            txtColumn.MinimumWidth = 120
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 8 Returnpayment - Checkbox
            ' --------------------------
            checkBoxColumn = New DataGridViewCheckBoxColumn
            With checkBoxColumn
                .HeaderText = LRS(60414)
                .FlatStyle = FlatStyle.Standard
                .CellTemplate = New DataGridViewCheckBoxCell()
                .CellTemplate.Style.BackColor = Color.White
                .TrueValue = True
                .FalseValue = False
            End With
            .Columns.Add(checkBoxColumn)


        End With

        ' Construct gridMenuItemPostingLine
        With gridMenuItemPostingline
            .Rows.Clear()  ' empty content of gridMenuItem

            .ReadOnly = False
            .Columns.Clear()

            .ScrollBars = ScrollBars.Both
            ' possible to add rows manually!
            .AllowUserToAddRows = True
            .RowHeadersVisible = True
            .BorderStyle = BorderStyle.None
            .BackgroundColor = Color.White
            .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
            .RowsDefaultCellStyle.SelectionForeColor = Color.Black
            .RowTemplate.Height = 18
            .CellBorderStyle = DataGridViewCellBorderStyle.None
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect

            ' Include all fields
            ' - 0 Company_ID                    (Hidden)
            ' - 1 ManualMenuItemID              (Hidden)
            ' - 2 ManualMenuItemPostingLineID   (Hidden)
            ' - 3 PostingLine
            ' - 4 FieldName                     (Hidden)
            ' - 5 FieldValue                    (Hidden)
            ' - 6 FieldSource                   (Hidden)
            ' - 7 PostingName

            ' 0 Company_ID (Hidden)
            ' --------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "CompanyID"
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 1 ManualmenuItemID (Hidden)
            ' --------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "ManualMenuItemID"
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 2 ManualMenuItemPostingLineID   (Hidden)
            ' ----------------------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "ManualMenuItemPostingLineID"
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 3 Postingline
            ' ----------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "PostingLine"
            txtColumn.Visible = True
            txtColumn.Width = 150
            txtColumn.MinimumWidth = 150
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 4 Fieldname, hidden
            ' -----------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60415)
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 5 FieldValue, hidden
            ' -----------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60416)
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 6 FieldSource, hidden
            ' ---------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60417)
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            ' 7 PostingName
            ' ---------------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60418)
            txtColumn.Width = 300
            txtColumn.MinimumWidth = 300
            .Columns.Add(txtColumn)
            txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable


        End With

    End Sub
    Private Sub ControlsConstruct()
        Dim ctl As System.Windows.Forms.Control
        Dim comboCtl As ComboBox
        ' fill up possible fieldnames
        For Each ctl In Me.Controls
            If VB.Left(ctl.Name, 12) = "cmbFieldName" Then
                comboCtl = ctl
                FillFieldNames_IntoCombo(comboCtl)
            End If
            'If VB.Left(ctl.Name, 12) = "cmbFieldSource1" Then
            ' comboCtl = ctl
            ' FillFieldSource_IntoCombo(comboCtl, False)
            ' End If
        Next
    End Sub
    Private Sub gridmenuItem_Fill()
        Dim sMySQL As String
        Dim myRow As DataGridViewRow
        Dim i As Integer
        Dim ComboColumn As DataGridViewComboBoxColumn
        Dim stmp As String = ""
        Dim oDal As vbBabel.DAL

        iManualMenuItemID = 0
        Try
            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

            sMySQL = "SELECT * FROM ManualMenuItem WHERE Company_ID = " & sCompany_ID
            oDal.SQL = sMySQL

            oDal.Reader_Execute()
            If oDal.Reader_HasRows Then
                With Me.gridMenuItem
                    Do While oDal.Reader_ReadRecord

                        ' add new line to gridERP
                        .Rows.Add()
                        myRow = .Rows(.RowCount - 2)

                        ' Include all fields
                        ' - 0 Company_ID        (Hidden)
                        ' - 1 ManualMenuItemID  (Hidden)
                        ' - 2 QOrder            (Hidden)
                        ' - 3 Name
                        ' - 4 SQL name
                        ' - 5 Special
                        ' - 6 AskFor (field) ComboBox
                        ' - 7 AskForDefaultValue
                        ' - 8 RefundPayment

                        For i = 0 To oDal.Reader_FieldCount
                            stmp = oDal.Reader_GetField(i)
                            If i = 6 Then
                                'FieldName, combobox
                                ComboColumn = .Columns.Item(i)
                                'ComboColumn.Items.Clear()
                                'ComboColumn.Items.Add("BB_Amount")
                                'ComboColumn.Items.Add("BB_AccountNo")
                                ' Currently only BB_Amount to select
                                If oDal.Reader_GetField(i).ToUpper = "BB_AMOUNT" Then
                                    'ComboBox = myRow.Cells(i)
                                    'ComboBox.Value = "BB_Amount"
                                    myRow.Cells(i).Value = "BB_Amount"
                                Else
                                    ComboColumn.Selected = -1
                                End If
                                ComboColumn.Width = 100

                            ElseIf i = 8 Then
                                ' Repayment, checkbox
                                myRow.Cells(i).Value = CBool(oDal.Reader_GetField(i))
                            Else
                                myRow.Cells(i).Value = oDal.Reader_GetField(i)
                            End If

                        Next i

                    Loop
                    ' Find ManualMenuItemID for first row
                    iManualMenuItemID = Val(.Rows.Item(0).Cells.Item(1).Value)

                    ' position to first row, first visible col
                    .CurrentCell = .Rows(0).Cells(3)
                    gridmenuItemPostingLine_Fill()

                End With
            Else
                iLevel3UsedItems = 0
                HideLevel3()
            End If
            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If



        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            ' Other type of error - User must be warned!
            Throw New Exception("Function: gridMenuItem_Fill" & vbCrLf & ex.Message)

        End Try

    End Sub
    Private Sub gridmenuItemPostingLine_Fill()
        Dim sMySQL As String
        Dim myRow As DataGridViewRow
        Dim i As Integer
        Dim oDal As vbBabel.DAL

        Try
            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

            sMySQL = "SELECT MAX(Company_ID), MAX(ManualMenuItemID), MIN(ManualMenuItemPostingLineID), PostingLine, Max(FieldName), Max(FieldValue), Max(FieldSource), Max(PostingName) FROM ManualMenuItemPostingLine WHERE Company_ID = " & sCompany_ID & " AND ManualMenuItemID = " & iManualMenuItemID & " GROUP BY PostingLine "
            oDal.SQL = sMySQL

            bInitMode = True
            oDal.Reader_Execute()
            If oDal.Reader_HasRows Then
                With Me.gridMenuItemPostingline
                    Me.gridMenuItemPostingline.Rows.Clear()
                    Do While oDal.Reader_ReadRecord

                        ' add new line to gridERP
                        .Rows.Add()
                        myRow = .Rows(.RowCount - 2)

                        ' Include all fields
                        ' - 0 Company_ID                    (Hidden)
                        ' - 1 ManualMenuItemID              (Hidden)
                        ' - 2 ManualMenuItemPostingLineID   (Hidden)
                        ' - 3 PostingLine                   
                        ' - 4 FieldName                     (Hidden)    
                        ' - 5 FieldValue                    (Hidden)
                        ' - 6 FieldSource                   (Hidden)
                        ' - 7 PostingName 

                        For i = 0 To oDal.Reader_FieldCount
                            myRow.Cells(i).Value = oDal.Reader_GetField(i)
                        Next i

                    Loop
                    ' position to first row, first visible col
                    .CurrentCell = .Rows(0).Cells(3)
                    iPostingLine = .Rows(0).Cells(3).Value
                    FillLevel3()

                End With
            Else
                Me.gridMenuItemPostingline.Rows.Clear()
                iLevel3UsedItems = 0
                HideLevel3()
            End If
            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If
            bInitMode = False

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If
            bInitMode = False

            ' Other type of error - User must be warned!
            Throw New Exception("Function: gridMenuItemPostingLine_Fill" & vbCrLf & ex.Message)

        End Try

    End Sub
    Private Sub ResetLevel3()
        ' Reset all controls at level 3 (used before we fill in new values)
        Me.cmbFieldName1.SelectedIndex = -1
        Me.cmbFieldSource1.SelectedIndex = -1
        Me.txtValue1.Text = ""

        Me.cmbFieldName2.SelectedIndex = -1
        Me.cmbFieldSource2.SelectedIndex = -1
        Me.txtValue2.Text = ""

        Me.cmbFieldName3.SelectedIndex = -1
        Me.cmbFieldSource3.SelectedIndex = -1
        Me.txtValue3.Text = ""

        Me.cmbFieldName4.SelectedIndex = -1
        Me.cmbFieldSource4.SelectedIndex = -1
        Me.txtValue4.Text = ""

        Me.cmbFieldName5.SelectedIndex = -1
        Me.cmbFieldSource5.SelectedIndex = -1
        Me.txtValue5.Text = ""

        Me.cmbFieldName6.SelectedIndex = -1
        Me.cmbFieldSource6.SelectedIndex = -1
        Me.txtValue6.Text = ""

        Me.cmbFieldName7.SelectedIndex = -1
        Me.cmbFieldSource7.SelectedIndex = -1
        Me.txtValue7.Text = ""

        Me.cmbFieldName8.SelectedIndex = -1
        Me.cmbFieldSource8.SelectedIndex = -1
        Me.txtValue8.Text = ""

    End Sub
    Private Sub FillLevel3()
        ' Show info at level 3, based on selected row in level 2 (gridMenuItemPostingLine)
        Dim i As Integer
        Dim iFirstRow As Integer = -1
        Dim sFieldSource As String
        Dim sMySQL As String
        Dim myRow As DataGridViewRow
        Dim oDal As vbBabel.DAL

        ' make sure txtBox-changes are not trapped;
        bInitMode = True
        ' First, reset all values at Level3
        ResetLevel3()

        Try
            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

            sMySQL = "SELECT  FieldName, FieldValue, FieldSource FROM ManualMenuItemPostingLine WHERE Company_ID = " & sCompany_ID & " AND ManualMenuItemID = " & iManualMenuItemID & " AND PostingLine = " & CStr(iPostingLine)
            oDal.SQL = sMySQL

            oDal.Reader_Execute()
            If oDal.Reader_HasRows Then
                iLevel3UsedItems = 0

        
                Do While oDal.Reader_ReadRecord

                    ' Pick info from postinglines for "active" ManualMenuItemID row
                    ' show this one
                    iLevel3UsedItems = iLevel3UsedItems + 1
                    Select Case oDal.Reader_GetString("FieldSource")
                        Case 1
                            sFieldSource = LRS(60419)
                        Case 2
                            sFieldSource = LRS(60420)
                        Case 3
                            sFieldSource = LRS(60421)
                        Case 4
                            sFieldSource = LRS(60422)
                        Case 5
                            sFieldSource = LRS(60423)
                    End Select

                    Select Case iLevel3UsedItems
                        Case 1
                            Me.cmbFieldName1.SelectedIndex = Val(Me.cmbFieldName1.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource1.SelectedIndex = Me.cmbFieldSource1.FindStringExact(sFieldSource)
                            Me.txtValue1.Text = oDal.Reader_GetString("FieldValue")
                        Case 2
                            Me.cmbFieldName2.SelectedIndex = Val(Me.cmbFieldName2.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource2.SelectedIndex = Me.cmbFieldSource2.FindStringExact(sFieldSource)
                            Me.txtValue2.Text = oDal.Reader_GetString("FieldValue")
                        Case 3
                            Me.cmbFieldName3.SelectedIndex = Val(Me.cmbFieldName3.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource3.SelectedIndex = Me.cmbFieldSource3.FindStringExact(sFieldSource)
                            Me.txtValue3.Text = oDal.Reader_GetString("FieldValue")
                        Case 4
                            Me.cmbFieldName4.SelectedIndex = Val(Me.cmbFieldName4.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource4.SelectedIndex = Me.cmbFieldSource4.FindStringExact(sFieldSource)
                            Me.txtValue4.Text = oDal.Reader_GetString("FieldValue")
                        Case 5
                            Me.cmbFieldName5.SelectedIndex = Val(Me.cmbFieldName5.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource5.SelectedIndex = Me.cmbFieldSource5.FindStringExact(sFieldSource)
                            Me.txtValue5.Text = oDal.Reader_GetString("FieldValue")
                        Case 6
                            Me.cmbFieldName6.SelectedIndex = Val(Me.cmbFieldName6.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource6.SelectedIndex = Me.cmbFieldSource6.FindStringExact(sFieldSource)
                            Me.txtValue6.Text = oDal.Reader_GetString("FieldValue")
                        Case 7
                            Me.cmbFieldName7.SelectedIndex = Val(Me.cmbFieldName7.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource7.SelectedIndex = Me.cmbFieldSource7.FindStringExact(sFieldSource)
                            Me.txtValue7.Text = oDal.Reader_GetString("FieldValue")
                        Case 8
                            Me.cmbFieldName8.SelectedIndex = Val(Me.cmbFieldName8.FindStringExact(oDal.Reader_GetString("FieldName")))
                            Me.cmbFieldSource8.SelectedIndex = Me.cmbFieldSource8.FindStringExact(sFieldSource)
                            Me.txtValue8.Text = oDal.Reader_GetString("FieldValue")
                    End Select
                Loop    'Do While oDal.Reader_ReadRecord
                ' reset to current cell
                ' Funker ikke - hvorfor ?????????? .CurrentCell = .Rows(lManualPosting_CurrentRow).Cells(lManualPosting_CurrentCell)
            Else
                iLevel3UsedItems = 0
            End If
            ' Show only as many level 3 as needed
            HideLevel3()
            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            bInitMode = False

        Catch ex As Exception
            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            ' Other type of error - User must be warned!
            Throw New Exception("Function: FillLevel3Info" & vbCrLf & ex.Message)

        End Try
    End Sub

    Private Sub gridMenuItem_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItem.CellValueChanged
        ' trap changes
        If Not bInitMode Then
            bChangesDone = True
        End If
    End Sub

    Private Sub gridMenuItem_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles gridMenuItem.DragDrop
        Dim rowIndexOfItemUnderMouseToDrop As Integer
        Dim clientPoint As Point
        clientPoint = gridMenuItem.PointToClient(New Point(e.X, e.Y))
        rowIndexOfItemUnderMouseToDrop = gridMenuItem.HitTest(clientPoint.X, clientPoint.Y).RowIndex

        If e.Effect = DragDropEffects.Move Then
            gridMenuItem.Rows.RemoveAt(lManualLine_CurrentRow)
            gridMenuItem.Rows.Insert(rowIndexOfItemUnderMouseToDrop, myRow)
        End If
    End Sub

    Private Sub gridMenuItem_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles gridMenuItem.DragEnter
        If gridMenuItem.SelectedRows.Count > 0 Then
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    'Private Sub gridMenuItem_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles gridMenuItem.MouseClick
    '    If gridMenuItem.SelectedRows.Count = 1 Then

    '        If e.Button = MouseButtons.Left Then

    '            myRow = gridMenuItem.SelectedRows(0)
    '            lManualLine_CurrentRow = gridMenuItem.SelectedRows(0).Index
    '            gridMenuItem.DoDragDrop(myRow, DragDropEffects.Move)
    '        End If
    '    End If

    'End Sub
    'Private Sub gridMenuItem_CellMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles gridMenuItem.CellMouseDown
    '    'If e.Button = MouseButtons.Left Then

    '    '    myRow = gridMenuItem.Rows(e.RowIndex)
    '    '    lManualLine_CurrentRow = e.RowIndex
    '    '    gridMenuItem.DoDragDrop(myRow, DragDropEffects.Move)
    '    'End If
    '    Dim data As DataGridView = CType(sender, DataGridView)

    '    ' Get the index of the item the mouse is below.

    '    Dim rowIndexFromMouseDown = gridMenuItem.HitTest(e.X, e.Y).RowIndex
    '    gridMenuItem.Rows(rowIndexFromMouseDown).Selected = True

    '    If gridMenuItem.SelectedRows.Count = 1 Then

    '        data.DoDragDrop(data.SelectedRows(0), DragDropEffects.Copy)
    '    End If
    'End Sub
    'Private Sub gridMenuItem_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItem.CellClick
    '    ' catch manualMenuItemID on current row, and "filter" postinglines in gridMenuItemsPostingLine
    '    With Me.gridMenuItem
    '        If Val(.Rows(e.RowIndex).Cells(1).Value) <> iManualMenuItemID Then
    '            iManualMenuItemID = Val(.Rows(e.RowIndex).Cells(1).Value)
    '            FilterPostingLines()
    '        End If
    '    End With
    'End Sub

    Private Sub gridMenuItem_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItem.RowEnter
        ' catch manualMenuItemID on current row, and "filter" postinglines in gridMenuItemsPostingLine
        If Not bInitMode Then
            With Me.gridMenuItem
                ' Check if we need to save
                SaveRightClickRow(e.RowIndex, -1)

                If iManualMenuItemID <> Val(.Rows(e.RowIndex).Cells(1).Value) Then
                    ' trap changes only if we hit another row than last time
                    iManualMenuItemID = Val(.Rows(e.RowIndex).Cells(1).Value)
                    lManualLine_CurrentCell = e.ColumnIndex
                    lManualLine_CurrentRow = e.RowIndex
                    gridmenuItemPostingLine_Fill()  ' Fill up Level2 for this MenuItemID

                    ' Position to first item at level2, and fill level 3
                    Me.gridMenuItemPostingline.CurrentCell = Me.gridMenuItemPostingline.Rows(0).Cells(3)
                    Me.gridMenuItemPostingline_RowEnter(Me.gridMenuItemPostingline, New DataGridViewCellEventArgs(3, 0))
                End If
            End With
        End If
    End Sub
    Private Sub cmbFieldSource1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource1.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource1, så gjøres generell
        If cmbFieldSource1.SelectedIndex = 0 Then
            ' 1  = Fritekst, ingen tester
            ' Show txtFieldValue1
            Me.txtValue1.Visible = True
        Else
            ' all other than 1 - Freetext, will have no input in txtValue
            Me.txtValue1.Visible = False
            Me.txtValue1.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource2.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource1, så gjøres generell
        If cmbFieldSource2.SelectedIndex = 0 Then
            ' 1  = Fritekst, ingen tester
            ' Show txtFieldValue1
            Me.txtValue2.Visible = True
        Else
            ' all other than 1 - Freetext, will have no input in txtValue
            Me.txtValue2.Visible = False
            Me.txtValue2.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource3.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource3, så gjøres generell
        If cmbFieldSource3.SelectedIndex = 0 Then
            ' 3  = Fritekst, ingen tester
            ' Show txtFieldValue3
            Me.txtValue3.Visible = True
        Else
            ' all other than 3 - Freetext, will have no input in txtValue
            Me.txtValue3.Visible = False
            Me.txtValue3.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource4.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource4, så gjøres generell
        If cmbFieldSource4.SelectedIndex = 0 Then
            ' 4  = Fritekst, ingen tester
            ' Show txtFieldValue4
            Me.txtValue4.Visible = True
        Else
            ' all other than 4 - Freetext, will have no input in txtValue
            Me.txtValue4.Visible = False
            Me.txtValue4.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource5.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource5, så gjøres generell
        If cmbFieldSource5.SelectedIndex = 0 Then
            ' 5  = Fritekst, ingen tester
            ' Show txtFieldValue5
            Me.txtValue5.Visible = True
        Else
            ' all other than 5 - Freetext, will have no input in txtValue
            Me.txtValue5.Visible = False
            Me.txtValue5.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource6_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource6.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource6, så gjøres generell
        If cmbFieldSource6.SelectedIndex = 0 Then
            ' 6  = Fritekst, ingen tester
            ' Show txtFieldValue6
            Me.txtValue6.Visible = True
        Else
            ' all other than 6 - Freetext, will have no input in txtValue
            Me.txtValue6.Visible = False
            Me.txtValue6.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource7.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource7, så gjøres generell
        If cmbFieldSource7.SelectedIndex = 0 Then
            ' 7  = Fritekst, ingen tester
            ' Show txtFieldValue7
            Me.txtValue7.Visible = True
        Else
            ' all other than 7 - Freetext, will have no input in txtValue
            Me.txtValue7.Visible = False
            Me.txtValue7.Text = ""

        End If
    End Sub
    Private Sub cmbFieldSource8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldSource8.SelectedIndexChanged
        ' selection in fieldsource will determine type of possible selections in FieldValue

        ' først for fieldsource8, så gjøres generell
        If cmbFieldSource8.SelectedIndex = 0 Then
            ' 8  = Fritekst, ingen tester
            ' Show txtFieldValue8
            Me.txtValue8.Visible = True
        Else
            ' all other than 8 - Freetext, will have no input in txtValue
            Me.txtValue8.Visible = False
            Me.txtValue8.Text = ""

        End If
    End Sub


    Private Sub cmbFieldName1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName1.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName1.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName2.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName2.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName3.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName3.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName4.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName4.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName5.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName5.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName6_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName6.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName6.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName7.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName7.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub
    Private Sub cmbFieldName8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFieldName8.SelectedIndexChanged
        ' For first 6 values in FieldName, we can have all kinds of source.
        ' for Fieldnames from 7 and up, we can have Fieldsource 1 Fixed, 2 From Grid, 4 from SQL and 5 SpecialFixed
        If cmbFieldName8.SelectedIndex < 6 Then
            ' all values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, False)
        Else
            ' 4 values in FieldSource
            FillFieldSource_IntoComboByName(sender.name, True)
        End If
    End Sub

    Private Sub FillFieldNames_IntoCombo(ByVal ctl As System.Windows.Forms.ComboBox)
        ' For all situations, pick first 5
        ctl.Items.Add("Not in use")  ' needs this one to "delete" posting
        ctl.Items.Add("AmountCredit")
        ctl.Items.Add("AmountDebit")
        ctl.Items.Add("Currency")
        ctl.Items.Add("Name")
        ctl.Items.Add("FreeText")
        ' For fieldsource 2 FromGridERP and 4 FromSQL pick all
        ctl.Items.Add("CustomerNo")
        ctl.Items.Add("aKontoID")
        ctl.Items.Add("Dim1")
        ctl.Items.Add("Dim2")
        ctl.Items.Add("Dim3")
        ctl.Items.Add("Dim4")
        ctl.Items.Add("Dim5")
        ctl.Items.Add("Dim6")
        ctl.Items.Add("Dim7")
        ctl.Items.Add("Dim8")
        ctl.Items.Add("Dim9")
        ctl.Items.Add("Dim10")
        ctl.Items.Add("DiscountAmount")
        ctl.Items.Add("ExternalClientNo")
        ctl.Items.Add("Extra1")
        ctl.Items.Add("GLAccount")
        ctl.Items.Add("InvoiceNo")
        ctl.Items.Add("MatchID")
        ctl.Items.Add("MyField")
        ctl.Items.Add("MyField2")
        ctl.Items.Add("MyField3")
        ctl.Items.Add("VendorNo")
    End Sub
    'Private Sub FillFieldSource_IntoCombo(ByVal ctl As System.Windows.Forms.ComboBox, Optional ByVal bRemovePayment As Boolean = False)
    '    ctl.Items.Add(LRS(60419))   ' 1 = Fixed value
    '    ctl.Items.Add(LRS(60420))   ' 2 - From gridresult
    '    If bRemovePayment Then
    '        ctl.Items.Add(LRS(60421))   ' 3 - From payment
    '    End If
    '    ctl.Items.Add(LRS(60422))   ' 4 - From SQL
    '    ctl.Items.Add(LRS(60423))   ' 5 - Special fixed value
    'End Sub
    Private Sub FillFieldSource_IntoComboByName(ByVal ctlName As String, Optional ByVal bRemovePayment As Boolean = False)
        ' Har ikke funnet noen måte å angi dette generelt, må ta control for kontroll
        Select Case ctlName
            Case "cmbFieldName1"
                With Me.cmbFieldSource1
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName2"
                With Me.cmbFieldSource2
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName3"
                With Me.cmbFieldSource3
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName4"
                With Me.cmbFieldSource4
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName5"
                With Me.cmbFieldSource5
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName6"
                With Me.cmbFieldSource6
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName7"
                With Me.cmbFieldSource7
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
            Case "cmbFieldName8"
                With Me.cmbFieldSource8
                    .Items.Clear()
                    .Items.Add(LRS(60419))   ' 1 = Fixed value
                    .Items.Add(LRS(60420))   ' 2 - From gridresult
                    If bRemovePayment Then
                        .Items.Add(LRS(60421))   ' 3 - From payment
                    End If
                    .Items.Add(LRS(60422))   ' 4 - From SQL
                    .Items.Add(LRS(60423))   ' 5 - Special fixed value
                End With
        End Select
    End Sub

    Private Sub gridMenuItemPostingline_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItemPostingline.CellValueChanged
        ' trap changes
        If Not bInitMode Then
            bChangesDone = True
        End If
    End Sub
    'Private Sub gridMenuItemPostingline_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItemPostingline.CellClick
    '    If Not bInitMode Then
    '        lManualPosting_CurrentCell = e.ColumnIndex
    '        lManualPosting_CurrentRow = e.RowIndex
    '    End If
    'End Sub

    Private Sub gridMenuItemPostingline_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItemPostingline.RowEnter
        ' Update values in level 3
        If Not bInitMode Then
            With Me.gridMenuItemPostingline
                ' check if we need to save previous values first
                SaveRightClickRow(-1, e.RowIndex)
                ' _RowEnter fires twice for some reason - do not reset values if we still are on the "previous" row
                If lManualPosting_CurrentRow <> e.RowIndex Then
                    iManualMenuItemPostingLineID = Val(.Rows(e.RowIndex).Cells(2).Value)
                    iPostingLine = Val(.Rows(e.RowIndex).Cells(3).Value)
                    lManualPosting_CurrentCell = e.ColumnIndex
                    lManualPosting_CurrentRow = e.RowIndex
                    ' When we jump to a blank line (last line), we have no level3-items
                    If iManualMenuItemPostingLineID = 0 Then
                        iLevel3UsedItems = 0
                    End If
                    FillLevel3()
                End If
            End With
        End If
    End Sub
    'Private Sub gridMenuItem_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItem.CellClick
    '    If Not bInitMode Then
    '        lManualLine_CurrentCell = e.ColumnIndex
    '        lManualLine_CurrentRow= e.RowIndex)
    '    End If
    'End Sub
    Private Sub cmbFieldName_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFieldName1.SelectionChangeCommitted, cmbFieldName2.SelectionChangeCommitted, cmbFieldName3.SelectionChangeCommitted, cmbFieldName4.SelectionChangeCommitted, cmbFieldName5.SelectionChangeCommitted, cmbFieldName6.SelectionChangeCommitted, cmbFieldName7.SelectionChangeCommitted, cmbFieldName8.SelectionChangeCommitted
        ' selection changed by user
        bChangesDone = True
    End Sub
    Private Sub cmbFieldSource_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFieldSource1.SelectionChangeCommitted, cmbFieldSource2.SelectionChangeCommitted, cmbFieldSource3.SelectionChangeCommitted, cmbFieldSource4.SelectionChangeCommitted, cmbFieldSource5.SelectionChangeCommitted, cmbFieldSource6.SelectionChangeCommitted, cmbFieldSource7.SelectionChangeCommitted, cmbFieldSource8.SelectionChangeCommitted
        ' selection changed by user
        bChangesDone = True

        ' trigger display of enough inoutboxes at level3
        Select Case sender.name
            Case "cmbFieldSource1"
                If iLevel3UsedItems < 2 Then
                    iLevel3UsedItems = 1
                    HideLevel3()
                End If
            Case "cmbFieldSource2"
                If iLevel3UsedItems < 3 Then
                    iLevel3UsedItems = 2
                    HideLevel3()
                End If
            Case "cmbFieldSource3"
                If iLevel3UsedItems < 4 Then
                    iLevel3UsedItems = 3
                    HideLevel3()
                End If
            Case "cmbFieldSource4"
                If iLevel3UsedItems < 4 Then
                    iLevel3UsedItems = 4
                    HideLevel3()
                End If
            Case "cmbFieldSource5"
                If iLevel3UsedItems < 5 Then
                    iLevel3UsedItems = 5
                    HideLevel3()
                End If
            Case "cmbFieldSource6"
                If iLevel3UsedItems < 6 Then
                    iLevel3UsedItems = 6
                    HideLevel3()
                End If
            Case "cmbFieldSource7"
                If iLevel3UsedItems < 7 Then
                    iLevel3UsedItems = 7
                    HideLevel3()
                End If
        End Select

    End Sub

    Private Sub txtValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue1.TextChanged, txtValue2.TextChanged, txtValue3.TextChanged, txtValue4.TextChanged, txtValue5.TextChanged, txtValue6.TextChanged, txtValue7.TextChanged, txtValue8.TextChanged
        ' selection changed by user
        If Not bInitMode Then
            bChangesDone = True

        End If
    End Sub

    Private Sub SaveRightClickRow(ByVal iLineRow As Short, ByVal iPostingLineRow As Short)
        ' Save the setupline we just left
        ' We have always knowledge of iManualMenuItemID and iManualMenuItemPostingLineID
        ' There may also be new items which are created in this inputform

        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim sErrorString As String = ""
        Dim i As Integer
        Dim sFieldName As String
        Dim iFieldSource As Integer
        Dim sFieldValue As String
        Dim oDal As vbBabel.DAL
        Dim iNewLineRow As Integer
        Dim iNewPostingLineRow As Integer

        iNewLineRow = iLineRow
        If iNewLineRow = -1 Then
            iNewLineRow = lManualLine_CurrentRow
        End If
        iNewPostingLineRow = iPostingLineRow
        If iNewPostingLineRow = -1 Then
            iNewPostingLineRow = lManualPosting_CurrentRow
        End If

        Try
            If bChangesDone Then
                ' do not ask if we have just clicked in another control,and get back to same line
                'If iManualMenuItemID <> Val(Me.gridMenuItem.Rows(Me.gridMenuItem.CurrentRow.Index).Cells(1).Value) Or _
                '    iPostingLine <> Val(Me.gridMenuItemPostingline.Rows(Me.gridMenuItemPostingline.CurrentRow.Index).Cells(3).Value) Then
                If lManualLine_CurrentRow <> iNewLineRow Or _
                    lManualPosting_CurrentRow <> iNewPostingLineRow Then

                    If MsgBox("Ønsker du å lagre " & LRS(60426) & " " & Me.gridMenuItemPostingline.Rows(lManualPosting_CurrentRow).Cells(3).Value & " for " & Chr(34) & Me.gridMenuItem.Rows(lManualLine_CurrentRow).Cells(3).Value & Chr(34), MsgBoxStyle.YesNo, "BabelBank") = vbYes Then

                        oDal = New vbBabel.DAL
                        oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                        If Not oDal.ConnectToDB() Then
                            Throw New System.Exception(oDal.ErrorMessage)
                        End If

                        If iManualMenuItemID > 0 Then
                            ' existing rightclick
                        Else
                            ' NEW rightclick
                            ' --------------
                            ' find highest ManualmenuItemID
                            sMySQL = "SELECT MAX(ManualMenuItemID) AS MaxID FROM ManualMenuItem"
                            oDal.SQL = sMySQL
                            oDal.Reader_Execute()
                            If oDal.Reader_HasRows Then
                                Do While oDal.Reader_ReadRecord
                                    iManualMenuItemID = Int(oDal.Reader_GetString("MaxID")) + 1
                                Loop
                            End If
                            sMySQL = "INSERT INTO ManualMenuItem "
                            sMySQL = sMySQL & "(Company_ID, ManualMenuItemID, QOrder) "
                            sMySQL = sMySQL & " VALUES (1, " & CStr(iManualMenuItemID) & ", " & lManualLine_CurrentRow & ")"
                            oDal.SQL = sMySQL
                            If Not oDal.ExecuteNonQuery Then
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
                            End If
                        End If


                        ' - 0 Company_ID        (Hidden)
                        ' - 1 ManualMenuItemID  (Hidden)
                        ' - 2 QOrder            (Hidden)
                        ' - 3 Name
                        ' - 4 SQL name
                        ' - 5 Special
                        ' - 6 AskFor (field) ComboBox
                        ' - 7 AskForDefaultValue
                        ' - 8 RefundPayment
                        ' then update info for actual row to table ManualMenuItem
                        With Me.gridMenuItem
                            sMySQL = "UPDATE ManualMenuItem SET "
                            sMySQL = sMySQL & "Qorder = " & CStr(lManualLine_CurrentRow + 1) & ", "
                            sMySQL = sMySQL & "Name = '" & .Rows(lManualLine_CurrentRow).Cells(3).Value & "', "
                            sMySQL = sMySQL & "BB_SQL = '" & .Rows(lManualLine_CurrentRow).Cells(4).Value & "', "
                            sMySQL = sMySQL & "Special = '" & .Rows(lManualLine_CurrentRow).Cells(5).Value & "', "
                            sMySQL = sMySQL & "AskFor = '" & .Rows(lManualLine_CurrentRow).Cells(6).Value & "', "
                            sMySQL = sMySQL & "AskForDefaultValue = '" & .Rows(lManualLine_CurrentRow).Cells(7).Value & "', "
                            sMySQL = sMySQL & "BackPayment = " & CBool(.Rows(lManualLine_CurrentRow).Cells(8).Value)
                            sMySQL = sMySQL & " WHERE Company_Id = 1 AND ManualMenuItemID = " & CStr(iManualMenuItemID)

                            oDal.SQL = sMySQL
                            If Not oDal.ExecuteNonQuery Then
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
                            End If

                        End With

                        ' Update ManualMenuPostingLine

                        ' We can have several Postinglines for one ManualMenuItem
                        ' the easist is to first delete selected one, and Insert the 
                        ' updated ones
                        sMySQL = "DELETE FROM ManualMenuItemPostingLine "
                        sMySQL = sMySQL & "WHERE ManualMenuItemID = " & CStr(iManualMenuItemID) & "AND PostingLine = " & iPostingLine
                        oDal.SQL = sMySQL
                        If Not oDal.ExecuteNonQuery Then
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
                        End If

                        With Me.gridMenuItemPostingline
                            ' NEW postingline(s)
                            ' ------------------
                            ' find highest ManualmenuItemPostingLineID
                            sMySQL = "SELECT MAX(ManualMenuItemPostingLineID)  AS MaxID FROM ManualMenuItemPostingLine"
                            oDal = New vbBabel.DAL
                            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                            If Not oDal.ConnectToDB() Then
                                Throw New System.Exception(oDal.ErrorMessage)
                            End If
                            oDal.SQL = sMySQL
                            oDal.Reader_Execute()
                            If oDal.Reader_HasRows Then
                                Do While oDal.Reader_ReadRecord
                                    iManualMenuItemPostingLineID = CShort(oDal.Reader_GetString("MaxID"))
                                Loop
                            End If
                            ' add as many Postinglines as there are data in level 3
                            For i = 1 To iLevel3UsedItems
                                iManualMenuItemPostingLineID = iManualMenuItemPostingLineID + 1

                                If i = 1 Then
                                    sFieldName = Me.cmbFieldName1.SelectedItem
                                    iFieldSource = Me.cmbFieldSource1.SelectedIndex + 1
                                    sFieldValue = Me.txtValue1.Text
                                End If
                                If i = 2 Then
                                    sFieldName = Me.cmbFieldName2.SelectedItem
                                    iFieldSource = Me.cmbFieldSource2.SelectedIndex + 1
                                    sFieldValue = Me.txtValue2.Text
                                End If
                                If i = 3 Then
                                    sFieldName = Me.cmbFieldName3.SelectedItem
                                    iFieldSource = Me.cmbFieldSource3.SelectedIndex + 1
                                    sFieldValue = Me.txtValue3.Text
                                End If
                                If i = 4 Then
                                    sFieldName = Me.cmbFieldName4.SelectedItem
                                    iFieldSource = Me.cmbFieldSource4.SelectedIndex + 1
                                    sFieldValue = Me.txtValue4.Text
                                End If
                                If i = 5 Then
                                    sFieldName = Me.cmbFieldName5.SelectedItem
                                    iFieldSource = Me.cmbFieldSource5.SelectedIndex + 1
                                    sFieldValue = Me.txtValue5.Text
                                End If
                                If i = 6 Then
                                    sFieldName = Me.cmbFieldName6.SelectedItem
                                    iFieldSource = Me.cmbFieldSource6.SelectedIndex + 1
                                    sFieldValue = Me.txtValue6.Text
                                End If
                                If i = 7 Then
                                    sFieldName = Me.cmbFieldName7.SelectedItem
                                    iFieldSource = Me.cmbFieldSource7.SelectedIndex + 1
                                    sFieldValue = Me.txtValue7.Text
                                End If
                                If i = 8 Then
                                    sFieldName = Me.cmbFieldName8.SelectedItem
                                    iFieldSource = Me.cmbFieldSource8.SelectedIndex + 1
                                    sFieldValue = Me.txtValue8.Text
                                End If

                                ' - 0 Company_ID                    (Hidden)
                                ' - 1 ManualMenuItemID              (Hidden)
                                ' - 2 ManualMenuItemPostingLineID   (Hidden)
                                ' - 3 PostingLine
                                ' - 4 FieldName                     
                                ' - 5 FieldValue                    
                                ' - 6 FieldSource                   
                                ' - 7 PostingName

                                ' do not save if FieldName = "Not in use"
                                If sFieldName <> "Not in use" Then
                                    ' Need a value in postingline. if not filled in, set one, like the row number
                                    If .Rows(lManualPosting_CurrentRow).Cells(3).Value = "" Then
                                        .Rows(lManualPosting_CurrentRow).Cells(3).Value = lManualPosting_CurrentRow + 1
                                    End If
                                    sMySQL = "INSERT INTO ManualMenuItemPostingLine "
                                    sMySQL = sMySQL & "(Company_ID, ManualMenuItemID, ManualMenuItemPostingLineID, PostingLine, PostingName, FieldName, FieldValue, FieldSource) "
                                    sMySQL = sMySQL & " VALUES (1, " & iManualMenuItemID & ", " & iManualMenuItemPostingLineID & ", " & .Rows(lManualPosting_CurrentRow).Cells(3).Value & ", '" & .Rows(lManualPosting_CurrentRow).Cells(7).Value & "', "
                                    sMySQL = sMySQL & "'" & sFieldName & "', '" & sFieldValue & "', " & CStr(iFieldSource) & ")"

                                    oDal.SQL = sMySQL
                                    If Not oDal.ExecuteNonQuery Then
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
                                    End If
                                End If
                            Next i
                        End With
                        bChangesDone = False
                    End If   ' if MsgBox ....
                End If   'If lManualLine_CurrentRow <> iNewLineRow Or lManualPosting_CurrentRow <> iNewPostingLineRow Then
            End If  ' If bChangesDone

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            Throw New Exception("Function: frmRightClick.SaveRightClickRow" & vbCrLf & ex.Message)

        End Try

    End Sub
    Private Sub DeleteRightClick()
        ' Delete a RightClick from table ManualMenuItem
        Dim sMySQL As String = ""
        Dim oDal As vbBabel.DAL

        Try
            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

            ' First, delete from ManualmenuItemPostingLine
            sMySQL = "DELETE FROM ManualMenuItemPostingLine WHERE ManualMenuItemID = " & CStr(iManualMenuItemID)
            oDal.SQL = sMySQL
            If Not oDal.ExecuteNonQuery Then
                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
            End If

            sMySQL = "DELETE FROM ManualMenuItem WHERE ManualMenuItemID = " & CStr(iManualMenuItemID)
            oDal.SQL = sMySQL
            If Not oDal.ExecuteNonQuery Then
                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
            End If

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            Throw New Exception("Function: frmRightClick.DeleteRightClick" & vbCrLf & ex.Message)

        End Try
    End Sub
    Private Sub DeletePostingLine()
        ' Delete a Postingline from table ManualMenuItemPostingLine
        Dim sMySQL As String = ""
        Dim oDal As vbBabel.DAL

        Try
            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

            sMySQL = "DELETE FROM ManualMenuItemPostingLine WHERE ManualMenuItemID = " & CStr(iManualMenuItemID) & " AND PostingLine = " & CStr(iPostingLine)
            oDal.SQL = sMySQL
            If Not oDal.ExecuteNonQuery Then
                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage & vbCrLf & sMySQL)
            End If

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            Throw New Exception("Function: frmRightClick.DeletePostingLine" & vbCrLf & ex.Message)

        End Try
    End Sub
    Private Sub cmdDeleteRightClick_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteRightClick.Click
        ' Delete a RightClick-row from ManualMenuItem
        If MsgBox(LRS(60427, Chr(34) & Me.gridMenuItem.Rows(lManualLine_CurrentRow).Cells(3).Value & Chr(34)), MsgBoxStyle.YesNo, "BabelBank") = vbYes Then
            DeleteRightClick()
            Me.gridMenuItem.Rows.Remove(Me.gridMenuItem.Rows(lManualLine_CurrentRow))
            bChangesDone = False
        End If
    End Sub

    Private Sub cmdDeleteLine_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteLine.Click
        ' Delete a PostingLine
        If MsgBox(LRS(60427, Chr(34) & Me.gridMenuItemPostingline.Rows(lManualPosting_CurrentRow).Cells(3).Value & Chr(34)) & " for " & Chr(34) & Me.gridMenuItem.Rows(lManualLine_CurrentRow).Cells(3).Value & Chr(34), MsgBoxStyle.YesNo, "BabelBank") = vbYes Then
            DeletePostingLine()
            Me.gridMenuItemPostingline.Rows.Remove(Me.gridMenuItemPostingline.Rows(lManualPosting_CurrentRow))
            bChangesDone = False
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        SaveRightClickRow(-9, -9)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ' 48020 = "Do You want to exit without saving?"
        If bChangesDone Then
            If MsgBox(LRS(48020), MsgBoxStyle.Question + MsgBoxStyle.YesNo, "BabelBank") = vbYes Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub gridMenuItemPostingline_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMenuItemPostingline.CellContentClick

    End Sub
End Class