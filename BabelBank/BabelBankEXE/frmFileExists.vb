Option Strict Off
Option Explicit On
Friend Class frmFileExists
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdAppend_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAppend.Click
		lblResult.Text = "APPEND"
		Me.Hide()
	End Sub
	
	Private Sub cmdOverwrite_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOverwrite.Click
		lblResult.Text = "OVERWRITE"
		Me.Hide()
	End Sub
	
	Private Sub cmdTerminate_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTerminate.Click
		lblResult.Text = "TERMINATE"
		Me.Hide()
	End Sub
End Class
