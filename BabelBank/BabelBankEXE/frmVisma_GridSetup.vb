﻿Imports VB = Microsoft.VisualBasic
Public Class frmVisma_GridSetup
    Dim bChanged As Boolean = False
    Dim sType As String = ""
    Dim iProfileGroupID As Integer = -1
    Private iProfileID As Short
    Dim iNextProfileID As Short
    Dim sBBDatabase As String
    Dim aFormats(,) As String
    Dim sUser As String = ""
	Dim bInit As Boolean = True
	Dim sOldFormatID As vbBabel.BabelFiles.FileType  ' added 06.10.2021
	Private Sub frmVisma_Setup_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim oVismaCommandLine As New VismaCommandLine

        FormLRSCaptions(Me)
        FormVismaStyle(Me)

        bChanged = False
        'Me.cmdOK.Enabled = False
        sprExportImportCreate()
        sprExportImportFill()
        bInit = False
    End Sub
    Public Sub PassType(ByVal sT As String)
        sType = sT  'E or I
        If sType = "E" Then
            Me.lblImpExp.Text = LRS(60008)  ' Files TO bank
        Else
            Me.lblImpExp.Text = LRS(60009)  ' Files FROM bank
        End If
    End Sub
    Public Sub PassProfileGroupID(ByVal iID As Integer)
        iProfileGroupID = iID
    End Sub
    Public Sub PassUser(ByVal sU As String)
        sUser = sU
    End Sub
    Public Sub PassbbDatabase(ByVal sdb As String)
        sBBDatabase = sdb
    End Sub
    Private Sub sprExportImportCreate()
        Dim x As Boolean
        Dim sErrorString As String
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim imgColumn As DataGridViewImageColumn
        Dim ComboColumn As DataGridViewComboBoxColumn

        On Error GoTo Err_Renamed

        With Me.gridImpExp

            .Rows.Clear()  ' empty content of gridImpExpImp
            .Columns.Clear()

            If .ColumnCount = 0 Then
                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.CellSelect
                .EditMode = DataGridViewEditMode.EditOnEnter
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If

            ' Col headings and widths:


            ' 0 Profilename
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(12)
            .Columns.Add(txtColumn)


            ' 1 Filename
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = Replace(LRS(60382), ":", "") 'Filename
            txtColumn.Width = WidthFromSpreadToGrid(22)
            .Columns.Add(txtColumn)


            ' 2 Image icon (+)
            ' ----------------
            imgColumn = New DataGridViewImageColumn
            imgColumn.HeaderText = " "
            imgColumn.Width = WidthFromSpreadToGrid(1)
            imgColumn.Resizable = False
            imgColumn.DefaultCellStyle.NullValue = Nothing
            .Columns.Add(imgColumn)

            ' 3 Format - Combobox
            ' -------------------
            ComboColumn = New DataGridViewComboBoxColumn
            ComboColumn.Width = WidthFromSpreadToGrid(12)
            .Columns.Add(ComboColumn)

            ' 4 Hidden col
            '--------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

        End With
        Exit Sub

Err_Renamed:

        If Not Err() Is Nothing Then
            Err.Raise(30011, "sprExportImportCreate", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30011, "sprExportImportCreate", sErrorString)
        End If

    End Sub
    Private Sub sprExportImportFill()
        ' Fill grids from table bbProfile
        ' -------------------------------
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim sSelectedFormatName As String
        Dim i As Short
        Dim sText As String
        Dim ComboCell As DataGridViewComboBoxCell
        Dim myRow As DataGridViewRow

        On Error GoTo VismaFillError

        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
        sErrorString = "Visma_Formats"
        ' function which returns a list of valid "Visma" formats
        aFormats = Visma_Formats(sType, oVismaDal, sBBDatabase) ' E=Export, I=Import

        '        rsVisma = New ADODB.Recordset
        If sType = "E" Then
            ' Export
            sMySQL = "SELECT ProfileName,Path_File, FormatName, ProfileID, P.FormatID  FROM " & sBBDatabase & ".dbo.bbProfile P, " & sBBDatabase & ".dbo.bbFormat F WHERE Export=1 AND P.FormatID = F.FormatID AND P.ProfileGroupID =" & iProfileGroupID.ToString
            Me.cmdEdit.Enabled = True
        Else
            sMySQL = "SELECT ProfileName, Path_File, FormatName, ProfileID, P.FormatID FROM " & sBBDatabase & ".dbo.bbProfile P, " & sBBDatabase & ".dbo.bbFormat F WHERE Export=0 AND P.FormatID = F.FormatID AND P.ProfileGroupID =" & iProfileGroupID.ToString
        End If
        '        rsVisma.Open(sMySQL, oVismaCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        sErrorString = "sprExportImportFill"
        With Me.gridImpExp

            ' first clear previous content
            .Rows.Clear()

            ' set headers
            If sType = "E" Then
                .Columns(0).HeaderText = Replace(LRS(60355), ":", "") 'sendprofile
            Else
                .Columns(0).HeaderText = Replace(LRS(60356), ":", "") 'returnprofile
            End If
            If sType = "E" Then
                .Columns(3).HeaderText = Replace(LRS(60369), ":", "") 'ExportFormat
            Else
                .Columns(3).HeaderText = Replace(LRS(60358), ":", "") 'ImportFormat
            End If


            oVismaDal.SQL = sMySQL
            If oVismaDal.Reader_Execute() Then
                If oVismaDal.Reader_HasRows = True Then
                    Do While oVismaDal.Reader_ReadRecord

                        'Me.gridImpExpImp.Rows.Add()
                        'myRow = .Rows(.RowCount - 1)
                        myRow = New DataGridViewRow
                        myRow.CreateCells(Me.gridImpExp)

                        ' Profilename
                        ' -----------
                        myRow.Cells(0).Value = oVismaDal.Reader_GetString("ProfileName").Trim

                        ' Filename
                        ' --------
                        myRow.Cells(1).Value = oVismaDal.Reader_GetString("Path_File").Trim


                        ' Icon for folder/file
                        ' --------------------
                        myRow.Cells(2).Value = frmMATCH_Manual.imgPlus.Image

                        ' Format, combobox
                        ' ----------------
                        sText = ""
                        ComboCell = New DataGridViewComboBoxCell
                        'ComboCell = .Columns.Item(3).Clone
                        'ComboColumn.MaxDropDownItems = 0
                        ComboCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                        'ComboCell.Items.Clear()
                        For i = 0 To UBound(aFormats, 2)
                            ComboCell.Items.Add(aFormats(0, i).Trim)
                            ' Which one is the selected format?
                            'If aFormats(0, i) = oVismaDal.Reader_GetString("FormatName").Trim Then
                            'ComboColumn.Selected = i
                            'ComboCell.DefaultCellStyle.NullValue = ComboCell.Items(i)
                            'End If
                        Next i
						ComboCell.Value = oVismaDal.Reader_GetString("FormatName").Trim  ' set selected format as default
						'ComboColumn.ReadOnly = True
						myRow.Cells(3) = ComboCell


                        ' Hidden col, ProfileID
                        ' ---------------------
                        myRow.Cells(4).Value = oVismaDal.Reader_GetString("ProfileID")

                        .Rows.Add(myRow)

                    Loop
                    .Rows(0).Cells(0).Selected = True

                End If
            End If
        End With

        oVismaDal.Close()
        oVismaDal = Nothing

        Me.gridImpExp.Visible = True
        Me.gridImpExp.Focus()

        Exit Sub

VismaFillError:
        oVismaDal.Close()
        oVismaDal = Nothing

        If Not Err() Is Nothing Then
            Err.Raise(30005, "sprExportImportfill", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30005, "sprExportImportfill", sErrorString)
        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAdd.Click
        '' add row to either sprExports or sprImports
        Dim i As Short
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim oMyRow As DataGridViewRow
        Dim oMyComboCell As DataGridViewComboBoxCell

        ' not possible to add profile if no profilegroup exists;

        If iProfileGroupID < 0 Then
            ' Det må være registrert minst en profilgruppe før vi kan legge til en Import eller Eksport !
            MsgBox(LRS(35256), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, LRS(35017))

        Else

            sErrorString = "Opens Visma Business database"
            oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
            ' function which returns a list of valid "Visma" formats
            aFormats = Visma_Formats(sType, oVismaDal, sBBDatabase) ' E=Export, I=Import

            '        If sCurrentSpread = "E" Then
            '            ' sprExports has focus
            With Me.gridImpExp
                ' Icon for folder/file
                ' --------------------
                oMyRow = New DataGridViewRow
                oMyRow.CreateCells(Me.gridImpExp)
                oMyRow.Cells(2).Value = frmMATCH_Manual.imgPlus.Image

                ' Fill in formats into combobox in col4
                oMyComboCell = oMyRow.Cells(3)
                oMyComboCell.Items.Clear()
                For i = 0 To UBound(aFormats, 2)
                    oMyComboCell.Items.Add(aFormats(0, i).Trim)
                Next i

                .Rows.Add(oMyRow)
                .Rows(.RowCount - 1).Cells(4).Value = 0 ' no profileID
                iProfileID = 0
                cmdEdit.Enabled = True
                .CurrentCell = .Rows(.RowCount - 1).Cells(0)        ' Focus to first cell in new line
                .Focus()

            End With
            bChanged = True
            'Me.cmdOK.Enabled = True
        End If


    End Sub
    Private Sub cmdDelete_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDelete.Click
        '        ' Delete row from either sprExports or sprImports
        Dim sErrorString As String
        Dim sText As String
        Dim iNextRow As Integer = 0

        On Error GoTo localerror

        With Me.gridImpExp
            If Not .CurrentRow Is Nothing Then
                If .CurrentRow.Visible = True Then
                    sText = .CurrentCell.Value
                    'If MsgBox("Vil du slette profilen " & sText, vbQuestion + vbYesNo, "BabelBank") = vbYes Then
                    If MsgBox(LRS(35107) & " " & sText, MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then

                        ' Position to a row near deleted row;
                        If .CurrentRow.Index < .RowCount - 2 Then
                            iNextRow = .CurrentRow.Index + 1
                        ElseIf .CurrentRow.Index > 0 Then
                            iNextRow = .CurrentRow.Index - 1
                        End If
                        .CurrentRow.Visible = False

                        If .Rows(iNextRow).Visible = True Then
                            .CurrentCell = .Rows(iNextRow).Cells(0)
                        End If
                    End If
                    bChanged = True
                    'Me.cmdOK.Enabled = True
                End If
            End If
        End With
        Exit Sub

localerror:
        If Not Err() Is Nothing Then
            Err.Raise(30003, "cmdDelete_Click", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30003, "cmdDelete_Click", sErrorString)
        End If

    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click

        ' save current grid
        SaveProfile()

        '        
        Exit Sub


    End Sub
    Private Sub SaveProfile()
        Dim i As Short
        Dim j As Short
        '		Dim lRecordsAffected As Integer
        Dim iProfileID As Short
        Dim sName As String
        Dim sFilename As String
        Dim sFormat As String
        Dim sFormatID As String
        Dim bContinue As Boolean
        '		Dim rsVisma As ADODB.Recordset

        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sMySQL As String
        Dim sErrorString As String = ""
        Dim myComboCell As DataGridViewComboBoxCell

        On Error GoTo LocalErrOR

        sErrorString = "Opening Visma Business database for reading"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sErrorString = "Selecting from bbProfile"
        bContinue = True

        'aFormats = Visma_Formats(sType, oVismaDal, sBBDatabase) ' E=Export, I=Import

        ' Find highest profileid if we havent had any new in this run
        ' first; find highest ProfileGroupID
        sMySQL = "SELECT MAX(ProfileId) AS MaxID FROM " & sBBDatabase & ".dbo.bbProfile"
        oVismaDal.SQL = sMySQL
        sErrorString = "Find  highest ProfileID"

        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows Then
                oVismaDal.Reader_ReadRecord()
                iNextProfileID = Val(oVismaDal.Reader_GetString("MaxID")) + 1
            End If
        End If
        oVismaDal.Close()
        oVismaDal = Nothing

        sErrorString = "Opening Visma Business database for updating"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
        oVismaDal.TEST_UseCommitFromOutside = True
        oVismaDal.TEST_BeginTrans()

        '		aFormats = Visma_Formats(sType, oVismaCon) ' E=Export, I=Import
        With gridImpExp

            For i = 0 To .RowCount - 1
                If .Rows(i).Visible = True Then
                    ' first, validate filename;
                    sName = .Rows(i).Cells(0).Value
                    sFilename = .Rows(i).Cells(1).Value

                    ' Test filenames;
                    ' --------------
                    bContinue = True

                    '13.03.2014 - added test for "blank" line, no Name, no filename, remove;
                    If Not (sFilename = "" And sName = "") Then

                        ' Not allowed with  * in filename for export
                        If sType = "E" And InStr(sFilename, "*") > 0 Then
                            bContinue = False
                        End If
                        ' either start with "//" or have : in second pos and / in third
                        If Not (VB.Left(sFilename, 2) = "//" Or VB.Left(sFilename, 2) = "\\" Or Mid(sFilename, 2, 2) = ":\") Then
                            ' ok good enough test
                            bContinue = False
                        End If

                        If Not bContinue Then
                            ' not a good filename
                            ' Filename %1 is not a valid filename
                            MsgBox(LRS(35099, sFilename), MsgBoxStyle.Information, "Visma Payment (BabelBank)")
                            'MsgBox "Filename " & sFilename & " is not a valid filename", vbInformation, "BabelBank"
                            bContinue = False
                        End If

                        ' then check if format is selected
                        '						.Col = 4
                        ' combobox
                        myComboCell = .Rows(i).Cells(3)
                        sFormat = myComboCell.Value

                        '						sFormat = .Text ' selected value
                        If EmptyString(sFormat) Then
                            MsgBox(LRS(35223, sFilename), MsgBoxStyle.Information, "Visma Payment (BabelBank)")
                            'MsgBox "Du må velge et format for filen "
                            bContinue = False
                        End If
                    Else
                        .Rows(i).Visible = True
                    End If
                End If

                If Not bContinue Then
                    Exit For
                End If
            Next i

            If bContinue Then
                For i = 0 To .RowCount - 1
                    If .Rows(i).Visible = False Then


                        ' deleted export/import;
                        '						.Col = 5 ' ProfileID
                        '						iProfileID = CShort(.Value)
                        iProfileID = .Rows(i).Cells(4).Value
                        sErrorString = "DELETE FROM bbParameter"
                        ' First, delete from Parameters table;
                        sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbParameter WHERE ProfileID = " & Str(iProfileID)
                        oVismaDal.SQL = sMySQL
                        oVismaDal.ExecuteNonQuery()
                        '						cmdBBUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
                        '						cmdBBUpdate.let_ActiveConnection(oVismaCon)
                        '						cmdBBUpdate.CommandText = sMySQL
                        '						cmdBBUpdate.Execute(lRecordsAffected)

                        ' then delete from bbProfile table
                        sErrorString = "DELETE FROM bbProfile"
                        sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbProfile WHERE ProfileID = " & Str(iProfileID)
                        oVismaDal.SQL = sMySQL
                        oVismaDal.ExecuteNonQuery()

                        '						cmdBBUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
                        '						cmdBBUpdate.let_ActiveConnection(oVismaCon)
                        '						cmdBBUpdate.CommandText = sMySQL
                        '						cmdBBUpdate.Execute(lRecordsAffected)

                    Else

                        sName = .Rows(i).Cells(0).Value
                        sFilename = .Rows(i).Cells(1).Value

                        '						.Col = 4
                        ' combobox
                        myComboCell = .Rows(i).Cells(3)
                        sFormat = myComboCell.Value

                        ' in aFormats, we have both name and FormatID (ENum-id).
                        ' Find FormatID from selected name;
                        For j = 0 To UBound(aFormats, 2)
                            If aFormats(0, j) = sFormat Then
                                sFormatID = aFormats(1, j)
                                Exit For
                            End If
                        Next j

                        iProfileID = .Rows(i).Cells(4).Value


                        If iProfileID = 0 Then
                            ' find selected profilegroupID
                            ' new import or exportprofile

                            sErrorString = "INSERT INTO bbProfile"
                            ' add to table
                            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbProfile "
                            sMySQL = sMySQL & "(ProfileID, ProfileName, ProfileGroupID, Export, FormatID, Path_file, ChDt, ChTm, ChUsr) VALUES ("
                            sMySQL = sMySQL & iNextProfileID & ", '"
                            sMySQL = sMySQL & sName & "',"
                            sMySQL = sMySQL & Str(iProfileGroupID) & ","
                            If sType = "E" Then
                                sMySQL = sMySQL & "1,"
                            Else
                                sMySQL = sMySQL & "0,"
                            End If
                            sMySQL = sMySQL & sFormatID & ", '"
                            sMySQL = sMySQL & sFilename
                            sMySQL = sMySQL & "', CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"

                            '							cmdBBUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
                            '							cmdBBUpdate.let_ActiveConnection(oVismaCon)
                            '							cmdBBUpdate.CommandText = sMySQL
                            '							cmdBBUpdate.Execute(lRecordsAffected)
                            oVismaDal.SQL = sMySQL
                            oVismaDal.ExecuteNonQuery()

                            ' Update iProfileID in spread for this newly saved profile;
                            '							.Value = CStr(iNextProfileID)
                            .Rows(i).Cells(4).Value = iNextProfileID.ToString
                            iNextProfileID = iNextProfileID + 1
                        Else
                            sErrorString = "UPDATE bbProfile"
                            ' may have changed data for an existing profile
                            sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbProfile SET "
                            sMySQL = sMySQL & "ProfileName = '" & sName & "',"
                            sMySQL = sMySQL & "ProfileGroupID =" & Str(iProfileGroupID) & ", "
                            If sType = "E" Then
                                sMySQL = sMySQL & "Export = 1,"
                            Else
                                sMySQL = sMySQL & "Export = 0,"
                            End If
                            sMySQL = sMySQL & "FormatID = " & sFormatID & ", "
                            sMySQL = sMySQL & "Path_file = '" & sFilename
                            sMySQL = sMySQL & "', ChUsr = '" & sUser & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', '') "
                            sMySQL = sMySQL & "WHERE ProfileID =" & Str(iProfileID)
                            '							cmdBBUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
                            '							cmdBBUpdate.let_ActiveConnection(oVismaCon)
                            '							cmdBBUpdate.CommandText = sMySQL
                            '							cmdBBUpdate.Execute(lRecordsAffected)
                            oVismaDal.SQL = sMySQL
                            oVismaDal.ExecuteNonQuery()
                        End If 'If iProfileID = 0 Then
                    End If 'If .Rows(i).Visible = False Then
                Next i
                bChanged = False
                'Me.cmdOK.Enabled = False
            End If ' If bContinue
        End With
        oVismaDal.TEST_CommitTrans()
        oVismaDal.Close()
        oVismaDal = Nothing

        Exit Sub

LocalErrOR:
        oVismaDal.TEST_Rollback()
        oVismaDal.Close()
        oVismaDal = Nothing
        If Not Err() Is Nothing Then
            Err.Raise(30010, "SaveProfile", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30010, "SaveProfile", sErrorString)
        End If


    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Dim bContinue As Boolean
        If bChanged Then
            ' Vil du avslutte uten lagring ?
            If MsgBox(LRS(48020), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                bContinue = True
                bChanged = False
            Else
                bContinue = False
                bChanged = True
                'Me.cmdOK.Enabled = True
            End If
        Else
            bContinue = True
        End If

        If bContinue Then
            Me.Hide()
        End If

    End Sub
    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim oVismaDal As vbBabel.DAL
        Dim bContinue As Boolean
        Dim sErrorString As String
        Dim lRow As Integer
		Dim sFormatID As vbBabel.BabelFiles.FileType
		Dim sFormat As String
		Dim myComboCell As DataGridViewComboBoxCell
        Dim j As Short



        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)


        bContinue = True
        lRow = Me.gridImpExp.CurrentRow.Index  ' save the row we're at now
        ' change parameters table
        ' if new Export/Import, must save it first, otherwise ProfileID = 0
        ' 11.11.2016 - always save if bChanged = True
        'If iProfileID = 0 Then
        If bChanged Then
            If MsgBox(LRS(35184), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then ' Save profiles ?
                ' save exports
                SaveProfile()
                bContinue = True
            Else
                bContinue = False
            End If
        Else
            bContinue = True
        End If
        ' go back to row with new profile
        ' Find profileID after the new profile has been saved, needs this to save Parameters
        iProfileID = Val(Me.gridImpExp.Rows(Me.gridImpExp.CurrentRow.Index).Cells(4).Value)

        If sType = "E" Then
            ' then check which format is selected
            '                Me.sprExports.Col = 4
            '                ' listbox
            '                sFormat = Me.sprExports.Text ' selected value
            ' combobox
            myComboCell = Me.gridImpExp.Rows(Me.gridImpExp.CurrentRow.Index).Cells(3)
            sFormat = myComboCell.Value   '.Items(myComboCell.RowIndex)
            ' in aFormats, we have both name and FormatID (ENum-id).

            ' in aFormats, we have both name and FormatID (ENum-id).
            ' Find FormatID from selected name;
            For j = 0 To UBound(aFormats, 2)
                If aFormats(0, j) = sFormat Then
                    sFormatID = aFormats(1, j)
                    Exit For
                End If
            Next j


        End If
        'Else
        '    ' existing profile;
        '    iProfileID = Val(Me.gridImpExp.Rows(Me.gridImpExp.CurrentRow.Index).Cells(4).Value)
        '    ' combobox
        '    myComboCell = Me.gridImpExp.Rows(Me.gridImpExp.CurrentRow.Index).Cells(3)
        '    sFormat = myComboCell.Value
        '    ' in aFormats, we have both name and FormatID (ENum-id).
        '    ' Find FormatID from selected name;
        '    For j = 0 To UBound(aFormats, 2)
        '        If aFormats(0, j) = sFormat Then
        '            sFormatID = aFormats(1, j)
        '            Exit For
        '        End If
        '    Next j

        'End If
        If bContinue Then
			frmVisma_Parameters.SetProfileID(iProfileID)
			' 06.10.2021 - set also old format, to see if format has changed
			frmVisma_Parameters.SetOldFormatID(sOldFormatID)
			frmVisma_Parameters.SetFormatID(sFormatID)
			VB6.ShowForm(frmVisma_Parameters, 1, Me)
            frmVisma_Parameters = Nothing
        End If
    End Sub

    Private Sub gridImpExpImp_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        bChanged = True
        'Me.cmdOK.Enabled = True
    End Sub
    Private Sub gridImpExpImp_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
		' find current ProfileID for this new row selected
		iProfileID = Me.gridImpExp.Rows(Me.gridImpExp.CurrentRow.Index).Cells(4).Value
		' 06.10.2021 - find FormatID for this profile
		sOldFormatID = Me.gridImpExp.Rows(Me.gridImpExp.CurrentRow.Index).Cells(3).Value
	End Sub
    Private Function FindFiles(ByRef sFF As String) As String
        ' Find filename for dialog

        Dim sPath As String
		Dim sFile As String

		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(sFF, "\") > 0 And InStr(sFF, ".") > 0 Then
            sFile = Mid(sFF, InStrRev(sFF, "\"))
        ElseIf InStr(sFF, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & sFF
        Else
            sFile = ""
        End If

        'spath = FixPath(Me.txtToBank.Text)
        'sPath = BrowseForFolderByPIDL(sFF, Me, "BabelBank", True)
        sPath = BrowseForFilesOrFolders(sFF, Me, "BabelBank", True)

        If Len(sPath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(sPath, ".") = 0 Then
                sFF = Replace(sPath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                sFF = sPath
            End If
        Else
            sFF = ""  ' cancelled
        End If
        FindFiles = sFF
    End Function
    Private Sub gridImpExpImp_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridImpExp.CellClick
		' check for click on +-button in grid (column index  2)
		Dim sFile As String
		Dim j As Integer = 0

		With Me.gridImpExp
			If e.RowIndex > -1 And e.ColumnIndex = 2 Then
				' find old filename
				sFile = .Rows(e.RowIndex).Cells(1).Value
				' pick folder/file from dialog
				sFile = FindFiles(sFile)
				If Not EmptyString(sFile) Then
					' set new filename in filenamecolumn
					.Rows(e.RowIndex).Cells(1).Value = sFile
					bChanged = True
					'Me.cmdOK.Enabled = True
				End If
			End If
			If e.RowIndex > -1 And e.ColumnIndex = 3 Then
				' click in combobox with formats
				If Not bInit Then
					bChanged = True
				End If
			End If
			If e.RowIndex > -1 Then
				' 06.10.2021 - find FormatID for this profile
				Dim s As String = ""
				' 12.10.2021:
				If Not EmptyOrNullString(.Rows(e.RowIndex).Cells(3).Value) Then
					s = .Rows(e.RowIndex).Cells(3).Value.ToString

					' in aFormats, we have both name and FormatID (ENum-id).
					' Find FormatID from selected name;
					For j = 0 To UBound(aFormats, 2)
						If aFormats(0, j) = s Then
							sOldFormatID = aFormats(1, j)
							Exit For
						End If
					Next j
				End If
			End If
        End With
    End Sub

    Private Sub gridImpExpImp_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridImpExp.CellValueChanged
        If Not bInit Then
            bChanged = True
            'Me.cmdOK.Enabled = True
        End If
    End Sub

End Class