<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSEBISO20022_SetupMisc
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkUseReturnPosting As System.Windows.Forms.CheckBox
	Public WithEvents chkUseReturnRejection As System.Windows.Forms.CheckBox
	Public WithEvents chkUseReturnConfirmation As System.Windows.Forms.CheckBox
	Public WithEvents chkTrimFreeText As System.Windows.Forms.CheckBox
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents txtHistoryDeleteDays As System.Windows.Forms.TextBox
	Public WithEvents lblUseReturnPosting As System.Windows.Forms.Label
	Public WithEvents lblUseReturnRejection As System.Windows.Forms.Label
	Public WithEvents lblUseReturnConfirmation As System.Windows.Forms.Label
    Public WithEvents lblTrimFreetext As System.Windows.Forms.Label
	Public WithEvents lblHistoryDeleteDays As System.Windows.Forms.Label
	Public WithEvents lblExplaination As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkUseReturnPosting = New System.Windows.Forms.CheckBox
        Me.chkUseReturnRejection = New System.Windows.Forms.CheckBox
        Me.chkUseReturnConfirmation = New System.Windows.Forms.CheckBox
        Me.chkTrimFreeText = New System.Windows.Forms.CheckBox
        Me.cmdSave = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.txtHistoryDeleteDays = New System.Windows.Forms.TextBox
        Me.lblUseReturnPosting = New System.Windows.Forms.Label
        Me.lblUseReturnRejection = New System.Windows.Forms.Label
        Me.lblUseReturnConfirmation = New System.Windows.Forms.Label
        Me.lblTrimFreetext = New System.Windows.Forms.Label
        Me.lblHistoryDeleteDays = New System.Windows.Forms.Label
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.ChkSEPASingle = New System.Windows.Forms.CheckBox
        Me.lblSEPASingle = New System.Windows.Forms.Label
        Me.chkEndToEndRefFromBabelBank = New System.Windows.Forms.CheckBox
        Me.lblEndToEndRefFromBabelBank = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'chkUseReturnPosting
        '
        Me.chkUseReturnPosting.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseReturnPosting.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseReturnPosting.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseReturnPosting.Location = New System.Drawing.Point(432, 228)
        Me.chkUseReturnPosting.Name = "chkUseReturnPosting"
        Me.chkUseReturnPosting.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseReturnPosting.Size = New System.Drawing.Size(17, 17)
        Me.chkUseReturnPosting.TabIndex = 4
        Me.chkUseReturnPosting.Text = "Check1"
        Me.chkUseReturnPosting.UseVisualStyleBackColor = False
        Me.chkUseReturnPosting.Visible = False
        '
        'chkUseReturnRejection
        '
        Me.chkUseReturnRejection.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseReturnRejection.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseReturnRejection.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseReturnRejection.Location = New System.Drawing.Point(432, 198)
        Me.chkUseReturnRejection.Name = "chkUseReturnRejection"
        Me.chkUseReturnRejection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseReturnRejection.Size = New System.Drawing.Size(17, 17)
        Me.chkUseReturnRejection.TabIndex = 3
        Me.chkUseReturnRejection.Text = "Check1"
        Me.chkUseReturnRejection.UseVisualStyleBackColor = False
        Me.chkUseReturnRejection.Visible = False
        '
        'chkUseReturnConfirmation
        '
        Me.chkUseReturnConfirmation.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseReturnConfirmation.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseReturnConfirmation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseReturnConfirmation.Location = New System.Drawing.Point(432, 168)
        Me.chkUseReturnConfirmation.Name = "chkUseReturnConfirmation"
        Me.chkUseReturnConfirmation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseReturnConfirmation.Size = New System.Drawing.Size(17, 17)
        Me.chkUseReturnConfirmation.TabIndex = 2
        Me.chkUseReturnConfirmation.Text = "Check1"
        Me.chkUseReturnConfirmation.UseVisualStyleBackColor = False
        Me.chkUseReturnConfirmation.Visible = False
        '
        'chkTrimFreeText
        '
        Me.chkTrimFreeText.BackColor = System.Drawing.SystemColors.Control
        Me.chkTrimFreeText.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkTrimFreeText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkTrimFreeText.Location = New System.Drawing.Point(432, 138)
        Me.chkTrimFreeText.Name = "chkTrimFreeText"
        Me.chkTrimFreeText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkTrimFreeText.Size = New System.Drawing.Size(17, 17)
        Me.chkTrimFreeText.TabIndex = 1
        Me.chkTrimFreeText.Text = "Check1"
        Me.chkTrimFreeText.UseVisualStyleBackColor = False
        Me.chkTrimFreeText.Visible = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(384, 350)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(73, 21)
        Me.cmdSave.TabIndex = 5
        Me.cmdSave.Text = "55034 &Save"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(464, 350)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(65, 21)
        Me.CmdCancel.TabIndex = 6
        Me.CmdCancel.Text = "55006 &Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'txtHistoryDeleteDays
        '
        Me.txtHistoryDeleteDays.AcceptsReturn = True
        Me.txtHistoryDeleteDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtHistoryDeleteDays.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHistoryDeleteDays.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHistoryDeleteDays.Location = New System.Drawing.Point(432, 104)
        Me.txtHistoryDeleteDays.MaxLength = 4
        Me.txtHistoryDeleteDays.Name = "txtHistoryDeleteDays"
        Me.txtHistoryDeleteDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHistoryDeleteDays.Size = New System.Drawing.Size(41, 20)
        Me.txtHistoryDeleteDays.TabIndex = 0
        '
        'lblUseReturnPosting
        '
        Me.lblUseReturnPosting.BackColor = System.Drawing.SystemColors.Control
        Me.lblUseReturnPosting.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUseReturnPosting.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUseReturnPosting.Location = New System.Drawing.Point(187, 228)
        Me.lblUseReturnPosting.Name = "lblUseReturnPosting"
        Me.lblUseReturnPosting.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUseReturnPosting.Size = New System.Drawing.Size(213, 19)
        Me.lblUseReturnPosting.TabIndex = 12
        Me.lblUseReturnPosting.Text = "Bruk Camt.054 (avregning)"
        Me.lblUseReturnPosting.Visible = False
        '
        'lblUseReturnRejection
        '
        Me.lblUseReturnRejection.BackColor = System.Drawing.SystemColors.Control
        Me.lblUseReturnRejection.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUseReturnRejection.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUseReturnRejection.Location = New System.Drawing.Point(187, 198)
        Me.lblUseReturnRejection.Name = "lblUseReturnRejection"
        Me.lblUseReturnRejection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUseReturnRejection.Size = New System.Drawing.Size(213, 19)
        Me.lblUseReturnRejection.TabIndex = 11
        Me.lblUseReturnRejection.Text = "Bruk ""negativ"" Pain.002"
        Me.lblUseReturnRejection.Visible = False
        '
        'lblUseReturnConfirmation
        '
        Me.lblUseReturnConfirmation.BackColor = System.Drawing.SystemColors.Control
        Me.lblUseReturnConfirmation.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUseReturnConfirmation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUseReturnConfirmation.Location = New System.Drawing.Point(187, 168)
        Me.lblUseReturnConfirmation.Name = "lblUseReturnConfirmation"
        Me.lblUseReturnConfirmation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUseReturnConfirmation.Size = New System.Drawing.Size(213, 19)
        Me.lblUseReturnConfirmation.TabIndex = 10
        Me.lblUseReturnConfirmation.Text = "Bruk ""positiv"" Pain.002"
        Me.lblUseReturnConfirmation.Visible = False
        '
        'lblTrimFreetext
        '
        Me.lblTrimFreetext.BackColor = System.Drawing.SystemColors.Control
        Me.lblTrimFreetext.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTrimFreetext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTrimFreetext.Location = New System.Drawing.Point(187, 138)
        Me.lblTrimFreetext.Name = "lblTrimFreetext"
        Me.lblTrimFreetext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTrimFreetext.Size = New System.Drawing.Size(214, 19)
        Me.lblTrimFreetext.TabIndex = 9
        Me.lblTrimFreetext.Text = "Gj�r plass til mest mulig meldingstekst"
        Me.lblTrimFreetext.Visible = False
        '
        'lblHistoryDeleteDays
        '
        Me.lblHistoryDeleteDays.BackColor = System.Drawing.SystemColors.Control
        Me.lblHistoryDeleteDays.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHistoryDeleteDays.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHistoryDeleteDays.Location = New System.Drawing.Point(187, 108)
        Me.lblHistoryDeleteDays.Name = "lblHistoryDeleteDays"
        Me.lblHistoryDeleteDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHistoryDeleteDays.Size = New System.Drawing.Size(214, 19)
        Me.lblHistoryDeleteDays.TabIndex = 8
        Me.lblHistoryDeleteDays.Text = "Slett i database etter nn dager"
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(184, 68)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(289, 33)
        Me.lblExplaination.TabIndex = 7
        Me.lblExplaination.Tag = "Sett opp "
        Me.lblExplaination.Text = "Verdier for bruk i konvertering fra/til ISO20022-filer."
        '
        'ChkSEPASingle
        '
        Me.ChkSEPASingle.BackColor = System.Drawing.SystemColors.Control
        Me.ChkSEPASingle.Cursor = System.Windows.Forms.Cursors.Default
        Me.ChkSEPASingle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ChkSEPASingle.Location = New System.Drawing.Point(432, 258)
        Me.ChkSEPASingle.Name = "ChkSEPASingle"
        Me.ChkSEPASingle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ChkSEPASingle.Size = New System.Drawing.Size(17, 17)
        Me.ChkSEPASingle.TabIndex = 14
        Me.ChkSEPASingle.Text = "Check1"
        Me.ChkSEPASingle.UseVisualStyleBackColor = False
        '
        'lblSEPASingle
        '
        Me.lblSEPASingle.BackColor = System.Drawing.SystemColors.Control
        Me.lblSEPASingle.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSEPASingle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSEPASingle.Location = New System.Drawing.Point(187, 258)
        Me.lblSEPASingle.Name = "lblSEPASingle"
        Me.lblSEPASingle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSEPASingle.Size = New System.Drawing.Size(213, 19)
        Me.lblSEPASingle.TabIndex = 15
        Me.lblSEPASingle.Text = "SEPA Single postering"
        '
        'chkEndToEndRefFromBabelBank
        '
        Me.chkEndToEndRefFromBabelBank.BackColor = System.Drawing.SystemColors.Control
        Me.chkEndToEndRefFromBabelBank.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkEndToEndRefFromBabelBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkEndToEndRefFromBabelBank.Location = New System.Drawing.Point(432, 288)
        Me.chkEndToEndRefFromBabelBank.Name = "chkEndToEndRefFromBabelBank"
        Me.chkEndToEndRefFromBabelBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkEndToEndRefFromBabelBank.Size = New System.Drawing.Size(17, 17)
        Me.chkEndToEndRefFromBabelBank.TabIndex = 16
        Me.chkEndToEndRefFromBabelBank.Text = "Check1"
        Me.chkEndToEndRefFromBabelBank.UseVisualStyleBackColor = False
        '
        'lblEndToEndRefFromBabelBank
        '
        Me.lblEndToEndRefFromBabelBank.BackColor = System.Drawing.SystemColors.Control
        Me.lblEndToEndRefFromBabelBank.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblEndToEndRefFromBabelBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEndToEndRefFromBabelBank.Location = New System.Drawing.Point(187, 288)
        Me.lblEndToEndRefFromBabelBank.Name = "lblEndToEndRefFromBabelBank"
        Me.lblEndToEndRefFromBabelBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblEndToEndRefFromBabelBank.Size = New System.Drawing.Size(213, 19)
        Me.lblEndToEndRefFromBabelBank.TabIndex = 17
        Me.lblEndToEndRefFromBabelBank.Text = "BabelBank lager unik EndToEndRef"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 342)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(520, 1)
        Me.lblLine1.TabIndex = 87
        Me.lblLine1.Text = "Label1"
        '
        'frmSEBISO20022_SetupMisc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(546, 377)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkEndToEndRefFromBabelBank)
        Me.Controls.Add(Me.lblEndToEndRefFromBabelBank)
        Me.Controls.Add(Me.ChkSEPASingle)
        Me.Controls.Add(Me.lblSEPASingle)
        Me.Controls.Add(Me.chkUseReturnPosting)
        Me.Controls.Add(Me.chkUseReturnRejection)
        Me.Controls.Add(Me.chkUseReturnConfirmation)
        Me.Controls.Add(Me.chkTrimFreeText)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.txtHistoryDeleteDays)
        Me.Controls.Add(Me.lblUseReturnPosting)
        Me.Controls.Add(Me.lblUseReturnRejection)
        Me.Controls.Add(Me.lblUseReturnConfirmation)
        Me.Controls.Add(Me.lblTrimFreetext)
        Me.Controls.Add(Me.lblHistoryDeleteDays)
        Me.Controls.Add(Me.lblExplaination)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(8, 30)
        Me.Name = "frmSEBISO20022_SetupMisc"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Diverse oppsett"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents ChkSEPASingle As System.Windows.Forms.CheckBox
    Public WithEvents lblSEPASingle As System.Windows.Forms.Label
    Public WithEvents chkEndToEndRefFromBabelBank As System.Windows.Forms.CheckBox
    Public WithEvents lblEndToEndRefFromBabelBank As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class