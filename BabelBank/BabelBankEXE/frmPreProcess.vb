﻿Imports System.IO
Imports System.Windows.Forms
Public Class frmPreProcess

    Private fFormatExpected As vbBabel.BabelFiles.FileType
    Private nLineLenExpected As Long
    Private fFormatDetected As vbBabel.BabelFiles.FileType
    Private nLineLenDetected As Long
    Private nNoOfLinesFound As Long
    Private aErrArray(2, 0) As String
    Private nNoOfLinesSoFar As Double
    Private nNoOfCharsSoFar As Double
    Private sPreviousLine As String
    Private sCurrentLine As String
    Private sPreviousRecord As String
    Private sCurrentRecord As String
    Private sLinePaddingChar As String
    Private sFileName As String
    Private sErrString As String = ""
    Private iMaxNoOfErrors As Integer = 0
    Private iNoOfErrors As Integer = 0
    Dim frmPre As frmPreProcess
    Dim bCanView As Boolean = False
    Dim aLines() As String
    Dim bErrorCheckingComplete As Boolean = False

    Private Sub frmPreProcess_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", 500, 60)
        FormLRSCaptions(Me)
        Me.cmdView.Enabled = False
        Me.cmdMail.Enabled = False
        Me.cmdMail.Enabled = False

        Me.lblEMail.Visible = False
        Me.lblErrorsFound.Visible = False
        Me.lblFunction.Visible = False
        Me.lblProgress.Visible = False
        Me.ProgressBar1.Visible = False
        Me.listError.Visible = False
        Me.listFileContent.Visible = True
        Me.lblAscii.Visible = True
        Me.txtAscii.Visible = True
        Me.txtEMailReceiver.Visible = False

    End Sub
    Private Sub cmdStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdStart.Click
        Me.Analyze()
    End Sub

    Public Property FormatExpected() As vbBabel.BabelFiles.FileType
        Get
            FormatExpected = fFormatExpected
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.FileType)
            fFormatExpected = Value
        End Set
    End Property
    Public Property LineLenExpected() As Long
        Get
            LineLenExpected = nLineLenExpected
        End Get
        Set(ByVal Value As Long)
            nLineLenExpected = Value
        End Set
    End Property
    Public ReadOnly Property LineLenDetected() As Long
        Get
            LineLenDetected = nLineLenDetected
        End Get
    End Property
    Public Property FileName() As String
        Get
            FileName = sFileName
        End Get
        Set(ByVal Value As String)
            sFileName = Value
        End Set
    End Property

    ' Methods
    '---------
    Private Function FindFileFormat() As vbBabel.BabelFiles.FileType
        ' Can we find out which format this file has ?
        fFormatDetected = DetectFileFormat(sFileName, False)
        If fFormatDetected <> vbBabel.BabelFiles.FileType.Unknown Then
            FindFileFormat = True
        Else
            FindFileFormat = False
        End If
    End Function
    Public Function SelectFileFormat() As vbBabel.BabelFiles.FileType
        ' Select in format if we have not detected format

    End Function
    Public Function CompareToSelectedFileFormat() As vbBabel.BabelFiles.FileType
    End Function
    Public Sub Analyze()
        Dim oStream As StreamReader
        Dim sLine As String = ""
        Dim sChar As String = ""
        Dim iPos As Integer = 0
        Dim bStop As Boolean = False
        Dim iAscii As Integer
        Dim nNoOfCRorLF As Double = 0
        Dim nNoOfIllegalChars As Double = 0
        bErrorCheckingComplete = False

        Try
            iMaxNoOfErrors = 5000
            Me.listError.Items.Clear()
            ReDim aErrArray(2, 0)
            iNoOfErrors = 0
            Me.lblErrorsFound.Text = LRS(60333) & " 0"  '"60333 - No of errors: ") & " 0"
            Me.ProgressBar1.Value = 0

            If EmptyString(sFileName) Then
                Me.cmdFileOpen_Click("cmdView", Nothing)
            End If

            Me.lblEMail.Visible = True
            Me.lblErrorsFound.Visible = True
            Me.lblFunction.Visible = True
            Me.lblFunction.ForeColor = Color.Black
            Me.lblProgress.Visible = True
            Me.ProgressBar1.Visible = True
            Me.listError.Visible = True
            Me.listFileContent.Visible = True
            Me.lblAscii.Visible = False
            Me.txtAscii.Visible = True
            Me.txtEMailReceiver.Visible = True

            '-  Find fileformat
            '------------------
            sErrString = "FindFileFormat"
            If Not FindFileFormat() Then
                ' not detected automatically
                SelectFileFormat()
            End If

            bCanView = True
            Me.cmdView.Enabled = True
            Me.cmdMail.Enabled = True

            ' Set values dependent upon format;
            Select Case fFormatDetected
                Case vbBabel.BabelFiles.FileType.Telepay2
                    nLineLenExpected = 80
                Case Else
                    nLineLenExpected = 80

            End Select


            '-  Open File
            '------------
            sErrString = "File.OpenText1"
            'oStream = File.OpenRead(sFileName)
            oStream = New StreamReader(sFileName, System.Text.Encoding.GetEncoding(1252)) '1252 Latin 1 is a single-byte encoding

            '- Check characterset
            '--------------------
            Me.lblFunction.Text = LRS(60337)   '("60337-Checking characterset")
            Pause(0.2) ' Wait 2/10 seconds to present text
            Me.ProgressBar1.Value = 10
            ' Can be problems finding CR/LF, problems with wrong charset,
            ' illegal chars, etc

            ' Read first 10 000 chars, or complete file if smaller
            sErrString = "oStream.Read"
            sLine = ""
            iPos = 0
            Do
                iPos = iPos + 1
                'If iPos = 640 Then
                ' iPos = iPos
                ' End If
                nNoOfLinesSoFar = CDbl(Int((Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars) / nLineLenExpected) + 1)
                If nNoOfLinesSoFar = 7 Then
                    iAscii = iAscii
                End If


                iAscii = oStream.Read
                ' Check illegal chars,  A-Z or number ?
                ' Ascii 32 - 96 is OK
                If (iAscii > 31 And iAscii < 97) Then
                    ' OK
                    sChar = Convert.ToChar(iAscii)
                ElseIf iAscii = 13 Or iAscii = 10 Then
                    ' OK, just keep on
                    sChar = Convert.ToChar(iAscii)
                ElseIf iAscii > 159 And iAscii < 256 Then
                    sChar = " "  ' change to blank, to not confuse tests below
                    ' but they will fuck up the editors, so count them !
                    'nNoOfIllegalChars = nNoOfIllegalChars + 1
                ElseIf iAscii > 127 And iAscii < 160 Then
                    ' TODO Hvilke ascii-er tillates?
                    ' Ifølge Telepaydoc skal standard ISO 8859-1 benyttes.
                    ' Der er koder 0-31 ikke lov, og 128-159 er ikke lov
                    ' extended ascii - many of these are allowed, like Ø = 216, Å = 197, etc ..
                    'addToErrorArray("Illegal character, ascii value = " & iAscii.ToString & ". Real position " & Len(sLine) & ". Position in editor " & Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                    addToErrorArray(LRS(60308) & iAscii.ToString & LRS(60309) & Len(sLine) & LRS(60310) & Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                    sChar = " "  ' set as blank, in case illegal chars may interupt further processing
                    nNoOfIllegalChars = nNoOfIllegalChars + 1
                    bCanView = False
                    Me.cmdView.Enabled = False
                ElseIf iAscii > 255 Then
                    ' Special chars with high ascii's
                    'addToErrorArray("Illegal character, ascii value = " & iAscii.ToString & ". Real position " & Len(sLine) & ". Position in editor " & Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                    addToErrorArray(LRS(60308) & iAscii.ToString & LRS(60309) & Len(sLine) & LRS(60310) & Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                    sChar = " "  ' set as blank, in case illegal chars may interupt further processing
                    nNoOfIllegalChars = nNoOfIllegalChars + 1
                    bCanView = False
                    Me.cmdView.Enabled = False
                ElseIf iAscii > 96 And iAscii < 127 Then
                    ' Ascii 97 til 126, small characters, not OK, but may be for some formats?
                    sChar = Convert.ToChar(iAscii)
                    '                    addToErrorArray("Capitalize letters, have found = "" " & sChar & """. Expected """ & UCase(sChar) & "" & ". Real position " & Len(sLine) + 1 & ". Position in editor " & Len(sLine) + 1 - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                Else
                    ' ascii below 32, not allowed
                    'addToErrorArray("Illegal character, ascii value = " & iAscii.ToString & ". Real position " & Len(sLine) & ". Position in editor " & Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                    addToErrorArray(LRS(60308) & iAscii.ToString & LRS(60309) & Len(sLine) & LRS(60310) & Len(sLine) - nNoOfCRorLF - nNoOfIllegalChars - ((nNoOfLinesSoFar - 1) * nLineLenExpected), , nNoOfLinesSoFar)
                    sChar = " "  ' set as blank, in case illegal chars may interupt further processing
                    nNoOfIllegalChars = nNoOfIllegalChars + 1
                    bCanView = False
                    Me.cmdView.Enabled = False
                End If

                sLine = sLine & sChar


                If oStream.EndOfStream Or Len(sLine) > 5000000 Then
                    Exit Do
                End If

                If iNoOfErrors > iMaxNoOfErrors Then
                    ' Don't bother checking more errors
                    Me.ProgressBar1.Value = 100
                    addToErrorArray(LRS(60340), , nNoOfLinesSoFar)  '"Maximum number of errors reached"
                    Me.lblFunction.Text = LRS(60340)
                    bStop = True
                    Exit Do
                End If
                If sChar = vbCr Or sChar = vbLf Then
                    nNoOfCRorLF = nNoOfCRorLF + 1
                End If
            Loop
            ' Close file after first read
            oStream.Close()


            ' Check for CRLF
            '---------------
            Me.lblFunction.Text = LRS(60406)   '"60406-"Checking CR and LF"
            Pause(0.2) ' Wait 2/10 seconds to present text
            Me.ProgressBar1.Value = 20
            sErrString = "Check for CRLF"
            If InStr(sLine, ControlChars.NewLine) = 0 Then
                ' no CRLF
                ' Check for CR and LF
                '--------------------
                sErrString = "Check for CR"
                iPos = InStr(sLine, ControlChars.Cr)
                If iPos <> nLineLenExpected And iPos > 0 Then
                    'addToErrorArray("Found no CRLF (ascii 13+ ascii 10), but found CR (ascii(13), but in wrong position. Expected position " & nLineLenExpected.ToString & ". Found in position " & iPos.ToString, sLine.Substring(0, iPos))
                    addToErrorArray(LRS(60311) & nLineLenExpected.ToString & ". " & LRS(60312) & iPos.ToString, sLine.Substring(0, iPos))
                ElseIf iPos > 0 Then
                    addToErrorArray(LRS(60313) & iPos.ToString, sLine.Substring(0, iPos))
                End If

                sErrString = "Check for LF"
                iPos = InStr(sLine, ControlChars.Lf)
                If iPos <> nLineLenExpected Then
                    'addToErrorArray("Found no CRLF (ascii 13+ ascii 10), but found LF (ascii(10), but in wrong position. Expected position " & nLineLenExpected.ToString & ". " & LRS(60312) & iPos.ToString, sLine.Substring(0, iPos))
                    addToErrorArray(LRS(60314) & nLineLenExpected.ToString & ". " & LRS(60312) & iPos.ToString, sLine.Substring(0, iPos))
                ElseIf iPos > 0 Then
                    'addToErrorArray("Found no CRLF (ascii 13+ ascii 10), but found LF (ascii(10). Found in position " & iPos.ToString, sLine.Substring(0, iPos))
                    addToErrorArray(LRS(60315) & iPos.ToString, sLine.Substring(0, iPos))
                End If

                If Not bStop Then
                    'fill up aLines
                    If iPos > 1 Then
                        If aLines Is Nothing Then
                            ReDim aLines(0)
                            aLines(0) = sLine.Substring(0, iPos - 1)
                        Else
                            ReDim Preserve aLines(aLines.GetUpperBound(0) + 1)
                            aLines(aLines.GetUpperBound(0)) = sLine.Substring(0, iPos - 1)
                        End If
                    End If
                End If
                ' no CR and no LF ?
                If InStr(sLine, vbLf) = 0 And InStr(sLine, vbCr) Then
                    'addToErrorArray("Found no CR (ascii 13) and no LF (ascii 10) in file. Stops processing.", sLine.Substring(0, 320))
                    addToErrorArray(LRS(60316), sLine.Substring(0, 320))
                    bStop = True
                End If
            Else
                ' CRLF found, is it in correct pos?
                sErrString = "Check for CRLF, position"
                iPos = 0
                nNoOfLinesFound = 1
                Do
                    iPos = InStr(sLine, ControlChars.NewLine)
                    If iPos = 0 Then
                        Exit Do
                    End If

                    If iPos <> nLineLenExpected + 1 Then
                        addToErrorArray(LRS(60317) & nLineLenExpected.ToString & ". " & LRS(60312) & iPos.ToString, sLine.Substring(0, iPos), nNoOfLinesFound.ToString)
                    End If
                    If iPos > 2 Then
                        ' do another test, to see if we have found a CR or LF in a line which ends with CRLF
                        If InStr(sLine.Substring(1, iPos - 2), vbCr) > 0 Then
                            addToErrorArray(LRS(60318) & " " & LRS(60312) & " " & InStr(sLine.Substring(1, iPos - 1), vbCr), sLine.Substring(0, iPos), nNoOfLinesFound.ToString)
                            ' stop test, fucks up rest !
                            bStop = True
                            Exit Do
                        End If
                        If InStr(sLine.Substring(1, iPos - 1), vbLf) > 0 Then
                            addToErrorArray(LRS(60319) & " " & LRS(60312) & " " & InStr(sLine.Substring(1, iPos - 1), vbLf), sLine.Substring(0, iPos), nNoOfLinesFound.ToString)
                            ' stop test, fucks up rest !
                            bStop = True
                            Exit Do
                        End If
                    End If
                    If Not bStop Then
                        'fill up aLines
                        If aLines Is Nothing Then
                            ReDim aLines(0)
                            aLines(0) = sLine.Substring(0, iPos - 1)
                        Else
                            ReDim Preserve aLines(aLines.GetUpperBound(0) + 1)
                            aLines(aLines.GetUpperBound(0)) = sLine.Substring(0, iPos - 1)
                        End If
                    End If

                    ' chop sLine, start on char after CRLF
                    sLine = sLine.Substring(iPos + 1)
                    nNoOfLinesFound = nNoOfLinesFound + 1

                    If iNoOfErrors > iMaxNoOfErrors Then
                        ' Don't bother checking more errors
                        Me.ProgressBar1.Value = 100
                        addToErrorArray(LRS(60340), , nNoOfLinesSoFar) '"Maximum number of errors reached"
                        Me.lblFunction.Text = LRS(60340)
                        bStop = True
                        Exit Do
                    End If

                Loop
            End If


            If Not bStop Then
                ' Open file again for second run
                sErrString = "File.OpenText1"
                oStream = New StreamReader(sFileName, System.Text.Encoding.GetEncoding(1252)) '1252 Latin 1 is a single-byte encoding

                Me.lblFunction.Text = LRS(60407)   '-Checking linelength")
                Pause(0.2) ' Wait 2/10 seconds to present text

                nNoOfLinesSoFar = 0
                nNoOfCharsSoFar = 0
                Do
                    '-	Read line by line
                    '--------------------
                    sErrString = "oStream.ReadLine"
                    sLine = oStream.ReadLine

                    '-	Count lines
                    '--------------
                    nNoOfLinesSoFar = nNoOfLinesSoFar + 1
                    '-	Count chars
                    nNoOfCharsSoFar = nNoOfCharsSoFar + Len(sLine)  ' ex CRLF !

                    '-	Check line lengths
                    '---------------------
                    Me.ProgressBar1.Value = 25
                    If Len(sLine) <> nLineLenExpected Then
                        nLineLenDetected = Len(sLine)
                        ' error in linelen, add to errorarray
                        addToErrorArray(LRS(60320) & Str(nLineLenDetected) & ". " & LRS(60321) & nLineLenExpected, sLine, nNoOfLinesSoFar.ToString)
                    End If

                    ' Save previous line
                    sPreviousLine = sLine

                    If oStream.EndOfStream Then
                        Exit Do
                    End If

                    If iNoOfErrors > iMaxNoOfErrors Then
                        ' Don't bother checking more errors
                        Me.ProgressBar1.Value = 100
                        addToErrorArray(LRS(60340), , nNoOfLinesSoFar) '"Maximum number of errors reached"
                        Me.lblFunction.Text = LRS(60340)
                        bStop = True
                        Exit Do
                    End If

                Loop

                oStream.Close()
                oStream = Nothing

                If aLines Is Nothing Then
                    bStop = True
                    ' nothing in array, may be some kind of specialchar at start of file?
                End If
                If Not bStop Then
                    '-  Run special analyzis for some formats
                    '----------------------------------------
                    Select Case fFormatDetected
                        Case vbBabel.BabelFiles.FileType.Telepay2
                            sErrString = ("Analysing Telepay")
                            AnalyzeTelepay()
                        Case Else
                            ' no more formats yet !
                            ' Take a quick one, and asume that they at least start Telepay with AH
                            If aLines(0).Substring(0, 2) = "AH" Then
                                sErrString = ("Analysing Telepay")
                                AnalyzeTelepay()
                            End If

                    End Select
                    Me.ProgressBar1.Value = 100
                    Me.lblFunction.Text = LRS(60338) ' Analysis completed

                End If
            Else
                bCanView = False
                Me.cmdView.Enabled = False
                ' no more formats yet !
                Me.ProgressBar1.Value = 100
                Me.lblFunction.Text = LRS(60339) ' Analysis stopped - Error found makes it impossible to proceed
                Me.lblFunction.ForeColor = Color.Red
                addToErrorArray(LRS(60339))
            End If
            bErrorCheckingComplete = True

            If Not aLines Is Nothing Then
                ' Fill up listFileContent
                For iPos = 0 To (aLines.GetUpperBound(0) - 1)
                    Me.listFileContent.Items.Add(Format(iPos + 1, "####") & ":" & aLines(iPos))
                Next iPos
            End If

        Catch ex As Exception
            ' pass the error on, "up the ladder" to be handled in ImportExport
            Dim sLocalErr As String
            sLocalErr = ""
            sLocalErr = sLocalErr & "(BB) Source: oPayment.ReadBabelBankMappingOut." & vbCrLf
            sLocalErr = sLocalErr & "ErrorNo: " & Err.Number & vbCrLf
            sLocalErr = sLocalErr & "ErrorInfo: " & sErrString & vbCrLf

            sLocalErr = sLocalErr & "ErrorMsgEx: " & ex.Message & vbCrLf

            sLocalErr = sLocalErr & "Callstack:" & Environment.StackTrace
            Throw New System.Exception(sLocalErr)
        End Try


    End Sub
    Public Sub AnalyzeTelepay()
        '-  More detailed check after Analyze is run
        Dim oStream As StreamReader
        Dim sRecord As String = ""
        Dim sPreviousBETFORWas As String = ""
        Dim sThisBETFORIs As String = ""
        Dim sPreviousRecord As String
        Dim i As Long
        Dim sHeader As String = ""
        Dim nSequence As Long = 0
        Dim nPreviousSequence As Long = 0
        Dim bStop As Boolean = False
        Dim bOK As Boolean = True
        Dim sDivision As String
        Dim sTmp As String

        nNoOfLinesSoFar = 0
        '-  Open File
        '------------
        sErrString = "File.OpenText"
        oStream = New StreamReader(sFileName, System.Text.Encoding.GetEncoding(1252)) '1252 Latin 1 is a single-byte encoding

        '-	Check filelen  ??
        Me.lblFunction.Text = LRS(60341)   '-Checking Telepay complete record")
        Pause(0.2) ' Wait 2/10 seconds to present text
        Me.ProgressBar1.Value = 50
        Do
            '-	Check each record
            '--------------------
            ' read 4 lines
            ' check if complete Telepayrecord (4 * 80)
            sErrString = "Read and check records"

            If Not oStream.EndOfStream Then
                sRecord = oStream.ReadLine
                nNoOfLinesSoFar = nNoOfLinesSoFar + 1
            Else
                'addToErrorArray("Incomplete file/record.", sRecord, nNoOfLinesSoFar.ToString)
                addToErrorArray(LRS(60322), sRecord, nNoOfLinesSoFar.ToString)
                bStop = True
            End If
            For i = 1 To 3
                If Not oStream.EndOfStream Then
                    sTmp = oStream.ReadLine
                    sRecord = sRecord & sTmp
                    nNoOfLinesSoFar = nNoOfLinesSoFar + 1
                Else
                    addToErrorArray(LRS(60322), sRecord, nNoOfLinesSoFar.ToString)
                    Exit For
                End If
                ' if incomplete number of lines in one record, exit
                ' that is if we find AH200 in start of line
                If sTmp.Length > 4 Then
                    If sTmp.Substring(0, 5) = "AH200" Then
                        addToErrorArray(LRS(60322), sRecord, nNoOfLinesSoFar.ToString)
                        bStop = True
                        Exit For
                    End If
                End If

            Next i

            sErrString = "Read and check records. " & sRecord
            '- Which Betfor, and is it valid ?
            '---------------------------------
            If sRecord.Length > 7 Then
                sThisBETFORIs = sRecord.Substring(40, 8)
                If Not TelepayValidBetfor(sThisBETFORIs) Then
                    If EmptyString(sThisBETFORIs) Then
                        'addToErrorArray("Missing BETFOR ", sRecord, nNoOfLinesSoFar)
                        addToErrorArray(LRS(60323), sRecord, nNoOfLinesSoFar)
                    Else
                        addToErrorArray(LRS(60324) & sThisBETFORIs, sRecord, nNoOfLinesSoFar)
                    End If
                    bOK = False
                End If
            End If

            If bStop Then
                addToErrorArray(LRS(60323), sRecord, nNoOfLinesSoFar)
                Exit Do
            End If

            '-	Check logic batches, and header
            '-------------------------------------
            If sHeader.Length > 8 Then
                sHeader = sRecord.Substring(0, 9)  ' like AH200TBII
                If sPreviousBETFORWas.Length > 0 Then
                    bOK = True
                    If sThisBETFORIs = "BETFOR00" And (sPreviousBETFORWas = "BETFOR99" Or sPreviousBETFORWas = "") Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR00" And (sPreviousBETFORWas = "BETFOR21" And sPreviousRecord.Length > 265) Then
                        If sPreviousRecord.Substring(266, 1) = "E" Then
                            ' ok,
                        Else
                            bOK = False
                        End If
                    ElseIf sThisBETFORIs = "BETFOR01" And (sPreviousBETFORWas = "BETFOR00" Or sPreviousBETFORWas = "BETFOR04") And sHeader.Substring(8, 1) = "U" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR02" And (sPreviousBETFORWas = "BETFOR01") And sHeader.Substring(8, 1) = "U" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR03" And (sPreviousBETFORWas = "BETFOR02") And sHeader.Substring(8, 1) = "U" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR04" And (sPreviousBETFORWas = "BETFOR03" Or sPreviousBETFORWas = "BETFOR04") And sHeader.Substring(8, 1) = "U" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR21" And (sPreviousBETFORWas = "BETFOR00" Or sPreviousBETFORWas = "BETFOR22" Or sPreviousBETFORWas = "BETFOR23") And sHeader.Substring(8, 1) = "I" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR21" And sPreviousBETFORWas = "BETFOR21" And sPreviousRecord.Length > 265 Then
                        If sPreviousRecord.Substring(266, 1) = "E" And sHeader.Substring(8, 1) = "I" Then
                            ' ok, To own account
                        Else
                            bOK = False
                        End If
                    ElseIf sThisBETFORIs = "BETFOR22" And (sPreviousBETFORWas = "BETFOR21" Or sPreviousBETFORWas = "BETFOR22") And sHeader.Substring(8, 1) = "1" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR23" And (sPreviousBETFORWas = "BETFOR21" Or sPreviousBETFORWas = "BETFOR23" Or sPreviousBETFORWas = "BETFOR23") And sHeader.Substring(8, 1) = "I" Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR99" And (sPreviousBETFORWas = "BETFOR04" Or sPreviousBETFORWas = "BETFOR23" Or sPreviousBETFORWas = "BETFOR22") Then
                        ' ok
                    ElseIf sThisBETFORIs = "BETFOR99" And (sPreviousBETFORWas = "BETFOR21" And sPreviousRecord.Length > 266) Then
                        If sPreviousRecord.Substring(266, 1) = "E" Then
                            ' ok, To own account
                        End If
                    End If
                    If Not bOK Then
                        ' all other gives error
                        ' Special test for not having batches between domestic and foreign
                        If sThisBETFORIs = "BETFOR21" And sPreviousBETFORWas = "BETFOR04" Then
                            'addToErrorArray("Illegal combination of records. Remember to separate domestic and international records by new batch (BETFOR00/99). Previous record " & sPreviousRecord, "This record: " & sRecord, nNoOfLinesSoFar.ToString)
                            addToErrorArray(LRS(60325) & sPreviousRecord, LRS(60326) & sRecord, nNoOfLinesSoFar.ToString)
                        ElseIf sThisBETFORIs = "BETFOR01" And (sPreviousBETFORWas = "BETFOR23" Or sPreviousBETFORWas = "BETFOR22") Then
                            addToErrorArray(LRS(60325) & sPreviousRecord, LRS(60326) & sRecord, nNoOfLinesSoFar.ToString)
                        Else
                            'addToErrorArray("Illegal combination of records. Previous record " & sPreviousRecord, "This record: " & sRecord, nNoOfLinesSoFar.ToString)
                            addToErrorArray(LRS(60327) & sPreviousRecord, LRS(60326) & sRecord, nNoOfLinesSoFar.ToString)
                        End If
                        bStop = True
                    End If
                End If
                '-	Check recordheader (AH…)
                '---------------------------
                sHeader = sRecord.Substring(0, 8)  ' like AH200TBI
                If sHeader = "AH200TBI" Or sHeader = "AH200TBI" Then
                    ' ok, sendfile
                ElseIf sHeader = "AH201TBR" Then
                    ' ok, return 1
                ElseIf sHeader = "AH202TBR" Then
                    ' ok, return 2
                Else
                    ' error
                    'addToErrorArray("Wrong header.", sRecord, nNoOfLinesSoFar.ToString)
                    addToErrorArray(LRS(60328), sRecord, nNoOfLinesSoFar.ToString)
                End If
            End If

            '-	Check sequence
            '-----------------
            If sRecord.Length > 74 Then
                If Not EmptyString(sRecord.Substring(70, 4)) Then
                    If IsNumeric(sRecord.Substring(70, 4)) Then
                        nSequence = CInt(sRecord.Substring(70, 4))
                    End If
                Else
                    nSequence = 0
                End If
                If nPreviousSequence > 0 And nSequence - nPreviousSequence <> 1 Then
                    ' not in sequence
                    'addToErrorArray("Sequenceerror, this sequence " & nSequence.ToString & " - Previous sequence " & nPreviousSequence.ToString, sRecord, (nNoOfLinesSoFar - 3).ToString)
                    addToErrorArray(LRS(60329) & nSequence.ToString & LRS(60330) & nPreviousSequence.ToString, sRecord, (nNoOfLinesSoFar - 3).ToString)
                End If
            End If
            ' Sequence may be broken if new division;
            If sRecord.Length > 7 Then
                If sRecord.Substring(40, 8) = "BETFOR00" Then
                    If sDivision <> sRecord.Substring(60, 11) Then
                        nPreviousSequence = 0
                        sDivision = sRecord.Substring(60, 11)
                    End If
                End If
                sPreviousBETFORWas = sThisBETFORIs
                sPreviousRecord = sRecord
                nPreviousSequence = nSequence
            End If
            If oStream.EndOfStream Then
                Exit Do
            End If
        Loop

        If Not bStop Then
            '-	Check fileend complete
            If sThisBETFORIs <> "BETFOR99" Or Len(sRecord) <> 320 Then
                'addToErrorArray("Incorrect completion of file", sRecord, nNoOfLinesSoFar.ToString)
                addToErrorArray(LRS(60331), sRecord, nNoOfLinesSoFar.ToString)
                bStop = True
            End If
            Me.ProgressBar1.Value = 100
        End If

        If bStop Then
            bCanView = False
            Me.cmdView.Enabled = False
        End If

        oStream.Close()
        oStream = Nothing

    End Sub
    Public Sub ReportError()
        '-	Complete report based on ErrArray
        '-	ToScreen
        '-	ToMail
        '-	ToFile
        '-	Toprint
    End Sub
    Public Function RetryWithMending() As Boolean
        ' Retry after LetBabelBankCorrect
        ' See if file is ok then
    End Function
    Public Function LetBabelBankCorrect() As Boolean
        '-	Fill in CR/LF
        '-	Complete with padding
        '-	Other
        '-	And retry Analyzis after that
    End Function
    Private Sub addToErrorArray(ByVal sErrString0 As String, Optional ByVal sErrString1 As String = "", Optional ByVal sErrstring2 As String = "")
        If Not aErrArray(0, 0) Is Nothing Then
            ReDim Preserve aErrArray(2, aErrArray.GetUpperBound(1) + 1)
        End If
        aErrArray(0, aErrArray.GetUpperBound(1)) = sErrString0
        aErrArray(1, aErrArray.GetUpperBound(1)) = sErrString1
        aErrArray(2, aErrArray.GetUpperBound(1)) = sErrstring2
        ' Update listbox with errorMsg
        If Not EmptyString(sErrstring2) Then
            Me.listError.Items.Add("Line: " & sErrstring2 & " " & sErrString0 & " " & sErrString1)
        Else
            Me.listError.Items.Add(sErrString0 & " " & sErrString1)
        End If

        iNoOfErrors = iNoOfErrors + 1
        Me.lblErrorsFound.Text = LRS(60333) & " " & iNoOfErrors.ToString   '- No of errors: ") 

    End Sub

    Private Function TelepayValidBetfor(ByVal sBETFOR As String) As Boolean
        Dim bReturn As Boolean = False
        Select Case sBETFOR
            Case "BETFOR00"
                bReturn = True
            Case "BETFOR01"
                bReturn = True
            Case "BETFOR02"
                bReturn = True
            Case "BETFOR03"
                bReturn = True
            Case "BETFOR04"
                bReturn = True
            Case "BETFOR21"
                bReturn = True
            Case "BETFOR22"
                bReturn = True
            Case "BETFOR23"
                bReturn = True
            Case "BETFOR99"
                bReturn = True
        End Select
        TelepayValidBetfor = bReturn
    End Function

    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Hent filnavn
        Dim spath As String

        Me.lblEMail.Visible = False
        Me.lblErrorsFound.Visible = False
        Me.lblFunction.Visible = False
        Me.lblProgress.Visible = False
        Me.ProgressBar1.Visible = False
        Me.listError.Visible = False
        Me.listFileContent.Visible = False
        Me.listFileContent.Items.Clear()
        aLines = Nothing
        Me.lblAscii.Visible = False
        Me.txtAscii.Visible = False
        Me.txtAscii.Text = ""
        Me.txtEMailReceiver.Visible = False

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        ' JANP sjekk LRS under
        spath = BrowseForFilesOrFolders(Me.txtFilename.Text, Me, LRS(60054), True)
        Me.txtFilename.Text = spath
        sFileName = spath
        Me.listError.Items.Clear()
        Me.lblErrorsFound.Text = LRS(60333) & " 0"
        Me.lblFunction.Text = ""

    End Sub

    Private Sub cmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Me.Close()
    End Sub

    Private Sub cmdView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdView.Click
        Dim oBabelFiles As vbBabel.BabelFiles
        Dim oBabel As vbBabel.BabelFile
        Dim oView As vbBabel.bbViewer
        Dim iImport As Integer

        oBabelFiles = New vbBabel.BabelFiles
        oBabel = oBabelFiles.Add()

        oBabel.ImportFormat = fFormatDetected
        oBabel.FilenameIn = sFileName
        'oBabel.VB_FilenameInNo = aImportInformation(5, iArrayCounter)
        iImport = oBabel.Import()

        If iImport = 1 Then
            oView = New vbBabel.bbViewer
            oView.BabelFiles = oBabelFiles
            oView.Show()
        End If

    End Sub

    Private Sub cmdMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMail.Click
        Dim myMail As vbBabel.vbMail
        Dim sCompany As String ' Companyname, who needs support ?
        Dim sMsgNote As String
        Dim sErrDescription As String
        Dim i As Integer

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sCompanyID As String = ""

        Try
            sCompanyID = "1"  ' TODO - Hardcoded CompanyID
            oMyDal = New vbBabel.DAL
            oMyDal.Provider = vbBabel.DAL.ProviderType.OleDb
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If
            sMySQL = "SELECT * FROM Company "
            sMySQL = sMySQL & "WHERE Company_ID = " & sCompanyID
            oMyDal.SQL = sMySQL

            If oMyDal.Reader_Execute() Then

                While oMyDal.Reader_ReadRecord
                    myMail = New vbBabel.vbMail

                    If oMyDal.Reader_GetString("EmailSMTP") Then
                        myMail.EMail_SMTP = True
                        ' show a form to make comments by user
                        myMail = New vbBabel.vbMail

                        myMail.EMail_SMTP = True
                        If Len(Trim(oMyDal.Reader_GetString("EmailSMTPHost"))) > 0 Then
                            myMail.EMail_SMTP = True
                            myMail.eMailSMTPHost = oMyDal.Reader_GetString("EmailSMTPHost")
                        End If
                    Else
                        myMail.EMail_SMTP = False  ' MAPI
                    End If

                    ' Use new class/dll vbSendmail
                    '-----------------------------
                    myMail.eMailSender = oMyDal.Reader_GetString("eMailSender")
                    myMail.eMailSenderDisplayName = oMyDal.Reader_GetString("eMailDisplayName")
                    myMail.AddReceivers(Me.txtEMailReceiver.Text)

                    myMail.Subject = LRS(60394) & " " & sFileName ' "Rapport for filen " & sFileName
                    myMail.AddAttachment(sFileName)

                    'sMsgNote = "Gyldig tegnsett ser sjekket mot standarden ISO 8859-1." & vbCrLf & vbCrLf
                    sMsgNote = LRS(60395) & vbCrLf & vbCrLf
                    sMsgNote = sMsgNote & LRS(60396) & vbCrLf   '"* (CR = CarriageReturn - Retur) " & vbCrLf
                    sMsgNote = sMsgNote & LRS(60397) & vbCrLf  '"* (LF = LineFeed - Linjeskift) " & vbCrLf
                    sMsgNote = sMsgNote & LRS(60398) & vbCrLf & vbCrLf  '"* (CRLF = CarriageReturn/LineFeed - Retur/Linjeskift) " & vbCrLf & vbCrLf

                    ' run through errorarray to create mailtext
                    'If aErrArray.GetUpperBound(1) = 0 Then
                    If aErrArray(0, 0) Is Nothing Then
                        sMsgNote = sMsgNote & LRS(60399)  '"Ingen feil ble funnet i filen."
                    Else
                        'sMsgNote = sMsgNote & "Følgende feil ble funnet i filen " & sFileName & vbCrLf & vbCrLf
                        sMsgNote = sMsgNote & LRS(60400) & sFileName & vbCrLf & vbCrLf

                        For i = 0 To aErrArray.GetUpperBound(1)
                            If EmptyString(aErrArray(1, i)) Then
                                sMsgNote = sMsgNote & "Line: " & aErrArray(2, i) & " " & aErrArray(0, i) & vbCrLf
                            Else
                                sMsgNote = sMsgNote & "Line: " & aErrArray(2, i) & " " & aErrArray(0, i) & vbCrLf & aErrArray(1, i) & vbCrLf
                            End If
                        Next
                    End If
                    myMail.Body = sMsgNote

                    myMail.Send()

                    myMail = Nothing
                End While
            End If

        Catch
            myMail = Nothing
            sErrDescription = Err.Description
            Err.Raise(15011, , sErrDescription)

            Exit Sub
        End Try
        Exit Sub

    End Sub

    Private Sub listError_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles listError.SelectedIndexChanged
        ' Display content from this line in listFileContent. Also last line and next line, if available
        Dim nLine As Double
        Dim sTmp As String
        Dim i As Integer
        ' use when errorchecking is completed !
        If bErrorCheckingComplete Then
            If InStr(listError.Text, "Line: ") > 0 Then
                ' which line in file
                sTmp = listError.Text.Substring(InStr(listError.Text, "Line: ") - 1)
                sTmp = sTmp.Replace("Line: ", "")
                nLine = Val(sTmp.Substring(0, InStr(sTmp, " "))) - 1  ' -1 to have i zerobased as array and listboxes
            End If
            If nLine < Me.listFileContent.Items.Count Then
                Me.listFileContent.SelectedIndex = nLine
            Else
                Me.listFileContent.SelectedIndex = Me.listFileContent.Items.Count - 1
            End If

            If (nLine - 5) > 1 Then
                Me.listFileContent.TopIndex = nLine - 5
            Else
                Me.listFileContent.TopIndex = nLine
            End If
            ' Then present ascii-values in txtAscii
            sTmp = ""
            Me.txtAscii.Text = ""
            For i = 0 To Len(aLines(nLine)) - 1
                sTmp = sTmp & "-" & Asc(aLines(nLine).Substring(i, 1)).ToString
            Next i
            Me.txtAscii.Text = sTmp
        End If
    End Sub

    Private Sub listFileContent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles listFileContent.SelectedIndexChanged
        Dim sTmp As String
        Dim sLine As String
        Dim i As Integer
        ' present ascii-values in txtAscii
        sLine = Me.listFileContent.Text
        ' remove leading linenumbers;
        sLine = sLine.Substring(InStr(sLine, ":"))
        Me.txtAscii.Text = ""
        For i = 0 To Len(sLine) - 1
            sTmp = sTmp & "-" & Asc(sLine.Substring(i, 1)).ToString
        Next i
        Me.txtAscii.Text = sTmp

    End Sub

End Class

