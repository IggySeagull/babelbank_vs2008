Option Strict Off
Option Explicit On

Friend Class frmVisma_SelectImportProfiles
	Inherits System.Windows.Forms.Form
	' XNET 21.06.2013 added frmVisma_SelectImportProfiles
	Private Declare Function DestroyCaret Lib "user32" () As Integer
	Private bContinue As Boolean
    Private aProfileArray(,) As String
	Private Const vbLightGray As Integer = &H808080
	Private Const vbGray As Integer = &HC0C0C0
	Private Sub frmVisma_SelectImportProfiles_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormLRSCaptions(Me)
		FormVismaStyle(Me)
		' Inactive cmdImport until a file is selected
        Me.cmdOK.Enabled = True

        SpreadHeaders()
        SpreadFill()

    End Sub
    Private Sub SpreadHeaders()
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim checkBoxColumn As DataGridViewCheckBoxColumn

        With Me.gridProfiles

            .Rows.Clear()  ' empty content of grid

            If .ColumnCount = 0 Then
                .ScrollBars = ScrollBars.Vertical
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If



            '' Col 1 is a checkbox, to select rows
            '.Col = 1
            '.set_ColWidth(-2, 3)
            '.Text = " "

            '- checkbox ?
            ' 1 Checkbox
            ' ----------------
            checkBoxColumn = New DataGridViewCheckBoxColumn
            With checkBoxColumn
                .FlatStyle = FlatStyle.Standard
                .CellTemplate = New DataGridViewCheckBoxCell()
                .CellTemplate.Style.BackColor = Color.Beige
                .Width = WidthFromSpreadToGrid(2)
                .TrueValue = True
                .FalseValue = False
            End With
            .Columns.Add(checkBoxColumn)


            '.Col = 2
            '.Text = LRS(35017) ' Profilgruppe
            '.set_ColWidth(-2, 30)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(24)
            txtColumn.HeaderText = LRS(35017) ' Profilgruppe
            .Columns.Add(txtColumn)

            '
            '.Col = 3
            '' hidden col
            '.Text = "ProfileGroupID"
            '.set_ColWidth(-2, 10)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)



        End With

    End Sub
    Private Sub SpreadFill()
        ' Fill up from table array
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim iPG_ID As Short
        Dim i As Short
        ' Show aProfilegroup only once
        Dim bActive As Boolean
        Dim sName As String
        'Dim rsVisma As ADODB.Recordset

        Dim sUID As String
        Dim sPWD As String
        Dim sBBDatabase As String
        Dim sSystemDatabase As String
        Dim sInstance As String
        Dim iNoOfActiveRows As Short
        Dim iFirstActiveRow As Short
        Dim oVismaCommandLine As New VismaCommandLine
        Dim myRow As DataGridViewRow

        iFirstActiveRow = 0
        iNoOfActiveRows = 0
        '
        On Error GoTo VismaFillError

        'VB;/S;F9998;vbsys;system;sa;Visma123;
        'sUID = xDelim(Command, ";", 6, Chr(34))
        'sPWD = xDelim(Command, ";", 7, Chr(34))
        'sBBDatabase = "BabelBank_" & xDelim(Command, ";", 4, Chr(34))
        'sSystemDatabase = xDelim(Command, ";", 4, Chr(34))

        sSystemDatabase = oVismaCommandLine.SystemDatabase
        sBBDatabase = oVismaCommandLine.BBDatabase
        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing

        '- kj�r en egen select for � f� ut profilegroupnames og ID, for ImportProfiles
        '(ligner p� den Kjell kj�rer i Visma_ImportExport
        'sMySQL = "SELECT P.Path_file, F.FormatID, F.Outgoing, F.Incoming, F.DirectDebit, F.eInvoice, P.ProfileGroupId, P.ProfileName FROM " & sBBDatabase & ".dbo.bbProfile P, " & sBBDatabase & ".dbo.bbFormat F WHERE P.Export = 'False' AND P.FormatId = F.FormatID"
        sMySQL = "SELECT P.ProfileGroupId, G.Name "
        sMySQL = sMySQL & "FROM " & sBBDatabase & ".dbo.bbProfile P, " & sBBDatabase & ".dbo.bbFormat F, " & sBBDatabase & ".dbo.bbProfileGroup G "
        sMySQL = sMySQL & "WHERE P.Export = 'False' AND P.FormatId = F.FormatID AND P.ProfileGroupID = G.ProfileGroupID "
        sMySQL = sMySQL & "GROUP BY G.Name, P.ProfileGroupId ORDER BY G.Name, P.ProfileGroupId"


        sErrorString = "Starts ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        'rsVisma = New ADODB.Recordset
        'rsVisma.Open(sMySQL, oVismaCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)
        oVismaDal.SQL = sMySQL

        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows = True Then


                With Me.gridProfiles
                    ' Fill up grid
                    '          rsVisma.MoveFirst()
                    '          Do While Not rsVisma.EOF
                    .Rows.Clear()
                    '			Do While Not rsVisma.EOF
                    Do While oVismaDal.Reader_ReadRecord

                        iPG_ID = oVismaDal.Reader_GetString("ProfileGroupID")
                        sName = oVismaDal.Reader_GetString("Name")
                        bActive = False

                        ' Check if there are files to be imported
                        ' In the Visma_Importarray this is marked with True in element 5
                        For i = 0 To UBound(aProfileArray, 2)
                            If CDbl(aProfileArray(3, i)) = iPG_ID And aProfileArray(5, i) = "True" Then
                                bActive = True
                                Exit For
                            End If
                        Next i

                        myRow = New DataGridViewRow
                        myRow.CreateCells(Me.gridProfiles)

    
                        ' Checkbox
                        '              .Col = 1
                        '              .CellType = FPSpread.CellTypeConstants.CellTypeCheckBox
                        '              .TypeCheckCenter = True
                        '              .Value = CStr(0)
                        '              .TypeButtonType = FPSpread.TypeButtonTypeConstants.TypeButtonTypeNormal
                        myRow.Cells(0).Value = False
                        If Not bActive Then
                            myRow.Cells(0).ReadOnly = True ' can't select where we have found no file
                        Else
                            ' find first profilegroup with an existing file;
                            If iFirstActiveRow = 0 Then
                                ' Myrow is not added yet
                                'iFirstActiveRow = myRow.Index
                                iFirstActiveRow = .RowCount
                            End If
                            iNoOfActiveRows = iNoOfActiveRows + 1
                        End If


                        ' ProfileGroupname
                        '              .Col = 2
                        '              .Value = rsVisma.Fields("Name").Value
                        myRow.Cells(1).Value = oVismaDal.Reader_GetString("Name")
                        myRow.Cells(1).ReadOnly = True    '              .Lock = True
                        If Not bActive Then
                            '"dim"
                            '                  .ForeColor = System.Drawing.ColorTranslator.FromOle(vbLightGray)
                            myRow.Cells(1).Style.ForeColor = System.Drawing.ColorTranslator.FromOle(vbLightGray)
                        End If

                        ' ProfileGroupID, hidden
                        '              .Col = 3
                        '              .Value = rsVisma.Fields("ProfileGroupID").Value
                        '              .ColHidden = True
                        myRow.Cells(2).Value = oVismaDal.Reader_GetString("ProfileGroupID")
                        .Rows.Add(myRow)
                    Loop
                    ' position to first selectable row
                    '          .Row = iFirstActiveRow
                    '          .Col = 1
                    '          .Action = FPSpread.ActionConstants.ActionActiveCell
                    If iNoOfActiveRows = 1 Then
                        ' only one active row, autoselect
                        .Rows(iFirstActiveRow).Selected = True
                        .Rows(iFirstActiveRow).Cells(0).Value = True
                        'Me.cmdOK.Enabled = True
                    End If
                End With
            End If
        End If

        sErrorString = "Closing Visma database"
        oVismaDal.Close()
        oVismaDal = Nothing
        '
        Exit Sub
VismaFillError:

        oVismaDal.Close()
        oVismaDal = Nothing

        If Not Err() Is Nothing Then
            Err.Raise(30018, "frmVisma_SelectImportProfiles.SpreadFill", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30018, "frmVisma_SelectImportProfiles.SpreadFill", sErrorString)
        End If

    End Sub
    '16.06.2016 - Not in use anymore. It didn't work and are unlogical.
    'Private Sub gridProfiles_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridProfiles.CellClick
    '    Dim sValue As String
    '    Dim lCurrentRow As Integer

    '    If e.RowIndex > -1 Then
    '        If e.ColumnIndex = 0 Then
    '            ' Singleclick in Checkbox column
    '            With Me.gridProfiles
    '                If .Rows(e.RowIndex).Cells(0).Value = True Then  ' True
    '                    Me.cmdOK.Enabled = True
    '                End If
    '            End With
    '        End If
    '    End If

    'End Sub
    Public Sub SetProfileArray(ByRef aP(,) As String)
        aProfileArray = VB6.CopyArray(aP)
    End Sub
    Public Function GetProfileArray() As String(,)
        GetProfileArray = VB6.CopyArray(aProfileArray)
    End Function

    Public Function DialogResult() As Boolean
        DialogResult = bContinue
    End Function
    'Private Sub frmVisma_SelectImportProfiles_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    Dim Cancel As Boolean = eventArgs.Cancel
    '    Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
    '    ' trap user clicking the X in upper right corner
    '    Cancel = True
    '    cmdCancel_Click(cmdCancel, New System.EventArgs())
    '    eventArgs.Cancel = Cancel
    'End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        'LRS(35135)  '"Vil du avbryte, uten import?"
        If MsgBox(LRS(35135), MsgBoxStyle.YesNo, "Visma Payment (Babelbank)") = MsgBoxResult.Yes Then
            Me.Hide()
            bContinue = False
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim i As Short
        Dim j As Short
        Dim iPG_ID As Short
        Dim bSelected As Boolean
        Dim bAtLeastOneSelected As Boolean = False

        bContinue = True
        bSelected = False
        Me.Hide()

        ' run through array, and "deselect" all profiles
        For i = 0 To UBound(aProfileArray, 2)
            aProfileArray(5, i) = "False"
        Next i

        With gridProfiles
            ' - m� g� gjennom profilgrupper istedenfor profiler
            iPG_ID = 0
            '	For i = 1 To .MaxRows
            For i = 0 To .RowCount - 1
                '		.Row = i
                '		.Col = 1
                If .Rows(i).Cells(0).Value = True Then
                    bSelected = True
                Else
                    bSelected = False
                End If

                ' Handle only the selected ones;
                If bSelected Then
                    '			.Row = i
                    '			.Col = 3
                    '			iPG_ID = CShort(.Value) 'ProfileGroupID
                    iPG_ID = .Rows(i).Cells(2).Value
                    ' run through array, and mark all profiles for this selected profilegroup
                    For j = 0 To UBound(aProfileArray, 2)
                        If CDbl(aProfileArray(3, j)) = iPG_ID Then
                            ' Mark profile as True, so we can handle this Profile in Visma_ImportExport
                            aProfileArray(5, j) = "True"
                        End If
                    Next j
                End If
            Next i

        End With
        '
        bContinue = True

    End Sub


End Class