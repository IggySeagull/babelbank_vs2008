<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMATCH_ManualeMail
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents txteMail As System.Windows.Forms.TextBox
	Public WithEvents lsteMails As System.Windows.Forms.CheckedListBox
	Public WithEvents txtSubject As System.Windows.Forms.TextBox
	Public WithEvents chkAttachment As System.Windows.Forms.CheckBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblColumns As System.Windows.Forms.Label
	Public WithEvents lblAccountRestAmount As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txteMail = New System.Windows.Forms.TextBox
        Me.lsteMails = New System.Windows.Forms.CheckedListBox
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.chkAttachment = New System.Windows.Forms.CheckBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblColumns = New System.Windows.Forms.Label
        Me.lblAccountRestAmount = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txteMail
        '
        Me.txteMail.AcceptsReturn = True
        Me.txteMail.BackColor = System.Drawing.SystemColors.Window
        Me.txteMail.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txteMail.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txteMail.Location = New System.Drawing.Point(195, 85)
        Me.txteMail.MaxLength = 0
        Me.txteMail.Name = "txteMail"
        Me.txteMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txteMail.Size = New System.Drawing.Size(293, 20)
        Me.txteMail.TabIndex = 0
        Me.txteMail.Text = "@"
        '
        'lsteMails
        '
        Me.lsteMails.BackColor = System.Drawing.SystemColors.Window
        Me.lsteMails.Cursor = System.Windows.Forms.Cursors.Default
        Me.lsteMails.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lsteMails.Location = New System.Drawing.Point(195, 153)
        Me.lsteMails.Name = "lsteMails"
        Me.lsteMails.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lsteMails.Size = New System.Drawing.Size(293, 124)
        Me.lsteMails.Sorted = True
        Me.lsteMails.TabIndex = 1
        Me.lsteMails.Visible = False
        '
        'txtSubject
        '
        Me.txtSubject.AcceptsReturn = True
        Me.txtSubject.BackColor = System.Drawing.SystemColors.Window
        Me.txtSubject.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSubject.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSubject.Location = New System.Drawing.Point(196, 307)
        Me.txtSubject.MaxLength = 168
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSubject.Size = New System.Drawing.Size(291, 20)
        Me.txtSubject.TabIndex = 2
        '
        'chkAttachment
        '
        Me.chkAttachment.BackColor = System.Drawing.SystemColors.Control
        Me.chkAttachment.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAttachment.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAttachment.Location = New System.Drawing.Point(195, 374)
        Me.chkAttachment.Name = "chkAttachment"
        Me.chkAttachment.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAttachment.Size = New System.Drawing.Size(293, 16)
        Me.chkAttachment.TabIndex = 3
        Me.chkAttachment.Text = "60176 - Skjermbilde som vedlegg til mail"
        Me.chkAttachment.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(414, 411)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(336, 411)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "55019-Send"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblColumns
        '
        Me.lblColumns.BackColor = System.Drawing.SystemColors.Control
        Me.lblColumns.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblColumns.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblColumns.Location = New System.Drawing.Point(195, 58)
        Me.lblColumns.Name = "lblColumns"
        Me.lblColumns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblColumns.Size = New System.Drawing.Size(328, 19)
        Me.lblColumns.TabIndex = 7
        Me.lblColumns.Text = "60177 - Send til (ePost adresse)"
        '
        'lblAccountRestAmount
        '
        Me.lblAccountRestAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccountRestAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccountRestAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccountRestAmount.Location = New System.Drawing.Point(195, 289)
        Me.lblAccountRestAmount.Name = "lblAccountRestAmount"
        Me.lblAccountRestAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccountRestAmount.Size = New System.Drawing.Size(209, 19)
        Me.lblAccountRestAmount.TabIndex = 6
        Me.lblAccountRestAmount.Text = "60175 - ePostens emnefelt"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 401)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(480, 1)
        Me.lblLine1.TabIndex = 82
        Me.lblLine1.Text = "Label1"
        '
        'frmMATCH_ManualeMail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(510, 443)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txteMail)
        Me.Controls.Add(Me.lsteMails)
        Me.Controls.Add(Me.txtSubject)
        Me.Controls.Add(Me.chkAttachment)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblColumns)
        Me.Controls.Add(Me.lblAccountRestAmount)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmMATCH_ManualeMail"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60174- Send eMail fra Manuell Avstemming"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
