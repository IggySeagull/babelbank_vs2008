<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDemoText
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtDemo As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblCaption As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDemoText))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtDemo = New System.Windows.Forms.TextBox
		Me.cmdOK = New System.Windows.Forms.Button
		Me.lblCaption = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Form1"
		Me.ClientSize = New System.Drawing.Size(475, 285)
		Me.Location = New System.Drawing.Point(8, 28)
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDemoText"
		Me.txtDemo.AutoSize = False
		Me.txtDemo.BackColor = System.Drawing.Color.FromARGB(255, 255, 192)
		Me.txtDemo.Size = New System.Drawing.Size(467, 207)
		Me.txtDemo.Location = New System.Drawing.Point(4, 44)
		Me.txtDemo.ReadOnly = True
		Me.txtDemo.MultiLine = True
		Me.txtDemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtDemo.TabIndex = 2
		Me.txtDemo.Text = "Text1"
		Me.txtDemo.AcceptsReturn = True
		Me.txtDemo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDemo.CausesValidation = True
		Me.txtDemo.Enabled = True
		Me.txtDemo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDemo.HideSelection = True
		Me.txtDemo.Maxlength = 0
		Me.txtDemo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDemo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDemo.TabStop = True
		Me.txtDemo.Visible = True
		Me.txtDemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtDemo.Name = "txtDemo"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.cmdOK.Size = New System.Drawing.Size(80, 25)
		Me.cmdOK.Location = New System.Drawing.Point(392, 256)
		Me.cmdOK.TabIndex = 1
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.lblCaption.Text = "BabelBank demo"
		Me.lblCaption.Font = New System.Drawing.Font("Arial", 24!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblCaption.ForeColor = System.Drawing.Color.FromARGB(255, 128, 0)
		Me.lblCaption.Size = New System.Drawing.Size(417, 33)
		Me.lblCaption.Location = New System.Drawing.Point(0, 8)
		Me.lblCaption.TabIndex = 0
		Me.lblCaption.BackColor = System.Drawing.SystemColors.Control
		Me.lblCaption.Enabled = True
		Me.lblCaption.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCaption.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCaption.UseMnemonic = True
		Me.lblCaption.Visible = True
		Me.lblCaption.AutoSize = False
		Me.lblCaption.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCaption.Name = "lblCaption"
		Me.Controls.Add(txtDemo)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(lblCaption)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
