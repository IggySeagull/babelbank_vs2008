﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSupportMail_New
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.lblPhone = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtPhone = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.lblComments = New System.Windows.Forms.Label
        Me.chkLicenseFile = New System.Windows.Forms.CheckBox
        Me.chkDatabaseFile = New System.Windows.Forms.CheckBox
        Me.lstSendProfiles = New System.Windows.Forms.ListBox
        Me.lblSendProfiles = New System.Windows.Forms.Label
        Me.lstReturnProfiles = New System.Windows.Forms.ListBox
        Me.lblReceiveProfiles = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblPhone
        '
        Me.lblPhone.BackColor = System.Drawing.SystemColors.Control
        Me.lblPhone.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPhone.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPhone.Location = New System.Drawing.Point(220, 251)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPhone.Size = New System.Drawing.Size(137, 18)
        Me.lblPhone.TabIndex = 96
        Me.lblPhone.Text = "Phone"
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(221, 227)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(137, 18)
        Me.lblName.TabIndex = 95
        Me.lblName.Text = "Name"
        '
        'txtPhone
        '
        Me.txtPhone.AcceptsReturn = True
        Me.txtPhone.BackColor = System.Drawing.SystemColors.Window
        Me.txtPhone.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPhone.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPhone.Location = New System.Drawing.Point(357, 250)
        Me.txtPhone.MaxLength = 0
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPhone.Size = New System.Drawing.Size(329, 20)
        Me.txtPhone.TabIndex = 92
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(358, 225)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(329, 20)
        Me.txtName.TabIndex = 91
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(0, 478)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(680, 1)
        Me.lblLine1.TabIndex = 97
        Me.lblLine1.Text = "Label1"
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(568, 491)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 94
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(480, 491)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 93
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'txtComments
        '
        Me.txtComments.AcceptsReturn = True
        Me.txtComments.BackColor = System.Drawing.SystemColors.Window
        Me.txtComments.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtComments.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtComments.Location = New System.Drawing.Point(221, 92)
        Me.txtComments.MaxLength = 0
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.txtComments.Size = New System.Drawing.Size(467, 126)
        Me.txtComments.TabIndex = 89
        '
        'lblComments
        '
        Me.lblComments.BackColor = System.Drawing.SystemColors.Control
        Me.lblComments.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblComments.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblComments.Location = New System.Drawing.Point(223, 72)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblComments.Size = New System.Drawing.Size(466, 18)
        Me.lblComments.TabIndex = 90
        Me.lblComments.Text = "Please describe the BabelBank problems:"
        '
        'chkLicenseFile
        '
        Me.chkLicenseFile.AutoSize = True
        Me.chkLicenseFile.Location = New System.Drawing.Point(222, 276)
        Me.chkLicenseFile.Name = "chkLicenseFile"
        Me.chkLicenseFile.Size = New System.Drawing.Size(63, 17)
        Me.chkLicenseFile.TabIndex = 98
        Me.chkLicenseFile.Text = "Lisensfil"
        Me.chkLicenseFile.UseVisualStyleBackColor = True
        '
        'chkDatabaseFile
        '
        Me.chkDatabaseFile.AutoSize = True
        Me.chkDatabaseFile.Location = New System.Drawing.Point(222, 300)
        Me.chkDatabaseFile.Name = "chkDatabaseFile"
        Me.chkDatabaseFile.Size = New System.Drawing.Size(72, 17)
        Me.chkDatabaseFile.TabIndex = 99
        Me.chkDatabaseFile.Text = "Database"
        Me.chkDatabaseFile.UseVisualStyleBackColor = True
        '
        'lstSendProfiles
        '
        Me.lstSendProfiles.BackColor = System.Drawing.SystemColors.Window
        Me.lstSendProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstSendProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstSendProfiles.Location = New System.Drawing.Point(222, 347)
        Me.lstSendProfiles.Name = "lstSendProfiles"
        Me.lstSendProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstSendProfiles.Size = New System.Drawing.Size(225, 121)
        Me.lstSendProfiles.TabIndex = 101
        '
        'lblSendProfiles
        '
        Me.lblSendProfiles.BackColor = System.Drawing.SystemColors.Control
        Me.lblSendProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSendProfiles.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSendProfiles.Location = New System.Drawing.Point(221, 328)
        Me.lblSendProfiles.Name = "lblSendProfiles"
        Me.lblSendProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSendProfiles.Size = New System.Drawing.Size(195, 20)
        Me.lblSendProfiles.TabIndex = 100
        Me.lblSendProfiles.Text = "60001"
        '
        'lstReturnProfiles
        '
        Me.lstReturnProfiles.BackColor = System.Drawing.SystemColors.Window
        Me.lstReturnProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstReturnProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstReturnProfiles.Location = New System.Drawing.Point(460, 347)
        Me.lstReturnProfiles.Name = "lstReturnProfiles"
        Me.lstReturnProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstReturnProfiles.Size = New System.Drawing.Size(225, 121)
        Me.lstReturnProfiles.TabIndex = 103
        '
        'lblReceiveProfiles
        '
        Me.lblReceiveProfiles.BackColor = System.Drawing.SystemColors.Control
        Me.lblReceiveProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceiveProfiles.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReceiveProfiles.Location = New System.Drawing.Point(459, 328)
        Me.lblReceiveProfiles.Name = "lblReceiveProfiles"
        Me.lblReceiveProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceiveProfiles.Size = New System.Drawing.Size(195, 20)
        Me.lblReceiveProfiles.TabIndex = 102
        Me.lblReceiveProfiles.Text = "60002"
        '
        'frmSupportMail_New
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(712, 524)
        Me.Controls.Add(Me.lstReturnProfiles)
        Me.Controls.Add(Me.lblReceiveProfiles)
        Me.Controls.Add(Me.lstSendProfiles)
        Me.Controls.Add(Me.lblSendProfiles)
        Me.Controls.Add(Me.chkDatabaseFile)
        Me.Controls.Add(Me.chkLicenseFile)
        Me.Controls.Add(Me.lblPhone)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtPhone)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtComments)
        Me.Controls.Add(Me.lblComments)
        Me.Name = "frmSupportMail_New"
        Me.Text = "frmSupportMail_New"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents lblPhone As System.Windows.Forms.Label
    Public WithEvents lblName As System.Windows.Forms.Label
    Public WithEvents txtPhone As System.Windows.Forms.TextBox
    Public WithEvents txtName As System.Windows.Forms.TextBox
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents txtComments As System.Windows.Forms.TextBox
    Public WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents chkLicenseFile As System.Windows.Forms.CheckBox
    Friend WithEvents chkDatabaseFile As System.Windows.Forms.CheckBox
    Public WithEvents lstSendProfiles As System.Windows.Forms.ListBox
    Public WithEvents lblSendProfiles As System.Windows.Forms.Label
    Public WithEvents lstReturnProfiles As System.Windows.Forms.ListBox
    Public WithEvents lblReceiveProfiles As System.Windows.Forms.Label
End Class
