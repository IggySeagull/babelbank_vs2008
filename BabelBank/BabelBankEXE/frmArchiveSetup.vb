Option Strict Off
Option Explicit On

Friend Class frmArchiveSetup
	Inherits System.Windows.Forms.Form
	Dim bx As Boolean
	Dim bAnythingChanged As Boolean
	Dim bActivateArchiving As Boolean
    Private oMyDal As vbBabel.DAL = Nothing
    Private iCompany_ID As Integer
	
    Private Sub chkActivate_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkActivate.CheckStateChanged

        If chkActivate.CheckState = System.Windows.Forms.CheckState.Checked Then
            txtConnstring.Enabled = True
            cmbDatabaseType.Enabled = True
            chkIncludeOCR.Enabled = True
        Else
            chkActivate.CheckState = False
            txtConnstring.Enabled = False
            cmbDatabaseType.Enabled = False
            chkIncludeOCR.Enabled = False
        End If

    End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bActivateArchiving = chkActivate.CheckState
		Me.Hide()
		
	End Sub
	Friend Function ActivateArchiving() As Boolean
		
		ActivateArchiving = bActivateArchiving
		
	End Function
	Friend Function SetActivateArchiving(ByRef bx As Boolean) As Boolean
		
		bActivateArchiving = SetActivateArchiving
		
	End Function
	Friend Function SetCompany_ID(ByRef l As Integer) As Boolean
		
        iCompany_ID = l 'TOD: Hardcoded CompanyID
		
	End Function
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click

        If Not EmptyString((txtConnstring.Text)) Then
            If Not EmptyString((cmbDatabaseType.Text)) Then

                UpdateArchiveInfo()

                'bx = SaveArchiveInfo(iCompany_ID, (txtConnstring.Text), (cmbDatabaseType.Text), (chkIncludeOCR.CheckState), (chkActivate.CheckState))
                'If bx Then
                bActivateArchiving = chkActivate.CheckState
                'End If
            Else
                MsgBox(LRS(48017), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(48016))
                'A databasetype must be provided to use the archive function - Error during saving data
            End If
        Else
            MsgBox(LRS(48018), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(48016))
            'A connectionstring must be provided to use the archive function. - Error during saving data
        End If

        Me.Hide()
		
	End Sub
	
	Private Sub cmdUse_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUse.Click
		If Not EmptyString((txtConnstring.Text)) Then
			If Not EmptyString((cmbDatabaseType.Text)) Then

                UpdateArchiveInfo()

                'bx = SaveArchiveInfo(iCompany_ID, (txtConnstring.Text), (cmbDatabaseType.Text), (chkIncludeOCR.CheckState), (chkActivate.CheckState))
                'If bx Then
                bActivateArchiving = chkActivate.CheckState
                'End If
            Else
                MsgBox(LRS(48017), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(48016))
                'A databasetype must be provided to use the archive function - Error during saving data
            End If
        Else
            MsgBox(LRS(48018), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(48016))
            'A connectionstring must be provided to use the archive function. - Error during saving data
        End If
		
	End Sub
	
	Private Sub CmdTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdTest.Click
        Dim oMyArchiveDal As vbBabel.DAL = Nothing

        Try
            oMyArchiveDal = New vbBabel.DAL
            Select Case Me.cmbProvider.SelectedIndex 'Me.cmbDatabaseType.Text

                Case 0
                    oMyArchiveDal.Provider = vbBabel.DAL.ProviderType.OleDb
                Case 1
                    oMyArchiveDal.Provider = vbBabel.DAL.ProviderType.Odbc
                Case 2
                    oMyArchiveDal.Provider = vbBabel.DAL.ProviderType.OracleClient
                Case 3
                    oMyArchiveDal.Provider = vbBabel.DAL.ProviderType.SqlClient
                Case 4
                    oMyArchiveDal.Provider = vbBabel.DAL.ProviderType.SqlServerCe_3_5

            End Select
            'oMyArchiveDal.TypeOfDB = vbBabel.DAL.DatabaseType.ArchiveDB
            oMyArchiveDal.Connectionstring = txtConnstring.Text
            oMyArchiveDal.UserID = Me.txtUserID.Text
            oMyArchiveDal.Password = Me.txtPassword.Text
            If Not oMyArchiveDal.ConnectToDB() Then
                Me.lblTestConnection.Text = LRS(48019) & vbCrLf & oMyArchiveDal.ErrorMessage
            Else
                Me.lblTestConnection.Text = Replace(LRS(25004), ":", "") & " OK!" 'Connectionstring OK
            End If

        Catch ex As Exception

            If Not oMyArchiveDal Is Nothing Then
                oMyArchiveDal.Close()
                oMyArchiveDal = Nothing
            End If

            Throw New Exception("Function: frmArchiveSetup CmdTest_Click" & vbCrLf & ex.Message)

        End Try

        If Not oMyArchiveDal Is Nothing Then
            oMyArchiveDal.Close()
            oMyArchiveDal = Nothing
        End If

        'conArchive = ConnectToERPDB((txtConnstring.Text), "", "", True, sErrorString, True)

	End Sub
	
	Private Sub frmArchiveSetup_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormvbStyle(Me, "dollar.jpg")
		FormLRSCaptions(Me)

        'Populate the possible providers
        Me.cmbProvider.Items.Insert(0, "OleDb")
        Me.cmbProvider.Items.Insert(1, "Odbc")
        Me.cmbProvider.Items.Insert(2, "OracleClient")
        Me.cmbProvider.Items.Insert(3, "SqlClient")
        Me.cmbProvider.Items.Insert(4, "SqlServerCe.3.5")

        Me.cmbDatabaseType.Items.Add(("Microsoft Access"))
        Me.cmbDatabaseType.Items.Add(("Oracle ver. ??"))
        Me.cmbDatabaseType.Items.Add(("SQL Server"))
        Me.cmbDatabaseType.Items.Add(("Oracle for Odin"))

        RetrieveArchiveInfo()

    End Sub

    Private Sub RetrieveArchiveInfo()
        Dim sMySQL As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT ArchiveConnString, ArchiveDatabasetype, ArchiveIncludeOCR, ArchiveUseArchive, ArchiveDatabasetype, "
            sMySQL = sMySQL & "ArchiveUID, ArchivePWD, ArchiveProvider "
            sMySQL = sMySQL & "FROM Company WHERE Company_ID = " & iCompany_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        Me.txtConnstring.Text = oMyDal.Reader_GetString("ArchiveConnString")
                        Me.cmbDatabaseType.SelectedIndex = CInt(oMyDal.Reader_GetString("ArchiveDatabasetype"))
                        If CBool(oMyDal.Reader_GetString("ArchiveIncludeOCR")) = True Then
                            Me.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked
                        Else
                            Me.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
                        End If
                        If CBool(oMyDal.Reader_GetString("ArchiveUseArchive")) = True Then
                            Me.chkActivate.CheckState = System.Windows.Forms.CheckState.Checked
                        Else
                            Me.chkActivate.CheckState = System.Windows.Forms.CheckState.Unchecked
                            Me.txtConnstring.Enabled = False
                            Me.cmbDatabaseType.Enabled = False
                            Me.chkIncludeOCR.Enabled = False
                        End If
                        Me.txtUserID.Text = oMyDal.Reader_GetString("ArchiveUID")
                        Me.txtPassword.Text = BBDecrypt(oMyDal.Reader_GetString("ArchivePWD"), 3)
                        Me.cmbProvider.SelectedIndex = oMyDal.Reader_GetString("ArchiveProvider")

                    Loop

                Else
                    Me.cmbDatabaseType.SelectedIndex = 0
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: RetrieveArchiveInfo" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub

    Private Sub UpdateArchiveInfo()
        Dim sMySQL As String

        Try
            If oMyDal Is Nothing Then
                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If
            End If

            sMySQL = "SELECT ArchiveConnString, ArchiveDatabasetype, ArchiveIncludeOCR, ArchiveUseArchive, ArchiveDatabasetype, "
            sMySQL = sMySQL & "ArchiveUID, ArchivePWD, ArchiveProvider "


            sMySQL = "UPDATE Company SET ArchiveConnString = '" & Me.txtConnstring.Text
            If Me.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                sMySQL = sMySQL & "', ArchiveIncludeOCR = True"
            Else
                sMySQL = sMySQL & "', ArchiveIncludeOCR = False"
            End If
            If Me.chkActivate.CheckState = System.Windows.Forms.CheckState.Checked Then
                sMySQL = sMySQL & ", ArchiveUseArchive = True"
            Else
                sMySQL = sMySQL & ", ArchiveUseArchive = False"
            End If
            sMySQL = sMySQL & ", ArchiveDatabasetype = " & Me.cmbDatabaseType.SelectedIndex.ToString
            sMySQL = sMySQL & ", ArchiveUID = '" & Me.txtUserID.Text
            sMySQL = sMySQL & "', ArchivePWD = '" & BBEncrypt((Me.txtPassword.Text), 3)
            sMySQL = sMySQL & "', ArchiveProvider = " & Me.cmbProvider.SelectedIndex.ToString
            sMySQL = sMySQL & " WHERE Company_ID = " & iCompany_ID.ToString

            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: UpdateArchiveInfo" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub
End Class
