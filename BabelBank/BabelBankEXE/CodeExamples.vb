﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module Codeexamples
    Private Sub UseOfDAL()
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim s As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If


            sMySQL = "Select Constring1 FROM DB_Profile WHERE Company_ID = 1"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    s = oMyDal.Reader_GetString("Constring1")

                Loop

            Else

                Throw New Exception("Feilmelding" & vbCrLf & "Feil: " & oMyDal.ErrorMessage & vbCrLf & vbCrLf & "SQL:" & sMySQL)

            End If

        Catch ex As Exception

            Throw New Exception("Funksjonsnavn" & ex.Message)

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub

    Private Sub Timing()

        ' TEMP - testing
        Dim nStartTime As DateTime
        Dim nStopTime As DateTime
        Dim elapsed_time As TimeSpan

        nStartTime = Now

        'Gjør(noe)

        nStopTime = Now
        elapsed_time = nStopTime.Subtract(nStartTime)
        Console.WriteLine(elapsed_time.TotalSeconds.ToString("0.000000"))


    End Sub
    Private Sub ExampleStopwatch()
        Dim sw As New Stopwatch()
        sw.Start()

        'Do some code process here

        sw.Stop()
        MsgBox("Process finished in " + sw.ElapsedMilliseconds.ToString() + " ms")

    End Sub
    Private Sub ExampleStringBuilder()
        Dim sb As New System.Text.StringBuilder()
        Dim i As Integer
        For i = 1 To 10000
            ' Note that we use two Append methods to
            ' avoid creating a temporary string
            sb.Append(CStr(i))
            sb.Append(", ")
        Next
        ' Get the result as a string
        Dim s As String = sb.ToString
    End Sub






    '************************************************************************************************************
    'Functions testing different aspects of ADO.NET
    '************************************************************************************************************
    Public Sub Testadonet()

        'ODBC-connection
        Dim myODBCConnection As System.Data.Odbc.OdbcConnection
        Dim myODBCCommand As System.Data.Odbc.OdbcCommand
        Dim myODBCReader As System.Data.Odbc.OdbcDataReader

        Dim sConnString As String
        Dim sMySQL As String

        Dim sOutput As String
        Dim bNextResult As Boolean
        Dim dNumber As Double
        Dim iIndexName As Integer
        Dim iIndexCurrencyCode As Integer
        Dim iIndexCountryCode As Integer
        Dim iIndexQuotation As Integer
        Dim iIndexBB_InvoiceIdentifier As Integer

        'AddHandler myODBCAdapter.FillError, New FillErrorEventHandler(AddressOf FillError)

        'Create a connectionstring
        sConnString = "Driver={Microsoft Access Driver (*.mdb)};" & "Dbq=C:\Projects\BabelBank\Profiles\Profile.mdb;"
        'sConnString = "DRIVER={SQL Server};SERVER=MyServer;Trusted_connection=yes;DATABASE=northwind;"

        'Create the connection
        myODBCConnection = New Odbc.OdbcConnection(sConnString)
        myODBCConnection.Open()

        '********* MODIFYING DATA **************************************************
        'INSERT/UPDATE, CHANGE/ADD TABLES etc...
        sMySQL = "INSERT INTO Version (VersionNo) Values('SERBIA')"
        myODBCCommand = New Odbc.OdbcCommand(sMySQL) 'Might also add the connection after the sMySQL in this line
        myODBCCommand.Connection = myODBCConnection
        myODBCCommand.ExecuteNonQuery()

        myODBCCommand.Dispose()
        '********* END MODIFYING DATA **************************************************


        '********* SELECTING DATA **************************************************
        'SELECT by using Datareader, ForwardOnly and ReadOnly
        myODBCCommand = New Odbc.OdbcCommand("SELECT * FROM Country", myODBCConnection)

        myODBCReader = myODBCCommand.ExecuteReader

        sOutput = ""
        If myODBCReader.HasRows Then
            If HasColumnAndValue(myODBCReader, "CurrencyCode") Then
                iIndexCurrencyCode = myODBCReader.GetOrdinal("CurrencyCode")
            Else
                iIndexCurrencyCode = -1
            End If
            If HasColumnAndValue(myODBCReader, "Name") Then
                iIndexName = myODBCReader.GetOrdinal("Name")
            Else
                iIndexName = -1
            End If
            If HasColumnAndValue(myODBCReader, "CountryCode") Then
                iIndexCountryCode = myODBCReader.GetOrdinal("CountryCode")
            Else
                iIndexCountryCode = -1
            End If
            If HasColumnAndValue(myODBCReader, "Quotation") Then
                iIndexQuotation = myODBCReader.GetOrdinal("Quotation")
            Else
                iIndexQuotation = -1
            End If
            If HasColumnAndValue(myODBCReader, "BB_InvoiceIdentifier") Then
                iIndexBB_InvoiceIdentifier = myODBCReader.GetOrdinal("BB_InvoiceIdentifier")
            Else
                iIndexBB_InvoiceIdentifier = -1
            End If

            Do While myODBCReader.Read()
                If iIndexName > 0 Then
                    'If Not IsDBNull(myODBCReader.GetString(iIndexName)) Then
                    If Not myODBCReader.IsDBNull(iIndexName) Then 'Not tested
                        sOutput = sOutput & "-" & myODBCReader.GetName(iIndexName) & ":" 'Returns the ColumnName of the fourth column (zerobased)
                        'To find the datatype of the column in the DB.
                        'May use Select Case in a seperate function, something like
                        's = GetReaderItemAsString(myODBCReader.GetValue(iIndex), myODBCReader.GetDataTypeName(iIndex)) As String
                        'Public Function GetReaderItemAsString(o as Object, sDataType as string) As String
                        'Select Case sDataType
                        'Case "Char", "VarChar"
                        '   GetReaderItemAsString = o.tostring
                        'Case "Integer"
                        '
                        'Case "Boolean"
                        '   
                        'End Function 
                        If myODBCReader.GetDataTypeName(iIndexName) = "VARCHAR" Then
                            sOutput = sOutput & "-" & myODBCReader.GetString(iIndexName) 'Returns the ColumnName of the fourth column (zerobased)
                        Else
                            sOutput = sOutput & "-" & myODBCReader.GetInt32(iIndexName).ToString
                        End If
                    End If
                End If
                If iIndexCurrencyCode > 0 Then
                    sOutput = sOutput & "-" & myODBCReader.GetString(iIndexCurrencyCode) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                If iIndexCountryCode > 0 Then
                    sOutput = sOutput & "-" & myODBCReader.GetString(iIndexCountryCode) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                If iIndexQuotation > 0 Then
                    sOutput = sOutput & "-" & myODBCReader.GetInt32(iIndexQuotation) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                If iIndexBB_InvoiceIdentifier > 0 Then
                    sOutput = sOutput & "-" & myODBCReader.GetString(iIndexBB_InvoiceIdentifier) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                'Denna funker ikke når vi kaller på funksjonen HasColumnAndValue. Eller OK
                'sOutput = sOutput & "-" & myODBCReader.GetInt32(0) 'Returns the value in the first column (zerobased), must be an integer in the DB from the 'active' row

                'Alternative måter å hente ut data:
                sOutput = sOutput & "-" & myODBCReader("Quotation") 'Works but bad performance, kan være aktuell ved spesialtilpasninger
                'sOutput = sOutput & "-" & GetReaderItemAsString(myODBCReader, 1) 'Egen funskjon

            Loop
        Else
            sOutput = "No rows returned."
            'Console.WriteLine("No rows returned.")
        End If

        myODBCReader.Close()
        myODBCCommand.Dispose()
        '********* END SELECTING DATA **************************************************

        'Dim nwindConn As SqlConnection = New SqlConnection("Data Source=localhost;Integrated Security=SSPI;Initial Catalog=northwind")

        'Dim selectCMD As SqlCommand = New SqlCommand("SELECT CustomerID, CompanyName FROM Customers", nwindConn)
        'selectCMD.CommandTimeout = 30

        'Dim custDA As SqlDataAdapter = New SqlDataAdapter
        'custDA.SelectCommand = selectCMD

        'nwindConn.Open()

        'Dim custDS As DataSet = New DataSet
        'custDA.Fill(custDS, "Customers")

        'nwindConn.Close()
        '********* END SELECTING DATA **************************************************


        '********* RETURNING A SINGLE VALUE **************************************************
        'Count(), Sum(), Avg() etc....

        sMySQL = "SELECT Count(*) FROM Country"
        myODBCCommand = New Odbc.OdbcCommand
        myODBCCommand.CommandText = sMySQL
        myODBCCommand.Connection = myODBCConnection

        dNumber = CDbl(myODBCCommand.ExecuteScalar)

        myODBCCommand.Dispose()

        '********* END RETURNING A SINGLE VALUE **************************************************

        'Dim ordersCMD As SqlCommand = New SqlCommand("SELECT Count(*) FROM Orders", nwindConn)

        'Dim count As Int32 = CInt(ordersCMD.ExecuteScalar())

        '********* END RETURNING A SINGLE VALUE - not tested **************************************************


        '********* STORED PROCEDURE - not tested **************************************************
        'myODBCCommand = New Odbc.OdbcCommand
        'myODBCCommand.CommandType = CommandType.StoredProcedure

        'Dim myParm As Odbc.OdbcParameter
        'myParm = myODBCCommand.Parameters.Add("@CustomerNo", Odbc.OdbcType.VarChar)
        'myParm.Value = "123456"

        'myODBCReader = myODBCCommand.ExecuteReader
        '********* END STORED PROCEDURE ***********************************************************


        '********* TWO RECORDSETS - not working **************************************************
        'Hente ut to recordset samtidig - funker ikke (kun på SQLConnection?)
        'myODBCCommand = New Odbc.OdbcCommand("SELECT * From Country; SELECT EmployeeID, LastName FROM Employees", myODBCConnection)

        'myODBCReader = myODBCCommand.ExecuteReader

        'bNextResult = True
        'Do Until Not bNextResult
        '    Console.WriteLine(vbTab & myODBCReader.GetName(0) & vbTab & myODBCReader.GetName(1))

        '    Do While myODBCReader.Read()
        '        sOutput = sOutput & "-" & myODBCReader.GetString(1) 'Returns the value in the fourth column (zerobased) from the 'active' row
        '    Loop

        '    bNextResult = myODBCReader.NextResult()
        'Loop

        'myODBCReader.Close()
        'myODBCCommand.Dispose()
        '********* END TWO RECORDSETS - not working **************************************************


        myODBCConnection.Close()
        myODBCConnection.Dispose() 'It is important to Dispose the connection

        'End ODBC Connection

        Exit Sub

        'SQL connection (antagelig kun for SQL Server)
        Dim mySQLConnection As System.Data.SqlClient.SqlConnection
        Dim MySQLCommand As System.Data.SqlClient.SqlCommand
        Dim myReader As System.Data.SqlClient.SqlDataReader '= myCommand.ExecuteReader()

        'Dim MySQLCommand As SqlCommand = New SqlCommand("SELECT CategoryID, CategoryName FROM Categories", mySQLConnection)

        mySQLConnection = New System.Data.SqlClient.SqlConnection("Driver={Microsoft Access Driver (*.mdb)};" & "Dbq=C:\Projects\BabelBank\Profiles\Profile.mdb;")

        'SQL Server
        'mySQLConnection = New SqlConnection("Data Source=localhost;" & "Integrated Security=SSPI;Initial Catalog=northwind")

        MySQLCommand = New System.Data.SqlClient.SqlCommand("SELECT * FROM Country", mySQLConnection)

        myReader = MySQLCommand.ExecuteReader

        sOutput = ""
        If myReader.HasRows Then
            Do While myReader.Read()
                sOutput = sOutput & myReader.GetName("Name")
                'Console.WriteLine(vbTab & "{0}" & vbTab & "{1}", myReader.GetInt32(0), myReader.GetString(1))
            Loop
        Else
            Console.WriteLine("No rows returned.")
        End If

        myReader.Close()
        MySQLCommand.Dispose()

        'Hente ut to recordset samtidig 
        MySQLCommand = New System.Data.SqlClient.SqlCommand("SELECT * From Country; SELECT EmployeeID, LastName FROM Employees", mySQLConnection)

        myReader = MySQLCommand.ExecuteReader

        bNextResult = True
        Do Until Not bNextResult
            Console.WriteLine(vbTab & myReader.GetName(0) & vbTab & myReader.GetName(1))

            Do While myReader.Read()
                Console.WriteLine(vbTab & myReader.GetInt32(0) & vbTab & myReader.GetString(1))
            Loop

            bNextResult = myReader.NextResult()
        Loop

        myReader.Close()
        MySQLCommand.Dispose()
        mySQLConnection.Close()
        mySQLConnection.Dispose()

        'End SQL Connection

        'Har(også)
        'OleDb.OleDbConnection
        'oracleconnection


        'Dim myOdbcCommand As New OdbcCommand(myInsertQuery)
        'myOdbcCommand.Connection = myConn
        'myConn.Open()
        'myOdbcCommand.ExecuteNonQuery()
        'myOdbcCommand.Connection.Close()





        'Dim nwindConn As SqlConnection = New SqlConnection("Data Source=localhost;" & _
        '                                                   "Integrated Security=SSPI;Initial Catalog=northwind")

        'Dim catCMD As SqlCommand = nwindConn.CreateCommand()

        'catCMD.CommandText = "SELECT CategoryID, CategoryName FROM Categories"

        'nwindConn.Open()

        'Dim myReader As SqlDataReader = catCMD.ExecuteReader()

        'Do While myReader.Read()
        '    Console.WriteLine(vbTab & "{0}" & vbTab & "{1}", myReader.GetInt32(0), myReader.GetString(1))
        'Loop

        'myReader.Close()
        'nwindConn.Close()


    End Sub
    Private Sub TestDataadapterODBC()
        Dim myODBCConnection As System.Data.Odbc.OdbcConnection
        Dim myODBCCommand As System.Data.Odbc.OdbcCommand
        Dim myODBCAdapter As System.Data.Odbc.OdbcDataAdapter
        Dim myDataSet As DataSet

        Dim sConnString As String
        Dim sMySQL As String

        sConnString = "Driver={Microsoft Access Driver (*.mdb)};" & "Dbq=C:\Projects\BabelBank\Profiles\Profile.mdb;"

        'Create the connection
        myODBCConnection = New Odbc.OdbcConnection(sConnString)
        myODBCConnection.Open()

        '********* SELECTING DATA **************************************************
        'SELECT by using Dataadapter
        myODBCCommand = New Odbc.OdbcCommand
        sMySQL = "SELECT * FROM Country"
        myODBCCommand.CommandText = sMySQL
        myODBCCommand.Connection = myODBCConnection

        myODBCCommand.CommandTimeout = 30

        myODBCAdapter = New Odbc.OdbcDataAdapter
        myODBCAdapter.SelectCommand = myODBCCommand

        myDataSet = New DataSet

        myODBCAdapter.Fill(myDataSet, "Tabbelnavn") 'The tablename is optional
        'May add more than one adapter to a dataset, 
        'like MyOtherAdapter.Fill(myDataSet, "OtherTableName"). May have seperate Conn and Command

        TestDataSet(myDataSet)

        myDataSet.Dispose()
        myODBCAdapter.Dispose()
        myODBCCommand.Dispose()
        myODBCConnection.Dispose()


    End Sub
    Private Sub TestDataadapterOLEDB()
        Dim myOleDbConnection As System.Data.OleDb.OleDbConnection
        Dim myOleDbCommand As System.Data.OleDb.OleDbCommand
        Dim myOleDbAdapter As System.Data.OleDb.OleDbDataAdapter
        Dim myDataSet As DataSet

        Dim sConnString As String
        Dim sMySQL As String

        sConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Projects\BabelBank\Profiles\Profile.mdb"

        'Create the connection
        myOleDbConnection = New System.Data.OleDb.OleDbConnection(sConnString)
        myOleDbConnection.Open()

        '********* SELECTING DATA **************************************************
        'SELECT by using Dataadapter
        myOleDbCommand = New System.Data.OleDb.OleDbCommand
        sMySQL = "SELECT * FROM Country"
        myOleDbCommand.CommandText = sMySQL
        myOleDbCommand.Connection = myOleDbConnection

        myOleDbCommand.CommandTimeout = 30

        myOleDbAdapter = New System.Data.OleDb.OleDbDataAdapter
        myOleDbAdapter.SelectCommand = myOleDbCommand

        myDataSet = New DataSet

        myOleDbAdapter.Fill(myDataSet, "Tabbelnavn") 'The tablename is optional
        'May add more than one adapter to a dataset, 
        'like MyOtherAdapter.Fill(myDataSet, "OtherTableName"). May have seperate Conn and Command

        TestDataSet(myDataSet)

        myDataSet.Dispose()
        myoledbAdapter.Dispose()
        myOleDbCommand.Dispose()
        myOleDbConnection.Dispose()


    End Sub
    Private Sub TestDataadapterDatabaseIndependent()
        Dim myConnection As System.Data.Common.DbConnection
        Dim myCommand As System.Data.Common.DbCommand
        Dim myReader As System.Data.Common.DbDataReader
        Dim sMySQL As String

        Dim sOutput As String
        Dim iIndexName As Integer
        Dim iIndexCurrencyCode As Integer
        Dim iIndexCountryCode As Integer
        Dim iIndexQuotation As Integer
        Dim iIndexBB_InvoiceIdentifier As Integer
        Dim bInitiated As Boolean

        myConnection = CreateDbConnection("System.Data.OleDb", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Projects\BabelBank\Profiles\Profile.mdb")
        'myConnection.Open()

        sMySQL = "SELECT * FROM Country"

        ' Check for valid DbConnection.
        If Not myConnection Is Nothing Then
            Using myConnection
                Try
                    ' Create the command.
                    'Dim command As System.Data.Common.DbCommand = myConnection.CreateCommand()
                    myCommand = myConnection.CreateCommand()
                    myCommand.CommandText = sMySQL
                    myCommand.CommandType = CommandType.Text


                    'Andre parametre som kan være interessante
                    'myCommand.CommandType = CommandType.StoredProcedure
                    'myCommand.Cancel() - Prøver å stoppe ekskveringen av en DbCommand (SQL-en)
                    'myCommand.CreateParameter - ParameterDirection til StoredProcedure?
                    'myCommand.ExecuteNonQuery - Kjører SQL som ikke returnerer felter, 'INSERT/UPDATE, CHANGE/ADD TABLES etc...
                    'myCommand.ExecuteScalar - Typisk SELECT COUNT(*) ....! Executes the query and returns the first column of the first row in the result set returned by the query.


                    ' Open the connection.
                    myConnection.Open()

                    ' Retrieve the data.
                    'Dim reader As System.Data.Common.DbDataReader = command.ExecuteReader()
                    myReader = myCommand.ExecuteReader

                    bInitiated = False
                    sOutput = ""
                    If myReader.HasRows Then
                        Do While myReader.Read()
                            If Not bInitiated Then
                                If HasColumnAndValue(myReader, "CurrencyCode") Then
                                    iIndexCurrencyCode = myReader.GetOrdinal("CurrencyCode")
                                Else
                                    iIndexCurrencyCode = -1
                                End If
                                If HasColumnAndValue(myReader, "Name") Then
                                    iIndexName = myReader.GetOrdinal("Name")
                                Else
                                    iIndexName = -1
                                End If
                                If HasColumnAndValue(myReader, "CountryCode") Then
                                    iIndexCountryCode = myReader.GetOrdinal("CountryCode")
                                Else
                                    iIndexCountryCode = -1
                                End If
                                If HasColumnAndValue(myReader, "Quotation") Then
                                    iIndexQuotation = myReader.GetOrdinal("Quotation")
                                Else
                                    iIndexQuotation = -1
                                End If
                                If HasColumnAndValue(myReader, "BB_InvoiceIdentifier") Then
                                    iIndexBB_InvoiceIdentifier = myReader.GetOrdinal("BB_InvoiceIdentifier")
                                Else
                                    iIndexBB_InvoiceIdentifier = -1
                                End If
                                bInitiated = True
                            End If

                            If iIndexName > 0 Then
                                'If Not IsDBNull(myReader.GetString(iIndexName)) Then
                                If Not myReader.IsDBNull(iIndexName) Then 'Not tested
                                    sOutput = sOutput & "-" & myReader.GetName(iIndexName) & ":" 'Returns the ColumnName of the fourth column (zerobased)
                                    'To find the datatype of the column in the DB.
                                    'May use Select Case in a seperate function, something like
                                    's = GetReaderItemAsString(myReader.GetValue(iIndex), myReader.GetDataTypeName(iIndex)) As String
                                    'Public Function GetReaderItemAsString(o as Object, sDataType as string) As String
                                    'Select Case sDataType
                                    'Case "Char", "VarChar"
                                    '   GetReaderItemAsString = o.tostring
                                    'Case "Integer"
                                    '
                                    'Case "Boolean"
                                    '   
                                    'End Function 
                                    If myReader.GetDataTypeName(iIndexName) = "DBTYPE_WVARCHAR" Then
                                        sOutput = sOutput & "-" & myReader.GetString(iIndexName) 'Returns the ColumnName of the fourth column (zerobased)
                                    Else
                                        sOutput = sOutput & "-" & myReader.GetInt32(iIndexName).ToString
                                    End If
                                End If
                            End If
                            If iIndexCurrencyCode > 0 Then
                                sOutput = sOutput & "-" & myReader.GetString(iIndexCurrencyCode) 'Returns the ColumnName of the fourth column (zerobased)
                            End If
                            If iIndexCountryCode > 0 Then
                                sOutput = sOutput & "-" & myReader.GetString(iIndexCountryCode) 'Returns the ColumnName of the fourth column (zerobased)
                            End If
                            If iIndexQuotation > 0 Then
                                sOutput = sOutput & "-" & myReader.GetInt32(iIndexQuotation) 'Returns the ColumnName of the fourth column (zerobased)
                            End If
                            If iIndexBB_InvoiceIdentifier > 0 Then
                                sOutput = sOutput & "-" & myReader.GetString(iIndexBB_InvoiceIdentifier) 'Returns the ColumnName of the fourth column (zerobased)
                            End If
                            'Denna funker ikke når vi kaller på funksjonen HasColumnAndValue. Eller OK
                            'sOutput = sOutput & "-" & myReader.GetInt32(0) 'Returns the value in the first column (zerobased), must be an integer in the DB from the 'active' row

                            'Alternative måter å hente ut data:
                            sOutput = sOutput & "-" & myReader("Quotation") 'Works but bad performance, kan være aktuell ved spesialtilpasninger
                            'sOutput = sOutput & "-" & GetReaderItemAsString(myReader, 1) 'Egen funskjon
                        Loop

                    Else
                        sOutput = "No rows returned."
                        'Console.WriteLine("No rows returned.")
                    End If

                Catch ex As Exception
                    Console.WriteLine("Exception.Message: {0}", ex.Message)
                End Try
            End Using
        Else
            Console.WriteLine("Failed: DbConnection is Nothing.")
        End If

        myReader.Close()
        myCommand.Dispose()
        myConnection.Close()
        myConnection.Dispose()

    End Sub
    Private Function HasColumnAndValue(ByVal reader As System.Data.Common.DbDataReader, ByVal columnName As String) As Boolean
        Dim i As Integer

        For i = 0 To reader.FieldCount - 1
            If reader.GetName(i).Equals(columnName) Then
                Return Not IsDBNull(reader.GetValue(i))
                'Return Not IsDBNull(reader(columnName))
            End If
        Next

        Return False
    End Function
    '        DbProviderFactory(provider = System.Data.Common.DbProviderFactories.GetFactory("System.Data.SqlClient"))
    'DbConnection conn = provider.CreateConnection();conn.ConnectionString = connectionString; 
    'Bug and Project Tracking with JIRA

    'Ads by TechClicks

    'Now you have you connection object ready to be used. We can now use it to call a stored procedure to the server:
    ' DbCommand dbcmd = conn.CreateCommand();dbcmd.Connection = conn;dbcmd.CommandText = "GetPageByID";dbcmd.CommandType = CommandType.StoredProcedure;// If your SP requires input parametersDbParameter oParam1 = dbcmd.CreateParameter();oParam1.ParameterName = "@PageID";oParam1.DbType = DbType.Int32;oParam1.Value = ID;dbcmd.Parameters.Add(oParam1);DbDataReader dr = dbcmd.ExecuteReader();
    '    End Sub
    'Public Shared Function GetFactoryClasses() As DataTable




    ' Given a provider, create a DbProviderFactory and DbConnection.
    ' Returns a DbConnection on success; Nothing on failure.
    Private Function CreateDbConnection(ByVal providerName As String, ByVal connectionString As String) As System.Data.Common.DbConnection

        ' Assume failure.
        Dim connection As System.Data.Common.DbConnection = Nothing

        ' Create the DbProviderFactory and DbConnection.
        If Not connectionString Is Nothing Then
            Try
                Dim factory As System.Data.Common.DbProviderFactory = System.Data.Common.DbProviderFactories.GetFactory(providerName)
                'System.Data.Odbc
                'System.Data.OleDb
                'System.Data.OracleClient
                'System.Data.SqlClient
                'System.Data.SqlServerCe.3.5


                connection = factory.CreateConnection()
                connection.ConnectionString = connectionString

            Catch ex As Exception
                ' Set the connection to Nothing if it was created.
                If Not connection Is Nothing Then
                    connection = Nothing
                End If
                Console.WriteLine(ex.Message)
            End Try
        End If

        ' Return the connection.
        Return connection
    End Function
    ' Takes a DbConnection and creates a DbCommand to retrieve data
    ' from the Categories table by executing a DbDataReader. 
    Private Sub DbCommandSelect(ByVal connection As System.Data.Common.DbConnection)

        Dim queryString As String = _
           "SELECT CategoryID, CategoryName FROM Categories"

        ' Check for valid DbConnection.
        If Not connection Is Nothing Then
            Using connection
                Try
                    ' Create the command.
                    Dim command As System.Data.Common.DbCommand = connection.CreateCommand()
                    command.CommandText = queryString
                    command.CommandType = CommandType.Text

                    ' Open the connection.
                    connection.Open()

                    ' Retrieve the data.
                    Dim reader As System.Data.Common.DbDataReader = command.ExecuteReader()
                    Do While reader.Read()
                        Console.WriteLine("{0}. {1}", reader(0), reader(1))
                    Loop

                Catch ex As Exception
                    Console.WriteLine("Exception.Message: {0}", ex.Message)
                End Try
            End Using
        Else
            Console.WriteLine("Failed: DbConnection is Nothing.")
        End If
    End Sub



    Private Sub TestDataSet(ByVal myDataSet As System.Data.DataSet)
        Dim MyGeneralDataReader As System.Data.DataTableReader
        Dim i As Integer
        Dim sCountries As String
        Dim iNoOfRecords As Integer

        'Test DataTableReader
        Dim sOutput As String
        Dim iIndexName As Integer
        Dim iIndexCurrencyCode As Integer
        Dim iIndexCountryCode As Integer
        Dim iIndexQuotation As Integer
        Dim iIndexBB_InvoiceIdentifier As Integer

        sCountries = ""
        iNoOfRecords = myDataSet.Tables("Tabbelnavn").Rows.Count - 1
        For i = 0 To myDataSet.Tables("Tabbelnavn").Rows.Count - 1
            sCountries = sCountries & "-" & myDataSet.Tables(0).Rows(i).Item("Name")
        Next

        MyGeneralDataReader = myDataSet.CreateDataReader()

        'If MyGeneralDataReader.HasRows Then
        '    Do While MyGeneralDataReader.Read()
        '        sOutput = sOutput & "-" & MyGeneralDataReader.GetString(3)
        '    Loop
        'End If

        sOutput = ""
        If MyGeneralDataReader.HasRows Then
            'If HasColumnAndValueGeneralReader(MyGeneralDataReader, "CurrencyCode") Then
            '    iIndexCurrencyCode = MyGeneralDataReader.GetOrdinal("CurrencyCode")
            'Else
            '    iIndexCurrencyCode = -1
            'End If
            'If HasColumnAndValueGeneralReader(MyGeneralDataReader, "Name") Then
            '    iIndexName = MyGeneralDataReader.GetOrdinal("Name")
            'Else
            '    iIndexName = -1
            'End If
            'If HasColumnAndValueGeneralReader(MyGeneralDataReader, "CountryCode") Then
            '    iIndexCountryCode = MyGeneralDataReader.GetOrdinal("CountryCode")
            'Else
            '    iIndexCountryCode = -1
            'End If
            'If HasColumnAndValueGeneralReader(MyGeneralDataReader, "Quotation") Then
            '    iIndexQuotation = MyGeneralDataReader.GetOrdinal("Quotation")
            'Else
            '    iIndexQuotation = -1
            'End If
            'If HasColumnAndValueGeneralReader(MyGeneralDataReader, "BB_InvoiceIdentifier") Then
            '    iIndexBB_InvoiceIdentifier = MyGeneralDataReader.GetOrdinal("BB_InvoiceIdentifier")
            'Else
            '    iIndexBB_InvoiceIdentifier = -1
            'End If

            Do While MyGeneralDataReader.Read()
                If iIndexName > 0 Then
                    'If Not IsDBNull(MyGeneralDataReader.GetString(iIndexName)) Then
                    If Not MyGeneralDataReader.IsDBNull(iIndexName) Then 'Not tested
                        sOutput = sOutput & "-" & MyGeneralDataReader.GetName(iIndexName) & ":" 'Returns the ColumnName of the fourth column (zerobased)
                        'To find the datatype of the column in the DB.
                        'May use Select Case in a seperate function, something like
                        's = GetReaderItemAsString(MyGeneralDataReader.GetValue(iIndex), MyGeneralDataReader.GetDataTypeName(iIndex)) As String
                        'Public Function GetReaderItemAsString(o as Object, sDataType as string) As String
                        'Select Case sDataType
                        'Case "Char", "VarChar"
                        '   GetReaderItemAsString = o.tostring
                        'Case "Integer"
                        '
                        'Case "Boolean"
                        '   
                        'End Function 
                        If MyGeneralDataReader.GetDataTypeName(iIndexName) = "VARCHAR" Then
                            sOutput = sOutput & "-" & MyGeneralDataReader.GetString(iIndexName) 'Returns the ColumnName of the fourth column (zerobased)
                        Else
                            sOutput = sOutput & "-" & MyGeneralDataReader.GetInt32(iIndexName).ToString
                        End If
                    End If
                End If
                If iIndexCurrencyCode > 0 Then
                    sOutput = sOutput & "-" & MyGeneralDataReader.GetString(iIndexCurrencyCode) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                If iIndexCountryCode > 0 Then
                    sOutput = sOutput & "-" & MyGeneralDataReader.GetString(iIndexCountryCode) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                If iIndexQuotation > 0 Then
                    sOutput = sOutput & "-" & MyGeneralDataReader.GetInt32(iIndexQuotation) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                If iIndexBB_InvoiceIdentifier > 0 Then
                    sOutput = sOutput & "-" & MyGeneralDataReader.GetString(iIndexBB_InvoiceIdentifier) 'Returns the ColumnName of the fourth column (zerobased)
                End If
                'Denna funker ikke når vi kaller på funksjonen HasColumnAndValue. Eller OK
                'sOutput = sOutput & "-" & MyGeneralDataReader.GetInt32(0) 'Returns the value in the first column (zerobased), must be an integer in the DB from the 'active' row

                'Alternative måter å hente ut data:
                sOutput = sOutput & "-" & MyGeneralDataReader("Quotation") 'Works but bad performance, kan være aktuell ved spesialtilpasninger
                'sOutput = sOutput & "-" & GetReaderItemAsString(MyGeneralDataReader, 1) 'Egen funskjon

            Loop
        Else
            sOutput = "No rows returned."
            'Console.WriteLine("No rows returned.")
        End If

        MyGeneralDataReader.Close()


    End Sub


    Private Function HasColumnAndValueOld(ByRef reader As System.Data.Odbc.OdbcDataReader, ByVal columnName As String) As Boolean
        'Public Function HasColumnAndValue(ByRef reader As IDataReader, ByVal columnName As String) As Boolean

        For i As Integer = 0 To reader.FieldCount - 1
            If reader.GetName(i).Equals(columnName) Then

                Return Not IsDBNull(reader(columnName))
            End If
        Next

        Return False
    End Function
    Private Sub FillError(ByVal sender As Object, ByVal args As FillErrorEventArgs)
        'If args.Errors.GetType() Is Type.GetType("System.OverflowException") Then
        '    ' Code to handle precision loss.
        '    ' Add a row to table using the values from the first two columns.
        '    DataRow(myRow = args.DataTable.Rows.Add(New Object() {args.Values(0), args.Values(1), DBNull.Value}))
        '    ' Set the RowError containing the value for the third column.
        '    args.RowError = "OverflowException Encountered. Value from data source: " & args.Values(2)

        '    args.Continue = True
        'End If
    End Sub
    '************************************************************************************************************
    'End - Functions testing different aspects of ADO.NET
    '************************************************************************************************************


End Module