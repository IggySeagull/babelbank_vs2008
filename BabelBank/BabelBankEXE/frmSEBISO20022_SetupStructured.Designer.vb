<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSEBISO20022_SetupStructured
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtCreditNote As System.Windows.Forms.TextBox
	Public WithEvents txtOCR As System.Windows.Forms.TextBox
	Public WithEvents txtInvoice As System.Windows.Forms.TextBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public WithEvents lblCreditNote As System.Windows.Forms.Label
	Public WithEvents lblExplaination As System.Windows.Forms.Label
	Public WithEvents lblOCR As System.Windows.Forms.Label
	Public WithEvents lblInvoice As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCreditNote = New System.Windows.Forms.TextBox
        Me.txtOCR = New System.Windows.Forms.TextBox
        Me.txtInvoice = New System.Windows.Forms.TextBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.lblCreditNote = New System.Windows.Forms.Label
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblOCR = New System.Windows.Forms.Label
        Me.lblInvoice = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtCreditNote
        '
        Me.txtCreditNote.AcceptsReturn = True
        Me.txtCreditNote.BackColor = System.Drawing.SystemColors.Window
        Me.txtCreditNote.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCreditNote.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCreditNote.Location = New System.Drawing.Point(328, 179)
        Me.txtCreditNote.MaxLength = 20
        Me.txtCreditNote.Name = "txtCreditNote"
        Me.txtCreditNote.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCreditNote.Size = New System.Drawing.Size(169, 20)
        Me.txtCreditNote.TabIndex = 2
        '
        'txtOCR
        '
        Me.txtOCR.AcceptsReturn = True
        Me.txtOCR.BackColor = System.Drawing.SystemColors.Window
        Me.txtOCR.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtOCR.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtOCR.Location = New System.Drawing.Point(328, 115)
        Me.txtOCR.MaxLength = 20
        Me.txtOCR.Name = "txtOCR"
        Me.txtOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtOCR.Size = New System.Drawing.Size(169, 20)
        Me.txtOCR.TabIndex = 0
        '
        'txtInvoice
        '
        Me.txtInvoice.AcceptsReturn = True
        Me.txtInvoice.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvoice.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInvoice.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInvoice.Location = New System.Drawing.Point(328, 147)
        Me.txtInvoice.MaxLength = 20
        Me.txtInvoice.Name = "txtInvoice"
        Me.txtInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInvoice.Size = New System.Drawing.Size(169, 20)
        Me.txtInvoice.TabIndex = 1
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(440, 214)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(65, 21)
        Me.CmdCancel.TabIndex = 4
        Me.CmdCancel.Text = "55006 &Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(360, 214)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(73, 21)
        Me.cmdSave.TabIndex = 3
        Me.cmdSave.Text = "55034 &Save"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'lblCreditNote
        '
        Me.lblCreditNote.BackColor = System.Drawing.SystemColors.Control
        Me.lblCreditNote.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCreditNote.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCreditNote.Location = New System.Drawing.Point(200, 179)
        Me.lblCreditNote.Name = "lblCreditNote"
        Me.lblCreditNote.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCreditNote.Size = New System.Drawing.Size(87, 11)
        Me.lblCreditNote.TabIndex = 8
        Me.lblCreditNote.Text = "Kreditnota"
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(200, 71)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(313, 41)
        Me.lblExplaination.TabIndex = 7
        Me.lblExplaination.Tag = "Sett opp "
        Me.lblExplaination.Text = "Sett opp tekster for gjenkjenning av strukturert informasjon i meldingstekst, for" & _
            " OCR, fakturanummer og kreditnota"
        '
        'lblOCR
        '
        Me.lblOCR.BackColor = System.Drawing.SystemColors.Control
        Me.lblOCR.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOCR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOCR.Location = New System.Drawing.Point(200, 115)
        Me.lblOCR.Name = "lblOCR"
        Me.lblOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOCR.Size = New System.Drawing.Size(87, 19)
        Me.lblOCR.TabIndex = 6
        Me.lblOCR.Text = "OCR"
        '
        'lblInvoice
        '
        Me.lblInvoice.BackColor = System.Drawing.SystemColors.Control
        Me.lblInvoice.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInvoice.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInvoice.Location = New System.Drawing.Point(200, 147)
        Me.lblInvoice.Name = "lblInvoice"
        Me.lblInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInvoice.Size = New System.Drawing.Size(87, 19)
        Me.lblInvoice.TabIndex = 5
        Me.lblInvoice.Text = "Faktura"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 207)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(505, 1)
        Me.lblLine1.TabIndex = 87
        Me.lblLine1.Text = "Label1"
        '
        'frmSEBISO20022_SetupStructured
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(525, 247)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtCreditNote)
        Me.Controls.Add(Me.txtOCR)
        Me.Controls.Add(Me.txtInvoice)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.lblCreditNote)
        Me.Controls.Add(Me.lblExplaination)
        Me.Controls.Add(Me.lblOCR)
        Me.Controls.Add(Me.lblInvoice)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(8, 30)
        Me.Name = "frmSEBISO20022_SetupStructured"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Strukturerte betalinger"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class