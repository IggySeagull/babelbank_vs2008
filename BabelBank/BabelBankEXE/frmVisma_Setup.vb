Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmVisma_Setup
	Inherits System.Windows.Forms.Form
	
    Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer
	Private Declare Function GetWindowLong Lib "user32"  Alias "GetWindowLongA"(ByVal hwnd As Integer, ByVal nIndex As Integer) As Integer
	Private Const WS_EX_APPWINDOW As Integer = &H40000
	Private Const GWL_EXSTYLE As Short = (-20)
	
	Private Declare Function DestroyCaret Lib "user32" () As Integer
	Private Const vbGray As Integer = &HC0C0C0
	Private Const vbLightGray As Integer = &H808080
	Private sCurrentSpread As String ' "E" (Export) or "I" (Import)
    Private aProfileGroups(,) As String
	Private aProfileGroupsStatus() As Short ' Holds status about new (0), deleted (-1)
    Private iSelectedProfileGroupIndex As Integer = -1
    Private iProfileID As Short
    Dim iProfileGroupID As Short
    'Private sMode As String
	Private sUID As String
	Private sPWD As String
	Private sBBDatabase As String
	Private sUser As String
	Private sInstance As String
    Private bChanged As Boolean = False
	Private sSystemDatabase As String
    '    Dim aFormats(,) As String
	Dim bLog As Boolean
	Dim oBabelLog As vbLog.vbLogging
	Dim bNewMode As Boolean
	Dim lPreviousImportRow As Integer
	Dim lPreviousExportRow As Integer
    'Dim iNextProfileID As Short
    Dim bInit As Boolean = True

	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
		Dim sURL As String
		Dim lLocalID As String
		
		' Call URL for Help-documentation
		' Link is dependant upon language
        If Len(Dir(My.Application.Info.DirectoryPath & "\language.txt")) > 0 Then
            lLocalID = GetLanguageCodeFromFile()
        Else
            ' Find the LocalID.
            lLocalID = Hex(GetUserDefaultLCID)
        End If
		
		If lLocalID = "414" Or lLocalID = "814" Then
			' Norwegian
			sURL = "http://doc.visma.net/userdoc/BabelBank/1.88/nb-no/VismaPaymentBabelbank.html"
		ElseIf UCase(lLocalID) = "41D" Then 
			' Swedish
			sURL = "http://doc.visma.net/userdoc/BabelBank/1.88/sv-se/VismaPaymentBabelbank.html"
		ElseIf lLocalID = "406" Then 
			' Danish
			sURL = "http://doc.visma.net/userdoc/BabelBank/1.88/da-dk/VismaPaymentBabelbank.html"
		Else
			' English
			sURL = "http://doc.visma.net/userdoc/BabelBank/1.88/en-gb/VismaPaymentBabelbank.html"
		End If
		''    sURL = "http://www.gulesider.no"    ' TODO Visma, bytt ut.
		
		ShellExecute(Me.Handle.ToInt32, "open", sURL, vbNullString, vbNullString, SW_SHOW)
		
	End Sub
    Private Sub Form_Initialize_Renamed()
        Dim FormStyles As Integer

        'FormStyles = GetWindowLong(Me.Handle.ToInt32, GWL_EXSTYLE)
        'FormStyles = FormStyles Or WS_EX_APPWINDOW
        'Call SetWindowLong(Me.Handle.ToInt32, GWL_EXSTYLE, FormStyles)
    End Sub
	
	Private Sub frmVisma_Setup_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim oVismaCommandLine As New VismaCommandLine
		
        sSystemDatabase = oVismaCommandLine.SystemDatabase
		sBBDatabase = oVismaCommandLine.BBDatabase
		sUser = oVismaCommandLine.User
		sUID = oVismaCommandLine.UID
		sPWD = oVismaCommandLine.PWD
		sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing
		
        'sMode = "NORMAL"
		FormLRSCaptions(Me)
        FormVismaStyle(Me)

        ' Hide all controls
        bChanged = False

		
        sprExportImportCreate()
        bInit = False

        'We will also need to present BabelBank versionno
        'Me.lblBBVersion.BackColor = System.Drawing.ColorTranslator.FromOle(&H808080)

        'If RunTime() Then  'DISKUTERNOTRUNTIME Dette blir feil i 2017
        Me.lblBBVersion.Text = LRS(60045) & " " & _
        Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\BabelBank.exe").ProductVersion, ".0.", ".")
        '(Redo from like 2.2.0.32 to 2.2.32)
        '??? System.Reflection.Assembly.GetExecutingAssembly.GetName.Version.Major
        'Else
        '    Me.lblBBVersion.Text = LRS(60045) & " " & Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo("C:\Projects.net\BabelBank\Bin\x86\release\vbbabel.dll").ProductVersion, ".0.", ".")
        'End If

        cmbProfileGroupFill()

        If Me.cmbProfileGroup.Items.Count > 0 Then
            iSelectedProfileGroupIndex = 0
            Me.cmbProfileGroup.SelectedIndex = 0
            Me.cmbProfileGroup.Visible = True
            'Me.cmdAdd.Visible = True
            'Me.cmdDelete.Visible = True
            'Me.cmdEdit.Visible = True
            'Me.cmdOK.Visible = True
            sCurrentSpread = "E"
            Me.lblExp.Text = LRS(60008)
            Me.lblExp.Visible = True

        End If


    End Sub
    Private Sub mnuConvertXToBabelBank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConvertXToBabelBank.Click
        ' Convert ConvertX database to BabelBank database
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sCXDatabase As String

        On Error GoTo Err_Renamed

        If MsgBox(LRS(35224), MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then
            ' Bekreft at du �nsker � konvertere Convert-X databasen til BabelBank database
            sErrorString = "Starts ConnectToVismaBusinessDB"
            '			oVismaCon = CreateObject ("ADODB.Connection")
            '			oVismaCon = ConnectToVismaBusinessDB(False, sErrorString, True, sUID, sPWD, sInstance)
            oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)


            sErrorString = "Retrieve values from the ConvertX database"
            'Retrieve the ConvertX database
            sCXDatabase = ""
            If Visma_FindCXDatabase(oVismaDal, sSystemDatabase, sCXDatabase) Then
                'VISMA TODO
                'Visma_ConvertFromCXtoBB(oVismaCon, sUID, sPWD, sBBDatabase, sCXDatabase, sUser, sInstance, False)
            Else
                'MsgBox "BabelBank is unable to find the appropriate ConvertX-database in Visma.", vbExclamation + vbOKOnly, "Can't find the ConvertX-database!"
                MsgBox(LRS(35200), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, LRS(35201))
            End If

            sErrorString = "Refreshing form"
            Me.prgActionProgress.Visible = False

            oVismaDal.Close()
            oVismaDal = Nothing
        End If
        Exit Sub

Err_Renamed:

        oVismaDal.Close()
        oVismaDal = Nothing

        If Not Err() Is Nothing Then
            Err.Raise(30008, "mnuConvertXTilBabel_Click", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30008, "mnuConvertXTilBabel_Click", sErrorString)
        End If

    End Sub

    Private Sub mnuLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLog.Click
        ' Present content of log
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLogPath As String
        Dim sErrorString As String
        Dim sText As String
        Dim aLines() As String
        Dim lUpper As Integer
        Dim l As Integer
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sMySQL As String
        Dim frmVisma_LogView As New frmVisma_LogView

        On Error GoTo localerror

        sErrorString = "Before finding logfile"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        ' Find logpath from EftInf or Windows programdata
        sLogPath = Visma_RetrieveProgramDataBabelBank(oVismaDal, sSystemDatabase) & "\Log.txt"
        oVismaDal.Close()
        oVismaDal = Nothing


        sErrorString = "Before set oFS="
        oFs = New Scripting.FileSystemObject
        If oFs.FileExists(sLogPath) Then
            sErrorString = "Logfile found"
            oFile = oFs.OpenTextFile(sLogPath, Scripting.IOMode.ForReading)
            sErrorString = "Before reading file"
            sText = oFile.ReadAll
            ' redo so we can have newest logginfo on top;
            aLines = Split(sText, vbNewLine)
            lUpper = UBound(aLines)
            sText = ""
            For l = lUpper To 0 Step -1
                'listbox1.additem slines(i)
                sText = sText & aLines(l) & vbCrLf
                If Len(sText) > 65000 Then
                    ' can't present more than 65365 chars in txtbox
                    Exit For
                End If
            Next l
            frmVisma_LogView.txtLog.Text = sText
            frmVisma_LogView.txtLogFile.Text = sLogPath
            oFile.Close()
            oFile = Nothing
            oFs = Nothing
        Else
            ' logpath not found
            frmVisma_LogView.txtLog.Text = ""
        End If

        frmVisma_LogView.txtLogFile.Text = sLogPath
        frmVisma_LogView.txtLogFile.Select(frmVisma_LogView.txtLogFile.Text.Length, 0)
        sErrorString = "Before frmVisma_Logview.Show"
        frmVisma_LogView.ShowDialog()
        frmVisma_LogView = Nothing

        Exit Sub

localerror:
        oFile.Close()
        oFile = Nothing
        oFs = Nothing

        If Not Err() Is Nothing Then
            Err.Raise(30002, "mnuLog_Click", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30002, "mnuLog_Click", sErrorString)
        End If

    End Sub
    Private Sub mnuUpgradeViews_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpgradeViews.Click
        ' Upgrade the BabelBank views

        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim oVismaDal2 As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim bOK As Boolean

        On Error GoTo Err_Renamed

        If MsgBox(LRS(35225), MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then
            ' Bekreft at du �nsker � oppgradere BabelBank view

            sErrorString = "Starts ConnectToVismaBusinessDB"
            oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
            oVismaDal2 = ConnectToVismaBusinessDB(False, sErrorString, True)

            sErrorString = "CreateNNFrmViews"
            bOK = Visma_CheckIfBBViewsExists(oVismaDal, oVismaDal2, sSystemDatabase, sBBDatabase, sUID, sPWD, sBBDatabase, True, Me)
            sErrorString = "CreateBBSystemdatabaseViews"
            bOK = Visma_CreateBBSystemdatabaseViews(sUID, sPWD, sSystemDatabase)
            If bOK Then
                'MsgBox "Vellykket oppgradering av BabelBank views", vbOKOnly, "BabelBank"
                MsgBox(LRS(35185), MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)")
            End If
            oVismaDal.Close()
            oVismaDal = Nothing
            ' hide progress
            Me.prgActionProgress.Visible = False
        End If

        Exit Sub

Err_Renamed:

        oVismaDal.Close()
        oVismaDal = Nothing

        If Not Err() Is Nothing Then
            Err.Raise(30009, "mnuUpdateViews_Click", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30009, "mnuUpdateViews_Click", sErrorString)
        End If

    End Sub
    Private Sub cmbProfileGroup_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbProfileGroup.SelectedIndexChanged
        Dim bContinue As Boolean
        bContinue = True
        'If sMode = "NORMAL" Then
        '    If bChanged Then
        '        ' If changes, then ask first if we will save for previous profilegroup;
        '        If MsgBox(LRS(48020), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
        '            cmdOK_Click(cmdOK, New System.EventArgs())
        '            bContinue = True
        '        Else
        '            bContinue = False
        '        End If
        '    End If
        'End If

        If bContinue Then
            ' select different profilegroup
            If iSelectedProfileGroupIndex > -1 Then
                iSelectedProfileGroupIndex = cmbProfileGroup.SelectedIndex 'aProfileGroups(0, cmbProfileGroup.ListIndex)
                iProfileGroupID = aProfileGroups(0, iSelectedProfileGroupIndex)
            End If
            'If sMode = "NORMAL" Then
            ' must refill grids for this profilegroup
            ' Show both export and import grids
            sprExportImportFill(Me.gridImp, "I")
            If Me.gridExp.RowCount > 0 Then
                Me.gridExp.Rows(0).Cells(4).Selected = True
                iProfileID = CShort(Me.gridExp.Rows(0).Cells(4).Value)
            End If

            'Else
            sprExportImportFill(Me.gridExp, "E")
            If Me.gridExp.RowCount > 0 Then
                Me.gridExp.Rows(0).Cells(4).Selected = True
                iProfileID = CShort(Me.gridExp.Rows(0).Cells(4).Value)
            End If
            'End If
        End If
        ' keep focus on cmbProfileGroup
        Me.cmbProfileGroup.Focus()
    End Sub
    'Private Sub cmbProfileGroup_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cmbProfileGroup.KeyDown
    'Dim KeyCode As Short = eventArgs.KeyCode
    'Dim Shift As Short = eventArgs.KeyData \ &H10000
    'Dim sText As String
    'Dim i As Short

    'If sMode = "NORMAL" Then

    'Else

    '    If KeyCode = System.Windows.Forms.Keys.Return Then
    '        ' confirm edit, and add to list
    '        sText = Me.cmbProfileGroup.Text
    '        aProfileGroups(1, iSelectedProfileGroupIndex) = sText
    '        ' then "refill" the combobox
    '        Me.cmbProfileGroup.Items.Clear()
    '        For i = 0 To UBound(aProfileGroupsStatus)
    '            ' Check status, if profilegroup is deleted or not
    '            If aProfileGroupsStatus(i) <> -1 Then
    '                Me.cmbProfileGroup.Items.Add(aProfileGroups(1, i))
    '            End If
    '        Next i
    '        Me.cmbProfileGroup.SelectedIndex = 0 'iSelectedProfileGroupIndex
    '        Me.cmbProfileGroup.Text = "" ' turn off selection to mark that we have edited
    '        Me.cmbProfileGroup.SelectionStart = 0
    '        Me.cmbProfileGroup.Refresh()
    '        bChanged = True
    '        Me.cmdOK.Enabled = True
    '        bNewMode = False ' mark that we have already updated cmbProfileGroup
    '    End If
    'End If
    'End Sub
    'Private Sub cmbProfileGroup_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbProfileGroup.Leave
    '    ' added 11.02.2014, so we do not need to press Enter at the end of editing cmbProfileGroup
    '    Dim sText As String
    '    Dim i As Short

    '    If sMode = "PROFILEGROUP" Then
    '        If bChanged Then
    '            sText = Me.cmbProfileGroup.Text
    '            If Not EmptyString(sText) Then
    '                aProfileGroups(1, iSelectedProfileGroupIndex) = sText
    '                ' then "refill" the combobox
    '                Me.cmbProfileGroup.Items.Clear()
    '                For i = 0 To UBound(aProfileGroupsStatus)
    '                    ' Check status, if profilegroup is deleted or not
    '                    If aProfileGroupsStatus(i) <> -1 Then
    '                        Me.cmbProfileGroup.Items.Add(aProfileGroups(1, i))
    '                    End If
    '                Next i
    '                Me.cmbProfileGroup.SelectedIndex = 0 'iSelectedProfileGroupIndex
    '                '''Me.cmbProfileGroup.Text = "" ' turn off selection to mark that we have edited
    '                Me.cmbProfileGroup.SelectionStart = 0
    '                Me.cmbProfileGroup.Refresh()
    '                bNewMode = False ' mark that we have already updated cmbProfileGroup
    '            End If
    '        End If
    '    End If
    'End Sub
    'Private Sub cmbProfileGroup_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbProfileGroup.TextChanged
    '    Me.cmdOK.Enabled = True
    '    bNewMode = False ' mark that we have already updated cmbProfileGroup
    'End Sub

    ' The up-down arrow keys must be allowed to enable
    ' mouse-less users to scroll the list.
    ' Unlock the combobox again on key up events:
    'Private Sub cmbProfileGroup_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cmbProfileGroup.KeyUp
    '    Dim KeyCode As Short = eventArgs.KeyCode
    '    Dim Shift As Short = eventArgs.KeyData \ &H10000
    '    ' VISMA_TODO
    '    'cmbProfileGroup.Locked = False
    '    'bChanged = True
    '    'Me.cmdOK.Visible = True
    'End Sub

    Private Sub mnuProfileGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProfileGroup.Click
        Dim frmVisma_ShowProfileGroups As New frmVisma_ShowProfileGroups
        frmVisma_ShowProfileGroups.ShowDialog()
        frmVisma_ShowProfileGroups.Close()
        frmVisma_ShowProfileGroups = Nothing
        ' refill cmbProfileGroup
        cmbProfileGroupFill()

    End Sub
    Private Sub sprExportImportCreate()
        Dim x As Boolean
        Dim sErrorString As String
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim imgColumn As DataGridViewImageColumn
        Dim ComboColumn As DataGridViewComboBoxColumn

        On Error GoTo Err_Renamed

        With Me.gridExp
            .Rows.Clear()  ' empty content of gridExpImp
            .Columns.Clear()

            If .ColumnCount = 0 Then
                '.Height = 100

                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.CellSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = True  ' can't edit grid in frmVisma_Setup
            End If

            ' Col headings and widths:
            ' 1 Profilename
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(12)
            .Columns.Add(txtColumn)

            ' 2 Filename
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = Replace(LRS(60382), ":", "") 'Filename
            txtColumn.Width = WidthFromSpreadToGrid(22)
            .Columns.Add(txtColumn)

            ' 3 Image icon (+)
            ' ----------------
            imgColumn = New DataGridViewImageColumn
            imgColumn.HeaderText = " "
            imgColumn.Width = WidthFromSpreadToGrid(1)
            imgColumn.Resizable = False
            imgColumn.DefaultCellStyle.NullValue = Nothing
            .Columns.Add(imgColumn)

            ' 4 Format - Combobox
            ' -------------------
            ComboColumn = New DataGridViewComboBoxColumn
            ComboColumn.Width = WidthFromSpreadToGrid(12)
            .Columns.Add(ComboColumn)

            ' 5 Hidden col
            '--------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

        End With

        With Me.gridImp
            .Rows.Clear()  ' empty content of gridExpImp
            .Columns.Clear()

            If .ColumnCount = 0 Then
                '.Height = 100

                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.CellSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If

            ' Col headings and widths:
            ' 1 Profilename
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(12)
            .Columns.Add(txtColumn)

            ' 2 Filename
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = Replace(LRS(60382), ":", "") 'Filename
            txtColumn.Width = WidthFromSpreadToGrid(22)
            .Columns.Add(txtColumn)

            ' 3 Image icon (+)
            ' ----------------
            imgColumn = New DataGridViewImageColumn
            imgColumn.HeaderText = " "
            imgColumn.Width = WidthFromSpreadToGrid(1)
            imgColumn.Resizable = False
            imgColumn.DefaultCellStyle.NullValue = Nothing
            .Columns.Add(imgColumn)

            ' 4 Format - Combobox
            ' -------------------
            ComboColumn = New DataGridViewComboBoxColumn
            ComboColumn.Width = WidthFromSpreadToGrid(12)
            .Columns.Add(ComboColumn)

            ' 5 Hidden col
            '--------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

        End With

        Exit Sub

Err_Renamed:

        If Not Err() Is Nothing Then
            Err.Raise(30011, "sprExportImportCreate", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30011, "sprExportImportCreate", sErrorString)
        End If

    End Sub
    Private Sub sprExportImportFill(ByVal grid As DataGridView, ByRef sType As String)
        ' Fill grids from table bbProfile
        ' -------------------------------
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim aFormats(,) As String
        Dim sSelectedFormatName As String
        Dim i As Short
        Dim sText As String
        Dim ComboCell As DataGridViewComboBoxCell
        Dim myRow As DataGridViewRow

        On Error GoTo VismaFillError

        If iSelectedProfileGroupIndex > -1 Then
            'sprExportImportCreate()
            bInit = True
            sErrorString = "ConnectToVismaBusinessDB"
            oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
            sErrorString = "Visma_Formats"
            ' function which returns a list of valid "Visma" formats
            aFormats = Visma_Formats(sType, oVismaDal, sBBDatabase) ' E=Export, I=Import
            If sType = "E" Then
                ' Export
                sMySQL = "SELECT ProfileName,Path_File, FormatName, ProfileID, P.FormatID  FROM " & sBBDatabase & ".dbo.bbProfile P, " & sBBDatabase & ".dbo.bbFormat F WHERE Export=1 AND P.FormatID = F.FormatID AND P.ProfileGroupID =" & aProfileGroups(0, CInt(Str(iSelectedProfileGroupIndex)))
            Else
                sMySQL = "SELECT ProfileName, Path_File, FormatName, ProfileID, P.FormatID FROM " & sBBDatabase & ".dbo.bbProfile P, " & sBBDatabase & ".dbo.bbFormat F WHERE Export=0 AND P.FormatID = F.FormatID AND P.ProfileGroupID =" & aProfileGroups(0, CInt(Str(iSelectedProfileGroupIndex)))
            End If
            '        rsVisma.Open(sMySQL, oVismaCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

            sErrorString = "sprExportImportFill"
            With grid

                ' first clear previous content
                .Rows.Clear()

                ' set headers
                If sType = "E" Then
                    .Columns(0).HeaderText = Replace(LRS(60355), ":", "") 'sendprofile
                Else
                    .Columns(0).HeaderText = Replace(LRS(60356), ":", "") 'returnprofile
                End If
                If sType = "E" Then
                    .Columns(3).HeaderText = Replace(LRS(60369), ":", "") 'ExportFormat
                Else
                    .Columns(3).HeaderText = Replace(LRS(60358), ":", "") 'ImportFormat
                End If


                oVismaDal.SQL = sMySQL
                If oVismaDal.Reader_Execute() Then
                    If oVismaDal.Reader_HasRows = True Then
                        Do While oVismaDal.Reader_ReadRecord

                            myRow = New DataGridViewRow
                            myRow.CreateCells(Me.gridExp)

                            ' Profilename
                            ' -----------
                            myRow.Cells(0).Value = oVismaDal.Reader_GetString("ProfileName").Trim

                            ' Filename
                            ' --------
                            myRow.Cells(1).Value = oVismaDal.Reader_GetString("Path_File").Trim

                            ' Icon for folder/file
                            ' --------------------
                            myRow.Cells(2).Value = frmMATCH_Manual.imgPlus.Image

                            ' Format, combobox
                            ' ----------------
                            sText = ""
                            ComboCell = New DataGridViewComboBoxCell
                            'ComboCell = .Columns.Item(3).Clone
                            'ComboColumn.MaxDropDownItems = 0
                            ComboCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                            'ComboCell.Items.Clear()
                            For i = 0 To UBound(aFormats, 2)
                                ComboCell.Items.Add(aFormats(0, i).Trim)
                                ' Which one is the selected format?
                                'If aFormats(0, i) = oVismaDal.Reader_GetString("FormatName").Trim Then
                                'ComboColumn.Selected = i
                                'ComboCell.DefaultCellStyle.NullValue = ComboCell.Items(i)
                                'End If
                            Next i
                            ComboCell.Value = oVismaDal.Reader_GetString("FormatName").Trim  ' set selected format as default
                            'ComboColumn.ReadOnly = True
                            myRow.Cells(3) = ComboCell


                            ' Hidden col, ProfileID
                            ' ---------------------
                            myRow.Cells(4).Value = oVismaDal.Reader_GetString("ProfileID")

                            .Rows.Add(myRow)

                        Loop
                        .Rows(0).Cells(0).Selected = True

                    End If
                End If
            End With

            oVismaDal.Close()
            oVismaDal = Nothing

            Me.gridExp.Visible = True
            Me.gridExp.Focus()
            bInit = False
        End If

        Exit Sub

VismaFillError:
        oVismaDal.Close()
        oVismaDal = Nothing

        If Not Err() Is Nothing Then
            Err.Raise(30005, "sprExportImportFill", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30005, "sprExportImportFill", sErrorString)
        End If

    End Sub
    'Private Sub cmdEdit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEdit.Click

    Public Sub cmbProfileGroupFill()
        ' Fill combobox up from table bbProfileGroup
        Dim oVismaDAL As vbBabel.DAL
        Dim oVismaDAL2 As vbBabel.DAL
        Dim sErrorString As String
        Dim sMySQL As String
        Dim i As Short
        Dim sCXDatabase As String

        On Error GoTo VismaFillError

        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDAL = ConnectToVismaBusinessDB(False, sErrorString, True)

        sErrorString = "Visma_BabelBankProfileGroupExists"
        'Check if The BabelBank database exists in Visma, and that the ProfileGroup table contain at least 1 record

        If Visma_BabelBankProfileGroupExists(oVismaDAL, sBBDatabase, sSystemDatabase, sUser) Then
            'OK, the BB setup is present

            '13.07.2016 - Moved this code outside the IF.
            'Then the database will be upgraded even if there is no ProfileGroup
            ' -------------------------------------
            ' Upgrade BabelBank database if needed;
            ' -------------------------------------
            'Visma_CheckVersion(sUser, oVismaDAL, sBBDatabase)

        Else
            'It is either a new installation or the conversion has not been run
            ' Visma_TODO
            'rsVisma = New ADODB.Recordset

            'First find the name of the ConvertX database, and if it exists

            sErrorString = "Check if ConvertX database exists"
            'Retrieve the ConvertX database
            sCXDatabase = ""
            If Visma_FindCXDatabase(oVismaDAL, sSystemDatabase, sCXDatabase) Then
                '    'OK, the ConvertX database exists

                sMySQL = "SELECT * FROM " & sCXDatabase & ".dbo.cxProfile"
                oVismaDAL.SQL = sMySQL
                If oVismaDAL.Reader_Execute() Then
                    If oVismaDAL.Reader_HasRows Then
                        oVismaDAL.Reader_ReadRecord()
                        'The conversion has not been run, do it!

                        sErrorString = "Convert from the ConvertX database"
                        If Visma_ConvertFromCXtoBB(oVismaDAL, sUID, sPWD, sBBDatabase, sCXDatabase, sUser, sInstance) Then
                            MsgBox(LRS(35172), MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                        Else
                            MsgBox(LRS(35173), MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                            'TODO VISMA (old): Add a form to say the conversion went down the drain!
                        End If
                    Else
                        'OK, it's a new installation. Just continue and BB will ask later about setting up the system
                    End If
                End If
            Else
                'No CX database, just continue and BB wil ask later about setting up the system
            End If

        End If

        '13.07.2016 - Moved this code here.
        ' -------------------------------------
        ' Upgrade BabelBank database if needed;
        ' -------------------------------------
        Visma_CheckVersion(sUser, oVismaDAL, sBBDatabase, , Me)

        oVismaDAL2 = ConnectToVismaBusinessDB(False, sErrorString, True)

        If Visma_CheckIfBBViewsExists(oVismaDAL, oVismaDAL2, sSystemDatabase, sBBDatabase, sUID, sPWD, sInstance, False, Me) Then
            '    ' b�r ikke logge dette, kommer hit hver gang, selv om vi ikke lager views
        Else
            If bLog Then
                oBabelLog.Heading = "Visma Payment (BabelBank) error: "
                'oBabelLog.AddLogEvent "Error when creating BabelBank views.", vbLogEventTypeInformation
                oBabelLog.AddLogEvent(LRS(35227), System.Diagnostics.TraceEventType.Error)
            End If
        End If

        Me.cmbProfileGroup.Items.Clear()
        i = -1

        sMySQL = "SELECT * FROM " & sBBDatabase & ".dbo.bbProfileGroup ORDER BY Name"
        sErrorString = "Fill arrays and cmdProfileGroup"

        ' Fill into aProfileGroups
        oVismaDAL.SQL = sMySQL
        If oVismaDAL.Reader_Execute() Then
            If oVismaDAL.Reader_HasRows = True Then
                'oVismaDAL.Reader_ReadRecord()

                Do While oVismaDAL.Reader_ReadRecord
                    i = i + 1
                    ReDim Preserve aProfileGroups(1, i)
                    ReDim Preserve aProfileGroupsStatus(i)
                    aProfileGroups(0, i) = oVismaDAL.Reader_GetString("ProfileGroupID")
                    aProfileGroups(1, i) = oVismaDAL.Reader_GetString("Name")
                    aProfileGroupsStatus(i) = oVismaDAL.Reader_GetString("ProfileGroupID")
                    ' Fill cmbProfileGroups
                    Me.cmbProfileGroup.Items.Add(oVismaDAL.Reader_GetString("Name"))
                Loop
                iSelectedProfileGroupIndex = 0
                Me.cmbProfileGroup.SelectedIndex = 0
            End If

        End If
        oVismaDAL.Close()
        oVismaDAL = Nothing

        If Not oVismaDAL2 Is Nothing Then
            oVismaDAL2.Close()
            oVismaDAL2 = Nothing
        End If

        Exit Sub

VismaFillError:
        If Not oVismaDAL Is Nothing Then
            oVismaDAL.Close()
            oVismaDAL = Nothing
        End If

        If Not oVismaDAL2 Is Nothing Then
            oVismaDAL2.Close()
            oVismaDAL2 = Nothing
        End If

        If Not Err() Is Nothing Then
            Err.Raise(30006, "cmbProfileGroupFill", sErrorString & vbCrLf & Trim(Str(Err.Number)) & ": " & Err.Description)
        Else
            Err.Raise(30006, "cmbProfileGroupFill", sErrorString)
        End If
    End Sub
    Public Sub SetbLog(ByRef bUselog As Boolean)
        bLog = bUselog
    End Sub
    Public Sub SetLogObject(ByRef oLog As vbLog.vbLogging)
        oBabelLog = oLog
    End Sub

    ' Languageselections
    Public Sub mnuNorwegian_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        LanguageSelect(("Norsk"))
    End Sub
    Public Sub mnuDanish_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        LanguageSelect(("Dansk"))
    End Sub
    Public Sub mnuEnglish_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        LanguageSelect(("English"))
    End Sub
    Public Sub mnuSwedish_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        LanguageSelect(("Svenska"))
    End Sub
    Private Sub LanguageSelect(ByRef sLanguage As String)
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sString As String
        Dim sCode As String

        Select Case sLanguage
            Case "Norsk"
                sString = "Norsk spr�k valgt."
                sCode = "414"
            Case "English"
                sString = "English language selected."
                sCode = "809"
            Case "Dansk"
                sString = "Dansk spr�g valgt."
                sCode = "406"
            Case "Svenska"
                sString = "Svensk spr�k vald."
                sCode = "41d"
        End Select

        MsgBox(sString & vbCrLf & "Please restart to update language! /" & vbCrLf & "Vennligst lukk og start igjen for � oppdatere spr�k !", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") 'TODO LRS(Me)
        ' change selections in file Language.txt
        ' No = 414
        'oFs = CreateObject ("Scripting.FileSystemObject")
        oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
        If oFs.FileExists(My.Application.Info.DirectoryPath & "\language.txt") Then
            ' file found, delete
            oFs.DeleteFile((My.Application.Info.DirectoryPath & "\language.txt"))
        End If
        oFile = oFs.OpenTextFile(My.Application.Info.DirectoryPath & "\language.txt", Scripting.IOMode.ForWriting, True)
        oFile.Write(sCode)
        oFile.Close()
        oFile = Nothing
        oFs = Nothing

    End Sub
    
    Private Sub DanskToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DanskToolStripMenuItem.Click
        LanguageSelect("Dansk")
    End Sub
    Private Sub EnglishToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnglishToolStripMenuItem.Click
        LanguageSelect("English")
    End Sub
    Private Sub NorskToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NorskToolStripMenuItem.Click
        LanguageSelect("Norsk")
    End Sub
    Private Sub SvenskaToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SvenskaToolStripMenuItem.Click
        LanguageSelect("Svenska")
    End Sub
    Private Sub mnuExportFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportFiles.Click
        Dim frmVisma_GridSetup As New frmVisma_GridSetup
        'ControlsVisible(True)
        'iSelectedProfileGroupIndex = -1

        'cmbProfileGroupFill() - hvorfor denne?
        sCurrentSpread = "E"

        ' select current, or first, ProfileGroup
        'If Me.cmbProfileGroup.Items.Count > 0 Then
        '    If iSelectedProfileGroupIndex > -1 Then
        '        ' position to last used iSelectedProfileGroupIndex = 1
        '        Me.cmbProfileGroup.SelectedIndex = iSelectedProfileGroupIndex
        '    Else
        '        iSelectedProfileGroupIndex = 0
        '        Me.cmbProfileGroup.SelectedIndex = 0
        '    End If
        'End If
        'Me.cmdOK.Enabled = False
        ' call frmVisma_GridSetup
        frmVisma_GridSetup.PassType("E")
        frmVisma_GridSetup.PassProfileGroupID(iProfilegroupID)
        frmVisma_GridSetup.PassUser(sUser)
        frmVisma_GridSetup.PassbbDatabase(sBBDatabase)
        frmVisma_GridSetup.ShowDialog(Me)
        frmVisma_GridSetup.Close()
        frmVisma_GridSetup = Nothing

        ' redraw grids to reflect changes done in frmGrid_Setup
        sprExportImportFill(Me.gridExp, "E")


    End Sub
    Private Sub mnuImportFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportFiles.Click
        Dim frmVisma_GridSetup As New frmVisma_GridSetup
        sCurrentSpread = "I"

        ' call frmVisma_GridSetup
        frmVisma_GridSetup.PassType("I")
        frmVisma_GridSetup.PassProfileGroupID(iProfileGroupID)
        frmVisma_GridSetup.PassUser(sUser)
        frmVisma_GridSetup.PassbbDatabase(sBBDatabase)
        frmVisma_GridSetup.ShowDialog(Me)
        frmVisma_GridSetup.Close()
        frmVisma_GridSetup = Nothing

        ' redraw grids to reflect changes done in frmGrid_Setup
        sprExportImportFill(Me.gridImp, "I")

    End Sub


    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        Dim frmVisma_About As New frmVisma_About
        Dim stmp As String = ""
        frmVisma_About.ShowDialog()

        'VB6.ShowForm(frmVisma_About, 1, Me)
        frmVisma_About = Nothing

    End Sub
    Private Sub mnuReportSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportSetup.Click
        Dim frmVisma_ReportSetup As New frmVisma_ReportSetup
        frmVisma_ReportSetup.ShowDialog()
        frmVisma_ReportSetup = Nothing

    End Sub

    Private Sub mnuClients_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClients.Click
        Dim frmVisma_ShowClients As New frmVisma_ShowClients
        frmVisma_ShowClients.ShowDialog()
        frmVisma_ShowClients.Close()
        frmVisma_ShowClients = Nothing
    End Sub

    Private Sub gridExp_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridExp.CellClick, gridExp.DoubleClick
        ' kall opp eget bilde for redigering av SendeProfiler ved klikk i kolonne 1 eller 2
        'If e.ColumnIndex = 1 Or e.ColumnIndex = 2 Or e.ColumnIndex = 3 Then
        mnuExportFiles_Click(Nothing, Nothing)
        'End If

    End Sub
    Private Sub gridImp_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridImp.CellClick, gridImp.DoubleClick
        ' kall opp eget bilde for redigering av ReturProfiler ved klikk i kolonne 1 eller 2
        'If e.ColumnIndex = 1 Or e.ColumnIndex = 2 Or e.ColumnIndex = 3 Then
        mnuImportFiles_Click(Nothing, Nothing)
        'End If

    End Sub

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        End
    End Sub
	Private Sub gridExp_Click(sender As Object, e As EventArgs) Handles gridExp.Click
		' 18.11.2021 - denne pausen under ser utrolig nok ut til � l�se problem med feil etter dobbelklikk i "Whitespace" utenfor gjeldende rader
		Pause(0.1)
	End Sub
	Private Sub gridImp_Click(sender As Object, e As EventArgs) Handles gridImp.Click
		' 18.11.2021 - denne pausen under ser utrolig nok ut til � l�se problem med feil etter dobbelklikk i "Whitespace" utenfor gjeldende rader
		Pause(0.1)
	End Sub
End Class