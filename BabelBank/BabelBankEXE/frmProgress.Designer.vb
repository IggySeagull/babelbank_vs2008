<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmProgress
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents prgActionProsess3 As System.Windows.Forms.ProgressBar
	Public WithEvents prgActionProsess2 As System.Windows.Forms.ProgressBar
	Public WithEvents prgActionProgress As System.Windows.Forms.ProgressBar
	Public WithEvents picPuzle1518 As System.Windows.Forms.PictureBox
	Public WithEvents lblPercent3 As System.Windows.Forms.Label
	Public WithEvents lblProsess3 As System.Windows.Forms.Label
	Public WithEvents lblProsess2 As System.Windows.Forms.Label
	Public WithEvents lblAction2 As System.Windows.Forms.Label
	Public WithEvents lblAction1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProgress))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.prgActionProsess3 = New System.Windows.Forms.ProgressBar
        Me.prgActionProsess2 = New System.Windows.Forms.ProgressBar
        Me.prgActionProgress = New System.Windows.Forms.ProgressBar
        Me.picPuzle1518 = New System.Windows.Forms.PictureBox
        Me.lblPercent3 = New System.Windows.Forms.Label
        Me.lblProsess3 = New System.Windows.Forms.Label
        Me.lblProsess2 = New System.Windows.Forms.Label
        Me.lblAction2 = New System.Windows.Forms.Label
        Me.lblAction1 = New System.Windows.Forms.Label
        CType(Me.picPuzle1518, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'prgActionProsess3
        '
        Me.prgActionProsess3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.prgActionProsess3.Location = New System.Drawing.Point(201, 231)
        Me.prgActionProsess3.Name = "prgActionProsess3"
        Me.prgActionProsess3.Size = New System.Drawing.Size(377, 17)
        Me.prgActionProsess3.TabIndex = 5
        Me.prgActionProsess3.Visible = False
        '
        'prgActionProsess2
        '
        Me.prgActionProsess2.Location = New System.Drawing.Point(201, 183)
        Me.prgActionProsess2.Name = "prgActionProsess2"
        Me.prgActionProsess2.Size = New System.Drawing.Size(377, 17)
        Me.prgActionProsess2.TabIndex = 4
        Me.prgActionProsess2.Visible = False
        '
        'prgActionProgress
        '
        Me.prgActionProgress.Location = New System.Drawing.Point(201, 135)
        Me.prgActionProgress.Name = "prgActionProgress"
        Me.prgActionProgress.Size = New System.Drawing.Size(377, 17)
        Me.prgActionProgress.TabIndex = 3
        '
        'picPuzle1518
        '
        Me.picPuzle1518.BackColor = System.Drawing.SystemColors.Control
        Me.picPuzle1518.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.picPuzle1518.Cursor = System.Windows.Forms.Cursors.Default
        Me.picPuzle1518.ForeColor = System.Drawing.SystemColors.ControlText
        Me.picPuzle1518.Image = CType(resources.GetObject("picPuzle1518.Image"), System.Drawing.Image)
        Me.picPuzle1518.Location = New System.Drawing.Point(13, 12)
        Me.picPuzle1518.Name = "picPuzle1518"
        Me.picPuzle1518.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picPuzle1518.Size = New System.Drawing.Size(160, 124)
        Me.picPuzle1518.TabIndex = 0
        Me.picPuzle1518.TabStop = False
        Me.picPuzle1518.Visible = False
        '
        'lblPercent3
        '
        Me.lblPercent3.BackColor = System.Drawing.SystemColors.Control
        Me.lblPercent3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPercent3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPercent3.Location = New System.Drawing.Point(331, 207)
        Me.lblPercent3.Name = "lblPercent3"
        Me.lblPercent3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPercent3.Size = New System.Drawing.Size(81, 25)
        Me.lblPercent3.TabIndex = 8
        Me.lblPercent3.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPercent3.Visible = False
        '
        'lblProsess3
        '
        Me.lblProsess3.BackColor = System.Drawing.SystemColors.Control
        Me.lblProsess3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProsess3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProsess3.Location = New System.Drawing.Point(201, 207)
        Me.lblProsess3.Name = "lblProsess3"
        Me.lblProsess3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProsess3.Size = New System.Drawing.Size(119, 25)
        Me.lblProsess3.TabIndex = 7
        Me.lblProsess3.Visible = False
        '
        'lblProsess2
        '
        Me.lblProsess2.BackColor = System.Drawing.SystemColors.Control
        Me.lblProsess2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProsess2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProsess2.Location = New System.Drawing.Point(201, 159)
        Me.lblProsess2.Name = "lblProsess2"
        Me.lblProsess2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProsess2.Size = New System.Drawing.Size(378, 21)
        Me.lblProsess2.TabIndex = 6
        Me.lblProsess2.Visible = False
        '
        'lblAction2
        '
        Me.lblAction2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAction2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAction2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAction2.Location = New System.Drawing.Point(202, 78)
        Me.lblAction2.Name = "lblAction2"
        Me.lblAction2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAction2.Size = New System.Drawing.Size(377, 48)
        Me.lblAction2.TabIndex = 2
        '
        'lblAction1
        '
        Me.lblAction1.BackColor = System.Drawing.SystemColors.Control
        Me.lblAction1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAction1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAction1.Location = New System.Drawing.Point(202, 44)
        Me.lblAction1.Name = "lblAction1"
        Me.lblAction1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAction1.Size = New System.Drawing.Size(377, 26)
        Me.lblAction1.TabIndex = 1
        Me.lblAction1.Text = "Action1"
        '
        'frmProgress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(598, 276)
        Me.Controls.Add(Me.prgActionProsess3)
        Me.Controls.Add(Me.prgActionProsess2)
        Me.Controls.Add(Me.prgActionProgress)
        Me.Controls.Add(Me.picPuzle1518)
        Me.Controls.Add(Me.lblPercent3)
        Me.Controls.Add(Me.lblProsess3)
        Me.Controls.Add(Me.lblProsess2)
        Me.Controls.Add(Me.lblAction2)
        Me.Controls.Add(Me.lblAction1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProgress"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "BabelBank"
        CType(Me.picPuzle1518, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
