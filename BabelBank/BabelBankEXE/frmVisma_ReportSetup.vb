Option Strict Off
Option Explicit On

Friend Class frmVisma_ReportSetup
	Inherits System.Windows.Forms.Form
	Private Declare Function DestroyCaret Lib "user32" () As Integer
	Private sUID As String
	Private sPWD As String
	Private sVBSys As String
	Private sUser As String
	Private sInstance As String
	Private sReportPath As String
	
    Private Sub cmdShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShow.Click
        ' Bring in a filebrowser for the reportpath
        ' Hent filnavn
        Dim sFilename As String = ""
        Dim aFArray() As String = Nothing

        'CommonDialogOpen.InitialDirectory = Me.txtReportPath.Text
        'CommonDialogOpen.Filter = "Apps (*.pdf)|*.pdf|All files (*.*)|*.*"
        'CommonDialogOpen.DefaultExt = "pdf"
        'CommonDialogOpen.Title = "Select File"
        'CommonDialogOpen.ShowDialog()

        'sFilename = CommonDialogOpen.FileName

        aFArray = Filenamepicker(frmStart.dlgFileNameOpen, False, "", Me.txtReportPath.Text, "Apps (*.pdf)|*.pdf|All files (*.*)|*.*")
        If Not aFArray Is Nothing Then
            sFilename = aFArray(0)
            If Not EmptyString(sFilename) Then
                ' open pdf-file
                ShellExecute(0, "OPEN", sFilename, "", "", 1)
            End If
        End If

    End Sub
    Private Sub frmVisma_ReportSetup_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'VB;/S;F9998;vbsys;system;sa;Visma123;
        'sUID = xDelim(Command, ";", 6, Chr(34))
        'sPWD = xDelim(Command, ";", 7, Chr(34))
        'sVBSys = xDelim(Command, ";", 4, Chr(34))
        'sUser = xDelim(Command, ";", 5, Chr(34))

        Dim oVismaCommandLine As New VismaCommandLine

        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sUser = oVismaCommandLine.User
        sVBSys = oVismaCommandLine.SystemDatabase
        sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing

        FormLRSCaptions(Me)
        FormVismaStyle(Me)

        RestoreReportPathFromDatabase()

    End Sub
    Private Sub RestoreReportPathFromDatabase()
        ' fetch reportpath from database
        ' Fill up from table bbSetup
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String = ""
        Dim sMySQL As String
        Dim i As Short
        Dim sSystemDatabase As String
        Dim oVismaCommandLine As New VismaCommandLine
        sSystemDatabase = oVismaCommandLine.SystemDatabase
        oVismaCommandLine = Nothing

        On Error GoTo VismaFillError

        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sErrorString = "Before SELECT"


        ' Find reportpath
        sMySQL = "SELECT * FROM BabelBank_" & sVBSys & ".dbo.bbSetup"
        oVismaDal.SQL = sMySQL
        sErrorString = "Find reportpath"

        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows = False Then
                ' No info info bbSetup table
                MsgBox("frmVisma_ReportSetup.RestoreReportPathFromDatabase: No info info bbSetup table", MsgBoxStyle.Information, "Visma Payment (BabelBank)") '"Found no formats in formattable"
            Else
                oVismaDal.Reader_ReadRecord()

                sReportPath = oVismaDal.Reader_GetString("ReportPath")
                ' if no reportpath from setup, then find in EftInf or windows programdata
                If EmptyString(sReportPath) Then
                    sReportPath = Visma_RetrieveProgramDataBabelBank(oVismaDal, sSystemDatabase)
                End If
                oVismaDal.Close()
                oVismaDal = Nothing
                Me.txtReportPath.Text = sReportPath
            End If
        End If


        Exit Sub
VismaFillError:
        oVismaDal.Close()
        oVismaDal = Nothing

        Err.Raise(30008, "frmVisma_ReportSetup.RestoreReportPathFromDatabase", sErrorString)


    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Hent filnavn
        Dim sPath As String = ""

        ' using a new function where we can pass a path
        sPath = BrowseForFilesOrFolders(sPath, Me, LRS(35212), True)

        ' If BrowseForFolderByPIDL returns empty path, then keep the old one;
        If Len(sPath) > 0 Then
            Me.txtReportPath.Text = sPath
            sReportPath = sPath
        End If

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        ' save reportpath to database
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String = ""
        Dim sMySQL As String
        Dim sBBDatabase As String
        Dim lRecordsAffected As Integer
        Dim bPathOK As Boolean

        On Error GoTo Err_Renamed
        bPathOK = True

        If Len(Me.txtReportPath.Text) = 0 Then
            bPathOK = False
        ElseIf Mid(Me.txtReportPath.Text, 2, 2) <> ":\" And Mid(Me.txtReportPath.Text, 1, 2) <> "\\" Then
            bPathOK = False
        End If

        If Not bPathOK Then
            If EmptyString((Me.txtReportPath.Text)) Then
                MsgBox(LRS(35230, "-"), MsgBoxStyle.Exclamation, LRS(35213)) ' Noe feil med rapportstien
                RestoreReportPathFromDatabase()
            Else
                MsgBox(LRS(35230, (Me.txtReportPath.Text)), MsgBoxStyle.Exclamation, LRS(35213)) ' Noe feil med rapportstien
            End If
        Else
            sBBDatabase = "BabelBank_" & sVBSys
            oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
            sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbSetup SET ReportPath = '" & Me.txtReportPath.Text & "', ChUsr = '" & sUser & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', '')"

            oVismaDal.TEST_UseCommitFromOutside = True
            oVismaDal.TEST_BeginTrans()
            oVismaDal.SQL = sMySQL
            If oVismaDal.ExecuteNonQuery Then
                If oVismaDal.RecordsAffected <> 1 Then

                    Err.Raise(35006, "frmVisma_ReportSetup, cmdOK_Click", "Error in updating reportpath" & vbCrLf & sMySQL)
                End If
                '			oVismaCon.CommitTrans()
                oVismaDal.TEST_CommitTrans()

                ' Clean up
                frmVisma_SelectProfile = Nothing
                oVismaDal.Close()
                oVismaDal = Nothing
            End If
        End If
        Me.Hide()

        Exit Sub

Err_Renamed:
        oVismaDal.TEST_Rollback()

        oVismaDal.Close()
        oVismaDal = Nothing

        If Not EmptyString(sMySQL) Then
            Err.Raise(Err.Number, "frmVisma_ReportSetup, cmdOK_Click", Err.Description & vbCrLf & "SQL: " & sMySQL)
        Else
            Err.Raise(Err.Number, "frmVisma_ReportSetup, cmdOK_Click", Err.Description)
        End If

    End Sub
    Private Sub frmVisma_ReportSetup_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        ' trap user clicking the X in upper right corner
        Cancel = True
        cmdClose_Click(cmdClose, New System.EventArgs())
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub

End Class