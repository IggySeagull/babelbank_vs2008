Option Strict Off
Option Explicit On

Friend Class frmSEBISO20022_SetupStructured
	Inherits System.Windows.Forms.Form
	Dim sCompany_ID As String
	Dim bSave As Boolean
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bSave = False
		Me.Hide()
		
	End Sub
	Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click
		bSave = True
		Me.Hide()
	End Sub
	Public Function Save() As Boolean
		Save = bSave
	End Function
	
	Private Sub frmSEBISO20022_SetupStructured_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

		sCompany_ID = "1"
		
		FormLRSCaptions(Me)
		FormvbStyle(Me, "dollar.jpg", 700)
		
	End Sub
End Class