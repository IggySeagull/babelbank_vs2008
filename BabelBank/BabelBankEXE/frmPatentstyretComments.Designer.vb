﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPatentstyretComments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.txtExternal = New System.Windows.Forms.TextBox
        Me.txtInternal = New System.Windows.Forms.TextBox
        Me.lblExternal = New System.Windows.Forms.Label
        Me.lblInternal = New System.Windows.Forms.Label
        Me.cmbExternal = New System.Windows.Forms.ComboBox
        Me.cmbInternal = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(331, 444)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 3
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 4
        Me.Cancel_Button.Text = "Avbryt"
        '
        'txtExternal
        '
        Me.txtExternal.Location = New System.Drawing.Point(25, 75)
        Me.txtExternal.Multiline = True
        Me.txtExternal.Name = "txtExternal"
        Me.txtExternal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtExternal.Size = New System.Drawing.Size(448, 148)
        Me.txtExternal.TabIndex = 1
        '
        'txtInternal
        '
        Me.txtInternal.Location = New System.Drawing.Point(29, 285)
        Me.txtInternal.Multiline = True
        Me.txtInternal.Name = "txtInternal"
        Me.txtInternal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInternal.Size = New System.Drawing.Size(448, 148)
        Me.txtInternal.TabIndex = 2
        '
        'lblExternal
        '
        Me.lblExternal.AutoSize = True
        Me.lblExternal.Location = New System.Drawing.Point(25, 51)
        Me.lblExternal.Name = "lblExternal"
        Me.lblExternal.Size = New System.Drawing.Size(72, 13)
        Me.lblExternal.TabIndex = 3
        Me.lblExternal.Text = "Velg standard"
        '
        'lblInternal
        '
        Me.lblInternal.AutoSize = True
        Me.lblInternal.Location = New System.Drawing.Point(26, 261)
        Me.lblInternal.Name = "lblInternal"
        Me.lblInternal.Size = New System.Drawing.Size(72, 13)
        Me.lblInternal.TabIndex = 4
        Me.lblInternal.Text = "Velg standard"
        '
        'cmbExternal
        '
        Me.cmbExternal.FormattingEnabled = True
        Me.cmbExternal.Location = New System.Drawing.Point(137, 47)
        Me.cmbExternal.Name = "cmbExternal"
        Me.cmbExternal.Size = New System.Drawing.Size(335, 21)
        Me.cmbExternal.TabIndex = 5
        '
        'cmbInternal
        '
        Me.cmbInternal.FormattingEnabled = True
        Me.cmbInternal.Location = New System.Drawing.Point(137, 258)
        Me.cmbInternal.Name = "cmbInternal"
        Me.cmbInternal.Size = New System.Drawing.Size(340, 21)
        Me.cmbInternal.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(168, 233)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 18)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Intern kommentar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(173, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(139, 18)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Ekstern kommentar"
        '
        'frmPatentstyretComments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(489, 485)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbInternal)
        Me.Controls.Add(Me.cmbExternal)
        Me.Controls.Add(Me.lblInternal)
        Me.Controls.Add(Me.lblExternal)
        Me.Controls.Add(Me.txtInternal)
        Me.Controls.Add(Me.txtExternal)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPatentstyretComments"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Kommentarer"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents txtExternal As System.Windows.Forms.TextBox
    Friend WithEvents txtInternal As System.Windows.Forms.TextBox
    Friend WithEvents lblExternal As System.Windows.Forms.Label
    Friend WithEvents lblInternal As System.Windows.Forms.Label
    Friend WithEvents cmbExternal As System.Windows.Forms.ComboBox
    Friend WithEvents cmbInternal As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
