Option Strict Off
Option Explicit On
Module Statistics
	
    Public Function Statistics_Update(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bStructured As Boolean, ByRef bOnlyManuallyMatched As Boolean, ByRef bSilent As Boolean, ByVal bUseLog As Boolean, ByVal oLog As vbLog.vbLogging, ByVal sSpecial As String) As Boolean
        'bStructured = True, bOnlyMatchedManual = False
        'Update only OCR and Autogiro (if they are not to be matched

        'bStructured = False, bOnlyMatchedManual = False
        'Update payments that are matched (also proposed) after automatic matching

        'bStructured = False, bOnlyMatchedManual = True
        'Update payments that are matched manually and unmatched items

        'How to read the database regarding partly matched:
        'They are found in the row which says how they are matched
        'The NoOfInvoices are thr total number of invoices which exists for payments that are matched, ProposedMatched or PartlyMatched
        '    - the important thing is that the total number may include invoices that aren't matched
        'To find the number of invoices that are not matched (in addition to the ones explicitly stated) do like this:
        '    NoOfInvoices - Proposed - Finally = Invoices not Matched
        'Totally not matched:
        ' Do the calculation above for all rules and then add the ones stated in OpenItem.


        Dim bReturnValue As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oClient As vbBabel.Client
        Dim sOldAccountNo As String
        Dim sOldClientNo As String
        Dim iCompany_ID As Short
        Dim iClient_ID As Short
        Dim sType, sOldType As String
        Dim iERP_ID As Short
        Dim sDate As String
        Dim nNoOfPayments As Double
        Dim nNoOfInvoices As Double
        Dim nTotalAmount As Double
        Dim nProposed As Double
        Dim nFinally As Double
        Dim nInError As Double
        Dim bChangeInVariables As Boolean
        Dim aStatistics(,) As String
        Dim bContinue As Boolean
        Dim bx As Boolean
        Dim bFirstPayment As Boolean
        Dim sHowMatched As String
        Dim bOpenItem As Boolean
        Dim sTypeOfUpdate As String
        Dim bAtLeastOneRecordUpdated As Boolean

        bReturnValue = True

        Try


            iCompany_ID = 0
            sOldClientNo = ""
            sOldAccountNo = ""
            sOldType = ""

            iERP_ID = -1
            sDate = "19900101"
            nNoOfPayments = 0
            nNoOfInvoices = 0
            nTotalAmount = 0
            nProposed = 0
            nFinally = 0
            nInError = 0
            bChangeInVariables = False
            bFirstPayment = True
            bOpenItem = False
            sHowMatched = ""
            sTypeOfUpdate = ""
            bAtLeastOneRecordUpdated = False

            If bStructured Then
                sTypeOfUpdate = "Struct"
            ElseIf bOnlyManuallyMatched Then
                sTypeOfUpdate = "Manual"
            Else
                sTypeOfUpdate = "Auto"
            End If

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'Test type of payment to see if we shall continue
                        bOpenItem = False
                        If bStructured Then
                            If IsOCR(oPayment.PayCode) Or IsAutogiro(oPayment.PayCode) Then
                                bContinue = True
                                sHowMatched = oPayment.MATCH_HowMatched
                            Else
                                bContinue = False
                            End If
                        Else
                            'This code is not correct for those who match OCR and Autogiro
                            If (Not IsOCR(oPayment.PayCode) And Not IsAutogiro(oPayment.PayCode)) Or sSpecial = "SG FINANS" Then '29.09.2020 - Added or sSpecial = "SG INANS"
                                bContinue = True
                                'Tetst if it's matched and if so if we shall calculate it in the statistics
                                If bOnlyManuallyMatched Then
                                    If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched Or oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched Then
                                        bOpenItem = True
                                        bContinue = True
                                    Else
                                        sHowMatched = oPayment.MATCH_HowMatched
                                        If InStrRev(sHowMatched, "Manuelt avstemt av:") > 0 Then
                                            sHowMatched = Mid(sHowMatched, InStrRev(sHowMatched, "Manuelt avstemt av:"))
                                            bContinue = True
                                        Else
                                            bContinue = False
                                        End If
                                    End If
                                Else
                                    If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched Then
                                        bOpenItem = True
                                        bContinue = True
                                    Else
                                        sHowMatched = oPayment.MATCH_HowMatched
                                    End If
                                End If
                            Else
                                bContinue = False
                            End If
                        End If

                        If bContinue Then
                            bAtLeastOneRecordUpdated = True
                            If oPayment.I_Account <> sOldAccountNo Then
                                oClient = FindClient(oBabelFile.VB_Profile, oBabelFile.VB_FileSetupID, oPayment.I_Account, oBabelFile.FilenameIn, oBabelFile.VB_FilenameInNo, False)
                            End If

                            'Must be activated when there are more than 1 company
                            '''                If sCompnayNo <> sOldCompanyNo Then
                            '''                    If sOldCompanyNo <> "" Then
                            '''                        'We must store the information on the old client
                            '''                        bChangeInVariables = True
                            '''                    Else
                            '''                        sOldCompanyNo = sCompanyNo
                            '''                    End If
                            '''                End If

                            If oClient Is Nothing Then
                                If bUseLog Then
                                    oLog.Heading = "BabelBank Warning: " & LRS(60018)
                                    oLog.AddLogEvent(LRS(15701, sOldAccountNo), System.Diagnostics.TraceEventType.Warning)
                                End If
                                If Not bSilent Then
                                    MsgBox(LRS(15701, sOldAccountNo), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, LRS(60018))
                                End If

                                Statistics_Update = False
                                Exit Function
                            End If

                            If oClient.ClientNo <> sOldClientNo Then
                                If sOldClientNo <> "" Then
                                    'We must store the information on the old client
                                    bChangeInVariables = True
                                Else
                                    sOldClientNo = oClient.ClientNo
                                End If
                            End If

                            If bOpenItem Then
                                sHowMatched = "Open item"
                                If sHowMatched <> sOldType Then
                                    If sOldType <> "" Then
                                        bChangeInVariables = True
                                    Else
                                        sOldType = sHowMatched
                                    End If
                                Else
                                    sOldType = sHowMatched
                                End If
                            ElseIf (Not IsOCR(oPayment.PayCode) And Not IsAutogiro(oPayment.PayCode)) Or sSpecial = "SG FINANS" Then '29.09.2020 - Added or sSpecial = "SG INANS"
                                If sHowMatched <> sOldType Then
                                    If sOldType <> "" Then
                                        bChangeInVariables = True
                                    Else
                                        sOldType = sHowMatched
                                    End If
                                Else
                                    sOldType = sHowMatched
                                End If
                            Else
                                If EmptyString(sOldType) Then
                                    If IsOCR(oPayment.PayCode) Then
                                        sOldType = "OCR"
                                    Else
                                        sOldType = "AG"
                                    End If
                                ElseIf IsOCR(oPayment.PayCode) And sOldType = "OCR" Then
                                    'OK, just add info
                                ElseIf IsAutogiro(oPayment.PayCode) And sOldType = "AG" Then
                                    'OK, just add info
                                Else
                                    bChangeInVariables = True
                                End If
                            End If

                            If bFirstPayment Then
                                bChangeInVariables = True
                            End If

                            If bChangeInVariables Then
                                If Not bFirstPayment Then
                                    bx = Statistics_UpdateArray(aStatistics, Trim(Str(iCompany_ID)), Trim(Str(iClient_ID)), sOldType, Trim(Str(iERP_ID)), sDate, Trim(Str(nNoOfPayments)), Trim(Str(nNoOfInvoices)), Trim(Str(nTotalAmount)), Trim(Str(nProposed)), Trim(Str(nFinally)), Trim(Str(nInError)), sTypeOfUpdate)
                                Else
                                    bFirstPayment = False
                                End If
                                iCompany_ID = oBabelFile.VB_Profile.Company_ID
                                iClient_ID = CShort(oClient.ClientNo) '16.10.2018 - Kjell - I think this may create an error, since ClinetNo is a string and the DB field is integer.
                                sType = ""
                                iERP_ID = -1
                                sDate = VB6.Format(Now, "YYYYMMDD")
                                nNoOfPayments = 0
                                nNoOfInvoices = 0
                                nTotalAmount = 0
                                nProposed = 0
                                nFinally = 0
                                nInError = 0

                                sOldClientNo = oClient.ClientNo
                                If IsOCR(oPayment.PayCode) And sSpecial <> "SG FINANS" Then '29.09.2020 - Added AND sSpecial <> "SG INANS"
                                    sOldType = "OCR"
                                ElseIf IsAutogiro(oPayment.PayCode) And sSpecial <> "SG FINANS" Then '29.09.2020 - Added AND sSpecial <> "SG INANS"
                                    sOldType = "AG"
                                Else
                                    sOldType = sHowMatched
                                End If
                                sOldAccountNo = oPayment.I_Account

                                bChangeInVariables = False

                            End If

                            If (IsOCR(oPayment.PayCode) Or IsAutogiro(oPayment.PayCode)) And sSpecial <> "SG FINANS" Then '29.09.2020 - Added AND sSpecial = "SG INANS"
                                nNoOfPayments = nNoOfPayments + 1
                                For Each oInvoice In oPayment.Invoices
                                    nNoOfInvoices = nNoOfInvoices + 1
                                    nTotalAmount = nTotalAmount + oInvoice.MON_InvoiceAmount
                                    nFinally = nFinally + 1
                                Next oInvoice
                            Else
                                nNoOfPayments = nNoOfPayments + 1
                                For Each oInvoice In oPayment.Invoices
                                    If oInvoice.MATCH_Final Then
                                        If Not bOnlyManuallyMatched Then
                                            nNoOfInvoices = nNoOfInvoices + 1
                                            nTotalAmount = nTotalAmount + oInvoice.MON_InvoiceAmount
                                            If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.ProposedMatched Then
                                                nProposed = nProposed + 1
                                            ElseIf oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched Then
                                                nFinally = nFinally + 1
                                            Else
                                                If oInvoice.MATCH_Matched Then
                                                    nFinally = nFinally + 1
                                                End If
                                            End If
                                        Else
                                            '05.01.2009 - added the next IF because no amount was stored on the manually matched payments
                                            ' This is done in the else clause
                                            If sHowMatched = "Open item" Then
                                                If Not oInvoice.MATCH_Matched Then
                                                    nNoOfInvoices = nNoOfInvoices + 1
                                                    nTotalAmount = nTotalAmount + oInvoice.MON_InvoiceAmount
                                                    If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.ProposedMatched Then
                                                        nProposed = nProposed + 1
                                                    ElseIf oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched Then
                                                        nFinally = nFinally + 1
                                                    Else
                                                        If oInvoice.MATCH_Matched Then
                                                            nFinally = nFinally + 1
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If oInvoice.MATCH_Matched Then
                                                    nNoOfInvoices = nNoOfInvoices + 1
                                                    nTotalAmount = nTotalAmount + oInvoice.MON_InvoiceAmount
                                                    If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.ProposedMatched Then
                                                        nProposed = nProposed + 1
                                                    ElseIf oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched Then
                                                        nFinally = nFinally + 1
                                                    Else
                                                        If oInvoice.MATCH_Matched Then
                                                            nFinally = nFinally + 1
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                Next oInvoice
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile


            If bAtLeastOneRecordUpdated Then
                bx = Statistics_UpdateArray(aStatistics, Trim(Str(iCompany_ID)), Trim(Str(iClient_ID)), sOldType, Trim(Str(iERP_ID)), sDate, Trim(Str(nNoOfPayments)), Trim(Str(nNoOfInvoices)), Trim(Str(nTotalAmount)), Trim(Str(nProposed)), Trim(Str(nFinally)), Trim(Str(nInError)), sTypeOfUpdate)

                bx = Statistics_StoreArray(aStatistics)
            End If


        Catch ex As Exception
            Throw New Exception("Statistics_Update" & vbCrLf & ex.Message)


        End Try

        Return bReturnValue


    End Function
    Private Function Statistics_UpdateArray(ByRef aStatistics(,) As String, ByRef sCompany_ID As String, ByRef sClient_ID As String, ByRef sType As String, ByRef sERP_ID As String, ByRef sDate As String, ByRef sNoOfPayments As String, ByRef sNoOfInvoices As String, ByRef sTotalAmount As String, ByRef sProposed As String, ByRef sFinally As String, ByRef sInError As String, ByRef sTypeOfUpdate As String) As Boolean
        Dim bAddNew As Boolean
        Dim lCounter As Integer
        Dim lY As Integer
        'The array consists of:
        '0 = Company_ID
        '1 = Client_ID
        '2 = Type
        '3 = ERP_ID
        '4 = Date
        '5 = NoOfPayments
        '6 = NoOfInvoices
        '7 = TotalAmount
        '8 = Number proposed of the matched
        '9 = Number finally posted in automatic matching
        '10 = Number in error during automatic matching
        '11 = Type of update - Struct, Auto or Manual

        If Array_IsEmpty(aStatistics) Then
            ReDim Preserve aStatistics(12, 0)
            aStatistics(0, 0) = sCompany_ID
            aStatistics(1, 0) = sClient_ID
            aStatistics(2, 0) = sType
            aStatistics(3, 0) = sERP_ID
            aStatistics(4, 0) = sDate
            aStatistics(5, 0) = sNoOfPayments
            aStatistics(6, 0) = sNoOfInvoices
            aStatistics(7, 0) = sTotalAmount
            aStatistics(8, 0) = sProposed
            aStatistics(9, 0) = sFinally
            aStatistics(10, 0) = sInError
            aStatistics(11, 0) = sTypeOfUpdate
        Else
            bAddNew = True
            For lCounter = 0 To UBound(aStatistics, 2)
                If aStatistics(0, lCounter) = sCompany_ID Then
                    If aStatistics(1, lCounter) = sClient_ID Then
                        If aStatistics(2, lCounter) = sType Then
                            If aStatistics(4, lCounter) = sDate Then
                                If aStatistics(11, lCounter) = sTypeOfUpdate Then
                                    bAddNew = False
                                    lY = lCounter
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                End If
            Next lCounter
            If bAddNew Then
                lY = UBound(aStatistics, 2) + 1
                ReDim Preserve aStatistics(12, lY)
                aStatistics(0, lY) = sCompany_ID
                aStatistics(1, lY) = sClient_ID
                aStatistics(2, lY) = sType
                aStatistics(3, lY) = sERP_ID
                aStatistics(4, lY) = sDate
                aStatistics(5, lY) = sNoOfPayments
                aStatistics(6, lY) = sNoOfInvoices
                aStatistics(7, lY) = sTotalAmount
                aStatistics(8, lY) = sProposed
                aStatistics(9, lY) = sFinally
                aStatistics(10, lY) = sInError
                aStatistics(11, lY) = sTypeOfUpdate
            Else
                aStatistics(3, lY) = sERP_ID
                aStatistics(5, lY) = Trim(Str(Val(aStatistics(5, lY)) + Val(sNoOfPayments)))
                aStatistics(6, lY) = Trim(Str(Val(aStatistics(6, lY)) + Val(sNoOfInvoices)))
                aStatistics(7, lY) = Trim(Str(Val(aStatistics(7, lY)) + Val(sTotalAmount)))
                aStatistics(8, lY) = Trim(Str(Val(aStatistics(8, lY)) + Val(sProposed)))
                aStatistics(9, lY) = Trim(Str(Val(aStatistics(9, lY)) + Val(sFinally)))
                aStatistics(10, lY) = Trim(Str(Val(aStatistics(10, lY)) + Val(sInError)))
            End If
        End If

        Statistics_UpdateArray = True

    End Function
	
    Private Function Statistics_StoreArray(ByRef aStatistics(,) As String) As Boolean
        Dim iCounter As Integer
        Dim iCounter2 As Integer
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCompanyID As Short
        Dim lAffected As Short
        Dim bReturnValue As Boolean

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            For iCounter = 0 To UBound(aStatistics, 2)
                For iCounter2 = 3 To UBound(aStatistics, 1)
                    If iCounter2 <> 4 Then
                        If EmptyString(aStatistics(iCounter2, iCounter)) Then
                            aStatistics(iCounter2, iCounter) = "0"
                        End If
                    End If
                Next iCounter2

                sMySQL = "Select * FROM [Statistics] WHERE Company_ID = " & aStatistics(0, iCounter)
                sMySQL = sMySQL & " AND Client_ID = " & aStatistics(1, iCounter)
                sMySQL = sMySQL & " AND Type = '" & aStatistics(2, iCounter)
                sMySQL = sMySQL & "' AND Date_Run = '" & aStatistics(4, iCounter)
                sMySQL = sMySQL & "' AND Infotype = '" & aStatistics(11, iCounter) & "'"
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then
                    If oMyDal.Reader_HasRows Then
                        sMySQL = "UPDATE [Statistics] SET NoofPayments = NoofPayments + " & Val(aStatistics(5, iCounter))
                        sMySQL = sMySQL & ", NoofInvoices = NoofInvoices + " & Val(aStatistics(6, iCounter))
                        sMySQL = sMySQL & ", Amount = Amount + " & Val(aStatistics(7, iCounter))
                        sMySQL = sMySQL & ", Proposed = Proposed + " & Val(aStatistics(8, iCounter))
                        sMySQL = sMySQL & ", Finally = Finally + " & Val(aStatistics(9, iCounter))
                        sMySQL = sMySQL & ", In_Error = In_Error + " & Val(aStatistics(10, iCounter))
                        sMySQL = sMySQL & " WHERE Company_ID = " & Val(aStatistics(0, iCounter))
                        sMySQL = sMySQL & " AND Client_ID =" & Val(aStatistics(1, iCounter))
                        sMySQL = sMySQL & " AND Type = '" & aStatistics(2, iCounter)
                        sMySQL = sMySQL & "' AND Date_Run = '" & aStatistics(4, iCounter)
                        sMySQL = sMySQL & "' AND Infotype = '" & aStatistics(11, iCounter) & "'" 'Added 08.07.2014

                        oMyDal.SQL = sMySQL
                        If oMyDal.ExecuteNonQuery Then
                            If oMyDal.RecordsAffected = 1 Then
                                bReturnValue = True
                            Else
                                Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                        End If

                    Else
                        sMySQL = "INSERT INTO [Statistics] "
                        sMySQL = sMySQL & "(Company_ID, Client_ID, Type, ERP_ID, Date_Run, NoofPayments, "
                        sMySQL = sMySQL & "NoofInvoices, Amount, Proposed, Finally, In_Error, Infotype) VALUES ("
                        sMySQL = sMySQL & aStatistics(0, iCounter) & ", " & aStatistics(1, iCounter) & ", '"
                        sMySQL = sMySQL & aStatistics(2, iCounter) & "', " & aStatistics(3, iCounter) & ", '"
                        sMySQL = sMySQL & aStatistics(4, iCounter) & "', " & aStatistics(5, iCounter) & ", "
                        sMySQL = sMySQL & aStatistics(6, iCounter) & ", " & aStatistics(7, iCounter) & ", "
                        sMySQL = sMySQL & aStatistics(8, iCounter) & ", " & aStatistics(9, iCounter) & ", "
                        sMySQL = sMySQL & aStatistics(10, iCounter) & ", '" & aStatistics(11, iCounter) & "')"

                        oMyDal.SQL = sMySQL

                        If oMyDal.ExecuteNonQuery Then
                            If oMyDal.RecordsAffected = 1 Then
                                bReturnValue = True
                            Else
                                Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                        End If

                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

            Next iCounter

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            bReturnValue = False

            Throw New Exception("Function: Statistics_StoreArray" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
End Module
