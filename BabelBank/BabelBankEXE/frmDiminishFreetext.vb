Option Strict Off
Option Explicit On

Friend Class frmDiminishFreetext
	Inherits System.Windows.Forms.Form
	Public bCancelled As Boolean
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		Me.Hide()
		bCancelled = True
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		Me.Hide()
		bCancelled = False
		
	End Sub
	
	Private Sub frmDiminishFreetext_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormLRSCaptions(Me)
		bCancelled = False
		
	End Sub
End Class
