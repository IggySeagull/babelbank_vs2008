Option Strict Off
Option Explicit On
Module CryptoSys
	' $Id: basCryptoSys.bas $
	' This module contains the full list of
	' classic Visual Basic/VBA declaration statements
	' for the CryptoSys(tm) API Version 4.0 library
	' plus some useful wrapper functions.
	' Last updated:
	'   $Date: 2007-09-12 20:06:00 $
	'   $Revision: 4.0.0 $
	
	'********************* COPYRIGHT NOTICE*********************
	' Copyright (c) 2001-7 DI Management Services Pty Limited.
	' All rights reserved.
	' This code may only be used by licensed users and in
	' accordance with the licence conditions.
	' The latest version of CryptoSys(tm) API and a licence
	' may be obtained from <www.cryptosys.net>.
	' This copyright notice must always be left intact.
	'****************** END OF COPYRIGHT NOTICE*****************
	
	
	' CONSTANTS
	Public Const ENCRYPT As Boolean = True
	Public Const DECRYPT As Boolean = False
	' Maximum number of bytes in hash digest byte array
	Public Const API_MAX_HASH_BYTES As Integer = 64
	Public Const API_SHA1_BYTES As Integer = 20
	Public Const API_SHA256_BYTES As Integer = 32
	Public Const API_SHA384_BYTES As Integer = 48
	Public Const API_SHA512_BYTES As Integer = 64
	Public Const API_MD5_BYTES As Integer = 16
	Public Const API_MD2_BYTES As Integer = 16
	' Maximum number of hex characters in hash digest
	Public Const API_MAX_HASH_CHARS As Integer = (2 * API_MAX_HASH_BYTES)
	Public Const API_SHA1_CHARS As Integer = (2 * API_SHA1_BYTES)
	Public Const API_SHA256_CHARS As Integer = (2 * API_SHA256_BYTES)
	Public Const API_SHA384_CHARS As Integer = (2 * API_SHA384_BYTES)
	Public Const API_SHA512_CHARS As Integer = (2 * API_SHA512_BYTES)
	Public Const API_MD5_CHARS As Integer = (2 * API_MD5_BYTES)
	Public Const API_MD2_CHARS As Integer = (2 * API_MD2_BYTES)
	' Maximum lengths of HMAC and CMAC
	Public Const API_MAX_HMAC_BYTES As Integer = 64
	Public Const API_MAX_CMAC_BYTES As Integer = 16
	Public Const API_MAX_HMAC_CHARS As Integer = (2 * API_MAX_HMAC_BYTES)
	Public Const API_MAX_CMAC_CHARS As Integer = (2 * API_MAX_CMAC_BYTES)
	' Synonyms retained for backwards compatibility
	Public Const API_MAX_SHA1_BYTES As Integer = 20
	Public Const API_MAX_SHA2_BYTES As Integer = 32
	Public Const API_MAX_MD5_BYTES As Integer = 16
	Public Const API_MAX_SHA1_CHARS As Integer = (2 * API_MAX_SHA1_BYTES)
	Public Const API_MAX_SHA2_CHARS As Integer = (2 * API_MAX_SHA2_BYTES)
	Public Const API_MAX_MD5_CHARS As Integer = (2 * API_MAX_MD5_BYTES)
	' Encryption block sizes in bytes
	Public Const API_BLK_DES_BYTES As Integer = 8
	Public Const API_BLK_TDEA_BYTES As Integer = 8
	Public Const API_BLK_BLF_BYTES As Integer = 8
	Public Const API_BLK_AES_BYTES As Integer = 16
	' Key size in bytes
	Public Const API_KEYSIZE_TDEA_BYTES As Integer = 24
	' Required size for RNG seed file
	Public Const API_RNG_SEED_BYTES As Integer = 64
	
	' Options for HASH functions
	Public Const API_HASH_SHA1 As Integer = 0
	Public Const API_HASH_MD5 As Integer = 1
	Public Const API_HASH_MD2 As Integer = 2
	Public Const API_HASH_SHA256 As Integer = 3
	Public Const API_HASH_SHA384 As Integer = 4
	Public Const API_HASH_SHA512 As Integer = 5
	Public Const API_HASH_MODE_TEXT As Integer = &H10000
	
	' Options for MAC functions
	Public Const API_CMAC_TDEA As Integer = &H100 ' ) synonyms
	Public Const API_CMAC_DESEDE As Integer = &H100 ' ) synonyms
	Public Const API_CMAC_AES128 As Integer = &H101
	Public Const API_CMAC_AES192 As Integer = &H102
	Public Const API_CMAC_AES256 As Integer = &H103
	
	' Options for RNG functions
	Public Const API_RNG_STRENGTH_112 As Integer = &H0
	Public Const API_RNG_STRENGTH_128 As Integer = &H1
	
	' GENERAL FUNCTIONS
	Public Declare Function API_ErrorLookup Lib "diCryptoSys.dll" (ByVal strErrMsg As String, ByVal nMaxChars As Integer, ByVal nErrCode As Integer) As Integer
	Public Declare Function API_PowerUpTests Lib "diCryptoSys.dll" (ByVal nReserved As Integer) As Integer
	Public Declare Function API_Version Lib "diCryptoSys.dll" () As Integer
	Public Declare Function API_LicenceType Lib "diCryptoSys.dll" (ByVal nReserved As Integer) As Integer
	Public Declare Function API_CompileTime Lib "diCryptoSys.dll" (ByVal strCompiledOn As String, ByVal nMaxChars As Integer) As Integer
	Public Declare Function API_ModuleName Lib "diCryptoSys.dll" (ByVal strModuleName As String, ByVal nMaxChars As Integer, ByVal nReserved As Integer) As Integer
	
	' ADVANCED ENCRYPTION STANDARD (AES) BLOCK CIPHER WITH 128-BIT KEY
	Public Declare Function AES128_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES128_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES128_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES128_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES128_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strB64IV As String) As Integer
	Public Declare Function AES128_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES128_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES128_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES128_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES128_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function AES128_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function AES128_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function AES128_InitError Lib "diCryptoSys.dll" () As Integer
	
	' ADVANCED ENCRYPTION STANDARD (AES) BLOCK CIPHER WITH 192-BIT KEY
	Public Declare Function AES192_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES192_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES192_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES192_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES192_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strB64IV As String) As Integer
	Public Declare Function AES192_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES192_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES192_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES192_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES192_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function AES192_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function AES192_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function AES192_InitError Lib "diCryptoSys.dll" () As Integer
	
	' ADVANCED ENCRYPTION STANDARD (AES) BLOCK CIPHER WITH 256-BIT KEY
	Public Declare Function AES256_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES256_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES256_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES256_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES256_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strB64IV As String) As Integer
	Public Declare Function AES256_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES256_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES256_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES256_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES256_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function AES256_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function AES256_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function AES256_InitError Lib "diCryptoSys.dll" () As Integer
	
	' BLOWFISH BLOCK CIPHER FUNCTIONS
	Public Declare Function BLF_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByVal bEncrypt As Integer) As Integer
	Public Declare Function BLF_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function BLF_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function BLF_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function BLF_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strB64IV As String) As Integer
	Public Declare Function BLF_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function BLF_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function BLF_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function BLF_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function BLF_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function BLF_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function BLF_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function BLF_InitError Lib "diCryptoSys.dll" () As Integer
	
	' DATA ENCRYPTION STANDARD (DES) BLOCK CIPHER
	Public Declare Function DES_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer) As Integer
	Public Declare Function DES_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function DES_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function DES_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function DES_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strB64IV As String) As Integer
	Public Declare Function DES_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function DES_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function DES_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function DES_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function DES_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function DES_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function DES_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function DES_InitError Lib "diCryptoSys.dll" () As Integer
	
	' Checks for weak or invalid-length DES or TDEA keys -- added in Version 3.0
	Public Declare Function DES_CheckKey Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function DES_CheckKeyHex Lib "diCryptoSys.dll" (ByVal strHexKey As String) As Integer
	
	' TRIPLE DATA ENCRYPTION ALGORITHM (TDEA) BLOCK CIPHER
	Public Declare Function TDEA_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer) As Integer
	Public Declare Function TDEA_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function TDEA_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function TDEA_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function TDEA_B64Mode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strB64Key As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strB64IV As String) As Integer
	Public Declare Function TDEA_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function TDEA_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function TDEA_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function TDEA_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function TDEA_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function TDEA_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function TDEA_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function TDEA_InitError Lib "diCryptoSys.dll" () As Integer
	
	' SECURE HASH ALGORITHM 1 (SHA-1) HASH FUNCTION
	Public Declare Function SHA1_StringHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strData As String) As Integer
	Public Declare Function SHA1_FileHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strFileName As String, ByVal strMode As String) As Integer
	Public Declare Function SHA1_BytesHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function SHA1_BytesHash Lib "diCryptoSys.dll" (ByRef abDigest As Byte, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function SHA1_Init Lib "diCryptoSys.dll" () As Integer
	Public Declare Function SHA1_AddString Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strData As String) As Integer
	Public Declare Function SHA1_AddBytes Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function SHA1_HexDigest Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal hContext As Integer) As Integer
	Public Declare Function SHA1_Reset Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function SHA1_Hmac Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function SHA1_HmacHex Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strHexData As String, ByVal strHexKey As String) As Integer
	
	' SECURE HASH ALGORITHM (SHA-256) HASH FUNCTION
	Public Declare Function SHA2_StringHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strData As String) As Integer
	Public Declare Function SHA2_FileHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strFileName As String, ByVal strMode As String) As Integer
	Public Declare Function SHA2_BytesHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function SHA2_BytesHash Lib "diCryptoSys.dll" (ByRef abDigest As Byte, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function SHA2_Init Lib "diCryptoSys.dll" () As Integer
	Public Declare Function SHA2_AddString Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strData As String) As Integer
	Public Declare Function SHA2_AddBytes Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function SHA2_HexDigest Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal hContext As Integer) As Integer
	Public Declare Function SHA2_Reset Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function SHA2_Hmac Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function SHA2_HmacHex Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strHexData As String, ByVal strHexKey As String) As Integer
	
	' RSA DATA SECURITY, INC. MD5 MESSAGE-DIGEST ALGORITHM
	Public Declare Function MD5_StringHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strData As String) As Integer
	Public Declare Function MD5_FileHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strFileName As String, ByVal strMode As String) As Integer
	Public Declare Function MD5_BytesHexHash Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function MD5_BytesHash Lib "diCryptoSys.dll" (ByRef abDigest As Byte, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function MD5_Init Lib "diCryptoSys.dll" () As Integer
	Public Declare Function MD5_AddString Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strData As String) As Integer
	Public Declare Function MD5_AddBytes Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function MD5_HexDigest Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal hContext As Integer) As Integer
	Public Declare Function MD5_Reset Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function MD5_Hmac Lib "diCryptoSys.dll" (ByVal strDigest As String, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function MD5_HmacHex Lib "diCryptoSys.dll" (ByVal strDigest As String, ByVal strHexData As String, ByVal strHexKey As String) As Integer
	
	' GENERIC MESSAGE DIGEST HASH FUNCTIONS
	Public Declare Function HASH_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_File Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Integer, ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_HexFromBytes Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_HexFromFile Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_HexFromHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strMsgHex As String, ByVal nOptions As Integer) As Integer
	' Alias for VB6 strings
	Public Declare Function HASH_HexFromString Lib "diCryptoSys.dll"  Alias "HASH_HexFromBytes"(ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strMessage As String, ByVal nStrLen As Integer, ByVal nOptions As Integer) As Integer
	
	' GENERIC MAC FUNCTIONS (HMAC and CMAC)
	Public Declare Function MAC_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByRef abKey As Byte, ByVal nKeyLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function MAC_HexFromBytes Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByRef abKey As Byte, ByVal nKeyLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function MAC_HexFromHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strMsgHex As String, ByVal strKeyHex As String, ByVal nOptions As Integer) As Integer
	
	' RC4-COMPATIBLE PC1 FUNCTIONS
	Public Declare Function PC1_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nDataLen As Integer, ByRef abKey As Byte, ByVal nKeyLen As Integer) As Integer
	Public Declare Function PC1_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strInputHex As String, ByVal strKeyHex As String) As Integer
	Public Declare Function PC1_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyLen As Integer) As Integer
	
	' RANDOM NUMBER GENERATOR (RNG) FUNCTIONS
	Public Declare Function RNG_KeyBytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nBytes As Integer, ByVal strSeed As String, ByVal nSeedLen As Integer) As Integer
	Public Declare Function RNG_KeyHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal nBytes As Integer, ByVal strSeed As String, ByVal nSeedLen As Integer) As Integer
	Public Declare Function RNG_NonceData Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function RNG_NonceDataHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal nBytes As Integer) As Integer
	Public Declare Function RNG_Test Lib "diCryptoSys.dll" (ByVal strFileName As String) As Integer
	' New in version 4.0...
	Public Declare Function RNG_Number Lib "diCryptoSys.dll" (ByVal nLower As Integer, ByVal nUpper As Integer) As Integer
	Public Declare Function RNG_BytesWithPrompt Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_HexWithPrompt Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal nBytes As Integer, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_Initialize Lib "diCryptoSys.dll" (ByVal strSeedFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_MakeSeedFile Lib "diCryptoSys.dll" (ByVal strSeedFile As String, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_UpdateSeedFile Lib "diCryptoSys.dll" (ByVal strSeedFile As String, ByVal nOptions As Integer) As Integer
	
	' ZLIB COMPRESSION FUNCTIONS
	Public Declare Function ZLIB_Deflate Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByRef abInput As Byte, ByVal nInputLen As Integer) As Integer
	Public Declare Function ZLIB_Inflate Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByRef abInput As Byte, ByVal nInputLen As Integer) As Integer
	
	' PASSWORD-BASED KEY DERIVATION FUNCTIONS
	Public Declare Function PBE_Kdf2 Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByRef abPwd As Byte, ByVal nPwdBytes As Integer, ByRef abSalt As Byte, ByVal nSaltBytes As Integer, ByVal nCount As Integer, ByVal nReserved As Integer) As Integer
	Public Declare Function PBE_Kdf2Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal nKeyBytes As Integer, ByVal strPwd As String, ByVal strSaltHex As String, ByVal nCount As Integer, ByVal nReserved As Integer) As Integer
	
	' HEX ENCODING CONVERSION FUNCTIONS
	Public Declare Function CNV_HexStrFromBytes Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	' See cnvHexStrFromBytes below
	Public Declare Function CNV_BytesFromHexStr Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Integer, ByVal strInput As String) As Integer
	' See cnvBytesFromHexStr below
	Public Declare Function CNV_HexFilter Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal nStrLen As Integer) As Integer
	' See cnvHexFilter below
	
	' BASE64 ENCODING CONVERSION FUNCTIONS
	Public Declare Function CNV_B64StrFromBytes Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	' See cnvB64StrFromBytes below
	Public Declare Function CNV_BytesFromB64Str Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutLen As Integer, ByVal strInput As String) As Integer
	' See cnvBytesFromHexB64r below
	Public Declare Function CNV_B64Filter Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal nStrLen As Integer) As Integer
	' See cnvB64Filter below
	
	' CRC FUNCTIONS
	Public Declare Function CRC_Bytes Lib "diCryptoSys.dll" (ByRef abInput As Byte, ByVal nBytes As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function CRC_String Lib "diCryptoSys.dll" (ByVal strInput As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CRC_File Lib "diCryptoSys.dll" (ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	
	' FUNCTIONS TO WIPE DATA
	Public Declare Function WIPE_File Lib "diCryptoSys.dll" (ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function WIPE_Data Lib "diCryptoSys.dll" (ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	' Alternative Aliases to cope with Byte and String types explicitly...
	Public Declare Function WIPE_Bytes Lib "diCryptoSys.dll"  Alias "WIPE_Data"(ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function WIPE_String Lib "diCryptoSys.dll"  Alias "WIPE_Data"(ByVal strData As String, ByVal nStrLen As Integer) As Integer
	
	' PADDING FUNCTIONS
	Public Declare Function PAD_BytesBlock Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByRef abInput As Byte, ByVal nInputLen As Integer, ByVal nBlockLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function PAD_UnpadBytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByRef abInput As Byte, ByVal nInputLen As Integer, ByVal nBlockLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function PAD_HexBlock Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strInputHex As String, ByVal nBlockLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function PAD_UnpadHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strInputHex As String, ByVal nBlockLen As Integer, ByVal nOptions As Integer) As Integer
	
	
	' *******************************************************************
	' DEPRECATED FUNCTIONS RETAINED FOR BACKWARDS COMPATABILITY
	' NOT RECOMMENDED FOR USE IN NEW APPLICATIONS
	' NB These are all thread-safe as of version 3
	
	' VERSION 1 ("Gutmann") RANDOM NUMBER GENERATOR
	Public Declare Function RAN_KeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_KeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal lngKeyBytes As Integer, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_DESKeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_DESKeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_TDEAKeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_TDEAKeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_Test Lib "diCryptoSys.dll" (ByVal strFileName As String) As Integer
	Public Declare Function RAN_Seed Lib "diCryptoSys.dll" (ByRef abSeed As Byte, ByVal lngSeedLen As Integer, ByVal bPromptUser As Boolean) As Integer
	Public Declare Function RAN_Nonce Lib "diCryptoSys.dll" (ByRef abNonce As Byte, ByVal lngNonceLen As Integer) As Integer
	Public Declare Function RAN_NonceHex Lib "diCryptoSys.dll" (ByVal sHexData As String, ByVal lngNonceLen As Integer) As Integer
	Public Declare Function RAN_Long Lib "diCryptoSys.dll" (ByVal lngLower As Integer, ByVal lngUpper As Integer) As Integer
	' KeyGen functions (and constants) superseded in version 3.0 by RNG_KeyBytes and RNG_KeyHex
	Public Const RNG_DEFAULT As Integer = &H0 ' Default flag
	Public Const RNG_NOCHECK As Integer = &H1 ' NO LONGER USED - IGNORED
	Public Const RNG_DESKEY As Integer = &H2 ' Set parity bits and check for weak DES keys
	' lngCheck is now ignored and can be set to zero
	Public Declare Function RNG_KeyGenerate Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Integer, ByVal strSeed As String, ByRef lngCheck As Integer, ByVal lngFlags As Integer) As Integer
	Public Declare Function RNG_KeyGenHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal lngKeyBytes As Integer, ByVal strSeed As String, ByRef lngCheck As Integer, ByVal lngFlags As Integer) As Integer
	' Nonce functions replaced with better parameters in RNG_NonceData
	Public Declare Function RNG_Nonce Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByVal nBytes As Integer, ByVal strSeed As String) As Integer
	Public Declare Function RNG_NonceHex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal nBytes As Integer, ByVal strSeed As String) As Integer
	' Superseded by RNG_Number
	Public Declare Function RNG_Long Lib "diCryptoSys.dll" (ByVal nLower As Integer, ByVal nUpper As Integer, ByVal strSeed As String) As Integer
	' OLD-STYLE RIJNDAEL BLOCK CIPHER FUNCTIONS
	' SUPERSEDED BY FASTER AESnnn FUNCTIONS
	Public Declare Function AES_Hex Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES_HexMode Lib "diCryptoSys.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strHexKey As String, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES_Bytes Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES_BytesMode Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES_File Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES_FileHex Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strHexKey As String, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abInitV As Byte) As Integer
	Public Declare Function AES_InitHex Lib "diCryptoSys.dll" (ByVal strHexKey As String, ByVal lngKeyBits As Integer, ByVal lngBlockBits As Integer, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strHexIV As String) As Integer
	Public Declare Function AES_Update Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function AES_UpdateHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexData As String) As Integer
	Public Declare Function AES_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function AES_Ecb Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES_EcbHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexBlock As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function AES_InitError Lib "diCryptoSys.dll" () As Integer
	' ORIGINAL di_Blowfish.DLL FUNCTIONS
	Public Declare Function bf_StringEnc Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function bf_StringDec Lib "diCryptoSys.dll" (ByRef abOutput As Byte, ByRef abData As Byte, ByVal nBytes As Integer, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function bf_FileEnc Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function bf_FileDec Lib "diCryptoSys.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function bf_Init Lib "diCryptoSys.dll" (ByRef abKey As Byte, ByVal nKeyBytes As Integer) As Integer
	Public Declare Function bf_BlockEnc Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abBlock As Byte) As Integer
	Public Declare Function bf_BlockDec Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abBlock As Byte) As Integer
	Public Declare Function bf_Final Lib "diCryptoSys.dll" (ByVal hContext As Integer) As Integer
	Public Declare Function BLF_EcbHex Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByVal strHexBlock As String, ByVal bEncrypt As Integer) As Integer
	Public Declare Function BLF_Ecb Lib "diCryptoSys.dll" (ByVal hContext As Integer, ByRef abData As Byte, ByVal nBytes As Integer, ByVal bEncrypt As Integer) As Integer
	' ... END OF DEPRECATED FUNCTIONS
	' *******************************************************************
	
	' SOME USEFUL WRAPPER FUNCTIONS
	
	' [2006-07-04] Conversion functions updated to handle errors better.
	
	Public Function cnvHexStrFromBytes(ByRef abData() As Byte) As String
		' Returns hex string encoding of bytes in abData or empty string if error
		Dim strHex As String
		Dim nHexLen As Integer
		Dim nDataLen As Integer
		
		On Error GoTo CatchEmptyData
		nDataLen = UBound(abData) - LBound(abData) + 1
		nHexLen = CNV_HexStrFromBytes(vbNullString, 0, abData(0), nDataLen)
		If nHexLen <= 0 Then
			Exit Function
		End If
		strHex = New String(" ", nHexLen)
		nHexLen = CNV_HexStrFromBytes(strHex, nHexLen, abData(0), nDataLen)
		If nHexLen <= 0 Then
			Exit Function
		End If
		cnvHexStrFromBytes = Left(strHex, nHexLen)
		
CatchEmptyData: 
		
	End Function
	
	Public Function cnvHexStrFromString(ByRef strData As String) As String
		' Returns hex string encoding of ASCII string or empty string if error
		Dim strHex As String
		Dim nHexLen As Integer
		Dim nDataLen As Integer
		Dim abData() As Byte
		
		If Len(strData) = 0 Then Exit Function
		'UPGRADE_ISSUE: Constant vbFromUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_TODO: Code was upgraded to use System.Text.UnicodeEncoding.Unicode.GetBytes() which may not have the same behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="93DD716C-10E3-41BE-A4A8-3BA40157905B"'
		abData = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(strData, vbFromUnicode))
		nDataLen = UBound(abData) - LBound(abData) + 1
		nHexLen = CNV_HexStrFromBytes(vbNullString, 0, abData(0), nDataLen)
		If nHexLen <= 0 Then
			Exit Function
		End If
		strHex = New String(" ", nHexLen)
		nHexLen = CNV_HexStrFromBytes(strHex, nHexLen, abData(0), nDataLen)
		If nHexLen <= 0 Then
			Exit Function
		End If
		cnvHexStrFromString = Left(strHex, nHexLen)
	End Function
	
	Public Function cnvBytesFromHexStr(ByRef strHex As String) As Object
		' Returns a Variant to an array of bytes decoded from a hex string
		Dim abData() As Byte
		Dim nDataLen As Integer
		
		' Set default return value that won't cause a run-time error
		'UPGRADE_ISSUE: Constant vbFromUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		cnvBytesFromHexStr = StrConv("", vbFromUnicode)
		nDataLen = CNV_BytesFromHexStr(0, 0, strHex)
		If nDataLen <= 0 Then
			Exit Function
		End If
		ReDim abData(nDataLen - 1)
		nDataLen = CNV_BytesFromHexStr(abData(0), nDataLen, strHex)
		If nDataLen <= 0 Then
			Exit Function
		End If
		ReDim Preserve abData(nDataLen - 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object cnvBytesFromHexStr. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		cnvBytesFromHexStr = VB6.CopyArray(abData)
	End Function
	
	Public Function cnvStringFromHexStr(ByVal strHex As String) As String
		' Converts string <strHex> in hex format to string of ANSI chars
		' with value between 0 and 255.
		' E.g. "6162632E" will be converted to "abc."
		Dim abData() As Byte
		If Len(strHex) = 0 Then Exit Function
		'UPGRADE_WARNING: Couldn't resolve default property of object cnvBytesFromHexStr(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		abData = cnvBytesFromHexStr(strHex)
		'UPGRADE_ISSUE: Constant vbUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		cnvStringFromHexStr = StrConv(System.Text.UnicodeEncoding.Unicode.GetString(abData), vbUnicode)
	End Function
	
	Public Function cnvHexFilter(ByRef strHex As String) As String
		' Returns a string stripped of any invalid hex characters
		Dim strFiltered As String
		Dim nLen As Integer
		
		strFiltered = New String(" ", Len(strHex))
		nLen = CNV_HexFilter(strFiltered, strHex, Len(strHex))
		If nLen > 0 Then
			strFiltered = Left(strFiltered, nLen)
		Else
			strFiltered = ""
		End If
		cnvHexFilter = strFiltered
	End Function
	
	Public Function cnvB64StrFromBytes(ByRef abData() As Byte) As String
		' Returns base64 string encoding of bytes in abData or empty string if error
		Dim strB64 As String
		Dim nB64Len As Integer
		Dim nDataLen As Integer
		
		On Error GoTo CatchEmptyData
		nDataLen = UBound(abData) - LBound(abData) + 1
		nB64Len = CNV_B64StrFromBytes(vbNullString, 0, abData(0), nDataLen)
		If nB64Len <= 0 Then
			Exit Function
		End If
		strB64 = New String(" ", nB64Len)
		nB64Len = CNV_B64StrFromBytes(strB64, nB64Len, abData(0), nDataLen)
		If nB64Len <= 0 Then
			Exit Function
		End If
		cnvB64StrFromBytes = Left(strB64, nB64Len)
		
CatchEmptyData: 
		
	End Function
	
	Public Function cnvB64StrFromString(ByRef strData As String) As String
		' Returns base64 string encoding of ASCII string or empty string if error
		Dim strB64 As String
		Dim nB64Len As Integer
		Dim nDataLen As Integer
		Dim abData() As Byte
		
		If Len(strData) = 0 Then Exit Function
		'UPGRADE_ISSUE: Constant vbFromUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_TODO: Code was upgraded to use System.Text.UnicodeEncoding.Unicode.GetBytes() which may not have the same behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="93DD716C-10E3-41BE-A4A8-3BA40157905B"'
		abData = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(strData, vbFromUnicode))
		nDataLen = UBound(abData) - LBound(abData) + 1
		nB64Len = CNV_B64StrFromBytes(vbNullString, 0, abData(0), nDataLen)
		If nB64Len <= 0 Then
			Exit Function
		End If
		strB64 = New String(" ", nB64Len)
		nB64Len = CNV_B64StrFromBytes(strB64, nB64Len, abData(0), nDataLen)
		If nB64Len <= 0 Then
			Exit Function
		End If
		cnvB64StrFromString = Left(strB64, nB64Len)
	End Function
	
	Public Function cnvBytesFromB64Str(ByRef strB64 As String) As Object
		' Returns a Variant to an array of bytes decoded from a base64 string
		Dim abData() As Byte
		Dim nDataLen As Integer
		
		' Set default return value that won't cause a run-time error
		'UPGRADE_ISSUE: Constant vbFromUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		cnvBytesFromB64Str = StrConv("", vbFromUnicode)
		nDataLen = CNV_BytesFromB64Str(0, 0, strB64)
		If nDataLen <= 0 Then
			Exit Function
		End If
		ReDim abData(nDataLen - 1)
		nDataLen = CNV_BytesFromB64Str(abData(0), nDataLen, strB64)
		If nDataLen <= 0 Then
			Exit Function
		End If
		ReDim Preserve abData(nDataLen - 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object cnvBytesFromB64Str. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		cnvBytesFromB64Str = VB6.CopyArray(abData)
	End Function
	
	Public Function cnvB64Filter(ByRef strB64 As String) As String
		' Returns a string stripped of any invalid base64 characters
		Dim strFiltered As String
		Dim nLen As Integer
		
		strFiltered = New String(" ", Len(strB64))
		nLen = CNV_B64Filter(strFiltered, strB64, Len(strB64))
		If nLen > 0 Then
			strFiltered = Left(strFiltered, nLen)
		Else
			strFiltered = ""
		End If
		cnvB64Filter = strFiltered
	End Function
	
	Public Function rngNonceHex(ByRef nBytes As Integer) As String
		' Returns a random nonce nBytes long encoded in hex
		Dim strHex As String
		Dim lngRet As Integer
		
		strHex = New String(" ", nBytes * 2)
		lngRet = RNG_NonceDataHex(strHex, Len(strHex), nBytes)
		If lngRet = 0 Then
			rngNonceHex = strHex
		End If
	End Function
	
	Public Function apiErrorLookup(ByRef nCode As Integer) As String
		' Returns a string with the error message for code nCode
		Dim strMsg As String
		Dim nRet As Integer
		
		strMsg = New String(" ", 128)
		nRet = API_ErrorLookup(strMsg, Len(strMsg), nCode)
		apiErrorLookup = Left(strMsg, nRet)
	End Function
	
	Public Function padHexString(ByVal strInputHex As String, ByRef nBlockLen As Integer) As String
		' Adds padding to a hex string up to next multiple of block length.
		' Returns a padded hex string or, on error, an empty string.
		
		Dim nOutChars As Integer
		Dim strOutputHex As String
		
		' In VB6 an uninitialised empty string is passed to a DLL as a NULL,
		' so we append a non-null empty string!
		strInputHex = strInputHex & ""
		
		nOutChars = PAD_HexBlock("", 0, strInputHex, nBlockLen, 0)
		'Debug.Print "Required length is " & nOutChars & " characters"
		' Check for error
		If (nOutChars <= 0) Then Exit Function
		
		' Pre-dimension output
		strOutputHex = New String(" ", nOutChars)
		
		nOutChars = PAD_HexBlock(strOutputHex, Len(strOutputHex), strInputHex, nBlockLen, 0)
		If (nOutChars <= 0) Then Exit Function
		'Debug.Print "Padded data='" & strOutputHex & "'"
		
		padHexString = strOutputHex
		
	End Function
	
	Public Function unpadHexString(ByRef strInputHex As String, ByRef nBlockLen As Integer) As String
		' Strips padding from a hex string.
		' Returns unpadded hex string or, on error, the original input string
		' -- we do this because an empty string is a valid result.
		' To check for error: a valid output string is *always* shorter than the input.
		
		Dim nOutChars As Integer
		Dim strOutputHex As String
		
		' No need to query for length because we know the output will be shorter than input
		' so make sure output is as long as the input
		strOutputHex = New String(" ", Len(strInputHex))
		nOutChars = PAD_UnpadHex(strOutputHex, Len(strOutputHex), strInputHex, nBlockLen, 0)
		'Debug.Print "Unpadded length is " & nOutChars & " characters"
		
		' Check for error
		If (nOutChars < 0) Then
			' Return unchanged input to indicate error
			unpadHexString = strInputHex
			Exit Function
		End If
		
		' Re-dimension the output to the correct length
		strOutputHex = Left(strOutputHex, nOutChars)
		'Debug.Print "Unpadded data='" & strOutputHex & "'"
		
		unpadHexString = strOutputHex
		
	End Function
	
	' ... END OF MODULE
	' *******************************************************************
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Added by Visual Banking
	' from here to end of module
	
	Public Function Crypt_FileHex(ByRef sUnCryptedFile As String, ByRef sCryptedFile As String, ByRef sKey As String) As Boolean
		' Crypt a file useing TripleDes (TDES) in HexMode
		Dim lngRet As Integer
		
		On Error GoTo ErrCrypt
		' If len sKey = 8(16) or 16(32), convert to 24 length
		' Key must be passed as hex, thats why lengt in chars is 16, 32 or 48
		If Len(sKey) = 16 Then
			sKey = sKey & sKey & sKey
		ElseIf Len(sKey) = 32 Then 
			' use first key same as third key
			sKey = sKey & Left(sKey, 16)
		End If
		
		' Uses no cInitial Key
		lngRet = TDEA_FileHex(sCryptedFile, sUnCryptedFile, sKey, True, "ECB", "")
		
		If lngRet = 0 Then
			Crypt_FileHex = True
		Else
			Crypt_FileHex = False
			Crypt_ErrMess(lngRet)
		End If
		Exit Function
		
ErrCrypt: 
		Err.Raise(5000, "CryptoSys", "Unspecified error in filecrypting")
		Crypt_FileHex = False
		
	End Function
	Public Function DeCrypt_FileHex(ByRef sCryptedFile As String, ByRef sDeCryptedFile As String, ByRef sKey As String) As Boolean
		' DeCrypt a file useing TripleDes (TDES) in HexMode
		Dim lngRet As Integer
		
		On Error GoTo ErrCrypt
		' If len sKey = 8(16) or 16(32), convert to 24 length
		' Key must be passed as hex, thats why lengt in chars is 16, 32 or 48
		If Len(sKey) = 16 Then
			sKey = sKey & sKey & sKey
		ElseIf Len(sKey) = 32 Then 
			' use first key same as third key
			sKey = sKey & Left(sKey, 16)
		End If
		
		' Uses no cInitial Key
		lngRet = TDEA_FileHex(sDeCryptedFile, sCryptedFile, sKey, False, "ECB", "")
		
		If lngRet = 0 Then
			DeCrypt_FileHex = True
		Else
			DeCrypt_FileHex = False
			Crypt_ErrMess(lngRet)
		End If
		Exit Function
		
ErrCrypt: 
		Err.Raise(5000, "CryptoSys", "Unspecified error in file Decrypting")
		DeCrypt_FileHex = False
		
	End Function
	
	Private Function Crypt_ErrMess(ByRef lngRet As Integer) As Object
		Dim sErrMsg As String
		
		Select Case lngRet
			Case 1
				sErrMsg = "1 = Cannot open input file"
			Case 2
				sErrMsg = "2 = Cannot create output file"
			Case 3
				sErrMsg = "3 = File read error"
			Case 4
				sErrMsg = "4 = File write error"
			Case 8
				sErrMsg = "8 = Input not multiple of 8 bytes long"
			Case 16
				sErrMsg = "16 = Input data corrupted"
			Case 17
				sErrMsg = "17 = Not enough room in output buffer"
			Case 33
				sErrMsg = "33 = Invalid key length"
			Case 34
				sErrMsg = "34 = Invalid block length"
			Case 35
				sErrMsg = "35 = Invalid mode"
			Case 39
				sErrMsg = "39 = Unexpected Null string found"
			Case 48
				sErrMsg = "48 = Invalid key"
			Case 49
				sErrMsg = "49 = Invalid block"
			Case 50
				sErrMsg = "50 = Invalid hexadecimal character"
			Case 51
				sErrMsg = "51 = Invalid initialisation vector"
			Case 64
				sErrMsg = "64 = Invalid context handle"
			Case 1024
				sErrMsg = "1024 = Not enough memory"
			Case 1281
				sErrMsg = "1281 = Random number memory error"
			Case 1282
				sErrMsg = "1282 = Random number generator power-up test failure"
			Case 1283
				sErrMsg = "1283 = Continuous random number generator test failure"
			Case 1284
				sErrMsg = "1284 = Random number setup error"
			Case 9999
				sErrMsg = "9999 = Miscellaneous error"
		End Select
		
		Err.Raise(CInt("123456"), "CryptoSys", Trim(Str(lngRet)) & sErrMsg)
		
	End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
End Module
