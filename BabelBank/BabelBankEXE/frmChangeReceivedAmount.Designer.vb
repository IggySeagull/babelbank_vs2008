<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmChangeReceivedAmount
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkUseOriginalAmount As System.Windows.Forms.CheckBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtCharges As System.Windows.Forms.TextBox
	Public WithEvents txtCurrency As System.Windows.Forms.TextBox
	Public WithEvents txtAmount As System.Windows.Forms.TextBox
	Public WithEvents lblExplaination As System.Windows.Forms.Label
    Public WithEvents lblCharges As System.Windows.Forms.Label
	Public WithEvents lblCurrency As System.Windows.Forms.Label
	Public WithEvents lblAmount As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkUseOriginalAmount = New System.Windows.Forms.CheckBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtCharges = New System.Windows.Forms.TextBox
        Me.txtCurrency = New System.Windows.Forms.TextBox
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblCharges = New System.Windows.Forms.Label
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'chkUseOriginalAmount
        '
        Me.chkUseOriginalAmount.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseOriginalAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseOriginalAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseOriginalAmount.Location = New System.Drawing.Point(208, 112)
        Me.chkUseOriginalAmount.Name = "chkUseOriginalAmount"
        Me.chkUseOriginalAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseOriginalAmount.Size = New System.Drawing.Size(249, 17)
        Me.chkUseOriginalAmount.TabIndex = 9
        Me.chkUseOriginalAmount.Text = "60263 - Use originally paid amount"
        Me.chkUseOriginalAmount.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(432, 184)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 7
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(352, 184)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtCharges
        '
        Me.txtCharges.AcceptsReturn = True
        Me.txtCharges.BackColor = System.Drawing.SystemColors.Window
        Me.txtCharges.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCharges.Enabled = False
        Me.txtCharges.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCharges.Location = New System.Drawing.Point(280, 150)
        Me.txtCharges.MaxLength = 0
        Me.txtCharges.Name = "txtCharges"
        Me.txtCharges.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCharges.Size = New System.Drawing.Size(67, 19)
        Me.txtCharges.TabIndex = 5
        Me.txtCharges.Visible = False
        '
        'txtCurrency
        '
        Me.txtCurrency.AcceptsReturn = True
        Me.txtCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.txtCurrency.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCurrency.Enabled = False
        Me.txtCurrency.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCurrency.Location = New System.Drawing.Point(280, 143)
        Me.txtCurrency.MaxLength = 0
        Me.txtCurrency.Name = "txtCurrency"
        Me.txtCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCurrency.Size = New System.Drawing.Size(40, 19)
        Me.txtCurrency.TabIndex = 4
        Me.txtCurrency.Visible = False
        '
        'txtAmount
        '
        Me.txtAmount.AcceptsReturn = True
        Me.txtAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAmount.Enabled = False
        Me.txtAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAmount.Location = New System.Drawing.Point(280, 136)
        Me.txtAmount.MaxLength = 0
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAmount.Size = New System.Drawing.Size(157, 19)
        Me.txtAmount.TabIndex = 1
        Me.txtAmount.Visible = False
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(208, 8)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(289, 97)
        Me.lblExplaination.TabIndex = 8
        Me.lblExplaination.Text = "60258 - Forklaring"
        '
        'lblCharges
        '
        Me.lblCharges.BackColor = System.Drawing.SystemColors.Control
        Me.lblCharges.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCharges.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCharges.Location = New System.Drawing.Point(208, 152)
        Me.lblCharges.Name = "lblCharges"
        Me.lblCharges.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCharges.Size = New System.Drawing.Size(70, 17)
        Me.lblCharges.TabIndex = 3
        Me.lblCharges.Text = "60154 Gebyr"
        Me.lblCharges.Visible = False
        '
        'lblCurrency
        '
        Me.lblCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.lblCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCurrency.Location = New System.Drawing.Point(208, 144)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCurrency.Size = New System.Drawing.Size(70, 17)
        Me.lblCurrency.TabIndex = 2
        Me.lblCurrency.Text = "60087 Valuta"
        Me.lblCurrency.Visible = False
        '
        'lblAmount
        '
        Me.lblAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAmount.Location = New System.Drawing.Point(208, 138)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAmount.Size = New System.Drawing.Size(70, 17)
        Me.lblAmount.TabIndex = 0
        Me.lblAmount.Text = "60086 Bel�p"
        Me.lblAmount.Visible = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 178)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(495, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmChangeReceivedAmount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(514, 213)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkUseOriginalAmount)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtCharges)
        Me.Controls.Add(Me.txtCurrency)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.lblExplaination)
        Me.Controls.Add(Me.lblCharges)
        Me.Controls.Add(Me.lblCurrency)
        Me.Controls.Add(Me.lblAmount)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmChangeReceivedAmount"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Mottatt bel�p"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
