Option Strict Off
Option Explicit On
Friend Class Progress
	Private WithEvents oBabel As vbBabel.BabelFile
	
	Friend Function SetImportObject(ByRef oImportObject As vbBabel.BabelFile) As Boolean
		
		oBabel = oImportObject
		SetImportObject = True
		
	End Function
	
	Public Sub oBabel_ProgressUpdated(ByVal dblProgressPercentage As Double) Handles oBabel.ProgressUpdated
		' 20 = Progressbar at the startpoint for import
		' 48 = Progressbar at the endpoint for import, difference 28
		ProgressPos((frmProgress.iStartPointImport + frmProgress.iProgImport * dblProgressPercentage / 100))
	End Sub
End Class
