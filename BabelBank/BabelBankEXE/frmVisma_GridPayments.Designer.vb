<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_GridPayments
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cmdExport As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents imgMinus As System.Windows.Forms.PictureBox
	Public WithEvents imgPlus As System.Windows.Forms.PictureBox
	Public WithEvents lblStatus As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisma_GridPayments))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdExport = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.imgMinus = New System.Windows.Forms.PictureBox
        Me.imgPlus = New System.Windows.Forms.PictureBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.gridClientPayments = New System.Windows.Forms.DataGridView
        CType(Me.imgMinus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPlus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdExport
        '
        Me.cmdExport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExport.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExport.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExport.Location = New System.Drawing.Point(994, 599)
        Me.cmdExport.Name = "cmdExport"
        Me.cmdExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExport.Size = New System.Drawing.Size(100, 24)
        Me.cmdExport.TabIndex = 8
        Me.cmdExport.Text = "35110-Eksporter"
        Me.cmdExport.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(1100, 599)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(100, 24)
        Me.cmdCancel.TabIndex = 7
        Me.cmdCancel.Text = "55006-Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'imgMinus
        '
        Me.imgMinus.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgMinus.Image = CType(resources.GetObject("imgMinus.Image"), System.Drawing.Image)
        Me.imgMinus.Location = New System.Drawing.Point(667, 67)
        Me.imgMinus.Name = "imgMinus"
        Me.imgMinus.Size = New System.Drawing.Size(16, 16)
        Me.imgMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgMinus.TabIndex = 16
        Me.imgMinus.TabStop = False
        Me.imgMinus.Visible = False
        '
        'imgPlus
        '
        Me.imgPlus.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPlus.Image = CType(resources.GetObject("imgPlus.Image"), System.Drawing.Image)
        Me.imgPlus.Location = New System.Drawing.Point(667, 40)
        Me.imgPlus.Name = "imgPlus"
        Me.imgPlus.Size = New System.Drawing.Size(16, 16)
        Me.imgPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgPlus.TabIndex = 17
        Me.imgPlus.TabStop = False
        Me.imgPlus.Visible = False
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.SystemColors.Control
        Me.lblStatus.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStatus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStatus.Location = New System.Drawing.Point(12, 598)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStatus.Size = New System.Drawing.Size(929, 30)
        Me.lblStatus.TabIndex = 6
        '
        'gridClientPayments
        '
        Me.gridClientPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridClientPayments.Location = New System.Drawing.Point(15, 88)
        Me.gridClientPayments.Name = "gridClientPayments"
        Me.gridClientPayments.Size = New System.Drawing.Size(1184, 505)
        Me.gridClientPayments.TabIndex = 20
        '
        'frmVisma_GridPayments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(1211, 637)
        Me.ControlBox = False
        Me.Controls.Add(Me.gridClientPayments)
        Me.Controls.Add(Me.cmdExport)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.imgMinus)
        Me.Controls.Add(Me.imgPlus)
        Me.Controls.Add(Me.lblStatus)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(3, 25)
        Me.Name = "frmVisma_GridPayments"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visma Payment (BabelBank)"
        CType(Me.imgMinus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPlus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridClientPayments As System.Windows.Forms.DataGridView
#End Region 
End Class