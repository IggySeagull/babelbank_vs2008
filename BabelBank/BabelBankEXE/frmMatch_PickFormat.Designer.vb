<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMATCH_PickFiletype
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents lstFormats As System.Windows.Forms.ListBox
	Public WithEvents lblHeading As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMATCH_PickFiletype))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdOK = New System.Windows.Forms.Button
		Me.CmdCancel = New System.Windows.Forms.Button
		Me.lstFormats = New System.Windows.Forms.ListBox
		Me.lblHeading = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "60246 - Select format"
		Me.ClientSize = New System.Drawing.Size(524, 296)
		Me.Location = New System.Drawing.Point(4, 30)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmMATCH_PickFiletype"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.cmdOK.Size = New System.Drawing.Size(73, 21)
		Me.cmdOK.Location = New System.Drawing.Point(416, 256)
		Me.cmdOK.TabIndex = 3
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.CmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CmdCancel.Text = "55006 - Avbryt"
		Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
		Me.CmdCancel.Location = New System.Drawing.Point(336, 256)
		Me.CmdCancel.TabIndex = 2
		Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.CmdCancel.CausesValidation = True
		Me.CmdCancel.Enabled = True
		Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdCancel.TabStop = True
		Me.CmdCancel.Name = "CmdCancel"
		Me.lstFormats.Size = New System.Drawing.Size(289, 176)
		Me.lstFormats.Location = New System.Drawing.Point(200, 72)
		Me.lstFormats.TabIndex = 1
		Me.lstFormats.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstFormats.BackColor = System.Drawing.SystemColors.Window
		Me.lstFormats.CausesValidation = True
		Me.lstFormats.Enabled = True
		Me.lstFormats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstFormats.IntegralHeight = True
		Me.lstFormats.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstFormats.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstFormats.Sorted = False
		Me.lstFormats.TabStop = True
		Me.lstFormats.Visible = True
		Me.lstFormats.MultiColumn = False
		Me.lstFormats.Name = "lstFormats"
		Me.lblHeading.Text = "60249 - Select format on the file to import"
		Me.lblHeading.Size = New System.Drawing.Size(296, 17)
		Me.lblHeading.Location = New System.Drawing.Point(200, 17)
		Me.lblHeading.TabIndex = 0
		Me.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
		Me.lblHeading.Enabled = True
		Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblHeading.UseMnemonic = True
		Me.lblHeading.Visible = True
		Me.lblHeading.AutoSize = False
		Me.lblHeading.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblHeading.Name = "lblHeading"
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(CmdCancel)
		Me.Controls.Add(lstFormats)
		Me.Controls.Add(lblHeading)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
