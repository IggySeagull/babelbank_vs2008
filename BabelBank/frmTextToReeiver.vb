﻿Option Strict Off
Option Explicit On
'.Printing.Compatibility.VB6
Public Class frmTextToReeiver
    Dim sReturnText As String
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 9, 130, 450, 130)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        sReturnText = ""
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If Me.optSalary.Checked = True Then
            sReturnText = "Salary Payment"
        ElseIf Me.optTravel.Checked = True Then
            sReturnText = "Travel Expenses"
        Else
            ' freetext
            sReturnText = Trim(Me.txtFreetext.Text)
        End If
        Me.Hide()
    End Sub
    Public Function DialogResultInForm() As String
        DialogResultInForm = sReturnText
    End Function

    Private Sub optSalary_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optSalary.CheckedChanged
        Me.txtFreetext.Enabled = False
    End Sub

    Private Sub optTravel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTravel.CheckedChanged
        Me.txtFreetext.Enabled = False
    End Sub

    Private Sub optFreetext_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optFreetext.CheckedChanged
        Me.txtFreetext.Enabled = True
        Me.txtFreetext.Select()
    End Sub
End Class