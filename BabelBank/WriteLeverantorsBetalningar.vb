Option Strict Off
Option Explicit On
Module WriteLeverantorBetalningar

    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    ' - fileniv�: sum bel�p, antall records, antall transer, siste dato
    '             disse p�virkes nedover i niv�ene
    Dim nFileSumAmount As Double, nFileNoRecords As Double
    Dim sPayNumber As String
    ' XOKNET 26.07.2013
    Dim nFileSumAmountSEK As Double
    Dim bFileStartWritten As Boolean
    ' XOKNET 06.07.2015 added next
    Dim eFileType As BabelFiles.FileType
    Dim sHeaderAccount As String  'added 23.05.2016

    ' XokNET 26.11.2013 added as string for sFilenameOut
    ' XOKNET 06.07.2015 added new parameter eFormat
    Function WriteLeverantorsBetalningar(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String, ByVal eFormat As BabelFiles.FileType) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double, k As Double, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim sNewOwnref As String
        Dim sTxt As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bInternational As Boolean
        Dim bThereAreDomestic As Boolean
        Dim bThereAreInternational As Boolean
        Dim sOldAccount As String
        ' XOKNET 29.10.2012 - Must have one FileStart-record pr I_Account (pr debet BankgiroNr) !!
        Dim sOldDebitAccount As String
        ' XOKNET 08.07.2015
        Dim sOldCurrency As String
        Dim sOldSupplierNo As String = ""
        Dim bContinue As Boolean

        ' lag en outputfil
        Try

            ' XOKNET 06.07.2015
            eFileType = eFormat

            'XokNET 27.01.2010
            nFileNoRecords = 0
            sOldAccount = "--------------------"

            ' New 15.02.06
            ' Did not write fileheader if we run more than once without leaving BabelBank
            If Len(Dir(sFilenameOut)) > 0 Then
                bFileStartWritten = True
            Else
                bFileStartWritten = False
            End If
            '-end 15.02.06 --------------

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            ' Possible with file with one domestic and one International batch
            ' Runs first all payments to create a Domestic batch
            ' Then a new run for International payments.
            ' This way there may be a nonsorted mix, which will be grouped in the exportfile
            bInternational = False
            ' First analyze, if there are domestic and or international payments in oBabelFiles
            bThereAreDomestic = False
            bThereAreInternational = False

            For Each oBabel In oBabelFiles

                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                        If oPayment.Cancel = False And Not oPayment.Exported Then ' XOKNET 25.11.2010 added this line and End if
                            If oPayment.PayType = "I" Then
                                If UCase(oBabel.Special) = "PGS_SWEDEN" Then
                                    bThereAreInternational = False
                                Else
                                    bThereAreInternational = True
                                End If
                            Else
                                bThereAreDomestic = True
                            End If
                            ' Jump out when we have found what we are searching for
                            If bThereAreDomestic And bThereAreInternational Then
                                Exit For
                            End If
                        End If 'oPayment.Cancel = False Then ' XOKNET 25.11.2010 added this line and End if
                    Next oPayment
                    ' Jump out when we have found what we are searching for
                    If bThereAreDomestic And bThereAreInternational Then
                        Exit For
                    End If
                Next oBatch
                ' Jump out when we have found what we are searching for
                If bThereAreDomestic And bThereAreInternational Then
                    Exit For
                End If
            Next oBabel

            If bThereAreDomestic Then
                ' First, run domestic
                bInternational = False
            Else
                ' Run only domestic
                bInternational = True
            End If

            Do
                For Each oBabel In oBabelFiles
                    bExportoBabel = False
                    'Only export statuscode 00 or 02. The others don't give any sense in Leverant�rsBetalningar
                    bContinue = False
                    If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) _
                        Or oBabel.StatusCode = "02" Then
                        bContinue = True
                    ElseIf oBabel.ImportFormat = BabelFiles.FileType.Pain002 Or oBabel.ImportFormat = BabelFiles.FileType.Pain002_Rejected Then
                        '18.05.2017 - It makes sense after all! For Gjensidige it does
                        bContinue = True
                    End If

                    If bContinue Then
                        'Have to go through each batch-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oBatch In oBabel.Batches
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported

                                    If oPayment.Cancel = False And Not oPayment.Exported Then
                                        If Not bMultiFiles Then
                                            bExportoBabel = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBabel = True
                                                End If
                                            End If
                                        End If
                                        ' XOKNET 02.12.2010 - for XTRA Personell, we do also have two imported accounts, and must find the correct account
                                        ' Find this in I_Account. It is used in WriteLevBetFileStart(oBabel)
                                        If oBabel.Special = "XTRAPERSONELL_SE" Then
                                            oBabel.Cargo.I_Account = oPayment.I_Account
                                        End If

                                    End If 'If oPayment.Cancel = False Then
                                End If
                                If bExportoBabel Then
                                    Exit For
                                End If
                                If UCase(oBabel.Special) = "PGS_SWEDEN" And oPayment.PayType = "I" Then
                                    bExportoBabel = False
                                End If

                            Next
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next
                    Else
                        For Each oBatch In oBabel.Batches
                            For Each oPayment In oBatch.Payments
                                oPayment.Exported = True
                            Next oPayment
                        Next oBatch
                    End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

                    If bExportoBabel Then
                        ' XOKNET 29.10.2012 - Added next IF / End If - Must have one FileStart-record pr I_Account (pr debet BankgiroNr) !!
                        If sOldDebitAccount <> oPayment.I_Account Then
                            sOldDebitAccount = oPayment.I_Account
                            bFileStartWritten = False

                            ' Write endrecord for previous debit Bankgiro
                            If nFileNoRecords > 0 Then
                                sLine = WriteLevBetFileEnd(oBabelFiles(1))
                                oFile.WriteLine(sLine)
                            End If

                        End If

                        If Not bFileStartWritten Then
                            ' write only once!
                            ' When merging Client-files, skip this one except for first client!
                            ' XOKNET 29.10.2012 - this is not correct. Must have one FileStart-record pr I_Account (pr debet BankgiroNr) !!

                            ' Possible with file with one domestic and one International batch
                            'Runs first all payments to create a Domestic batch
                            If bInternational Then
                                sLine = WriteLevBetInt_Anvandarpost(oBabel)
                                oFile.WriteLine(sLine)
                            Else
                                ' 10.02.2020 - added sI_Account, used by Presskontakterna
                                sLine = WriteLevBetFileStart(oBabel, oBabel.Batches(1).Payments(1).MON_TransferCurrency, sI_Account)  ' XOKNET 08.07.2015 added last parameter
                                oFile.WriteLine(sLine)
                                ' XOKNET 29.10.2012 - added Rubrikpost
                                If Not EmptyString(oBatch.REF_Bank) Then
                                    sLine = WriteLevBetRubrikpost(oBatch)
                                    oFile.WriteLine(sLine)
                                End If

                            End If
                            sPayNumber = "0"
                            bFileStartWritten = True
                        End If

                        i = 0
                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                                    If oPayment.Cancel = False And Not oPayment.Exported Then

                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If 'If oPayment.Cancel = False Then
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next


                            If bExportoBatch Then
                                i = i + 1

                                j = 0
                                For Each oPayment In oBatch.Payments
                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        'Don't export payments that have been cancelled
                                        'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                                        If oPayment.Cancel = False And Not oPayment.Exported Then

                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If InStr(oPayment.REF_Own, "&?") Then
                                                        'Set in the part of the OwnRef that BabelBank is using
                                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                    Else
                                                        sNewOwnref = vbNullString
                                                    End If
                                                    If sNewOwnref = sOwnRef Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        Else
                                            oPayment.Exported = True
                                        End If 'If oPayment.Cancel = False Then
                                    End If

                                    If UCase(oBabel.Special) = "PGS_SWEDEN" And oPayment.PayType = "I" Then
                                        bExportoPayment = False
                                    End If

                                    If bExportoPayment Then
                                        j = j + 1

                                        If oPayment.PayType <> "I" Then
                                            If Not bInternational Then
                                                ' Two "runs" - one for domestic, one for int

                                                ' XOKNET 08.07.2015 - Added next IF / End If - Must have one FileStart-record pr Currency (SEK or EUR) for Domestic !
                                                If sOldCurrency <> oPayment.MON_TransferCurrency Then
                                                    sOldCurrency = oPayment.MON_TransferCurrency
                                                    bFileStartWritten = False

                                                    ' Write endrecord for previous debit Bankgiro
                                                    If nFileNoRecords > 0 Then
                                                        sLine = WriteLevBetFileEnd(oBabelFiles(1))
                                                        oFile.WriteLine(sLine)

                                                        ' write new startrecord
                                                        ' 10.02.2020 - added sI_Account, used by Presskontakterna
                                                        sLine = WriteLevBetFileStart(oBabel, oPayment.MON_TransferCurrency, sI_Account)  ' XOKNET 08.07.2015 added last parameter
                                                        oFile.WriteLine(sLine)
                                                        If Not EmptyString(oBatch.REF_Bank) Then
                                                            sLine = WriteLevBetRubrikpost(oBatch)
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                        sPayNumber = "0"
                                                        bFileStartWritten = True
                                                    End If
                                                End If

                                                For Each oInvoice In oPayment.Invoices
                                                    sTxt = ""

                                                    If Not oInvoice.Exported Then
                                                        ' 04.12.2008; Added "203" under because of salaries to real bankaccounts (for xTraPersonell)
                                                        If oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Or oPayment.PayCode = "160" Or oPayment.PayCode = "151" Or oPayment.PayCode = "203" Or Len(Trim$(oPayment.E_Account)) = 0 Then
                                                            ' Payment without accountno, or with real BANKaccount
                                                            ' organize several 14/16 records under one 040-record for same receiver
                                                            'If oPayment.E_Account <> sOldAccount Then
                                                            ' 25.11.2016 added test for SupplierNo, since account may be empty (Kontoisetting)
                                                            If oPayment.E_Account <> sOldAccount Or oInvoice.SupplierNo <> sOldSupplierNo Then
                                                                ' If Visma, we have info about payer in oInvoice.SupplierNo
                                                                If Not EmptyString(oInvoice.SupplierNo) Then
                                                                    sPayNumber = Right$(oInvoice.SupplierNo, 5)
                                                                Else
                                                                    sPayNumber = Trim$(Str(Val(sPayNumber) + 1))
                                                                End If
                                                            End If
                                                        End If

                                                        ' added 151 24.08.06
                                                        ' added 223 (Salary) 03.11.2008
                                                        If oPayment.PayCode = "151" Or oPayment.PayCode = "203" Or oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Or oPayment.PayType = "S" Then
                                                            ' added test for new receivers account 12.11.2007
                                                            ' organize several 14/15 records under one 040-record for same receiver

                                                            '23.05.2018 - can have different receivers with same account!!!
                                                            'If oPayment.E_Account <> sOldAccount Then
                                                            If oPayment.E_Account <> sOldAccount Or oInvoice.SupplierNo <> sOldSupplierNo Then
                                                                ' XOKNET 08.11.2012 added oBatch
                                                                sLine = WriteLevBetKontoinsetting(oBatch, oPayment, oInvoice, sTxt)
                                                                oFile.WriteLine(sLine)
                                                            End If
                                                        End If

                                                        ' 24.08.06 - added paycode 151
                                                        ' Moved this section after For Each oInvoice
                                                        ' 13.12.2013 Moved here !
                                                        If oPayment.PayCode = "160" Or oPayment.PayCode = "151" Or Len(Trim$(oPayment.E_Account)) = 0 Then
                                                            ' organize several 14/15 records under one 040-record for same receiver
                                                            'If oPayment.E_Account <> sOldAccount Then
                                                            ' 25.11.2016 added test for SupplierNo, since account may be empty (Kontoisetting)
                                                            If oPayment.E_Account <> sOldAccount Or oInvoice.SupplierNo <> sOldSupplierNo Then

                                                                ' There may / may not be adress-spesifications on payment level
                                                                sLine = WriteLevBetAdressOne(oPayment)
                                                                oFile.WriteLine(sLine)
                                                                sLine = WriteLevBetAdressTwo(oPayment)
                                                                oFile.WriteLine(sLine)
                                                            End If
                                                        End If
                                                        ' 22.03.2019
                                                        If oBabel.Special = "RAMUDDEN" Then
                                                            sLine = WriteLevBetAdressOne(oPayment)
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                        sOldSupplierNo = oInvoice.SupplierNo

                                                        ' XOKNET 29.10.2012 Moved an If some lines down

                                                        ' For Kontoisettinger m� vi tilslutt ogs� ha med selve betalingsrecorden !?
                                                        ' Rekkef�lgen er viktig; 40, 26, 27, 14
                                                        ' For BG-giro betalinger er det kun paymentrecorden (14)
                                                        ' XOKNET 08.11.2012 added oBatch

                                                        ' XNET 26.11.2013 Added handling of PLusgirot
                                                        If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Or oPayment.PayCode = "190" Then
                                                            sLine = WriteLevBetPaymentPlusgirot(oBatch, oPayment, oInvoice, sTxt)
                                                        Else
                                                            sLine = WriteLevBetPayment(oBatch, oPayment, oInvoice, sTxt)
                                                        End If
                                                        oFile.WriteLine(sLine)

                                                        ' XOKNET 29.10.2012 Moved this If some lines down
                                                        ' If more than 25 chars in freetext, we must use infolines:
                                                        If Len(sTxt) > 25 Then
                                                            Do
                                                                ' XOKNET 29.10.2012 - take whole freetext, including what has been laid out in 14-record
                                                                'sTxt = Mid$(sTxt, 26)
                                                                ' XNET 26.11.2013 Added handling of PLusgirot
                                                                If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Or oPayment.PayCode = "190" Then
                                                                    sLine = WriteLevBetInfoPlusgirot(oPayment, sTxt)
                                                                Else
                                                                    sLine = WriteLevBetInfo(oPayment, sTxt)
                                                                End If
                                                                oFile.WriteLine(sLine)
                                                                If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Or oPayment.PayCode = "190" Then
                                                                    ' 35 chars in one Informationspost
                                                                    If Len(sTxt) > 35 Then
                                                                        sTxt = Mid$(sTxt, 36)
                                                                    Else
                                                                        Exit Do
                                                                    End If
                                                                Else
                                                                    ' 50 chars in one Informationspost
                                                                    If Len(sTxt) > 50 Then
                                                                        sTxt = Mid$(sTxt, 51)
                                                                    Else
                                                                        Exit Do
                                                                    End If
                                                                End If
                                                            Loop
                                                        End If

                                                        '18.05.2017 - Added this next code for Gjensidige OW
                                                        'I think this TK94 may be som jumbalaya they use to understand that it is a rejection and not a part of the format.
                                                        If oBabel.ImportFormat = BabelFiles.FileType.Pain002 Or oBabel.ImportFormat = BabelFiles.FileType.Pain002_Rejected Then
                                                            sTxt = ""
                                                            If Not EmptyString(oPayment.StatusText) Then
                                                                sTxt = oPayment.StatusText
                                                            Else
                                                                For Each oFreetext In oInvoice.Freetexts
                                                                    sTxt = oFreetext.Text
                                                                Next
                                                            End If
                                                            If Not EmptyString(sTxt) Then
                                                                oFile.WriteLine("49" & sTxt)
                                                            End If
                                                        End If

                                                    End If ' If Not oInvoice.Exported Then
                                                    sOldAccount = oPayment.E_Account

                                                    ' XNET 30.05.2014 added next line
                                                    oInvoice.Exported = True

                                                Next oInvoice

                                                oPayment.Exported = True

                                            End If 'If not bInternational
                                        Else
                                            ' International
                                            ' NB! Content may be dependent upon bank
                                            If bInternational Then
                                                ' Two "runs" - one for domestic, one for int

                                                ' XOKNET 26.07.2013
                                                ' moved Invoiceloop here, makes it similar to ConvertX
                                                For Each oInvoice In oPayment.Invoices
                                                    If Not oInvoice.Exported Then

                                                        sPayNumber = Trim$(Str(Val(sPayNumber) + 1))
                                                        sLine = WriteLevBetInt_Namnpost(oPayment)
                                                        oFile.WriteLine(sLine)
                                                        sLine = WriteLevBetInt_Adresspost(oPayment)
                                                        oFile.WriteLine(sLine)
                                                        sLine = WriteLevBetInt_Bankpost(oPayment)
                                                        oFile.WriteLine(sLine)

                                                        ' XOKNET 26.07.2013 - adds freetext for this invoice
                                                        sTxt = vbNullString
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sTxt = sTxt & " " & Trim$(oFreetext.Text)
                                                        Next
                                                        sTxt = Trim(sTxt)

                                                        ' XOKNET 26.07.2013 - two runs of invoice/riksbank, to be able to pass ownref for use in returnfiles
                                                        ' 1. run
                                                        If oInvoice.MON_InvoiceAmount < 0 Then
                                                            sLine = WriteLevBetInt_Kreditnotpost(oPayment, oInvoice, sTxt, False)
                                                        Else
                                                            sLine = WriteLevBetInt_Betalningspost(oPayment, oInvoice, sTxt, False)
                                                        End If
                                                        oFile.WriteLine(sLine)
                                                        sLine = WriteLevBetInt_Riksbankpost(oInvoice)
                                                        oFile.WriteLine(sLine)

                                                        ' XokNET 03.02.2015
                                                        ' added option to not use returnfiles (for Visma) - then no need for 2. run
                                                        If oBabel.UseReturnFiles Then
                                                            ' 2. run
                                                            If Not EmptyString(oInvoice.REF_Own) Then
                                                                ' XokNET 15.08.2013 - Must also test for kreditnote here, and use creditnoterecord for second run
                                                                If oInvoice.MON_InvoiceAmount < 0 Then
                                                                    sLine = WriteLevBetInt_Kreditnotpost(oPayment, oInvoice, oInvoice.REF_Own, True)
                                                                Else
                                                                    sLine = WriteLevBetInt_Betalningspost(oPayment, oInvoice, oInvoice.REF_Own, True)
                                                                End If
                                                                oFile.WriteLine(sLine)
                                                                sLine = WriteLevBetInt_Riksbankpost(oInvoice)
                                                                oFile.WriteLine(sLine)
                                                            End If
                                                        End If

                                                        ' XNET 30.05.2014 added next line
                                                        oInvoice.Exported = True
                                                    End If
                                                Next oInvoice

                                                oPayment.Exported = True
                                            End If 'Not bInternational Then
                                        End If ' Paytype <> "I"
                                    End If

                                Next ' payment

                            End If

                        Next 'batch
                    End If
                Next 'Babelfile

                'XokNET 27.01.2011
                ' Added next lines/If to avoid having a Avstamningspost/FileEndRecord when there are no payments exported
                If nFileNoRecords > 0 Then
                    If bInternational Then
                        sLine = WriteLevBetInt_Avstamningspost(oBabelFiles(1))
                    Else
                        sLine = WriteLevBetFileEnd(oBabelFiles(1))
                    End If
                    oFile.WriteLine(sLine)
                End If


                ' Check if we are going to run through oBabelFiles again
                If bInternational Then
                    ' when we have completed International, then we have completed
                    Exit Do
                Else
                    ' do we have any International?
                    If bThereAreInternational Then
                        bInternational = True
                        bFileStartWritten = False ' Must have special headerrecord for international
                    Else
                        ' No international to write
                        Exit Do
                    End If
                End If
            Loop

            ' added 17.09.2007
            ' For PGS_SWEDEN there may be cases where we have no domestic. Must then delete the lev.bet file
            'If UCase(oBabelFiles(1).Special) = "PGS_SWEDEN" Then
            'XokNET 27.01.2011 Added XTRAPERSONELL_SE
            If UCase(oBabelFiles(1).Special) = "PGS_SWEDEN" Or UCase(oBabelFiles(1).Special) = "XTRAPERSONELL_SE" Then
                If nFileNoRecords = 0 Then
                    ' Delete the file
                    If Len(Dir(sFilenameOut)) > 0 Then
                        Kill(sFilenameOut)
                    End If
                End If
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteLeverantorsBetalningar" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteLeverantorsBetalningar = True

    End Function
    ' XOKNET 08.07.2015 added sCurrency (can be SEK or EUR)
    Function WriteLevBetFileStart(ByVal oBabelFile As BabelFile, ByVal sCurrency As String, ByVal sI_Account As String) As String
        Dim sLine As String
        Dim sTmp As String

        ' Reset file-totals:
        nFileSumAmount = 0
        ' XOKNET 26.07.2013
        nFileSumAmountSEK = 0
        nFileNoRecords = 0

        sLine = "11"        ' 1-2
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avs�ndarens kontonummer
                ' 06.07.2007
                ' Normally only one bankgirono pr file, then we can find accountno from first payment.
                ' BUT - for PGS Sweden we have splitted imported file in a TBIO-file and a LB-file, and
                ' we do not know what payments are LB and not. Thats why we have put accountno into
                ' XOKNET 02.12.2010 added XTRAPERSONELL_SE to next if
                If oBabelFile.Special = "PGS_SWEDEN" Or oBabelFile.Special = "XTRAPERSONELL_SE" Then
                    sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Cargo.I_Account, "-", ""), " ", ""), 10, "0") '3-12
                    sHeaderAccount = PadLeft(Replace(Replace(oBabelFile.Cargo.I_Account, "-", ""), " ", ""), 10, "0")
                    'ElseIf oBabelFile.Special = "RAMUDDEN_RETURN" Then
                    '    sLine = sLine & PadLeft("26", 10, "0") '3-12
                    '    sHeaderAccount = PadLeft("26", 10, "0")
                ElseIf oBabelFile.Special = "PRESSKONTAKTERNA_RETURN" Then
                    ' 10.02.2020 - added sI_Account, used by Presskontakterna
                    sLine = sLine & PadLeft(Replace(Replace(sI_account, "-", ""), " ", ""), 10, "0") '3-12
                    sHeaderAccount = PadLeft(Replace(Replace(sI_account, "-", ""), " ", ""), 10, "0")
                Else
                    sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 10, "0") '3-12
                    sHeaderAccount = PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 10, "0")
                End If
                sLine = sLine & Format(Date.Today, "yyMMdd")  '13-18
                sLine = sLine & "LEVERANT�RSBETALNINGAR" '19-40
                If oBabelFile.StatusCode = "02" Then
                    sTmp = Space(6)
                    ' "avregningsretur"
                    ' legg f�rste betalingens dato som betalningsdato;
                    If oBabelFile.Batches.Count > 0 Then
                        If oBabelFile.Batches(1).Payments.Count > 0 Then
                            sTmp = oBabelFile.Batches(1).Payments(1).DATE_Payment   ' 20161130
                            sTmp = Mid(sTmp, 3)                                     ' 161130
                        End If
                    End If
                    sLine = sLine & sTmp     ' Betalningsdatum 41-46
                    sLine = sLine & "1"      ' Redovosningskod 47
                    sLine = sLine & Space(12)   '48-59
                Else
                    sLine = sLine & Space(6) ' Betalningsdatum, blank, angis for hver post  41-46
                    '06.01.2017 - Added next If, first part is new fr Gjensidige.
                    If oBabelFile.StatusCode <> "00" And oBabelFile.StatusCode <> "01" Then
                        sLine = sLine & "6"      ' Redovosningskod 47
                        sLine = sLine & Space(12)   '48-59
                    Else
                        sLine = sLine & Space(13)   '47-59
                    End If
                End If
                ' added 12.04.2019
                If oBabelFile.Special = "RAMUDDEN_RETURN" Then
                    sLine = sLine & PadRight(oBabelFile.Batches(1).Payments(1).MON_TransferCurrency, 3, "0")       '60-62
                Else
                    sLine = sLine & "SEK"       '60-62
                End If
                sLine = sLine & Space(18)    '63-80
            End If
        End If
        'pad to 80 with 0
        sLine = PadLine(sLine, 80, "0")

        WriteLevBetFileStart = sLine

    End Function
    Function WriteLevBetPayment(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String) As String
        Dim sLine As String
        Dim oFreetext As FreeText
        Dim bBevakning As Boolean  ' Kreditnota med bevakning

        If oPayment.MON_InvoiceAmount < 0 Then
            ' 04.11.2015 - added next If
            If oPayment.PayCode = "177" Then
                bBevakning = True
            End If
            ' For Vismasolution, if ever on Pro:
            If IsThisVismaIntegrator() Then
                bBevakning = True
            End If
        Else
            bBevakning = False
        End If

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        ' XOKNET 26.07.2013
        nFileSumAmountSEK = nFileSumAmountSEK + oInvoice.MON_LocalAmount

        nFileNoRecords = nFileNoRecords + 1
        sTxt = vbNullString

        ' XNET 31.03.2014
        ' Did not previously concider OCR !
        ' added next If and code for OCR in Else
        If EmptyString(oInvoice.Unique_Id) Then
            For Each oFreetext In oInvoice.Freetexts
                ' 29.10.2012 If record "13" Rubrikpost from import, we have a standardtext for all payments in a batch
                ' This text is BOTH in oBatch.Ref_Bank AND as freetext (because we may have export in other formats)
                ' Make sure this text is not exported twice
                If Not EmptyString(oBatch.REF_Bank) Then
                    sTxt = sTxt & " " & Trim$(Replace(oFreetext.Text, oBatch.REF_Bank, ""))
                Else
                    sTxt = sTxt & " " & Trim$(oFreetext.Text)
                End If
            Next
        Else
            ' pick up OCR
            sTxt = oInvoice.Unique_Id
        End If
        sTxt = Trim(sTxt)

        ' 16.12.2014 Added Bevakning (TK16)
        If bBevakning Then
            sLine = "16"        '1-2
        ElseIf oInvoice.MON_InvoiceAmount >= 0 Then
            sLine = "14"        '1-2
        Else
            sLine = "15"        '1-2
        End If

        'If Len(Trim$(oPayment.E_Account)) > 0 Then
        ' changed 08.12.06 due to NSA
        ' added And Not oPayment.PayCode = "203" because of Salaries to account (for xTraPersonell)
        ' XNET 05.02.2015 - added "opayment.E_AccountType <> SE_Bankaccount And "
        If oPayment.E_AccountType <> BabelFiles.AccountType.SE_Bankaccount And Len(Trim$(oPayment.E_Account)) > 0 And Not oPayment.PayCode = "151" And Not oPayment.PayCode = "203" Then
            ' 22.06.2007 Added removal of - from E_Account
            sLine = sLine & PadLeft(Replace(oPayment.E_Account, "-", ""), 10, "0") ' 3-12  Mottaker bankgironummer
        Else
            sLine = sLine & "0000"      '3-6
            sLine = sLine & PadLeft(sPayNumber, 5, "0") & " "  ' 7-12  Utbetalningsnummer
        End If
        sLine = sLine & PadRight(sTxt, 25, " ")                '13-37  Spesifikation
        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 12, "0")  '38-49 Belopp, i �re

        ' 16.11.2016 - added for returnfiles;
        If oPayment.StatusCode = "02" Then
            sLine = sLine & "0"  ' 50 behandlingskod
            sLine = sLine & "0000000000"  '51-60

        Else
            If bBevakning Then
                ' Use GENAST to have cover first possible date
                ' IS this OK ?
                ' XNET 24.04.2015 NO, it's NOT OK - use a specific date (set from Visma, or from other inputsystem)
                'sLine = sLine & "GENAST" '50-55
                sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6) '50-55
            Else
                sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6) '50-55
            End If

            sLine = sLine & Space(5)    '56-60
        End If
        ' Changed 03.08.2007
        ' PGS Sweden needs REF_Own, but it is only present at Payment-level
        ' Added test that if no REF_Own at Invoice, then use REF_Own at Paymentlevel
        If Not EmptyString(oInvoice.REF_Own) Then
            ' as pre 03.08.2007
            sLine = sLine & PadRight(oInvoice.REF_Own, 20, " ")  '61-80
        Else
            ' new 03.08.2007
            sLine = sLine & PadRight(oPayment.REF_Own, 20, " ")  '61-80
        End If

        WriteLevBetPayment = sLine

    End Function

    ' XOKNET 29.10.2012 - several changes, take whole function
    Function WriteLevBetKontoinsetting(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String) As String
        Dim sLine As String
        Dim oFreetext As FreeText

        ' Removed 14.12.2006
        ' For each 40-record, there must also be a 14-record, where totals are added up!
        ' Add to Batch-totals:
        'nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        'nFileNoRecords = nFileNoRecords + 1
        sTxt = vbNullString

        For Each oFreetext In oInvoice.Freetexts
            ' 29.10.2012 If record "13" Rubrikpost from import, we have a standardtext for all payments in a batch
            ' This text is BOTH in oBatch.Ref_Bank AND as freetext (because we may have export in other formats)
            ' Make sure this text is not exported twice
            If Not EmptyString(oBatch.REF_Bank) Then
                sTxt = sTxt & " " & Trim$(Replace(oFreetext.Text, oBatch.REF_Bank, ""))
            Else
                sTxt = sTxt & " " & Trim$(oFreetext.Text)
            End If
        Next
        sTxt = Trim(sTxt)

        sLine = "40"        '1-2
        sLine = sLine & "0000"      '3-6
        sLine = sLine & PadLeft(sPayNumber, 5, "0") & " "  ' 7-12  Utbetalningsnummer
        sLine = sLine & Left$(oPayment.E_Account, 4) & PadLeft(Mid$(oPayment.E_Account, 5), 12, "0") '13 - 28 Mottagarens bankkontonummer. Position 13-16 clearingnummer, position 17-28 kontonumrets �vriga siffror, h�gerst�llt, nollutfyllt.
        If Not EmptyString(oPayment.Text_E_Statement) Then
            sLine = sLine & PadRight(oPayment.Text_E_Statement, 12, "0")  '29-40 Text on statement
        Else
            sLine = sLine & PadRight(sTxt, 12, "0")  '29-40 Text on statement
        End If
        If oPayment.PayCode = "203" Then ' Salary
            sLine = sLine & "L"      '41
        Else
            sLine = sLine & " "      '41
        End If

        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteLevBetKontoinsetting = sLine

    End Function
    Function WriteLevBetInfo(ByVal oPayment As Payment, ByVal sTxt As String) As String
        Dim sLine As String

        sLine = "25"        '1-2
        If Len(oPayment.E_Account) > 0 Then
            ' 22.06.2007 Added removal of - from E_Account
            sLine = sLine & PadLeft(Replace(oPayment.E_Account, "-", ""), 10, "0") ' 3-12  Mottaker bankgironummer
        Else
            sLine = "0000"      '3-6
            sLine = sLine & PadLeft(sPayNumber, 5, "0") & " "  ' 7-12  Utbetalningsnummer
        End If

        sLine = sLine & PadRight(sTxt, 50, " ")                '13-62  Spesifikation
        sLine = sLine & Space(18)    '63-80

        WriteLevBetInfo = sLine

    End Function
    Function WriteLevBetAdressOne(ByVal oPayment As Payment) As String
        Dim sLine As String

        sLine = "26"        '1-2
        sLine = sLine & "0000"      '3-6
        sLine = sLine & PadLeft(sPayNumber, 5, "0") & " "  ' 7-12  Utbetalningsnummer
        sLine = sLine & PadRight(UCase(oPayment.E_Name), 35, " ") '13-47  Mottakernamn
        sLine = sLine & Space(33)                          '48-80

        WriteLevBetAdressOne = sLine

    End Function
    Function WriteLevBetAdressTwo(ByVal oPayment As Payment) As String
        Dim sLine As String

        sLine = "27"        '1-2
        sLine = sLine & "0000"      '3-6
        sLine = sLine & PadLeft(sPayNumber, 5, "0") & " "  ' 7-12  Utbetalningsnummer
        If Len(Trim$(UCase(oPayment.E_Adr1))) > 0 Then
            sLine = sLine & PadRight(UCase(oPayment.E_Adr1), 35, " ") '13-47  Mottakeradr1
        Else
            sLine = sLine & PadRight(UCase(oPayment.E_Adr2), 35, " ") '13-47  Mottakeradr2
        End If
        sLine = sLine & PadRight(UCase(oPayment.E_Zip), 5, " ")  '48-52  Postnr
        If Len(oPayment.E_City) > 1 Then
            sLine = sLine & PadRight(UCase(oPayment.E_City), 20, " ") '53-72  Sted
        Else
            sLine = sLine & PadRight(UCase(oPayment.E_Adr2), 20, " ") '53-72  Sted
        End If
        sLine = sLine & Space(8)                           '73-80

        WriteLevBetAdressTwo = sLine

    End Function
    Function WriteLevBetFileEnd(ByVal oBabelFile As BabelFile) As String
        Dim sLine As String

        sLine = "29"        ' 1-2
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avs�ndarens kontonummer
                'sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 10, "0") '3-12
                ' Changed 23.05.2016 to be able to handle several companies in the Visma Business solution
                sLine = sLine & sHeaderAccount '3-12
                sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0") '13-20
                sLine = sLine & PadLeft(Str(Math.Abs(nFileSumAmount)), 12, "0") '21-32
                If nFileSumAmount < 0 Then
                    sLine = sLine & "-"                  '33-33 Minus if negative amount
                Else
                    sLine = sLine & " "                  '33-33 Otherwise blank
                End If
            End If
        End If
        'pad to 80 with 0
        sLine = PadLine(sLine, 80, " ")

        WriteLevBetFileEnd = sLine

    End Function
    Function WriteLevBetInt_Anvandarpost(ByVal oBabelFile As BabelFile) As String
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        ' XOKNET 26.07.2013
        nFileSumAmountSEK = 0
        ' XOKNET 26.07.2013
        nFileNoRecords = 1

        sLine = "0"        ' 1
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avs�ndarnummer
                sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 8, "0") '2-9
                sLine = sLine & Format(Date.Today, "yyMMdd")  '10-15
                ' XOKNET 26.07.2013
                If oBabelFile.VB_ProfileInUse Then
                    sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyName, 22, " ") '16-37
                    sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyZip & " " & oBabelFile.VB_Profile.CompanyCity, 35, " ") '38-72
                Else
                    If IsThisVismaIntegrator Then
                        sLine = sLine & PadRight(oBabelFile.Visma_FrmName, 22, " ") '16-37
                        sLine = sLine & Space(35) '38-72
                    Else
                        sLine = sLine & Space(22) '16-37
                        sLine = sLine & Space(35) '38-72
                    End If
                End If
                sLine = sLine & Space(6) ' Betalningsdatum, blank, angis for hver post  73-78
                sLine = sLine & "2"    '79
                sLine = sLine & Space(1)    '80
            End If
        End If
        ' XOKNET 26.07.2013
        'pad to 80 with blank
        sLine = PadLine(sLine, 80, " ")

        WriteLevBetInt_Anvandarpost = sLine

    End Function
    Function WriteLevBetInt_Namnpost(ByVal oPayment As Payment) As String
        Dim sLine As String
        ' XOKNET 26.07.2013
        nFileNoRecords = nFileNoRecords + 1

        sLine = "2"        '1
        ' XOKNET 26.07.2013
        If Not EmptyString(oPayment.Invoices(1).SupplierNo) Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).SupplierNo, 7, "0")   ' 2-8  L�penr mottager
        Else
            sLine = sLine & PadLeft(sPayNumber, 7, "0")  ' 2-8  L�penr mottager
        End If

        sLine = sLine & PadRight(UCase(oPayment.E_Name), 30, " ")  '9-38  Mottakernamn
        sLine = sLine & Space(35)                           '39-73  Mottakernamn2
        sLine = sLine & Space(7)                           '74-80

        sLine = PadLine(sLine, 80, " ")
        WriteLevBetInt_Namnpost = sLine

    End Function
    Function WriteLevBetInt_Adresspost(ByVal oPayment As Payment) As String
        Dim sLine As String

        ' XOKNET 15.08.2013
        nFileNoRecords = nFileNoRecords + 1

        sLine = "3"        '1
        ' XOKNET 26.07.2013
        If Not EmptyString(oPayment.Invoices(1).SupplierNo) Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).SupplierNo, 7, "0")   ' 2-8  L�penr mottager
        Else
            sLine = sLine & PadLeft(sPayNumber, 7, "0")   ' 2-8  L�penr mottager
        End If
        sLine = sLine & PadRight(UCase(oPayment.E_Adr1), 30, " ")  '9-38  Mottaker adr1
        ' XOKNET 06.02.2013, changed test under
        'If Len(Trim$(oPayment.E_Adr3)) = 0 Then
        If Len(Trim$(oPayment.E_Adr3)) > 0 Then
            sLine = sLine & PadRight(UCase(oPayment.E_Adr3), 35, " ")  '39-73
        Else
            sLine = sLine & PadRight(UCase(oPayment.E_Adr2), 35, " ")  '39-73
        End If
        ' Bankspesifik kode:
        ' XOKNET 26.07.2013
        ' probably not important, but changed to reflect ConvertX
        ' XNET 05.03.2014 - changed behaviour of pos 74
        'sLine = sLine & "0"     ' 74
        ' Kod f�r belastning, Pos 74
        'Kod �0� = Checkkonto (SEK)
        'Kod �1� = Valutakonto i aktuell valuta.
        ' XNET 20.03.2014 added  Or oPayment.MON_AccountCurrency = ""
        If oPayment.MON_AccountCurrency = "SEK" Or oPayment.MON_AccountCurrency = "" Then
            sLine = sLine & "0"     ' 74
        Else
            sLine = sLine & "1"     ' 74
        End If

        ' Statebank countrycode - use receicers countrycode
        sLine = sLine & PadRight(UCase(oPayment.E_CountryCode), 2, " ") '75-76
        ' Bankspesifik kode:
        sLine = sLine & " "     ' 77
        '==============================================
        ' Leverant�rsbetalingaer (SISU):
        ' ------------------------------
        '3.9 "Avgiftskod" inneb�r att avs�ndaren v�ljer
        'vem som skall betala svenska och utl�ndska
        'kostnader f�r uppdraget.
        'Kod "0" = avs�ndaren betalar svenska kostnader
        'Kod "2" = alla kostnader betalas av mottagaren.
        'Kod "3" = alla kostnader betalas av avs�ndaren
        '
        ' SPISU:
        ' ------
        ' �2�=Mottagaren st�r f�r sina bankkostnader (SHA)
        ' �0�=Avs�ndaren st�r f�r alla kostnader (OUR)
        '==============================================

        ' XOKNET 06.07.2015 added charges for SPISU
        If eFileType = vbBabel.BabelFiles.FileType.SPISU Then
            If oPayment.MON_ChargeMeAbroad Then
                sLine = sLine & "0"     ' 78 Kod "0" = alla kostnader betalas av avs�ndaren
            Else
                sLine = sLine & "2"     ' 78 Kod "2" = avs�ndaren betalar svenska kostnader (SHA)
            End If
        Else
            If oPayment.MON_ChargeMeAbroad Then
                sLine = sLine & "3"     ' 78 Kod "3" = alla kostnader betalas av avs�ndaren
            Else
                ' 04.05.2016 - Added possibility for "2" - Receiver pays all charges - (NOT valid in SEPA, but customers asks for it !!!)
                If oPayment.MON_ChargeMeDomestic = False Then
                    sLine = sLine & "2"     ' 78 Kod "2" = Mottaker betaler alle kostnader
                Else
                    sLine = sLine & "0"     ' 78 Kod "0" = avs�ndaren betalar svenska kostnader (SHA)
                End If
            End If
        End If

        ' XOKNET 06.07.2015 added charges for SPISU
        If eFileType = vbBabel.BabelFiles.FileType.SPISU Then
            sLine = sLine & " "     ' 79 Not in use
        Else
            ' Bankspesifik kode:
            ' XOKNET 26.07.2013
            ' probably not important, but changed to reflect ConvertX
            sLine = sLine & "1"     ' 79 Betalningsform
        End If
        If oPayment.Priority Then
            sLine = sLine & "1"     ' 80 Betalningskod, Express
            'ElseIf Len(Trim$(oPayment.E_Account)) = 0 Then
            '    sLine = sLine & "2"     ' 80 Betalningskod, check
            ' XOKNET 21.12.2015 - correction, 2 = Konsernintern
        ElseIf oPayment.ToOwnAccount Then
            sLine = sLine & "2"     ' 80 Kod �2� = koncernintern betalning
        Else
            sLine = sLine & "0"     ' 80 Betalningskod, normal
        End If

        WriteLevBetInt_Adresspost = sLine

    End Function
    Function WriteLevBetInt_Bankpost(ByVal oPayment As Payment) As String
        Dim sLine As String

        ' XOKNET 15.08.2013
        nFileNoRecords = nFileNoRecords + 1

        sLine = "4"        '1
        ' XOKNET 26.07.2013
        If Not EmptyString(oPayment.Invoices(1).SupplierNo) Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).SupplierNo, 7, "0")   ' 2-8  L�penr mottager
        Else
            sLine = sLine & PadLeft(sPayNumber, 7, "0")   ' 2-8  L�penr mottager
        End If
        ' NB Bankspesifikt - Dette passer for F�reningssparbanken!!!
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 12, " ")  '9-20  SWIFT

        ' XokNET 26.08.2014 nesten resten av funksjonen
        sLine = sLine & PadRight(Replace(oPayment.E_Account, " ", ""), 30, " ") '21-50 Iban kontonr
        'sLine = sLine & PadRight(UCase(oPayment.BANK_Name), 30, " ")  '51-80 Bankens navn
        ' for certain countries, fill in clearingcode
        ' Australia, Canada, New Zeeland, Singapore, South Afrika, Storbritania, Turkey, USA
        If oPayment.BANK_CountryCode = "AU" Or oPayment.BANK_CountryCode = "CA" Or oPayment.BANK_CountryCode = "NZ" Or oPayment.BANK_CountryCode = "SG" Or _
            oPayment.BANK_CountryCode = "ZA" Or oPayment.BANK_CountryCode = "GB" Or oPayment.BANK_CountryCode = "TR" Or oPayment.BANK_CountryCode = "US" Then
            ' But if IBAN is given, send no clearingcode
            If IsIBANNumber(oPayment.E_Account, False, False) Then
                sLine = sLine & Space(30)  '51-80 blank
            Else
                sLine = sLine & PadRight(Replace(oPayment.BANK_BranchNo, " ", ""), 30, " ")  '51-80 bank clearingcode
            End If
        Else
            sLine = sLine & Space(30)  '51-80 blank
        End If

        WriteLevBetInt_Bankpost = sLine
    End Function

    Function WriteLevBetInt_Kreditnotpost(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String, ByVal bZeroAmount As Boolean) As String  ' XOKNET 26.07.2013
        Dim sLine As String
        Dim sAmount As String
        ' XOKNET 15.08.2013 Take whole function !

        If Not bZeroAmount Then
            nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
            ' XOKNET 26.07.2013
            nFileSumAmountSEK = nFileSumAmountSEK + oInvoice.MON_LocalAmount
        End If
        nFileNoRecords = nFileNoRecords + 1

        sLine = "5"        '1
        ' XOKNET 26.07.2013
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 7, "0")   ' 2-8  L�penr mottager
        Else
            sLine = sLine & PadLeft(sPayNumber, 7, "0")  ' 2-8  L�penr mottager
        End If
        sLine = sLine & PadRight(sTxt, 25, " ")             '9-33 Betalningsanledning

        sAmount = LTrim(Str(Math.Abs(oInvoice.MON_LocalAmount)))                          '34-44 Bel�p i SEK
        ' Use last char as signed
        Select Case Right$(sAmount, 1)
            Case "0"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "-"
            Case "1"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "J"
            Case "2"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "K"
            Case "3"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "L"
            Case "4"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "M"
            Case "5"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "N"
            Case "6"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "O"
            Case "7"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "P"
            Case "8"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "Q"
            Case "9"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "R"
        End Select
        If bZeroAmount Then
            sAmount = 0
        End If
        sLine = sLine & PadLeft(sAmount, 11, "0")                            '34-44 Bel�p i SEK

        sLine = sLine & StrDup(10, "0")                          '45-54 Ikke i bruk her
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") '55-57 Valutakod
        ' Put Siste bevakningsdata 30 days ahead!
        'sLine = sLine & Format(Date + 30, "YYMMDD")         '58-63 Siste bevakningsdag
        ' Nei, bruk den dato vi har. Den f�r heller settes riktig i importrutinen eller TreatSpecial
        sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6)         '58-63 Siste bevakningsdag
        ' bankspesifikt
        sLine = sLine & Space(1)                           '64
        sLine = sLine & Space(1)                           '65

        sAmount = LTrim(Str(Math.Abs(oInvoice.MON_InvoiceAmount)))
        ' Use last char as signed
        Select Case Right$(sAmount, 1)
            Case "0"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "-"
            Case "1"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "J"
            Case "2"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "K"
            Case "3"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "L"
            Case "4"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "M"
            Case "5"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "N"
            Case "6"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "O"
            Case "7"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "P"
            Case "8"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "Q"
            Case "9"
                sAmount = Left$(sAmount, Len(sAmount) - 1) & "R"
        End Select
        If bZeroAmount Then
            sAmount = 0
        End If
        sLine = sLine & PadLeft(sAmount, 13, "0")  '66-78 FakturaBelopp, 2 des.

        ' Bankspesifik kode:
        sLine = sLine & " "     ' 79
        ' XOKNET 26.07.2013
        sLine = sLine & "0"     ' 80

        WriteLevBetInt_Kreditnotpost = sLine

    End Function
    Function WriteLevBetInt_Betalningspost(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String, ByVal bZeroAmount As Boolean) As String  '' XOKNET 26.07.2013
        Dim sLine As String
        Dim sAmount As String

        ' XOKNET 15.08.2013 added If
        If Not bZeroAmount Then
            nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
            ' XOKNET 26.07.2013
            nFileSumAmountSEK = nFileSumAmountSEK + oInvoice.MON_LocalAmount
        End If
        nFileNoRecords = nFileNoRecords + 1

        sLine = "6"        '1
        ' XOKNET 26.07.2013
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 7, "0")   ' 2-8  L�penr mottager
        Else
            sLine = sLine & PadLeft(sPayNumber, 7, "0")  ' 2-8  L�penr mottager
        End If
        sLine = sLine & PadRight(sTxt, 25, " ")             '9-33 Betalningsanledning
        ' XOKNET 26.07.2013
        If bZeroAmount Then
            ' we will some times need to add a 0-amount payment, to be able to pass ownref
            sLine = sLine & StrDup(11, "0")                           '34-44 Bel�p i SEK,
        Else
            sLine = sLine & PadLeft(oInvoice.MON_LocalAmount, 11, "0")                            '34-44 Bel�p i SEK,
        End If
        sLine = sLine & StrDup(10, "0")                          '35-54 Ikke i bruk her
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") '55-57 Valutakod
        ' Put Siste bevakningsdata 30 days ahead!
        sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6)       '58-63 Betalningsdag, YYMMDD
        ' bankspesifikt
        ' XOKNET 26.07.2013
        sLine = sLine & "0"                           '64
        sLine = sLine & Space(1)                           '65

        sAmount = Str(Math.Abs(oInvoice.MON_InvoiceAmount))
        ' XOKNET 26.07.2013
        If bZeroAmount Then
            ' we will some times need to add a 0-amount payment, to be able to pass ownref
            sAmount = 0
        End If
        sLine = sLine & PadLeft(sAmount, 13, "0")  '66-78 FakturaBelopp, 2 des.

        ' Bankspesifik kode:
        sLine = sLine & " "     ' 79
        ' XOKNET 26.07.2013
        sLine = sLine & "0"     ' 80

        WriteLevBetInt_Betalningspost = sLine

    End Function
    Function WriteLevBetInt_Riksbankpost(ByVal oInvoice As Invoice) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "7"        '1
        ' XOKNET 26.07.2013
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 7, "0")   ' 2-8  L�penr mottager
        Else
            sLine = sLine & PadLeft(sPayNumber, 7, "0")  ' 2-8  L�penr mottager
        End If
        ' NB Bankspesifikt - Dette passer for F�reningssparbanken!!!
        ' XOKNET 26.07.13 changed from "0" to " ", added trim, padleft
        sLine = sLine & PadLeft(Trim(oInvoice.STATEBANK_Code), 3, " ")   '9-11  Riksbankkode
        sLine = sLine & Space(69)       '12-80, Bankspecific, not used by F�reningssparbanken

        WriteLevBetInt_Riksbankpost = sLine

    End Function

    Function WriteLevBetInt_Avstamningspost(ByVal oBabelFile As BabelFile) As String
        Dim sLine As String
        nFileNoRecords = nFileNoRecords + 1

        ' error in totals !!!!
        sLine = "9"        ' 1
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avs�ndarnummer
                sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 8, "0") '2-9
                'sLine = sLine & PadLeft("0", 12, "0") ' 10-21 Sum reskontrobel�p
                ' XOKNET 26.07.2013
                sLine = sLine & PadLeft(nFileSumAmountSEK, 12, "0") ' 10-21 Sum reskontrobel�p
                sLine = sLine & Space(10)                        ' 22-31 Blank
                sLine = sLine & Space(12)                        ' 32-43 Blank
                sLine = sLine & PadLeft(nFileNoRecords, 12, "0") ' 44-55 Total antall poster
                sLine = sLine & Space(8)                        ' 56-63 Blank
                sLine = sLine & PadLeft(nFileSumAmount, 15, "0") ' 64-78 Hashtotal fakturabel�p
                sLine = sLine & Space(2)    '79-80
            End If
        End If
        'pad to 80 with 0
        sLine = PadLine(sLine, 80, "0")

        WriteLevBetInt_Avstamningspost = sLine

    End Function
    ' XOKNET 29.10.2012 added Rubrikpost (13)
    Function WriteLevBetRubrikpost(ByVal oBatch As Batch) As String
        Dim sLine As String

        sLine = "13"        '1-2
        'sLine = sLine & PadRight(oBatch.REF_Bank, 25, " ")   '3-27  Spesifikasjon for samtlige betalinger
        ' 27.10.2015 Changed to reflect complete line imported from other formats, or from Lev.bet
        sLine = sLine & PadRight(oBatch.REF_Bank, 78, " ")   '3-27  Spesifikasjon for samtlige betalinger
        'sLine = sLine & Space(52)

        WriteLevBetRubrikpost = sLine

    End Function
    Function WriteLevBetPaymentPlusgirot(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String) As String
        Dim sLine As String
        Dim oFreetext As Freetext

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileSumAmountSEK = nFileSumAmountSEK + oInvoice.MON_LocalAmount

        nFileNoRecords = nFileNoRecords + 1
        sTxt = vbNullString

        ' XokNET 31.03.2014
        ' Did not previously concider OCR !
        ' added next If and code for OCR in Else
        If EmptyString(oInvoice.Unique_Id) Then
            For Each oFreetext In oInvoice.Freetexts
                ' 29.10.2012 If record "13" Rubrikpost from import, we have a standardtext for all payments in a batch
                ' This text is BOTH in oBatch.Ref_Bank AND as freetext (because we may have export in other formats)
                ' Make sure this text is not exported twice
                If Not EmptyString(oBatch.REF_Bank) Then
                    sTxt = sTxt & " " & Trim$(Replace(oFreetext.Text, oBatch.REF_Bank, ""))
                Else
                    sTxt = sTxt & " " & Trim$(oFreetext.Text)
                End If
            Next
        Else
            ' pick up OCR
            sTxt = oInvoice.Unique_Id
        End If

        sTxt = Trim(sTxt)

        If oInvoice.MON_InvoiceAmount >= 0 Then
            sLine = "54"        '1-2
        Else
            ' Credits to Plusgirot not allowed
            Err.Raise(12005, "WriteLevBetPaymentPlusgirot", LRS(12005) & vbCrLf & vbCrLf & "Account: " & oPayment.E_Account & "Amount: " & Str(oInvoice.MON_InvoiceAmount / 100))
        End If
        sLine = sLine & PadLeft(Replace(oPayment.E_Account, "-", ""), 10, "0") ' 3-12  Mottaker plusgironummer
        sLine = sLine & PadRight(sTxt, 25, " ")                '13-37  Spesifikation
        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 12, "0")  '38-49 Belopp, i �re
        sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6) '50-55
        sLine = sLine & Space(5)    '56-60
        If Not EmptyString(oInvoice.REF_Own) Then
            sLine = sLine & PadRight(oInvoice.REF_Own, 20, " ")  '61-80
        Else
            sLine = sLine & PadRight(oPayment.REF_Own, 20, " ")  '61-80
        End If

        WriteLevBetPaymentPlusgirot = sLine
    End Function
    Function WriteLevBetInfoPlusgirot(ByVal oPayment As Payment, ByVal sTxt As String) As String
        Dim sLine As String

        sLine = "65"        '1-2
        If Len(oPayment.E_Account) > 0 Then
            sLine = sLine & PadLeft(Replace(oPayment.E_Account, "-", ""), 10, "0") ' 3-12  Mottaker plusgironummer
        End If

        sLine = sLine & PadRight(sTxt, 35, " ")                '13-47  Spesifikation
        sLine = sLine & Space(33)    '48-80

        WriteLevBetInfoPlusgirot = sLine

    End Function


End Module
