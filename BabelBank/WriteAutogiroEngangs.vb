Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module WriteAutoGiroEngangs
	Dim sLine As String ' en output-linje
	Dim i As Double
	Dim oBatch As Batch
	' Vi m� dimme noen variable for
	' - batchniv�: sum bel�p, antall records, antall transer, f�rste dato, siste dato
	Dim nBatchNoRecords, nBatchSumAmount, nBatchNoTransactions As Double
	Dim dBatchLastDate, dBatchFirstDate As Date
	' - fileniv�: sum bel�p, antall records, antall transer, siste dato
	Dim nFileNoRecords, nFileSumAmount, nFileNoTransactions As Double
    Function WriteAutogiroEngangsFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String) As Object
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim sContractNo As String

        Try

            ' new 19.12.02, due to problems running two Autogiro-export connescutive (without leaving BB.EXE)
            nBatchSumAmount = 0
            nBatchNoRecords = 0
            nBatchNoTransactions = 0
            dBatchLastDate = CDate(Nothing)
            dBatchFirstDate = CDate(Nothing)
            nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0

            oFs = New Scripting.FileSystemObject

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            sContractNo = ""

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord89 = False
                nExistingLines = 0
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 8) = "NY000089" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord89 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                If Not bFoundRecord89 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    MsgBox("The existing file is not a BBS-file")
                    'UPGRADE_WARNING: Couldn't resolve default property of object WriteAutogiroEngangsFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    WriteAutogiroEngangsFile = False
                    Exit Function
                    'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nFileNoTransactions = Val(Mid(sLine, 9, 8))
                    'Subtract 1 because we have deleted the 89-line
                    nFileNoRecords = Val(Mid(sLine, 17, 8)) - 1
                    nFileSumAmount = Val(Mid(sLine, 25, 17))
                    'Kill'em all
                    oFileRead.Close()
                    'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileRead = Nothing
                    oFileWrite.Close()
                    'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects to
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments

                        If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                            ' Only process payments marked as Autogiro (611 and 612)!
                            ' This must be done because we are useing this exportmodule
                            ' to export Autogiro before Matching, when oBabelFiles are filled
                            ' with many other payments than Autogiro
                            If oPayment.PayCode = "611" Or oPayment.PayCode = "612" Then

                                '''''''---If oPayment.VB_FilenameOut_ID = iFormat_ID Then

                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBabel = True
                                            End If
                                        Else
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_AutogiroEngangsFileStart(oBabel, sAdditionalID)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1

                            sLine = Wr_AutogiroEngangsBatchStart(oBatch, sContractNo)
                            oFile.WriteLine((sLine))

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If
                                If bExportoPayment Then
                                    j = j + 1

                                    For Each oInvoice In oPayment.Invoices
                                        ' Add to transactionno
                                        nTransactionNo = nTransactionNo + 1
                                        sLine = Wr_AutogiroEngangsPayOne(oPayment, oInvoice, nTransactionNo)
                                        oFile.WriteLine((sLine))
                                        sLine = Wr_AutogiroEngangsPayTwo(oPayment, nTransactionNo)
                                        oFile.WriteLine((sLine))
                                        oPayment.Exported = True
                                    Next oInvoice 'invoice
                                End If

                            Next oPayment ' payment

                            sLine = Wr_AutogiroEngangsBatchEnd()
                            oFile.WriteLine((sLine))

                            If StringToDate((oBatch.DATE_Production)) > dProductionDate Then
                                dProductionDate = StringToDate((oBatch.DATE_Production))
                            End If

                        End If
                    Next oBatch 'batch

                End If

            Next oBabel


            If nFileNoRecords > 0 Then
                sLine = Wr_AutogiroEngangsFileEnd(dProductionDate)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteAutogiroEngangsFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteAutogiroEngangsFile = True

    End Function
    Function Wr_AutogiroEngangsFileStart(ByRef oBabelFile As BabelFile, ByRef sAdditionalID As String) As String

        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0

        sLine = "NY000010"
        If sAdditionalID <> "" Then
            sLine = sLine & PadLeft(sAdditionalID, 8, "0")
        Else
            sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' dataavsender
        End If

        If Len(oBabelFile.File_id) = 0 Then
            ' Construct unique Oppdragsnr, 7 digits; mddhMMs
            sLine = sLine & Right(Str(Month(Now)), 1) & Right("0" & Trim(Str(VB.Day(Now))), 2) & Right(CStr(Hour(Now)), 1) & Right("0" & Trim(Str(Minute(Now))), 2) & Right(CStr(Second(Now)), 1)
            ' Must show Forsendelsesnr, used by sender as reference to BBS;
            MsgBox("Forsendelsesnr." & Right(sLine, 7), , "Autogiro")
        ElseIf Len(oBabelFile.File_id) < 8 Then
            sLine = sLine & PadLeft(Mid(oBabelFile.File_id, 1, 7), 7, "0") ' forsendelsesnr
        Else
            sLine = sLine & Right(oBabelFile.File_id, 7)
        End If

        If Len(oBabelFile.IDENT_Receiver) = 0 Then
            sLine = sLine & "00008080"
        Else
            sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Receiver, 1, 8), 8, "0") ' datamottaker
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_AutogiroEngangsFileStart = sLine

    End Function
    Public Function Wr_AutogiroEngangsPayOne(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef nTransactionNo As Double) As String
        Dim sLine As String

        ' Add to Batch-totals:
        nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
        nBatchNoRecords = nBatchNoRecords + 2 'both 30 and 31, payment1 and 2
        nBatchNoTransactions = nBatchNoTransactions + 1


        sLine = "NY02"
        sLine = sLine & "02" ' Uten melding
        sLine = sLine & "30"
        sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        If StringToDate((oPayment.DATE_Payment)) > Now Then
            If StringToDate((oPayment.DATE_Payment)) > System.Date.FromOADate(Now.ToOADate + 393) Then
                ' hvis vi har lagret en dato, bruk denne. Max 13 mnd fram i tid
                oPayment.DATE_Payment = DateToString(System.Date.FromOADate(Now.ToOADate + 393))
            End If
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "ddmmyy")
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "ddmmyy")
            oPayment.DATE_Payment = DateToString(Now)
        End If
        ' Payers accountno
        sLine = sLine & oPayment.E_Account

        sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 17, "0") ' Amount
        ' New 04.01.06
        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If Len(Trim(oPayment.Invoices(1).Unique_Id)) > 0 Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & PadLeft(oPayment.Invoices(1).Unique_Id, 25, " ")
        Else
            sLine = sLine & Space(25)
        End If

        sLine = PadLine(sLine, 80, "0")

        If StringToDate((oPayment.DATE_Payment)) > dBatchLastDate Then
            dBatchLastDate = StringToDate((oPayment.DATE_Payment))
        End If
        If StringToDate((oPayment.DATE_Payment)) < dBatchFirstDate Then
            dBatchFirstDate = StringToDate((oPayment.DATE_Payment))
        End If


        Wr_AutogiroEngangsPayOne = sLine


    End Function

    Public Function Wr_AutogiroEngangsPayTwo(ByRef oPayment As Payment, ByRef nTransactionNo As Double) As String
        Dim sLine As String

        sLine = "NY02"
        ' trekker transtype fra Babelbanks interne kodesett
        sLine = sLine & "02" ' Uten melding
        sLine = sLine & "31"
        sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        sLine = sLine & PadLeft(oPayment.E_Name, 10, "0") ' shortname
        sLine = sLine & PadRight(Mid(oPayment.REF_Own, 1, 25), 25, " ") ' Ownref
        ' Changed 04.01.06sLine = sLine & PadRight(Mid(oPayment.REF_Bank1, 1, 25), 25, " ") ' Ref receiver
        sLine = sLine & PadRight(Mid(oPayment.Text_E_Statement, 1, 25), 25, " ") '

        sLine = PadLine(sLine, 80, "0")

        Wr_AutogiroEngangsPayTwo = sLine

    End Function

    Public Function Wr_AutogiroEngangsFileEnd(ByRef dProductionDate As Date) As String

        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1 ' also count 89-recs

        sLine = "NY000089"

        sLine = sLine & PadLeft(Str(nFileNoTransactions), 8, "0") ' No of transactions in file (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0") ' No of records in file (including 20 and 88)
        sLine = sLine & PadLeft(Str(nFileSumAmount), 17, "0") ' Total amount in file

        If dProductionDate > DateSerial(CInt("1999"), CInt("12"), CInt("01")) Then
            sLine = sLine & VB6.Format(dProductionDate, "DDMMYY")
        Else
            sLine = sLine & VB6.Format(Now, "ddmmyy") ' BBS date (Productiondate of file)
        End If

        ' pad with 0 to 80
        sLine = PadLine(sLine, 80, "0")

        Wr_AutogiroEngangsFileEnd = sLine

    End Function
    Function Wr_AutogiroEngangsBatchStart(ByRef oBatch As Batch, ByRef sContractNo As String) As String
        Dim sLine As String

        ' Reset Batch-totals:
        nBatchSumAmount = 0
        nBatchNoRecords = 1
        nBatchNoTransactions = 0
        dBatchLastDate = System.Date.FromOADate(Now.ToOADate - 1000)
        dBatchFirstDate = System.Date.FromOADate(Now.ToOADate + 1000)

        sLine = "NY020020"
        If sContractNo <> "" Then
            sLine = sLine & PadLeft(sContractNo, 9, "0") ' avtaleid
        Else
            sLine = sLine & PadLeft(oBatch.REF_Own, 9, "0") ' avtaleid
        End If
        If oBatch.Batch_ID = "" Then
            ' Construct unique Oppdragsnr, 7 digits; mddhMMs
            sLine = sLine & Right(Str(Month(Now)), 1) & Right("0" & Trim(Str(VB.Day(Now))), 2) & Right(CStr(Hour(Now)), 1) & Right("0" & Trim(Str(Minute(Now))), 2) & Right(CStr(Second(Now)), 1)
        Else
            sLine = sLine & PadLeft(Mid(oBatch.Batch_ID, 1, 7), 7, "0") ' oppdragsnr
        End If
        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sLine = sLine & PadLeft(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, "0") ' oppdragskonto

        sLine = PadLine(sLine, 80, "0")

        Wr_AutogiroEngangsBatchStart = sLine

    End Function

    Private Function Wr_AutogiroEngangsBatchEnd() As String
        Dim sLine As String
        nBatchNoRecords = nBatchNoRecords + 1 'Also count 88-recs

        ' Add to File-totals:
        nFileSumAmount = nFileSumAmount + nBatchSumAmount
        nFileNoRecords = nFileNoRecords + nBatchNoRecords
        nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions

        sLine = "NY020088"
        sLine = sLine & PadLeft(Str(nBatchNoTransactions), 8, "0") ' No of transactions in batch (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nBatchNoRecords), 8, "0") ' No of records in batch (including 20 and 88)

        sLine = sLine & PadLeft(Str(nBatchSumAmount), 17, "0") ' Total amount in batch
        sLine = sLine & VB6.Format(dBatchFirstDate, "ddmmyy") ' First date in batch,DDMMYY
        sLine = sLine & VB6.Format(dBatchLastDate, "ddmmyy") ' First date in batch, DDMMYY

        sLine = PadLine(sLine, 80, "0")

        Wr_AutogiroEngangsBatchEnd = sLine

    End Function
End Module
