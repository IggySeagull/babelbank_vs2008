Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_510_DayTotals
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim iBabelFile_ID As Integer
    Dim iBatch_ID As Integer
    Dim sBreakField As String

    Dim iDetailLevel As Integer
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim bSQLServer As Boolean = False
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_510_DayTotals_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize

        Fields.Add("BreakField")

    End Sub

    Private Sub rp_510_DayTotals_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_510_DayTotals_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        iBabelFile_ID = -1
        iBatch_ID = -1
        bShowBatchfooterTotals = True
        sBreakField = ""

        Select Case iBreakLevel
            'Only 2 possibilities

            Case 0 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True

            Case 999
                bShowDATE_Production = True
                bShowClientName = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = True

        End Select

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40076)   ' Day Totals
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 

        'Detail
        Me.lblIB.Text = LRS(40118) '"Inng�ende saldo" 
        Me.lblImported.Text = LRS(40119) '"Innlest BabelBank" 
        Me.lblTotalAfterImport.Text = LRS(40120) '"Total etter import" 
        Me.lblProductionToday.Text = LRS(40121) '"Dagens produksjon" 
        Me.lblOCR.Text = LRS(40122) '"OCR-poster" 
        Me.lblMatched.Text = LRS(40123) '"Postert reskontro" 
        Me.lblMatchedGL.Text = LRS(40124) '"Postert hovedbok" 
        Me.lblUB.Text = LRS(40125) '"Utg�ende saldo" 
        Me.lblUnMatched.Text = LRS(40126) '"�pne poster" 
        Me.lblDiff.Text = LRS(40127) '"Differanse" 

        If sSpecial = "CONECTO_OP" Then
            Me.lblUnmatchedERP.Text = "Saldo 99999994"
            Me.lblUnmatchedERP.Visible = True
            Me.txtUnmatchedERP.DataField = "= Matched_GL"
            Me.txtUnmatchedERP.Visible = True
            Me.lblDiffERP.Text = "Avvik: "
            Me.lblDiffERP.Visible = True
            Me.txtDiffERP.DataField = "= Matched_GL - Unmatched_BB"
            Me.txtDiffERP.Visible = True
            Me.Line4.Visible = True
            Me.Line5.Visible = True
        End If

    End Sub

    Private Sub rp_510_DayTotals_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        Dim sDATE_Production As String

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = Me.Fields("AccountNo").Value
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            'START WORKING HERE
            If Not IsDBNull(Me.Fields("AccountNo").Value) Then
                sI_Account = Me.Fields("AccountNo").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            sDATE_Production = Me.Fields("DATE_Production").Value
            Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))



        End If

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
