Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("CAppAdmExport_NET.CAppAdmExport")> Public Class CAppAdmExport
	
	' Input object
    Private oBabelFiles As vbBabel.BabelFiles
	Private oBabelFile As vbBabel.BabelFile
	Private oBatch As vbBabel.Batch
	Private oPayment As vbBabel.Payment
	Private oInvoice As vbBabel.Invoice
    Private lBabelFileIndex As Integer
	Private lBatchIndex As Integer
	Private lPaymentIndex As Integer
	Private lInvoiceIndex As Integer
	Private iFilesetupOut As Short
	Private iFilenameInNo As Short
	Private sEDIFormat As String
	Private bCreateNewLIN As Boolean
	'Output object
    Private oFs As Scripting.FileSystemObject
	Private oFile As Scripting.TextStream
	
	Private sSegmentSep, sElementSep, sGroupSep As String
	Private sEscape, sDecimalSep, sSegmentLen As String
	
	Private lUNHCounter As Integer
	Private lLINCounter As Integer
	Private lSEQCounter As Integer
	'Private lDOCCounter As Long
	Private lSegmentCounter As Integer
	
	Private bJustWarning As Boolean
	Private bRaiseValidationError As Boolean
	
    Private eReceivingBank As vbBabel.BabelFiles.Bank

    Private sSpecial As String
    Private bWriteWithoutCrLf As Boolean
    Private bWrite80 As Boolean = False
    Private sI_AccountToBeExported As String
    Private sClientNoToBeExported As String

    Private sDnBNOR_INPSString As String

    'Mappingfiles
    Private xmlMapDoc As New MSXML2.DOMDocument40
    Private mrEDISettingsExport As New CEdiSettingsExport
    Private xmlParameterDoc As New MSXML2.DOMDocument40

    Private Const E_Info As Short = 0
    Private Const I_Info As Short = 1
    Private Const CorrBank As Short = 2
    'Mapping object
    Friend Function Init(ByRef oImportObject As vbBabel.BabelFiles, ByRef iFileOutID As Short, ByRef iFilenameInNumber As Short, ByRef sFilenameOut As String, ByRef sMappingFile As String, ByRef sTagSettingsFile As String, ByRef sExportFormat As String, ByRef sTreatSpecial As String, ByRef sI_Account As String, ByRef sClientNo As String) As Boolean

        On Error GoTo errorHandler

        oBabelFiles = oImportObject
        iFilesetupOut = iFileOutID
        iFilenameInNo = iFilenameInNumber
        sEDIFormat = sExportFormat
        bRaiseValidationError = False
        oFs = New Scripting.FileSystemObject

        xmlParameterDoc.load(My.Application.Info.DirectoryPath & "\Parameter.xml")
        oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
        'If Not EmptyString(sMappingFile) Then
        xmlMapDoc.load(sMappingFile)
        'End If

        If Not mrEDISettingsExport.Init(sTagSettingsFile) Then ', sValues) Then
            GoTo errorHandler
        End If
        mrEDISettingsExport.MapDoc = xmlMapDoc
        sSpecial = sTreatSpecial
        sI_AccountToBeExported = sI_Account
        sClientNoToBeExported = sClientNo

EndOK:
        Init = True
        Exit Function

errorHandler:
        Init = False

    End Function
    Friend Function CreateEDIFile(ByRef sCompanyNo As String, ByRef eBank As vbBabel.BabelFiles.Bank, ByRef sFormat As String) As Boolean
        Dim bx As Boolean
        Dim sUNBReference As String
        Dim sUNHReference As String
        Dim sBGMSegment As String
        Dim lOriginalUNHCounter As Integer
        Dim sOldUNHReference As String
        Dim sUNHAppendix As String
        Dim sUNZReference As String
        Dim sNADSegment As String
        Dim sAvtaleNr As String
        Dim sReferanse As String
        Dim sEDIBruker As String
        Dim sIdentSender As String
        Dim sIdentReceiver As String
        'Dim aFreetextArray() As String
        Dim sStatebankText, sLocalFreetext, sStatebankCode As String
        Dim sLocalREF_Own As String
        Dim oFreeText As vbBabel.Freetext
        Dim nAmountInSEQ As Double
        Dim lCounter, lArrayCounter As Integer
        Dim lMaxLengthOfFreetext As Integer
        Dim bThisIsOverforselFraKontoIUtland As Boolean
        Dim bTheInformationIsStructured As Boolean
        Dim sOldAccountNo, sI_SWIFTAddr As String
        Dim sLocalPayCode As String
        Dim bWriteUNH, bUNBWritten, bUNHWritten As Boolean
        Dim bWriteUNZ As Boolean
        Dim bThisIsFirstUNH As Boolean
        Dim bWriteUNTForEachBabelFile As Boolean
        Dim bThisIsKID As Boolean
        Dim bNewMessage As Boolean
        Dim bNewBatch As Boolean
        Dim lPaymentCounter As Integer
        Dim sAdditionalNo, sEDIMessageNo As String
        Dim bReturnValue As Boolean
        Dim bNewCodeForGjensidige As Boolean
        Dim iPos As Integer ' 11.05.2010 Integer
        '03.05.2010 - the use of oGroupPayment implemented for PAYMUL-export to SEB
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim bNewLIN As Boolean
        Dim bGroupPayments As Boolean
        Dim lCounter2 As Integer
        Dim oFilesetup As vbBabel.FileSetup ' added 11.05.2010

        'The next lines must be removed when new code for Gjensidige is implemented
        'Marked 2CONTRL - remove And bNewCodeForGjensidige
        'SLETTTTTTTTTES
        bNewCodeForGjensidige = False
        'bNewCodeForGjensidige = True

        bWriteWithoutCrLf = False

        'If sSpecial = VISMA_FOKUS we don't want to validate some parameters. We want to send the payment to
        ' to the bank with the errors included.
        'All info necessary shall be stated in the importfile (also paytype)
        If sSpecial = "VISMA_FOKUS" Then
            bRaiseValidationError = False
        ElseIf eBank = vbBabel.BabelFiles.Bank.SEB_NO Then
            bRaiseValidationError = False
        Else
            bRaiseValidationError = True
        End If

        On Error GoTo ErrorCreateEDIFile

        bJustWarning = True

        eReceivingBank = eBank

        lUNHCounter = 0
        lSEQCounter = 0
        sUNBReference = ""
        sUNHReference = ""
        sUNZReference = ""
        bThisIsFirstUNH = True
        Select Case eBank

            Case vbBabel.BabelFiles.Bank.BBS
                bReturnValue = CreateBBSEDIFile(sCompanyNo, sFormat)

            Case vbBabel.BabelFiles.Bank.DnBNOR_INPS
                bReturnValue = CreateDnBNOR_INPS_EDIFile(sCompanyNo, sFormat)

            Case vbBabel.BabelFiles.Bank.DnB
                If sSpecial = "VISMA_FOKUS" Then
                    'bWriteWithoutCrLf = True
                End If
                bReturnValue = CreateDnBNOR_EDIFile(sCompanyNo, sFormat)

            Case vbBabel.BabelFiles.Bank.SEB_SE
                bReturnValue = CreateSEB_SE_EDIfile(sCompanyNo, sFormat)

                'XNET - 22.11.2011 - Added next case
            Case vbBabel.BabelFiles.Bank.HSBC_UK
                bReturnValue = CreateHSBC_EDIFile(sCompanyNo, sFormat)

                'XNET - 22.01.2012 - Added next case
            Case vbBabel.BabelFiles.Bank.Nordea_eGateway
                bReturnValue = CreateNordea_eGateway_EDIFile(sCompanyNo, sFormat)


            Case vbBabel.BabelFiles.Bank.BBS

                Select Case sFormat
                    Case "CREMUL"

                        sOldAccountNo = ""

                        sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                        sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                        sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                        sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                        sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                        sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                        oFile.Write(("UNA:+,? '"))
                        bUNBWritten = False
                        bUNHWritten = False
                        bWriteUNTForEachBabelFile = True
                        lUNHCounter = 0

                        For Each oBabelFile In oBabelFiles
                            If Not bUNBWritten Then
                                If EmptyString((oBabelFile.EDI_MessageNo)) Then
                                    sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
                                Else
                                    If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.bec Then
                                        If Len(oBabelFile.EDI_MessageNo) > 14 Then
                                            'Normally for BEC
                                            'YMMDDmmss + 5 last digits in accountnumber
                                            sUNBReference = Mid(oBabelFile.EDI_MessageNo, 4, 5) & Mid(oBabelFile.EDI_MessageNo, 11, 4) & Right(oBabelFile.EDI_MessageNo, 5)
                                        Else
                                            sUNBReference = Trim(oBabelFile.EDI_MessageNo)
                                        End If
                                    Else
                                        If Len(oBabelFile.EDI_MessageNo) > 14 Then
                                            sUNBReference = Trim(Right(oBabelFile.EDI_MessageNo, 14))
                                        Else
                                            sUNBReference = Trim(oBabelFile.EDI_MessageNo)
                                        End If
                                    End If
                                End If
                                'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                                ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                                If Not EmptyString(sCompanyNo) Then
                                    sIdentReceiver = Trim(sCompanyNo)
                                Else
                                    '"No receiverident is stated." & vbCrLf & vbCrLf & "Contact Your dealer."
                                    Err.Raise(15120, "CreateEDIFile", LRS(15120))
                                End If
                                WriteSegment("UNB+UNOC:3+00008080+" & sIdentReceiver & "+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "++++1")

                                bUNBWritten = True
                            End If

                            If oBabelFile.Special = "EMENTOR_DK" Then
                                bWriteUNTForEachBabelFile = False
                                If bUNHWritten Then
                                    bWriteUNH = False
                                Else
                                    bWriteUNH = True
                                End If
                            Else
                                bWriteUNH = True
                            End If

                            If bWriteUNH Then
                                lSegmentCounter = 0
                                lUNHCounter = lUNHCounter + 1
                                WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CREMUL:D:96A:UN")
                                WriteSegment("BGM+455+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS"))
                                WriteDTMSegment("137", DateToString(Now), "102")
                                WriteSegment("NAD+MR+" & sIdentReceiver)
                                lLINCounter = 0
                                bUNHWritten = True
                            End If

                            For Each oBatch In oBabelFile.Batches
                                If oBatch.Payments.Count > 0 Then
                                    oPayment = oBatch.Payments.Item(1)
                                    lSEQCounter = 0
                                    '4-LIN
                                    WriteLINSegment()
                                    '4-DTM
                                    WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                    If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.bec Then
                                        WriteDTMSegment("209", (oPayment.DATE_Payment), "102")
                                    Else
                                        WriteDTMSegment("209", (oPayment.DATE_Value), "102")
                                    End If
                                    '4-BUS
                                    sLocalPayCode = bbGetPayCode((oPayment.PayCode), "CREMUL")
                                    WriteBUSSegment((oPayment.PayType), sLocalPayCode)
                                    If sLocalPayCode = "230" Or sLocalPayCode = "232" Then
                                        bTheInformationIsStructured = True
                                    Else
                                        bTheInformationIsStructured = False
                                    End If
                                    '4-MOA
                                    WriteMOASegment("60", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                    '5-RFF
                                    WriteRFFSegment("ACK", (oBatch.REF_Bank))
                                    '5-DTM
                                    WriteDTMSegment("171", (oPayment.DATE_Payment), "102")
                                    '6-FII
                                    WriteFIISegment(I_Info, False, (oPayment.I_CountryCode))
                                    'UPGRADE_NOTE: Object oPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                    oPayment = Nothing
                                    For Each oPayment In oBatch.Payments
                                        lCounter = 0
                                        lArrayCounter = 0
                                        nAmountInSEQ = 0
                                        If bTheInformationIsStructured Then
                                            For Each oInvoice In oPayment.Invoices
                                                '10-SEQ
                                                WriteSEQSegment()
                                                '10-DTM
                                                'In standard BBS, "203" - effektueringsdato is used
                                                WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                                'WriteDTMSegment "209", oPayment.DATE_Value, "102"
                                                '10-FII
                                                WriteFIISegment(E_Info, False, (oPayment.I_CountryCode), , True)
                                                '11-RFF
                                                WriteRFFSegment("ACK", (oPayment.REF_Bank1))
                                                If Not EmptyString((oPayment.REF_Bank2)) Then
                                                    WriteRFFSegment("ACD", (oPayment.REF_Bank2))
                                                End If
                                                '13-MOA
                                                WriteMOASegment("60", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                                '14-NAD
                                                WriteNADSegment(True, False)
                                                If Not EmptyString((oPayment.I_Name)) Then
                                                    WriteNADSegment(False, False, "PL")
                                                End If
                                                WriteSegment("INP+BF+2:SI")
                                                If Not EmptyString((oPayment.Text_I_Statement)) Then
                                                    WriteFTXSegment("AAG", (oPayment.Text_I_Statement))
                                                End If
                                                WriteSegment("PRC+8")
                                                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.bec Then
                                                    WriteDOCSegment("999", oInvoice.Unique_Id & oPayment.E_Account)
                                                Else
                                                    WriteDOCSegment("999", (oInvoice.Unique_Id))
                                                End If
                                                'The currency is not stated in this format.
                                                WriteMOASegment(CStr(12), (oInvoice.MON_InvoiceAmount), "")
                                                WriteSegment("GIS+37")
                                            Next oInvoice
                                        Else 'Not structured info
                                            '10-SEQ
                                            WriteSEQSegment()
                                            '10-DTM
                                            'In standard BBS, "203" - effektueringsdato is used
                                            WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                            'WriteDTMSegment "209", oPayment.DATE_Value, "102"
                                            '10-FII
                                            WriteFIISegment(E_Info, False, (oPayment.I_CountryCode), , True)
                                            '11-RFF
                                            WriteRFFSegment("ACK", (oPayment.REF_Bank1))
                                            If Not EmptyString((oPayment.REF_Bank2)) Then
                                                WriteRFFSegment("ACD", (oPayment.REF_Bank2))
                                            End If
                                            '13-MOA
                                            WriteMOASegment("60", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                            '14-NAD
                                            WriteNADSegment(True, False, "PL")
                                            If Not EmptyString((oPayment.I_Name)) Then
                                                WriteNADSegment(False, False)
                                            End If
                                            WriteSegment("INP+BF+2:SI")
                                            If Not EmptyString((oPayment.Text_I_Statement)) Then
                                                WriteFTXSegment("AAG", (oPayment.Text_I_Statement))
                                            End If
                                            WriteSegment("PRC+11")
                                            sLocalFreetext = ""
                                            For Each oInvoice In oPayment.Invoices
                                                For Each oFreeText In oInvoice.Freetexts
                                                    If oFreeText.Qualifier = CDbl("1") Then
                                                        sLocalFreetext = sLocalFreetext & oFreeText.Text
                                                    End If
                                                Next oFreeText
                                            Next oInvoice
                                            WriteFTXSegment("PMD", sLocalFreetext)
                                        End If 'if btheinformationisstructured
                                        oPayment.Exported = True
                                    Next oPayment

                                End If 'oBatch.Payments.Count > 0
                            Next oBatch
                            If bWriteUNTForEachBabelFile Then
                                WriteCNTSegment()
                                WriteUNTSegment()
                            End If
                        Next oBabelFile
                        If Not bWriteUNTForEachBabelFile Then
                            WriteCNTSegment()
                            WriteUNTSegment()
                        End If
                        WriteUNZSegment(sUNBReference)

                    Case Else
                        Err.Raise(20001, "CreateEDIFile", LRS(15106, sFormat, GetEnumBank(eBank)))
                        '20001: BabelBank is not adopted to create this format. Format: %1 Bank: %2
                End Select


            Case vbBabel.BabelFiles.Bank.Danske_Bank_dk, vbBabel.BabelFiles.Bank.Danske_Bank_NO

                'If sSpecial = VISMA_FOKUS we don't want to validate some parameters. We want to send the payment to
                ' to the bank with the errors included.


                sOldAccountNo = ""
                bWriteUNZ = False

                'New 26.03.2007/14.05.2007
                'If Not (sFormat = "PAYMUL" And bImport = False) Then 'No need for mappingfiles when we write a EDI-file
                '    sFilename = App.path & "\" & sFormat & sEDIVersion & sCountry & sMappingName & ".XML"

                'Because of a change in FindMappingFiles (see commend code over) the mappingfiles
                '  is no longer set.
                '09.07.2007
                'bx = mrEDISettingsExport.InitiateSpecialSegments
                'mrEDISettingsExport.SetCurrentElement ("//HEADER/ATTRIBUTES")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                oFile.Write(("UNA:+,? '"))
                bUNBWritten = False
                sOldUNHReference = ""

                lUNHCounter = 0
                lOriginalUNHCounter = 0

                For Each oBabelFile In oBabelFiles

                    If sSpecial <> "VISMA_FOKUS" Then
                        If Not bUNBWritten Then
                            sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            If Not EmptyString(sCompanyNo) Then
                                sIdentSender = Trim(sCompanyNo)
                            ElseIf Not EmptyString((oBabelFile.IDENT_Sender)) Then
                                sIdentSender = Trim(oBabelFile.IDENT_Sender)
                            Else
                                Err.Raise(15121, "CreateEDIFile", LRS(15121))
                            End If
                            If EmptyString(oBabelFile.VB_Profile.Custom1Value) Then
                                Err.Raise(15122, "CreateEDIFile", LRS(15122))
                            End If
                            If EmptyString(oBabelFile.VB_Profile.Custom2Value) Then
                                Err.Raise(15123, "CreateEDIFile", LRS(15123))
                            End If
                            'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A++0+" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1
                            If sSpecial = "VISMA_FOKUS" Then
                                '1 is set in element 0031, to get a CONTRL message
                                WriteSegment("UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "+" & Trim(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A++" & "1" & "+" & Trim(oBabelFile.VB_Profile.Custom2Value)) ' & "+1", If test add +1
                            Else
                                WriteSegment("UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "+" & Trim(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A+++" & Trim(oBabelFile.VB_Profile.Custom2Value)) ' & "+1", If test add +1
                            End If
                            bUNBWritten = True
                        End If

                        lSegmentCounter = 0
                        lUNHCounter = lUNHCounter + 1
                        WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN")
                        lLINCounter = 0
                        If sSpecial = "VISMA_FOKUS" Then
                            'To obtain (-)DEBMUL AND BANSTA
                            WriteSegment("BGM+452+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS") & "++AF2")
                        Else
                            WriteSegment("BGM+452+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS"))
                        End If
                        WriteDTMSegment("137", DateToString(Now), "102")
                        'oFile.WriteLine ("DTM+137:" &VB6.Format(Now(), "YYYYMMDD") & ":102")
                    End If

                    For Each oBatch In oBabelFile.Batches

                        bNewBatch = True

                        If sSpecial = "VISMA_FOKUS" Then
                            If sOldUNHReference <> oBatch.UNHString Then

                                If bUNBWritten Then
                                    WriteCNTSegment()
                                    WriteUNTSegment(sUNHReference)
                                    WriteUNZSegment(sUNBReference)
                                    lUNHCounter = 0
                                End If

                                lOriginalUNHCounter = lOriginalUNHCounter + 1
                                'Build a new UNB reference
                                '1. part - First character in the importfilename
                                sUNBReference = Mid(oBabelFile.FilenameIn, InStrRev(oBabelFile.FilenameIn, "\") + 1, 1)
                                '2. part - Original UNB reference
                                iPos = 1
                                For lCounter = 1 To 5
                                    iPos = InStr(iPos, oBabelFile.UNBSegmentet, "+", CompareMethod.Text) + 1
                                Next lCounter
                                sUNBReference = sUNBReference & Mid(oBabelFile.UNBSegmentet, iPos, InStr(iPos, oBabelFile.UNBSegmentet, "+", CompareMethod.Text) - iPos)
                                '3. part - Filenumber/counter (UNH-number (position) in original PAYMUL-file)
                                sUNHAppendix = PadLeft(Trim(Str(lOriginalUNHCounter)), 3, "0")
                                '4. part - Total filenumber/count (UNH-segments)
                                sUNHAppendix = sUNHAppendix & PadLeft(Trim(Str(oBabelFile.No_Of_Transactions)), 3, "0")
                                sUNBReference = sUNBReference & sUNHAppendix

                                sNADSegment = oBatch.NADString

                                sAvtaleNr = Left(sNADSegment, InStr(1, sNADSegment, "+") - 1)
                                sReferanse = Mid(sNADSegment, InStr(1, sNADSegment, "+") + 1, InStr(1, sNADSegment, ":") - InStr(1, sNADSegment, "+") - 1)
                                sEDIBruker = Right(sNADSegment, Len(sNADSegment) - InStr(1, sNADSegment, ":"))

                                'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                                ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                                ''                        If Not EmptyString(oBabelFile.IDENT_Sender) Then
                                ''                            sIdentSender = Trim$(oBabelFile.IDENT_Sender)
                                ''                        ElseIf Not EmptyString(sCompanyNo) Then
                                ''                            sIdentSender = Trim$(sCompanyNo)

                                If Not EmptyString(sReferanse) Then
                                    sIdentSender = sReferanse
                                Else
                                    Err.Raise(15121, "CreateEDIFile", LRS(15121))
                                End If
                                If EmptyString(sEDIBruker) Then
                                    Err.Raise(15122, "CreateEDIFile", LRS(15122))
                                End If
                                If EmptyString(sAvtaleNr) Then
                                    Err.Raise(15123, "CreateEDIFile", LRS(15123))
                                End If
                                'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A++0+" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1

                                '1 is set in element 0031, to get a CONTRL message
                                'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A++" & "1" & "+" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1
                                WriteSegment("UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "+" & sEDIBruker & "+DBTS96A++" & "1" & "+" & sAvtaleNr) ' & "+1", If test add +1
                                '16.02.2010 - Added next line
                                lUNHCounter = 0

                                sOldUNHReference = oBatch.UNHString

                                lSegmentCounter = 0
                                lUNHCounter = lUNHCounter + 1
                                '16.02.2010 - Added next line
                                sUNHReference = Trim(Str(lUNHCounter)) & "-" & sUNHAppendix
                                'Old code
                                'sUNHReference = Mid$(oBatch.UNHString, InStr(1, oBatch.UNHString, "UNH+", vbTextCompare) + 4, InStr(1, oBatch.UNHString, "+PAYMUL", vbTextCompare) - (InStr(1, oBatch.UNHString, "UNH+", vbTextCompare) + 4)) & "-" & sUNHAppendix
                                WriteSegment("UNH+" & sUNHReference & "+PAYMUL:D:96A:UN")
                                lLINCounter = 0
                                sBGMSegment = oBatch.BGMString
                                If Left(sBGMSegment, 3) = "452" Then
                                    WriteSegment("BGM+452+" & Mid(sBGMSegment, 4) & "++AF2")
                                Else
                                    WriteSegment("BGM+452+" & sBGMSegment & "++AF2")
                                End If
                                'WriteSegment "BGM" & '"BGM+452+BABELBANK" &VB6.Format(Now(), "YYYYMMDDHHMMSS") & "++AF2"
                                WriteDTMSegment("137", DateToString(Now), "102")

                                bUNBWritten = True

                            End If

                        End If

                        If oBabelFile.Special = "GEHEALTHCARE" Then
                            'New 13.08.2009 - This seems strange, but keep it for GE which is using this format
                            lLINCounter = 0
                        End If
                        For Each oPayment In oBatch.Payments
                            If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                'bNewPayments = True Don't need a new payments when the debit account is changed
                                If oBabelFile.VB_ProfileInUse And bRaiseValidationError Then
                                    bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                End If
                                sOldAccountNo = Trim(oPayment.I_Account)
                            End If

                            'Find country for receiver if not stated
                            If bRaiseValidationError Then
                                If EmptyString((oPayment.I_CountryCode)) Then
                                    If Not EmptyString((oPayment.BANK_I_SWIFTCode)) Then
                                        oPayment.I_CountryCode = Mid(oPayment.BANK_I_SWIFTCode, 5, 2)
                                    Else
                                        oPayment.I_CountryCode = Mid(sI_SWIFTAddr, 5, 2)
                                        'If debit account and receiver is in the same country it must be
                                        '  a domestic payment
                                        If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
                                            oPayment.PayType = "I"
                                        Else
                                            oPayment.PayType = "D"
                                        End If
                                    End If
                                End If
                                oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
                                If sSpecial <> "VISMA_FOKUS" Then
                                    lSEQCounter = 0
                                End If
                                If Left(oPayment.BANK_I_SWIFTCode, 4) <> "DABA" Then
                                    bThisIsOverforselFraKontoIUtland = True
                                Else
                                    bThisIsOverforselFraKontoIUtland = False
                                End If
                            Else
                                bThisIsOverforselFraKontoIUtland = False
                                'opayment.I_CountryCode is vital for this function. How to set it correct?
                                If sSpecial = "VISMA_FOKUS" Then
                                    oPayment.I_CountryCode = "NO"
                                End If
                            End If

                            If (oPayment.I_CountryCode = "FI" And oPayment.PayType = "I") Or bThisIsOverforselFraKontoIUtland = True Then

                                If IsHoliday(StringToDate((oPayment.DATE_Payment)), "XX", True) Then
                                    oPayment.DATE_Payment = DateToString(GetBankday(Now, "XX", 1))
                                End If


                                lCounter = 0
                                lArrayCounter = 0
                                nAmountInSEQ = 0

                                oPayment.E_Name = CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+")
                                oPayment.E_Adr1 = CheckForValidCharacters((oPayment.E_Adr1), True, True, False, True, " /-?:().,'+")
                                oPayment.E_Adr2 = CheckForValidCharacters((oPayment.E_Adr2), True, True, False, True, " /-?:().,'+")
                                oPayment.E_Adr3 = CheckForValidCharacters((oPayment.E_Adr3), True, True, False, True, " /-?:().,'+")
                                oPayment.E_City = CheckForValidCharacters((oPayment.E_City), True, True, False, True, " /-?:().,'+")
                                oPayment.BANK_Name = CheckForValidCharacters((oPayment.BANK_Name), True, True, False, True, " /-?:().,'+")
                                oPayment.BANK_Adr1 = CheckForValidCharacters((oPayment.BANK_Adr1), True, True, False, True, " /-?:().,'+")
                                oPayment.BANK_Adr2 = CheckForValidCharacters((oPayment.BANK_Adr2), True, True, False, True, " /-?:().,'+")
                                oPayment.BANK_Adr3 = CheckForValidCharacters((oPayment.BANK_Adr3), True, True, False, True, " /-?:().,'+")

                                If bThisIsOverforselFraKontoIUtland Then
                                    lMaxLengthOfFreetext = 140
                                Else
                                    Select Case oPayment.I_CountryCode

                                        Case "DE"
                                            If oPayment.PayType = "I" Then
                                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                lMaxLengthOfFreetext = 140
                                            Else
                                                lMaxLengthOfFreetext = 162
                                            End If
                                        Case "DK"
                                            If oPayment.PayType = "I" Then
                                                Err.Raise(15124, "CreateEDIFile", LRS(15124))
                                            Else
                                                lMaxLengthOfFreetext = 1435
                                            End If
                                        Case "FI"
                                            If oPayment.PayType = "I" Then
                                                lMaxLengthOfFreetext = 140
                                            Else
                                                lMaxLengthOfFreetext = 420
                                            End If
                                        Case "GB"
                                            If oPayment.PayType = "I" Then
                                                lMaxLengthOfFreetext = 140
                                            Else 'Domestic, BACS

                                            End If
                                        Case "SE"
                                            If oPayment.PayType = "I" Then
                                                'Possibility to send international payments from Sweden" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                                Err.Raise(15125, "CreateEDIFile", LRS(15125))
                                            Else
                                                lMaxLengthOfFreetext = 1050
                                            End If
                                        Case Else
                                            '"Possibility to send payments from " & oPayment.I_CountryCode & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                            Err.Raise(15126, "CreateEDIFile", LRS(1526, oPayment.I_CountryCode))
                                    End Select
                                End If

                                If lMaxLengthOfFreetext = 0 Then
                                    lMaxLengthOfFreetext = 140
                                End If

                                'This functionality may be used if you want to split payments into
                                ' more than one LIN to be able to send all the freetext information.
                                '  GE Healthcare don't want it anymore
                                'Changes done later in the code marked with 'NOMORE'
                                'Changed 19.05.2005

                                'aFreetextArray = CreateFreetextSEQAmountArray(lMaxLengthOfFreetext)
                                'Do While lArrayCounter <= UBound(aFreetextArray(), 2)
                                If sSpecial <> "VISMA_FOKUS" Then
                                    lSEQCounter = 0
                                End If


                                If sSpecial = "VISMA_FOKUS" Then
                                    If bNewBatch Then
                                        lSEQCounter = 0

                                        bNewBatch = False
                                        '4-LIN
                                        WriteLINSegment()
                                        '4-DTM
                                        If bThisIsOverforselFraKontoIUtland Then
                                            WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                        Else
                                            Select Case oPayment.I_CountryCode
                                                Case "GB"
                                                    WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                                Case "FI"
                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102", True)
                                                Case Else
                                                    'XOKNET (01.02.2011) - added next IF
                                                    If oPayment.DATE_Payment = "19900101" Then
                                                        WriteDTMSegment("203", DateToString(Now()), "102")
                                                    Else
                                                        If oPayment.PayType = "I" Then
                                                            'XOKNET (01.03.2011) - Changed next line from AFS to 203 according to mail from Johan Birger Stokmo
                                                            WriteDTMSegment("203", oPayment.DATE_Payment, "102")
                                                            'WriteDTMSegment("AFS", DateToString(Now), "102")
                                                        Else
                                                            WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                        End If
                                                    End If
                                            End Select
                                        End If
                                        '4-RFF
                                        If Len(oPayment.REF_Own) > 0 Then
                                            WriteRFFSegment("CR2", CheckForValidCharacters((oPayment.REF_Own), True, True, False, True, " /-?:().,'+"))
                                        End If
                                        If oPayment.PayType = "D" And oPayment.I_CountryCode = "DE" Then
                                            If Len(oPayment.Text_I_Statement) > 0 Then
                                                WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.Text_I_Statement), True, True, False, True, " /-?:().,'+"), 10)
                                            Else
                                                If Not EmptyString((oPayment.E_Name)) Then
                                                    WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+"), 10)
                                                End If
                                            End If
                                        Else
                                            If Len(oPayment.Text_I_Statement) > 0 Then
                                                WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.Text_I_Statement), True, True, False, True, " /-?:().,'+"), 16)
                                            Else
                                                If Not EmptyString((oPayment.E_Name)) Then
                                                    WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+"), 16)
                                                End If
                                            End If
                                        End If
                                        '4-BUS
                                        WriteBUSSegment((oPayment.PayType))
                                        '4-FCA
                                        If oPayment.PayType = "I" Then
                                            WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True) 'XNET - 16.04.2012
                                        Else
                                            If oPayment.I_CountryCode = "DE" Then
                                                WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True) 'XNET - 16.04.2012
                                            End If
                                        End If
                                        '5-MOA
                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                            'NOMORE
                                            'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                            WriteMOASegment("9", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                        Else
                                            'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            'NOMORE
                                            'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                            WriteMOASegment("9", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                            '5-CUX
                                            WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                        End If
                                        '5-RFF
                                        If oPayment.ERA_ExchRateAgreed <> 0 And Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                            WriteRFFSegment("1", (oPayment.ERA_DealMadeWith))
                                        ElseIf oPayment.FRW_ForwardContractRate <> 0 And Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                            WriteRFFSegment("FX", (oPayment.FRW_ForwardContractNo))
                                        Else
                                            If oPayment.PayType = "I" Then
                                                WriteRFFSegment("3", "")
                                            End If
                                        End If
                                        '6-FII
                                        'Changed 08.04.05 - Kjell
                                        'This is not completely correct, but will be OK for Amersham.
                                        If oPayment.I_CountryCode = "FI" And oPayment.MON_TransferCurrency = "EUR" And bThisIsOverforselFraKontoIUtland = False Then
                                            WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , True)
                                        Else
                                            WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , False)
                                        End If
                                        '8-INP
                                        If oPayment.NOTI_NotificationParty <> 0 Then
                                            '"Possibility to send bank notifications" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                            Err.Raise(15127, "CreateEDIFile", LRS(15127))
                                        End If
                                    End If
                                Else
                                    '4-LIN
                                    WriteLINSegment()
                                    '4-DTM
                                    If bThisIsOverforselFraKontoIUtland Then
                                        WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                    Else
                                        Select Case oPayment.I_CountryCode
                                            Case "GB"
                                                WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                            Case "FI"
                                                WriteDTMSegment("203", (oPayment.DATE_Payment), "102", True)
                                            Case Else
                                                If oPayment.PayType = "I" Then
                                                    ' XOKNET 01.03.2011
                                                    WriteDTMSegment("203", oPayment.DATE_Payment, "102")
                                                    'WriteDTMSegment("AFS", DateToString(Now), "102")
                                                Else
                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                End If
                                        End Select
                                    End If
                                    '4-RFF
                                    If Len(oPayment.REF_Own) > 0 Then
                                        WriteRFFSegment("CR2", CheckForValidCharacters((oPayment.REF_Own), True, True, False, True, " /-?:().,'+"))
                                    End If
                                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "DE" Then
                                        If Len(oPayment.Text_I_Statement) > 0 Then
                                            WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.Text_I_Statement), True, True, False, True, " /-?:().,'+"), 10)
                                        Else
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+"), 10)
                                            End If
                                        End If
                                    Else
                                        If Len(oPayment.Text_I_Statement) > 0 Then
                                            WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.Text_I_Statement), True, True, False, True, " /-?:().,'+"), 16)
                                        Else
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                WriteRFFSegment("AXX", CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+"), 16)
                                            End If
                                        End If
                                    End If
                                    '4-BUS
                                    WriteBUSSegment((oPayment.PayType))
                                    '4-FCA
                                    If oPayment.PayType = "I" Then
                                        WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True)  'XNET - 16.04.2012
                                    Else
                                        If oPayment.I_CountryCode = "DE" Then
                                            WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True) 'XNET - 16.04.2012
                                        End If
                                    End If
                                    '5-MOA
                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                        'NOMORE
                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                        WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                    Else
                                        'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                        'NOMORE
                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                        WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                        '5-CUX
                                        WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                    End If
                                    '5-RFF
                                    If oPayment.ERA_ExchRateAgreed <> 0 And Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                        WriteRFFSegment("1", (oPayment.ERA_DealMadeWith))
                                    ElseIf oPayment.FRW_ForwardContractRate <> 0 And Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                        WriteRFFSegment("FX", (oPayment.FRW_ForwardContractNo))
                                    Else
                                        If oPayment.PayType = "I" Then
                                            WriteRFFSegment("3", "")
                                        End If
                                    End If
                                    '6-FII
                                    'Changed 08.04.05 - Kjell
                                    'This is not completely correct, but will be OK for Amersham.
                                    If oPayment.I_CountryCode = "FI" And oPayment.MON_TransferCurrency = "EUR" And bThisIsOverforselFraKontoIUtland = False Then
                                        WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , True)
                                    Else
                                        WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , False)
                                    End If
                                    '8-INP
                                    If oPayment.NOTI_NotificationParty <> 0 Then
                                        '"Possibility to send bank notifications" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                        Err.Raise(15127, "CreateEDIFile", LRS(15127))
                                    End If
                                End If

                                'Use the Invoice who contains last freetext
                                'NOMORE
                                'Set oInvoice = oPayment.Invoices.Item(Val(aFreetextArray(2, lArrayCounter)))
                                sLocalFreetext = ""
                                sLocalREF_Own = ""
                                For Each oInvoice In oPayment.Invoices
                                    For Each oFreeText In oInvoice.Freetexts
                                        If oFreeText.Qualifier = CDbl("1") Then
                                            sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                        End If
                                    Next oFreeText
                                Next oInvoice
                                If oBabelFile.Special = "GEHEALTHCARE" Then
                                    If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                        sLocalFreetext = "SEE SEPERATE LETTER."
                                    End If
                                End If
                                sLocalFreetext = RTrim(Left(sLocalFreetext, lMaxLengthOfFreetext))
                                If oPayment.Invoices.Count > 0 Then
                                    sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                End If

                                '11-SEQ
                                WriteSEQSegment()
                                '11-MOA
                                If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                    'NOMORE
                                    'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                    WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                Else
                                    'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                    'NOMORE
                                    'WriteMOASegment "57", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                    WriteMOASegment("57", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                End If
                                '11-PAI
                                'FIX, Hard-coded paymentTypes for each country, debending on the I_CountryCode
                                ' Should have been more flexible
                                If bThisIsOverforselFraKontoIUtland Then
                                    '11-DTM
                                    WriteDTMSegment("EKS", (oPayment.DATE_Payment), "102", True)
                                    '11-RFF
                                    If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                        WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                    End If
                                    If Not EmptyString(sLocalREF_Own) > 0 Then
                                        WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                    End If
                                    'This format  is used when payers account is in another bank than Danske Bank
                                    If oPayment.PayType = "I" Then
                                        '11-PAI
                                        'WriteSegment "PAI+::UBB:::ALO"
                                        If oPayment.ToOwnAccount Then
                                            WriteSegment("PAI+::MTA:::SUI")
                                        ElseIf oPayment.Cheque Then
                                            WriteSegment("PAI+::MTA:::SUC")
                                        ElseIf oPayment.Priority Then
                                            WriteSegment("PAI+::MTA:::SUU")
                                        Else
                                            WriteSegment("PAI+::MTA:::SUE")
                                        End If
                                        '12-FII
                                        WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), True)
                                        '13-NAD
                                        '08.04.2005 Changed from
                                        'WriteNADSegment True, False - to
                                        WriteNADSegment(True, True)
                                        '16-PRC
                                        WriteSegment("PRC+11")
                                        '16-FTX
                                        'NOMORE
                                        'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                        WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                        '23-GIS
                                        WriteSegment("GIS+37")
                                    Else 'Domestic, BACS
                                        '11-PAI
                                        'WriteSegment "PAI+::ULB"
                                        If oPayment.ToOwnAccount Then
                                            WriteSegment("PAI+::MTA:::SII")
                                        ElseIf oPayment.PayType = "S" Then
                                            WriteSegment("PAI+::MTA:::SIS")
                                        ElseIf oPayment.Priority Then
                                            WriteSegment("PAI+::MTA:::SIU")
                                        Else
                                            WriteSegment("PAI+::MTA:::SIE")
                                        End If
                                        '12-FII
                                        WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), True)
                                        '13-NAD
                                        If Not EmptyString((oPayment.E_Name)) Then
                                            WriteSegment("NAD+PE+++" & Trim(CheckForSeperators(CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+"))))
                                        End If
                                    End If

                                Else
                                    Select Case oPayment.I_CountryCode

                                        Case "DE"
                                            If oPayment.PayType = "I" Then
                                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                '11-DTM
                                                WriteDTMSegment("EKS", (oPayment.DATE_Payment), "102")
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                                End If
                                                '11-PAI
                                                If oPayment.Priority Then
                                                    'Express/Urgent
                                                    WriteSegment("PAI+::MTA:::SUU")
                                                Else
                                                    'Regular
                                                    WriteSegment("PAI+::MTA:::SUE")
                                                End If
                                                '12-FCA
                                                WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True) 'XNET - 16.04.2012
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "BE", False) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                                End If
                                                '11-PAI
                                                WriteSegment("PAI+::DEO")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "PE", True) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "DK"
                                            '11-RFF
                                            If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"), 20)
                                            End If
                                            If Not EmptyString(sLocalREF_Own) > 0 Then
                                                WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                            End If
                                            If oPayment.PayType = "I" Then
                                                '"Possibility to send international payments from Denmark" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                Err.Raise(15124, "CreateEDIFile", LRS(15124))
                                            Else
                                                '11-PAI
                                                WriteSegment("PAI+::IBB:::UUA")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                'WriteNADSegment True, True, "", True ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "FI"
                                            If oPayment.PayType = "I" Then
                                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                '11-DTM
                                                'WriteDTMSegment "EKS", oPayment.DATE_Payment, "102"
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                                End If
                                                '11-PAI
                                                If oPayment.Priority Then
                                                    'Express/Urgent
                                                    WriteSegment("PAI+::UBB:::EXP")
                                                Else
                                                    'Regular
                                                    WriteSegment("PAI+::UBB:::ALO")
                                                End If
                                                'FCA
                                                WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True) 'XNET - 16.04.2012
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "BE", False) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                                End If
                                                '11-PAI
                                                WriteSegment("PAI+::FLK:::FKM")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "PE", True) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "GB"
                                            '11-DTM
                                            WriteDTMSegment("EKS", (oPayment.DATE_Payment), "102", True)
                                            '11-RFF
                                            If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                            End If
                                            If Not EmptyString(sLocalREF_Own) > 0 Then
                                                WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                            End If
                                            'This format is used when payers account is in another bank than Danske Bank
                                            'Probably should use bThisIsOverforselFraKontoIUtland = true
                                            If oPayment.PayType = "I" Then
                                                '11-PAI
                                                'WriteSegment "PAI+::UBB:::ALO"
                                                WriteSegment("PAI+::UBB:::SUE")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, False)
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else 'Domestic, BACS
                                                '11-PAI
                                                'WriteSegment "PAI+::ULB"
                                                WriteSegment("PAI+::UBB:::SIE")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                If Not EmptyString((oPayment.E_Name)) Then
                                                    WriteSegment("NAD+PE+++" & Trim(CheckForSeperators(CheckForValidCharacters((oPayment.E_Name), True, True, False, True, " /-?:().,'+"))))
                                                End If
                                            End If

                                        Case "SE"
                                            '11-RFF
                                            If oPayment.PayType = "I" Then
                                                '"Possibility to send international payments from Sweden" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                Err.Raise(15125, "CreateEDIFile", LRS(15125))
                                            Else
                                                If Len(oPayment.E_Account) > 8 Then
                                                    If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                        WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"), 25)
                                                    End If
                                                    If Not EmptyString(sLocalREF_Own) > 0 Then
                                                        WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                                    End If
                                                    WriteSegment("PAI+::SLK:::SKU") 'AccountNo
                                                Else
                                                    If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                        WriteRFFSegment("CR", CheckForValidCharacters((oPayment.Text_E_Statement), True, True, False, True, " /-?:().,'+"))
                                                    End If
                                                    If Not EmptyString(sLocalREF_Own) > 0 Then
                                                        WriteRFFSegment("CR3", CheckForValidCharacters(sLocalREF_Own, True, True, False, True, " /-?:().,'+"))
                                                    End If
                                                    WriteSegment("PAI+::SLG:::SGU") 'BankgiroNo
                                                End If
                                                'Remove hyphens (-) from the accountno
                                                oPayment.E_Account = Replace(oPayment.E_Account, "-", "")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True)
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, False, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If
                                        Case Else
                                            '"Possibility to send payments from " & oPayment.I_CountryCode & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            Err.Raise(1, "CreateEDIFile", LRS(15126, oPayment.I_CountryCode))
                                    End Select
                                End If 'If bThisIsOverforselFraKontoIUtland Then
                                lArrayCounter = lArrayCounter + 1

                                'NOMORE
                                'Loop

                            Else

                                If sSpecial = "VISMA_FOKUS" Then
                                    If bNewBatch Then
                                        bNewBatch = False

                                        lSEQCounter = 0
                                        '4-LIN
                                        WriteLINSegment()
                                        '4-DTM
                                        If bThisIsOverforselFraKontoIUtland Then
                                            WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                        Else
                                            Select Case oPayment.I_CountryCode
                                                Case "GB"
                                                    WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                                Case "FI"
                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102", True)
                                                Case Else
                                                    If oPayment.PayType = "I" Then
                                                        'XNET - 11.02.2011 - Changed the line below.
                                                        WriteDTMSegment("203", oPayment.DATE_Payment, "102")
                                                        'XNET - 16.12.2010 - Changed the line below.
                                                        'WriteDTMSegment("AFS", oPayment.DATE_Payment, "102")
                                                        'Old code
                                                        'WriteDTMSegment "AFS", DateToString(Now()), "102"
                                                    Else
                                                        WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                    End If
                                            End Select
                                        End If
                                        '4-RFF
                                        If Len(oBatch.REF_Own) > 0 Then 'New 06.08.2009 - May cause problems for other users (GE HEalthcare, but probably are oBatch.REF_own = "" for them
                                            WriteRFFSegment("CR2", (oBatch.REF_Own))
                                        ElseIf Len(oPayment.REF_Own) > 0 Then
                                            WriteRFFSegment("CR2", (oPayment.REF_Own))
                                        End If
                                        If oPayment.PayType = "D" And oPayment.I_CountryCode = "DE" Then
                                            If Len(oPayment.Text_I_Statement) > 0 Then
                                                WriteRFFSegment("AXX", (oPayment.Text_I_Statement), 10)
                                            Else
                                                If Not EmptyString((oPayment.E_Name)) Then
                                                    WriteRFFSegment("AXX", (oPayment.E_Name), 10)
                                                End If
                                            End If
                                        Else
                                            If Len(oPayment.Text_I_Statement) > 0 Then
                                                WriteRFFSegment("AXX", (oPayment.Text_I_Statement), 16)
                                            Else
                                                If bRaiseValidationError Then
                                                    If Not EmptyString((oPayment.E_Name)) Then
                                                        WriteRFFSegment("AXX", (oPayment.E_Name), 16)
                                                    End If
                                                End If
                                            End If
                                        End If
                                        '4-BUS
                                        'LONN_FOKUS_20120926
                                        'XOKNET - 13.11.2012 - added next IF
                                        If oPayment.PayType = "S" Then
                                            WriteSegment("BUS+1:FO1+DO")
                                        Else
                                            WriteBUSSegment(oPayment.PayType)
                                        End If
                                        '4-FCA
                                        If oPayment.PayType = "I" Then
                                            WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True)
                                        Else
                                            If oPayment.I_CountryCode = "DE" Then
                                                WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                            End If
                                        End If
                                        '5-MOA
                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                            WriteMOASegment("9", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                        Else
                                            'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            WriteMOASegment("57", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                            '5-CUX
                                            WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                        End If
                                        '5-RFF
                                        If oPayment.ERA_ExchRateAgreed <> 0 And Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                            WriteRFFSegment("1", (oPayment.ERA_DealMadeWith))
                                        ElseIf oPayment.FRW_ForwardContractRate <> 0 And Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                            WriteRFFSegment("FX", (oPayment.FRW_ForwardContractNo))
                                        Else
                                            If oPayment.PayType = "I" Then
                                                WriteRFFSegment("3", "")
                                            End If
                                        End If
                                        '6-FII
                                        'Changed 08.04.05 - Kjell
                                        'This is not completely correct, but will be OK for Amersham.
                                        If oPayment.I_CountryCode = "FI" And oPayment.MON_TransferCurrency = "EUR" And bThisIsOverforselFraKontoIUtland = False Then
                                            WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , True)
                                        Else
                                            WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , False)
                                        End If
                                        '8-INP
                                        If oPayment.NOTI_NotificationParty <> 0 Then
                                            '15127: Possibility to state bank notifications" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer.
                                            Err.Raise(15127, "CreateEDIFile", LRS(15127))
                                        End If

                                    End If
                                Else
                                    '4-LIN
                                    WriteLINSegment()
                                    '4-DTM
                                    If bThisIsOverforselFraKontoIUtland Then
                                        WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                    Else
                                        Select Case oPayment.I_CountryCode
                                            Case "GB"
                                                WriteDTMSegment("AFS", (oPayment.DATE_Payment), "102", True)
                                            Case "FI"
                                                WriteDTMSegment("203", (oPayment.DATE_Payment), "102", True)
                                            Case Else
                                                If oPayment.PayType = "I" Then
                                                    'XNET - 11.02.2011
                                                    WriteDTMSegment("203", oPayment.DATE_Payment, "102")
                                                    'WriteDTMSegment("AFS", DateToString(Now), "102")
                                                Else
                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                End If
                                        End Select
                                    End If
                                    '4-RFF
                                    If Len(oBatch.REF_Own) > 0 Then 'New 06.08.2009 - May cause problems for other users (GE HEalthcare, but probably are oBatch.REF_own = "" for them
                                        WriteRFFSegment("CR2", (oBatch.REF_Own))
                                    ElseIf Len(oPayment.REF_Own) > 0 Then
                                        WriteRFFSegment("CR2", (oPayment.REF_Own))
                                    End If
                                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "DE" Then
                                        If Len(oPayment.Text_I_Statement) > 0 Then
                                            WriteRFFSegment("AXX", (oPayment.Text_I_Statement), 10)
                                        Else
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                WriteRFFSegment("AXX", (oPayment.E_Name), 10)
                                            End If
                                        End If
                                    Else
                                        If Len(oPayment.Text_I_Statement) > 0 Then
                                            WriteRFFSegment("AXX", (oPayment.Text_I_Statement), 16)
                                        Else
                                            If bRaiseValidationError Then
                                                If Not EmptyString((oPayment.E_Name)) Then
                                                    WriteRFFSegment("AXX", (oPayment.E_Name), 16)
                                                End If
                                            End If
                                        End If
                                    End If
                                    '4-BUS
                                    WriteBUSSegment((oPayment.PayType))
                                    '4-FCA
                                    If oPayment.PayType = "I" Then
                                        WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                    Else
                                        If oPayment.I_CountryCode = "DE" Then
                                            WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                        End If
                                    End If
                                    '5-MOA
                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                        WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                    Else
                                        'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                        WriteMOASegment("57", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                        '5-CUX
                                        WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                    End If
                                    '5-RFF
                                    If oPayment.ERA_ExchRateAgreed <> 0 And Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                        WriteRFFSegment("1", (oPayment.ERA_DealMadeWith))
                                    ElseIf oPayment.FRW_ForwardContractRate <> 0 And Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                        WriteRFFSegment("FX", (oPayment.FRW_ForwardContractNo))
                                    Else
                                        If oPayment.PayType = "I" Then
                                            WriteRFFSegment("3", "")
                                        End If
                                    End If
                                    '6-FII
                                    'Changed 08.04.05 - Kjell
                                    'This is not completely correct, but will be OK for Amersham.
                                    If oPayment.I_CountryCode = "FI" And oPayment.MON_TransferCurrency = "EUR" And bThisIsOverforselFraKontoIUtland = False Then
                                        WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , True)
                                    Else
                                        WriteFIISegment(I_Info, True, (oPayment.I_CountryCode), , , False)
                                    End If
                                    '8-INP
                                    If oPayment.NOTI_NotificationParty <> 0 Then
                                        '15127: Possibility to state bank notifications" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer.
                                        Err.Raise(15127, "CreateEDIFile", LRS(15127))
                                    End If
                                End If

                                If bThisIsOverforselFraKontoIUtland Then
                                    lMaxLengthOfFreetext = 140
                                Else
                                    Select Case oPayment.I_CountryCode

                                        Case "DE"
                                            If oPayment.PayType = "I" Then
                                                lMaxLengthOfFreetext = 140
                                            Else
                                                lMaxLengthOfFreetext = 162
                                            End If
                                        Case "DK"
                                            If oPayment.PayType = "I" Then
                                                '15124: Possibility to send international payments from Denmark" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer.
                                                Err.Raise(15124, "CreateEDIFile", LRS(15124))
                                            Else
                                                lMaxLengthOfFreetext = 1435
                                            End If
                                        Case "FI"
                                            If oPayment.PayType = "I" Then
                                                lMaxLengthOfFreetext = 140
                                            Else
                                                lMaxLengthOfFreetext = 420
                                            End If
                                        Case "GB"
                                            If oPayment.PayType = "I" Then
                                                lMaxLengthOfFreetext = 140
                                            Else 'Domestic, BACS

                                            End If
                                        Case "SE"
                                            If oPayment.PayType = "I" Then
                                                '"15125: Possibility to send international payments from Sweden" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                                                Err.Raise(15125, "CreateEDIFile", LRS(15125))
                                            Else
                                                lMaxLengthOfFreetext = 1050
                                            End If
                                        Case "NO"
                                            If oPayment.PayType = "I" Then
                                                lMaxLengthOfFreetext = 140
                                            Else
                                                'XNET - 01.09.2011 - changed the line under
                                                lMaxLengthOfFreetext = 1470 'Mail from Johan Birger Stokmo 31.08.2011, previous it was set to max 420 '23.02.2010 Changed from 1470
                                            End If
                                        Case Else
                                            '"15126: Possibility to send payments from %1" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                                            Err.Raise(15126, "CreateEDIFile", LRS(15126, oPayment.I_CountryCode))
                                    End Select
                                End If

                                If lMaxLengthOfFreetext = 0 Then
                                    lMaxLengthOfFreetext = 140
                                End If

                                lCounter = 0
                                lArrayCounter = 0
                                nAmountInSEQ = 0

                                'NOMORE
                                'aFreetextArray = CreateFreetextSEQAmountArray(lMaxLengthOfFreetext)
                                'Do While lArrayCounter <= UBound(aFreetextArray(), 2)

                                'NOMORE
                                'Use the Invoice who contains last freetext
                                'Set oInvoice = oPayment.Invoices.Item(Val(aFreetextArray(2, lArrayCounter)))

                                'New IF 06.08.2009
                                If oPayment.PayCode = "629" Then 'Structured. Not used in Danske Bank
                                    sLocalFreetext = ""
                                    sLocalREF_Own = ""
                                    For Each oInvoice In oPayment.Invoices
                                        If Not EmptyString((oInvoice.InvoiceNo)) Then
                                            If oInvoice.MON_InvoiceAmount < 0 Then
                                                sLocalFreetext = sLocalFreetext & LRSCommon(41022) & " " & Trim(oInvoice.InvoiceNo) & " " & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), ".", ",") & " "
                                            Else
                                                sLocalFreetext = sLocalFreetext & LRSCommon(41002) & " " & Trim(oInvoice.InvoiceNo) & " " & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), ".", ",") & " "
                                            End If
                                        ElseIf Not EmptyString((oInvoice.Unique_Id)) Then
                                            sLocalFreetext = sLocalFreetext & "KID: " & Trim(oInvoice.Unique_Id) & " " & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), ".", ",") & " "
                                        End If
                                        For Each oFreeText In oInvoice.Freetexts
                                            If oFreeText.Qualifier = CDbl("1") Then
                                                sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                            End If
                                        Next oFreeText
                                    Next oInvoice
                                Else
                                    'Old code
                                    sLocalFreetext = ""
                                    sLocalREF_Own = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            If oFreeText.Qualifier = CDbl("1") Then
                                                sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                            End If
                                        Next oFreeText
                                    Next oInvoice
                                End If
                                If oBabelFile.Special = "GEHEALTHCARE" Then
                                    If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                        sLocalFreetext = "SEE SEPERATE LETTER."
                                    End If
                                End If
                                sLocalFreetext = RTrim(Left(sLocalFreetext, lMaxLengthOfFreetext))
                                If oPayment.Invoices.Count > 0 Then
                                    sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                End If

                                '11-SEQ
                                WriteSEQSegment()
                                '11-MOA
                                If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                    'NOMORE
                                    'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                    WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                Else
                                    'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                    'NOMORE
                                    'WriteMOASegment "57", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                    WriteMOASegment("57", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                End If
                                '11-PAI
                                'FIX, Hard-coded paymentTypes for each country, debending on the I_CountryCode
                                ' Should have been more flexible
                                If bThisIsOverforselFraKontoIUtland Then
                                    '11-DTM
                                    WriteDTMSegment("EKS", (oPayment.DATE_Payment), "102", True)
                                    '11-RFF
                                    If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                        WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                    End If
                                    If Not EmptyString(sLocalREF_Own) > 0 Then
                                        WriteRFFSegment("CR3", sLocalREF_Own)
                                    End If
                                    'This format is used when payers account is in another bank than Danske Bank
                                    If oPayment.PayType = "I" Then
                                        '11-PAI
                                        'WriteSegment "PAI+::UBB:::ALO"
                                        If oPayment.ToOwnAccount Then
                                            WriteSegment("PAI+::MTA:::SUI")
                                        ElseIf oPayment.Cheque Then
                                            WriteSegment("PAI+::MTA:::SUC")
                                        ElseIf oPayment.Priority Then
                                            WriteSegment("PAI+::MTA:::SUU")
                                        Else
                                            WriteSegment("PAI+::MTA:::SUE")
                                        End If
                                        '12-FII
                                        WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), True)
                                        '13-NAD
                                        '08.04.2005 Changed from
                                        'WriteNADSegment True, False - to
                                        WriteNADSegment(True, True)
                                        '16-PRC
                                        WriteSegment("PRC+11")
                                        '16-FTX
                                        'NOMORE
                                        'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                        WriteFTXSegment("PMD", sLocalFreetext)
                                        '23-GIS
                                        WriteSegment("GIS+37")
                                    Else 'Domestic, BACS
                                        '11-PAI
                                        'WriteSegment "PAI+::ULB"
                                        If oPayment.ToOwnAccount Then
                                            WriteSegment("PAI+::MTA:::SII")
                                        ElseIf oPayment.PayType = "S" Then
                                            WriteSegment("PAI+::MTA:::SIS")
                                        ElseIf oPayment.Priority Then
                                            WriteSegment("PAI+::MTA:::SIU")
                                        Else
                                            WriteSegment("PAI+::MTA:::SIE")
                                        End If
                                        '12-FII
                                        WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), True)
                                        '13-NAD
                                        If Not EmptyString((oPayment.E_Name)) Then
                                            WriteSegment("NAD+PE+++" & Trim(CheckForSeperators((oPayment.E_Name))))
                                        End If
                                    End If

                                Else
                                    Select Case oPayment.I_CountryCode

                                        Case "DE"
                                            If oPayment.PayType = "I" Then
                                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                '11-DTM
                                                WriteDTMSegment("EKS", (oPayment.DATE_Payment), "102")
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", Trim(oPayment.Text_E_Statement))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", Trim(sLocalREF_Own))
                                                End If
                                                '11-PAI
                                                If oPayment.Priority Then
                                                    'Express/Urgent
                                                    WriteSegment("PAI+::MTA:::SUU")
                                                Else
                                                    'Regular
                                                    WriteSegment("PAI+::MTA:::SUE")
                                                End If
                                                'FCA
                                                WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "BE", False) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", sLocalREF_Own)
                                                End If
                                                '11-PAI
                                                WriteSegment("PAI+::DEO")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "PE", True) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "DK"
                                            '11-RFF
                                            If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                WriteRFFSegment("CR", (oPayment.Text_E_Statement), 20)
                                            End If
                                            If Not EmptyString(sLocalREF_Own) > 0 Then
                                                WriteRFFSegment("CR3", sLocalREF_Own)
                                            End If
                                            If oPayment.PayType = "I" Then
                                                '"15124: Possibility to send international payments from Denmark" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                                                Err.Raise(15124, "CreateEDIFile", LRS(15124))
                                            Else
                                                '11-PAI
                                                WriteSegment("PAI+::IBB:::UUA")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                'WriteNADSegment True, True, "", True ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "FI"
                                            If oPayment.PayType = "I" Then
                                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                '11-DTM
                                                'WriteDTMSegment "EKS", oPayment.DATE_Payment, "102"
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", sLocalREF_Own)
                                                End If
                                                '11-PAI
                                                If oPayment.Priority Then
                                                    'Express/Urgent
                                                    WriteSegment("PAI+::UBB:::EXP")
                                                Else
                                                    'Regular
                                                    WriteSegment("PAI+::UBB:::ALO")
                                                End If
                                                'FCA
                                                WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True) 'XNET - 16.04.2012
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "BE", False) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                '11-RFF
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                                End If
                                                If Not EmptyString(sLocalREF_Own) > 0 Then
                                                    WriteRFFSegment("CR3", sLocalREF_Own)
                                                End If
                                                '11-PAI
                                                WriteSegment("PAI+::FLK:::FKM")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True, "PE", True) ' NAD is not mandatory
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "GB"
                                            '11-DTM
                                            WriteDTMSegment("EKS", (oPayment.DATE_Payment), "102", True)
                                            '11-RFF
                                            If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                            End If
                                            If Not EmptyString(sLocalREF_Own) > 0 Then
                                                WriteRFFSegment("CR3", sLocalREF_Own)
                                            End If
                                            'This format is used when payers account is in another bank than Danske Bank
                                            'Probably should use bThisIsOverforselFraKontoIUtland = true
                                            If oPayment.PayType = "I" Then
                                                '11-PAI
                                                'WriteSegment "PAI+::UBB:::ALO"
                                                WriteSegment("PAI+::UBB:::SUE")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, False)
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else 'Domestic, BACS
                                                '11-PAI
                                                'WriteSegment "PAI+::ULB"
                                                WriteSegment("PAI+::UBB:::SIE")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                If Not EmptyString((oPayment.E_Name)) Then
                                                    WriteSegment("NAD+PE+++" & Trim(CheckForSeperators((oPayment.E_Name))))
                                                End If
                                            End If

                                        Case "SE"
                                            '11-RFF
                                            If oPayment.PayType = "I" Then
                                                '"15125: Possibility to send international payments from Sweden" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                                                Err.Raise(15125, "CreateEDIFile", LRS(15125))
                                            Else
                                                If Len(oPayment.E_Account) > 8 Then
                                                    If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                        WriteRFFSegment("CR", (oPayment.Text_E_Statement), 25)
                                                    End If
                                                    If Not EmptyString(sLocalREF_Own) > 0 Then
                                                        WriteRFFSegment("CR3", sLocalREF_Own)
                                                    End If
                                                    WriteSegment("PAI+::SLK:::SKU") 'AccountNo
                                                Else
                                                    If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                        WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                                    End If
                                                    If Not EmptyString(sLocalREF_Own) > 0 Then
                                                        WriteRFFSegment("CR3", sLocalREF_Own)
                                                    End If
                                                    WriteSegment("PAI+::SLG:::SGU") 'BankgiroNo
                                                End If
                                                'Remove hyphens (-) from the accountno
                                                oPayment.E_Account = Replace(oPayment.E_Account, "-", "")
                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                '13-NAD
                                                WriteNADSegment(True, True)
                                                '16-PRC
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", aFreetextArray(0, lArrayCounter)
                                                WriteFTXSegment("PMD", sLocalFreetext)
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            End If

                                        Case "NO"

                                            If oPayment.PayType = "I" Then
                                                'KOKOKO
                                                '11-RFF
                                                If Len(oBatch.REF_Own) > 0 Then 'New 06.08.2009 - May cause problems for other users (GE HEalthcare, but probably are oBatch.REF_own = "" for them
                                                    If Not EmptyString((oPayment.REF_Own)) Then
                                                        WriteRFFSegment("CR3", (oPayment.REF_Own))
                                                    Else
                                                        If Not EmptyString(sLocalREF_Own) Then
                                                            WriteRFFSegment("CR3", sLocalREF_Own)
                                                        End If
                                                    End If
                                                Else
                                                    WriteRFFSegment("CR3", sLocalREF_Own)
                                                End If
                                                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                                                    WriteRFFSegment("CR", (oPayment.Text_E_Statement))
                                                End If
                                                '11-PAI
                                                'UBB - Foreign account transfer
                                                '23 - Foreign cheque
                                                'MTC - Foreign cheque to be crossed (What the fuck is this?)
                                                '4435
                                                'ALO - Ordinary transfer
                                                'EXP - Express transfer
                                                'TE - Tele transfer (Not implemented)
                                                'EUR -Euro transfer (Not implemented)
                                                'KON - Group transfer

                                                'DFA - Cheque to be sent to account holder
                                                'DFF - Cheque to be collected in branch
                                                'DFM - Cheque to be sent to beneficiary

                                                If oPayment.ToOwnAccount Then
                                                    WriteSegment("PAI+::UBB:::KON")
                                                ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                                                    WriteSegment("PAI+::23:::DFA")
                                                ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
                                                    WriteSegment("PAI+::23:::DFM")
                                                ElseIf oPayment.Priority Then
                                                    WriteSegment("PAI+::UBB:::EXP")
                                                Else
                                                    WriteSegment("PAI+::UBB:::ALO")
                                                End If

                                                '12-FII
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, False)
                                                '13-NAD
                                                WriteNADSegment(True, True)

                                                WriteSegment("GIS+10")
                                                sStatebankText = ""
                                                sStatebankCode = ""
                                                For Each oInvoice In oPayment.Invoices
                                                    sStatebankText = oInvoice.STATEBANK_Text
                                                    sStatebankCode = oInvoice.STATEBANK_Code
                                                    If Not EmptyString(sStatebankText) Then
                                                        Exit For
                                                    End If
                                                Next oInvoice

                                                '05.03.2010 - New code regarding message to Toll/Avgift
                                                If EmptyString(sStatebankCode) Then
                                                    WriteSegment("FTX+REG++KAT+14")
                                                Else
                                                    WriteSegment("FTX+REG++KAT+" & sStatebankCode)
                                                End If
                                                If EmptyString(sStatebankText) Then
                                                    WriteSegment("FTX+REG++ATR+Eksport/Import av varer")
                                                Else
                                                    WriteSegment("FTX+REG++ATR+" & sStatebankText)
                                                End If

                                                'Old code
                                                '''''                                        If EmptyString(sStatebankText) Then
                                                '''''                                            If EmptyString(sStatebankCode) Then
                                                '''''                                                WriteFTXSegment "REG", "Export/Import av varer", 14
                                                '''''                                            Else
                                                '''''                                                WriteFTXSegment "REG", "Export/Import av varer", sStatebankCode
                                                '''''                                            End If
                                                '''''                                        Else
                                                '''''                                            If EmptyString(sStatebankCode) Then
                                                '''''                                                WriteFTXSegment "REG", sStatebankText, 14
                                                '''''                                            Else
                                                '''''                                                WriteFTXSegment "REG", sStatebankText, sStatebankCode
                                                '''''                                            End If
                                                '''''                                        End If

                                                If Not EmptyString(sLocalFreetext) Then
                                                    WriteSegment("PRC+11")
                                                    '16-FTX
                                                    'NOMORE
                                                    'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                    WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
                                                    '23-GIS
                                                    WriteSegment("GIS+37")
                                                Else
                                                    'No advice
                                                End If

                                                oPayment.Exported = True

                                            Else
                                                '11-RFF
                                                If Len(oBatch.REF_Own) > 0 Then 'New 06.08.2009 - May cause problems for other users (GE HEalthcare, but probably are oBatch.REF_own = "" for them
                                                    If Not EmptyString((oPayment.REF_Own)) Then
                                                        WriteRFFSegment("CR3", (oPayment.REF_Own))
                                                    Else
                                                        If Not EmptyString(sLocalREF_Own) Then
                                                            WriteRFFSegment("CR3", sLocalREF_Own)
                                                        End If
                                                    End If
                                                Else
                                                    WriteRFFSegment("CR3", sLocalREF_Own)
                                                End If

                                                bThisIsKID = False

                                                If sSpecial = "VISMA_FOKUS" Then
                                                    If oPayment.PayCode = "629" Then
                                                        If oPayment.Invoices.Count = 1 Then
                                                            If Not EmptyString((oPayment.Invoices.Item(1).Unique_Id)) Then
                                                                oPayment.PayCode = "510"
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                                If IsOCR((oPayment.PayCode)) And oPayment.Invoices.Count = 1 Then
                                                    'Since KID is stated in the SEQ segment we may run into problems if we have stated more than 1 KID in the invoiceobject
                                                    bThisIsKID = True
                                                    '11-RFF
                                                    WriteRFFSegment("CR", (oPayment.Invoices.Item(1).Unique_Id))
                                                    '11-PAI
                                                    WriteSegment("PAI+::NLK:::NKR") 'OCR
                                                Else
                                                    '11-RFF
                                                    If Not EmptyString((oPayment.Text_E_Statement)) Then
                                                        WriteRFFSegment("CR", (oPayment.Text_E_Statement), 25)
                                                    End If
                                                    '11-PAI
                                                    If oPayment.PayType = "S" Then
                                                        WriteSegment("PAI+::NLL")
                                                    ElseIf oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                                        WriteSegment("PAI+::NLU:::NUU") 'Cheque
                                                    ElseIf sSpecial = "VISMA_FOKUS" Then
                                                        If EmptyString(sLocalFreetext) Then
                                                            WriteSegment("PAI+::NLK:::NKM") 'No message
                                                        Else
                                                            WriteSegment("PAI+::NLK:::NKU") 'Message
                                                        End If
                                                    ElseIf Len(oPayment.E_Account) = CDbl("11") Then
                                                        If oPayment.E_Account = "00000000019" Then
                                                            'Used in some cases where no account is stated. Will be cheque
                                                            WriteSegment("PAI+::NLU:::NUU") 'Cheque
                                                            oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver
                                                        Else
                                                            If EmptyString(sLocalFreetext) Then
                                                                WriteSegment("PAI+::NLK:::NKM") 'No message
                                                            Else
                                                                WriteSegment("PAI+::NLK:::NKU") 'Message
                                                            End If
                                                        End If
                                                    Else
                                                        WriteSegment("PAI+::NLU:::NUU") 'Cheque
                                                        oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver
                                                    End If
                                                End If

                                                '12-FII
                                                If Not oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                                    WriteFIISegment(E_Info, True, (oPayment.I_CountryCode))
                                                End If
                                                '13-NAD
                                                WriteNADSegment(True, True)
                                                If oPayment.PayType <> "S" And Not EmptyString(sLocalFreetext) And Not bThisIsKID Then
                                                    '16-PRC
                                                    WriteSegment("PRC+11")
                                                    '16-FTX
                                                    WriteFTXSegment("PMD", sLocalFreetext)
                                                    '23-GIS
                                                    WriteSegment("GIS+37")
                                                End If
                                            End If

                                        Case Else
                                            '"15126: Possibility to send payments from %1" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                                            Err.Raise(15126, "CreateEDIFile", LRS(15126, oPayment.I_CountryCode))
                                    End Select
                                End If 'If bThisIsOverforselFraKontoIUtland Then
                                lArrayCounter = lArrayCounter + 1

                                'NOMORE
                                'Loop
                            End If 'If oPayment.I_CountryCode = "FI" And oPayment.PayType = "I" Then
                            'Next oInvoice
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch
                    WriteCNTSegment()
                    WriteUNTSegment(sUNHReference)

                Next oBabelFile
                WriteUNZSegment(sUNBReference)
                lUNHCounter = 0

            Case vbBabel.BabelFiles.Bank.SEB_NO
                'bx = mrEDISettingsExport.InitiateSpecialSegments
                'mrEDISettingsExport.SetCurrentElement ("//HEADER/ATTRIBUTES")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                oFile.Write(("UNA:+,? '"))
                bUNBWritten = False
                bNewMessage = True
                bUNHWritten = False

                oGroupPayment = New vbBabel.GroupPayments  '23.05.2017CreateObject ("vbBabel.GroupPayments")
                oGroupPayment.GroupCrossBorderPayments = False
                bGroupPayments = False

                lLINCounter = 0

                For Each oBabelFile In oBabelFiles
                    'The next IF may be removed when new code for Gjensidige is implmented
                    'Marked 2CONTRL
                    If Not bUNBWritten And Not bNewCodeForGjensidige Then
                        If EmptyString((oBabelFile.EDI_MessageNo)) Then
                            sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
                        Else
                            sUNBReference = oBabelFile.EDI_MessageNo
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        sAdditionalNo = "" ' moved here 11.05.2010


                        ' added 11.05.2010 to be able to have different CompanyNo's, like Gjensidige:
                        For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                            If oFilesetup.FileSetup_ID = iFilesetupOut Then
                                If Not EmptyString(oFilesetup.CompanyNo) Then
                                    sCompanyNo = oFilesetup.CompanyNo
                                End If
                                If Not EmptyString(oFilesetup.AdditionalNo) Then
                                    sAdditionalNo = oFilesetup.AdditionalNo
                                End If
                            End If
                        Next oFilesetup

                        If Not EmptyString(sCompanyNo) Then
                            sIdentSender = Trim(sCompanyNo)
                        Else
                            '"No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer."
                            Err.Raise(1, "CreateEDIFile", LRS(15121))
                        End If
                        'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A+++" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1
                        '03.03.2008 Changed by Kjell
                        'sAdditionalNo = ""  ' 11.05.2010 moved up
                        If oBabelFiles.Count > 0 Then
                            If EmptyString(sAdditionalNo) Then ' added 11.05.2010
                                If Not EmptyString(oBabelFiles.Item(1).VB_Profile.AdditionalNo) Then
                                    sAdditionalNo = Trim(oBabelFiles.Item(1).VB_Profile.AdditionalNo)
                                End If
                            End If
                        End If
                        If Not EmptyString(sAdditionalNo) Then
                            WriteSegment("UNB+UNOC:3+" & sIdentSender & "+" & sAdditionalNo & ":30+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference) ' & "+1"
                        Else
                            '"15128: No 'Recipient Identification' is stated." & vbCrLf & "This may be entered in the company setup in the field 'AdditionalNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                            Err.Raise(15128, "CreateEDIFile", LRS(15128))
                        End If
                        'Old code
                        'WriteSegment "UNB+UNOC:3+" & sIdentSender & "+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference   ' & "+1"
                        bUNBWritten = True
                    End If

                    'The next IF may be removed when new code for Gjensidige is implmented
                    'Marked 2CONTRL
                    If bNewMessage And Not bNewCodeForGjensidige Then
                        lUNHCounter = lUNHCounter + 1
                        lSegmentCounter = 0
                        WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN:SEBV50")
                        WriteSegment("BGM+452+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS"))
                        If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                            WriteDTMSegment("137", DateToString(Now), "102")
                        Else
                            WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                        End If
                        WriteSegment("FII+MR++ESSESESS")
                        If Not EmptyString((oBabelFile.IDENT_Sender)) Then
                            WriteSegment("NAD+MS+" & Trim(oBabelFile.IDENT_Sender))
                        ElseIf Not EmptyString(oBabelFile.VB_Profile.CompanyNo) Then
                            WriteSegment("NAD+MS+" & Trim(oBabelFile.VB_Profile.CompanyNo))
                        Else
                            WriteSegment("NAD+MS+BabelBank")
                        End If
                        bNewMessage = False
                    End If
                    'maybe we need
                    'writesegment "NAD+MS+" & Code identifying a party involved in a transaction MS = Sender

                    For Each oBatch In oBabelFile.Batches

                        'The next IF must be used when new code for Gjensidige is implemented
                        'Marked 2CONTRL - remove the IF-sentence
                        If bNewCodeForGjensidige Then
                            'This code is vital for Gjensidige and must not be changed
                            If Not EmptyString((oBatch.Batch_ID)) Then
                                If sUNHReference <> oBatch.Batch_ID Then
                                    bUNHWritten = False
                                End If
                            End If
                        End If

                        'The next IF must be used when new code for Gjensidige is implemented
                        'Marked 2CONTRL - remove And bNewCodeForGjensidige
                        If Not bUNBWritten And bNewCodeForGjensidige Then
                            If bUNHWritten Then
                                'We have already written UNH before, and have to write a CNT and UNT before we write a new one
                                WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
                                If EmptyString(sUNHReference) Then
                                    WriteUNTSegment()
                                Else
                                    WriteUNTSegment(sUNHReference)
                                End If
                                WriteUNZSegment(sUNBReference)
                                bUNHWritten = False
                            End If
                            If Not EmptyString(sUNBReference) Then
                                'Nothing to do
                            ElseIf EmptyString((oBabelFile.EDI_MessageNo)) Then
                                sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
                            Else
                                sUNBReference = oBabelFile.EDI_MessageNo
                            End If
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            If Not EmptyString(sCompanyNo) Then
                                sIdentSender = Trim(sCompanyNo)
                            Else
                                '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                                Err.Raise(15121, "CreateEDIFile", LRS(15121))
                            End If
                            'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A+++" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1
                            '03.03.2008 Changed by Kjell
                            sAdditionalNo = ""
                            If oBabelFiles.Count > 0 Then
                                If Not EmptyString(oBabelFiles.Item(1).VB_Profile.AdditionalNo) Then
                                    sAdditionalNo = Trim(oBabelFiles.Item(1).VB_Profile.AdditionalNo)
                                End If
                            End If
                            If Not EmptyString(sAdditionalNo) Then
                                WriteSegment("UNB+UNOC:3+" & sIdentSender & "+" & sAdditionalNo & ":30+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference) ' & "+1"
                            Else
                                '"15128: No 'Recipient Identification' is stated." & vbCrLf & "This may be entered in the company setup in the field 'AdditionalNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                                Err.Raise(15128, "CreateEDIFile", LRS(15128))
                            End If
                            'Old code
                            'WriteSegment "UNB+UNOC:3+" & sIdentSender & "+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference   ' & "+1"
                            bUNBWritten = True
                        End If

                        'The next IF must be used when new code for Gjensidige is implemented
                        'Marked 2CONTRL - remove And bNewCodeForGjensidige

                        If Not bUNHWritten And bNewCodeForGjensidige Then

                            If Not bThisIsFirstUNH Then
                                'We have already written UNH before, and have to write a CNT and UNT before we write a new one
                                WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
                                If EmptyString(sUNHReference) Then
                                    WriteUNTSegment()
                                Else
                                    WriteUNTSegment(sUNHReference)
                                End If
                                bUNHWritten = False
                            End If
                            lUNHCounter = lUNHCounter + 1
                            lLINCounter = 0
                            lSegmentCounter = 0

                            If Not EmptyString((oBatch.Batch_ID)) Then
                                sUNHReference = oBatch.Batch_ID
                            End If

                            If EmptyString(sUNHReference) Then
                                sUNHReference = Trim(Str(lUNHCounter))
                            End If
                            WriteSegment("UNH+" & sUNHReference & "+PAYMUL:D:96A:UN:SEBV50")
                            WriteSegment("BGM+452+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS"))
                            If EmptyString((oBabelFile.DATE_Production)) Then
                                WriteDTMSegment("137", DateToString(Now), "102")
                            Else
                                WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                            End If
                            WriteSegment("FII+MR++ESSESESS")
                            If EmptyString((oBabelFile.IDENT_Sender)) Then
                                WriteSegment("NAD+MS+BabelBank")
                            Else
                                WriteSegment("NAD+MS+" & Trim(oBabelFile.IDENT_Sender))
                            End If
                            bNewMessage = False
                            bUNHWritten = True
                            bThisIsFirstUNH = False
                        End If

                        '05.03.2010
                        If oBatch.Payments.Count > 0 Then
                            If oBatch.Payments.Item(1).PayType = "M" And oBatch.Payments.Item(1).PayCode = "222" Then
                                oGroupPayment.SetGroupingParameters(True, True, True, True, False, True)
                                bGroupPayments = True
                            Else
                                oGroupPayment.SetGroupingParameters(False, False, False, False, False, False)
                                bGroupPayments = False
                            End If
                        Else
                            oGroupPayment.SetGroupingParameters(False, False, False, False, False, False)
                            bGroupPayments = False
                        End If

                        For lCounter2 = 1 To oBatch.Payments.Count
                            bNewLIN = True

                            oGroupPayment.Batch = oBatch
                            oGroupPayment.MarkIdenticalPayments()
                            'Group the payments inside a batch
                            lPaymentCounter = 0

                            For Each oPayment In oBatch.Payments
                                If oPayment.SpecialMark And Not oPayment.Exported Then
                                    lPaymentCounter = lPaymentCounter + 1
                                    If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                        'bNewPayments = True Don't need a new payments when the debit account is changed
                                        If oBabelFile.VB_ProfileInUse Then
                                            bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                        End If
                                        sOldAccountNo = Trim(oPayment.I_Account)
                                    End If

                                    'Find country for receiver if not stated
                                    If EmptyString((oPayment.I_CountryCode)) Then
                                        'XNET - 12.01.2012 - Added next IF
                                        If IsPaymentDomestic(oPayment) Then
                                            oPayment.PayType = "D"
                                        Else
                                            oPayment.PayType = "I"
                                        End If

                                        'If Not EmptyString((oPayment.BANK_I_SWIFTCode)) Then
                                        '    oPayment.I_CountryCode = Mid(oPayment.BANK_I_SWIFTCode, 5, 2)
                                        'Else
                                        '    oPayment.I_CountryCode = Mid(sI_SWIFTAddr, 5, 2)
                                        '    'If debit account and receiver is in the same country it must be
                                        '    '  a domestic payment
                                        '    'If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
                                        '    '    oPayment.PayType = "I"
                                        '    'Else
                                        '    '    oPayment.PayType = "D"
                                        '    'End If
                                        'End If
                                    End If


                                        '********************************************************************
                                        'Be careful to change this for other customers!!!!!!!
                                        'Removed 04.02.2008 for Gjensidige. It is correctly set during import
                                        ' Created problems for domestic payments where receiver has an address abroad

                                        '16.04.2008 - Kjell
                                        'Should be OK to remove the comment on the IsDomestic part now, because we have
                                        '  introduced a new variable in the Payment-Class (oPayment.PayTypeSetInImport).
                                        '  If this is property is True (as it now should be for Gjensidige), we use the value
                                        '  in oPayment.PayType in the IsPaymentDomestic function.
                                        'Because I'm a chicken I haven't dared to remove it yet!

                                        '                    If IsPaymentDomestic(oPayment, sI_SWIFTAddr) Then
                                        '                        oPayment.PayType = "D"
                                        '                    Else
                                        '                        oPayment.PayType = "I"
                                        '                    End If

                                        oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
                                        '''''lSEQCounter = 0
                                        If Left(oPayment.BANK_I_SWIFTCode, 4) <> "ESSE" Then
                                            '"Possibility to send payments from another bank" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            Err.Raise(15129, "CreateEDIFile", LRS(15129))
                                            'bThisIsOverforselFraKontoIUtland = True
                                        Else
                                            bThisIsOverforselFraKontoIUtland = False
                                        End If

                                        lCounter = 0
                                        lArrayCounter = 0
                                        nAmountInSEQ = 0

                                        'Not validated against valid chars for SEB
                                        oPayment.E_Name = CheckForValidCharacters((oPayment.E_Name), True, True, True, True, " /-?:().,'+")
                                        oPayment.E_Adr1 = CheckForValidCharacters((oPayment.E_Adr1), True, True, True, True, " /-?:().,'+")
                                        oPayment.E_Adr2 = CheckForValidCharacters((oPayment.E_Adr2), True, True, True, True, " /-?:().,'+")
                                        oPayment.E_Adr3 = CheckForValidCharacters((oPayment.E_Adr3), True, True, True, True, " /-?:().,'+")
                                        oPayment.E_City = CheckForValidCharacters((oPayment.E_City), True, True, True, True, " /-?:().,'+")
                                        oPayment.BANK_Name = CheckForValidCharacters((oPayment.BANK_Name), True, True, True, True, " /-?:().,'+")
                                        oPayment.BANK_Adr1 = CheckForValidCharacters((oPayment.BANK_Adr1), True, True, True, True, " /-?:().,'+")
                                        oPayment.BANK_Adr2 = CheckForValidCharacters((oPayment.BANK_Adr2), True, True, True, True, " /-?:().,'+")
                                        oPayment.BANK_Adr3 = CheckForValidCharacters((oPayment.BANK_Adr3), True, True, True, True, " /-?:().,'+")

                                        If bThisIsOverforselFraKontoIUtland Then
                                            lMaxLengthOfFreetext = 140
                                        Else
                                            Select Case oPayment.I_CountryCode

                                                Case "DE"
                                                    If oPayment.PayType = "I" Then
                                                        'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                        lMaxLengthOfFreetext = 140
                                                    Else
                                                        lMaxLengthOfFreetext = 162
                                                    End If
                                                Case "DK"
                                                    If oPayment.PayType = "I" Then
                                                        lMaxLengthOfFreetext = 140
                                                    Else
                                                        lMaxLengthOfFreetext = 1435
                                                    End If
                                                Case "SE"
                                                    If oPayment.PayType = "I" Then
                                                        lMaxLengthOfFreetext = 140
                                                    Else
                                                        lMaxLengthOfFreetext = 1050
                                                    End If
                                                Case "US"
                                                    If oPayment.PayType = "I" Then
                                                        lMaxLengthOfFreetext = 140
                                                    Else
                                                        lMaxLengthOfFreetext = 140
                                                    End If
                                                Case "NO"
                                                    If oPayment.PayType = "I" Then
                                                        lMaxLengthOfFreetext = 140
                                                    Else
                                                        lMaxLengthOfFreetext = 140 'According to SEB
                                                    End If

                                                Case Else
                                                    'sReturnString = "15126: Possibility to send payments from %1" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                                                    Err.Raise(15126, "CreateEDIFile", LRS(15126))
                                            End Select
                                        End If

                                        If lMaxLengthOfFreetext = 0 Then
                                            lMaxLengthOfFreetext = 140
                                        End If

                                        'This functionality may be used if you want to split payments into
                                        ' more than one LIN to be able to send all the freetext information.
                                        'aFreetextArray = CreateFreetextSEQAmountArray(lMaxLengthOfFreetext)
                                        'Do While lArrayCounter <= UBound(aFreetextArray(), 2)
                                        If bNewLIN Then
                                            If bGroupPayments Then
                                                bNewLIN = False
                                            End If
                                            lSEQCounter = 0
                                            '4-LIN
                                            WriteLINSegment()
                                            '4-DTM
                                            If bThisIsOverforselFraKontoIUtland Then

                                            Else
                                                Select Case oPayment.I_CountryCode
                                                    Case Else
                                                        WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                End Select
                                            End If
                                            '4-RFF
                                            If Len(oBatch.REF_Own) > 0 Then
                                                '20.02.2008 - This is done for Gjensidige to do the reference at the LIN-level
                                                ' unique.
                                                'oBatch.REF_Own = oBatch.REF_Own & "-" & Trim$(Str(lPaymentCounter))
                                                WriteRFFSegment("AEK", CheckForValidCharacters(Trim(oBatch.REF_Own) & "-" & Trim(Str(lPaymentCounter)), True, True, True, True, " /-?:().,'+"))
                                            Else
                                                WriteRFFSegment("AEK", sUNBReference & Trim(Str(lLINCounter)), 35)
                                            End If
                                            '4-BUS
                                            '03.05.2010 - for Gjensidege
                                            If oPayment.PayType = "M" And oPayment.PayCode = "222" Then
                                                WriteSegment("BUS+1:ZZZ+DO")
                                            Else
                                                WriteBUSSegment((oPayment.PayType))
                                            End If
                                            '4-FCA
                                            If oPayment.PayType = "I" Then
                                            WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                            End If
                                            '5-MOA
                                            If Len(Trim(oPayment.MON_TransferCurrency)) <> 3 Then
                                                If EmptyString((oPayment.MON_TransferCurrency)) Then
                                                    oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency
                                                Else
                                                    '"Wrong transfercurrency stated on the payment." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & vbCrLf & "Currency: " & Trim(oPayment.MON_TransferCurrency) & vbCrLf & vbCrLf & "Contact Your dealer.")
                                                    Err.Raise(15130, "CreateEDIFile", LRS(15130) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & vbCrLf & vbCrLf & LRS(10016))
                                                End If
                                            End If

                                            If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                                'NOMORE
                                                'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                                '03.05.2010
                                                If bGroupPayments Then
                                                    WriteMOASegment("9", oGroupPayment.MON_InvoiceAmount, (oPayment.MON_InvoiceCurrency))
                                                Else
                                                    WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                                End If
                                                'Old code
                                                'WriteMOASegment "9", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency
                                            Else
                                                'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                'NOMORE
                                                'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                                '03.05.2010
                                                If bGroupPayments Then
                                                    WriteMOASegment("57", oGroupPayment.MON_InvoiceAmount, "")
                                                Else
                                                    WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
                                                End If
                                                'Old code
                                                'WriteMOASegment "57", oPayment.MON_InvoiceAmount, ""
                                                '5-CUX AND 5-RFF
                                                If oPayment.ERA_ExchRateAgreed = 0 Then
                                                    If oPayment.FRW_ForwardContractRate = 0 Then
                                                        'No rates
                                                        WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                                        'Do not use 5-RFF
                                                    Else
                                                        'Raise an error because this code is not tested
                                                        '"The use of 'Forward contract rate' is not yet implemented in this format." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & vbCrLf & vbCrLf & "Contact Your dealer."
                                                        Err.Raise(15131, "CreateEDIFile", LRS(15131) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                                        'Code to be used will be something like this
                                                        WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.FRW_ForwardContractRate), , ","))
                                                        If Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                                            WriteRFFSegment("ACX", Trim(oPayment.FRW_ForwardContractNo))
                                                        Else
                                                            '"No 'Forward contract number' is stated when a" & vbCrLf & "'Forward exchange rate' is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
                                                            Err.Raise(15132, "CreateEDIFile", LRS(15132) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                                        End If
                                                    End If
                                                Else
                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.ERA_ExchRateAgreed), , ","))
                                                    If Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                                        WriteRFFSegment("FX", Trim(oPayment.ERA_DealMadeWith))
                                                    Else
                                                        '"No 'Foreign exchange contract number' is stated when an" & vbCrLf & "agreed exchangerate is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
                                                        Err.Raise(15133, "CreateEDIFile", LRS(15133) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                                    End If
                                                End If
                                            End If
                                            '6-FII
                                            'Changed 08.04.05 - Kjell
                                            WriteSegment("FII+OR+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")

                                            'Use the Invoice who contains last freetext
                                            'NOMORE
                                            'Set oInvoice = oPayment.Invoices.Item(Val(aFreetextArray(2, lArrayCounter)))
                                            If oPayment.I_CountryCode = "NO" And Trim(oPayment.PayCode) = "301" Then
                                                bThisIsKID = True
                                            Else
                                                bThisIsKID = False
                                                sLocalFreetext = ""
                                                sLocalREF_Own = ""
                                                For Each oInvoice In oPayment.Invoices
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        If oFreeText.Qualifier = CDbl("1") Then
                                                            sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                                        End If
                                                    Next oFreeText
                                                Next oInvoice
                                                If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                                    'sLocalFreetext = "SEE SEPERATE LETTER."
                                                End If
                                                sLocalFreetext = RTrim(Left(sLocalFreetext, lMaxLengthOfFreetext))
                                            End If
                                            If oPayment.Invoices.Count > 0 Then
                                                sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                            End If
                                        End If 'If bNewLIN

                                        '11-SEQ
                                        WriteSEQSegment()
                                        '11-MOA
                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                            'NOMORE
                                            'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                            WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                        Else
                                            'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            'NOMORE
                                            'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                            WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
                                        End If
                                        '11-DTM
                                        'Only to be used  for cash pool internal transfers
                                        'Must do some sort of test for this
                                        'WriteDTMSegment "209", oPayment.DATE_Payment, "102", True
                                        '11-RFF
                                        If Len(Trim(oPayment.REF_Own)) > 0 Then
                                            WriteRFFSegment("CR", CheckForValidCharacters((oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                                        Else
                                            '"No customer reference (own reference is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount
                                            Err.Raise(15134, "CreateEDIFile", LRS(15134) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                        End If
                                        '11-PAI
                                        If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                            If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                                                WriteSegment("PAI+::23")
                                            ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
                                                WriteSegment("PAI+::23")
                                            Else
                                                WriteSegment("PAI+::26")
                                            End If
                                        ElseIf oPayment.Priority Then
                                            WriteSegment("PAI+::ZZ")
                                        Else
                                            'For danish payments (and maybe others, we should be more specific)
                                            'NO PAI-segment
                                        End If
                                        '11 - FCA - Can't be included if FCA is stated at LIN-level
                                        'If oPayment.PayType = "I" Then
                                        '    WriteFCASegment oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad
                                        'End If
                                        '12 - FII
                                        If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                            'Do not use the FI-segment
                                        Else
                                            If oPayment.PayType = "I" Then
                                                WriteFIISegmentSEB(E_Info, True, (oPayment.I_CountryCode), True, True)
                                            Else
                                                If oPayment.I_CountryCode = "NO" Then
                                                    WriteFIISegmentSEB(E_Info, True, (oPayment.I_CountryCode), True, False)
                                                Else
                                                    WriteFIISegmentSEB(E_Info, True, (oPayment.I_CountryCode), True, False)
                                                End If
                                            End If
                                            If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Or Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
                                                WriteFIISegmentSEB(CorrBank, True, (oPayment.I_CountryCode), True, True)
                                            End If
                                        End If
                                        '13 - NAD
                                        WriteNADSegment(True, True)
                                        '13 - CTA
                                        'Not implemented
                                        '13 - COM
                                        'Not implemented
                                        '14 - INP
                                        'Added code here 06.03.2008
                                        '03.05.2010 - reopened the usage of text on receivers statement.
                                        If oPayment.PayType = "M" And oPayment.PayCode = "222" Then
                                            If Not EmptyString((oPayment.Text_E_Statement)) Then
                                                'Set the info in the FTX beneath on receivers bankaccount
                                                WriteSegment("INP+3:4+1:EI")
                                                '14 - FTX
                                                WriteFTXSegment("AAG", Left(Trim(oPayment.Text_E_Statement), 70))
                                            End If
                                        End If
                                        '                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "NO" Then
                                        '                        If Not EmptyString(oPayment.Text_E_Statement) Then
                                        '                            'Set the info in the FTX beneath on receivers bankaccount
                                        '                            WriteSegment "INP+3:4+1:AD"
                                        '                            '14 - FTX
                                        '                            WriteFTXSegment "AAG", Left$(Trim$(oPayment.Text_E_Statement), 70)
                                        '                        End If
                                        '                    End If
                                        '15 - GIS
                                        If oPayment.I_CountryCode = "NO" And oPayment.PayType = "I" Then
                                            'Central Bank Reporting
                                            WriteSegment("GIS+10")
                                            sStatebankText = ""
                                            sStatebankCode = ""
                                            For Each oInvoice In oPayment.Invoices
                                                sStatebankText = oInvoice.STATEBANK_Text
                                                sStatebankCode = oInvoice.STATEBANK_Code
                                                If Not EmptyString(sStatebankText) Then
                                                    Exit For
                                                End If
                                            Next oInvoice
                                            If EmptyString(sStatebankText) Then
                                                If EmptyString(sStatebankCode) Then
                                                    WriteFTXSegment("REG", "Export/Import av varer", CStr(14))
                                                Else
                                                    WriteFTXSegment("REG", "Export/Import av varer", sStatebankCode)
                                                End If
                                            Else
                                                If EmptyString(sStatebankCode) Then
                                                    WriteFTXSegment("REG", sStatebankText, CStr(14))
                                                Else
                                                    WriteFTXSegment("REG", sStatebankText, sStatebankCode)
                                                End If
                                            End If

                                        End If
                                        '15 - FTX
                                        'WriteFTXSegment "REG", CheckForValidCharacters(oInvoice.STATEBANK_Text + ???oInvoice.STATEBANK_Code
                                        '16-PRC
                                        If bThisIsKID Then
                                            For Each oInvoice In oPayment.Invoices
                                                WriteSegment("PRC+8")
                                                WriteDOCSegment("YW3", Trim(oInvoice.Unique_Id), 35)
                                                WriteMOASegment(CStr(12), (oInvoice.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                            Next oInvoice
                                            '23-GIS
                                            WriteSegment("GIS+37")
                                            '23-MOA
                                            WriteMOASegment("128", (oPayment.MON_InvoiceAmount), "")
                                        Else
                                            If Not EmptyString(sLocalFreetext) Then
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                'No advice
                                            End If
                                        End If
                                        oPayment.Exported = True

                                    End If 'If oPayment.SpecialMark And Not oPayment.Exported Then

                            Next oPayment
                            oGroupPayment.RemoveSpecialMark()
                            If oGroupPayment.AllPaymentsMarkedAsExported Then
                                Exit For
                            End If
                        Next lCounter2
                    Next oBatch
                Next oBabelFile
                WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))

                'The next IF must be used when new code for Gjensidige is implemented
                'Marked 2CONTRL - remove the IF
                If bNewCodeForGjensidige Then
                    WriteUNTSegment(sUNHReference)
                Else
                    WriteUNTSegment()
                End If
                WriteUNZSegment(sUNBReference)

            Case Else
                Err.Raise(20001, "CreateEDIFile", LRS(15106, sFormat, GetEnumBank(eBank)))
                '20001: BabelBank is not adopted to create this format. Format: %1 Bank: %2


        End Select

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        CreateEDIFile = True


        Exit Function

ErrorCreateEDIFile:
        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        Err.Raise(Err.Number, Err.Source, Err.Description)

    End Function

    Private Function CreateFreetextSEQAmountArray(Optional ByRef lMaxLength As Integer = 1750) As String()
        Dim aLocalArray(,) As String
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aLocalArray() As String
        Dim lArrayCounter, lInvoiceCounter As Integer
        Dim oLocalInvoice As vbBabel.Invoice
        Dim oLocalFreetext As vbBabel.Freetext
        Dim sTempstring, sFreetextstring As String
        Dim nSEQAmount As Double

        lArrayCounter = 0
        lInvoiceCounter = 0
        nSEQAmount = 0
        sTempstring = ""

        For Each oLocalInvoice In oPayment.Invoices
            lInvoiceCounter = lInvoiceCounter + 1
            nSEQAmount = nSEQAmount + oLocalInvoice.MON_InvoiceAmount
            For Each oLocalFreetext In oLocalInvoice.Freetexts
                sFreetextstring = sFreetextstring & RTrim(oLocalFreetext.Text) & " "
            Next oLocalFreetext
            'Take care. If the text we add on the last freetextobject make the string to long
            ' First add the old string to the array, then empty the old string and add the new string
            If Len(sTempstring) + Len(sFreetextstring) > lMaxLength Then
                'The maxlength of the string is reached, add an element to the array
                'First test a special case where the new freetext in itself are longer than the
                '  maxlength. If so, write what is possible
                If Len(sTempstring) = 0 Then
                    sTempstring = sFreetextstring
                    sFreetextstring = ""
                End If
                ReDim aLocalArray(2, lArrayCounter)
                'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(0, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aLocalArray(0, lArrayCounter) = Left(sTempstring, lMaxLength)
                If nSEQAmount < 0 Then
                    '"The total amount of all invoices joined regarding the freetext is negative." & vbCrLf & vbCrLf & "Contact Your dealer!")
                    Err.Raise(15135, "CreateFreetext", LRS(15135) & vbCrLf & vbCrLf & LRS(10016))
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(1, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aLocalArray(1, lArrayCounter) = Trim(Str(nSEQAmount - oLocalInvoice.MON_InvoiceAmount))
                'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(2, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aLocalArray(2, lArrayCounter) = Str(lInvoiceCounter)
                'If Len(sTempstring) = lMaxLength Then
                '   sTempstring = ""
                'Else
                '   sTempstring = Mid$(sTempstring, lMaxLength + 1)
                'End If
                sTempstring = sFreetextstring
                sFreetextstring = ""
                lArrayCounter = lArrayCounter + 1
                nSEQAmount = oLocalInvoice.MON_InvoiceAmount
            Else
                sTempstring = sTempstring & sFreetextstring
                sFreetextstring = ""
            End If
        Next oLocalInvoice

        'Usually the some freetext are not written, add it to the array
        If Len(sTempstring) > 0 Then
            ReDim Preserve aLocalArray(2, lArrayCounter)
            'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(0, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aLocalArray(0, lArrayCounter) = Left(sTempstring, lMaxLength)
            'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(1, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aLocalArray(1, lArrayCounter) = Trim(Str(nSEQAmount))
            'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(2, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aLocalArray(2, lArrayCounter) = Str(lInvoiceCounter)
            sTempstring = ""
        End If

        'Even if we don't have any freetext add an entry to the array.
        ' Then we dont't have to test for array_isempty later
        If Array_IsEmpty(aLocalArray) Then
            ReDim Preserve aLocalArray(2, lArrayCounter)
            'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(0, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aLocalArray(0, lArrayCounter) = ""
            'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(0, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aLocalArray(0, lArrayCounter) = "0"
            'UPGRADE_WARNING: Couldn't resolve default property of object aLocalArray(2, lArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aLocalArray(2, lArrayCounter) = Str(lInvoiceCounter)
        End If

        CreateFreetextSEQAmountArray = VB6.CopyArray(aLocalArray)

    End Function

    Private Function WriteSegment(ByRef sSegmentString As String, Optional ByRef bWriteTheRestOfTheString As Boolean = False) As Boolean

        If Not EmptyString(sSegmentString) Or (bWriteTheRestOfTheString = True And Not EmptyString(sDnBNOR_INPSString)) Then

            If Not Left(sSegmentString, 3) = "UNA" Then
                If bWriteTheRestOfTheString = False Then
                    lSegmentCounter = lSegmentCounter + 1
                End If
            End If

            If Not EmptyString(sSegmentString) Then
                sSegmentString = sSegmentString & "'"
            End If

            If Not RunTime() And 1 = 2 Then
                oFile.WriteLine((sSegmentString))
                'XNET - 14.09.2011 - Added next ElseIf
            ElseIf bWrite80 Then
                If bWriteTheRestOfTheString Then
                    oFile.WriteLine(sDnBNOR_INPSString)
                Else
                    sDnBNOR_INPSString = sDnBNOR_INPSString + sSegmentString
                    If Len(sDnBNOR_INPSString) > 80 Then
                        oFile.WriteLine(Left$(sDnBNOR_INPSString, 80))
                        sDnBNOR_INPSString = Mid$(sDnBNOR_INPSString, 81)
                    End If
                End If
            Else
                If bWriteWithoutCrLf Then
                    oFile.Write(sSegmentString)
                ElseIf eReceivingBank = vbBabel.BabelFiles.Bank.SEB_NO Then
                    'If RunTime Then
                    oFile.Write((sSegmentString))
                    'Else
                    '    oFile.WriteLine (sSegmentString)
                    'End If
                    'Use the next statement if You want a file with CrLf per segment

                    'Uncomment the nest ElseIF if you want CrLf at end of each segment
                    ' cut80, 80 length, length 80, 80 line  (so we can search if we want to uncomment ! )
                    'ElseIf Not RunTime() Then
                    '   oFile.WriteLine((sSegmentString))
                    'Use the next statement if You want a DnBNOR INPS file with CrLf per segment
                    'ElseIf eReceivingBank = DnBNOR_INPS And RunTime Then
                    'Use the next statement if You want a DnBNOR INPS file with CrLf per 80 positions
                ElseIf eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Or eReceivingBank = vbBabel.BabelFiles.Bank.DnB Or eReceivingBank = vbBabel.BabelFiles.Bank.BBS Then
                    If bWriteTheRestOfTheString Then
                        oFile.WriteLine((sDnBNOR_INPSString))
                    Else
                        sDnBNOR_INPSString = sDnBNOR_INPSString & sSegmentString
                        If Len(sDnBNOR_INPSString) > 80 Then
                            oFile.WriteLine((Left(sDnBNOR_INPSString, 80)))
                            sDnBNOR_INPSString = Mid(sDnBNOR_INPSString, 81)
                        End If
                    End If
                Else
                    oFile.WriteLine((sSegmentString))
                End If
            End If
        End If

    End Function

    Private Function WriteBUSSegment(ByRef sPaymentType As String, Optional ByRef sPayCode As String = "", Optional ByRef sAdditionalCode As String = "") As Boolean
        Dim sLine As String
        Dim lCounter As Integer


        sLine = "BUS++"

        If sPaymentType = "I" Then
            sLine = sLine & "IN"
        Else
            sLine = sLine & "DO"
        End If

        If Not EmptyString(sPayCode) Then
            sLine = sLine & "++" & sPayCode
        End If

        If Not EmptyString(sAdditionalCode) Then
            sLine = sLine & sAdditionalCode
        End If

        WriteSegment(sLine)

    End Function

    Private Function WriteCNTSegment() As Boolean

        WriteSegment("CNT+LIN:" & Trim(Str(lLINCounter)))

    End Function

    Private Function WriteCUXSegment(ByRef sInvoiceCurrency As String, ByRef sTransferCurrency As String, Optional ByRef sExchangeRate As String = "") As Boolean
        Dim sLine As String

        sLine = "CUX+"

        If Not EmptyString(sInvoiceCurrency) Then
            sLine = sLine & "2:" & sInvoiceCurrency
            If Not EmptyString(sTransferCurrency) Then
                sLine = sLine & "+3:" & sTransferCurrency
            End If
        ElseIf Not EmptyString(sTransferCurrency) Then
            sLine = sLine & "+3:" & sTransferCurrency
        End If

        If sExchangeRate <> "" Then
            If CDbl(sExchangeRate) <> 0 Then '0 is the default value in the collections
                sLine = sLine & "+" & sExchangeRate 'The exchangerate must be correct formatted in CreateEDIFile
            End If
        End If

        If Len(sLine) > 4 Then
            WriteSegment(sLine)
        End If

    End Function
    Private Function WriteDOCSegment(ByRef sQualifier As String, ByRef sReference As String, Optional ByRef iMaxLength As Short = 35) As Boolean
        Dim sLine As String

        sLine = "DOC+" & sQualifier

        If Not EmptyString(sReference) Then
            sLine = sLine & "+" & Left(RTrim(CheckForSeperators(sReference)), iMaxLength)
        End If

        If Len(sLine) > 4 Then
            WriteSegment(sLine)
        End If

    End Function
    Private Function WriteDTMSegment(ByRef sQualifier As String, ByRef sDate As String, ByRef sFormat As String, Optional ByRef bNotEarlierThanToday As Boolean = False) As Boolean
        Dim sLine As String

        sLine = ""

        If bNotEarlierThanToday Then
            If StringToDate(sDate) < Now Then 'DateToString (Now())
                sDate = DateToString(Now)
            End If
        End If
        Select Case sFormat
            Case "102"
                sLine = "DTM+" & sQualifier & ":" & VB6.Format(StringToDate(sDate), "YYYYMMDD") & ":" & sFormat

        End Select

        If Len(sLine) > 0 Then
            WriteSegment(sLine)
        End If

    End Function

    Private Function WriteFCASegment(ByVal bChargeMeDomestic As Boolean, ByVal bChargeMeAbroad As Boolean, ByVal bOutgoingPayment As Boolean) As Boolean
        Dim sLine As String

        sLine = "FCA+"

        If bOutgoingPayment Then
            If bChargeMeDomestic Then
                If bChargeMeAbroad Then
                    sLine = sLine & "15"
                Else
                    sLine = sLine & "14"
                End If
            Else
                sLine = sLine & "13"
            End If
        Else
            If bChargeMeDomestic Then
                If bChargeMeAbroad Then
                    sLine = sLine & "13"
                Else
                    sLine = sLine & "14"
                End If
            Else
                sLine = sLine & "15"
            End If
        End If

        If Len(sLine) > 4 Then
            WriteSegment(sLine)
        End If

    End Function

    Private Function WriteFIISegment(ByRef iTypeOfInformation As Short, ByRef bOutgoingPayment As Boolean, ByRef sCountryCode As String, Optional ByRef bDanskeBankOverforselFraKontoIUtland As Boolean = False, Optional ByRef bMandatory As Boolean = False, Optional ByRef bWriteCountryCode As Boolean = True, Optional ByRef sQualificator As String = "", Optional ByRef eBank As vbBabel.BabelFiles.Bank = vbBabel.BabelFiles.Bank.No_Bank_Specified) As Boolean

        'iTypeOfInformation is set to E_Info if You want to write the E_variables
        ' I_Info is to write the I-varables
        ' CorrBank to write the BANKCORR-variables
        'bOutgoingPayment is true for PAYMUL, DEBMUL etc,, False for CREMUL

        Dim sLine, sTempText As String
        Dim bSWIFTCodeWritten, bBranchNoWritten As Boolean
        Dim bEnoughBankInfoWritten As Boolean
        Dim sBankNameAddress, sBankNameAddress2 As String
        Dim bWriteSegment As Boolean
        Dim sBranchCodeString As String

        bSWIFTCodeWritten = False
        bBranchNoWritten = False
        bEnoughBankInfoWritten = False
        sBankNameAddress = ""
        sBankNameAddress2 = ""
        sTempText = ""
        bWriteSegment = False
        sBranchCodeString = ""
        sLine = "FII+"

        If Not bDanskeBankOverforselFraKontoIUtland Then
            If iTypeOfInformation = E_Info Then
                sQualificator = Trim(sQualificator)
                If Len(sQualificator) = 2 Then
                    'Qualificator is passed to the function
                    sLine = sLine & sQualificator & "+"
                ElseIf bOutgoingPayment Then
                    'Receiver
                    sLine = sLine & "BF+"
                Else
                    'Payer
                    sLine = sLine & "OR+"
                End If
                'AccountNumber
                If Not EmptyString((oPayment.E_Account)) Then
                    sLine = sLine & CheckForSeperators((oPayment.E_Account))
                    bWriteSegment = True
                End If
                Select Case sCountryCode 'This is payers countrycode
                    Case "DE"
                        If oPayment.PayType = "I" Then
                            If IsIBANNumber((oPayment.E_Account), False) = Scripting.Tristate.TristateMixed Then
                                If eReceivingBank <> vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                                    bEnoughBankInfoWritten = True
                                Else
                                    bEnoughBankInfoWritten = False
                                End If
                            End If
                            If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                                bSWIFTCodeWritten = True
                                bEnoughBankInfoWritten = True
                            End If
                            If Not EmptyString((oPayment.BANK_BranchNo)) Then
                                If bSWIFTCodeWritten Then
                                    sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                                Else
                                    sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                                End If

                                Select Case eBank

                                    Case vbBabel.BabelFiles.Bank.DnBNOR_INPS
                                        Select Case oPayment.BANK_BranchType
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                sLine = sLine & sTempText & "25:131"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                sLine = sLine & sTempText & "25:137"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Chips
                                                sLine = sLine & sTempText & "44:114"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                                sLine = sLine & sTempText & "25:19"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.SortCode
                                                sLine = sLine & sTempText & "154:133"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.US_ABA
                                                sLine = sLine & sTempText & "25:114"
                                                bBranchNoWritten = True
                                            Case Else
                                                'Nothing to do, don't add sTempText
                                        End Select

                                    Case Else

                                        Select Case oPayment.BANK_BranchType
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                sLine = sLine & sTempText & "BL:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                sLine = sLine & sTempText & "AT:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Chips
                                                sLine = sLine & sTempText & "CP:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                                sLine = sLine & sTempText & "FW:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.SortCode
                                                sLine = sLine & sTempText & "SC:130"
                                                bBranchNoWritten = True
                                                '                Case BankBranchType.US_ABA
                                                '                    'Not implemented
                                                '                    sLine = sLine & sTempText & "SC:130"
                                                '                    bBranchNoWritten = True
                                            Case Else
                                                'Nothing to do, don't add sTempText
                                        End Select
                                End Select

                            End If
                            If Not bEnoughBankInfoWritten Then
                                If Not EmptyString((oPayment.BANK_Name)) Then
                                    sTempText = ""
                                    If bBranchNoWritten Then
                                        sTempText = ":"
                                    Else
                                        If bSWIFTCodeWritten Then
                                            sTempText = "::::::"
                                        Else
                                            sTempText = "+::::::"
                                        End If
                                    End If
                                    sBankNameAddress = CheckForSeperators((oPayment.BANK_Name)) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr1))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr2))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr3)))
                                    If Len(sBankNameAddress) > 70 Then
                                        If Len(sBankNameAddress) > 140 Then
                                            sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                                        Else
                                            sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                                        End If
                                        sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                                    Else
                                        sBankNameAddress = Trim(sBankNameAddress)
                                    End If
                                    sLine = sLine & sTempText & sBankNameAddress
                                    If Len(sBankNameAddress2) > 0 Then
                                        sLine = sLine & ":" & sBankNameAddress2
                                    End If
                                    If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                                        If bEnoughBankInfoWritten Then
                                            'OK, we don't need the countrycode
                                        Else
                                            ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                        End If
                                    Else
                                        sLine = sLine & "+" & oPayment.BANK_CountryCode
                                    End If
                                    bEnoughBankInfoWritten = True
                                End If
                            End If
                            If Not bEnoughBankInfoWritten Then
                                ' KJell, her er det ikke noe info om betalingen. Ref Mats M�ller
                                ErrorWritingEDI(1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            Else
                                bWriteSegment = True
                            End If
                        Else
                            If IsIBANNumber((oPayment.E_Account), False) = Scripting.Tristate.TristateMixed Then
                                If eReceivingBank <> vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                                    bEnoughBankInfoWritten = True
                                Else
                                    bEnoughBankInfoWritten = False
                                End If
                            End If

                            'For DnBNOR INPS add SWIFTCode as well
                            If Not EmptyString((oPayment.BANK_SWIFTCode)) And eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                                sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                                bSWIFTCodeWritten = True
                                bEnoughBankInfoWritten = True
                            End If
                            'Add BLZ-Code
                            If Not EmptyString(oPayment.BANK_BranchNo) Then
                                If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Bankleitzahl Then
                                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                                        sBranchCodeString = ":25:131"
                                    Else
                                        'XokNET - 09.01.2014 - Added comment
                                        'Do we lack an + here?
                                        sBranchCodeString = ":BL:130"
                                    End If
                                    If bSWIFTCodeWritten Then
                                        sLine = sLine & ":" & CheckForSeperators(oPayment.BANK_BranchNo) & sBranchCodeString
                                    Else
                                        sLine = sLine & "+:::" & CheckForSeperators(oPayment.BANK_BranchNo) & sBranchCodeString
                                    End If
                                    bEnoughBankInfoWritten = True
                                    bWriteSegment = True
                                End If
                            End If
                            'XokNET 09.01.2013 - Added next IF..Else
                            If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                                If bEnoughBankInfoWritten Then
                                    'OK, we don't need the countrycode
                                Else
                                    ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                End If
                            Else
                                sLine = sLine & "+" & oPayment.BANK_CountryCode
                            End If

                            If Not bEnoughBankInfoWritten Then
                                'The sortcode is mandatory
                                ErrorWritingEDI(1, "WriteFIISegment", "Bankleitzahl is mandatory for domestic German payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If
                        End If
                    Case "DK"
                        If oPayment.PayType = "I" Then
                            sLine = WriteFIISegmentCrossBorderStandard(iTypeOfInformation, bOutgoingPayment, sCountryCode, bDanskeBankOverforselFraKontoIUtland, bMandatory, bWriteCountryCode)
                            bSWIFTCodeWritten = True
                            bEnoughBankInfoWritten = True
                            '                If IsIBANNumber(oPayment.E_Account, False) = TristateMixed Then
                            '                    If Not eReceivingBank = Bank.DnBNOR_INPS Then
                            '                        bEnoughBankInfoWritten = True
                            '                    End If
                            '                End If
                            '                If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                            '                    If Not eReceivingBank = Bank.DnBNOR_INPS Then
                            '                        sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                            '                    Else
                            '                        sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                            '                    End If
                            '                    bSWIFTCodeWritten = True
                            '                    bEnoughBankInfoWritten = True
                            '                End If
                        Else
                            'No more information needed
                        End If

                    Case "GB", "FI"
                        If oPayment.PayType = "I" Then
                            If IsIBANNumber((oPayment.E_Account), False) = Scripting.Tristate.TristateMixed Then
                                bEnoughBankInfoWritten = True
                            End If
                            If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                                bSWIFTCodeWritten = True
                                bEnoughBankInfoWritten = True
                            End If
                            If Not EmptyString((oPayment.BANK_BranchNo)) Then
                                If bSWIFTCodeWritten Then
                                    sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                                Else
                                    sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                                End If

                                Select Case eBank

                                    Case vbBabel.BabelFiles.Bank.DnBNOR_INPS
                                        Select Case oPayment.BANK_BranchType
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                sLine = sLine & sTempText & "25:131"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                sLine = sLine & sTempText & "25:137"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Chips
                                                sLine = sLine & sTempText & "44:114"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                                sLine = sLine & sTempText & "25:19"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.SortCode
                                                sLine = sLine & sTempText & "154:133"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.US_ABA
                                                sLine = sLine & sTempText & "25:114"
                                                bBranchNoWritten = True
                                            Case Else
                                                'Nothing to do, don't add  sTempText
                                        End Select

                                    Case Else

                                        Select Case oPayment.BANK_BranchType
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                sLine = sLine & sTempText & "BL:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                sLine = sLine & sTempText & "AT:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Chips
                                                sLine = sLine & sTempText & "CP:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                                sLine = sLine & sTempText & "FW:130"
                                                bBranchNoWritten = True
                                            Case vbBabel.BabelFiles.BankBranchType.SortCode
                                                sLine = sLine & sTempText & "SC:130"
                                                bBranchNoWritten = True
                                                '                Case BankBranchType.US_ABA
                                                '                    'Not implemented
                                                '                    sLine = sLine & sTempText & "SC:130"
                                                '                    bBranchNoWritten = True
                                            Case Else
                                                'Nothing to do, don't add sTempText
                                        End Select

                                End Select
                            End If
                            'If Not bEnoughBankInfoWritten Then
                            If Not EmptyString((oPayment.BANK_Name)) Then
                                sTempText = ""
                                If bBranchNoWritten Then
                                    sTempText = ":"
                                Else
                                    If bSWIFTCodeWritten Then
                                        sTempText = "::::"
                                    Else
                                        sTempText = "+::::::"
                                    End If
                                End If
                                sBankNameAddress = CheckForSeperators((oPayment.BANK_Name)) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr1))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr2))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr3)))
                                If Len(sBankNameAddress) > 70 Then
                                    If Len(sBankNameAddress) > 140 Then
                                        sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                                    Else
                                        sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                                    End If
                                    sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                                Else
                                    sBankNameAddress = Trim(sBankNameAddress)
                                End If
                                sLine = sLine & sTempText & sBankNameAddress
                                If Len(sBankNameAddress2) > 0 Then
                                    sLine = sLine & ":" & sBankNameAddress2
                                End If
                                If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                                    If bEnoughBankInfoWritten Then
                                        'OK, we don't need the countrycode
                                    Else
                                        ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                    End If
                                Else
                                    bEnoughBankInfoWritten = True
                                    sLine = sLine & "+" & oPayment.BANK_CountryCode
                                End If
                            End If
                            'End If
                            If Not bEnoughBankInfoWritten Then
                                ' KJell, her er det ikke noe info om betalingen. Ref Mats M�ller
                                ErrorWritingEDI(1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            Else
                                bWriteSegment = True
                            End If
                        Else
                            If sCountryCode = "GB" Then
                                'Have to add SortCode

                                bSWIFTCodeWritten = False
                                If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                                        sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                                    Else
                                        sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                                    End If
                                    bSWIFTCodeWritten = True
                                    bEnoughBankInfoWritten = True
                                End If
                                If Not EmptyString((oPayment.BANK_BranchNo)) Then
                                    If bSWIFTCodeWritten Then
                                        sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                                    Else
                                        sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                                    End If

                                    If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode Then
                                        If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                                            sLine = sLine & sTempText & "154:133"
                                        Else
                                            sLine = sLine & sTempText & "SC:130"
                                        End If
                                        bWriteSegment = True
                                    Else
                                        ErrorWritingEDI(1, "WriteFIISegment", "Sortcode is mandatory for BACS-payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                    End If
                                Else
                                    'The sortcode is mandatory
                                    ErrorWritingEDI(1, "WriteFIISegment", "Sortcode is mandatory for BACS-payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                End If
                            Else '"FI"
                                'XNET - 13.10.2011 - Added new code in this Else clause
                                If IsIBANNumber(oPayment.E_Account, False) = Scripting.Tristate.TristateMixed Then
                                    bEnoughBankInfoWritten = True
                                End If
                                If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                                    bSWIFTCodeWritten = True
                                    bEnoughBankInfoWritten = True
                                End If

                            End If
                        End If
                    Case "NO"
                        If oPayment.PayType = "I" Then
                            sLine = WriteFIISegmentCrossBorderStandard(iTypeOfInformation, bOutgoingPayment, sCountryCode, bDanskeBankOverforselFraKontoIUtland, bMandatory, bWriteCountryCode)
                            bSWIFTCodeWritten = True
                            bEnoughBankInfoWritten = True
                        Else
                            'No more information needed
                        End If


                    Case "SE"
                        If oPayment.PayType = "I" Then
                            sLine = WriteFIISegmentCrossBorderStandard(iTypeOfInformation, bOutgoingPayment, sCountryCode, bDanskeBankOverforselFraKontoIUtland, bMandatory, bWriteCountryCode)
                            bSWIFTCodeWritten = True
                            bEnoughBankInfoWritten = True
                        Else
                            'This function is based on that the E_AccountType is set correct.

                            'DnBNOR INPS
                            'Sweden:Applicable for Domestic Clearing payments:
                            'Swedish Bank Giro Credits
                            '3434 = 9900
                            '1131 = 157
                            '3055 = 118
                            'FII+BF+94837261+:::9900:157:118'
                            'Credit to Swedish Bank account.
                            '3434 = "Branch Clearing Number" n4
                            '1131 = 157
                            '3055 = 118
                            'Note; SG6 FII OR must reference a bank giro numnber


                            'If eReceivingBank = DnBNOR_INPS Then
                            'This code is written for DnBOR_INPS, may noy be correct for other receivers
                            Select Case oPayment.E_AccountType

                                Case vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                    sLine = sLine & "+:::9900:157:118"
                                Case vbBabel.BabelFiles.AccountType.SE_Bankaccount
                                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS And oPayment.Priority Then
                                        If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                            sLine = sLine & "+" & oPayment.BANK_SWIFTCode
                                        Else
                                            sLine = sLine & "+:::" & oPayment.BANK_BranchNo & ":157:118"
                                        End If
                                    Else
                                        sLine = sLine & "+:::" & oPayment.BANK_BranchNo & ":157:118"
                                    End If

                                Case vbBabel.BabelFiles.AccountType.SE_PlusGiro
                                    sLine = sLine & "+:::" & oPayment.BANK_BranchNo & ":157:118"
                                Case Else


                            End Select

                        End If

                    Case "US"
                        bSWIFTCodeWritten = False
                        bBranchNoWritten = False

                        If oPayment.PayType = "I" Then
                            sLine = WriteFIISegmentCrossBorderStandard(iTypeOfInformation, bOutgoingPayment, sCountryCode, bDanskeBankOverforselFraKontoIUtland, bMandatory, bWriteCountryCode)
                            bSWIFTCodeWritten = True
                            bEnoughBankInfoWritten = True
                        Else
                            '25 / 114 = ABA  (n9)
                            '25 / 19 = Fedwire ID (n9)
                            '43 / 114 = CHIPS Partner ID (n3)
                            '44 / 114 = CHIPS Universal ID (n6)

                            'If eReceivingBank = DnBNOR_INPS Then
                            'This code is written for DnBOR_INPS, may noy be correct for other receivers
                            If IsIBANNumber((oPayment.E_Account), False, False) Then
                                If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                                End If
                            Else
                                If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                                    bSWIFTCodeWritten = True
                                End If

                                Select Case oPayment.BANK_BranchType

                                    Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                        bBranchNoWritten = True
                                        bEnoughBankInfoWritten = True
                                        If bSWIFTCodeWritten Then
                                            sLine = sLine & ":" & Trim(oPayment.BANK_BranchNo) & ":25:19"
                                        Else
                                            sLine = sLine & "+:::" & Trim(oPayment.BANK_BranchNo) & ":25:19"
                                        End If
                                    Case vbBabel.BabelFiles.BankBranchType.Chips
                                        bBranchNoWritten = True
                                        bEnoughBankInfoWritten = True
                                        If bSWIFTCodeWritten Then
                                            sLine = sLine & ":" & Trim(oPayment.BANK_BranchNo) & ":43:114"
                                        Else
                                            sLine = sLine & "+:::" & Trim(oPayment.BANK_BranchNo) & ":43:114"
                                        End If

                                    Case vbBabel.BabelFiles.BankBranchType.US_ABA
                                        bBranchNoWritten = True
                                        bEnoughBankInfoWritten = True
                                        If bSWIFTCodeWritten Then
                                            sLine = sLine & ":" & Trim(oPayment.BANK_BranchNo) & ":25:114"
                                        Else
                                            sLine = sLine & "+:::" & Trim(oPayment.BANK_BranchNo) & ":25:114"
                                        End If

                                    Case Else

                                End Select

                                If Not EmptyString((oPayment.BANK_Name)) Then
                                    sTempText = ""
                                    If bBranchNoWritten Then
                                        sTempText = ":"
                                    Else
                                        If bSWIFTCodeWritten Then
                                            sTempText = "::::"
                                        Else
                                            sTempText = "+::::::"
                                        End If
                                    End If
                                    sBankNameAddress = CheckForSeperators((oPayment.BANK_Name)) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr1))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr2))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr3)))
                                    If Len(sBankNameAddress) > 70 Then
                                        If Len(sBankNameAddress) > 140 Then
                                            sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                                        Else
                                            sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                                        End If
                                        sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                                    Else
                                        sBankNameAddress = Trim(sBankNameAddress)
                                    End If
                                    sLine = sLine & sTempText & sBankNameAddress
                                    If Len(sBankNameAddress2) > 0 Then
                                        sLine = sLine & ":" & sBankNameAddress2
                                    End If
                                    If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                                        If bEnoughBankInfoWritten Then
                                            'OK, we don't need the countrycode
                                        Else
                                            ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                        End If
                                    Else
                                        bEnoughBankInfoWritten = True
                                        sLine = sLine & "+" & oPayment.BANK_CountryCode
                                    End If
                                End If

                            End If

                        End If


                    Case "BBS"
                        'Special code for BBS - not possible to state information about the receiving institution
                        If Not EmptyString((oPayment.E_Name)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Name)
                        ElseIf Not EmptyString((oPayment.E_Adr1)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Adr1)
                        ElseIf Not EmptyString((oPayment.E_Adr2)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Adr2)
                        Else
                            sLine = sLine & ":"
                        End If
                        If Not EmptyString((oPayment.E_Zip)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City)
                        End If

                    Case Else
                        'Raises an error earlier

                End Select

            ElseIf iTypeOfInformation = I_Info Then
                sQualificator = Trim(sQualificator)
                If Len(sQualificator) = 2 Then
                    'Qualificator is passed to the function
                    sLine = sLine & sQualificator & "+"
                ElseIf bOutgoingPayment Then
                    'Payer
                    sLine = sLine & "OR+"
                Else
                    'Receiver
                    sLine = sLine & "BF+"
                End If
                'AccountNumber
                If Not EmptyString((oPayment.I_Account)) Then
                    sLine = sLine & oPayment.I_Account
                    bWriteSegment = True
                End If
                If Len(sCountryCode) > 0 And bWriteCountryCode Then
                    sLine = sLine & "++" & sCountryCode
                End If

            ElseIf iTypeOfInformation = CorrBank Then
                If bOutgoingPayment Then
                    'Payer
                    sLine = sLine & "I1+"
                Else
                    'Receiver
                    sLine = sLine & "I1+" '????
                End If
                'AccountNumber not used for Corrbank
                If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Then
                    sLine = sLine & "+" & oPayment.BANK_SWIFTCodeCorrBank & ":25:5"
                    bSWIFTCodeWritten = True
                    bEnoughBankInfoWritten = True
                End If
                If Not EmptyString((oPayment.BANK_BranchNoCorrBank)) Then
                    If bSWIFTCodeWritten Then
                        sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNoCorrBank)) & ":"
                    Else
                        sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNoCorrBank)) & ":"
                    End If

                    Select Case eBank

                        Case vbBabel.BabelFiles.Bank.DnB
                            Select Case oPayment.BANK_BranchTypeCorrBank
                                Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                    sLine = sLine & sTempText & "25:131"
                                    bBranchNoWritten = True
                                Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                                    sLine = sLine & sTempText & "25:137"
                                    bBranchNoWritten = True
                                    'Case BankBranchType.Chips
                                    '    sLine = sLine & sTempText & "44:114"
                                    '    bBranchNoWritten = True
                                Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                    sLine = sLine & sTempText & "25:19"
                                    bBranchNoWritten = True
                                Case vbBabel.BabelFiles.BankBranchType.SortCode
                                    sLine = sLine & sTempText & "154:133"
                                    bBranchNoWritten = True
                                    'Case BankBranchType.US_ABA
                                    '    sLine = sLine & sTempText & "25:114"
                                    '    bBranchNoWritten = True
                                Case Else
                                    'Nothing to do, don't add sTempText
                            End Select

                    End Select

                End If
                If Not bEnoughBankInfoWritten Then
                    If Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
                        sTempText = ""
                        If bBranchNoWritten Then
                            sTempText = ":"
                        Else
                            If bSWIFTCodeWritten Then
                                sTempText = "::::::"
                            Else
                                sTempText = "+::::::"
                            End If
                        End If
                        sBankNameAddress = CheckForSeperators((oPayment.BANK_NameAddressCorrBank2)) & " " & Trim(CheckForSeperators((oPayment.BANK_NameAddressCorrBank3))) & " " & Trim(CheckForSeperators((oPayment.BANK_NameAddressCorrBank4)))
                        If Len(sBankNameAddress) > 70 Then
                            If Len(sBankNameAddress) > 140 Then
                                sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                            Else
                                sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                            End If
                            sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                        Else
                            sBankNameAddress = Trim(sBankNameAddress)
                        End If
                        sLine = sLine & sTempText & sBankNameAddress
                        If Len(sBankNameAddress2) > 0 Then
                            sLine = sLine & ":" & sBankNameAddress2
                        End If
                        'Don't show error messages for corr banks yet!
                        If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                            If bEnoughBankInfoWritten Then
                                'OK, we don't need the countrycode
                            Else
                                'ErrorWritingEDI 1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer."
                            End If
                        Else
                            sLine = sLine & "+" & oPayment.BANK_CountryCode
                        End If
                        bEnoughBankInfoWritten = True
                    End If
                End If
                If Not bEnoughBankInfoWritten Then
                    'ErrorWritingEDI 1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer."
                    bWriteSegment = True
                Else
                    bWriteSegment = True
                End If

            Else
                'iTypeOfInformation = CorrBank
                'Special code for BBS - not possible to state information about the receiving institution
                Select Case sCountryCode 'This is payers countrycode
                    Case "BBS"
                        'Don't validate this information
                        sLine = sLine & "I2+"
                        'Bankaccountno.
                        sLine = sLine & Trim(oPayment.E_Account)
                        'Receivers name
                        sLine = sLine & ":" & Trim(oPayment.E_Name)
                        sLine = sLine & ":" & oPayment.MON_TransferCurrency

                        If Not EmptyString((oPayment.E_Name)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Name)
                        ElseIf Not EmptyString((oPayment.E_Adr1)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Adr1)
                        ElseIf Not EmptyString((oPayment.E_Adr2)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Adr2)
                        Else
                            sLine = sLine & ":"
                        End If
                        If Not EmptyString((oPayment.E_Zip)) Then
                            sLine = sLine & ":" & Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City)
                        End If
                End Select
            End If
        Else
            If iTypeOfInformation = E_Info Then
                sQualificator = Trim(sQualificator)
                If Len(sQualificator) = 2 Then
                    'Qualificator is passed to the function
                    sLine = sLine & sQualificator & "+"
                Else
                    'Receiver
                    sLine = sLine & "BF+"
                End If
                'AccountNumber
                If Not EmptyString((oPayment.E_Account)) Then
                    sLine = sLine & CheckForSeperators((oPayment.E_Account))
                    bWriteSegment = True
                End If
                If oPayment.PayType = "I" Then
                    If IsIBANNumber((oPayment.E_Account), False) = Scripting.Tristate.TristateMixed Then
                        bEnoughBankInfoWritten = True
                    End If
                    If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                        sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                        bSWIFTCodeWritten = True
                        bEnoughBankInfoWritten = True
                    End If
                    If Not EmptyString((oPayment.BANK_BranchNo)) Then
                        If bSWIFTCodeWritten Then
                            sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                        Else
                            sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                        End If
                        Select Case oPayment.BANK_BranchType
                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                sLine = sLine & sTempText & "BL:130"
                                bBranchNoWritten = True
                            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                                sLine = sLine & sTempText & "AT:130"
                                bBranchNoWritten = True
                            Case vbBabel.BabelFiles.BankBranchType.Chips
                                sLine = sLine & sTempText & "CP:130"
                                bBranchNoWritten = True
                            Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                sLine = sLine & sTempText & "FW:130"
                                bBranchNoWritten = True
                            Case vbBabel.BabelFiles.BankBranchType.SortCode
                                sLine = sLine & sTempText & "SC:130"
                                bBranchNoWritten = True
                                '                Case BankBranchType.US_ABA
                                '                    'Not implemented
                                '                    sLine = sLine & sTempText & "SC:130"
                                '                    bBranchNoWritten = True
                            Case Else
                                'Nothing to do, don't add sTempText
                        End Select
                    End If
                    'If Not bEnoughBankInfoWritten Then
                    If Not EmptyString((oPayment.BANK_Name)) Then
                        sTempText = ""
                        If bBranchNoWritten Then
                            sTempText = ":"
                        Else
                            If bSWIFTCodeWritten Then
                                sTempText = "::::"
                            Else
                                sTempText = "+::::::"
                            End If
                        End If
                        sBankNameAddress = CheckForSeperators((oPayment.BANK_Name)) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr1))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr2))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr3)))
                        If Len(sBankNameAddress) > 70 Then
                            If Len(sBankNameAddress) > 140 Then
                                sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                            Else
                                sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                            End If
                            sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                        Else
                            sBankNameAddress = Trim(sBankNameAddress)
                        End If
                        sLine = sLine & sTempText & sBankNameAddress
                        If Len(sBankNameAddress2) > 0 Then
                            sLine = sLine & ":" & sBankNameAddress2
                        End If
                        If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                            If bEnoughBankInfoWritten Then
                                'OK, we don't need the countrycode
                            Else
                                ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If
                        Else
                            bEnoughBankInfoWritten = True
                            sLine = sLine & "+" & oPayment.BANK_CountryCode
                        End If
                    End If
                    'End If
                    If Not bEnoughBankInfoWritten Then
                        ErrorWritingEDI(1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer.")
                    Else
                        bWriteSegment = True
                    End If
                Else
                    If sCountryCode = "GB" Then
                        'Have to add SortCode
                        If Not EmptyString((oPayment.BANK_BranchNo)) Then
                            If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode Then
                                sLine = sLine & "+:::" & oPayment.BANK_BranchNo & ":SC:130"
                                bWriteSegment = True
                            Else
                                ErrorWritingEDI(1, "WriteFIISegment", "Sortcode is mandatory for BACS-payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If
                        Else
                            'The sortcode is mandatory
                            ErrorWritingEDI(1, "WriteFIISegment", "Sortcode is mandatory for BACS-payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                        End If
                    Else '"FI"
                        'No more information needed
                    End If
                End If
            End If

        End If 'if bDanskeBankOverforselFraKontoIUtland
        If bWriteSegment Then
            WriteSegment(sLine)
        Else
            If bMandatory Then
                Do While True
                    If Right(sLine, 1) = "+" Or Right(sLine, 1) = ":" Then
                        sLine = Mid(sLine, 1, Len(sLine) - 1)
                    Else
                        Exit Do
                    End If
                Loop
                WriteSegment(sLine)
            End If
        End If

    End Function
    Private Function WriteFIISegmentCrossBorderStandard(ByVal iTypeOfInformation As Integer, ByVal bOutgoingPayment As Boolean, ByVal sCountryCode As String, Optional ByVal bDanskeBankOverforselFraKontoIUtland As Boolean = False, Optional ByVal bMandatory As Boolean = False, Optional ByVal bWriteCountryCode As Boolean = True) As String
        Dim sLine As String, sTempText As String
        Dim bSWIFTCodeWritten As Boolean, bBranchNoWritten As Boolean
        Dim bEnoughBankInfoWritten As Boolean
        Dim sBankNameAddress As String, sBankNameAddress2 As String
        Dim bWriteSegment As Boolean

        bSWIFTCodeWritten = False
        bBranchNoWritten = False
        bEnoughBankInfoWritten = False
        sBankNameAddress = vbNullString
        sBankNameAddress2 = vbNullString
        sTempText = vbNullString
        bWriteSegment = False

        sLine = "FII+"

        If iTypeOfInformation = E_Info Then

            If bOutgoingPayment Then
                'Receiver
                sLine = sLine & "BF+"
            Else
                'Payer
                sLine = sLine & "OR+"
            End If
            'AccountNumber
            If Not EmptyString(oPayment.E_Account) Then
                sLine = sLine & CheckForSeperators(oPayment.E_Account)
                bWriteSegment = True
            End If

            If IsIBANNumber(oPayment.E_Account, False) = Scripting.Tristate.TristateMixed Then
                If Not eReceivingBank = BabelFiles.Bank.DnBNOR_INPS Then
                    bEnoughBankInfoWritten = True
                End If
            End If
            If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                If Not eReceivingBank = BabelFiles.Bank.DnBNOR_INPS Then
                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:17"
                Else
                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                End If
                bSWIFTCodeWritten = True
                bEnoughBankInfoWritten = True
            End If
            If Not EmptyString(oPayment.BANK_BranchNo) Then
                If bSWIFTCodeWritten Then
                    sTempText = ":" & CheckForSeperators(oPayment.BANK_BranchNo) & ":"
                Else
                    sTempText = "+:::" & CheckForSeperators(oPayment.BANK_BranchNo) & ":"
                End If

                Select Case eReceivingBank

                    Case BabelFiles.Bank.DnBNOR_INPS
                        'The following combinations of codes (1131/3055) are applicable
                        '  for international payments via SWIFT:-
                        ''25 / 114 = ABA  (n9)-
                        ''25 / 19 = Fedwire ID (n9)-
                        '43 / 114 = CHIPS Partner ID (n3)-
                        ''44 / 114 = CHIPS Universal ID (n6)-
                        '25 / 119 = IT (an11..23)-
                        ''25 / 131 = BL (German Bankleitzahl) (n8)-
                        ''154 / 133 = SC (APACS Brittish sorting code) (n6)-
                        ''25 / 137 = AT (Austria Bankleitzahl) (n5)-
                        '25 / 170 = CC (Canadian routing code) (n9)


                        Select Case oPayment.BANK_BranchType
                            Case BabelFiles.BankBranchType.Bankleitzahl
                                sLine = sLine & sTempText & "25:131"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                sLine = sLine & sTempText & "25:137"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Chips
                                sLine = sLine & sTempText & "44:114" '43 eller 44?
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Fedwire
                                sLine = sLine & sTempText & "25:19"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.US_ABA
                                sLine = sLine & sTempText & "25:114"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.SortCode
                                sLine = sLine & sTempText & "154:133"
                                bBranchNoWritten = True
                                '                Case BankBranchType.US_ABA
                                '                    'Not implemented
                                '                    sLine = sLine & sTempText & "SC:130"
                                '                    bBranchNoWritten = True
                            Case Else
                                sLine = sLine & ":::"
                                bBranchNoWritten = True
                                'Nothing to do, don't add sTempText
                        End Select

                        '15.03.2011 - XokNET - Added next case
                    Case BabelFiles.Bank.DnB 'This is BSK-standard
                        Select Case oPayment.BANK_BranchType
                            Case BabelFiles.BankBranchType.Bankleitzahl
                                sLine = sLine & sTempText & "25:131"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                sLine = sLine & sTempText & "25:137"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Fedwire
                                sLine = sLine & sTempText & "25:19"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.SortCode
                                sLine = sLine & sTempText & "154:133"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.US_ABA
                                sLine = sLine & sTempText & "25:114"
                                bBranchNoWritten = True
                            Case Else
                                'Nothing to do, don't add sTempText
                        End Select

                    Case Else
                        Select Case oPayment.BANK_BranchType
                            Case BabelFiles.BankBranchType.Bankleitzahl
                                sLine = sLine & sTempText & "BL:130"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                sLine = sLine & sTempText & "AT:130"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Chips
                                sLine = sLine & sTempText & "CP:130"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.Fedwire
                                sLine = sLine & sTempText & "FW:130"
                                bBranchNoWritten = True
                            Case BabelFiles.BankBranchType.SortCode
                                sLine = sLine & sTempText & "SC:130"
                                bBranchNoWritten = True
                                '                Case BankBranchType.US_ABA
                                '                    'Not implemented
                                '                    sLine = sLine & sTempText & "SC:130"
                                '                    bBranchNoWritten = True
                            Case Else
                                sLine = sLine & ":::"
                                bBranchNoWritten = True
                                'Nothing to do, don't add sTempText
                        End Select

                End Select

            End If
            'If Not bEnoughBankInfoWritten Then
            If Not EmptyString(oPayment.BANK_Name) Then
                sTempText = vbNullString
                If bBranchNoWritten Then
                    sTempText = ":"
                Else
                    If bSWIFTCodeWritten Then
                        sTempText = "::::"
                    Else
                        sTempText = "+::::"
                    End If
                End If
                sBankNameAddress = CheckForSeperators(oPayment.BANK_Name) & " " & Trim$(CheckForSeperators(oPayment.BANK_Adr1)) & " " & Trim$(CheckForSeperators(oPayment.BANK_Adr2)) & " " & Trim$(CheckForSeperators(oPayment.BANK_Adr3))
                If Len(sBankNameAddress) > 70 Then
                    If Len(sBankNameAddress) > 140 Then
                        sBankNameAddress2 = Trim$(Mid$(sBankNameAddress, 71, 70))
                    Else
                        sBankNameAddress2 = Trim$(Mid$(sBankNameAddress, 71))
                    End If
                    sBankNameAddress = Trim$(Left$(sBankNameAddress, 70))
                Else
                    sBankNameAddress = Trim$(sBankNameAddress)
                End If
                sLine = sLine & sTempText & sBankNameAddress
                If Len(sBankNameAddress2) > 0 Then
                    sLine = sLine & ":" & sBankNameAddress2
                End If
                If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                    If bEnoughBankInfoWritten Then
                        'OK, we don't need the countrycode
                    Else
                        ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                    End If
                Else
                    sLine = sLine & "+" & oPayment.BANK_CountryCode
                End If
                bEnoughBankInfoWritten = True
            End If
            'End If
            If Not bEnoughBankInfoWritten Then
                ErrorWritingEDI(1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer.")
            Else
                bWriteSegment = True
            End If
        End If


        WriteFIISegmentCrossBorderStandard = sLine

    End Function
    Private Function WriteFIISegmentSEB(ByRef iTypeOfInformation As Short, ByRef bOutgoingPayment As Boolean, ByRef sCountryCode As String, Optional ByRef bMandatory As Boolean = False, Optional ByRef bWriteCountryCode As Boolean = True) As Boolean

        'iTypeOfInformation is set to E_Info if You want to write the E_variables
        ' I_Info is to write the I-varables
        ' CorrBank to write the BANKCORR-variables
        'bOutgoingPayment is true for PAYMUL, DEBMUL etc,, False for CREMUL

        Dim sLine, sTempText As String
        Dim bSWIFTCodeWritten, bBranchNoWritten As Boolean
        Dim bEnoughBankInfoWritten As Boolean
        Dim sBankNameAddress, sBankNameAddress2 As String
        Dim bWriteSegment As Boolean

        bSWIFTCodeWritten = False
        bBranchNoWritten = False
        bEnoughBankInfoWritten = False
        sBankNameAddress = ""
        sBankNameAddress2 = ""
        sTempText = ""
        bWriteSegment = False
        sLine = "FII+"

        If iTypeOfInformation = E_Info Then
            If bOutgoingPayment Then
                'Receiver
                sLine = sLine & "BF+"
            Else
                'Payer
                sLine = sLine & "OR+"
            End If
            'AccountNumber
            If Not EmptyString((oPayment.E_Account)) Then
                sLine = sLine & CheckForSeperators((oPayment.E_Account))
                bWriteSegment = True
            End If
            If oPayment.PayType = "I" Then
                If IsIBANNumber((oPayment.E_Account), False) = Scripting.Tristate.TristateMixed Then
                    bEnoughBankInfoWritten = True
                End If
                If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                    bSWIFTCodeWritten = True
                    bEnoughBankInfoWritten = True
                End If
                If Not EmptyString((oPayment.BANK_BranchNo)) Then
                    If bSWIFTCodeWritten Then
                        sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                    Else
                        sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                    End If
                    Select Case oPayment.BANK_BranchType

                        Case vbBabel.BabelFiles.BankBranchType.Fedwire
                            sLine = sLine & sTempText & "25:114"
                            bBranchNoWritten = True
                        Case vbBabel.BabelFiles.BankBranchType.Chips
                            sLine = sLine & sTempText & "43:114"
                            bBranchNoWritten = True
                            'Chaps is not implemented in BB yet
                            '            Case BankBranchType.Chaps
                            '                sLine = sLine & sTempText & "44:114"
                            '                bBranchNoWritten = True
                            'Some italian stuff that I have no idea about
                            '            Case BankBranchType.IT
                            '                sLine = sLine & sTempText & "25:119"
                            '                bBranchNoWritten = True
                        Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                            sLine = sLine & sTempText & "25:131"
                            bBranchNoWritten = True
                        Case vbBabel.BabelFiles.BankBranchType.SortCode
                            sLine = sLine & sTempText & "25:133"
                            bBranchNoWritten = True
                        Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                            sLine = sLine & sTempText & "25:137"
                            bBranchNoWritten = True
                            'Canadian routing code is not yet implemented in BB
                            '            Case BankBranchType.CanadianRoutingCode
                            '                sLine = sLine & sTempText & "25:170"
                            '                bBranchNoWritten = True
                        Case Else
                            'Nothing to do, don't add sTempText
                    End Select
                End If
                If Not bEnoughBankInfoWritten Then
                    If Not EmptyString((oPayment.BANK_Name)) Then
                        sTempText = ""
                        If bBranchNoWritten Then
                            sTempText = ":"
                        Else
                            If bSWIFTCodeWritten Then
                                sTempText = "::::::"
                            Else
                                sTempText = "+::::::"
                            End If
                        End If
                        sBankNameAddress = CheckForSeperators((oPayment.BANK_Name)) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr1))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr2))) & " " & Trim(CheckForSeperators((oPayment.BANK_Adr3)))
                        If Len(sBankNameAddress) > 70 Then
                            If Len(sBankNameAddress) > 140 Then
                                sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                            Else
                                sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                            End If
                            sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                        Else
                            sBankNameAddress = Trim(sBankNameAddress)
                        End If
                        sLine = sLine & sTempText & sBankNameAddress
                        If Len(sBankNameAddress2) > 0 Then
                            sLine = sLine & ":" & sBankNameAddress2
                        End If
                        If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                            If bEnoughBankInfoWritten Then
                                'OK, we don't need the countrycode
                            Else
                                ErrorWritingEDI(1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If
                        Else
                            sLine = sLine & "+" & oPayment.BANK_CountryCode
                        End If
                        bEnoughBankInfoWritten = True
                    End If
                End If
                If Not bEnoughBankInfoWritten Then
                    ErrorWritingEDI(1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer.")
                Else
                    bWriteSegment = True
                End If
            Else
                If IsIBANNumber((oPayment.E_Account), False) = Scripting.Tristate.TristateMixed Then
                    bEnoughBankInfoWritten = True
                End If
                If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                    sLine = sLine & "+" & oPayment.BANK_SWIFTCode & ":25:5"
                    bSWIFTCodeWritten = True
                    bEnoughBankInfoWritten = True
                End If
                If sCountryCode = "DE" Or sCountryCode = "GB" Or sCountryCode = "US" Or sCountryCode = "AT" Then
                    If Not EmptyString((oPayment.BANK_BranchNo)) Then
                        'What do we do about the swedish branch clearing number
                        If bSWIFTCodeWritten Then
                            sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                        Else
                            sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNo)) & ":"
                        End If
                    End If

                    Select Case sCountryCode 'This is payers countrycode
                        'Implemented for SE, DK, DE and US
                        Case "DE"
                            sLine = sLine & sTempText & "25:131"
                            bBranchNoWritten = True
                        Case "GB"
                            sLine = sLine & sTempText & "25:133"
                            bBranchNoWritten = True
                        Case "US"
                            Select Case oPayment.BANK_BranchType

                                Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                    sLine = sLine & sTempText & "25:114"
                                    bBranchNoWritten = True
                                Case vbBabel.BabelFiles.BankBranchType.Chips
                                    sLine = sLine & sTempText & "43:114"
                                    bBranchNoWritten = True
                                    'Chaps is not implemented in BB yet
                                    '            Case BankBranchType.Chaps
                                    '                sLine = sLine & sTempText & "44:114"
                                    '                bBranchNoWritten = True
                            End Select
                        Case "AT"
                            sLine = sLine & sTempText & "25:137"
                            bBranchNoWritten = True
                        Case Else
                            'Do nothing
                    End Select
                Else
                    Select Case sCountryCode 'This is payers countrycode
                        Case "SE"
                            'Assumes bankgiro number and not bankaccount number
                            '************* Have to do some changes here!!!!!!!!!!!!!!!!!!!!!!!!
                            If bSWIFTCodeWritten Then
                                sLine = sLine & ":9900:157:118"
                            Else
                                sLine = sLine & "+:::9900:157:118"
                            End If
                            bBranchNoWritten = True
                            'Some italian stuff that I have no idea about
                            '            Case "IT"
                            '                If bSWIFTCodeWritten Then
                            '                    sLine = sLine & ":?????????:157:118"
                            '                Else
                            '                    sLine = sLine & "+:::?????????:157:118"
                            '                End If
                            '                bBranchNoWritten = True
                        Case "NO"
                            'No Code List And Shit
                            'If bSWIFTCodeWritten Then
                            '    sLine = sLine & "::157:124"
                            'Else
                            '    sLine = sLine & "+::::157:124"
                            'End If
                            bBranchNoWritten = True
                        Case "FI"
                            If bSWIFTCodeWritten Then
                                sLine = sLine & "::157:125"
                            Else
                                sLine = sLine & "+::::157:125"
                            End If
                            bBranchNoWritten = True
                        Case "DK"
                            If bSWIFTCodeWritten Then
                                sLine = sLine & "::157:130"
                            Else
                                sLine = sLine & "+::::157:130"
                            End If
                            bBranchNoWritten = True

                    End Select
                End If
            End If

        ElseIf iTypeOfInformation = I_Info Then
            If bOutgoingPayment Then
                'Payer
                sLine = sLine & "OR+"
            Else
                'Receiver
                sLine = sLine & "BF+"
            End If
            'AccountNumber
            If Not EmptyString((oPayment.I_Account)) Then
                sLine = sLine & oPayment.I_Account
                bWriteSegment = True
            End If
            If Len(sCountryCode) > 0 And bWriteCountryCode Then
                sLine = sLine & "++" & sCountryCode
            End If
        ElseIf iTypeOfInformation = CorrBank Then
            If bOutgoingPayment Then
                'Payer
                sLine = sLine & "I1+"
            Else
                'Receiver
                sLine = sLine & "I1+" '????
            End If
            'AccountNumber not used for Corrbank
            If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Then
                sLine = sLine & "+" & oPayment.BANK_SWIFTCodeCorrBank & ":25:5"
                bSWIFTCodeWritten = True
                bEnoughBankInfoWritten = True
            End If
            If Not EmptyString((oPayment.BANK_BranchNoCorrBank)) Then
                If bSWIFTCodeWritten Then
                    sTempText = ":" & CheckForSeperators((oPayment.BANK_BranchNoCorrBank)) & ":"
                Else
                    sTempText = "+:::" & CheckForSeperators((oPayment.BANK_BranchNoCorrBank)) & ":"
                End If
                Select Case oPayment.BANK_BranchTypeCorrBank

                    Case vbBabel.BabelFiles.BankBranchType.Fedwire
                        sLine = sLine & sTempText & "25:114"
                        bBranchNoWritten = True
                    Case vbBabel.BabelFiles.BankBranchType.Chips
                        sLine = sLine & sTempText & "43:114"
                        bBranchNoWritten = True
                        'Chaps is not implemented in BB yet
                        '            Case BankBranchType.Chaps
                        '                sLine = sLine & sTempText & "44:114"
                        '                bBranchNoWritten = True
                        'Some italian stuff that I have no idea about
                        '            Case BankBranchType.IT
                        '                sLine = sLine & sTempText & "25:119"
                        '                bBranchNoWritten = True
                    Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                        sLine = sLine & sTempText & "25:131"
                        bBranchNoWritten = True
                    Case vbBabel.BabelFiles.BankBranchType.SortCode
                        sLine = sLine & sTempText & "25:133"
                        bBranchNoWritten = True
                    Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                        sLine = sLine & sTempText & "25:137"
                        bBranchNoWritten = True
                        'Canadian routing code is not yet implemented in BB
                        '            Case BankBranchType.CanadianRoutingCode
                        '                sLine = sLine & sTempText & "25:170"
                        '                bBranchNoWritten = True
                    Case Else
                        'Nothing to do, don't add sTempText
                End Select
            End If
            If Not bEnoughBankInfoWritten Then
                If Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
                    sTempText = ""
                    If bBranchNoWritten Then
                        sTempText = ":"
                    Else
                        If bSWIFTCodeWritten Then
                            sTempText = "::::::"
                        Else
                            sTempText = "+::::::"
                        End If
                    End If
                    sBankNameAddress = CheckForSeperators((oPayment.BANK_NameAddressCorrBank2)) & " " & Trim(CheckForSeperators((oPayment.BANK_NameAddressCorrBank3))) & " " & Trim(CheckForSeperators((oPayment.BANK_NameAddressCorrBank4)))
                    If Len(sBankNameAddress) > 70 Then
                        If Len(sBankNameAddress) > 140 Then
                            sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71, 70))
                        Else
                            sBankNameAddress2 = Trim(Mid(sBankNameAddress, 71))
                        End If
                        sBankNameAddress = Trim(Left(sBankNameAddress, 70))
                    Else
                        sBankNameAddress = Trim(sBankNameAddress)
                    End If
                    sLine = sLine & sTempText & sBankNameAddress
                    If Len(sBankNameAddress2) > 0 Then
                        sLine = sLine & ":" & sBankNameAddress2
                    End If
                    'Don't show error messages for corr banks yet!
                    If Len(oPayment.BANK_CountryCode) <> 2 Or Not bWriteCountryCode Then
                        If bEnoughBankInfoWritten Then
                            'OK, we don't need the countrycode
                        Else
                            'ErrorWritingEDI 1, "WriteFIISegment", "The banks countrycode is mandatory " & vbCrLf & "when no bankcodes are supplied." & vbCrLf & vbCrLf & "Contact Your dealer."
                        End If
                    Else
                        sLine = sLine & "+" & oPayment.BANK_CountryCode
                    End If
                    bEnoughBankInfoWritten = True
                End If
            End If
            If Not bEnoughBankInfoWritten Then
                'ErrorWritingEDI 1, "WriteFIISegment", "There is not stated enough bankinformation in this payment." & vbCrLf & vbCrLf & "Contact Your dealer."
                bWriteSegment = True
            Else
                bWriteSegment = True
            End If
        End If

        If bWriteSegment Then
            WriteSegment(sLine)
        Else
            If bMandatory Then
                Do While True
                    If Right(sLine, 1) = "+" Or Right(sLine, 1) = ":" Then
                        sLine = Mid(sLine, 1, Len(sLine) - 1)
                    Else
                        Exit Do
                    End If
                Loop
                WriteSegment(sLine)
            End If
        End If

    End Function

    Private Function WriteFTXSegment(ByVal sQualifier As String, ByVal sText As String, Optional ByVal sCode As String = vbNullString, Optional ByVal iTextLength As Integer = 70) As Boolean
        Dim sLine As String
        Dim lEleCounter As Long, lSegCounter As Long
        Dim lFTXCounter As Long

        lSegCounter = 0
        lEleCounter = 0
        lFTXCounter = 0

        '14.04.2009 - Added And Not EmptyString(sText) to the Do While because SEB doesn't allow empty elements in the FTX-segment.
        Do While Len(sText) > 0 And Not EmptyString(sText)
            If lEleCounter = 0 Then
                'lFTXCounter = lFTXCounter + 1
                sLine = "FTX+" & sQualifier & "++" & Trim$(sCode) & "+"
                If Len(sText) > iTextLength Then
                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                        sLine = sLine & RTrim$(CheckForSeperators(Left$(sText, iTextLength))) 'XNET - 22.11.2011 and several more places in the function
                    Else
                        sLine = sLine & CheckForSeperators(Left$(sText, iTextLength))
                    End If
                    sText = Mid$(sText, iTextLength + 1)
                Else
                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                        sLine = sLine & RTrim$(CheckForSeperators(sText))
                    Else
                        sLine = sLine & CheckForSeperators(sText)
                    End If
                    sText = vbNullString
                End If
                lEleCounter = 1
            Else
                If Len(sText) > iTextLength Then
                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                        sLine = sLine & ":" & RTrim$(CheckForSeperators(Left$(sText, iTextLength)))
                    Else
                        sLine = sLine & ":" & CheckForSeperators(Left$(sText, iTextLength))
                    End If
                    sText = Mid$(sText, iTextLength + 1)
                Else
                    If eReceivingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                        sLine = sLine & ":" & RTrim$(CheckForSeperators(sText))
                    Else
                        sLine = sLine & ":" & CheckForSeperators(sText)
                    End If
                    sText = vbNullString
                End If
                lEleCounter = lEleCounter + 1
                If lEleCounter = 5 Then
                    'If lFTXCounter = 5 Then
                    If lSegCounter = 4 Then
                        'No more space for freetext, write what You got
                        ' XNET 12.09.2012 commented next line, because we may have to many FTX-es, and last one is written at the end of this function
                        'WriteSegment sLine
                        Exit Do
                    Else
                        'No more elements, reset counter to create a new FTX
                        WriteSegment(sLine)
                        sLine = vbNullString
                        lSegCounter = lSegCounter + 1
                        lEleCounter = 0
                    End If
                Else
                    'nothing to do
                End If
            End If
        Loop

        ' XNET 12.09.2012 Do not write empty lines
        If Not EmptyString(sLine) Then
            WriteSegment(sLine)
        End If

    End Function

    Private Function WriteLINSegment() As Boolean

        lLINCounter = lLINCounter + 1
        WriteSegment("LIN+" & Trim(Str(lLINCounter)))

    End Function

    Private Function WriteMOASegment(ByRef sQualifier As String, ByRef nAmount As Double, ByRef sCurrency As String, Optional ByRef bTruncateDecimals As Boolean = False) As Boolean
        Dim sAmount As String
        Dim sLine As String

        sLine = "MOA+" & sQualifier & ":"

        sAmount = ConvertFromAmountToString(nAmount, "", sDecimalSep)
        If bTruncateDecimals Then
            If Right(sAmount, 3) = sDecimalSep & "00" Then
                sAmount = Left(sAmount, Len(sAmount) - 3)
            ElseIf Right(sAmount, 1) = "0" Then
                sAmount = Left(sAmount, Len(sAmount) - 1)
            End If
        End If

        sLine = sLine & sAmount

        If Len(sCurrency) > 0 Then
            sLine = sLine & ":" & sCurrency
        End If

        WriteSegment(sLine)

    End Function

    Private Function WriteNADSegment(ByRef bE_Information As Boolean, ByRef bStructured As Boolean, Optional ByRef sQualifier As String = "", Optional ByRef bJustTheName As Boolean = False, Optional ByRef bIncomingPayment As Boolean = False) As Boolean
        '01.02.2008 A lot of changes

        Dim sLine As String
        Dim sTempText As String
        Dim bWriteSegment As Boolean
        Dim sAdr1 As String
        Dim bAdr1Used As Boolean
        Dim sAdr2 As String
        Dim bAdr2Used As Boolean
        Dim sAdr3 As String
        Dim bAdr3Used As Boolean
        Dim sAdr4 As String
        Dim bAdr4Used As Boolean

        'New 02.01.2008
        'Move addressinformation from oPayment-properties to variables to prevent 'empty' addresses inbetween.
        bAdr1Used = False
        bAdr2Used = False
        bAdr3Used = False
        bAdr4Used = False

        If bE_Information Then
            If Not EmptyString((oPayment.E_Adr1)) Then
                sAdr1 = Trim(CheckForSeperators((oPayment.E_Adr1)))
                bAdr1Used = True
            End If
            If Not EmptyString((oPayment.E_Adr2)) Then
                If bAdr1Used Then
                    sAdr2 = Trim(CheckForSeperators((oPayment.E_Adr2)))
                    bAdr2Used = True
                Else
                    sAdr1 = Trim(CheckForSeperators((oPayment.E_Adr2)))
                    bAdr1Used = True
                End If
            End If
            If Not EmptyString((oPayment.E_Adr3)) Then
                If bAdr1Used Then
                    If bAdr2Used Then
                        sAdr3 = Trim(CheckForSeperators((oPayment.E_Adr3)))
                        bAdr3Used = True
                    Else
                        sAdr2 = Trim(CheckForSeperators((oPayment.E_Adr3)))
                        bAdr2Used = True
                    End If
                Else
                    sAdr1 = Trim(CheckForSeperators((oPayment.E_Adr3)))
                    bAdr1Used = True
                End If
            End If
            If Not bStructured Then
                sTempText = ""
                If Not EmptyString(oPayment.E_Zip & oPayment.E_City) Then
                    If EmptyString((oPayment.E_Zip)) Then
                        sTempText = Trim(CheckForSeperators((oPayment.E_City)))
                    Else
                        If EmptyString((oPayment.E_City)) Then
                            sTempText = Trim(CheckForSeperators((oPayment.E_Zip)))
                        Else
                            sTempText = Trim(CheckForSeperators((oPayment.E_Zip))) & " " & Trim(CheckForSeperators((oPayment.E_City)))
                        End If
                    End If
                    If bAdr1Used Then
                        If bAdr2Used Then
                            If bAdr3Used Then
                                sAdr4 = sTempText
                                bAdr4Used = True
                            Else
                                sAdr3 = sTempText
                                bAdr3Used = True
                            End If
                        Else
                            sAdr2 = sTempText
                            bAdr2Used = True
                        End If
                    Else
                        sAdr1 = sTempText
                        bAdr1Used = True
                    End If
                End If
            End If
        Else
            If Not EmptyString((oPayment.I_Adr1)) Then
                sAdr1 = Trim(CheckForSeperators((oPayment.I_Adr1)))
                bAdr1Used = True
            End If
            If Not EmptyString((oPayment.I_Adr2)) Then
                If bAdr1Used Then
                    sAdr2 = Trim(CheckForSeperators((oPayment.I_Adr2)))
                    bAdr2Used = True
                Else
                    sAdr1 = Trim(CheckForSeperators((oPayment.I_Adr2)))
                    bAdr1Used = True
                End If
            End If
            If Not EmptyString((oPayment.I_Adr3)) Then
                If bAdr1Used Then
                    If bAdr2Used Then
                        sAdr3 = Trim(CheckForSeperators((oPayment.I_Adr3)))
                        bAdr3Used = True
                    Else
                        sAdr2 = Trim(CheckForSeperators((oPayment.I_Adr3)))
                        bAdr2Used = True
                    End If
                Else
                    sAdr1 = Trim(CheckForSeperators((oPayment.I_Adr3)))
                    bAdr1Used = True
                End If
            End If
            If Not bStructured Then
                sTempText = ""
                If Not EmptyString(oPayment.I_Zip & oPayment.I_City) Then
                    If EmptyString((oPayment.I_Zip)) Then
                        sTempText = Trim(CheckForSeperators((oPayment.I_City)))
                    Else
                        If EmptyString((oPayment.I_City)) Then
                            sTempText = Trim(CheckForSeperators((oPayment.I_Zip)))
                        Else
                            sTempText = Trim(CheckForSeperators((oPayment.I_Zip))) & " " & Trim(CheckForSeperators((oPayment.I_City)))
                        End If
                    End If
                    If bAdr1Used Then
                        If bAdr2Used Then
                            If bAdr3Used Then
                                sAdr4 = sTempText
                                bAdr4Used = True
                            Else
                                sAdr3 = sTempText
                                bAdr3Used = True
                            End If
                        Else
                            sAdr2 = sTempText
                            bAdr2Used = True
                        End If
                    Else
                        sAdr1 = sTempText
                        bAdr1Used = True
                    End If
                End If
            End If
        End If
        sTempText = ""
        sLine = "NAD+"

        If bE_Information Then
            '    If oPayment.I_CountryCode = "DK" Then
            '        sLine = sLine & "5+++"
            '    Else
            If sQualifier = "" Then
                sLine = sLine & "BE++"
            Else
                sLine = sLine & sQualifier & "++"
            End If
            If bStructured Then
                sLine = sLine & "+"
                If Not EmptyString((oPayment.E_Name)) Then
                    bWriteSegment = True
                    sLine = sLine & Left(Trim(CheckForSeperators((oPayment.E_Name))), 35)
                End If
                If Not bJustTheName Then
                    sTempText = "+"
                    If bAdr1Used Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Left(sAdr1, 35)
                        sTempText = ":"
                    Else
                        sTempText = sTempText & ":"
                    End If
                    If bAdr2Used Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Left(sAdr2, 35)
                        sTempText = ":"
                    Else
                        sTempText = sTempText & ":"
                    End If
                    If bAdr3Used Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Left(sAdr3, 35)
                        sTempText = ":"
                    Else
                        sTempText = sTempText & ":"
                    End If
                    sTempText = Replace(sTempText, ":", "") 'Remove trailing ":"
                    If Not EmptyString((oPayment.E_City)) Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & "+" & Left(Trim(CheckForSeperators((oPayment.E_City))), 35)
                        sTempText = "++"
                    Else
                        sTempText = sTempText & "+++"
                    End If
                    If Not EmptyString((oPayment.E_Zip)) Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Trim(oPayment.E_Zip)
                        sTempText = "+"
                    Else
                        sTempText = sTempText & "+"
                    End If
                    If Len(oPayment.E_CountryCode) = 2 Then
                        sLine = sLine & sTempText & Trim(oPayment.E_CountryCode)
                    End If
                End If '        If Not bJustTheName Then
            Else
                If Not EmptyString((oPayment.E_Name)) Then
                    bWriteSegment = True
                    sLine = sLine & Left(Trim(CheckForSeperators((oPayment.E_Name))), 35)
                End If
                If Not bJustTheName Then
                    If bAdr1Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr1, 35)
                    End If
                    If bAdr2Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr2, 35)
                    End If
                    If bAdr3Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr3, 35)
                    End If
                    If bAdr4Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr4, 35)
                    End If
                    If Len(oPayment.E_CountryCode) = 2 Then
                        sLine = sLine & "+++++" & oPayment.E_CountryCode
                    Else
                        If oPayment.PayType = "I" Then
                            If Not bIncomingPayment Then
                                'If it is an incoming payment then country ain't mandatory
                                ErrorWritingEDI(1, "WriteNADSegment", "The beneficiaries country code is mandatory for cross-border payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If
                        End If
                    End If
                End If '        If Not bJustTheName Then
            End If
        Else
            If sQualifier = "" Then
                sLine = sLine & "OY++"
            Else
                sLine = sLine & sQualifier & "++"
            End If
            If bStructured Then
                sLine = sLine & "+"
                If Not EmptyString((oPayment.I_Name)) Then
                    bWriteSegment = True
                    sLine = sLine & Left(Trim(CheckForSeperators((oPayment.I_Name))), 35)
                End If
                If Not bJustTheName Then
                    sTempText = "+"
                    If bAdr1Used Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Left(sAdr1, 35)
                        sTempText = ":"
                    Else
                        sTempText = sTempText & ":"
                    End If
                    If bAdr2Used Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Left(sAdr2, 35)
                        sTempText = ":"
                    Else
                        sTempText = sTempText & ":"
                    End If
                    If bAdr3Used Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Left(sAdr3, 35)
                        sTempText = ":"
                    Else
                        sTempText = sTempText & ":"
                    End If
                    sTempText = Replace(sTempText, ":", "") 'Remove trailing ":"
                    If Not EmptyString((oPayment.E_City)) Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & "+" & Left(Trim(CheckForSeperators((oPayment.I_City))), 35)
                        sTempText = "++"
                    Else
                        sTempText = sTempText & "+++"
                    End If
                    If Not EmptyString((oPayment.I_Zip)) Then
                        bWriteSegment = True
                        sLine = sLine & sTempText & Trim(oPayment.I_Zip)
                        sTempText = "+"
                    Else
                        sTempText = sTempText & "+"
                    End If
                    If Len(oPayment.I_CountryCode) = 2 Then
                        sLine = sLine & sTempText & Trim(oPayment.I_CountryCode)
                    End If
                    'If we only write the name, we have to remove ++ at the end
                    If Right(sLine, 2) = "++" Then
                        sLine = Left(sLine, Len(sLine) - 2)
                    End If
                End If '        If Not bJustTheName Then
            Else
                If Not EmptyString((oPayment.I_Name)) Then
                    bWriteSegment = True
                    sLine = sLine & Left(Trim(CheckForSeperators((oPayment.I_Name))), 35)
                End If
                If Not bJustTheName Then
                    If bAdr1Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr1, 35)
                    End If
                    If bAdr2Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr2, 35)
                    End If
                    If bAdr3Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr3, 35)
                    End If
                    If bAdr4Used Then
                        bWriteSegment = True
                        sLine = sLine & ":" & Left(sAdr4, 35)
                    End If
                    If Len(oPayment.I_CountryCode) = 2 Then
                        sLine = sLine & "+++++" & oPayment.I_CountryCode
                    Else
                        '16.12.2019 - Removed this IF. CountryCode is not mandatory.
                        '   Created problems for Patentstyret
                        'If oPayment.PayType = "I" Then
                        'ErrorWritingEDI(1, "WriteNADSegment", "The beneficiaries country code is mandatory for cross-border payments." & vbCrLf & vbCrLf & "Contact Your dealer.")
                        'End If
                    End If
                End If '        If Not bJustTheName Then
            End If
        End If

        If bWriteSegment Then
            WriteSegment(sLine)
        End If

    End Function

    Private Function WriteRFFSegment(ByRef sQualifier As String, ByRef sReference As String, Optional ByRef iMaxLength As Short = 35) As Boolean
        Dim sLine As String

        sLine = "RFF+" & sQualifier

        If Not EmptyString(sReference) Then
            sLine = sLine & ":" & Left(RTrim(CheckForSeperators(sReference)), iMaxLength)
        End If

        If Len(sLine) > 4 Then
            WriteSegment(sLine)
        End If

    End Function

    Private Function WriteSEQSegment() As Boolean

        lSEQCounter = lSEQCounter + 1
        WriteSegment("SEQ++" & Trim(Str(lSEQCounter)))

    End Function

    Private Function WriteUNTSegment(Optional ByRef sUNHReference As String = "") As Boolean

        lSegmentCounter = lSegmentCounter + 1
        If EmptyString(sUNHReference) Then
            WriteSegment("UNT+" & Trim(Str(lSegmentCounter)) & "+" & Trim(Str(lUNHCounter)))
        Else
            WriteSegment("UNT+" & Trim(Str(lSegmentCounter)) & "+" & sUNHReference)
        End If

    End Function

    Private Function WriteUNZSegment(ByRef sUNBReference As String) As Boolean

        WriteSegment("UNZ+" & Trim(Str(lUNHCounter)) & "+" & sUNBReference)

    End Function

    Friend Function ErrorWritingEDI(ByRef lNumber As Integer, ByRef sSource As String, ByRef sMessage As String) As Boolean
        Dim iResponse As Short

        'New 06.08.2009
        If bRaiseValidationError Then
            ' added 21.10.2008
            ' Show which payment causes problems
            If Not oPayment Is Nothing Then
                If bJustWarning Then
                    sMessage = oPayment.E_Name & " - " & oPayment.DATE_Payment & " - " & oPayment.MON_InvoiceCurrency & " " & VB6.Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf & sMessage
                End If
            End If

            If bJustWarning Then
                ' 21.10.2008 In the errormessage there is no question - add this
                'sMessage = sMessage & vbCrLf & "Fortsett BabelBankkj�ringen ?"
                'iResponse = MsgBox(Trim$(Str(lNumber)) & ": " & sMessage, vbYesNo + vbQuestion, sSource)
                'If iResponse = vbNo Then
                'ErrorWritingEDI 1, "Program avbrytes", "BabelBank kj�ringen avsluttes." & vbCrLf & vbCrLf & "Det ble ikke dannet noen betalingsfil."
                Err.Raise(lNumber, sSource, sMessage)

                'End If
            Else
                ErrorWritingEDI(lNumber, sSource, sMessage)
            End If
        End If

    End Function
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()

        lBabelFileIndex = 0
        lBatchIndex = 0
        lPaymentIndex = 0
        lInvoiceIndex = 0
        lUNHCounter = 0

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    Private Function RemoveTrailingSeperators(ByRef sString As String) As String
        'This function does not take into consideration the use of escape characters
        Dim lCounter As Integer

        'sElementSep = ":"
        'sSegmentSep = "'"
        'sGroupSep = "+"
        'sDecimalSep = ","
        'sEscape = "?"


        For lCounter = Len(sString) To 0 Step -1
            If Right(sString, 1) = sGroupSep Then
                sString = Left(sString, lCounter - 1)
            ElseIf Right(sString, 1) = sElementSep Then
                sString = Left(sString, lCounter - 1)
            ElseIf Right(sString, 1) = sSegmentSep Then
                sString = Left(sString, lCounter - 1)
            Else
                Exit For
            End If
        Next lCounter

        RemoveTrailingSeperators = sString

    End Function
    Private Function CheckForSeperators(ByRef sString As String) As String

        sString = Replace(sString, sEscape, sEscape & sEscape)
        sString = Replace(sString, sElementSep, sEscape & sElementSep)
        sString = Replace(sString, sGroupSep, sEscape & sGroupSep)
        sString = Replace(sString, sSegmentSep, sEscape & sSegmentSep)

        CheckForSeperators = sString

    End Function
    Private Function CreateDnBNOR_INPS_EDIFile(ByRef sCompanyNo As String, ByRef sFormat As String) As Boolean
        Dim bx As Boolean
        Dim sUNBReference As String
        Dim sIdentSender As String
        Dim sIdentReceiver As String
        'Dim aFreetextArray() As String
        Dim sStatebankText, sLocalFreetext, sStatebankCode As String
        Dim sLocalREF_Own As String
        Dim oFreeText As vbBabel.Freetext
        Dim lCounter, lArrayCounter As Integer
        Dim lCounter2 As Integer
        Dim lMaxLengthOfFreetext As Integer
        Dim bThisIsDNBNOROverforselInstruction As Boolean
        Dim bPaymentFromOtherBank As Boolean
        Dim bTheInformationIsStructured As Boolean
        Dim sOldAccountNo, sI_SWIFTAddr As String
        Dim sLocalPayCode As String
        Dim bWriteUNH, bUNBWritten, bUNHWritten As Boolean
        Dim bWriteUNTForEachBabelFile As Boolean
        Dim bThisIsKID As Boolean
        Dim bNewMessage, bNewLIN As Boolean
        Dim lPaymentCounter As Integer
        Dim sAdditionalNo, sEDIMessageNo As String
        Dim sBGMReference As String
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim sCurrencyToUse As String
        Dim sPaymentReference As String
        Dim nSequenceNo As Double
        Dim bSequenceNoFound As Boolean

        On Error GoTo ERR_CreateDnBNOR_INPS_EDIFile

        Select Case sFormat

            Case "PAYMUL"

                'bx = mrEDISettingsExport.InitiateSpecialSegments
                'mrEDISettingsExport.SetCurrentElement ("//HEADER/ATTRIBUTES")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                WriteSegment("UNA:+,? ")
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                'oGroupPayment = CreateObject ("vbBabel.GroupPayments")
                oGroupPayment = New vbBabel.GroupPayments
                oGroupPayment.GroupCrossBorderPayments = False
                oGroupPayment.SetGroupingParameters(True, True, True, True, False, True)

                'Because of the grouping we do, we have set the PayType before we
                ' start the writing of the file.

                sOldAccountNo = ""
                bSequenceNoFound = False
                For Each oBabelFile In oBabelFiles

                    If sSpecial = "DNBNORFINANS" And Not bSequenceNoFound Then
                        'Only for the first batch if we have one sequenceseries!
                        nSequenceNo = oBabelFile.VB_Profile.FileSetups(iFilesetupOut).SeqNoTotal
                        If nSequenceNo > 9999 Then
                            nSequenceNo = 0
                        End If
                        bSequenceNoFound = True
                    End If

                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                'bNewPayments = True Don't need a new payments when the debit account is changed
                                If oBabelFile.VB_ProfileInUse Then
                                    bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                End If
                                sOldAccountNo = Trim(oPayment.I_Account)
                            End If

                            If IsPaymentDomestic(oPayment, sI_SWIFTAddr, False, , True) Then
                                oPayment.PayType = "D"
                            Else
                                oPayment.PayType = "I"
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
                sOldAccountNo = ""

                For Each oBabelFile In oBabelFiles

                    If Not bUNBWritten Then
                        If EmptyString((oBabelFile.EDI_MessageNo)) Then
                            sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
                        Else
                            sUNBReference = oBabelFile.EDI_MessageNo
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentSender = Trim(sCompanyNo)
                        Else
                            '"No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer."
                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
                        End If
                        'QUESTION
                        WriteSegment("UNB+UNOC:3+" & sIdentSender & "+DNBANOKK+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "++++1")
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above
                    sBGMReference = CreateMessageReference(oBabelFile)

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN:INPS05")
                    'To create the unique message_ID use the total amount

                    WriteSegment("BGM+452+" & sBGMReference)
                    If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now), "102")
                    Else
                        WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                    End If
                    WriteSegment("FII+MR++DNBANOKK:25:5")
                    'QUESTION
                    'If EmptyString(oBabelFile.IDENT_Sender) Then
                    '    WriteSegment "NAD+MS+BabelBank"
                    'Else
                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                    'End If

                    For Each oBatch In oBabelFile.Batches
                        For lCounter2 = 1 To oBatch.Payments.Count
                            bNewLIN = True

                            oGroupPayment.Batch = oBatch
                            oGroupPayment.MarkIdenticalPayments()
                            'Group the payments inside a batch and create a UNH per group
                            lPaymentCounter = 0
                            For Each oPayment In oBatch.Payments

                                If oPayment.SpecialMark And Not oPayment.Exported Then
                                    lPaymentCounter = lPaymentCounter + 1
                                    If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                        'bNewPayments = True Don't need a new payments when the debit account is changed
                                        If oBabelFile.VB_ProfileInUse Then
                                            bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                        End If
                                        sOldAccountNo = Trim(oPayment.I_Account)
                                    End If

                                    'Find country for receiver if not stated
                                    If EmptyString((oPayment.I_CountryCode)) Then
                                        If Not EmptyString((oPayment.BANK_I_SWIFTCode)) Then
                                            oPayment.I_CountryCode = Mid(oPayment.BANK_I_SWIFTCode, 5, 2)
                                        Else
                                            oPayment.I_CountryCode = Mid(sI_SWIFTAddr, 5, 2)
                                            'If debit account and receiver is in the same country it must be
                                            '  a domestic payment
                                            'If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
                                            '    oPayment.PayType = "I"
                                            'Else
                                            '    oPayment.PayType = "D"
                                            'End If
                                        End If
                                    End If

                                    oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
                                    If Left(oPayment.BANK_I_SWIFTCode, 4) <> "DNBA" Then
                                        bPaymentFromOtherBank = True
                                    Else
                                        bPaymentFromOtherBank = False
                                    End If

                                    'lArrayCounter = 0

                                    'Not validated against valid chars for SEB
                                    If sSpecial = "DNBNORFINANS" Then
                                        RetrieveZipAndCityFromTheAddress(oPayment, False)
                                    End If
                                    oPayment.E_Name = CheckForValidCharacters((oPayment.E_Name), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr1 = CheckForValidCharacters((oPayment.E_Adr1), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr2 = CheckForValidCharacters((oPayment.E_Adr2), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr3 = CheckForValidCharacters((oPayment.E_Adr3), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_City = CheckForValidCharacters((oPayment.E_City), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Name = CheckForValidCharacters((oPayment.BANK_Name), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr1 = CheckForValidCharacters((oPayment.BANK_Adr1), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr2 = CheckForValidCharacters((oPayment.BANK_Adr2), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr3 = CheckForValidCharacters((oPayment.BANK_Adr3), True, True, True, True, " /-?:().,'+")
                                    oPayment.I_Name = CheckForValidCharacters((oPayment.I_Name), True, True, True, True, " /-?:().,'+")


                                    If bPaymentFromOtherBank Then
                                        lMaxLengthOfFreetext = 140
                                    Else
                                        Select Case oPayment.I_CountryCode

                                            'Danish account transfer: 41�35 characters.
                                            'Danish transfer via PBS: 41�35 characters.
                                            'Danish and foreign cheque: 14�35 (7�70) characters.
                                            'Swedish payments: 30�35 characters.
                                            'Finnish payments: 12�35 characters.
                                            'Norwegian payments: 12�35 characters.

                                            '                        Case "DE"
                                            '                            If oPayment.PayType = "I" Then
                                            '                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            '                                lMaxLengthOfFreetext = 140
                                            '                            Else
                                            '                                lMaxLengthOfFreetext = 162
                                            '                            End If
                                            Case "DK"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                                    lMaxLengthOfFreetext = 1435
                                                ElseIf oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1435
                                                End If
                                            Case "FI"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "NO"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "SE"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1050
                                                End If
                                                '                        Case "US"
                                                '                            If oPayment.PayType = "I" Then
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            Else
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            End If

                                            Case Else
                                                bThisIsDNBNOROverforselInstruction = True
                                                lMaxLengthOfFreetext = 140
                                        End Select
                                    End If

                                    If lMaxLengthOfFreetext = 0 Then
                                        lMaxLengthOfFreetext = 140
                                    End If

                                    'Create a new LIN?
                                    If bNewLIN Then
                                        bNewLIN = False
                                        lSEQCounter = 0
                                        '4-LIN
                                        WriteLINSegment()
                                        '4-DTM
                                        '25.11.2008 - removed the IF
                                        'If bPaymentFromOtherBank Then

                                        'Else
                                        Select Case oPayment.I_CountryCode
                                            Case Else
                                                If oPayment.DATE_Payment < VB6.Format(Now, "YYYYMMDD") Then
                                                    WriteDTMSegment("203", VB6.Format(Now, "YYYYMMDD"), "102")
                                                Else
                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                End If
                                        End Select
                                        'End If
                                        '4-RFF
                                        If Len(oBatch.REF_Own) > 0 Then
                                            '20.02.2008 - This is done for Gjensidige to do the reference at the LIN-level
                                            ' unique.
                                            'oBatch.REF_Own = oBatch.REF_Own & "-" & Trim$(Str(lPaymentCounter))
                                            WriteRFFSegment("AEK", CheckForValidCharacters(Trim(oBatch.REF_Own) & "-" & Trim(Str(lPaymentCounter)), True, True, True, True, " /-?:().,'+"))
                                        Else
                                            WriteRFFSegment("AEK", sUNBReference & PadLeft(Trim(Str(lUNHCounter)), 4, "0") & PadLeft(Trim(Str(lLINCounter)), 8, "0"), 35)
                                        End If
                                        '4-BUS
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            WriteSegment("BUS++TRF")
                                        Else
                                            WriteBUSSegment((oPayment.PayType))
                                        End If
                                        '4-FCA
                                        'If oPayment.PayType = "I" Then
                                        WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                        'End If
                                        '5-MOA
                                        If Len(Trim(oPayment.MON_TransferCurrency)) <> 3 Then
                                            If EmptyString((oPayment.MON_TransferCurrency)) Then
                                                oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency
                                                sCurrencyToUse = Trim(oPayment.MON_TransferCurrency)
                                            Else
                                                '"Wrong transfercurrency stated on the payment." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & vbCrLf & "Currency: " & Trim(oPayment.MON_TransferCurrency) & vbCrLf & vbCrLf & "Contact Your dealer.")
                                                Err.Raise(15130, "CreateEDIFile", LRS(15130) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        Else
                                            sCurrencyToUse = Trim(oPayment.MON_TransferCurrency)
                                        End If

                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                            If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                                WriteMOASegment("9", (oPayment.MON_InvoiceAmount), sCurrencyToUse)
                                            Else
                                                WriteMOASegment("9", oGroupPayment.MON_InvoiceAmount, sCurrencyToUse)
                                            End If
                                            'WriteMOASegment "9", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency
                                        Else
                                            WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
                                            '5-CUX AND 5-RFF
                                            If oPayment.ERA_ExchRateAgreed = 0 Then
                                                If oPayment.FRW_ForwardContractRate = 0 Then
                                                    'No rates
                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                                    'Do not use 5-RFF
                                                Else
                                                    '"The use of 'Forward contract rate' is not yet implemented in this format." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & vbCrLf & vbCrLf & "Contact Your dealer."
                                                    Err.Raise(15131, "CreateEDIFile", LRS(15131) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                                    'Code to be used will be something like this
                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.FRW_ForwardContractRate), , ","))
                                                    If Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                                        WriteRFFSegment("ACX", Trim(oPayment.FRW_ForwardContractNo))
                                                    Else
                                                        '"No 'Forward contract number' is stated when a" & vbCrLf & "'Forward exchange rate' is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
                                                        Err.Raise(15132, "CreateEDIFile", LRS(15132) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                                    End If
                                                End If
                                            Else
                                                WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.ERA_ExchRateAgreed), , ","))
                                                If Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                                    WriteRFFSegment("FX", Trim(oPayment.ERA_DealMadeWith))
                                                Else
                                                    '"No 'Foreign exchange contract number' is stated when an" & vbCrLf & "agreed exchangerate is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
                                                    Err.Raise(15133, "CreateEDIFile", LRS(15133) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                                End If
                                            End If
                                        End If
                                        '6-FII
                                        'Changed 08.04.05 - Kjell
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            WriteSegment("FII+RB+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")
                                        Else
                                            WriteSegment("FII+OR+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")
                                        End If

                                    End If 'if bNewLIN

                                    'If oPayment.I_CountryCode = "NO" And Trim$(oPayment.PayCode) = "301" Then
                                    'XNET 07.01.2011 Added "FI"
                                    If (oPayment.I_CountryCode = "NO" Or oPayment.I_CountryCode = "FI") And Trim$(oPayment.PayCode) = "301" Then
                                        bThisIsKID = True
                                    Else
                                        bThisIsKID = False
                                        sLocalFreetext = ""
                                        sLocalREF_Own = ""
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier = CDbl("1") Then
                                                    sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                                End If
                                            Next oFreeText
                                        Next oInvoice
                                        If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                            'sLocalFreetext = "SEE SEPERATE LETTER."
                                        End If
                                        sLocalFreetext = RTrim(Left(sLocalFreetext, lMaxLengthOfFreetext))
                                    End If
                                    If oPayment.Invoices.Count > 0 Then
                                        sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                    End If

                                    '11-SEQ
                                    WriteSEQSegment()
                                    '11-MOA
                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                        'NOMORE
                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                        WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                    Else
                                        'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                        'NOMORE
                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                        WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
                                    End If
                                    '11-DTM
                                    '11-RFF
                                    'Special case for DnBNORFinans
                                    'oPayment.REF_Own is always DEBITOR UTBET., therefore the ownreference
                                    ' from the first Invoice is used

                                    If Len(Trim(oPayment.REF_Own)) > 0 And Not sSpecial = "DNBNORFINANS" Then
                                        WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                                    Else
                                        If oPayment.Invoices.Count > 0 Then
                                            If Len(Trim(oPayment.Invoices.Item(1).REF_Own)) > 0 Then
                                                If sSpecial = "DNBNORFINANS" Then
                                                    nSequenceNo = nSequenceNo + 1
                                                    If nSequenceNo > 9999 Then
                                                        nSequenceNo = 0
                                                    End If
                                                    sPaymentReference = ""
                                                    sPaymentReference = sPaymentReference & VB6.Format(Now, "YYMMDD") & PadLeft(Trim(Str(nSequenceNo)), 4, "0")
                                                    '26.09.2008 - Changed to create
                                                    'Old code
                                                    'WriteRFFSegment "CR", CheckForValidCharacters(Trim$(oPayment.Invoices.Item(1).REF_Own) & "-" &VB6.Format(Now(), "YYYYMMDDHHMMSS") & "-" & oPayment.MON_InvoiceAmount, True, True, True, True, " /-?:().,'+")
                                                    'New code
                                                    WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.Invoices.Item(1).REF_Own) & "-" & sPaymentReference & "-" & oPayment.MON_InvoiceAmount & "-" & Trim(oPayment.MON_InvoiceCurrency), True, True, True, True, " /-?:().,'+"))
                                                Else
                                                    WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.Invoices.Item(1).REF_Own), True, True, True, True, " /-?:().,'+"))
                                                End If
                                            Else
                                                '"No customer reference (own reference is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount
                                                Err.Raise(15134, "CreateEDIFile", LRS(15134) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        Else
                                            '"No customer reference (own reference is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount
                                            Err.Raise(15134, "CreateEDIFile", LRS(15134) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
                                        End If
                                    End If
                                    '11-PAI
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                                            WriteSegment("PAI+::20")
                                        ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
                                            WriteSegment("PAI+::23")
                                        Else
                                            WriteSegment("PAI+::26")
                                        End If
                                    ElseIf oPayment.Priority Then
                                        WriteSegment("PAI+::ZZ")
                                    ElseIf oPayment.PayType = "D" And oPayment.I_CountryCode = "DK" Then
                                        WriteSegment("PAI+::UUA")
                                    Else
                                        'For danish payments (and maybe others, we should be more specific)
                                        'F71     FIKORT 71 Paying slip (DK)
                                        'F73     FIKORT 73 Paying slip (DK)
                                        'F75     FIKORT 75 Paying slip (DK)
                                        'G01     GIRO 01 Paying slip (DK)
                                        'G04     GIRO 04 Paying slip (DK)
                                        'G15     GIRO 15 Paying slip (DK)
                                        'ULA     Account transfer with Immediate advice (DK)
                                        'Immediate advice, letter to beneficiary (SG16-FTX)
                                        'UUA     Account transfer with Long-form advice (DK)
                                        'NO PAI-segment
                                    End If
                                    '11 - FCA - Can't be included if FCA is stated at LIN-level
                                    'If oPayment.PayType = "I" Then
                                    '    WriteFCASegment oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad
                                    'End If
                                    '12 - FII
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        'Do not use the FI-segment
                                    Else
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            If oPayment.PayType = "I" Then
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , , vbBabel.BabelFiles.Bank.DnBNOR_INPS)
                                            Else
                                                If IsIBANNumber((oPayment.E_Account), True, False) Then
                                                    'XNET - 16.11.2010 - Changed the name of the function below
                                                    'XokNET - 12.12.2013 - Added next IF, do not convert from IBAN to BBAN in Germany
                                                    If oPayment.I_CountryCode = "DE" And IsSEPAPayment(oPayment, sI_SWIFTAddr) Then
                                                        oPayment.BANK_CountryCode = Left(oPayment.E_Account, 2)
                                                        'Keep the IBAN-number
                                                    Else
                                                        If ExtractInfoFromIBAN(oPayment, False) Then
                                                            'If ExtractAccountFromIBANN(oPayment, False) Then

                                                        End If
                                                    End If
                                                End If
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , , vbBabel.BabelFiles.Bank.DnBNOR_INPS)
                                            End If
                                        ElseIf oPayment.PayType = "I" Then
                                            WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , , vbBabel.BabelFiles.Bank.DnBNOR_INPS)
                                        Else
                                            If oPayment.I_CountryCode = "SE" Then
                                                If sSpecial = "DNBNORFINANS" Then
                                                    'Find the correct accounttype
                                                    If Not IsIBANNumber((oPayment.E_Account), False, False) Then
                                                        If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                                            Select Case oPayment.BANK_SWIFTCode
                                                                Case "BANKGIRO"
                                                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                                                Case "NDEASESS"
                                                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro
                                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                                                                    oPayment.E_Account = Mid(oPayment.E_Account, 5)
                                                                Case Else
                                                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                                                            End Select
                                                        Else
                                                            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                                        End If
                                                    Else
                                                        'IBAN number, this will create an BANSTA
                                                        'Set it to SE_Bankgiro
                                                        oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                                    End If
                                                End If
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True)
                                            ElseIf oPayment.I_CountryCode = "DK" Then
                                                'Convert from IBAN to BBANN
                                                If IsIBANNumber((oPayment.E_Account), True, False) Then
                                                    'XNET - 16.11.2010 - Changed the name of the function below
                                                    If ExtractInfoFromIBAN(oPayment, False) Then
                                                        'If ExtractAccountFromIBANN(oPayment, False) Then

                                                    End If
                                                End If
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True)
                                            Else
                                                'If oPayment.I_CountryCode = "NO" Then
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True)
                                                'End If
                                            End If
                                        End If
                                        If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Or Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
                                            WriteFIISegment(CorrBank, True, (oPayment.I_CountryCode), False, True)
                                        End If
                                    End If
                                    '13 - NAD
                                    bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                    WriteNADSegment(True, True)
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                                            WriteNADSegment(False, True, "RV")
                                        ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
                                            WriteNADSegment(True, True, "RV")
                                        Else
                                            WriteNADSegment(True, True, "RV")
                                        End If
                                    End If
                                    '13 - CTA
                                    'Not implemented
                                    '13 - COM
                                    'Not implemented
                                    '14 - INP
                                    '22.10.2008 - Removed the INP segment after phone from H�kon Belt in DnBNOR
                                    'It is of no use when stated like INP+3:4+1:AD, but it creates problems for DnBNOR
                                    '24.11.2008 - Added INP for Sweden after phone from H�kon Belt in DnBNOR
                                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "SE" Then
                                        WriteSegment("INP+3:4+1:AD")
                                    End If
                                    '15 - GIS
                                    If oPayment.I_CountryCode = "NO" And oPayment.PayType = "I" Then
                                        'Central Bank Reporting
                                        WriteSegment("GIS+10")
                                        sStatebankText = ""
                                        sStatebankCode = ""
                                        For Each oInvoice In oPayment.Invoices
                                            sStatebankText = oInvoice.STATEBANK_Text
                                            sStatebankCode = oInvoice.STATEBANK_Code
                                            If Not EmptyString(sStatebankText) Then
                                                Exit For
                                            End If
                                        Next oInvoice
                                        If EmptyString(sStatebankText) Then
                                            If EmptyString(sStatebankCode) Then
                                                WriteFTXSegment("REG", "Export/Import av varer", CStr(14))
                                            Else
                                                WriteFTXSegment("REG", "Export/Import av varer", sStatebankCode)
                                            End If
                                        Else
                                            If EmptyString(sStatebankCode) Then
                                                WriteFTXSegment("REG", sStatebankText, CStr(14))
                                            Else
                                                WriteFTXSegment("REG", sStatebankText, sStatebankCode)
                                            End If
                                        End If

                                    End If
                                    '15 - FTX
                                    'WriteFTXSegment "REG", CheckForValidCharacters(oInvoice.STATEBANK_Text + ???oInvoice.STATEBANK_Code
                                    '16-PRC
                                    If bThisIsKID Then
                                        For Each oInvoice In oPayment.Invoices
                                            WriteSegment("PRC+8")
                                            'WriteDOCSegment "YW3", Trim$(oInvoice.Unique_Id), 35
                                            'WriteMOASegment 12, oInvoice.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency
                                            ' XNET 07.01.2011, changes next 2 lines
                                            WriteDOCSegment("999", Trim$(oInvoice.Unique_Id), 35)
                                            WriteMOASegment(9, oInvoice.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency)
                                        Next oInvoice
                                        '23-GIS
                                        WriteSegment("GIS+37")
                                        '23-MOA
                                        WriteMOASegment("128", (oPayment.MON_InvoiceAmount), "")
                                    Else
                                        If Not EmptyString(sLocalFreetext) Then
                                            WriteSegment("PRC+11")
                                            '16-FTX
                                            'NOMORE
                                            'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                            WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
                                            '23-GIS
                                            WriteSegment("GIS+37")
                                        Else
                                            'No advice
                                        End If
                                    End If
                                    oPayment.Exported = True

                                    If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                        'Always new LIN per payment even if it is a domestic payment
                                        Exit For
                                    End If


                                End If 'If oPayment.SpecialMark And Not oPayment.Exported Then
                            Next oPayment
                            oGroupPayment.RemoveSpecialMark()
                            If oGroupPayment.AllPaymentsMarkedAsExported Then
                                Exit For
                            End If
                        Next lCounter2
                    Next oBatch
                    '04.09.2008 - Moved from beneath
                    WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)

                If sSpecial = "DNBNORFINANS" Then
                    For Each oBabelFile In oBabelFiles
                        oBabelFile.VB_Profile.FileSetups(iFilesetupOut).SeqNoTotal = nSequenceNo
                        oBabelFile.VB_Profile.Status = 2
                        oBabelFile.VB_Profile.FileSetups(iFilesetupOut).Status = 1
                        Exit For
                    Next oBabelFile
                End If

        End Select

        'To write the rest of the file
        WriteSegment("", True)

        If Not oGroupPayment Is Nothing Then
            oGroupPayment = Nothing
        End If

        On Error GoTo 0
        Exit Function

ERR_CreateDnBNOR_INPS_EDIFile:
        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
            oFs = Nothing
        End If
        If Not oGroupPayment Is Nothing Then
            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oGroupPayment = Nothing
        End If

        Err.Raise(Err.Number, "CreateDnBNOR_INPS_EDIFile", Err.Description)

    End Function

    Private Function CreateDnBNOR_EDIFile(ByRef sCompanyNo As String, ByRef sFormat As String) As Boolean
        Dim bx As Boolean
        Dim sUNBReference As String
        Dim sIdentSender As String
        Dim sIdentReceiver As String
        'Dim aFreetextArray() As String
        Dim sStatebankText, sLocalFreetext, sStatebankCode As String
        Dim sLocalREF_Own As String
        Dim oFreeText As vbBabel.Freetext
        Dim lCounter, lArrayCounter As Integer
        Dim lCounter2 As Integer
        Dim lMaxLengthOfFreetext As Integer
        Dim bThisIsDNBNOROverforselInstruction As Boolean
        Dim bPaymentFromOtherBank As Boolean
        Dim bTheInformationIsStructured As Boolean
        Dim sOldAccountNo, sI_SWIFTAddr As String
        Dim sLocalPayCode As String
        Dim bWriteUNH, bUNBWritten, bUNHWritten As Boolean
        Dim bLINWritten As Boolean
        Dim bWriteUNTForEachBabelFile As Boolean
        Dim bThisIsKID As Boolean
        Dim bNewMessage, bNewLIN As Boolean
        Dim lPaymentCounter As Integer
        Dim sAdditionalNo, sEDIMessageNo As String
        Dim sBGMReference As String
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim sCurrencyToUse As String
        Dim sPaymentReference As String
        Dim nSequenceNo As Double
        Dim bSequenceNoFound As Boolean
        Dim sUNBSegment As String
        Dim sUNHSegment As String
        Dim sUCISegment As String
        Dim sOriginalUNBReference As String
        Dim sTemp As String
        Dim iPos1 As Short
        Dim iPos2 As Short
        Dim bAllUNHRejected As Boolean
        Dim bExportBabel As Boolean
        Dim bExportBatch As Boolean
        Dim nMONBatchAmount As Double = 0 '23.03.2020 - Added for Patentstyret

        Dim sVismaOriginalUNBReference As String
        Dim sVismaOldOriginalUNBReference As String
        Dim sVismaOriginalUNHNumber As String
        Dim sVismaTempOriginalUNBReference As String
        Dim oVismaBabelFile As vbBabel.BabelFile
        Dim oVismaBatch As vbBabel.Batch
        Dim sVismaTotalNumberOfUNH As String
        Dim iVismaNumberOfUNH As String
        Dim bValidationOK As Boolean
        Dim bVismaNewUCI As Boolean
        Dim iVismaUNTBabelFileIndex As Short
        Dim iVismaUNTBatchIndex As Short

        On Error GoTo ERR_CreateDnBNOR_EDIFile

        sUNBReference = ""
        sIdentSender = ""
        sIdentReceiver = ""
        sOriginalUNBReference = ""

        sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
        sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
        sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
        sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
        sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
        sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

        'WriteSegment("UNA:+,? ")

        Select Case sFormat
            '11.03.2011 - XNET - Added next Case
            Case "PAYMUL"
                WriteSegment("UNA:+,? ")
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                oGroupPayment = New vbBabel.GroupPayments  '23.05.2017 CreateObject ("vbBabel.GroupPayments")
                oGroupPayment.GroupCrossBorderPayments = False
                oGroupPayment.SetGroupingParameters(True, True, True, True, False, True)

                'Because of the grouping we do, we have set the PayType before we
                ' start the writing of the file.

                sOldAccountNo = vbNullString
                bSequenceNoFound = False
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If sOldAccountNo <> Trim$(oPayment.I_Account) Then
                                'bNewPayments = True Don't need a new payments when the debit account is changed
                                If oBabelFile.VB_ProfileInUse Then
                                    bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                End If
                                sOldAccountNo = Trim$(oPayment.I_Account)
                            End If

                            If IsPaymentDomestic(oPayment, sI_SWIFTAddr, False, , True) Then
                                If oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                    'OK
                                Else
                                    oPayment.PayType = "D"
                                End If
                            Else
                                oPayment.PayType = "I"
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
                sOldAccountNo = vbNullString

                For Each oBabelFile In oBabelFiles

                    If Not bUNBWritten Then
                        If EmptyString(oBabelFile.EDI_MessageNo) Then
                            sUNBReference = "BAB" & Format(Now(), "MMddHHMMss")
                        Else
                            sUNBReference = oBabelFile.EDI_MessageNo
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentSender = Trim$(sCompanyNo)
                        Else
                            '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
                        End If
                        'QUESTION
                        WriteSegment("UNB+UNOC:3+" & sIdentSender & "+00810506482+" & Format(Now(), "yyMMdd") & ":" & Format(Now(), "HHMM") & "+" & sUNBReference & "++++1")
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above
                    sBGMReference = CreateMessageReference(oBabelFile)

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim$(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN")
                    'To create the unique message_ID use the total amount

                    WriteSegment("BGM+452+" & sBGMReference)
                    If EmptyString(oBabelFile.DATE_Production) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now()), "102")
                    Else
                        WriteDTMSegment("137", oBabelFile.DATE_Production, "102")
                    End If

                    'If EmptyString(oBabelFile.IDENT_Sender) Then
                    '    WriteSegment "NAD+MS+BabelBank"
                    'Else
                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                    'End If

                    For Each oBatch In oBabelFile.Batches
                        For lCounter2 = 1 To oBatch.Payments.Count
                            bNewLIN = True

                            oGroupPayment.Batch = oBatch
                            oGroupPayment.MarkIdenticalPayments()
                            'Group the payments inside a batch and create a UNH per group
                            lPaymentCounter = 0
                            For Each oPayment In oBatch.Payments

                                If oPayment.SpecialMark And Not oPayment.Exported Then
                                    lPaymentCounter = lPaymentCounter + 1
                                    If sOldAccountNo <> Trim$(oPayment.I_Account) Then
                                        'bNewPayments = True Don't need a new payments when the debit account is changed
                                        If oBabelFile.VB_ProfileInUse Then
                                            bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                        End If
                                        sOldAccountNo = Trim$(oPayment.I_Account)
                                    End If

                                    'Find country for receiver if not stated
                                    If EmptyString(oPayment.I_CountryCode) Then
                                        If Not EmptyString(oPayment.BANK_I_SWIFTCode) Then
                                            oPayment.I_CountryCode = Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)
                                        Else
                                            oPayment.I_CountryCode = Mid$(sI_SWIFTAddr, 5, 2)
                                            'If debit account and receiver is in the same country it must be
                                            '  a domestic payment
                                            'If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
                                            '    oPayment.PayType = "I"
                                            'Else
                                            '    oPayment.PayType = "D"
                                            'End If
                                        End If
                                    End If

                                    oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
                                    If Left$(oPayment.BANK_I_SWIFTCode, 4) <> "DNBA" Then
                                        '24.11.2008 - Added the possibility to pay from accounts outside DnBNOR
                                        bPaymentFromOtherBank = True
                                    Else
                                        bPaymentFromOtherBank = False
                                    End If

                                    'lArrayCounter = 0

                                    oPayment.E_Name = CheckForValidCharacters(oPayment.E_Name, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr1 = CheckForValidCharacters(oPayment.E_Adr1, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr2 = CheckForValidCharacters(oPayment.E_Adr2, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr3 = CheckForValidCharacters(oPayment.E_Adr3, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_City = CheckForValidCharacters(oPayment.E_City, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Name = CheckForValidCharacters(oPayment.BANK_Name, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr1 = CheckForValidCharacters(oPayment.BANK_Adr1, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr2 = CheckForValidCharacters(oPayment.BANK_Adr2, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr3 = CheckForValidCharacters(oPayment.BANK_Adr3, True, True, True, True, " /-?:().,'+")
                                    oPayment.I_Name = CheckForValidCharacters(oPayment.I_Name, True, True, True, True, " /-?:().,'+")

                                    'REMOVE SOME OF THIS??????????????????????????
                                    If bPaymentFromOtherBank Then
                                        lMaxLengthOfFreetext = 140
                                    Else
                                        Select Case oPayment.I_CountryCode

                                            'Danish account transfer: 41�35 characters.
                                            'Danish transfer via PBS: 41�35 characters.
                                            'Danish and foreign cheque: 14�35 (7�70) characters.
                                            'Swedish payments: 30�35 characters.
                                            'Finnish payments: 12�35 characters.
                                            'Norwegian payments: 12�35 characters.

                                            '                        Case "DE"
                                            '                            If oPayment.PayType = "I" Then
                                            '                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            '                                lMaxLengthOfFreetext = 140
                                            '                            Else
                                            '                                lMaxLengthOfFreetext = 162
                                            '                            End If
                                            Case "DK"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                                    lMaxLengthOfFreetext = 1435
                                                ElseIf oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1435
                                                End If
                                            Case "FI"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "NO"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "SE"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1050
                                                End If
                                                '                        Case "US"
                                                '                            If oPayment.PayType = "I" Then
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            Else
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            End If

                                            Case Else
                                                bThisIsDNBNOROverforselInstruction = True
                                                lMaxLengthOfFreetext = 140
                                        End Select
                                    End If

                                    If lMaxLengthOfFreetext = 0 Then
                                        lMaxLengthOfFreetext = 140
                                    End If
                                    'END 'REMOVE SOME OF THIS??????????????????????????

                                    'Create a new LIN?
                                    If bNewLIN Then
                                        bNewLIN = False
                                        lSEQCounter = 0
                                        '4-LIN
                                        WriteLINSegment()
                                        '4-DTM
                                        Select Case oPayment.I_CountryCode
                                            Case Else
                                                If oPayment.DATE_Payment < Format(Now(), "yyyyMMdd") Then
                                                    WriteDTMSegment("203", Format(Now(), "yyyyMMdd"), "102")
                                                Else
                                                    WriteDTMSegment("203", oPayment.DATE_Payment, "102")
                                                End If
                                        End Select
                                        '4-RFF
                                        If Len(oBatch.REF_Own) > 0 Then
                                            '20.02.2008 - This is done for Gjensidige to do the reference at the LIN-level
                                            ' unique.
                                            'oBatch.REF_Own = oBatch.REF_Own & "-" & Trim$(Str(lPaymentCounter))
                                            WriteRFFSegment("CR", CheckForValidCharacters(Trim$(oBatch.REF_Own) & "-" & Trim$(Str(lPaymentCounter)), True, True, True, True, " /-?:().,'+"))
                                        Else
                                            WriteRFFSegment("CR", sUNBReference & PadLeft(Trim$(Str(lUNHCounter)), 4, "0") & PadLeft$(Trim$(Str(lLINCounter)), 8, "0"), 35)
                                        End If
                                        '4-BUS
                                        'Maybe we should be more flexible/specific about the paycode, something like:
                                        'sTemp = ""
                                        'Select Case oPayment.PayCode
                                        'Case 999
                                        '    sTemp = 'CAS'
                                        'Case
                                        'End Select
                                        'WriteBUSSegment oPayment.PayType, sTemp
                                        If oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                            WriteSegment("BUS+1:SAL+DO")
                                        Else
                                            WriteBUSSegment(oPayment.PayType, "ZZZ") '- Mutually Defined
                                        End If
                                        '4-FCA
                                        'Not in use
                                        '5-MOA
                                        If Len(Trim$(oPayment.MON_TransferCurrency)) <> 3 Then
                                            If EmptyString(oPayment.MON_TransferCurrency) Then
                                                oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency
                                                sCurrencyToUse = Trim$(oPayment.MON_TransferCurrency)
                                            Else
                                                '"Wrong transfercurrency stated on the payment."
                                                Err.Raise(15130, "CreateEDIFile", LRS(15130) & _
                                                        vbCrLf & LRS(35003, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & Trim$(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        Else
                                            sCurrencyToUse = Trim$(oPayment.MON_TransferCurrency)
                                        End If

                                        If oPayment.PayType = "D" Or oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                            WriteMOASegment("347", oGroupPayment.MON_InvoiceAmount, "")
                                        ElseIf oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                            WriteMOASegment("347", oGroupPayment.MON_InvoiceAmount, sCurrencyToUse)
                                        Else
                                            WriteMOASegment("57", oPayment.MON_InvoiceAmount, vbNullString)
                                            '5-CUX AND 5-RFF
                                            If oPayment.ERA_ExchRateAgreed = 0 Then
                                                If oPayment.FRW_ForwardContractRate = 0 Then
                                                    'No rates
                                                    WriteCUXSegment(oPayment.MON_InvoiceCurrency, oPayment.MON_TransferCurrency)
                                                    'Do not use 5-RFF
                                                Else
                                                    WriteCUXSegment(oPayment.MON_InvoiceCurrency, oPayment.MON_TransferCurrency, ConvertFromAmountToString(oPayment.FRW_ForwardContractRate, , ","))
                                                    If Not EmptyString(oPayment.FRW_ForwardContractNo) Then
                                                        WriteRFFSegment("FX", Trim$((oPayment.FRW_ForwardContractNo)))
                                                    Else
                                                        '"15132: No forward contract number is stated even though a forward exchange rate is stated."
                                                        Err.Raise(15132, "CreateEDIFile", LRS(15132) & _
                                                            vbCrLf & LRS(35003, oPayment.E_Name) & _
                                                            vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                            vbCrLf & LRS(35070, ConvertFromAmountToString(oPayment.FRW_ForwardContractRate, , ",")) & _
                                                            vbCrLf & vbCrLf & LRS(10016))
                                                    End If
                                                End If
                                            Else
                                                WriteCUXSegment(oPayment.MON_InvoiceCurrency, oPayment.MON_TransferCurrency, ConvertFromAmountToString(oPayment.ERA_ExchRateAgreed, , ","))
                                                If Not EmptyString(oPayment.ERA_DealMadeWith) Then
                                                    WriteRFFSegment("ACX", Trim$((oPayment.ERA_DealMadeWith)))
                                                Else
                                                    '"15133: No foreign exchange contract number is stated even though an agreed exchangerate is stated."
                                                    Err.Raise(15133, "CreateEDIFile", LRS(15133) & _
                                                            vbCrLf & LRS(35003, oPayment.E_Name) & _
                                                            vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                            vbCrLf & vbCrLf & LRS(10016))
                                                End If
                                            End If
                                        End If
                                        '6-FII
                                        'Changed 08.04.05 - Kjell
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            WriteSegment("FII+RB+" & Trim(oPayment.I_Account))
                                        Else
                                            WriteSegment("FII+OR+" & Trim(oPayment.I_Account))
                                        End If

                                    End If 'if bNewLIN

                                    'If oPayment.I_CountryCode = "NO" And Trim$(oPayment.PayCode) = "301" Then
                                    'XOKNET 07.01.2011 Added "FI"
                                    If oPayment.PayType = "D" And Trim$(oPayment.PayCode) = "301" Then
                                        bThisIsKID = True
                                    Else
                                        bThisIsKID = False
                                        sLocalFreetext = vbNullString
                                        sLocalREF_Own = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier = "1" Then
                                                    sLocalFreetext = sLocalFreetext & RTrim$(oFreeText.Text) & " "
                                                End If
                                            Next oFreeText
                                        Next oInvoice
                                        If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                            'sLocalFreetext = "SEE SEPERATE LETTER."
                                        End If
                                        sLocalFreetext = RTrim$(Left$(sLocalFreetext, lMaxLengthOfFreetext))
                                    End If
                                    If oPayment.Invoices.Count > 0 Then
                                        sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                    End If

                                    '11-SEQ
                                    WriteSEQSegment()
                                    '11-MOA
                                    WriteMOASegment("9", oPayment.MON_InvoiceAmount, "")
                                    '11-DTM
                                    '11-RFF - A bit unsure what AGN is and what AFO is. Not a proper description in the documentation
                                    If Len(Trim$(oPayment.REF_Own)) > 0 Then
                                        WriteRFFSegment("AGN", CheckForValidCharacters(Trim$(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                                    Else
                                        If oPayment.Invoices.Count > 0 Then
                                            If Len(Trim$(oPayment.Invoices.Item(1).REF_Own)) > 0 Then
                                                WriteRFFSegment("AGN", CheckForValidCharacters(Trim$(oPayment.Invoices.Item(1).REF_Own), True, True, True, True, " /-?:().,'+"))
                                            Else
                                                '"15134: No customer reference (own reference) is stated."
                                                Err.Raise(15134, "CreateEDIFile", LRS(15134) & _
                                                            vbCrLf & LRS(35003, oPayment.E_Name) & _
                                                            vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                            vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        Else
                                            '"15134: No customer reference (own reference) is stated."
                                            Err.Raise(15134, "CreateEDIFile", LRS(15134) & _
                                                        vbCrLf & LRS(35003, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                        End If
                                    End If

                                    'If Not EmptyString(oPayment.Text_E_Statement) Then
                                    '    WriteRFFSegment "AFO", CheckForValidCharacters(Trim$(oPayment.Text_E_Statement), True, True, True, True, " /-?:().,'+")
                                    'End If

                                    '11-PAI
                                    If oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                        If oPayment.Cheque > BabelFiles.ChequeType.noCheque Or oPayment.PayCode = "160" Then '160 = Utbetalingsanvisning
                                            WriteSegment("PAI+::21")
                                        End If
                                    Else
                                        If oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                            If oPayment.Cheque = BabelFiles.ChequeType.SendToPayer Then
                                                WriteSegment("PAI+::23")
                                            ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToReceiver Then
                                                If oPayment.PayType = "D" Then
                                                    WriteSegment("PAI+::21")
                                                Else
                                                    WriteSegment("PAI+::26")
                                                End If
                                            Else
                                                WriteSegment("PAI+::26")
                                            End If
                                        ElseIf oPayment.Priority Then
                                            WriteSegment("PAI+::ZZ")
                                        End If
                                    End If

                                    '11 - FCA
                                    If oPayment.PayType = "I" Then
                                        WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True)
                                    End If
                                    '12 - FII
                                    If oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                        'Do not use the FI-segment
                                    Else
                                        If oPayment.PayType = "I" Then
                                            WriteFIISegment(E_Info, True, oPayment.I_CountryCode, False, True)
                                        Else
                                            WriteFIISegment(E_Info, True, oPayment.I_CountryCode, False, True)
                                        End If
                                        If oPayment.PayType = "I" Then
                                            If Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Or Not EmptyString(oPayment.BANK_NameAddressCorrBank1) Then
                                                WriteFIISegment(CorrBank, True, oPayment.I_CountryCode, False, True)
                                            End If
                                        End If
                                    End If
                                    '12 - CTA
                                    'Not implemented
                                    '12 - COM
                                    'Not implemented
                                    '13 - NAD
                                    bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                    WriteNADSegment(True, True)
                                    '13 - CTA
                                    'Not implemented
                                    '13 - COM
                                    'Not implemented
                                    '14 - INP
                                    If Not EmptyString(oPayment.Text_E_Statement) Then
                                        WriteSegment("INP+BF:BE+1:SI")
                                        WriteFTXSegment("AAG", CheckForValidCharacters(Trim$(oPayment.Text_E_Statement), True, True, True, True, " /-?:().,'+"))
                                    End If
                                    '15 - GIS
                                    If oPayment.PayType = "I" Then
                                        'Central Bank Reporting
                                        WriteSegment("GIS+10")
                                        sStatebankText = vbNullString
                                        sStatebankCode = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            sStatebankText = oInvoice.STATEBANK_Text
                                            sStatebankCode = oInvoice.STATEBANK_Code
                                            If Not EmptyString(sStatebankText) Then
                                                Exit For
                                            End If
                                        Next oInvoice
                                        If EmptyString(sStatebankText) Then
                                            If EmptyString(sStatebankCode) Then
                                                WriteFTXSegment("REG", "Export/Import av varer", 14)
                                            Else
                                                WriteFTXSegment("REG", "Export/Import av varer", sStatebankCode)
                                            End If
                                        Else
                                            If EmptyString(sStatebankCode) Then
                                                WriteFTXSegment("REG", sStatebankText, 14)
                                            Else
                                                WriteFTXSegment("REG", sStatebankText, sStatebankCode)
                                            End If
                                        End If

                                    End If
                                    '15 - FTX
                                    'See above
                                    '16-PRC
                                    'Not implemented structured payments where invoiceno is stated
                                    If oPayment.PayType <> "S" And oPayment.PayType <> "M" Then
                                        If bThisIsKID Then
                                            WriteSegment("PRC+8")
                                            For Each oInvoice In oPayment.Invoices
                                                'WriteDOCSegment "YW3", Trim$(oInvoice.Unique_Id), 35
                                                'WriteMOASegment 12, oInvoice.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency
                                                ' XOKNET 07.01.2011, changes next 2 lines
                                                WriteDOCSegment("999", Trim$(oInvoice.Unique_Id), 35)
                                                WriteMOASegment(9, oInvoice.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency)
                                            Next oInvoice
                                            '23-GIS
                                            WriteSegment("GIS+37")
                                            '23-MOA
                                            WriteMOASegment("128", oPayment.MON_InvoiceAmount, vbNullString)
                                        Else
                                            If Not EmptyString(sLocalFreetext) Then
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                'No advice
                                            End If
                                        End If
                                    End If
                                    oPayment.Exported = True

                                    If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                        'Always new LIN per payment even if it is a domestic payment
                                        Exit For
                                    End If

                                End If 'If oPayment.SpecialMark And Not oPayment.Exported Then
                            Next oPayment
                            oGroupPayment.RemoveSpecialMark()
                            If oGroupPayment.AllPaymentsMarkedAsExported Then
                                Exit For
                            End If
                        Next lCounter2
                    Next oBatch
                    '04.09.2008 - Moved from beneath
                    WriteSegment("CNT+LI:" & Trim$(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)
                'XNET - End

            Case "CREMUL"
                'The grouping in Batch (LIN) must be done correct before we enter this part of the code
                'All payments will be exported
                'bWrite80 = True
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0

                WriteSegment("UNA:+,? ")

                For Each oBabelFile In oBabelFiles

                    bExportBabel = False
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If Not oPayment.Exported Then
                                bExportBabel = True
                                Exit For
                            End If
                        Next oPayment
                        If bExportBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportBabel Then

                        If Not bUNBWritten Then
                            If EmptyString(oBabelFile.EDI_MessageNo) Then
                                sUNBReference = "BAB" & Format(Now(), "MMddHHMMss")
                            Else
                                sUNBReference = Right(oBabelFile.EDI_MessageNo, 14)   ' 23.10.2019 added Right(..,14)
                            End If
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            If Not EmptyString(sCompanyNo) Then
                                sIdentReceiver = Trim$(sCompanyNo)
                            ElseIf Not EmptyString(oBabelFile.IDENT_Receiver) Then
                                sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
                            Else
                                Err.Raise(1, "CreateEDIFile", "No receiver-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If
                            'QUESTION
                            WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & Format(Now(), "yyMMdd") & ":" & Format(Now(), "HHmm") & "+" & sUNBReference)
                            bUNBWritten = True
                        End If

                        lSegmentCounter = 0
                        lLINCounter = 0 '04.09.2008, moved from above
                        sBGMReference = CreateBGMReference(oBabelFile)

                        lUNHCounter = lUNHCounter + 1
                        WriteSegment("UNH+" & Trim$(Str(lUNHCounter)) & "+CREMUL:D:96A:UN")
                        'To create the unique message_ID use the total amount

                        WriteSegment("BGM+455+" & sBGMReference)
                        If EmptyString(oBabelFile.DATE_Production) Or oBabelFile.DATE_Production = "19900101" Then
                            WriteDTMSegment("137", DateToString(Now()), "102")
                        Else
                            WriteDTMSegment("137", oBabelFile.DATE_Production, "102")
                        End If

                        'WriteSegment "NAD+MR+" & sIdentReceiver

                        For Each oBatch In oBabelFile.Batches

                            bExportBatch = False
                            For Each oPayment In oBatch.Payments
                                If Not oPayment.Exported Then
                                    bExportBatch = True
                                    Exit For
                                End If
                            Next oPayment

                            If oBatch.Payments.Count > 0 And bExportBatch Then
                                'Write a new LIN
                                lSEQCounter = 0
                                '4-LIN
                                WriteLINSegment()
                                '4-DTM
                                WriteDTMSegment("209", oBatch.Payments.Item(1).DATE_Value, "102")
                                'WriteDTMSegment "202", oBatch.Payments.Item(1).DATE_Payment, "102" 'Normally not stated
                                '4-BUS
                                If oBatch.Payments.Item(1).PayType = "I" Then
                                    WriteSegment("BUS++IN")
                                Else
                                    WriteSegment("BUS++DO++" & bbGetPayCode(oBatch.Payments.Item(1).PayCode, "CREMUL") & ":25:124")
                                End If
                                '4-MOA
                                If sSpecial = "PATENTSTYRET_FEILINNBET" Then
                                    nMONBatchAmount = 0
                                    For Each oPayment In oBatch.Payments
                                        If Not oPayment.Exported Then
                                            nMONBatchAmount = nMONBatchAmount + oPayment.MON_InvoiceAmount
                                        End If
                                    Next oPayment
                                Else
                                    nMONBatchAmount = oBatch.MON_InvoiceAmount
                                End If
                                WriteMOASegment("60", nMONBatchAmount, oBatch.Payments.Item(1).MON_InvoiceCurrency)
                                '5-RFF og DTM
                                If oBatch.Payments.Item(1).PayType = "I" Then
                                    If EmptyString(oBatch.REF_Bank) Then
                                        WriteSegment("RFF+AII")
                                    Else
                                        WriteRFFSegment("AII", CheckForValidCharacters(oBatch.REF_Bank, True, True, True, True, " /-?:().,'+"))
                                        'WriteDTMSegment "171", oBatch.Payments.Item(1).DATE_Payment, "102"
                                    End If
                                Else
                                    If EmptyString(oBatch.REF_Bank) Then
                                        WriteSegment("RFF+ACK")
                                    Else
                                        WriteRFFSegment("ACK", CheckForValidCharacters(oBatch.REF_Bank, True, True, True, True, " /-?:().,'+"))
                                        'WriteDTMSegment "171", oBatch.Payments.Item(1).DATE_Payment, "102"
                                    End If
                                End If
                                '6-FII
                                'Changed 08.04.05 - Kjell
                                '20.01.2020 - Added next IF for Patentstyret
                                If sSpecial = "PATENTSTYRET_FEILINNBET" Then
                                    WriteSegment("FII+BF+99999999995")
                                Else
                                    WriteSegment("FII+BF+" & Trim(oBatch.Payments.Item(1).I_Account))
                                End If
                            End If

                            lPaymentCounter = 0
                            For Each oPayment In oBatch.Payments

                                If Not oPayment.Exported Then
                                    lPaymentCounter = lPaymentCounter + 1
                                    'If sOldAccountNo <> Trim$(oPayment.I_Account) Then
                                    '    If oBabelFile.VB_ProfileInUse Then
                                    '        bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                    '    End If
                                    '    sOldAccountNo = Trim$(oPayment.I_Account)
                                    'End If

                                    oPayment.E_Name = CheckForValidCharacters(oPayment.E_Name, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr1 = CheckForValidCharacters(oPayment.E_Adr1, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr2 = CheckForValidCharacters(oPayment.E_Adr2, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr3 = CheckForValidCharacters(oPayment.E_Adr3, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_City = CheckForValidCharacters(oPayment.E_City, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Name = CheckForValidCharacters(oPayment.BANK_Name, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr1 = CheckForValidCharacters(oPayment.BANK_Adr1, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr2 = CheckForValidCharacters(oPayment.BANK_Adr2, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr3 = CheckForValidCharacters(oPayment.BANK_Adr3, True, True, True, True, " /-?:().,'+")
                                    oPayment.I_Name = CheckForValidCharacters(oPayment.I_Name, True, True, True, True, " /-?:().,'+")

                                    If oPayment.PayType = "D" And Trim$(oPayment.PayCode) = "301" Then
                                        bThisIsKID = True
                                    Else
                                        bThisIsKID = False
                                    End If
                                    If oPayment.Invoices.Count > 0 Then
                                        sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                    End If

                                    '10-SEQ
                                    WriteSEQSegment()
                                    '10-DTM
                                    'Have no idea which qualifier to use, because they don't seem to state book- and valuedate
                                    If oPayment.PayType = "I" Then
                                        WriteDTMSegment("209", oPayment.DATE_Value, "102")
                                    Else
                                        WriteDTMSegment("193", oPayment.DATE_Payment, "102")
                                    End If
                                    '10-FII
                                    If EmptyString(oPayment.E_Account) Then
                                        WriteSegment("FII+OR")
                                    Else
                                        WriteSegment("FII+OR+" & Trim(oPayment.E_Account))
                                    End If
                                    '11-RFF
                                    If Not EmptyString(oPayment.REF_Bank1) Then
                                        WriteRFFSegment("ACK", oPayment.REF_Bank1)
                                        If Not EmptyString(oPayment.REF_Bank2) Then
                                            WriteRFFSegment("ACD", oPayment.REF_Bank2)
                                        End If
                                    Else
                                        If Not EmptyString(oPayment.REF_Bank2) Then
                                            WriteRFFSegment("ACD", oPayment.REF_Bank2)
                                        End If
                                    End If
                                    '13-MOA
                                    If oPayment.PayType = "I" Then
                                        WriteMOASegment("60", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency)
                                        If oPayment.MON_OriginallyPaidAmount = 0 Then
                                            WriteMOASegment("98", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency)
                                            WriteCUXSegment(oPayment.MON_InvoiceCurrency, oPayment.MON_InvoiceCurrency)
                                        Else
                                            WriteMOASegment("98", oPayment.MON_OriginallyPaidAmount, oPayment.MON_OriginallyPaidCurrency)
                                            WriteCUXSegment(oPayment.MON_OriginallyPaidCurrency, oPayment.MON_InvoiceCurrency) 'Because this is an incoming payment the stated currecncies will be the other way around
                                        End If
                                    Else
                                        WriteMOASegment("119", oPayment.MON_InvoiceAmount, "")
                                    End If
                                    '14-NAD
                                    'bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                    '16.12.2019 - Added the last parameter True to all calls to WriteNADSegment
                                    '   bIncomingPayments = True
                                    If Not EmptyString(oPayment.I_Name) Then
                                        If EmptyString(oPayment.I_Zip) Then
                                            WriteNADSegment(False, False, "BE", , True)
                                        Else
                                            WriteNADSegment(False, True, "BE", , True)
                                        End If
                                    End If
                                    If Not EmptyString(oPayment.E_Name) Then
                                        If EmptyString(oPayment.E_Zip) Then
                                            WriteNADSegment(True, False, "PL", , True)
                                        Else
                                            WriteNADSegment(True, True, "PL", , True)
                                        End If
                                    End If
                                    '15 - INP
                                    If Not EmptyString(oPayment.Text_E_Statement) Then
                                        WriteSegment("INP+BF+2:SI")
                                        '15 - FTX
                                        WriteFTXSegment("AAG", oPayment.Text_E_Statement)
                                    End If
                                    '17 - FCA
                                    If oPayment.PayType = "I" Then
                                        If oPayment.MON_ChargeMeAbroad Then
                                            If oPayment.MON_ChargeMeDomestic Then
                                                WriteSegment("FCA+13")
                                            Else
                                                'Not possible
                                            End If
                                        Else
                                            If oPayment.MON_ChargeMeDomestic Then
                                                WriteSegment("FCA+14")
                                            Else
                                                WriteSegment("FCA+15")
                                            End If
                                        End If
                                        If oPayment.MON_ChargesAmount <> 0 Then
                                            WriteSegment("MOA+131:" & ConvertFromAmountToString(oPayment.MON_ChargesAmount, vbNullString, sDecimalSep) & "::4")
                                        End If
                                    End If
                                    If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "629" Then
                                        '20 - PRC
                                        WriteSegment("PRC+8")
                                        If IsOCR(oPayment.PayCode) Then
                                            For Each oInvoice In oPayment.Invoices
                                                '21 - DOC
                                                '21 - MOA
                                                If oInvoice.MON_InvoiceAmount < 0 Then
                                                    WriteSegment("DOC+998+" & oInvoice.Unique_Id)
                                                Else
                                                    WriteSegment("DOC+999+" & oInvoice.Unique_Id)
                                                End If
                                                WriteMOASegment("12", Math.Abs(oInvoice.MON_InvoiceAmount), "")
                                            Next oInvoice
                                        Else
                                            'Structured
                                            For Each oInvoice In oPayment.Invoices
                                                '21 - DOC
                                                '21 - MOA
                                                If oInvoice.MON_InvoiceAmount < 0 Then
                                                    WriteSegment("DOC+381+" & oInvoice.InvoiceNo)
                                                Else
                                                    WriteSegment("DOC+380+" & oInvoice.InvoiceNo)
                                                End If
                                                WriteMOASegment("12", Math.Abs(oInvoice.MON_InvoiceAmount), "")
                                            Next oInvoice
                                        End If
                                        '27 - GIS
                                        WriteSegment("GIS+37")
                                    Else
                                        '20 - PRC
                                        WriteSegment("PRC+11")
                                        sLocalFreetext = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier = "1" Then
                                                    sLocalFreetext = sLocalFreetext & RTrim$(oFreeText.Text) & " "
                                                End If
                                            Next oFreeText
                                        Next oInvoice
                                        WriteFTXSegment("PMD", sLocalFreetext)

                                    End If
                                    oPayment.Exported = True
                                Else
                                    'err.Raise 1, "CreateEDIFile", "The payment is already exported." & _
                                    'vbCrLf & "Receiver: " & oPayment.E_Name & _
                                    'vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount
                                End If 'If Not oPayment.Exported Then
                            Next oPayment
                        Next oBatch
                        '04.09.2008 - Moved from beneath
                        WriteSegment("CNT+LI:" & Trim$(Str(lLINCounter)))
                        WriteUNTSegment()
                    End If 'If bExportBabel
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)
                'XokNET - End

            Case "DEBMUL"
                WriteSegment("UNA:+,? ")
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                sOldAccountNo = ""
                bSequenceNoFound = False

                For Each oBabelFile In oBabelFiles

                    sUNBSegment = oBabelFile.UNBSegmentet
                    sUNHSegment = oBabelFile.UNHSegmentet

                    If Not bUNBWritten Then
                        iPos1 = 1
                        iPos2 = 1
                        sTemp = ""
                        For lCounter = 1 To 5
                            iPos1 = iPos2
                            iPos2 = iPos2 + 1
                            iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
                        Next lCounter
                        sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                        sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
                        'If EmptyString(oBabelFile.EDI_MessageNo) Then
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentReceiver = Trim(sCompanyNo)
                        ElseIf sSpecial = "VISMA_FOKUS" Then
                            sIdentReceiver = "1438086569"
                        ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
                            sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
                        Else
                            '"15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
                        End If
                        'QUESTION
                        WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference) '& "++++1"
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+DEBMUL:D:96A:UN")
                    'To create the unique message_ID use the total amount

                    If Not EmptyString((oBabelFile.EDI_MessageNo)) Then
                        sBGMReference = oBabelFile.EDI_MessageNo
                    Else
                        sBGMReference = CreateMessageReference(oBabelFile)
                    End If
                    WriteSegment("BGM+470+" & sBGMReference)
                    If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now), "102")
                    Else
                        WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                    End If
                    'WriteSegment "NAD+MR+" & Organisasjonsnummer
                    'QUESTION
                    'If EmptyString(oBabelFile.IDENT_Sender) Then
                    '    WriteSegment "NAD+MS+BabelBank"
                    'Else
                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                    'End If

                    For Each oBatch In oBabelFile.Batches

                        bLINWritten = False
                        For Each oPayment In oBatch.Payments

                            If Not bLINWritten Then
                                '4-LIN
                                WriteLINSegment()
                                '4-DTM
                                If oPayment.DATE_Value = "19900101" Then
                                    'WriteDTMSegment "137", DateToString(Now()), "102"
                                Else
                                    WriteDTMSegment("209", (oPayment.DATE_Value), "102")
                                End If
                                If oPayment.DATE_Payment = "19900101" Then
                                    'WriteDTMSegment "137", DateToString(Now()), "102"
                                Else
                                    WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                End If
                                '4-BUS
                                If oPayment.PayType = "I" Then
                                    WriteSegment("BUS++IN")
                                Else
                                    WriteSegment("BUS++DO")
                                End If
                                '4-MOA
                                WriteMOASegment("60", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                '5-RFF
                                If Not EmptyString((oBatch.REF_Bank)) Then
                                    WriteRFFSegment("ACK", (oBatch.REF_Bank))
                                End If
                                If Not EmptyString((oBatch.REF_Own)) Then
                                    WriteRFFSegment("CR", (oBatch.REF_Own))
                                End If
                                '6-FII
                                If Not EmptyString((oPayment.I_Account)) Then
                                    WriteSegment("FII+OR+" & oPayment.I_Account)
                                End If
                                '7-FCA
                                '7-MOA
                                '(8-ALC)
                                bLINWritten = True
                            End If
                            '10-SEQ
                            WriteSEQSegment()
                            '10-DTM

                            '(10-BUS)

                            '(10-FII)
                            WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), , False)

                            If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Or Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
                                WriteFIISegment(CorrBank, True, (oPayment.I_CountryCode), , False)
                            End If

                            '11-RFF
                            If Not EmptyString((oPayment.REF_Own)) Then
                                WriteRFFSegment("AGN", Trim(oPayment.REF_Own))
                            End If
                            If Not EmptyString((oPayment.REF_Bank1)) Then
                                WriteRFFSegment("ACK", Trim(oPayment.REF_Bank1))
                            End If
                            If Not EmptyString((oPayment.REF_Bank2)) Then
                                WriteRFFSegment("ACD", Trim(oPayment.REF_Bank2))
                            End If

                            '('12-PAI)

                            '13-MOA
                            WriteMOASegment("60", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))

                            If Not EmptyString((oPayment.MON_TransferCurrency)) Then
                                WriteMOASegment("98", (oPayment.MON_TransferredAmount), (oPayment.MON_TransferCurrency))
                            End If

                            '13-CUX ikke implementert
                            If oPayment.MON_AccountExchRate > 0 Then
                                WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), CStr(oPayment.MON_AccountExchRate))
                            End If

                            '14-NAD
                            WriteNADSegment(True, True, "BE")

                            '17-FCA
                            '17-MOA
                            If oPayment.PayType = "I" Then
                                WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                If oPayment.MON_ChargesAmount > 0 Then
                                    WriteMOASegment("23", (oPayment.MON_ChargesAmount), (oPayment.MON_ChargesCurrency))
                                End If
                            End If

                            oPayment.Exported = True

                        Next oPayment

                    Next oBatch

                    '04.09.2008 - Moved from beneath
                    WriteSegment("CNT+LI:" & Trim(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)

            Case "BANSTA"
                WriteSegment("UNA:+,? ")
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                sOldAccountNo = ""
                bSequenceNoFound = False

                For Each oBabelFile In oBabelFiles

                    sUNBSegment = oBabelFile.UNBSegmentet
                    sUNHSegment = oBabelFile.UNHSegmentet

                    If Not bUNBWritten Then
                        iPos1 = 1
                        iPos2 = 1
                        sTemp = ""
                        For lCounter = 1 To 5
                            iPos1 = iPos2
                            iPos2 = iPos2 + 1
                            iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
                        Next lCounter
                        sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                        sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
                        'If EmptyString(oBabelFile.EDI_MessageNo) Then
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentReceiver = Trim(sCompanyNo)
                        ElseIf sSpecial = "VISMA_FOKUS" Then
                            sIdentReceiver = "1438086569"
                        ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
                            sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
                        Else
                            '"15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
                        End If
                        'QUESTION
                        WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference) '& "++++1"
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+BANSTA:D:96A:UN")
                    'To create the unique message_ID use the total amount

                    If Not EmptyString((oBabelFile.EDI_MessageNo)) Then
                        sBGMReference = oBabelFile.EDI_MessageNo
                    Else
                        sBGMReference = CreateMessageReference(oBabelFile)
                    End If
                    WriteSegment("BGM+77+" & sBGMReference)
                    If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now), "102")
                    Else
                        WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                    End If
                    WriteSegment("FII+MS++DNBANOKK:25:5")
                    'QUESTION
                    'If EmptyString(oBabelFile.IDENT_Sender) Then
                    '    WriteSegment "NAD+MS+BabelBank"
                    'Else
                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                    'End If

                    For Each oBatch In oBabelFile.Batches

                        For Each oPayment In oBatch.Payments
                            '4-LIN
                            WriteLINSegment()
                            '5-RFF
                            'UNB-reference from original message
                            If Not EmptyString((oPayment.REF_Bank1)) Then
                                WriteRFFSegment("DM", CheckForValidCharacters(Trim(oPayment.REF_Bank1), True, True, True, True, " /-?:().,'+"))
                            End If
                            'LIN-reference from original message
                            If Not EmptyString((oPayment.REF_Bank2)) Then
                                WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.REF_Bank2), True, True, True, True, " /-?:().,'+"))
                            End If
                            'SEQ-reference from original message
                            If Not EmptyString((oPayment.REF_Own)) Then
                                WriteRFFSegment("AGN", CheckForValidCharacters(Trim(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                            End If

                            '6-SEQ
                            WriteSEQSegment()
                            '6-GIS
                            'To get the correct code, pass the oPayment.Statuscode to a function
                            sTemp = oPayment.StatusCode
                            sTemp = VismaFokusConvertBANSTACode(sTemp)

                            WriteSegment("GIS+" & sTemp)
                            '6-FTX
                            If Not EmptyString((oPayment.StatusText)) Then
                                WriteFTXSegment("AAO", (oPayment.StatusText))
                            End If

                            oPayment.Exported = True

                        Next oPayment

                    Next oBatch

                    '04.09.2008 - Moved from beneath
                    WriteSegment("CNT+LI:" & Trim(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)

            Case "CONTRL"
                WriteSegment("UNA:+,? ")

                If sSpecial = "VISMA_FOKUS" Then
                    bNewMessage = True

                    lUNHCounter = 0

                    iVismaUNTBabelFileIndex = -1
                    iVismaUNTBatchIndex = -1

                    sOldAccountNo = ""
                    bSequenceNoFound = False

                    sVismaOldOriginalUNBReference = ""

                    For Each oBabelFile In oBabelFiles

                        sUNBSegment = oBabelFile.UNBSegmentet
                        sUNHSegment = oBabelFile.UNHSegmentet

                        For Each oBatch In oBabelFile.Batches

                            'sVismaOriginalUNBReference holds the reference of the actual batch
                            'sVismaOldOriginalUNBReference holds the reference of the last batch written
                            'sVismaTempOriginalUNBReference holds the reference of batches traversed through.
                            '  Theese references are used to check that the total numer of batches with an identical
                            '  reference imported (CONTRL), fits with the total number sent to the bank (PAYMUL).
                            '  If so this means that we have received CONTRL on all PAYMULS sent in one file.
                            sVismaOriginalUNBReference = xDelim((oBatch.UCIString), "+", 2)
                            sVismaOriginalUNBReference = Mid(sVismaOriginalUNBReference, 2) 'For Visma_Fokus remove added information to the UNB reference
                            sVismaOriginalUNHNumber = Mid(sVismaOriginalUNBReference, Len(sVismaOriginalUNBReference) - 5, 3)
                            sVismaOriginalUNBReference = Left(sVismaOriginalUNBReference, Len(sVismaOriginalUNBReference) - 6)

                            If sVismaOriginalUNBReference <> sVismaOldOriginalUNBReference Then
                                'Check if all UNHs from the priginal has received a CONTRL message
                                iVismaNumberOfUNH = CStr(0)
                                bValidationOK = False
                                bAllUNHRejected = True
                                bVismaNewUCI = True
                                bUNBWritten = False
                                sVismaOldOriginalUNBReference = sVismaOriginalUNBReference
                                sVismaTotalNumberOfUNH = Right(xDelim((oBatch.UCIString), "+", 2), 3)
                                For Each oVismaBabelFile In oBabelFiles
                                    For Each oVismaBatch In oVismaBabelFile.Batches
                                        sVismaTempOriginalUNBReference = xDelim(oVismaBatch.UCIString, "+", 2)
                                        sVismaTempOriginalUNBReference = Mid(sVismaTempOriginalUNBReference, 2) 'For Visma_Fokus remove added information to the UNB reference
                                        sVismaTempOriginalUNBReference = Left(sVismaTempOriginalUNBReference, Len(sVismaTempOriginalUNBReference) - 6)
                                        If sVismaTempOriginalUNBReference = sVismaOriginalUNBReference Then
                                            If Left(xDelim(oVismaBatch.UCIString, "+", 5), 1) = "7" Then
                                                bAllUNHRejected = False
                                            End If
                                            iVismaNumberOfUNH = CStr(CDbl(iVismaNumberOfUNH) + 1)
                                            If CDbl(iVismaNumberOfUNH) = Val(sVismaTotalNumberOfUNH) Then
                                                'OK, We've found all CONTRLs
                                                bValidationOK = True
                                                iVismaUNTBabelFileIndex = oVismaBabelFile.Index
                                                iVismaUNTBatchIndex = oVismaBatch.Index
                                                Exit For
                                            End If
                                        Else
                                            'Just continue to next batch
                                        End If
                                    Next oVismaBatch
                                Next oVismaBabelFile
                            Else
                                'It's the same reference as the previous and thus tested.
                                ' Do as You did with previous batch according to the value of bValidationOK
                            End If

                            sUCISegment = oBatch.UCIString

                            If bValidationOK Then

                                If bVismaNewUCI Then

                                    If Not bUNBWritten Then
                                        iPos1 = 1
                                        iPos2 = 1
                                        sTemp = ""
                                        For lCounter = 1 To 5
                                            iPos1 = iPos2
                                            iPos2 = iPos2 + 1
                                            iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
                                        Next lCounter
                                        sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                        sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
                                        'If EmptyString(oBabelFile.EDI_MessageNo) Then
                                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                                        If Not EmptyString(sCompanyNo) Then
                                            sIdentReceiver = Trim(sCompanyNo)
                                        ElseIf sSpecial = "VISMA_FOKUS" Then
                                            sIdentReceiver = "1438086569"
                                        ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
                                            sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
                                        Else
                                            '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
                                        End If
                                        'QUESTION
                                        WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++CONTRL")
                                        bUNBWritten = True

                                        lSegmentCounter = 0

                                        lUNHCounter = lUNHCounter + 1
                                        WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CONTRL:3:1:UN")
                                    End If

                                    iPos1 = 1
                                    iPos2 = 1
                                    sTemp = ""
                                    For lCounter = 1 To 4
                                        iPos1 = iPos2
                                        iPos2 = iPos2 + 1
                                        iPos2 = InStr(iPos2, sUCISegment, "+", CompareMethod.Text)
                                        If lCounter = 2 Then
                                            sOriginalUNBReference = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                            sOriginalUNBReference = Mid(sOriginalUNBReference, 2) 'For Visma_Fokus remove added information to the UNB reference
                                            sOriginalUNBReference = Left(sOriginalUNBReference, Len(sOriginalUNBReference) - 6)
                                        ElseIf lCounter = 3 Then  'Original sender
                                            sIdentSender = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                            If InStr(1, sIdentSender, ":") > 0 Then
                                                sIdentSender = Left(sIdentSender, InStr(1, sIdentSender, ":") - 1)
                                            End If
                                        ElseIf lCounter = 4 Then  'Original receiver
                                            sIdentReceiver = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                            sIdentReceiver = "00810506482" 'Always for Visma
                                        End If
                                    Next lCounter
                                    If sSpecial = "VISMA_FOKUS" Then
                                        sIdentSender = "1438086569"
                                    End If
                                    If bAllUNHRejected Then
                                        WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & "+4")
                                    Else
                                        WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & "+7")
                                    End If
                                    bVismaNewUCI = False

                                End If 'If bVismaNewUCI Then

                                If Not bAllUNHRejected Then
                                    'Don't write UCM if all UNH's are rejected
                                    WriteSegment("UCM+" & sVismaOriginalUNHNumber & "+PAYMUL:D:96A:UN+" & Left(xDelim(sUCISegment, "+", 5), 1))
                                End If

                            Else
                                Err.Raise(15101, "CreateDnBNOR_EDIFile", LRS(15101))

                            End If ''if bValidation OK

                            For Each oPayment In oBatch.Payments
                                oPayment.Exported = True
                            Next oPayment

                            If iVismaUNTBabelFileIndex = oBabelFile.Index Then
                                If iVismaUNTBatchIndex = oBatch.Index Then
                                    WriteUNTSegment()
                                End If
                            End If

                        Next oBatch

                    Next oBabelFile

                    WriteUNZSegment(sUNBReference)

                Else
                    'This message is not complete. We lack the usage of UCM

                    bUNBWritten = False
                    bNewMessage = True

                    lUNHCounter = 0

                    sOldAccountNo = ""
                    bSequenceNoFound = False

                    For Each oBabelFile In oBabelFiles

                        sUNBSegment = oBabelFile.UNBSegmentet
                        sUNHSegment = oBabelFile.UNHSegmentet

                        If Not bUNBWritten Then
                            iPos1 = 1
                            iPos2 = 1
                            sTemp = ""
                            For lCounter = 1 To 5
                                iPos1 = iPos2
                                iPos2 = iPos2 + 1
                                iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
                            Next lCounter
                            sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                            sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
                            'If EmptyString(oBabelFile.EDI_MessageNo) Then
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            If Not EmptyString(sCompanyNo) Then
                                sIdentReceiver = Trim(sCompanyNo)
                            ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
                                sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
                            Else
                                '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                                Err.Raise(15121, "CreateEDIFile", LRS(15121))
                            End If
                            'QUESTION
                            WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++CONTRL")
                            bUNBWritten = True
                        End If

                        lSegmentCounter = 0

                        lUNHCounter = lUNHCounter + 1
                        WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CONTRL:3:1:UN")
                        'To create the unique message_ID use the total amount

                        For Each oBatch In oBabelFile.Batches

                            'This should be rewritten when we get a new customer other than Visma.
                            sUCISegment = oBatch.UCIString
                            iPos1 = 1
                            iPos2 = 1
                            sTemp = ""
                            For lCounter = 1 To 4
                                iPos1 = iPos2
                                iPos2 = iPos2 + 1
                                iPos2 = InStr(iPos2, sUCISegment, "+", CompareMethod.Text)
                                If lCounter = 2 Then
                                    sOriginalUNBReference = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                ElseIf lCounter = 3 Then  'Original sender
                                    sIdentSender = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                    If InStr(1, sIdentSender, ":") > 0 Then
                                        sIdentSender = Left(sIdentSender, InStr(1, sIdentSender, ":") - 1)
                                    End If
                                ElseIf lCounter = 4 Then  'Original receiver
                                    sIdentReceiver = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                End If
                            Next lCounter
                            WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & "+" & oBabelFile.StatusCode)
                            For Each oPayment In oBatch.Payments
                                oPayment.Exported = True
                            Next oPayment

                        Next oBatch

                        WriteUNTSegment()

                    Next oBabelFile

                    WriteUNZSegment(sUNBReference)
                End If

        End Select

        'To write the rest of the file
        WriteSegment("", True)

        If Not oGroupPayment Is Nothing Then
            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oGroupPayment = Nothing
        End If

        On Error GoTo 0
        Exit Function

ERR_CreateDnBNOR_EDIFile:

        If Not oGroupPayment Is Nothing Then
            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oGroupPayment = Nothing
        End If

        Err.Raise(Err.Number, "CreateDnBNOR_EDIFile", Err.Description)

    End Function
    Private Function CreateSEB_SE_EDIFile(ByRef sCompanyNo As String, ByRef sFormat As String) As Boolean
        'KJELL:
        'This code was probably copied from SEB_NO code to start programming new format for Gjensidige F2100, but that should be Nordea eGateway not SEB_SE
        'I don't remember for sure but I don't think a lot pf changes was done in the code and
        ' I think the changes done has nothing to do with SEB_SE
        'It may have been done for another custer (but probablu not)
        'Therefore, I keep the code, but it's commented.

        '        Dim bx As Boolean
        '        Dim sUNBReference As String
        '        Dim sIdentSender As String
        '        Dim sIdentReceiver As String
        '        'Dim aFreetextArray() As String
        '        Dim sStatebankText, sLocalFreetext, sStatebankCode As String
        '        Dim sLocalREF_Own As String
        '        Dim oFreeText As vbBabel.Freetext
        '        Dim lCounter, lArrayCounter As Integer
        '        Dim lCounter2 As Integer
        '        Dim lMaxLengthOfFreetext As Integer
        '        Dim bThisIsOverforselFraKontoIUtland As Boolean
        '        Dim bPaymentFromOtherBank As Boolean
        '        Dim bTheInformationIsStructured As Boolean
        '        Dim sOldAccountNo, sI_SWIFTAddr As String
        '        Dim sLocalPayCode As String
        '        Dim bWriteUNH, bUNBWritten, bUNHWritten As Boolean
        '        Dim bLINWritten As Boolean
        '        Dim bWriteUNTForEachBabelFile As Boolean
        '        Dim bThisIsKID As Boolean
        '        Dim bNewMessage, bNewLIN As Boolean
        '        Dim lPaymentCounter As Integer
        '        Dim sAdditionalNo, sEDIMessageNo As String
        '        Dim sBGMReference As String
        '        Dim oGroupPayment As vbBabel.GroupPayments
        '        Dim sCurrencyToUse As String
        '        Dim sPaymentReference As String
        '        Dim nSequenceNo As Double
        '        Dim bSequenceNoFound As Boolean
        '        Dim sUNBSegment As String
        '        Dim sUNHSegment As String
        '        Dim sUCISegment As String
        '        Dim sOriginalUNBReference As String
        '        Dim sTemp As String
        '        Dim iPos1 As Short
        '        Dim iPos2 As Short
        '        Dim bAllUNHRejected As Boolean

        '        Dim bGroupPayments As Boolean
        '        Dim sUNHReference As String = ""
        '        Dim bThisIsFirstUNH As Boolean
        '        Dim nAmountInSEQ As Double
        '        Dim oFileSetup As vbBabel.FileSetup

        '        On Error GoTo ERR_CreateSEB_SE_EDIFile

        '        sUNBReference = ""
        '        sIdentSender = ""
        '        sIdentReceiver = ""
        '        sOriginalUNBReference = ""

        '        sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
        '        sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
        '        sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
        '        sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
        '        sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
        '        sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

        '        WriteSegment("UNA:+,? ")

        '        Select Case sFormat
        '            '11.03.2011 - XNET - Added next Case
        '            Case "PAYMUL"
        '                bUNBWritten = False
        '                bNewMessage = True
        '                bUNHWritten = False

        '                oGroupPayment = CreateObject ("vbBabel.GroupPayments")
        '                oGroupPayment.GroupCrossBorderPayments = False
        '                bGroupPayments = False

        '                lLINCounter = 0

        '                For Each oBabelFile In oBabelFiles
        '                    'The next IF may be removed when new code for Gjensidige is implmented
        '                    'Marked 2CONTRL
        '                    If Not bUNBWritten Then
        '                        If EmptyString((oBabelFile.EDI_MessageNo)) Then
        '                            sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
        '                        Else
        '                            sUNBReference = oBabelFile.EDI_MessageNo
        '                        End If
        '                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
        '                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
        '                        sAdditionalNo = "" ' moved here 11.05.2010


        '                        ' added 11.05.2010 to be able to have different CompanyNo's, like Gjensidige:
        '                        For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
        '                            If oFilesetup.FileSetup_ID = iFilesetupOut Then
        '                                If Not EmptyString(oFilesetup.CompanyNo) Then
        '                                    sCompanyNo = oFilesetup.CompanyNo
        '                                End If
        '                                If Not EmptyString(oFilesetup.AdditionalNo) Then
        '                                    sAdditionalNo = oFilesetup.AdditionalNo
        '                                End If
        '                            End If
        '                        Next oFilesetup

        '                        If Not EmptyString(sCompanyNo) Then
        '                            sIdentSender = Trim(sCompanyNo)
        '                        Else
        '                            '"No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer."
        '                            Err.Raise(1, "CreateEDIFile", LRS(15121))
        '                        End If
        '                        'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A+++" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1
        '                        '03.03.2008 Changed by Kjell
        '                        'sAdditionalNo = ""  ' 11.05.2010 moved up
        '                        If oBabelFiles.Count > 0 Then
        '                            If EmptyString(sAdditionalNo) Then ' added 11.05.2010
        '                                If Not EmptyString(oBabelFiles.Item(1).VB_Profile.AdditionalNo) Then
        '                                    sAdditionalNo = Trim(oBabelFiles.Item(1).VB_Profile.AdditionalNo)
        '                                End If
        '                            End If
        '                        End If
        '                        If Not EmptyString(sAdditionalNo) Then
        '                            WriteSegment("UNB+UNOC:3+" & sIdentSender & "+" & sAdditionalNo & ":30+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference) ' & "+1"
        '                        Else
        '                            '"15128: No 'Recipient Identification' is stated." & vbCrLf & "This may be entered in the company setup in the field 'AdditionalNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                            Err.Raise(15128, "CreateEDIFile", LRS(15128))
        '                        End If
        '                        'Old code
        '                        'WriteSegment "UNB+UNOC:3+" & sIdentSender & "+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference   ' & "+1"
        '                        bUNBWritten = True
        '                    End If

        '                    'The next IF may be removed when new code for Gjensidige is implmented
        '                    'Marked 2CONTRL
        '                    If bNewMessage Then
        '                        lUNHCounter = lUNHCounter + 1
        '                        lSegmentCounter = 0
        '                        WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN:SEBV50")
        '                        WriteSegment("BGM+452+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS"))
        '                        If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
        '                            WriteDTMSegment("137", DateToString(Now), "102")
        '                        Else
        '                            WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
        '                        End If
        '                        WriteSegment("FII+MR++ESSESESS")
        '                        If Not EmptyString((oBabelFile.IDENT_Sender)) Then
        '                            WriteSegment("NAD+MS+" & Trim(oBabelFile.IDENT_Sender))
        '                        ElseIf Not EmptyString(oBabelFile.VB_Profile.CompanyNo) Then
        '                            WriteSegment("NAD+MS+" & Trim(oBabelFile.VB_Profile.CompanyNo))
        '                        Else
        '                            WriteSegment("NAD+MS+BabelBank")
        '                        End If
        '                        bNewMessage = False
        '                    End If
        '                    'maybe we need
        '                    'writesegment "NAD+MS+" & Code identifying a party involved in a transaction MS = Sender

        '                    For Each oBatch In oBabelFile.Batches

        '                        'The next IF must be used when new code for Gjensidige is implemented
        '                        'Marked 2CONTRL - remove the IF-sentence
        '                        'This code is vital for Gjensidige and must not be changed
        '                        If Not EmptyString((oBatch.Batch_ID)) Then
        '                            If sUNHReference <> oBatch.Batch_ID Then
        '                                bUNHWritten = False
        '                            End If
        '                        End If

        '                        'The next IF must be used when new code for Gjensidige is implemented
        '                        'Marked 2CONTRL - remove And bNewCodeForGjensidige
        '                        If Not bUNBWritten Then
        '                            If bUNHWritten Then
        '                                'We have already written UNH before, and have to write a CNT and UNT before we write a new one
        '                                WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
        '                                If EmptyString(sUNHReference) Then
        '                                    WriteUNTSegment()
        '                                Else
        '                                    WriteUNTSegment(sUNHReference)
        '                                End If
        '                                WriteUNZSegment(sUNBReference)
        '                                bUNHWritten = False
        '                            End If
        '                            If Not EmptyString(sUNBReference) Then
        '                                'Nothing to do
        '                            ElseIf EmptyString((oBabelFile.EDI_MessageNo)) Then
        '                                sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
        '                            Else
        '                                sUNBReference = oBabelFile.EDI_MessageNo
        '                            End If
        '                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
        '                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
        '                            If Not EmptyString(sCompanyNo) Then
        '                                sIdentSender = Trim(sCompanyNo)
        '                            Else
        '                                '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                                Err.Raise(15121, "CreateEDIFile", LRS(15121))
        '                            End If
        '                            'WriteSegment "UNB+UNOC:3+" & sIdentSender & ":ZZ+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference & "+" & Trim$(oBabelFile.VB_Profile.Custom1Value) & "+DBTS96A+++" & Trim$(oBabelFile.VB_Profile.Custom2Value) ' & "+1", If test add +1
        '                            '03.03.2008 Changed by Kjell
        '                            sAdditionalNo = ""
        '                            If oBabelFiles.Count > 0 Then
        '                                If Not EmptyString(oBabelFiles.Item(1).VB_Profile.AdditionalNo) Then
        '                                    sAdditionalNo = Trim(oBabelFiles.Item(1).VB_Profile.AdditionalNo)
        '                                End If
        '                            End If
        '                            If Not EmptyString(sAdditionalNo) Then
        '                                WriteSegment("UNB+UNOC:3+" & sIdentSender & "+" & sAdditionalNo & ":30+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference) ' & "+1"
        '                            Else
        '                                '"15128: No 'Recipient Identification' is stated." & vbCrLf & "This may be entered in the company setup in the field 'AdditionalNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                                Err.Raise(15128, "CreateEDIFile", LRS(15128))
        '                            End If
        '                            'Old code
        '                            'WriteSegment "UNB+UNOC:3+" & sIdentSender & "+5790000243440:14+" &VB6.Format(Now(), "YYMMDD") & ":" &VB6.Format(Now(), "HHMM") & "+" & sUNBReference   ' & "+1"
        '                            bUNBWritten = True
        '                        End If

        '                        'The next IF must be used when new code for Gjensidige is implemented
        '                        'Marked 2CONTRL - remove And bNewCodeForGjensidige

        '                        If Not bUNHWritten Then

        '                            If Not bThisIsFirstUNH Then
        '                                'We have already written UNH before, and have to write a CNT and UNT before we write a new one
        '                                WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
        '                                If EmptyString(sUNHReference) Then
        '                                    WriteUNTSegment()
        '                                Else
        '                                    WriteUNTSegment(sUNHReference)
        '                                End If
        '                                bUNHWritten = False
        '                            End If
        '                            lUNHCounter = lUNHCounter + 1
        '                            lLINCounter = 0
        '                            lSegmentCounter = 0

        '                            If Not EmptyString((oBatch.Batch_ID)) Then
        '                                sUNHReference = oBatch.Batch_ID
        '                            End If

        '                            If EmptyString(sUNHReference) Then
        '                                sUNHReference = Trim(Str(lUNHCounter))
        '                            End If
        '                            WriteSegment("UNH+" & sUNHReference & "+PAYMUL:D:96A:UN:SEBV50")
        '                            WriteSegment("BGM+452+BABELBANK" & VB6.Format(Now, "YYYYMMDDHHMMSS"))
        '                            If EmptyString((oBabelFile.DATE_Production)) Then
        '                                WriteDTMSegment("137", DateToString(Now), "102")
        '                            Else
        '                                WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
        '                            End If
        '                            WriteSegment("FII+MR++ESSESESS")
        '                            If EmptyString((oBabelFile.IDENT_Sender)) Then
        '                                WriteSegment("NAD+MS+BabelBank")
        '                            Else
        '                                WriteSegment("NAD+MS+" & Trim(oBabelFile.IDENT_Sender))
        '                            End If
        '                            bNewMessage = False
        '                            bUNHWritten = True
        '                            bThisIsFirstUNH = False
        '                        End If

        '                        '05.03.2010
        '                        If oBatch.Payments.Count > 0 Then
        '                            If oBatch.Payments.Item(1).PayType = "M" And oBatch.Payments.Item(1).PayCode = "222" Then
        '                                oGroupPayment.SetGroupingParameters(True, True, True, True)
        '                                bGroupPayments = True
        '                            Else
        '                                oGroupPayment.SetGroupingParameters(False, False, False, False)
        '                                bGroupPayments = False
        '                            End If
        '                        Else
        '                            oGroupPayment.SetGroupingParameters(False, False, False, False)
        '                            bGroupPayments = False
        '                        End If

        '                        For lCounter2 = 1 To oBatch.Payments.Count
        '                            bNewLIN = True

        '                            oGroupPayment.Batch = oBatch
        '                            oGroupPayment.MarkIdenticalPayments()
        '                            'Group the payments inside a batch
        '                            lPaymentCounter = 0

        '                            For Each oPayment In oBatch.Payments
        '                                If oPayment.SpecialMark And Not oPayment.Exported Then
        '                                    lPaymentCounter = lPaymentCounter + 1
        '                                    If sOldAccountNo <> Trim(oPayment.I_Account) Then
        '                                        'bNewPayments = True Don't need a new payments when the debit account is changed
        '                                        If oBabelFile.VB_ProfileInUse Then
        '                                            bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
        '                                        End If
        '                                        sOldAccountNo = Trim(oPayment.I_Account)
        '                                    End If

        '                                    'Find country for receiver if not stated
        '                                    If EmptyString((oPayment.I_CountryCode)) Then
        '                                        If Not EmptyString((oPayment.BANK_I_SWIFTCode)) Then
        '                                            oPayment.I_CountryCode = Mid(oPayment.BANK_I_SWIFTCode, 5, 2)
        '                                        Else
        '                                            oPayment.I_CountryCode = Mid(sI_SWIFTAddr, 5, 2)
        '                                            'If debit account and receiver is in the same country it must be
        '                                            '  a domestic payment
        '                                            'If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
        '                                            '    oPayment.PayType = "I"
        '                                            'Else
        '                                            '    oPayment.PayType = "D"
        '                                            'End If
        '                                        End If
        '                                    End If


        '                                    '********************************************************************
        '                                    'Be careful to change this for other customers!!!!!!!
        '                                    'Removed 04.02.2008 for Gjensidige. It is correctly set during import
        '                                    ' Created problems for domestic payments where receiver has an address abroad

        '                                    '16.04.2008 - Kjell
        '                                    'Should be OK to remove the comment on the IsDomestic part now, because we have
        '                                    '  introduced a new variable in the Payment-Class (oPayment.PayTypeSetInImport).
        '                                    '  If this is property is True (as it now should be for Gjensidige), we use the value
        '                                    '  in oPayment.PayType in the IsPaymentDomestic function.
        '                                    'Because I'm a chicken I haven't dared to remove it yet!

        '                                    '                    If IsPaymentDomestic(oPayment, sI_SWIFTAddr) Then
        '                                    '                        oPayment.PayType = "D"
        '                                    '                    Else
        '                                    '                        oPayment.PayType = "I"
        '                                    '                    End If

        '                                    oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
        '                                    '''''lSEQCounter = 0
        '                                    If Left(oPayment.BANK_I_SWIFTCode, 4) <> "ESSE" Then
        '                                        '"Possibility to send payments from another bank" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
        '                                        Err.Raise(15129, "CreateEDIFile", LRS(15129))
        '                                        'bThisIsOverforselFraKontoIUtland = True
        '                                    Else
        '                                        bThisIsOverforselFraKontoIUtland = False
        '                                    End If

        '                                    lCounter = 0
        '                                    lArrayCounter = 0
        '                                    nAmountInSEQ = 0

        '                                    'Not validated against valid chars for SEB
        '                                    oPayment.E_Name = CheckForValidCharacters((oPayment.E_Name), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.E_Adr1 = CheckForValidCharacters((oPayment.E_Adr1), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.E_Adr2 = CheckForValidCharacters((oPayment.E_Adr2), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.E_Adr3 = CheckForValidCharacters((oPayment.E_Adr3), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.E_City = CheckForValidCharacters((oPayment.E_City), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.BANK_Name = CheckForValidCharacters((oPayment.BANK_Name), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.BANK_Adr1 = CheckForValidCharacters((oPayment.BANK_Adr1), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.BANK_Adr2 = CheckForValidCharacters((oPayment.BANK_Adr2), True, True, True, True, " /-?:().,'+")
        '                                    oPayment.BANK_Adr3 = CheckForValidCharacters((oPayment.BANK_Adr3), True, True, True, True, " /-?:().,'+")

        '                                    If bThisIsOverforselFraKontoIUtland Then
        '                                        lMaxLengthOfFreetext = 140
        '                                    Else
        '                                        Select Case oPayment.I_CountryCode

        '                                            Case "DE"
        '                                                If oPayment.PayType = "I" Then
        '                                                    'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
        '                                                    lMaxLengthOfFreetext = 140
        '                                                Else
        '                                                    lMaxLengthOfFreetext = 162
        '                                                End If
        '                                            Case "DK"
        '                                                If oPayment.PayType = "I" Then
        '                                                    lMaxLengthOfFreetext = 140
        '                                                Else
        '                                                    lMaxLengthOfFreetext = 1435
        '                                                End If
        '                                            Case "SE"
        '                                                If oPayment.PayType = "I" Then
        '                                                    lMaxLengthOfFreetext = 140
        '                                                Else
        '                                                    lMaxLengthOfFreetext = 1050
        '                                                End If
        '                                            Case "US"
        '                                                If oPayment.PayType = "I" Then
        '                                                    lMaxLengthOfFreetext = 140
        '                                                Else
        '                                                    lMaxLengthOfFreetext = 140
        '                                                End If
        '                                            Case "NO"
        '                                                If oPayment.PayType = "I" Then
        '                                                    lMaxLengthOfFreetext = 140
        '                                                Else
        '                                                    lMaxLengthOfFreetext = 140 'According to SEB
        '                                                End If

        '                                            Case Else
        '                                                'sReturnString = "15126: Possibility to send payments from %1" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
        '                                                Err.Raise(15126, "CreateEDIFile", LRS(15126))
        '                                        End Select
        '                                    End If

        '                                    If lMaxLengthOfFreetext = 0 Then
        '                                        lMaxLengthOfFreetext = 140
        '                                    End If

        '                                    'This functionality may be used if you want to split payments into
        '                                    ' more than one LIN to be able to send all the freetext information.
        '                                    'aFreetextArray = CreateFreetextSEQAmountArray(lMaxLengthOfFreetext)
        '                                    'Do While lArrayCounter <= UBound(aFreetextArray(), 2)
        '                                    If bNewLIN Then
        '                                        If bGroupPayments Then
        '                                            bNewLIN = False
        '                                        End If
        '                                        lSEQCounter = 0
        '                                        '4-LIN
        '                                        WriteLINSegment()
        '                                        '4-DTM
        '                                        If bThisIsOverforselFraKontoIUtland Then

        '                                        Else
        '                                            Select Case oPayment.I_CountryCode
        '                                                Case Else
        '                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
        '                                            End Select
        '                                        End If
        '                                        '4-RFF
        '                                        If Len(oBatch.REF_Own) > 0 Then
        '                                            '20.02.2008 - This is done for Gjensidige to do the reference at the LIN-level
        '                                            ' unique.
        '                                            'oBatch.REF_Own = oBatch.REF_Own & "-" & Trim$(Str(lPaymentCounter))
        '                                            WriteRFFSegment("AEK", CheckForValidCharacters(Trim(oBatch.REF_Own) & "-" & Trim(Str(lPaymentCounter)), True, True, True, True, " /-?:().,'+"))
        '                                        Else
        '                                            WriteRFFSegment("AEK", sUNBReference & Trim(Str(lLINCounter)), 35)
        '                                        End If
        '                                        '4-BUS
        '                                        '03.05.2010 - for Gjensidege
        '                                        If oPayment.PayType = "M" And oPayment.PayCode = "222" Then
        '                                            WriteSegment("BUS+1:ZZZ+DO")
        '                                        Else
        '                                            WriteBUSSegment((oPayment.PayType))
        '                                        End If
        '                                        '4-FCA
        '                                        If oPayment.PayType = "I" Then
        '                                            WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad))
        '                                        End If
        '                                        '5-MOA
        '                                        If Len(Trim(oPayment.MON_TransferCurrency)) <> 3 Then
        '                                            If EmptyString((oPayment.MON_TransferCurrency)) Then
        '                                                oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency
        '                                            Else
        '                                                '"Wrong transfercurrency stated on the payment." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & vbCrLf & "Currency: " & Trim(oPayment.MON_TransferCurrency) & vbCrLf & vbCrLf & "Contact Your dealer.")
        '                                                Err.Raise(15130, "CreateEDIFile", LRS(15130) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & vbCrLf & vbCrLf & LRS(10016))
        '                                            End If
        '                                        End If

        '                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
        '                                            'NOMORE
        '                                            'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
        '                                            '03.05.2010
        '                                            If bGroupPayments Then
        '                                                WriteMOASegment("9", oGroupPayment.MON_InvoiceAmount, (oPayment.MON_InvoiceCurrency))
        '                                            Else
        '                                                WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
        '                                            End If
        '                                            'Old code
        '                                            'WriteMOASegment "9", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency
        '                                        Else
        '                                            'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
        '                                            'NOMORE
        '                                            'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
        '                                            '03.05.2010
        '                                            If bGroupPayments Then
        '                                                WriteMOASegment("57", oGroupPayment.MON_InvoiceAmount, "")
        '                                            Else
        '                                                WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
        '                                            End If
        '                                            'Old code
        '                                            'WriteMOASegment "57", oPayment.MON_InvoiceAmount, ""
        '                                            '5-CUX AND 5-RFF
        '                                            If oPayment.ERA_ExchRateAgreed = 0 Then
        '                                                If oPayment.FRW_ForwardContractRate = 0 Then
        '                                                    'No rates
        '                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
        '                                                    'Do not use 5-RFF
        '                                                Else
        '                                                    'Raise an error because this code is not tested
        '                                                    '"The use of 'Forward contract rate' is not yet implemented in this format." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & vbCrLf & vbCrLf & "Contact Your dealer."
        '                                                    Err.Raise(15131, "CreateEDIFile", LRS(15131) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
        '                                                    'Code to be used will be something like this
        '                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.FRW_ForwardContractRate), , ","))
        '                                                    If Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
        '                                                        WriteRFFSegment("ACX", Trim(oPayment.FRW_ForwardContractNo))
        '                                                    Else
        '                                                        '"No 'Forward contract number' is stated when a" & vbCrLf & "'Forward exchange rate' is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
        '                                                        Err.Raise(15132, "CreateEDIFile", LRS(15132) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
        '                                                    End If
        '                                                End If
        '                                            Else
        '                                                WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.ERA_ExchRateAgreed), , ","))
        '                                                If Not EmptyString((oPayment.ERA_DealMadeWith)) Then
        '                                                    WriteRFFSegment("FX", Trim(oPayment.ERA_DealMadeWith))
        '                                                Else
        '                                                    '"No 'Foreign exchange contract number' is stated when an" & vbCrLf & "agreed exchangerate is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
        '                                                    Err.Raise(15133, "CreateEDIFile", LRS(15133) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
        '                                                End If
        '                                            End If
        '                                        End If
        '                                        '6-FII
        '                                        'Changed 08.04.05 - Kjell
        '                                        WriteSegment("FII+OR+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")

        '                                        'Use the Invoice who contains last freetext
        '                                        'NOMORE
        '                                        'Set oInvoice = oPayment.Invoices.Item(Val(aFreetextArray(2, lArrayCounter)))
        '                                        If oPayment.I_CountryCode = "NO" And Trim(oPayment.PayCode) = "301" Then
        '                                            bThisIsKID = True
        '                                        Else
        '                                            bThisIsKID = False
        '                                            sLocalFreetext = ""
        '                                            sLocalREF_Own = ""
        '                                            For Each oInvoice In oPayment.Invoices
        '                                                For Each oFreeText In oInvoice.Freetexts
        '                                                    If oFreeText.Qualifier = CDbl("1") Then
        '                                                        sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
        '                                                    End If
        '                                                Next oFreeText
        '                                            Next oInvoice
        '                                            If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
        '                                                'sLocalFreetext = "SEE SEPERATE LETTER."
        '                                            End If
        '                                            sLocalFreetext = RTrim(Left(sLocalFreetext, lMaxLengthOfFreetext))
        '                                        End If
        '                                        If oPayment.Invoices.Count > 0 Then
        '                                            sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
        '                                        End If
        '                                    End If 'If bNewLIN

        '                                    '11-SEQ
        '                                    WriteSEQSegment()
        '                                    '11-MOA
        '                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
        '                                        'NOMORE
        '                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
        '                                        WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
        '                                    Else
        '                                        'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
        '                                        'NOMORE
        '                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
        '                                        WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
        '                                    End If
        '                                    '11-DTM
        '                                    'Only to be used  for cash pool internal transfers
        '                                    'Must do some sort of test for this
        '                                    'WriteDTMSegment "209", oPayment.DATE_Payment, "102", True
        '                                    '11-RFF
        '                                    If Len(Trim(oPayment.REF_Own)) > 0 Then
        '                                        WriteRFFSegment("CR", CheckForValidCharacters((oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
        '                                    Else
        '                                        '"No customer reference (own reference is stated." & vbCrLf & "Receiver: " & oPayment.E_Name & vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount
        '                                        Err.Raise(15134, "CreateEDIFile", LRS(15134) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & LRS(35002, oPayment.MON_InvoiceAmount) & vbCrLf & vbCrLf & LRS(10016))
        '                                    End If
        '                                    '11-PAI
        '                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
        '                                        If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
        '                                            WriteSegment("PAI+::23")
        '                                        ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
        '                                            WriteSegment("PAI+::23")
        '                                        Else
        '                                            WriteSegment("PAI+::26")
        '                                        End If
        '                                    ElseIf oPayment.Priority Then
        '                                        WriteSegment("PAI+::ZZ")
        '                                    Else
        '                                        'For danish payments (and maybe others, we should be more specific)
        '                                        'NO PAI-segment
        '                                    End If
        '                                    '11 - FCA - Can't be included if FCA is stated at LIN-level
        '                                    'If oPayment.PayType = "I" Then
        '                                    '    WriteFCASegment oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad
        '                                    'End If
        '                                    '12 - FII
        '                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
        '                                        'Do not use the FI-segment
        '                                    Else
        '                                        If oPayment.PayType = "I" Then
        '                                            WriteFIISegmentSEB(E_Info, True, (oPayment.I_CountryCode), True, True)
        '                                        Else
        '                                            If oPayment.I_CountryCode = "NO" Then
        '                                                WriteFIISegmentSEB(E_Info, True, (oPayment.I_CountryCode), True, False)
        '                                            Else
        '                                                WriteFIISegmentSEB(E_Info, True, (oPayment.I_CountryCode), True, False)
        '                                            End If
        '                                        End If
        '                                        If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Or Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
        '                                            WriteFIISegmentSEB(CorrBank, True, (oPayment.I_CountryCode), True, True)
        '                                        End If
        '                                    End If
        '                                    '13 - NAD
        '                                    WriteNADSegment(True, True)
        '                                    '13 - CTA
        '                                    'Not implemented
        '                                    '13 - COM
        '                                    'Not implemented
        '                                    '14 - INP
        '                                    'Added code here 06.03.2008
        '                                    '03.05.2010 - reopened the usage of text on receivers statement.
        '                                    If oPayment.PayType = "M" And oPayment.PayCode = "222" Then
        '                                        If Not EmptyString((oPayment.Text_E_Statement)) Then
        '                                            'Set the info in the FTX beneath on receivers bankaccount
        '                                            WriteSegment("INP+3:4+1:EI")
        '                                            '14 - FTX
        '                                            WriteFTXSegment("AAG", Left(Trim(oPayment.Text_E_Statement), 70))
        '                                        End If
        '                                    End If
        '                                    '                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "NO" Then
        '                                    '                        If Not EmptyString(oPayment.Text_E_Statement) Then
        '                                    '                            'Set the info in the FTX beneath on receivers bankaccount
        '                                    '                            WriteSegment "INP+3:4+1:AD"
        '                                    '                            '14 - FTX
        '                                    '                            WriteFTXSegment "AAG", Left$(Trim$(oPayment.Text_E_Statement), 70)
        '                                    '                        End If
        '                                    '                    End If
        '                                    '15 - GIS
        '                                    If oPayment.I_CountryCode = "NO" And oPayment.PayType = "I" Then
        '                                        'Central Bank Reporting
        '                                        WriteSegment("GIS+10")
        '                                        sStatebankText = ""
        '                                        sStatebankCode = ""
        '                                        For Each oInvoice In oPayment.Invoices
        '                                            sStatebankText = oInvoice.STATEBANK_Text
        '                                            sStatebankCode = oInvoice.STATEBANK_Code
        '                                            If Not EmptyString(sStatebankText) Then
        '                                                Exit For
        '                                            End If
        '                                        Next oInvoice
        '                                        If EmptyString(sStatebankText) Then
        '                                            If EmptyString(sStatebankCode) Then
        '                                                WriteFTXSegment("REG", "Export/Import av varer", CStr(14))
        '                                            Else
        '                                                WriteFTXSegment("REG", "Export/Import av varer", sStatebankCode)
        '                                            End If
        '                                        Else
        '                                            If EmptyString(sStatebankCode) Then
        '                                                WriteFTXSegment("REG", sStatebankText, CStr(14))
        '                                            Else
        '                                                WriteFTXSegment("REG", sStatebankText, sStatebankCode)
        '                                            End If
        '                                        End If

        '                                    End If
        '                                    '15 - FTX
        '                                    'WriteFTXSegment "REG", CheckForValidCharacters(oInvoice.STATEBANK_Text + ???oInvoice.STATEBANK_Code
        '                                    '16-PRC
        '                                    If bThisIsKID Then
        '                                        For Each oInvoice In oPayment.Invoices
        '                                            WriteSegment("PRC+8")
        '                                            WriteDOCSegment("YW3", Trim(oInvoice.Unique_Id), 35)
        '                                            WriteMOASegment(CStr(12), (oInvoice.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
        '                                        Next oInvoice
        '                                        '23-GIS
        '                                        WriteSegment("GIS+37")
        '                                        '23-MOA
        '                                        WriteMOASegment("128", (oPayment.MON_InvoiceAmount), "")
        '                                    Else
        '                                        If Not EmptyString(sLocalFreetext) Then
        '                                            WriteSegment("PRC+11")
        '                                            '16-FTX
        '                                            'NOMORE
        '                                            'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
        '                                            WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
        '                                            '23-GIS
        '                                            WriteSegment("GIS+37")
        '                                        Else
        '                                            'No advice
        '                                        End If
        '                                    End If
        '                                    oPayment.Exported = True

        '                                End If 'If oPayment.SpecialMark And Not oPayment.Exported Then

        '                            Next oPayment
        '                            oGroupPayment.RemoveSpecialMark()
        '                            If oGroupPayment.AllPaymentsMarkedAsExported Then
        '                                Exit For
        '                            End If
        '                        Next lCounter2
        '                    Next oBatch
        '                Next oBabelFile
        '                WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))

        '                'The next IF must be used when new code for Gjensidige is implemented
        '                'Marked 2CONTRL - remove the IF
        '                WriteUNTSegment()
        '                WriteUNZSegment(sUNBReference)

        '            Case "DEBMUL"

        '                bUNBWritten = False
        '                bNewMessage = True

        '                lUNHCounter = 0
        '                'lLINCounter = 0 - 04.09.2008 moved down

        '                sOldAccountNo = ""
        '                bSequenceNoFound = False

        '                For Each oBabelFile In oBabelFiles

        '                    sUNBSegment = oBabelFile.UNBSegmentet
        '                    sUNHSegment = oBabelFile.UNHSegmentet

        '                    If Not bUNBWritten Then
        '                        iPos1 = 1
        '                        iPos2 = 1
        '                        sTemp = ""
        '                        For lCounter = 1 To 5
        '                            iPos1 = iPos2
        '                            iPos2 = iPos2 + 1
        '                            iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
        '                        Next lCounter
        '                        sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                        sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
        '                        'If EmptyString(oBabelFile.EDI_MessageNo) Then
        '                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
        '                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
        '                        If Not EmptyString(sCompanyNo) Then
        '                            sIdentReceiver = Trim(sCompanyNo)
        '                        ElseIf sSpecial = "VISMA_FOKUS" Then
        '                            sIdentReceiver = "1438086569"
        '                        ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
        '                            sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
        '                        Else
        '                            '"15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
        '                        End If
        '                        'QUESTION
        '                        WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference) '& "++++1"
        '                        bUNBWritten = True
        '                    End If

        '                    lSegmentCounter = 0
        '                    lLINCounter = 0 '04.09.2008, moved from above

        '                    lUNHCounter = lUNHCounter + 1
        '                    WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+DEBMUL:D:96A:UN")
        '                    'To create the unique message_ID use the total amount

        '                    If Not EmptyString((oBabelFile.EDI_MessageNo)) Then
        '                        sBGMReference = oBabelFile.EDI_MessageNo
        '                    Else
        '                        sBGMReference = CreateMessageReference(oBabelFile)
        '                    End If
        '                    WriteSegment("BGM+470+" & sBGMReference)
        '                    If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
        '                        WriteDTMSegment("137", DateToString(Now), "102")
        '                    Else
        '                        WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
        '                    End If
        '                    'WriteSegment "NAD+MR+" & Organisasjonsnummer
        '                    'QUESTION
        '                    'If EmptyString(oBabelFile.IDENT_Sender) Then
        '                    '    WriteSegment "NAD+MS+BabelBank"
        '                    'Else
        '                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
        '                    'End If

        '                    For Each oBatch In oBabelFile.Batches

        '                        bLINWritten = False
        '                        For Each oPayment In oBatch.Payments

        '                            If Not bLINWritten Then
        '                                '4-LIN
        '                                WriteLINSegment()
        '                                '4-DTM
        '                                If oPayment.DATE_Value = "19900101" Then
        '                                    'WriteDTMSegment "137", DateToString(Now()), "102"
        '                                Else
        '                                    WriteDTMSegment("209", (oPayment.DATE_Value), "102")
        '                                End If
        '                                If oPayment.DATE_Payment = "19900101" Then
        '                                    'WriteDTMSegment "137", DateToString(Now()), "102"
        '                                Else
        '                                    WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
        '                                End If
        '                                '4-BUS
        '                                If oPayment.PayType = "I" Then
        '                                    WriteSegment("BUS++IN")
        '                                Else
        '                                    WriteSegment("BUS++DO")
        '                                End If
        '                                '4-MOA
        '                                WriteMOASegment("60", (oBatch.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
        '                                '5-RFF
        '                                If Not EmptyString((oBatch.REF_Bank)) Then
        '                                    WriteRFFSegment("ACK", (oBatch.REF_Bank))
        '                                End If
        '                                If Not EmptyString((oBatch.REF_Own)) Then
        '                                    WriteRFFSegment("CR", (oBatch.REF_Own))
        '                                End If
        '                                '6-FII
        '                                If Not EmptyString((oPayment.I_Account)) Then
        '                                    WriteSegment("FII+OR+" & oPayment.I_Account)
        '                                End If
        '                                '7-FCA
        '                                '7-MOA
        '                                '(8-ALC)
        '                                bLINWritten = True
        '                            End If
        '                            '10-SEQ
        '                            WriteSEQSegment()
        '                            '10-DTM

        '                            '(10-BUS)

        '                            '(10-FII)
        '                            WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), , False)

        '                            If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Or Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
        '                                WriteFIISegment(CorrBank, True, (oPayment.I_CountryCode), , False)
        '                            End If

        '                            '11-RFF
        '                            If Not EmptyString((oPayment.REF_Own)) Then
        '                                WriteRFFSegment("AGN", Trim(oPayment.REF_Own))
        '                            End If
        '                            If Not EmptyString((oPayment.REF_Bank1)) Then
        '                                WriteRFFSegment("ACK", Trim(oPayment.REF_Bank1))
        '                            End If
        '                            If Not EmptyString((oPayment.REF_Bank2)) Then
        '                                WriteRFFSegment("ACD", Trim(oPayment.REF_Bank2))
        '                            End If

        '                            '('12-PAI)

        '                            '13-MOA
        '                            WriteMOASegment("60", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))

        '                            If Not EmptyString((oPayment.MON_TransferCurrency)) Then
        '                                WriteMOASegment("98", (oPayment.MON_TransferredAmount), (oPayment.MON_TransferCurrency))
        '                            End If

        '                            '13-CUX ikke implementert
        '                            If oPayment.MON_AccountExchRate > 0 Then
        '                                WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), CStr(oPayment.MON_AccountExchRate))
        '                            End If

        '                            '14-NAD
        '                            WriteNADSegment(True, True, "BE")

        '                            '17-FCA
        '                            '17-MOA
        '                            If oPayment.PayType = "I" Then
        '                                WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad))
        '                                If oPayment.MON_ChargesAmount > 0 Then
        '                                    WriteMOASegment("23", (oPayment.MON_ChargesAmount), (oPayment.MON_ChargesCurrency))
        '                                End If
        '                            End If

        '                            oPayment.Exported = True

        '                        Next oPayment

        '                    Next oBatch

        '                    '04.09.2008 - Moved from beneath
        '                    WriteSegment("CNT+LI:" & Trim(Str(lLINCounter)))
        '                    WriteUNTSegment()
        '                Next oBabelFile
        '                'Old code
        '                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
        '                'WriteUNTSegment
        '                WriteUNZSegment(sUNBReference)

        '            Case "BANSTA"

        '                bUNBWritten = False
        '                bNewMessage = True

        '                lUNHCounter = 0
        '                'lLINCounter = 0 - 04.09.2008 moved down

        '                sOldAccountNo = ""
        '                bSequenceNoFound = False

        '                For Each oBabelFile In oBabelFiles

        '                    sUNBSegment = oBabelFile.UNBSegmentet
        '                    sUNHSegment = oBabelFile.UNHSegmentet

        '                    If Not bUNBWritten Then
        '                        iPos1 = 1
        '                        iPos2 = 1
        '                        sTemp = ""
        '                        For lCounter = 1 To 5
        '                            iPos1 = iPos2
        '                            iPos2 = iPos2 + 1
        '                            iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
        '                        Next lCounter
        '                        sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                        sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
        '                        'If EmptyString(oBabelFile.EDI_MessageNo) Then
        '                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
        '                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
        '                        If Not EmptyString(sCompanyNo) Then
        '                            sIdentReceiver = Trim(sCompanyNo)
        '                        ElseIf sSpecial = "VISMA_FOKUS" Then
        '                            sIdentReceiver = "1438086569"
        '                        ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
        '                            sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
        '                        Else
        '                            '"15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
        '                        End If
        '                        'QUESTION
        '                        WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference) '& "++++1"
        '                        bUNBWritten = True
        '                    End If

        '                    lSegmentCounter = 0
        '                    lLINCounter = 0 '04.09.2008, moved from above

        '                    lUNHCounter = lUNHCounter + 1
        '                    WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+BANSTA:D:96A:UN")
        '                    'To create the unique message_ID use the total amount

        '                    If Not EmptyString((oBabelFile.EDI_MessageNo)) Then
        '                        sBGMReference = oBabelFile.EDI_MessageNo
        '                    Else
        '                        sBGMReference = CreateMessageReference(oBabelFile)
        '                    End If
        '                    WriteSegment("BGM+77+" & sBGMReference)
        '                    If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
        '                        WriteDTMSegment("137", DateToString(Now), "102")
        '                    Else
        '                        WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
        '                    End If
        '                    WriteSegment("FII+MS++DNBANOKK:25:5")
        '                    'QUESTION
        '                    'If EmptyString(oBabelFile.IDENT_Sender) Then
        '                    '    WriteSegment "NAD+MS+BabelBank"
        '                    'Else
        '                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
        '                    'End If

        '                    For Each oBatch In oBabelFile.Batches

        '                        For Each oPayment In oBatch.Payments
        '                            '4-LIN
        '                            WriteLINSegment()
        '                            '5-RFF
        '                            'UNB-reference from original message
        '                            If Not EmptyString((oPayment.REF_Bank1)) Then
        '                                WriteRFFSegment("DM", CheckForValidCharacters(Trim(oPayment.REF_Bank1), True, True, True, True, " /-?:().,'+"))
        '                            End If
        '                            'LIN-reference from original message
        '                            If Not EmptyString((oPayment.REF_Bank2)) Then
        '                                WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.REF_Bank2), True, True, True, True, " /-?:().,'+"))
        '                            End If
        '                            'SEQ-reference from original message
        '                            If Not EmptyString((oPayment.REF_Own)) Then
        '                                WriteRFFSegment("AGN", CheckForValidCharacters(Trim(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
        '                            End If

        '                            '6-SEQ
        '                            WriteSEQSegment()
        '                            '6-GIS
        '                            'To get the correct code, pass the oPayment.Statuscode to a function
        '                            sTemp = oPayment.StatusCode
        '                            sTemp = VismaFokusConvertBANSTACode(sTemp)

        '                            WriteSegment("GIS+" & sTemp)
        '                            '6-FTX
        '                            If Not EmptyString((oPayment.StatusText)) Then
        '                                WriteFTXSegment("AAO", (oPayment.StatusText))
        '                            End If

        '                            oPayment.Exported = True

        '                        Next oPayment

        '                    Next oBatch

        '                    '04.09.2008 - Moved from beneath
        '                    WriteSegment("CNT+LI:" & Trim(Str(lLINCounter)))
        '                    WriteUNTSegment()
        '                Next oBabelFile
        '                'Old code
        '                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
        '                'WriteUNTSegment
        '                WriteUNZSegment(sUNBReference)

        '            Case "CONTRL"

        '                If sSpecial = "VISMA_FOKUS" Then
        '                    bNewMessage = True

        '                    lUNHCounter = 0


        '                    sOldAccountNo = ""
        '                    bSequenceNoFound = False

        '                    For Each oBabelFile In oBabelFiles

        '                        sUNBSegment = oBabelFile.UNBSegmentet
        '                        sUNHSegment = oBabelFile.UNHSegmentet

        '                        For Each oBatch In oBabelFile.Batches

        '                            sUCISegment = oBatch.UCIString


        '                            If Not bUNBWritten Then
        '                                iPos1 = 1
        '                                iPos2 = 1
        '                                sTemp = ""
        '                                For lCounter = 1 To 5
        '                                    iPos1 = iPos2
        '                                    iPos2 = iPos2 + 1
        '                                    iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
        '                                Next lCounter
        '                                sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
        '                                'If EmptyString(oBabelFile.EDI_MessageNo) Then
        '                                'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
        '                                ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
        '                                If Not EmptyString(sCompanyNo) Then
        '                                    sIdentReceiver = Trim(sCompanyNo)
        '                                ElseIf sSpecial = "VISMA_FOKUS" Then
        '                                    sIdentReceiver = "1438086569"
        '                                ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
        '                                    sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
        '                                Else
        '                                    '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                                    Err.Raise(15121, "CreateEDIFile", LRS(15121))
        '                                End If
        '                                'QUESTION
        '                                WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++CONTRL")
        '                                bUNBWritten = True

        '                                lSegmentCounter = 0

        '                                lUNHCounter = lUNHCounter + 1
        '                                WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CONTRL:3:1:UN")
        '                            End If

        '                            iPos1 = 1
        '                            iPos2 = 1
        '                            sTemp = ""
        '                            For lCounter = 1 To 4
        '                                iPos1 = iPos2
        '                                iPos2 = iPos2 + 1
        '                                iPos2 = InStr(iPos2, sUCISegment, "+", CompareMethod.Text)
        '                                If lCounter = 2 Then
        '                                    sOriginalUNBReference = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                    sOriginalUNBReference = Mid(sOriginalUNBReference, 2) 'For Visma_Fokus remove added information to the UNB reference
        '                                    sOriginalUNBReference = Left(sOriginalUNBReference, Len(sOriginalUNBReference) - 6)
        '                                ElseIf lCounter = 3 Then  'Original sender
        '                                    sIdentSender = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                    If InStr(1, sIdentSender, ":") > 0 Then
        '                                        sIdentSender = Left(sIdentSender, InStr(1, sIdentSender, ":") - 1)
        '                                    End If
        '                                ElseIf lCounter = 4 Then  'Original receiver
        '                                    sIdentReceiver = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                    sIdentReceiver = "00810506482" 'Always for Visma
        '                                End If
        '                            Next lCounter
        '                            If sSpecial = "VISMA_FOKUS" Then
        '                                sIdentSender = "1438086569"
        '                            End If
        '                            If bAllUNHRejected Then
        '                                WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & "+4")
        '                            Else
        '                                WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & "+7")
        '                            End If


        '                            For Each oPayment In oBatch.Payments
        '                                oPayment.Exported = True
        '                            Next oPayment

        '                        Next oBatch

        '                    Next oBabelFile

        '                    WriteUNZSegment(sUNBReference)

        '                Else
        '                    'This message is not complete. We lack the usage of UCM

        '                    bUNBWritten = False
        '                    bNewMessage = True

        '                    lUNHCounter = 0

        '                    sOldAccountNo = ""
        '                    bSequenceNoFound = False

        '                    For Each oBabelFile In oBabelFiles

        '                        sUNBSegment = oBabelFile.UNBSegmentet
        '                        sUNHSegment = oBabelFile.UNHSegmentet

        '                        If Not bUNBWritten Then
        '                            iPos1 = 1
        '                            iPos2 = 1
        '                            sTemp = ""
        '                            For lCounter = 1 To 5
        '                                iPos1 = iPos2
        '                                iPos2 = iPos2 + 1
        '                                iPos2 = InStr(iPos2, sUNBSegment, "+", CompareMethod.Text)
        '                            Next lCounter
        '                            sTemp = Mid(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                            sUNBReference = Mid(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", CompareMethod.Text) - iPos2 - 1)
        '                            'If EmptyString(oBabelFile.EDI_MessageNo) Then
        '                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
        '                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
        '                            If Not EmptyString(sCompanyNo) Then
        '                                sIdentReceiver = Trim(sCompanyNo)
        '                            ElseIf Not EmptyString((oBabelFile.IDENT_Receiver)) Then
        '                                sIdentReceiver = Trim(oBabelFile.IDENT_Receiver)
        '                            Else
        '                                '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
        '                                Err.Raise(15121, "CreateEDIFile", LRS(15121))
        '                            End If
        '                            'QUESTION
        '                            WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++CONTRL")
        '                            bUNBWritten = True
        '                        End If

        '                        lSegmentCounter = 0

        '                        lUNHCounter = lUNHCounter + 1
        '                        WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CONTRL:3:1:UN")
        '                        'To create the unique message_ID use the total amount

        '                        For Each oBatch In oBabelFile.Batches

        '                            'This should be rewritten when we get a new customer other than Visma.
        '                            sUCISegment = oBatch.UCIString
        '                            iPos1 = 1
        '                            iPos2 = 1
        '                            sTemp = ""
        '                            For lCounter = 1 To 4
        '                                iPos1 = iPos2
        '                                iPos2 = iPos2 + 1
        '                                iPos2 = InStr(iPos2, sUCISegment, "+", CompareMethod.Text)
        '                                If lCounter = 2 Then
        '                                    sOriginalUNBReference = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                ElseIf lCounter = 3 Then  'Original sender
        '                                    sIdentSender = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                    If InStr(1, sIdentSender, ":") > 0 Then
        '                                        sIdentSender = Left(sIdentSender, InStr(1, sIdentSender, ":") - 1)
        '                                    End If
        '                                ElseIf lCounter = 4 Then  'Original receiver
        '                                    sIdentReceiver = Mid(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
        '                                End If
        '                            Next lCounter
        '                            WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & "+" & oBabelFile.StatusCode)
        '                            For Each oPayment In oBatch.Payments
        '                                oPayment.Exported = True
        '                            Next oPayment

        '                        Next oBatch

        '                        WriteUNTSegment()

        '                    Next oBabelFile

        '                    WriteUNZSegment(sUNBReference)
        '                End If

        '        End Select

        '        'To write the rest of the file
        '        WriteSegment("", True)

        '        If Not oGroupPayment Is Nothing Then
        '            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '            oGroupPayment = Nothing
        '        End If

        '        On Error GoTo 0
        '        Exit Function

        'ERR_CreateSEB_SE_EDIFile:

        '        If Not oGroupPayment Is Nothing Then
        '            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '            oGroupPayment = Nothing
        '        End If

        '        Err.Raise(Err.Number, "CreateDnBNOR_EDIFile", Err.Description)

    End Function
    Private Function CreateBBSEDIFile(ByRef sCompanyNo As String, ByRef sFormat As String) As Boolean
        Dim bx As Boolean
        Dim sUNBReference As String
        Dim sIdentSender As String
        Dim sIdentReceiver As String
        'Dim aFreetextArray() As String
        Dim sStatebankText, sLocalFreetext, sStatebankCode As String
        Dim sLocalREF_Own As String
        Dim oFreeText As vbBabel.Freetext
        Dim lCounter, lArrayCounter As Integer
        Dim lCounter2 As Integer
        Dim lMaxLengthOfFreetext As Integer
        Dim bThisIsDNBNOROverforselInstruction As Boolean
        Dim bPaymentFromOtherBank As Boolean
        Dim bTheInformationIsStructured As Boolean
        Dim sOldAccountNo, sI_SWIFTAddr As String
        Dim sLocalPayCode As String
        Dim bWriteUNH, bUNAWritten, bUNBWritten, bUNHWritten As Boolean
        Dim bWriteUNTForEachBabelFile As Boolean
        Dim bThisIsKID As Boolean
        Dim bNewMessage, bNewLIN As Boolean
        Dim lPaymentCounter As Integer
        Dim sAdditionalNo, sEDIMessageNo As String
        Dim sBGMReference As String
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim sCurrencyToUse As String
        Dim sPaymentReference As String
        Dim nSequenceNo As Double
        Dim bSequenceNoFound As Boolean
        Dim bStructured As Boolean

        Dim dLINAmount As Double
        Dim sLINCurrency As String
        Dim nDummy As Double
        Dim sTemp As String
        Dim sTemp2 As String

        On Error GoTo ERR_CreateBBSEDIFile

        Select Case sFormat

            Case "CREMUL"

                lUNHCounter = 0

                bUNAWritten = False
                bUNBWritten = False
                bNewMessage = True

                For Each oBabelFile In oBabelFiles

                    bUNHWritten = False

                    If oBabelFile.BabelfileContainsPaymentsAccordingToQualifiers(iFilenameInNo, iFilesetupOut, False, sClientNoToBeExported, True, "") Then

                        If Not bUNAWritten Then
                            sElementSep = ":"
                            sSegmentSep = "'"
                            sGroupSep = "+"
                            sDecimalSep = ","
                            sEscape = "?"
                            sSegmentLen = CStr(3)
                            WriteSegment("UNA" & sElementSep & sGroupSep & sDecimalSep & sEscape & " ")
                            bUNAWritten = True
                        End If

                        If Not bUNBWritten Then
                            'Used for BI - an alternative is to use
                            'If sSpecial = "BI" then

                            If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL And Not EmptyString((oBabelFile.UNBSegmentet)) Then
                                WriteSegment(Left(oBabelFile.UNBSegmentet, Len(oBabelFile.UNBSegmentet) - 1)) '-1 to remove the '
                                'Store the UNB reference. To be used in UNZ
                                sUNBReference = Right(oBabelFile.UNBSegmentet, Len(oBabelFile.UNBSegmentet) - InStrRev(oBabelFile.UNBSegmentet, "+", , CompareMethod.Text))
                                sUNBReference = Left(sUNBReference, Len(sUNBReference) - 1)
                            ElseIf EmptyString(oBabelFile.EDI_MessageNo) Then
                                sUNBReference = CreateBGMReference(oBabelFile)
                            Else
                                If sSpecial = "GJENSIDIGE_FDC" Then
                                    'Only digits (Max 14 characters like in Sweden????)
                                    sTemp = oBabelFile.EDI_MessageNo
                                    sTemp2 = ""
                                    For lCounter = Len(sTemp) To 1 Step -1
                                        If vbIsNumeric(Mid(sTemp, lCounter, 1), "0123456789") Then
                                            sTemp2 = Mid(sTemp, lCounter, 1) & sTemp2
                                        End If
                                        If Len(sTemp2) > 4 Then
                                            Exit For
                                        End If
                                    Next lCounter
                                    sUNBReference = sTemp2
                                    sTemp = ""
                                    sTemp2 = ""
                                Else
                                    sUNBReference = Left(Trim(oBabelFile.EDI_MessageNo), 35)
                                End If
                            End If
                            'QUESTION
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            If Not EmptyString(sCompanyNo) Then
                                sIdentReceiver = Trim(sCompanyNo)
                            ElseIf Not EmptyString((oBabelFile.IDENT_Sender)) Then
                                sIdentReceiver = oBabelFile.IDENT_Sender
                            Else
                                '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                                Err.Raise(15121, "CreateEDIFile", LRS(15121))
                            End If
                            WriteSegment("UNB+UNOC:3+00008080+" & sIdentReceiver & "+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "++++1")
                            bUNBWritten = True
                        End If

                        If Not bUNHWritten Then
                            lSegmentCounter = 0
                            lLINCounter = 0 '04.09.2008, moved from above
                            'Used for BI - an alternative is to use
                            'If sSpecial = "HANDELSHOYSKOLENBI" then
                            lUNHCounter = lUNHCounter + 1

                            'If oBabelFile.ImportFormat = FileType.CREMUL And Not EmptyString(oBabelFile.UNHSegmentet) Then
                            '    WriteSegment Left$(oBabelFile.UNHSegmentet, Len(oBabelFile.UNHSegmentet) - 1)  '-1 to remove the '
                            'Else
                            WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CREMUL:D:96A:UN")
                            'End If

                            If EmptyString((oBabelFile.EDI_MessageNo)) Then
                                'To create the unique message_ID use the total amount
                                sBGMReference = CreateMessageReference(oBabelFile)
                            Else
                                If sSpecial = "GJENSIDIGE_FDC" Then
                                    If Not EmptyString(oBabelFile.EDI_MessageNo) Then
                                        sBGMReference = Trim(oBabelFile.EDI_MessageNo)
                                    Else
                                        sBGMReference = Right(sUNBReference & oBabelFile.Index.ToString, 5)
                                    End If
                                Else
                                    sBGMReference = oBabelFile.EDI_MessageNo
                                End If
                            End If

                            WriteSegment("BGM+435+" & sBGMReference)
                            If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                                WriteDTMSegment("137", DateToString(Now), "102")
                            Else
                                WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                            End If
                            WriteSegment("NAD+MR+" & oBabelFile.IDENT_Receiver)
                            bUNHWritten = True

                            'QUESTION
                            'If EmptyString(oBabelFile.IDENT_Sender) Then
                            '    WriteSegment "NAD+MS+BabelBank"
                            'Else
                            '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                            'End If
                        End If

                        For Each oBatch In oBabelFile.Batches

                            If oBatch.BatchContainsPaymentsAccordingToQualifiers(iFilesetupOut, True, sClientNoToBeExported, nDummy, True, "") Then

                                bNewLIN = True

                                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL And Not EmptyString(sClientNoToBeExported) Then
                                    'BI Some payments shall be removed. We must calculate a new LIN-amount
                                    ' - May use
                                    'If sSpecial = "BI"
                                    dLINAmount = 0
                                    sLINCurrency = ""
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.PaymentIsAccordingToQualifiers(iFilenameInNo, True, sClientNoToBeExported, True) Then
                                            dLINAmount = dLINAmount + oPayment.MON_InvoiceAmount
                                            If Len(oPayment.MON_InvoiceCurrency) = 3 Then
                                                sLINCurrency = oPayment.MON_InvoiceCurrency
                                            ElseIf Len(oPayment.MON_TransferCurrency) = 3 Then
                                                sLINCurrency = oPayment.MON_TransferCurrency
                                            End If
                                        End If
                                    Next oPayment
                                Else
                                    'It seems logic to use the batchleve as imported
                                    dLINAmount = oBatch.MON_InvoiceAmount
                                End If

                                For Each oPayment In oBatch.Payments

                                    If oPayment.PaymentIsAccordingToQualifiers(iFilesetupOut, True, sClientNoToBeExported, True) Then

                                        If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                            'bNewPayments = True Don't need a new payments when the debit account is changed
                                            If oBabelFile.VB_ProfileInUse Then
                                                bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                            End If
                                            sOldAccountNo = Trim(oPayment.I_Account)
                                        End If

                                        '                                If IsPaymentDomestic(oPayment, sI_SWIFTAddr, False, , True) Then
                                        '                                    oPayment.PayType = "D"
                                        '                                Else
                                        '                                    oPayment.PayType = "I"
                                        '                                End If

                                        If bNewLIN Then
                                            lSEQCounter = 0
                                            '4-LIN
                                            WriteLINSegment()

                                            '4-DTM (Date is stored in the Payment-object
                                            If oPayment.DATE_Payment = "19900101" Then
                                                If oPayment.DATE_Value = "19900101" Then
                                                    WriteDTMSegment("202", VB6.Format(Now, "YYYYMMDD"), "102")
                                                Else
                                                    WriteDTMSegment("202", (oPayment.DATE_Value), "102")
                                                End If
                                            Else
                                                WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                            End If
                                            If oPayment.DATE_Value = "19900101" Then
                                                If oPayment.DATE_Payment = "19900101" Then
                                                    WriteDTMSegment("209", VB6.Format(Now, "YYYYMMDD"), "102")
                                                Else
                                                    WriteDTMSegment("209", (oPayment.DATE_Payment), "102")
                                                End If
                                            Else
                                                WriteDTMSegment("209", (oPayment.DATE_Value), "102")
                                            End If

                                            '4-BUS
                                            WriteBUSSegment((oPayment.PayType), bbGetPayCode((oPayment.PayCode), "CREMUL"), ":25:124")

                                            '4-MOA
                                            WriteMOASegment("60", dLINAmount, sLINCurrency)

                                            '5-RFF
                                            If Len(oBatch.REF_Bank) > 0 Then
                                                WriteRFFSegment("ACK", CheckForValidCharacters(Trim(oBatch.REF_Bank), True, True, True, True, " /-?:().,'+"))
                                            Else
                                                WriteRFFSegment("ACK", sUNBReference & PadLeft(Trim(Str(lUNHCounter)), 4, "0") & PadLeft(Trim(Str(lLINCounter)), 8, "0"), 35)
                                            End If

                                            '5-DTM The date the reference is created (not a part of the BB collection, use valuedate
                                            If oPayment.DATE_Value <> "19900101" Then
                                                WriteDTMSegment("171", (oPayment.DATE_Value), "102")
                                            Else
                                                'Don't write a date
                                            End If

                                            '6-FII
                                            If EmptyString((oPayment.I_Account)) Then
                                                '"15136: Receivers acount are not stated on the payment. Can't create a valid CREMUL-file."
                                                Err.Raise(15136, "CreateEDIFile", LRS(15136) & _
                                                        vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                            Else
                                                WriteSegment("FII+BF+" & Trim(oPayment.I_Account))
                                            End If

                                            bNewLIN = False

                                        End If 'If bNewLIN Then

                                        'Start writing SEQ information
                                        '10-SEQ
                                        WriteSEQSegment()

                                        '10-DTM
                                        If oPayment.DATE_Value <> "19900101" Then
                                            WriteDTMSegment("203", (oPayment.DATE_Value), "102")
                                        Else
                                            'Don't write a date
                                        End If

                                        '10-FII
                                        WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , "OR")

                                        '11-RFF
                                        If Not EmptyString((oPayment.REF_Bank2)) Then
                                            WriteRFFSegment("AEK", CheckForValidCharacters(Trim(oPayment.REF_Bank2), True, True, True, True, " /-?:().,'+"))
                                        End If
                                        If Not EmptyString((oPayment.REF_Bank1)) Then
                                            WriteRFFSegment("ACD", CheckForValidCharacters(Trim(oPayment.REF_Bank1), True, True, True, True, " /-?:().,'+"))
                                        Else
                                            If Not EmptyString((oBatch.REF_Bank)) Then
                                                WriteRFFSegment("AEK", oBatch.REF_Bank & PadLeft(Trim(Str(lSEQCounter)), 4, "0"), 35)
                                            Else
                                                '15137: No bankreference is stated."
                                                Err.Raise(15137, "CreateEDIFile", LRS(15137) & vbCrLf & _
                                                        vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        End If

                                        '13-MOA
                                        WriteMOASegment("98", (oPayment.MON_InvoiceAmount), "", True)

                                        '14-NAD
                                        WriteNADSegment(True, True, "PL")
                                        If Not EmptyString((oPayment.I_Name)) Then
                                            WriteNADSegment(False, True, "BE")
                                        End If

                                        '15-INP
                                        If Not EmptyString((oPayment.Text_I_Statement)) Then
                                            WriteSegment("INP+BF+2:SI")
                                            '15-FTX
                                            WriteFTXSegment("AAG", (oPayment.Text_I_Statement))
                                        End If

                                        bStructured = False

                                        '04.04.2013 - Added IsAutogiro and not EmptyString for Gjensidige GPF to check that it is structured
                                        If (IsOCR((oPayment.PayCode)) Or IsAutogiro(oPayment.PayCode)) Then
                                            If oPayment.Invoices.Count > 0 Then
                                                If Not EmptyString(oPayment.Invoices.Item(1).Unique_Id) Then
                                                    bStructured = True
                                                End If
                                            End If
                                        End If

                                        If bStructured Then
                                            For Each oInvoice In oPayment.Invoices
                                                '20-PRC
                                                WriteSegment("PRC+8")

                                                If Not EmptyString((oInvoice.Unique_Id)) Then
                                                    '21-DOC
                                                    WriteDOCSegment("999", (oInvoice.Unique_Id))
                                                Else
                                                    '15138: No KID stated on the payment even if it is marked as a KID payment."
                                                    Err.Raise(15138, "CreateEDIFile", LRS(15138) & _
                                                        vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                                End If

                                                '21-MOA
                                                WriteMOASegment("12", (oInvoice.MON_InvoiceAmount), "", True)

                                            Next oInvoice

                                            '27-GIS
                                            WriteSegment("GIS+37")

                                        Else
                                            'PRC+11FTX+PMD+++faktura nr. 556928  -- 1 %'
                                            '20-PRC
                                            WriteSegment("PRC+11")

                                            'FTX
                                            sLocalFreetext = ""
                                            For Each oInvoice In oPayment.Invoices
                                                For Each oFreeText In oInvoice.Freetexts
                                                    If oFreeText.Qualifier = CDbl("1") Then
                                                        sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                                    End If
                                                Next oFreeText
                                            Next oInvoice

                                            sLocalFreetext = RTrim(sLocalFreetext)

                                            WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))

                                            '15139: BabelBank is not adaptes to this paymenttype."
                                            'Err.Raise(15139, "CreateEDIFile", LRS(15139) & _
                                            '    vbCrLf & LRS(35071, oPayment.E_Name) & _
                                            '    vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                            '    vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                            '    vbCrLf & "Type: " & Trim(oPayment.PayCode) & _
                                            '    vbCrLf & vbCrLf & LRS(10016))
                                        End If

                                        oPayment.Exported = True

                                    End If 'If oPayment.PaymentIsAccordingToQualifiers

                                Next oPayment

                            End If 'If oBatch.BatchContainsPaymentsAccordingToQualifiers

                        Next oBatch

                        WriteSegment("CNT+LI:" & Trim(Str(lLINCounter)))
                        WriteUNTSegment()
                    End If 'If oBabel.BabelfileContainsPaymentsAccordingToQualifiers
                Next oBabelFile
                WriteUNZSegment(sUNBReference)

                'WriteSegment "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

            Case "PAYMUL"

                'NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNB
                'COPIED FROM THE INPS FUNCTION - Not developed

                'bx = mrEDISettingsExport.InitiateSpecialSegments
                'mrEDISettingsExport.SetCurrentElement ("//HEADER/ATTRIBUTES")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                WriteSegment("UNA:+,? ")
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                oGroupPayment = New vbBabel.GroupPayments  '23.05.2017 CreateObject ("vbBabel.GroupPayments")
                oGroupPayment.GroupCrossBorderPayments = False
                oGroupPayment.SetGroupingParameters(True, True, True, True, False, True)

                'Because of the grouping we do, we have set the PayType before we
                ' start the writing of the file.

                sOldAccountNo = ""
                bSequenceNoFound = False
                For Each oBabelFile In oBabelFiles

                    If sSpecial = "DNBNORFINANS" And Not bSequenceNoFound Then
                        'Only for the first batch if we have one sequenceseries!
                        nSequenceNo = oBabelFile.VB_Profile.FileSetups(iFilesetupOut).SeqNoTotal
                        If nSequenceNo > 9999 Then
                            nSequenceNo = 0
                        End If
                        bSequenceNoFound = True
                    End If

                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                'bNewPayments = True Don't need a new payments when the debit account is changed
                                If oBabelFile.VB_ProfileInUse Then
                                    bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                End If
                                sOldAccountNo = Trim(oPayment.I_Account)
                            End If

                            If IsPaymentDomestic(oPayment, sI_SWIFTAddr, False, , True) Then
                                oPayment.PayType = "D"
                            Else
                                oPayment.PayType = "I"
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
                sOldAccountNo = ""

                For Each oBabelFile In oBabelFiles

                    If Not bUNBWritten Then
                        If EmptyString((oBabelFile.EDI_MessageNo)) Then
                            sUNBReference = "BAB" & VB6.Format(Now, "MMDDHHMMSS")
                        Else
                            sUNBReference = oBabelFile.EDI_MessageNo
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentSender = Trim(sCompanyNo)
                        Else
                            '15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                            Err.Raise(15121, "CreateEDIFile", LRS(15121))
                        End If
                        'QUESTION
                        WriteSegment("UNB+UNOC:3+" & sIdentSender & "+DNBANOKK+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM") & "+" & sUNBReference & "++++1")
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above
                    sBGMReference = CreateMessageReference(oBabelFile)

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN:INPS05")
                    'To create the unique message_ID use the total amount

                    WriteSegment("BGM+452+" & sBGMReference)
                    If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now), "102")
                    Else
                        WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                    End If
                    WriteSegment("FII+MR++DNBANOKK:25:5")
                    'QUESTION
                    'If EmptyString(oBabelFile.IDENT_Sender) Then
                    '    WriteSegment "NAD+MS+BabelBank"
                    'Else
                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                    'End If

                    For Each oBatch In oBabelFile.Batches
                        For lCounter2 = 1 To oBatch.Payments.Count
                            bNewLIN = True

                            oGroupPayment.Batch = oBatch
                            oGroupPayment.MarkIdenticalPayments()
                            'Group the payments inside a batch and create a UNH per group
                            lPaymentCounter = 0
                            For Each oPayment In oBatch.Payments

                                If oPayment.SpecialMark And Not oPayment.Exported Then
                                    lPaymentCounter = lPaymentCounter + 1
                                    If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                        'bNewPayments = True Don't need a new payments when the debit account is changed
                                        If oBabelFile.VB_ProfileInUse Then
                                            bx = FindAccSWIFTAddr((oPayment.I_Account), oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                        End If
                                        sOldAccountNo = Trim(oPayment.I_Account)
                                    End If

                                    'Find country for receiver if not stated
                                    If EmptyString((oPayment.I_CountryCode)) Then
                                        If Not EmptyString((oPayment.BANK_I_SWIFTCode)) Then
                                            oPayment.I_CountryCode = Mid(oPayment.BANK_I_SWIFTCode, 5, 2)
                                        Else
                                            oPayment.I_CountryCode = Mid(sI_SWIFTAddr, 5, 2)
                                            'If debit account and receiver is in the same country it must be
                                            '  a domestic payment
                                            'If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
                                            '    oPayment.PayType = "I"
                                            'Else
                                            '    oPayment.PayType = "D"
                                            'End If
                                        End If
                                    End If

                                    oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
                                    If Left(oPayment.BANK_I_SWIFTCode, 4) <> "DNBA" Then
                                        '24.11.2008 - Added the possibility to pay from accounts outside DnBNOR
                                        bPaymentFromOtherBank = True
                                    Else
                                        bPaymentFromOtherBank = False
                                    End If

                                    'lArrayCounter = 0

                                    'Not validated against valid chars for SEB
                                    If sSpecial = "DNBNORFINANS" Then
                                        RetrieveZipAndCityFromTheAddress(oPayment, False)
                                    End If
                                    oPayment.E_Name = CheckForValidCharacters((oPayment.E_Name), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr1 = CheckForValidCharacters((oPayment.E_Adr1), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr2 = CheckForValidCharacters((oPayment.E_Adr2), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr3 = CheckForValidCharacters((oPayment.E_Adr3), True, True, True, True, " /-?:().,'+")
                                    oPayment.E_City = CheckForValidCharacters((oPayment.E_City), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Name = CheckForValidCharacters((oPayment.BANK_Name), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr1 = CheckForValidCharacters((oPayment.BANK_Adr1), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr2 = CheckForValidCharacters((oPayment.BANK_Adr2), True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr3 = CheckForValidCharacters((oPayment.BANK_Adr3), True, True, True, True, " /-?:().,'+")
                                    oPayment.I_Name = CheckForValidCharacters((oPayment.I_Name), True, True, True, True, " /-?:().,'+")


                                    If bPaymentFromOtherBank Then
                                        lMaxLengthOfFreetext = 140
                                    Else
                                        Select Case oPayment.I_CountryCode

                                            'Danish account transfer: 41�35 characters.
                                            'Danish transfer via PBS: 41�35 characters.
                                            'Danish and foreign cheque: 14�35 (7�70) characters.
                                            'Swedish payments: 30�35 characters.
                                            'Finnish payments: 12�35 characters.
                                            'Norwegian payments: 12�35 characters.

                                            '                        Case "DE"
                                            '                            If oPayment.PayType = "I" Then
                                            '                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            '                                lMaxLengthOfFreetext = 140
                                            '                            Else
                                            '                                lMaxLengthOfFreetext = 162
                                            '                            End If
                                            Case "DK"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                                    lMaxLengthOfFreetext = 1435
                                                ElseIf oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1435
                                                End If
                                            Case "FI"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "NO"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "SE"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1050
                                                End If
                                                '                        Case "US"
                                                '                            If oPayment.PayType = "I" Then
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            Else
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            End If

                                            Case Else
                                                bThisIsDNBNOROverforselInstruction = True
                                                lMaxLengthOfFreetext = 140
                                        End Select
                                    End If

                                    If lMaxLengthOfFreetext = 0 Then
                                        lMaxLengthOfFreetext = 140
                                    End If

                                    'Create a new LIN?
                                    If bNewLIN Then
                                        bNewLIN = False
                                        lSEQCounter = 0
                                        '4-LIN
                                        WriteLINSegment()
                                        '4-DTM
                                        '25.11.2008 - removed the IF
                                        'If bPaymentFromOtherBank Then

                                        'Else
                                        Select Case oPayment.I_CountryCode
                                            Case Else
                                                If oPayment.DATE_Payment < VB6.Format(Now, "YYYYMMDD") Then
                                                    WriteDTMSegment("203", VB6.Format(Now, "YYYYMMDD"), "102")
                                                Else
                                                    WriteDTMSegment("203", (oPayment.DATE_Payment), "102")
                                                End If
                                        End Select
                                        'End If
                                        '4-RFF
                                        If Len(oBatch.REF_Own) > 0 Then
                                            '20.02.2008 - This is done for Gjensidige to do the reference at the LIN-level
                                            ' unique.
                                            'oBatch.REF_Own = oBatch.REF_Own & "-" & Trim$(Str(lPaymentCounter))
                                            WriteRFFSegment("AEK", CheckForValidCharacters(Trim(oBatch.REF_Own) & "-" & Trim(Str(lPaymentCounter)), True, True, True, True, " /-?:().,'+"))
                                        Else
                                            WriteRFFSegment("AEK", sUNBReference & PadLeft(Trim(Str(lUNHCounter)), 4, "0") & PadLeft(Trim(Str(lLINCounter)), 8, "0"), 35)
                                        End If
                                        '4-BUS
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            WriteSegment("BUS++TRF")
                                        Else
                                            WriteBUSSegment((oPayment.PayType))
                                        End If
                                        '4-FCA
                                        'If oPayment.PayType = "I" Then
                                        WriteFCASegment((oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad), True)
                                        'End If
                                        '5-MOA
                                        If Len(Trim(oPayment.MON_TransferCurrency)) <> 3 Then
                                            If EmptyString((oPayment.MON_TransferCurrency)) Then
                                                oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency
                                                sCurrencyToUse = Trim(oPayment.MON_TransferCurrency)
                                            Else
                                                '15130: Wrong transfercurrency stated on the payment.
                                                Err.Raise(15130, "CreateEDIFile", LRS(15130) & _
                                                    vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                    vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                    vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                    vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        Else
                                            sCurrencyToUse = Trim(oPayment.MON_TransferCurrency)
                                        End If

                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                            If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                                WriteMOASegment("9", (oPayment.MON_InvoiceAmount), sCurrencyToUse)
                                            Else
                                                WriteMOASegment("9", oGroupPayment.MON_InvoiceAmount, sCurrencyToUse)
                                            End If
                                            'WriteMOASegment "9", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency
                                        Else
                                            WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
                                            '5-CUX AND 5-RFF
                                            If oPayment.ERA_ExchRateAgreed = 0 Then
                                                If oPayment.FRW_ForwardContractRate = 0 Then
                                                    'No rates
                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency))
                                                    'Do not use 5-RFF
                                                Else
                                                    'Raise an error because this code is not tested
                                                    '"The use of 'Forward contract rate' is not yet implemented in BabelBank for this format."
                                                    Err.Raise(15140, "CreateEDIFile", LRS(15140) & _
                                                        vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & LRS(35070, ConvertFromAmountToString((oPayment.FRW_ForwardContractRate), , ",")) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                                    'Code to be used will be something like this
                                                    WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.FRW_ForwardContractRate), , ","))
                                                    If Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                                        WriteRFFSegment("ACX", Trim(oPayment.FRW_ForwardContractNo))
                                                    Else
                                                        '15132: No forward contract number is stated even though a forward exchange rate is stated.
                                                        Err.Raise(15132, "CreateEDIFile", LRS(15132) & _
                                                            vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                            vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                            vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                            vbCrLf & LRS(35070, ConvertFromAmountToString((oPayment.FRW_ForwardContractRate), , ",")) & _
                                                            vbCrLf & vbCrLf & LRS(10016))
                                                    End If
                                                End If
                                            Else
                                                WriteCUXSegment((oPayment.MON_InvoiceCurrency), (oPayment.MON_TransferCurrency), ConvertFromAmountToString((oPayment.ERA_ExchRateAgreed), , ","))
                                                If Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                                    WriteRFFSegment("FX", Trim(oPayment.ERA_DealMadeWith))
                                                Else
                                                    '15133: No foreign exchange contract number is stated even though an agreed exchangerate is stated.
                                                    Err.Raise(15133, "CreateEDIFile", LRS(15133) & _
                                                        vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & LRS(35070, ConvertFromAmountToString(CDbl(oPayment.ERA_DealMadeWith), , ",")) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                                End If
                                            End If
                                        End If
                                        '6-FII
                                        'Changed 08.04.05 - Kjell
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            WriteSegment("FII+RB+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")
                                        Else
                                            WriteSegment("FII+OR+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")
                                        End If

                                    End If 'if bNewLIN

                                    If oPayment.I_CountryCode = "NO" And Trim(oPayment.PayCode) = "301" Then
                                        bThisIsKID = True
                                    Else
                                        bThisIsKID = False
                                        sLocalFreetext = ""
                                        sLocalREF_Own = ""
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier = CDbl("1") Then
                                                    sLocalFreetext = sLocalFreetext & RTrim(oFreeText.Text) & " "
                                                End If
                                            Next oFreeText
                                        Next oInvoice
                                        If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                            'sLocalFreetext = "SEE SEPERATE LETTER."
                                        End If
                                        sLocalFreetext = RTrim(Left(sLocalFreetext, lMaxLengthOfFreetext))
                                    End If
                                    If oPayment.Invoices.Count > 0 Then
                                        sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                    End If

                                    '11-SEQ
                                    WriteSEQSegment()
                                    '11-MOA
                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                        'NOMORE
                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                        WriteMOASegment("9", (oPayment.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                    Else
                                        'ErrorWritingEDI 1, "CreateEDIFile", "Difference between currency of the invoice and the currency of the payment" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                        'NOMORE
                                        'WriteMOASegment "9", Val(aFreetextArray(1, lArrayCounter)), oPayment.MON_InvoiceCurrency
                                        WriteMOASegment("57", (oPayment.MON_InvoiceAmount), "")
                                    End If
                                    '11-DTM
                                    '11-RFF
                                    'Special case for DnBNORFinans
                                    'oPayment.REF_Own is always DEBITOR UTBET., therefore the ownreference
                                    ' from the first Invoice is used

                                    If Len(Trim(oPayment.REF_Own)) > 0 And Not sSpecial = "DNBNORFINANS" Then
                                        WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                                    Else
                                        If oPayment.Invoices.Count > 0 Then
                                            If Len(Trim(oPayment.Invoices.Item(1).REF_Own)) > 0 Then
                                                If sSpecial = "DNBNORFINANS" Then
                                                    nSequenceNo = nSequenceNo + 1
                                                    If nSequenceNo > 9999 Then
                                                        nSequenceNo = 0
                                                    End If
                                                    sPaymentReference = ""
                                                    sPaymentReference = sPaymentReference & VB6.Format(Now, "YYMMDD") & PadLeft(Trim(Str(nSequenceNo)), 4, "0")
                                                    '26.09.2008 - Changed to create
                                                    'Old code
                                                    'WriteRFFSegment "CR", CheckForValidCharacters(Trim$(oPayment.Invoices.Item(1).REF_Own) & "-" &VB6.Format(Now(), "YYYYMMDDHHMMSS") & "-" & oPayment.MON_InvoiceAmount, True, True, True, True, " /-?:().,'+")
                                                    'New code
                                                    WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.Invoices.Item(1).REF_Own) & "-" & sPaymentReference & "-" & oPayment.MON_InvoiceAmount & "-" & Trim(oPayment.MON_InvoiceCurrency), True, True, True, True, " /-?:().,'+"))
                                                Else
                                                    WriteRFFSegment("CR", CheckForValidCharacters(Trim(oPayment.Invoices.Item(1).REF_Own), True, True, True, True, " /-?:().,'+"))
                                                End If
                                            Else
                                                '15134: No customer reference (own reference) is stated.
                                                Err.Raise(15134, "CreateEDIFile", LRS(15134) & _
                                                    vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                    vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                    vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                    vbCrLf & vbCrLf & LRS(10016))
                                            End If
                                        Else
                                            '15134: No customer reference (own reference) is stated.
                                            Err.Raise(15134, "CreateEDIFile", LRS(15134) & _
                                                vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                vbCrLf & vbCrLf & LRS(10016))
                                        End If
                                    End If
                                    '11-PAI
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                                            WriteSegment("PAI+::20")
                                        ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
                                            WriteSegment("PAI+::23")
                                        Else
                                            WriteSegment("PAI+::26")
                                        End If
                                    ElseIf oPayment.Priority Then
                                        WriteSegment("PAI+::ZZ")
                                    ElseIf oPayment.PayType = "D" And oPayment.I_CountryCode = "DK" Then
                                        WriteSegment("PAI+::UUA")
                                    Else
                                        'For danish payments (and maybe others, we should be more specific)
                                        'F71     FIKORT 71 Paying slip (DK)
                                        'F73     FIKORT 73 Paying slip (DK)
                                        'F75     FIKORT 75 Paying slip (DK)
                                        'G01     GIRO 01 Paying slip (DK)
                                        'G04     GIRO 04 Paying slip (DK)
                                        'G15     GIRO 15 Paying slip (DK)
                                        'ULA     Account transfer with Immediate advice (DK)
                                        'Immediate advice, letter to beneficiary (SG16-FTX)
                                        'UUA     Account transfer with Long-form advice (DK)
                                        'NO PAI-segment
                                    End If
                                    '11 - FCA - Can't be included if FCA is stated at LIN-level
                                    'If oPayment.PayType = "I" Then
                                    '    WriteFCASegment oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad
                                    'End If
                                    '12 - FII
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        'Do not use the FI-segment
                                    Else
                                        If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                            If oPayment.PayType = "I" Then
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , , vbBabel.BabelFiles.Bank.DnBNOR_INPS)
                                            Else
                                                If IsIBANNumber((oPayment.E_Account), True, False) Then
                                                    'XNET - 16.11.2010 - Changed the name of the function below
                                                    If ExtractInfoFromIBAN(oPayment, False) Then
                                                        'If ExtractAccountFromIBANN(oPayment, False) Then

                                                    End If
                                                End If
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , , vbBabel.BabelFiles.Bank.DnBNOR_INPS)
                                            End If
                                        ElseIf oPayment.PayType = "I" Then
                                            WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , , vbBabel.BabelFiles.Bank.DnBNOR_INPS)
                                        Else
                                            If oPayment.I_CountryCode = "SE" Then
                                                If sSpecial = "DNBNORFINANS" Then
                                                    'Find the correct accounttype
                                                    If Not IsIBANNumber((oPayment.E_Account), False, False) Then
                                                        If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                                            Select Case oPayment.BANK_SWIFTCode
                                                                Case "BANKGIRO"
                                                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                                                Case "NDEASESS"
                                                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro
                                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                                                                    oPayment.E_Account = Mid(oPayment.E_Account, 5)
                                                                Case Else
                                                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                                                            End Select
                                                        Else
                                                            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                                        End If
                                                    Else
                                                        'IBAN number, this will create an BANSTA
                                                        'Set it to SE_Bankgiro
                                                        oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                                    End If
                                                End If
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True)
                                            ElseIf oPayment.I_CountryCode = "DK" Or oPayment.I_CountryCode = "FI" Then
                                                'Convert from IBAN to BBANN
                                                If IsIBANNumber((oPayment.E_Account), True, False) Then
                                                    'XNET - 16.11.2010 - Changed the name of the function below
                                                    If ExtractInfoFromIBAN(oPayment, False) Then
                                                        'If ExtractAccountFromIBANN(oPayment, False) Then

                                                    End If
                                                End If
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True)
                                            Else
                                                'If oPayment.I_CountryCode = "NO" Then
                                                WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True)
                                                'End If
                                            End If
                                        End If
                                        If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Or Not EmptyString((oPayment.BANK_NameAddressCorrBank1)) Then
                                            WriteFIISegment(CorrBank, True, (oPayment.I_CountryCode), False, True)
                                        End If
                                    End If
                                    '13 - NAD
                                    bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                    WriteNADSegment(True, True)
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                                            WriteNADSegment(False, True, "RV")
                                        ElseIf oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver Then
                                            WriteNADSegment(True, True, "RV")
                                        Else
                                            WriteNADSegment(True, True, "RV")
                                        End If
                                    End If
                                    '13 - CTA
                                    'Not implemented
                                    '13 - COM
                                    'Not implemented
                                    '14 - INP
                                    '22.10.2008 - Removed the INP segment after phone from H�kon Belt in DnBNOR
                                    'It is of no use when stated like INP+3:4+1:AD, but it creates problems for DnBNOR
                                    '24.11.2008 - Added INP for Sweden after phone from H�kon Belt in DnBNOR
                                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "SE" Then
                                        WriteSegment("INP+3:4+1:AD")
                                    End If
                                    '15 - GIS
                                    If oPayment.I_CountryCode = "NO" And oPayment.PayType = "I" Then
                                        'Central Bank Reporting
                                        WriteSegment("GIS+10")
                                        sStatebankText = ""
                                        sStatebankCode = ""
                                        For Each oInvoice In oPayment.Invoices
                                            sStatebankText = oInvoice.STATEBANK_Text
                                            sStatebankCode = oInvoice.STATEBANK_Code
                                            If Not EmptyString(sStatebankText) Then
                                                Exit For
                                            End If
                                        Next oInvoice
                                        If EmptyString(sStatebankText) Then
                                            If EmptyString(sStatebankCode) Then
                                                WriteFTXSegment("REG", "Export/Import av varer", CStr(14))
                                            Else
                                                WriteFTXSegment("REG", "Export/Import av varer", sStatebankCode)
                                            End If
                                        Else
                                            If EmptyString(sStatebankCode) Then
                                                WriteFTXSegment("REG", sStatebankText, CStr(14))
                                            Else
                                                WriteFTXSegment("REG", sStatebankText, sStatebankCode)
                                            End If
                                        End If

                                    End If
                                    '15 - FTX
                                    'WriteFTXSegment "REG", CheckForValidCharacters(oInvoice.STATEBANK_Text + ???oInvoice.STATEBANK_Code
                                    '16-PRC
                                    If bThisIsKID Then
                                        For Each oInvoice In oPayment.Invoices
                                            WriteSegment("PRC+8")
                                            WriteDOCSegment("YW3", Trim(oInvoice.Unique_Id), 35)
                                            WriteMOASegment(CStr(12), (oInvoice.MON_InvoiceAmount), (oPayment.MON_InvoiceCurrency))
                                        Next oInvoice
                                        '23-GIS
                                        WriteSegment("GIS+37")
                                        '23-MOA
                                        WriteMOASegment("128", (oPayment.MON_InvoiceAmount), "")
                                    Else
                                        If Not EmptyString(sLocalFreetext) Then
                                            WriteSegment("PRC+11")
                                            '16-FTX
                                            'NOMORE
                                            'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                            WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
                                            '23-GIS
                                            WriteSegment("GIS+37")
                                        Else
                                            'No advice
                                        End If
                                    End If
                                    oPayment.Exported = True

                                    If bThisIsDNBNOROverforselInstruction Or bPaymentFromOtherBank Then
                                        'Always new LIN per payment even if it is a domestic payment
                                        Exit For
                                    End If


                                End If 'If oPayment.SpecialMark And Not oPayment.Exported Then
                            Next oPayment
                            oGroupPayment.RemoveSpecialMark()
                            If oGroupPayment.AllPaymentsMarkedAsExported Then
                                Exit For
                            End If
                        Next lCounter2
                    Next oBatch
                    '04.09.2008 - Moved from beneath
                    WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)

                If sSpecial = "DNBNORFINANS" Then
                    For Each oBabelFile In oBabelFiles
                        oBabelFile.VB_Profile.FileSetups(iFilesetupOut).SeqNoTotal = nSequenceNo
                        oBabelFile.VB_Profile.Status = 2
                        oBabelFile.VB_Profile.FileSetups(iFilesetupOut).Status = 1
                        Exit For
                    Next oBabelFile
                End If

        End Select

        'To write the rest of the file
        WriteSegment("", True)

        If Not oGroupPayment Is Nothing Then
            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oGroupPayment = Nothing
        End If

        On Error GoTo 0
        Exit Function

ERR_CreateBBSEDIFile:

        If Not oGroupPayment Is Nothing Then
            'UPGRADE_NOTE: Object oGroupPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oGroupPayment = Nothing
        End If

        Err.Raise(Err.Number, "CreateBBSEDIFile", Err.Description)

    End Function
    Private Function CreateHSBC_EDIFile(ByVal sCompanyNo As String, ByVal sFormat As String) As Boolean
        Dim bx As Boolean, sUNBReference As String
        Dim sIdentSender As String
        Dim sIdentReceiver As String
        'Dim aFreetextArray() As String
        Dim sLocalFreetext As String, sStatebankText As String, sStatebankCode As String
        Dim sLocalREF_Own As String
        Dim oFreeText As vbBabel.Freetext
        Dim lCounter As Long, lArrayCounter As Long
        Dim lCounter2 As Long
        Dim lMaxLengthOfFreetext As Long
        Dim bThisIsDNBNOROverforselInstruction As Boolean
        Dim bPaymentFromOtherBank As Boolean
        Dim bTheInformationIsStructured As Boolean
        Dim sOldAccountNo As String, sI_SWIFTAddr As String
        Dim sLocalPayCode As String
        Dim bUNBWritten As Boolean, bWriteUNH As Boolean, bUNHWritten As Boolean
        Dim bWriteUNTForEachBabelFile As Boolean
        Dim bThisIsKID As Boolean
        Dim bNewMessage As Boolean, bNewLIN As Boolean
        Dim lPaymentCounter As Long
        Dim sAdditionalNo As String, sEDIMessageNo As String
        Dim sBGMReference As String
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim sCurrencyToUse As String
        Dim sPaymentReference As String
        Dim nSequenceNo As Double, bSequenceNoFound As Boolean
        Dim sSenderIdentification As String
        Dim sReceiverIdentification As String
        Dim sReverseRouting As String
        Dim sBranchCode As String
        Dim sIAccountNo As String
        Dim eBranchType As vbBabel.BabelFiles.BankBranchType
        Dim bChangeI_BankInfo As Boolean
        Dim sAccountCountryCode As String
        Dim sAccountCurrency As String
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim sI_Account As String
        Dim sAccountName As String
        Dim bAccountFound As Boolean
        Dim sTemp As String

        On Error GoTo ERR_CreateHSBC_EDIFile

        Select Case sFormat

            Case "PAYMUL"

                'ONLY IMPLEMENTED FOR NORWEGIAN DEBIT ACCOUNTS

                'bx = mrEDISettingsExport.InitiateSpecialSegments
                'mrEDISettingsExport.SetCurrentElement ("//HEADER/ATTRIBUTES")

                bChangeI_BankInfo = False

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "." 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = 3 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                'WriteSegment "UNA:+,? " - Not used by HSBC
                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                oGroupPayment = New vbBabel.GroupPayments  '23.05.2017 CreateObject ("vbBabel.GroupPayments")
                oGroupPayment.GroupCrossBorderPayments = False
                oGroupPayment.GroupPriorityPayments = False
                oGroupPayment.SetGroupingParameters(True, True, True, True, False, True)

                'Because of the grouping we do, we have set the PayType before we
                ' start the writing of the file.

                sOldAccountNo = vbNullString
                bSequenceNoFound = False
                For Each oBabelFile In oBabelFiles

                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If sOldAccountNo <> Trim$(oPayment.I_Account) Then
                                'bNewPayments = True Don't need a new payments when the debit account is changed
                                If oBabelFile.VB_ProfileInUse Then
                                    bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False)
                                End If
                                sOldAccountNo = Trim$(oPayment.I_Account)
                            End If

                            If IsPaymentDomestic(oPayment, sI_SWIFTAddr, False, , True) Then
                                If oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                    'OK
                                Else
                                    oPayment.PayType = "D"
                                End If
                            Else
                                oPayment.PayType = "I"
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
                sOldAccountNo = vbNullString

                For Each oBabelFile In oBabelFiles

                    If Not bUNBWritten Then
                        If EmptyString(oBabelFile.EDI_MessageNo) Then
                            sUNBReference = CreateBGMReference(oBabelFile)
                        Else
                            sUNBReference = oBabelFile.EDI_MessageNo
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        sSenderIdentification = ""
                        If oBabelFile.VB_ProfileInUse Then
                            sSenderIdentification = Trim(oBabelFile.VB_Profile.AdditionalNo)
                        End If
                        If Not EmptyString(sCompanyNo) Then
                            sReverseRouting = Trim$(sCompanyNo)
                        Else
                            err.Raise(1, "CreateEDIFile", "No reverse routing address is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Please contact Your dealer.")
                        End If
                        'XNET - 15.02.2012 - Changed the line below
                        sReceiverIdentification = "" '???? Is this necessary, which field to use (it seems that it differs from case to case.
                        WriteSegment("UNB+UNOA:3+" & sSenderIdentification & "::" & sReverseRouting & "+" & sReceiverIdentification & "::HEXAGON ABC+" & Format(Now(), "YYMMDD") & ":" & Format(Now(), "HHMM") & "+" & Right(sUNBReference, 15))
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above
                    sBGMReference = sUNBReference

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim$(Str(lUNHCounter)) & "+PAYMUL:D:96A:UN:FUN01G")
                    'To create the unique message_ID use the total amount

                    'XNET - 14.02.2012 - Changed line below
                    WriteSegment("BGM+452+" & Right(sBGMReference, 15) & "+9")
                    If EmptyString(oBabelFile.DATE_Production) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now()), "102")
                    Else
                        WriteDTMSegment("137", oBabelFile.DATE_Production, "102")
                    End If
                    'QUESTION
                    'If EmptyString(oBabelFile.IDENT_Sender) Then
                    '    WriteSegment "NAD+MS+BabelBank"
                    'Else
                    '    WriteSegment "NAD+MS+" & Trim$(oBabelFile.IDENT_Sender)
                    'End If

                    For Each oBatch In oBabelFile.Batches
                        For lCounter2 = 1 To oBatch.Payments.Count
                            bNewLIN = True

                            oGroupPayment.Batch = oBatch
                            oGroupPayment.MarkIdenticalPayments()
                            'Group the payments inside a batch and create a UNH per group
                            lPaymentCounter = 0
                            For Each oPayment In oBatch.Payments

                                If oPayment.SpecialMark And Not oPayment.Exported Then
                                    lPaymentCounter = lPaymentCounter + 1
                                    If sOldAccountNo <> Trim$(oPayment.I_Account) Then
                                        If oPayment.VB_ProfileInUse Then
                                            sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If sI_Account = oaccount.Account Or sI_Account = Trim(oaccount.ConvertedAccount) Then
                                                            sAccountName = Trim$(oClient.Name)
                                                            bAccountFound = True
                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If

                                        'bNewPayments = True Don't need a new payments when the debit account is changed
                                        If oBabelFile.VB_ProfileInUse Then
                                            bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, iFilesetupOut, sI_SWIFTAddr, False, sAccountCountryCode, sAccountCurrency)
                                        End If
                                        sOldAccountNo = Trim$(oPayment.I_Account)
                                        If Len(sOldAccountNo) = 15 And Mid(sOldAccountNo, 7, 1) = " " Then
                                            sBranchCode = Left(sOldAccountNo, 6)
                                            sIAccountNo = Right(sOldAccountNo, 8)
                                            eBranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                            bChangeI_BankInfo = True
                                        End If
                                    End If

                                    If bChangeI_BankInfo Then
                                        oPayment.BANK_BranchNo = sBranchCode
                                        oPayment.I_Account = sIAccountNo
                                        oPayment.BANK_BranchType = eBranchType
                                    End If

                                    'Find country for receiver if not stated
                                    If EmptyString(oPayment.I_CountryCode) Then
                                        If Not EmptyString(oPayment.BANK_I_SWIFTCode) Then
                                            oPayment.I_CountryCode = Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)
                                        Else
                                            oPayment.I_CountryCode = Mid$(sI_SWIFTAddr, 5, 2)
                                            'If debit account and receiver is in the same country it must be
                                            '  a domestic payment
                                            'If oPayment.E_CountryCode <> oPayment.I_CountryCode Then
                                            '    oPayment.PayType = "I"
                                            'Else
                                            '    oPayment.PayType = "D"
                                            'End If
                                        End If
                                    End If

                                    oPayment.BANK_I_SWIFTCode = sI_SWIFTAddr
                                    If Left$(oPayment.BANK_I_SWIFTCode, 4) <> "HSBC" Then
                                        '24.11.2008 - Added the possibility to pay from accounts outside DnBNOR
                                        err.Raise(1, "CreateEDIFile", "Possibility to send payments from another bank" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                        bPaymentFromOtherBank = True
                                    Else
                                        bPaymentFromOtherBank = False
                                    End If

                                    If Mid(oPayment.BANK_I_SWIFTCode, 5, 2) <> "NO" Then
                                        err.Raise(1, "CreateEDIFile", "Paying from accounts from the SWIFT-address " & oPayment.BANK_I_SWIFTCode & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                    Else
                                        If oPayment.PayType = "I" Then
                                            err.Raise(1, "CreateEDIFile", "International payments from accounts in Norway " & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer.")
                                        End If
                                    End If

                                    'lArrayCounter = 0

                                    oPayment.E_Name = CheckForValidCharacters(oPayment.E_Name, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr1 = CheckForValidCharacters(oPayment.E_Adr1, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr2 = CheckForValidCharacters(oPayment.E_Adr2, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_Adr3 = CheckForValidCharacters(oPayment.E_Adr3, True, True, True, True, " /-?:().,'+")
                                    oPayment.E_City = CheckForValidCharacters(oPayment.E_City, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Name = CheckForValidCharacters(oPayment.BANK_Name, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr1 = CheckForValidCharacters(oPayment.BANK_Adr1, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr2 = CheckForValidCharacters(oPayment.BANK_Adr2, True, True, True, True, " /-?:().,'+")
                                    oPayment.BANK_Adr3 = CheckForValidCharacters(oPayment.BANK_Adr3, True, True, True, True, " /-?:().,'+")
                                    oPayment.I_Name = CheckForValidCharacters(oPayment.I_Name, True, True, True, True, " /-?:().,'+")

                                    If bPaymentFromOtherBank Then
                                        lMaxLengthOfFreetext = 140
                                    Else
                                        Select Case oPayment.I_CountryCode

                                            'NOT VALIDATED

                                            'Danish account transfer: 41�35 characters.
                                            'Danish transfer via PBS: 41�35 characters.
                                            'Danish and foreign cheque: 14�35 (7�70) characters.
                                            'Swedish payments: 30�35 characters.
                                            'Finnish payments: 12�35 characters.
                                            'Norwegian payments: 12�35 characters.

                                            '                        Case "DE"
                                            '                            If oPayment.PayType = "I" Then
                                            '                                'ErrorWritingEDI 1, "CreateEDIFile", "Possibility to send international payments from Germany" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                            '                                lMaxLengthOfFreetext = 140
                                            '                            Else
                                            '                                lMaxLengthOfFreetext = 162
                                            '                            End If
                                            Case "DK"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                                    lMaxLengthOfFreetext = 1435
                                                ElseIf oPayment.PayType = "I" Then
                                                    'err.Raise 1, "CreateEDIFile", "Possibility to send international payments from Denmark" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1435
                                                End If
                                            Case "FI"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "NO"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 420
                                                End If
                                            Case "SE"
                                                bThisIsDNBNOROverforselInstruction = False
                                                If oPayment.PayType = "I" Then
                                                    lMaxLengthOfFreetext = 140
                                                Else
                                                    lMaxLengthOfFreetext = 1050
                                                End If
                                                '                        Case "US"
                                                '                            If oPayment.PayType = "I" Then
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            Else
                                                '                                lMaxLengthOfFreetext = 140
                                                '                            End If

                                            Case Else
                                                bThisIsDNBNOROverforselInstruction = True
                                                lMaxLengthOfFreetext = 140
                                                'err.Raise 1, "CreateEDIFile", "Possibility to send payments from " & oPayment.I_CountryCode & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Contact Your dealer."
                                        End Select
                                    End If

                                    If lMaxLengthOfFreetext = 0 Then
                                        lMaxLengthOfFreetext = 140
                                    End If

                                    If bNewLIN Then
                                        bNewLIN = False
                                        lSEQCounter = 0
                                        '4-LIN
                                        WriteLINSegment()
                                        '4-DTM
                                        '25.11.2008 - removed the IF
                                        'If bPaymentFromOtherBank Then

                                        'Else
                                        Select Case oPayment.I_CountryCode
                                            Case "NO"
                                                If oPayment.DATE_Payment < Format(Now(), "yyyyMMdd") Then
                                                    WriteDTMSegment("203", Format(Now(), "yyyyMMdd"), "102")
                                                Else
                                                    WriteDTMSegment("203", oPayment.DATE_Payment, "102")
                                                End If
                                        End Select
                                        'End If
                                        '4-RFF
                                        If Len(oBatch.REF_Own) > 0 Then
                                            '20.02.2008 - This is done for Gjensidige to do the reference at the LIN-level
                                            ' unique.
                                            'oBatch.REF_Own = oBatch.REF_Own & "-" & Trim$(Str(lPaymentCounter))
                                            WriteRFFSegment("AEK", CheckForValidCharacters(Trim$(oBatch.REF_Own), True, True, True, True, " /-?:().,'+"))
                                        ElseIf sSpecial = "RICOH" Then
                                            If oPayment.PayType = "S" Then
                                                WriteRFFSegment("AEK", "LONN")
                                            Else
                                                WriteRFFSegment("AEK", "REMITTERING")
                                            End If
                                        Else
                                            WriteRFFSegment("AEK", sUNBReference & PadLeft(Trim$(Str(lUNHCounter)), 4, "0") & PadLeft$(Trim$(Str(lLINCounter)), 8, "0"), 35)
                                        End If
                                        '4-BUS
                                        If oPayment.PayType = "S" Then
                                            WriteSegment("BUS+1:SAL+DO")
                                        Else
                                            WriteBUSSegment("BUS+1+DO")
                                        End If
                                        '                                '4-FCA
                                        '                                'If oPayment.PayType = "I" Then
                                        '                                WriteFCASegment oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad
                                        'End If
                                        '5-MOA
                                        If Len(Trim$(oPayment.MON_TransferCurrency)) <> 3 Then
                                            If EmptyString(oPayment.MON_TransferCurrency) Then
                                                oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency
                                                sCurrencyToUse = Trim$(oPayment.MON_TransferCurrency)
                                            Else
                                                err.Raise(1, "CreateEDIFile", "Wrong transfercurrency stated on the payment." & _
                                                        vbCrLf & "Receiver: " & oPayment.E_Name & _
                                                        vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & _
                                                        vbCrLf & "Currency: " & Trim$(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & vbCrLf & "Contact Your dealer.")
                                            End If
                                        Else
                                            sCurrencyToUse = Trim$(oPayment.MON_TransferCurrency)
                                        End If

                                        Select Case oPayment.I_CountryCode
                                            Case "NO"
                                                If oPayment.PayType <> "I" Then
                                                    If Not EmptyString(sCurrencyToUse) And sCurrencyToUse <> "NOK" Then
                                                        err.Raise(1, "CreateEDIFile", "Wrong currency stated on the dometic payment." & _
                                                                vbCrLf & "Receiver: " & oPayment.E_Name & _
                                                                vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & _
                                                                vbCrLf & "Currency: " & Trim$(oPayment.MON_TransferCurrency) & _
                                                                vbCrLf & vbCrLf & "Contact Your dealer.")
                                                    Else
                                                        WriteMOASegment("9", oGroupPayment.MON_InvoiceAmount, "NOK")
                                                    End If
                                                End If

                                        End Select

                                        '6-FII
                                        'Changed 08.04.05 - Kjell
                                        If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode Then
                                            WriteSegment("FII+OR+" & Trim(oPayment.I_Account) & ":" & sAccountName & "::" & sAccountCurrency & "+:::" & oPayment.BANK_BranchNo & ":154:133+" & sAccountCountryCode)
                                        Else
                                            WriteSegment("FII+OR+" & Trim(oPayment.I_Account) & "+" & oPayment.BANK_I_SWIFTCode & ":25:5")
                                        End If

                                        '7-NAD is optional
                                        If oBabelFile.VB_ProfileInUse Then
                                            'Info from Company
                                            If Not EmptyString(oBabelFile.VB_Profile.CompanyName) Then
                                                sTemp = ""
                                                sTemp = "NAD+OY+++" & oBabelFile.VB_Profile.CompanyName & "+" & oBabelFile.VB_Profile.CompanyAdr1
                                                If Not EmptyString(oBabelFile.VB_Profile.CompanyAdr2) Then
                                                    sTemp = sTemp & ":" & oBabelFile.VB_Profile.CompanyAdr2
                                                    If Not EmptyString(oBabelFile.VB_Profile.CompanyAdr3) Then
                                                        sTemp = sTemp & ":" & oBabelFile.VB_Profile.CompanyAdr3
                                                    End If
                                                End If
                                                sTemp = sTemp & "+" & oBabelFile.VB_Profile.CompanyCity
                                                sTemp = sTemp & "++" & oBabelFile.VB_Profile.CompanyZip
                                                sTemp = sTemp & "+" & Left(oBabelFile.VB_Profile.BaseCurrency, 2)
                                                WriteSegment(sTemp)
                                            End If
                                        End If

                                    End If 'if bNewLIN

                                    'If oPayment.I_CountryCode = "NO" And Trim$(oPayment.PayCode) = "301" Then
                                    'XOKNET 07.01.2011 Added "FI"
                                    If (oPayment.I_CountryCode = "NO" Or oPayment.I_CountryCode = "FI") And (Trim$(oPayment.PayCode) > "300" And oPayment.PayCode < "304") Then
                                        bThisIsKID = True
                                    Else
                                        bThisIsKID = False
                                        sLocalFreetext = vbNullString
                                        sLocalREF_Own = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier = "1" Then
                                                    sLocalFreetext = sLocalFreetext & RTrim$(oFreeText.Text) & " "
                                                End If
                                            Next oFreeText
                                        Next oInvoice
                                        If Len(sLocalFreetext) > lMaxLengthOfFreetext Then
                                            'sLocalFreetext = "SEE SEPERATE LETTER."
                                        End If
                                        sLocalFreetext = RTrim$(Left$(sLocalFreetext, lMaxLengthOfFreetext))
                                    End If
                                    If oPayment.Invoices.Count > 0 Then
                                        sLocalREF_Own = oPayment.Invoices.Item(1).REF_Own
                                    End If

                                    '11-SEQ
                                    WriteSEQSegment()
                                    '11-MOA
                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_TransferCurrency Then
                                        WriteMOASegment("9", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency)
                                    Else
                                        err.Raise("CreateEDIFile", "BabelBank is not yet tested for reference and target currency being different." & _
                                                            vbCrLf & "Receiver: " & oPayment.E_Name & _
                                                            vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount & _
                                                            vbCrLf & "Currency: " & Trim$(oPayment.MON_TransferCurrency) & _
                                                            vbCrLf & vbCrLf & "Contact Your dealer.")
                                        WriteMOASegment("57", oPayment.MON_InvoiceAmount, vbNullString)
                                    End If
                                    '11-DTM - Optional but not used by HSBC

                                    '11-RFF
                                    'Special case for DnBNORFinans
                                    'oPayment.REF_Own is always DEBITOR UTBET., therefore the ownreference
                                    ' from the first Invoice is used

                                    If oBabelFile.Special = "RICOH" Then
                                        nSequenceNo = nSequenceNo + 1
                                        If nSequenceNo > 9999 Then
                                            nSequenceNo = 0
                                        End If
                                        sPaymentReference = vbNullString
                                        sPaymentReference = sPaymentReference & Format(Now(), "YYMMDD") & PadLeft$(Trim$(Str(nSequenceNo)), 4, "0")
                                        WriteRFFSegment("CR", CheckForValidCharacters(Trim$(oPayment.Invoices.Item(1).REF_Own) & "-" & sPaymentReference & "-" & oPayment.MON_InvoiceAmount & "-" & Trim$(oPayment.MON_InvoiceCurrency), True, True, True, True, " /-?:().,'+"))
                                    ElseIf Len(Trim$(oPayment.REF_Own)) > 0 Then
                                        WriteRFFSegment("CR", CheckForValidCharacters(Trim$(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                                    Else
                                        If oPayment.Invoices.Count > 0 Then
                                            If Len(Trim$(oPayment.Invoices.Item(1).REF_Own)) > 0 Then
                                                WriteRFFSegment("CR", CheckForValidCharacters(Trim$(oPayment.Invoices.Item(1).REF_Own), True, True, True, True, " /-?:().,'+"))
                                            Else
                                                'XNET - 03.12.2011 - Added next IF
                                                If Len(oPayment.E_Name) > 0 Then
                                                    WriteRFFSegment("CR", CheckForValidCharacters(Trim$(oPayment.E_Name), True, True, True, True, " /-?:().,'+") & Str(oPayment.Index))
                                                Else
                                                    err.Raise(1, "CreateEDIFile", "No customer reference (own reference is stated." & _
                                                        vbCrLf & "Receiver: " & oPayment.E_Name & _
                                                        vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
                                                End If
                                            End If
                                        Else
                                            'err.Raise 1, "CreateEDIFile", "No customer reference (own reference is stated." & _
                                            'vbCrLf & "Receiver: " & oPayment.E_Name & _
                                            'vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount
                                        End If
                                    End If

                                    If oBabelFile.Special = "RICOH" Then
                                        If oPayment.PayType = "S" Then
                                            WriteRFFSegment("PQ", "LONN RICOH NORGE")
                                        End If
                                    ElseIf Len(Trim$(oPayment.REF_EndToEnd)) > 0 Then
                                        WriteRFFSegment("PQ", CheckForValidCharacters(Trim$(oPayment.REF_EndToEnd), True, True, True, True, " /-?:().,'+"))
                                    End If

                                    '11-PAI
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        Err.Raise(1, "CreateEDIFile", "BabelBank is not yet tested for checkpayments." & _
                                            vbCrLf & "Receiver: " & oPayment.E_Name & _
                                            vbCrLf & "Amount: " & oPayment.MON_InvoiceAmount)
                                    ElseIf oPayment.Priority Then
                                        If oPayment.ToOwnAccount Then
                                            WriteSegment("PAI+::52:::Z24")
                                        Else
                                            WriteSegment("PAI+::52")
                                        End If
                                    Else
                                        WriteSegment("PAI+::2")
                                    End If
                                    '11 - FCA - Optional
                                    'If oPayment.PayType = "I" Then
                                    '    WriteFCASegment oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad
                                    'End If
                                    '12 - FII
                                    If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Then
                                        'Do not use the FI-segment
                                    Else
                                        If oPayment.PayType = "I" Then
                                            WriteFIISegment(E_Info, True, oPayment.I_CountryCode, False, True)
                                        Else
                                            'Example from James Croft: FII+BF+82001000000:BENEFICIARY NAME::NOK+::::154:25+NO'
                                            'XNET - 14.02.2012 - Changed next line
                                            'New example from James: FII+BF+82001000000:BENEFICIARY NAME::NOK++NO'
                                            WriteSegment("FII+BF+" & oPayment.E_Account & ":" & Trim(oPayment.E_Name) & "::NOK++NO")
                                            'WriteSegment "FII+BF+" & oPayment.E_Account & ":" & Trim(oPayment.E_Name) & "::NOK+::::154:25+NO"
                                        End If
                                        If Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Or Not EmptyString(oPayment.BANK_NameAddressCorrBank1) Then
                                            WriteFIISegment(CorrBank, True, oPayment.I_CountryCode, False, True)
                                        End If
                                    End If
                                    '13 - NAD
                                    bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                    WriteNADSegment(True, True)
                                    '13 - CTA
                                    'Not implemented
                                    '13 - COM
                                    'Not implemented
                                    '14 - INP
                                    '22.10.2008 - Removed the INP segment after phone from H�kon Belt in DnBNOR
                                    'It is of no use when stated like INP+3:4+1:AD, but it creates problems for DnBNOR
                                    '24.11.2008 - Added INP for Sweden after phone from H�kon Belt in DnBNOR
                                    If oPayment.PayType = "D" And oPayment.I_CountryCode = "SE" Then
                                        WriteSegment("INP+3:4+1:AD")
                                    End If
                                    '15 - GIS
                                    If oPayment.I_CountryCode = "NO" And oPayment.PayType = "I" Then
                                        'Central Bank Reporting
                                        sStatebankText = vbNullString
                                        sStatebankCode = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            sStatebankText = oInvoice.STATEBANK_Text
                                            sStatebankCode = oInvoice.STATEBANK_Code
                                            If Not EmptyString(sStatebankText) Then
                                                Exit For
                                            End If
                                        Next oInvoice
                                        If EmptyString(sStatebankText) Then
                                            If EmptyString(sStatebankCode) Then
                                                WriteSegment("GIS+14")
                                                'WriteFTXSegment "REG", "Export/Import av varer"
                                            Else
                                                WriteSegment("GIS+" & sStatebankCode)
                                                'WriteFTXSegment "REG", "Export/Import av varer"
                                            End If
                                        Else
                                            If EmptyString(sStatebankCode) Then
                                                WriteSegment("GIS+14")
                                                'WriteFTXSegment "REG", CheckForValidCharacters(sSTATEBANK_Text, True, True, True, True, " /-?:().,'+")
                                            Else
                                                WriteSegment("GIS+" & sStatebankCode)
                                                'WriteFTXSegment "REG", CheckForValidCharacters(sSTATEBANK_Text, True, True, True, True, " /-?:().,'+")
                                            End If
                                        End If

                                    End If
                                    '16-PRC
                                    If oPayment.I_CountryCode = "NO" Then
                                        If bThisIsKID Then
                                            For Each oInvoice In oPayment.Invoices
                                                WriteSegment("PRC+8")
                                                If Len(oInvoice.Unique_Id) > 0 Then
                                                    If oInvoice.MON_InvoiceAmount < 0 Then
                                                        WriteDOCSegment("998", Trim$(oInvoice.Unique_Id), 35)
                                                    Else
                                                        WriteDOCSegment("999", Trim$(oInvoice.Unique_Id), 35)
                                                    End If
                                                ElseIf Len(oInvoice.InvoiceNo) > 0 Then
                                                    If oInvoice.MON_InvoiceAmount < 0 Then
                                                        WriteDOCSegment("381", Trim$(oInvoice.InvoiceNo), 35)
                                                    Else
                                                        WriteDOCSegment("380", Trim$(oInvoice.InvoiceNo), 35)
                                                    End If
                                                Else
                                                    If oInvoice.MON_InvoiceAmount < 0 Then
                                                        WriteDOCSegment("381", "FAKTURA", 35)
                                                    Else
                                                        WriteDOCSegment("380", "FAKTURA", 35)
                                                    End If
                                                End If
                                                ' XNET 16.03.2012 - added ABS to Amount, because we will not have minus in front for 381
                                                WriteMOASegment(12, Math.Abs(oInvoice.MON_InvoiceAmount), oPayment.MON_InvoiceCurrency)
                                            Next oInvoice
                                            '23-GIS
                                            WriteSegment("GIS+37")
                                        Else
                                            If Not EmptyString(sLocalFreetext) Then
                                                WriteSegment("PRC+11")
                                                '16-FTX
                                                'NOMORE
                                                'WriteFTXSegment "PMD", CheckForValidCharacters(aFreetextArray(0, lArrayCounter), True, True, False, True, " /-?:().,'+")
                                                WriteFTXSegment("PMD", CheckForValidCharacters(sLocalFreetext, True, True, True, True, " /-?:().,'+"))
                                                '23-GIS
                                                WriteSegment("GIS+37")
                                            Else
                                                'No advice
                                            End If
                                        End If
                                    End If

                                    oPayment.Exported = True

                                End If 'If oPayment.SpecialMark And Not oPayment.Exported Then
                            Next oPayment

                            oGroupPayment.RemoveSpecialMark()
                            If oGroupPayment.AllPaymentsMarkedAsExported Then
                                Exit For
                            End If
                        Next lCounter2
                    Next oBatch
                    '04.09.2008 - Moved from beneath
                    WriteSegment("CNT+27:" & Trim$(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                'XNET - 15.02.2012 - Changed the line below
                WriteUNZSegment(Right(sUNBReference, 15))

        End Select

        'To write the rest of the file
        WriteSegment(vbNullString, True)

        If Not oGroupPayment Is Nothing Then
            oGroupPayment = Nothing
        End If

        On Error GoTo 0
        Exit Function

ERR_CreateHSBC_EDIFile:

        If Not oGroupPayment Is Nothing Then
            oGroupPayment = Nothing
        End If

        err.Raise(err.Number, "CreateHSBC_EDIFile", err.Description)

    End Function
    'XNET - 02.01.2012 - Whole function
    Private Function CreateNordea_eGateway_EDIFile(ByVal sCompanyNo As String, ByVal sFormat As String) As Boolean
        Dim oFreetext As vbBabel.Freetext
        Dim eCountryName As vbBabel.BabelFiles.CountryName
        Dim bUNBWritten As Boolean
        Dim bUNAWritten As Boolean
        Dim bUNHWritten As Boolean
        Dim bNewMessage As Boolean
        Dim sTemp As String
        Dim sTemp2 As String
        Dim sTempREF As String
        Dim nTemp As Double
        Dim sLINCurrency As String
        Dim sUNBSegment As String
        Dim sUNHSegment As String
        Dim sUCISegment As String
        Dim sIdentSender As String
        Dim sIdentReceiver As String
        Dim sStatus As String
        Dim bSequenceNoFound As Boolean
        Dim sUNBReference As String
        Dim sOriginalUNBReference As String
        Dim sBGMReference As String
        Dim bLINWritten As Boolean
        Dim bNewLIN As Boolean
        Dim oFileSetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oAccount As vbBabel.Account
        Dim sOldAccountNo As String
        Dim bFoundClient As Boolean
        Dim bClientExists As Boolean
        Dim sCountryCodeFromSetup As String
        Dim sAccountCurrencyFromSetup As String
        Dim sI_SWIFT_FromSetup As String
        Dim sStatusCode As String
        Dim bRFFFound As Boolean
        Dim lCounter As Integer
        Dim iPos1 As Integer
        Dim iPos2 As Integer
        Dim bStateBankInfoFound As Boolean = False
        Dim sStateBankCode As String = ""
        Dim sStateBankText As String = ""
        Dim bStructured As Boolean
        Dim bThisIsBankgiro As Boolean = False
        Dim bRejection As Boolean = False

        On Error GoTo ERR_CreateNordea_eGateway_EDIFile

        bWrite80 = True 'Essential for Gjensidige aginst FDC2100, if changed we must use a speial to set this for Gjensidige!!!!!!!!!

        Select Case sFormat

            Case "BANSTA"

                WriteSegment("UNA:+,? ")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0
                'lLINCounter = 0 - 04.09.2008 moved down

                sOldAccountNo = vbNullString
                bSequenceNoFound = False

                For Each oBabelFile In oBabelFiles

                    sUNBSegment = oBabelFile.UNBSegmentet
                    sUNHSegment = oBabelFile.UNHSegmentet

                    If Not bUNBWritten Then
                        If EmptyString(sUNBSegment) Then 'The imported file has no unique message ID
                            If EmptyString(oBabelFile.EDI_MessageNo) Then
                                sUNBReference = CreateBGMReference(oBabelFile)
                            Else
                                If sSpecial = "GJENSIDIGE_FDC" Then
                                    'Max 14 characters and only digits
                                    sTemp = oBabelFile.EDI_MessageNo
                                    sTemp2 = ""
                                    For lCounter = Len(sTemp) To 1 Step -1
                                        If vbIsNumeric(Mid(sTemp, lCounter, 1), "0123456789") Then
                                            sTemp2 = Mid(sTemp, lCounter, 1) & sTemp2
                                        End If
                                        If Len(sTemp2) > 13 Then
                                            Exit For
                                        End If
                                    Next lCounter
                                    sUNBReference = sTemp2
                                    sTemp = ""
                                    sTemp2 = ""
                                Else
                                    sUNBReference = Left(Trim(oBabelFile.EDI_MessageNo), 35)
                                End If
                            End If
                        Else
                            iPos1 = 1
                            iPos2 = 1
                            sTemp = vbNullString
                            For lCounter = 1 To 5
                                iPos1 = iPos2
                                iPos2 = iPos2 + 1
                                iPos2 = InStr(iPos2, sUNBSegment, "+", vbTextCompare)
                            Next lCounter
                            sTemp = Mid$(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                            sUNBReference = Mid$(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", vbTextCompare) - iPos2 - 1)
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentReceiver = Trim$(sCompanyNo)
                        ElseIf Not EmptyString(oBabelFile.IDENT_Receiver) Then
                            sIdentReceiver = Trim$(oBabelFile.IDENT_Receiver)
                        Else
                            Err.Raise(1, "CreateEDIFile", "No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer.")
                        End If

                        'If sSpecial = "GJENSIDIGE_FDC" Then
                        '    sIdentReceiver = Replace(sIdentReceiver, "-", "")
                        'End If

                        'Add date and time
                        sTemp = Format(Now(), "yyMMdd") & ":" & Format(Now(), "hhmm")

                        If oBabelFile.IDENT_Sender = "NORDEATEST" Then
                            WriteSegment("UNB+UNOC:3+NORDEATEST:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++BANSTA")
                        Else
                            WriteSegment("UNB+UNOC:3+NORDEAPROD:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++BANSTA")
                        End If
                        'WriteSegment("UNB+UNOC:3+5164070384:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++BANSTA")
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim$(Str(lUNHCounter)) & "+BANSTA:D:96A:UN")
                    'To create the unique message_ID use the total amount

                    If Not EmptyString(oBabelFile.EDI_MessageNo) Then
                        If sSpecial = "GJENSIDIGE_FDC" Then
                            sBGMReference = sUNBReference
                        Else
                            sBGMReference = oBabelFile.EDI_MessageNo
                        End If
                    Else
                        sBGMReference = CreateBGMReference(oBabelFile)
                    End If
                    WriteSegment("BGM+23+" & sBGMReference)

                    If EmptyString(oBabelFile.DATE_Production) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now()), "102")
                    Else
                        WriteDTMSegment("137", oBabelFile.DATE_Production, "102")
                    End If

                    'Maybe it is more correct to at the code P + countrycode according to the mappingfile
                    WriteSegment("BUS++++EGW")

                    WriteSegment("FII+MS++NDEASESS")

                    '3 - NAD - Not implemented

                    For Each oBatch In oBabelFile.Batches

                        For Each oPayment In oBatch.Payments
                            '4-LIN
                            lSEQCounter = 0
                            WriteLINSegment()

                            '5-RFF
                            'UNB-reference from original message (must be from a part of the PmtInfID
                            'WHERE TO FIND THIS?
                            '                If Not EmptyString(oBatch.REF_Bank1) Then
                            '                    WriteRFFSegment "DM", CheckForValidCharacters(Trim$(oBatch.REF_Bank1), True, True, True, True, " /-?:().,'+")
                            '                End If
                            'SEQ-reference from original message - Only used when status is reported on the C-level
                            If Not EmptyString(oPayment.REF_Own) Then
                                WriteRFFSegment("CR", CheckForValidCharacters(Trim$(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                            End If

                            'LIN-reference from original message
                            If oBabelFile.Cargo.Special = "GJENSIDIGE_FDC" Then
                                bRejection = False
                                If oBabelFile.ImportFormat = BabelFiles.FileType.Pain002_Rejected Then
                                    bRejection = True
                                ElseIf oBabelFile.StatusCode = "00" Then
                                    bRejection = False
                                ElseIf oBabelFile.StatusCode = "01" Then
                                    bRejection = False
                                ElseIf oBabelFile.StatusCode = "02" Then
                                    bRejection = False
                                Else
                                    bRejection = True
                                End If
                                If bRejection Then
                                    sTempREF = oBatch.REF_Own
                                    If EmptyString(sTempREF) Then
                                        sTempREF = oBabelFile.File_id
                                    End If
                                    iPos1 = InStr(sTempREF, "-")
                                    If iPos1 > 0 Then
                                        sTempREF = Left(sTempREF, iPos1 - 1)
                                    End If
                                    WriteRFFSegment("AEK", CheckForValidCharacters(Trim$(sTempREF), True, True, True, True, " /-?:().,'+"))
                                Else
                                    sTempREF = oPayment.REF_Bank2
                                    iPos1 = InStr(sTempREF, "-")
                                    sTempREF = Left(oPayment.REF_Bank2, iPos1 - 1)
                                    WriteRFFSegment("AEK", CheckForValidCharacters(Trim$(sTempREF), True, True, True, True, " /-?:().,'+"))
                                End If
                            Else
                                If Not EmptyString(oBatch.REF_Own) Then
                                    WriteRFFSegment("AEK", CheckForValidCharacters(Trim$(oBatch.REF_Own), True, True, True, True, " /-?:().,'+"))
                                End If
                            End If

                            '6-SEQ
                            WriteSEQSegment()
                            '6-GIS

                            Select Case oPayment.ImportFormat

                                Case vbBabel.BabelFiles.FileType.Camt054_Incoming, vbBabel.BabelFiles.FileType.Camt054
                                    sTemp = "1"

                                Case vbBabel.BabelFiles.FileType.Pain002_Rejected
                                    sTemp2 = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreetext In oInvoice.Freetexts
                                            sTemp2 = sTemp2 & " " & oFreetext.Text
                                        Next
                                    Next oInvoice
                                    If EmptyString(sTemp2) Then
                                        sTemp = "3"
                                    Else
                                        sTemp = "2"
                                    End If

                                Case Else
                                    'To get the correct code, pass the oPayment.Statuscode to a function
                                    If oPayment.StatusCode = "02" Then
                                        sTemp = "1"
                                    Else
                                        If Not EmptyString(oPayment.StatusText) Then
                                            sTemp = "2"
                                            sTemp2 = oPayment.StatusText
                                        Else
                                            sTemp = "3"
                                        End If

                                    End If

                            End Select

                            WriteSegment("GIS+" & sTemp)

                            '6-FTX
                            If Not EmptyString(sTemp2) Then
                                WriteFTXSegment("AAO", sTemp2)
                            End If

                            oPayment.Exported = True

                            '8-NAD - Not implemented
                            'This segment contains the internal customer identification of the ordering customer when 
                            'sent in PAYMUL in segment NAD, segment group 7, with qualifier �ZZZ�.

                        Next oPayment

                    Next oBatch

                    '            WriteSegment "CNT+LI:" & Trim$(Str(lLINCounter))
                    WriteUNTSegment()
                Next oBabelFile
                'Old code
                'WriteSegment "CNT+2:" & Trim$(Str(lLINCounter))
                'WriteUNTSegment
                WriteUNZSegment(sUNBReference)

            Case "CONTRL"
                'This message is not complete. We lack the usage of UCM
                WriteSegment("UNA:+,? ")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0

                sOldAccountNo = vbNullString
                bSequenceNoFound = False

                For Each oBabelFile In oBabelFiles

                    sUNBSegment = oBabelFile.UNBSegmentet
                    sUNHSegment = oBabelFile.UNHSegmentet

                    If Not bUNBWritten Then
                        If Not EmptyString(oBabelFile.EDI_MessageNo) Then
                            If sSpecial = "GJENSIDIGE_FDC" Then
                                'Max 14 characters and only digits
                                sTemp = oBabelFile.EDI_MessageNo
                                sTemp2 = ""
                                For lCounter = Len(sTemp) To 1 Step -1
                                    If vbIsNumeric(Mid(sTemp, lCounter, 1), "0123456789") Then
                                        sTemp2 = Mid(sTemp, lCounter, 1) & sTemp2
                                    End If
                                    If Len(sTemp2) > 13 Then
                                        Exit For
                                    End If
                                Next lCounter
                                sUNBReference = sTemp2
                                sTemp = ""
                                sTemp2 = ""
                            Else
                                sUNBReference = Left(Trim(oBabelFile.EDI_MessageNo), 35)
                            End If
                        ElseIf EmptyString(sUNBSegment) Then 'The imported file has no unique message ID
                            sUNBReference = CreateBGMReference(oBabelFile)
                        Else
                            iPos1 = 1
                            iPos2 = 1
                            sTemp = vbNullString
                            For lCounter = 1 To 5
                                iPos1 = iPos2
                                iPos2 = iPos2 + 1
                                iPos2 = InStr(iPos2, sUNBSegment, "+", vbTextCompare)
                            Next lCounter
                            sTemp = Mid$(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                            sUNBReference = Mid$(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", vbTextCompare) - iPos2 - 1)
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentReceiver = Trim$(sCompanyNo)
                        ElseIf Not EmptyString(oBabelFile.IDENT_Receiver) Then
                            sIdentReceiver = Trim$(oBabelFile.IDENT_Receiver)
                        Else
                            Err.Raise(1, "CreateEDIFile", "No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer.")
                        End If

                        'If sSpecial = "GJENSIDIGE_FDC" Then
                        '    sIdentReceiver = Replace(sIdentReceiver, "-", "")
                        'End If

                        'WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++CONTRL")

                        'Add date and time
                        sTemp = Format(Now(), "yyMMdd") & ":" & Format(Now(), "hhmm")

                        If oBabelFile.IDENT_Sender = "NORDEATEST" Then
                            WriteSegment("UNB+UNOC:3+NORDEATEST:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++CONTRL")
                        Else
                            WriteSegment("UNB+UNOC:3+NORDEAPROD:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++CONTRL")
                        End If

                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim$(Str(lUNHCounter)) & "+CONTRL:D:3:UN")
                    'To create the unique message_ID use the total amount

                    For Each oBatch In oBabelFile.Batches

                        sUCISegment = oBatch.UCIString
                        If oBabelFile.ImportFormat = BabelFiles.FileType.Pain002 Then
                            sOriginalUNBReference = oBabelFile.File_id
                            sIdentSender = oBabelFile.IDENT_Receiver
                            sIdentReceiver = oBabelFile.IDENT_Sender
                        ElseIf EmptyString(sUCISegment) Then
                            'Where to get the information needed, like original messageno?
                            sOriginalUNBReference = oBatch.REF_Own
                            sIdentSender = oBabelFile.IDENT_Receiver
                            sIdentReceiver = oBabelFile.IDENT_Sender
                            sStatusCode = FindCONTRLErrorCode(oBatch.StatusCode, oBabelFile.ImportFormat, oBabelFile.BankByCode, vbBabel.BabelFiles.Bank.Nordea_eGateway)
                        Else
                            iPos1 = 1
                            iPos2 = 1
                            sTemp = vbNullString
                            For lCounter = 1 To 4
                                iPos1 = iPos2
                                iPos2 = iPos2 + 1
                                iPos2 = InStr(iPos2, sUCISegment, "+", vbTextCompare)
                                If lCounter = 2 Then
                                    sOriginalUNBReference = Mid$(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                ElseIf lCounter = 3 Then 'Original sender
                                    sIdentSender = Mid$(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                    If InStr(1, sIdentSender, ":") > 0 Then
                                        sIdentSender = Left$(sIdentSender, InStr(1, sIdentSender, ":") - 1)
                                    End If
                                ElseIf lCounter = 4 Then 'Original receiver
                                    sIdentReceiver = Mid$(sUCISegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                End If
                            Next lCounter
                            sStatusCode = oBabelFile.StatusCode
                        End If
                        If oBabelFile.Cargo.Special = "GJENSIDIGE_FDC" Then
                            If oBabelFile.RejectsExists Then
                                sTemp = ""
                                For Each oPayment In oBatch.Payments
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreetext In oInvoice.Freetexts
                                            sTemp = oFreetext.Text & " "
                                        Next oFreetext
                                    Next oInvoice
                                Next
                                sTemp = Trim(sTemp)
                                Select Case sTemp
                                    Case "Duplicate message id"
                                        sTemp = "26"

                                    Case Else
                                        sTemp = "18"

                                End Select
                                WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & ":ZZ+" & sIdentReceiver & ":ZZ+4:" & sTemp) '18 = Unspecified error
                            Else
                                WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & ":ZZ+" & sIdentReceiver & ":ZZ+7")
                            End If
                        Else
                            If oBabelFile.RejectsExists Then
                                WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & ":ZZ+4:" & sStatusCode)
                            Else
                                WriteSegment("UCI+" & sOriginalUNBReference & "+" & sIdentSender & "+" & sIdentReceiver & ":ZZ+7")
                            End If
                        End If
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment

                    Next oBatch

                    WriteUNTSegment()

                Next oBabelFile

                WriteUNZSegment(sUNBReference)

            Case "CREMUL"

                lUNHCounter = 0

                bUNAWritten = False
                bUNBWritten = False
                bNewMessage = True

                For Each oBabelFile In oBabelFiles

                    bUNHWritten = False

                    If oBabelFile.BabelfileContainsPaymentsAccordingToQualifiers(iFilenameInNo, iFilesetupOut, False, sClientNoToBeExported, True, "") Then

                        If Not bUNAWritten Then
                            sElementSep = ":"
                            sSegmentSep = "'"
                            sGroupSep = "+"
                            sDecimalSep = ","
                            sEscape = "?"
                            sSegmentLen = CStr(3)
                            WriteSegment("UNA" & sElementSep & sGroupSep & sDecimalSep & sEscape & " ")
                            bUNAWritten = True
                        End If

                        If Not bUNBWritten Then
                            If Not EmptyString(oBabelFile.EDI_MessageNo) Then
                                If sSpecial = "GJENSIDIGE_FDC" Then
                                    'Max 14 characters and only digits
                                    sTemp = oBabelFile.EDI_MessageNo
                                    sTemp2 = ""
                                    For lCounter = Len(sTemp) To 1 Step -1
                                        If vbIsNumeric(Mid(sTemp, lCounter, 1), "0123456789") Then
                                            sTemp2 = Mid(sTemp, lCounter, 1) & sTemp2
                                        End If
                                        If Len(sTemp2) > 9 Then
                                            Exit For
                                        End If
                                    Next lCounter
                                    sUNBReference = sTemp2
                                    sTemp = ""
                                    sTemp2 = ""
                                Else
                                    sUNBReference = Left(Trim(oBabelFile.EDI_MessageNo), 35)
                                End If
                            ElseIf EmptyString(sUNBSegment) Then 'The imported file has no unique message ID
                                sUNBReference = CreateBGMReference(oBabelFile)
                            Else
                                iPos1 = 1
                                iPos2 = 1
                                sTemp = vbNullString
                                For lCounter = 1 To 5
                                    iPos1 = iPos2
                                    iPos2 = iPos2 + 1
                                    iPos2 = InStr(iPos2, sUNBSegment, "+", vbTextCompare)
                                Next lCounter
                                sTemp = Mid$(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                                sUNBReference = Mid$(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", vbTextCompare) - iPos2 - 1)
                            End If
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                            ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                            If Not EmptyString(sCompanyNo) Then
                                sIdentReceiver = Trim$(sCompanyNo)
                            ElseIf Not EmptyString(oBabelFile.IDENT_Receiver) Then
                                sIdentReceiver = Trim$(oBabelFile.IDENT_Receiver)
                            Else
                                Err.Raise(1, "CreateEDIFile", "No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer.")
                            End If

                            'If sSpecial = "GJENSIDIGE_FDC" Then
                            '    sIdentReceiver = Replace(sIdentReceiver, "-", "")
                            'End If

                            'WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++CONTRL")

                            'Add date and time
                            sTemp = Format(Now(), "yyMMdd") & ":" & Format(Now(), "hhmm")

                            If oBabelFile.IDENT_Sender = "NORDEATEST" Then
                                WriteSegment("UNB+UNOC:3+NORDEATEST:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++CREMUL")
                            Else
                                WriteSegment("UNB+UNOC:3+NORDEAPROD:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++CREMUL")
                            End If

                            bUNBWritten = True
                        End If

                        If Not bUNHWritten Then
                            lSegmentCounter = 0
                            lLINCounter = 0 '04.09.2008, moved from above
                            'Used for BI - an alternative is to use
                            'If sSpecial = "HANDELSHOYSKOLENBI" then
                            lUNHCounter = lUNHCounter + 1

                            'If oBabelFile.ImportFormat = FileType.CREMUL And Not EmptyString(oBabelFile.UNHSegmentet) Then
                            '    WriteSegment Left$(oBabelFile.UNHSegmentet, Len(oBabelFile.UNHSegmentet) - 1)  '-1 to remove the '
                            'Else
                            WriteSegment("UNH+" & Trim(Str(lUNHCounter)) & "+CREMUL:D:96A:UN")
                            'End If

                            If sSpecial = "GJENSIDIGE_FDC" Then
                                sBGMReference = sUNBReference
                            Else
                                If EmptyString((oBabelFile.EDI_MessageNo)) Then
                                    'To create the unique message_ID use the total amount
                                    sBGMReference = CreateMessageReference(oBabelFile)
                                Else
                                    sBGMReference = oBabelFile.EDI_MessageNo
                                End If
                            End If
                            WriteSegment("BGM+435+" & sBGMReference)

                            If EmptyString((oBabelFile.DATE_Production)) Or oBabelFile.DATE_Production = "19900101" Then
                                WriteDTMSegment("137", DateToString(Now), "102")
                            Else
                                WriteDTMSegment("137", (oBabelFile.DATE_Production), "102")
                            End If
                            WriteSegment("FII+MS++NDEASESS")
                            bUNHWritten = True

                            '3-NAD - Not implemented
                        End If

                        For Each oBatch In oBabelFile.Batches

                            If oBatch.BatchContainsPaymentsAccordingToQualifiers(iFilesetupOut, True, sClientNoToBeExported, nTemp, True, "") Then

                                bNewLIN = True

                                For Each oPayment In oBatch.Payments

                                    If oPayment.PaymentIsAccordingToQualifiers(iFilesetupOut, True, sClientNoToBeExported, True) Then

                                        If oPayment.VB_ProfileInUse Then
                                            If oPayment.I_Account <> sOldAccountNo Then
                                                bFoundClient = False
                                                sCountryCodeFromSetup = ""
                                                If oPayment.VB_ProfileInUse Then 'If FalseOCR then calculate the Oppdragsnr.
                                                    '        'Have to find the Avtale_ID when the import-format isn't OCR
                                                    '        sContractNo = ""
                                                    '        bFoundContractNo = False
                                                    For Each oFileSetup In oBatch.VB_Profile.FileSetups
                                                        For Each oClient In oFileSetup.Clients
                                                            bClientExists = True
                                                            sAccountCurrencyFromSetup = ""
                                                            For Each oAccount In oClient.Accounts
                                                                sOldAccountNo = oPayment.I_Account
                                                                If oAccount.Account = oPayment.I_Account Then
                                                                    bFoundClient = True                                                                '                        
                                                                ElseIf Not EmptyString((oAccount.ConvertedAccount)) And (Trim(oAccount.ConvertedAccount) = oPayment.I_Account) Then
                                                                    bFoundClient = True
                                                                End If

                                                                If bFoundClient Then
                                                                    sAccountCurrencyFromSetup = oAccount.CurrencyCode
                                                                    sCountryCodeFromSetup = oAccount.AccountCountryCode 'TODO: - Is this correct
                                                                    sI_SWIFT_FromSetup = Trim(oAccount.SWIFTAddress)
                                                                    Exit For
                                                                End If
                                                            Next oAccount
                                                            If bFoundClient Then
                                                                Exit For
                                                            End If
                                                        Next oClient

                                                        If bFoundClient Then
                                                            Exit For
                                                        End If
                                                    Next oFileSetup

                                                    If Not bFoundClient Then
                                                        'sEnglish = "35028: The account %1 is not registered in BabelBank." & vbLf & "You must enter this in the client-setup part of BabelBank."
                                                        Err.Raise(35028, "CreateNordea eGateway CREMUL", LRS(35028, oPayment.I_Account))
                                                    End If

                                                    If Len(sI_SWIFT_FromSetup) <> 8 And Len(sI_SWIFT_FromSetup) <> 11 Then
                                                        '"15181: A valid BIC/SWIFT-code must be stated for the payers account number." & vbCrLf & _
                                                        '"It must either be stated in the paymentfile or in the clientsetup of BabelBank." & vbCrLf & "Swift stated: %1"
                                                        If EmptyString(sI_SWIFT_FromSetup) Then
                                                            sI_SWIFT_FromSetup = "Nothing"
                                                        End If
                                                        Err.Raise(15181, "CreateNordea eGateway CREMUL", LRS(15181, sI_SWIFT_FromSetup, oPayment.I_Account))
                                                    Else
                                                        eCountryName = FindCountryNameFromSWIFT(sI_SWIFT_FromSetup)
                                                    End If

                                                    If EmptyString(sCompanyNo) Then
                                                        If bFoundClient Then
                                                            'sName = ValidateISO20022Characters(oClient.Name)
                                                            sCompanyNo = oClient.CompNo
                                                            'sDivision = oClient.Division
                                                        ElseIf bClientExists Then
                                                            sCompanyNo = ""
                                                        Else
                                                            'sName = ValidateISO20022Characters(oPayment.VB_Profile.CompanyName)
                                                            sCompanyNo = oPayment.VB_Profile.CompanyNo
                                                        End If
                                                        If EmptyString(sCompanyNo) Then
                                                            Err.Raise(35073, "CreateNordea eGateway CREMUL", LRS(35073, oPayment.I_Account))
                                                        End If
                                                    End If
                                                End If

                                            End If 'If oPayment.I_Account <> sOldAccountNo Then
                                        End If 'If oPayment.VB_Profile

                                        If bNewLIN Then
                                            lSEQCounter = 0
                                            '4-LIN
                                            WriteLINSegment()

                                            '4-DTM (Date is stored in the Payment-object
                                            If oPayment.DATE_Payment = "19900101" Then
                                                WriteDTMSegment("202", VB6.Format(Now, "YYYYMMDD"), "102")
                                            Else
                                                WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                            End If
                                            If Not oPayment.DATE_Value = "19900101" Then
                                                WriteDTMSegment("209", (oPayment.DATE_Value), "102")
                                            End If

                                            '4-BUS
                                            '*AAB Incoming international payments
                                            '*AAE Incoming OCR-payments
                                            '*AAH Incoming paperbased payments
                                            '*AAW Incoming electronic payments
                                            'BGI(Bankgiro)
                                            'CON Cash Pool transactions
                                            'DDT Direct debit transactions
                                            'PGI(TotalIN)
                                            'POS Point of sale (card trans.) (DK, FI, SE)
                                            'REC Returned money order/cheque(FI, SE)
                                            'RET(Corrections(FI))
                                            'TRF(Transfer)
                                            bThisIsBankgiro = False
                                            If oPayment.PayType = "I" Then
                                                WriteBUSSegment(oPayment.PayType, "AAE", "CREMUL")
                                            Else
                                                sTemp = ""
                                                If eCountryName = BabelFiles.CountryName.Sweden And oPayment.PayType <> "I" And sI_SWIFT_FromSetup <> "PGSISESS" Then
                                                    'Difference between structured/OCR and message?
                                                    If IsAutogiro(oPayment.PayCode) Then
                                                        WriteSegment("BUS+1:ZZZ:::XAP+DO++DDT")
                                                        sTemp = ""
                                                    Else
                                                        bThisIsBankgiro = True
                                                        sTemp = "BGI"
                                                    End If
                                                Else
                                                    If IsOCR(oPayment.PayCode) Then
                                                        sTemp = "AAE"
                                                    ElseIf oPayment.PayCode = "602" Then
                                                        sTemp = "AAH"
                                                    ElseIf oPayment.PayCode = "601" Then
                                                        sTemp = "AAW"
                                                    Else
                                                        sTemp = "TRF"
                                                    End If
                                                End If
                                                If sTemp <> "" Then 'If "", the the segment is already written.
                                                    WriteBUSSegment(oPayment.PayType, sTemp)
                                                End If
                                            End If

                                            If Len(oPayment.MON_InvoiceCurrency) = 3 Then
                                                sLINCurrency = oPayment.MON_InvoiceCurrency
                                            ElseIf Len(oPayment.MON_TransferCurrency) = 3 Then
                                                sLINCurrency = oPayment.MON_TransferCurrency
                                            ElseIf oPayment.PayType <> "I" Then
                                                sLINCurrency = sAccountCurrencyFromSetup
                                            End If

                                            '4-MOA
                                            WriteMOASegment("60", oBatch.MON_InvoiceAmount, sLINCurrency)

                                            '5-RFF
                                            If Len(oBatch.REF_Bank) > 0 Then
                                                WriteRFFSegment("ACK", CheckForValidCharacters(Trim(oBatch.REF_Bank), True, True, True, True, " /-?:().,'+"))
                                            Else
                                                WriteRFFSegment("ACK", sUNBReference & PadLeft(Trim(Str(lUNHCounter)), 4, "0") & PadLeft(Trim(Str(lLINCounter)), 8, "0"), 35)
                                            End If

                                            '5-DTM The date the reference is created (not a part of the BB collection, use valuedate
                                            'Not implemented
                                            'If oPayment.DATE_Value <> "19900101" Then
                                            '    WriteDTMSegment("171", (oPayment.DATE_Value), "102")
                                            'Else
                                            '    'Don't write a date
                                            'End If

                                            '6-FII
                                            If EmptyString((oPayment.I_Account)) Then
                                                '"15136: Receivers acount are not stated on the payment. Can't create a valid CREMUL-file."
                                                Err.Raise(15136, "CreateEDIFile", LRS(15136) & _
                                                        vbCrLf & LRS(35071, oPayment.E_Name) & _
                                                        vbCrLf & LRS(35002, ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",")) & _
                                                        vbCrLf & LRS(40007) & " " & Trim(oPayment.MON_TransferCurrency) & _
                                                        vbCrLf & vbCrLf & LRS(10016))
                                            Else
                                                'FII+BF+31441709999+NDEASESS+SE'
                                                If EmptyString(sCountryCodeFromSetup) Then
                                                    WriteSegment("FII+BF+" & Trim(oPayment.I_Account) & "+" & sI_SWIFT_FromSetup & "+" & Mid(sI_SWIFT_FromSetup, 5, 2))
                                                Else
                                                    WriteSegment("FII+BF+" & Trim(oPayment.I_Account) & "+" & sI_SWIFT_FromSetup & "+" & sCountryCodeFromSetup)
                                                End If
                                            End If

                                            '7-FCA - Not implemented yet
                                            '7-MOA - Not implemented yet

                                            bNewLIN = False

                                        End If 'If bNewLIN Then

                                        'Start writing SEQ information
                                        '10-SEQ
                                        WriteSEQSegment()

                                        '10-DTM
                                        If oPayment.DATE_Payment <> "19900101" Then
                                            WriteDTMSegment("202", (oPayment.DATE_Payment), "102")
                                        Else
                                            WriteDTMSegment("202", VB6.Format(Now, "YYYYMMDD"), "102")
                                        End If
                                        If oPayment.DATE_Value <> "19900101" Then
                                            WriteDTMSegment("209", (oPayment.DATE_Value), "102")
                                        Else
                                            'Don't write a date
                                        End If
                                        If oPayment.PayType = "I" Then
                                            If eCountryName = BabelFiles.CountryName.Sweden Then
                                                If oPayment.DATE_Payment <> "19900101" Then
                                                    WriteDTMSegment("193", (oPayment.DATE_Value), "102")
                                                Else
                                                    WriteDTMSegment("193", VB6.Format(Now, "YYYYMMDD"), "102")
                                                End If
                                            End If
                                        End If

                                        '10-BUS
                                        'Not implemented yet
                                        'This segment is only used for transactions through TotalIN. It is used to specify whether the transaction is domestic or international.

                                        '10-FII
                                        WriteFIISegment(E_Info, True, (oPayment.I_CountryCode), False, True, , "OR")

                                        'Intermediary bank (3035=I1) can only be present for international payments to Denmark or Sweden, 
                                        '  or for PlusGiro transactions from the GiroDirekt service (PGD).
                                        If oPayment.PayType = "I" Then
                                            If eCountryName = BabelFiles.CountryName.Denmark Or eCountryName = BabelFiles.CountryName.Sweden Then
                                                If Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Then
                                                    WriteSegment("FII+I1++" & oPayment.BANK_SWIFTCodeCorrBank)
                                                End If
                                            End If
                                        End If

                                        '11-RFF
                                        If Not EmptyString((oPayment.REF_Bank1)) Then
                                            WriteRFFSegment("AEK", CheckForValidCharacters(Trim(oPayment.REF_Bank1), True, True, True, True, " /-?:().,'+"))
                                        Else
                                            WriteRFFSegment("ZZZ", sUNBReference & PadLeft(Trim(Str(lUNHCounter)), 4, "0") & PadLeft(Trim(Str(lLINCounter)), 6, "0") & PadLeft(Trim(Str(lSEQCounter)), 6, "0"), 35)
                                        End If
                                        If Not EmptyString(oPayment.REF_Bank2) Then
                                            WriteRFFSegment("ACD", CheckForValidCharacters(Trim(oPayment.REF_Bank2), True, True, True, True, " /-?:().,'+"))
                                        End If
                                        If Not EmptyString(oPayment.REF_Own) Then
                                            WriteRFFSegment("AHK", CheckForValidCharacters(Trim(oPayment.REF_Own), True, True, True, True, " /-?:().,'+"))
                                        End If
                                        If Not EmptyString(oPayment.REF_EndToEnd) Then
                                            WriteRFFSegment("PQ", CheckForValidCharacters(Trim(oPayment.REF_EndToEnd), True, True, True, True, " /-?:().,'+"))
                                        End If

                                        '13-MOA
                                        '60: Final (posted) amount
                                        '9: Amount(payable)
                                        '36: Converted(amount)
                                        '98: Original(amount)
                                        '143: Transfer(amount)
                                        WriteMOASegment("60", (oPayment.MON_AccountAmount), "")
                                        If Not (sSpecial = "GJENSIDIGE_FDC" And oPayment.PayType <> "I") Then
                                            If oPayment.MON_TransferredAmount <> 0 Then
                                                WriteMOASegment("143", oPayment.MON_TransferredAmount, oPayment.MON_TransferCurrency)
                                            End If
                                            If oPayment.MON_InvoiceAmount <> 0 Then
                                                WriteMOASegment("36", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency)
                                            End If
                                            If oPayment.MON_OriginallyPaidAmount <> 0 Then
                                                WriteMOASegment("98", oPayment.MON_OriginallyPaidAmount, oPayment.MON_OriginallyPaidCurrency)
                                            End If
                                        End If

                                        '13-CUX
                                        If oPayment.MON_AccountExchRate > 0 Then
                                            WriteCUXSegment(oPayment.MON_AccountCurrency, oPayment.MON_TransferCurrency, oPayment.MON_AccountExchRate)
                                        End If

                                        '13-RFF - Not implemented yet
                                        'If Not EmptyString(oPayment.FRW_ForwardContractNo) Then
                                        '    WriteRFFSegment("FX", Trim$(oPayment.FRW_ForwardContractNo))
                                        'End If

                                        '14-NAD
                                        If Not EmptyString(oPayment.E_Zip) Then
                                            WriteNADSegment(True, True, "PL", , True)
                                        Else
                                            WriteNADSegment(True, False, "PL", , True)
                                        End If
                                        If Not EmptyString((oPayment.I_Name)) Then
                                            If Not EmptyString(oPayment.I_Zip) Then
                                                WriteNADSegment(False, True, "BE")
                                            Else
                                                'Beneficiary countrycode is mandatory in WriteNAD for international payments, but not in this case with incoming payments
                                                If oPayment.PayType = "I" And oPayment.I_CountryCode.Length <> 2 Then
                                                    WriteNADSegment(False, False, "BE", True)
                                                Else
                                                    WriteNADSegment(False, False, "BE")
                                                End If
                                            End If
                                        End If

                                        '15-INP
                                        'If Not EmptyString((oPayment.Text_I_Statement)) Then
                                        '    WriteSegment("INP+BF+2:SI")
                                        '    '15-FTX
                                        '    WriteFTXSegment("AAG", (oPayment.Text_I_Statement))
                                        'End If

                                        '16-GIS
                                        '16-FTX
                                        'A group of segments providing information for subsequent use by regulatory authorities requiring statistical and other types of data.
                                        If oPayment.PayType = "I" Then
                                            If eCountryName = BabelFiles.CountryName.Norway Or eCountryName = BabelFiles.CountryName.Sweden Then
                                                bStateBankInfoFound = False
                                                sStateBankCode = ""
                                                sStateBankText = ""
                                                For Each oInvoice In oPayment.Invoices
                                                    'We store statbank info in Invoice, but ISO20022 its's written on the 'payment' level.
                                                    If Not EmptyString(oInvoice.STATEBANK_Code) Then
                                                        sStateBankCode = Trim(oInvoice.STATEBANK_Code)
                                                        bStateBankInfoFound = True
                                                    End If
                                                    If Not EmptyString(oInvoice.STATEBANK_Text) Then
                                                        sStateBankText = Trim(oInvoice.STATEBANK_Text)
                                                        bStateBankInfoFound = True
                                                    End If
                                                    If bStateBankInfoFound Then
                                                        Exit For
                                                    End If
                                                Next
                                                If bStateBankInfoFound Then
                                                    WriteSegment("GIS+10")
                                                    'FTX+REG++14+Fish export'
                                                    WriteFTXSegment("REG", sStateBankText, sStateBankCode)
                                                End If
                                            End If
                                        End If

                                        '17-FCA
                                        If oPayment.PayType = "I" Then
                                            WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True)
                                        End If

                                        '18-ALC - Not implemented yet
                                        '18-MOA - Not implemented(yet)
                                        '05.12.2019 - Fixed error, changed from oPayment.PayType = '629' to oPayment.PayCode = '629' 
                                        If IsOCR((oPayment.PayCode)) Or oPayment.PayCode = "629" Or IsAutogiro(oPayment.PayCode) Then
                                            If oPayment.PayType <> "I" Then
                                                Select Case eCountryName

                                                    Case BabelFiles.CountryName.Norway
                                                        bStructured = True
                                                    Case BabelFiles.CountryName.Sweden
                                                        bStructured = True
                                                    Case BabelFiles.CountryName.Finland
                                                        bStructured = True
                                                    Case BabelFiles.CountryName.Denmark
                                                        bStructured = True
                                                    Case Else
                                                        bStructured = False
                                                End Select
                                            Else
                                                bStructured = False
                                            End If
                                        Else
                                            bStructured = False
                                        End If
                                        If bStructured Then
                                            '20-PRC
                                            'This is for Sweden
                                            WriteSegment("PRC+8")
                                            For Each oInvoice In oPayment.Invoices

                                                '21-DOC
                                                If Not EmptyString((oInvoice.Unique_Id)) Then
                                                    If oInvoice.MON_InvoiceAmount > 0 Then
                                                        WriteDOCSegment("YW3", (oInvoice.Unique_Id))
                                                    Else
                                                        WriteDOCSegment("420", (oInvoice.Unique_Id))
                                                    End If
                                                Else
                                                    If Not EmptyString(oInvoice.InvoiceNo) Then
                                                        If oInvoice.MON_InvoiceAmount > 0 Then
                                                            WriteDOCSegment("380", oInvoice.InvoiceNo)
                                                        Else
                                                            WriteDOCSegment("381", oInvoice.InvoiceNo)
                                                        End If
                                                    Else
                                                        WriteSegment("DOC+380")
                                                    End If
                                                End If

                                                '21-MOA
                                                WriteMOASegment("9", System.Math.Abs(oInvoice.MON_InvoiceAmount), "", True)

                                                '21-DTM
                                                'A segment specifying the date of the referenced document.
                                                'NOT IMPLEMENTED YET

                                                '21-RFF
                                                'NOT IMPLEMENTED YET
                                                'Should be used for Bankgiro in Sweden

                                                '21-FTX
                                                sTemp = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    sTemp = sTemp & " " & Trim(oFreetext.Text)
                                                Next oFreetext
                                                sTemp = Trim(sTemp)
                                                If Not EmptyString(sTemp) Then
                                                    If (Left(sTemp, 6) = "Bel�p:" And InStr(sTemp, "/") > 0) Or IsAutogiro(oPayment.PayCode) Then
                                                        'Don't write any freetext if there is added standard BB-info
                                                    Else
                                                        WriteFTXSegment("PMD", sTemp)
                                                    End If
                                                End If
                                            Next oInvoice

                                        Else
                                            '20-PRC
                                            WriteSegment("PRC+11")
                                            '20-FTX
                                            sTemp = ""
                                            For Each oInvoice In oPayment.Invoices
                                                For Each oFreetext In oInvoice.Freetexts
                                                    sTemp = sTemp & " " & Trim(oFreetext.Text)
                                                Next oFreetext
                                            Next oInvoice
                                            If Not EmptyString(sTemp) Then
                                                WriteFTXSegment("PMD", sTemp)
                                            End If
                                        End If

                                        '27-GIS
                                        WriteSegment("GIS+37")

                                        oPayment.Exported = True

                                    End If 'If oPayment.PaymentIsAccordingToQualifiers

                                Next oPayment

                            End If 'If oBatch.BatchContainsPaymentsAccordingToQualifiers

                        Next oBatch

                        WriteSegment("CNT+2:" & Trim(Str(lLINCounter)))
                        WriteUNTSegment()
                    End If 'If oBabel.BabelfileContainsPaymentsAccordingToQualifiers
                Next oBabelFile
                WriteUNZSegment(sUNBReference)

            Case "DEBMUL"

                WriteSegment("UNA:+,? ")

                sElementSep = ":" 'mrEDISettingsExport.CurrentElement.getAttribute("elementsep")
                sSegmentSep = "'" 'mrEDISettingsExport.CurrentElement.getAttribute("segmentsep")
                sGroupSep = "+" 'mrEDISettingsExport.CurrentElement.getAttribute("groupsep")
                sDecimalSep = "," 'mrEDISettingsExport.CurrentElement.getAttribute("decimalsep")
                sEscape = "?" 'mrEDISettingsExport.CurrentElement.getAttribute("escape")
                sSegmentLen = CStr(3) 'mrEDISettingsExport.CurrentElement.getAttribute("segmentlen")

                bUNBWritten = False
                bNewMessage = True

                lUNHCounter = 0

                sOldAccountNo = vbNullString
                bSequenceNoFound = False

                For Each oBabelFile In oBabelFiles

                    sUNBSegment = oBabelFile.UNBSegmentet
                    sUNHSegment = oBabelFile.UNHSegmentet

                    If Not bUNBWritten Then
                        If EmptyString(sUNBSegment) Then 'The imported file has no unique message ID
                            If EmptyString(oBabelFile.EDI_MessageNo) Then
                                sUNBReference = CreateBGMReference(oBabelFile)
                            Else
                                sUNBReference = oBabelFile.EDI_MessageNo
                            End If
                        Else
                            iPos1 = 1
                            iPos2 = 1
                            sTemp = vbNullString
                            For lCounter = 1 To 5
                                iPos1 = iPos2
                                iPos2 = iPos2 + 1
                                iPos2 = InStr(iPos2, sUNBSegment, "+", vbTextCompare)
                            Next lCounter
                            sTemp = Mid$(sUNBSegment, iPos1 + 1, iPos2 - iPos1 - 1)
                            sUNBReference = Mid$(sUNBSegment, iPos2 + 1, InStr(iPos2 + 1, sUNBSegment, "+", vbTextCompare) - iPos2 - 1)
                        End If
                        'sCompanyNo is found in BabelBank.EXE.PrepareExport. If a CompanyNo is stated
                        ' in FileSetup it will be used. If not stated in FileSetup Company.CompanyNo is used
                        If Not EmptyString(sCompanyNo) Then
                            sIdentReceiver = Trim$(sCompanyNo)
                        ElseIf Not EmptyString(oBabelFile.IDENT_Receiver) Then
                            sIdentReceiver = Trim$(oBabelFile.IDENT_Receiver)
                        Else
                            Err.Raise(1, "CreateEDIFile", "No receiverident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "Contact Your dealer.")
                        End If

                        'If sSpecial = "GJENSIDIGE_FDC" Then
                        '    sIdentReceiver = Replace(sIdentReceiver, "-", "")
                        'End If

                        'Add date and time
                        sTemp = Format(Now(), "yyMMdd") & ":" & Format(Now(), "hhmm")

                        If oBabelFile.IDENT_Sender = "NORDEATEST" Then
                            WriteSegment("UNB+UNOC:3+NORDEATEST:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++DEBMUL")
                        Else
                            WriteSegment("UNB+UNOC:3+NORDEAPROD:ZZ+" & sIdentReceiver & ":ZZ+" & sTemp & "+" & sUNBReference & "++DEBMUL")
                        End If

                        'WriteSegment("UNB+UNOC:3+00810506482+" & sIdentReceiver & "+" & sTemp & "+" & sUNBReference & "++DEBMUL")
                        bUNBWritten = True
                    End If

                    lSegmentCounter = 0
                    lLINCounter = 0 '04.09.2008, moved from above

                    lUNHCounter = lUNHCounter + 1
                    WriteSegment("UNH+" & Trim$(Str(lUNHCounter)) & "+DEBMUL:D:96A:UN")
                    'To create the unique message_ID use the total amount

                    If Not EmptyString(oBabelFile.EDI_MessageNo) Then
                        sBGMReference = oBabelFile.EDI_MessageNo
                    Else
                        sBGMReference = CreateBGMReference(oBabelFile)
                    End If
                    WriteSegment("BGM+456+" & sBGMReference)
                    If EmptyString(oBabelFile.DATE_Production) Or oBabelFile.DATE_Production = "19900101" Then
                        WriteDTMSegment("137", DateToString(Now()), "102")
                    Else
                        WriteDTMSegment("137", oBabelFile.DATE_Production, "102")
                    End If
                    WriteSegment("FII+MS++NDEASESS")

                    For Each oBatch In oBabelFile.Batches

                        bLINWritten = False
                        For Each oPayment In oBatch.Payments

                            If oPayment.VB_ProfileInUse Then
                                If oPayment.I_Account <> sOldAccountNo Then
                                    bFoundClient = False
                                    sCountryCodeFromSetup = ""
                                    If oPayment.VB_ProfileInUse Then 'If FalseOCR then calculate the Oppdragsnr.
                                        '        'Have to find the Avtale_ID when the import-format isn't OCR
                                        '        sContractNo = ""
                                        '        bFoundContractNo = False
                                        For Each oFileSetup In oBatch.VB_Profile.FileSetups
                                            For Each oClient In oFileSetup.Clients
                                                bClientExists = True
                                                sAccountCurrencyFromSetup = ""
                                                For Each oAccount In oClient.Accounts
                                                    sOldAccountNo = oPayment.I_Account
                                                    If oAccount.Account = oPayment.I_Account Then
                                                        bFoundClient = True                                                                '                        
                                                    ElseIf Not EmptyString((oAccount.ConvertedAccount)) And (Trim(oAccount.ConvertedAccount) = oPayment.I_Account) Then
                                                        bFoundClient = True
                                                    End If

                                                    If bFoundClient Then
                                                        sAccountCurrencyFromSetup = oAccount.CurrencyCode
                                                        sCountryCodeFromSetup = oAccount.DebitAccountCountryCode 'TODO: - Is this correct
                                                        sI_SWIFT_FromSetup = Trim(oAccount.SWIFTAddress)
                                                        Exit For
                                                    End If
                                                Next oAccount
                                                If bFoundClient Then
                                                    Exit For
                                                End If
                                            Next oClient

                                            If bFoundClient Then
                                                Exit For
                                            End If
                                        Next oFileSetup

                                        If Len(sI_SWIFT_FromSetup) <> 8 And Len(sI_SWIFT_FromSetup) <> 11 Then
                                            '"15144: A valid BIC/SWIFT-code must be stated for the payers account number." & vbCrLf & _
                                            '"It must either be stated in the paymentfile or in the clientsetup of BabelBank." & vbCrLf & "Swift stated: %1"
                                            Err.Raise(15144, "Nordea eGateway DEBMUL", LRS(15144, sI_SWIFT_FromSetup))
                                        Else
                                            eCountryName = FindCountryNameFromSWIFT(sI_SWIFT_FromSetup)
                                        End If

                                        If bFoundClient Then
                                            'sName = ValidateISO20022Characters(oClient.Name)
                                            sCompanyNo = oClient.CompNo
                                            'sDivision = oClient.Division
                                        ElseIf bClientExists Then
                                            sCompanyNo = ""
                                        Else
                                            'sName = ValidateISO20022Characters(oPayment.VB_Profile.CompanyName)
                                            sCompanyNo = oPayment.VB_Profile.CompanyNo
                                        End If
                                        If EmptyString(sCompanyNo) Then
                                            Err.Raise(35073, "WriteISO_20022Nordea", LRS(35073, oPayment.I_Account))
                                        End If
                                    End If

                                End If 'If oPayment.I_Account <> sOldAccountNo Then
                            End If 'If oPayment.VB_Profile

                            If Not bLINWritten Then
                                '4-LIN
                                lSEQCounter = 0
                                WriteLINSegment()
                                '4-DTM
                                If oPayment.DATE_Value = "19900101" Then
                                    'WriteDTMSegment "209", DateToString(Now()), "102"
                                Else
                                    WriteDTMSegment("209", oPayment.DATE_Value, "102")
                                End If
                                If oPayment.DATE_Payment = "19900101" Then
                                    WriteDTMSegment("202", DateToString(Now()), "102")
                                Else
                                    WriteDTMSegment("202", oPayment.DATE_Payment, "102")
                                End If
                                '4-BUS
                                'Should we add code for bankoperation? PayCode
                                'AAC Outgoing international payments BGI Bank-/bankgiro transfer
                                'CON Internal Cash Pool transactions
                                'DDT Direct debit transactions
                                'MSC(Miscellaneous)
                                'PGI Plusgiro transfer
                                'RET Returned items
                                'TRF(Transfer)
                                If oPayment.PayType = "I" Then
                                    WriteSegment("BUS++IN")
                                Else
                                    If oPayment.I_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
                                        WriteSegment("BUS++DO++PGI")
                                    ElseIf oPayment.I_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
                                        WriteSegment("BUS++DO++BGI")
                                    Else
                                        WriteSegment("BUS++DO++BGI")
                                    End If
                                End If
                                '4-MOA
                                WriteMOASegment("60", oBatch.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency, True)
                                '5-RFF
                                If Not EmptyString(oBatch.REF_Bank) Then
                                    WriteRFFSegment("AEK", oBatch.REF_Bank)
                                Else
                                    WriteRFFSegment("ZZZ", sBGMReference & "-" & lLINCounter.ToString)
                                End If
                                If Not EmptyString(oBatch.REF_Own) Then
                                    WriteRFFSegment("ACK", oBatch.REF_Own)
                                End If
                                '6-FII
                                If Not EmptyString(oPayment.I_Account) Then
                                    'May retrieve SWIFTADDRESS and COUNTRYCODE from the setup.
                                    sTemp = ""
                                    If Not EmptyString(oPayment.BANK_I_SWIFTCode) Then
                                        sTemp = oPayment.BANK_I_SWIFTCode
                                    ElseIf Not EmptyString(sI_SWIFT_FromSetup) Then
                                        sTemp = sI_SWIFT_FromSetup
                                    End If
                                    If Not EmptyString(sTemp) Then
                                        If Not EmptyString(sCountryCodeFromSetup) Then
                                            WriteSegment("FII+OR+" & oPayment.I_Account & "+" & sTemp & "+" & sCountryCodeFromSetup)
                                        Else
                                            WriteSegment("FII+OR+" & oPayment.I_Account & "+" & sTemp)
                                        End If
                                    Else
                                        WriteSegment("FII+OR+" & oPayment.I_Account)
                                    End If
                                End If
                                '7-FCA - Not implemented
                                '7-MOA - Not implemented
                                bLINWritten = True
                            End If
                            '10-SEQ
                            WriteSEQSegment()
                            '10-DTM
                            If oPayment.DATE_Value = "19900101" Then
                                'WriteDTMSegment "209", DateToString(Now()), "102"
                            Else
                                WriteDTMSegment("209", oPayment.DATE_Value, "102")
                            End If
                            If oPayment.DATE_Payment = "19900101" Then
                                'WriteDTMSegment("202", DateToString(Now()), "102")
                            Else
                                WriteDTMSegment("202", oPayment.DATE_Payment, "102")
                            End If
                            If oPayment.PayType = "I" Then
                                If oPayment.DATE_Payment = "19900101" Then
                                    WriteDTMSegment("193", DateToString(Now()), "102")
                                Else
                                    WriteDTMSegment("193", oPayment.DATE_Payment, "102")
                                End If
                            End If

                            '(10-BUS)

                            '(10-FII)
                            If EmptyString(oPayment.E_Account) Then
                                WriteSegment("FII+BF")
                            Else
                                WriteFIISegment(E_Info, True, oPayment.I_CountryCode, , False)
                            End If

                            If Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Then
                                WriteSegment("FII+I1++" & Trim(oPayment.BANK_SWIFTCodeCorrBank))
                            End If

                            If Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Or Not EmptyString(oPayment.BANK_NameAddressCorrBank1) Then
                                WriteFIISegment(CorrBank, True, oPayment.I_CountryCode, , False)
                            End If

                            '11-RFF
                            bRFFFound = False
                            If Not EmptyString(oPayment.REF_Own) Then
                                WriteRFFSegment("CR", Trim$(oPayment.REF_Own))
                            End If
                            If Not EmptyString(oPayment.REF_Bank1) Then
                                WriteRFFSegment("AIK", Trim$(oPayment.REF_Bank1))
                                bRFFFound = True
                            End If
                            If Not EmptyString(oPayment.REF_Bank2) Then
                                WriteRFFSegment("ACD", Trim$(oPayment.REF_Bank2))
                                bRFFFound = True
                            End If

                            If Not bRFFFound Then
                                WriteRFFSegment("ZZZ", sBGMReference)
                                bRFFFound = True
                            End If

                            '13-MOA

                            If oPayment.PayType = "I" Then
                                'Currency stated for international payments, not for domestic
                                WriteMOASegment("60", oPayment.MON_AccountAmount, oPayment.MON_AccountCurrency, True)

                                If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    WriteMOASegment("98", oPayment.MON_InvoiceAmount, oPayment.MON_InvoiceCurrency, True)
                                End If

                                If Not EmptyString(oPayment.MON_TransferCurrency) Then
                                    WriteMOASegment("143", oPayment.MON_TransferredAmount, oPayment.MON_TransferCurrency, True)
                                End If

                            Else
                                WriteMOASegment("60", oPayment.MON_AccountAmount, "", True)

                                If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    WriteMOASegment("98", oPayment.MON_InvoiceAmount, "", True)
                                End If

                                If Not EmptyString(oPayment.MON_TransferCurrency) Then
                                    WriteMOASegment("143", oPayment.MON_TransferredAmount, "", True)
                                End If

                            End If

                            '13-CUX
                            If oPayment.MON_AccountExchRate > 0 Then
                                WriteCUXSegment(oPayment.MON_AccountCurrency, oPayment.MON_TransferCurrency, oPayment.MON_AccountExchRate)
                            End If

                            '13-RFF - Not implemented yet
                            If Not EmptyString(oPayment.FRW_ForwardContractNo) Then
                                WriteRFFSegment("FX", Trim$(oPayment.FRW_ForwardContractNo))
                            End If

                            '14-NAD
                            WriteNADSegment(True, True, "BE")

                            If oPayment.PayType = "I" Then
                                If eCountryName = BabelFiles.CountryName.Sweden Then
                                    If oPayment.MON_ChargeMeAbroad = True And oPayment.MON_ChargeMeDomestic = True Then
                                        '17-FCA - Only 15 allowed (for international swedish payments only)
                                        WriteFCASegment(oPayment.MON_ChargeMeDomestic, oPayment.MON_ChargeMeAbroad, True)
                                        '17-ALC - Not implemented yet
                                        '17-MOA
                                        If oPayment.MON_ChargesAmount > 0 Then
                                            WriteMOASegment("58", oPayment.MON_ChargesAmount, oPayment.MON_ChargesCurrency)
                                        End If
                                    End If
                                End If
                            End If

                            '20-PRC
                            If oPayment.PayType = "I" Then
                                If eCountryName = BabelFiles.CountryName.Sweden Then
                                    If Val(oPayment.StatusCode) > 2 Then
                                        WriteSegment("PRC+11")
                                        WriteFTXSegment("AAO", oPayment.StatusCode) 'We should translate the errorcodes
                                    End If
                                End If
                            End If

                            oPayment.Exported = True

                        Next oPayment

                    Next oBatch

                    WriteSegment("CNT+2:" & Trim$(Str(lLINCounter)))
                    WriteUNTSegment()
                Next oBabelFile
                WriteUNZSegment(sUNBReference)

        End Select

        'To write the rest of the file
        WriteSegment(vbNullString, True)

        On Error GoTo 0
        Exit Function

ERR_CreateNordea_eGateway_EDIFile:

        err.Raise(err.Number, "CreateNordea_eGateway_EDIFile", err.Description)

    End Function

    Private Function VismaFokusConvertBANSTACode(ByRef sCode As String) As String
        Dim sReturnCode As String

        Select Case sCode

            Case "K0001"
                sReturnCode = "1SK"
            Case "K0002"
                sReturnCode = "1S"
            Case "K0003"
                sReturnCode = "1SK"
            Case "K0004"
                sReturnCode = "1SK"
            Case "K0005"
                sReturnCode = "1SK"
            Case "K0006"
                sReturnCode = "1SK"
            Case "K0007"
                sReturnCode = "1SK"
            Case "K0008"
                sReturnCode = "1S"
            Case "K0009"
                sReturnCode = "1S"
            Case "K0010"
                sReturnCode = "1SK"
            Case "K0011"
                sReturnCode = "1S"
            Case "K0012"
                sReturnCode = "1S"
            Case "K0013"
                sReturnCode = "1S"
            Case "K0014"
                sReturnCode = "1SK"
            Case "K0015"
                sReturnCode = "1SK"
            Case "K0016"
                sReturnCode = "1SK"
            Case "K0017"
                sReturnCode = "1S"
            Case "K0018"
                sReturnCode = "1SK"
            Case "K0019"
                sReturnCode = "1SK"
            Case "K0020"
                sReturnCode = "1SK"
            Case "K0021"
                sReturnCode = "1SK"
            Case "K0022"
                sReturnCode = "1SK"
            Case "K0023"
                sReturnCode = "1SK"
            Case "K0024"
                sReturnCode = "1S"
            Case "K0025"
                sReturnCode = "1S"
            Case "K0026"
                sReturnCode = "1S"
            Case "K0027"
                sReturnCode = "1SK"
            Case "K0028"
                sReturnCode = "1SK"
            Case "K0029"
                sReturnCode = "1S"
            Case "K0030"
                sReturnCode = "1SK"
            Case "K0031"
                sReturnCode = "1SK"
            Case "K0032"
                sReturnCode = "1S"
            Case "K0033"
                sReturnCode = "1S"
            Case "K0034"
                sReturnCode = "1SK"
            Case "K0035"
                sReturnCode = "1S"
            Case "K0036"
                sReturnCode = "0SK"
            Case "K0037"
                sReturnCode = "1SK"
            Case "K0038"
                sReturnCode = "1S"
            Case "K0039"
                sReturnCode = "1S"
            Case "K0040"
                sReturnCode = "1S"
            Case "K0041"
                sReturnCode = "1SK"
            Case "K0042"
                sReturnCode = "1SK"
            Case "K0043"
                sReturnCode = "1SK"
            Case "K0044"
                sReturnCode = "1S"
            Case "K0045"
                sReturnCode = "1S"
            Case "K0046"
                sReturnCode = "1S"
            Case "K0047"
                sReturnCode = "1S"
            Case "K0048"
                sReturnCode = "1S"
            Case "K0049"
                sReturnCode = "1S"
            Case "K0050"
                sReturnCode = "1S"
            Case "K0051"
                sReturnCode = "1S"
            Case "K0052"
                sReturnCode = "0SK"
            Case "K0053"
                sReturnCode = "1S"
            Case "K0054"
                sReturnCode = "1S"
            Case "K0055"
                sReturnCode = "1S"
            Case "K0056"
                sReturnCode = "1S"
            Case "K0057"
                sReturnCode = "1S"
            Case "K0058"
                sReturnCode = "1S"
            Case "K0059"
                sReturnCode = "1S"
            Case "K0060"
                sReturnCode = "1S"
            Case "K0061"
                sReturnCode = "1S"
            Case "K0062"
                sReturnCode = "1S"
            Case "K0063"
                sReturnCode = "1S"
            Case "K0064"
                sReturnCode = "1S"
            Case "K0065"
                sReturnCode = "1S"
            Case "K0066"
                sReturnCode = "1S"
            Case "K0067"
                sReturnCode = "1S"
            Case "K0068"
                sReturnCode = "1S"
            Case "K0069"
                sReturnCode = "1S"
            Case "K0070"
                sReturnCode = "1S"
            Case "K0071"
                sReturnCode = "1S"
            Case "K0072"
                sReturnCode = "1SK"
            Case "K0073"
                sReturnCode = "1S"
            Case "K0074"
                sReturnCode = "1S"
            Case "K0075"
                sReturnCode = "1S"
            Case "K0076"
                sReturnCode = "1S"
            Case "K0077"
                sReturnCode = "1S"
            Case "K0078"
                sReturnCode = "1S"
            Case "K0079"
                sReturnCode = "1S"
            Case "K0080"
                sReturnCode = "1S"
            Case "K0081"
                sReturnCode = "1S"
            Case "K0082"
                sReturnCode = "1S"
            Case "K0083"
                sReturnCode = "1S"
            Case "K0084"
                sReturnCode = "1S"
            Case "K0085"
                sReturnCode = "1S"
            Case "K0086"
                sReturnCode = "1S"
            Case "K0087"
                sReturnCode = "1S"
            Case "K0088"
                sReturnCode = "1S"
            Case "K0089"
                sReturnCode = "1S"
            Case "K0090"
                sReturnCode = "1S"
            Case "K0091"
                sReturnCode = "1S"
            Case "K0092"
                sReturnCode = "1S"
            Case "K0093"
                sReturnCode = "1S"
            Case "K0094"
                sReturnCode = "1S"
            Case "K0095"
                sReturnCode = "1S"
            Case "K0096"
                sReturnCode = "1S"
            Case "K0097"
                sReturnCode = "1S"
            Case "K0098"
                sReturnCode = "1S"
            Case "K0099"
                sReturnCode = "1S"
            Case "K0100"
                sReturnCode = "1S"
            Case "K0101"
                sReturnCode = "1S"
            Case "K0102"
                sReturnCode = "1S"
            Case "K0103"
                sReturnCode = "1S"
            Case "K0104"
                sReturnCode = "1S"
            Case "K0105"
                sReturnCode = "1S"
            Case "K0106"
                sReturnCode = "1S"
            Case "K0107"
                sReturnCode = "1S"
            Case "K0108"
                sReturnCode = "1SK"
            Case "K0109"
                sReturnCode = "1S"
            Case "K0110"
                sReturnCode = "0A"
            Case "K0111"
                sReturnCode = "0A"
            Case "K0112"
                sReturnCode = "0X"
            Case "K0113"
                sReturnCode = "1SK"
            Case "K0119"
                sReturnCode = "1S"
            Case "K0120"
                sReturnCode = "1S"
            Case "K0121"
                sReturnCode = "1S"
            Case "K0122"
                sReturnCode = "1SK"
            Case "K0123"
                sReturnCode = "1S"
            Case "K0124"
                sReturnCode = "1S"
            Case "K0125"
                sReturnCode = "1S"
            Case "K0126"
                sReturnCode = "1S"
            Case "K0127"
                sReturnCode = "1S"
            Case "K0128"
                sReturnCode = "1S"
            Case "K0129"
                sReturnCode = "1S"
            Case "K0130"
                sReturnCode = "1S"
            Case "K0131"
                sReturnCode = "1S"
            Case "K0132"
                sReturnCode = "1S"
            Case "K0133"
                sReturnCode = "1S"
            Case "K0134"
                sReturnCode = "1S"
            Case "K0135"
                sReturnCode = "1SK"
            Case "K0136"
                sReturnCode = "1S"
            Case "K0137"
                sReturnCode = "1S"
            Case "K0138"
                sReturnCode = "1S"
            Case "K0139"
                sReturnCode = "1SK"
            Case "K0140"
                sReturnCode = "1S"
            Case "K0141"
                sReturnCode = "1S"
            Case "K0142"
                sReturnCode = "1S"
            Case "K0143"
                sReturnCode = "1SK"
            Case "K0144"
                sReturnCode = "1S"
            Case "K0145"
                sReturnCode = "1S"
            Case "K0146"
                sReturnCode = "1S"
            Case "K0147"
                sReturnCode = "1S"
            Case "K0148"
                sReturnCode = "1S"
            Case "K0149"
                sReturnCode = "1S"
            Case "K0150"
                sReturnCode = "1S"
            Case "K0151"
                sReturnCode = "1S"
            Case "K0152"
                sReturnCode = "1S"
            Case "K0153"
                sReturnCode = "1S"
            Case "K0154"
                sReturnCode = "1S"
            Case "K0155"
                sReturnCode = "1S"
            Case "K0156"
                sReturnCode = "1S"
            Case "K0157"
                sReturnCode = "1S"
            Case "K0158"
                sReturnCode = "1SK"
            Case "K0159"
                sReturnCode = "1S"
            Case "K0160"
                sReturnCode = "1S"
            Case "K0161"
                sReturnCode = "1SK"
            Case "K0162"
                sReturnCode = "1S"
            Case "K0163"
                sReturnCode = "1S"
            Case "K0164"
                sReturnCode = "1S"
            Case "K0165"
                sReturnCode = "1S"
            Case "K0166"
                sReturnCode = "1S"
            Case "K0167"
                sReturnCode = "1S"
            Case "K0168"
                sReturnCode = "1S"
            Case "K0169"
                sReturnCode = "1S"
            Case "K0170"
                sReturnCode = "1S"
            Case "K0171"
                sReturnCode = "1S"
            Case "K0172"
                sReturnCode = "1S"
            Case "K0173"
                sReturnCode = "1S"
            Case "K0174"
                sReturnCode = "1S"
            Case "K0175"
                sReturnCode = "1SK"
            Case "K0176"
                sReturnCode = "1S"
            Case "K0177"
                sReturnCode = "1S"
            Case "K0178"
                sReturnCode = "1S"
            Case "K0179"
                sReturnCode = "1SK"
            Case "K0180"
                sReturnCode = "1S"
            Case "K0181"
                sReturnCode = "1S"
            Case "K0182"
                sReturnCode = "1S"
            Case "K0183"
                sReturnCode = "1S"
            Case "K0184"
                sReturnCode = "1S"
            Case "K0185"
                sReturnCode = "1S"
            Case "K0186"
                sReturnCode = "1S"
            Case "K0187"
                sReturnCode = "1S"
            Case "K0188"
                sReturnCode = "1S"
            Case "K0189"
                sReturnCode = "1S"
            Case "K0190"
                sReturnCode = "1S"
            Case "K0191"
                sReturnCode = "1SK"
            Case "K0192"
                sReturnCode = "1S"
            Case "K0193"
                sReturnCode = "1S"
            Case "K0194"
                sReturnCode = "1S"
            Case "K0195"
                sReturnCode = "1S"
            Case "K0196"
                sReturnCode = "1S"
            Case "K0197"
                sReturnCode = "1S"
            Case "K0198"
                sReturnCode = "1S"
            Case "K0199"
                sReturnCode = "1S"
            Case "K0201"
                sReturnCode = "1S"
            Case "K0202"
                sReturnCode = "1S"
            Case "K0203"
                sReturnCode = "1S"
            Case "K0204"
                sReturnCode = "1S"
            Case "K0205"
                sReturnCode = "1S"
            Case "K0206"
                sReturnCode = "1S"
            Case "K0207"
                sReturnCode = "1S"
            Case "K0208"
                sReturnCode = "1S"
            Case "K0209"
                sReturnCode = "1S"
            Case "K0210"
                sReturnCode = "1S"
            Case "K0211"
                sReturnCode = "1S"
            Case "K0212"
                sReturnCode = "1S"
            Case "K0213"
                sReturnCode = "1S"
            Case "K0214"
                sReturnCode = "1S"
            Case "K0215"
                sReturnCode = "0A"
            Case "K0216"
                sReturnCode = "1S"
            Case "K0217"
                sReturnCode = "1S"
            Case "K0218"
                sReturnCode = "1S"
            Case "K0219"
                sReturnCode = "1SK"
            Case "K0220"
                sReturnCode = "1S"
            Case "K0221"
                sReturnCode = "1S"
            Case "K0222"
                sReturnCode = "1S"
            Case "K0223"
                sReturnCode = "1S"
            Case "K0224"
                sReturnCode = "1S"
            Case "K0225"
                sReturnCode = "1S"
            Case "K0226"
                sReturnCode = "1S"
            Case "K0227"
                sReturnCode = "1S"
            Case "K0228"
                sReturnCode = "1S"
            Case "K0229"
                sReturnCode = "1S"
            Case "K0230"
                sReturnCode = "1SK"
            Case "K0231"
                sReturnCode = "1SK"
            Case "K0232"
                sReturnCode = "1S"
            Case "K0233"
                sReturnCode = "1S"
            Case "K0234"
                sReturnCode = "1S"
            Case "K0235"
                sReturnCode = "1S"
            Case "K0236"
                sReturnCode = "1S"
            Case "K0245"
                sReturnCode = "1SK"
            Case "K0246"
                sReturnCode = "1SK"
            Case "K0247"
                sReturnCode = "1SK"
            Case "K0248"
                sReturnCode = "1S"
            Case "K0249"
                sReturnCode = "1SK"
            Case "K0250"
                sReturnCode = "1S"
            Case "K0251"
                sReturnCode = "1SK"
            Case "K0252"
                sReturnCode = "1S"
            Case "K0253"
                sReturnCode = "1S"
            Case "K0254"
                sReturnCode = "1S"
            Case "K0255"
                sReturnCode = "1S"
            Case "K0256"
                sReturnCode = "1S"
            Case "K0257"
                sReturnCode = "1SK"
            Case "K0258"
                sReturnCode = "1S"
            Case "K0259"
                sReturnCode = "1S"
            Case "K0260"
                sReturnCode = "1S"
            Case "K0261"
                sReturnCode = "1SK"
            Case "K0267"
                sReturnCode = "1S"
            Case "K0268"
                sReturnCode = "1S"
            Case "K0269"
                sReturnCode = "1S"
            Case "K0270"
                sReturnCode = "1S"
            Case "K0271"
                sReturnCode = "1S"
            Case "K0272"
                sReturnCode = "1S"
            Case "K0273"
                sReturnCode = "1S"
            Case "K0274"
                sReturnCode = "1S"
            Case "K0275"
                sReturnCode = "1S"
            Case "K0276"
                sReturnCode = "0SK"
            Case "K0277"
                sReturnCode = "1S"
            Case "K0278"
                sReturnCode = "1S"
            Case "K0279"
                sReturnCode = "1S"
            Case "K0281"
                sReturnCode = "0A"
            Case "K0282"
                sReturnCode = "1S"
            Case "K0283"
                sReturnCode = "1S"
            Case "K0284"
                sReturnCode = "1SK"
            Case "K0286"
                sReturnCode = "1S"
            Case "K0291"
                sReturnCode = "1S"
            Case "K0292"
                sReturnCode = "1S"
            Case "K0293"
                sReturnCode = "1S"
            Case "K0294"
                sReturnCode = "1S"
            Case "K0295"
                sReturnCode = "1S"
            Case "K0296"
                sReturnCode = "1S"
            Case "K0297"
                sReturnCode = "1SK"
            Case "K0298"
                sReturnCode = "1S"
            Case "K0299"
                sReturnCode = "1SK"
            Case "K0300"
                sReturnCode = "1S"
            Case "K0302"
                sReturnCode = "1S"
            Case "K0303"
                sReturnCode = "0A"
            Case "K0304"
                sReturnCode = "1S"
            Case "K0305"
                sReturnCode = "1S"
            Case "K0306"
                sReturnCode = "1S"
            Case "K0307"
                sReturnCode = "1SK"
            Case "K0308"
                sReturnCode = "1S"
            Case "K0309"
                sReturnCode = "1S"
            Case "K0310"
                sReturnCode = "1S"
            Case "K0311"
                sReturnCode = "1S"
            Case "K0313"
                sReturnCode = "1S"
            Case "K0314"
                sReturnCode = "1S"
            Case "K0315"
                sReturnCode = "1S"
            Case "K0316"
                sReturnCode = "1SK"
            Case "K0317"
                sReturnCode = "1S"
            Case "K0318"
                sReturnCode = "1S"
            Case "K0319"
                sReturnCode = "1S"
            Case "K0320"
                sReturnCode = "1S"
            Case "K0321"
                sReturnCode = "1S"
            Case "K0322"
                sReturnCode = "1S"
            Case "K0323"
                sReturnCode = "1S"
            Case "K0324"
                sReturnCode = "1S"
            Case "K0325"
                sReturnCode = "1S"
            Case "K0326"
                sReturnCode = "1S"
            Case "K0327"
                sReturnCode = "1S"
            Case "K0328"
                sReturnCode = "1S"
            Case "K0329"
                sReturnCode = "1S"
            Case "K0330"
                sReturnCode = "1S"
            Case "K0331"
                sReturnCode = "1S"
            Case "K0332"
                sReturnCode = "1S"
            Case "K0333"
                sReturnCode = "1S"
            Case "K0334"
                sReturnCode = "1S"
            Case "K0335"
                sReturnCode = "1S"
            Case "K0336"
                sReturnCode = "1S"
            Case "K0337"
                sReturnCode = "1S"
            Case "K0338"
                sReturnCode = "1S"
            Case "K0339"
                sReturnCode = "1S"
            Case "K0340"
                sReturnCode = "1S"
            Case "K0341"
                sReturnCode = "1S"
            Case "K0342"
                sReturnCode = "1S"
            Case "K0343"
                sReturnCode = "1SK"
            Case "K0344"
                sReturnCode = "1S"
            Case "K0345"
                sReturnCode = "1S"
            Case "K0346"
                sReturnCode = "1S"
            Case "K0347"
                sReturnCode = "1S"
            Case "K0348"
                sReturnCode = "1S"
            Case "K0349"
                sReturnCode = "1S"
            Case "K0350"
                sReturnCode = "1S"
            Case "K0351"
                sReturnCode = "1S"
            Case "K0352"
                sReturnCode = "1SK"
            Case "K0353"
                sReturnCode = "1S"
            Case "K0354"
                sReturnCode = "1S"
            Case "K0355"
                sReturnCode = "1S"
            Case "K0356"
                sReturnCode = "1S"
            Case "K0357"
                sReturnCode = "1S"
            Case "K0358"
                sReturnCode = "1SK"
            Case "K0359"
                sReturnCode = "1SK"
            Case "K0360"
                sReturnCode = "1SK"
            Case "K0361"
                sReturnCode = "1S"
            Case "K0362"
                sReturnCode = "1S"
            Case "K0363"
                sReturnCode = "1S"
            Case "K0364"
                sReturnCode = "1SK"
            Case "K0365"
                sReturnCode = "1SK"
            Case "K0366"
                sReturnCode = "1SK"
            Case "K0368"
                sReturnCode = "1S"
            Case "K0369"
                sReturnCode = "1S"
            Case "K0370"
                sReturnCode = "1SK"
            Case "K0371"
                sReturnCode = "1S"
            Case "K0372"
                sReturnCode = "1S"
            Case "K0374"
                sReturnCode = "1S"
            Case "K0375"
                sReturnCode = "1S"
            Case "K0376"
                sReturnCode = "1S"
            Case "K0377"
                sReturnCode = "1SK"
            Case "K0378"
                sReturnCode = "1S"
            Case "K0379"
                sReturnCode = "1S"
            Case "K0380"
                sReturnCode = "0SK"
            Case "K0381"
                sReturnCode = "1S"
            Case "K0382"
                sReturnCode = "1S"
            Case "K0383"
                sReturnCode = "1S"
            Case "K0384"
                sReturnCode = "1S"
            Case "K0385"
                sReturnCode = "1S"
            Case "K0386"
                sReturnCode = "1S"
            Case "K0387"
                sReturnCode = "1S"
            Case "K0388"
                sReturnCode = "1S"
            Case "K0389"
                sReturnCode = "1S"
            Case "K0390"
                sReturnCode = "1S"
            Case "K0391"
                sReturnCode = "1S"
            Case "K0392"
                sReturnCode = "1S"
            Case "K0393"
                sReturnCode = "1SK"
            Case "K0394"
                sReturnCode = "1S"
            Case "K0395"
                sReturnCode = "1SK"
            Case "K0396"
                sReturnCode = "1S"
            Case "K0397"
                sReturnCode = "1S"
            Case "K0398"
                sReturnCode = "1S"
            Case "K0399"
                sReturnCode = "1S"
            Case "K0400"
                sReturnCode = "1S"
            Case "K0401"
                sReturnCode = "1S"
            Case "K0402"
                sReturnCode = "1S"
            Case "K0403"
                sReturnCode = "1S"
            Case "K0404"
                sReturnCode = "1S"
            Case "K0405"
                sReturnCode = "1S"
            Case "K0406"
                sReturnCode = "1SK"
            Case "K0407"
                sReturnCode = "1S"
            Case "K0408"
                sReturnCode = "1S"
            Case "K0409"
                sReturnCode = "1S"
            Case "K0410"
                sReturnCode = "1S"
            Case "K0411"
                sReturnCode = "1S"
            Case "K0412"
                sReturnCode = "1S"
            Case "K0413"
                sReturnCode = "1S"
            Case "K0414"
                sReturnCode = "1S"
            Case "K0415"
                sReturnCode = "1S"
            Case "K0416"
                sReturnCode = "1S"
            Case "K0417"
                sReturnCode = "1S"
            Case "K0418"
                sReturnCode = "1S"
            Case "K0419"
                sReturnCode = "1S"
            Case "K0420"
                sReturnCode = "1S"
            Case "K0421"
                sReturnCode = "1SK"
            Case "K0422"
                sReturnCode = "1S"
            Case "K0423"
                sReturnCode = "1S"
            Case "K0424"
                sReturnCode = "1S"
            Case "K0425"
                sReturnCode = "1SK"
            Case "K0426"
                sReturnCode = "1S"
            Case "K0427"
                sReturnCode = "1S"
            Case "K0428"
                sReturnCode = "1S"
            Case "K0429"
                sReturnCode = "1S"
            Case "K0430"
                sReturnCode = "1S"
            Case "K0431"
                sReturnCode = "1S"
            Case "K0432"
                sReturnCode = "1S"
            Case "K0433"
                sReturnCode = "1S"
            Case "K0434"
                sReturnCode = "1S"
            Case "K0435"
                sReturnCode = "1S"
            Case "K0436"
                sReturnCode = "1S"
            Case "K0437"
                sReturnCode = "1S"
            Case "K0438"
                sReturnCode = "1S"
            Case "K0439"
                sReturnCode = "1S"
            Case "K0440"
                sReturnCode = "1SK"
            Case "K0441"
                sReturnCode = "1SK"
            Case "K0442"
                sReturnCode = "1S"
            Case "K0443"
                sReturnCode = "1S"
            Case "K0444"
                sReturnCode = "1S"
            Case "K0445"
                sReturnCode = "1S"
            Case "K0446"
                sReturnCode = "1SK"
            Case "K0447"
                sReturnCode = "1S"
            Case "K0448"
                sReturnCode = "1S"
            Case "K0449"
                sReturnCode = "1S"
            Case "K0450"
                sReturnCode = "1S"
            Case "K0451"
                sReturnCode = "1S"
            Case "K0452"
                sReturnCode = "1S"
            Case "K0453"
                sReturnCode = "1S"
            Case "K0454"
                sReturnCode = "1S"
            Case "K0455"
                sReturnCode = "1S"
            Case "K0456"
                sReturnCode = "1S"
            Case "K0457"
                sReturnCode = "1S"
            Case "K0458"
                sReturnCode = "1S"
            Case "K0459"
                sReturnCode = "1S"
            Case "K0460"
                sReturnCode = "1S"
            Case "K0461"
                sReturnCode = "0A"
            Case "K0462"
                sReturnCode = "0A"
            Case "K0463"
                sReturnCode = "1S"
            Case "K0464"
                sReturnCode = "1S"
            Case "K0465"
                sReturnCode = "0A"
            Case "K0466"
                sReturnCode = "1S"
            Case "K0467"
                sReturnCode = "1S"
            Case "K0468"
                sReturnCode = "1S"
            Case "K0469"
                sReturnCode = "1S"
            Case "K0470"
                sReturnCode = "1S"
            Case "K0471"
                sReturnCode = "0SK"
            Case "K0472"
                sReturnCode = "1SK"
            Case "K0486"
                sReturnCode = "1S"
            Case "K0487"
                sReturnCode = "1S"
            Case Else
                sReturnCode = "1S"
        End Select

        VismaFokusConvertBANSTACode = sReturnCode

    End Function

    Protected Overrides Sub Finalize()
        If Not xmlMapDoc Is Nothing Then
            xmlMapDoc = Nothing
        End If
        If Not mrEDISettingsExport Is Nothing Then
            mrEDISettingsExport = Nothing
        End If
        If Not xmlParameterDoc Is Nothing Then
            mrEDISettingsExport = Nothing
        End If

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        MyBase.Finalize()
    End Sub
    Private Function FindCONTRLErrorCode(ByVal sCode As String, ByVal eImportFileType As vbBabel.BabelFiles.FileType, ByVal eImportBank As vbBabel.BabelFiles.Bank, ByVal eExportBank As vbBabel.BabelFiles.Bank) As String
        'See end of function for documentations of formats
        Dim sBBCode As String
        Dim sReturnValue As String

        sReturnValue = ""

        Select Case eImportFileType
            Case vbBabel.BabelFiles.FileType.CONTRL
                Select Case eImportBank
                    Case vbBabel.BabelFiles.Bank.DnBNOR_INPS

                        Select Case sCode
                            Case "12"

                            Case Else
                                sBBCode = "99" 'Test only

                        End Select

                End Select


        End Select

        Select Case eExportBank
            Case vbBabel.BabelFiles.Bank.Nordea_eGateway
                Select Case sBBCode
                    Case "99"
                        sReturnValue = "18"

                    Case Else
                        sReturnValue = "18"
                End Select

        End Select


        'Nordea_eGateway
        '2 = Syntax version not supported
        '12 = Invalid Value
        '13 = Missing
        '16 = Too many constituents
        '18 = Unspecified error
        '24 = Too old
        '26 = Duplicate detected
        '28 = References do not match
        '29 = Control count does not match number of instances received
        '39 = Data element too long
        '40 = Data element too short

    End Function
    Private Function CreateBGMReference(ByVal oBabelFile As vbBabel.BabelFile) As String
        'Dim oLocalBabelfile As vbBabel.BabelFile
        Dim oLocalBatch As vbBabel.Batch
        Dim oLocalPayment As vbBabel.Payment
        Dim nTotalAmount As Double
        Dim nTotalDate As Double
        Dim nTotalAccount As Double
        Dim sAccount As String

        nTotalAmount = 0
        nTotalDate = 0
        nTotalAccount = 0
        'For Each oBabelFile In oBabelFiles
        nTotalAmount = nTotalAmount + oBabelFile.MON_InvoiceAmount
        For Each oLocalBatch In oBabelFile.Batches
            For Each oLocalPayment In oLocalBatch.Payments
                nTotalDate = nTotalDate + Val(oLocalPayment.DATE_Payment)
                sAccount = RemoveCharacters(UCase(Trim$(oLocalPayment.E_Account)), "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
                If vbIsNumeric(sAccount, "0123456789") Then
                    nTotalAccount = nTotalAccount + Val(Right$(sAccount, 10))
                End If
            Next oLocalPayment
        Next oLocalBatch
        'Next oBabelFile

        CreateBGMReference = Right$("BABELBANK" & Trim$(Str(nTotalAmount + nTotalDate + nTotalAccount)), 35)

    End Function

End Class
