Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("BabelFileHandling_NET.BabelFileHandling")> Public Class BabelFileHandling
	
	Private sSourceFile, sBackUpPath As String
	Private bDeleteOriginalFile As Boolean
	Private sMessage, sMessageHeader As String
	Private sBankAccounting, sInOut, sClient As String
	Private iFilesetupID As Short
	Private iFilenameNo As Short
	Private bTheFilenameHasACounter As Boolean
	Private bAddOrigToFileName As Boolean
	
	'********* START PROPERTY SETTINGS ***********************
	Public WriteOnly Property SourceFile() As String
		Set(ByVal Value As String)
			sSourceFile = Value
		End Set
	End Property
	Public WriteOnly Property TheFilenameHasACounter() As Boolean
		Set(ByVal Value As Boolean)
			bTheFilenameHasACounter = Value
		End Set
	End Property
	Public WriteOnly Property DeleteOriginalFile() As Boolean
		Set(ByVal Value As Boolean)
			bDeleteOriginalFile = Value
		End Set
	End Property
	Public WriteOnly Property BackupPath() As String
		Set(ByVal Value As String)
			sBackUpPath = Value
		End Set
	End Property
	'02.03.2010 - Added next variable to the class
	Public WriteOnly Property AddOrigToFileName() As Boolean
		Set(ByVal Value As Boolean)
			bAddOrigToFileName = Value
		End Set
	End Property
	Public ReadOnly Property Message() As String
		Get
			If Not EmptyString(sMessageHeader) Then
				Message = sMessageHeader & vbCrLf & sMessage
			Else
				Message = sMessage
			End If
		End Get
	End Property
	Public WriteOnly Property MessageHeader() As String
		Set(ByVal Value As String)
			sMessageHeader = Value
		End Set
	End Property
	Public WriteOnly Property InOut() As String
		Set(ByVal Value As String)
			' New by JanP 18.02.03
			' I = In file (import into )to BabelBank
			' O = Outfile (export) from BabelBank
			sInOut = Value
		End Set
	End Property
	Public WriteOnly Property FilenameNo() As Short
		Set(ByVal Value As Short)
			' New by JanP 18.02.03
			' 1, 2 or 3 FilenameIn1,2 or 3, or FilenameOut1,2 or 3
			iFilenameNo = Value
		End Set
	End Property
	
	Public WriteOnly Property BankAccounting() As String
		Set(ByVal Value As String)
			' New by JanP 18.02.03
			' B = To/From Bank
            ' A = To/From Accounting
            ' S = SwapFile  ' 16.07.2019
			sBankAccounting = Value
		End Set
	End Property
	Public WriteOnly Property Client() As String
		Set(ByVal Value As String)
			' New by JanP 18.02.03
			' In in use, clientno or clientname
			sClient = Value
		End Set
	End Property
	Public WriteOnly Property FilesetupID() As Short
		Set(ByVal Value As Short)
			' New by JanP 18.02.03
			' FilesetupID for current profile
			iFilesetupID = Value
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	Public Function CreateBackup() As Boolean
		
        Dim oFs As Scripting.FileSystemObject
		Dim sDestinationFile, sOriginalFile As String
		Dim sDate, sTime As String
        Dim sFilename As String
		Dim sFilesetupID As String
		Dim bContinue As Boolean
		Dim iFilenameCounter, iDotPositionInOutputPath As Short
		Dim iStatus As Short
        Dim iCounter As Integer = 0

		'Changed 02.05.2005 by Kjell, added code using
		'  sOriginalFile, bContinue and bTheFilenameHasACounter

        oFs = New Scripting.FileSystemObject

		bContinue = True
		iFilenameCounter = 0
		
		Do While bContinue
			If bTheFilenameHasACounter Then
				iFilenameCounter = iFilenameCounter + 1
				iDotPositionInOutputPath = InStrRev(sSourceFile, ".")
				sOriginalFile = Left(sSourceFile, iDotPositionInOutputPath - 1) & Trim(Str(iFilenameCounter)) & Mid(sSourceFile, iDotPositionInOutputPath)
			Else
				sOriginalFile = sSourceFile
				'Only 1 loop in the normal situation
				bContinue = False
			End If
            sDate = VB6.Format(Now, "YYYYMMDD")
            sTime = VB6.Format(Now, "HHMM")
			sFilename = Mid(sOriginalFile, InStrRev(sOriginalFile, "\") + 1)
			If bAddOrigToFileName Then
				sFilename = "Orig_" & sFilename
			End If
			
			'sDestinationFile = sBackUpPath & "\" & sDate & "_" & sTime & "_" & sFilename
			' Changed by JanP 18.02.03
			' Added
			' - FilesetupID, 2 digits
			' - InOut, I or O
			' - BankAccount, B or A
			' - FilenameNo, 1, 2 or 3
			' - ClientNo or Name, empty if no cliens involved
			sFilesetupID = PadLeft(Trim(Str(iFilesetupID)), 2, "0")
			sDestinationFile = sBackUpPath & "\" & sDate & "_" & sTime & "_" & sFilesetupID & sInOut & sBankAccounting & Trim(Str(iFilenameNo)) & "_" & sClient & "_" & sFilename
			
			' New by JanP 18.09.02
			' Make sure sOriginalFile exists!
            If oFs.FileExists(sOriginalFile) Then
                iStatus = CheckFolder(sDestinationFile, (vb_Utils.ErrorMessageType.Dont_Show), False, LRS(12006))
                If iStatus > 50 Then
                    ' something wrong
                    If iStatus = 51 Then
                        ' File is readonly. Check if it is in use by another program.
                        Err.Raise(30027, "CreateBackup", "CreateBackup:" & vbCrLf & "30027 " & LRSCommon(30027, sDestinationFile))
                    ElseIf iStatus = 52 Then
                        ' File is locked. Check if it is in use by another program.
                        Err.Raise(30026, "CreateBackup", "CreateBackup:" & vbCrLf & "30026 " & LRSCommon(30026, sDestinationFile))
                    Else
                        Err.Raise(30023, "CreateBackup", "CreateBackup:" & vbCrLf & "30023 " & LRSCommon(30023, sDestinationFile))
                    End If

                Else
                    oFs.CopyFile(sOriginalFile, sDestinationFile)
                    'Changed 02.02.2005, moved the next IF-statement inside the IF oFS.FileExis....
                    ' It was previouslly outside the IF-statement.
                    If bDeleteOriginalFile Then
                        ' 17.09.2010 - Check if we are allowed to delete file
                        iStatus = CheckFile(sOriginalFile, (vb_Utils.ErrorMessageType.Dont_Show), "CreateBackup")
                        If iStatus > 50 Then
                            ' something wrong
                            If iStatus = 51 Then
                                ' File is readonly. Check if it is in use by another program.
                                Err.Raise(30027, "CreateBackup", "CreateBackup:" & vbCrLf & "30027 " & LRSCommon(30027, sOriginalFile))
                            ElseIf iStatus = 52 Then
                                ' 27.11.2012
                                ' Gjensidige has problems with locked importfile, causing problems when moving file to backup
                                For iCounter = 1 To 10
                                    ' First, try to change attributes
                                    IO.File.SetAttributes(sOriginalFile, IO.FileAttributes.Normal)
                                    Application.DoEvents()

                                    ' Then, wait, and do a new check
                                    ' "sleep" for 0,5 second
                                    Threading.Thread.Sleep(500)
                                    iStatus = CheckFile(sOriginalFile, (vb_Utils.ErrorMessageType.Dont_Show), "CreateBackup")
                                    If iStatus <> 52 Then
                                        ' file is released, jump out
                                        Exit For
                                    End If
                                Next iCounter
                                'If iStatus = 52 Then
                                '    ' 05.12.2012 - file is still locked - like problem at Gjensidige, with Swedish file
                                '    ' vi feiger ut, og renamer filen isteden !
                                '    sDestinationFile = IO.Path.GetDirectoryName(sOriginalFile) & "\Locked_" & VB6.Format(Today, "YYYYMMDD") & "_" & VB6.Format(TimeOfDay, "hhmmss") & "_" & IO.Path.GetFileName(sOriginalFile)
                                '    'My.Computer.FileSystem.RenameFile(sOriginalFile, sDestinationFile)
                                '    System.IO.File.Move(sOriginalFile, sDestinationFile)
                                '    ' File is locked. Check if it is in use by another program.
                                '    'Err.Raise(30026, "CreateBackup", "CreateBackup:" & vbCrLf & "30026 " & LRSCommon(30026, sOriginalFile))
                                'End If

                            Else
                                Err.Raise(30023, "CreateBackup", "CreateBackup:" & vbCrLf & "30023 " & LRSCommon(30023, sOriginalFile))
                            End If

                            bContinue = False
                        Else
                            oFs.DeleteFile(sOriginalFile)
                        End If
                    End If
                End If
            Else
                bContinue = False
            End If
        Loop
		
        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        ' 03.04.2007 JanP: The next line is dangerous! Does not report errors to calling function!!!!!!
		'CreateBackup = True
		' Added this one instead:
		CreateBackup = bContinue
		Exit Function
		
ErrCreateBackup: 
        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        bContinue = False
        Err.Raise(30023, "CreateBackup", "CreateBackup:" & vbCrLf & "30023 " & LRSCommon(30023, sOriginalFile))
	End Function
	Public Function BackupPathExists() As Boolean
        Dim oFs As Scripting.FileSystemObject
		Dim bReturnValue As Boolean
		
		bReturnValue = False

        oFs = New Scripting.FileSystemObject

        If Not sBackUpPath = "" Then
            If oFs.FolderExists(sBackUpPath) Then
                bReturnValue = True
            End If
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        If bReturnValue = False Then
            'Back-up folder %sbackuppath% does not exist. & "Do You want to create it"
            sMessage = LRS(60005, sBackUpPath) & vbCrLf & LRS(60006)
        End If

        BackupPathExists = bReturnValue

    End Function
    Public Function CreateFolderOK() As Short
        'Dim oFs As New FileSystemObject
        Dim iValue As Short
        Dim iReturnValue As Short ' changed 03.04.2007 by JanP from Boolean, which might give us seriours errors!!!!!!

        iValue = CheckFolder(sBackUpPath & "\", vb_Utils.ErrorMessageType.Dont_Show, True)

        Select Case iValue
            Case 0
                iReturnValue = 0
            Case 1
                iReturnValue = 0
            Case 11
                'The folder %sfoldername% is read-only,Change the folder, or change the attributes of the folder.
                sMessage = LRS(60007, sBackUpPath)
                iReturnValue = 11
            Case 13
                '"Folder " & sFolderName & "doesn't exist."
                sMessage = LRS(60008, sBackUpPath)
                iReturnValue = 13
            Case 14
                'Can't create the folder %sbackuppath%. Attributes is read-only.
                sMessage = LRS(60009, sBackUpPath) & vbCrLf & LRS(60010)
                iReturnValue = 14
            Case 17
                'Can't create the folder %sbackuppath%. The drive does not exist.
                sMessage = LRS(60009, sBackUpPath) & vbCrLf & LRS(60011)
                iReturnValue = 17
            Case 90
                'Can't create the folder %sbackuppath%. No folder specified.
                sMessage = LRS(60009, sBackUpPath) & vbCrLf & LRS(60012)
                iReturnValue = 90
            Case 91
                'Can't create the folder %sbackuppath%. Wrong specification of folder.
                sMessage = LRS(60009, sBackUpPath) & vbCrLf & LRS(60013)
                iReturnValue = 91
            Case Else
                'Unknown Error occured during checking/creating the backupfolder %sBackupPath%
                sMessage = LRS(60014, sBackUpPath)
                iReturnValue = 92
        End Select

        CreateFolderOK = iReturnValue

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        bDeleteOriginalFile = False
        ' When vbbabel.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If
        sSourceFile = ""
        bDeleteOriginalFile = False
        sBackUpPath = ""
        bTheFilenameHasACounter = False
        sMessageHeader = ""
        bAddOrigToFileName = False
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Public Function RemoveOldFiles(ByVal iLifeTime As Integer) As Boolean
        ' XNET 19.09.2013 Take whole function
        ' New 15.05.03 by JanP
        ' Delete old files. Run through all files in backuppath,
        ' and check "our" datestamp
        ' Filename in backuppath: sBackUpPath & "\" & sDate & "_" & sTime & "_" & sFilename

        Dim sTempname As String
        Dim sTmpDate As String
        Dim dDateStamp As Date
        Dim sErrorString As String
        On Error GoTo ErrorHandling

        'Exit Function
        RemoveOldFiles = True
        sErrorString = "Before Dir()"
        sTempname = Dir(sBackUpPath & "\*.*") ' Retrieve the first entry.

        sErrorString = "Before BackupPathExists"
        If BackupPathExists Then
            'iCount = 0
            Do While sTempname <> vbNullString   ' Start the loop.
                ' Ignore the current directory and the encompassing directory.
                If sTempname <> "." And sTempname <> ".." Then
                    sErrorString = "Before Mid$(sTempname, Instr(..))"
                    sTmpDate = Mid$(sTempname, InStr(sTempname, "\") + 1, 8)

                    sErrorString = "vbIsNumeric(sTmpDate)"
                    'If vbIsNumeric(sTmpDate) And Mid$(sTempname, 9, 1) = "-" Then
                    ' 02.02.2018
                    ' REALLY? Har sletterutinene i BabelBank i alle �r v�rt feil????
                    ' I linjen under skal vi lete etter _ (underscore), ikke som i If-en som er kommentert over, etter "-" (bindestrek)
                    ' Ellers kommer vi aldri inn i If-en under, og f�r aldri slettet !!!!!!!!
                    If vbIsNumeric(sTmpDate) And Mid$(sTempname, 9, 1) = "_" Then
                        ' Neste linje er feil!
                        dDateStamp = CDate(StringToDate(sTmpDate))
                        sErrorString = "If dDateStamp..."
                        If dDateStamp < (System.DateTime.FromOADate(Date.Today.ToOADate - iLifeTime)) Then
                            ' To old, kill!
                            sErrorString = "Before Kill.."
                            Kill(sBackUpPath & "\" & sTempname)
                        End If
                    End If
                End If
                sTempname = Dir()   ' Get next entry.
            Loop
        End If

        Exit Function

ErrorHandling:
        sMessage = LRS(60016) & vbCrLf & sErrorString & vbCrLf & Err.Description    '"Unable to remove files from backuppath"
        RemoveOldFiles = False

    End Function
End Class
