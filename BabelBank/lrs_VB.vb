﻿Imports System.Threading
Module lrs_VB
    Public Function LRS(ByVal StringIndex As Long, Optional ByVal sString1 As String = vbNullString, Optional ByVal sString2 As String = vbNullString, Optional ByVal sString3 As String = vbNullString) As String
        Dim sReturnString As String
        Dim sLanguage As String
        Dim sNorwegian As String = ""
        Dim sEnglish As String = ""
        Dim sSwedish As String = ""
        Dim sDanish As String = ""
        Dim bUseNorwegian As Boolean = False
        Dim bUseDanish As Boolean = False
        Dim bUseSwedish As Boolean = False
        Dim sTmp As String
        Dim oFS As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Static sLanguageFromFile As String = ""

        sReturnString = ""
        'sLanguage = Thread.CurrentThread.CurrentCulture.ToString
        
        sTmp = System.Configuration.ConfigurationManager.AppSettings("Language")

        If sTmp = "" Then
            sLanguage = Thread.CurrentThread.CurrentCulture.ToString
        Else
            sLanguage = sTmp
        End If
        ' Check if we have "Language.txt" file with Language code set in file.
        If sLanguageFromFile = "" Then
            ' check languagefile
            oFS = New Scripting.FileSystemObject
            If oFS.FileExists(My.Application.Info.DirectoryPath & "\language.txt") Then
                ' file found, import
                oFile = oFS.OpenTextFile(My.Application.Info.DirectoryPath & "\language.txt", Scripting.IOMode.ForReading)
                sLanguageFromFile = oFile.ReadAll
                oFile.Close()
                oFile = Nothing
                oFS = Nothing
            Else
                sLanguageFromFile = "no file"
            End If
        End If
        If sLanguageFromFile <> "no file" Then
            If sLanguageFromFile = "406" Then
                sLanguage = "da"
            ElseIf sLanguageFromFile = "41d" Then
                sLanguage = "se"
            ElseIf sLanguageFromFile = "414" Then
                sLanguage = "nn-NO"
            ElseIf sLanguageFromFile = "809" Then
                sLanguage = "en"
            End If
        End If


        If sLanguage = "nb-NO" Or sLanguage = "nn-NO" Then
            bUseNorwegian = True
        ElseIf sLanguage = "da" Then
            bUseDanish = True
        ElseIf sLanguage = "se" Then
            bUseSwedish = True
        Else
            bUseNorwegian = False
        End If

        Select Case StringIndex
            Case 10000
                sNorwegian = "--------- (Error Handling) ---------"
                sEnglish = "--------- (Error Handling) ---------"
                sSwedish = "--------- (Error Handling) ---------"
                sDanish = "--------- (Error Handling) ---------"
            Case 10001
                sNorwegian = "10001: Ugyldig objektreferanse"
                sEnglish = "10001: Invalid object reference"
                sSwedish = "10001: Ogiltig objektreferens"
                sDanish = "10001: Ugyldig objektreference"
            Case 10002
                sNorwegian = "10002: Filen %1 er ødelagt, eller eksisterer ikke."
                sEnglish = "10002: Input file %1 is invalid or does not exist"
                sSwedish = "10002: Filen %1 är skadad eller finns inte."
                sDanish = "10002: Filen %1 er ødelagt, eller eksisterer ikke."
            Case 10003
                sNorwegian = "10003: Test"
                sEnglish = "10003: The Profile property contains an invalid object reference or is not set"
                sSwedish = "10003: Test"
                sDanish = "10003: Test"
            Case 10004
                sNorwegian = "10004: Uventet feil"
                sEnglish = "10004: Unexpected error"
                sSwedish = "10004: Oväntat fel"
                sDanish = "10004: Uventet fejl"
            Case 10005
                sNorwegian = "10005: Kan ikke finne lisensfil"
                sEnglish = "10005: Could not read license file"
                sSwedish = "10005: Kan inte hitta licensfil"
                sDanish = "10005: Kan ikke finde licensfil"
            Case 10006
                sNorwegian = "10006: Test"
                sEnglish = "10006: Property License is invalid or is not set. Program execution cannot continue"
                sSwedish = "10006: Test"
                sDanish = "10006: Test"
            Case 10007
                sNorwegian = "10007: Lisensen har utgått. Programmet kan ikke kjøres"
                sEnglish = "10007: The license has expired. Program execution cannot continue"
                sSwedish = "10007: Licensen har utgått. Programmet kan inte köras"
                sDanish = "10007: Licensen er udløbet. Programmet kan ikke køres"
            Case 10008
                sNorwegian = "10008: Lisensen til programmet vil utgå om %1 dager"
                sEnglish = "10008: The program license will expire in %1 days"
                sSwedish = "10008: Licensen till programmet utgår om %1 dagar"
                sDanish = "10008: Licensen til programmet udløber om %1 dage"
            Case 10009
                sNorwegian = "10009: Feil importformat spesifisert"
                sEnglish = "10009: Invalid importformat specified"
                sSwedish = "10009: Fel importformat har angetts"
                sDanish = "10009: Forkert importformat specificeret"
            Case 10010
                sNorwegian = "10010: Uventet record"
                sEnglish = "10010: Unexpected record!"
                sSwedish = "10010: Oväntad post"
                sDanish = "10010: Uventet post"
            Case 10011
                sNorwegian = "10011: Feil format"
                sEnglish = "10011: Incorrect format!"
                sSwedish = "10011: Fel format"
                sDanish = "10011: Forkert format"
            Case 10012
                sNorwegian = "10012: Ingen applikasjonsheader"
                sEnglish = "10012: No application-header!"
                sSwedish = "10012: Inget applikationssidhuvud"
                sDanish = "10012: Ingen applikationsheader"
            Case 10013
                sNorwegian = "Test"
                sEnglish = "Test"
                sSwedish = "Test"
                sDanish = "Test"
            Case 10014
                sNorwegian = "10014: Egenskapen <CreateCONTRLMessage> kan ikke settes til <true>." & vbCr & "Importformatet må være et EDI-format, og " & vbLf & "egenskapen <EDIformat> må være <true>."
                sEnglish = "10014: The property <CreateCONTRLMessage> can't be set to <true>." & vbLf & "The import-format has to be an EDI-format, and " & vbLf & "the property <EDIformat> must be <true>."
                sSwedish = "10014: Egenskapen <CreateCONTRLMessage> kan inte anges som <true>." & vbLf & "Importformatet måste vara ett EDI-format och " & vbLf & "egenskapen <EDIformat> ska vara <true>. 	"
                sDanish = "10014: Egenskaben <CreateCONTRLMessage> kan ikke indstilles <true>." & vbLf & "Importformatet skal være et EDI-format, og " & vbLf & "egenskaben <EDIformat> skal være <true>. 	"
            Case 10015
                sNorwegian = "BabelBank avbrytes."
                sEnglish = "BabelBank terminates!"
                sSwedish = "BabelBank avbryts."
                sDanish = "BabelBank afbrydes."
            Case 10016
                sNorwegian = "Vennligst kontakt din leverandør."
                sEnglish = "Please contact your dealer."
                sSwedish = "Kontakta din leverantör."
                sDanish = "Kontakt din leverandør."
            Case 10017
                sNorwegian = "Program avbrytes."
                sEnglish = "Program terminates."
                sSwedish = "Program avbryts."
                sDanish = "Program afbrydes."
            Case 10018
                sNorwegian = "10018: Greier ikke å skrive filinnhold til standardskriver"
                sEnglish = "10018: Unable to write to the default printer."
                sSwedish = "10018: Kan inte skriva filinnehåll till standardskrivare"
                sDanish = "10018: Kan ikke skrive filindhold til standardprinter"
            Case 10019
                sNorwegian = "10019: Feil eksportformat spesifisert"
                sEnglish = "10019: Invalid exportformat specified"
                sSwedish = "10019: Felspecificerat exportformat"
                sDanish = "10019: Forkert eksportformat specificeret"
            Case 10020
                sNorwegian = "10020: Filen du prøver å importere er ikke gyldig i henhold til spesifisert format," & vbLf & "eller den er ødelagt."
                sEnglish = "10020: The content og the file you try to import" & vbLf & "is not in the expected format or it is corupt."
                sSwedish = "10020: Filen som du försöker importera är ogiltigt för det angivna formatet," & vbLf & "eller så är den skadad.	"
                sDanish = "10020: Filen du prøver at importere er ikke gyldig i henhold til specificeret format," & vbLf & "eller den er ødelagt.	"
            Case 10021
                sNorwegian = "10021: Forfallsdatoen, %1," & vbLf & "kan ikke være tidligere enn opprettelsesdaton for filen, %2."
                sEnglish = "10021: The date of execution, %1," & vbLf & "can't be earlier than the disk creation date, %2."
                sSwedish = "10021: Förfallodatum %1" & vbLf & "kan inte vara tidigare än när filen skapades, %2.	"
                sDanish = "10021: Forfaldsdatoen, %1," & vbLf & "kan ikke være tidligere end oprettelsessdatoen for filen, %2.	"
            Case 10022
                sNorwegian = "10022: Forfallsdatoen, %1," & vbLf & "kan ikke være mer enn 15 dager seinere enn opprettelsesdatoen for filen, %2."
                sEnglish = "10022: The date of execution, %1," & vbLf & "can't be more than 15 days later than the disk creation date, %2."
                sSwedish = "10022: Förfallodatum %1" & vbLf & "kan inte vara mer än 15 dagar senare än när filen skapades, %2.	"
                sDanish = "10022: Forfaldsdatoen, %1," & vbLf & "kan ikke være mere end 15 dage senere end oprettelsesdatoen for filen, %2.	"
            Case 10023
                sNorwegian = "10023: En feil oppstod under verifiseringen av bruken av 'extension parts'."
                sEnglish = "10023: An error occured when verifying the use of extension parts."
                sSwedish = "10023: Ett fel uppstod under verifiering av använda ''extension parts''."
                sDanish = "10023: Der opstod en fejl under verificeringen af brugen af 'extension parts'."
            Case 10024
                sNorwegian = "10024: En feil oppstod under lesing av en ny betaling." & vbLf & "Den første linjen som ble lest er ikke av recordtype C."
                sEnglish = "10024: An error occured when trying to read a new payment." & vbLf & "The first line read is not of data record type C."
                sSwedish = "10024: Ett fel uppstod under inläsning av en ny betalning." & vbLf & "Den första raden som lästes in är inte av registertyp C.	"
                sDanish = "10024: Der opstod en fejl under læsning af en ny betaling." & vbLf & "Den første linje, der blev læst har ikke posttype C.	"
            Case 10025
                sNorwegian = "10025: Tekstkoden, %1, er ikke implementert." & vbLf & "Venligst kontakt din forhandler."
                sEnglish = "10025: The textcode, %1, is not implemented." & vbLf & "Please contact Your vendor."
                sSwedish = "10025: Textkoden %1 har inte implementerats." & vbLf & "Kontakta återförsäljaren.	"
                sDanish = "10025: Tekstkoden, %1, er ikke implementeret." & vbLf & "Kontakt din forhandler.	"
            Case 10026
                sNorwegian = "10026: 'Extension part' av typen, %1, kan ikke oppgis mer enn en gang."
                sEnglish = "10026: The extension part of type, %1, can't be stated more than once."
                sSwedish = "10026: ''Extension part'' av typen %1 kan inte anges mer än en gång."
                sDanish = "10026: 'Extension part' af typen, %1, kan ikke angives mere end en gang."
            Case 10027
                sNorwegian = "10027: En ugyldig verdi, %1, er oppgitt som" & vbLf & "identifikasjon av en 'extension part'."
                sEnglish = "10027: An invalid value, %1, is stated for" & vbLf & "the indication of the extension part."
                sSwedish = "10027: Ett ogiltigg värde, %1, har angetts som" & vbLf & "identifikation av en ''extension part''.	"
                sDanish = "10027: En ugyldig værdi, %1, er angivet som" & vbLf & "identifikation af en 'extension part'.	"
            Case 10028
                sNorwegian = "10028: 'Extension part' av typen 'reason for payment'," & vbLf & "kan ikke bli repetert mer enn 13 ganger."
                sEnglish = "10028: The extesion part of type 'reason for payment'," & vbLf & "can't be repeated more than 13 times."
                sSwedish = "10028: ''Extension part'' av typen ''reason for payment''" & vbLf & "kan inte upprepas mer än 13 gånger.	"
                sDanish = "10028: 'Extension part' af typen 'reason for payment'," & vbLf & "kan ikke gentages mere end 13 gange.	"
            Case 10029
                sNorwegian = "10029: En uventet feil oppstod." & vbLf & "Det ble ikke funnet noen 'slutt av fil' record."
                sEnglish = "10029: An unexpected error occured." & vbLf & "No end of file record found."
                sSwedish = "10029: Ett oväntat fel uppstod." & vbLf & "Hittade ingen post med ''slut på fil''.	"
                sDanish = "10029: Der opstod en uventet fejl." & vbLf & "Der blev ikke fundet nogen 'slut af fil'-post.	"
            Case 10030
                sNorwegian = "10030: En feil oppstod under validering av data." & vbLf & "Kontrollen av antall betalinger rapporterer en feil." & vbLf & "Antall importert: %1" & vbLf & "Antall oppgitt i filen: %2"
                sEnglish = "10030: An error occured during validation of data." & vbLf & "The control of number of payments reports an error." & vbLf & "Numbers imported: %1" & vbLf & "Numbers stated in the file end record: %2"
                sSwedish = "10030: Ett fel uppstod under validering av data." & vbLf & "Kontrollen av antal betalningar genererade ett fel." & vbLf & "Antal importerade: %1" & vbLf & "Antal angivna i filen: %2	"
                sDanish = "10030: Der opstod en fejl under validering af data." & vbLf & "Kontrollen af antal betalinger rapporterer en fejl." & vbLf & "Antal importeret: %1" & vbLf & "Antal angivet i filen: %2	"
            Case 10031
                sNorwegian = "10031: En feil oppstod under validering av data." & vbLf & "Kontrollen av %3 rapporterer en feil." & vbLf & "%3 importert: %1" & vbLf & "%3 ble oppgitt i filen i record: %2"
                sEnglish = "10031: An error occured during validation of data." & vbLf & "The control of %3 reports an error." & vbLf & "%3 imported: %1" & vbLf & "%3 stated in the file end record: %2"
                sSwedish = "10031: Ett fel uppstod under validering av data." & vbLf & "Kontrollen av %3 genererade ett fel." & vbLf & "%3 importerade: %1" & vbLf & "%3 har angetts i filen i post: %2	"
                sDanish = "10031: Der opstod en fejl under validering af data." & vbLf & "Kontrollen af %3 rapporterer en fejl." & vbLf & "%3 importeret: %1" & vbLf & "%3 blev angivet i filen i post: %2	"
            Case 10032
                sNorwegian = "10032: Betalingstypen, %1, er ikke implementert i denne versjonen." & vbLf & "Vennligst kontakt din forhandler."
                sEnglish = "10032: The paymenttype, %1, is not implemented in this version." & vbLf & "Please contact Your vendor."
                sSwedish = "10032: Betalningstypen %1 har inte implementerats i den här versionen." & vbLf & "Kontakta återförsäljaren.	"
                sDanish = "10032: Betalingstypen, %1, er ikke implementeret i denne version." & vbLf & "Kontakt din forhandler.	"
            Case 10033
                sNorwegian = "10033: Valideringen mot skjemafilen, rapporterte følgende feil:" & vbLf & "%1"
                sEnglish = "10033: The vaidation against the schemafile reported the following error:" & vbLf & "%1"
                sSwedish = "10033: Valideringen mot schemafilen genererade följande fel:" & vbLf & "%1	"
                sDanish = "10033: Valideringen med skemafilen, rapporterede følgende fejl:" & vbLf & "%1	"
            Case 10034
                sNorwegian = "10034: En feil oppstod under lesing av filen. Tomme linjer er ikke tillatt."
                sEnglish = "10034: An error occured during import. Empty lines are not allowed."
                sSwedish = "10034: Ett fel uppstod under inläsning av filen. Tomma rader tillåts inte."
                sDanish = "10034: Der opstod en fejl under læsning af filen. Tomme linjer er ikke tilladt."
            Case 10035
                sNorwegian = "10035: En feil oppstod under lesing av filen. En linje skal inneholde eksakt 85 skilletegn."
                sEnglish = "10035: An error occured during import. A line must contain exactly 85 delimiters."
                sSwedish = "10035: Ett fel uppstod under inläsning av filen. En rad ska ha exakt 85 skiljetecken."
                sDanish = "10035: Der opstod en fejl under læsning af filen. En linje skal indeholde eksakt 85 skilletegn."
            Case 10036
                sNorwegian = "10036: Filen er tom."
                sEnglish = "10036: The file is empty."
                sSwedish = "10036: Filen är tom."
                sDanish = "10036: Filen er tom."
            Case 10037
                sNorwegian = "10037: Betalingstypen '%1' er ennå ikke implementert i BabelBank."
                sEnglish = "10037: The paymenttype '%1' is not yet implemented in BabelBank."
                sSwedish = "10037: Betalningstypen %1 har ännu inte implementerats i BabelBank."
                sDanish = "10037: Betalingstypen '%1' er endnu ikke implementeret i BabelBank."
            Case 10038
                sNorwegian = "10038: Kontonr skal være på 8 posisjoner, kun siffer. BabelBank kan heller ikke finne noen belstningskonto hverken på fil eller i database."
                sEnglish = "10038: AccountNo must be 8 digits long. Also, BabelBank can not find payers account, neither in the file nor in the database."
                sSwedish = "10038: Kontonr ska ha 8 teckenpositioner, bara siffror. BabelBank kan heller inte hitta något belastningskonto på fil eller i databasen."
                sDanish = "10038: Kontonr. skal være på 8 tegn, kun tal. BabelBank kan heller ikke finde nogen frakonto hverken på fil eller i database."
            Case 10039
                sNorwegian = "10039: Feil i beløp." & vbLf & "Oppgitt beløp: %1"
                sEnglish = "10039: Error in amount." & vbLf & "Amount stated: %1"
                sSwedish = "10039: Fel i belopp." & vbLf & "Angivet belopp: %1	"
                sDanish = "10039: Fejl i beløb." & vbLf & "Angivet beløb: %1	"
            Case 10040
                sNorwegian = "10040: Feil i koden for 'Immediate Destination'. Koden skal starte med en <blank>. " & vbLf & "Oppgitt kode: %1"
                sEnglish = "10040: Error in the Code for 'Immediate Destination'. The code should start with a <blank>. " & vbLf & "Code stated: %1"
                sSwedish = "10040: Fel i koden för ''Immediate Destination''. Koden ska börja med en <blank>." & vbLf & "Angiven kod: %1	"
                sDanish = "10040: Fejl i koden for 'Immediate Destination'. Koden skal starte med en <blank>." & vbLf & "Angivet kode: %1	"
            Case 10041
                sNorwegian = "10041: Feil i koden for '%2'. Koden skal starte med 1, 3 eller 9. " & vbLf & "Oppgitt kode: %1"
                sEnglish = "10041: Error in the Code for '%2'. The code should start with 1, 3 or 9. " & vbLf & "Code stated: %1"
                sSwedish = "10041: Fel i koden för ''%2''. Koden ska börja med 1, 3 eller 9." & vbLf & "Angiven kod: %1	"
                sDanish = "10041: Fejl i koden for '%2'. Koden skal starte med 1, 3 eller 9." & vbLf & "Angivet kode: %1	"
            Case 10042
                sNorwegian = "10042: Feil 'Service Class Code' angitt. Gyldige koder er '200'. '220' or '225'."
                sEnglish = "10042: Illegal 'Service Class Code' stated. Valid codes are '200'. '220' or '225'."
                sSwedish = "10042: Fel ''Service Class Code'' har angetts. Giltiga koder är ''200''. ''220'' eller ''225''."
                sDanish = "10042: Forkert 'Service Class Code' angivet. Gyldige koder er '200'. '220' eller '225'."
            Case 10043
                sNorwegian = "10043: Ulovlig 'Standard Entry Class'. Programmet er kun tilpasset kodene %1 og %2."
                sEnglish = "10043: Illegal 'Standard Entry Class'. The program is only adjusted to %1 and %2."
                sSwedish = "10043: Otillåten ''Standard Entry Class''. Programmet är endast anpassat för koderna %1 och %2."
                sDanish = "10043: Ulovlig 'Standard Entry Class'. Programmet er kun tilpasset koderne %1 og %2."
            Case 10044
                sNorwegian = "10044: Feil i nummerering av 'batchene'. " & vbLf & "Angitt i filen: %1. Forventet av importrutinen: %2."
                sEnglish = "10044: Error in sequential enumeration af the batches. " & vbLf & "Stated in file: %1. Expexted during import: %2."
                sSwedish = "10044: Fel i numrering av ''batcharna''." & vbLf & "Angivet i filen: %1. Förväntat av importrutinen: %2.	"
                sDanish = "10044: Fejl i nummerering af 'batchene'." & vbLf & "Angivet i filen: %1. Forventet af importrutinen: %2.	"
            Case 10045
                sNorwegian = "10045: Ugyldig transaksjonskode, %1, oppgittIllegal transaction code."
                sEnglish = "10045: Illegal transaction code, %1, stated."
                sSwedish = "10045: Ogiltig transaktionskod, %1, Illegal transaction code har angetts."
                sDanish = "10045: Ugyldig transaktionskode, %1, angivet Illegal transaction code."
            Case 10046
                sNorwegian = "10046: Uventet record. En 'addenda' record er oppgitt," & vbLf & "men i følge 'entry detail' recorden skal ikke dette angis."
                sEnglish = "10046: Unexpected record. An addenda record is found," & vbLf & "but according to the entry detail record no addenda record should be stated."
                sSwedish = "10046: Oväntad post av typen ''addenda'' har angetts" & vbLf & "men i enlighet med posten ''entry detail'' ska inte detta anges.	"
                sDanish = "10046: Uventet post. En 'addenda-post er angivet," & vbLf & "" & vbLf & "men ifølge posten 'entry detail' skal den ikke angives.	"
            Case 10047
                sNorwegian = "10047: Sekvensnummeret i 'addenda' recorden er ikke identisk med" & vbLf & "sekvensnummeret angitt i 'entry detail' recorden." & vbLf & "Sekvensnummer i 'addenda'-recorden: %1" & vbLf & "Sekvensnummer i 'detail'-record: %2"
                sEnglish = "10047: The sequence number stated in the addenda record is not identical with" & vbLf & "the number stated in the entry detail record." & vbLf & "In the addenda record: %1" & vbLf & "In the detail record: %2"
                sSwedish = "10047: Sekvensnumret i posten ''addenda'' är inte identiskt med" & vbLf & "sekvensnumret som har angetts i posten ''entry detail''." & vbLf & "Sekvensnummer i ''addenda''-posten: %1" & vbLf & "Sekvensnummer i ''detail''-posten: %2	"
                sDanish = "10047: Sekvensnummeret i posten 'addenda' er ikke identisk med" & vbLf & "sekvensnummeret angivet i posten 'entry detail'." & vbLf & "Sekvensnummer i 'addenda'-posten: %1" & vbLf & "" & vbLf & "" & vbLf & "Sekvensnummer i 'detail'-posten: %2	"
            Case 10048
                sNorwegian = "10048: Total beløp på batch/fil angitt i filen avviker fra summen av alle betalinger." & vbCrLf & "Totalt summert beløp: %1" & vbCrLf & "Total beløp angitt i filen: %2"
                sEnglish = "10048: The total amount entry differs from the total amount of all payments." & vbCrLf & "Total amount entry: %1" & vbCrLf & "Total amount of payments: %2"
                sSwedish = "10048: Totalt belopp i batch/fil som angetts i filen avviker från summan av alla betalningar." & vbLf & "Totalt summerat belopp: %1" & vbLf & "Totalt belopp som angetts i filen: %2	"
                sDanish = "10048: Totalt beløb for batch/fil angivet i filen afviger fra summen af alle betalinger." & vbLf & "Totalt sammenlagt beløb: %1" & vbLf & "Totalt beløb angivet i filen: %2	"
            Case 10049
                sNorwegian = "10049: Dette formatet krever at det meldes inn en klient med Klientnummer=Kreditornummer"
                sEnglish = "10049: You have to add a client where ClientNo = KreditorNo"
                sSwedish = "10049: Detta format kräver registrering av en klient med Klientnummer=Kreditornummer"
                sDanish = "10049: Dette format kræver, at der indmeldes en klient med Klientnummer=Kreditornummer"
            Case 10050
                sNorwegian = "--- Lisensmeldinger ---"
                sEnglish = "--- Licensmessages ---"
                sSwedish = "--- Lisensmeldinger ---"
                sDanish = "--- Lisensmeldinger ---"
            Case 10051
                sNorwegian = "10051: Lisensen har utløpt på dato"
                sEnglish = "10051: Licensfile has expired on date"
                sSwedish = "10051: Licensen har utgått vid datum"
                sDanish = "10051: Licensen er udløbet på datoen"
            Case 10052
                sNorwegian = "10052: Antall lisensierte betalinger er overskredet"
                sEnglish = "10052: Your license has run out of available records"
                sSwedish = "10052: Antal licensierade betalningar har överskridits"
                sDanish = "10052: Antal licenserede betalinger er overskredet"
            Case 10053
                sNorwegian = "10053: Lisensen utløp for %1 dager siden"
                sEnglish = "10053: Licensfile expired %1 days ago"
                sSwedish = "10053: Licensen utgick för %1 dagar sedan"
                sDanish = "10053: Licensen udløb for %1 dage siden"
            Case 10054
                sNorwegian = "10054: Kun %1 dager igjen av lisensperioden. Har du sendt bestillingsskjema til Visual Banking ?"
                sEnglish = "10054: Only %1 days left of licensperiod. Have you sent an orderform to Visual Banking?"
                sSwedish = "10054: Bara %1 dagar kvar av licensperioden. Har du skickat in beställningsplan till Visual Banking ?"
                sDanish = "10054: Kun %1 dage tilbage af licensperioden. Har du sendt bestillingsskema til Visual Banking ?"
            Case 10055
                sNorwegian = "10055: Lisensoppgraderingsfil er for gammel"
                sEnglish = "10055: Licenseupgradefile is to old"
                sSwedish = "10055: Licensuppgraderingsfilen är för gammal"
                sDanish = "10055: Licensopgraderingsfilen er for gammel"
            Case 10056
                sNorwegian = "10056: Kan ikke finne lisensfilen"
                sEnglish = "10056: Can not find licensefile"
                sSwedish = "10056: Kan inte hitta licensfilen"
                sDanish = "10056: Kan ikke finde licensfilen"
            Case 10057
                sNorwegian = "10057: Dato har blitt endret for mange ganger"
                sEnglish = "10057: Date has been changed to many times"
                sSwedish = "10057: Datum har ändrats för många gånger"
                sDanish = "10057: Datoen er blevet ændret for mange gange"
            Case 10058
                sNorwegian = "10058: PC-ens dato har blitt endret tilbake til forrige år. PC-ens dato må endres til nytt år."
                sEnglish = "10058: Date has been set to previous year. Set to current year"
                sSwedish = "10058: PC:ns datum har ändrats tillbaka till föregående år. PC:ns datum måste ändras till nytt år."
                sDanish = "10058: PC'ens dato er blevet ændret tilbage til forrige år. PC'ens dato skal ændres til nyt år."
            Case 10059
                sNorwegian = "10059: Noe er feil med innholdet i lisensfilen"
                sEnglish = "10059: Something wrong with licensefile"
                sSwedish = "10059: Det finns fel i innehållet i licensfilen"
                sDanish = "10059: Der er noget galt med indholdet i licensfilen"
            Case 10060
                sNorwegian = "10060: Kun %1 betalinger igjen i lisensen"
                sEnglish = "10060: Only %1 payments left in license"
                sSwedish = "10060: Bara %1 betalningar kvar i licensen"
                sDanish = "10060: Kun %1 betalinger tilbage i licensen"
            Case 10061
                sNorwegian = "10061: Lisensfilen, %1, er skrivebeskyttet."
                sEnglish = "10061: The license file, %1, is writeprotected."
                sSwedish = "10061: Licensfilen, %1, är skrivskyddad ."
                sDanish = "10061: Licensfilen, %1, er skrivebeskyttet."
            Case 10062
                sNorwegian = "10062: Feil i nummereringen av betaling."
                sEnglish = "10062: Error in paymentcounter."
                sSwedish = "10062: Fel i numrering av betalning."
                sDanish = "10062: Fejl i nummereringen af betaling."
            Case 10063
                sNorwegian = "10063: Lisensnummer i lisenskode er ulikt lisensnummer i lisensfilen."
                sEnglish = "10063: Lisencenumber in the licensecodeis different from the licensecode in the licensefile."
                sSwedish = "10063: Licensnummer i licenskode skiljer seg fra licensnummer i licensfilen."
                sDanish = "10063: Licensnummer i licenskode adskiller seg fra licensnummer i licensfilen."
            Case 10064
                sNorwegian = "10064: Noe er galt med lisenskoden."
                sEnglish = "10064: Something is wrong with the licenscode."
                sSwedish = "10064: Något är fel med licenskoden."
                sDanish = "10064: Noget er galt med licenskoden."
            Case 10065
                sNorwegian = "Meldingsnummeret i filen har blitt benyttet tidligere. Det må være unikt."
                sNorwegian = sNorwegian & "Dette kan oppstå av forskjellige grunner:" & vbCrLf
                sNorwegian = sNorwegian & "  1. En identisk fil har blitt laget tidligere." & vbCrLf
                sNorwegian = sNorwegian & "  2. En feil oppstod i BabelBank, da den samme filen ble forsøkt konvertert tidligere." & vbCrLf
                sNorwegian = sNorwegian & "  3. Dette kan også oppstå hvis BabelBank har kalkulert et identisk meldingsnummer tidligere." & vbCrLf & vbCrLf
                sNorwegian = sNorwegian & "Den samme filen ble importert %1 filnavn %2." & vbCrLf & vbCrLf
                sNorwegian = sNorwegian & "Vennligst kontakt Visual Banking for å løse situasjonen."
                sEnglish = "The messagenumber in the file has been used before. It must be unique." & vbCrLf
                sEnglish = sEnglish & "This may occure for several reasons:" & vbCrLf
                sEnglish = sEnglish & "  1. An identical file has been created earlier." & vbCrLf
                sEnglish = sEnglish & "  2. BabelBank raised an error when trying to create this file earlier and crashed after storing the informastion." & vbCrLf
                sEnglish = sEnglish & "  3. This may also occure if BabelBank by incident has calculated the exactly same message number earlier." & vbCrLf & vbCrLf
                sEnglish = sEnglish & "The same file was imported %1 from the file %2." & vbCrLf & vbCrLf
                sEnglish = sEnglish & "Please contact Visual Banking to resolve the situation."
                sSwedish = "Meddelandenumret i filen har använts tidigare. Det måste vara unikt." & vbCrLf
                sSwedish = sSwedish & "Detta kan uppstå av olika skäl:" & vbCrLf
                sSwedish = sSwedish & "  1. En identisk fil har skapats tidigare." & vbCrLf
                sSwedish = sSwedish & "  2. Ett fel inträffade i BabelBank, när samma fil blev försökt konverterat tidigare." & vbCrLf
                sSwedish = sSwedish & "  3. Detta kan också inträffa om BabelBank har beräknat ett identiskt meddelandenummer tidligare." & vbCrLf & vbCrLf
                sSwedish = sSwedish & "Samma fil importerades %1 filnamn %2." & vbCrLf & vbCrLf
                sSwedish = sSwedish & "Vänligen kontakta Visual Banking för att lösa situationen."
                sDanish = "Meddelelsesnummeret i filen er tidligere brugt. Det skal være unikt." & vbCrLf
                sDanish = sDanish & "Dette kan opstå af forskellige årsager:" & vbCrLf
                sDanish = sDanish & "  1. En identisk fil er blevet oprettet i fortiden." & vbCrLf
                sDanish = sDanish & "  2. Der opstod en fejl i BabelBank, da man forsøgte at konvertere den samme fil tidligere." & vbCrLf
                sDanish = sDanish & "  3. Dette kan også opstå, hvis BabelBank tidligere har beregnet et identisk meddelelsesnummer." & vbCrLf & vbCrLf
                sDanish = sDanish & "Den samme fil blev importeret% 1 filnavn% 2." & vbCrLf & vbCrLf
                sDanish = sDanish & "Kontakt Visual Banking for at løse situationen."

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 10070
            Case 11000
                sNorwegian = "--------- (Error Import) ---------"
                sEnglish = "--------- (Error Import/Sorting) ---------"
                sSwedish = "--------- (Error Import) ---------"
                sDanish = "--------- (Error Import) ---------"
            Case 11001
                sNorwegian = "Formatfeil Direkte remittering - Vennligst kontakt Visual Banking!"
                sEnglish = "Error in Direkte Remitteringsformat - Please contact BabelBank support!"
                sSwedish = "Formatfel vid direkt remittering - Kontakta Visual Banking!"
                sDanish = "Formatfejl Direkte betaling - Kontakt Visual Banking!"
            Case 11002
                sNorwegian = "En feil oppstod under import."
                sEnglish = "An error occured during import."
                sSwedish = "Ett fel uppstod under import."
                sDanish = "Der opstod en fejl under import."
            Case 11003
                sNorwegian = "Importlinje:"
                sEnglish = "Importline:"
                sSwedish = "Importrad:"
                sDanish = "Importlinje:"
            Case 11004
                sNorwegian = "Feltnavn:"
                sEnglish = "Fieldname:"
                sSwedish = "Fältnamn:"
                sDanish = "Feltnavn:"
            Case 11005
                sNorwegian = "Feltnummer:"
                sEnglish = "Fieldnumber:"
                sSwedish = "Fältnummer:"
                sDanish = "Feltnummer:"
            Case 11006
                sNorwegian = "11006: Antall 'entry detils' record talt opp avviker fra hva som er angitt i filen." & vbLf & "Antall talt oppi filen: %1" & vbLf & "Antall angitt i filen: %2"
                sEnglish = "11006: The total number of entry details record stated differs from the number in the batch." & vbLf & "Total number in the batch/file: %1" & vbLf & "Total number stated: %2"
                sSwedish = "11006: Posten med räknade ''entry details'' avviker från det som anges i filen." & vbLf & "Räknat antal i filen: %1" & vbLf & "Angivet antal i filen: %2	"
                sDanish = "11006: Antal 'entry details'-post optalt, afviger fra det, der er angivet i filen." & vbLf & "Antal talt i filen: %1" & vbLf & "Antal angivet i filen: %2	"
            Case 11007
                sNorwegian = "11007: Angitt kontrollsum for 'bank identification' avviker fra sum kalkulert under import." & vbLf & "Sum kalkulert under import: %1" & vbLf & "Kontrollsum i fil: %2"
                sEnglish = "11007: The stated hash for bank identifications differs from the calculated hash." & vbLf & "Calculated hash: %1" & vbLf & "Hash stated: %2"
                sSwedish = "11007: Angiven kontrollsumma för ''bank identification'' avviker från den beräknade summan under import." & vbLf & "Beräknad summa under import: %1" & vbLf & "Kontrollsumma i fil: %2	"
                sDanish = "11007: Angivet kontrolsum for 'bank identification' afviger fra sum beregnet under import." & vbLf & "Sum beregnet under import: %1" & vbLf & "Kontrolsum i fil: %2	"
            Case 11008
                sNorwegian = "11008: Firma identifikasjonen er forskjellig i starten og slutten av batchen." & vbLf & "Firma ID i starten av batch: %1" & vbLf & "Firma ID i slutten av batchen: %2"
                sEnglish = "11008: The Company identifictaion differs between batch start and batch end." & vbLf & "Company ID in batch start: %1" & vbLf & "Company ID in batch end: %2"
                sSwedish = "11008: Företagsidentifikationen är olika i början och slutet av batchen." & vbLf & "Företags-ID i början av batch: %1" & vbLf & "Företags-ID i slutet av batchen: %2	"
                sDanish = "11008: Firmaidentifikationen er forskellig i starten og slutningen af batchen." & vbLf & "Firma ID i starten af batchen: %1" & vbLf & "Firma ID i slutningen af batchen: %2	"
            Case 11009
                sNorwegian = "11009: BabelBank kan ikke sortere på de ønskede kriteriene."
                sEnglish = "11009: BabelBank is unable to sort on the requested criterias."
                sSwedish = "11009: BabelBank kan inte sortera efter de önskade kriterierna."
                sDanish = "11009: BabelBank kan ikke sortere efter de ønskede kriterier."
            Case 11010
                sNorwegian = "11010: En uventet feil oppstod!" & vbLf & "BabelBank kan ikke dekryptere filen:" & vbLf & "%1"
                sEnglish = "11010: An unexcpected error occured!" & vbLf & "BabelBank can't decrypt the file:" & vbLf & "%1"
                sSwedish = "11010: Ett oväntat fel uppstod!" & vbLf & "BabelBank kan inte dekryptera filen:" & vbLf & "%1	"
                sDanish = "11010: Der opstod en uventet fejl!" & vbLf & "BabelBank kan ikke dekryptere filen:" & vbLf & "%1	"
            Case 11011
                sNorwegian = "11011: Kan ikke utlede betalers kontonummer"
                sEnglish = "11011: Can't find any debit accountnumber"
                sSwedish = "11011: Kan inte härleda betalarens kontonummer"
                sDanish = "11011: Kan ikke udlede betalers kontonummer"
            Case 11012
                sNorwegian = ", fordi ingen klienter ble funnet på profilen."
                sEnglish = ", because no client is found for this Profile."
                sSwedish = ", eftersom det saknas klienter i profilen."
                sDanish = ", fordi der ikke blev fundet klienter på profilen."
            Case 11013
                sNorwegian = ", fordi mer enn en klient bruker denne profilen."
                sEnglish = ", because more than 1 client uses this Profile."
                sSwedish = ", eftersom mer än en klient använder profilen."
                sDanish = ", fordi mere end en klient bruger denne profil."
            Case 11014
                sNorwegian = ", fordi det ikke er lagt inn et kontonummer på klienten."
                sEnglish = ", because no accountnumber is stated on the client."
                sSwedish = ", eftersom inga kontonummer har angetts för klienten."
                sDanish = ", fordi der ikke er lagt et kontonummer ind på klienten."
            Case 11015
                sNorwegian = ", fordi det finnes mer enn ett kontonummer registrert på klienten."
                sEnglish = ", because more than 1 accountnumber is stated on the client."
                sSwedish = ", eftersom det finns mer än ett kontonummer registrerat för klienten."
                sDanish = ", fordi der er registreret mere end et kontonummer på klienten."
            Case 11016
                sNorwegian = "Totalt antall betalinger angitt i filen avviker fra antallet som er importert." & vbLf & "Antall angitt i filen: %1" & vbLf & "Antall importert: %2" & vbLf & "Filnavn: %3"
                sEnglish = "The total number of payments stated on the file differs from the number of payments imported." & vbLf & "The number of payments stated:     %1" & vbLf & "The number of payments imported: %2" & vbLf & "Filename:   %3"
                sSwedish = "Totalt antal betalningar som har angetts i filen avviker från antalet som har importerats." & vbLf & "Antal angivna i filen: %1" & vbLf & "Antal importerade: %2" & vbLf & "Filnamn: %3	"
                sDanish = "Totalt antal betalinger angivet i filen afviger fra antallet, som er importeret." & vbLf & "Antal angivet i filen: %1" & vbLf & "Antal importeret: %2" & vbLf & "Filnavn: %3	"
            Case 11017
                sNorwegian = "Kontonummerene angitt i filen samsvarer ikke." & vbLf & "Linje lest: %1" & vbLf & "Filnavn: %2"
                sEnglish = "Inconsistant accountnumbers stated in the file." & vbLf & "Line read: %1" & vbLf & "Filenam: %2"
                sSwedish = "Kontonumren som har angetts i filen matchar inte." & vbLf & "Läst rad: %1" & vbLf & "Filnamn: %2	"
                sDanish = "Kontonumrene, der er angivet i filen, matcher ikke." & vbLf & "Linje læst: %1" & vbLf & "Filnavn: %2	"
            Case 11018
                sNorwegian = "Kortnummer (strippet):"
                sEnglish = "Cardnumber (stripped):"
                sSwedish = "Kortnummer (strippat):"
                sDanish = "Kortnummer (uden specialtegn):"
            Case 11019
                sNorwegian = "Oppgitt totalbeløp avviker fra summen av korttrekk." & vbLf & "Oppgitt totalbeløp:       %1" & vbLf & "Sum av trekk:       %2" & vbLf & "·	Mottakers navn:    %3" & vbLf & "Filnavn:       %4"
                sEnglish = "The total amount stated differs from the sum of all withdrawals." & vbLf & "The total stated:       %1" & vbLf & "The sum of withdrawals: %2" & vbLf & "Merchant name:      %3" & vbLf & "Filename:   %4"
                sSwedish = "Angivet totalbelopp avviker från summan av kortavdrag." & vbLf & "Angivet totalbelopp:       %1" & vbLf & "Summa avdrag:       %2" & vbLf & "·	Mottagarens namn:    %3" & vbLf & "Filnamn:       %4	"
                sDanish = "Angivet totalbeløb afviger fra summen af korttræk." & vbLf & "Angivet totalbeløb:       %1" & vbLf & "Sum af træk:       %2" & vbLf & "·	Modtagers navn:    %3" & vbLf & "Filnavn:       %4	"
            Case 11020
                sNorwegian = "Filen % er ikke en gyldig autogirofil fra Bankgirot"
                sEnglish = "The file %1 is not a valid autogirofile from Bankgirot"
                sSwedish = "Filen % är inte en giltig autogirofil från Bankgirot"
                sDanish = "Filen % er ikke en gyldig autogirofil fra Bankgiro"
            Case 11021
                sNorwegian = "Filen % er ikke en gyldig SAP PAYEXT PEXR2002 fil"
                sEnglish = "The file % is not a valid SAP PAYEXT PEXR2002 file"
                sSwedish = "Filen % är inte en giltig SAP PAYEXT PEXR2002-fil"
                sDanish = "Filen % er ikke en gyldig SAP PAYEXT PEXR2002 fil"
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 11040
            Case 11040
                sNorwegian = "11040 - Feil eller manglende 'Special' i profiloppsettet." & vbLf & "Spesial er satt som: %1"
                sEnglish = "11040 - Wrong or missing 'Special' in profile setup." & vbLf & "Special set as: %1"
                sSwedish = "11040 - Fel eller saknades 'Special' i profiloppsettet." & vbLf & "Spesial är satt som: %1"
                sDanish = "11040 - Fejl eller manglende 'Special' i profiloppsettet." & vbLf & "Spesial er satt som: %1"
            Case 11041
                sNorwegian = "11041 - Feil statuskode angitt i pain.002 filen." & vbLf & "Statuskode: %1"
                sEnglish = "11041 - Invalid statuscode stated in the pain.002 file." & vbLf & "Statuscode: %1"
                sSwedish = "11041 - Fel statuskode angivt i pain.002 filen." & vbLf & "Statuskode: %1"
                sDanish = "11041 - Fejl statuskode angivet i pain.002 filen." & vbLf & "Statuskode: %1"
            Case 11042
                sNorwegian = "11042 - Programmet finner ingen konto for beveglser på %3-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
                sEnglish = "11042 - The program can't find any account stated on the %3 file." & vbLf & "Filename: %1" & vbLf & "Id: %2"
                sSwedish = "11042 - Programmet hittar inget konto för rörelse på %3-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
                sDanish = "11042 - Programmet finder ingen konto for bevæglser på %3-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
            Case 11043
                sNorwegian = "11043 - Kan ikke finne betalingstype på følgende bunt:"
                sEnglish = "11043 - No paymenttype is found on the following account entry:"
                sSwedish = "11043 - Det går inte att hitta betalningstyp på följande bunt:"
                sDanish = "11043 - Kan ikke finde betalingstype på følgende bunt:"
            Case 11044
                sNorwegian = vbLf & "Filnavn: %1" & vbLf & "Buntreferanse: %2" & vbLf & "Betaling: %3"
                sEnglish = vbLf & "Filename: %1" & vbLf & "Entry reference: %2" & vbLf & "Payment: %3"
                sSwedish = vbLf & "Filnavn: %1" & vbLf & "Buntreferanse: %2" & vbLf & "Betalning: %3"
                sDanish = vbLf & "Filnavn: %1" & vbLf & "Buntreferanse: %2" & vbLf & "Betaling: %3"
            Case 11045
                sNorwegian = "11045 - Programmet finner ikke debet/kredit-merke for beveglser på CAMT.05-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
                sEnglish = "11045 - The program can't find any debit/credit-mark stated on the CAMT.054 file." & vbLf & "Filename: %1" & vbLf & "Id: %2"
                sSwedish = "11045 - Programmet hittar inte debet/kredit-merke for rörelser på CAMT.05-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
                sDanish = "11045 - Programmet finder ikke debet/kredit-merke for bevæglser på CAMT.05-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
            Case 11046
                sNorwegian = "11046 - Programmet finner ikke landet til parten som styrer kontoen på vegne av kontoeier på %3-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
                sEnglish = "11046 - The program can't find find the country party that manages the account on behalf of the account owner on the %3 file." & vbLf & "Filename: %1" & vbLf & "Id: %2"
                sSwedish = "11046 - Programmet hittar inte landet till den part som styr konto på uppdrag av kontoinnehavaren på %3-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
                sDanish = "11046 - Programmet finder ikke landet til parten som styrer kontoen på vegne av kontoeier på %3-filen." & vbLf & "Filnavn: %1" & vbLf & "Id: %2"
            Case 11047
                sNorwegian = "Betalingsnummer: "
                sEnglish = "Paymentcounter: "
                sSwedish = "Betalningsnummer: "
                sDanish = "Betalingsnummer: "
            Case 11048
                sNorwegian = "11048 - Formatet %1 krever at bank angis i oppsettet av profilen."
                sEnglish = "11048 - The format %1 requires that a bank is stated in the setup of the profile."
                sSwedish = "11048 - Formatet %1 kräver att banken anges i profile inställning."
                sDanish = "11048 - Formatet %1 kræver at banken er angivet i profile setup."
            Case 11049
                sNorwegian = "11049 - Det finnes ikke en gyldig debet/kredit indikator på posteringen."
                sEnglish = "11049 - No valid debit/credit indicator is stated on the posting."
                sSwedish = "11049 - Det finns inte ett giltigt debit/kredit indikator på posteringen."
                sDanish = "11049 - Der findes ikke en gyldig debet/kredit indikator på posteringen."
            Case 11050
                sNorwegian = "11050 - Det finnes ikke en gyldig debit/credit indikator på posteringen."
                sEnglish = "11050 - No valid debit/credit indicator is stated on the posting."
                sSwedish = "11050 - Det finns inte ett giltigt debit/kredit indikator på posteringen."
                sDanish = "11050 - Der findes ikke en gyldig debet/kredit indikator på posteringen."
            Case 11051
                sNorwegian = "11051 - Den oppgitte bevegelsen på kontoen, %1," & vbCrLf & "avviker fra forskjellen mellom inn- og utgående saldo, %2."
                sEnglish = "11051 - The net entry amount stated, %1," & vbCrLf & "differs from the difference between the opening balance and the closing balance, %2."
                sSwedish = "11051 - Den angivna rörelsen på kontot, %1," & vbCrLf & "avviker från skillnaden mellan inkommande och utgående balans, %2."
                sDanish = "11051 - Den givne bevægelse på kontoen, %1," & vbCrLf & "afviger fra forskellen mellem ind- og udgående balance, %2."
            Case 11052
                sNorwegian = "11052 - Den oppgitte bevegelsen på kontoen, %1" & vbCrLf & ", avviker fra totalt importert beløp, %2."
                sEnglish = "11052 - The net entry amount stated, %1" & vbCrLf & ", differs from the total amount imported, %2."
                sSwedish = "11052 - Den angivna rörelsen på kontot, %1" & vbCrLf & ", avviker från det totalt importerade belopp, %2."
                sDanish = "11052 - Den givne bevægelse på kontoen, %1" & vbCrLf & ", afviger fra totalt imortert beløp, %2."
            Case 11053
                sNorwegian = "11053 - Filen du forsøker å importere, %1, er ikke en %2 fil." & Environment.NewLine & Environment.NewLine & "Verdien i xmlns atributten er: %3"
                sEnglish = "11053 - The file you try to import, %1, is not a %2 file." & Environment.NewLine & Environment.NewLine & "The xmlns attributevalue is: %3"
                sSwedish = "11053 - Filen du forsøker å importere, %1, är inte en %2 fil." & Environment.NewLine & Environment.NewLine & "Värdet i xmlns attributet är: %3"
                sDanish = "11053 - Filen du forsøger at importere, %1, er ikke en %2 fil." & Environment.NewLine & Environment.NewLine & "Værdien af xmlns atributten er: %3"
            Case 11054
                sNorwegian = "11054 - Dette er ikke en OCBC SG Giro returfil"
                sEnglish = "11054 - This is not a OCBC SG Giro returnfile"
                sSwedish = "11054 - Det her er inte en OCBC SG Giro returfil"
                sDanish = "11054 - Dette er ikke en OCBC SG GIro returfil"
            Case 11055
                sNorwegian = "Importert linje: "
                sEnglish = "Imported record:"
                sSwedish = "Importerad rad"
                sDanish = "Importert linje"

            Case 11500
                sNorwegian = "----------(ERROR MATCHING) ---------------"
                sEnglish = "---------- (ERROR MATCHING) -----------"
                sSwedish = "----------(ERROR MATCHING) ---------------"
                sDanish = "----------(ERROR MATCHING) ---------------"
            Case 11501
                sNorwegian = "Fakturaen %1 er delbetalt!"
                sEnglish = "The invoice %1 is partly paid!"
                sSwedish = "Fakturan %1 är delbetald!"
                sDanish = "Fakturaen %1 er delbetalt!"

            Case 12000
                sNorwegian = "--------- (Error Export) ---------"
                sEnglish = "--------- (Error Export) ---------"
                sSwedish = "--------- (Error Export) ---------"
                sDanish = "--------- (Error Export) ---------"
            Case 12001
                sNorwegian = "Feil ID-lengde. Skal være %1"
                sEnglish = "Wrong length of paymentID. Must be %"
                sSwedish = "Fel ID-längd. Ska vara %1"
                sDanish = "Forkert ID-længde. Skal være %1"
            Case 12002
                sNorwegian = "Telleren som benyttes ved eksport av formatet %1 er av feil type." & vbLf & "Eksportfil: %2"
                sEnglish = "The type of counter used during export of the format %1 is not valid ." & vbLf & "Exportfile: %2"
                sSwedish = "Räknaren som används för att exportera formatet %1 är av fel typ." & vbLf & "Exportfil: %2	"
                sDanish = "Den tæller, der benyttes ved eksport af formatet %1 er en forkert type." & vbLf & "Eksportfil: %2	"
            Case 12003
                sNorwegian = "12003: Konto %1 er ukjent. Vennligst legg den inn under 'Klienter'."
                sEnglish = "12003: Could not find account %1. Please enter the account in Clients."
                sSwedish = "12003: Konto %1 är okänt. Lägg upp det under ''Klienter''."
                sDanish = "12003: Konto %1 er ukendt. Opret den under 'Klienter'."
            Case 12004
                sNorwegian = "12004: Skal filen %1 overskrives"
                sEnglish = "12004: Do You want to overwrite the file %1?"
                sSwedish = "Ska filen %1 skrivas över"
                sDanish = "Skal filen %1 overskrives"
            Case 12005
                sNorwegian = "12005: Feil i filsti for CONTRL-melding"
                sEnglish = "12005: Error in filepath for CONTRL-message"
                sSwedish = "12005: Kreditnotor är inte tillåtna vid överföring till plusgirokonto"
                sDanish = "12005: Kreditnotaer ikke tilladt ved Plusgirokontooverførsel"
            Case 12006
                sNorwegian = "12006: Feil i filsti for backup"
                sEnglish = "12006: Error in filepath for backup"
                sSwedish = "12006: Fel i filsti for backup"
                sDanish = "12006: Fejl i filsti for backup"
            Case 12007
                sNorwegian = "12007: Minst en tilbakebetaling ble ikke eksportert."
                sEnglish = "12007: On or more returnpayments where not exported."
                sSwedish = "12007: Minst en återbetalning blev inte exporterad."
                sDanish = "12007: Mindst en tilbagebetaling blev ikke eksportert."
            Case 13000
                sNorwegian = "--------- (Error Report) ---------"
                sEnglish = "--------- (Error Report) ---------"
                sSwedish = "--------- (Error Report) ---------"
                sDanish = "--------- (Error Report) ---------"
            Case 13001
                sNorwegian = "13001: Feil når variabelen %1 settes." & vbLf & "Rapporttypen %2 er ikke gyldig."
                sEnglish = "13001: Error initializing the variable %1." & vbLf & "The reporttype %2 is not valid."
                sSwedish = "13001: Fel när variabeln %1 angavs." & vbLf & "Rapporttypen %2 är ogiltig.	"
                sDanish = "13001: Fejl når variablen %1 indstilles." & vbLf & "Rapporttypen %2 er ikke gyldig.	"
            Case 13002
                sNorwegian = "13002: Du forsøker å starte en rapport, uten å spesifisere rapporttype." & vbLf & "Spesifiser rapporttype enten i egenskapen REPORT_Print eller i REPORT_Screen."
                sEnglish = "13002: You are trying to start a report, without specifying which report to start." & vbLf & "Specify the reporttype in either the property REPORT_Print or REPORT_Screen."
                sSwedish = "13002: Du försöker starta en rapport utan att specificera rapporttyp." & vbLf & "Specificerarapporttyp med egenskapen REPORT_Print eller REPORT_Screen.	"
                sDanish = "13002: Du prøver at starte en rapport, uden at specificere rapporttype." & vbLf & "Specificer rapporttype enten i egenskaben REPORT_Print eller i REPORT_Screen.	"
            Case 13003
                sNorwegian = "Ingen data å rapportere!"
                sEnglish = "No data to report!"
                sSwedish = "Inga data att rapportera!"
                sDanish = "Ingen data at rapportere!"
            Case 13004
                sNorwegian = "Feil ved forhåndsvisning av rapport %1"
                sEnglish = "Error previewing report %1"
                sSwedish = "Fel vid förhandsgranskning av rapport %1"
                sDanish = "Fejl ved forhåndsvisning af rapport %1"
            Case 13005
                sNorwegian = "Feil ved utskrift av rapport %1"
                sEnglish = "Error printing report %1"
                sSwedish = "Fel vid utskrift av rapport %1"
                sDanish = "Fejl ved udskrivning af rapport %1"
            Case 13006
                sNorwegian = "Feil ved eksport av fil %1"
                sEnglish = "Error exporting report %1"
                sSwedish = "Fel vid export av fil %1"
                sDanish = "Fejl ved eksport af fil %1"
            Case 13007
                sNorwegian = "Feil ved sending av E-mail av rapport %1"
                sEnglish = "Error e-mailing report %1"
                sSwedish = "Fel vid utskick av e-post med rapport %1"
                sDanish = "Fejl ved afsendelse af e-mail af rapport %1"
            Case 13008
                sNorwegian = "Feil i rapportmodul"
                sEnglish = "Error in report module"
                sSwedish = "Fel i rapportmodul"
                sDanish = "Fejl i rapportmodul"
            Case 13009
                sNorwegian = "Feil i filsti for rapport"
                sEnglish = "Error in filepath for report"
                sSwedish = "Fel i filsti for rapport"
                sDanish = "Fejl i filsti for rapport"
            Case 13010
                sNorwegian = "Feil oppdaget under kontroll av avstemming." & vbCrLf & "Sjekk rapport %1"
                sEnglish = "An error was detected during the control of the matching." & vbCrLf & "Check report %1"
                sSwedish = "Fel upptäckt under kontroll av avstämning." & vbCrLf & "Check rapport %1"
                sDanish = "Fejl opdaget under kontrol af afstemning." & vbCrLf & "Sjekk rapport %1"
            Case 13011
                sNorwegian = "Feil i avstemming."
                sEnglish = "Error in matching."
                sSwedish = "Fel i avstämning."
                sDanish = "Fejl i afstemning."
            Case 13012
                sNorwegian = "BabelBank er ikke tilpasset å kjøre rapportnummer: %1."
                sEnglish = "BabelBank is not adopted to run report number %1."
                sSwedish = "BabelBank är inte anpassad til at kjöra rapport: %1."
                sDanish = "BabelBank er ikke tilpasset å køre rapportnummer: %1."
            Case 14000
                sNorwegian = "--------- (Error saving/retrieving from DB) --------"
                sEnglish = "--------- (Error saving/retrieving from DB) --------"
                sSwedish = "--------- (Error saving/retrieving from DB) --------"
                sDanish = "--------- (Error saving/retrieving from DB) --------"
            Case 14001
                sNorwegian = "14001: En feil oppstod ved %1 av data."
                sEnglish = "14001: An error occured during %1 data."
                sSwedish = "14001: Ett fel uppstod vid %1 av data."
                sDanish = "14001: Der opstod en fejl ved %1 af data."
            Case 14002
                sNorwegian = "Kilde:"
                sEnglish = "Source:"
                sSwedish = "Källa:"
                sDanish = "Kilde:"
            Case 14003
                sNorwegian = "Beskrivelse:"
                sEnglish = "Description:"
                sSwedish = "Beskrivning:"
                sDanish = "Beskrivelse:"
            Case 14004
                sNorwegian = "14004: En ukjent feil oppstod under uthenting av data."
                sEnglish = "14004: An unknown error occured during the retrieve data process."
                sSwedish = "14004: Ett okänt fel uppstod under hämtning av data."
                sDanish = "14004: Der opstod en ukendt fejl under hentning af data."
            Case 14005
                sNorwegian = "Det er ingen åpne poster."
                sEnglish = "There are no open records."
                sSwedish = "Det finns inga öppna poster."
                sDanish = "Det er ingen åbne poster."
            Case 14006
                sNorwegian = "14006: En uventet feil oppstod." & vbLf & "Metoden %1 er benyttet i feil sammenheng."
                sEnglish = "14006: An unexcpected error occured." & vbLf & "The method %1 is used in the wrong context."
                sSwedish = "14006: Ett oväntat fel uppstod." & vbLf & "Metoden %1 har använts i fel sammanhang.	"
                sDanish = "14006: Der opstod en uventet fejl." & vbLf & "Metoden %1 blev benyttet i forkert sammenhæng.	"
            Case 14007
                sNorwegian = "Du har kommet til den %1 åpne posten."
                sEnglish = "You have reached the %1 open payment."
                sSwedish = "Du har kommet till den %1 öppna posten."
                sDanish = "Du er kommet til den %1 åbne post."
            Case 14008
                sNorwegian = "14008: Under sletting av poster i %1, oppstod følgende feil:"
                sEnglish = "14008: When deleting records in %1, the following error occured:"
                sSwedish = "14008: Under borttagning av poster i %1 uppstod följande fel:"
                sDanish = "14008: Under sletning af poster i %1, opstod følgende fejl:"
            Case 14009
                sNorwegian = "lagring"
                sEnglish = "saving"
                sSwedish = "spara"
                sDanish = "gemme"
            Case 14010
                sNorwegian = "uthenting"
                sEnglish = "retrieving"
                sSwedish = "hämtning"
                sDanish = "hentning"
            Case 14011
                sNorwegian = "siste"
                sEnglish = "last"
                sSwedish = "sista"
                sDanish = "sidste"
            Case 14012
                sNorwegian = "første"
                sEnglish = "first"
                sSwedish = "första"
                sDanish = "første"
            Case 14013
                sNorwegian = "14013: Finner ingen betalinger å hente ut." & vbLf & "" & vbLf & "Ingen betalinger oppfyller utvalgskriteriene."
                sEnglish = "14013: No payments to retrieve." & vbLf & "" & vbLf & "No payments among the stored payments match the criterias."
                sSwedish = "14013: Hittar inga betalningar att hämta." & vbLf & "" & vbLf & "Inga betalningar uppfyller urvalskriterierna.	"
                sDanish = "14013: Kan ikke finde betalinger, der skal hentes ud." & vbLf & "" & vbLf & "Ingen betalinger opfylder valgkriterierne.	"
            Case 15000
                sNorwegian = "15000: Feilmeldinger i EDI2XML	"
                sSwedish = "15000: Felmeddelanden i EDI2XML"
                sDanish = "15000: Fejlmeddelelse i EDI2XML"
            Case 15001
                sNorwegian = "15001: Importfilen %1 er ødelagt, eller eksisterer ikke.	"
                sEnglish = "15001: The importfile %1 is corrupt, or doesn't exist.	"
                sSwedish = "15001: Importfilen %1 är skadad eller finns inte."
                sDanish = "15001: Importfilen %1 er ødelagt, eller eksisterer ikke."
            Case 15002
                sNorwegian = "15002: Ukjent avsender av EDI-fil. Avsender-ID: %1.	"
                sEnglish = "15002: Unknown sender of EDI-file. Sender-ID: %1.	"
                sSwedish = "15002: Okänd avsändare av EDI-fil. Avsändar-ID: %1."
                sDanish = "15002: Ukendt afsender af EDI-fil. Afsender-ID: %1."
            Case 15003
                sNorwegian = "15003: Mappingfilen %1 for EDI er ødelagt, eller eksisterer ikke.	"
                sEnglish = "15003: The EDI mappingfile %1 is either corrupt, or doesn't exist.	"
                sSwedish = "15003: Mappningfilen %1 för EDI är skadad eller finns inte."
                sDanish = "15003: Mappingfilen %1 for EDI er ødelagt, eller eksisterer ikke."
            Case 15004
                sNorwegian = "15004: Feil under initiering av EDI2XML-klasser.	"
                sEnglish = "15004: Error during initialization of the EDI2XML-classes.	"
                sSwedish = "15004: Fel under initiering av EDI2XML-klasser."
                sDanish = "15004: Fejl under initiering af EDI2XML-klasser."
            Case 15005
                sNorwegian = "15005: Feil under initieringen av midertidig XML-objekt."
                sEnglish = "15005: Error during initialization of the temporary XML-object.	"
                sSwedish = "15005: Fel under initieringen av temporärt XML-objekt."
                sDanish = "15005: Fejl under initiering af midlertidigt XML-objekt."
            Case 15006
                sNorwegian = "15006: Feil under initiering av EDI-klassen.	"
                sEnglish = "15006: Error during initialization of the EDI-class.	"
                sSwedish = "15006: Fel under initiering av EDI-klassen."
                sDanish = "15006: Fejl under initiering af EDI-klassen."
            Case 15007
                sNorwegian = "15007: Kan ikke finne første segment (UNA eller UNB) i filen: % 1. Segmentet mangler eller det er ikke en EDI-fil.	"
                sEnglish = "15007: Can't find the first segment (UNA or UNB) in the file: %1. The segment is missing or it is not an EDI-file.	"
                sSwedish = "15007: Kan inte hitta förste segmentet (UNA eller UNB) i filen: % 1. Segmentet saknas eller så är det inte en EDI-fil."
                sDanish = "15007: Kan ikke finde første segment (UNA eller UNB) i filen: % 1. Segmentet mangler eller det er ikke en EDI-fil."
            Case 15008
                sNorwegian = "15008: Kan ikke lese første segmentet i filen: %1 Filen er ikke en EDI-fil.	"
                sEnglish = "15008: Can't read the first segment in the file: %1. The file is either corrupt, or it is not an EDI-file.	"
                sSwedish = "15008: Kan inte läsa första segmentet i filen: %1 Filen är inte en EDI-fil."
                sDanish = "15008: Kan ikke læse første segment i filen: %1 Filen er ikke en EDI-fil."
            Case 15009
                sNorwegian = "15009: Kan ikke skrive header-informasjonen til XML-objektet.	"
                sEnglish = "15009: Can't write the header-information to the XML-object.	"
                sSwedish = "15009: Kan inte skriva sidhuvudsinformationen till XML-objektet."
                sDanish = "15009: Kan ikke skrive header-informationen til XML-objektet."
            Case 15010
                sNorwegian = "15010: Feil under initiering av verdier for grupperepitisjon.	"
                sEnglish = "15010: Error during initialization of group-repetition values.	"
                sSwedish = "15010: Fel under initiering av värden för gruppupprepning."
                sDanish = "15010: Fejl under initiering af værdier for grupperepetition."
            Case 15011
                sNorwegian = "15011: Kan ikke finne %1-segmentet i filen: %2	"
                sEnglish = "15011: Can't find the %1-segment in the file: %2	"
                sSwedish = "15011: Kan inte hitta %1-segmentet i filen: %2"
                sDanish = "15011: Kan ikke finde %1-segmentet i filen: %2"
            Case 15012
                sNorwegian = "15012: Obligatorisk segment %1 mangler i filen: %2	"
                sEnglish = "15012: Mandatory %1 is missing in the file: %2	"
                sSwedish = "15012: Obligatoriskt segment %1 saknas i filen: %2"
                sDanish = "15012: Obligatorisk segment %1 mangler i filen: %2"
            Case 15013
                sNorwegian = "15013: Kan ikke lese segmentet etter %1-segmentet i filen: %2	"
                sEnglish = "15013: Can't read the segment following the %1-segment in the file: %2	"
                sSwedish = "15013: Kan inte läsa segmentet efter %1-segmentet i filen: %2"
                sDanish = "15013: Kan ikke læse segmentet efter %1-segmentet i filen: %2"
            Case 15014
                sNorwegian = "15014: Feil under lesning av filen: %1 LIN nr.:%2 SEQ nr. %3	"
                sEnglish = "15014: Error during reading the file: %1. LIN no.:%2. SEQ no. %3	"
                sSwedish = "15014: Fel under läsning av filen: %1 LIN nr:%2 SEQ nr %3"
                sDanish = "15014: Fejl under læsning af filen: %1 LIN nr.:%2 SEQ nr. %3"
            Case 15015
                sNorwegian = "15015: Feil i nummereringen av %1."
                sEnglish = "15015: Error in nummeration of %1. Wrong number is %2.	"
                sSwedish = "15015: Fel i numreringen av %1."
                sDanish = "15015: Fejl i nummereringen af %1."
            Case 15016
                sNorwegian = "15016: Feil antall LIN i filen: %1. Antall angitt i filen: %2. Antall talt opp i filen: %3	"
                sEnglish = "15016: Wrong number of LIN in the file: %1. The number stated in the file: %2. The number counted during import: %3	"
                sSwedish = "15016: Fel antal LIN i filen: %1. Antal angivet i filen: %2. Antal räknat i filen: %3"
                sDanish = "15016: Forkert antal LIN i filen: %1. Antal angivet i filen: %2. Antal talt op i filen: %3"
            Case 15017
                sNorwegian = "15017: Meldingsreferansen i UNT: %1 er ikke identisk med meldingsreferansen i UNH: %2. Filnavn: %3	"
                sEnglish = "15017: The messagereference in UNT: %1 is not identical to the messagereference in UNH: %2. Filename: %3	"
                sSwedish = "15017: Meddelandereferensen i UNT: %1 är inte identisk med meddelandereferensen i UNH: %2. Filnamn: %3"
                sDanish = "15017: Meddelelsesreferencen i UNT: %1 er ikke identisk med meddelelsesreferencen i UNH: %2. Filnavn: %3"
            Case 15018
                sNorwegian = "15018: Gruppereferansen i UNE: %1 er ikke identisk med referansen i UNG: %2. Filnavn: %3	"
                sEnglish = "15018: The groupreference in UNE: %1 is not identical to  the reference in UNG: %2. Filename: %3	"
                sSwedish = "15018: Gruppreferensen i UNE: %1 är inte identisk med referensen i UNG: %2. Filnamn: %3"
                sDanish = "15018: Gruppereferencen i UNE: %1 er ikke identisk med referencen i UNG: %2. Filnavn: %3"
            Case 15019
                sNorwegian = "15019: Utvekslingsreferansen i UNZ: %1 er ikke identisk med utvekslingsreferansen i UNB: %2. Filnavn: %3	"
                sEnglish = "15019: The interchangereference in UNZ: %1 is not identical to the interchangereference in UNB: %2. Filename: %3	"
                sSwedish = "15019: Utväxlingsreferensen i UNZ: %1 är inte identisk med utväxlingsreferensen i UNB: %2. Filnamn: %3"
                sDanish = "15019: Udvekslingsreferencen i UNZ: %1 er ikke identisk med udvekslingsreferencen i UNB: %2. Filnavn: %3"
            Case 15020
                sNorwegian = "15020: Kan ikke finne segmentgruppe nr. %1 i mappingfilen: %2	"
                sEnglish = "15020: Can't find the segmentgroup no.: %1 in the mappingfile: %2	"
                sSwedish = "15020: Kan inte hitta segmentgrupp nr %1 i mappningsfilen: %2"
                sDanish = "15020: Kan ikke finde segmentgruppe nr. %1 i mappingfilen: %2"
            Case 15021
                sNorwegian = "15021: Gruppe %1 repetert for mange ganger. repeated to many times. Filename: XXXXX	"
                sEnglish = "15021: Segmentgroup %1 is repeated too many times. Filename: %2	"
                sSwedish = "15021: Grupp %1 har upprepats för många gånger. repeated to many times. Filename: XXXXX"
                sDanish = "15021: Gruppe %1 gentaget for mange gange. repeated to many times. Filename: XXXXX"
            Case 15022
                sNorwegian = "15022: Segmentet %1 repetert for mange ganger.	"
                sEnglish = "15022: The segment %1 is repeated too many times.	"
                sSwedish = "15022: Segmentet %1 har upprepats för många gånger."
                sDanish = "15022: Segmentet %1 gentaget for mange gange."
            Case 15023
                sNorwegian = "15023: Obligatorisk segment %1 mangler. Segment lest: %2	"
                sEnglish = "15023: Mandatory segment %1 is missing. Segment read: %2	"
                sSwedish = "15023: Obligatoriskt segment %1 saknas. Segment läst: %2"
                sDanish = "15023: Obligatorisk segment %1 mangler. Segment læst: %2"
            Case 15024
                sNorwegian = "15024: For mange nivåer.	"
                sEnglish = "15024: Too many levels.	"
                sSwedish = "15024: För många nivåer."
                sDanish = "15024: For mange niveauer."
            Case 15025
                sNorwegian = "15025: Uventet segment %1.	"
                sEnglish = "15025: Unexpected segment %1.	"
                sSwedish = "15025: Oväntat segment %1."
                sDanish = "15025: Uventet segment %1."
            Case 15026
                sNorwegian = "15026: Intern feil i EDI2XML. %1 er ikke en gruppe.	"
                sEnglish = "15026: Internal error in EDI2XML. %1 is not a group.	"
                sSwedish = "15026: Internt fel i EDI2XML. %1 är inte en grupp."
                sDanish = "15026: Intern fejl i EDI2XML. %1 er ikke en gruppe."
            Case 15027
                sNorwegian = "15027: Uventet element (tag).	"
                sEnglish = "15027: Unexpected element (tag).	"
                sSwedish = "15027: Oväntat element (tagg)."
                sDanish = "15027: Uventet element (tag)."
            Case 15028
                sNorwegian = "15028: Ugyldig verdi i elementet (tag).	"
                sEnglish = "15028: Invalid value in the element (tag).	"
                sSwedish = "15028: Ogiltigt värde i elementet (tagg)."
                sDanish = "15028: Ugyldig værdi i elementet (tag)."
            Case 15029
                sNorwegian = "15029: Obligatorisk element (tag) mangler.	"
                sEnglish = "15029: Mandatory element (tag) is missing.	"
                sSwedish = "15029: Obligatoriskt element (tagg) saknas."
                sDanish = "15029: Obligatorisk element (tag) mangler."
            Case 15030
                sNorwegian = "Filnavn: %1 "
                sEnglish = "Filename: %1 "
                sSwedish = "Filnamn: %1 "
                sDanish = "Filnavn: %1 "
            Case 15031
                sNorwegian = "LIN-nr.: %1 "
                sEnglish = "LIN-no.: %1 "
                sSwedish = "LIN-nr: %1 "
                sDanish = "LIN-nr.: %1 "
            Case 15032
                sNorwegian = "SEQ-nr.: %1 "
                sEnglish = "SEQ-no.: %1 "
                sSwedish = "SEQ-nr: %1"
                sDanish = "SEQ-nr.: %1"
            Case 15033
                sNorwegian = "Taggroup: %1 "
                sEnglish = "Taggroup: %1 "
                sSwedish = "Taggroup: %1 "
                sDanish = "Taggroup: %1 "
            Case 15034
                sNorwegian = "Tag: %1 "
                sEnglish = "Tag: %1 "
                sSwedish = "Tagg: %1 "
                sDanish = "Tag: %1 "
            Case 15035
                sNorwegian = "Verdi: %1 "
                sEnglish = "Value: %1 "
                sSwedish = "Värde: %1"
                sDanish = "Værdi: %1"
            Case 15036
                sNorwegian = "Segmenttype: %1 "
                sEnglish = "Segmenttype: %1 "
                sSwedish = "Segmenttyp: %1 "
                sDanish = "Segmenttype: %1 "
            Case 15037
                sNorwegian = "Segment: "
                sEnglish = "Segment: "
                sSwedish = "Segment: "
                sDanish = "Segment: "
            Case 15038
                sNorwegian = "Segmentgruppe: %1 "
                sEnglish = "Segmentgroup: %1 "
                sSwedish = "Segmentgrupp: %1 "
                sDanish = "Segmentgruppe: %1 "
            Case 15040
                sNorwegian = "15040: Obligatorisk elementgruppe (taggroup) mangler.	"
                sEnglish = "15040: Mandatory elementgroup (taggroup) is missing.	"
                sSwedish = "15040: Obligatorisk elementgrupp (taggroup) saknas."
                sDanish = "15040: Obligatorisk elementgruppe (taggroup) mangler."
            Case 15041
                sNorwegian = "15041: Elementet (taggen) %1 er ikke definert i mappingen	"
                sEnglish = "15041: The element (tag) %1 is not defined in the mapping.	"
                sSwedish = "15041: Elementet (taggen) %1 är inte definierad i mappningen"
                sDanish = "15041: Elementet (taggen) %1 er ikke defineret i mappingen"
            Case 15042
                sNorwegian = "15042: Elementet (taggen) skal være nummerisk.	"
                sEnglish = "15042: The element (tag) should be numeric.	"
                sSwedish = "15042: Elementet (taggen) ska vara numerisk."
                sDanish = "15042: Elementet (taggen) skal være numerisk."
            Case 15043
                sNorwegian = "15043: Elementverdien (tagverdien) er for lang. Maks.lengde er %1.	"
                sEnglish = "15043: The elementvalue (tagvalue) is too long. Maximum length id: %1.	"
                sSwedish = "15043: Elementvärdet (taggvärdet) är för långt. Maxlängden är %1."
                sDanish = "15043: Elementværdien (tagværdien) er for lang. Maks. længde er %1."
            Case 15044
                sNorwegian = "15044: Feil antall segmenter. Antall angitt i filen: %1. Antall talt opp i filen: %2	"
                sEnglish = "15044: Wrong number of segments. The number stated in the file: %1. The number counted during import: %2	"
                sSwedish = "15044: Fel antal segment. Antal angivet i filen: %1. Antal räknat i filen: %2"
                sDanish = "15044: Forkert antal segmenter. Antal angivet i filen: %1. Antal talt op i filen: %2"
            Case 15045
                sNorwegian = "15045: Uventet slutt på fil. Filen kan være skadet.	"
                sEnglish = "15045: Unexpected end of file. The file may be damaged.	"
                sSwedish = "15045: Oväntat slut på fil. Filen kan vara skadad."
                sDanish = "15045: Uventet slutning på fil. Filen kan være skadet."
            Case 15046
                sNorwegian = "15046: Segmentet inneholder data som ikke er definert i mappingen.	"
                sEnglish = "15046: The segmentet contains data that isn't defined in the mapping.	"
                sSwedish = "15046: Segmentet innehåller data som inte är definierade i mappningen."
                sDanish = "15046: Segmentet indeholder data, som ikke er defineret i mappingen."
            Case 15047
                sNorwegian = "15047: Feil under eksport til TBI-XML format	"
                sEnglish = "15047: An error occured during export to the TBI-XML format	"
                sSwedish = "15047: Fel under export till TBI-XML-format"
                sDanish = "15047: Fejl under eksport til TBI-XML format"
            Case 15048
                sNorwegian = "15048: Finner ikke skjemafilen: %1	"
                sEnglish = "15048: Can't find the schema-file: %1	"
                sSwedish = "15048: Hittar inte schemafilen: %1"
                sDanish = "15048: Kan ikke finde skemafilen: %1"
            Case 15049
                sNorwegian = "15049: SWIFT-adressen til debetkontoen %1 er ikke gyldig. Landkoden er ikke implementert. Landkode: %2	"
                sEnglish = "15049: The SWIFT-address for the debit bankaccount %1 is not valid. The landcode is not implemented. Landcode: %2	"
                sSwedish = "15049: SWIFT-adressen till debetkontot %1 är ogiltig. Landsnumret har inte implementerats. Landsnummer: %2"
                sDanish = "15049: SWIFT-adressen til debetkontoen %1 er ikke gyldig. Landekoden er ikke implementeret. Landekode: %2"
            Case 15050
                sNorwegian = "15050: Eksportformatet TBIW få genereres av BabelBank. Man kan ikke benytte vbBabel.dll til å lage dette formatet.	"
                sEnglish = "15050: The export-format TBIW needs to be run through BabelBank. You can't use the vbBabel.dll to create this format.	"
                sSwedish = "15050: Exportformatet TBIW genereras från BabelBank. Det går inte att använda vbBabel.dll för generering av formatet."
                sDanish = "15050: Eksportformatet TBIW skal genereres af BabelBank. Man kan ikke benytte vbBabel.dll til at lave dette format."
            Case 15051
                sNorwegian = "15051: Ukjent betalingstype: %1	"
                sSwedish = "15051: Okänd betalningstyp: %1"
                sDanish = "15051: Ukendt betalingstype: %1"
            Case 15052
                sNorwegian = "15052: Variablen %1 i skjemafilen er ikke implementert i Babelbank.	"
                sEnglish = "15052: The variable %1 in the schema file is not implemented in Babelbank.	"
                sSwedish = "15052: Variabeln %1 i schemafilen har inte implementerats i Babelbank."
                sDanish = "15052: Variablen %1 i skemafilen er ikke implementeret i Babelbank."
            Case 15053
                sNorwegian = "15053: Brukeren har avbrutt prosessen. Det ble ikke lagd noen eksportfiler.	"
                sEnglish = "15053: The user har cancelled the prosess. No exportfiles are created.	"
                sSwedish = "15053: Användaren har avbrutit processen. Inga exportfiler skapades."
                sDanish = "15053: Brugeren har afbrudt processen. Der blev ikke lavet nogen eksportfiler."
            Case 15054
                sNorwegian = "15054: En uventet feil oppstod under korreksjonsprosedyren.	"
                sEnglish = "15054: Unexpected error during the correct-procedure.	"
                sSwedish = "15054: Ett oväntat fel uppstod under korrigeringsproceduren."
                sDanish = "15054: Der opstod en uventet fejl under korrektionsproceduren."
            Case 15055
                sNorwegian = "15055: BabelBank kan ikke kalkulere totalsummen ved eksport.	"
                sEnglish = "15055: Unable to calculate total amount during export.	"
                sSwedish = "15055: BabelBank kan inte beräkna totalsumman vid export."
                sDanish = "15055: BabelBank kan ikke beregne totalsummen ved eksport."
            Case 15056
                sNorwegian = "15056: Beskrivelsen av variabeltypen %1 ble ikke funnet i skjemafilen.	"
                sEnglish = "15056: The description of variabletype %1 is not found in the schema-file.	"
                sSwedish = "15056: Beskrivningen av variabeltypen %1 hittades inte i schemafilen."
                sDanish = "15056: Beskrivelsen af variabeltypen %1 blev ikke fundet i skemafilen."
            Case 15057
                sNorwegian = "15057: Egenskapen 'Pattern' er ikke implementert for datatypen 'Date' i denne versjonen av BabelBank. Kontakt din leverandør. BabelBank avslutter.	"
                sEnglish = "15057: The constraining facet 'Pattern' is not implemented for the datatype 'Date' in this version of BabelBank. Contact Your dealer.  BabelBank will terminate	"
                sSwedish = "15057: Egenskapen ''Pattern'' implementerades inte för datatypen ''Date'' i denna version av BabelBank. Kontakta din leverantör. BabelBank avslutas."
                sDanish = "15057: Egenskaben 'Pattern' er ikke implementeret for datatypen 'Date' i denne version af BabelBank. Kontakt din leverandør. BabelBank afslutter."
            Case 15059
                sNorwegian = "15059: Variabeltypen %1 er ikke definert i BabelBank. BabelBank kan ikke lage betalingsfilen til DNB.	"
                sEnglish = "15059: The variable type %1 is not defined in BabelBank. BabelBank can't create the paymentfile to DNB.	"
                sSwedish = "15059: Variabeltyp %1 har inte definierats i BabelBank. BabelBank kan inte skapa betalningsfilen till DNB."
                sDanish = "15059: Variabeltypen %1 er ikke defineret i BabelBank. BabelBank kan ikke oprette betalingsfilen til DNB."
            Case 15060
                sNorwegian = "BabelBank kan ikke lage betalingsfilen."
                sEnglish = "BabelBank can't create the paymentfile."
                sSwedish = "BabelBank kan inte skapa betalningsfilen till DNB."
                sDanish = "BabelBank kan ikke oprette betalingsfilen til DNB."
            Case 15061
                sNorwegian = "15061: %1 er obligatorisk, men ingen verdi er oppgitt.	"
                sEnglish = "15061: %1 is mandatory, but the variable has no value.	"
                sSwedish = "15061: %1 är obligatoriskt, men inget värde har angetts."
                sDanish = "15061: %1 er obligatorisk, men ingen værdi er angivet."
            Case 15062
                sNorwegian = "15062: En vekslingskurs er angitt, men navn og referanse er ikke angitt. Begge variabler er obligatoriske ved bruk av vekslingskurs.	"
                sEnglish = "15062: An exchange rate is stated, but the dealers name and a reference is not stated. Both are mandatory when using an exchangerate.	"
                sSwedish = "15062: En växelkurs har angetts men namnet och referensen är inte inställda. Båda variablerna behövs vid användning av växelkurs."
                sDanish = "15062: Der er angivet en vekselkurs, men navn og reference er ikke angivet. Begge variabler er obligatoriske ved brug af vekselkurs."
            Case 15063
                sNorwegian = "15063: En vekslingskurs er angitt, men navn er ikke angitt. Dette er obligatorisk ved bruk av vekslingskurs.	"
                sEnglish = "15063: An exchange rate is stated, but the dealers name is not stated. The dealers name is mandatory when using an exchangerate.	"
                sSwedish = "15063: En växelkurs har angetts men namnet är inte specificerat. Detta är obligatoriskt vid användning av växelkurs."
                sDanish = "15063: Der er angivet en vekselkurs, men navn er ikke angivet. Dette er obligatorisk ved brug af vekselkurs."
            Case 15064
                sNorwegian = "15064: En vekslingskurs er angitt, men ingen referanse er angitt. Dette er obligatorisk ved bruk av vekslingskurs.	"
                sEnglish = "15064: An exchange rate is stated, but a reference is not stated. The reference is mandatory when using an exchangerate.	"
                sSwedish = "15064: En växelkurs har angetts men ingen referens är specificerad. Detta är obligatoriskt vid användning av växelkurs."
                sDanish = "15064: Der er angivet en vekselkurs, men ingen reference er angivet. Dette er obligatorisk ved brug af vekselkurs."
            Case 15065
                sNorwegian = "15065: Oppgitt betalingsdato %1, er forfalt.	"
                sEnglish = "15065: The paymentdate stated %1, is overdue.	"
                sSwedish = "15065: Angivet betalningsdatum %1 har förfallit."
                sDanish = "15065: Den angivne betalingsdato %1, er forfaldet."
            Case 15066
                sNorwegian = "15066: Feltet 'Accountname' er obligatorisk.	"
                sEnglish = "15066: The field 'Accountname' is mandatory.	"
                sSwedish = "15066: Fält ''Accountname'' är obligatoriskt."
                sDanish = "15066: Feltet 'Accountname' er obligatorisk."
            Case 15067
                sNorwegian = "15067: Feltet 'BankID' er obligatorisk og nummerisk, og må være eksakt %1 sifferer langt. Oppgitt verdi er: %2	"
                sEnglish = "15067: The field 'BankID' is mandatory and numeric, and is of a length of exactly %1 digits. The value stated is: %2	"
                sSwedish = "15067: Fältet ''BankID'' är obligatoriskt och numeriskt, och måste vara exakt %1 siffror. Angett värde är: %2"
                sDanish = "15067: Feltet 'BankID' er obligatorisk og numerisk, og skal være eksakt %1 cifre langt. Angivet værdi er: %2"
            Case 15068
                sNorwegian = "15068: Feltet 'BankID' må være nummerisk, og må være eksakt %1 siffer langt. Oppgitt verdi er: %2	"
                sEnglish = "15068: The field 'BankID' must be numeric, and is of a length of exactly %1 digits. The value stated is: %2	"
                sSwedish = "15068: Fältet ''Bank ID'' måste vara numeriskt och måste vara exakt %1 siffror. Angett värde är: %2"
                sDanish = "15068: Feltet 'BankID' skal være numerisk, og skal være eksakt %1 cifre langt. Angivet værdi er: %2"
            Case 15069
                sNorwegian = "15069: Feltet 'Bank ID' er obligatorisk.	"
                sEnglish = "15069: The field 'Bank ID' is mandatory.	"
                sSwedish = "15069: Fältet 'Bank ID' är obligatoriskt."
                sDanish = "15069: Feltet 'Bank ID' er obligatorisk."
            Case 15070
                sNorwegian = "15070: Feltet 'Info To Beneficiary' er obligatorisk.	"
                sEnglish = "15070: The field 'Info To Beneficiary' is mandatory.	"
                sSwedish = "15070: Fältet ''Info till mottagaren'' är obligatoriskt."
                sDanish = "15070: Feltet 'Info To Beneficiary' er obligatorisk."
            Case 15071
                sNorwegian = "15071: Feil i Bank-ID:	"
                sEnglish = "15071: Error in the Bank-ID:	"
                sSwedish = "15071: Fel i Bank - ID:"
                sDanish = "15071: Fejl i Bank-ID:"
            Case 15072
                sNorwegian = "15072: Feil i til korresponderende bank:	"
                sEnglish = "15072: Error in the Bank-ID to correspondent bank:	"
                sSwedish = "15072: Fel i till-korrespondentbank:"
                sDanish = "15072: Fejl til korresponderende bank:"
            Case 15073
                sNorwegian = "15073: %1 må være eksakt %2 posisjoner lang.	"
                sEnglish = "15073: %1 have to be exactly %2 long.	"
                sSwedish = "15073: %1 måste vara exakt %2 teckenpositioner långt."
                sDanish = "15073: %1 skal være eksakt %2 tegn langt."
            Case 15074
                sNorwegian = "15074: Verdien %1 er ikke gyldig for feltet %2 i henhold til skjemafilen.	"
                sEnglish = "15074: The value %1 is not valid for the variable %2 according to the schema file.	"
                sSwedish = "15074: Värdet %1 är inte giltigt för fältet %2 i enlighet med schemafilen."
                sDanish = "15074: Værdien %1 er ikke gyldig for feltet %2 i henhold til skemafilen."
            Case 15075
                sNorwegian = "15075: Tallet %1 brukt i variablen %2,inneholder ugyldige karakterer.	"
                sEnglish = "15075: The number %1 used in the variable %2, contains illegal characters.	"
                sSwedish = "15075: Värdet %1 som används i variabeln %2 innehåller ogiltiga tecken."
                sDanish = "15075: Tallet %1 brugt i variablen %2, indeholder ugyldige karakterer."
            Case 15076
                sNorwegian = "15076: Antall siffer i %1 benyttet i variablen %2 er for mange. Maksimalt antall siffer er %3.	"
                sEnglish = "15076: The number of digits in %1 used in the variable %2, is too many. Maximiulength is %3.	"
                sSwedish = "15076: Antal siffror i %1 som används i variabeln %2 är för många. Max antal siffror är %3."
                sDanish = "15076: Antal cifre i %1 benyttet i variablen %2 er for højt. Maksimalt antal cifre er %3."
            Case 15077
                sNorwegian = "15077: Antall desimalsiffer %1 benyttet i variablen %2 er for mange. Maksimalt antall siffer er %3.	"
                sEnglish = "15077: The number of decimalnumbers in %1 used in the variable %2, is too many. Maximum is %3.	15078: The value %1 used in the variable %2, is too low. Minimum value is %3	"
                sSwedish = "15076: Antal decimaler i %1 som används i variabeln %2 är för många. Max antal siffror är %3."
                sDanish = "15077: Antal decimaltegn %1 benyttet i variablen %2 er for højt. Maksimalt antal cifre %3."
            Case 15078
                sNorwegian = "15078: Verdien %1 benyttet i variablen %2er for lav. Minimum verdi er %3	"
                sEnglish = "15078: The value %1 used in the variable %2, is too low. Minimum value is %3	"
                sSwedish = "15078: Värdet %1 använt i variablen %2er för låg. Minimum verdi er %3	"
                sDanish = "15078: Værdien %1 anvendt i variablen %2er for lav. Minimum verdi er %3	"
            Case 15079
                sNorwegian = "15079: %1 benyttet i variablen %2 er for kort. Minimumlengde er %3.	"
                sEnglish = "15079: The value %1 used in the variable %2, is too short. Minimum length is %3.	"
                sSwedish = "15079: %1 som används i variabeln %2 är för kort. Minimivärdet är %3."
                sDanish = "15079: %1 benyttet i variablen %2 er for kort. Minimumlængden er %3."
            Case 15080
                sNorwegian = "15080: %1 benyttet i variabelen %2, er for lang. Maksimumlengde er %3.	"
                sEnglish = "15080: The value %1 used in the variable %2, is too long. Maximum length is %3.	In BabelBank BENEFICIARYADDRESS4 is put together from the fields 'Zipcode' + 'City' + 'Address3'.	"
                sSwedish = "15080: %1 som används i variabeln %2 är för långt. Maxvärdet är %3."
                sDanish = "15080: %1 benyttet i variablen %2, er for lang. Maksimumlængden er %3."
            Case 15081
                sNorwegian = "15081: I BabelBank er feltet BENEFICIARYADDRESS4 satt sammen av feltene 'Zipcode' + 'City' + 'Address3'.	"
                sSwedish = "15081: I BabelBank är fältet BENEFICIARYADDRESS4 sammansatt av fälten ''Zipcode'' + ''City'' + ''Address3''."
                sDanish = "15081: I BabelBank er feltet BENEFICIARYADDRESS4 sat sammen af felterne 'Zipcode' + 'City' + 'Address3'."
            Case 15082
                sNorwegian = "15082: Feil ved behandling av 0-transer.	"
                sEnglish = "15082: Error handling 0-amounts	"
                sSwedish = "15082: Fel vid behandling av 0-trans."
                sDanish = "15082: Fejl ved behandling af 0-transakt."
            Case 15083
                sNorwegian = "15083: Kun en faktura, og denne har 0-beløp.	"
                sEnglish = "15083: Only one invoice, and it has a 0-amount.	"
                sSwedish = "15083: Endast en faktura och den har 0-belopp."
                sDanish = "15083: Kun en faktura, og denne har 0-beløb."
            Case 15084
                sNorwegian = "15084: Ingen fakturaer for denne betaling.	"
                sEnglish = "15084: No invoices for current payment.	"
                sSwedish = "15084: Inga fakturor för denna betalning."
                sDanish = "15084: Ingen fakturaer for denne betaling."
            Case 15085
                sNorwegian = "15085: Oppgitt betalingsdatoen %1, er ikke en gyldig bankdag. Betalingsdato settes til første gyldige bankdag.	"
                sEnglish = "15085: The paymentdate stated %1, is a bank holiday. The date is set to the first valid date.	"
                sSwedish = "15085: Angivet betalningsdatum %1 är inte en giltig bankdag. Betalningsdatum ändras till första giltiga bankdag."
                sDanish = "15085: Angivet betalingsdato %1, er ikke en gyldig bankdag. Betalingsdato sættes til første gyldige bankdag."
            Case 15086
                sNorwegian = "15086: En Referanse betaling må være i Euro.	"
                sEnglish = "15086: A Reference payment must be in Euro.	"
                sSwedish = "15086: En referensbetalning måste vara i euro."
                sDanish = "15086: En Referencebetaling skal være i Euro."
            Case 15087
                sNorwegian = "15087: Ingen bane for filen Values.XML er angitt.	"
                sEnglish = "15087: No filepath for Values.XML are specified.	"
                sSwedish = "15087: Ingen sökväg har angetts till filen Values.XML."
                sDanish = "15087: Der er ikke angivet nogen bane for filen Values.XML."
            Case 15088
                sNorwegian = "15088: Kan ikke laste filen Values.XML.	"
                sEnglish = "15088: Can't load Values.XML.	"
                sSwedish = "15088: Kan inte överföra filen Values.XML."
                sDanish = "15088: Kan ikke indlæse filen Values.XML."
            Case 15089
                sNorwegian = "15089: En innlands General Payment i USA, må inneholde enten en gyldig bank-ID og hvilken type ID, eller et bank-navn og adresse.	"
                sEnglish = "15089: In a domestic General Payment in the United States, you must state either a valid bank-ID and the type of ID, or a bank name and adress.	"
                sSwedish = "15089: En inrikes General Payment i USA måste innehålla antingen ett giltigt bank-ID och typ av ID eller ett banknamn och adress."
                sDanish = "15089: En indenlandsk General Payment i USA, skal indeholde enten et gyldigt bank-ID og hvilken type ID, eller et banknavn og adresse."
            Case 15090
                sNorwegian = "15090: Norges bank krever at du oppgir en gyldig kode og forklaring på hva betaling gjelder, ved utlandsbetalinger belastet en konto i Norge. Kode: %1. Forklaring:%2	"
                sEnglish = "15090: The Norwegian National Bank require that You state a valid code and an explanation for foreign payments debited a norwegian bankaccount. Code: %1. Explanation:%2	"
                sSwedish = "15090: Riksbanken kräver att du anger en giltig kod och förklaring av vad betalningen gäller vid utlandsbetalningar som belastas ett konto i Sverige. Kod: %1. Förklaring:%2"
                sDanish = "15090: Nationalbanken kræver, at du angiver en gyldig kode og forklaring på, hvad betalingen gælder for, ved udlandsbetalinger en konto i Danmark. Kode: %1. Forklaring:%2"
            Case 15091
                sNorwegian = "15091: Filen som forsøkes importert er fra en annen bank enn hva som er satt opp i profilen. Bank i profil: %1. Bank i fil: %2. Fil: %3	"
                sEnglish = "15091: The file beeing imported is sent from another bank than specified in the profile. Bank in profile: %1. Bank ni file: %2. Filname: %3	"
                sSwedish = "15091: Filen som du försöker importera är från en annan bank än vad som angetts i profilen. Bank i profil: %1. Bank i fil: %2. Fil: %3"
                sDanish = "15091: Filen som forsøges importeret er fra en anden bank end den, der er indstillet i profilen. Bank i profil: %1. Bank i fil: %2. Fil: %3"
            Case 15092
                sNorwegian = "15092: Det ble ikke funnet et SEQ-segment for dette LIN-segmentet.	"
                sEnglish = "15092: No SEQ-segment found for this LIN-segment. BIC (Swift) must be set for EUR-payments to this receivercountry IBAN must be used for EUR-payments to this receivercountry	"
                sSwedish = "15092: Hittade inget SEQ-segment för detta LIN-segment."
                sDanish = "15092: Der blev ikke fundet et SEQ-segment for dette LIN-segment."
            Case 15093
                sNorwegian = "15093: Det må være angitt BIC (Swift) for betalinger i EUR til dette mottakerland	"
                sEnglish = "15093: BIC (Swift) must be set for EUR-payments to this receivercountry"
                sSwedish = "15093: BIC (Swift) måste anges för betalninger i EUR till detta mottagarland"
                sDanish = "15093: Der skal være angivet BIC (Swift) for betalinger i EUR til dette modtagerland"
            Case 15094
                sNorwegian = "15094: Det må være angitt BBAN for betalinger i EUR til dette mottakerland	"
                sEnglish = "15094: BBAN must be used for EUR-payments to this receivercountry"
                sSwedish = "15094: IBAN måste anges för betalninger i EUR till detta mottagarland"
                sDanish = "15094: Der skal være angivet IBAN for betalinger i EUR til dette modtagerland"
            Case 15095
                sNorwegian = "15095: Vennligst bruk enten kode for korresponderende bank, BIC (SWIFT) eller FedWire	"
                sEnglish = "15095: Please include either Swiftcode, Fedwire or Intermediary bankcode	"
                sSwedish = "15095: Använd antingen kod för korrespondentbank, BIC (SWIFT) eller FedWire"
                sDanish = "15095: Brug enten kode for korresponderende bank, BIC (SWIFT) eller FedWire"
            Case 15096
                sNorwegian = "15096: Hvis korresponderende bank, vennligst spesifiser enten BIC (Swift) eller mottakers kontonummer	"
                sEnglish = "15096: When using Intermediary bank, please specify either a valid BIC (SWIFT) or receivers accountnumber	"
                sSwedish = "15096: Om korrespondentbank används ska antingen BIC (Swift) eller mottagarens kontonummer anges"
                sDanish = "15096: Hvis korresponderende bank, specificeres enten BIC (Swift) eller modtagers kontonummer"
            Case 15097
                sNorwegian = "15097: Hvis IBAN benyttes, vennligst benytt BIC (SWIFT)	"
                sEnglish = "15097: If IBAN used, BIC mandatory	"
                sSwedish = "15097: Om IBAN används ska BIC (SWIFT) anges"
                sDanish = "15097: Hvis IBAN benyttes, anvendes BIC (SWIFT)"
            Case 15098
                sNorwegian = "15098: Det må være mellom 11 og 14 siffer i mottakerkonto i Danmark	"
                sEnglish = "15098: There must be between 11 and 14 digits in receivers account in Denmark	"
                sSwedish = "15098: Mottagarkonton i Danmark måste ha mellan 11 och 14 siffror"
                sDanish = "15098: Der skal være mellem 11 og 14 cifre i modtagerkontoen i Danmark"
            Case 15099
                sNorwegian = "15099: Kun tillatt med siffer i danske kontonummer	"
                sEnglish = "15099: Digits only in Danish accountnumbers	"
                sSwedish = "15099: Endast tillåtet med siffror i danska kontonummer"
                sDanish = "15099: Kun tilladt med cifre i danske kontonumre"
            Case 15100
                sNorwegian = "15100: Ved SEPA-betalinger må bankomkostninger endres til delte omkostninger (SHA)	"
                sEnglish = "15100: For SEPA-payments the bankcharges must be changed to shared (SHA)	"
                sSwedish = "15100: Vid SEPA-betalningar måste bankavgifter ändras till delade kostnader (SHA)"
                sDanish = "15100: Ved SEPA-betalinger skal bankomkostninger ændres til delte omkostninger (SHA)"
            Case 15101
                sNorwegian = "15101: CONTRL-filene som ble mottatt fra Fokus Bank er ikke komplett. Minst en CONTRL mangler.	"
                sEnglish = "15101: The CONTRL-files received from Fokus Bank is not complete.	"
                sSwedish = "15101: CONTRL-filerna som mottogs från Fokus Bank är ofullständiga. Minst en CONTRL saknas."
                sDanish = "15101: CONTRL-filerne som blev modtaget fra Fokus Bank er ikke komplette. Mindst en CONTRL mangler."
            Case 15102
                sNorwegian = "15102: Det ma angis en Fedwire, som starter med 0 for locale betalinger Canada	"
                sEnglish = "15102: Must have Fedwire, starting with 0 for local payments Canada	"
                sSwedish = "15102: Du måste ange en Fedwire som börjar med 0 för lokala betalningar i Kanada"
                sDanish = "15102: Der skal angives en Fedwire, som starter med 0 for lokale betalinger Canada"
            Case 15103
                sNorwegian = "15103: Må ha ABA kode, 6 siffer, for lokale betalinger i Australia	"
                sEnglish = "15103: Must have ABA, 6 digits for local payments Australia	"
                sSwedish = "15103: Du måste ange ABA-kod, 6 siffror, för lokala betalningar i Australien"
                sDanish = "15103: Skal have ABA kode, 6 cifre, for lokale betalinger i Australien"
            Case 15104
                sNorwegian = "15104: Debetkontoens valuta må være den samme som betalingsvalutaen. Se Klient-oppsettet i BabelBank.	"
                sEnglish = "15104: DebitAccounts currency must be the same as the paymentcurrency. Please refer to the CLient-setup in BabelBank.	"
                sSwedish = "15104: Debetkontots valuta måste vara samma som betalningsvalutan. Se Klient-vyn i BabelBank."
                sDanish = "15104: Debetkontoens valuta skal være den samme som betalingsvalutaen. Se Klient-opsætningen i BabelBank."
            Case 15105
                sNorwegian = "15105: Må ha Fedwire, 9 siffer, for lokale betalinger i Canada	"
                sEnglish = "15105: Must have Fedwire, 9 digits, for local payments in Canada	"
                sSwedish = "15105: Måste ha Fedwire, 9 siffror, för lokala betalningar i Kanada"
                sDanish = "15105: Skal have Fedwire, 9 cifre, for lokale betalinger i Canada"
            Case 15106
                sNorwegian = "15106: BabelBank er ikke tilpasset for å lage dette formatet. Format: %1. Bank: %2. ERROR EXPORTING FILES	"
                sEnglish = "15106: BabelBank is not adapted to create this format. Format: %1. Bank: %2	"
                sSwedish = "15106 BabylonBank är inte anpassat för att skapa det här formatet. Format: %1 Bank: %2 ERROR EXPORTING FILES"
                sDanish = "15106: BabelBank er ikke tilpasset til at oprette dette format. Format: %1. Bank: %2. ERROR EXPORTING FILES"
            Case 15107
                sNorwegian = "15107: ERROR EXPORTING FILES"
                sEnglish = "15107: ERROR EXPORTING FILES	"
                sSwedish = "15107: ERROR EXPORTING FILES"
                sDanish = "15107: ERROR EXPORTING FILES"
            Case 15108
                sNorwegian = "15108: Betalinger som skal krediteres konti i Tyskland krever 11 karakterer i BIC adressen."
                sEnglish = "15108: Payments to be credited accounts located in Germany requires 11 characters in BIC address."
                sSwedish = "15108 Betalningar som ska krediteras på konton i Tyskland kräver 11 tecken i BIC-adressen."
                sDanish = "15108: Betalinger som skal krediteres konti i Tyskland kræver 11 tegn i BIC adressen."
            Case 15109
                sNorwegian = "15109: Det må være maksimum 34 siffer i mottakerkonto i Singapore"
                sEnglish = "15109: There must be a maximum of 34 digits in receivers account in Singapore"
                sSwedish = "15109: Det får finnas högst 34 siffror på mottagarkonto i Singapore"
                sDanish = "15109: Der må maksimalt være 34 cifre i modtagerkontoen i Singapore"
            Case 15110
                sNorwegian = "15110: Betalinger med BBAN og BLZ må være hastebetalinger (Urgent).2"
                sEnglish = "15110: Payments with BBAN and BLZ must be prioritypayments (Urgent)"
                sSwedish = "15110 Betalningar med BBAN och BLZ måste vara brådskande betalningar (Urgent)."
                sDanish = "15110: Betalinger med BBAN og BLZ skal være hastebetalinger (Urgent)."
            Case 15111
                sNorwegian = "15111: Purposecode skal være på 4 tegn"
                sEnglish = "15111: Purposecode must be 4 characters"
                sSwedish = "15111: Purposecode skall vara 4 tegn"
                sDanish = "15111: Purposecode skal være på 4 tegn"
            Case 15112
                sNorwegian = "15112: LocalCheck er ikke tillatt i Australia."
                sEnglish = "15112: LocalCheck not allowed in Australia."
                sSwedish = "15112: LocalCheck är inte tillåtet i Australien."
                sDanish = "15112: LocalCheck er ikke tilladt i Australia."


                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 15120
            Case 15120
                sNorwegian = "15120: Ingen mottaker-ID er angitt." & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15120: No receiverident is stated." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15120: Ingen mottagere-ID specificerad." & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15120: Ingen modtager-ID specificeret." & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15121
                sNorwegian = "15121: Ingen avsender-ID er angitt." & vbCrLf & "Dette kan legges inn under firmaopplysninger i feltet 'Foretaksnummer'" & vbCrLf & vbCrLf & "Er du i tvil, vennligst kontakt din forhandler."
                sEnglish = "15121: No sender-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                sSwedish = "15121: Nej sender-ID anges." & vbCrLf & "Detta kan tas upp under företagsinformasjon i fältet 'Företagsnummer'" & vbCrLf & vbCrLf & "Om du är osäker, vänligen kontakta din återförsäljare."
                sDanish = "15121: Ingen sender-ID er angivet." & vbCrLf & "Dette kan indtastes under selskabets oplysninger i feltet 'Firmanummer'" & vbCrLf & vbCrLf & "Er du i tvivl, venligst kontakt din forhandler."
            Case 15122
                sNorwegian = "15122: Det er ikke angitt et DBTS brukernummer." & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15122: No DBTS user nummer is stated." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15122: Det anges inte en DBTS bruksnummer." & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15122: Det er ikke angivet en DBTS bruksnummer." & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15123
                sNorwegian = "15123: Det er ikke angitt et avtalenummer." & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15123: No Agreementnumber is stated." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15123: Det är inte skrivit ett avtalsnummer." & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15123: Det er ikke indtastet en traktat nummer." & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15124
                sNorwegian = "15124: Muligheten til å sende internasjonale betalinger fra konti i Danmark" & vbCrLf & "er ennå ikke implementert i BabelBank" & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15124: Possibility to send international payments from Denmark" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15124: Möjligheten att skicka internationella betalningar från konton i Danmark" & vbCrLf & "är ännu inte genomförts i BabelBank" & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15124: Evnen til at sende internationale betalinger fra konti i Danmark" & vbCrLf & "er endnu ikke implementeret i BabelBank" & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15125
                sNorwegian = "15125: Muligheten til å sende internasjonale betalinger fra konti i Sverige" & vbCrLf & "er ennå ikke implementert i BabelBank" & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15125: Possibility to send international payments from Sweden" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15125: Möjligheten att skicka internationella betalningar från konton i Sverige" & vbCrLf & "är ännu inte genomförts i BabelBank" & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15125: Evnen til at sende internationale betalinger fra konti i Sverige" & vbCrLf & "er endnu ikke implementeret i BabelBank" & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15126
                sNorwegian = "15126: Muligheten til å sende betalinger fra konti i %1" & vbCrLf & "er ennå ikke implementert i BabelBank" & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15126: Possibility to send payments from %1" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15126: Möjligheten att skicka betalningar från konton i %1" & vbCrLf & "är ännu inte genomförts i BabelBank" & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15126: Evnen til at sende betalinger fra konti i %1" & vbCrLf & "er endnu ikke implementeret i BabelBank" & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15127
                sNorwegian = "15127: Muligheten til å angi meldinger til bank" & vbCrLf & "er ennå ikke implementert i BabelBank" & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15127: Possibility to state bank notifications" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15127: Förmåga att tilldela meddelanden till bank" & vbCrLf & "är ännu inte genomförts i BabelBank" & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15127: Muligheten til å angi meldinger til bank" & vbCrLf & "er endnu ikke implementeret i BabelBank" & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15128
                sNorwegian = "15128: Det er ikke angitt en mottaker-ID ('Recipient Identification')." & vbCrLf & "Dette kan legges inn under firmaopplysninger i feltet 'Kunde-ID'" & vbCrLf & vbCrLf & "Er du i tvil, vennligst kontakt din forhandler."
                sEnglish = "15128: No 'Recipient Identification' is stated." & vbCrLf & "This may be entered in the company setup in the field 'AdditionalNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                sSwedish = "15128: Det är inte angiven mottagare-ID ('Recipient Identification')." & vbCrLf & "Detta kan tas upp under företagsinformation i 'kund-ID'" & vbCrLf & vbCrLf & "Om du är osäker, vänligen kontakta din återförsäljare."
                sDanish = "15128:  Det er ikke angivet en modtager-ID ('Recipient Identification')." & vbCrLf & "Dette kan indtastes under selskabets oplysninger i 'Kunde-id'" & vbCrLf & vbCrLf & "Er du i tvivl, venligst kontakt din forhandler."
            Case 15129
                sNorwegian = "15129: Muligheten til å belaste konti i andre banker" & vbCrLf & "er ennå ikke implementert i BabelBank" & vbCrLf & vbCrLf & "Vennligst kontakt din forhandler."
                sEnglish = "15129: Possibility to send payments from another bank" & vbCrLf & "is not yet implemented in BabelBank." & vbCrLf & vbCrLf & "Please contact Your dealer."
                sSwedish = "15129: Möjligheten att ladda konton i andra banker" & vbCrLf & "är ännu inte genomförts i BabelBank" & vbCrLf & vbCrLf & "Vänligen kontakta din återförsäljare."
                sDanish = "15129: Muligheten til at oplade konti i andre banker" & vbCrLf & "er endnu ikke implementeret i BabelBank" & vbCrLf & vbCrLf & "Venligst kontakt din forhandler."
            Case 15130
                sNorwegian = "15130: Feil overføringsvaluta angitt."
                sEnglish = "15130: Incorrect transfercurrency stated on the payment."
                sSwedish = "15130: Fel överföringsvaluta har angetts."
                sDanish = "15130: Forkert overførselsvaluta angivet."
            Case 15131
                sNorwegian = "15131: Bruk av terminkontrakter er ennå ikke implementert for dette formatet."
                sEnglish = "15131: The use of 'Forward contract rate' is not yet implemented in this format."
                sSwedish = "15131: Användning av kontrakt ännu inte genomförts för detta format."
                sDanish = "15131: Anvendelse af kontrakter er endnu ikke implementeret for dette format."
            Case 15132
                sNorwegian = "15132: Det er ikke angitt et terminkontraktsnummer selv om en terminkurs er angitt."
                sEnglish = "15132: No forward contract number is stated even though a forward exchange rate is stated."
                sSwedish = "15132: Det är inte angett ett terminskontrakt siffror även om en terminspriset är inställd."
                sDanish = "15132: Der er ikke indtastet en terminskontrakt numre selv om en fremad prisen er sat."
            Case 15133
                sNorwegian = "15133: Det er ikke angitt en avtale id selv om en avtalt kurs er angitt."
                sEnglish = "15133: No agreement is stated even though an agreed exchangerate is stated."
                sSwedish = "15133: Det är inte ingått avtal id även en överenskommen kurs ställs in."
                sDanish = "15133:  Der er ikke indgået en aftale id selvom en aftalt kurs er indstillet."
            Case 15134
                sNorwegian = "15134: Det er ikke angitt en egenreferanse på oppdraget."
                sEnglish = "15134: No customer reference (own reference) is stated."
                sSwedish = "15134: Det anges inte en hänvisning på uppdraget."
                sDanish = "15134: Det er ikke angivet en henvisning på oppdraget."
            Case 15135
                sNorwegian = "15135: Totalbeløpet av alle fakturaer, som er slått sammen på grunn av meldingsteksten, er negativ."
                sEnglish = "15135: The total amount of all invoices joined regarding the freetext is negative."
                sSwedish = "15135: Totalbeloppet av alle fakturor, som är sammanslagna  på grund av meddelandetexten , er negativ."
                sDanish = "15135: Samledebeløb af alle fakturaer, som er slået sammen på grund af beskedteksten, er negativ."
            Case 15136
                sNorwegian = "15136: Mottakers kontonummer finnes ikke på betalingen. BabelBank kan ikke lage en gyldig CREMUL-fil."
                sEnglish = "15136: Receivers acount are not stated on the payment. Can't create a valid CREMUL-file."
                sSwedish = "15136: Mottagarens kontonummer existerar inte på betalning. BabelBank kan inte göra en giltig CREMUL-fil."
                sDanish = "15136: Modtager kontonummer findes ikke på betaling. BabelBank kan ikke lave en gyldig CREMUL-fil."
            Case 15137
                sNorwegian = "15137: Det er ikke angitt en bankreferanse på betalingen."
                sEnglish = "15137: No bankreference is stated."
                sSwedish = "15137: Det är inte indicerat en bankreferens på betalningen."
                sDanish = "15137: Det er ikke angivet en bankhenvisning på betalingen."
            Case 15138
                sNorwegian = "15138: Det er ikke angitt noen KID på betalingen, selv om betalingen er markert som en OCR-betaling."
                sEnglish = "15138: No KID stated on the payment even if it is marked as a KID payment."
                sSwedish = "15138: Det är inte indicerat noen KID på betalingen, även om betalningen är markerad som en OCR-betalning."
                sDanish = "15138: Det er ikke angivet noen KID på betalingen, selv om betalingen er markeret som en OCR-betaling."
            Case 15139
                sNorwegian = "15139: BabelBank er ikke tilpasset denne betalingstypen."
                sEnglish = "15139: BabelBank is not adaptes to this paymenttype."
                sSwedish = "15139: BabelBank är inte anpassad denna betalningstyp."
                sDanish = "15139: BabelBank er ikke tilpasset denne betalingstypen."
            Case 15140
                sNorwegian = "15140: Betalinger hvor det benyttes terminhandler er ennå ikke implementert for dette formatet."
                sEnglish = "15140: The use of 'Forward contract rate' is not yet implemented in BabelBank for this format."
                sSwedish = "15140: Betalningar som innebär användning av terminshandel är ännu inte implementeras för detta format."
                sDanish = "15140: Betalinger som indebærer anvendelse af terminskontrakter er endnu ikke implementeret for dette format."
            Case 15141
                sNorwegian = "15141: Debitors navn (betaler) er for langt. Maksimum 70 posisjoner."
                sEnglish = "15141: The name of debtor (the ordering party is too long. Maximum 70 positions."
                sSwedish = "15141: Debtors namn (betaler) är för långt. Maximum 70 posisjoner."
                sDanish = "15141: Debitors navn (betaler) er for langt. Maksimum 70 posisjoner."
            Case 15142
                sNorwegian = "15142: Betalers kontonummer er ikke et gyldig IBAN nummer. " & vbCrLf & "Konto: %1"
                sEnglish = "15142: Payers accountnumber is not a valid IBAN number." & vbCrLf & "Account: %1"
                sSwedish = "15142: Betalarens kontonummer är inte ett giltigt IBAN nummer. " & vbCrLf & "Konto: %1"
                sDanish = "15142: Betalers kontonummer er ikke et gyldig IBAN nummer. " & vbCrLf & "Konto: %1"
            Case 15143
                sNorwegian = "15143: Betalers kontonummer må være et IBAN nummer. " & vbCrLf & "Konto: %1"
                sEnglish = "15143: Payers accountnumber must be an IBAN number." & vbCrLf & "Account: %1"
                sSwedish = "15143: Betalarens kontonummer måste vara ett IBAN nummer. " & vbCrLf & "Konto: %1"
                sDanish = "15143: Betalers kontonummer må være et IBAN nummer. " & vbCrLf & "Konto: %1"
            Case 15144
                sNorwegian = "15144: En gyldig BIC/SWIFT-kode må være angitt for betalers kontonummer." & vbCrLf & _
                "Det må angis i BabelBank sitt klientoppsett." & vbCrLf & "Angitt Swift: %1"
                sEnglish = "15144: A valid BIC/SWIFT-code must be stated for the payers account number." & vbCrLf & _
                "It must be stated in the clientsetup of BabelBank." & vbCrLf & "Swift stated: %1"
                sSwedish = "15144: En giltig BIC/SWIFT-kode måste anges för betalarens  kontonummer." & vbCrLf & _
                              "Det skall anges i BabelBank sitt klientoppsett." & vbCrLf & "Angivt Swift: %1"
                sDanish = "15144: En gyldig BIC/SWIFT-kode skal være angivne for betalers kontonummer." & vbCrLf & _
                              "Det skal angives i BabelBank sitt klientoppsett." & vbCrLf & "Angivne Swift: %1"
            Case 15145
                sNorwegian = "15145: En gyldig valutakode må angis på betalers kontonummer." & vbCrLf & _
                "Det må angis i BabelBank sitt klientoppsett." & vbCrLf & "Kontonummer: %1"
                sEnglish = "15145: A valid currencycode must be stated for the payers account number." & vbCrLf & _
                "It must be stated in the clientsetup of BabelBank." & vbCrLf & "Account number: %1"
                sSwedish = "15145: En giltigt valutakod måste anges på betalers kontonummer." & vbCrLf & _
                              "Det måste anges i BabelBank klientinställning." & vbCrLf & "Kontonummer: %1"
                sDanish = "15145: En gyldig valutakode skal angives på betalers kontonummer." & vbCrLf & _
                              "Det skal angives i BabelBank sitt klientoppsett." & vbCrLf & "Kontonummer: %1"
            Case 15146
                sNorwegian = "15146: Overføringsvalutaen må enten være identisk med kontoens valuta, " & vbCrLf & _
                "eller valutasorten til beløpet som er angitt." & vbCrLf & _
                "Overføringsvaluta: %1" & vbCrLf & _
                "Kontoens valuta: %2" & vbCrLf & _
                "Beløpets valuta: %3"
                sEnglish = "15146: The transfercurrency of the amount to be paid must either equal the account's currency " & vbCrLf & _
                "or the currency of the amount stated on the payment." & vbCrLf & _
                "Transfer currency: %1" & vbCrLf & _
                "Account currency: %2" & vbCrLf & _
                "Invoice currency: %3"
                sSwedish = "15146: Överföringsvalutan måste antingen vara identisk med den kontots valuta, " & vbCrLf & _
                              "eller valutaen i det anførte beløb." & vbCrLf & _
                              "Överföringsvaluta: %1" & vbCrLf & _
                              "Kontots valuta: %2" & vbCrLf & _
                              "Beløbets valuta: %3"
                sDanish = "15146: Overføringsvalutaen må enten være identisk med kontoens valuta, " & vbCrLf & _
                              "eller valutasorten til beløbet som er angitt." & vbCrLf & _
                              "Overføringsvaluta: %1" & vbCrLf & _
                              "Kontoens valuta: %2" & vbCrLf & _
                              "Beløbets valuta: %3"
            Case 15147
                sNorwegian = "15147: Det er angitt en ugyldig valutasort på beløpet som skal overføres." & vbCrLf & _
                "Beløpets valuta: %1"
                sEnglish = "15147: The currency code stated on the amount to be paid is not valid." & vbCrLf & _
                "Invoice currency: %1"
                sSwedish = "15147: Det er angitt en ogiltig valutasort på beløbet som skal överföras." & vbCrLf & _
                              "Beløbets valuta: %1"
                sDanish = "15147: Det er angitt en ugyldig valutasort på beløbet som skal overført." & vbCrLf & _
                              "Beløpets valuta: %1"
            Case 15148
                sNorwegian = "15148: BIC(SWIFT) koden må være på enten 8 eller 11 posisjoner." & vbCrLf & _
                "BIC: %1"
                sEnglish = "15148: The BIC(SWIFT) code must be either 8 or 11 characters long." & vbCrLf & _
                "BIC: %1"
                sSwedish = "15148: BIC(SWIFT) kod måste vara antingen 8 eller 11 tecken." & vbCrLf & _
                             "BIC: %1"
                sDanish = "15148: BIC(SWIFT) kode skal være enten 8 eller 11 karakterer." & vbCrLf & _
                             "BIC: %1"
            Case 15149
                sNorwegian = "15149: Når mottakers navn er angitt så må også betalers landkode angis med 2 posisjoner." & vbCrLf & _
                "Mottakers navn: %1"
                sEnglish = "15149: When receivers name is stated then a valid country code (2 positions) must also be stated." & vbCrLf & _
                "Receivers name: %1"
                sSwedish = "15149: När mottagarens namn anges så måste också betalars landskod anges med 2 tecken." & vbCrLf & _
                              "Mottagarens namn: %1"
                sDanish = "15149: Når modtagerens navn indtastes så skal også betalers landekode angives med to positioner." & vbCrLf & _
                              "Modtagers navn: %1"
            Case 15150
                sNorwegian = "15150: Maksimum 2 adresselinjer kan angis for mottaker, for betalinger via DNB." & vbCrLf & _
                "Betalers navn: %1"
                sEnglish = "15150: Maximum 2 addresslines for receiver may be stated, for payments via DNB Germany." & vbCrLf & _
                "Payers name: %1"
                sSwedish = "15150: Maximum 2 adresslinjerna kan indikeras för mottagar, för betalingar via DNB." & vbCrLf & _
                             "Betalers navn: %1"
                sDanish = "15150: Maksimum 2 adresselinier kan angivs for modtager, for betalinger via DNB." & vbCrLf & _
                             "Betalers navn: %1"
            Case 15151
                sNorwegian = "15151: For denne typen betaling må det angis et gyldig IBAN-nummer." & vbCrLf & _
                "Mottakers konto: %1"
                sEnglish = "15151: A valid IBAN-number must be stated for this type of payment." & vbCrLf & _
                "Receivers account: %1"
                sSwedish = "15151: För denna typ av betalning måste det anges ett giltigt IBAN-nummer." & vbCrLf & _
                             "Mottagarsen konto: %1"
                sDanish = "15151: For denne typen betaling skal det angives et gyldig IBAN-nummer." & vbCrLf & _
                             "Modtagers konto: %1"
            Case 15152
                sNorwegian = "15152: Det kan maksimalt angis %2 spesifikasjoner på strukturerte betalinger (KID/Fakturanr/Kred.nota)." & vbCrLf & _
                "Mottakers navn: %1"
                sEnglish = "15152: Maximum %2 specifications may be used for structured payments." & vbCrLf & _
                "Receivers name: %1"
                sSwedish = "15152: Det kan maximalt anges %2 specifikationer på strukturerade betalningar (KID/Fakturanr/Kred.nota)." & vbCrLf & _
                              "Mottagarens namn: %1"
                sDanish = "15152: Det kan maksimalt angives %2 specifikationer på strukturerede betalinger (KID/Fakturanr/Kred.nota)." & vbCrLf & _
                              "Modtagers navn: %1"
            Case 15153
                sNorwegian = "15153: Lengde på meldingstekst til mottaker kan ikke overstige %1 tegn." & vbCrLf & _
                "Lengden på meldingstekst (inkludert beløp): %2" & vbCrLf & _
                "Mottakers navn: %3"
                sEnglish = "15153: The maximum limit of the message to receiver is %1 characters." & vbCrLf & _
                "Length on this payment: %2" & vbCrLf & _
                "Receivers name: %3"
                sSwedish = "15153: Längd på meddelandetext  till mottager får inte överstiga %1 tegn." & vbCrLf & _
                             "Längd på meddelandetext (inkludert beløb): %2" & vbCrLf & _
                             "Mottagarens namn: %3"
                sDanish = "15153: Længde på meldingstekst til modtager kan ikke overstige %1 tegn." & vbCrLf & _
                             "Længden på meldingstekst (inkludert beløb): %2" & vbCrLf & _
                             "Modtagers navn: %3"
            Case 15154
                sNorwegian = "15154: Et gyldig organisasjonsnummer må være angitt." & vbCrLf & _
                "Det må angis i BabelBank sitt klientoppsett som divisjon."
                sEnglish = "15154: A valid organization ID must be stated." & vbCrLf & _
                "It must be stated in the clientsetup of BabelBank as division."
                sSwedish = "15154: Ett giltigt organisationsnummer måste anges." & vbCrLf & _
                              "Det må anges i BabelBank sitt klientoppsett som division."
                sDanish = "15154: Et gyldigt organisasjonsnummer skal angives." & vbCrLf & _
                              "Det skal angives i BabelBank sitt klientoppsett som division."
            Case 15155
                sNorwegian = "15155: Ved SEPA-betaling må valutasort være EUR."
                sEnglish = "15155: SEPA payments must be in EUR."
                sSwedish = "15155: Genom SEPA-betaling skall vara valuta EUR."
                sDanish = "15155: SEPA-betaling skal være valuta EUR."
            Case 15156
                sNorwegian = "15156: BabelBank er ikke tilpasset den valgte banks dialekt av ISO20022." & vbCrLf & "Bank: %1"
                sEnglish = "15156: BabelBank is not adopted to the selected banks dialect of ISO20022." & vbCrLf & "Bank: %1"
                sSwedish = "15156: BabelBank är inte anpassad till den valda banks dialekt av ISO20022." & vbCrLf & "Bank: %1"
                sDanish = "15156: BabelBank er ikke tilpasset den valgte banks dialekt av ISO20022." & vbCrLf & "Bank: %1"
            Case 15157
                sNorwegian = "15157: Bank Party ID må angis for Finland og de Baltiske landene."
                sEnglish = "15157: Bank Party ID is mandatory for Finland and the Baltic countries."
                sSwedish = "15157: Bank Party ID måste anges for Finland och de Baltiska länderna."
                sDanish = "15157: Bank Party ID må angives for Finland og de Baltiske lande."
            Case 15158
                sNorwegian = "15158: Kontoens valuta må angis ved utenlandsbetalinger og svenske hastebetalinger."
                sEnglish = "15158: Account currency is required for international and high value payments in Sweden."
                sSwedish = "15158: Kontoens valuta måste anges ved utenlandsbetalinger og svenske expressbetalningar."
                sDanish = "15158: Kontoens valuta må angives ved utenlandsbetalinger og svenske hastebetalinger."
            Case 15159
                sNorwegian = "15159: Det er ikke angitt en gyldig valutasort overføringsbeløpet (kredit valuta)." & vbCrLf & "Beløp skal angis i denne valutaen og det er derfor obligatorisk."
                sEnglish = "15159: A valid credit currency is not stated." & vbCrLf & "The currency is mandatory because the value stated must be in this currency."
                sSwedish = "15159: Det är inte angivt en giltig valuta överföringsbelopp  (kredit valuta)." & vbCrLf & "Beløp skal angis i denne valutaen og det er derfor obligatorisk."
                sDanish = "15159: Det er ikke angitt en gyldig valutasort overføringsbeløbet (kredit valuta)." & vbCrLf & "Beløp skal angis i denne valutaen og det er derfor obligatorisk."
            Case 15160
                sNorwegian = "15160: Det må angis enten BIC eller mottakers bank for betalinger til utlandet."
                sEnglish = "15160: For international payments, either a BIC or the receiving banks name and address must be stated."
                sSwedish = "15160: Det måste anges antingen BIC eller mottagars bank för betalningar til utlandet."
                sDanish = "15160: Det må angives enten BIC eller modtagerens bank for betalinger til udlandet."
            Case 15161
                sNorwegian = "15161: Den angitte bankkodetypen er ikke implementert for dette formatet." & vbCrLf & "Intern bankkodetype angitt: %1"
                sEnglish = "15161: The stated bank branchtype is not implemented for this format." & vbCrLf & "Internal branchtype stated: %1"
                sSwedish = "15161: Den angivna bankkodtyp är inte implementert för detta formatet." & vbCrLf & "Intern bankkodtyp angivn: %1"
                sDanish = "15161: Den angivne bankkodetypen er ikke implementert for dette formatet." & vbCrLf & "Intern bankkodetype angivt: %1"
            Case 15162
                sNorwegian = "15162: Et ulovlig tegn ble oppdaget i betalingen." & vbCrLf & "Ulovlig(e) tegn: %1" & vbCrLf & "Ulovlig tekst (først posisjoner): %2"
                sEnglish = "15162: An illegal charcter was found in the payment." & vbCrLf & "Illegal character(s): %1" & vbCrLf & "Illegal text (first posistions): %2"
                sSwedish = "15162: Ett ogiltigt tecken upptäcktes i betalningen." & vbCrLf & "Ulovlig(e) tegn: %1" & vbCrLf & "Ulaglig text (først posisioner): %2"
                sDanish = "15162: Et ulovlig tegn blev opdaget i betalingen." & vbCrLf & "Ulovlig(e) tegn: %1" & vbCrLf & "Ulovlig tekst (først posisioner): %2"
            Case 15163
                sNorwegian = "15163: Lengde på unik ID til mottaker (KID) kan ikke overstige %1 tegn." & vbCrLf & _
                "Unik ID: %2" & vbCrLf & _
                "Mottakers navn: %3"
                sEnglish = "15163: The maximum limit of the unique ID to receiver is %1 characters." & vbCrLf & _
                "Unique ID: %2" & vbCrLf & _
                "Receivers name: %3"
                sSwedish = "15163: Längd på unik ID till mottagaren (KID) kan inte överstiga %1 teck." & vbCrLf & _
                             "Unik ID: %2" & vbCrLf & _
                             "Mottagaers namn: %3"
                sDanish = "15163: Længde på unik ID til modtager (KID) kan ikke overstige %1 tegn." & vbCrLf & _
                             "Unik ID: %2" & vbCrLf & _
                             "Modtagers navn: %3"
            Case 15164
                sNorwegian = "15164: Bankkode er obligatorisk for betalinger til dette landet."
                sEnglish = "15164: Branchcode is mandatory for payments to this country."
                sSwedish = "15164: Bankkod är obligatoriskt för betalningar till detta land."
                sDanish = "15164: Bankkode er obligatorisk for betalinger til dette landet."
            Case 15165
                sNorwegian = "15165: Lengden på adressen til banken kan ikke overstige 140 posisjoner."
                sEnglish = "15165: Total length of the bank's address can't be longer than 140 characters."
                sSwedish = "15165: Længden på adressen till banken kan inte överstiga 140 posisjoner."
                sDanish = "15165: Længden på adressen til banken kan ikke overstige 140 posisjoner."
            Case 15166
                sNorwegian = "15166: Når mottakers banks navn og adresse er angitt så er bankens landkode obligatorisk."
                sEnglish = "15166: When the receivers bank's name and address is stated, the bank's countrycode is mandatory."
                sSwedish = "15166: När mottagars banks namn och adress är angivtet så er bankens landkode obligatorisk."
                sDanish = "15166: Når modtagers banks navn og adresse er angivet så er bankens landkode obligatorisk."
            Case 15167
                sNorwegian = "15167: Mottakers navn er obligatorisk på sjekkutbetalinger."
                sEnglish = "15167: Receiver's name is mandatory for check-payments."
                sSwedish = "15167: Mottagars namn är obligatorisk på checkutbetalningar."
                sDanish = "15167: Modtagers navn er obligatorisk på checkudbetalinger."
            Case 15168
                sNorwegian = "15168: Mottakers adresse er obligatorisk på sjekkutbetalinger."
                sEnglish = "15168: Receiver's address is mandatory for check-payments."
                sSwedish = "15168: Mottagars adress är obligatorisk på checkutbetalningar."
                sDanish = "15168: Modtagers adresse er obligatorisk på checkudbetalinger."
            Case 15169
                sNorwegian = "15169: Mottakers by er obligatorisk på sjekkutbetalinger."
                sEnglish = "15169: Receiver's city is mandatory for check-payments."
                sSwedish = "15169: Mottagars stad är obligatorisk på checkutbetalningar."
                sDanish = "15169: Modtagers by er obligatorisk på checkudbetalinger."
            Case 15170
                sNorwegian = "15170: Lengden på adressen til mottaker kan ikke overstige 140 posisjoner."
                sEnglish = "15170: Total length of the receiver's address can't be longer than 140 characters."
                sSwedish = "15170: Längden på adressen till mottagaren kan inte överstiga 140 positioner."
                sDanish = "15170: Længden på adressen til modtageren kan ikke overstige 140 positioner."
            Case 15171
                sNorwegian = "15171: Lengden på postnummeret til mottaker kan ikke overstige 9 posisjoner."
                sEnglish = "15171: Total length of the receiver's zipcode can't be longer than 9 characters."
                sSwedish = "15171: Längden på postnumret till mottagaren kan inte överstiga 9 positioner."
                sDanish = "15171: Længden på postnummeret til modtageren kan ikke overstige 9 positioner."
            Case 15172
                sNorwegian = "15172: Lengden på poststed til mottaker kan ikke overstige %1 posisjoner."
                sEnglish = "15172: Total length of the receiver's townname can't be longer than %1 characters."
                sSwedish = "15172: Längden på postort till mottagaren kan inte överstiga %1 positioner."
                sDanish = "15172: Længden på by til modtageren kan ikke overstige %1 positioner."
            Case 15173
                sNorwegian = "15173: Når strukturert adresse benyttes er landkode obligatorisk."
                sEnglish = "15173: When using structured address, receiver's countrycode is mandatory."
                sSwedish = "15173: När strukturerad adress används är landskod obligatorisk."
                sDanish = "15173: Når strukturert adresse bruges er landkode obligatorisk."
            Case 15174
                sNorwegian = "15174: Landkode er obligatorisk på utlandsbetalinger når adresse er angitt."
                sEnglish = "15174: Countrycode is mandatory for international payments when an address i s stated."
                sSwedish = "15174: Landkod är obligatorisk på utlandsbetalningar när adresse anges."
                sDanish = "15174: Landkode er obligatorisk på udlandsbetalinger når adresse er angivt."
            Case 15175
                sNorwegian = "15175: Mottakers navn er obligatorisk for betalinger i dette landet."
                sEnglish = "15175: Receiver's name is mandatory for payments in this country."
                sSwedish = "15175: Mottagars namn är obligatorisk för betalningar i detta landet."
                sDanish = "15175: Modtagers navn er obligatorisk for betalinger i dette landet."
            Case 15176
                sNorwegian = "15176: Mottakers kontonummer, %1, har feil lengde."
                If EmptyString(sString3) Then
                    sNorwegian = sNorwegian & vbCrLf & "Kontonummeret skal være på %2 posisjoner."
                Else
                    sNorwegian = sNorwegian & vbCrLf & "Kontonummeret skal være fra %2 til %3 posisjoner."
                End If
                sEnglish = "15176: The lengt of the accountnumber, %1, is wrong."
                If EmptyString(sString3) Then
                    sEnglish = sEnglish & vbCrLf & "The accountnumber must be %2 positions long."
                Else
                    sEnglish = sEnglish & vbCrLf & "The length of the accountnumber must be between %2 and %3."
                End If
                sSwedish = "15176: Mottagars kontonummer, %1, har fel längd."
                If EmptyString(sString3) Then
                    sSwedish = sSwedish & vbCrLf & "Kontonummeret ska vara på %2 positioner."
                Else
                    sSwedish = sSwedish & vbCrLf & "Kontonummeret ska vara från %2 til %3 positioner."
                End If
                sDanish = "15176: Modtagers kontonummer, %1, har fejl længde."
                If EmptyString(sString3) Then
                    sDanish = sDanish & vbCrLf & "Kontonummeret skal være på %2 positioner."
                Else
                    sDanish = sDanish & vbCrLf & "Kontonummeret skal være fra %2 til %3 positioner."
                End If
            Case 15177
                sNorwegian = "15177: Mottakers kontonummer må være et IBAN-nummer."
                sEnglish = "15177: Receivers accountnumber must be an IBAN-number."
                sSwedish = "15177: Mottagars kontonummer måste vara et IBAN-nummer."
                sDanish = "15177: Modtagers kontonummer skal være et IBAN-nummer."
            Case 15178
                sNorwegian = "15178: Mottakers kontonummer, %1, har feil lengde." & vbCrLf & "Kontonummeret skal være på maksimalt %2 posisjoner."
                sEnglish = "15178: The lengt of the accountnumber, %1, is wrong." & vbCrLf & "The accountnumber must be maximum %2 positions long."
                sSwedish = "15178: Mottagars kontonummer, %1, har fel längd." & vbCrLf & "Kontonummeret ska vara på maximalt %2 positioner."
                sDanish = "15178: Modtagers kontonummer, %1, har fejl længde." & vbCrLf & "Kontonummeret skal være på maximalt %2 positioner."
            Case 15179
                sNorwegian = "15179: Mottakers kontonummer er obligatorisk når betalingen ikke er angitt som en sjekkbetaling."
                sEnglish = "15179: Receivers account number is mandatory when the payment isn't marked as a cheque payment."
                sSwedish = "15179: Mottagars kontonummer är obligatorisk når betalningen inte är angivt som en checkbetalning."
                sDanish = "15179: Modtagers kontonummer er obligatorisk når betalingen ikke er angivt som en checkbetaling."
            Case 15180
                sNorwegian = "15180: Ingen mottaker-ID er angitt." & vbCrLf & "Dette kan legges inn under firmaopplysninger i feltet 'Foretaksnummer'" & vbCrLf & vbCrLf & "Er du i tvil, vennligst kontakt din forhandler."
                sEnglish = "15180: No receiver-ident is stated." & vbCrLf & "This may be entered in the company setup in the field 'CompanyNo'" & vbCrLf & vbCrLf & "If in doubt, please contact Your dealer."
                sSwedish = "15180: Ingen mottagar-ID är angivt." & vbCrLf & "Detta kan läggas inn under företagsopplysninger i fältet 'Företagsnummer'" & vbCrLf & vbCrLf & "Er du i tvil, vennligst kontakt din forhandler."
                sDanish = "15180: Ingen modtager-ID er angivt." & vbCrLf & "Dette kan legges inn under firmaopplysninger i feltet 'Foretaksnummer'" & vbCrLf & vbCrLf & "Er du i tvil, vennligst kontakt din forhandler."
            Case 15181
                sNorwegian = "15181: En gyldig BIC/SWIFT-kode må være angitt for mottakers kontonummer, %2." & vbCrLf & _
                "Det må angis i BabelBank sitt klientoppsett." & vbCrLf & "Angitt Swift: %1"
                sEnglish = "15181: A valid BIC/SWIFT-code must be stated for receiver's account number, %2." & vbCrLf & _
                "It must be stated in the clientsetup of BabelBank." & vbCrLf & "Swift stated: %1"
                sSwedish = "15181: En giltig BIC/SWIFT-kode måste vara angivt för mottagars kontonummer, %2." & vbCrLf & _
                              "Det måste angives i BabelBank sitt klientoppsett." & vbCrLf & "Angitt Swift: %1"
                sDanish = "15181: En gyldig BIC/SWIFT-kode skal være angivt for modtagers kontonummer, %2." & vbCrLf & _
                              "Det må angivs i BabelBank sitt klientoppsett." & vbCrLf & "Angivt Swift: %1"
            Case 15182
                sNorwegian = "15182: For konserninterne betalinger innen DNB, må både debetkontoens og kreditkontoens valutakode settes i BabelBanks klientoppsett. Debetkonto: %1, Kreditkonto: %2."
                sEnglish = "15182: For intracompany payments inside DNB, both the debitaccount and creditaccounts currency must be set in BabelBanks clientsetup. Debitaccount: %1, Creditkonto: %2."
                sSwedish = "15182: För koncerninterna betalningar inom DNB anges både debetkontots och kreditkontots valutakod som i BabelBanks klientinställningar. Debetkonto: %1, kreditkonto: %2."
                sDanish = "15182: For koncerninterne betalinger inden DNB, skal både debetkontoens og kreditkontoens valutakode indstilles i BabelBanks klientopsætning. Debetkonto: %1, Kreditkonto: %2."
            Case 15183
                sNorwegian = "15183: Maksimal tillatt lengde på feltet er %1 posisjoner."
                sEnglish = "15183: Maximum length allowed for this field is %1 characters."
                sSwedish = "15183: Maximal tillåten längd på fältet är %1 posisjoner."
                sDanish = "15183: Maximal tilladt længde på feltet er %1 posisjoner."
            Case 15184
                sNorwegian = "15184: Det må angis en kode til Norges Bank for betalinger til utlandet."
                sEnglish = "15184: A statebankcode must be stated on international payments."
                sSwedish = "15184: Det måste anges en kod til Norges Bank för betalningar till utlandet."
                sDanish = "15184: Det skal angives en kode til Norges Bank for betalinger i udlandet."
            Case 15185
                sNorwegian = "15185: Det må angis en tekst til Norges Bank for betalinger til utlandet."
                sEnglish = "15185: A statebanktext must be stated on international payments."
                sNorwegian = "15185:  Det måste anges en text til Norges Bank för betalningar till utlandet."
                sNorwegian = "15185: Det skal angives en tekst til Norges Bank for betalinger i udlandet."
            Case 15186
                sNorwegian = "15186: Ikke mulig å betale med sjekk i Tyskland eller Danmark"
                sEnglish = "15186: Checkpayments not allowed in Germany or Denmark"
                sSwedish = "15186: Det går inte att betala med check i Tyskland og Danmark"
                sDanish = "15186: Ikke muligt at betale med check i Tyskland og Danmark"
            Case 15187
                sNorwegian = "15187: Det må være angitt BBAN, ikke IBAN for konserninterne betalinger innen DNB."
                sEnglish = "15187: BBAN, not IBAN, must be used for companyinternal transfers inside DNB."
                sSwedish = "15187: BBAN måste anges, inte IBAN, för koncerninterna betalningar inom DNB."
                sDanish = "15187: Der skal være angivet BBAN, ikke IBAN for koncerninterne betalinger inden DNB."
            Case 15188
                sNorwegian = "15188: Det skal være angitt IBAN/Feil IBAN for finske mottakerkonti."
                sEnglish = "15188: IBAN must be used/error in IBAN for payments to Finish accounts."
                sSwedish = "15188: Det måste anges IBAN/Fel IBAN för finsk mottakerkonto."
                sDanish = "15188: Der skal være angivet IBAN/Feil IBAN for finske mottakerkonti"
            Case 15189
                sNorwegian = "15189: Det kan være max. %2 siffer i svenske %1. Kontonr er "
                sEnglish = "15189: There can be max. %2 digits in Swedish %1. Accountnumber is "
                sSwedish = "15189: Det måste anges max. %2 siffer i svenske %1. Kontonr er"
                sDanish = "15189: Det må være max. %2 siffer i svenske %1. Kontonr. er"
            Case 15190
                sNorwegian = "15190: Det er ikke angitt et gyldig IBAN-nummer på betaler." & vbCr & "Konto: %1"
                sEnglish = "15190: No valid IBAN account is stated on payer." & vbCr & "Account: %1"
                sSwedish = "15190: Det är inte angett ett giltigt IBAN-nummer på betalar." & vbCr & "Konto: %1"
                sDanish = "15190: Det er ikke angivet et gyldigt IBAN-nummer på betaler." & vbCr & "Konto: %1"
            Case 15191
                sNorwegian = "15191: Filen er ikke en gyldig EDI fil." & vbCrLf & vbCrLf & "Start av filen: " & vbCrLf & "%2"
                sEnglish = "15191: The file %1 is not a valid EDI-file." & vbCrLf & vbCrLf & "Start of file: " & vbCrLf & "%2"
                sSwedish = "15191: Filen är inte en giltigt EDI-fil." & vbCrLf & vbCrLf & "Start av filen: " & vbCrLf & "%2"
                sDanish = "15191: Filen er ikke en gyldig EDI fil." & vbCrLf & vbCrLf & "Start af filen: " & vbCrLf & "%2"
            Case 15192
                sNorwegian = "15192: Betalingsdato kan ikke være eldre enn %1 kalenderdager for denne banken."
                sEnglish = "15192: Invalid requested execution date. It should not be older than %1 calendar days."
                sSwedish = "15192: Betalningsdagen kan inte vara äldre än %1 kalenderdagar för denne banken."
                sDanish = "15192: Betalingsdato kan ikke være ældre end %1 kalenderdage for denne banken."
            Case 15193
                sNorwegian = "15193: Lengden på adressen til mottaker er for lang." & vbCrLf & "Lengde per linje: %1" & vbCrLf & "Antall repitisjoner: %2" & vbCrLf & "Adresse: %3"
                sEnglish = "15193: Total length of the receiver's address is too long" & vbCrLf & "Length per line: %1" & vbCrLf & "Number of repititions: %2" & vbCrLf & "Address: %3"
                sSwedish = "15193: Längden på adressen till mottagaren är för lång." & vbCrLf & "Längd per rad: %1" & vbCrLf & "Antal repetitioner: %2" & vbCrLf & "Adress: %3"
                sDanish = "15193: Længden på adressen til modtageren er for lang." & vbCrLf & "Længde pr linje: %1" & vbCrLf & "Antal gentagelser: %2" & vbCrLf & "Adresse: %3"
            Case 15194
                sNorwegian = "15194: For betalinger i KRW, må debetkonto være i DNB Norge (Debetkontos BIC må være DNBANOKK). Debetkonto BIC er her: %1."
                sEnglish = "15194: For payments in KRW, the debitaccount must be in DNB Norway (Debitaccount's BIC have to be DNBANOKK). Debitaccount BIC here is: %1."
                sSwedish = "15194: För betalningar i KRW, måste debetkonto vara i DNB (Debetkontos BIC måste vara DNBANOKK). Debetkonto BIC er her: %1."
                sDanish = "15194: For betalinger i KRW, skal debetkonto være i DNB Norge (Debetkontos BIC skal være DNBANOKK). Debetkonto BIC er her: %1"
            Case 15195
                sNorwegian = "15195: NOK betalinger til Filippinene er ikke tillatt."
                sEnglish = "15195: NOK payments to Philippines are not allowed."
                sSwedish = "15195: NOK betalningar till Filippinerna är inte tillåtet."
                sDanish = "15195: NOK betalinger til Filippinerne er ikke tilladt."

                ' 12.10.2017 added next 2
            Case 15704
                sNorwegian = "15704: Det er ikke angitt noen observasjonskonto." & vbLf & "" & vbLf & "Legg inn en observasjonskonto i klientoppsettet."
                sEnglish = "15704: No observationaccount is stated." & vbLf & "" & vbLf & "Enter an observationaccount in Client-setup."
                sSwedish = "15704: Det finns inget angivet observationskonto." & vbLf & "" & vbLf & "Ange ett observationskonto i klientinställningarna.	"
                sDanish = "15704: Der er ikke angivet nogen observationskonto." & vbLf & "" & vbLf & "Tilføj en observationskonto i klientopsætningen.	"
            Case 15705
                sNorwegian = "15705: Det er angitt mer enn en observasjonskonto." & vbLf & "" & vbLf & "Slett ugyldige observasjonskonti i klientoppsettet."
                sEnglish = "15705: More than one observationaccount is stated." & vbLf & "" & vbLf & "Delete the invalid observationaccount(s) in Client-setup."
                sSwedish = "15705: Det finns mer än ett angivet observationskonto." & vbLf & "" & vbLf & "Ta bort ogiltiga observationskonton i klientinställningarna.	"
                sDanish = "15705: De er angivet mere end en observationskonto." & vbLf & "" & vbLf & "Slet ugyldige observationskonti i klientopsætningen.	"
            Case 15706
                sNorwegian = "15706: BabelBank finner ikke avvikskontoen: %1." & vbLf & "" & vbLf & "Legg inn kontoen i klientoppsettet."
                sEnglish = "15706: BabelBank isn't able to find the account: %1." & vbLf & "" & vbLf & "Enter the account in the Client-setup."
                sSwedish = "15706: BabelBank finner inte avvikskontoet: %1." & vbLf & "" & vbLf & "Ange kontoet i klientinställningarna.	"
                sDanish = "15706: BabelBank finner ikke avvikskontoen: %1." & vbLf & "" & vbLf & "Tilføj kontoen i klientopsætningen.	"

            Case 20000
                sNorwegian = "Kontonummer %1 er ukjent for Babelbank. Kjør oppsett på nytt, og angi kontonummer. " & vbCr & "" & vbLf & " Programmet avbrytes."
                sEnglish = "Account %1 unknown to BabelBank.  Program terminates!"
                sSwedish = "Kontonummer %1 är okänt för Babelbank. Kör inställningen igen och ange kontonummer." & vbLf & " Programmet avbryts.	"
                sDanish = "Kontonummer %1 er ukendt for Babelbank. Kør opsætning på ny, og angiv kontonummer." & vbLf & " Programmet afbrydes.	"
            Case 20001
                sNorwegian = "Kontonummer %1 er ukjent for Babelbank. Ønsker du å eksportere den som en vanlig klient. " & vbCr & "" & vbLf & "" & vbCr & "" & vbLf & "Angi i så fall korrekt klientnummer."
                sEnglish = "Account %1 unknown to BabelBank. Export as an ordinar client ? Please specify correct clientnumber!"
                sSwedish = "Kontonummer %1 är okänt för Babelbank. Vill du exportera det som en vanlig klient" & vbLf & "" & vbLf & "så ange rätt klientnummer.	"
                sDanish = "Kontonummer %1 er ukendt for Babelbank. Vil du eksportere det som en almindelig klient." & vbLf & "" & vbLf & "Angiv i så fald korrekt klientnummer.	"
            Case 20002
                sNorwegian = "Du har oppgitt feil antall posisjoner i klientnummeret! " & vbCr & "" & vbLf & "Prøv på nytt."
                sEnglish = "Wrong number of characters/numbers in clientnumber"
                sSwedish = "Du har angett fel antal teckenpositioner i klientnumret!" & vbLf & "Försök igen.	"
                sDanish = "Du har angivet forkert antal tegn i klientnummeret!" & vbLf & "Prøv igen.	"
            Case 20003
                sNorwegian = "Feil i klientnummeret"
                sEnglish = "Something wrong with clientnumber"
                sSwedish = "Fel i klientnumret"
                sDanish = "Fejl i klientnummeret"
            Case 20004
                sNorwegian = "Klientnummer:"
                sEnglish = "Clientnumber:"
                sSwedish = "Klientnummer:"
                sDanish = "Klientnummer:"
            Case 20005
                sNorwegian = "Kontonummer:"
                sEnglish = "Accountnumber:"
                sSwedish = "Kontonummer:"
                sDanish = "Kontonummer:"
            Case 20006
                sNorwegian = "Eksportformat:"
                sEnglish = "Exportformat:"
                sSwedish = "Exportformat:"
                sDanish = "Eksportformat:"
            Case 20007
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 20008
                sNorwegian = "Ukjent konto"
                sEnglish = "Unknown account"
                sSwedish = "Okänt konto"
                sDanish = "Ukendt konto"
            Case 30000
                sNorwegian = "----- (Vb_Utils messages) ----"
                sEnglish = "----- (Vb_Utils messages) ----"
                sSwedish = "----- (Vb_Utils messages) ----"
                sDanish = "----- (Vb_Utils messages) ----"
            Case 30001
                sNorwegian = "30001: Kan ikke opprette katalogen %1." & vbLf & "Feil spesifikajon av katalogen."
                sEnglish = "30001: Can't create the folder %1." & vbLf & "" & vbLf & "Wrong specification of folder."
                sSwedish = "30001: Kan inte skapa katalogen %1." & vbLf & "Fel specifikation av katalogen.	"
                sDanish = "30001: Kan ikke oprette mappen %1." & vbLf & "Forkert specifikation for mappe.	"
            Case 30002
                sNorwegian = "30002: Katalogen %1 er skrivebeskyttet."
                sEnglish = "30002: The folder %1 is read-only."
                sSwedish = "30002: Katalogen %1 är skrivskyddad."
                sDanish = "30002: Mappen %1 er skrivebeskyttet."
            Case 30003
                sNorwegian = "Legg inn en annen katalog, eller endre rettighetene."
                sEnglish = "Change the folder, or change the attributes of the folder."
                sSwedish = "Amge en annan katalog eller annan behörighet."
                sDanish = "Opret en anden mappe, eller ændr rettighederne."
            Case 30004
                sNorwegian = "30004: Katalogen %1 finnes ikke."
                sEnglish = "30004: Folder %1 does not exist."
                sSwedish = "30004: Katalogen %1 finns inte."
                sDanish = "30004: Mappen %1 findes ikke."
            Case 30005
                sNorwegian = "Ønsker du å opprette katalogen?"
                sEnglish = "Do You want to create the folder?"
                sSwedish = "Vill du skapa katalogen?"
                sDanish = "Ønsker du at oprette mappen?"
            Case 30006
                sNorwegian = "Opprett katalogen, eller endre stien under oppsett."
                sEnglish = "Create the folder or change the path in setup."
                sSwedish = "Skapa katalogen eller ändra sökväg i inställningen."
                sDanish = "Opret mappen, eller ændr stien under opsætningen."
            Case 30007
                sNorwegian = "Stasjon %1 finnes ikke."
                sEnglish = "The drive %1 does not exist."
                sSwedish = "Station %1 finns inte."
                sDanish = "Drevet %1 findes ikke."
            Case 30008
                sNorwegian = "30008: Feil i innlesningsmodul." & vbLf & "Det må angis mulige desimalskilletegn på beløp."
                sEnglish = "30008: Error in the importmodul." & vbLf & "There must be entered a possible decimalseperator for the amounts."
                sSwedish = "30008: Fel i inläsningsmodul." & vbLf & "Antal tillåtna decimaler måste anges för belopp.	"
                sDanish = "30008: Fejl i indlæsningsmodul." & vbLf & "Der skal angives eventuelle decimalskilletegn i beløbet.	"
            Case 30009
                sNorwegian = "30009: Feil i beløp." & vbLf & "Beløpet %1 er ikke gyldig."
                sEnglish = "30009: Error in amount." & vbLf & "The amount %1 is not valid."
                sSwedish = "30009: Fel i belopp." & vbLf & "Beloppet %1 är ogiltigt.	"
                sDanish = "30009: Fejl i beløb." & vbLf & "Beløbet %1 er ikke gyldigt.	"
            Case 30010
                sNorwegian = "30010: Feil i dato som legges inn i %1 i klassen %2." & vbLf & "Dato %3 er ikke gyldig." & vbLf & "vbBabel benytter formatet YYYYMMDD."
                sEnglish = "30010: Error in date inserted into %1 in the class %2." & vbLf & "The date %3 is not valid." & vbLf & "vbBabel uses the format YYYYMMDD."
                sSwedish = "30010: Fel i datum som angetts i %1 i klassen %2." & vbLf & "Datum %3 är ogiltigt." & vbLf & "vbBabel använder formatet ÅÅÅÅMMDD.	"
                sDanish = "30010: Fejl i dato lægges ind i %1 i klassen %2." & vbLf & "Dato %3 er ikke gyldig." & vbLf & "vbBabel benytter formatet YYYYMMDD.	"
            Case 30011
                'TODO Bør ikke vb_util msg i CommonLRS ?
                sNorwegian = "Feil i bankkode %1."
                sEnglish = "Error in bankcode %1."
                sSwedish = "Fel i bankkod."
                sDanish = "Fejl i bankkode."
            Case 35000
                sNorwegian = "--- FEIL I EKSPORTFORMAT ---"
                sEnglish = "--- ERROR IN EXPORTFORMAT ---"
                sSwedish = "--- FEIL I EKSPORTFORMAT ---"
                sDanish = "--- FEIL I EKSPORTFORMAT ---"
            Case 35001
                sNorwegian = "Importert fil: %1"
                sEnglish = "File imported: %1"
                sSwedish = "Importerad fil: %1"
                sDanish = "Importeret fil: %1"
            Case 35002
                sNorwegian = "Beløp: %1"
                sEnglish = "Amount: %1"
                sSwedish = "Belopp: %1"
                sDanish = "Beløb: %1"
            Case 35003
                sNorwegian = "Mottaker: %1"
                sEnglish = "Creditor name: %1"
                sSwedish = "Mottagare: %1"
                sDanish = "Modtager: %1"
            Case 35004
                sNorwegian = "Eksportformat: %1"
                sEnglish = "Exportformat: %1"
                sSwedish = "Exportformat: %1"
                sDanish = "Eksportformat: %1"
            Case 35005
                sNorwegian = "Filnavn: %1"
                sEnglish = "Filename: %1"
                sSwedish = "Filnamn: %1"
                sDanish = "Filnavn: %1"
            Case 35006
                sNorwegian = "35006: Formatet %1 har ikke plass til all fritekst."
                sEnglish = "35006: The freetext exceeds maximum freetext in the format %1."
                sSwedish = "35006: Formatet %1 har inte plats för all fritext."
                sDanish = "35006: Formatet %1 har ikke plads til al fritekst."
            Case 35007
                sNorwegian = "35007: Det er angitt et kontraktsnummer for fast kurs, men ingen fast kurs."
                sEnglish = "35007: A contractnumber for an agreed rate is stated, but no agreed rate."
                sSwedish = "35007: Ett kontraktsnummer för fast kurs har angetts, men ingen fast kurs."
                sDanish = "35007: Der er angivet et kontraktnummer for fast kurs, men ingen fast kurs."
            Case 35008
                sNorwegian = "35008: Det er angitt en fast kurs, men ikke angitt noe kontraktsnummer."
                sEnglish = "35008: An agreed rate is stated, but no corresponding contractnumber is stated."
                sSwedish = "35008: En fast kurs har angetts men inget kontraktsnummer."
                sDanish = "35008: Der er angivet en fast kurs, men ikke angivet noget kontraktnummer."
            Case 35009
                sNorwegian = "35009: Valutakoden %1 er ikke gyldig i formatet %2."
                sEnglish = "35009: The currencycode %1 is not valid in the format %2."
                sSwedish = "35009: Valutakoden %1 är inte giltig i formatet %2."
                sDanish = "35009: Valutakoden %1 er ikke gyldig i formatet %2."
            Case 35010
                sNorwegian = "35010: Mottakers navn er obligatorisk i formatet %1."
                sEnglish = "35010: Receivers name is mandatory in the format %1"
                sSwedish = "35010: Mottagarens namn måste anges i formatet %1."
                sDanish = "35010: Modtagers navn er obligatorisk i formatet %1."
            Case 35011
                sNorwegian = "35011: Postnummer må være nummerisk i formatet %1."
                sEnglish = "35011: The zipcode must be numeric in the format %1."
                sSwedish = "35011: Postnummer måste anges med siffror i formatet %1."
                sDanish = "35011: Postnummer skal være numerisk i formatet %1."
            Case 35012
                sNorwegian = "35012: Postnummer er obligatorisk i formatet %1."
                sEnglish = "35012: The zipcode is mandatory in the format %1."
                sSwedish = "35012: Postnummer måste anges med formatet %1."
                sDanish = "35012: Postnummer er obligatorisk i formatet %1."
            Case 35013
                sNorwegian = "35013: Postnummeret er for langt, maksimal lengde er %1."
                sEnglish = "35013: The zipcode is too long, maximum number of digits is %1."
                sSwedish = "35013: Postnumret är för långt, maxlängd är %1."
                sDanish = "35013: Postnummeret er for langt, maksimal længde er %1."
            Case 35014
                sNorwegian = "35014: Feil under generering av kortarkkode for betalerinformasjon."
                sEnglish = "35014: Error during creation of betaleridentifikasjon, kortartkode"
                sSwedish = "35014: Fel under generering av referensnummer för betalningsinformation."
                sDanish = "35014: Fejl under generering af kortartkode for betalerinformation."
            Case 35015
                sNorwegian = "35015: Forfallsdatoen %1 er feil. Kan ikke være mer enn %2 dager frem i tid."
                sEnglish = "35015: The duedate %1 is incorrect. Maximum is %2 days in the future."
                sSwedish = "35015: Förfallodatum %1 är fel, kan inte vara mer än %2 dagar framåt i tiden."
                sDanish = "35015: Forfaldsdatoen %1 er forkert. Kan ikke være mere end %2 dage frem i tiden."
            Case 35016
                sNorwegian = "35016: Feil i koden til Nationalbanken. Koden %1 er ikke gyldig."
                sEnglish = "35016: Wrong statebankcode, code %1 is not valid."
                sSwedish = "35016: Fel i kod till nationalbanken. Koden %1 är ogiltig."
                sDanish = "35016: Fejl i koden til Nationalbanken. Koden %1 er ikke gyldig."
            Case 35017
                sNorwegian = "35017: Feil i import/eksport dato. %1 er ikke en gyldig verdi."
                sEnglish = "35017: Wrong import/export date stated, %1 is not valid."
                sSwedish = "35017: Fel i import-/exportdatum. %1 är inte ett giltigt värde."
                sDanish = "35017: Fejl i import-/eksportdato. %1 er ikke en gyldig værdi."
            Case 35018
                sNorwegian = "35018: Feil landkode. %1 er ikke en gyldig verdi."
                sEnglish = "35018: Wrong countrycode stated, %1 is not valid."
                sSwedish = "35018: Fel landsnummer. %1 är inte ett giltigt värde."
                sDanish = "35018: Forkert landekode. %1 er ikke en gyldig værdi."
            Case 35019
                sNorwegian = "35019: Summen av alle fakturabeløp stemmer ikke med beløp på betalingen." & vbCrLf & "Variable: %3" & vbCrLf & "Sum av fakturaer: %1" & vbCrLf & "Betalingsbeløp: %2" & vbCrLf & "Filnavn: %4"
                sEnglish = "35019: The total of all invoices differs from the payment's amount." & vbCrLf & "Variable: %3" & vbCrLf & "Total sum invoices: %1" & vbCrLf & "Amount on payment: %2" & vbCrLf & "Filename: %4"
                sSwedish = "35019: Summan av alla fakturabelopp stämmer inte med beloppet i betalningen." & vbLf & "Variabel: %3" & vbLf & "Summa av fakturor: %1" & vbLf & "Betalningsbelopp: %2" & vbLf & "Filnamn: %4	"
                sDanish = "35019: Summen af alle fakturabeløb stemmer ikke overens med beløb på betalingen." & vbLf & "Variabel: %3" & vbLf & "Sum af fakturaer: %1" & vbLf & "Betalingsbeløb: %2" & vbLf & "Filnavn: %4	"
            Case 35020
                sNorwegian = "35020: Summen av alle betalinger, stemmer ikke med totalbeløp på batch." & vbCrLf & "Variable: %3" & vbCrLf & "Sum av betalinger: %1" & vbCrLf & "Totalbeløp batch: %2" & vbCrLf & "Filnavn: %4"
                sEnglish = "35020: The total of all payments differs from the amount on the batch." & vbCrLf & "Variable: %3" & vbCrLf & "Total sum payments: %1" & vbCrLf & "Amount on batch: %2" & vbCrLf & "Filename: %4"
                sSwedish = "35020: Summan av alla betalningar stämmer inte med totalbeloppet i batchen." & vbLf & "Variabel: %3" & vbLf & "Summa av betalningar: %1" & vbLf & "Totalbelopp i batch: %2" & vbLf & "Filnamn: %4	"
                sDanish = "35020: Summen af alle betalinger, stemmer ikke overens med totalbeløb på batch." & vbLf & "Variabel: %3" & vbLf & "Sum af betalinger: %1" & vbLf & "Totalbeløb batch: %2" & vbLf & "Filnavn: %4	"
            Case 35021
                sNorwegian = "35021: Betalingsbeløpet er negativt." & vbCrLf & "Variable: %2" & vbCrLf & "Beløp: %1"
                sEnglish = "35021: The payment amount is negative." & vbCrLf & "Variable: %2" & vbCrLf & "Amount: %1"
                sSwedish = "35021: Betalningsbeloppet är negativt." & vbLf & "Variabel: %2" & vbLf & "Belopp: %1	"
                sDanish = "35021: Betalingsbeløbet er negativt." & vbLf & "Variabel: %2" & vbLf & "Beløb: %1	"
            Case 35022
                sNorwegian = "35022: Summen av alle batcher, stemmer ikke med totalbeløp på fil." & vbCrLf & "Variable: %3" & vbCrLf & "Sum av batcher: %1" & vbCrLf & "Totalbeløp fil: %2"
                sEnglish = "35022: The total of all batches differs from the amount on the file." & vbCrLf & "Variable: %3" & vbCrLf & "Total sum batches: %1" & vbCrLf & "Amount on file: %2"
                sSwedish = "35022: Summan av alla batchar stämmer inte med totalbeloppet i filen." & vbLf & "Variabele: %3" & vbLf & "Summa av batchar: %1" & vbLf & "Totalbelopp i fil: %2	"
                sDanish = "35022: Summen af alle batches, stemmer ikke overens med totalbeløb på fil." & vbLf & "Variabel: %3" & vbLf & "Sum af batches: %1" & vbLf & "Totalbeløb fil: %2	"
            Case 35023
                sNorwegian = "35023: Ingen %2 oppgitt for konto: %1." & vbCrLf & "Kan ikke lage korrekt OCR-fil." & vbLf & "Oppgi dette under valget klienter i BabelBank."
                sEnglish = "35023: No Contract-id (%2) stated for account: %1." & vbCrLf & "Unable to create a correct OCR-file." & vbCrLf & "You must enter this in the client-setup part of BabelBank."
                sSwedish = "35023: %2 har inte angetts för konto: %1." & vbLf & "Kan inte skapa korrekt OCR-fil." & vbLf & "Ange detta under valda klienter i BabelBank.	"
                sDanish = "35023: Ingen %2 angivet for konto: %1." & vbLf & "Kan ikke lave korrekt OCR-fil." & vbLf & "Angiv dette under valget klienter i BabelBank.	"
            Case 35024
                sNorwegian = "35024: Feil i mottakers kontonummer!" & vbCrLf & "Kontonummer: %1"
                sEnglish = "35024: Error in beneficiary's accountnumber!" & vbCrLf & "Accountno: %1"
                sSwedish = "35024: Fel i mottagarens kontonummer!" & vbLf & "Kontonummer: %1	"
                sDanish = "35024: Fejl i modtagers kontonummer!" & vbLf & "Kontonummer: %1	"
            Case 35025
                sNorwegian = "35025: Feil betalingstype oppgitt!" & vbCrLf & "Betalingstype: %1"
                sEnglish = "35025: Wrong payment specification stated!" & vbCrLf & "Specification: %1"
                sSwedish = "35025: Fel betalningstyp har angetts!" & vbLf & "Betalningstyp: %1	"
                sDanish = "35025: Forkert betalingstype angivet!" & vbLf & "Betalingstype: %1	"
            Case 35026
                sNorwegian = "35026: BabelBank støtter ikke import av formatet %1" & vbCrLf & "Filnavn: %2"
                sEnglish = "35026: BabelBank doesn't support import of the format %1" & vbCrLf & "Filename: %2"
                sSwedish = "35026: BabelBank stöder inte import av formatet %1"
                sDanish = "35026: BabelBank støtter ikke import af formatet %1"
            Case 35027
                sNorwegian = "35027: BabelBank støtter ikke eksport av formatet %1"
                sEnglish = "35027: BabelBank doesn't support export of the format %1"
                sSwedish = "35027: BabelBank stöder inte export av formatet %1"
                sDanish = "35027: BabelBank støtter ikke eksport af formatet %1"
            Case 35028
                sNorwegian = "35028: Kontonummeret %1 er ikke registrert i BabelBank." & vbCrLf & "Oppgi dette under valget klienter i BabelBank."
                sEnglish = "35028: The account %1 is not registered in BabelBank." & vbCrLf & "You must enter this in the client-setup part of BabelBank."
                sSwedish = "35028: Kontonumret %1 är inte registrerat i BabelBank." & vbLf & "Ange detta under valda klienter i BabelBank.	"
                sDanish = "35028: Kontonummeret %1 er ikke registreret i BabelBank." & vbLf & "Angiv dette under valget klienter i BabelBank.	"
            Case 35029
                sNorwegian = "35029: Ingen %1 er oppgitt. %1 er obligatorisk."
                sEnglish = "35029: No %1 is stated. %1 is mandatory."
                sSwedish = "35029: %1 har inte angetts. %1 måste anges."
                sDanish = "35029: Ingen %1 er angivet. %1 er obligatorisk."
            Case 35030
                sNorwegian = "Debet konto:"
                sEnglish = "Debit account:"
                sSwedish = "Debetkonto:"
                sDanish = "Debet konto:"
            Case 35031
                sNorwegian = "Kredit konto:"
                sEnglish = "Credit account:"
                sSwedish = "Kreditkonto:"
                sDanish = "Kredit konto:"
            Case 35032
                sNorwegian = "Egenreferanse:"
                sEnglish = "Own reference:"
                sSwedish = "Egen referens:"
                sDanish = "Egenreference:"
            Case 35033
                sNorwegian = "35033: Kredit kontonummer må være nummerisk."
                sEnglish = "35033: Credit account must be numeric."
                sSwedish = "35033: Kreditkontonummer måste anges med siffror."
                sDanish = "35033: Kredit kontonummer skal være numerisk."
            Case 35034
                sNorwegian = "35034: Debet kontonummer må være nummerisk."
                sEnglish = "35034: Debit account must be numeric."
                sSwedish = "35034: Debetkontonummer måste anges med siffror."
                sDanish = "35034: Debet kontonummer skal være numerisk."
            Case 35035
                sNorwegian = "35035: Debet kontonummer er for langt. Det skal være på 10 posisjoner."
                sEnglish = "35035: Debit accountnumber is too long. It must be exact 10 positions."
                sSwedish = "35035: Debetkontonumret är för långt. Det ska ha 10 teckenpositioner."
                sDanish = "35035: Debet kontonummer er for langt. Det skal være på 10 tegn."
            Case 35036
                sNorwegian = "35036: Debett kontonummer er for kort. Det skal være på 10 posisjoner."
                sEnglish = "35036: Debit accountnumber is too short. It must be exact 10 positions."
                sSwedish = "35036: Debetkontonumret är för kort. Det ska ha 10 teckenpositioner."
                sDanish = "35036: Debet kontonummer er for kort. Det skal være på 10 tegn."
            Case 35037
                sNorwegian = "35037: Feil i feltet 'bank code numeric'. Verdi: %1." & vbLf & "Feltet skal være nummerisk, og på eksakt 4 posisjoner."
                sEnglish = "35037: Error in the field 'bank code numeric'. Value: %1." & vbLf & "The field must be numeric, and exactly 4 positions long."
                sSwedish = "35037: Fel i fältet ''bank code numeric''. Värde: %1." & vbLf & "Fältet ska anges med siffror, exakt 4 teckenpositioner.	"
                sDanish = "35037: Fejl i feltet 'bank code numeric'. Værdi: %1." & vbLf & "Feltet skal være numerisk, og på nøjagtigt 4 tegn.	"
            Case 35039
                sNorwegian = "35039: Feil i feltet 'K-Symbol'. Verdi: %1." & vbLf & "Feltet skal være nummerisk, og på maksimalt 10 posisjoner."
                sEnglish = "35039: Error in the field 'K-Symbol'. Value: %1." & vbLf & "The field must be numeric, and maximum 10 positions long."
                sSwedish = "35039: Fel i fältet ''K-symbol''. Värde: %1." & vbLf & "Fältet ska anges med siffror, max 10 teckenpositioner.	"
                sDanish = "35039: Fejl i feltet 'K-Symbol'. Værdi: %1." & vbLf & "Feltet skal være numerisk, og på maksimalt 10 tegn.	"
            Case 35040
                sNorwegian = "35040: Beløpet som skal betales må være positivt."
                sEnglish = "35040: The amount to be paid, must be larger than zero."
                sSwedish = "35040: Det belopp som ska betalas måste vara positivt."
                sDanish = "35040: Beløbet der skal betales skal være positivt."
            Case 35041
                sNorwegian = "35041: Feil i feltet 'Beneficiary Account'. Verdi: %1." & vbLf & "Feltet skal være nummerisk, og på maksimalt %2 posisjoner."
                sEnglish = "35041: Error in the field 'Beneficiary account'. Value: %1." & vbLf & "The field must be numeric, and maximum %2 positions long."
                sSwedish = "35041: Fel i fältet 'Beneficiary Account'. Värde: %1." & vbLf & "Fältet ska vara nummeriskt, och ha högst %2 positioner.	"
                sDanish = "35041: Fejl i feltet 'Beneficiary Account'. Værdi: %1." & vbLf & "Feltet skal være numerisk, og på maksimalt %2 tegn.	"
            Case 35042
                sNorwegian = "Ikke lov med blanding av fritekst og strukturert oppdrag (KID/Fakturanr)"
                sEnglish = "Mix of structured (KID/InvoiceNo) and unstructured info not allowed"
                sSwedish = "Ej tillåtet att blanda text och strukturerade uppdrag (OCR/Fakturanr)"
                sDanish = "Ikke tilladt med blanding af fritekst og struktureret opgave (KID/Fakturanr)"
            Case 35043
                sNorwegian = "Du har overskredet maksgrensen på 999 BETFOR23/BETFOR01 i ett oppdrag"
                sEnglish = "The maximum number of 999 BETFOR23 is exceeded"
                sSwedish = "Du har överskridit maxgränsen på 999 BETFOR23/BETFOR01 i ett uppdrag"
                sDanish = "Du har overskredet maks. grænsen på 999 BETFOR23/BETFOR01 i en opgave"
            Case 35044
                sNorwegian = "Du har overskredet maksgrensen på 9999 BETFOR22 i ett oppdrag"
                sEnglish = "The maximum number of 9999 BETFOR22 is exceeded"
                sSwedish = "Du har överskridit maxgränsen på 9999 BETFOR22 i ett uppdrag"
                sDanish = "Du har overskredet maks. grænsen på 9999 BETFOR22 i en opgave"
            Case 35045
                sNorwegian = "Du har overskredet maksgrensen på 25 fritekstlinjer i BETFOR23"
                sEnglish = "The maximum number of freetextlines (25) in BETFOR23 is exceeded"
                sSwedish = "Du har överskridit maxgränsen på 25 textrader i BETFOR23"
                sDanish = "Du har overskredet maks. grænsen på 25 fritekstlinjer i BETFOR23"
            Case 35046
                sNorwegian = "Feil/manglende postnummer %1"
                sEnglish = "Missing/wrong zipcode %1"
                sSwedish = "Fel/saknat postnummer %1"
                sDanish = "Forkert/manglende postnummer %1"
            Case 35047
                sNorwegian = "Feil i mottakers kontonummer %1"
                sEnglish = "Error in receivers accountnumber %1"
                sSwedish = "Fel i mottagarens kontonummer %1"
                sDanish = "Fejl i modtagerens kontonummer %1"
            Case 35048
                sNorwegian = "35048: Ingen Lockboxnummer oppgitt for konto: %1." & vbLf & "Kan ikke lage korrekt BAI Lockbox-fil." & vbLf & "Oppgi dette under valget klienter i BabelBank."
                sEnglish = "35048: No Lockboxnumber set for account: %1." & vbLf & "Unable to create a correct BAI Lockbox-file." & vbLf & "You must enter this in the client-setup part of BabelBank."
                sSwedish = "35048: Inget betalningsfilnummer angivet för konto: %1." & vbLf & "Kan inte skapa korrekt BAI betalningsfil." & vbLf & "Ange detta under valda klienter i BabelBank.	"
                sDanish = "35048: Ikke angivet noget Lockboxnummer for kontoen: %1." & vbLf & "Kan ikke oprette korrekt BAI Lockbox-fil." & vbLf & "Angiv dette under valget klienter i BabelBank.	"
            Case 35049
                sNorwegian = "35049: Det er ikke angitt valutakode på kontoen %1" & vbLf & "Dette må legges inn under klientoppsettet."
                sEnglish = "35049: No currencycode stated on the account %1." & vbLf & "You must enter this in the client setup."
                sSwedish = "35049: Det finns ingen angiven valutakod på kontot %1." & vbLf & "Detta måste anges under klientuppställningen.	"
                sDanish = "35049: Der er ikke angivet en valutakode på kontoen %1." & vbLf & "Denne skal tilføjes under klientopsætningen.	"
            Case 35050
                sNorwegian = "35050: Det er ikke angitt landkode på kontoen %1" & vbLf & "Dette må legges inn under klientoppsettet."
                sEnglish = "35050: No countrycode stated on the account %1." & vbLf & "You must enter this in the client setup."
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 35070

                sSwedish = "35050: Det finns inget angivet landsnummer på kontot %1." & vbLf & "Detta måste anges under klientuppställningen.	"
                sDanish = "35050: Der er ikke angivet en landekode på kontoen %1." & vbLf & "Denne skal tilføjes under klientopsætningen.	"
            Case 35070
                sNorwegian = "Kurs: %1"
                sEnglish = "Rate: %1"
                sSwedish = "Kurs: %1"
                sDanish = "Kurs: %1"
            Case 35071
                sNorwegian = "Betaler: %1"
                sEnglish = "Payer: %1"
                sSwedish = "Betalar: %1"
                sDanish = "Betaler: %1"
            Case 35072
                sNorwegian = "35072: Det er ikke angitt noe firmanavn for konto: %1." & vbLf & "Kan ikke lage korrekt ISO20022-fil." & vbLf & "Oppgi dette under firmaopplysninger eller i BabelBank sitt klientoppsett."
                sEnglish = "35072: No companyname stated for account: %1." & vbLf & "Unable to create a correct ISO20022-file." & vbLf & "You must enter this either in the companysetup or in the clientsetup of BabelBank."
                sSwedish = "35072: Det är inte angett något firmanamn för konto: %1." & vbLf & "Kan inte skapa korrekt ISO20022-fil." & vbLf & "Ange dette enligt företagsinformation eller i BabelBank sin klientinställning."
                sDanish = "35072: Det er ikke angivet noe firmanavn for konto: %1." & vbLf & "Kan ikke lave korrekt ISO20022-fil." & vbLf & "Angiv dette under virksomhedensoplysninger eller i BabelBank sitt klientoppsett."
            Case 35073
                sNorwegian = "35073: Det er ikke angitt noen ID for konto: %1." & vbLf & "Kan ikke lage korrekt ISO20022-fil." & vbLf & "Oppgi dette under firmaopplysninger eller i BabelBank sitt klientoppsett."
                sEnglish = "35073: No ID is stated for account: %1." & vbLf & "Unable to create a correct ISO20022-file." & vbLf & "You must enter this either in the companysetup or in the clientsetup of BabelBank."
                sSwedish = "35073: Inget ID angett för konto: %1." & vbLf & "Kan inte skapa korrekt ISO20022-fil." & vbLf & "Du måste ange det antingen i företagsuppsättningen eller i klientuppsättningen av BabelBank."
                sDanish = "35073: Der er ikke angivet noget ID for konto: %1." & vbLf & "Kan ikke lave korrekt ISO20022-fil." & vbLf & "Du skal indtaste dette enten i companysetup eller i clientetup af BabelBank."
            Case 35074
                sNorwegian = "35074: Validering av filen %2 mot skjemafil rapporterte følgende feil: " & vbLf & "%1" & vbCrLf & "Kan ikke lage korrekt ISO20022-fil."
                sEnglish = "35074: Validation of the file %2 against the schemafile caused errors: " & vbLf & "%1" & vbCrLf & "Unable to create a correct ISO20022-file."
                sSwedish = "35074: Validering av filen %2 mot schemafilen orsakade fel: " & vbLf & "%1" & vbCrLf & "Kan inte skapa korrekt ISO20022-fil."
                sDanish = "35074: Validering af filen %2 mod schemafilen forårsagede fejl: " & vbLf & "%1" & vbCrLf & "Kan ikke lave korrekt ISO20022-fil."

            Case 36001
                sNorwegian = "36001: Navn for mottager mangler"
                sEnglish = "36001: Missing receivers name"
                sSwedish = "36001: Saknar navn till mottagare"
                sDanish = "36001: Mangler navn for modtager"
            Case 36002
                sNorwegian = "36002: Mangler adresse til mottaker"
                sEnglish = "36002: Missing receivers address"
                sSwedish = "36002: Saknar adress till mottagare"
                sDanish = "36002: Mangler adresse på modtager"
            Case 36003
                sNorwegian = "36003: Mangler postnummer til mottaker"
                sEnglish = "36003: Missing receivers zipcode"
                sSwedish = "36003: Saknar postnummer till mottagare"
                sDanish = "36003: Mangler postnummer på modtager"
            Case 36004
                sNorwegian = "36004: Mangler poststed til mottaker"
                sEnglish = "36004: Missing receivers place/city"
                sSwedish = "36004: Saknar mottagarens ort"
                sDanish = "36004: Mangler by på modtager"
            Case 36005
                sNorwegian = "36005: Feil i IBAN mottakerkonto"
                sEnglish = "36005: Error in receiving banks IBAN"
                sSwedish = "36005: Fel i IBAN-mottagarkonto"
                sDanish = "36005: Fejl i IBAN modtagerkonto"
            Case 36006
                sNorwegian = "36006: Feilet i CDV-kontroll norsk mottakerkonto"
                sEnglish = "36006: CDV-error in receivering banks accoountnumber"
                sSwedish = "36006: Fel i CDV-kontroll av norskt mottagarkonto"
                sDanish = "36006: Fejl i CDV-kontrol dansk modtagerkonto"
            Case 36007
                sNorwegian = "36007: Feil i mottakerbanks SWIFT (BIC)"
                sEnglish = "36007: Error in receiving banks SWIFT (BIC)"
                sSwedish = "36007: Fel i den mottagande bankens SWIFT (BIC)"
                sDanish = "36007: Fejl i modtagerbanks SWIFT (BIC)"
            Case 36008
                sNorwegian = "36008: Det kreves Swift for mottakers bank for SEPA-betalinger"
                sEnglish = "36008: SWIFT is reqiured for receiver's bank for SEPA payments"
                sSwedish = "36008: Det krävs Swift för mottagarens bank vid SEPA-betalningar"
                sDanish = "36008: Der kræves Swift for modtagers bank for SEPA-betalinger"
            Case 36009
                sNorwegian = "36009: Det kreves IBAN til mottaker for SEPA-betalinger"
                sEnglish = "36009: IBAN account for receiver's account is required for SEPA payments"
                sSwedish = "36009: Det krävs IBAN till mottagaren av SEPA-betalningar"
                sDanish = "36009: Der kræves IBAN til modtager for SEPA-betalinger"
            Case 36010
                sNorwegian = "36010: Det er ikke angitt korrekt landkode for mottakers bank"
                sEnglish = "36010:  Correct countrycode for receivers banks is not set"
                sSwedish = "36010:Inte rätt angivet landsnummer för mottagarens bank"
                sDanish = "36010: Der er ikke angivet korrekt landekode for modtagers bank"
            Case 36011
                sNorwegian = "36011: Feilet i CDV-kontroll norsk belastningskonto"
                sEnglish = "36011: CDV-error in payers bankaccount"
                sSwedish = "36011: Fel i CDV-kontroll  av norskt debiteringskonto"
                sDanish = "36011: Fejl i CDV-kontrol dansk frakonto"
            Case 36012
                sNorwegian = "36012: Mangler avsenderbanks SWIFT (BIC)"
                sEnglish = "36012: SWIFT for payers bank is missing"
                sSwedish = "36012: Saknar avsändarbankens SWIFT (BIC)"
                sDanish = "36012: Mangler afsenderbanks SWIFT (BIC)"
            Case 36013
                sNorwegian = "36013: Feil i avsenderbanks SWIFT(BIC)"
                sEnglish = "36013: Error in payers banks SWIFT (BIC)"
                sSwedish = "36013: Fel i avsändarbankens SWIFT (BIC)"
                sDanish = "36013: Fejl i afsenderbanks SWIFT(BIC)"
            Case 36014
                sNorwegian = "36014: Debetkontos valutakode er ikke angitt"
                sEnglish = "36014: Debitaccount's currencycode is missing"
                sSwedish = "36014: Debetkontots valutakod är inte rätt"
                sDanish = "36014: Debetkontoens valutakode er ikke angivet"
            Case 36015
                sNorwegian = "36015: Deklareringskode er ikke angitt"
                sEnglish = "36015: No declarationcode is set"
                sSwedish = "36015: Deklareringskod har inte angetts"
                sDanish = "36015: Deklareringskode er ikke angivet"
            Case 36016
                sNorwegian = "36016: Valutakode skal være angitt, som tre bokstavers kode"
                sEnglish = "36016: Currencycode must be given as a three charactercode"
                sSwedish = "36016: Valutakod ska anges som en kod på tre bokstäver"
                sDanish = "36016: Valutakode skal være angivet som kode med tre bogstaver"
            Case 36017
                sNorwegian = "36017: Betalingen må ha et positivt fakturabeløp - denne er negativ"
                sEnglish = "36017: A payment must have a positive invoiceamount - this one i negative"
                sSwedish = "36017: Betalningen måste ha positivt fakturabelopp - detta är negativt"
                sDanish = "36017: Betalingen skal have et positivt fakturabeløb - denne er negativ"
            Case 36018
                sNorwegian = "36018: Betalingen må ha et positivt overføringsbeløp - denne er negativ"
                sEnglish = "36018: A payment must have a positive transferamount - this one i negative"
                sSwedish = "36018: Betalningen måste ha positivt överföringsbelopp - detta är negativt"
                sDanish = "36018: Betalingen skal have et positivt overførselsbeløb - denne er negativ"
            Case 36019
                sNorwegian = "36019: Feil i KID"
                sEnglish = "36019: Error in KID"
                sSwedish = "36019: Fel i OCR"
                sDanish = "36019: Fejl i KID"
            Case 36020
                sNorwegian = "36020: Deklareringskode ikke angitt"
                sEnglish = "36020: No declarationtext is set"
                sSwedish = "36020: Deklareringskod har inte angetts"
                sDanish = "36020: Deklareringskode ikke angivet"
            Case 36021
                sNorwegian = "36021: Negativt beløp som ikke lar seg dekke inn mot betaling til samme mottaker"
                sEnglish = "36021: Negative amount which can not be coverered against payment to same receiver"
                sSwedish = "36021: Negativa belopp som inte kan täckas mot betalning till samma mottagare"
                sDanish = "36021: Negativt beløb som ikke kan dækkes ind af betaling til samme modtager"
            Case 36022
                sNorwegian = "36022: Dato for kreditnota er endret ved bunting"
                sEnglish = "36022: The date of the creditnote is changed when bundling payments"
                sSwedish = "36022: Datum för kreditfaktura ändrades genom sammanslagning"
                sDanish = "36022: Dato for kreditnota er ændret ved bundtning"
            Case 36023
                sNorwegian = "36023: Kreditnotaen er delt mot flere betalinger"
                sEnglish = "36023: The creditnote is splitted into several payments"
                sSwedish = "36023: Kreditnotan är delad över flera betalningar"
                sDanish = "36023: Kreditnotaen er delt til flere betalinger"
            Case 36024
                sNorwegian = "36024:  Dette formatet støtter ikke utlandsbetalinger"
                sEnglish = "36024: This format does not support international payments"
                sSwedish = "36024: Det här formatet stöder inte utlandsbetalningar"
                sDanish = "36024:  Dette format støtter ikke udlandsbetalinger"
            Case 36025
                sNorwegian = "leave for validate"
                sEnglish = "leave for validate"
                sSwedish = "leave for validate"
                sDanish = "leave for validate"
            Case 36050
                sNorwegian = "36050: Ikke tillatt med negativt beløp for sammenslått betaling"
                sEnglish = "36050: Negative amount for merged payment"
                sSwedish = "36050: Negativt belopp för sammanslagen betalning"
                sDanish = "36050: Negativt beløb for sammenlagt betaling"
            Case 36051
                sNorwegian = "36051: For mange underliggende betalinger til denne sammenslåtte betaling. Max. antall tillett er %1"
                sEnglish = "36051: Too many underlaying payments in this merged payment. Max.no. of underlaying are %1"
                sSwedish = "36051: För många underliggande betalningar i denna sammanslagna betalning. Max. tillåtet antal är %1"
                sDanish = "36051: For mange underliggende betalinger til denne sammenlagte betaling. Maks. antal tilladt er %1"
            Case 36052
                sNorwegian = "36052: Fakturavaluta (InvoISO) og Betalingsvaluta (PayISO) må være like"
                sEnglish = "36052: Invoicecurrency (InvoISO) and transfercurrency (PayISO) msut be equal"
                sSwedish = "36052: Fakturavaluta (InvoISO) och betalningsvaluta (PayISO) måste vara samma"
                sDanish = "36052: Fakturavaluta (InvoISO) og Betalingsvaluta (PayISO) skal være ens"
            Case 36053
                sNorwegian = "36053: Org.nr. / abonnementsnr. bank mangler"
                sEnglish = "36053: OrganizationNo / SubscriptionNo bank is missing"
                sSwedish = "36053: Org.nr/abonnemangsnr till bank saknas"
                sDanish = "36053: CVR.nr. / abonnementsnr. bank mangler"
            Case 36054
                sNorwegian = "36054: Feil valuta for innlandsbetaling"
                sEnglish = "36054: Wrong currency for domestic payment"
                sSwedish = "36054: Fel valuta för inlandsbetalning"
                sDanish = "36054: Forkert valuta for indlandsbetaling"
            Case 36055
                sNorwegian = "36055: Feil lengde på norsk belastningskonto (skal være 11)"
                sEnglish = "36055: Incorrect length in Norwegian debit account (should be 11)"
                sSwedish = "36055: Fel längd på norskt debiteringskonto (bör vara 11)"
                sDanish = "36055: Forkert længde på dansk frakonto (skal være 11)"
            Case 36056
                sNorwegian = "36056: Betalinger til USA skal ha ABA/Fedwire kode på 9 siffer"
                sEnglish = "36056: Payments to USA requires a 9 digits ABA/Fedwirecode"
                sSwedish = "36056: Betalningar till USA ska ha ABA-/Fedwire-kod på 9 siffror"
                sDanish = "36056: Betalinger til USA skal have ABA/Fedwire-kode på 9 cifre"
            Case 36057
                sNorwegian = "36057: Betalinger til England skal ha Sortcode på 6 siffer"
                sEnglish = "36057: Payments to UK requires a 6 digits sortcode"
                sSwedish = "36057: Betalningar till England ska ha Sortcode på 6 siffror"
                sDanish = "36057: Betalinger til England skal have Sortcode på 6 cifre"
            Case 36058
                sNorwegian = "36058: Betalinger til Canada skal ha ABA/Fedwire kode på 9 siffer"
                sEnglish = "36058: Payments to Canada requires a 9 digits ABA/Fedwirecode"
                sSwedish = "36058: Betalningar till Kanada ska ha ABA-/Fedwire-kod på 9 siffror"
                sDanish = "36058: Betalinger til Canada skal have ABA/-kode på 9 cifre"
            Case 36059
                sNorwegian = "Mottaksretur"
                sEnglish = "Recipt acknowledgement"
                sSwedish = "Mottagningskvitto"
                sDanish = "Modtagelsesretur"
            Case 36060
                sNorwegian = "Avregningsretur"
                sEnglish = "Processing reply"
                sSwedish = "Avräkningsretur"
                sDanish = "Afregningsretur"
            Case 36061
                sNorwegian = "Avvisningsretur"
                sEnglish = "Rejection reply"
                sSwedish = "Avvisningsretur"
                sDanish = "Afvisningsretur"
            Case 36062
                sNorwegian = "Innbetaling"
                sEnglish = "Incoming"
                sSwedish = "Inbetalning"
                sDanish = "Indbetaling"
            Case 36063
                sNorwegian = "Utbetaling, innsending"
                sEnglish = "Outgoing payment, sendfile"
                sSwedish = "Utbetalning, insändning"
                sDanish = "Udbetaling, indsendelse"
            Case 36064
                sNorwegian = "36064: Ikke tillatt med kreditnota til Plusgirokonto"
                sEnglish = "36064: Creditnotes not allowed to Plusgiroaccounts"
                sSwedish = "36064: Ej tillåtet med kreditnota till Plusgirokonto"
                sDanish = "36064: Ikke tilladt med kreditnota til Plusgirokonto"
            Case 36065
                sNorwegian = "36065: Det kreves avsendernavn ved bruk av format %1"
                sEnglish = "36065: Payers name is required for the format %1"
                sSwedish = "36065: Det krävs avsändarens namn i formatet"
                sDanish = "36065: Det kræver afsenderens navn for formatet %1"
            Case 36066
                sNorwegian = "36066: Dette er ikke en gyldig SEPA-betaling"
                sEnglish = "36066: This is not a valid SEPA payment."
                sSwedish = "36066: Detta är inte en giltig SEPA-betalning"
                sDanish = "36066: Dette er ikke et gyldigt SEPA betaling"
            Case 36067
                sNorwegian = "36067: Feil i IBAN for betalers konto"
                sEnglish = "36067: Error in IBAN of the payer's account."
                sSwedish = "36067: Fel i IBAN av betalarens konto"
                sDanish = "36067: Fejl i betalerens IBAN konto"
            Case 36068
                sNorwegian = "36068: Det kreves betalers navn ved bruk av Pain.001"
                sEnglish = "36068: Payers name is required for Pain.001"
                sSwedish = "36068: Det krävs betalarens namn for Pain.001"
                sDanish = "36068: Det kræves betalers navn ved bruk Pain.001"
            Case 36069
                sNorwegian = "For kort mottakerkonto"
                sEnglish = "Receivers account to short"
                sSwedish = "För kort mottagarkonto"
                sDanish = "For kort modtager konto"

            Case 40000
                sNorwegian = "--- Rapporter ---"
                sEnglish = "--- Reports ---"
                sSwedish = "--- Rapporter ---"
                sDanish = "--- Rapporter ---"
            Case 40001
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 40002
                sNorwegian = "CREMUL"
                sEnglish = "CREMUL"
                sSwedish = "CREMUL"
                sDanish = "CREMUL"
            Case 40003
                sNorwegian = "Val.dato:"
                sEnglish = "Valuedate:"
                sSwedish = "Val.datum:"
                sDanish = "Val.dato:"
            Case 40004
                sNorwegian = "Beløp konto:"
                sEnglish = "Amount Statement:"
                sSwedish = "Belopp konto:"
                sDanish = "Beløb konto:"
            Case 40005
                sNorwegian = "Referanse:"
                sEnglish = "Reference:"
                sSwedish = "Referens:"
                sDanish = "Reference:"
            Case 40006
                sNorwegian = "Konto:"
                sEnglish = "Account:"
                sSwedish = "Konto:"
                sDanish = "Konto:"
            Case 40007
                sNorwegian = "Valuta:"
                sEnglish = "Currency:"
                sSwedish = "Valuta:"
                sDanish = "Valuta:"
            Case 40008
                sNorwegian = "Trans.type:"
                sEnglish = "Trans.type:"
                sSwedish = "Trans.typ:"
                sDanish = "Trans.type:"
            Case 40009
                sNorwegian = "Side"
                sEnglish = "Page"
                sSwedish = "Sida"
                sDanish = "Side"
            Case 40010
                sNorwegian = "Beløpet gjelder:"
                sEnglish = "Payment concerns:"
                sSwedish = "Beloppet gäller:"
                sDanish = "Beløbet gælder:"
            Case 40011
                sNorwegian = "Arkivref.:"
                sEnglish = "Archive ref.:"
                sSwedish = "Arkivref.:"
                sDanish = "Arkivref.:"
            Case 40012
                sNorwegian = "Blankettref.:"
                sEnglish = "Giro ref.:"
                sSwedish = "Blankettref.:"
                sDanish = "Blanketref.:"
            Case 40013
                sNorwegian = "Kontonr.:"
                sEnglish = "Accountno.:"
                sSwedish = "Kontonr.:"
                sDanish = "Kontonr.:"
            Case 40014
                sNorwegian = "Antall betalinger:"
                sEnglish = "No of Payments"
                sSwedish = "Antal betalningar:"
                sDanish = "Antal betalinger:"
            Case 40015
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 40016
                sNorwegian = "Innlest dato:"
                sEnglish = "In date:"
                sSwedish = "Inläst datum:"
                sDanish = "Indlæst dato:"
            Case 40017
                sNorwegian = "Sum beløp:"
                sEnglish = "Total amount:"
                sSwedish = "Summa belopp:"
                sDanish = "Sum beløb:"
            Case 40018
                sNorwegian = "Antall batcher"
                sEnglish = "No of Batches:"
                sSwedish = "Antal batchar"
                sDanish = "Antal batches"
            Case 40019
                sNorwegian = "Antall betalinger"
                sEnglish = "No of Payments:"
                sSwedish = "Antal betalningar"
                sDanish = "Antal betalinger"
            Case 40020
                sNorwegian = "Mottakerkonto:"
                sEnglish = "Receivers account"
                sSwedish = "Mottagarkonto:"
                sDanish = "Modtagerkonto:"
            Case 40021
                sNorwegian = "Beløp"
                sEnglish = "Amount"
                sSwedish = "Belopp"
                sDanish = "Beløb"
            Case 40022
                sNorwegian = "Referanse"
                sEnglish = "Reference"
                sSwedish = "Referens"
                sDanish = "Reference"
            Case 40023
                sNorwegian = "Posteringsdato:"
                sEnglish = "Posting Date:"
                sSwedish = "Bokföringsdatum:"
                sDanish = "Posteringsdato:"
            Case 40024
                sNorwegian = "Trans.type"
                sEnglish = "Trans.type"
                sSwedish = "Trans.typ"
                sDanish = "Trans.type"
            Case 40025
                sNorwegian = "Skriver ut BabelBank rapport"
                sEnglish = "Producing BabelBank report"
                sSwedish = "Skriver ut BabelBank-rapport"
                sDanish = "Udskriver BabelBank rapport"
            Case 40026
                sNorwegian = "Rapport"
                sEnglish = "Report"
                sSwedish = "Rapport"
                sDanish = "Rapport"
            Case 40027
                sNorwegian = "Prod.dato:"
                sEnglish = "Prod.date"
                sSwedish = "Prod.datum:"
                sDanish = "Prod.dato:"
            Case 40028
                sNorwegian = "Postert"
                sEnglish = "Posted"
                sSwedish = "Bokfört"
                sDanish = "Posteret"
            Case 40029
                sNorwegian = "Uposterte"
                sEnglish = "Unposted"
                sSwedish = "Ej bokfört"
                sDanish = "Ikke posterede"
            Case 40030
                sNorwegian = "Klient:"
                sEnglish = "Client"
                sSwedish = "Klient:"
                sDanish = "Klient:"
            Case 40031
                sNorwegian = "Sum kontoutdrag"
                sEnglish = "Total account"
                sSwedish = "Summa kontoutdrag"
                sDanish = "Sum kontoudskrift"
            Case 40032
                sNorwegian = "Differanse"
                sEnglish = "Difference"
                sSwedish = "Differens"
                sDanish = "Difference"
            Case 40033
                sNorwegian = "Kontrollrapport"
                sEnglish = "Control report"
                sSwedish = "Kontroll"
                sDanish = "Kontrol"
            Case 40034
                sNorwegian = "Betalingsoversikt, med fakturainformasjon"
                sEnglish = "Remittance Payments"
                sSwedish = "Betalningsöversikt, med fakturainformation"
                sDanish = "Betalingsoversigt, med fakturainformation"
            Case 40035
                sNorwegian = "Kurs:"
                sEnglish = "Exch.rate"
                sSwedish = "Kurs:"
                sDanish = "Kurs:"
            Case 40036
                sNorwegian = "Postert beløp:"
                sEnglish = "Posted Amount"
                sSwedish = "Bokfört belopp:"
                sDanish = "Posteret beløb:"
            Case 40037
                sNorwegian = "Opprinnelig beløp:"
                sEnglish = "Original Amount"
                sSwedish = "Ursprungligt belopp:"
                sDanish = "Oprindeligt beløb:"
            Case 40038
                sNorwegian = "Kontobeløp:"
                sEnglish = "Account Amount"
                sSwedish = "Kontobelopp:"
                sDanish = "Kontobeløb:"
            Case 40039
                sNorwegian = "Endret betaling"
                sEnglish = "Corrected payment"
                sSwedish = "Ändrad betalning"
                sDanish = "Ændret betaling"
            Case 40040
                sNorwegian = "Betalingsoversikt"
                sEnglish = "Payments"
                sSwedish = "Betalningsöversikt"
                sDanish = "Betalingsoversigt"
            Case 40041
                sNorwegian = "Avsenderbankens omkostninger:"
                sEnglish = "Payers banks charges"
                sSwedish = "Avsändarbankens kostnader:"
                sDanish = "Afsenderbankens omkostninger:"
            Case 40042
                sNorwegian = "Mottakerbankens omkostninger:"
                sEnglish = "Receiving banks charges"
                sSwedish = "Mottagarbankens kostnader:"
                sDanish = "Modtagerbankens omkostninger:"
            Case 40043
                sNorwegian = "Gebyr"
                sEnglish = "Charges"
                sSwedish = "Avgift"
                sDanish = "Gebyr"
            Case 40044
                sNorwegian = "Bokf.dato"
                sEnglish = "Bookdate"
                sSwedish = "Bokf.datum"
                sDanish = "Bogf.dato"
            Case 40045
                sNorwegian = "Kontoref:"
                sEnglish = "Account ref"
                sSwedish = "Kontoref:"
                sDanish = "Kontoref:"
            Case 40046
                sNorwegian = "KID"
                sEnglish = "ID"
                sSwedish = "KID"
                sDanish = "KID"
            Case 40047
                sNorwegian = "Sum på kontoutdrag:"
                sEnglish = "Grouptotal statement"
                sSwedish = "Summa på kontoutdrag:"
                sDanish = "Sum på kontoudskrift:"
            Case 40048
                sNorwegian = "Betalers konto"
                sEnglish = "Payers account"
                sSwedish = "Betalarens konto"
                sDanish = "Betalers konto"
            Case 40049
                sNorwegian = "Innbetalingsdato"
                sEnglish = "Paymentdate"
                sSwedish = "Inbetalningsdatum"
                sDanish = "Indbetalingsdato"
            Case 40050
                sNorwegian = "aKonto"
                sEnglish = "CustomerAccount"
                sSwedish = "aKonto"
                sDanish = "aconto"
            Case 40051
                sNorwegian = "Fakturanr."
                sEnglish = "InvoiceNo."
                sSwedish = "Fakturanr."
                sDanish = "Fakturanr."
            Case 40052
                sNorwegian = "Kreditnotanr."
                sEnglish = "CreditNo"
                sSwedish = "Kreditnotenr."
                sDanish = "Kreditnotanr."
            Case 40053
                sNorwegian = "KreditKID"
                sEnglish = "CreditKID"
                sSwedish = "KreditKID"
                sDanish = "KreditKID"
            Case 40054
                sNorwegian = "Tekst kontoutdrag"
                sEnglish = "Text on statement"
                sSwedish = "Text kontoutdrag"
                sDanish = "Tekst kontoudskrift"
            Case 40055
                sNorwegian = "Nattsafe, Total"
                sEnglish = "Depositbox, total"
                sSwedish = "Nattsafe, summa"
                sDanish = "Natsafe, Total"
            Case 40056
                sNorwegian = "Pose/Innkastnr."
                sEnglish = "BagNo./DropNo."
                sSwedish = "Depositionsnr."
                sDanish = "Pose/Indkastnr."
            Case 40057
                sNorwegian = "Registrert beløp"
                sEnglish = "Amount recorded"
                sSwedish = "Registrerat belopp"
                sDanish = "Registreret beløb"
            Case 40058
                sNorwegian = "Opptalt beløp"
                sEnglish = "Amount counted"
                sSwedish = "Angivet belopp"
                sDanish = "Optalt beløb"
            Case 40059
                sNorwegian = "Avdeling"
                sEnglish = "Department"
                sSwedish = "Avdelning"
                sDanish = "Afdeling"
            Case 40060
                sNorwegian = "Tellesentral"
                sEnglish = "Operator"
                sSwedish = "Räkningscentral"
                sDanish = "Tællecentral"
            Case 40061
                sNorwegian = "Registreringsdato"
                sEnglish = "Deliverydate"
                sSwedish = "Registreringsdatum"
                sDanish = "Registreringsdato"
            Case 40062
                sNorwegian = "Intern differanse"
                sEnglish = "Internal difference"
                sSwedish = "Intern differens"
                sDanish = "Intern difference"
            Case 40063
                sNorwegian = "Differanse telling"
                sEnglish = "Difference counted"
                sSwedish = "Differens i räkning"
                sDanish = "Difference tælling"
            Case 40064
                sNorwegian = "Nattsafe, differanse"
                sEnglish = "Depositbox, differences"
                sSwedish = "Nattsafe, differens"
                sDanish = "Natsafe, difference"
            Case 40065
                sNorwegian = "Sedler"
                sEnglish = "Notes"
                sSwedish = "Sedlar"
                sDanish = "Sedler"
            Case 40066
                sNorwegian = "Mynt"
                sEnglish = "Coins"
                sSwedish = "Mynt"
                sDanish = "Mønter"
            Case 40067
                sNorwegian = "Sjekker"
                sEnglish = "Cheques"
                sSwedish = "Checkar"
                sDanish = "Check"
            Case 40068
                sNorwegian = "Bilagsnummer:"
                sEnglish = "VoucherNo"
                sSwedish = "Verifikationsnummer:"
                sDanish = "Bilagsnummer:"
            Case 40069
                sNorwegian = "Avviksrapport ERPsystem/BabelBank"
                sEnglish = "Deviationreport ERPsystem/BabelBank"
                sSwedish = "Avvikelserapport ERPsystem/BabelBank"
                sDanish = "Afvigelsesrapport ERPsystem/BabelBank"
            Case 40070
                sNorwegian = "ERP_ID"
                sEnglish = "ERP_ID"
                sSwedish = "ERP_ID"
                sDanish = "ERP_ID"
            Case 40071
                sNorwegian = "Betalers navn"
                sEnglish = "Payers name"
                sSwedish = "Betalarens namn"
                sDanish = "Betalers navn"
            Case 40072
                sNorwegian = "Kortart"
                sEnglish = "Cardcode"
                sSwedish = "Referens"
                sDanish = "Kortart"
            Case 40073
                sNorwegian = "Kundenr."
                sEnglish = "Customerno."
                sSwedish = "Kundnr."
                sDanish = "Kundenr."
            Case 40074
                sNorwegian = "Hvordan avstemt:"
                sEnglish = "How matched:"
                sSwedish = "Typ av avstämning:"
                sDanish = "Hvordan afstemt:"
            Case 40075
                sNorwegian = "Åpne poster"
                sEnglish = "Open items"
                sSwedish = "Öppna poster"
                sDanish = "Åbne poster"
            Case 40076
                sNorwegian = "Dagtotaler"
                sEnglish = "Day Totals"
                sSwedish = "Dagsummor"
                sDanish = "Dagtotaler"
            Case 40077
                sNorwegian = "Hovedbok"
                sEnglish = "General Ledger"
                sSwedish = "Huvudbok"
                sDanish = "Hovedbog"
            Case 40078
                sNorwegian = "Leverandørnr."
                sEnglish = "VendorNo."
                sSwedish = "Leverantörsnr."
                sDanish = "Leverandørnr."
            Case 40079
                sNorwegian = "Bankinfo"
                sEnglish = "Bankinfo"
                sSwedish = "Bankinfo"
                sDanish = "Bankinfo"
            Case 40080
                sNorwegian = "Betalinger med nullbeløp"
                sEnglish = "Payments with zeroamounts"
                sSwedish = "Betalningar med nollbelopp"
                sDanish = "Betalinger med nulbeløb"
            Case 40081
                sNorwegian = "Sjekk routing transit no:"
                sEnglish = "Check Routing Transit No:"
                sSwedish = "Kontrollera routningstransitnr:"
                sDanish = "Check routing transit no:"
            Case 40082
                sNorwegian = "Sjekk DDA Nummer:"
                sEnglish = "Check DDA Number:"
                sSwedish = "Kontrollera DDA-nummer:"
                sDanish = "Check DDA Nummer:"
            Case 40083
                sNorwegian = "Sjekk nummer:"
                sEnglish = "Check Number:"
                sSwedish = "Kontrollera nummer:"
                sDanish = "Check nummer:"
            Case 40084
                sNorwegian = "Sjekk dato:"
                sEnglish = "Check Date:"
                sSwedish = "Kontrollera datum:"
                sDanish = "Check dato:"
            Case 40085
                sNorwegian = "Trace Number:"
                sEnglish = "Trace Number:"
                sSwedish = "Trace Number:"
                sDanish = "Trace Number:"
            Case 40086
                sNorwegian = "Receiving Bank ID:"
                sEnglish = "Receiving Bank ID:"
                sSwedish = "Receiving Bank ID:"
                sDanish = "Receiving Bank ID:"
            Case 40087
                sNorwegian = "Receiving company"
                sEnglish = "Receiving company"
                sSwedish = "Receiving company"
                sDanish = "Receiving company"
            Case 40088
                sNorwegian = "Ordering Company"
                sEnglish = "Ordering Company"
                sSwedish = "Ordering Company"
                sDanish = "Ordering Company"
            Case 40089
                sNorwegian = "Rec. Company ID"
                sEnglish = "Rec. Company ID"
                sSwedish = "Rec. Company ID"
                sDanish = "Rec. Company ID"
            Case 40090
                sNorwegian = "Standard Entry Class"
                sEnglish = "Standard Entry Class"
                sSwedish = "Standard Entry Class"
                sDanish = "Standard Entry Class"
            Case 40091
                sNorwegian = "Ordering Company ID"
                sEnglish = "Ordering Company ID"
                sSwedish = "Ordering Company ID"
                sDanish = "Ordering Company ID"
            Case 40092
                sNorwegian = "Ordering Bank ID"
                sEnglish = "Ordering Bank ID"
                sSwedish = "Ordering Bank ID"
                sDanish = "Ordering Bank ID"
            Case 40093
                sNorwegian = "Ordering Company Desc"
                sEnglish = "Ordering Company Desc"
                sSwedish = "Ordering Company Desc"
                sDanish = "Ordering Company Desc"
            Case 40094
                sNorwegian = "Detaljer:"
                sEnglish = "Details:"
                sSwedish = "Detaljer:"
                sDanish = "Detaljer:"
            Case 40095
                sNorwegian = "Avviste betalinger"
                sEnglish = "Rejected payments"
                sSwedish = "Avvisade betalningar"
                sDanish = "Afviste betalinger"
            Case 40096
                sNorwegian = "Ref. til kontoeier"
                sEnglish = "Ref. account owner"
                sSwedish = "Ref. till kontoägare"
                sDanish = "Ref. til kontoejer"
            Case 40097
                sNorwegian = "CONTRL FEIL"
                sEnglish = "CONTRL ERROR"
                sSwedish = "CONTRL FEL"
                sDanish = "CONTRL FEJL"
            Case 40098
                sNorwegian = "CONTRL OK"
                sEnglish = "CONTRL OK"
                sSwedish = "CONTRL OK"
                sDanish = "CONTRL OK"
            Case 40099
                sNorwegian = "Fil ID"
                sEnglish = "File ID"
                sSwedish = "Fil-ID"
                sDanish = "Fil ID"
            Case 40100
                sNorwegian = "Avsender"
                sEnglish = "Sender"
                sSwedish = "Avsändare"
                sDanish = "Afsender"
            Case 40101
                sNorwegian = "Statuskode"
                sEnglish = "Statuscode"
                sSwedish = "Statuskod"
                sDanish = "Statuskode"
            Case 40102
                sNorwegian = "Filtrerte og slettede betalinger"
                sEnglish = "Filtered and deleted payments"
                sSwedish = "Filtrerade och borttagna betalningar"
                sDanish = "Filtrerede og slettede betalinger"
            Case 40103
                sNorwegian = "Totalsum"
                sEnglish = "Hash total"
                sSwedish = "Totalsumma"
                sDanish = "Totalsum"
            Case 40104
                sNorwegian = "Beløp:"
                sEnglish = "Amount:"
                sSwedish = "Belopp:"
                sDanish = "Beløb:"
            Case 40105
                sNorwegian = "Deltotal - antall:"
                sEnglish = "Parttotal - Count:"
                sSwedish = "Deltotal - antal:"
                sDanish = "Deltotal - antal:"
            Case 40106
                sNorwegian = "Total - Antall:"
                sEnglish = "Grand total - Count:"
                sSwedish = "Totalt - antal:"
                sDanish = "Total - Antal:"
            Case 40107
                sNorwegian = "Ingen klientinfo angitt"
                sEnglish = "No clientinfo stated"
                sSwedish = "Ingen klientinformation har angetts"
                sDanish = "Ingen klientinfo angivet"
            Case 40108
                sNorwegian = "Betaler:"
                sEnglish = "Payor:"
                sSwedish = "Betalare:"
                sDanish = "Betaler:"
            Case 40109
                sNorwegian = "Bankkode:"
                sEnglish = "Branch number:"
                sSwedish = "Bankkod:"
                sDanish = "Bankkode:"
            Case 40110
                sNorwegian = "Innbet.dag:"
                sEnglish = "Date paid:"
                sSwedish = "Inbet.dag:"
                sDanish = "Indbet.dag:"
            Case 40111
                sNorwegian = "Antall feil:"
                sEnglish = "No. of errors:"
                sSwedish = "Antal fel:"
                sDanish = "Antal fejl:"
            Case 40112
                sNorwegian = "Uposterte innbetalinger"
                sEnglish = "Unposted payments"
                sSwedish = "Ej bokförda inbetalningar"
                sDanish = "Ikke posterede indbetalinger"
            Case 40113
                sNorwegian = "BabelBank har benyttet opprinnelig betalt beløp."
                sEnglish = "BabelBank has used originally paid amount"
                sSwedish = "BabelBank har använt det ursprungliga betalda beloppet."
                sDanish = "BabelBank har benyttet oprindeligt betalt beløb."
            Case 40114
                sNorwegian = "Bilagstekst:"
                sEnglish = "Vouchertext:"
                sSwedish = "Verifikationstext:"
                sDanish = "Bilagstekst:"
            Case 40115
                sNorwegian = "Posterte innbetalinger"
                sEnglish = "Posted payments"
                sSwedish = "Bokförda inbetalningar"
                sDanish = "Posterede indbetalinger"
            Case 40116
                sNorwegian = "Oppsummering innbetalinger"
                sEnglish = "Summary"
                sSwedish = "Summering av inbetalningar"
                sDanish = "Opsummering indbetalinger"
            Case 40117
                sNorwegian = "Totalt"
                sEnglish = "Total"
                sSwedish = "Totalt"
                sDanish = "Totalt"
            Case 40118
                sNorwegian = "Inngående saldo"
                sEnglish = "Incoming balance"
                sSwedish = "Ingående saldo"
                sDanish = "Indgående saldo"
            Case 40119
                sNorwegian = "Innlest BabelBank"
                sEnglish = "Imported BabelBank"
                sSwedish = "Inläst BabelBank"
                sDanish = "Indlæst BabelBank"
            Case 40120
                sNorwegian = "Total etter import"
                sEnglish = "Total after import"
                sSwedish = "Totalt efter import"
                sDanish = "Total efter import"
            Case 40121
                sNorwegian = "Dagens produksjon"
                sEnglish = "Todays production"
                sSwedish = "Dagens produktion"
                sDanish = "Dagens produktion"
            Case 40122
                sNorwegian = "OCR-poster"
                sEnglish = "OCR-payments"
                sSwedish = "OCR-poster"
                sDanish = "OCR-poster"
            Case 40123
                sNorwegian = "Postert reskontro"
                sEnglish = "Posted receivabels"
                sSwedish = "Bokfört i reskontra"
                sDanish = "Posteret samlekonto"
            Case 40124
                sNorwegian = "Postert hovedbok"
                sEnglish = "Posted GL"
                sSwedish = "Bokfört i huvudbok"
                sDanish = "Posteret hovedbog"
            Case 40125
                sNorwegian = "Utgående saldo"
                sEnglish = "Outgoing balance"
                sSwedish = "Utgående saldo"
                sDanish = "Udgående saldo"
            Case 40126
                sNorwegian = "Åpne poster"
                sEnglish = "Open payments"
                sSwedish = "Öppna poster"
                sDanish = "Åbne poster"
            Case 40127
                sNorwegian = "Differanse"
                sEnglish = "Difference"
                sSwedish = "Differens"
                sDanish = "Difference"
            Case 40128
                sNorwegian = "BabelBank statistikk"
                sEnglish = "BabelBank statistics"
                sSwedish = "BabelBank-statistik"
                sDanish = "BabelBank statistik"
            Case 40129
                sNorwegian = "Fakturaer"
                sEnglish = "Invoices"
                sSwedish = "Fakturor"
                sDanish = "Fakturaer"
            Case 40130
                sNorwegian = "Totalt importert"
                sEnglish = "Total imported"
                sSwedish = "Totalt importerat"
                sDanish = "Totalt importeret"
            Case 40131
                sNorwegian = "Andre"
                sEnglish = "Other"
                sSwedish = "Annat"
                sDanish = "Andre"
            Case 40132
                sNorwegian = "Automatpostert (inkl. forslagsavstemte)"
                sEnglish = "Automatic posted (incl. proposed)"
                sSwedish = "Autobokfört (inkl. avstämningsförslag)"
                sDanish = "Automatposteret (inkl. foreslåede afstemte)"
            Case 40133
                sNorwegian = "Forslag"
                sEnglish = "Proposed"
                sSwedish = "Förslag"
                sDanish = "Forslag"
            Case 40134
                sNorwegian = "Endelig"
                sEnglish = "Final"
                sSwedish = "Klart"
                sDanish = "Endelig"
            Case 40135
                sNorwegian = "Upostert etter automatisk"
                sEnglish = "Unposted after automatic"
                sSwedish = "Ej autobokfört"
                sDanish = "Ikke posteret efter automatisk"
            Case 40136
                sNorwegian = "Manuelt postert"
                sEnglish = "Manually posted"
                sSwedish = "Manuellt bokfört"
                sDanish = "Manuelt posteret"
            Case 40137
                sNorwegian = "Upostert etter manuell avstemming"
                sEnglish = "Unposted after manual posting"
                sSwedish = "Ej bokfört efter manuell avstämning"
                sDanish = "Ikke posteret efter manuel afstemning"
            Case 40138
                sNorwegian = "Egen konto:"
                sEnglish = "Own account:"
                sSwedish = "Egen konto"
                sDanish = "Egen konto"
            Case 40139
                sNorwegian = "Ekstern konto:"
                sEnglish = "External account:"
                sSwedish = "Ekstern konto:"
                sDanish = "Ekstern konto:"
            Case 40140
                sNorwegian = "Kommentar:"
                sEnglish = "Comment:"
                sSwedish = "Kommentar:"
                sDanish = "Kommentar:"
            Case 40141
                sNorwegian = "AML Rapport"
                sEnglish = "AML Report"
                sSwedish = "AML Rapport"
                sDanish = "AML Rapport"
            Case 40142
                sNorwegian = "Alle betalinger aksepterte (ACTC)"
                sEnglish = "All payments accepted (ACTC)"
                sSwedish = "Alla betalningar accepteras (ACTC)"
                sDanish = "Alle betalinger accepteres (ACTC)"
            Case 40143
                sNorwegian = "Nettobeløp"
                sEnglish = "Net amount"
                sSwedish = "Nettobelopp"
                sDanish = "Nettobeløb"
            Case 40144
                sNorwegian = "Bruttobeløp"
                sEnglish = "Gross amount"
                sSwedish = "Bruttobelopp"
                sDanish = "Bruttobeløb"
            Case 40145
                sNorwegian = "Posterte KID innbetalinger"
                sEnglish = "Posted KID payments"
                sSwedish = "Bokförda KID inbetalningar"
                sDanish = "Posterede KID indbetalinger"


            Case 48000
                sNorwegian = "----- DATABASE -------------"
                sEnglish = "----- DATABASE ------"
                sSwedish = "----- DATABASE -------------"
                sDanish = "----- DATABASE -------------"
            Case 48003
                sNorwegian = "Avst. på fakturanummer og beløp."
                sEnglish = "Match on invoicenumber and amount."
                sSwedish = "Avst. av fakturanummer och belopp."
                sDanish = "Afst. på fakturanummer og beløb."
            Case 48004
                sNorwegian = "Avst. på kontonummer og beløp."
                sEnglish = "Match on accountnumbewr and amount."
                sSwedish = "Avst. av kontonummer och belopp."
                sDanish = "Afst. på kontonummer og beløb."
            Case 48005
                sNorwegian = "Avst. av generalnota."
                sEnglish = "Match on generalnota."
                sSwedish = "Avst. av saldobesked."
                sDanish = "Afst. af generalnota."
            Case 48006
                sNorwegian = "Avst. på fakturanr. uavhengig av firma."
                sEnglish = "Match on invoiceno. independent of client."
                sSwedish = "Avst. av fakturanr oberoende från företag."
                sDanish = "Afst. på fakturanr. uafhængig af firma."
            Case 48007
                sNorwegian = "For fremtidig bruk."
                sEnglish = "For future use."
                sSwedish = "För framtida bruk."
                sDanish = "Til fremtidig brug."
            Case 48008
                sNorwegian = "A-konto vha. kontonummer"
                sEnglish = "Add customernumber to the payment."
                sSwedish = "A-konto för kontonummer"
                sDanish = "Aconto vha. kontonummer"
            Case 48009
                sNorwegian = "Oppdatere kontonummer på kunde."
                sEnglish = "Update customer with accountnumber."
                sSwedish = "Uppdatera kontonummer för kund."
                sDanish = "Opdater kontonummer på kunde."
            Case 48010
                sNorwegian = "MinSQL"
                sEnglish = "MySQL"
                sSwedish = "MinSQL"
                sDanish = "MinSQL"
            Case 48011
                sNorwegian = "Mottaker:"
                sEnglish = "Reveiver:"
                sSwedish = "Mottagare:"
                sDanish = "Modtager:"
            Case 48012
                sNorwegian = "En uventet feil har oppstått, den opprinnelige fakturaspesifikasjonen er fjernet!"
                sEnglish = "Due to an unexpected error, the original invoice(s) and message(s) were removed!"
                sSwedish = "Ett oväntat fel har uppstått. Den ursprungliga fakturaspecifikationen har tagits bort!"
                sDanish = "Der opstod en uventet fejl, den oprindelige fakturaspecifikation er fjernet!"
            Case 48013
                sNorwegian = "Endelig mottaker:"
                sEnglish = "Ultimate receiver:"
                sSwedish = "Slutligen mottagare:"
                sDanish = "Endelig modtager:"
            Case 48014
                sNorwegian = "Endelig betaler:"
                sEnglish = "Ultimate payor:"
                sSwedish = "Slutligen betalare:"
                sDanish = "Endelig betaler:"

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 48020

            Case 55000
                sNorwegian = "---   Buttons ---"
                sEnglish = "---  Buttons  ---"
                sSwedish = "---   Buttons ---"
                sDanish = "---   Buttons ---"
            Case 55001
                sNorwegian = "&Avbryt"
                sEnglish = "&Cancel"
                sSwedish = "&Avbryt"
                sDanish = "&Afbryd"
            Case 55002
                sNorwegian = "&OK"
                sEnglish = "&OK"
                sSwedish = "&OK"
                sDanish = "&OK"
            Case 55003
                sNorwegian = "&Utskrift"
                sEnglish = "&Print"
                sSwedish = "&Utskrift"
                sDanish = "&Udskrift"
            Case 55004
                sNorwegian = "&Alle oppdrag"
                sEnglish = "&All payments"
                sSwedish = "&Alla uppdrag"
                sDanish = "&Alle opgaver"
            Case 55005
                sNorwegian = "&Merket linje"
                sEnglish = "&Selected line"
                sSwedish = "&Markerad rad"
                sDanish = "&Markeret linje"
            Case 55006
                sNorwegian = "&Sammendrag"
                sEnglish = "&Summary"
                sSwedish = "&Sammandrag"
                sDanish = "&Sammendrag"
            Case 55007
                sNorwegian = "&Avslutt"
                sEnglish = "&Close"
                sSwedish = "&Avsluta"
                sDanish = "&Afslut"
            Case 55008
                sNorwegian = "Med &detaljer"
                sEnglish = "Print &details"
                sSwedish = "Med &detaljer"
                sDanish = "Med &detaljer"
            Case 55009
                sNorwegian = "&Uten detaljer"
                sEnglish = "&No details"
                sSwedish = "&Utan detaljer"
                sDanish = "&Uden detaljer"
            Case 55010
                sNorwegian = "&Merket oppdrag"
                sEnglish = "&Selected line"
                sSwedish = "&Markerat uppdrag"
                sDanish = "&Markeret opgave"
            Case 55011
                sNorwegian = "Avbryt prosessen"
                sEnglish = "Cancel process"
                sSwedish = "Avbryt processen"
                sDanish = "Afbryd processen"
            Case 55012
                sNorwegian = "Fortsett kjøring"
                sEnglish = "Continue process"
                sSwedish = "Fortsätt körning"
                sDanish = "Fortsæt kørsel"
            Case 55013
                sNorwegian = "&Neste"
                sEnglish = "&Next"
                sSwedish = "&Nästa"
                sDanish = "&Næste"
            Case 55014
                sNorwegian = "&Slett denne"
                sEnglish = "&Delete this"
                sSwedish = "&Ta bort denna"
                sDanish = "&Slet denne"
            Case 55015
                sNorwegian = "Kopier tilbake"
                sEnglish = "Copy to orig."
                sSwedish = "Kopiera tillbaka"
                sDanish = "Kopier tilbage"
            Case 55016
                sNorwegian = "Vis filinnhold"
                sEnglish = "&Browse file"
                sSwedish = "Visa filinnehåll"
                sDanish = "Vis filindhold"
            Case 55017
                sNorwegian = "Supportmail"
                sEnglish = "Supportmail"
                sSwedish = "Supportpost"
                sDanish = "Supportmail"
            Case 55018
                sNorwegian = "Avbryt &denne"
                sEnglish = "&Undo this"
                sSwedish = "Avbryt &denna"
                sDanish = "Afbryd &denne"
            Case 55019
                sNorwegian = "Endre navn/adresse"
                sEnglish = "Change name/address"
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 55030
                sSwedish = "Ändra namn/adress"
                sDanish = "Rediger navn/adresse"
            Case 55030
                sNorwegian = "&Vis"
                sEnglish = "&Show"
                sSwedish = "&Visa"
                sDanish = "&Vis"
            Case 55031
                sNorwegian = "&Søk"
                sEnglish = "&Find"
                sSwedish = "&Sök"
                sDanish = "&Søg"
            Case 55032
                sNorwegian = "&Lukk"
                sEnglish = "&Close"
                sSwedish = "&Stänga"
                sDanish = "&Luk"
            Case 55033
                sNorwegian = "&Analyser"
                sEnglish = "&Analyze"
                sSwedish = "&Analysera"
                sDanish = "&Analyser"


            Case 60000
                sNorwegian = "---   Display on screen ---"
                sEnglish = "---  Display on screen  ---"
                sSwedish = "---   Display on screen ---"
                sDanish = "---   Display on screen ---"
            Case 60001
                sNorwegian = "Avviste betalinger"
                sEnglish = "Rejected payments"
                sSwedish = "Avvisade betalningar"
                sDanish = "Afviste betalinger"
            Case 60002
                sNorwegian = "Betalinger"
                sEnglish = "Payments"
                sSwedish = "Betalningar"
                sDanish = "Betalinger"
            Case 60003
                sNorwegian = "Filen %1 finnes fra før."
                sEnglish = "The file %1 exists."
                sSwedish = "Filen %1 finns redan."
                sDanish = "Filen %1 findes fra før."
            Case 60004
                sNorwegian = "Filen %1 finnes fra før." & vbLf & "Programmet avbrytes!"
                sEnglish = "The file %1 exists." & vbLf & "The program terminates!"
                sSwedish = "Filen %1 finns redan." & vbLf & "Programmet avbryts!	"
                sDanish = "Filen %1 findes fra før." & vbLf & "Programmet afbrydes!	"
            Case 60005
                sNorwegian = "Katalogen %1 finnes ikke."
                sEnglish = "Folder %1 does not exist."
                sSwedish = "Katalogen %1 finns inte."
                sDanish = "Kataloget %1 findes ikke."
            Case 60006
                sNorwegian = "Ønsker du å opprette katalogen?"
                sEnglish = "Do You want to create the folder?"
                sSwedish = "Vill du skapa katalogen?"
                sDanish = "Ønsker du at oprette kataloget?"
            Case 60007
                sNorwegian = "60007: Folderen %1 er skrivebeskyttet." & vbLf & "Endre katalog, eller endre tilgangen til katalogen."
                sEnglish = "60007: The folder %1 is read-only." & vbLf & "Change the folder, or change the attributes of the folder."
                sSwedish = "60007: Mappen %1 är skrivskyddad." & vbLf & "Byt katalog eller ändra åtkomst till katalogen.	"
                sDanish = "60007: Mappen %1 er skrivebeskyttet." & vbLf & "Ændr katalog, eller ændr adgangen til kataloget.	"
            Case 60008
                sNorwegian = "60008: Katalogen %1 finnes ikke."
                sEnglish = "60008: Folder %1 doesn't exist."
                sSwedish = "60008: Katalogen %1 finns inte."
                sDanish = "60008: Mappen %1 findes ikke."
            Case 60009
                sNorwegian = "60009: Kan ikke opprette katalogen %1."
                sEnglish = "60009: Can't create the folder %1."
                sSwedish = "60009: Kan inte skapa katalogen %1."
                sDanish = "60009: Kan ikke oprette mappe %1."
            Case 60010
                sNorwegian = "60010: Katalogen er skrivebeskyttet."
                sEnglish = "Attribute is read-only."
                sSwedish = "60010: Katalogen är skrivskyddad."
                sDanish = "60010: Mappen er skrivebeskyttet."
            Case 60011
                sNorwegian = "60011: Stasjonen eksisterer ikke."
                sEnglish = "60010: The drive does not exist."
                sSwedish = "60011: Stationen finns inte."
                sDanish = "60011: Drevet eksisterer ikke."
            Case 60012
                sNorwegian = "Ingen katalog er spesifisert."
                sEnglish = "60011: No folder specified."
                sSwedish = "Ingen katalog har angetts."
                sDanish = "Der er ikke specificeret en mappe."
            Case 60013
                sNorwegian = "60013: Feil angivelse av katalog."
                sEnglish = "60012: Wrong specification of folder."
                sSwedish = "60013: Fel angivelse av katalog."
                sDanish = "60013: Forkert angivelse af mappe."
            Case 60014
                sNorwegian = "60014: Ukjent feil oppstod under kontroll/opprettelse av katalogen" & vbLf & "%1"
                sEnglish = "Unknown Error occured during checking/creating the folder" & vbLf & "%1"
                sSwedish = "60014: Okänt fel uppstod under kontroll/generering av katalogen" & vbLf & "%1	"
                sDanish = "60014: Der opstod en ukendt fejl ved kontrol/oprettelse af mappen" & vbLf & "%1	"
            Case 60015
                sNorwegian = "Kun rapport"
                sEnglish = "Report Only"
                sSwedish = "Endast rapport"
                sDanish = "Kun rapport"
            Case 60016
                sNorwegian = "60016: Kunne ikke fjerne gamle backupfiler"
                sEnglish = "60016: Unable to remove files from backupcatalog"
                sSwedish = "60016: Kunde inte ta bort gamla säkerhetskopieringsfiler"
                sDanish = "60016: Kunne ikke fjerne gamle backupfiler"
            Case 60058
                sNorwegian = "Ukjent filtype"
                sEnglish = "Unknown filetype"
                sSwedish = "Okänd filtyp"
                sDanish = "Ukendt filtype"
            Case 60059
                sNorwegian = "Antall"
                sEnglish = "No of payments"
                sSwedish = "Antal"
                sDanish = "Antal"
            Case 60060
                sNorwegian = "Beløp"
                sEnglish = "Amount"
                sSwedish = "Belopp"
                sDanish = "Beløb"
            Case 60061
                sNorwegian = "Filnavn"
                sEnglish = "Filename"
                sSwedish = "Filnamn"
                sDanish = "Filnavn"
            Case 60062
                sNorwegian = "Divisjon/Konto"
                sEnglish = "Division/Account"
                sSwedish = "Division/Konto"
                sDanish = "Division/Konto"
            Case 60063
                sNorwegian = "Sekv."
                sEnglish = "Seq."
                sSwedish = "Sekv."
                sDanish = "Sekv."
            Case 60064
                sNorwegian = "Mottakerkonto"
                sEnglish = "Receivers account"
                sSwedish = "Mottagarkonto"
                sDanish = "Modtagerkonto"
            Case 60065
                sNorwegian = "Dato"
                sEnglish = "Date"
                sSwedish = "Datum"
                sDanish = "Dato"
            Case 60066
                sNorwegian = "Valuta"
                sEnglish = "Currency"
                sSwedish = "Valuta"
                sDanish = "Valuta"
            Case 60067
                sNorwegian = "Betaler"
                sEnglish = "Payer"
                sSwedish = "Betalare"
                sDanish = "Betaler"
            Case 60068
                sNorwegian = "KID"
                sEnglish = "KID"
                sSwedish = "KID"
                sDanish = "KID"
            Case 60069
                sNorwegian = "Betalers konto"
                sEnglish = "Payers Account"
                sSwedish = "Betalarens konto"
                sDanish = "Betalers konto"
            Case 60070
                sNorwegian = "Belastningskonto"
                sEnglish = "From account"
                sSwedish = "Belastningskonto"
                sDanish = "Frakonto"
            Case 60071
                sNorwegian = "Mottaker"
                sEnglish = "Receiver"
                sSwedish = "Mottagare"
                sDanish = "Modtager"
            Case 60072
                sNorwegian = "Egenreferanse"
                sEnglish = "Own reference"
                sSwedish = "Egen referens"
                sDanish = "Egenreference"
            Case 60073
                sNorwegian = "Kreditorreferanse"
                sEnglish = "KID/Info to receiver"
                sSwedish = "Kreditorreferens"
                sDanish = "Kreditorreference"
            Case 60074
                sNorwegian = "Ref."
                sEnglish = "Ref."
                sSwedish = "Ref."
                sDanish = "Ref."
            Case 60075
                sNorwegian = "Divisjon:"
                sEnglish = "Division"
                sSwedish = "Division:"
                sDanish = "Division:"
            Case 60076
                sNorwegian = "Konto"
                sEnglish = "Account"
                sSwedish = "Konto"
                sDanish = "Konto"
            Case 60077
                sNorwegian = "Blankettnr."
                sEnglish = "Giro no."
                sSwedish = "Blankettnr."
                sDanish = "Blanketnr."
            Case 60078
                sNorwegian = "Postgiro:"
                sEnglish = "Postalgiro:"
                sSwedish = "Postgiro:"
                sDanish = "Postgiro:"
            Case 60079
                sNorwegian = "Arkivref:"
                sEnglish = "ArchiveRef:"
                sSwedish = "Arkivref:"
                sDanish = "Arkivref:"
            Case 60080
                sNorwegian = "Sum på kontoutdrag"
                sEnglish = "Total amount on statement"
                sSwedish = "Summa på kontoutdrag"
                sDanish = "Sum på kontoudskrift"
            Case 60081
                sNorwegian = "Kontoref."
                sEnglish = "Accounts ref."
                sSwedish = "Kontoref."
                sDanish = "Kontoref."
            Case 60082
                sNorwegian = "Dagsum"
                sEnglish = "Day total"
                sSwedish = "Dagsumma"
                sDanish = "Dagsum"
            Case 60083
                sNorwegian = "Sum for konto:"
                sEnglish = "Accounts total"
                sSwedish = "Summa för konto:"
                sDanish = "Sum for konto:"
            Case 60084
                sNorwegian = "Status"
                sEnglish = "Status"
                sSwedish = "Status"
                sDanish = "Status"
            Case 60085
                sNorwegian = "Meldingstekst"
                sEnglish = "Message"
                sSwedish = "Meddelandetext"
                sDanish = "Meddelelsestekst"
            Case 60086
                sNorwegian = "Navn"
                sEnglish = "Name"
                sSwedish = "Namn"
                sDanish = "Navn"
            Case 60087
                sNorwegian = "BabelBank melding:"
                sEnglish = "Comment from BabelBank:"
                sSwedish = "BabelBank-meddelande:"
                sDanish = "BabelBank meddelelse:"
            Case 60088
                sNorwegian = "Postert"
                sEnglish = "Posted"
                sSwedish = "Bokfört"
                sDanish = "Posteret"
            Case 60089
                sNorwegian = "Innbetalt"
                sEnglish = "Transferred"
                sSwedish = "Inbetalt"
                sDanish = "Indbetalt"
            Case 60090
                sNorwegian = "Betalers navn"
                sEnglish = "Payers name"
                sSwedish = "Betalarens namn"
                sDanish = "Betalers navn"
            Case 60091
                sNorwegian = "Betalingstype"
                sEnglish = "Payment type"
                sSwedish = "Betalningstyp"
                sDanish = "Betalingstype"
            Case 60092
                sNorwegian = "Betalingsdato"
                sEnglish = "Paymentdate"
                sSwedish = "Betalningsdatum"
                sDanish = "Betalingsdato"
            Case 60093
                sNorwegian = "Bankinformasjon"
                sEnglish = "Bank information"
                sSwedish = "Bankinformation"
                sDanish = "Bankinformation"
            Case 60094
                sNorwegian = "Annen informasjon"
                sEnglish = "Other information"
                sSwedish = "Annan information"
                sDanish = "Anden information"
            Case 60095
                sNorwegian = "Foretaksnummer"
                sEnglish = "Enterpriseno."
                sSwedish = "Företagsnummer"
                sDanish = "Virksomhedsnummer"
            Case 60096
                sNorwegian = "Totalbeløp"
                sEnglish = "Total amount"
                sSwedish = "Totalbelopp"
                sDanish = "Totalbeløb"
            Case 60097
                sNorwegian = "Postnummer"
                sEnglish = "Zipcode"
                sSwedish = "Postnummer"
                sDanish = "Postnummer"
            Case 60098
                sNorwegian = "Sted"
                sEnglish = "City"
                sSwedish = "Ort"
                sDanish = "Sted"
            Case 60099
                sNorwegian = "Mottakers landkode"
                sEnglish = "Receivers countrycode"
                sSwedish = "Mottagarens landsnummer"
                sDanish = "Modtagerens landekode"
            Case 60100
                sNorwegian = "Banknavn"
                sEnglish = "Bankname"
                sSwedish = "Banknamn"
                sDanish = "Banknavn"
            Case 60101
                sNorwegian = "SWIFT"
                sEnglish = "SWIFT"
                sSwedish = "SWIFT"
                sDanish = "SWIFT"
            Case 60102
                sNorwegian = "Bankavdeling"
                sEnglish = "Bankbranch"
                sSwedish = "Bankavdelning"
                sDanish = "Bankafdeling"
            Case 60103
                sNorwegian = "Bankens landkode"
                sEnglish = "Banks countrycode"
                sSwedish = "Bankens landsnummer"
                sDanish = "Bankens landekode"
            Case 60104
                sNorwegian = "Betaling med sjekk"
                sEnglish = "Chequepayment"
                sSwedish = "Betalning med check"
                sDanish = "Betaling med check"
            Case 60105
                sNorwegian = "Antall feil:"
                sEnglish = "No of errors:"
                sSwedish = "Antal fel:"
                sDanish = "Antal fejl:"
            Case 60106
                sNorwegian = "Antall advarsler:"
                sEnglish = "No of warnings:"
                sSwedish = "Antal varningar:"
                sDanish = "Antal advarsler:"
            Case 60107
                sNorwegian = "Klient"
                sEnglish = "Client"
                sSwedish = "Klient"
                sDanish = "Klient"
            Case 60108
                sNorwegian = "Profil"
                sEnglish = "Profile"
                sSwedish = "Profil"
                sDanish = "Profil"
            Case 60109
                sNorwegian = "Type fil"
                sEnglish = "Filetype"
                sSwedish = "Typ av fil"
                sDanish = "Type fil"
            Case 60110
                sNorwegian = "Filformat"
                sEnglish = "Fileformat"
                sSwedish = "Filformat"
                sDanish = "Filformat"
            Case 60111
                sNorwegian = "Størrelse"
                sEnglish = "Size"
                sSwedish = "Storlek"
                sDanish = "Størrelse"
            Case 60112
                sNorwegian = "Fra regnskap"
                sEnglish = "From accountingsystem"
                sSwedish = "Från redovisning"
                sDanish = "Fra regnskab"
            Case 60113
                sNorwegian = "Fra bank"
                sEnglish = "From bank"
                sSwedish = "Från bank"
                sDanish = "Fra bank"
            Case 60114
                sNorwegian = "Til regnskap"
                sEnglish = "To accountingsystem"
                sSwedish = "Till redovisning"
                sDanish = "Til regnskab"
            Case 60115
                sNorwegian = "Til bank"
                sEnglish = "To bank"
                sSwedish = "Till bank"
                sDanish = "Til bank"
            Case 60116
                sNorwegian = "Visning av backupfiler"
                sEnglish = "View backupfiles"
                sSwedish = "Visning av säkerhetskopieringsfiler"
                sDanish = "Visning af backupfiler"
            Case 60117
                sNorwegian = "Backupkatalog"
                sEnglish = "Backuppath"
                sSwedish = "Säkerhetskopieringskatalog"
                sDanish = "Backupkatalog"
            Case 60118
                sNorwegian = "Rapport delsummer"
                sEnglish = "Report batches"
                sSwedish = "Rapportdelsummor"
                sDanish = "Rapport delsummer"
            Case 60119
                sNorwegian = "Vil du avslutte uten lagring?"
                sEnglish = "Cancel without saving changes?"
                sSwedish = "Vill du avsluta utan att spara?"
                sDanish = "Vil du afslutte uden at gemme?"
            Case 60120
                sNorwegian = "Mottagende bank"
                sEnglish = "Beneficiary Bank"
                sSwedish = "Mottagande bank"
                sDanish = "Modtagende bank"
            Case 60121
                sNorwegian = "Bank ID (SWIFT, FW, SC, ..)"
                sEnglish = "Bank ID (SWIFT, FedWire, ...)"
                sSwedish = "Bank-ID (SWIFT, FW, SC, ..)"
                sDanish = "Bank ID (SWIFT, FW, SC, ..)"
            Case 60122
                sNorwegian = "ID Type"
                sEnglish = "ID Type"
                sSwedish = "ID-typ"
                sDanish = "ID Type"
            Case 60123
                sNorwegian = "Bank Adr1"
                sEnglish = "Bank Adr1"
                sSwedish = "Bankadr1"
                sDanish = "Bank Adr1"
            Case 60124
                sNorwegian = "Bank Adr2"
                sEnglish = "Bank Adr2"
                sSwedish = "Bankadr2"
                sDanish = "Bank Adr2"
            Case 60125
                sNorwegian = "Bank Adr3"
                sEnglish = "Bank Adr3"
                sSwedish = "Bankadr3"
                sDanish = "Bank Adr3"
            Case 60126
                sNorwegian = "Bank Adr4"
                sEnglish = "Bank Adr4"
                sSwedish = "Bankadr4"
                sDanish = "Bank Adr4"
            Case 60127
                sNorwegian = "Info til betalers bank"
                sEnglish = "Info to payers bank"
                sSwedish = "Info till betalarens bank"
                sDanish = "Info til betalers bank"
            Case 60128
                sNorwegian = "Korrespondentbank"
                sEnglish = "Intermediary Bank (Correspondent bank)"
                sSwedish = "Korrespondentbank"
                sDanish = "Korrespondentbank"
            Case 60129
                sNorwegian = "Valutakurskontrakt"
                sEnglish = "Exchangerate Contract"
                sSwedish = "Valutakurskontrakt"
                sDanish = "Valutakurskontrakt"
            Case 60130
                sNorwegian = "Avtaledato"
                sEnglish = "Contract Date"
                sSwedish = "Avtalsdatum"
                sDanish = "Aftaledato"
            Case 60131
                sNorwegian = "Avtalen gjort med"
                sEnglish = "Deal made with"
                sSwedish = "Avtal med"
                sDanish = "Aftale indgået med"
            Case 60132
                sNorwegian = "Avtalt kurs"
                sEnglish = "Exchangerate Agreed"
                sSwedish = "Avtalad kurs"
                sDanish = "Aftalt kurs"
            Case 60133
                sNorwegian = "Avtalereferanse"
                sEnglish = "Contracts reference"
                sSwedish = "Avtalsreferens"
                sDanish = "Aftalereference"
            Case 60134
                sNorwegian = "Nasjonalbankkode"
                sEnglish = "Statebankcode"
                sSwedish = "Nationalbankkod"
                sDanish = "Nationalbankkode"
            Case 60135
                sNorwegian = "Nasjonalbanktekst"
                sEnglish = "Statebanktext"
                sSwedish = "Nationalbanktext"
                sDanish = "Nationalbanktekst"
            Case 60136
                sNorwegian = "Hastebetaling"
                sEnglish = "Urgent"
                sSwedish = "Expressbetalning"
                sDanish = "Hastebetaling"
            Case 60137
                sNorwegian = "Mottakers landkode"
                sEnglish = "Receivers countrycode"
                sSwedish = "Mottagarens landsnummer"
                sDanish = "Modtagerens landekode"
            Case 60138
                sNorwegian = "Feil/Varsler"
                sEnglish = "Errors/Warnings"
                sSwedish = "Fel/Varningar"
                sDanish = "Fejl/advarsler"
            Case 60139
                sNorwegian = "Innlandsbetaling"
                sEnglish = "Domestic payment"
                sSwedish = "Inlandsbetalning"
                sDanish = "Indlandsbetaling"
            Case 60140
                sNorwegian = "Internasjonal betaling"
                sEnglish = "International payment"
                sSwedish = "Internationell betalning"
                sDanish = "International betaling"
            Case 60141
                sNorwegian = "Massetrans"
                sEnglish = "Masstransfer"
                sSwedish = "Masstrans"
                sDanish = "Massetransaktion"
            Case 60142
                sNorwegian = "Lønnstrans"
                sEnglish = "Salaraytransfer"
                sSwedish = "Lönestrans"
                sDanish = "Løntransaktion"
            Case 60143
                sNorwegian = "Ukjent betalingstype"
                sEnglish = "Unknown paymenttype"
                sSwedish = "Okänd betalningstyp"
                sDanish = "Ukendt betalingstype"
            Case 60144
                sNorwegian = "Info til mottakers bank"
                sEnglish = "Info to benificiary bank"
                sSwedish = "Info till mottagarens bank"
                sDanish = "Info til modtagers bank"
            Case 60145
                sNorwegian = "Betalingsdato er overskredet!"
                sEnglish = "The paymentdate stated is overdue!"
                sSwedish = "Betalningsdatum har överskridits!"
                sDanish = "Betalingsdato er overskredet!"
            Case 60146
                sNorwegian = "Adresse"
                sEnglish = "Address"
                sSwedish = "Adress"
                sDanish = "Adresse"
            Case 60147
                sNorwegian = "Vil du fjerne denne betalingen?"
                sEnglish = "Remove current payment?"
                sSwedish = "Vill du ta bort betalningen?"
                sDanish = "Vil du fjerne denne betaling?"
            Case 60148
                sNorwegian = "Feilmelding:"
                sEnglish = "Error:"
                sSwedish = "Felmeddelande:"
                sDanish = "Fejlmeddelelse:"
            Case 60149
                sNorwegian = "Feltnavn:"
                sEnglish = "Variablename:"
                sSwedish = "Fältnamn:"
                sDanish = "Feltnavn:"
            Case 60150
                sNorwegian = "Opprinnelig verdi:"
                sEnglish = "Initial value:"
                sSwedish = "Ursprungligt värde:"
                sDanish = "Oprindelig værdi:"
            Case 60151
                sNorwegian = "Endret til:"
                sEnglish = "Changed to:"
                sSwedish = "Ändrat till:"
                sDanish = "Ændret til:"
            Case 60152
                sNorwegian = "Importerte filer"
                sEnglish = "Imported files"
                sSwedish = "Importerade filer"
                sDanish = "Importerede filer"
            Case 60153
                sNorwegian = "Pakkseddelnummer:"
                sEnglish = "Despatch Advice Number:"
                sSwedish = "Packsedelsnummer:"
                sDanish = "Følgeseddelnummer:"
            Case 60154
                sNorwegian = "Levereringsnummer:"
                sEnglish = "Delivery Number:"
                sSwedish = "Leveransnummer:"
                sDanish = "Levereringsnummer:"
            Case 60155
                sNorwegian = "Kjøpers ordrenummer:"
                sEnglish = "Buyer's Order Number:"
                sSwedish = "Köparens ordernummer:"
                sDanish = "Købers ordrenummer:"
            Case 60156
                sNorwegian = "Kontraktsnummer:"
                sEnglish = "Contract Number:"
                sSwedish = "Kontraktsnummer:"
                sDanish = "Kontraktnummer:"
            Case 60157
                sNorwegian = "Kundenummer:"
                sEnglish = "Internal Customer Number:"
                sSwedish = "Kundnummer:"
                sDanish = "Kundenummer:"
            Case 60158
                sNorwegian = "Fakturanummer:"
                sEnglish = "Invoice Number:"
                sSwedish = "Fakturanummer:"
                sDanish = "Fakturanummer:"
            Case 60159
                sNorwegian = "Omkostninger egen bank"
                sEnglish = "Charges own bank"
                sSwedish = "Kostnader egen bank"
                sDanish = "Omkostninger egen bank"
            Case 60160
                sNorwegian = "Omkostninger mottakerbank"
                sEnglish = "Charges correspondent bank"
                sSwedish = "Avgifter mottagarbank"
                sDanish = "Omkostninger modtagerbank"
            Case 60161
                sNorwegian = "Fradato (YYYYMMDD)"
                sEnglish = "From date (YYYYMMDD)"
                sSwedish = "Från-datum (ÅÅÅÅMMDD)"
                sDanish = "Fradato (YYYYMMDD)"
            Case 60162
                sNorwegian = "Tildato (YYYYMMDD)"
                sEnglish = "To date (YYYYMMDD)"
                sSwedish = "Till-datum (ÅÅÅÅMMDD)"
                sDanish = "Tildato (YYYYMMDD)"
            Case 60163
                sNorwegian = "Statistikk"
                sEnglish = "Statstics"
                sSwedish = "Statistik"
                sDanish = "Statistik"
            Case 60164
                sNorwegian = "er ingen gyldig dato - format YYYYMMDD"
                sEnglish = "is not a valid date. Format YYYYMMDD"
                sSwedish = "är inget giltigt datum - format ÅÅÅÅMMDD"
                sDanish = "er ingen gyldig dato - format YYYYMMDD"
            Case 60165
                sNorwegian = "Passord"
                sEnglish = "Password"
                sSwedish = "Lösenord"
                sDanish = "Kodeord"
            Case 60166
                sNorwegian = "Finner ingen statistikkposter i datointervallet"
                sEnglish = "Finds not statisticsrecord within given dateinterval"
                sSwedish = "Hittar inga statistikposter i datumintervallet"
                sDanish = "Finder ingen statistikposter i datointervallet"
            Case 60167
                sNorwegian = "Prod.dato"
                sEnglish = "Prod.date"
                sSwedish = "Prod.datum"
                sDanish = "Prod.dato"
            Case 60168
                sNorwegian = "Filen %1 har blitt kopiert"
                sEnglish = "File %1 has been copied"
                sSwedish = "Filen %1 har kopierats"
                sDanish = "Filen %1 er blevet kopieret"
            Case 60169
                sNorwegian = "Rapportoppsett"
                sEnglish = "Reportsetup"
                sSwedish = "Rapportvy"
                sDanish = "Rapportopsætning"
            Case 60170
                sNorwegian = "Rapportoverskrift"
                sEnglish = "Reportheading"
                sSwedish = "Rapportrubrik"
                sDanish = "Rapportoverskrift"
            Case 60171
                sNorwegian = "Sortering"
                sEnglish = "Sorting"
                sSwedish = "Sortering"
                sDanish = "Sortering"
            Case 60172
                sNorwegian = "Vis på skjerm"
                sEnglish = "To screen"
                sSwedish = "Visa på skärm"
                sDanish = "Vis på skærm"
            Case 60173
                sNorwegian = "Til skriver"
                sEnglish = "To print"
                sSwedish = "Till skrivare"
                sDanish = "Til printer"
            Case 60174
                sNorwegian = "Eksport"
                sEnglish = "Export"
                sSwedish = "Export"
                sDanish = "Eksport"
            Case 60175
                sNorwegian = "Filnavn for eksport"
                sEnglish = "Filename for export"
                sSwedish = "Filnamn för export"
                sDanish = "Filnavn for eksport"
            Case 60176
                sNorwegian = "e-Post"
                sEnglish = "e-Mail"
                sSwedish = "E-post"
                sDanish = "e-mail"
            Case 60177
                sNorwegian = "Kan ikke finne mappingfilen %1. Sjekk profiloppsett."
                sEnglish = "Could not find the mappingfile %1. Please check the profilesetup."
                sSwedish = "Kan inte hitta mappningsfilen %1. Kontrollera profilinställningar."
                sDanish = "Kan ikke finde mappingfilen %1. Kontroller profilopsætning."
            Case 60176
                sNorwegian = "SWIFT remburs bank:"
                sEnglish = "SWIFT Interm. bank:"
                sSwedish = "SWIFT mellan bank"
                sDanish = "SWIFT mellem bank"

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 60200

            Case 60200
                sNorwegian = "Vedlagt rapporter produsert av BabelBank fra Visual Banking"
                sEnglish = "Attached reports produced by BabelBank from Visual Banking"
                sSwedish = "Bifogat rapporter genererad av BabelBank från Visual Banking"
                sDanish = "Vedhæftet rapporter produceret av BabelBank fra Visual Banking"
            Case 60201
                sNorwegian = "Klientoversikt"
                sEnglish = "Clients"
                sSwedish = "Klientöversikt"
                sDanish = "Klientoversigt"
            Case 60202
                sNorwegian = "Klientnr:"
                sEnglish = "Clientno:"
                sSwedish = "Klientnr:"
                sDanish = "Klientnr:"
            Case 60203
                sNorwegian = "Framdrift"
                sEnglish = "Progress"
                sSwedish = "Framsteg"
                sDanish = "Fremskridt"
            Case 60204
                sNorwegian = "Funksjon"
                sEnglish = "Task"
                sSwedish = "Funktion"
                sDanish = "Funktion"
            Case 60205
                sNorwegian = "Antall feil:"
                sEnglish = "No of errors:"
                sSwedish = "Antal fel:"
                sDanish = "Antal fejl:"
            Case 60206
                sNorwegian = "Analyse av betalingsfiler"
                sEnglish = "Paymentfile analysis"
                sSwedish = "Analys av betalningsfiler"
                sDanish = "Analyse af betalingsfiler"
            Case 60207
                sNorwegian = "Sjekker CR og LF"
                sEnglish = "Checking CR and LF"
                sSwedish = "Checkar CR och LF"
                sDanish = "Checker CR og LF"
            Case 60208
                sNorwegian = "Sjekker linjelengde"
                sEnglish = "Checking linelength"
                sSwedish = "Checkar linjelängd"
                sDanish = "Checker linjælengde"
            Case 60209
                sNorwegian = "Sjekker om Telepayfil/records er komplette"
                sEnglish = "Checking Telepay complete record/file"
                sSwedish = "Checkar om Telepayfil/records är fullständig"
                sDanish = "Checker om Telepayfil/records er komplet"
            Case 60210
                sNorwegian = "Sjekker karaktersett"
                sEnglish = "Checking characterset"
                sSwedish = "Checkar karaktersett"
                sDanish = "Checker karaktersett"
            Case 60211
                sNorwegian = "Analyse fullført."
                sEnglish = "Analysis completed."
                sSwedish = "Analys slutförts."
                sDanish = "Analyse fuldført."
            Case 60212
                sNorwegian = "Feilen som ble oppdaget gjør det ikke mulig å fortsette analyse av filen."
                sEnglish = "The error found makes it impossible to continue the fileanalysis."
                sSwedish = "Detekterade felet gör det omöjligt att fortsätta analysen av filen."
                sDanish = "Detekteret fejl gør det umuligt at fortsætte analyse af filen."
            Case 60213
                sNorwegian = "Maksimalt antall feil er nådd"
                sEnglish = "Maximum number of errors reached"
                sSwedish = "Maximalt antal fel är nådd"
                sDanish = "Maksimalt antal fejl er nået"
            Case 60214
                sNorwegian = "Faktnr: "
                sEnglish = "Invoice: "
                sSwedish = "Faktnr: "
                sDanish = "Faktnr: "
            Case 60215
                sNorwegian = "Kreditnota: "
                sEnglish = "Creditnote: "
                sSwedish = "Kreditnota: "
                sDanish = "Kreditnota: "
            Case 60216
                sNorwegian = "Er du helt sikker på at du vil kopiere tilbake databasen?"
                sEnglish = "Are you absolutely sure you want to restore the database?"
                sSwedish = "Är du helt säker på att du vill kopiera tillbaka databasen?"
                sDanish = "Er du helt sikker på at du vil kopiere tilbage databasen?"
            Case 60217
                sNorwegian = "Du fjerner da alt som ligger i databasen du nå kjører, og erstatter dette med oppsett fra valgt databasekopi."
                sEnglish = "You will delete all data in the current database, and replace with the setup from the selected databasecopy."
                sSwedish = "Du vill da ta bort allt i databasen du nu körar, och ersättar dette med oppsett från valda databasekopi."
                sDanish = "Du fjerner da alt som befinder sig i databasen du nå kører, og erstatter dette med oppsett fra valgt databasekopi."
            Case 60218
                sNorwegian = "Søk etter"
                sEnglish = "Find "
                sSwedish = "Sök efter"
                sDanish = "Søg efter"
            Case 60219
                sNorwegian = "Kun gjeldende kolonne"
                sEnglish = "Current account only"
                sSwedish = "Endast den aktuella kolonn"
                sDanish = "Kun den aktuelle kolonne"
            Case 60220
                sNorwegian = "Sett filter"
                sEnglish = "Filter on"
                sSwedish = "Set filter"
                sDanish = "Sæt filter"
            Case 60221
                sNorwegian = "Fra starten av felt"
                sEnglish = "From start of text"
                sSwedish = "Från början av fält"
                sDanish = "Fra starten af felt"
            Case 60222
                sNorwegian = "Søk alle"
                sEnglish = "Find all"
                sSwedish = "Sök alla"
                sDanish = "Søg alle"
            Case 60223
                sNorwegian = "Søk neste"
                sEnglish = "Find next"
                sSwedish = "Sök näste"
                sDanish = "Søg næste"
            Case 60224
                sNorwegian = "Sending av mail fullført!"
                sEnglish = "Sending of supportmail completed!"
                sSwedish = "E-postutskicket har slutförts!"
                sDanish = "Afsendelse af mail fuldført!"
            Case 60225
                sNorwegian = "Hele filen avvist (RJCT)"
                sEnglish = "Complete file rejected (RJCT)"
                sSwedish = "Hela filen avvisad (RJCT)"
                sDanish = "Hele filen avvist (RJCT)"
            Case 60226
                sNorwegian = "Feil i verifisering av SecureEnvelope"
                sEnglish = "Error verifying SecureEnvelope"
                sSwedish = "Fel i verifisering av SecureEnvelope"
                sDanish = "Feil i verifisering av SecureEnvelope"

            Case 64000
                sNorwegian = "-------- (Standard text on screen) -----"
                sEnglish = "-------- (Standard text on screen) -----"
                sSwedish = "-------- (Standard text on screen) -----"
                sDanish = "-------- (Standard text on screen) -----"
            Case 64001
                sNorwegian = "Melding:"
                sEnglish = "Message:"
                sSwedish = "Meddelande:"
                sDanish = "Meddelelse:"
            Case 64002
                sNorwegian = "Format:"
                sEnglish = "Format:"
                sSwedish = "Format:"
                sDanish = "Format:"
            Case 64003
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 64004
                sNorwegian = "Linje:"
                sEnglish = "Record:"
                sSwedish = "Rad:"
                sDanish = "Linje:"
            Case 64005
                sNorwegian = "--- bbTextCodeString ---"
                sEnglish = "--- bbTextCodeString ---"
                sSwedish = "--- bbTextCodeString ---"
                sDanish = "--- bbTextCodeString ---"
            Case 64006
                sNorwegian = "Faste oppdrag"
                sEnglish = "Periodic payment"
                sSwedish = "Fasta uppdrag"
                sDanish = "Faste opgaver"
            Case 64007
                sNorwegian = "Husleie"
                sEnglish = "Rent"
                sSwedish = "Hyra"
                sDanish = "Husleje"
            Case 64008
                sNorwegian = "Kontingent"
                sEnglish = "Subscription"
                sSwedish = "Avgifter"
                sDanish = "Kontingent"
            Case 64009
                sNorwegian = "AB Kontrakt"
                sEnglish = "Pay off contract"
                sSwedish = "AB Kontrakt"
                sDanish = "AB Kontrakt"
            Case 64010
                sNorwegian = "Skatt/avgift"
                sEnglish = "Tax"
                sSwedish = "Skatt/avgift"
                sDanish = "Skat/afgift"
            Case 64011
                sNorwegian = "Moms"
                sEnglish = "VAT"
                sSwedish = "Moms"
                sDanish = "Moms"
            Case 64012
                sNorwegian = "Overføring med melding"
                sEnglish = "Transfer with statement"
                sSwedish = "Överföring med meddelande"
                sDanish = "Overførsel med meddelelse"
            Case 64013
                sNorwegian = "Utbet. uten mottakerkonto"
                sEnglish = "Money order"
                sSwedish = "Utbet. utan mottagarkonto"
                sDanish = "Udbet. uden modtagerkonto"
            Case 64014
                sNorwegian = "Statens konsernkonto"
                sEnglish = "Government groupaccount"
                sSwedish = "Statskassan"
                sDanish = "Statens konsernkonto"
            Case 64015
                sNorwegian = "Pensjon"
                sEnglish = "Pension"
                sSwedish = "Pension"
                sDanish = "Pension"
            Case 64016
                sNorwegian = "Barnetrygd"
                sEnglish = "Sosial security, child"
                sSwedish = "Reserver"
                sDanish = "Børnefamilieydelse"
            Case 64017
                sNorwegian = "Lønn"
                sEnglish = "Salary"
                sSwedish = "Lön"
                sDanish = "Løn"
            Case 64018
                sNorwegian = "Hyretrekk"
                sEnglish = "Seaman's pay"
                sSwedish = "Hyresavdrag"
                sDanish = "Løntræk"
            Case 64019
                sNorwegian = "Landbruksoppgjør"
                sEnglish = "Agricultural payment"
                sSwedish = "Lantbruksredovisning"
                sDanish = "Landbrugsafregning"
            Case 64020
                sNorwegian = "Div overføring uten melding"
                sEnglish = "Transer without statement"
                sSwedish = "Div ospecificerade överföringar"
                sDanish = "Div. overførsler uden meddelelse"
            Case 64021
                sNorwegian = "Statens konsernkonto lønn"
                sEnglish = "Government groupaccount, salary"
                sSwedish = "Lön ur statskassan"
                sDanish = "Statens konsernkonto løn"
            Case 64022
                sNorwegian = "Bet. med KID"
                sEnglish = "KID transfer"
                sSwedish = "Bet. med KID"
                sDanish = "Bet. med KID"
            Case 64023
                sNorwegian = "KID med underspesifikasjoner"
                sEnglish = "KID transfer with spesifications"
                sSwedish = "KID med underspecifikationer"
                sDanish = "KID med underspecifikationer"
            Case 64024
                sNorwegian = "Kid og kreditnota"
                sEnglish = "KID transfer with credit advise"
                sSwedish = "KID och kreditnota"
                sDanish = "Kid og kreditnota"
            Case 64025
                sNorwegian = "Visa"
                sEnglish = "Visa"
                sSwedish = "Visa"
                sDanish = "Visa"
            Case 64026
                sNorwegian = "Trans. fra giro belastet konto"
                sEnglish = "Girotransfer from account"
                sSwedish = "Trans. från giro, belastat konto"
                sDanish = "Trans. fra giro belastet konto"
            Case 64027
                sNorwegian = "Trans fra Faste Oppdrag"
                sEnglish = "Transfer from periodic payment"
                sSwedish = "Trans från fasta uppdrag"
                sDanish = "Trans. fra Faste opgaver"
            Case 64028
                sNorwegian = "Trans fra Dir rem"
                sEnglish = "Transfer from Dir.rem."
                sSwedish = "Trans från Dir rem"
                sDanish = "Trans. fra Dir bet"
            Case 64029
                sNorwegian = "Trans fra BTG"
                sEnglish = "Transfer from BTG"
                sSwedish = "Trans från BTG"
                sDanish = "Trans. fra BTG"
            Case 64030
                sNorwegian = "Trans fra skrankegiro"
                sEnglish = "Transfer from giro"
                sSwedish = "Trans från kassagiro"
                sDanish = "Trans. fra giro i bank"
            Case 64031
                sNorwegian = "Trans fra avtalegiro"
                sEnglish = "Transfer from 'Avtalegiro'"
                sSwedish = "Trans från avtalsgiro"
                sDanish = "Trans. fra aftalegiro"
            Case 64032
                sNorwegian = "Trans fra Telegiro"
                sEnglish = "Transfer from Telegiro"
                sSwedish = "Trans från Telegiro"
                sDanish = "Trans. fra Telegiro"
            Case 64033
                sNorwegian = "Trans fra giro - betalt kontant"
                sEnglish = "Transfer from giro, paid cash"
                sSwedish = "Trans från giro - betalt kontant"
                sDanish = "Trans. fra giro - betalt kontant"
            Case 64034
                sNorwegian = "Feil KID"
                sEnglish = "Wrong KID"
                sSwedish = "Fel KID"
                sDanish = "Forkert KID"
            Case 64035
                sNorwegian = "Elektronisk betaling"
                sEnglish = "Electronic payment"
                sSwedish = "Elektronisk betalning"
                sDanish = "Elektronisk betaling"
            Case 64036
                sNorwegian = "Manuell betaling"
                sEnglish = "Manual payment"
                sSwedish = "Manuell betalning"
                sDanish = "Manuel betaling"
            Case 64037
                sNorwegian = "Intern transaksjon"
                sEnglish = "Internal transfer"
                sSwedish = "Intern transaktion"
                sDanish = "Intern transaktion"
            Case 64038
                sNorwegian = "Autogiro"
                sEnglish = "Autogiro"
                sSwedish = "Autogiro"
                sDanish = "Autogiro"
            Case 64039
                sNorwegian = "Hastebetaling"
                sEnglish = "Urgent payment"
                sSwedish = "Expressbetalning"
                sDanish = "Hastebetaling"
            Case 64040
                sNorwegian = "Vent på bekreftelse"
                sEnglish = "Payment on hold"
                sSwedish = "Vänta på bekräftelse"
                sDanish = "Vent på bekræftelse"
            Case 64041
                sNorwegian = "Autogiro med melding"
                sEnglish = "Autogiro with message"
                sSwedish = "Autogiro med meddelande"
                sDanish = "Autogiro med meddelelse"
            Case 64042
                sNorwegian = "Info FBO"
                sEnglish = "Info FBO"
                sSwedish = "Info DD"
                sDanish = "Info dir. debitering"
            Case 64043
                sNorwegian = "Ny FBO"
                sEnglish = "New FBO"
                sSwedish = "Ny DD"
                sDanish = "Ny dir. debitering"
            Case 64044
                sNorwegian = "Slettet FBO"
                sEnglish = "Deleted FBO"
                sSwedish = "Borttagen DD"
                sDanish = "Slettet dir. debitering"
            Case 64045
                sNorwegian = "Endre melding til mottaker"
                sEnglish = "Change message to beneficiary"
                sSwedish = "Ändra meddelande till mottagare"
                sDanish = "Ændr meddelelse til modtager"
            Case 64046
                sNorwegian = "Tekst som kommer med på betaling:"
                sEnglish = "Message to beneficiary:"
                sSwedish = "Text som visas på betalning:"
                sDanish = "Tekst som kommer med på betaling:"
            Case 64047
                sNorwegian = "Tekst som utelates på betaling:"
                sEnglish = "Message that will be removed:"
                sSwedish = "Text som utelämnas på betalning:"
                sDanish = "Tekst som udelades på betaling:"
            Case 64048
                sNorwegian = "Informasjon om betaling:"
                sEnglish = "Payment information:"
                sSwedish = "Information om betalning:"
                sDanish = "Information om betaling:"
            Case 64049
                sNorwegian = "Melding til mottager er for lang. Maksimal lengde er %1 posisjoner." & vbLf & "Overskytende tekst vil ikke sendes mottaker."
                sEnglish = "The message to beneficiary is too long. Maximum length is %1 positions." & vbLf & "Additional text will not be sent to beneficiary."
                sSwedish = "Meddelandet till mottagaren är för långt. Maxlängden är %1 tecken." & vbLf & "Överskottstext skickas inte till mottagaren.	"
                sDanish = "Meddelelse til modtager er for lang. Maksimal længde er %1 tegn." & vbLf & "Overskydende tekst sendes ikke til modtager.	"
            Case 64050
                sNorwegian = "Vennligst velg uttrekksnummeret du ønsker å betale."
                sEnglish = "Please choose the 'Uttrekksnummer' You want to be paid."
                sSwedish = "Välj nummer på utdraget som du vill betala."
                sDanish = "Vælg hvilket ID-nummer du ønsker at betale."
            Case 64051
                sNorwegian = "Uttrekksnummer:"
                sEnglish = "Uttrekksnummer:"
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 64070

                sSwedish = "Utdragsnummer:"
                sDanish = "ID-nummer:"
            Case 64100
                sNorwegian = "--- bbGetTextString ferdig ---"
                sEnglish = "--- bbGetTextString completed ---"
                sDanish = "--- bbGetTextString färdig ---"
                sSwedish = "--- bbGetTextString færdig ---"

                'VBDAL-messages

            Case 70001
                sNorwegian = "70001: Kunne ikke koble til databasen."
                sEnglish = "70001: Couldn't connect to the database."
                sSwedish = "70001: Kunde inte ansluta till databasen."
                sDanish = "70001: Kunne ikke forbinde til databasen."
            Case 70002
                sNorwegian = "Påloggingen til databasen rapporterte følgende feil:"
                sEnglish = "The connection reported the following error:"
                sSwedish = "Inloggingen till databasen rapporterade följande fel:"
                sDanish = "Logon til databasen rapporterte følgende fejl:"
            Case 70003
                sNorwegian = "Feil under oppbygning av påloggingsstreng."
                sEnglish = "Error during the building of the connectionstring."
                sSwedish = "Fel under oppbygning av inloggningssträng."
                sDanish = "Fejl under oppbygning af loginstreng."
            Case 70004
                sNorwegian = "Påloggingsstreng:"
                sEnglish = "Connectionstring:"
                sNorwegian = "Inloggningssträng:"
                sDanish = "Logingstreng:"
            Case 70005
                sNorwegian = "For å teste/endre påloggingsstreng, gå inn på Firmaopplysninger under Oppsett i BabelBank."
                sEnglish = "To test/change the connectionstring, start BabelBank and select Company under Setup in the menu."
                sSwedish = "För att testa/ändra inloggningssträng, gå till Företagsinformation under Oppsett i BabelBank."
                sDanish = "For at teste/ændre logingstreng, gå til Firmaopplysninger under Oppsett i BabelBank."
            Case 70006
                sNorwegian = "70006: Det er ikke angitt en gyldig 'databaseprovider'."
                sEnglish = "70006: No valid databaseprovider is stated."
                sSwedish = "70006: Det är inte angivt en giltig 'databaseprovider'."
                sDanish = "70006: Det er ikke angitt en gyldig 'databaseprovider'."
            Case 70007
                sNorwegian = "Databasefeil."
                sEnglish = "Database-error."
                sSwedish = "Databasefel."
                sDanish = "Databasefejl."
            Case 70008
                sNorwegian = "70008: BabelBank kan ikke hente ut verdien fra felter av typen %1"
                sEnglish = "70008: BabelBank is not able to retrieve fields of type: %1"
                sSwedish = "70008: BabelBank kan inte hente ut värde från fält av typen %1"
                sDanish = "70008: BabelBank kan ikke hente ut værdien fra felter af typen %1"
            Case 70009
                sNorwegian = "70009: Firmanummer er ikke angitt." & vbCrLf & "BabelBank er ikke i stand til å hente ut påloggingsinformasjon til ERP-databasen."
                sEnglish = "70009: No Company_ID stated." & vbCrLf & "BabelBank is not able to retrieve the connection information to the ERP database."
                sSwedish = "70009: Företagsnummer är inte angivet." & vbCrLf & "BabelBank er ikke i stand til å hämta ut inloggningsinformation till ERP-databasen."
                sDanish = "70009: Virksomhednummer er ikke angivet." & vbCrLf & "BabelBank er ikke i stand til at hente ut logingsinformation til ERP-databasen."
            Case 70010
                sNorwegian = "70010: Ingen pålogginger er assosiert med dette firmaet."
                sEnglish = "70010: No connections are associated with this company."
                sSwedish = "70010: Ingen inloggninger är assosiert med detta företag."
                sDanish = "70010: Ingen logins er assosiert med dette firmaet."
            Case 70011
                sNorwegian = "70011: Ingen påloggingsstreng til ERP systemet er angitt."
                sEnglish = "70011: No connectionstring to the ERP system are stated."
                sSwedish = "70011: Ingen inloggningssträng till ERP systemet är angivt."
                sDanish = "70011: Ingen logingsstreng til ERP systemet er angitt."

        End Select

        If bUseDanish Then
            sReturnString = sDanish
        ElseIf bUseSwedish Then
            sReturnString = sSwedish
        ElseIf bUseNorwegian Then
            sReturnString = sNorwegian
        Else
            sReturnString = sEnglish
        End If
        ' If no hit, then use English;
        If EmptyString(sReturnString) Then
            sReturnString = sEnglish
        End If
        ' If still not found
        If EmptyString(sReturnString) Then
            sReturnString = StringIndex & " languagestring is missing."
        End If

        sReturnString = Replace(sReturnString, "%1", Trim$(sString1))
        sReturnString = Replace(sReturnString, "%2", Trim$(sString2))
        sReturnString = Replace(sReturnString, "%3", Trim$(sString3))

        LRS = sReturnString


    End Function

End Module
