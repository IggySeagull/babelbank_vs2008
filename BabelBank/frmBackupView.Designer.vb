<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmBackupView
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cmdCopy As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdView As System.Windows.Forms.Button
	Public WithEvents lblBackupPath As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdCopy = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdView = New System.Windows.Forms.Button
        Me.lblBackupPath = New System.Windows.Forms.Label
        Me.gridFiles = New System.Windows.Forms.DataGridView
        Me.cmdMail = New System.Windows.Forms.Button
        CType(Me.gridFiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdCopy
        '
        Me.cmdCopy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCopy.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCopy.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCopy.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCopy.Location = New System.Drawing.Point(777, 480)
        Me.cmdCopy.Name = "cmdCopy"
        Me.cmdCopy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCopy.Size = New System.Drawing.Size(90, 25)
        Me.cmdCopy.TabIndex = 1
        Me.cmdCopy.Text = "55015 - Copy to original"
        Me.cmdCopy.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(961, 480)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(90, 25)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "55001 - Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdView
        '
        Me.cmdView.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdView.BackColor = System.Drawing.SystemColors.Control
        Me.cmdView.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdView.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdView.Location = New System.Drawing.Point(869, 480)
        Me.cmdView.Name = "cmdView"
        Me.cmdView.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdView.Size = New System.Drawing.Size(90, 25)
        Me.cmdView.TabIndex = 2
        Me.cmdView.Text = "55016 - View"
        Me.cmdView.UseVisualStyleBackColor = False
        '
        'lblBackupPath
        '
        Me.lblBackupPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblBackupPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBackupPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBackupPath.Location = New System.Drawing.Point(192, 58)
        Me.lblBackupPath.Name = "lblBackupPath"
        Me.lblBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBackupPath.Size = New System.Drawing.Size(603, 20)
        Me.lblBackupPath.TabIndex = 3
        Me.lblBackupPath.Text = "Path"
        '
        'gridFiles
        '
        Me.gridFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridFiles.Location = New System.Drawing.Point(195, 82)
        Me.gridFiles.Name = "gridFiles"
        Me.gridFiles.Size = New System.Drawing.Size(850, 382)
        Me.gridFiles.TabIndex = 0
        '
        'cmdMail
        '
        Me.cmdMail.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdMail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMail.Location = New System.Drawing.Point(681, 480)
        Me.cmdMail.Name = "cmdMail"
        Me.cmdMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMail.Size = New System.Drawing.Size(90, 25)
        Me.cmdMail.TabIndex = 5
        Me.cmdMail.Text = "60176 - eMail"
        Me.cmdMail.UseVisualStyleBackColor = False
        '
        'frmBackupView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1064, 514)
        Me.Controls.Add(Me.cmdMail)
        Me.Controls.Add(Me.gridFiles)
        Me.Controls.Add(Me.cmdCopy)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdView)
        Me.Controls.Add(Me.lblBackupPath)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmBackupView"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60116 - View backupfiles"
        CType(Me.gridFiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridFiles As System.Windows.Forms.DataGridView
    Public WithEvents cmdMail As System.Windows.Forms.Button
#End Region 
End Class
