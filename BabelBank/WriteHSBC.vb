﻿Option Strict Off
Option Explicit On
Module WriteHSBC

    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    Dim nBatchDebitAmount As Double
    Dim nBatchCreditAmount As Double
    Dim nFileDebitAmount As Double
    Dim nFileCreditAmount As Double
    Dim nFileAmount As Double
    Dim iNoOfRecords As Double = 0
    Dim sSpecial As String
    Dim iBatchSequence As Integer = 0

    Public Function WriteHSBC_SG_ACH(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sCompanyNo As String, ByVal sBranch As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sNewOwnref As String
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim lCounter As Long
        Dim iProcessNumber As Integer = 0

        ' lag en outputfil
        Try


            ' We need to do the whole process two times
            ' First time just for counting number of lines to be exported,and use that number in the header
            ' Second time, do the export
            For iProcessNumber = 1 To 2
                If iProcessNumber = 2 Then
                    ' reset all exported marks from run 1
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = False
                        Next
                    Next
                End If

                oGroupPayment = New vbBabel.GroupPayments
                oGroupPayment.GroupCrossBorderPayments = True
                oGroupPayment.SetGroupingParameters(True, True, False, True, True, True)

                If iProcessNumber = 2 Then
                    oFs = New Scripting.FileSystemObject
                    oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
                End If

                For Each oBabel In oBabelFiles
                    bExportoBabel = False
                    sSpecial = oBabel.Special

                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then
                        If Not bFileStartWritten Then
                            ' write only once!
                            ' When merging Client-files, skip this one except for first client!
                            sLine = WriteHSBC_SG_ACH_FileStart(sCompanyNo, sBranch)
                            If iProcessNumber = 2 Then
                                oFile.WriteLine(sLine)
                                bFileStartWritten = True
                            Else
                                iNoOfRecords = iNoOfRecords + 1
                            End If
                        End If

                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If 'If oPayment.Cancel = False Then
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                For lCounter = 1 To oBatch.Payments.Count

                                    oGroupPayment.Batch = oBatch
                                    oGroupPayment.MarkIdenticalPayments(True)

                                    sLine = WriteHSBC_SG_ACH_BatchStart(oBabel, oBabel.Batches(1))
                                    If iProcessNumber = 2 Then
                                        oFile.WriteLine(sLine)
                                    Else
                                        iNoOfRecords = iNoOfRecords + 1
                                    End If

                                    For Each oPayment In oBatch.Payments
                                        bExportoPayment = False
                                        'Have to go through the payment-object to see if we have objects to
                                        ' be exported to this exportfile
                                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                            'Don't export payments that have been cancelled
                                            If oPayment.Cancel = False Then
                                                If Not bMultiFiles Then
                                                    bExportoPayment = True
                                                Else
                                                    If oPayment.I_Account = sI_Account Then
                                                        If InStr(oPayment.REF_Own, "&?") Then
                                                            'Set in the part of the OwnRef that BabelBank is using
                                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                        Else
                                                            sNewOwnref = ""
                                                        End If
                                                        If sNewOwnref = sOwnRef Then
                                                            bExportoPayment = True
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                oPayment.Exported = True
                                            End If 'If oPayment.Cancel = False Then
                                        End If

                                        If bExportoPayment Then
                                            If oPayment.SpecialMark Then
                                                'OK, export the payment
                                            Else
                                                bExportoPayment = False
                                            End If
                                        End If

                                        If bExportoPayment Then
                                            For Each oInvoice In oPayment.Invoices
                                                sLine = WriteHSBC_SG_ACH_Payment(oPayment, oInvoice)
                                                If iProcessNumber = 2 Then
                                                    oFile.WriteLine(sLine)
                                                Else
                                                    iNoOfRecords = iNoOfRecords + 1
                                                End If
                                            Next oInvoice
                                            oPayment.Exported = True
                                        End If
                                    Next oPayment ' payment

                                    oGroupPayment.RemoveSpecialMark()
                                    If oGroupPayment.AllPaymentsMarkedAsExported Then
                                        Exit For
                                    End If

                                Next lCounter
                            End If

                        Next oBatch 'batch
                    End If
                Next oBabel 'Babelfile

                If Not oGroupPayment Is Nothing Then
                    oGroupPayment = Nothing
                End If
            Next iProcessNumber

        Catch ex As Exception

            If Not oGroupPayment Is Nothing Then
                oGroupPayment = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteHSBC_SG_ACH" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteHSBC_SG_ACH = True

    End Function
    Private Function WriteHSBC_SG_ACH_FileStart(ByRef sCompanyNo As String, ByRef sBranch As String) As String
        Dim sLine As String

        sLine = "IFH,"              '1. Recordtype
        sLine = sLine & "IFILE,"    '2. File Format
        sLine = sLine & "CSV,"      '3. File Type

        sLine = sLine & sCompanyNo & ","    '4. HSBC Connect Customer ID
        sLine = sLine & sBranch & ","       '5. HSBCnet Customer ID
        sLine = sLine & Format(Today, "yyyyMMdd") & Format(TimeOfDay, "hhmmss") & "," ' 6. File reference (unique)
        sLine = sLine & Year(Now) & "/" & PadLeft(Month(Now), 2, "0") & "/" & PadLeft(Microsoft.VisualBasic.DateAndTime.Day(Now), 2, "0") & ","   'Format(Today, "yyyy/MM/dd") & ","  '7 File creation date CCYY/MM/DD
        sLine = sLine & Format(TimeOfDay, "hh:mm:ss") & ","  '8 File Creation Time hh:mm:ss

        sLine = sLine & "P," ' 9. Authorization Type
        sLine = sLine & "1.0," ' 10. File Version

        sLine = sLine & iNoOfRecords.ToString & ","   '11. Record Count 

        WriteHSBC_SG_ACH_FileStart = sLine

    End Function
    Function WriteHSBC_SG_ACH_BatchStart(ByRef oBabel As BabelFile, ByRef oBatch As Batch) As String
        ' Bunthode record

        Dim sLine As String = ""
        Dim sName As String = ""
        Dim lNoOfRecordsInBatch As Long = 0
        Dim oTmpPayment As Payment

        iBatchSequence = iBatchSequence + 1

        sLine = "BATHDR,"           ' 1. Record Type
        sLine = sLine & "ACH-CR,"    ' 2. Instruction Type
        ' Find no of records in this batch;
        ' need to find first payment in this "batch" that will be exported;
        For Each oTmpPayment In oBatch.Payments
            If oTmpPayment.SpecialMark Then
                lNoOfRecordsInBatch = lNoOfRecordsInBatch + 1
            End If
        Next oTmpPayment

        sLine = sLine & lNoOfRecordsInBatch.ToString & "," '3. Total # of instructions

        sLine = sLine & ",,,,,," ' 4. - 9. Not used
        sLine = sLine & "@1ST@," ' 10. Constant Eye Catcher

        ' need to find first payment in this "batch" that will be exported;
        For Each oTmpPayment In oBatch.Payments
            If oTmpPayment.SpecialMark Then
                'OK, use this one
                Exit For
            End If
        Next oTmpPayment
        sLine = sLine & oTmpPayment.DATE_Payment & ","  '11. Payment Initiation Date, YYYYMMDD
        sLine = sLine & oTmpPayment.I_Account & ","  '12. First Party Account
        sLine = sLine & oTmpPayment.MON_InvoiceCurrency & ","  '13. Transaction Currency

        sLine = sLine & ",,,,," ' 14. - 18. Not used
        sLine = sLine & "," ' 19. 1st Party A/C Currency, Not used
        sLine = sLine & "," ' 20. Payment Amt in DR A/C

        If IsThisVismaIntegrator() Then
            sName = oBabel.Visma_ISO20022InitgNm
        Else
            sName = oBatch.VB_Profile.CompanyName
        End If
        sLine = sLine & sName & "," ' 21. First party name
        sLine = sLine & ",,,," ' 22. - 25. Not used
        sLine = sLine & Right(Microsoft.VisualBasic.DateAndTime.Day(Now).ToString, 1) & PadLeft(iBatchSequence.ToString, 2, "0") & "," ' 26. Payment Code - Must be unique pr batch within 3 days
        If Not EmptyString(oBatch.REF_Own) Then
            sLine = sLine & Left(oBatch.REF_Own, 24) & ","  '27. Reference Line 1
        Else
            sLine = sLine & Format(Today, "yyyyMMdd") & Format(TimeOfDay, "hhmmss") & ","
        End If

        sLine = sLine & "," ' 28. Reference Line 2

        WriteHSBC_SG_ACH_BatchStart = sLine

    End Function
    Private Function WriteHSBC_SG_ACH_Payment(ByRef oPayment As Payment, ByVal oInvoice As Invoice) As String

        Dim sLine As String

        sLine = "SEQPTY,"        ' 1. Record Type
        sLine = sLine & oPayment.E_Account.Trim & "," ' 2. 2nd Party Account Number
        sLine = sLine & oPayment.E_Name.Trim & "," ' 3. 2nd Party Name
        If Not EmptyString(oInvoice.CustomerNo) Then
            sLine = sLine & Left(oInvoice.CustomerNo, 12) & "," ' 4. 2nd Party Identifier
        Else
            sLine = sLine & oPayment.E_Account.Trim & "," ' 4. 2nd Party Identifier
        End If
        sLine = sLine & Left(oPayment.BANK_BranchNo, 4) & "," ' 5. Beneficiary Bank Number/ID
        sLine = sLine & ","                             ' 6. Beneficiary Branch Number, not used
        sLine = sLine & ","                             ' 7. Transaction Code, not used

        sLine = sLine & ConvertToAmount(oPayment.MON_InvoiceAmount, "", ".") & ","   ' 8 2nd Party Amount
        sLine = sLine & ","                             ' 9. Entry Value Date, not used

        If Not EmptyString(oInvoice.InvoiceNo) Then
            sLine = sLine & Left(oInvoice.InvoiceNo, 12) & ","  '10. 2nd Party Reference
        Else
            If oInvoice.Freetexts.Count > 0 Then
                sLine = sLine & Left(oInvoice.Freetexts(1).Text, 12) & ","  '10. 2nd Party Reference
            Else
                sLine = sLine & Left(oPayment.REF_EndToEnd, 12) & ","  '10. 2nd Party Reference
            End If
        End If

        sLine = sLine & ",,,," ' 11. - 14. Not used
        sLine = sLine & "N," ' 15. Advice Indicator
        sLine = sLine & "N" ' 16. Withholding Tax Indicator


        WriteHSBC_SG_ACH_Payment = sLine

    End Function
End Module
