Option Strict Off
Option Explicit On
Module WriteTBIXML
    Dim oFs As Scripting.FileSystemObject
    'Private docXMLExport As New MSXML2.DOMDocument40
    'Private docXMLSchema As New MSXML2.DOMDocument40
    ' changed next 2 lines 11.02.2011, beacuse it bombs later on. Added New later on
    'Private docXMLExport As MSXML2.DOMDocument40
    'Private docXMLSchema As MSXML2.DOMDocument40

    Private aErrorArray(5, 0) As Object
	'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
	'DOTNETT REDIM Private aErrorArray() As Variant
	Dim oBabel As vbBabel.BabelFile
	Dim oBatch As vbBabel.Batch
    Private oPayment As vbBabel.Payment
	Dim oInvoice As vbBabel.Invoice
	Dim oFreeText As vbBabel.Freetext
	Dim lBabelIndex As Integer
	Dim lBatchIndex As Integer
	Dim lPaymentIndex As Integer
	Dim lInvoiceIndex As Integer
	Dim sPaymentType As String
	Dim bUseFrmCorrect As Boolean
	Dim bShowJustErrors As Boolean
    ' XNET 21.08.2012
    Dim bShowAllPaymentsInfrmCorrect As Boolean
    ' XNET 31.01.2013
    Dim bEditAllInfrmCorrect As Boolean

	Public Function WriteTBIXMLFile(ByRef oImportObject As vbBabel.BabelFiles, ByRef sOutputPath As String, ByRef sVB_Version As String, ByRef sFormat As String) As Boolean
        ' 04.03.2016 added several places  "or sFormat = "PAIN.001 (ISO 20022)" Then	"
		
		Dim oFilesetup As vbBabel.FileSetup
		Dim oFileSetupIn As vbBabel.FileSetup
		Dim sSWIFTAddr As String
        Dim bPaymentOK, bFirstValidation As Boolean
        Dim oCorrect As New vbBabel.Correct
		Dim bCorrectFinished As Boolean
		Dim bExportThePayment, bJustWarnings As Boolean
		Dim lArrayCounter As Integer
		Dim bFirstPayment As Boolean
        Dim sCountryCode, sErrorString As String
		Dim bx As Boolean
		
        'Dim newEle As MSXML2.IXMLDOMElement
        'Dim pi As MSXML2.IXMLDOMProcessingInstruction
        'Dim nodTemp As MSXML2.IXMLDOMNode
        'Dim nodPaymentsSeqList As MSXML2.IXMLDOMNodeList
        'Dim nodPaymentSeqList As MSXML2.IXMLDOMNodeList
        'Dim nodDetailsSeqList As MSXML2.IXMLDOMNodeList
        'Dim nodRemovedNode As MSXML2.IXMLDOMNode
        Dim bNewPayment, bNewPayments, bNewDetails As Boolean
        Dim lNodeListCounter As Integer
		Dim sOldAccountNo As String
		Dim sMandatory As String
		' M = Mandatory
		' O = Optional
		' N = Not Applicable
		
        'Dim nodPaymentsNode As MSXML2.IXMLDOMElement
        'Dim nodPaymentNode As MSXML2.IXMLDOMElement
        'Dim nodDetailsNode As MSXML2.IXMLDOMElement
		Dim sVariableName As String
		Dim lVariableLength As Integer
		
        'Dim nodPaymentsList As MSXML2.IXMLDOMNodeList
        'Dim nodPaymentDetailsList As MSXML2.IXMLDOMNodeList
        Dim lCounter As Integer
		
        'Dim xmlschema As MSXML2.XMLSchemaCache40
		Dim lNoOfPaymentsWritten As Integer
		Dim iNoOfFilesWritten As Short
        'Dim iDotPositionInOutputPath As Short
        'Dim sNewFilename As String
		
		
		Dim bReturnValue As Boolean
		Dim sTmp As String
		
		Dim dFirstValidPaymentDate As Date
		
		'Variables used with FI
		Dim sKortartKode As String
		Dim sBetalerIdent As String
		Dim sKreditorNo As String
		
		'Variables used with ACH and Salary, to check if we have to create a new Payment
		Dim bFirstMass As Boolean
		Dim sOldCurrencyCode As String
		Dim sOldFromAccount As String
		Dim sOldPayersReference As String
		Dim sOldPaymentDate As String
		Dim sOldUrgent As String
		
        '	Dim lZero As Integer
		
		Dim aNBCodesArray() As String
		Dim bNBCodeFound As Boolean
		' added 29.07.2010
        Dim sDebitAccount As String = ""
        Dim sAccountCurrencyCode As String = ""
		Dim oClient As vbBabel.Client
		Dim oaccount As vbBabel.Account
        Dim oTmpFileSetup As vbBabel.FileSetup

        Dim aNodeListArray() As String
		'---
        ' 11.01.2011 Already dimmed at the top of this module !!!! Dim aErrorArray(5, 0) As Object
		bReturnValue = True
		
        'On Error GoTo err_Renamed
        Try



            sTmp = ""
            sOldAccountNo = ""
            bNewPayments = True
            bFirstPayment = True
            bFirstMass = True
            bNewPayment = True
            bNewDetails = True
            lBabelIndex = 0
            lNoOfPaymentsWritten = 0
            iNoOfFilesWritten = 0
            bExportThePayment = False

            'Retrieve valid Statebankcodes for Norway.
            aNBCodesArray = LoadNBCodes(True, False)
            ' XNET 21.08.2012
            bShowAllPaymentsInfrmCorrect = False

            For Each oBabel In oImportObject
                'Detect if we shall use formcorrect or not, and how
                If oBabel.VB_ProfileInUse Then
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                If oPayment.VB_FilenameOut_ID = oFilesetup.FileSetup_ID Then
                                    Select Case oFilesetup.ShowCorrections
                                        Case 0
                                            bUseFrmCorrect = False
                                            bShowJustErrors = True
                                        Case 1
                                            bUseFrmCorrect = True
                                            bShowJustErrors = True
                                        Case 2
                                            bUseFrmCorrect = True
                                            bShowJustErrors = False
                                            ' XNET 21.08.2012 Added new option; Show all payments in frmCorrect
                                        Case 3
                                            bUseFrmCorrect = True
                                            bShowJustErrors = True
                                            bShowAllPaymentsInfrmCorrect = True
                                    End Select

                                    ' XNET 31.01.2013
                                    ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                                    ' This is true if ofilesetup.splitafter450 is true
                                    bEditAllInfrmCorrect = oFilesetup.SplitAfter450

                                    Exit For
                                End If
                            Next oFilesetup
                            If Not oFilesetup Is Nothing Then
                                For Each oFileSetupIn In oBabel.VB_Profile.FileSetups
                                    If oFilesetup.FileSetupOut = oFileSetupIn.FileSetup_ID Then
                                        Exit For
                                    End If
                                Next oFileSetupIn
                            End If
                            '07.07.2008
                            If Not oFilesetup Is Nothing Then
                                Exit For
                            End If
                        Next oPayment
                        '07.07.2008
                        If Not oFilesetup Is Nothing Then
                            Exit For
                        End If
                    Next oBatch
                    If Not oPayment Is Nothing Then
                        'UPGRADE_NOTE: Object oPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        oPayment = Nothing
                    End If
                    If Not oBatch Is Nothing Then
                        'UPGRADE_NOTE: Object oBatch may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        oBatch = Nothing
                    End If
                End If
                lBabelIndex = lBabelIndex + 1
                lBatchIndex = 0
                For Each oBatch In oBabel.Batches
                    lBatchIndex = lBatchIndex + 1
                    lPaymentIndex = 0
                    For Each oPayment In oBatch.Payments
                        bFirstValidation = True
                        bPaymentOK = False
                        lPaymentIndex = lPaymentIndex + 1


                        ' 18.05.2015 
                        ' Commented lines, because we ALWAYS has TELEPAYPLUS !!
                        ' ------------------------------------------------------

                        '' Not allowed to export 0-transactions to  TBI
                        '' Check for, and remove, invoices with 0-amount
                        '' XNET 20.12.2010 added next If, keep 0-amounts for TelepayPlus
                        'If sFormat <> "TELEPAYPLUS" Then
                        '    lZero = RemoveZeroAmounts(oPayment)
                        'End If

                        Do Until bPaymentOK
                            bPaymentOK = True
                            lInvoiceIndex = 0

    
                            '        Case "TELEPAYPLUS"
                            ' added 03.02.2009
                            ' export all payments
                            bExportThePayment = True
                            sPaymentType = oPayment.DnBNORTBIPayType

                            'End Select

                            ' XNET 14.12.2010 - added next If,
                            ' to avoid exporting Gb or FI or DE when set to false in setup
                            ' XNET 14.01.2011 - tatt vekk denne testen, for at vi kan verifisere ogs� TP+ postene
                            'If oPayment.ExtraD1 = "TP+" Then
                            ' bExportThePayment = False
                            'End If

                            sCountryCode = oPayment.I_CountryCode
                            sSWIFTAddr = oPayment.BANK_I_SWIFTCode

                            If bExportThePayment Then

                                '"AMOUNT""AMOUNTDETAILS"

                                For Each oInvoice In oPayment.Invoices
                                    lInvoiceIndex = lInvoiceIndex + 1

                                    ' Not in use for TelepayPlus
                                    'Select Case lZero
                                    '    Case -3
                                    '        ' Error from RemoveZeroAmounts
                                    '        bx = AddToErrorArray("AMOUNT", LRS(15082), "1")
                                    '    Case -2
                                    '        ' Only one invoice in payment, and zero-amount
                                    '        bx = AddToErrorArray("AMOUNT", LRS(15083, "AMOUNT"), "1")
                                    '    Case -1
                                    '        ' No invoices in payment
                                    '        bx = AddToErrorArray("AMOUNT", LRS(15084, "AMOUNT"), "1")
                                    'End Select

                                    bNewPayment = True
                                    bNewDetails = True
                                    If bNewPayments Then
                                        'Check if the import-format is Telepay,
                                        If oBabel.ImportFormat = vbBabel.BabelFiles.FileType.Telepay Then
                                            If Len(oBabel.IDENT_Sender) > 10 Then
                                                oBabel.IDENT_Sender = Right(Trim(oBabel.IDENT_Sender), 10)
                                            End If
                                        End If
                                        '                            nTotalPaymentsAmount = 0
                                        '                            lTotalNumberOfPayments = 0
                                        bNewPayments = False
                                        'Add information to the Payments-level.

                                        '-------------------
                                        '  vi m� gj�re om prinsippene her, til ikke � bruke noe docxml og schema,
                                        'til isteden � g� gjennom felt for felt uten case select under
                                        ' Lag en array av felter ??????????????????????????????????????????????????????????????



                                        ' First Check Enterprisenumber, only done once pr file
                                        '"ENTERPRISENUMBER"
                                        '-----------------------
                                        If oBabel.VB_ProfileInUse Then
                                            ' Added funcionlaity to have different EnterpriseNo for each profile
                                            ' used at least for Singapore
                                            If Not EmptyString(oFilesetup.CompanyNo) Then
                                                sTmp = Trim(oFilesetup.CompanyNo)
                                            Else
                                                ' (before 10.02.06 did always use this (oBabel.VB_Profile.CompanyNo)
                                                sTmp = Trim(oBabel.VB_Profile.CompanyNo)
                                            End If
                                            oBabel.IDENT_Sender = sTmp
                                        End If

                                        ' added 15.04.2009
                                        ' if paymenttype from setup is "US General/ACH" (USGENACH) and Telepay2 then
                                        ' pick Enterprise no from Telepayfile
                                        If (Trim(oFilesetup.PaymentType) = "USGENACH" Or Trim(oFileSetupIn.PaymentType) = "USGENACH") And (oBabel.ImportFormat = vbBabel.BabelFiles.FileType.Telepay Or oBabel.ImportFormat = vbBabel.BabelFiles.FileType.Telepay2) Then
                                            sTmp = Trim(oBatch.I_EnterpriseNo)
                                            oBabel.IDENT_Sender = sTmp
                                        End If
                                        ' Special for PGSNO for Salaries in UK, and PGS Arrow in UK
                                        ' 02.07.2009 also added PGS_TBI_UK for PGS MTEM
                                        If oBabel.Special = "PGSNO_UK_SALARY" Or oBabel.Special = "PGS_ARROW" Or oBabel.Special = "PGS_TBI_UK" Then
                                            sTmp = Trim(oBabel.VB_Profile.AdditionalNo)
                                        End If

                                        ' EnterpriseNo always Mandatory
                                        sMandatory = "M"
                                        If Len(sTmp) = 0 Then
                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15061, "ENTERPRISENUMBER"), "1")
                                            'sVariableName is mandatory, but the variable has no value."
                                        End If
                                        bNewPayment = True

                                    End If 'bNewPayments


                                    '--------------

                                    'Check first if we have to create a new payment for ACH and salary.
                                    Select Case sPaymentType
                                        'Case "GEN", "FIK", "REF"
                                        ' 16.03.2009 added local paymenttypes
                                        Case "GEN", "FIK", "REF", "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                            bFirstMass = True
                                            ' 05.10.05; Endret flere steder fra Now() til Date pga at now testet ogs� klokkeslett
                                            'If IsHoliday(Today, Mid(sSWIFTAddr, 5, 2), True) Then
                                            If IsHoliday(Date.Today, Mid(sSWIFTAddr, 5, 2), True) Then
                                                dFirstValidPaymentDate = GetBankday(Date.Today, Mid(sSWIFTAddr, 5, 2), 1)
                                            Else
                                                dFirstValidPaymentDate = Date.Today
                                            End If

                                        Case "SAL", "ACH"

                                            If Mid(sSWIFTAddr, 5, 2) = "GB" And sPaymentType = "ACH" Then
                                                ' 05.10.05; Endret flere steder fra Now() til Date pga at now testet ogs� klokkeslett
                                                If IsHoliday(Date.Today, "GB", True) Then
                                                    dFirstValidPaymentDate = GetBankday(Date.Today, "GB", 3)
                                                Else
                                                    dFirstValidPaymentDate = GetBankday(Date.Today, "GB", 2)
                                                End If
                                            Else
                                                If IsHoliday(Date.Today, Mid(sSWIFTAddr, 5, 2), True) Then
                                                    'dFirstValidPaymentDate = GetBankday(Now(), Mid$(sSWIFTAddr, 5, 2), 1)
                                                    dFirstValidPaymentDate = GetBankday(Date.Today, Mid(sSWIFTAddr, 5, 2), 1)
                                                Else
                                                    dFirstValidPaymentDate = Date.Today 'Now() Endret 05.10.05
                                                End If
                                            End If

                                            If StringToDate((oPayment.DATE_Payment)) < dFirstValidPaymentDate Then
                                                If bFirstValidation And Not bShowJustErrors Then
                                                    sTmp = oPayment.DATE_Payment
                                                Else
                                                    sTmp = DateToString(dFirstValidPaymentDate)
                                                End If
                                            Else
                                                If IsHoliday(StringToDate((oPayment.DATE_Payment)), Mid(sSWIFTAddr, 5, 2), True) Then
                                                    sTmp = DateToString(GetBankday(StringToDate((oPayment.DATE_Payment)), Mid(sSWIFTAddr, 5, 2), 1))
                                                Else
                                                    sTmp = oPayment.DATE_Payment
                                                End If
                                            End If

                                            If bFirstMass = True Then
                                                bFirstMass = False
                                                bNewPayment = True
                                            Else
                                                If sOldCurrencyCode = oPayment.MON_InvoiceCurrency Then
                                                    If sOldFromAccount = oPayment.I_Account Then
                  
                                                        If sOldPaymentDate = sTmp Then
                                                            If sOldUrgent = Str(oPayment.Priority) Then
                                                                bNewPayment = False
                                                            End If
                                                        Else
                                                            bNewPayment = True
                                                        End If
                                                    Else
                                                        bNewPayment = True
                                                    End If
                                                Else
                                                    bNewPayment = True
                                                End If
                                                'Set the new Old... values
                                            End If
                                    End Select

                                    If bNewPayment Then

                                        bNewPayment = False
                                        'newEle = docXMLExport.createElement("Payment")
                                        'nodPaymentsNode.appendChild(newEle)
                                        'nodPaymentNode = newEle
                                        lNoOfPaymentsWritten = lNoOfPaymentsWritten + 1
                                        'Add information to the Payment-level.

                                        ' Legger inn alle variabler vi skal teste p� i en array - lik nodelisten vi hadde fra gamle TBI-schemafilen

                                        ReDim aNodeListArray(25)
                                        aNodelistArray(0) = "VALUEDATE"
                                        aNodelistArray(1) = "AMOUNT"
                                        aNodelistArray(2) = "AMOUNTCHARGED"
                                        aNodelistArray(3) = "AMOUNTTRANSFERRED"
                                        aNodelistArray(4) = "BANKREFERENCE"
                                        aNodelistArray(5) = "CHARGESCORRBANK"
                                        aNodelistArray(6) = "CHARGESDEBITBANK"
                                        aNodelistArray(7) = "CHECK"
                                        aNodelistArray(8) = "CURRENCYCODE"
                                        aNodelistArray(9) = "EXCHANGEDATE"
                                        aNodelistArray(10) = "EXCHANGEDEALER"
                                        aNodelistArray(11) = "EXCHANGERATEACTUAL"
                                        aNodelistArray(12) = "EXCHANGERATEAGREED"
                                        aNodelistArray(13) = "EXCHANGEREF"
                                        aNodelistArray(14) = "FROMACCOUNT"
                                        aNodelistArray(15) = "FROMACCOUNTSWIFTADDR"
                                        aNodelistArray(16) = "INFOTODNB"
                                        aNodelistArray(17) = "NATIONALCODE"
                                        aNodelistArray(18) = "NATIONALNARR"
                                        aNodelistArray(19) = "ORDERINGCUSTOMER"
                                        aNodelistArray(20) = "PAYERSREFERENCE"
                                        aNodelistArray(21) = "PAYMENTDATE"
                                        aNodelistArray(22) = "PAYMENTDETAILS"
                                        aNodelistArray(23) = "PAYMENTTYPE"
                                        aNodelistArray(24) = "TBIWREFERENCE"
                                        aNodelistArray(25) = "URGENT"
                                        sMandatory = ""

                                        'For lNodeListCounter = 0 To nodPaymentSeqList.length - 1
                                        For lNodeListCounter = 0 To aNodeListArray.GetUpperBound(0)
                                            'sVariableName = nodPaymentSeqList.item(lNodeListCounter).attributes.getNamedItem("name").nodeValue
                                            sVariableName = aNodeListArray(lNodeListCounter)
                                            Select Case UCase(sVariableName)

                                                Case "AMOUNT"
                                                    If oPayment.MON_InvoiceAmount <> 0 Then
                                                        'sTmp = Left$(oPayment.MON_InvoiceAmount, Len(oPayment.MON_InvoiceAmount) - 2) & "." & Right(oPayment.MON_InvoiceAmount, 2)
                                                        sTmp = Replace(VB6.Format(oPayment.MON_InvoiceAmount / 100, "##0.00"), ",", ".")
                                                    Else
                                                        sTmp = "0.00"
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                    End Select

                                                Case "AMOUNTCHARGED"
                                                    sMandatory = "N"

                                                Case "AMOUNTTRANSFERRED"
                                                    sMandatory = "N"

                                                Case "BANKREFERENCE"
                                                    sTmp = oPayment.REF_Bank1
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "N"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "N"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "CHARGESCORRBANK"
                                                    ' Changed 13.10.03 (jps/ki)
                                                    If oPayment.MON_ChargeMeAbroad = True Then
                                                        'sTmp = "0"
                                                        sTmp = "1"
                                                    Else
                                                        'sTmp = "1"
                                                        sTmp = "0"
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"

                                                            ' added 08.09.2009
                                                            ' For SEPA-payments, there have to be shared charges
                                                            ' Must then have ChargesCorrbank = 0, that is
                                                            ' oPayment.MON_ChargeMeAbroad = False
                                                            If Not SEPA_Charges(oPayment, (oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad)) Then

                                                                ' change ChargesCorrBank
                                                                ' 28.09.2009 - Do NOT change, as requested by DnBNOR
                                                                'oPayment.MON_ChargeMeAbroad = False
                                                                'sTmp = "0"
                                                                ' Warn that we have changed charges !
                                                                ' 28.09.2009 Errormark, requested by DnBNOR
                                                                'bx = AddToErrorArray(UCase(sVariableName), LRS(15100), "0")
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15100), "1")
                                                            End If

                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sTmp = CStr(0)
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sTmp = "0" ' added 30.04.2010, always shared charges for local
                                                            sMandatory = "M"
                                                    End Select

                                                Case "CHARGESDEBITBANK"
                                                    ' Changed 13.10.03 (jps/ki)
                                                    If oPayment.MON_ChargeMeDomestic = True Then
                                                        'sTmp = "0"
                                                        sTmp = "1"
                                                    Else
                                                        'sTmp = "1"
                                                        sTmp = "0"
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"

                                                            ' added 08.09.2009
                                                            ' For SEPA-payments, there have to be shared charges
                                                            ' Must then have ChargesCorrbank = 0, that is
                                                            ' oPayment.MON_ChargeMeAbroad = False
                                                            If Not SEPA_Charges(oPayment, (oPayment.MON_ChargeMeDomestic), (oPayment.MON_ChargeMeAbroad)) Then
                                                                ' change ChargesCorrBank
                                                                ' 28.09.2009 - Do NOT change, as requested by DnBNOR
                                                                'oPayment.MON_ChargeMeDomestic = True
                                                                'sTmp = "1"
                                                                ' Warn that we have changed charges !
                                                                ' 28.09.2009 Errormark, requested by DnBNOR
                                                                'bx = AddToErrorArray(UCase(sVariableName), LRS(15100), "0")
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15100), "1")
                                                            End If

                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sTmp = "1"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sTmp = "1" ' added 30.04.2010, always shared charges for local
                                                            sMandatory = "M"
                                                    End Select

                                                Case "CHECK"
                                                    If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.noCheque Then
                                                        sTmp = "0"
                                                    Else
                                                        sTmp = "1"
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "N"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "N"
                                                    End Select

                                                Case "CURRENCYCODE"
                                                    sTmp = oPayment.MON_InvoiceCurrency
                                                    'sTmp = "DDD"
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            If sTmp <> "EUR" Then
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15063), "1")
                                                                '"A Reference payment must be in Euro"
                                                            End If
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                            ' added 29.07.2010
                                                            ' Must check currency of payment against currency of debitaccount
                                                            ' Must always be the same currency for locals !
                                                            If sDebitAccount <> oPayment.I_Account Then
                                                                sDebitAccount = oPayment.I_Account
                                                                For Each oTmpFileSetup In oPayment.VB_Profile.FileSetups
                                                                    If oPayment.VB_FilenameOut_ID = oTmpFileSetup.FileSetup_ID Then
                                                                        For Each oClient In oTmpFileSetup.Clients
                                                                            For Each oaccount In oClient.Accounts
                                                                                If Trim(oaccount.Account) = Trim(sDebitAccount) Or Trim(oaccount.ConvertedAccount) = Trim(sDebitAccount) Then
                                                                                    sAccountCurrencyCode = oaccount.CurrencyCode
                                                                                    Exit For
                                                                                End If
                                                                            Next oaccount
                                                                        Next oClient
                                                                    End If
                                                                Next oTmpFileSetup
                                                            End If 'If sDebitAccount <> oPayment.I_Account Then
                                                            If sAccountCurrencyCode <> oPayment.MON_InvoiceCurrency Then
                                                                ' err
                                                                ' DebitAccounts currency must be the same as the paymentcurrency.
                                                                ' Refer to ClientSetup in BabelBank
                                                                AddToErrorArray(UCase(sVariableName), LRS(15104), "1")
                                                            End If

                                                    End Select

                                                    ' XNET 17.01.2013 - added a special test for IntraCompany for TP+
                                                    ' Test for transfers inside same DNB subsidiary
                                                    If oPayment.ToOwnAccount Then
                                                        If Left$(oPayment.BANK_SWIFTCode, 4) = "DNBA" And Left$(oPayment.BANK_SWIFTCode, 8) = Left$(oPayment.BANK_I_SWIFTCode, 8) Then
                                                            ' need both currencycode for debit and creditaccount
                                                            ' these have been filled in TreatTBI, if they have been set up in Clients/Account
                                                            If EmptyString(oPayment.MON_AccountCurrency) Or (Trim(oPayment.MON_CreditAccountCurrency) Is Nothing) Then
                                                                AddToErrorArray(UCase(sVariableName), LRS(15182, oPayment.I_Account, oPayment.E_Account), "1")
                                                            End If
                                                        End If
                                                    End If
                                                    ' 16.01.2016
                                                    ' New test for KRW (Corean Wong)
                                                    ' Debit DNBANOKK only, otherwise, reject
                                                    If oPayment.MON_InvoiceCurrency = "KRW" And Left(oPayment.BANK_I_SWIFTCode, 8) <> "DNBANOKK" Then
                                                        AddToErrorArray(UCase(sVariableName), LRS(15194, oPayment.BANK_I_SWIFTCode), "1")
                                                        'For betalinger i KRW, m� debetkonto v�re i DNB Norge (Debetkontos BIC m� v�re DNBANOKK). Debetkonto BIC er her %1
                                                    End If

                                                Case "EXCHANGEDATE"
                                                    sTmp = VB6.Format(StringToDate((oPayment.ERA_Date)), "YYYY.MM.DD")
                                                    If sTmp = "1990.01.01" Then
                                                        sTmp = ""
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "EXCHANGEDEALER"
                                                    sTmp = oPayment.ERA_DealMadeWith
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "EXCHANGERATEACTUAL"
                                                    sTmp = Trim(Str(oPayment.MON_AccountExchRate))
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "N"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "N"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "EXCHANGERATEAGREED"
                                                    sTmp = Trim(Str(oPayment.ERA_ExchRateAgreed))
                                                    If sTmp <> "0" Then
                                                        'We have an exchrateagreed
                                                        'Then A dealers name and a reference is mandatory
                                                        If oPayment.ERA_DealMadeWith = "" Then
                                                            If oPayment.FRW_ForwardContractNo = "" Then
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15062), "1")
                                                                'An exchange rate is stated, but the dealers name and a reference is not stated. Both are mandatory when using an exchangerate.
                                                            Else
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15063), "1")
                                                                '"An exchange rate is stated, but the dealers name is not stated. The dealers name is mandatory when using an exchangerate."
                                                            End If
                                                        Else
                                                            If oPayment.FRW_ForwardContractNo = "" Then
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15064), "1")
                                                                'An exchange rate is stated, but a reference is not stated. The reference is mandatory when using an exchangerate.
                                                            Else
                                                                'Everything OK
                                                            End If
                                                        End If
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "EXCHANGEREF"
                                                    sTmp = oPayment.FRW_ForwardContractNo
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "FROMACCOUNT"
                                                    sTmp = oPayment.I_Account
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                    End Select

                                                Case "FROMACCOUNTSWIFTADDR"
                                                    sTmp = sSWIFTAddr
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                    End Select

                                                Case "INFOTODNB"
                                                    sTmp = ""
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "NATIONALCODE"
                                                    'New 31.10.2005
                                                    'must be seen in relation with the field 'NATIONALNARR'.
                                                    'Test regarding info to Norges Bank
                                                    sTmp = Trim(oInvoice.STATEBANK_Code)
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            If sCountryCode = "NO" And Len(oPayment.BANK_SWIFTCode) > 6 And Mid(oPayment.BANK_SWIFTCode, 5, 2) <> "NO" Then
                                                                ' XNET 06.11.2012 - added Snd sTmp="", because we should use the imported statebankcode if present
                                                                If oFileSetupIn.CodeNB <> -1 And sTmp = "" Then
                                                                    ' XNET 06.11.2012 - oppsettet sier legg p� kode kun hvis ikke NOK, eller alltid
                                                                    If (oPayment.MON_InvoiceCurrency = "NOK" And oFileSetupIn.CodeNBCurrencyOnly = False) Or (oPayment.MON_InvoiceCurrency <> "NOK") Then
                                                                        sTmp = oFileSetupIn.CodeNB
                                                                        oInvoice.STATEBANK_Code = Trim$(Str(oFileSetupIn.CodeNB))
                                                                        oInvoice.STATEBANK_Text = oFileSetupIn.ExplanationNB
                                                                    End If
                                                                End If
                                                                ' XNET 06.11.2012 - test for NOK-only, with limit of NOK 100 000
                                                                If Len(sTmp) = 0 And oPayment.MON_InvoiceCurrency = "NOK" And oPayment.MON_InvoiceAmount > 10000000 Then
                                                                    '15090: The Norwegian National Bank require that You state a valid code and an explanation
                                                                    'for foreign payments debited a norwegian bankaccount.
                                                                    'Code: %1
                                                                    'Explanation:%2
                                                                    ' XNET 06.11.2012 removed next IF
                                                                    'If oFileSetupIn.CodeNB = -1 Then
                                                                    If EmptyString(oInvoice.STATEBANK_Text) Then
                                                                        bx = AddToErrorArray("INVOICE_STATEBANK_TEXT", LRS(15090, " ", " "), "1")
                                                                    Else
                                                                        bx = AddToErrorArray("INVOICE_STATEBANK_CODE", LRS(15090, " ", Trim$(oInvoice.STATEBANK_Text)), "1")
                                                                    End If
                                                                    sMandatory = "O" ' to prevent 2 errormessages
                                                                    'Else
                                                                    '   sTmp = Trim$(Str(oFileSetupIn.CodeNB))
                                                                    '   oInvoice.STATEBANK_Code = Trim$(Str(oFileSetupIn.CodeNB))
                                                                    '   oInvoice.STATEBANK_Text = oFileSetupIn.ExplanationNB
                                                                    'End If
                                                                Else
                                                                    bNBCodeFound = False
                                                                    If Not Array_IsEmpty(aNBCodesArray) Then
                                                                        For lCounter = 0 To UBound(aNBCodesArray, 1)
                                                                            If Trim$(sTmp) = aNBCodesArray(lCounter) Then
                                                                                bNBCodeFound = True
                                                                                Exit For
                                                                            End If
                                                                        Next lCounter
                                                                    End If
                                                                    ' XNET 06.11.2012 - test for NOK-only, with limit of NOK 100 000
                                                                    If oPayment.MON_InvoiceCurrency = "NOK" And oPayment.MON_InvoiceAmount > 10000000 Then
                                                                        If bNBCodeFound Then
                                                                            sMandatory = "M"
                                                                        Else
                                                                            bx = AddToErrorArray("INVOICE_STATEBANK_CODE", LRS(15090, " ", sTmp), "1")
                                                                            sMandatory = "O" ' to prevent 2 errormessages
                                                                        End If
                                                                    Else
                                                                        sMandatory = "O"
                                                                    End If
                                                                End If
                                                            Else
                                                                sMandatory = "O"
                                                            End If
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "NATIONALNARR"
                                                    sTmp = Trim(oInvoice.STATEBANK_Text)
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            If sCountryCode = "NO" And Len(oPayment.BANK_SWIFTCode) > 6 And Mid(oPayment.BANK_SWIFTCode, 5, 2) <> "NO" Then

                                                                If Len(sTmp) = 0 Then
                                                                    If Not EmptyString((oInvoice.STATEBANK_Code)) Then
                                                                        '15090: The Norwegian National Bank require that You state a valid code and an explanation
                                                                        'for foreign payments debited a norwegian bankaccount.
                                                                        'Code: %1
                                                                        'Explanation:%2
                                                                        If oInvoice.STATEBANK_Code = Trim$(Str(oFileSetupIn.CodeNB)) Then
                                                                            ' XNET 06.11.2012 - oppsettet sier legg p� kode kun hvis ikke NOK, eller alltid
                                                                            If (oPayment.MON_InvoiceCurrency = "NOK" And oFilesetup.CodeNBCurrencyOnly = False) Or (oPayment.MON_InvoiceCurrency <> "NOK") Then
                                                                                sTmp = oFilesetup.ExplanationNB
                                                                            End If
                                                                        Else
                                                                            ' XNET 06.11.2012 - test for NOK-only, with limit of NOK 100 000
                                                                            If oPayment.MON_InvoiceCurrency = "NOK" And oPayment.MON_InvoiceAmount > 10000000 Then
                                                                                bx = AddToErrorArray("INVOICE_STATEBANK_TEXT", LRS(15090, Trim$(oInvoice.STATEBANK_Code), " "), "1")
                                                                            Else
                                                                                sMandatory = "O"
                                                                            End If
                                                                        End If
                                                                    Else
                                                                        'Errormessage already added to the array.
                                                                    End If
                                                                    sMandatory = "O"
                                                                Else
                                                                    sMandatory = "M"
                                                                End If
                                                            Else
                                                                sMandatory = "O"
                                                            End If
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "ORDERINGCUSTOMER"
                                                    sTmp = ""
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "PAYERSREFERENCE"
                                                    sTmp = RTrim(oPayment.REF_Own)

                                                    ' added 09.08.2010 -
                                                    ' TelepayPlus will accept 35 characters PayersReference (Ref_Own)
                                                    If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                        sMandatory = "O"
                                                        If Len(sTmp) > 30 Then
                                                            If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "30"), "0")
                                                                'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                            Else
                                                                sTmp = Left(sTmp, 35)
                                                            End If
                                                        End If
                                                    Else
                                                        ' as pre 09.08.2010
                                                        Select Case sPaymentType
                                                            Case "GEN"
                                                                sMandatory = "O"
                                                                If Len(sTmp) > 16 Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "16"), "0")
                                                                        'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                    Else
                                                                        sTmp = Left(sTmp, 16)
                                                                    End If
                                                                End If
                                                            Case "ACH"
                                                                sMandatory = "O"
                                                                If sCountryCode = "SG" Or sCountryCode = "FI" Then
                                                                    If Len(sTmp) > 12 Then
                                                                        If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "12"), "0")
                                                                            'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                        Else
                                                                            sTmp = Left(sTmp, 12)
                                                                        End If
                                                                    End If
                                                                Else
                                                                    If Len(sTmp) > 16 Then
                                                                        If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "16"), "0")
                                                                            'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                        Else
                                                                            sTmp = Left(sTmp, 16)
                                                                        End If
                                                                    End If
                                                                End If
                                                            Case "SAL"
                                                                sMandatory = "O"
                                                                If sCountryCode = "SG" Or sCountryCode = "FI" Then
                                                                    If Len(sTmp) > 12 Then
                                                                        If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "12"), "0")
                                                                            'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                        Else
                                                                            sTmp = Left(sTmp, 12)
                                                                        End If
                                                                    End If
                                                                Else
                                                                    If Len(sTmp) > 16 Then
                                                                        If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "16"), "0")
                                                                            'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                        Else
                                                                            sTmp = Left(sTmp, 16)
                                                                        End If
                                                                    End If
                                                                End If
                                                            Case "FIK"
                                                                sMandatory = "O"
                                                                If Len(sTmp) > 16 Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "16"), "0")
                                                                        'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                    Else
                                                                        sTmp = Left(sTmp, 16)
                                                                    End If
                                                                End If

                                                            Case "REF"
                                                                ' 20.03.2009 JanP
                                                                ' This is not the correct place to errormark Referencenumber
                                                                ' MOved to INFOTOBENEFICIARY

                                                                ' 19.02.2010
                                                                ' REF: After discussion with Sigurd Wiig, and case Actavis FI,
                                                                ' it is correct to fetch from oInvoice.Unique_Id and put into Payers Reference !!!

                                                                sTmp = Trim(oInvoice.Unique_Id)
                                                                If Not ValidateReference(sTmp, sErrorString) Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), sErrorString, "1")
                                                                End If
                                                                sMandatory = "M"
                                                            Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                                sMandatory = "O"
                                                                If Len(sTmp) > 16 Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "16"), "0")
                                                                        'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                    Else
                                                                        sTmp = Left(sTmp, 16)
                                                                    End If
                                                                End If
                                                        End Select
                                                    End If

                                                Case "PAYMENTDATE"
                                                    'sTmp = oPayment.DATE_Payment 'Format(StringToDate(oPayment.DATE_Payment), "DD.MM.YYYY")

                                                    'If StringToDate(oPayment.DATE_Payment) < dFirstValidPaymentDate Then
                                                    ' XNET 15.11.2010
                                                    ' Also test for, and errormark, dates older than 3 months
                                                    'If StringToDate(oPayment.DATE_Payment) < dFirstValidPaymentDate And _
                                                    '                bx = AddToErrorArray(UCase(sVariableName), LRS(15065, VB6.Format(StringToDate((oPayment.DATE_Payment)), "YYYY-MM-DD")), "0") Then
                                                    'sTmp = DateToString(dFirstValidPaymentDate)
                                                    If DateDiff(DateInterval.Day, StringToDate(oPayment.DATE_Payment), Date.Today) > 180 Then
                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15065, Format(StringToDate(oPayment.DATE_Payment), "yyyy-MM-dd")), "1")
                                                        sTmp = DateToString(dFirstValidPaymentDate)
                                                    ElseIf StringToDate(oPayment.DATE_Payment) < dFirstValidPaymentDate Then
                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15065, Format(StringToDate(oPayment.DATE_Payment), "yyyy-MM-dd")), "0")  ' warning only
                                                        sTmp = DateToString(dFirstValidPaymentDate)

                                                    Else
                                                        If IsHoliday(StringToDate((oPayment.DATE_Payment)), Mid(sSWIFTAddr, 5, 2), True) Then
                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15085, VB6.Format(StringToDate((oPayment.DATE_Payment)), "YYYY-MM-DD")), "0")
                                                            sTmp = DateToString(GetBankday(StringToDate((oPayment.DATE_Payment)), Mid(sSWIFTAddr, 5, 2), 1))
                                                        Else
                                                            sTmp = oPayment.DATE_Payment
                                                        End If
                                                    End If


                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                    End Select

                                                Case "PAYMENTDETAILS"
                                                    bNewDetails = True

                                                Case "PAYMENTTYPE"
                                                    sTmp = sPaymentType
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sTmp = "General"
                                                        Case "ACH"
                                                            sTmp = "ACH"
                                                        Case "SAL"
                                                            sTmp = "Salary"
                                                        Case "FIK"
                                                            sTmp = "FI"
                                                        Case "REF"
                                                            sTmp = "RefPayment"
                                                        Case "LOCALLOW" ' added 16.03.2010
                                                            sTmp = "Local"
                                                        Case "LOCALHIGH" ' added 16.03.2010
                                                            sTmp = "Local"
                                                        Case "LOCALCHECK" ' added 16.03.2010
                                                            sTmp = "Local"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sTmp = "IPB"
                                                    End Select
                                                    sMandatory = "M"

                                                Case "TBIWREFERENCE"
                                                    'Only for return
                                                    sMandatory = "N"

                                                Case "URGENT"
                                                    If oPayment.Priority = True Then
                                                        sTmp = "1"
                                                    Else
                                                        sTmp = "0"
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW"
                                                            sMandatory = "M"
                                                            ' 21.04.2010 Put value 0 into urgent for LocalLow
                                                            sTmp = "0"
                                                        Case "LOCALHIGH"
                                                            sMandatory = "M"
                                                            ' 21.04.2010 Put value 1 into urgent for LocalHigh
                                                            sTmp = "1"
                                                        Case "LOCALCHECK"
                                                            sMandatory = "M"
                                                            ' 21.04.2010 Put value 2 into urgent for LocalCheck
                                                            sTmp = "2"
                                                        Case "IPBLOW"
                                                            sMandatory = "M"
                                                            ' Put value 0 into urgent for LocalLow
                                                            sTmp = "0"
                                                        Case "IPBHIGH"
                                                            sMandatory = "M"
                                                            ' Put value 1 into urgent for LocalHigh
                                                            sTmp = "1"
                                                    End Select

                                                Case "VALUEDATE"
                                                    sTmp = VB6.Format(StringToDate((oPayment.DATE_Value)), "YYYY-MM-DD")
                                                    If sTmp = "1990-01-01" Then
                                                        sTmp = ""
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "O"
                                                        Case "SAL"
                                                            sMandatory = "O"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case Else
                                                    'If InStr(1, sVariableName, "DATE", vbTextCompare) > 0 Then

                                                    'End If
                                                    sMandatory = "O"
                                                    sTmp = ""
                                                    'err.Raise 15052, , LRS(15052, sVariableName)
                                                    '"The variable " & sVariableName & " in the schema file is not implemented in Babelbank."

                                            End Select

                                            Select Case sMandatory
                                                Case "M"
                                                    If Len(sTmp) = 0 Then
                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15061, sVariableName), "1")
                                                        'err.Raise 1, , sVariableName & " is mandatory, but the variable has no value."
                                                    End If
                                                    'If TBIOElementOK(sTmp, nodPaymentSeqList.item(lNodeListCounter), sVariableName, True, sFormat) Then
                                                    '    newEle = docXMLExport.createElement(sVariableName)
                                                    '    newEle.nodeTypedValue = sTmp

                                                    '    'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    '    'nodPaymentNode.appendChild(newEle)
                                                    'End If

                                                Case "O"
                                                    'Nothing to do
                                                    'If TBIOElementOK(sTmp, nodPaymentSeqList.item(lNodeListCounter), sVariableName, False, sFormat) Then
                                                    '    newEle = docXMLExport.createElement(sVariableName)
                                                    '    newEle.nodeTypedValue = sTmp
                                                    '    'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    '    'nodPaymentNode.appendChild(newEle)
                                                    'End If

                                                Case "N"
                                                    sTmp = ""
                                                    'If TBIOElementOK(sTmp, nodPaymentSeqList.item(lNodeListCounter), sVariableName, False, sFormat) Then
                                                    '    newEle = docXMLExport.createElement(sVariableName)
                                                    '    newEle.nodeTypedValue = sTmp
                                                    '    'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    '    'nodPaymentNode.appendChild(newEle)
                                                    'End If
                                            End Select

                                        Next lNodeListCounter

                                    End If 'bNewPayment

                                    If bNewDetails Then
                                        bNewDetails = False
                                        'newEle = docXMLExport.createElement("PaymentDetails")
                                        'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        'nodPaymentNode.appendChild(newEle)

                                        ' 211204 -
                                        ' KJELL - HVORFOR TRYNER VI HER, etter � ha slettet 2 poster
                                        ' deretter valgt Neste p� tredje post?
                                        ' Se case med Odfjell-fil

                                        'nodDetailsNode = newEle
                                        'nTotalPaymentsAmount = nTotalPaymentsAmount + oInvoice.MON_InvoiceAmount
                                        'lTotalNumberOfPayments = lTotalNumberOfPayments + 1
                                        'Add information to the Details-level.
                                        ReDim aNodeListArray(28)
                                        aNodeListArray(0) = "ACHACCOUNTNAME"
                                        aNodeListArray(1) = "ACHBABELBANKREFERENCE"
                                        aNodeListArray(2) = "ACHBANKID"
                                        aNodeListArray(3) = "ACHPAYERSREF"
                                        aNodeListArray(4) = "ACHBENEFICIARYREF"
                                        aNodeListArray(5) = "AMOUNTDETAILS"
                                        aNodeListArray(6) = "BENBANKID"
                                        aNodeListArray(7) = "BENBANKIDTYPE"
                                        aNodeListArray(8) = "BENEFICIARYADDRESS1"
                                        aNodeListArray(9) = "BENEFICIARYADDRESS2"
                                        aNodeListArray(10) = "BENEFICIARYADDRESS3"
                                        aNodeListArray(11) = "BENEFICIARYADDRESS4"
                                        aNodeListArray(12) = "BENEFICIARYBANKADDRESS1"
                                        aNodeListArray(13) = "BENEFICIARYBANKADDRESS2"
                                        aNodeListArray(14) = "BENEFICIARYBANKADDRESS3"
                                        aNodeListArray(15) = "BENEFICIARYBANKADDRESS4"
                                        aNodeListArray(16) = "BENEFICIARYBANKCOUNTRYCODE"
                                        aNodeListArray(17) = "INFOBENBANKCODE"
                                        aNodeListArray(18) = "INFOTOBENBANK"
                                        aNodeListArray(19) = "INFOTOBENEFICIARY"
                                        aNodeListArray(20) = "INTBANKID"
                                        aNodeListArray(21) = "INTBANKIDTYPE"
                                        aNodeListArray(22) = "INTBANKNAMEADDRESS1"
                                        aNodeListArray(23) = "INTBANKNAMEADDRESS2"
                                        aNodeListArray(24) = "INTBANKNAMEADDRESS3"
                                        aNodeListArray(25) = "INTBANKNAMEADDRESS4"
                                        aNodeListArray(26) = "TOACCOUNT"
                                        aNodeListArray(27) = "REF_ENDTOEND"
                                        aNodeListArray(28) = "PURPOSECODE"
                                        'For lNodeListCounter = 0 To nodDetailsSeqList.length - 1
                                        For lNodeListCounter = 0 To aNodeListArray.GetUpperBound(0)
                                            sMandatory = ""
                                            sVariableName = aNodeListArray(lNodeListCounter)

                                            Select Case UCase(sVariableName)

                                                Case "ACHACCOUNTNAME"
                                                    If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                        sTmp = RTrim(oPayment.E_Name)
                                                        Select Case sCountryCode
                                                            Case "GB" 'BACS
                                                                lVariableLength = 18
                                                            Case "US" ' ACH(US)
                                                                lVariableLength = 16
                                                            Case "SG" 'MEPS
                                                                lVariableLength = 140
                                                            Case "DE" 'DTA
                                                                lVariableLength = 27
                                                            Case "SE" 'BG/PG
                                                                lVariableLength = 35
                                                            Case "DK" 'FI
                                                                lVariableLength = 35
                                                            Case "NO" 'BBS
                                                                lVariableLength = 140
                                                            Case "FI"
                                                                ' XNET 14.12.2010 changed from 20 to 35 for FI
                                                                lVariableLength = 35 ' 20
                                                            Case Else
                                                                lVariableLength = 140
                                                        End Select
                                                        If Len(sTmp) = 0 Then
                                                            '"The field 'Accountname' is mandatory."
                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15066), "1")
                                                        End If
                                                        If Len(sTmp) > lVariableLength Then
                                                            If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15080, sTmp, UCase(sVariableName)) & " " & Trim$(Str(lVariableLength)), "0")
                                                                'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                            Else
                                                                sTmp = Left(sTmp, lVariableLength)
                                                            End If
                                                        End If
                                                    Else
                                                        sTmp = ""
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "N"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            sMandatory = "N"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "ACHBABELBANKREFERENCE"
                                                    'Case "BABELBANKREF"  ' changed 16.01.2008
                                                    ' 16.01.2008 added some testcode for BabelBankreference
                                                    '     sTmp = RTrim$(oPayment.REF_Own) & "%%%"
                                                    ' run through all invoices
                                                    '     sTmp = sTmp & oInvoice.REF_Own & "#" & LTrim(Str(oInvoice.MON_InvoiceAmount)) & "���"
                                                    '--------------------------
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "O"
                                                        Case "SAL"
                                                            sMandatory = "O"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "ACHBANKID"
                                                    If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                        sTmp = oPayment.BANK_BranchNo
                                                        Select Case sCountryCode
                                                            Case "GB" 'BACS
                                                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                                                lVariableLength = 6
                                                                If Len(sTmp) <> lVariableLength Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15067, CStr(6), sTmp), "1")
                                                                    '"The field 'BankID' is mandatory and numeric, and is of a length of exactly 6 digits. The value stated is: " & sTmp
                                                                Else
                                                                    If Not vbIsNumeric(sTmp, "0123456789") Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15068, CStr(6), sTmp), "1")
                                                                        '"The field 'BankID' must be numeric, and is of a length of exactly 6 digits. The value stated is: " & sTmp
                                                                    End If
                                                                End If
                                                            Case "ZA" ' added 27.02.2017
                                                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.ZASortCode
                                                                lVariableLength = 6
                                                                If Len(sTmp) <> lVariableLength Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15067, CStr(6), sTmp), "1")
                                                                    '"The field 'BankID' is mandatory and numeric, and is of a length of exactly 6 digits. The value stated is: " & sTmp
                                                                Else
                                                                    If Not vbIsNumeric(sTmp, "0123456789") Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15068, CStr(6), sTmp), "1")
                                                                        '"The field 'BankID' must be numeric, and is of a length of exactly 6 digits. The value stated is: " & sTmp
                                                                    End If
                                                                End If
                                                            Case "US" ' ACH(US)
                                                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Fedwire
                                                                lVariableLength = 9
                                                                If Len(sTmp) <> lVariableLength Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15067, CStr(9), sTmp) & sTmp, "1")
                                                                Else
                                                                    If Not vbIsNumeric(sTmp, "0123456789") Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15068, CStr(9), sTmp), "1")
                                                                    End If
                                                                End If
                                                            Case "SG" 'MEPS
                                                                ' XokNET 16.07.2014 - added G3-test
                                                                If oPayment.DATE_Payment > "20151122" Then
                                                                    ' no need for BankID after G3 has started
                                                                    sMandatory = "N"
                                                                Else

                                                                    ' No need for BankID if payment to DnBSG
                                                                    If Left$(oPayment.BANK_SWIFTCode, 6) <> "DNBASG" Then

                                                                        ' 07.04.06
                                                                        ' According to Sigurd Wiig and Daniel Mah BankID is not needed for ACH
                                                                        ' Thus, added next if and added emptystring(sTmp) 5 lines down
                                                                        If Not EmptyString(sTmp) Then
                                                                            oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.MEPS
                                                                        End If
                                                                        lVariableLength = 7
                                                                        If Len(sTmp) <> lVariableLength And Not EmptyString(sTmp) Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15067, CStr(7), sTmp), "1")
                                                                        Else
                                                                            If Not vbIsNumeric(sTmp, "0123456789") Then
                                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15068, CStr(7), sTmp), "1")
                                                                            End If
                                                                        End If
                                                                    End If
                                                                End If
                                                            Case "DE" 'DTA
                                                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                                lVariableLength = 8
                                                                If Len(sTmp) = 0 Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15069), "1")
                                                                    '"The field 'Bank ID' is mandatory."
                                                                End If
                                                            Case "SE" 'BG/PG
                                                                ' 06.06.2016
                                                                ' Vi trenger ikke noen ACHbankID for massetrans Sverige
                                                                sMandatory = "O"
                                                                'lVariableLength = 35
                                                                'If Len(sTmp) <> lVariableLength Then
                                                                '    bx = AddToErrorArray(UCase(sVariableName), LRS(15067, CStr(35), sTmp), "1")
                                                                'Else
                                                                '    If Not vbIsNumeric(sTmp, "0123456789") Then
                                                                '        bx = AddToErrorArray(UCase(sVariableName), LRS(15068, CStr(35), sTmp) & sTmp, "1")
                                                                '    End If
                                                                'End If
                                                            Case "DK" 'FI
                                                                lVariableLength = 35
                                                                If Len(sTmp) = 0 Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15069), "1")
                                                                End If
                                                            Case "NO"
                                                                lVariableLength = 35
                                                            Case "FI"
                                                                ' 20.09.06
                                                                ' Probably not needed, but documentation from DnBNOR says ???
                                                                lVariableLength = 35
                                                        End Select
                                                    Else
                                                        sTmp = ""
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "N"
                                                            ' XokNET 03.09.2014 - added G3-test, ta hele "ACH"-delen
                                                        Case "ACH"
                                                            ' XokNET 19.09.2011
                                                            ' No need for BankID if payment to DnBSG
                                                            If sCountryCode = "SG" Then
                                                                If Left$(oPayment.BANK_SWIFTCode, 6) = "DNBASG" Then
                                                                    sMandatory = "O"
                                                                Else
                                                                    If oPayment.DATE_Payment > "20151122" Then
                                                                        ' no need for BankID after G3 has started
                                                                        sMandatory = "N"
                                                                    Else
                                                                        sMandatory = "M"
                                                                    End If
                                                                End If
                                                            ElseIf sCountryCode = "NO" Then
                                                                ' 20.04.2016
                                                                ' no need for ACH bankid in Norway
                                                                sMandatory = "N"
                                                            Else
                                                                sMandatory = "M"
                                                            End If
                                                            ' 07.04.06
                                                            ' According to Sigurd Wiig and Daniel Mah BankID is not needed for ACH
                                                            ' 27.04.06 Which proved to be wrong!!!
                                                            'sMandatory = "O"
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                            ' added 06.06.2016
                                                            If sCountryCode = "SE" Then
                                                                sMandatory = "O"
                                                            End If

                                                            ' added 20.09.06
                                                            If sCountryCode = "FI" Then
                                                                sMandatory = "O"
                                                            End If
                                                            ' XOKNET 29.10.2010
                                                            ' No need for bankcodes for Norwegian salaries
                                                            If sCountryCode = "NO" Then
                                                                sMandatory = "N"
                                                            End If
                                                            ' XokNET 16.07.2014 After G3 in SG no need for ACHBankID
                                                            If sCountryCode = "SG" Then
                                                                If oPayment.DATE_Payment > "20151122" Then
                                                                    ' no need for BankID after G3 has started
                                                                    sMandatory = "N"
                                                                End If
                                                            End If
                                                        Case "FIK"
                                                            sMandatory = "N"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "ACHPAYERSREF"
                                                    ' added 09.08.2010 -
                                                    ' TelepayPlus will accept 35 characters PayersReference (Ref_Own)
                                                    If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                        ' XNET 22.10.2013
                                                        If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                            sTmp = RTrim(oInvoice.REF_Own)
                                                        Else
                                                            sTmp = ""
                                                        End If
                                                        sMandatory = "O"
                                                        ' XNET 14.12.2010, changed from 30 to 35
                                                        If Len(sTmp) > 35 Then  '30 Then
                                                            If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", "30"), "0")
                                                                'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                            Else
                                                                sTmp = Left(sTmp, 35)
                                                            End If
                                                        End If
                                                    Else
                                                        If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                            sTmp = RTrim(oInvoice.REF_Own)
                                                            If sTmp = "" Then
                                                                If oBabel.ImportFormat = vbBabel.BabelFiles.FileType.DTAUS Then
                                                                    oInvoice.REF_Own = oInvoice.CustomerNo
                                                                    sTmp = oInvoice.CustomerNo
                                                                End If
                                                            End If
                                                            Select Case sCountryCode
                                                                Case "GB" 'BACS
                                                                    lVariableLength = 35
                                                                Case "US" ' ACH(US)
                                                                    lVariableLength = 35
                                                                Case "SG" 'GIRO
                                                                    lVariableLength = 12
                                                                Case "DE"
                                                                    lVariableLength = 35
                                                                Case "SE"
                                                                    lVariableLength = 35
                                                                Case "DK" 'FI
                                                                    lVariableLength = 35
                                                                Case "NO"
                                                                    lVariableLength = 35
                                                                Case "FI"
                                                                    lVariableLength = 12
                                                            End Select
                                                            If Len(sTmp) = 0 Then
                                                                '"The field 'Accountname' is mandatory."
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15066), "1")
                                                            End If
                                                            If Len(sTmp) > lVariableLength Then
                                                                If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                                Else
                                                                    sTmp = Left(sTmp, lVariableLength)
                                                                End If
                                                            End If
                                                        Else
                                                            sTmp = ""
                                                        End If
                                                        Select Case sPaymentType
                                                            Case "GEN"
                                                                sMandatory = "N"
                                                            Case "ACH"
                                                                sMandatory = "M"
                                                            Case "SAL"
                                                                sMandatory = "M"
                                                            Case "FIK"
                                                                sMandatory = "N"
                                                            Case "REF"
                                                                sMandatory = "N"
                                                            Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                                sMandatory = "N"
                                                        End Select
                                                    End If

                                                Case "ACHBENEFICIARYREF"
                                                    If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                        sTmp = ""
                                                        For Each oFreeText In oInvoice.Freetexts
                                                            sTmp = sTmp & oFreeText.Text
                                                        Next oFreeText
                                                        ' Added 20.09.06
                                                        sTmp = Trim(sTmp)
                                                        Select Case sCountryCode
                                                            Case "GB" 'BACS
                                                                lVariableLength = 18
                                                            Case "US" ' ACH(US)
                                                                lVariableLength = 35
                                                            Case "SG" 'MEPS
                                                                lVariableLength = 140
                                                            Case "DE" 'DTA
                                                                lVariableLength = 54
                                                            Case "SE" 'BG/PG
                                                                lVariableLength = 25
                                                            Case "DK" 'FI
                                                                lVariableLength = 35
                                                            Case "NO"
                                                                lVariableLength = 140
                                                            Case "FI"
                                                                ' XNET 14.12.2010 changed from 12 to 140
                                                                lVariableLength = 140   '12
                                                        End Select
                                                        If Len(sTmp) = 0 Then
                                                            ' XNET 17.11.2010, added next if
                                                            If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                                ' not mandatory for TP+
                                                                sMandatory = "O"
                                                            Else
                                                                '"The field 'Accountname' is mandatory."
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15070), "1")
                                                            End If
                                                        End If
                                                        If Len(sTmp) > lVariableLength Then
                                                            If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                            Else
                                                                sTmp = Left(sTmp, lVariableLength)
                                                            End If
                                                        End If
                                                    Else
                                                        sTmp = ""
                                                    End If
                                                    ' XokNET 21.03.2011, added next if
                                                    If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                        ' not mandatory for TP+
                                                        sMandatory = "O"
                                                    Else
                                                        Select Case sPaymentType
                                                            Case "GEN"
                                                                sMandatory = "N"
                                                            Case "ACH"
                                                                sMandatory = "M"
                                                            Case "SAL"
                                                                sMandatory = "M"
                                                            Case "FIK"
                                                                sMandatory = "N"
                                                            Case "REF"
                                                                sMandatory = "N"
                                                            Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                                sMandatory = "N"
                                                        End Select
                                                    End If

                                                Case "AMOUNTDETAILS"
                                                    ' Next test set by JanP
                                                    ' Error raised for 0-amounts !
                                                    If oInvoice.MON_InvoiceAmount <> 0 Then
                                                        'sTmp = Left$(oInvoice.MON_InvoiceAmount, Len(oInvoice.MON_InvoiceAmount) - 2) & "." & Right(oInvoice.MON_InvoiceAmount, 2)
                                                        sTmp = Replace(VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##0.00"), ",", ".")
                                                    Else
                                                        sTmp = "0.00"
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "M"
                                                        Case "ACH"
                                                            sMandatory = "M"
                                                            ' XokNET 12.09.2014 - added test on max 99.999.999,00 in amount;
                                                            If oPayment.I_CountryCode = "SG" And oPayment.DATE_Payment > "20151122" Then
                                                                If Len(Replace(sTmp, ".", "")) > 10 Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15080, sTmp, UCase(sVariableName)) & " 10", "1")
                                                                End If
                                                            End If
                                                        Case "SAL"
                                                            sMandatory = "M"
                                                            If Len(sTmp) > 11 Then
                                                                'XokNET 12.09.2014 Changed %3 part of errormsg
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15080, sTmp, UCase(sVariableName)) & " 10", "1")
                                                                'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " "Length"
                                                            End If
                                                        Case "FIK"
                                                            sMandatory = "M"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                    End Select

                                                Case "BENBANKID"
                                                    If oPayment.BANK_BranchType > vbBabel.BabelFiles.BankBranchType.SWIFT Then
                                                        sTmp = oPayment.BANK_BranchNo
                                                    Else
                                                        sTmp = oPayment.BANK_SWIFTCode
                                                        ' XokNET 06.05.2011 - added BIC-test for Germany
                                                        ' Krav om fullt ifyllt BIC lengde 11
                                                        ' -   Kravet m� legges inn i Babel bank. Bruker sendes til "Form correct" hvis BIC er p� 8 karakterer (selv om denne er gyldig)
                                                        'Melding: "Payments to be credited accounts located in Germany requires 11 characters in BIC address."
                                                        'Ved eksempelvis BIC COBADEFF s� returnerer 912 COBADEFF600
                                                        ' XNET 16.01.2013 - DNB has discussed this demand for Germany, about 11 chars i BIC. They will have it removed !
                                                        'If oPayment.BANK_CountryCode = "DE" Then
                                                        'If Len(sTmp) <> 11 Or (Len(sTmp) = 11 And Right$(sTmp, 3) = "XXX") Then
                                                        ' XokNET 22.08.2011, changed If to
                                                        '    If Len(sTmp) <> 11 Then
                                                        '        ' "Payments to be credited accounts located in Germany requires 11 characters in BIC address."
                                                        '        bx = AddToErrorArray(UCase(sVariableName), LRS(15108) & vbCrLf & sErrorString, "1")
                                                        '        sErrorString = vbNullString
                                                        '    End If
                                                        'End If

                                                    End If

                                                    ' 27.02.2017
                                                    ' added test for SouthAfrica - must have bankid
                                                    If oPayment.BANK_CountryCode = "ZA" Then
                                                        If EmptyString(oPayment.BANK_BranchNo) Then
                                                            ' must have bankcode in ZA
                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15069), "1")
                                                            '"The field 'Bank ID' is mandatory."
                                                        End If
                                                    End If


                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            If Len(sTmp) > 0 Then
                                                                If Not ValidateBankID(oPayment.BANK_BranchType, sTmp, False, sErrorString) Then
                                                                    If oPayment.BANK_BranchType > BabelFiles.BankBranchType.SWIFT Then
                                                                        bx = AddToErrorArray("BENBANKID_BRANCH", LRS(15071) & vbCrLf & sErrorString, "1")
                                                                    Else
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15071) & vbCrLf & sErrorString, "1")
                                                                    End If
                                                                    sErrorString = vbNullString
                                                                End If
                                                            End If
                                                            If oPayment.BANK_BranchType = BabelFiles.BankBranchType.NoBranchType Then
                                                                sMandatory = "O"
                                                            Else
                                                                sMandatory = "M"
                                                            End If

                                                            ' added 03.09.2009
                                                            ' Must have bankcode for International payments.
                                                            ' This is not perfect, because we may lack countrycodes.
                                                            ' No use in taking them from Swift, because this test is meaningful
                                                            ' only to check if we have correct swift or bankbranch.
                                                            ' 20.04.2016 - not for cheque !
                                                            If Not EmptyString(oPayment.E_Account) Then
                                                                If Not EmptyString(oPayment.I_CountryCode) And Not EmptyString(oPayment.BANK_CountryCode) Then
                                                                    If oPayment.I_CountryCode <> oPayment.BANK_CountryCode Then
                                                                        sMandatory = "M"
                                                                    End If
                                                                End If
                                                                If Not EmptyString(oPayment.I_CountryCode) And Not EmptyString(oPayment.E_CountryCode) Then
                                                                    If oPayment.I_CountryCode <> oPayment.E_CountryCode Then
                                                                        sMandatory = "M"
                                                                    End If
                                                                End If
                                                            End If

                                                            ' XokNET 23.12.2014
                                                            ' Some changes, and comments removed
                                                            ' xnet to comment End xnet 23.12.2014
                                                            'If ((oPayment.BANK_AccountCountryCode = "FI" And oPayment.BANK_CountryCode = "FI") Or _
                                                            '    (oPayment.BANK_AccountCountryCode = "DE" And oPayment.BANK_CountryCode = "DE")) And _
                                                            '    IsIBANNumber(oPayment.E_Account, False, False) And _
                                                            '    oPayment.MON_ChargeMeAbroad = False And oPayment.MON_ChargeMeDomestic = True And oPayment.MON_TransferCurrency = "EUR" Then
                                                            '    ' no need for SWIFT if sepapayment from FI or DE
                                                            ' 04.02.2016 - changed BIC-test
                                                            If IsSEPACountry(oPayment.BANK_AccountCountryCode) And IsIBANNumber(oPayment.E_Account, False, False) And _
                                                                oPayment.MON_ChargeMeAbroad = False And oPayment.MON_ChargeMeDomestic = True And oPayment.MON_TransferCurrency = "EUR" Then
                                                                ' - no need for Swift (BIC) to receiver inside SEPA

                                                            ElseIf oPayment.E_CountryCode = "DE" And oPayment.BANK_BranchType = BabelFiles.BankBranchType.Bankleitzahl And IsNumeric(Left$(oPayment.E_Account, 2)) = True Then
                                                                '1.    DTA payments - reg code 68 - Urgent shall be required if BBAN and BLZ is sent from the client. (Payments with BBAN and BLZ shall be stopped if Urgent is not present)
                                                                If oPayment.Priority = False Then
                                                                    ' have to be urgentpayment if DE and BLZ and BBAN!
                                                                    ' 15110: Betalinger med BBAN og BLZ m� b�re hastebetalinger (Urgent)
                                                                    bx = AddToErrorArray("BENBANKID_BRANCH", LRS(15110) & vbCrLf & sErrorString, "1")
                                                                    sErrorString = vbNullString
                                                                End If

                                                                ' End xnet 23.12.2014
                                                                ' -------------------


                                                            ElseIf oPayment.MON_TransferCurrency = "EUR" Then
                                                                ' Check receivers country;
                                                                If oPayment.E_CountryCode = "BE" Or oPayment.E_CountryCode = "FI" Or _
                                                                    oPayment.E_CountryCode = "FR" Or oPayment.E_CountryCode = "GR" Or _
                                                                    oPayment.E_CountryCode = "IE" Or oPayment.E_CountryCode = "IT" Or _
                                                                    oPayment.E_CountryCode = "LU" Or oPayment.E_CountryCode = "NL" Or _
                                                                    oPayment.E_CountryCode = "PT" Or oPayment.E_CountryCode = "SI" Or _
                                                                    oPayment.E_CountryCode = "ES" Or oPayment.E_CountryCode = "DE" Or _
                                                                    oPayment.E_CountryCode = "AT" Or oPayment.E_CountryCode = "CY" Or _
                                                                    oPayment.E_CountryCode = "MT" Then
                                                                    ' Must have Swift receiver for those countries
                                                                    ' XokNET 25.02.2014
                                                                    ' After discussions with H�kon Belt, we remove the demand for BIC
                                                                    'If EmptyString(oPayment.BANK_SWIFTCode) Then
                                                                    '    bx = AddToErrorArray("BENBANKID_BRANCH", LRS(15093) & vbCrLf & sErrorString, "0")
                                                                    '    sErrorString = vbNullString
                                                                    'End If
                                                                End If
                                                            End If


                                                        Case "ACH"
                                                            ' XNET 23.06.2014 added test for SG G3
                                                            ' Singapore G3, from 23.03.2015
                                                            ' -----------------------------
                                                            ' BIC is mandatory;
                                                            If oPayment.I_CountryCode = "SG" And oPayment.DATE_Payment > "20151122" Then
                                                                sTmp = oPayment.BANK_SWIFTCode
                                                                oPayment.BANK_BranchType = BabelFiles.BankBranchType.SWIFT
                                                                'XNET - Next If
                                                                If Not ValidateBankID(oPayment.BANK_BranchType, oPayment.BANK_SWIFTCode, False, sErrorString) Then
                                                                    bx = AddToErrorArray("BENBANKID", LRS(15148) & vbCrLf & sErrorString, "1")
                                                                End If
                                                                sMandatory = "M"
                                                            Else
                                                                sMandatory = "N"
                                                            End If
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            If Len(oPayment.BANK_SWIFTCode) > 0 And oPayment.BANK_BranchType < 2 Then
                                                                If Not ValidateBankID((oPayment.BANK_BranchType), (oPayment.BANK_SWIFTCode), False, sErrorString) Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15093) & vbCrLf & sErrorString, "1")
                                                                    sErrorString = ""
                                                                End If
                                                            Else
                                                                sTmp = ""
                                                            End If
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                            If oPayment.BANK_CountryCode = "CA" Then
                                                                If Len(oPayment.BANK_BranchNo) <> 9 Or Left(oPayment.BANK_BranchNo, 1) <> "0" Then
                                                                    ' Must have Fedwire, starting with 0 for local payments Canada
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15102) & vbCrLf & sErrorString, "1")
                                                                    sErrorString = ""
                                                                End If
                                                            ElseIf oPayment.BANK_CountryCode = "AU" Then
                                                                If Len(oPayment.BANK_BranchNo) <> 6 Then
                                                                    ' Must have ABA, 6 digits for local payments Australia
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15103) & vbCrLf & sErrorString, "1")
                                                                    sErrorString = ""
                                                                End If
                                                                ' Singapore G3, IPB
                                                                ' -----------------------------
                                                                ' BIC is mandatory;
                                                            ElseIf oPayment.I_CountryCode = "SG" Then
                                                                sTmp = oPayment.BANK_SWIFTCode
                                                                oPayment.BANK_BranchType = BabelFiles.BankBranchType.SWIFT
                                                                If Not ValidateBankID(oPayment.BANK_BranchType, oPayment.BANK_SWIFTCode, False, sErrorString) Then
                                                                    bx = AddToErrorArray("BENBANKID", LRS(15148) & vbCrLf & sErrorString, "1")
                                                                End If
                                                                sMandatory = "M"
                                                            Else
                                                                sMandatory = "N"
                                                            End If

                                                        Case "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "N"
                                                            ' 03.04.2019 - LocalCheck AU not longer allowed
                                                            bx = AddToErrorArray("PaymentType", LRS(15112) & vbCrLf & sErrorString, "1")
                                                            sErrorString = ""
                                                    End Select

                                                Case "BENBANKIDTYPE"
                                                    Select Case oPayment.BANK_BranchType
                                                        Case vbBabel.BabelFiles.BankBranchType.NoBranchType
                                                            sTmp = ""
                                                        Case vbBabel.BabelFiles.BankBranchType.SWIFT
                                                            sTmp = "SW"
                                                        Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                                            sTmp = "FW"
                                                        Case vbBabel.BabelFiles.BankBranchType.SortCode
                                                            sTmp = "SC"
                                                        Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                            sTmp = "BL"
                                                        Case vbBabel.BabelFiles.BankBranchType.Chips
                                                            sTmp = "CH"
                                                        Case vbBabel.BabelFiles.BankBranchType.MEPS
                                                            sTmp = "ME"
                                                        Case vbBabel.BabelFiles.BankBranchType.BSB
                                                            sTmp = "AU"
                                                        Case vbBabel.BabelFiles.BankBranchType.CC
                                                            sTmp = "CC"
                                                        Case vbBabel.BabelFiles.BankBranchType.ZASortCode
                                                            sTmp = "ZA"
                                                        Case Else
                                                            sTmp = ""
                                                    End Select
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            ' 29.12.2008 added new tests here based on Gordons rules, setup for DnBNOR US Penta format
                                                            If sCountryCode = "US" And (oPayment.PayType = "I" Or oPayment.PayCode = "403") Then ' 403=Internal
                                                                ' must have Swift or Fedwire if no intermediary bank
                                                                If EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) And EmptyString((oPayment.BANK_SWIFTCode)) And Len(oPayment.BANK_BranchNo) <> 9 Then
                                                                    ' Intermediary Bank, valid 9-digit transit routing number, valid SWIFT BIC
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15095) & vbCrLf & sErrorString, "1")
                                                                    ' 15095: Please include either Swiftcode, Fedwire or Intermediary bankcode
                                                                ElseIf Len(oPayment.BANK_SWIFTCodeCorrBank) > 6 And EmptyString((oPayment.BANK_SWIFTCode)) And EmptyString((oPayment.E_Account)) And oPayment.PayCode = "403" Then  ' 09.01.2009 added oPayment.PayCode = "403"
                                                                    ' For Internals (403) only, if Intermediary Bank field used (as in this case), valid SWIFT BIC or account number
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15096) & vbCrLf & sErrorString, "1")
                                                                    ' 15096: When using Intermediary bank, please specify either a valid BIC (SWIFT) or receivers accountnumber
                                                                ElseIf IsIBANNumber((oPayment.E_Account), True, False) > 0 And EmptyString((oPayment.BANK_SWIFTCode)) Then
                                                                    ' If IBAN used, BIC mandatory
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15097) & vbCrLf & sErrorString, "1")
                                                                    ' 15097: If IBAN used, BIC mandatory
                                                                End If
                                                                ' as pre 29.12.2008
                                                                'Special check for GEN domestic payments in US
                                                                'If sCountryCode = "US" And oPayment.PayCode <> "I" Then
                                                                ' 06.10.06 Changed by JanP - PayCode must be wrong to use here!!
                                                            ElseIf sCountryCode = "US" And oPayment.PayType <> "I" Then

                                                                If (oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Fedwire Or vbBabel.BabelFiles.BankBranchType.Chips Or vbBabel.BabelFiles.BankBranchType.US_ABA) And Len(oPayment.BANK_BranchNo) > 0 Then
                                                                    'OK, A valid BANK_BranchType and a BANK_BranchNo is stated
                                                                ElseIf oPayment.DnBNORTBIPayType = "GEN" And oPayment.BANK_SWIFTCode = "IRVTUS3N" Then
                                                                    ' added this test 07.11.2008, for Checks in US
                                                                    ' OK, checks should be Swift to IRVTUS3N
                                                                    ' 14.06.2016 added test for US check
                                                                ElseIf oPayment.DnBNORTBIPayType = "GEN" And oPayment.E_Account = "" And oPayment.Cheque <> BabelFiles.ChequeType.noCheque Then
                                                                    'OK with check in US!

                                                                    ' XNET 10.01.2013: After discussions with DNB, take away the tests below for US
                                                                    'ElseIf Len(oPayment.BANK_Name) > 0 Or Len(oPayment.BANK_Adr1) > 0 Or Len(oPayment.BANK_Adr2) > 0 Or Len(oPayment.BANK_Adr3) > 0 Then
                                                                    'Ok, A bankname/adress is stated
                                                                    ' XNET 10.01.2013 - but instead test for BIC
                                                                ElseIf oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT And (Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11) Then
                                                                    'OK, A valid Swift is stated
                                                                    ' XokNET 15.09.2011  - opened up for payments within DnBUS
                                                                ElseIf Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAUS" And Left$(oPayment.BANK_I_SWIFTCode, 6) = "DNBAUS" Then
                                                                    ' OK, inside DNBAUS
                                                                Else
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15089) & vbCrLf & sErrorString, "1")
                                                                    bx = AddToErrorArray("BENEFICIARYBANKADDRESS1", LRS(15089) & vbCrLf & sErrorString, "1")
                                                                    '15089: In a domestic General Payment in the United States, You must state either a valid bank-ID and the type of ID, or a bank name and adress.
                                                                End If
                                                            End If
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            If sTmp <> "SW" Then
                                                                sTmp = ""
                                                            End If
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            If sTmp <> "SW" Then
                                                                sTmp = ""
                                                            End If
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"
                                                            ' having problems if both Swift and Fedwire given, and Fedwire is not preceded with FW
                                                            ' this may be the case f.ex. in Telepay. Must test for Fedwire;
                                                            If oPayment.BANK_CountryCode = "CA" And oPayment.BANK_BranchType <> vbBabel.BabelFiles.BankBranchType.Fedwire Then
                                                                If Len(oPayment.BANK_BranchNo) = 9 Then
                                                                    ' XokNET 23.01.2012 - this is not correct
                                                                    'oPayment.BANK_BranchType = BankBranchType.Fedwire
                                                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.CC

                                                                    ' XOKNET 04.11.2010 added next lines
                                                                    ' If we had no initial BankBranchType, then had no BenBankIDType
                                                                    If EmptyString(sTmp) Then
                                                                        ' XNET 23.01.2012 - this is not correct
                                                                        'sTmp = "FW"
                                                                        sTmp = "CC"  ' but see for locallow further down !
                                                                    End If
                                                                End If
                                                            End If
                                                            ' XokNET 23.01l.2012 - this is not correct
                                                            'If oPayment.BANK_CountryCode = "CA" And oPayment.BANK_BranchType <> BankBranchType.Fedwire Then
                                                            If oPayment.BANK_CountryCode = "CA" And oPayment.BANK_BranchType <> vbBabel.BabelFiles.BankBranchType.CC Then
                                                                ' XOKNET 20.10.2010 - changed from LRS(15089) to LRS(15105)
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15105) & vbCrLf & sErrorString, "1")
                                                            End If
                                                            ' having problems if both Swift and BSB given, and BSB is not preceded with BS
                                                            ' this may be the case f.ex. in Telepay. Must test for BSB/ABA;
                                                            If oPayment.BANK_CountryCode = "AU" And oPayment.BANK_BranchType <> vbBabel.BabelFiles.BankBranchType.BSB Then
                                                                If Len(oPayment.BANK_BranchNo) = 6 Then
                                                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.BSB
                                                                    ' XNET 04.11.2010 added next lines
                                                                    ' If we had no initial BankBranchType, then had no BenBankIDType
                                                                    If EmptyString(sTmp) Then
                                                                        sTmp = "AU"
                                                                    End If
                                                                End If
                                                            End If
                                                            If oPayment.BANK_CountryCode = "AU" And oPayment.BANK_BranchType <> vbBabel.BabelFiles.BankBranchType.BSB Then
                                                                ' XNET 20.10.2010 - changed from LRS(15089) to LRS(15103)
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15103) & vbCrLf & sErrorString, "1")
                                                            End If
                                                            ' for Canada locals, we must set BENBANKIDTYPE to
                                                            ' SC for local low
                                                            ' CC for local high
                                                            If oPayment.BANK_CountryCode = "CA" And sPaymentType = "LOCALLOW" Then
                                                                sTmp = "SC"
                                                            End If
                                                            If oPayment.BANK_CountryCode = "CA" And sPaymentType = "LOCALHIGH" Then
                                                                sTmp = "CC"
                                                            End If
                                                        Case "LOCALCHECK" 'added 16.03.2010
                                                            sMandatory = "N"
                                                            sTmp = "" ' no value for LocalCheck in TBI-file for BENBANKIDTYPE
                                                    End Select

                                                Case "BENEFICIARYADDRESS1"
                                                    ' XNET 27.11.2012 added Trim$
                                                    sTmp = Trim$(oPayment.E_Name)
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.noCheque Then
                                                                ' XNET 27.11.2012 - for Finansielle betalinger m� navn v�re med
                                                                If oPayment.PayCode = "406" Then
                                                                    sMandatory = "M"
                                                                Else
                                                                    sMandatory = "O"
                                                                End If
                                                            Else
                                                                sMandatory = "M"
                                                            End If
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "M"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"

                                                    End Select

                                                Case "BENEFICIARYADDRESS2"
                                                    ' XNET 27.11.2012 added Trim$
                                                    sTmp = Trim$(oPayment.E_Adr1)
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            ' XNET 27.11.2012 - for Finansielle betalinger m� adresse v�re med, i NO, SE, FI, US
                                                            ' XNET 19.05.2013 - the tests below are not good - have added parenthesis
                                                            If oPayment.PayCode = "406" And _
                                                            (oPayment.BANK_AccountCountryCode = "SE" Or _
                                                            oPayment.BANK_AccountCountryCode = "FI" Or _
                                                            oPayment.BANK_AccountCountryCode = "US") Then
                                                                sMandatory = "M"
                                                            ElseIf oPayment.PayCode = "406" And _
                                                            (oPayment.BANK_AccountCountryCode = "NO" And _
                                                            oPayment.BANK_SWIFTCode = oPayment.E_Account) Then
                                                                ' Mandatory for NO debits financial payments only when E_Account is blank (then we have filled E_Account with Swiftcode)
                                                                sMandatory = "M"
                                                            Else
                                                                sMandatory = "O"
                                                            End If
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            'No further address information - according to specification for RefPayment from DnB.
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "M"  ' changed 05.09.2019 "O"

                                                    End Select

                                                Case "BENEFICIARYADDRESS3"
                                                    ' XNET 27.11.2012 added Trim$
                                                    sTmp = Trim$(oPayment.E_Adr2)
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"

                                                    End Select

                                                Case "BENEFICIARYADDRESS4"
                                                    ' New by JanP 22.10.04
                                                    ' Must reset sTmp, otherwise it has the value
                                                    ' from last field, which then may show up in BENEFICIARYADDRESS4
                                                    sTmp = ""

                                                    If Len(oPayment.E_Zip) > 0 Then
                                                        sTmp = oPayment.E_Zip
                                                        If Len(oPayment.E_City) > 0 Then
                                                            sTmp = sTmp & " - " & oPayment.E_City
                                                        End If
                                                    Else
                                                        If Len(oPayment.E_City) > 0 Then
                                                            sTmp = oPayment.E_City
                                                        End If
                                                    End If
                                                    If Len(oPayment.E_Adr3) > 0 Then
                                                        If Len(sTmp) > 0 Then
                                                            sTmp = sTmp & " , " & oPayment.E_Adr3
                                                        Else
                                                            sTmp = oPayment.E_Adr3
                                                        End If
                                                    End If
                                                    If Len(sTmp) > 35 Then
                                                        sTmp = Left(sTmp, 35)
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "N"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"

                                                    End Select

                                                Case "BENEFICIARYBANKADDRESS1"
                                                    sTmp = oPayment.BANK_Name
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 16.03.2010, changed 05.07.2010
                                                            ' 31.05.2010 - Mandatory for Locals
                                                            If EmptyString(sTmp) Then
                                                                ' XNET 19.10.2010
                                                                ' Changed from "-" to "."
                                                                ' sTmp = "-"
                                                                ' XokNET 23.01.2012
                                                                'If sFormat <> "TELEPAYPLUS" Then
                                                                If sFormat <> "TELEPAYPLUS" And sFormat <> "PAIN.001 (ISO 20022)" Then
                                                                    sTmp = "."
                                                                End If

                                                            End If
                                                            ' XNET 17.04.2012 - Optional according to Daniel Mah
                                                            'sMandatory = "M"
                                                            sMandatory = "O"
                                                        Case "LOCALCHECK" ' added 26.06.2010
                                                            sMandatory = "N"
                                                            sTmp = ""
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"

                                                    End Select

                                                Case "BENEFICIARYBANKADDRESS2"
                                                    sTmp = oPayment.BANK_Adr1
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "LOCALCHECK" ' added 26.06.2010
                                                            sMandatory = "N"
                                                            sTmp = ""
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "BENEFICIARYBANKADDRESS3"
                                                    sTmp = oPayment.BANK_Adr2
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "LOCALCHECK" ' added 26.06.2010
                                                            sMandatory = "N"
                                                            sTmp = ""
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "BENEFICIARYBANKADDRESS4"
                                                    sTmp = oPayment.BANK_Adr3
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 16.03.2010, changed 05.07.2010
                                                            sMandatory = "O"
                                                        Case "LOCALCHECK" ' added 26.06.2010
                                                            sMandatory = "N"
                                                            sTmp = ""
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "BENEFICIARYBANKCOUNTRYCODE"
                                                    ' XNET 27.04.2012
                                                    ' Dette m� v�re spr�yte galt !!!
                                                    ' Beneficiary er mottaker, da skal vi ikke ha som unde
                                                    'sTmp = oPayment.BANK_AccountCountryCode
                                                    ' men isteden
                                                    sTmp = oPayment.BANK_CountryCode
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "O"
                                                        Case "SAL"
                                                            sMandatory = "O"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 16.03.2010
                                                            sMandatory = "M"
                                                        Case "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "O"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select
                                                    ' 08.03.2017
                                                    ' added test for NOK-payments to PH (Philiphines) - not allowed ref Kjell Chr Martinsen
                                                    If sTmp = "PH" And (oPayment.MON_TransferCurrency = "NOK" Or (oPayment.MON_TransferCurrency = "" And oPayment.MON_InvoiceCurrency = "NOK")) Then
                                                        ' reject, bring to frmReject
                                                        ' NOK payments to Philippines are not allowed
                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15195) & vbCrLf & sErrorString, "1")

                                                    End If

                                                Case "INFOBENBANKCODE"
                                                    ' New 25.11.04
                                                    'If oPayment.NOTI_NotificationParty = 1 Then  ' Info to payers bank (DnB)
                                                    ' ENDRES - etter m�te med Sigurd 24.11.2008 skal vi legge inn i
                                                    ' INFOBENBANKCODE hvis meldingen er en av f�lgende ord:
                                                    ' ..... Sigurd sender liste (bl.a. REC/KONCERN
                                                    ' Hvis /REC/KONCERN, fjern ledende /

                                                    ' new 25.11.2008;
                                                    If oPayment.NOTI_NotificationParty = 1 Then
                                                        sTmp = oPayment.NOTI_NotificationMessageToBank

                                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "/REC/KONCERN") > 0 Then
                                                        ' change from /REC/KONCERN to REC/KONCERN
                                                        sTmp = "REC/KONCERN"

                                                        ' 10.06.2010 - rearranged codes, to be able to put
                                                        '              first part ONLY into INFOBENBANKCODE
                                                        '----------------------------------------------------------------
                                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "ACC") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "INS") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "REC") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "INT") > 0 Then
                                                        sTmp = Left(oPayment.NOTI_NotificationMessageToBank, 3)
                                                        ' XNET 30.11.2010
                                                        ' Have to take away ACC or INS or .. not to have it ending up twice
                                                        ' in the info to bank-field (:72: in Swift)
                                                        oPayment.NOTI_NotificationMessageToBank = Mid$(oPayment.NOTI_NotificationMessageToBank, 4)

                                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "BONL") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "CHQB") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "CMSW") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "CMTO") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "CM2B") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "CORT") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "HOLD") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "INTC") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "NETS") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "OTHR") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "PHOB") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "PHOI") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "PHON") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "REPA") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "RTGS") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "SDVA") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "TELB") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "TELE") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "TELI") > 0 Or InStr(oPayment.NOTI_NotificationMessageToBank, "URGP") > 0 Then
                                                        sTmp = Left(oPayment.NOTI_NotificationMessageToBank, 4)
                                                        ' XNET 30.11.2010
                                                        ' Have to take away BONL or CHQB or .. not to have it ending up twice
                                                        ' in the info to bank-field (:72: in Swift)
                                                        oPayment.NOTI_NotificationMessageToBank = Mid$(oPayment.NOTI_NotificationMessageToBank, 5)

                                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "PAYACH") > 0 Then
                                                        sTmp = Left(oPayment.NOTI_NotificationMessageToBank, 6)

                                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "REC/NAT") Then
                                                        sTmp = Left(oPayment.NOTI_NotificationMessageToBank, 7)
                                                        ' XNET 30.11.2010
                                                        ' Have to take away REC/NAT or .. not to have it ending up twice
                                                        ' in the info to bank-field (:72: in Swift)
                                                        oPayment.NOTI_NotificationMessageToBank = Mid$(oPayment.NOTI_NotificationMessageToBank, 8)

                                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "REC/KONCERN") Then
                                                        sTmp = Left(oPayment.NOTI_NotificationMessageToBank, 11)
                                                        ' XNET 30.11.2010
                                                        ' Have to take away REC/KONCERN or .. not to have it ending up twice
                                                        ' in the info to bank-field (:72: in Swift)
                                                        oPayment.NOTI_NotificationMessageToBank = Mid$(oPayment.NOTI_NotificationMessageToBank, 12)

                                                        '                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "ACC") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "BONL") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "CHQB") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "CMSW") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "CMTO") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "CM2B") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "CORT") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "HOLD") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "INS") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "INT") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "INTC") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "NETS") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "OTHR") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "PAYACH") > 0 Then
                                                        '                                        ' must split if because of to long line
                                                        '                                        sTmp = Trim$(oPayment.NOTI_NotificationMessageToBank)
                                                        '
                                                        '                                    ElseIf InStr(oPayment.NOTI_NotificationMessageToBank, "PHOB") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "PHOI") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "PHON") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "REC") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "REC/KONCERN") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "REC/NAT") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "REPA") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "RTGS") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "SDVA") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "TELB") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "TELE") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "TELI") > 0 Or _
                                                        ''                                        InStr(oPayment.NOTI_NotificationMessageToBank, "URGP") > 0 Then
                                                        '                                        sTmp = Trim$(oPayment.NOTI_NotificationMessageToBank)
                                                    Else
                                                        sTmp = ""
                                                    End If
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW" ' added 16.03.2010
                                                            sMandatory = "N"
                                                            ' 12.10.2016 - this is for OLD tbi only. remove
                                                            'sTmp = "PAYACH"
                                                            ' XokNET 23.01.2012
                                                            'If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                            '    oPayment.NOTI_NotificationMessageToBank = sTmp
                                                            'End If
                                                        Case "LOCALHIGH" ' added 16.03.2010
                                                            sMandatory = "N"
                                                            ' 12.10.2016 - this is for OLD tbi only. remove
                                                            'sTmp = "SDVA"
                                                            'If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                            'oPayment.NOTI_NotificationMessageToBank = sTmp
                                                            'End If
                                                        Case "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "N"
                                                            ' 12.10.2016 - this is for OLD tbi only. remove
                                                            'sTmp = "CHQB"
                                                            '' XokNET 23.01.2012
                                                            'If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                            '    oPayment.NOTI_NotificationMessageToBank = sTmp
                                                            'End If
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "INFOTOBENBANK"
                                                    sTmp = oPayment.NOTI_NotificationMessageToBank
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK" ' added 16.03.2010
                                                            sTmp = "" ' not to be filled in for local !
                                                            sMandatory = "N"
                                                        Case "IPBLOW", "IPBHIGH"
                                                            sMandatory = "N"
                                                    End Select

                                                Case "INFOTOBENEFICIARY"
                                                    sTmp = ""
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        '29.12.2008 - Why do we add & " " at end?
                                                        ' This will cause overflow of info for formats supporing exactly
                                                        ' 140 chars of freetext !! Like US Penta
                                                        ' JanP Suggests that we change this to
                                                        sTmp = sTmp & Trim(oFreeText.Text)
                                                        'sTmp = sTmp & Trim$(oFreetext.Text) & " "
                                                    Next oFreeText
                                                    sTmp = RTrim(sTmp)
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                            ' 21.10.2009 -
                                                            ' According to Bjarne in DnBNOR we can have as much text as we like (?)
                                                            'If sFormat = "TELEPAYPLUS" Then
                                                            ' will we need to specify maxlen for Pain.001 ?
                                                            If sFormat = "TELEPAYPLUS" Or sFormat = "PAIN.001 (ISO 20022)" Then
                                                                lVariableLength = 999999
                                                            Else
                                                                If sCountryCode = "DK" Then
                                                                    If oPayment.MON_InvoiceCurrency = "DKK" Then
                                                                        'UDUS
                                                                        lVariableLength = 1435
                                                                    Else
                                                                        lVariableLength = 140
                                                                    End If
                                                                Else
                                                                    lVariableLength = 140
                                                                End If
                                                            End If
                                                            If Len(sTmp) > lVariableLength Then
                                                                If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                Else
                                                                    sTmp = Left(sTmp, lVariableLength)
                                                                End If
                                                            End If


                                                        Case "ACH"
                                                            sMandatory = "O"
                                                        Case "SAL"
                                                            sMandatory = "O"
                                                        Case "FIK"
                                                            Select Case Left(oInvoice.Unique_Id, 2)
                                                                Case "01"
                                                                    sMandatory = "O"
                                                                Case "04"
                                                                    sMandatory = "N"
                                                                Case "71"
                                                                    sMandatory = "N"
                                                                Case "73"
                                                                    sMandatory = "O"
                                                                Case "75"
                                                                    sMandatory = "N"
                                                                Case Else

                                                            End Select
                                                        Case "REF"
                                                            ' 20.03.2009 next 5 lines moved here from OwnReference
                                                            'sMandatory = "O"
                                                            'sTmp = Trim$(oInvoice.Unique_Id)
                                                            'If Not ValidateReference(sTmp, sErrorString) Then
                                                            '    bx = AddToErrorArray(UCase(sVariableName), sErrorString, "1")
                                                            'End If
                                                            'sMandatory = "M"

                                                            ' 19.02.2010
                                                            ' REF: After discussion with Sigurd Wiig, and case Actavis FI,
                                                            ' it is correct to fetch from oInvoice.Unique_Id and put into Payers Reference !!!
                                                            ' and from oPayment.REF_Own and put into InfoToBeneficiary  !!!

                                                            sTmp = RTrim(oPayment.REF_Own)
                                                            sMandatory = "O"
                                                        Case "LOCALLOW" ' added 16.03.2010
                                                            sMandatory = "O"
                                                            If oPayment.BANK_CountryCode = "CA" Then
                                                                sMandatory = "N"
                                                                lVariableLength = 0
                                                                If Len(sTmp) > lVariableLength Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    Else
                                                                        sTmp = ""
                                                                    End If
                                                                End If
                                                            ElseIf oPayment.BANK_CountryCode = "AU" Then
                                                                sMandatory = "O"
                                                                lVariableLength = 18
                                                                If Len(sTmp) > lVariableLength Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    Else
                                                                        sTmp = Left(sTmp, 18)
                                                                    End If
                                                                End If
                                                            End If
                                                        Case "LOCALHIGH", "LOCALCHECK" ' added 16.03.2010
                                                            sMandatory = "O"
                                                            lVariableLength = 140
                                                            If Len(sTmp) > lVariableLength Then
                                                                If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                Else
                                                                    sTmp = Left(sTmp, 140)
                                                                End If
                                                            End If
                                                        Case "IPBLOW"
                                                            If oPayment.BANK_CountryCode = "SG" Then
                                                                sMandatory = "N"
                                                                lVariableLength = 140
                                                                If Len(sTmp) > lVariableLength Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    Else
                                                                        sTmp = Left(sTmp, 140)
                                                                    End If
                                                                End If
                                                            ElseIf oPayment.BANK_CountryCode = "AU" Then
                                                                sMandatory = "O"
                                                                lVariableLength = 18
                                                                If Len(sTmp) > lVariableLength Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    Else
                                                                        sTmp = Left(sTmp, 18)
                                                                    End If
                                                                End If
                                                            End If
                                                        Case "IPBHIGH"
                                                            If oPayment.BANK_CountryCode = "SG" Then
                                                                sMandatory = "N"
                                                                lVariableLength = 140
                                                                If Len(sTmp) > lVariableLength Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    Else
                                                                        sTmp = Left(sTmp, 140)
                                                                    End If
                                                                End If
                                                            ElseIf oPayment.BANK_CountryCode = "AU" Then
                                                                sMandatory = "O"
                                                                lVariableLength = 140
                                                                If Len(sTmp) > lVariableLength Then
                                                                    If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim(Str(lVariableLength))), "0")
                                                                    Else
                                                                        sTmp = Left(sTmp, 140)
                                                                    End If
                                                                End If
                                                            End If

                                                    End Select

                                                    ' XNET 22.10.2013 -
                                                    ' Added test for freetext in FI (Finland)
                                                    ' Max 140 char of text
                                                    If oPayment.BANK_AccountCountryCode = "FI" Then
                                                        If dbSelectAccountCurrency(oPayment.I_Account) = "EUR" And _
                                                        oPayment.MON_InvoiceCurrency = "EUR" And _
                                                        IsIBANNumber(oPayment.E_Account, False, False) And _
                                                        oPayment.MON_ChargeMeAbroad = False And _
                                                        oPayment.MON_ChargeMeDomestic = True Then
                                                            ' Check if len of freetext > 140;
                                                            If Len(sTmp) > 140 Then
                                                                If bFirstValidation And bUseFrmCorrect And Not bShowJustErrors Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15080, sTmp, UCase(sVariableName)), "%3", Trim$(Str(lVariableLength))), "0")
                                                                Else
                                                                    sTmp = Left$(sTmp, 140)
                                                                    ' keep 140 chars only;

                                                                End If
                                                            End If
                                                        End If
                                                    End If

                                                Case "INTBANKID"
                                                    If oPayment.BANK_BranchTypeCorrBank > vbBabel.BabelFiles.BankBranchType.SWIFT Then
                                                        sTmp = oPayment.BANK_BranchNoCorrBank
                                                    Else
                                                        sTmp = oPayment.BANK_SWIFTCodeCorrBank
                                                    End If

                                                    ''''sTmp = oPayment.BANK_SWIFTCodeCorrBank

                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            If Len(sTmp) > 0 Then
                                                                If Not ValidateBankID((oPayment.BANK_BranchTypeCorrBank), sTmp, True, sErrorString) Then
                                                                    If oPayment.BANK_BranchTypeCorrBank > vbBabel.BabelFiles.BankBranchType.SWIFT Then
                                                                        bx = AddToErrorArray("INTBANKID_BRANCH", LRS(15072) & vbCrLf & sErrorString, "1")
                                                                    Else
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15072) & vbCrLf & sErrorString, "1")
                                                                    End If
                                                                    sErrorString = ""
                                                                End If
                                                            End If
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "INTBANKIDTYPE"
                                                    Select Case oPayment.BANK_BranchTypeCorrBank
                                                        Case vbBabel.BabelFiles.BankBranchType.NoBranchType
                                                            sTmp = ""
                                                        Case vbBabel.BabelFiles.BankBranchType.SWIFT
                                                            sTmp = "SW"
                                                        Case vbBabel.BabelFiles.BankBranchType.Fedwire
                                                            sTmp = "FW"
                                                        Case vbBabel.BabelFiles.BankBranchType.SortCode
                                                            sTmp = "SC"
                                                        Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                                                            sTmp = "BL"
                                                        Case vbBabel.BabelFiles.BankBranchType.Chips
                                                            sTmp = "CH"
                                                        Case vbBabel.BabelFiles.BankBranchType.MEPS
                                                            sTmp = "ME"
                                                        Case vbBabel.BabelFiles.BankBranchType.CC
                                                            sTmp = "CC"
                                                        Case vbBabel.BabelFiles.BankBranchType.ZASortCode
                                                            sTmp = "ZA"
                                                        Case Else
                                                            sTmp = ""
                                                    End Select
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "INTBANKNAMEADDRESS1"
                                                    sTmp = oPayment.BANK_NameAddressCorrBank1
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "INTBANKNAMEADDRESS2"
                                                    sTmp = oPayment.BANK_NameAddressCorrBank2
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "INTBANKNAMEADDRESS3"
                                                    sTmp = oPayment.BANK_NameAddressCorrBank3
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "INTBANKNAMEADDRESS4"
                                                    sTmp = oPayment.BANK_NameAddressCorrBank4
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            sMandatory = "O"
                                                        Case "ACH"
                                                            sMandatory = "N"
                                                        Case "SAL"
                                                            sMandatory = "N"
                                                        Case "FIK"
                                                            sMandatory = "O"
                                                        Case "REF"
                                                            sMandatory = "O"
                                                        Case "LOCALLOW", "LOCALHIGH", "LOCALCHECK", "IPBLOW", "IPBHIGH"
                                                            sMandatory = "O"
                                                    End Select

                                                Case "TOACCOUNT"
                                                    sTmp = oPayment.E_Account
                                                    Select Case sPaymentType
                                                        Case "GEN"
                                                            If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.noCheque Then
                                                                ' added 05.07.2010 - Norway domestic can be without account, then check!
                                                                If oPayment.BANK_AccountCountryCode = "NO" And oPayment.BANK_CountryCode = "NO" Then
                                                                    sMandatory = "O"
                                                                    ' added 08.06.2016 - also DK and SE
                                                                ElseIf oPayment.BANK_AccountCountryCode = "DK" And oPayment.BANK_CountryCode = "DK" Then
                                                                    sMandatory = "O"
                                                                ElseIf oPayment.BANK_AccountCountryCode = "SE" And oPayment.BANK_CountryCode = "SE" Then
                                                                    sMandatory = "O"
                                                                Else
                                                                    sMandatory = "M"
                                                                End If
                                                            Else
                                                                sMandatory = "O"
                                                            End If
                                                            ' XNET 23.10.2013
                                                            ' If ToOwnAccount inside DNB, BBAN Only!
                                                            If Left$(oPayment.BANK_SWIFTCode, 4) = "DNBA" And (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                                                                If IsIBANNumber(oPayment.E_Account, True, False, sErrorString) = Scripting.Tristate.TristateTrue Then
                                                                    ' BBAN Only !
                                                                    AddToErrorArray(UCase(sVariableName), LRS(15094) & vbCrLf & sErrorString, "0")
                                                                    sErrorString = vbNullString
                                                                End If
                                                                ' Added 17.06.2008
                                                                ' Must have IBAN for EUR-payments to certain countries;
                                                            ElseIf oPayment.MON_TransferCurrency = "EUR" Then
                                                                ' Check receivers country;
                                                                ' If empty, use countrycode from Swift
                                                                If EmptyString((oPayment.E_CountryCode)) Then
                                                                    If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT Then
                                                                        oPayment.E_CountryCode = Mid(oPayment.BANK_SWIFTCode, 5, 2)
                                                                    End If
                                                                End If

                                                                ' XNET 22.10.2013
                                                                '                                            If oPayment.E_CountryCode = "BE" Or oPayment.E_CountryCode = "FI" Or _
                                                                '                                                oPayment.E_CountryCode = "FR" Or oPayment.E_CountryCode = "GR" Or _
                                                                '                                                oPayment.E_CountryCode = "IE" Or oPayment.E_CountryCode = "IT" Or _
                                                                '                                                oPayment.E_CountryCode = "LU" Or oPayment.E_CountryCode = "NL" Or _
                                                                '                                                oPayment.E_CountryCode = "PT" Or oPayment.E_CountryCode = "SI" Or _
                                                                '                                                oPayment.E_CountryCode = "ES" Or oPayment.E_CountryCode = "DE" Or _
                                                                '                                                oPayment.E_CountryCode = "AT" Or oPayment.E_CountryCode = "CY" Or _
                                                                '                                                oPayment.E_CountryCode = "MT" Then
                                                                If IsSEPACountry(oPayment.E_CountryCode) Then

                                                                    ' 20.04.2016 - Do not test for check !
                                                                    If Not EmptyString(oPayment.E_Account) Then
                                                                        '                                                ' Must have IBAN for those countries
                                                                        If IsIBANNumber(oPayment.E_Account, True, False, sErrorString) = Scripting.Tristate.TristateFalse Then

                                                                            ' XNET 12.08.2013 - added new test from Kjell Christian at DNB, based on new set of rules from 01.02.2014
                                                                            If oPayment.DATE_Payment > "20140201" And oPayment.E_CountryCode = "DE" And oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Bankleitzahl And oPayment.Priority Then
                                                                                '  DTA payments - reg code 68 - Urgent shall be required if BBAN and BLZ is sent from the client. (Payments with BBAN and BLZ shall be stopped if Urgent is not present)
                                                                                ' So, BBAN is allowed in this situation!
                                                                            Else
                                                                                ' as pres 12.08.2013
                                                                                AddToErrorArray(UCase(sVariableName), LRS(15094) & vbCrLf & sErrorString, "0")
                                                                                sErrorString = vbNullString
                                                                            End If
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If

                                                            ' 24.06.2009
                                                            ' Added tests for ToAccount in DK, used for Telepay+ purpose
                                                            'If sCountryCode = "DK" Then
                                                            ' 08.07.2009 ERROR, must test on E_Countrycode, or if no countrycode (Domestic) then sCountrycode !!!!
                                                            ' XNET 30.04.2012 - do not test for Europark
                                                            If sCountryCode = "DK" And oPayment.PayType <> "I" And oBabel.Special <> "EUROPARK" Then
                                                                ' 14.09.2009 - do not warn if check!
                                                                If Not (oPayment.PayCode = "160" And EmptyString((oPayment.E_Account))) Then
                                                                    ' 08.06.2016 do not test for FIK
                                                                    If Not (oPayment.PayCode = "371" Or oPayment.PayCode = "373" Or oPayment.PayCode = "375") Then
                                                                        ' Must be between 11 and 14 digits
                                                                        If Len(sTmp) > 14 Or Len(sTmp) < 11 Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15098), "1")
                                                                            ' 15098: Det m� v�re mellom 11 og 14 siffer i mottakerkonto i Danmark
                                                                        End If
                                                                    End If
                                                                    ' 02.07.2009
                                                                    ' Added test of blanks in receivers accountno
                                                                    If Len(sTmp) <> Len(Replace(sTmp, " ", "")) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If
                                                                    ' Numbers only
                                                                    If Not IsNumeric(sTmp) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If
                                                                End If
                                                            End If
                                                            If sCountryCode = "SG" And oPayment.E_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" Then
                                                                ' XokNET 11.09.2014 In G3 Singapore accounts can be 34
                                                                If Len(sTmp) > 34 Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15109), "1")
                                                                    ' 15109: Max 34 digits in receivers account in Singapore
                                                                End If
                                                            End If
                                                            ' XNET 22.10.2013 -
                                                            ' Test for Checks Germany, not allowed
                                                            If sCountryCode = "DE" And EmptyString(oPayment.E_Account) Then
                                                                ' Checkpayments not allowed in Germany
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15186), "1")
                                                            End If

                                                            ' 11.03.2016 Changed
                                                            ' Checks inside Denmark
                                                            ' changed 20.10.2016 - all cheques to Danish receovers are rejected;
                                                            'If sCountryCode = "DK" And EmptyString(oPayment.E_Account) And oPayment.MON_InvoiceCurrency = "DKK" Then
                                                            If oPayment.E_CountryCode = "DK" And EmptyString(oPayment.E_Account) Then
                                                                ' test also for receivers countrycode in DK (or empty)
                                                                'If oPayment.E_CountryCode = "DK" Then
                                                                If oPayment.DATE_Payment > "20161231" Then
                                                                    ' Checkpayments not allowed to Danish receivers (after 01.01.2017
                                                                    sMandatory = "O"  ' not to get new errormsg
                                                                    bx = AddToErrorArray(UCase(sVariableName), LRS(15186), "1")
                                                                End If
                                                                'End If
                                                                ' If countrycode NOT given, errormark receivers countrycode, with possiblity to set it
                                                                ' removed test 20.10.2016
                                                                'If oPayment.E_CountryCode = "" Then
                                                                '    If oPayment.DATE_Payment > "20161231" Then
                                                                '        ' Checkpayments not allowed to Danish receivers (after 01.01.2017
                                                                '        sMandatory = "O"  ' not to get new errormsg
                                                                '        bx = AddToErrorArray("E_COUNTRYCODE", LRS(15186), "1")
                                                                '    End If
                                                                'End If

                                                            End If



                                                            ' XokNET 23.12.2013 og 24.02.2014
                                                            ' Must have IBAN to Finland (also domestic
                                                            If oPayment.BANK_CountryCode = "FI" And Not EmptyString(oPayment.E_Account) Then
                                                                ' Gjelder ikke for internoverf�ringer
                                                                If Not (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                                                                    If Not IsIBANNumber(oPayment.E_Account, True, False) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15188), "1")
                                                                    End If
                                                                End If
                                                            End If

                                                            ' XokNET 23.12.2013
                                                            ' If postgiro in Sweden, max 9 digits
                                                            If Len(oPayment.E_Account) > 3 Then ' added If 12.02.2013
                                                                If sCountryCode = "SE" And (oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Or Mid$(oPayment.E_Account, Len(oPayment.E_Account) - 1, 1) = "-") Then
                                                                    If Len(Replace(oPayment.E_Account, "-", "")) > 9 Then
                                                                        ' XokNET 03.10.2014 changed call to addtoerrorarray
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15189, "Postgiro", "9") & " " & oPayment.E_Account, "1")
                                                                    End If
                                                                End If
                                                            End If

                                                            ' XokNET 23.12.2013
                                                            ' If bankgiro in Sweden, max 8 digits
                                                            If Not EmptyString(oPayment.E_Account) Then ' added If 12.02.2013
                                                                If Len(oPayment.E_Account) > 5 Then  ' If shorter than 5, bumbs in next If !!
                                                                    If sCountryCode = "SE" And (oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Or Mid$(oPayment.E_Account, Len(oPayment.E_Account) - 5, 1) = "-") Then
                                                                        If Len(Replace(oPayment.E_Account, "-", "")) > 8 Then
                                                                            ' XNET 03.10.2014 changed call to addtoerrorarray
                                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15189, "Bankgiro", "8") & " " & oPayment.E_Account, "1")
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If

                                                        Case "ACH"
                                                            Select Case sCountryCode
                                                                Case "GB"
                                                                    lVariableLength = 8
                                                                Case "US"
                                                                    lVariableLength = 17
                                                                Case "SG"
                                                                    sTmp = Replace(sTmp, " ", "")
                                                                    sTmp = Replace(sTmp, "-", "")
                                                                    If Not IsNumeric(sTmp) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15075, sTmp, sVariableName), "1")
                                                                        'The number " & sValue & " used in the variable " & sVariableName  "," & vbCrLf & "contains illegal characters."
                                                                    End If
                                                                    If oPayment.E_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" Then
                                                                        ' XokNET 11.09.2014 In G3 Singapore accounts can be 34
                                                                        If Len(sTmp) > 34 Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15109), "1")
                                                                            ' 15109: Max 34 digits in receivers account in Singapore
                                                                        End If
                                                                    End If

                                                                    lVariableLength = 11
                                                                Case "DE"
                                                                    lVariableLength = 10
                                                                Case "SE"
                                                                    lVariableLength = 35
                                                                Case "DK"
                                                                    lVariableLength = 35
                                                                    ' 24.06.2009
                                                                    ' Added tests for ToAccount in DK, used for Telepay+ purpose
                                                                    ' Must be between 11 and 14 digits
                                                                    If Len(sTmp) > 14 Or Len(sTmp) < 11 Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15098), "1")
                                                                        ' 15098: Det m� v�re mellom 11 og 14 siffer i mottakerkonto i Danmark
                                                                    End If
                                                                    ' 02.07.2009
                                                                    ' Added test of blanks in receivers accountno
                                                                    If Len(sTmp) <> Len(Replace(sTmp, " ", "")) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If
                                                                    ' Numbers only
                                                                    If Not IsNumeric(sTmp) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If

                                                                Case "NO"
                                                                    lVariableLength = 16
                                                                Case "FI"
                                                                    sTmp = Replace(sTmp, " ", vbNullString)
                                                                    sTmp = Replace(sTmp, "-", vbNullString)
                                                                    ' XokNET 23.12.2013
                                                                    ' Must have IBAN to Finland (also domestic
                                                                    If Not IsIBANNumber(oPayment.E_Account, True, False) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15188), "1")
                                                                    End If

                                                                    ' XNET 27.06.2011
                                                                    ' May use IBAN - remove test !
                                                                    'If Not IsNumeric(sTmp) Then
                                                                    '    bx = AddToErrorArray(UCase(sVariableName), LRS(15075, sTmp, sVariableName), "1")
                                                                    '    'The number " & sValue & " used in the variable " & sVariableName  "," & vbCrLf & "contains illegal characters."
                                                                    'End If
                                                                    'lVariableLength = 14   ' DnBNOR says 11, but format says 14!
                                                            End Select
                                                            'If Len(oPayment.E_Account) > lVariableLength Then
                                                            '    bx = AddToErrorArray(UCase(sVariableName), Replace(LRS(15076, (oPayment.E_Account), sVariableName), "%3", Trim(Str(lVariableLength))), "1")
                                                            '    '15076: The number of digits in %1 used in the variable %2, is too many. Maximiulength is %3.
                                                            '    'bx = AddToErrorArray(UCase(sVariableName), LRS(15073, sVariableName, Trim$(Str(lVariableLength))), "1")
                                                            '    'sVariableName & " have to be exactly " & Trim$(Str(lVariableLength)) & " long."
                                                            'End If
                                                            'sMandatory = "M"
                                                        Case "SAL"
                                                            Select Case sCountryCode
                                                                Case "GB"
                                                                    lVariableLength = 8
                                                                Case "US"
                                                                    lVariableLength = 16
                                                                Case "SG"
                                                                    sTmp = Replace(sTmp, " ", "")
                                                                    sTmp = Replace(sTmp, "-", "")
                                                                    If Not IsNumeric(sTmp) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15075, sTmp, sVariableName), "1")
                                                                        'The number " & sValue & " used in the variable " & sVariableName  "," & vbCrLf & "contains illegal characters."
                                                                    End If
                                                                    ' XokNET 11.09.2014 In G3 Singapore accounts can be 34
                                                                    If Len(sTmp) > 34 Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15109), "1")
                                                                        ' 15109: Max 34 digits in receivers account in Singapore
                                                                    End If
                                                                    lVariableLength = 11
                                                                Case "DE"
                                                                    lVariableLength = 10
                                                                Case "SE"
                                                                    lVariableLength = 35
                                                                Case "DK"
                                                                    lVariableLength = 35
                                                                    ' 24.06.2009
                                                                    ' Added tests for ToAccount in DK, used for Telepay+ purpose
                                                                    ' Must be between 11 and 14 digits
                                                                    ' Must be between 11 and 14 digits
                                                                    If Len(sTmp) > 14 Or Len(sTmp) < 11 Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15098), "1")
                                                                        ' 15098: Det m� v�re mellom 11 og 14 siffer i mottakerkonto i Danmark
                                                                    End If
                                                                    ' 02.07.2009
                                                                    ' Added test of blanks in receivers accountno
                                                                    If Len(sTmp) <> Len(Replace(sTmp, " ", "")) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If
                                                                    ' Numbers only
                                                                    If Not IsNumeric(sTmp) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If

                                                                Case "NO"
                                                                    lVariableLength = 16
                                                                Case "FI"
                                                                    sTmp = Replace(sTmp, " ", vbNullString)
                                                                    sTmp = Replace(sTmp, "-", vbNullString)

                                                                    ' XokNET 23.12.2013
                                                                    ' Must have IBAN to Finland (also domestic
                                                                    If Not IsIBANNumber(oPayment.E_Account, True, False) Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15188), "1")
                                                                    End If

                                                                    ' XNET 27.06.2011
                                                                    ' May use IBAN - remove test !
                                                                    'If Not IsNumeric(sTmp) Then
                                                                    '    bx = AddToErrorArray(UCase(sVariableName), LRS(15075, sTmp, sVariableName), "1")
                                                                    '    'The number " & sValue & " used in the variable " & sVariableName  "," & vbCrLf & "contains illegal characters."
                                                                    'End If
                                                                    'lVariableLength = 14   ' DnBNOR says 11, but format says 14!
                                                            End Select
                                                            'If Len(oPayment.E_Account) > lVariableLength Then
                                                            'bx = AddToErrorArray(UCase(sVariableName), LRS(15073, sVariableName, Trim(Str(lVariableLength))), "1")
                                                            'End If
                                                            sMandatory = "M"
                                                        Case "FIK"
                                                            ' XNET 25.06.2012 - Europark vil ikke ha validering av FIK, ref Thomas i DK.
                                                            If oBabel.Special <> "EUROPARK" Then
                                                                If Len(sTmp) > 0 Then
                                                                    'For some types there are no Betaleridentifikation
                                                                    If Len(oInvoice.Unique_Id) > 2 Then
                                                                        sTmp = Left(oInvoice.Unique_Id, 2) & "-" & Mid(oInvoice.Unique_Id, 3) & "-" & sTmp
                                                                    Else
                                                                        sTmp = Left(oInvoice.Unique_Id, 2) & "-" & sTmp
                                                                    End If

                                                                    ' XokNET  22.04.2015
                                                                    ' Det er rapportert fra DNB om at det ligger FI hos kunder p� kontonummer
                                                                    ' Det pr�ver vi � stoppe her;
                                                                    ' Numbers only
                                                                    If InStr(oPayment.E_Account, "FI") > 0 Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15099), "1")
                                                                        ' Kun tillatt med siffer i danske kontonummer
                                                                    End If

                                                                Else
                                                                    If Len(oInvoice.Unique_Id) > 0 Then
                                                                        sTmp = Left(oInvoice.Unique_Id, 2) & "-" & Mid(oInvoice.Unique_Id, 3)
                                                                    Else
                                                                        sTmp = ""
                                                                    End If
                                                                End If
                                                                If Not ValidateFI(sTmp, sKortartKode, sBetalerIdent, sKreditorNo, vbBabel.BabelFiles.FileType.TBIXML, sErrorString, True) Then
                                                                    bx = AddToErrorArray(UCase(sVariableName), sErrorString, "1")
                                                                End If
                                                                sMandatory = "M"
                                                            Else
                                                                sMandatory = "O"
                                                            End If

                                                        Case "REF"
                                                            ' XokNET 23.12.2013
                                                            ' Must have IBAN to Finland (also domestic
                                                            ' 11.03.2016 Changed second parameter to False (do not validate)
                                                            If Not IsIBANNumber(oPayment.E_Account, False, False) Then
                                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15188), "1")
                                                            End If
                                                            sMandatory = "M"
                                                        Case "LOCALLOW", "LOCALHIGH" ' added 30.03.2010
                                                            Select Case sCountryCode
                                                                Case "CA"
                                                                    If Len(oPayment.E_Account) < 5 Or Len(oPayment.E_Account) > 12 Then
                                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15073, sVariableName, "5 - 12"), "1")
                                                                    End If
                                                                Case "AU"
                                                                    'If Len(oPayment.E_Account) < 1 Or Len(oPayment.E_Account) > 34 Then
                                                                    ' XokNET 08.09.2011 Changed to max 9 digits, see worddoc from Kjell Chr.
                                                                    ' XokNET 23.09.2011 - difference between LOCALLOW and LOCALHIGH
                                                                    If sPaymentType = "LOCALLOW" Then
                                                                        If Len(oPayment.E_Account) < 1 Or Len(oPayment.E_Account) > 9 Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15073, sVariableName, "1 - 9"), "1")
                                                                        End If
                                                                    Else
                                                                        ' LOCALHIGH
                                                                        If Len(oPayment.E_Account) < 1 Or Len(oPayment.E_Account) > 34 Then
                                                                            bx = AddToErrorArray(UCase(sVariableName), LRS(15073, sVariableName, "1 - 34"), "1")
                                                                        End If
                                                                    End If

                                                            End Select
                                                            sMandatory = "M"

                                                        Case "LOCALCHECK" ' added 30.03.2010
                                                            sMandatory = "N"
                                                    End Select

                                                Case Else
                                                    sMandatory = "O"
                                                    sTmp = ""

                                            End Select
                                            Select Case sMandatory
                                                Case "M"
                                                    If Len(sTmp) = 0 Then
                                                        bx = AddToErrorArray(UCase(sVariableName), LRS(15061, sVariableName), "1")
                                                        'err.Raise 1, , sVariableName & " is mandatory, but the variable has no value."
                                                    End If
                                                    'If TBIOElementOK(sTmp, nodDetailsSeqList.item(lNodeListCounter), sVariableName, True, sFormat) Then
                                                    'newEle = docXMLExport.createElement(sVariableName)
                                                    'newEle.nodeTypedValue = sTmp
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    'nodDetailsNode.appendChild(newEle)
                                                    'End If

                                                Case "O"
                                                    'If TBIOElementOK(sTmp, nodDetailsSeqList.item(lNodeListCounter), sVariableName, False, sFormat) Then
                                                    'newEle = docXMLExport.createElement(sVariableName)
                                                    'newEle.nodeTypedValue = sTmp
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    'nodDetailsNode.appendChild(newEle)

                                                    'End If

                                                Case "N"
                                                    sTmp = ""
                                                    'If TBIOElementOK(sTmp, nodDetailsSeqList.item(lNodeListCounter), sVariableName, False, sFormat) Then
                                                    '    newEle = docXMLExport.createElement(sVariableName)
                                                    '    newEle.nodeTypedValue = sTmp
                                                    '    'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    '    nodDetailsNode.appendChild(newEle)
                                                    'End If
                                            End Select

                                        Next lNodeListCounter
                                        '=========================================================================================
                                        ' Ends the check of each field in the collection here
                                        '=========================================================================================

                                        ' XokNET 23.06.2014
                                        ' Added new tests here not from XML-node list
                                        ' XOKNET 12.11.2015 added test for Receivers bank in SG, and currency SGD
                                        'If oPayment.I_CountryCode = "SG" And oPayment.BANK_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" And oPayment.DATE_Payment > "20151122" Then                                            ' PURPOSECODE
                                        ' XOKNET 16.11.2015 - added ACH and SAL
                                        If oPayment.I_CountryCode = "SG" And oPayment.BANK_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" And oPayment.DATE_Payment > "20151122" And (oPayment.DnBNORTBIPayType = "ACH" Or oPayment.DnBNORTBIPayType = "SAL") Then
                                            ' -----------
                                            sVariableName = "PURPOSECODE"
                                            sTmp = oPayment.PurposeCode

                                            If Len(sTmp) < 4 Then
                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15111), "1")
                                            End If

                                            ' XokNET 16.01.2015
                                            ' ENDTOENDREF
                                            ' -----------
                                            sVariableName = "REF_ENDTOEND"
                                            sTmp = Trim$(oPayment.REF_EndToEnd)

                                            If Len(sTmp) = 0 Then
                                                bx = AddToErrorArray(UCase(sVariableName), LRS(15061, sVariableName), "1")
                                            End If
                                        End If  'If oPayment.I_CountryCode = "SG" And oPayment.DATE_Payment > "20151122" Then

                                    End If 'bNewPayment

                                    If aErrorArray(0, 0) <> vbNullString Or bShowAllPaymentsInfrmCorrect Then
                                        ' XNET 21.08.2012
                                        ' must have content in aErrorArray
                                        If aErrorArray(0, 0) = vbNullString Then
                                            AddToErrorArray("", "", "")
                                        End If
                                        If Not bFirstValidation Then
                                            bJustWarnings = True
                                            For lArrayCounter = 0 To UBound(aErrorArray, 2)
                                                If aErrorArray(3, lArrayCounter) = 1 Then
                                                    bJustWarnings = False
                                                    Exit For
                                                End If
                                            Next lArrayCounter
                                        End If
                                        'oCorrect = CreateObject ("vbbabel.correct")
                                        oCorrect.Filename = sOutputPath
                                        oCorrect.BabelFiles = oImportObject
                                        oCorrect.InfoArray = VB6.CopyArray(aErrorArray)
                                        ' XNET 21.08.2012 added next If
                                        If bShowAllPaymentsInfrmCorrect Then
                                            oCorrect.ShowAll = True
                                        End If
                                        ' XNET 31.01.2013 added next If
                                        If bEditAllInfrmCorrect Then
                                            oCorrect.EditAll = True
                                        End If
                                        bCorrectFinished = False
                                        Do Until bCorrectFinished
                                            If Not bFirstValidation And bJustWarnings Then
                                                bCorrectFinished = True
                                            Else
                                                oCorrect.Show()
                                                Select Case oCorrect.CorrectReturn
                                                    Case 1
                                                        'OK, validate again
                                                        bPaymentOK = False
                                                        bCorrectFinished = True
                                                        'Remove the nodes from the DOMExport object
                                                        'If it is the first Payment we also have to remove the paymentslevel
                                                        If bFirstPayment Then
                                                            'nodRemovedNode = nodPaymentsNode.parentNode.removeChild(nodPaymentsNode)
                                                            bNewPayments = True
                                                            lNoOfPaymentsWritten = lNoOfPaymentsWritten - 1
                                                        Else
                                                            If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                                lNoOfPaymentsWritten = lNoOfPaymentsWritten - 1
                                                            End If
                                                            lNoOfPaymentsWritten = lNoOfPaymentsWritten - 1
                                                            'End If
                                                        End If
                                                        bFirstValidation = False
                                                    Case 2
                                                        'Delete, the payment has been removed
                                                        bPaymentOK = True
                                                        bCorrectFinished = True
                                                        lPaymentIndex = lPaymentIndex - 1

                                                        'If it is the first Payment we also have to remove the paymentslevel
                                                        If bFirstPayment Then
                                                            bNewPayments = True
                                                            lNoOfPaymentsWritten = lNoOfPaymentsWritten - 1
                                                        Else
                                                            If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                                                            Else
                                                                lNoOfPaymentsWritten = lNoOfPaymentsWritten - 1
                                                            End If
                                                        End If
                                                        bFirstValidation = False
                                                    Case 3
                                                        'Cancel, end the export and return cancel
                                                        Err.Raise(15053, , "(BB0)" & LRS(15053)) ' added (BB0) to tell BB not to raise an error at the "top"
                                                        '"The user har cancelled the prosess. No exportfiles are created"
                                                    Case 4
                                                        'Cancel this one, populate the frmCorrect again
                                                        bCorrectFinished = False
                                                    Case Else
                                                        Err.Raise(15054, , LRS(15054))
                                                        '"Unexpected error during the correct-procedure."
                                                End Select
                                            End If 'If Not bFirstValidation And bJustWarnings Then
                                        Loop  'Do Until bCorrectFinished


                                        'remove paymentnode from export-document
                                        'Set nodRemovedNode = nodPaymentNode.parentNode.removeChild(nodPaymentNode)
                                        'Emprty the array
                                        ReDim aErrorArray(5, 0)
                                    Else
                                        bPaymentOK = True
                                        bFirstValidation = False
                                    End If

                                Next oInvoice
                                If bPaymentOK Then
                                    '                        nTotalPaymentsAmount = nTotalPaymentsAmount + oPayment.MON_InvoiceAmount
                                    '                        lTotalNumberOfPayments = lTotalNumberOfPayments + oPayment.Invoices.Count
                                    'Old code
                                    'lTotalNumberOfPayments = lTotalNumberOfPayments + 1
                                    'If sFormat <> "TELEPAYPLUS" Then ' added 03.02.2009, for TekepayPlus DK, exported in vbBabel.dll
                                    If sFormat <> "TELEPAYPLUS" And sFormat <> "PAIN.001 (ISO 20022)" Then
                                        oPayment.Exported = True
                                    End If
                                    If bFirstPayment Then
                                        bFirstPayment = False
                                    End If
                                End If
                            Else
                                'Don't export
                            End If 'if bExportThePayment
                            sOldAccountNo = oPayment.I_Account
                        Loop  ' do while bPaymentOK

                        'ZZZZZZ
                        'The masspayment has been written, store some values in OLD-variables
                        If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
                            sOldCurrencyCode = oPayment.MON_InvoiceCurrency
                            sOldFromAccount = oPayment.I_Account
                            sOldPayersReference = Trim(oPayment.REF_Own)

                            If StringToDate((oPayment.DATE_Payment)) < dFirstValidPaymentDate Then
                                sOldPaymentDate = DateToString(dFirstValidPaymentDate)
                            Else
                                If IsHoliday(StringToDate((oPayment.DATE_Payment)), Mid(sSWIFTAddr, 5, 2), True) Then
                                    sOldPaymentDate = DateToString(GetBankday(StringToDate((oPayment.DATE_Payment)), Mid(sSWIFTAddr, 5, 2), 1))
                                Else
                                    sOldPaymentDate = oPayment.DATE_Payment
                                End If
                            End If

                            sOldUrgent = CStr(oPayment.Priority)
                        End If

                    Next oPayment
                    ' New 08.03.05
                    ' Update batch-amounts in case we have deleted any payments in form correct
                    oBatch.MON_InvoiceAmount = 0
                    oBatch.AddInvoiceAmountOnBatch()
                Next oBatch

            Next oBabel

            bReturnValue = True

        Catch exx As vbBabel.Payment.PaymentException

            Throw New vbBabel.Payment.PaymentException(exx.Message, exx.InnerException, Nothing, exx.LastLineRead, exx.FilenameImported) ' - Testing Try ... Catch

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteTBIXMLFile" & vbCrLf & ex.Message, ex, oPayment, "", sOutputPath)

        Finally

            If Not oImportObject Is Nothing Then
                oImportObject = Nothing
            End If

            If Not oFilesetup Is Nothing Then
                oFilesetup = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteTBIXMLFile = bReturnValue

    End Function
    Private Function SEPA_Charges(ByRef oPayment As vbBabel.Payment, ByRef bChargesOwn As Boolean, ByRef bChargesAbroad As Boolean) As Boolean
        ' 08.09.2009 - Check if shared charges for SEPA - payments.
        ' Payments within about 35 countries in the "homeland" currency must have shared (SHA) bankcharges
        'Belgia  BE  BE  EUR EU: SEPA-land med euro som nasjonal valuta
        'Finland FI  FI  EUR       "
        'Frankrike   FR  FR  EUR      "
        'Hellas  GR  GR  EUR      "
        'Irland  IE  IE  EUR      "
        'Italia  IT  IT  EUR      "
        'Kypros  CY  CY  EUR     (CYP: fram til 1.1.08)    "
        'Luxembourg  LU  LU  EUR       "
        'Malta   MT  MT  EUR (MTL: fram til 1.1.08)    "
        'Nederland   NL  NL  EUR      "
        'Portugal    PT  PT  EUR      "
        'Slovakia    SK  SK  EUR (SKK: fram til 1.1.2009 "
        'Slovenia    SI  SI  EUR      "
        'Spania  ES  ES  EUR      "
        'Tyskland    DE  DE  EUR     "
        '�sterrike   AT  AT  EUR     "
        'Bulgaria    BG  BG  BGL EU: SEPA-land med annen nasjonal valuta
        'Danmark DK  DK  DKK "
        'Estland EE  EE  EEK "
        'Latvia  LV  LV  LVL "
        'Litauen LT  LT  LTL "
        'Polen   PL  PL  PLN "
        'Romania     RO  RO  RON "
        'Storbritannia   GB  GB eller IE GBP IBAN prefiks IE n�r nordirsk bank clearer i IE
        'Sverige SE  SE  SEK "
        'Tsjekkia    CZ  CZ  CZK "
        'Ungarn  HU  HU  HUF "
        'Island  IS  IS  ISK E�S: SEPA-land fra E�S-omr�det
        'Liechtenstein   LI  LI  CHF "
        'Norge   NO  NO  NOK "
        'Sveits  CH  CH  CHF Uavhengig land med deltakelse i SEPA
        'Martinique  MQ  FR      SEPA-omr�de via Frankrike
        'Guadeloupe  GP  FR
        'Fransk Guyana   GF  FR      "
        'R�union     RE  FR  EUR "
        'Gibraltar   GI  GI  GIP SEPA-omr�de via Storbritannia
        'Azorene PT  PT  EUR SEPA-omr�de via Portugal
        'Madeira PT  PT  EUR "
        'Ceuta   ES  ES      SEPA-omr�de via Spania
        'Melilla     ES  ES      "
        'Kanari�yene ES  ES  EUR "
        '�land   FI  FI  EUR SEPA-omr�de via Finland
        Dim bISSepaCountry As Boolean

        bISSepaCountry = False
        SEPA_Charges = True ' Allowed, default

        ' Do not check SEPA if payments from other that EU/E�S-area;
        FindDomesticCurrency((oPayment.I_CountryCode), bISSepaCountry)

        If bISSepaCountry Then ' is From Account in a SEPA-country?
            ' if charges BEN ( False, False) then
            ' All payments in EU/E�S-currency to EU/E�S currency must be marked, and charges changed to SHA
            If bChargesOwn = False And bChargesAbroad = False Then ' BEN
                ' if charges BEN ( False, False) then
                ' All payments in EU/E�S-currency to EU/E�S currency must be marked, and charges changed to SHA

                ' check if TO EU/E�S-currency
                ' check if to EU/E�S-country
                ' check if FROM EU/E�S-country ?????????

                If IsSEPACurrency((oPayment.MON_InvoiceCurrency)) Then
                    ' 1. The payment is in EU/E�S-currency and to a SEPA-country
                    '---------------------------------------------------------------------------------
                    ' Check then if it is as SEPA country
                    FindDomesticCurrency((oPayment.BANK_CountryCode), bISSepaCountry)
                    If bISSepaCountry Then ' check SepaCountries only
                        '----------------------------------------------
                        ' BEN and "national" currency, and SEPA-country
                        '-----------------------------------------------
                        SEPA_Charges = False ' BEN-charges not allowed in SEPA-payment!
                    Else
                        ' not SEPA-payment
                        SEPA_Charges = True ' Allowed
                    End If
                Else
                    ' not a SEPA-payment, because currency is not EU/E�S
                    SEPA_Charges = True ' Allowed
                End If

            ElseIf bChargesOwn = True And bChargesAbroad = True Or bChargesOwn = False And bChargesAbroad = True Then  ' OUR
                ' 2. if OUR and payment in EUR to EU/E�S-country, then mark as error
                '-----------------------------------------------------------
                If oPayment.MON_InvoiceCurrency = "EUR" Then
                    FindDomesticCurrency((oPayment.BANK_CountryCode), bISSepaCountry)
                    ' to EU/E�S-country ?
                    If bISSepaCountry Then
                        '-----------------------------
                        ' OUR AND EUR and SEPA-country
                        '-----------------------------
                        ' mark as error
                        SEPA_Charges = False ' OUR-charges not allowed in SEPA-payment in currency EUR
                    Else
                        SEPA_Charges = True ' Allowed
                    End If
                End If
                '3. Payment in other EU/E�S-currency than EUR, and the payment is in the same currency than the accounts currency
                '----------------------------------------------------------------------------------------------------------------
                If oPayment.MON_InvoiceCurrency <> "EUR" Then
                    ' 15.12.2017
                    ' DNB Kjell Chr: Shared charges for all payments with or without exchange in SEPA
                    ' thus, removed next If
                    'If IsSEPACurrency((oPayment.MON_InvoiceCurrency)) Then
                    FindDomesticCurrency((oPayment.BANK_CountryCode), bISSepaCountry)
                    ' to EU/E�S-country ?
                    If bISSepaCountry Then
                        ' 15.12.2017
                        ' DNB Kjell Chr: Shared charges for all payments with or without exchange in SEPA
                        ' thus, removed next If
                        'If oPayment.MON_InvoiceCurrency <> oPayment.MON_AccountCurrency Then
                        '------------------------------------------------------------------------------
                        ' OUR AND other EU/E�S-currency than EUR and SEPA-country and accounts currency
                        '------------------------------------------------------------------------------
                        ' mark as error
                        SEPA_Charges = False ' OUR-charges not allowed in SEPA-payment in currency EUR
                        'End If
                    Else
                        'not to SEPA-country
                        SEPA_Charges = True ' Allowed
                    End If
                    'Else
                    '   ' not SEPA-currency
                    '  SEPA_Charges = True ' Allowed
                    'End If
            End If

            Else
                ' SHA
                SEPA_Charges = True ' Allowed
            End If
        Else
            SEPA_Charges = True ' Allowed, from account NOT in SEPA-country
        End If
    End Function
    '    Private Function WriteAndValidateXMLFile(ByRef sOutputPath As String, ByRef xmlschema As MSXML2.XMLSchemaCache40, ByRef nodPaymentsSeqList As MSXML2.IXMLDOMNodeList, ByRef nodPaymentSeqList As MSXML2.IXMLDOMNodeList) As Boolean

    '        Dim nodPaymentNode As MSXML2.IXMLDOMElement
    '        Dim nodPaymentsList As MSXML2.IXMLDOMNodeList
    '        Dim nodPayments As MSXML2.IXMLDOMNode
    '        Dim nodAmountList As MSXML2.IXMLDOMNodeList
    '        Dim nodAmount As MSXML2.IXMLDOMNode
    '        Dim nTotalPaymentsAmount As Double
    '        Dim lCounter As Integer
    '        Dim lCounter2 As Integer
    '        Dim s As String
    '        Dim bReturnValue As Boolean

    '        'Dim xmlschema As MSXML2.XMLSchemaCache40

    '        bReturnValue = True

    '        On Error GoTo ERR_WriteAndValidateXMLFile

    '        'Don't export if there is no payments
    '        If docXMLExport.childNodes(1).hasChildNodes Then

    '            'If it is an ACH or Salary payment, add amount to the payment-level
    '            '13.09.04 - Changed, removed next IF-statement
    '            'If sPaymentType = "ACH" Or sPaymentType = "SAL" Then
    '            'Make a list of all Payment-objects
    '            nodPaymentsList = docXMLExport.selectNodes("child::*/child::*/child::*[name()='Payment']") 'and attribute::name='payments']/child::*[name()='sequence']/child::*[name()='element']")
    '            For lCounter = 0 To nodPaymentsList.length - 1
    '                nTotalPaymentsAmount = 0
    '                nodPaymentNode = nodPaymentsList.Item(lCounter)
    '                nodAmountList = nodPaymentNode.selectNodes("child::*/child::*[name()='AmountDetails']")
    '                For lCounter2 = 0 To nodAmountList.length - 1
    '                    nodAmount = nodAmountList.Item(lCounter2)
    '                    nTotalPaymentsAmount = nTotalPaymentsAmount + Val(Replace(nodAmount.nodeTypedValue, ".", ""))
    '                Next lCounter2
    '                ' Added 09.01.07 due to problems with amounts like 0.01 and 0.11
    '                If Len(Trim(Str(nTotalPaymentsAmount))) = 1 Then
    '                    ' 0.1
    '                    s = "0.0" & Trim(Str(nTotalPaymentsAmount))
    '                ElseIf Len(Trim(Str(nTotalPaymentsAmount))) = 2 Then
    '                    '0.11
    '                    s = "0." & Trim(Str(nTotalPaymentsAmount))
    '                Else
    '                    s = Left(Trim(Str(nTotalPaymentsAmount)), Len(Trim(Str(nTotalPaymentsAmount))) - 2) & "." & Right(Trim(Str(nTotalPaymentsAmount)), 2)
    '                End If
    '                If TBIOElementOK(s, nodPaymentSeqList.Item(1), "Amount", True, "") Then
    '                    nodAmount = nodPaymentNode.selectSingleNode("child::*[name()='Amount']")
    '                    nodAmount.nodeTypedValue = s
    '                Else
    '                    err.Raise(15055, , LRS(15055))
    '                    '"Unable to calculate total amount during export."
    '                End If

    '            Next lCounter

    '            'End If

    '            'Have to add the totalamount to the payments-level.

    '            'Make a list over all Payments-nodes
    '            'Set nodPaymentsList = docXMLExport.selectNodes("child::*/child::*[name()='Payments' and attribute::name='payments']/child::*[name()='sequence']/child::*[name()='element']")
    '            On Error Resume Next
    '            nodPaymentsList = docXMLExport.selectNodes("child::*/child::*[name()='Payments']") 'and attribute::name='payments']/child::*[name()='sequence']/child::*[name()='element']")
    '            On Error GoTo 0
    '            For lCounter = 0 To nodPaymentsList.length - 1
    '                nTotalPaymentsAmount = 0
    '                nodPayments = nodPaymentsList.Item(lCounter)
    '                'Set nodPaymentList = docXMLSchema.selectNodes("child::*/child::*[name()='complexType' and attribute::name='payment']/child::*[name()='sequence']/child::*[name()='element']")
    '                nodAmountList = nodPayments.selectNodes("child::*/child::*[name()='Amount']")
    '                For lCounter2 = 0 To nodAmountList.length - 1
    '                    nodAmount = nodAmountList.Item(lCounter2)
    '                    nTotalPaymentsAmount = nTotalPaymentsAmount + Val(Replace(nodAmount.nodeTypedValue, ".", ""))
    '                Next lCounter2
    '                ' Added 09.01.07 due to problems with amounts like 0.01 and 0.11
    '                If Len(Trim(Str(nTotalPaymentsAmount))) = 1 Then
    '                    ' 0.1
    '                    s = "0.0" & Trim(Str(nTotalPaymentsAmount))
    '                ElseIf Len(Trim(Str(nTotalPaymentsAmount))) = 2 Then
    '                    '0.11
    '                    s = "0." & Trim(Str(nTotalPaymentsAmount))
    '                Else
    '                    s = Left(Trim(Str(nTotalPaymentsAmount)), Len(Trim(Str(nTotalPaymentsAmount))) - 2) & "." & Right(Trim(Str(nTotalPaymentsAmount)), 2)
    '                End If
    '                If TBIOElementOK(s, nodPaymentsSeqList.Item(1), "TotalAmount", True, "") Then
    '                    nodPayments.childNodes(1).nodeTypedValue = s
    '                Else
    '                    err.Raise(15055, , LRS(15055))
    '                    '"Unable to calculate total amount during export."
    '                End If

    '            Next lCounter

    '            '   'Validate no. 1
    '            'Can't use this sort of validation because it creates av validation-error.
    '            ' The reason is the use of namespaces in the XML-file:
    '            ' "xsi:schemaLocation", "http://www.ibm.com IntegratorXMLSchema.xsd"
    '            '    'From SDK
    '            '    'Create a schema cache and add books.xsd to it.
    '            '    Set xmlschema = New MSXML2.XMLSchemaCache40
    '            '    xmlschema.Add "http://www.ibm.com", App.path & "\IntegratorXMLSchema.xsd"
    '            '
    '            '    'Assign the schema cache to the DOM document.
    '            '    'schemas collection, and load the XML-document
    '            '    Set docXMLExport.schemas = xmlschema
    '            '    'Validate no. 1 of the XMLfile
    '            '    'docXMLExport.resolveExternals = False
    '            '    Set objErr = docXMLExport.Validate()
    '            '    If objErr.errorCode = 0 Then
    '            '        'MsgBox ("Document is valid")
    '            '    Else
    '            '        bReturnValue = False
    '            '        MsgBox ("Validation error:" + objErr.reason)
    '            '    End If

    '            'Validate no. 2 Write the xml-document, and create a new DOMdocument
    '            docXMLExport.Save(sOutputPath)
    '            'UPGRADE_NOTE: Object docXMLExport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            docXMLExport = Nothing
    '            docXMLExport = New MSXML2.DOMDocument40
    '            docXMLExport.async = False

    '            xmlschema = New MSXML2.XMLSchemaCache40
    '            xmlschema.Add("http://www.ibm.com", My.Application.Info.DirectoryPath & "\IntegratorXMLSchema.xsd")
    '            docXMLExport.schemas = xmlschema

    '            docXMLExport.Load(sOutputPath)

    '            'Return validation results in message to the user.
    '            If docXMLExport.parseError.errorCode <> 0 Then
    '                ' FIX: legg p� heading p� melding
    '                ' ta med file/linepos
    '                ' kontakt leverand�r
    '                MsgBox(docXMLExport.parseError.errorCode & " " & docXMLExport.parseError.reason, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical, "PARSE ERROR")
    '                oFs.DeleteFile(sOutputPath)
    '                bReturnValue = False
    '            Else
    '                'MsgBox "No Error"
    '            End If

    '        End If 'If docXMLExport.childNodes(1).hasChildNodes Then

    '        WriteAndValidateXMLFile = bReturnValue

    '        'UPGRADE_NOTE: Object nodPaymentNode may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodPaymentNode = Nothing
    '        'UPGRADE_NOTE: Object nodPaymentsList may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodPaymentsList = Nothing
    '        'UPGRADE_NOTE: Object nodPayments may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodPayments = Nothing
    '        'UPGRADE_NOTE: Object nodAmountList may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodAmountList = Nothing
    '        'UPGRADE_NOTE: Object nodAmount may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodAmount = Nothing

    '        On Error GoTo 0
    '        Exit Function

    'ERR_WriteAndValidateXMLFile:
    '        'UPGRADE_NOTE: Object nodPaymentNode may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodPaymentNode = Nothing
    '        'UPGRADE_NOTE: Object nodPaymentsList may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodPaymentsList = Nothing
    '        'UPGRADE_NOTE: Object nodPayments may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodPayments = Nothing
    '        'UPGRADE_NOTE: Object nodAmountList may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodAmountList = Nothing
    '        'UPGRADE_NOTE: Object nodAmount may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        nodAmount = Nothing

    '        bReturnValue = False
    '        err.Raise(Err.Number, "WriteAndValidateXMLFile", Err.Description)
    '    End Function

    'Private Function TBIOElementOK(ByRef sValue As String, ByRef nodMapping As MSXML2.IXMLDOMNode, ByRef sTagName As String, ByRef bMandatory As Boolean, ByRef sFormat As String) As Boolean
    '    ' 23.10.2009 - added sFormat as parameter
    '    'Dim bReturnValue, bFoundValue As Boolean
    '    'Dim sVariableType As String
    '    'Dim nodElementDescr, nodTemp As MSXML2.IXMLDOMNode
    '    'Dim nodPossibleValues As MSXML2.IXMLDOMNodeList
    '    'Dim lListCounter As Integer
    '    'Dim sDecimalSep As String
    '    'Dim bx As Boolean

    '    ' XokNET 11.09.2014 Do not test against schema anylonger. it's outdated
    '    TBIOElementOK = True
    '    Exit Function

    '    'bReturnValue = True 'Assume success
    '    ''UPGRADE_WARNING: Couldn't resolve default property of object nodMapping.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    'sVariableType = nodMapping.Attributes.getNamedItem("type").nodeValue
    '    'If UCase(Left(sVariableType, 5)) = "TBIW:" Then
    '    '    'Get the simpleType-element in the Schema that fits our variable
    '    '    's = "child::*/child::*[name()='simpleType' and attribute::name='" & Mid$(sVariableType, 6) & "']"
    '    '    nodElementDescr = docXMLSchema.selectSingleNode("child::*/child::*[name()='simpleType' and attribute::name='" & Mid(sVariableType, 6) & "']") '/child::*[name()='sequence']/child::*[name()='element']")
    '    '    If nodElementDescr Is Nothing Then
    '    '        bReturnValue = False
    '    '        err.Raise(15056, , LRS(15056, sVariableType))
    '    '        '15056: The description of variabletype %1 is not found in the schema-file.
    '    '    End If
    '    '    'Don't do the following check if the length of the variable is 0 and
    '    '    '    the field is Optional or Not in Use.
    '    '    If Len(sValue) = 0 And bMandatory = False Then
    '    '        'nothing to do
    '    '    Else
    '    '        'Check if there are some restrictions regarding the value
    '    '        nodPossibleValues = nodElementDescr.selectNodes("child::*/child::*[name()='enumeration']")
    '    '        bFoundValue = True
    '    '        For lListCounter = 0 To nodPossibleValues.length - 1
    '    '            bFoundValue = False
    '    '            If nodPossibleValues.Item(lListCounter).Attributes.getNamedItem("value").nodeValue = sValue Then
    '    '                bFoundValue = True
    '    '                Exit For
    '    '            End If
    '    '        Next lListCounter
    '    '        If Not bFoundValue Then
    '    '            bReturnValue = False
    '    '            bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), LRS(15074, sValue, sVariableType), "1")
    '    '            'The value %1 is not valid for the variable %2 according to the schema file.
    '    '        End If
    '    '    End If
    '    '    'Overwrite the sVariableType with the base-type of the TBIW namespace-variable
    '    '    'UPGRADE_WARNING: Couldn't resolve default property of object nodElementDescr.selectSingleNode().Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '    sVariableType = nodElementDescr.selectSingleNode("child::*[name()='restriction']").Attributes.getNamedItem("base").nodeValue
    '    '    'If UCase(sVariableType) = "DECIMAL" Then
    '    '    nodElementDescr = nodElementDescr.selectSingleNode("child::*[name()='restriction']")
    '    '    'End If
    '    'Else
    '    '    nodElementDescr = docXMLSchema.selectSingleNode("child::*/child::*[name()='simpleType' and attribute::name='" & Mid(sVariableType, 6) & "']") '/child::*[name()='sequence']/child::*[name()='element']")
    '    '    'If UCase(sVariableType) = "DECIMAL" Then
    '    'End If

    '    'sDecimalSep = "."

    '    'Select Case UCase(sVariableType)

    '    '    Case "DECIMAL"
    '    '        'If the variable isn't used and its optional. Return OK
    '    '        If Len(sValue) = 0 And bMandatory = False Then
    '    '            sValue = "0.00"
    '    '            TBIOElementOK = True
    '    '            Exit Function
    '    '        End If
    '    '        If Not nodElementDescr Is Nothing Then
    '    '            'Test for valid charcters
    '    '            If Not IsNumeric(Replace(sValue, sDecimalSep, "")) Then
    '    '                bReturnValue = False
    '    '                bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), LRS(15075, sValue, (nodMapping.Attributes.getNamedItem("name").nodeValue)), "1")
    '    '                'The number " & sValue & " used in the variable " & nodMapping.Attributes.getNamedItem("name").nodeValue &  "," & vbCrLf & "contains illegal characters."
    '    '            End If
    '    '            'Test for maxlength
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='totalDigits']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                If Len(sValue) > nodTemp.Attributes.getNamedItem("value").nodeValue Then
    '    '                    bReturnValue = False
    '    '                    bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15076, sValue, (nodMapping.Attributes.getNamedItem("name").nodeValue)), "%3", nodTemp.Attributes.getNamedItem("value").nodeValue), "1")
    '    '                    '"The number of digits in " & sValue & " used in the variable " & nodMapping.Attributes.getNamedItem("name").nodeValue & "," & vbCrLf & "is too many. Maximiulength is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                End If
    '    '            End If
    '    '            'Test for number of digits
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='fractionDigits']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                If Len(sValue) - InStr(1, sValue, sDecimalSep) > nodTemp.Attributes.getNamedItem("value").nodeValue Then
    '    '                    bReturnValue = False
    '    '                    bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15077, sValue, (nodMapping.Attributes.getNamedItem("name").nodeValue)), "%3", nodTemp.Attributes.getNamedItem("value").nodeValue), "1")
    '    '                    '"The number of decimalnumbers in " & sValue & " used in the variable " & nodMapping.Attributes.getNamedItem("name").nodeValue & "," & vbCrLf & "is too many. Maximum is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                End If
    '    '            End If
    '    '            'Test for minimumvalue
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='minExclusive']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                If Val(sValue) < Val(nodTemp.Attributes.getNamedItem("value").nodeValue) Then
    '    '                    ' XokNET 18.08.2011 - Do not check negative amounts for TelepayPlusexport, when importing Telepay, when NOT Mass or Salary
    '    '                    If (oPayment.PayType <> "S" And oPayment.PayType <> "M") And ((oBabel.ImportFormat = FileType.Telepay2 Or oBabel.ImportFormat = FileType.TelepayPlus Or oBabel.ImportFormat = FileType.TelepayTBIO) And sFormat = "TELEPAYPLUS") Then
    '    '                        ' do not warn about negative amounts pr invoice
    '    '                        ' otherwise, warn
    '    '                    Else
    '    '                        bReturnValue = False
    '    '                        bx = AddToErrorArray(nodMapping.attributes.getNamedItem("name").nodeValue, Replace(LRS(15078, sValue, nodMapping.attributes.getNamedItem("name").nodeValue), "%3", nodTemp.attributes.getNamedItem("value").nodeValue), "1")
    '    '                        '"The value " & sValue & " used in the variable " & nodMapping.Attributes.getNamedItem("name").nodeValue & "," & vbCrLf & "is too low. Mimimum value is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                    End If
    '    '                End If
    '    '            End If
    '    '        End If

    '    '    Case "DATE"
    '    '        If Not nodElementDescr Is Nothing Then
    '    '            'Test if some specific pattern is stated. If so terminate.
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='pattern']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                bReturnValue = False
    '    '                err.Raise(15057, , LRS(15057) & vbCrLf & vbCrLf & LRS(15058))
    '    '                '"The constraining facet 'Pattern' is not implemented for datatype 'Date' in this version of BabelBank." & vbCrLf & _
    '    '                ''"Contact Your dealer" & vbCrLf & "BabelBank will terminate."
    '    '            Else
    '    '                sValue = VB6.Format(StringToDate(sValue), "YYYY-MM-DD")
    '    '            End If
    '    '        Else
    '    '            'Have to format the value accourding to w3.org standard
    '    '            sValue = VB6.Format(StringToDate(sValue), "YYYY-MM-DD")
    '    '        End If

    '    '    Case "DOUBLE"
    '    '        'If the variable isn't used and its optional. Return OK
    '    '        If Len(sValue) = 0 And bMandatory = False Then
    '    '            sValue = "0"
    '    '            TBIOElementOK = True
    '    '            Exit Function
    '    '        End If
    '    '        If sValue = "" Then
    '    '            sValue = "0"
    '    '        End If
    '    '        If Not IsNumeric(Replace(sValue, sDecimalSep, "")) Then
    '    '            bReturnValue = False
    '    '            bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), LRS(15075, sValue, (nodMapping.Attributes.getNamedItem("name").nodeValue)), "1")
    '    '            'The number " & sValue & " used in the variable " & nodMapping.Attributes.getNamedItem("name").nodeValue &  "," & vbCrLf & "contains illegal characters.
    '    '        End If

    '    '    Case "INTEGER"
    '    '        'If the variable isn't used and its optional. Return OK
    '    '        If Len(sValue) = 0 And bMandatory = False Then
    '    '            sValue = "0"
    '    '            TBIOElementOK = True
    '    '            Exit Function
    '    '        End If
    '    '        If Not IsNumeric(Replace(sValue, sDecimalSep, "")) Then
    '    '            bReturnValue = False
    '    '            bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), LRS(15075, sValue, (nodMapping.Attributes.getNamedItem("name").nodeValue)), "1")
    '    '            'The number " & sValue & " used in the variable " & nodMapping.Attributes.getNamedItem("name").nodeValue &  "," & vbCrLf & "contains illegal characters.
    '    '        End If

    '    '    Case "STRING"
    '    '        'If the variable isn't used and its optional. Return OK
    '    '        If Len(sValue) = 0 And bMandatory = False Then
    '    '            sValue = ""
    '    '            TBIOElementOK = True
    '    '            Exit Function
    '    '        End If
    '    '        If Not nodElementDescr Is Nothing Then
    '    '            'Test if a minimumlength is stated, and if the variable is shorter than the minimum length
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='minLength']") '"child::*[name()='minLength']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                If Len(Trim(sValue)) < Val(nodTemp.Attributes.getNamedItem("value").nodeValue) Then
    '    '                    bReturnValue = False
    '    '                    bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15079, sValue, sTagName), "%3", nodTemp.Attributes.getNamedItem("value").nodeValue), "1")
    '    '                    '"The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too short. Mimimum value is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                End If
    '    '            End If
    '    '            'Test if a maximumlength is stated, and if the variable is longer than the maximum length.
    '    '            '   If so, truncate it.
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='maxLength']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                If Len(sValue) > Val(nodTemp.Attributes.getNamedItem("value").nodeValue) Then
    '    '                    'Truncate the value, don't raise an error.
    '    '                    bReturnValue = True
    '    '                    If UCase(sTagName) = "BENEFICIARYADDRESS4" Then
    '    '                        bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15080, sValue, sTagName), "%3", nodTemp.Attributes.getNamedItem("value").nodeValue) & vbCrLf & LRS(15081), "0")
    '    '                        'The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " & nodTemp.Attributes.getNamedItem("value").nodeValue & vbCrLf & "In BabelBank BENEFICIARYADDRESS4 is put together from the fields 'Zipcode' + 'City' + 'Address3'."
    '    '                    Else
    '    '                        ' added next if 23.10.2009 to be able to handle 11 digits enterpriseno in TelepayPlus
    '    '                        If (sTagName = "EnterpriseNumber" And sFormat = "TELEPAYPLUS") Then
    '    '                            If Len(sValue) > 11 Then
    '    '                                bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15080, sValue, sTagName), "%3", "11"), "0")
    '    '                                '"The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                            End If
    '    '                            ' 09.08.2010 added next if for OwnReference, which can be 30 in TelepayPlus (20 in TBI)
    '    '                        ElseIf (sTagName = "PayersReference" And sFormat = "TELEPAYPLUS") Then
    '    '                            If Len(sValue) > 30 Then
    '    '                                bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15080, sValue, sTagName), "%3", "30"), "0")
    '    '                                '"The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                            End If

    '    '                        Else
    '    '                            ' as pre 23.10.2009
    '    '                            bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), Replace(LRS(15080, sValue, sTagName), "%3", nodTemp.Attributes.getNamedItem("value").nodeValue), "0")
    '    '                            '"The value " & sValue & " used in the variable " & sTagName & "," & vbCrLf & "is too long. Maximum length is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                        End If
    '    '                    End If
    '    '                    sValue = Left(Trim(sValue), Val(nodTemp.Attributes.getNamedItem("value").nodeValue))

    '    '                    '            bReturnValue = False
    '    '                    '            err.Raise 1, , "The value " & sValue & " used in the variable " & sTagName & _
    '    '                    ''               "," & vbCrLf & "is too short. Mimimum value is " & nodTemp.Attributes.getNamedItem("value").nodeValue
    '    '                End If
    '    '            End If
    '    '            nodTemp = nodElementDescr.selectSingleNode("child::*[name()='pattern']")
    '    '            If Not nodTemp Is Nothing Then
    '    '                'Babelbank is not going to check patterns other than explicit defined values
    '    '                '  This is not a good test, but should be sufficient
    '    '                If InStr(1, nodTemp.Attributes.getNamedItem("value").nodeValue, "{") > 0 Then
    '    '                    'Don't check the pattern
    '    '                Else
    '    '                    If InStr(1, nodTemp.Attributes.getNamedItem("value").nodeValue, sValue) = 0 Then
    '    '                        bx = AddToErrorArray((nodMapping.Attributes.getNamedItem("name").nodeValue), LRS(15074, sValue, sTagName), "1")
    '    '                        'The value %1 is not valid for the variable %2 according to the schema file.
    '    '                    End If
    '    '                End If
    '    '            End If
    '    '        End If

    '    '    Case Else
    '    '        bReturnValue = False
    '    '        err.Raise(15059, , LRS(15059, sVariableType))
    '    '        '"The variable type " & sVariableType & " is not defined in BabelBank."

    '    'End Select

    '    ''UPGRADE_NOTE: Object nodElementDescr may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    'nodElementDescr = Nothing
    '    ''UPGRADE_NOTE: Object nodTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    'nodTemp = Nothing
    '    ''UPGRADE_NOTE: Object nodPossibleValues may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    'nodPossibleValues = Nothing

    '    'TBIOElementOK = bReturnValue

    'End Function

    Private Function AddToErrorArray(ByVal sVariableName As String, ByVal sErrorText As String, ByVal sErrorType As String) As Boolean
        ' XNET 31.01.2013 - take whole function
        ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)

        Dim sErrVariableName As String
        Dim sErrPaymentInfo As String
        'sErrorType
        '0 = Warning
        '1 = Error
        Dim sAllowEdit As String
        '0 = Don't allow
        '1 = Allow
        Dim lErrorCounter As Long


        Select Case UCase(sVariableName)

            Case "ENTERPRISENUMBER"
                sErrVariableName = "Babel_CompanyNo"
                sErrorType = "1"
                sAllowEdit = "0"

            Case "TOTALAMOUNT"
                sErrVariableName = "Babel_MON_InvoiceAmount"
                sAllowEdit = "0"

                'Payment-tag
            Case "AMOUNT"
                sErrVariableName = "Payment_MON_InvoiceAmount"
                ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                If bEditAllInfrmCorrect Then
                    sAllowEdit = "1"
                Else
                    sAllowEdit = "0"
                End If

            Case "BANKREFERENCE"
                sErrVariableName = "Payment_REF_Bank1"
                ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                If bEditAllInfrmCorrect Then
                    sAllowEdit = "1"
                Else
                    sAllowEdit = "0"
                End If

            Case "CHARGESCORRBANK"
                sErrVariableName = "Payment_MON_ChargeMeAbroad"
                sAllowEdit = "1"

            Case "CHARGESDEBITBANK"
                sErrVariableName = "Payment_MON_ChargeMeDomestic"
                sAllowEdit = "1"

            Case "CHECK"
                sErrVariableName = "Payment_Cheque"
                sAllowEdit = "1"

            Case "CURRENCYCODE"
                sErrVariableName = "Payment_MON_InvoiceCurrency"
                ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                ' AND changed AllowEdit to 0 for other customers !
                If bEditAllInfrmCorrect Then
                    sAllowEdit = "1"
                Else
                    sAllowEdit = "0"
                End If


            Case "EXCHANGEDATE"
                sErrVariableName = "Payment_ERA_Date"
                sAllowEdit = "1"

            Case "EXCHANGEDEALER"
                sErrVariableName = "Payment_ERA_DealMadeWith"
                sAllowEdit = "1"

            Case "EXCHANGERATEACTUAL"
                sErrVariableName = "Payment_MON_AccountExchRate"
                ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                If bEditAllInfrmCorrect Then
                    sAllowEdit = "1"
                Else
                    sAllowEdit = "0"
                End If

            Case "EXCHANGERATEAGREED"
                sErrVariableName = "Payment_ERA_ExchRateAgreed"
                sAllowEdit = "1"

            Case "EXCHANGEREF"
                sErrVariableName = "Payment_FRW_ForwardContractNo"
                sAllowEdit = "1"

            Case "FROMACCOUNT"
                sErrVariableName = "Payment_I_Account"
                sAllowEdit = "0"

            Case "FROMACCOUNTSWIFTADDR"
                sErrVariableName = "Payment_Bank_I_SWIFTCode"
                sAllowEdit = "0"

            Case "INFOTODNB"
                sErrVariableName = "Payment_NOTI_NotificationMessageToBank"
                sAllowEdit = "1"

            Case "NATIONALCODE"
                sErrVariableName = "Invoice_STATEBANK_Code"
                sAllowEdit = "1"

            Case "NATIONALNARR"
                sErrVariableName = "Invoice_STATEBANK_Text"
                sAllowEdit = "1"

            Case "ORDERINGCUSTOMER"
                sErrVariableName = "Ordering_Customer" 'Will be shown use Case Else in form.correct
                sAllowEdit = "1"

            Case "PAYERSREFERENCE"
                sErrVariableName = "Payment_REF_Own"
                sAllowEdit = "1"

            Case "PAYMENTDATE"
                sErrVariableName = "Payment_DATE_Payment"
                sAllowEdit = "1"

            Case "PAYMENTTYPE"
                sErrVariableName = "Payment_Paytype"
                sAllowEdit = "0"

            Case "URGENT"
                sErrVariableName = "Payment_Priority"
                sAllowEdit = "1"

            Case "VALUEDATE"
                sErrVariableName = "Payment.DATE_Value"
                ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                If bEditAllInfrmCorrect Then
                    sAllowEdit = "1"
                Else
                    sAllowEdit = "0"
                End If


                'Details-tag
            Case "ACHACCOUNTNAME"
                sErrVariableName = "Payment_E_Name"
                sAllowEdit = "1"

            Case "ACHBANKID"
                sErrVariableName = "Payment_Bank_Swiftcode"
                sAllowEdit = "1"

            Case "ACHPAYERSREF"
                sErrVariableName = "Invoice_REF_Own"
                sAllowEdit = "1"

            Case "ACHBENEFICIARYREF"
                sErrVariableName = "Freetext"
                sAllowEdit = "1"

            Case "AMOUNTDETAILS"
                sErrVariableName = "Invoice_MON_InvoiceAmount"
                ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                If bEditAllInfrmCorrect Then
                    sAllowEdit = "1"
                Else
                    sAllowEdit = "0"
                End If

            Case "BENBANKID"
                sErrVariableName = "Payment_BANK_SWIFTCode"
                sAllowEdit = "1"

            Case "BENBANKID_BRANCH"
                sErrVariableName = "Payment_BANK_BranchNo"
                sAllowEdit = "1"

            Case "BENBANKIDTYPE"
                sErrVariableName = "Payment_BANK_BranchType"
                sAllowEdit = "1"

            Case "BENEFICIARYADDRESS1"
                sErrVariableName = "Payment_E_Name"
                sAllowEdit = "1"

            Case "BENEFICIARYADDRESS2"
                sErrVariableName = "Payment_E_Adr1"
                sAllowEdit = "1"

            Case "BENEFICIARYADDRESS3"
                sErrVariableName = "Payment_E_Adr2"
                sAllowEdit = "1"

            Case "BENEFICIARYADDRESS4"
                sErrVariableName = "Payment_E_Adr3" 'Payment_E_City
                sAllowEdit = "1"

            Case "BENEFICIARYBANKADDRESS1"
                sErrVariableName = "Payment_BANK_Name"
                sAllowEdit = "1"

            Case "BENEFICIARYBANKADDRESS2"
                sErrVariableName = "Payment_BANK_Adr1"
                sAllowEdit = "1"

            Case "BENEFICIARYBANKADDRESS3"
                sErrVariableName = "Payment_BANK_Adr2"
                sAllowEdit = "1"

            Case "BENEFICIARYBANKADDRESS4"
                sErrVariableName = "Payment_BANK_Adr3"
                sAllowEdit = "1"

            Case "INFOBENBANKCODE"
                sErrVariableName = "Payment_NOTI_NotificationParty"
                sAllowEdit = "1"

            Case "INFOTOBENBANK"
                sErrVariableName = "Payment_NOTI_NotificationMessageToBank"
                sAllowEdit = "1"

            Case "INFOTOBENEFICIARY"
                sErrVariableName = "Freetext"
                sAllowEdit = "1"

            Case "INTBANKID"
                sErrVariableName = "Payment_BANK_SWIFTCodeCorrBank"
                sAllowEdit = "1"

            Case "INTBANKID_BRANCH"
                sErrVariableName = "Payment_BANK_BranchNoCorrBank"
                sAllowEdit = "1"

            Case "INTBANKIDTYPE"
                sErrVariableName = "Payment_BANK_BranchTypeCorrBank"
                sAllowEdit = "1"

            Case "INTBANKNAMEADDRESS1"
                sErrVariableName = "Payment_BANK_NameAddressCorrBank1"
                sAllowEdit = "1"

            Case "INTBANKNAMEADDRESS2"
                sErrVariableName = "Payment_BANK_NameAddressCorrBank2"
                sAllowEdit = "1"

            Case "INTBANKNAMEADDRESS3"
                sErrVariableName = "Payment_BANK_NameAddressCorrBank3"
                sAllowEdit = "1"

            Case "INTBANKNAMEADDRESS4"
                sErrVariableName = "Payment_BANK_NameAddressCorrBank4"
                sAllowEdit = "1"

            Case "TOACCOUNT"
                sErrVariableName = "Payment_E_Account"
                If sPaymentType = "FIK" Then
                    sAllowEdit = "1"
                Else
                    ' Added option for those who will like to edit ALL information (needs a password in setup to allow this)
                    If bEditAllInfrmCorrect Then
                        sAllowEdit = "1"
                    Else
                        sAllowEdit = "0"
                    End If
                End If

            Case "INVOICE_STATEBANK_CODE"
                sErrVariableName = "INVOICE_STATEBANK_CODE"
                sAllowEdit = "1"

            Case "INVOICE_STATEBANK_TEXT"
                sErrVariableName = "INVOICE_STATEBANK_TEXT"
                sAllowEdit = "1"

                ' XokNET 16.07.2014
            Case "PURPOSECODE"
                sErrVariableName = "PURPOSECODE"
                sAllowEdit = "0"

            Case "REF_ENDTOEND"
                sErrVariableName = "REF_ENDTOEND"
                sAllowEdit = "0"

                ' added 11.03.2016
            Case "E_COUNTRYCODE"
                sErrVariableName = "E_COUNTRYCODE"
                sAllowEdit = "1"

        End Select

        If bUseFrmCorrect Then
            ' XNET 21.08.2012 added code for those who will show all payments in frmCorrect
            If bShowAllPaymentsInfrmCorrect Then
                If aErrorArray(0, 0) = Nothing Then
                    'Empty array, don't redim
                Else
                    ReDim Preserve aErrorArray(5, UBound(aErrorArray, 2) + 1)
                End If

                lErrorCounter = UBound(aErrorArray, 2)

                aErrorArray(0, lErrorCounter) = Trim$(Str(lBabelIndex)) & "," & Trim$(Str(lBatchIndex)) & "," & Trim$(Str(lPaymentIndex)) & "," & Trim$(Str(lInvoiceIndex))


                ' XNET 21.08.2012 added Else in front of If
            ElseIf bShowJustErrors And sErrorType = 0 Then
                'nothing to do
            Else
                If aErrorArray(0, 0) = vbNullString Then
                    'Empty array, don't redim
                Else
                    ReDim Preserve aErrorArray(5, UBound(aErrorArray, 2) + 1)
                End If

                lErrorCounter = UBound(aErrorArray, 2)

                aErrorArray(0, lErrorCounter) = Trim$(Str(lBabelIndex)) & "," & Trim$(Str(lBatchIndex)) & "," & Trim$(Str(lPaymentIndex)) & "," & Trim$(Str(lInvoiceIndex))
                aErrorArray(1, lErrorCounter) = sErrVariableName
                'Remove CRLF and Errornumber
                sErrorText = Replace(Replace(sErrorText, Chr(10), vbNullString), Chr(13), vbNullString)
                If IsNumeric(Left$(sErrorText, 5)) Then
                    sErrorText = Mid$(sErrorText, 8)
                End If
                aErrorArray(2, lErrorCounter) = sErrorText
                aErrorArray(3, lErrorCounter) = sErrorType
                aErrorArray(4, lErrorCounter) = sAllowEdit
                Select Case sPaymentType
                    Case "GEN"
                        aErrorArray(5, lErrorCounter) = "General Payment"
                    Case "FIK"
                        aErrorArray(5, lErrorCounter) = "FI Payment"
                    Case "ACH"
                        aErrorArray(5, lErrorCounter) = "ACH Payment"
                    Case "SAL"
                        aErrorArray(5, lErrorCounter) = "Salary"
                    Case "LOCALLOW"
                        aErrorArray(5, lErrorCounter) = "Local low"
                    Case "LOCALHIGH"
                        aErrorArray(5, lErrorCounter) = "Local high"
                    Case "LOCALCHECK"
                        aErrorArray(5, lErrorCounter) = "Local check"
                    Case "IPBLOW"
                        aErrorArray(5, lErrorCounter) = "IPBLow"
                    Case "LOCALHIGH"
                        aErrorArray(5, lErrorCounter) = "IPBHigh"
                End Select
            End If
        Else
            If sErrorType = 1 Then
                'Add errorinformation, which helps the user to identify the payment in matter
                sErrPaymentInfo = "From account: " & oPayment.I_Account & vbCrLf
                sErrPaymentInfo = sErrPaymentInfo & "Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                sErrPaymentInfo = sErrPaymentInfo & "To: " & oPayment.E_Name & vbCrLf
                sErrPaymentInfo = sErrPaymentInfo & "To account: " & oPayment.E_Account & vbCrLf
                sErrPaymentInfo = sErrPaymentInfo & "Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                If IsNumeric(Left$(sErrorText, 5)) Then
                    err.Raise(Val(Left$(sErrorText, 5)), "EDI2xml", sErrorText & vbCrLf & sErrPaymentInfo & vbCrLf & vbCrLf & LRS(15060))
                Else
                    err.Raise(15107, "EDI2xml", sErrorText & vbCrLf & sErrPaymentInfo & vbCrLf & vbCrLf & LRS(15060))
                End If
            End If
        End If

        AddToErrorArray = True

    End Function
    Private Function RemoveZeroAmounts(ByRef oPayment As vbBabel.Payment) As Integer
        ' Not allowed to export 0-transactions to TBI
        ' Check for, and remove, invoices with 0-amount
        Dim l, j As Integer
        Dim oInvoice As vbBabel.Invoice
        Dim aFreetexts() As String

        'On Error GoTo ERRHAndler

        ReDim aFreetexts(0)
        RemoveZeroAmounts = 0

        If oPayment.Invoices.Count = 0 Then
            ' No invoices in payment, handle this in WriteTBIXMLFile
            RemoveZeroAmounts = -1
        ElseIf oPayment.Invoices.Count = 1 Then
            If oPayment.Invoices(1).MON_InvoiceAmount = 0 Then
                RemoveZeroAmounts = -2
            End If
        Else
            ' More than one invoice
            For l = oPayment.Invoices.Count To 1 Step -1
                oInvoice = oPayment.Invoices(l)

                If oInvoice.MON_InvoiceAmount = 0 Then
                    ' must remove this one, but take care for text!
                    For Each oFreeText In oInvoice.Freetexts
                        If oFreeText.Index > 1 Then
                            ReDim Preserve aFreetexts(oFreeText.Index - 1)
                        End If
                        aFreetexts(UBound(aFreetexts)) = oFreeText.Text
                    Next oFreeText
                    oPayment.Invoices.Remove((oInvoice.Index))
                    RemoveZeroAmounts = 1 ' One or more invoices removed
                Else

                    ' if freetext from deleted invoice, then add to current
                    If UBound(aFreetexts) > 0 Or aFreetexts(0) <> "" Then
                        For j = 0 To UBound(aFreetexts)
                            oInvoice.Freetexts.Add()
                            oInvoice.Freetexts(oInvoice.Freetexts.Count).Text = aFreetexts(j)
                        Next j
                    End If
                    ReDim aFreetexts(0)
                End If
            Next
            ' Check if all invoices are zero
            If oPayment.Invoices.Count = 0 Then
                ' No invoices left in payment, handle this in WriteTBIXMLFile
                RemoveZeroAmounts = -1
            Else
                If UBound(aFreetexts) > 0 Or aFreetexts(0) <> "" Then
                    oInvoice = oPayment.Invoices(1)
                    For j = 0 To UBound(aFreetexts)
                        oInvoice.Freetexts.Add()
                        oInvoice.Freetexts(oInvoice.Freetexts.Count).Text = aFreetexts(j)
                    Next j
                End If
            End If
        End If
        oInvoice = Nothing
        Exit Function

        'ERRHAndler:
        '        RemoveZeroAmounts = -3
        '        oInvoice = Nothing

    End Function
End Module
