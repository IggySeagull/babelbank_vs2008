Option Strict Off
Option Explicit On
Module WriteBankDataDK_45
	
	'Dim nNoOfTransactions As Double
	Dim nFileSumAmount As Double
    Dim nFileNoRecords As Short
    Dim bThisIsVismaIntegrator As Boolean
    Function WriteBankdata_DK_45(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilename As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' NB: Version 4.5 of Bankdata


        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim i As Short
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim bFileStartWritten As Boolean
        Dim nNoOfPayments As Integer
        Dim iFileNo As Short
        Dim bSplitFiles As Boolean
        Dim sFilenameOut As String
        Dim iFilesetupOut As Short

        '***************VISMA*****************************
        ' Testing if this is Visma Integrator
        bThisIsVismaIntegrator = IsThisVismaIntegrator()

        Try

            oFs = New Scripting.FileSystemObject

            bAppendFile = False
            nNoOfPayments = 0 ' Added 11.09.06, to be able to split exportfile into chuncks of 999 payments
            If bThisIsVismaIntegrator Then
                sFilenameOut = sFilename
            Else

                iFilesetupOut = oBabelFiles(1).VB_Profile.FileSetups(oBabelFiles(1).VB_FileSetupID).FileSetupOut
                If oBabelFiles(1).VB_Profile.FileSetups(iFilesetupOut).SplitAfter450 Then
                    iFileNo = 1 ' Added 11.09.06, to be able to split exportfile into chuncks of 999 payments
                    bSplitFiles = True
                    ' Added 11.09.06, to be able to split exportfile into chuncks of 999 payments
                    ' Put in a filecounter before .extension
                    sFilenameOut = Replace(sFilename, ".", Trim(Str(iFileNo)) & ".")
                Else
                    sFilenameOut = sFilename
                End If
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bFileStartWritten = False

            For Each oBabel In oBabelFiles


                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteBankdata_Start()
                        oFile.WriteLine((sLine))
                        bFileStartWritten = True
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    nNoOfPayments = nNoOfPayments + 1 ' added 11.09.06
                                    ' Added next If/ElseIF block 11.09.06
                                    If nNoOfPayments > 999 Then
                                        If bSplitFiles Then
                                            ' Split in separate files
                                            ' Write End of File
                                            sLine = WriteBankdata_Slut()
                                            oFile.WriteLine((sLine))
                                            oFile.Close()
                                            ' Create new file
                                            iFileNo = iFileNo + 1
                                            nNoOfPayments = 1
                                            ' Put in a filecounter before .extension
                                            'UPGRADE_WARNING: Couldn't resolve default property of object sFilename. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            sFilenameOut = Replace(sFilename, ".", Trim(Str(iFileNo)) & ".")
                                            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
                                            bFileStartWritten = False
                                            If Not bFileStartWritten Then
                                                ' write only once!
                                                ' When merging Client-files, skip this one except for first client!
                                                sLine = WriteBankdata_Start()
                                                oFile.WriteLine((sLine))
                                                bFileStartWritten = True
                                            End If
                                        End If
                                    End If
                                    ' --- End 11.09.06 ---

                                    '-------- fetch content of each payment ---------
                                    ' Total up freetext from all invoices;
                                    sFreetext = ""
                                    For Each oInvoice In oPayment.Invoices
                                        ' XOKnet 17.09.2015
                                        If Not oInvoice.Exported Then
                                            For Each oFreeText In oInvoice.Freetexts
                                                'sFreetext = sFreetext & " " & oFreeText.Text
                                                ' changed 13.11.06
                                                ' in danish formats we usually have 35 chars textslines
                                                ' in BB we have 40. Test if last 5 are blank, and if so, use only 35 first
                                                If EmptyString(Mid(oFreeText.Text, 36, 5)) Then
                                                    sFreetext = sFreetext & " " & Left(oFreeText.Text, 35)
                                                Else
                                                    sFreetext = sFreetext & " " & oFreeText.Text
                                                End If
                                            Next oFreeText
                                        End If
                                    Next oInvoice
                                    sFreetext = Trim(sFreetext)

                                    If oPayment.ToOwnAccount Then
                                        sLine = WriteBankdata_OwnAccount(oPayment)
                                        oFile.WriteLine((sLine))
                                        oPayment.Exported = True
                                    ElseIf oPayment.PayCode = "301" Then
                                        ' For FI/Giro-cards, must process one invoice at a time:
                                        For Each oInvoice In oPayment.Invoices
                                            ' XOKNET 17.09.2015
                                            ' Do not export cancelled payments
                                            If Not oInvoice.Exported Then
                                                sLine = WriteBankdata_Indbetalingskort(oPayment, oInvoice, sFreetext)
                                                oFile.WriteLine((sLine))
                                            End If
                                        Next oInvoice
                                        oPayment.Exported = True
                                    Else
                                        If oPayment.PayType <> "I" Then
                                            sLine = WriteBankdata_Indland(oPayment, sFreetext)
                                            oFile.WriteLine((sLine))
                                            oPayment.Exported = True
                                            ' Do not write recordtype Afsenderoplysninger og referencefelter (Index 2)

                                            ' If more freetext than 9*35, we must use Index 3 and 4
                                            If Len(Trim(sFreetext)) > (9 * 35) Then
                                                ' 22 lines a 35, index "0003"
                                                sLine = WriteBankdata_Adviseringslinier(Mid(sFreetext, 9 * 35 + 1, 22 * 35), "0003")
                                                oFile.WriteLine((sLine))
                                                If Len(sFreetext) > (9 * 35) + (22 * 35) Then
                                                    ' 22 lines a 35, index "0004"
                                                    sLine = WriteBankdata_Adviseringslinier(Mid(sFreetext, 9 * 35 + 22 * 35 + 1, 22 * 35), "0004")
                                                    oFile.WriteLine((sLine))
                                                End If
                                            End If

                                        Else
                                            sLine = WriteBankdata_Udland(oPayment, sFreetext)
                                            oFile.WriteLine((sLine))
                                            oPayment.Exported = True
                                        End If
                                    End If


                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                    sLine = WriteBankdata_Slut()
                    oFile.WriteLine((sLine))
                    ' XOKNET 17.09.2015
                    bFileStartWritten = False

                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBankdata_DK_45" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBankdata_DK_45 = True

    End Function
	Private Function WriteBankdata_Start() As String
		Dim sLine As String
		
		' Reset file-totals:
		nFileSumAmount = 0
		nFileNoRecords = 0
		
		sLine = Chr(34) & "IB000000000000" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & VB6.Format(Date.Today, "YYYYMMDD") & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(90) & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(255) & Chr(34)

        WriteBankdata_Start = sLine
    End Function
    Private Function WriteBankdata_OwnAccount(ByRef oPayment As Payment) As String
        Dim sLine As String

        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = Chr(34) & "IB030201000002" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & "," ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & "," ' 26 Expeditionsdato
        sLine = sLine & Chr(34) & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        If Len(oPayment.MON_InvoiceCurrency) = 0 Then
            oPayment.MON_InvoiceCurrency = "DKK"
        End If
        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_InvoiceCurrency, 3, " ") & Chr(34) & "," '54 M�ntsort
        sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 60 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & "," ' 64 Kontonr afsender, plus ledende 0
        sLine = sLine & Chr(34) & Left(oPayment.E_Account, 4) & Chr(34) & "," ' 82 Regnr modtager
        sLine = sLine & Chr(34) & Mid(oPayment.E_Account, 5, 10) & Chr(34) & "," ' 89 Kontonr modtager
        ' XokNET 25.11.2015 VISMA
        If bThisIsVismaIntegrator Then
            If oPayment.Invoices.Count > 0 Then
                ' set InvoiceNo as Egenref if present;
                If Not EmptyString(oPayment.Invoices(1).InvoiceNo) Then
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).InvoiceNo, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
                ElseIf Not EmptyString(oPayment.Invoices(1).Unique_Id) Then
                    ' or use KID/FIK if present
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).Unique_Id, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
                Else
                    ' or ref_own
                    sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
                End If
            Else
                sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
            End If
        Else
            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
        End If


        sLine = sLine & Chr(34) & Space(20) & Chr(34) & "," ' 140
        sLine = sLine & Chr(34) & Space(255) & Chr(34) ' 163
        sLine = sLine & Chr(34) & Space(255) & Chr(34) ' 421
        sLine = sLine & Chr(34) & Space(215) & Chr(34) ' 879

        WriteBankdata_OwnAccount = sLine

    End Function
    Function WriteBankdata_Indland(ByVal oPayment As Payment, ByVal sFreetext As String) As String
        Dim sLine As String
        Dim i As Integer
        Dim nMonAmount As Double
        ' XOKNET 07.01.2016 - now finds text from not exported invoices
        Dim oInvoice As Invoice
        Dim oFreetext As FreeText
        sFreetext = ""
        nMonAmount = 0


        sLine = Chr(34) & "IB030202000005" & Chr(34) & ","                  ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & ","                       ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & ","     ' 26 Expeditionsdato
        ' XokNET 07.01.2016
        ' Due to Visma, we may have invoices set to not export
        ' Total up amount, and find freetext;
        For Each oInvoice In oPayment.Invoices
            If Not oInvoice.Exported Then
                nMonAmount = nMonAmount + oInvoice.MON_InvoiceAmount
                For Each oFreetext In oInvoice.Freetexts
                    sFreetext = sFreetext & oFreetext.Text
                Next oFreetext
            End If
        Next oInvoice
        ' XOKNET 07.01.2016
        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + nMonAmount
        nFileNoRecords = nFileNoRecords + 1
        'sLine = sLine & Chr(34) & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        sLine = sLine & Chr(34) & PadLeft(Str(nMonAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        If Len(oPayment.MON_InvoiceCurrency) = 0 Then
            oPayment.MON_InvoiceCurrency = "DKK"
        End If
        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_InvoiceCurrency, 3, " ") & Chr(34) & ","  '54 M�ntsort
        sLine = sLine & Chr(34) & "2" & Chr(34) & ","                       ' 60 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & ","      ' 64 Kontonr afsender, plus ledende 0
        If Len(Trim$(oPayment.E_Account)) = 0 Then
            sLine = sLine & Chr(34) & "1" & Chr(34) & ","                 ' 82 - "1" = Send check
        Else
            sLine = sLine & Chr(34) & "2" & Chr(34) & ","                 ' 82 - "2" = Overf�r til bankkonto
        End If
        sLine = sLine & Chr(34) & PadRight(Left$(oPayment.E_Account, 4), 4, " ") & Chr(34) & ","     ' 86 Regnr modtager
        sLine = sLine & Chr(34) & PadRight(Mid$(oPayment.E_Account, 5, 10), 10, " ") & Chr(34) & ","    ' 93 Kontonr modtager

        If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.SendToReceiver Then
            sLine = sLine & Chr(34) & "2" & Chr(34) & ","                 ' 106 - "2" = Send check til modtager
            'ElseIf oPayment.PayCode = "404" Then ''' changed 07.2.05 Or Len(Trim$(sFreetext)) > 35 Then
            ' XOKNET 17.09.2015
        ElseIf oPayment.PayCode = "404" Or oPayment.Priority Then
            ' If "Betaling med melding", eller melding s� lang at vi ikke f�r plass p� 35 pos, i tekst kontoutdrag
            'sLine = sLine & Chr(34) & "1" & Chr(34) & ","                 ' 106 - "1" = Hurtigadvis Endret til 3
            ' XOKNET 17.09.2015,
            sLine = sLine & Chr(34) & "3" & Chr(34) & ","                 ' 106 - "1" = Hurtigadvis Endret til 3
            sLine = sLine & Chr(34) & PadRight("", 35, " ") & Chr(34) & "," ' 110 Posteringstekst
        Else
            ' XOKNET 17.09.2015
            ' Det kan vel ikke settes henvendelse p� mottakser kontoutskrift hvis det ikke er angitt et kontonummer???
            ' Men trenger adresse !
            If Len(oPayment.E_Account) = 0 And Len(oPayment.E_Name) > 0 And Len(oPayment.E_Adr1) > 0 Then
                sLine = sLine & Chr(34) & "2" & Chr(34) & ","                 ' 106 - "2" = Send check til modtager
                sLine = sLine & Chr(34) & PadRight("", 35, " ") & Chr(34) & "," ' 110 Posteringstekst
            Else
                sLine = sLine & Chr(34) & "0" & Chr(34) & ","                 ' 106 - "0" = Meddelelse p� modtagers kontoudskrift
                If Len(Trim(oPayment.Text_E_Statement)) > 0 Then
                    sLine = sLine & Chr(34) & PadRight(oPayment.Text_E_Statement, 35, " ") & Chr(34) & "," ' 110 Posteringstekst
                Else
                    sLine = sLine & Chr(34) & PadRight(Left(sFreetext, 35), 35, " ") & Chr(34) & "," ' 110 Posteringstekst
                End If
            End If
        End If
        ' XOKNET 17.09.15 Flytt inn i If-en over
        'sLine = sLine & Chr(34) & PadRight(oPayment.Text_E_Statement, 35, " ") & Chr(34) & "," ' 110 Posteringstekst

        sLine = sLine & Chr(34) & PadRight(oPayment.E_Name, 32, " ") & Chr(34) & ","    ' 148 Navn p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr1, 32, " ") & Chr(34) & ","    ' 183 Adr1 p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr2, 32, " ") & Chr(34) & ","    ' 218 Adr2 p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Zip, 4, " ") & Chr(34) & ","      ' 253 Postnr
        sLine = sLine & Chr(34) & PadRight(oPayment.E_City, 32, " ") & Chr(34) & ","    ' 260 Bynavn

        ' XokNET 25.11.2015 VISMA
        If bThisIsVismaIntegrator Then
            If oPayment.Invoices.Count > 0 Then
                ' set InvoiceNo as Egenref if present;
                If Not EmptyString(oPayment.Invoices(1).InvoiceNo) Then
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).InvoiceNo, 35, " ") & Chr(34) & ","        ' 295 Eget bilagsnr
                ElseIf Not EmptyString(oPayment.Invoices(1).Unique_Id) Then
                    ' or use KID/FIK if present
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).Unique_Id, 35, " ") & Chr(34) & ","        ' 295 Eget bilagsnr
                Else
                    ' or start of freetext
                    sLine = sLine & Chr(34) & PadRight(sFreetext, 35, " ") & Chr(34) & ","        ' 295 Eget bilagsnr
                End If
            Else
                sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 295 Eget bilagsnr
            End If
        Else
            ' as pre 25.11.2015
            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","   ' 295 Eget bilagsnr
        End If

        ' Upto 9 fields of 35 chars with freetext, Notification to receiver
        ' Pad to 9*35
        sFreetext = PadRight(sFreetext, 9 * 35, " ")
        For i = 1 To 9
            sLine = sLine & Chr(34) & Mid$(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 333 - 637 Adviseringstekst 1-10
        Next i

        sLine = sLine & Chr(34) & Space(1) & Chr(34) & ","                  ' 675
        sLine = sLine & Chr(34) & Space(215) & Chr(34)                      ' 679

        ' Check if we are going to write index 2;
        ' 14.03.06 Added oPayment.Text_I_Statement
        If Len(Trim$(oPayment.REF_Bank1)) > 0 Or Len(Trim$(oPayment.REF_Bank2)) > 0 _
        Or Len(Trim$(oPayment.Text_I_Statement)) > 0 Then
            sLine = sLine & vbCrLf  ' Must also write line 1 !
            sLine = sLine & Chr(34) & "IB030202000005" & Chr(34) & ","                 ' 1-18
            sLine = sLine & Chr(34) & "0002" & Chr(34) & ","                       ' 19 Index

            sLine = sLine & Chr(34) & PadRight(oPayment.I_Name, 35, " ") & Chr(34) & ","    ' 3  -26
            sLine = sLine & Chr(34) & PadRight(oPayment.I_Adr1, 35, " ") & Chr(34) & ","    ' 4  -64
            sLine = sLine & Chr(34) & PadRight(oPayment.I_Adr2, 35, " ") & Chr(34) & ","    ' 5  -102
            sLine = sLine & Chr(34) & PadRight(oPayment.I_Adr3, 35, " ") & Chr(34) & ","    ' 6  -140
            sLine = sLine & Chr(34) & Space(35) & Chr(34) & ","                             ' 7  -178


            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Bank1, 35, " ") & Chr(34) & ","          ' 8  -216   Creditor ID
            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Bank2, 35, " ") & Chr(34) & ","          ' 9  -254   Primary document
            sLine = sLine & Chr(34) & PadRight(oPayment.Text_I_Statement, 35, " ") & Chr(34) & ","   ' 10 -292   DebitorID (text on statement)
            sLine = sLine & Chr(34) & Space(48) & Chr(34) & ","
            sLine = sLine & Chr(34) & Space(255) & Chr(34) & ","
            sLine = sLine & Chr(34) & Space(255) & Chr(34)

        End If
        WriteBankdata_Indland = sLine

    End Function

    Function WriteBankdata_Udland(ByRef oPayment As Payment, ByVal sFreetext As String) As String
        Dim sLine As String
        Dim i As Short

        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = Chr(34) & "IB030204000003" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & "," ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & "," ' 26 Expeditionsdato
        sLine = sLine & Chr(34) & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 54 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & "," ' 58 Kontonr afsender, plus ledende 0

        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_InvoiceCurrency, 3, " ") & Chr(34) & "," '76 M�ntkode, der avregnes
        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_TransferCurrency, 3, " ") & Chr(34) & "," '82 Overf�rselsm�nt

        If Len(Trim(oPayment.E_Account)) = 0 Then ' Overf�rselstype
            sLine = sLine & Chr(34) & "21" & Chr(34) & "," ' 88 - "21" = Send check
        ElseIf oPayment.ToOwnAccount Then
            sLine = sLine & Chr(34) & "22" & Chr(34) & "," ' 88 - "22" = SEB koncernbetaling
        ElseIf oPayment.Priority Then
            sLine = sLine & Chr(34) & "26" & Chr(34) & "," ' 88 - "26" = Ekspress
        Else
            sLine = sLine & Chr(34) & "25" & Chr(34) & "," ' 88 - "25" = Normal betaling
        End If

        ' 4 fields of 35 chars with freetext, Notification to receiver
        ' Pad to 4*35
        sFreetext = PadRight(sFreetext, 4 * 35, " ")
        For i = 1 To 4
            sLine = sLine & Chr(34) & Mid(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 93 - 207 Adviseringstekst 1-4
        Next i

        sLine = sLine & Chr(34) & PadRight(oPayment.E_Name, 35, " ") & Chr(34) & "," ' 245 Navn p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr1, 35, " ") & Chr(34) & "," ' 283 Adr1 p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr2, 35, " ") & Chr(34) & "," ' 321 Adr2 p� modtager
        ' XOKNET 17.09.2015
        If Len(Trim$(oPayment.E_Account)) = 0 Then
            ' felt 17 land, kun for sjekk
            sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr3, 35, " ") & Chr(34) & ","    ' 359 Land
        Else
            sLine = sLine & Chr(34) & "" & Chr(34) & ","    ' 359 Land
        End If

        ' Data to DanskeRiksbank
        sLine = sLine & Chr(34) & PadRight(Left(oPayment.Invoices(1).STATEBANK_Code, 4), 4, " ") & Chr(34) & "," ' 397 Betalingsform�lskode til Nationalbanken
        sLine = sLine & Chr(34) & Space(6) & Chr(34) & "," ' 404 M�ned/�r for inf�rsel, ikke i bruk
        sLine = sLine & Chr(34) & PadRight(Left(oPayment.E_CountryCode, 2), 2, " ") & Chr(34) & "," ' 413, landkode kreditor
        sLine = sLine & Chr(34) & PadRight(Left(oPayment.Invoices(1).STATEBANK_Text, 75), 75, " ") & Chr(34) & "," ' 418 Andre betalingslinjer - 1
        If Len(oPayment.Invoices(1).STATEBANK_Text) > 75 Then
            sLine = sLine & Chr(34) & PadRight(Mid(oPayment.Invoices(1).STATEBANK_Text, 76, 75), 75, " ") & Chr(34) & "," ' 496 Andre betalingslinjer - 2
        Else
            sLine = sLine & Chr(34) & Space(75) & Chr(34) & "," ' 496
        End If
        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().STATEBANK_Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If Len(oPayment.Invoices(1).STATEBANK_Text) > 150 Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().STATEBANK_Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & Chr(34) & PadRight(Mid(oPayment.Invoices(1).STATEBANK_Text, 151, 75), 75, " ") & Chr(34) & "," ' 574 Andre betalingslinjer - 3
        Else
            sLine = sLine & Chr(34) & Space(75) & Chr(34) & "," ' 574
        End If

        sLine = sLine & Chr(34) & Space(24) & Chr(34) & "," ' 652
        sLine = sLine & Chr(34) & Space(215) & Chr(34) ' 679

        'Write index 2

        sLine = sLine & vbCrLf ' Must also write line 1 !
        sLine = sLine & Chr(34) & "IB030204000003" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & "0002" & Chr(34) & "," ' 19 Index
        If oPayment.MON_ChargeMeDomestic Then
            sLine = sLine & Chr(34) & "0" & Chr(34) & "," ' 26 Omk-DK  0=Ordregiver
        Else
            sLine = sLine & Chr(34) & "1" & Chr(34) & "," ' 26 Omk-DK  1=Modtager
        End If
        If Len(Trim(oPayment.E_Account)) = 0 Then
            sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 30 - "2" = Sendes til modtager
        Else
            sLine = sLine & Chr(34) & " " & Chr(34) & "," ' 30
        End If

        sLine = sLine & Chr(34) & PadRight(oPayment.BANK_SWIFTCode, 11, " ") & Chr(34) & "," ' 34 Swift
        sLine = sLine & Chr(34) & PadRight(oPayment.BANK_Name, 35, " ") & Chr(34) & "," ' 48 banknavn
        sLine = sLine & Chr(34) & PadRight(oPayment.BANK_Adr1, 35, " ") & Chr(34) & "," ' 86 Bankadr1
        sLine = sLine & Chr(34) & PadRight(oPayment.BANK_Adr2, 35, " ") & Chr(34) & "," ' 124 Bank adr2
        sLine = sLine & Chr(34) & PadRight(oPayment.BANK_Adr3, 35, " ") & Chr(34) & "," ' 162 Bank land
        sLine = sLine & Chr(34) & PadRight(oPayment.BANK_BranchNo, 33, " ") & Chr(34) & "," ' 200 Bankkode

        ' Either AcountNo or IBANN
        If IsNumeric(Left(oPayment.E_Account, 2)) Then
            sLine = sLine & Chr(34) & PadRight(oPayment.E_Account, 34, " ") & Chr(34) & "," ' 236 Mottaker konto
            sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 273 IBANN
        Else
            sLine = sLine & Chr(34) & Space(34) & Chr(34) & "," ' 236
            sLine = sLine & Chr(34) & PadRight(oPayment.E_Account, 35, " ") & Chr(34) & "," ' 273 IBANN
        End If

        If oPayment.MON_ChargeMeAbroad Then
            sLine = sLine & Chr(34) & "0" & Chr(34) & "," ' 311 Omk-udland  0=Ordregiver
        Else
            sLine = sLine & Chr(34) & "1" & Chr(34) & "," ' 311 Omk-udland  1=Modtager
        End If
        sLine = sLine & Chr(34) & Space(15) & Chr(34) & "," ' 315 Gebyrkonto, not in use

        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 333 Bem�rkning, not in use
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 371 Bem�rkning, not in use
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 409 Bem�rkning, not in use

        ' XokNET 25.11.2015 VISMA
        If bThisIsVismaIntegrator Then
            If oPayment.Invoices.Count > 0 Then
                ' set InvoiceNo as Egenref if present;
                If Not EmptyString(oPayment.Invoices(1).InvoiceNo) Then
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).InvoiceNo, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
                ElseIf Not EmptyString(oPayment.Invoices(1).Unique_Id) Then
                    ' or use KID/FIK if present
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).Unique_Id, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr
                Else
                    ' or start of freetext
                    sLine = sLine & Chr(34) & PadRight(sFreetext, 35, " ") & Chr(34) & ","        ' 447 Eget bilagsnr
                End If
            Else
                sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 447 Eget bilagsnr
            End If
        Else
            ' as pre 25.11.2015
            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","         ' 447 Eget bilagsnr
        End If

        sLine = sLine & Chr(34) & Space(15) & Chr(34) & "," ' 485 Frakonto1 not in use
        sLine = sLine & Chr(34) & Space(13) & Chr(34) & "," ' 503 Bel�b 1 not in use

        ' From 519 - 715 NOT IMPLEMENTED
        sLine = sLine & Chr(34) & Space(1) & Chr(34) & "," ' 519 Not implemented
        sLine = sLine & Chr(34) & Space(7) & Chr(34) & "," ' 523 Not implemented
        sLine = sLine & Chr(34) & Space(15) & Chr(34) & "," ' 533 Not implemented
        sLine = sLine & Chr(34) & Space(13) & Chr(34) & "," ' 551 Not implemented
        sLine = sLine & Chr(34) & Space(1) & Chr(34) & "," ' 567 Not implemented
        sLine = sLine & Chr(34) & Space(7) & Chr(34) & "," ' 571 Not implemented
        sLine = sLine & Chr(34) & Space(15) & Chr(34) & "," ' 581 Not implemented
        sLine = sLine & Chr(34) & Space(13) & Chr(34) & "," ' 599 Not implemented
        sLine = sLine & Chr(34) & Space(1) & Chr(34) & "," ' 615 Not implemented
        sLine = sLine & Chr(34) & Space(7) & Chr(34) & "," ' 619 Not implemented
        sLine = sLine & Chr(34) & Space(15) & Chr(34) & "," ' 629 Not implemented
        sLine = sLine & Chr(34) & Space(13) & Chr(34) & "," ' 647 Not implemented
        sLine = sLine & Chr(34) & Space(1) & Chr(34) & "," ' 663 Not implemented
        sLine = sLine & Chr(34) & Space(7) & Chr(34) & "," ' 667 Not implemented
        sLine = sLine & Chr(34) & Space(15) & Chr(34) & "," ' 677 Not implemented
        sLine = sLine & Chr(34) & Space(13) & Chr(34) & "," ' 695 Not implemented
        sLine = sLine & Chr(34) & Space(1) & Chr(34) & "," ' 711 Not implemented
        sLine = sLine & Chr(34) & Space(7) & Chr(34) & "," ' 715 Not implemented

        sLine = sLine & Chr(34) & Space(169) & Chr(34) ' 725


        WriteBankdata_Udland = sLine

    End Function
    Function WriteBankdata_Indbetalingskort(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sFreetext As String) As String
        Dim sLine As String
        Dim i As Integer

        'PS Must run once for each invoice in a payment!

        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = Chr(34) & "IB030207000002" & Chr(34) & ","                  ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & ","                       ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & ","     ' 26 Expeditionsdato
        sLine = sLine & Chr(34) & PadLeft(Str(oInvoice.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p

        sLine = sLine & Chr(34) & "2" & Chr(34) & ","                       ' 54 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & ","      ' 58 Kontonr afsender, plus ledende 0

        sLine = sLine & Chr(34) & Left$(oInvoice.Unique_Id, 2) & Chr(34) & ","                      ' 76 - kortart
        ' XOKNET 07.01.2016 - Ikke betalingsid for girokort 01
        If Left$(oInvoice.Unique_Id, 2) = "01" Then
            sLine = sLine & Chr(34) & Space(19) & Chr(34) & ","    ' 81 Betalingsid
        Else
            sLine = sLine & Chr(34) & PadRight(Mid$(oInvoice.Unique_Id, 3), 19, " ") & Chr(34) & ","    ' 81 Betalingsid
        End If
        ' XOKNET 07.01.2016 - for Girokort, bruk giro konto
        If Left$(oInvoice.Unique_Id, 2) = "01" Or Left$(oInvoice.Unique_Id, 2) = "04" Or Left$(oInvoice.Unique_Id, 2) = "15" Then
            sLine = sLine & Chr(34) & Space(4) & Chr(34) & ","                  ' 103 Giro reg.nr Ikke i bruk
            sLine = sLine & Chr(34) & PadRight(oPayment.E_Account, 10, " ") & Chr(34) & ","                  ' 110 Giro konto.nr N�r brukes dette ?
            sLine = sLine & Chr(34) & Space(8) & Chr(34) & "," ' 123 Modtagers kreditornr
        Else
            sLine = sLine & Chr(34) & Space(4) & Chr(34) & ","                  ' 103 Giro reg.nr Ikke i bruk
            sLine = sLine & Chr(34) & Space(10) & Chr(34) & ","                  ' 110 Giro konto.nr N�r brukes dette ?
            sLine = sLine & Chr(34) & PadLeft(oPayment.E_Account, 8, "0") & Chr(34) & "," ' 123 Modtagers kreditornr
        End If
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Name, 32, " ") & Chr(34) & ","    ' 134 Navn p� modtager
        sLine = sLine & Chr(34) & Space(32) & Chr(34) & ","                             ' 169 Navn fra register, ikke i bruk

        ' XokNET 25.11.2015 VISMA
        If bThisIsVismaIntegrator Then
            If oPayment.Invoices.Count > 0 Then
                ' set InvoiceNo as Egenref if present;
                If Not EmptyString(oPayment.Invoices(1).InvoiceNo) Then
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).InvoiceNo, 35, " ") & Chr(34) & ","        ' 204 Eget bilagsnr
                ElseIf Not EmptyString(oPayment.Invoices(1).Unique_Id) Then
                    ' or use KID/FIK if present
                    sLine = sLine & Chr(34) & PadRight(oPayment.Invoices(1).Unique_Id, 35, " ") & Chr(34) & ","        ' 204 Eget bilagsnr
                Else
                    ' or start of freetext
                    sLine = sLine & Chr(34) & PadRight(sFreetext, 35, " ") & Chr(34) & ","        ' 204 Eget bilagsnr
                End If
            Else
                sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 204 Eget bilagsnr
            End If
        Else
            ' as pre 25.11.2015
            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","   ' 204 Eget bilagsnr
        End If

        sLine = sLine & Chr(34) & Space(35) & Chr(34) & ","                      ' 242
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & ","                      ' 280
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & ","                      ' 318
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & ","                      ' 356
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & ","                      ' 394

        ' Upto 6 fields of 35 chars with freetext, Notification to receiver
        ' Pad to 6*35
        sFreetext = PadRight(sFreetext, 6 * 35, " ")
        For i = 1 To 6
            sLine = sLine & Chr(34) & Mid$(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 432 - 622 Adviseringstekst 1-7
        Next i

        sLine = sLine & Chr(34) & Space(16) & Chr(34) & ","                 ' 660
        sLine = sLine & Chr(34) & Space(215) & Chr(34)                      ' 679

        WriteBankdata_Indbetalingskort = sLine

    End Function

    Function WriteBankdata_Adviseringslinier(ByVal sFreetext As String, ByRef sIndex As String) As String
        Dim sLine As String
        Dim i As Short

        ' Pad sfreetext to 22 * 35
        sFreetext = PadRight(sFreetext, 22 * 35, " ")

        sLine = Chr(34) & "IB030202000005" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & sIndex & Chr(34) & "," ' 19 Index
        For i = 1 To 22
            sLine = sLine & Chr(34) & Mid(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 26 - 861 Adviseringstekst 1-22
        Next i
        sLine = sLine & Chr(34) & Space(32) & Chr(34) ' 862

        WriteBankdata_Adviseringslinier = sLine

    End Function

    Function WriteBankdata_Slut() As String
        Dim sLine As String

        sLine = Chr(34) & "IB999999999999" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & VB6.Format(Date.Today, "YYYYMMDD") & Chr(34) & "," ' 19
        sLine = sLine & Chr(34) & PadLeft(Str(nFileNoRecords), 6, "0") & Chr(34) & "," ' 30
        sLine = sLine & Chr(34) & PadLeft(Str(nFileSumAmount), 13, "0") & "+" & Chr(34) & "," ' 39
        sLine = sLine & Chr(34) & Space(64) & Chr(34) & "," ' 56
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & "," ' 123
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & "," ' 381
        sLine = sLine & Chr(34) & Space(255) & Chr(34) ' 639

        WriteBankdata_Slut = sLine
    End Function
End Module
