Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("ClientsHandled_NET.ClientsHandled")> Public Class ClientsHandled
	Implements System.Collections.IEnumerable
	
	'local variable to hold collection
	Private mCol As Collection
	'local variable(s) to hold property value(s)
	Private mvarHeader As String 'local copy
	Private bEmailSMTP As Boolean
    Private sEmailReplyAdress, sEmailDisplayName As String
    Private frmClientsHandled As New frmClientsHandled
	Dim seMailSMTPHost, sEmailSender As String
	
	
	Public Property Header() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Header
			Header = mvarHeader
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Header = 5
			mvarHeader = Value
		End Set
	End Property
	Public WriteOnly Property EmailSMTP() As Object
		Set(ByVal Value As Object)
			'UPGRADE_WARNING: Couldn't resolve default property of object NewVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			bEmailSMTP = Value
		End Set
	End Property
	Public WriteOnly Property EmailSMTPHost() As Object
		Set(ByVal Value As Object)
			'UPGRADE_WARNING: Couldn't resolve default property of object NewVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			seMailSMTPHost = Value
		End Set
	End Property
	Public WriteOnly Property EmailReplyAdress() As Object
		Set(ByVal Value As Object)
			'UPGRADE_WARNING: Couldn't resolve default property of object NewVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sEmailReplyAdress = Value
		End Set
	End Property
	Public WriteOnly Property EmailDisplayName() As Object
		Set(ByVal Value As Object)
			'UPGRADE_WARNING: Couldn't resolve default property of object NewVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sEmailDisplayName = Value
		End Set
	End Property
	Public WriteOnly Property EmailSender() As Object
		Set(ByVal Value As Object)
			'UPGRADE_WARNING: Couldn't resolve default property of object NewVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sEmailSender = Trim(Value)
		End Set
	End Property
	
	Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As ClientHandled
		Get
			'used when referencing an element in the collection
			'vntIndexKey contains either the Index or Key to the collection,
			'this is why it is declared as a Variant
			'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
			Item = mCol.Item(vntIndexKey)
		End Get
	End Property
	
	
	
	Public ReadOnly Property Count() As Integer
		Get
			'used when retrieving the number of elements in the
			'collection. Syntax: Debug.Print x.Count
			Count = mCol.Count()
		End Get
	End Property
	
	
	'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
	'Public ReadOnly Property NewEnum() As stdole.IUnknown
		'Get
			'this property allows you to enumerate
			'this collection with the For...Each syntax
			'NewEnum = mCol._NewEnum
		'End Get
	'End Property
	
	Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
		'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        GetEnumerator = mCol.GetEnumerator
	End Function
	Public Function Start() As Boolean
		Dim bStatus As Boolean
        'Collection mCol no holds info about clients, filenames imported or exported,
		' email-adresses (filetypes and filestatus later)
		
		' New by JanP 06.08.02, user may cancel rest of process
		bStatus = True
		bStatus = NotificationToSpread("Screen")
		
		NotificationToSpread("Print")
		NotificationToSpread("File")
        'NotificationToMail()
		
		Start = bStatus
	End Function
    '	Friend Sub NotificationToMail()
    '		'Check to see if any of the clients is ment to be notified by mail
    '		' Run through all email-adresses to group mail by email adress
    '		Dim aEMailAdresses() As String
    '		Dim aMailContent() As String
    '		Dim iNoOfAdresses As Short
    '		Dim iNoOfLines As Short
    '		Dim sMailAdr As String
    '		Dim bHandledSome As Boolean
    '		Dim oClientHandled As ClientHandled
    '		'Dim sCompany As String ' Companyname, who needs support ?
    '		Dim sMsgNote As String
    '		Dim SendMail As String
    '		'Dim sRecipDisplaynames As String
    '		Dim i As Short
    '		Dim iSessionTries As Short
    '		Dim poSendMail As BabelSetup.clsSendMail 'SMTP
    '		Dim sErrDescription As String

    '		bHandledSome = False
    '		iNoOfAdresses = -1
    '		iNoOfLines = -1
    '		ReDim aEMailAdresses(0)
    '		ReDim aMailContent(0)

    '		' Loop through collection as long as there are items to email
    '		Do 

    '			For	Each oClientHandled In mCol 'mCol = ClientsHandled
    '				If oClientHandled.ClientToMail Then
    '					If sMailAdr = "" Then
    '						' First for this email-adress
    '						sMailAdr = oClientHandled.EMail
    '						If Len(sMailAdr) > 0 Then
    '							iNoOfAdresses = iNoOfAdresses + 1
    '							ReDim Preserve aEMailAdresses(iNoOfAdresses)
    '							aEMailAdresses(iNoOfAdresses) = sMailAdr
    '						End If
    '					End If
    '					If oClientHandled.EMail = sMailAdr And Len(sMailAdr) > 0 Then
    '						' Fill in details in content of email
    '						If bHandledSome Then
    '							' this adress has more than one client/file,
    '							' just add new lines with client and filename only:
    '						Else
    '							' first for this adress, let mailadress be part of mailcontent
    '							iNoOfLines = iNoOfLines + 1
    '							ReDim Preserve aMailContent(iNoOfLines)
    '							' Blank linje for ny adresse i mail;
    '							aMailContent(iNoOfLines) = " "
    '							iNoOfLines = iNoOfLines + 1
    '							ReDim Preserve aMailContent(iNoOfLines)
    '							aMailContent(iNoOfLines) = "For " & oClientHandled.EMail & " :"
    '						End If
    '						iNoOfLines = iNoOfLines + 1
    '						ReDim Preserve aMailContent(iNoOfLines)
    '						aMailContent(iNoOfLines) = "   - Klient: " & oClientHandled.Client & " Filnavn " & oClientHandled.File

    '						oClientHandled.ClientToMail = False ' handled!
    '						bHandledSome = True
    '					End If
    '				End If
    '			Next oClientHandled
    '			' one mailadress handled, keep on if anyone left
    '			If Not bHandledSome Then
    '				Exit Do
    '			Else
    '				' possibly more to come ...
    '				sMailAdr = ""
    '				bHandledSome = False
    '			End If

    '		Loop 


    '		'If bHandledSome Then
    '		If iNoOfAdresses > -1 Then
    '			If bEmailSMTP Then
    '				' Use SMTP

    '				On Error GoTo errSMTP
    '				poSendMail = New BabelSetup.clsSendMail
    '				If Len(Trim(seMailSMTPHost)) > 0 Then
    '					' Leave SMTPHost blank if unknown, clsSendMail will search!
    '					poSendMail.SMTPHost = seMailSMTPHost
    '				End If
    '				poSendMail.From = sEmailSender
    '				poSendMail.FromDisplayName = sEmailDisplayName
    '				For i = 0 To iNoOfAdresses
    '					If i = 0 Then
    '						poSendMail.Recipient = aEMailAdresses(i)
    '					Else
    '						poSendMail.Recipient = poSendMail.Recipient & ";" & aEMailAdresses(i)
    '					End If
    '				Next i
    '				poSendMail.ReplyToAddress = sEmailReplyAdress
    '				poSendMail.Subject = "Babelbank" & " " & mvarHeader
    '				'poSendMail.Attachment = BB_DatabasePath & ";" & BB_LicensePath
    '				' Put toghether from emailadresses, clientno and filename;
    '				For i = 0 To iNoOfLines '- 1
    '					sMsgNote = sMsgNote & aMailContent(i) & vbCr
    '				Next i
    '				poSendMail.Message = sMsgNote
    '				poSendMail.Send()
    '				'UPGRADE_NOTE: Object poSendMail may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '				poSendMail = Nothing

    '			Else

    '				' Use MAPI
    '				' Compose mail
    '				On Error GoTo MAPIFail

    '				frmClientsHandled.MAPISession1.SignOn()

    '				With frmClientsHandled.MAPIMessages1


    '					' If problems with sessionid:
    '					Do While iSessionTries < 1000
    '						.SessionID = frmClientsHandled.MAPISession1.SessionID
    '						If .SessionID > 0 Then
    '							Exit Do
    '						End If

    '						iSessionTries = iSessionTries + 1
    '					Loop 


    '					If .SessionID > 0 Then

    '						'.MsgIndex = -1
    '						.Compose()
    '						.MsgSubject = "Babelbank" & " " & mvarHeader

    '						sMsgNote = "Babelbank" & " " & mvarHeader & vbCr & vbCr
    '						' Mailtext;
    '						' Put toghether from emailadresses, clientno and filename;
    '						For i = 0 To iNoOfLines '- 1
    '							sMsgNote = sMsgNote & aMailContent(i) & vbCr
    '						Next i
    '						.MsgNoteText = sMsgNote


    '						For i = 0 To iNoOfAdresses
    '							.RecipIndex = i
    '							.RecipType = MSMAPI.RecipTypeConstants.mapToList
    '							'.RecipAddress = aEMailAdresses(i)
    '							.RecipDisplayName = aEMailAdresses(i)
    '						Next i
    '						'.AddressResolveUI = False
    '						'.RecipType = 1

    '						.Send(True)

    '						frmClientsHandled.MAPISession1.SignOff()

    '					End If
    '				End With

    '			End If ' bHandledSome
    '		End If 'SMTP

    '		Exit Sub

    'errSMTP: 
    '		'UPGRADE_NOTE: Object poSendMail may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '		poSendMail = Nothing
    '		sErrDescription = Err.Description
    '		Err.Raise(15011,  , sErrDescription)

    '		Exit Sub

    'MAPIFail: 
    '		' Test errors:
    '		'Cancel of Mail-session gives Error.!
    '		If Err.Number <> 32001 Then ' User cancelled process
    '			SendMail = Err.Description
    '		End If
    '		If Err.Number <> 32053 Then
    '			frmClientsHandled.MAPISession1.SignOff()
    '		End If

    '	End Sub
	Friend Function NotificationToSpread(ByRef sWhereTo As String) As Boolean
		' Check to see if any of the clients is ment to be notified on screen, printer or to file
		' sWhereTo = Screen , Printer or File
		Dim oClientHandled As ClientHandled
		Dim bFileReportedBefore As Boolean
		Dim aFilesHandled() As String
		Dim i As Integer
        Dim bStatus As Boolean
        Dim txtColumn As DataGridViewTextBoxColumn
		bStatus = True
		
		ReDim aFilesHandled(0)
		aFilesHandled(0) = "xxxxxx"

        With frmClientsHandled.gridClientsHandled
            .ScrollBars = ScrollBars.Both
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            ' not possible to add rows manually!
            .AllowUserToAddRows = False
            .RowHeadersVisible = False
            '.MultiSelect = False
            .BorderStyle = BorderStyle.FixedSingle
            .RowTemplate.Height = 18
            .BackgroundColor = Color.White
            ' to have no marker for "selected" line
            .RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.White
            .RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
            .CellBorderStyle = DataGridViewCellBorderStyle.None
            .ReadOnly = True

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(40030) 'Client
            txtColumn.Width = WidthFromSpreadToGrid(6)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(40030) 'Client
            txtColumn.Width = WidthFromSpreadToGrid(10)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60061) 'Filename
            txtColumn.Width = WidthFromSpreadToGrid(25)
            .Columns.Add(txtColumn)

            frmClientsHandled.Text = mvarHeader

            ' fill grid
            For Each oClientHandled In mCol 'mCol = ClientsHandled
                If (oClientHandled.ClientToScreen And sWhereTo = "Screen") Or (oClientHandled.ClientToPrint And sWhereTo = "Print") Or (oClientHandled.ClientToFile And sWhereTo = "File") Then

                    ' Check if same file have been reported previously
                    bFileReportedBefore = False
                    For i = 0 To UBound(aFilesHandled)
                        If aFilesHandled(i) = oClientHandled.File Then
                            bFileReportedBefore = True
                            Exit For
                        End If
                    Next i

                    If Not bFileReportedBefore Then
                        .Rows.Add()

                        ' 0 Clientnumber
                        .Rows(.RowCount - 1).Cells(0).Value = oClientHandled.Client

                        ' 1 Clientname
                        .Rows(.RowCount - 1).Cells(1).Value = oClientHandled.ClientName

                        ' 2 Filename
                        .Rows(.RowCount - 1).Cells(2).Value = oClientHandled.File

                        ReDim Preserve aFilesHandled(UBound(aFilesHandled) + 1)
                        aFilesHandled(UBound(aFilesHandled)) = oClientHandled.File

                    End If
                End If
            Next oClientHandled

            ' sort on clientno
            .Sort(.Columns.Item(0), System.ComponentModel.ListSortDirection.Ascending)
            '.Rows.Item(0).Cells(0).Selected = True

            'UPGRADE_WARNING: Couldn't resolve default property of object frmClientsHandled.sprClientsHandled.MaxRows. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If .RowCount > 0 Then

                If sWhereTo = "Screen" Then
                    ' to screen only
                    frmClientsHandled.cmdCancel.Text = LRS(55011) 'Cancel process
                    frmClientsHandled.cmdOK.Text = LRS(55012) 'Continue process
                    frmClientsHandled.cmdPrint.Text = LRS(55003) 'Print
                    frmClientsHandled.ShowDialog()
                    ' Give a choice wether to continue or to cancel further processing
                    bStatus = frmClientsHandled.bClientsStatus
                    frmClientsHandled.Close()

                ElseIf sWhereTo = "Print" Then
                    ' to print only

                    ' TODO call new report functionality, ActiveReports

                ElseIf sWhereTo = "File" Then
                    '.SaveTabFile(My.Application.Info.DirectoryPath & "\Clients.txt")

                End If
            End If '.maxrows > 0
        End With
		
		NotificationToSpread = bStatus
		
		
	End Function
	
	Public Function Add(Optional ByRef sKey As String = "") As ClientHandled
		'Public Function Add(File As Variant, Client As Variant, EMail As Variant, FileType As Variant, FileStatus As Variant, Optional sKey As String) As ClientHandled
		'create a new object
		Dim objNewMember As ClientHandled
		objNewMember = New ClientHandled
		
		If Len(sKey) = 0 Then
			mCol.Add(objNewMember)
		Else
			mCol.Add(objNewMember, sKey)
			objNewMember.Index = CObj(sKey)
		End If
		
		'return the object created
		Add = objNewMember
		'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		objNewMember = Nothing
		
		
	End Function
	
	
	Public Sub Remove(ByRef vntIndexKey As Object)
		'used when removing an element from the collection
		'vntIndexKey contains either the Index or Key, which is why
		'it is declared as a Variant
		'Syntax: x.Remove(xyz)
		
		
		mCol.Remove(vntIndexKey)
	End Sub
	
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		'creates the collection when this class is created
		mCol = New Collection
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'destroys collection when this class is terminated
		'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mCol = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class
