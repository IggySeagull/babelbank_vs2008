Option Explicit On
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

'Things to do when implementing this report in BB
'Search for 'LRS(, and replace the text with the LRS
'Search for vbBabel.MatchType. and replace the number stated with the correct Enum

Public Class rp_002_Payment

    Private oBabelFiles As BabelFiles
    ' Declare all objects to be used in actual report:
    Dim oBabelFile As BabelFile
    Dim oBatch As Batch
    Dim oPayment As Payment
    Dim oInvoice As Invoice

    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim sClientName As String

    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim bShowFilename As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean = False


    Private Sub rp_002_Payment_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Fields.Add("BreakField")

        If Not bReportFromDatabase Then
            Fields.Add("Grouping")
            Fields.Add("Company_ID")
            Fields.Add("Babelfile_ID")
            Fields.Add("Batch_ID")
            Fields.Add("Payment_ID")
            Fields.Add("Invoice_ID")
            Fields.Add("Filename")
            Fields.Add("StatementAmount")
            Fields.Add("Client")
            Fields.Add("DATE_Production")
            Fields.Add("BatchRef")
            Fields.Add("I_Account")
            Fields.Add("MON_TransferredAmount")
            Fields.Add("MON_InvoiceCurrency")
            Fields.Add("REF_Bank1")
            Fields.Add("E_Account")
            Fields.Add("E_Name")
            Fields.Add("DATE_Value")
            Fields.Add("DATE_Payment")
            Fields.Add("Paycode")
        End If

    End Sub

    Private Sub rp_002_Payment_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_002_Payment_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart
        Dim pLocation As System.Drawing.PointF
        Dim pSize As System.Drawing.SizeF
        Dim pFontFilename As New System.Drawing.Font("Arial", 14)

        bShowBatchfooterTotals = True

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False
                bShowFilename = False

            Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = True

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

        End Select

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtDATE_Value.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtDATE_Value.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40040)   'Payments "Betalinger"  '
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        'Set the caption of the labels

        'grBatchHeader
        Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        If iBreakLevel = 1 Then
            Me.lblDate_Production.Text = LRS(40044)  'bokf�ringssdato  
        Else
            Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        End If

        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40138) '"Own account:" 'LRS(40020) '"Mottakerkonto:" 

        'grBatchFooter
        Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" '

        ''Me.lblFileSum = LRS(40028)

        sOldI_Account = ""
        sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 

        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If
        If Not bShowFilename Then
            Me.txtFilename.Visible = False
        End If

        If sSpecial = "VismaBusiness" Then
            'Special adjustments for Visma

            Me.lblDate_Production.Text = LRS(40044)

            Me.grBatchHeader.Height = 0.927

            Me.lblFilename.Font = pFontFilename
            Me.txtFilename.Font = pFontFilename

            pSize.Width = Me.shapeBatchHeader.Width
            pSize.Height = 0.75
            Me.shapeBatchHeader.Size = pSize
            pLocation.X = 0
            pLocation.Y = 0.1
            Me.lblFilename.Location = pLocation
            pSize.Width = Me.lblFilename.Width
            pSize.Height = 0.23
            Me.lblFilename.Size = pSize
            pLocation.X = 1.35
            pLocation.Y = 0.1
            Me.txtFilename.Visible = True
            Me.txtFilename.Location = pLocation
            pSize.Width = 4.925
            pSize.Height = 0.23
            Me.txtFilename.Size = pSize
            pLocation.X = 0
            pLocation.Y = 0.41
            Me.lblDate_Production.Location = pLocation
            pLocation.X = 1.375
            pLocation.Y = 0.41
            Me.txtDATE_Production.Location = pLocation
            pLocation.X = 3.375
            pLocation.Y = 0.41
            Me.lblClient.Location = pLocation
            pLocation.X = 4.375
            pLocation.Y = 0.41
            Me.txtClientName.Location = pLocation
            pLocation.X = 3.375
            pLocation.Y = 0.57
            Me.lblI_Account.Location = pLocation
            pLocation.X = 4.375
            pLocation.Y = 0.57
            Me.txtI_Account.Location = pLocation

            pSize.Width = 1.3
            pSize.Height = Me.txtE_Account.Height
            Me.txtE_Name.Size = pSize

            pLocation.X = 1.37
            pLocation.Y = 0
            Me.txtE_Account.Location = pLocation
            pSize.Width = 1
            pSize.Height = Me.txtE_Account.Height
            Me.txtE_Account.Size = pSize

            pLocation.X = 2.5
            pLocation.Y = 0
            Me.txtREF_Bank1.Location = pLocation
            Me.txtREF_Bank1.CanGrow = True
            pSize.Width = 1.05
            pSize.Height = Me.txtREF_Bank1.Height
            Me.txtREF_Bank1.Size = pSize

            pLocation.X = 3.55
            pLocation.Y = 0
            Me.txtDATE_Value.Location = pLocation

            Me.txtREF_Own.Visible = True
            pLocation.X = 4.17
            pLocation.Y = 0
            Me.txtREF_Own.Location = pLocation
            pSize.Width = 1.3
            pSize.Height = Me.txtREF_Own.Height
            Me.txtREF_Own.Size = pSize
            Me.txtREF_Own.CanGrow = True

            pLocation.X = 5.5
            pLocation.Y = 0
            Me.txtMON_P_InvoiceCurrency.Location = pLocation

        Else
            Me.txtREF_Own.Visible = False
        End If

        If sSpecial = "DNBNORFINANS_CAMT054D" Then
            Me.txtREF_Own.Visible = True
            pLocation.X = Me.txtREF_Own.Location.X + 0.9
            pLocation.Y = Me.txtREF_Own.Location.Y
            Me.txtREF_Own.Location = pLocation
            pLocation.X = Me.txtREF_Bank1.Location.X + 0.7
            pLocation.Y = Me.txtREF_Bank1.Location.Y
            Me.txtREF_Bank1.Location = pLocation
        End If
    End Sub
    Private Sub rp_002_Payment_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        ' Counters for items in the different Babel collections
        Static iBabelFiles As Long
        Static iLastUsedBabelFilesItem As Integer
        Static iBatches As Long
        Static iPayments As Long
        Static iInvoices As Long
        'Static iInvoices As Integer
        'Static iFreeTexts As Integer
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String
        Static xBabelFiles As Long
        Static bOriginalFreetext As Boolean

        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String
        Dim sDATE_Value As String
        Dim sDate_Payment As String

        Dim bContinue As Boolean
        Dim bUseValueDate As Boolean
        Dim nInvoiceTotals As Double
        Dim sREF_Own As String
        Dim sTmp As String = ""

        Dim bProfileInUse As Boolean

        If Not bReportFromDatabase Then
            eArgs.EOF = False
            iPayments = iPayments + 1
            If iBabelFiles = 0 Then
                iBabelFiles = 1
                iBatches = 1
                iPayments = 1
                xCreditAccount = "xxxx"
                xDate = "x"
                '    Files = 0
            End If

            ' Spin through collections to find next suitbable record before any other
            ' proecessing is done
            ' Position to correct items in collections
            Do Until iBabelFiles > oBabelFiles.Count
                ' Try to set as few times as possible
                If oBabelFile Is Nothing Then
                    oBabelFile = oBabelFiles(iBabelFiles)
                    iLastUsedBabelFilesItem = 1 'oBabelFile.Index
                Else
                    If iLastUsedBabelFilesItem <> iBabelFiles Then 'oBabelFile.Index Then
                        ' Added 19.05.2004
                        xCreditAccount = "xxxx"
                        xDate = "x"
                        oBabelFile = oBabelFiles(iBabelFiles)
                        iLastUsedBabelFilesItem = iBabelFiles  'oBabelFile.Index
                    End If
                End If

                '05.11.2009 - Kjell. Added the use of bUseValueDate in this module
                bUseValueDate = False
                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.Telepay2 And oBabelFile.StatusCode = "02" Then
                    bUseValueDate = True
                End If

                Do Until iBatches > oBabelFile.Batches.Count
                    'If oBatch Is Nothing Then
                    '    Set oBatch = oBabelFile.Batches(iBatches)
                    'ElseIf iBatches <> oBatch.Index Or iBatches = 1 Then
                    oBatch = oBabelFile.Batches(iBatches)
                    'End If

                    ' Find the next payment in this batch which is original
                    Do Until iPayments > oBatch.Payments.Count
                        oPayment = oBatch.Payments.Item(iPayments)


                        If bReportOnSelectedItems Then 'For Visma solution this is set in the propertysetting of Special
                            iInvoices = 1
                            If oPayment.ToSpecialReport Then

                                ' Postiton to a, final invoice
                                ' Find the next invoice in this payment which has .ToSpecialReport = True
                                nInvoiceTotals = 0
                                Do Until iInvoices > oPayment.Invoices.Count
                                    oInvoice = oPayment.Invoices.Item(iInvoices)

                                    If oInvoice.ToSpecialReport Then
                                        If oInvoice.MON_TransferredAmount <> 0 Then
                                            nInvoiceTotals = nInvoiceTotals + oInvoice.MON_TransferredAmount / 100
                                        Else
                                            nInvoiceTotals = nInvoiceTotals + oInvoice.MON_InvoiceAmount / 100
                                        End If
                                        'Exit Do
                                    Else
                                        nInvoiceTotals = nInvoiceTotals
                                    End If
                                    iInvoices = iInvoices + 1
                                Loop 'iInvoices

                                If nInvoiceTotals = 0 Then
                                    ' all invoices for this payment are excluded from report
                                    iPayments = iPayments + 1
                                Else
                                    bContinue = True
                                    Exit Do
                                End If
                            Else
                                iPayments = iPayments + 1
                            End If

                        Else

                            'iPayments = iPayments + 1
                            bContinue = True
                            Exit Do

                        End If

                    Loop 'iPayments
                    If bContinue Then
                        Exit Do
                    End If
                    iBatches = iBatches + 1
                    iPayments = 1
                Loop ' iBatches
                If bContinue Then
                    Exit Do
                End If
                iBabelFiles = iBabelFiles + 1
                iBatches = 1
            Loop ' iBabelFiles

            If iBabelFiles > oBabelFiles.Count Then
                eArgs.EOF = True
                iBabelFiles = 0 ' to reset statics next time !

                Exit Sub
            End If

            bProfileInUse = oBabelFile.VB_ProfileInUse
            If oBabelFile.VB_ProfileInUse Then
                Me.Fields("Grouping").Value = Trim(Str(oPayment.VB_Profile.Company_ID)) & "-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                Me.Fields("Company_ID").Value = oPayment.VB_Profile.Company_ID
            Else
                Me.Fields("Grouping").Value = "1-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                Me.Fields("Company_ID").Value = "1"
            End If
            Me.Fields("Babelfile_ID").Value = oBabelFile.Index
            Me.Fields("Batch_ID").Value = oBatch.Index
            Me.Fields("Payment_ID").Value = oPayment.Index
            If Not oInvoice Is Nothing Then '27.07.2016 - Added this IF
                Me.Fields("Invoice_ID").Value = oInvoice.Index
            End If
            If Me.txtFilename.Width < 4 Then
                Me.Fields("Filename").Value = Right(oBabelFile.FilenameIn, 20)
            Else
                'Visma
                If oBabelFile.FilenameIn.Length > 36 Then
                    Fields("Filename").Value = "..." & Right(oBabelFile.FilenameIn, 36)
                Else
                    Fields("Filename").Value = oBabelFile.FilenameIn
                End If
            End If
            Me.Fields("StatementAmount").Value = oBatch.MON_InvoiceAmount
            Me.Fields("Client").Value = oPayment.VB_Client

            ' 10.02.2016 Changed to Date_Payment for Break on Account+Date
            If iBreakLevel = 1 Then
                Me.Fields("DATE_Production").Value = oPayment.DATE_Payment
            Else
                Me.Fields("DATE_Production").Value = oBabelFile.DATE_Production
            End If

            Me.Fields("BatchRef").Value = oBatch.REF_Bank
            Me.Fields("I_Account").Value = oPayment.I_Account
            'Me.Fields("Paycode").Value = oPayment.PayCode
            ' 04.08.2016
            ' Litt dum test her, men MON_TransferredAmount er ofte 0. Kan jo v�re 0-bel�ps transer, men da blir det riktig nok � bruke MON_InvoiceAmount
            ' M� derfor bruke MON_InvoiceAmount.
            If oPayment.MON_TransferredAmount <> 0 Then
                Me.Fields("MON_TransferredAmount").Value = oPayment.MON_TransferredAmount / 100 '10.02.2016
                Me.Fields("MON_InvoiceCurrency").Value = oPayment.MON_TransferCurrency
            Else
                Me.Fields("MON_TransferredAmount").Value = oPayment.MON_InvoiceAmount / 100 '10.02.2016
                Me.Fields("MON_InvoiceCurrency").Value = oPayment.MON_InvoiceCurrency
            End If

            Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
            Me.Fields("E_Account").Value = oPayment.E_Account
            Me.Fields("E_Name").Value = oPayment.E_Name
            Me.Fields("DATE_Value").Value = oPayment.DATE_Value
            Me.Fields("DATE_Payment").Value = oPayment.DATE_Payment

            Me.Fields("Paycode").Value = oPayment.PayCode
            sREF_Own = oPayment.REF_Own
            sClientInfo = oPayment.I_Client
            ' 17.09.2019 Changed to "DNBNORFINANS_CAMT054D" from "DNBNORFINANS"
            If sSpecial = "DNBNORFINANS_CAMT054D" Then
                ' 17.09.2019 changed from ref_own to xDelim(oPayment.REF_EndToEnd, "-", 1)
                Me.txtREF_Own.Value = xDelim(oPayment.REF_EndToEnd, "-", 1)
            End If

        End If

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    ' 03.02.2016 changed to DATE_Payment
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                    Me.Fields("BreakField").Value = Me.Fields("BabelFile_ID").Value.ToString

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            If sSpecial = "VismaBusiness" Then
                Me.txtREF_Own.Value = sREF_Own
            End If

            'Store the keys for usage in the subreport(s)
            iCompany_ID = Me.Fields("Company_ID").Value
            iBabelfile_ID = Me.Fields("Babelfile_ID").Value
            iBatch_ID = Me.Fields("Batch_ID").Value
            iPayment_ID = Me.Fields("Payment_ID").Value
            'START WORKING HERE
            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If bProfileInUse Then '15.07.2016 - Added this test and the use of bProfileInUse
                If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                    sClientInfo = FindClientName(sI_Account)
                    sOldI_Account = sI_Account
                End If
            Else
                'sClientInfo is set when retrieving data from the collections (if set at all)
            End If

            If Me.Fields("Paycode").Value = "602" Then
                Me.txtE_Name.Value = "GIROMAIL"
            Else
                Me.txtE_Name.Value = Me.Fields("E_Name").Value
            End If

            Me.txtClientName.Value = sClientInfo

            Me.txtRunDate.Visible = False   ' 21.11.2016 - added extra date field, used for Gulfmark
            ' 10.02.2016 Changed to Date_Payment for Break on Account+Date
            If iBreakLevel = 1 And sSpecial <> "VismaBusiness" Then '15.07.2016 Added Special
                sDate_Payment = Me.Fields("DATE_Payment").Value
                If sSpecial = "GULFMARK" Then
                    ' 21.11.2016 - Gulfmark will always have date as DD/MM/YYYY
                    ' make date DD/MM/YYYY
                    Me.txtDATE_Production.Value = Right(sDate_Payment, 2) & "/" & Mid(sDate_Payment, 5, 2) & "/" & Left(sDate_Payment, 4)
                    ' also header date;
                    rptInfoPageHeaderDate.Visible = False
                    ' must use a specific headerfield
                    sTmp = DateToString(Date.Today)
                    Me.txtRunDate.Visible = True
                    Me.txtRunDate.Text = Right(sTmp, 2) & "/" & Mid(sTmp, 5, 2) & "/" & Left(sTmp, 4)
                Else
                    Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDate_Payment, 4)), CInt(Mid$(sDate_Payment, 5, 2)), CInt(Right$(sDate_Payment, 2)))
                End If
            Else
                sDATE_Production = Me.Fields("DATE_Production").Value
                Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))
            End If
            sDATE_Value = "19900101"
            If Me.Fields("DATE_Value").Value > "19900101" Then
                ' use valuedate if specified
                sDATE_Value = Me.Fields("DATE_Value").Value
            ElseIf Me.Fields("DATE_Payment").Value > "19900101" Then
                ' Use bookdate
                sDATE_Value = Me.Fields("DATE_Payment").Value
            End If
            If sSpecial = "GULFMARK" Then
                ' 21.11.2016 - Gulfmark will always have date as DD/MM/YYYY
                ' make date DD/MM/YYYY
                Me.txtDATE_Value.Value = Right(sDATE_Value, 2) & "/" & Mid(sDATE_Value, 5, 2) & "/" & Left(sDATE_Value, 4)
            Else
                Me.txtDATE_Value.Value = DateSerial(CInt(Left$(sDATE_Value, 4)), CInt(Mid$(sDATE_Value, 5, 2)), CInt(Right$(sDATE_Value, 2)))
            End If
        End If

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
            If sSpecial = "VismaBusiness" Then
                bReportOnSelectedItems = True
            End If
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub grBatchHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grBatchHeader.Format
        Dim pLocation As System.Drawing.PointF
        Dim pSize As System.Drawing.SizeF

        'If Me.txtFilename.Visible Then
        'pLocation.X = 1.5
        'pLocation.Y = 0.3
        'Me.txtFilename.Location = pLocation
        'pSize.Height = 0.19
        'pSize.Width = 4.5
        'Me.txtFilename.Size = pSize
        'pLocation.X = 0.125
        'pLocation.Y = 0.3
        'Me.lblI_Account.Location = pLocation
        'Me.lblI_Account.Value = LRS(20007)
        'Me.txtI_Account.Visible = False
        'End If

    End Sub
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
