Option Strict Off
Option Explicit On
Module WriteHandelsbanken_DK
	
    Private Enum HBGOInstructionCode
        No_Code = 0
        URGP = 1
        INTC = 2
        CORT = 3
        OTHR = 4
        EKON = 5
        CHQB = 6
    End Enum
	
	
	'Dim nNoOfTransactions As Double
    Function WriteHandelsbanken_DK_Domestic(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        '
        ' *** NB ***
        ' 09.03.05: Added International payment as well
        '
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim nAmount As Double
        Dim sToAccount, sFromAccount, sShortNotification As String
        Dim i As Short
        Dim sAmount As String
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim sERH As String

        Try

            ' create an outputfile
            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If oPayment.PayType <> "I" Then
                                        ' Domestic payments
                                        '-------- fetch content of each payment ---------
                                        sFromAccount = Trim(oPayment.I_Account)
                                        sToAccount = Trim(oPayment.E_Account)
                                        sName = Trim(Left(oPayment.E_Name, 35))
                                        sOwnRef = Trim(Left(oPayment.REF_Own, 20))

                                        ' New 14.03.05
                                        ' If FIK, may have more than  one invoice with FIK
                                        For iInvoiceCounter = 1 To oPayment.Invoices.Count

                                            If oPayment.PayCode = "301" Then 'FIK
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                sShortNotification = Trim(oPayment.Invoices(iInvoiceCounter).Unique_Id)
                                                ' New 23.03.05
                                                ' If FI71 then only pick "71" + last 15 digits
                                                If Left(sShortNotification, 2) = "71" Then
                                                    sShortNotification = Left(sShortNotification, 17)
                                                End If
                                                '  01           Empty           6 or 7 pos
                                                '  04           16 pos          6 or 7 pos
                                                '  15           16 pos
                                                '  44
                                                '  48
                                                '  71           15 pos          8 pos
                                                '  73           Empty           8 pos
                                                '  75           16 pos          8 pos

                                                Select Case Left(sShortNotification, 2)
                                                    Case "01"
                                                        'Girokort 01
                                                        sERH = "ERH354"
                                                        sShortNotification = "01"
                                                    Case "04"
                                                        'Girokort 04
                                                        sERH = "ERH352"
                                                        sShortNotification = Left(sShortNotification, 18)
                                                    Case "15"
                                                        'Girokort 15
                                                        sERH = "ERH352"
                                                        sShortNotification = Left(sShortNotification, 18)
                                                    Case "41"
                                                        'Girokort 41
                                                        sERH = "ERH354"
                                                        sShortNotification = "41"
                                                    Case "71"
                                                        ' FI 71
                                                        sERH = "ERH351"
                                                        ' If FI71 then only pick "71" + next 15 digits
                                                        sShortNotification = Left(sShortNotification, 17)

                                                    Case "73"
                                                        ' FI 73
                                                        sERH = "ERH357"
                                                        sShortNotification = "73"

                                                    Case "75"
                                                        ' FI 75
                                                        ' If FI75 then only pick "75" + next 16 digits
                                                        sShortNotification = Left(sShortNotification, 18)
                                                        sERH = "ERH358"

                                                End Select


                                            Else
                                                sERH = "ERH356"
                                                If oPayment.Invoices.Count = 1 Then
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices(1).Freetexts. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    If oPayment.Invoices(1).Freetexts.Count > 0 Then
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Freetexts. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        sShortNotification = Trim(Left(oPayment.Invoices(iInvoiceCounter).Freetexts(1).Text, 20))
                                                    Else
                                                        sShortNotification = ""
                                                    End If
                                                ElseIf oPayment.Invoices.Count > 1 Then
                                                    sShortNotification = Str(CDbl(Trim(CStr(oPayment.Invoices.Count)))) & " fakturabetalinger"
                                                Else
                                                    sShortNotification = Trim(oPayment.I_Account)
                                                End If
                                            End If

                                            If oPayment.PayCode = "301" Then 'FIK
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().MON_InvoiceAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                nAmount = oPayment.Invoices(iInvoiceCounter).MON_InvoiceAmount
                                            Else
                                                nAmount = 0
                                                For Each oInvoice In oPayment.Invoices
                                                    nAmount = nAmount + Val(CStr(oInvoice.MON_InvoiceAmount))
                                                Next oInvoice
                                            End If
                                            sAmount = VB6.Format(nAmount / 100, "###0.00")


                                            sDate = Right(oPayment.DATE_Payment, 2) & Mid(oPayment.DATE_Payment, 5, 2) & Mid(oPayment.DATE_Payment, 3, 2) 'skal v�re DDMMYY
                                            'If oPayment.PayCode = "301" Then   'FIK
                                            '    sLine = Chr(34) & "ERH351" & Chr(34) & "," 'Betalingstype
                                            'Else
                                            '    sLine = Chr(34) & "ERH356" & Chr(34) & "," 'Betalingstype
                                            'End If
                                            sLine = Chr(34) & sERH & Chr(34) & "," 'Betalingstype
                                            sLine = sLine & Chr(34) & sFromAccount & Chr(34) & "," '1 Fra konto
                                            sLine = sLine & Chr(34) & sOwnRef & Chr(34) & "," '3 Egenreferance
                                            If oPayment.PayCode = "301" Then 'FIK or GIROcard
                                                If Left(sShortNotification, 1) = "7" Then
                                                    ' FI71, 73 or 75
                                                    sLine = sLine & Chr(34) & "FI  " & sToAccount & Chr(34) & "," '4 Til konto
                                                Else
                                                    ' Girocard 01, 04, 15, 41
                                                    sLine = sLine & Chr(34) & "GIRO  " & sToAccount & Chr(34) & "," '4 Til konto
                                                End If
                                            Else
                                                sLine = sLine & Chr(34) & sToAccount & Chr(34) & "," '4 Til konto
                                            End If
                                            sLine = sLine & Chr(34) & sName & Chr(34) & "," '5 Navn
                                            sLine = sLine & Chr(34) & Chr(34) & "," '6 Frekvens (not in use)
                                            sLine = sLine & Chr(34) & Chr(34) & "," '7 Antall gange (not in use)
                                            sLine = sLine & Chr(34) & Chr(34) & "," '8 Slutdato (not in use)
                                            sLine = sLine & Chr(34) & sAmount & Chr(34) & "," '9 Bel�p
                                            sLine = sLine & Chr(34) & sDate & Chr(34) & "," '10 Betalingsdato YYMMDD
                                            sLine = sLine & Chr(34) & sShortNotification & Chr(34) & "," '11 Kort reference
                                            sLine = sLine & Chr(34) & Chr(34) & "," '12
                                            sLine = sLine & Chr(34) & Chr(34) & "," '13
                                            sLine = sLine & Chr(34) & Chr(34) & "," '14
                                            sLine = sLine & Chr(34) & Chr(34) & "," '15
                                            sLine = sLine & Chr(34) & Chr(34) & "," '16
                                            sLine = sLine & Chr(34) & Chr(34) & "," '17
                                            sLine = sLine & Chr(34) & Chr(34) & "," '18
                                            sLine = sLine & Chr(34) & Chr(34) & "," '19
                                            sLine = sLine & Chr(34) & Chr(34) & "," '20
                                            sLine = sLine & Chr(34) & Chr(34) & "," '21
                                            sLine = sLine & Chr(34) & Chr(34) & "," '22
                                            sLine = sLine & Chr(34) & Chr(34) & "," '23
                                            sLine = sLine & Chr(34) & Chr(34) & "," '24
                                            sLine = sLine & Chr(34) & Chr(34) & "," '25
                                            sLine = sLine & Chr(34) & Chr(34) & "," '26
                                            sLine = sLine & Chr(34) & Chr(34) & "," '27
                                            sLine = sLine & Chr(34) & Chr(34) & "," '28
                                            sLine = sLine & Chr(34) & Chr(34) & "," '29
                                            sLine = sLine & Chr(34) & Chr(34) & "," '30
                                            sLine = sLine & Chr(34) & Chr(34) & "," '31
                                            sLine = sLine & Chr(34) & Chr(34) & "," '32
                                            i = 33
                                            For Each oInvoice In oPayment.Invoices
                                                For Each oFreeText In oInvoice.Freetexts
                                                    i = i + 1
                                                    If i > 73 Then
                                                        Exit For
                                                    End If
                                                    sLine = sLine & Chr(34) & Trim(Left(oFreeText.Text, 35)) & Chr(34) & ","
                                                Next oFreeText
                                                If i > 73 Then
                                                    Exit For
                                                End If
                                            Next oInvoice
                                            ' remove last comma
                                            If Right(sLine, 1) = "," Then
                                                sLine = Left(sLine, Len(sLine) - 1)
                                            End If

                                            If Len(sLine) > 0 Then
                                                oFile.WriteLine((sLine))
                                            End If
                                            oPayment.Exported = True

                                            ' New 14.03.05 due to more than one FIK-invoice
                                            If oPayment.PayCode <> "301" Then
                                                ' if not FIK, finished
                                                Exit For
                                            Else
                                                ' If FIK, may have more invoices
                                            End If
                                        Next iInvoiceCounter
                                    Else
                                        ' International payments
                                        '-------- fetch content of each payment ---------
                                        sFromAccount = Trim(oPayment.I_Account)
                                        sToAccount = Trim(oPayment.E_Account)
                                        sName = Trim(Left(oPayment.E_Name, 35))
                                        sOwnRef = Trim(Left(oPayment.REF_Own, 20))

                                        nAmount = 0
                                        For Each oInvoice In oPayment.Invoices
                                            nAmount = nAmount + Val(CStr(oInvoice.MON_InvoiceAmount))
                                        Next oInvoice
                                        sAmount = VB6.Format(nAmount / 100, "###0.00")

                                        'sDate = Mid$(oPayment.DATE_Payment, 3) 'YYMMDD
                                        sDate = Right(oPayment.DATE_Payment, 2) & Mid(oPayment.DATE_Payment, 5, 2) & Mid(oPayment.DATE_Payment, 3, 2) 'skal v�re DDMMYY

                                        sLine = Chr(34) & "ERH400" & Chr(34) & "," 'Betalingstype
                                        sLine = sLine & Chr(34) & sFromAccount & Chr(34) & "," '1 Fra konto
                                        sLine = sLine & Chr(34) & sOwnRef & Chr(34) & "," '3 Egenreferance
                                        sLine = sLine & Chr(34) & sToAccount & Chr(34) & "," '4 Til konto
                                        sLine = sLine & Chr(34) & sName & Chr(34) & "," '5 Navn
                                        sLine = sLine & Chr(34) & Trim(oPayment.E_Adr1) & Chr(34) & "," '6 Adr1
                                        sLine = sLine & Chr(34) & Trim(oPayment.E_Adr2) & Chr(34) & "," '7 Adr2
                                        sLine = sLine & Chr(34) & Trim(oPayment.E_Adr3) & Chr(34) & "," '8 Adr3

                                        sLine = sLine & Chr(34) & Chr(34) & "," '9 Bel�p i DKK, ikke angitt
                                        sLine = sLine & Chr(34) & sDate & Chr(34) & "," '10 Betalingsdato YYMMDD
                                        sLine = sLine & Chr(34) & Trim(oPayment.BANK_SWIFTCode) & Chr(34) & "," '11 Swift
                                        sLine = sLine & Chr(34) & Chr(34) & "," '12 Reserved
                                        sLine = sLine & Chr(34) & oPayment.MON_InvoiceCurrency & Chr(34) & "," '13 Currency
                                        sLine = sLine & Chr(34) & sAmount & Chr(34) & "," '14 Udenlandsk bel�b
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices(1).STATEBANK_Code. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & Chr(34) & oPayment.Invoices(1).STATEBANK_Code & Chr(34) & "," '15 Nationalbank kode
                                        ' 17 Bankcode og bankname
                                        If Not EmptyString((oPayment.BANK_BranchNo)) Then
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_BranchNo) & Chr(34) & "," '16 Bank clearingcode
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Adr1) & Chr(34) & "," '17 Adr1
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Adr2) & Chr(34) & "," '18 Adr2
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Adr3) & Chr(34) & "," '19 Adr3
                                        Else
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Name) & Chr(34) & "," '16 Name
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Adr1) & Chr(34) & "," '17 Adr1
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Adr2) & Chr(34) & "," '18 Adr2
                                            sLine = sLine & Chr(34) & Trim(oPayment.BANK_Adr3) & Chr(34) & "," '19 Adr3
                                        End If
                                        If oPayment.Priority Then
                                            sLine = sLine & Chr(34) & "01" & Chr(34) & "," '20 Overf�rselstype
                                        Else
                                            sLine = sLine & Chr(34) & "02" & Chr(34) & "," '20 Overf�rselstype
                                        End If
                                        sLine = sLine & Chr(34) & Chr(34) & "," '21 Not in use
                                        If oPayment.MON_ChargeMeDomestic And oPayment.MON_ChargeMeAbroad Then
                                            ' Payer pays all charges
                                            sLine = sLine & Chr(34) & "O" & Chr(34) & "," '22 Omkostningskode
                                        ElseIf oPayment.MON_ChargeMeDomestic And (Not oPayment.MON_ChargeMeAbroad) Then
                                            ' Payer pays charges only in DK
                                            sLine = sLine & Chr(34) & "S" & Chr(34) & "," '22 Omkostningskode
                                        Else
                                            ' Payer pays no charges
                                            sLine = sLine & Chr(34) & "B" & Chr(34) & "," '22 Omkostningskode
                                        End If
                                        If oPayment.NOTI_NotificationParty = (1 Or 3) Then
                                            'Receiver shall be notified by payors bank
                                            sLine = sLine & Chr(34) & Chr(34) & "," '23 Advisering modtager
                                            If CBool(oPayment.NOTI_NotificationType) = ("TELEX" Or "FAX") Then
                                                sLine = sLine & Chr(34) & Trim(oPayment.NOTI_NotificationIdent) & Chr(34) & "," '24 Telexnr
                                            Else
                                                sLine = sLine & Chr(34) & Chr(34) & "," '24 Telexnr
                                            End If
                                        Else
                                            sLine = sLine & Chr(34) & Chr(34) & "," '23 Advisering modtager
                                            sLine = sLine & Chr(34) & Chr(34) & "," '24 Telexnr
                                        End If

                                        sLine = sLine & Chr(34) & Chr(34) & "," '25 Tilbakesvar
                                        sLine = sLine & Chr(34) & Left(oPayment.NOTI_NotificationMessageToBank, 35) & Chr(34) & "," '26 Bem�rkning 1
                                        sLine = sLine & Chr(34) & Mid(oPayment.NOTI_NotificationMessageToBank, 36, 35) & Chr(34) & "," '27 Bem�rkning 2

                                        sLine = sLine & Chr(34) & Chr(34) & "," '28 Reservert
                                        sLine = sLine & Chr(34) & Chr(34) & "," '29 Reservert
                                        sLine = sLine & Chr(34) & Chr(34) & "," '30 Reservert
                                        sLine = sLine & Chr(34) & Chr(34) & "," '31 Reservert
                                        sLine = sLine & Chr(34) & Chr(34) & "," '32 Reservert

                                        sFreetext = ""
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                sFreetext = sFreetext & " " & Trim(oFreeText.Text)
                                            Next oFreeText
                                        Next oInvoice
                                        ' remove first space
                                        sFreetext = Trim(sFreetext)
                                        sLine = sLine & Chr(34) & Left(sFreetext, 35) & Chr(34) & "," '33 tekst1
                                        sLine = sLine & Chr(34) & Mid(sFreetext, 36, 35) & Chr(34) & "," '34 tekst2
                                        sLine = sLine & Chr(34) & Mid(sFreetext, 71, 35) & Chr(34) & "," '35 tekst3
                                        sLine = sLine & Chr(34) & Mid(sFreetext, 105, 35) & Chr(34) & "," '36 tekst4

                                        sLine = sLine & Chr(34) & Chr(34) & "," '37 Importtidspunkt (kjenner vi ikke)
                                        sLine = sLine & Chr(34) & oPayment.E_CountryCode & Chr(34) '38 Modtagers landkode


                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine((sLine))
                                        End If
                                        oPayment.Exported = True
                                    End If ' <> "I"
                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            '26.06.2019 - F�r overgang til ny feilh�ndtering ble ikke ny feilmelding kastet
            Throw New vbBabel.Payment.PaymentException("Function: WriteHandelsbanken_DK_Domestic" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteHandelsbanken_DK_Domestic = True

    End Function

    Function WriteHBGlobalOnline_MT101File(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef sBranch As String, ByRef sVersion As String, ByRef nSequenceNumberStart As Double, ByRef nSequenceNumberEnd As Double) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim i As Short
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String
        Dim bSequenceAWritten As Boolean
        Dim eCountryCode As vbBabel.BabelFiles.CountryName
        Dim eInstructionCode As HBGOInstructionCode
        Dim bDebitAccountInHandelsbanken As Boolean
        Dim sLastUsedExecutionDate As String
        Dim sToday As String
        Dim lCounter, lCounter2 As Integer
        Dim sAmount As String
        Dim sInfoToWriteInTag36, sTemp As String
        Dim sErrFixedText As String
        Dim sKortartKode As String
        Dim sBetalerIdent As String
        Dim sKreditorNo As String
        Dim sErrorString As String
        Dim bMessageIsMandatory As Boolean
        Dim sFile_ID As String
        Dim bThisIsAPostBankAccount As Boolean
        Dim bNormalPayment As Boolean
        Dim iCharactersPerLine As Short
        Dim sClearingCode As String
        Dim sOldPayType As String
        Dim bx As Boolean
        'Dim sErrorString As String

        ' create an outputfile
        Try

            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bSequenceAWritten = False
            sToday = VB6.Format(Now, "YYYYMMDD")
            lCounter = 0
            bFoundClient = False
            sOldPayType = ""

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If oPayment.I_Account <> sOldAccount Then
                                        bFoundClient = False

                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Or Trim(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                    sSwift = Trim(oaccount.SWIFTAddress)
                                                    sOldAccount = oPayment.I_Account
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundClient Then
                                                'Get the companyno from a property passed from BabelExport
                                                If EmptyString(sCompanyNo) Then
                                                    Err.Raise(1, "WriteHBGlobalOnline_MT101File", "No Customer identity in Handelsbanken stated")
                                                End If
                                                Exit For
                                            End If


                                        Next oClient
                                        bSequenceAWritten = False
                                    End If

                                    If Not bFoundClient Then
                                        Err.Raise(1, "WriteHBGlobalOnline_MT101File", "The debitaccount " & oPayment.I_Account & " is unknown to BabelBank. You must enter information about the account in the Client setup.")
                                    End If

                                    If oPayment.DATE_Payment = "19900101" Then
                                        oPayment.DATE_Payment = sToday
                                    End If

                                    If oPayment.DATE_Payment < sToday Then
                                        If sToday = sLastUsedExecutionDate Then
                                            'OK, no new sequence A
                                        Else
                                            'Change in date, write a new sequence A
                                            sLastUsedExecutionDate = sToday
                                            bSequenceAWritten = False
                                        End If
                                    ElseIf oPayment.DATE_Payment = sLastUsedExecutionDate Then
                                        'OK, no new sequence A
                                    Else
                                        'Change in date, write a new sequence A
                                        sLastUsedExecutionDate = oPayment.DATE_Payment
                                        bSequenceAWritten = False
                                    End If

                                    oPayment.BANK_I_SWIFTCode = sSwift
                                    'Create en fixed error text with info from the payment
                                    sErrFixedText = ""
                                    sErrFixedText = "Beneficiary's name: " & oPayment.E_Name & vbCrLf
                                    sErrFixedText = sErrFixedText & "Beneficiary's account: " & oPayment.E_Account & vbCrLf
                                    sErrFixedText = sErrFixedText & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf
                                    sErrFixedText = sErrFixedText & "Payer's account: " & oPayment.I_Account

                                    bDebitAccountInHandelsbanken = True

                                    If Left(sSwift, 4) <> "HAND" Then
                                        'Err.Raise 1, "WriteHBGlobalOnline_MT101File", "BabelBank is not yet implemented for using accounts in other banks. Contact Your dealer."
                                        'bAccountInOtherBankExists = True
                                        bDebitAccountInHandelsbanken = False
                                    End If

                                    If Not IsPaymentDomestic(oPayment, sSwift) Then
                                        oPayment.PayType = "I"
                                    Else
                                        '10.03.2010 - New code to take care of salarypayments for Ulefos
                                        If oPayment.PayType <> "S" Then
                                            oPayment.PayType = "D"
                                        End If
                                    End If

                                    If Len(Trim(sSwift)) = 8 Or Len(Trim(sSwift)) = 11 Then
                                        Select Case UCase(Mid(sSwift, 5, 2))
                                            Case "DK"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Denmark
                                            Case "FI"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Finland
                                            Case "FR"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.France
                                            Case "DE"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Germany
                                            Case "GB"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Great_Britain
                                            Case "HK"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Hong_Kong
                                            Case "LU"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Luxemburg
                                            Case "NL"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Netherlands
                                            Case "NO"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Norway
                                            Case "PL"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Poland
                                            Case "SG"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Singapore
                                            Case "SE"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.Sweden
                                            Case "US"
                                                eCountryCode = vbBabel.BabelFiles.CountryName.USA

                                            Case Else
                                                Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong Swift code in Handelsbanken stated." & vbCrLf & vbCrLf & "Swiftcode:" & sSwift & vbCrLf & "Debit bankgiro: " & oPayment.I_Account & vbCrLf & sErrFixedText)

                                        End Select
                                    Else
                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong Swift code in Handelsbanken stated." & vbCrLf & vbCrLf & "Swiftcode:" & sSwift & vbCrLf & "Debit bankgiro: " & oPayment.I_Account & vbCrLf & sErrFixedText)
                                    End If

                                    If sOldPayType <> oPayment.PayType Then
                                        bSequenceAWritten = False
                                        sOldPayType = oPayment.PayType
                                    End If

                                    If Not bSequenceAWritten Then
                                        If Not EmptyString((oBabel.File_id)) Then
                                            sFile_ID = Right(Trim(oBabel.File_id) & Trim(Str(lCounter)) & Left(VB6.Format(Now, "SSMMHHDDMMYY"), 16), 16)
                                        Else
                                            sFile_ID = Right(VB6.Format(Now, "YYMMDDHHMMSS") & Trim(Str(lCounter)), 16)
                                        End If
                                        sLine = CStr(WriteSequenceARecord(oFile, oaccount, oPayment, eCountryCode, sCompanyNo, sSwift, sLastUsedExecutionDate, lCounter, sErrFixedText, Right(sFile_ID, 16)))
                                        bSequenceAWritten = True
                                    End If

                                    lCounter = lCounter + 1

                                    'For Financial payments, value CORT in 23E, to Svenska Handelsbanken Denmark the Beneficary account in
                                    'Tag 59 must be an IBAN
                                    'To other countries beneficary account in Handelsbaken has to be a BBAN (traditional account no)

                                    If oPayment.PayType = "I" Then
                                        'Create 1 sequence B section per invoice
                                        'For Each oInvoice In oPayment.Invoices
                                        If EmptyString((oPayment.REF_Own)) Then
                                            oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                        Else
                                            oFile.WriteLine(":21:" & Right(Trim(oPayment.REF_Own), 16)) 'Transactionref - 16x
                                        End If

                                        'Check if we have ERA or FRW
                                        sInfoToWriteInTag36 = ""
                                        If oPayment.ERA_ExchRateAgreed <> 0 Then
                                            If Not EmptyString((oPayment.ERA_DealMadeWith)) Then
                                                If bDebitAccountInHandelsbanken Then
                                                    Select Case eCountryCode
                                                        Case vbBabel.BabelFiles.CountryName.Denmark, vbBabel.BabelFiles.CountryName.Finland, vbBabel.BabelFiles.CountryName.Norway, vbBabel.BabelFiles.CountryName.Germany, vbBabel.BabelFiles.CountryName.Great_Britain, vbBabel.BabelFiles.CountryName.Netherlands, vbBabel.BabelFiles.CountryName.Poland, vbBabel.BabelFiles.CountryName.Hong_Kong, vbBabel.BabelFiles.CountryName.Singapore, vbBabel.BabelFiles.CountryName.USA
                                                            oFile.WriteLine(":21F:" & Left(Trim(oPayment.ERA_DealMadeWith), 16)) 'F/X Deal reference - 16x - not in use
                                                            sInfoToWriteInTag36 = ":36:" & HBGOConvertToCorrectAmount((oPayment.ERA_ExchRateAgreed))
                                                        Case Else
                                                            'Not allowed
                                                    End Select
                                                End If
                                            End If
                                        ElseIf oPayment.FRW_ForwardContractRate <> 0 Then
                                            If Not EmptyString((oPayment.FRW_ForwardContractNo)) Then
                                                If bDebitAccountInHandelsbanken Then
                                                    Select Case eCountryCode
                                                        Case vbBabel.BabelFiles.CountryName.Denmark, vbBabel.BabelFiles.CountryName.Finland, vbBabel.BabelFiles.CountryName.Norway, vbBabel.BabelFiles.CountryName.Germany, vbBabel.BabelFiles.CountryName.Great_Britain, vbBabel.BabelFiles.CountryName.Netherlands, vbBabel.BabelFiles.CountryName.Poland, vbBabel.BabelFiles.CountryName.Hong_Kong, vbBabel.BabelFiles.CountryName.Singapore, vbBabel.BabelFiles.CountryName.USA
                                                            oFile.WriteLine(":21F:" & Left(Trim(oPayment.FRW_ForwardContractNo), 16)) 'F/X Deal reference - 16x - not in use
                                                            sInfoToWriteInTag36 = ":36:" & HBGOConvertToCorrectAmount((oPayment.FRW_ForwardContractRate))
                                                        Case Else
                                                            'Not allowed
                                                    End Select
                                                End If
                                            End If
                                        End If

                                        'Instruction code - 4a[/30x]
                                        '� CHQB = cross border cheque payment (only Handelsbanken SE, GB, and LU)
                                        '� URGP = urgent
                                        '� INTC = Intra Company payment N.B. Beneficiary account number in Tag 59 is optional
                                        '� CORT = Financial payment N.B Beneficiary account number in Tag 59 is optional.
                                        '� OTHR/EKON = Economy payment (only Handelsbanken DK)

                                        If oPayment.ToOwnAccount Then
                                            oFile.WriteLine(":23E:INTC")
                                            eInstructionCode = HBGOInstructionCode.INTC
                                        ElseIf oPayment.Priority Then
                                            oFile.WriteLine(":23E:URGP")
                                            eInstructionCode = HBGOInstructionCode.URGP
                                        ElseIf oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                            Select Case eCountryCode
                                                Case vbBabel.BabelFiles.CountryName.Great_Britain, vbBabel.BabelFiles.CountryName.Sweden, vbBabel.BabelFiles.CountryName.Luxemburg
                                                    oFile.WriteLine(":23E:CHQB")
                                                    eInstructionCode = HBGOInstructionCode.CHQB
                                                Case Else
                                                    oFile.WriteLine(":23E:CORT")
                                                    eInstructionCode = HBGOInstructionCode.CORT
                                            End Select
                                        Else
                                            'No need to state a paymenttype
                                            'oFile.WriteLine ":23E:CORT"
                                        End If

                                        sLine = ":32B:"
                                        If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No currencycode stated. The code must be 3 positions.." & vbCrLf & vbCrLf & "Currencycode:" & sSwift & vbCrLf & sErrFixedText)
                                        ElseIf Len(Trim(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode stated. The code must be 3 positions.." & vbCrLf & vbCrLf & "Currencycode:" & sSwift & vbCrLf & sErrFixedText)
                                        Else
                                            sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                        End If
                                        sAmount = HBGOConvertToCorrectAmount((oPayment.MON_InvoiceAmount))
                                        sLine = sLine & sAmount
                                        oFile.WriteLine(sLine) 'Currency/Transaction Amount - 3!a15d - Currency and amount

                                        'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                        'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                        'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B

                                        ':56A: Intermediary
                                        If bDebitAccountInHandelsbanken Then
                                            If Not EmptyString((oPayment.BANK_SWIFTCodeCorrBank)) Then
                                                If eInstructionCode = HBGOInstructionCode.INTC Or eInstructionCode = HBGOInstructionCode.CORT Then
                                                    oFile.WriteLine(":56A:" & Trim(oPayment.BANK_SWIFTCodeCorrBank))
                                                End If
                                            End If
                                        Else
                                            'WARNING! Other banks may also allow the use of intermediary, according to agreement
                                        End If

                                        ':57A: Account With Institution Identification of beneficiary's bank
                                        ':57C:
                                        If Not eInstructionCode = HBGOInstructionCode.CHQB Then
                                            If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
                                                oPayment.BANK_SWIFTCode = Trim(oPayment.BANK_SWIFTCode)
                                                If Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11 Then
                                                    If Not EmptyString((oPayment.BANK_BranchNo)) And oPayment.BANK_BranchType > BabelFiles.BankBranchType.SWIFT Then
                                                        sTemp = ""
                                                        Select Case oPayment.BANK_BranchType
                                                            Case BabelFiles.BankBranchType.Bankleitzahl
                                                                sTemp = "BL"
                                                            Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                                sTemp = "AT"
                                                            Case BabelFiles.BankBranchType.Chips
                                                                sTemp = "FW"
                                                            Case BabelFiles.BankBranchType.Fedwire
                                                                sTemp = "FW"
                                                                'Case BankBranchType.MEPS
                                                                '    Not used
                                                            Case BabelFiles.BankBranchType.SortCode
                                                                sTemp = "SC"
                                                            Case BabelFiles.BankBranchType.US_ABA
                                                                sTemp = "FW"
                                                        End Select
                                                        oFile.WriteLine(":57A://" & sTemp & Trim(oPayment.BANK_BranchNo))
                                                        oFile.WriteLine(oPayment.BANK_SWIFTCode)
                                                    Else
                                                        oFile.WriteLine(":57A:" & oPayment.BANK_SWIFTCode)
                                                    End If
                                                Else
                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong SWIFT-code detected. The code must be either 8 or 11 positions." & vbCrLf & vbCrLf & "SWIFT-code:" & oPayment.BANK_SWIFTCode & vbCrLf & sErrFixedText)
                                                End If
                                            Else
                                                If Not EmptyString((oPayment.BANK_BranchNo)) And oPayment.BANK_BranchType > BabelFiles.BankBranchType.SWIFT Then
                                                    sTemp = ""
                                                    Select Case oPayment.BANK_BranchType
                                                        Case BabelFiles.BankBranchType.Bankleitzahl
                                                            sTemp = "BL"
                                                        Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                            sTemp = "AT"
                                                        Case BabelFiles.BankBranchType.Chips
                                                            sTemp = "FW"
                                                        Case BabelFiles.BankBranchType.Fedwire
                                                            sTemp = "FW"
                                                            'Case BankBranchType.MEPS
                                                            '    Not used
                                                        Case BabelFiles.BankBranchType.SortCode
                                                            sTemp = "SC"
                                                        Case BabelFiles.BankBranchType.US_ABA
                                                            sTemp = "FW"
                                                    End Select
                                                    oFile.WriteLine(":57C://" & sTemp & Trim(oPayment.BANK_BranchNo))
                                                Else
                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Neither a SWIFT-code nor a branchcode is stated. You must at least state one of the codes." & vbCrLf & vbCrLf & "SWIFT-code:" & oPayment.BANK_SWIFTCode & vbCrLf & sErrFixedText)
                                                End If
                                            End If
                                        End If

                                        'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                        sTemp = ""
                                        If EmptyString((oPayment.E_Account)) Then
                                            If eInstructionCode = HBGOInstructionCode.CHQB Then
                                                'OK, without ac.no.
                                            ElseIf eCountryCode = vbBabel.BabelFiles.CountryName.Sweden And (eInstructionCode = HBGOInstructionCode.CORT Or eInstructionCode = HBGOInstructionCode.INTC) Then
                                                'OK, without ac.no.
                                            Else
                                                Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & oPayment.E_Account & vbCrLf & sErrFixedText)
                                            End If
                                        End If
                                        sLine = ""
                                        If Not EmptyString((oPayment.E_Account)) Then
                                            sLine = ":59:/" & Trim(oPayment.E_Account)
                                        End If
                                        If Not EmptyString((oPayment.E_Name)) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left(Trim(oPayment.E_Name), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Name), 35)
                                            End If
                                        End If
                                        If Not EmptyString((oPayment.E_Adr1)) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left(Trim(oPayment.E_Adr1), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr1), 35)
                                            End If
                                        End If
                                        If Not EmptyString((oPayment.E_Adr2)) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left(Trim(oPayment.E_Adr2), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr2), 35)
                                            End If
                                        End If
                                        If Not EmptyString((oPayment.E_Zip)) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
                                            End If
                                        ElseIf Not EmptyString((oPayment.E_City)) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left(Trim(oPayment.E_City), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_City), 35)
                                            End If
                                        End If
                                        oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.

                                        '-------- fetch content of each payment ---------
                                        ' Total up freetext from all invoices;
                                        sFreetext = ""
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                sFreetext = sFreetext & Trim(oFreeText.Text)
                                            Next oFreeText
                                        Next oInvoice
                                        If Len(sFreetext) > 140 Then
                                            If oBabel.VB_ProfileInUse Then
                                                sFreetext = DiminishFreetext(sFreetext, 140, oPayment.E_Name & vbCrLf & oPayment.MON_InvoiceAmount)
                                            Else
                                                sFreetext = Left(sFreetext, 140)
                                            End If
                                        End If
                                        lCounter2 = 1
                                        For lCounter2 = 1 To 4
                                            If Not EmptyString(sFreetext) Then
                                                If lCounter2 = 1 Then
                                                    oFile.WriteLine(":70:" & Left(sFreetext, 35))
                                                Else
                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                End If
                                                If Len(sFreetext) > 35 Then
                                                    sFreetext = Mid(sFreetext, 36)
                                                Else
                                                    sFreetext = ""
                                                    Exit For
                                                End If
                                            Else
                                                Exit For
                                            End If
                                        Next lCounter2

                                        'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                        Select Case Mid(oPayment.BANK_I_SWIFTCode, 5, 2)
                                            Case "LU" 'Luxemburg
                                                'Regulatory reporting is mandatory
                                                sLine = "/" & bbGetStateBankCode((oPayment.Invoices.Item(1).STATEBANK_Code), "LU") & "/" & Trim(oPayment.E_CountryCode)
                                                If Not EmptyString((oPayment.Invoices.Item(1).STATEBANK_Text)) Then
                                                    sLine = sLine & Left(Trim(oPayment.Invoices.Item(1).STATEBANK_Text), 19)
                                                End If

                                            Case "NO" 'Norge
                                                'Regulatory reporting is mandatory
                                                sLine = "/" & bbGetStateBankCode((oPayment.Invoices.Item(1).STATEBANK_Code), "NO") & "/" & Trim(oPayment.E_CountryCode)
                                                If Not EmptyString((oPayment.Invoices.Item(1).STATEBANK_Text)) Then
                                                    sLine = sLine & Left(Trim(oPayment.Invoices.Item(1).STATEBANK_Text), 19)
                                                End If

                                            Case "SE" 'Sverige
                                                'Regulatory reporting is mandatory
                                                sLine = "/" & bbGetStateBankCode((oPayment.Invoices.Item(1).STATEBANK_Code), "SE") & "/" & Trim(oPayment.E_CountryCode)
                                                If Not EmptyString((oPayment.Invoices.Item(1).STATEBANK_Text)) Then
                                                    sLine = sLine & Left(Trim(oPayment.Invoices.Item(1).STATEBANK_Text), 19)
                                                End If

                                            Case Else
                                                'Regulatory reporting is not mandatory, just state the countrycode
                                                sLine = "//" & Mid(oPayment.BANK_I_SWIFTCode, 5, 2)

                                        End Select
                                        oFile.WriteLine(":77B:" & sLine)

                                        ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                        'Not in use

                                        'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                        'SHA = Each party pays its costs. This is the normal case
                                        'BEN = Beneficiary pays all costs
                                        'OUR = Remitter pays all costs
                                        'N.B. for Handelsbanken USA always treated As OUR

                                        If eCountryCode = vbBabel.BabelFiles.CountryName.USA Then
                                            oFile.WriteLine(":71A:OUR")
                                        Else
                                            If oPayment.MON_ChargeMeDomestic Then
                                                If oPayment.MON_ChargeMeAbroad Then
                                                    oFile.WriteLine(":71A:OUR")
                                                Else
                                                    oFile.WriteLine(":71A:SHA")
                                                End If
                                            Else
                                                If oPayment.MON_ChargeMeAbroad Then
                                                    'Not possible but set default
                                                    oFile.WriteLine(":71A:SHA")
                                                Else
                                                    oFile.WriteLine(":71A:BEN")
                                                End If
                                            End If
                                        End If

                                        ':25A: Charges Account /34x Not used
                                        'Not in use

                                        ':36: Exchange Rate /12d Not used
                                        If Not EmptyString(sInfoToWriteInTag36) Then
                                            oFile.WriteLine(sInfoToWriteInTag36)
                                        End If


                                        'Next oInvoice

                                    Else

                                        Select Case eCountryCode

                                            Case vbBabel.BabelFiles.CountryName.Denmark
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString((oInvoice.REF_Own)) Then
                                                        oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right(Trim(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    If oPayment.PayType = "S" Then
                                                        oFile.WriteLine(":23E:SALY") 'Salary
                                                    Else
                                                        'Local non-urgent. Don't state a code
                                                    End If

                                                    'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    sLine = ":32B:"
                                                    If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                        'Assume DKK
                                                        sLine = sLine & "DKK"
                                                    ElseIf Len(Trim(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected. The code must be 3 positions." & vbCrLf & vbCrLf & "Currencycode:" & Trim(oPayment.MON_InvoiceCurrency) & vbCrLf & sErrFixedText)
                                                    Else
                                                        sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount((oInvoice.MON_InvoiceAmount))
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)

                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    'oFile.WriteLine ':57A: Account With Institution Identification of beneficiary's bank

                                                    'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                                    sTemp = ""

                                                    '11.04.2008 - Don't allow IBAN
                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                    If ValidateIBAN((oPayment.E_Account), False) Then
                                                        'IBAN format: DKkk BBBB CCCC CCCC CC
                                                        'B = bank No., C = account No.
                                                        oPayment.E_Account = Mid(oPayment.E_Account, 5)
                                                    End If

                                                    If Len(Trim(oPayment.E_Account)) > 0 Then
                                                        'For some types there are no Betaleridentifikation
                                                        If Len(oInvoice.Unique_Id) > 2 Then
                                                            sTemp = Left(oInvoice.Unique_Id, 2) & "-" & Mid(oInvoice.Unique_Id, 3) & "-" & sTemp
                                                        ElseIf Len(oInvoice.Unique_Id) > 0 Then
                                                            sTemp = Left(oInvoice.Unique_Id, 2) & "-" & sTemp
                                                        Else
                                                            sTemp = ""
                                                        End If
                                                    Else
                                                        If Len(oInvoice.Unique_Id) > 0 Then
                                                            sTemp = Left(oInvoice.Unique_Id, 2) & "-" & Mid(oInvoice.Unique_Id, 3)
                                                        Else
                                                            sTemp = ""
                                                        End If
                                                    End If

                                                    bMessageIsMandatory = False
                                                    sFreetext = ""

                                                    If oPayment.PayType = "S" Then
                                                        'Account payment
                                                        If Not EmptyString((oPayment.E_Account)) Then
                                                            oFile.WriteLine(":59:/" & Trim(oPayment.E_Account))
                                                        Else
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                        End If
                                                        oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                        bMessageIsMandatory = False
                                                        sFreetext = ""
                                                    ElseIf Len(sTemp) = 0 Then
                                                        'Account payment
                                                        If Not EmptyString((oPayment.E_Account)) Then
                                                            oFile.WriteLine(":59:/" & Trim(oPayment.E_Account))
                                                        Else
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                        End If
                                                        oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                        bMessageIsMandatory = False
                                                        sFreetext = ""
                                                        For Each oFreeText In oInvoice.Freetexts
                                                            sFreetext = sFreetext & Trim(oFreeText.Text)
                                                        Next oFreeText
                                                    Else
                                                        'FI Kort
                                                        If Not ValidateFI(sTemp, sKortartKode, sBetalerIdent, sKreditorNo, (BabelFiles.FileType.HB_GlobalOnline_MT101), sErrorString, True) Then
                                                            'May add some errorinfo here
                                                            'bx = AddToErrorArray(UCase(sVariableName), sErrorString, "1")
                                                        End If
                                                        Select Case sKortartKode
                                                            Case "01"
                                                                If Not EmptyString((oPayment.E_Account)) Then
                                                                    oFile.WriteLine(":59:/GIRO" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = ""
                                                                For Each oFreeText In oInvoice.Freetexts
                                                                    sFreetext = sFreetext & Trim(oFreeText.Text)
                                                                Next oFreeText
                                                            Case "04", "15"
                                                                If Not EmptyString((oPayment.E_Account)) Then
                                                                    oFile.WriteLine(":59:/GIRO" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = sKortartKode & sBetalerIdent
                                                            Case "71", "75"
                                                                If Not EmptyString((oPayment.E_Account)) Then
                                                                    oFile.WriteLine(":59:/FI" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = sKortartKode & sBetalerIdent
                                                            Case "73"
                                                                If Not EmptyString((oPayment.E_Account)) Then
                                                                    oFile.WriteLine(":59:/FI" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                sFreetext = ""
                                                                For Each oFreeText In oInvoice.Freetexts
                                                                    sFreetext = sFreetext & Trim(oFreeText.Text)
                                                                Next oFreeText


                                                        End Select
                                                    End If

                                                    lCounter2 = 1

                                                    If Len(sFreetext) > 140 Then
                                                        If oBabel.VB_ProfileInUse Then
                                                            sFreetext = DiminishFreetext(sFreetext, 140, oPayment.E_Name & vbCrLf & oPayment.MON_InvoiceAmount)
                                                        Else
                                                            sFreetext = Left(sFreetext, 140)
                                                        End If
                                                    End If

                                                    For lCounter2 = 1 To 4
                                                        If Not EmptyString(sFreetext) Then
                                                            If lCounter2 = 1 Then
                                                                oFile.WriteLine(":70:" & Left(sFreetext, 35))
                                                            Else
                                                                oFile.WriteLine(Left(sFreetext, 35))
                                                            End If
                                                            If Len(sFreetext) > 35 Then
                                                                sFreetext = Mid(sFreetext, 36)
                                                            Else
                                                                sFreetext = ""
                                                                Exit For
                                                            End If
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case vbBabel.BabelFiles.CountryName.Finland
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString((oInvoice.REF_Own)) Then
                                                        oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right(Trim(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use
                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    '10.03.2010 - New code for Ulefos regarding salary
                                                    If oPayment.PayType = "S" And oPayment.PayCode = "203" Then 'Salary
                                                        oFile.WriteLine(":23E:OTHR/SALY")
                                                    End If
                                                    sLine = ":32B:"
                                                    If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Len(Trim(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected. The code must be 3 positions." & vbCrLf & vbCrLf & "Currencycode:" & Trim(oPayment.MON_InvoiceCurrency) & vbCrLf & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteHBGlobalOnline_MT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount((oInvoice.MON_InvoiceAmount))
                                                    '                                        If Len(Trim$(oInvoice.MON_InvoiceAmount)) = 1 Then
                                                    '                                            sAmount = "0,0" & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        ElseIf Len(Trim$(oInvoice.MON_InvoiceAmount)) = 2 Then
                                                    '                                            sAmount = "0," & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        Else
                                                    '                                            sAmount = Trim$(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                            sAmount = Left$(sAmount, Len(sAmount) - 2) & "," & Right$(sAmount, 2)
                                                    '                                        End If
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine) 'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    'oFile.WriteLine ':57A: Account With Institution Identification of beneficiary's bank
                                                    If EmptyString((oPayment.E_Account)) Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                    Else
                                                        sLine = ":59:/" & Trim(oPayment.E_Account)
                                                        If Not EmptyString((oPayment.E_Name)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Adr1)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Adr2)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr2), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Zip)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString((oPayment.E_City)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                                    '10.03.2010 - New code for Ulefos regarding salary
                                                    If oPayment.PayType = "S" And oPayment.PayCode = "203" Then 'Salary
                                                        If Not EmptyString((oPayment.e_OrgNo)) Then
                                                            oFile.WriteLine(":70:" & Replace(Trim(oPayment.e_OrgNo), "-", ""))
                                                        End If
                                                    Else
                                                        If Not EmptyString((oInvoice.Unique_Id)) Then
                                                            oInvoice.Unique_Id = RemoveLeadingCharacters((oInvoice.Unique_Id), "0")
                                                            If Len(Trim(oInvoice.Unique_Id)) > 20 Then
                                                                Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "The reference stated is too long. Maximum length is 20 digits." & vbCrLf & vbCrLf & "Reference:" & Trim(oInvoice.Unique_Id) & vbCrLf & sErrFixedText)
                                                            ElseIf Not vbIsNumeric(Trim(oInvoice.Unique_Id), "0123456789") Then
                                                                Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "The reference stated is not valid, it must be numeric." & vbCrLf & vbCrLf & "Reference:" & Trim(oInvoice.Unique_Id) & vbCrLf & sErrFixedText)
                                                            Else
                                                                oFile.WriteLine(":70:" & Trim(oInvoice.Unique_Id))
                                                            End If
                                                        Else
                                                            sFreetext = ""
                                                            For Each oFreeText In oInvoice.Freetexts
                                                                sFreetext = sFreetext & Trim(oFreeText.Text)
                                                            Next oFreeText
                                                            lCounter2 = 1
                                                            For lCounter2 = 1 To 4
                                                                If Not EmptyString(sFreetext) Then
                                                                    If lCounter2 = 1 Then
                                                                        oFile.WriteLine(":70:" & Left(sFreetext, 35))
                                                                    Else
                                                                        oFile.WriteLine(Left(sFreetext, 35))
                                                                    End If
                                                                    If Len(sFreetext) > 35 Then
                                                                        sFreetext = Mid(sFreetext, 36)
                                                                    Else
                                                                        sFreetext = ""
                                                                        Exit For
                                                                    End If
                                                                Else
                                                                    Exit For
                                                                End If
                                                            Next lCounter2
                                                        End If
                                                    End If

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice
                                                'XNET - 31.01.2012 - Added next case
                                            Case vbBabel.BabelFiles.CountryName.France
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use
                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    '10.03.2010 - New code for Ulefos regarding salary
                                                    If oPayment.PayType = "S" And oPayment.PayCode = "203" Then 'Salary
                                                        oFile.WriteLine(":23E:OTHR/SALY")
                                                    End If
                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Trim(oPayment.MON_InvoiceCurrency) = "EUR" Then
                                                        sLine = sLine & "EUR"
                                                    Else
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected. The code must be EUR." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    'oFile.WriteLine ':57A: Account With Institution Identification of beneficiary's bank
                                                    If EmptyString(oPayment.E_Account) Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                           "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        sLine = ":59:/" & Trim$(oPayment.E_Account)
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr1) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr2) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Zip) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString(oPayment.E_City) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                                    sFreetext = vbNullString
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim$(oFreeText.Text)
                                                    Next oFreeText
                                                    lCounter2 = 1
                                                    For lCounter2 = 1 To 4
                                                        If Not EmptyString(sFreetext) Then
                                                            If lCounter2 = 1 Then
                                                                oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                            Else
                                                                oFile.WriteLine(Left$(sFreetext, 35))
                                                            End If
                                                            If Len(sFreetext) > 35 Then
                                                                sFreetext = Mid$(sFreetext, 36)
                                                            Else
                                                                sFreetext = vbNullString
                                                                Exit For
                                                            End If
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case vbBabel.BabelFiles.CountryName.Germany
                                                'Create 1 sequence B section per invoice
                                                'The format requires account no and BLZ
                                                If ValidateIBAN((oPayment.E_Account), False) Then
                                                    'IBAN format: DEkk BBBB BBBB CCCC CCCC CC
                                                    'B = sort code (Bankleitzahl/BLZ), C = account No.
                                                    oPayment.BANK_BranchType = BabelFiles.BankBranchType.Bankleitzahl
                                                    oPayment.BANK_BranchNo = Mid(oPayment.E_Account, 5, 8)
                                                    oPayment.E_Account = Mid(oPayment.E_Account, 13)
                                                End If
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString((oInvoice.REF_Own)) Then
                                                        oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right(Trim(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use
                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    sLine = ":32B:"
                                                    If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Trim(oPayment.MON_InvoiceCurrency) <> "EUR" Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected for German local payments. The code must be EUR." & vbCrLf & vbCrLf & "Currencycode:" & Trim(oPayment.MON_InvoiceCurrency) & vbCrLf & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteHBGlobalOnline_MT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount((oInvoice.MON_InvoiceAmount))
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine) 'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    If Left(oPayment.BANK_BranchNo, 2) = "BL" Then
                                                        oPayment.BANK_BranchNo = Mid(oPayment.BANK_BranchNo, 3)
                                                    End If
                                                    If oPayment.BANK_BranchType <> BabelFiles.BankBranchType.Bankleitzahl And oPayment.BANK_BranchType <> BabelFiles.BankBranchType.NoBranchType Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong branchtype stated for a domestic German payment. A BLZ code is mandatory." & vbCrLf & vbCrLf & sErrFixedText)
                                                    ElseIf Not ValidateBankID((BabelFiles.BankBranchType.Bankleitzahl), (oPayment.BANK_BranchNo), False, sErrorString) Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", sErrorString & vbCrLf & vbCrLf & "BLZ Code:" & Trim(oPayment.BANK_BranchNo) & vbCrLf & sErrFixedText)
                                                    Else
                                                        oFile.WriteLine(":57C://BL" & Trim(oPayment.BANK_BranchNo)) 'BLZ - Account With Institution Identification of beneficiary's bank
                                                    End If
                                                    If EmptyString((oPayment.E_Account)) Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                    Else
                                                        sLine = ":59:/" & Trim(oPayment.E_Account)
                                                        If Not EmptyString((oPayment.E_Name)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Adr1)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Adr2)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr2), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Zip)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString((oPayment.E_City)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                                    sFreetext = ""
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                    Next oFreeText
                                                    'Maxlength = 108
                                                    lCounter2 = 1
                                                    For lCounter2 = 1 To 4
                                                        If Not EmptyString(sFreetext) Then
                                                            If lCounter2 = 1 Then
                                                                oFile.WriteLine(":70:" & Left(sFreetext, 35))
                                                            ElseIf lCounter2 = 4 Then
                                                                oFile.WriteLine(Left(sFreetext, 3))
                                                            Else
                                                                oFile.WriteLine(Left(sFreetext, 35))
                                                            End If
                                                            If Len(sFreetext) > 35 Then
                                                                sFreetext = Mid(sFreetext, 36)
                                                            Else
                                                                sFreetext = ""
                                                                Exit For
                                                            End If
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case vbBabel.BabelFiles.CountryName.Great_Britain
                                                'NOT implemented yet
                                                'Create 1 sequence B section per invoice

                                                'The format requires account no and Sortcode
                                                If ValidateIBAN(Trim(oPayment.E_Account), False) Then
                                                    '(22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                                                    'B = alphabetical bank code, S = sort code (often a specific branch), C = account No.
                                                    oPayment.BANK_BranchType = BabelFiles.BankBranchType.SortCode
                                                    oPayment.BANK_BranchNo = Mid(oPayment.E_Account, 9, 6)
                                                    oPayment.E_Account = Mid(oPayment.E_Account, 15)
                                                End If

                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString((oInvoice.REF_Own)) Then
                                                        oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right(Trim(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    If oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                                        oPayment.Priority = False 'To be sure in a later test
                                                        bNormalPayment = False
                                                        oFile.WriteLine(":23E:CHQB") 'Instruction code - 4a[/30x]
                                                    ElseIf oPayment.Priority Then
                                                        oFile.WriteLine(":23E:URGP") 'Instruction code - 4a[/30x]
                                                        bNormalPayment = False
                                                    Else
                                                        bNormalPayment = True 'BACS
                                                        'Do not specify
                                                    End If

                                                    sLine = ":32B:"
                                                    If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                        'Assume GBP
                                                        sLine = sLine & "GBP"
                                                    ElseIf Len(Trim(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected for British local payments. The code must be GBP." & vbCrLf & vbCrLf & "Currencycode:" & Trim(oPayment.MON_InvoiceCurrency) & vbCrLf & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteHBGlobalOnline_MT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount((oInvoice.MON_InvoiceAmount))
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine) 'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    If Not oPayment.Cheque Then
                                                        If oPayment.BANK_BranchType <> BabelFiles.BankBranchType.SortCode And oPayment.BANK_BranchType <> BabelFiles.BankBranchType.NoBranchType Then
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong branchtype stated for a domestic Britsh payment. A sort code is mandatory." & vbCrLf & vbCrLf & sErrFixedText)
                                                        ElseIf Not ValidateBankID((BabelFiles.BankBranchType.SortCode), (oPayment.BANK_BranchNo), False, sErrorString) Then
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", sErrorString & vbCrLf & vbCrLf & "BLZ Code:" & Trim(oPayment.BANK_BranchNo) & vbCrLf & sErrFixedText)
                                                        Else
                                                            oFile.WriteLine(":57C://SC" & Trim(oPayment.BANK_BranchNo)) 'SC - Account With Institution Identification of beneficiary's bank
                                                        End If
                                                    End If
                                                    If bNormalPayment Then 'BACS
                                                        sLine = ":59:/" & Trim(oPayment.E_Account)
                                                        If Not EmptyString((oPayment.E_Name)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Name), 18)
                                                        End If
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        If EmptyString((oPayment.E_Account)) And Not oPayment.Cheque Then
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                        Else
                                                            If CBool(oPayment.Cheque) Then
                                                                If EmptyString(oPayment.E_Name & oPayment.E_Adr1 & oPayment.E_Adr2 & oPayment.E_Adr3) Then
                                                                    Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Full name and address must be No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Name + address: " & Trim(oPayment.E_Name) & "-" & Trim(oPayment.E_Adr1) & "-" & Trim(oPayment.E_Adr2) & "-" & Trim(oPayment.E_Adr3) & "-" & Trim(oPayment.E_Zip) & "-" & Trim(oPayment.E_City) & vbCrLf & sErrFixedText)
                                                                End If
                                                            End If
                                                            sLine = ":59:/" & Trim(oPayment.E_Account)
                                                            If Not EmptyString((oPayment.E_Name)) Then
                                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Name), 35)
                                                            End If
                                                            If Not EmptyString((oPayment.E_Adr1)) Then
                                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr1), 35)
                                                            End If
                                                            If Not EmptyString((oPayment.E_Adr2)) Then
                                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr2), 35)
                                                            End If
                                                            If Not EmptyString((oPayment.E_Zip)) Then
                                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
                                                            ElseIf Not EmptyString((oPayment.E_City)) Then
                                                                sLine = sLine & vbCrLf & Left(Trim(oPayment.E_City), 35)
                                                            End If
                                                        End If
                                                        oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                                    End If
                                                    sFreetext = ""
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                    Next oFreeText
                                                    If bNormalPayment Then 'BACS
                                                        'Maxlength = 18
                                                        oFile.WriteLine(":70:" & Left(sFreetext, 18))
                                                    Else
                                                        'Maxlength = 140
                                                        lCounter2 = 1
                                                        For lCounter2 = 1 To 4
                                                            If Not EmptyString(sFreetext) Then
                                                                If lCounter2 = 1 Then
                                                                    oFile.WriteLine(":70:" & Left(sFreetext, 35))
                                                                Else
                                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                                End If
                                                                If Len(sFreetext) > 35 Then
                                                                    sFreetext = Mid(sFreetext, 36)
                                                                Else
                                                                    sFreetext = ""
                                                                    Exit For
                                                                End If
                                                            Else
                                                                Exit For
                                                            End If
                                                        Next lCounter2
                                                    End If
                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case vbBabel.BabelFiles.CountryName.Hong_Kong

                                            Case vbBabel.BabelFiles.CountryName.Luxemburg

                                            Case vbBabel.BabelFiles.CountryName.Netherlands
                                                'Create 1 sequence B section per invoice
                                                'The format requires account no and not IBAN
                                                If ValidateIBAN((oPayment.E_Account), False) Then
                                                    'NLkk BBBB CCCC CCCC CC
                                                    'The first 4 alphanumeric characters represent a bank
                                                    '   and the last 10 digits an account
                                                    'NL98FTSB0244721734
                                                    'oPayment.BANK_BranchType = BankBranchType.SWIFT
                                                    oPayment.E_Account = RemoveLeadingCharacters(Mid(oPayment.E_Account, 9), "0")
                                                End If
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString((oInvoice.REF_Own)) Then
                                                        oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right(Trim(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    If oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                                        oPayment.Priority = False 'To be sure in a later test
                                                        bNormalPayment = False
                                                        oFile.WriteLine(":23E:OTHR/SALY") 'Instruction code - 4a[/30x]
                                                    ElseIf oPayment.Priority Then
                                                        oFile.WriteLine(":23E:URGP") 'Instruction code - 4a[/30x]
                                                        bNormalPayment = False
                                                    Else
                                                        bNormalPayment = True
                                                        'Do not specify
                                                    End If

                                                    sLine = ":32B:"
                                                    If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Trim(oPayment.MON_InvoiceCurrency) <> "EUR" Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected for Dutch local payments. The code must be EUR." & vbCrLf & vbCrLf & "Currencycode:" & Trim(oPayment.MON_InvoiceCurrency) & vbCrLf & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteHBGlobalOnline_MT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount((oInvoice.MON_InvoiceAmount))
                                                    '                                        If Len(Trim$(oInvoice.MON_InvoiceAmount)) = 1 Then
                                                    '                                            sAmount = "0,0" & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        ElseIf Len(Trim$(oInvoice.MON_InvoiceAmount)) = 2 Then
                                                    '                                            sAmount = "0," & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        Else
                                                    '                                            sAmount = Trim$(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                            sAmount = Left$(sAmount, Len(sAmount) - 2) & "," & Right$(sAmount, 2)
                                                    '                                        End If
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine) 'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                    If Len(oPayment.E_Account) < 3 Or Len(oPayment.E_Account) > 9 Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No or wrong accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                    Else
                                                        If Len(oPayment.E_Account) < 8 Then
                                                            bThisIsAPostBankAccount = True
                                                        Else
                                                            bThisIsAPostBankAccount = False
                                                        End If
                                                        If bThisIsAPostBankAccount Or oPayment.Priority Then
                                                            bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                                        End If
                                                        sLine = ":59:/" & oPayment.E_Account
                                                        If Not EmptyString((oPayment.E_Name)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Adr1)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString((oPayment.E_Adr2)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Adr2), 35)
                                                        End If
                                                        'Sometimes the city is mandatory
                                                        If bThisIsAPostBankAccount Then
                                                            If EmptyString((oPayment.E_City)) Then
                                                                Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "When a payment is done to a post bankaccount, the receivers city is mandatory." & vbCrLf & vbCrLf & "City:" & Trim(oPayment.E_City) & vbCrLf & sErrFixedText)
                                                            End If
                                                        ElseIf oPayment.Priority Then
                                                            If EmptyString((oPayment.E_City)) Then
                                                                Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "When it is an urgent payment, the receivers city is mandatory." & vbCrLf & vbCrLf & "City:" & Trim(oPayment.E_City) & vbCrLf & sErrFixedText)
                                                            End If
                                                        End If
                                                        If Not EmptyString((oPayment.E_Zip)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString((oPayment.E_City)) Then
                                                            sLine = sLine & vbCrLf & Left(Trim(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary�s account number, name and address.
                                                    If bNormalPayment And Not EmptyString((oInvoice.Unique_Id)) Then
                                                        oInvoice.Unique_Id = RemoveLeadingCharacters((oInvoice.Unique_Id), "0")
                                                        If Len(Trim(oInvoice.Unique_Id)) > 18 Or Len(Trim(oInvoice.Unique_Id)) < 9 Then
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "The reference stated on the acceptgiro-payment is of wrong length. The length must be bewtween 9 and 18 digits including the form code." & vbCrLf & vbCrLf & "Reference:" & Trim(oInvoice.Unique_Id) & vbCrLf & sErrFixedText)
                                                        ElseIf Not vbIsNumeric(Trim(oInvoice.Unique_Id), "0123456789") Then
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "The reference stated on the acceptgiro-payment is not valid, it must be numeric." & vbCrLf & vbCrLf & "Reference:" & Trim(oInvoice.Unique_Id) & vbCrLf & sErrFixedText)
                                                        Else
                                                            oFile.WriteLine(":70:" & Trim(oInvoice.Unique_Id))
                                                        End If
                                                    Else
                                                        sFreetext = ""
                                                        For Each oFreeText In oInvoice.Freetexts
                                                            sFreetext = sFreetext & Trim(oFreeText.Text)
                                                        Next oFreeText
                                                        If bNormalPayment Then
                                                            iCharactersPerLine = 32
                                                        Else
                                                            iCharactersPerLine = 35
                                                        End If
                                                        lCounter2 = 1
                                                        For lCounter2 = 1 To 4
                                                            If Not EmptyString(sFreetext) Then
                                                                If lCounter2 = 1 Then
                                                                    oFile.WriteLine(":70:" & Left(sFreetext, iCharactersPerLine))
                                                                Else
                                                                    oFile.WriteLine(Left(sFreetext, iCharactersPerLine))
                                                                End If
                                                                If Len(sFreetext) > iCharactersPerLine Then
                                                                    sFreetext = Mid(sFreetext, iCharactersPerLine + 1)
                                                                Else
                                                                    sFreetext = ""
                                                                    Exit For
                                                                End If
                                                            Else
                                                                Exit For
                                                            End If
                                                        Next lCounter2
                                                    End If

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case vbBabel.BabelFiles.CountryName.Norway

                                            Case vbBabel.BabelFiles.CountryName.Poland

                                            Case vbBabel.BabelFiles.CountryName.Singapore

                                            Case vbBabel.BabelFiles.CountryName.Sweden
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString((oInvoice.REF_Own)) Then
                                                        oFile.WriteLine(":21:" & Trim(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right(Trim(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    If oPayment.PayType = "S" Then
                                                        oFile.WriteLine(":23E:SALY")
                                                    End If

                                                    'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    sLine = ":32B:"
                                                    If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                        'Assume SEK
                                                        sLine = sLine & "SEK"
                                                    ElseIf Len(Trim(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "Wrong currencycode detected. The code must be 3 positions." & vbCrLf & vbCrLf & "Currencycode:" & Trim(oPayment.MON_InvoiceCurrency) & vbCrLf & sErrFixedText)
                                                    Else
                                                        sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = HBGOConvertToCorrectAmount((oInvoice.MON_InvoiceAmount))
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)

                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used

                                                    'oFile.WriteLine ':57C: Account With Institution Identification of beneficiary's bank
                                                    If ValidateIBAN((oPayment.E_Account), False) Then
                                                        'IBAN format: SEkk BBBB CCCC CCCC CCCC CCCC
                                                        'The Bs represent the bank code and the Cs the account number.
                                                        oPayment.E_Account = Trim(oPayment.E_Account)
                                                        sClearingCode = Mid(oPayment.E_Account, 5, 4)
                                                        If sClearingCode = "6000" Then
                                                            'Treat Handelsbanken in a special way. Clearing code is NOT stated as a part of the AccountNo in IBAN
                                                            oPayment.E_Account = sClearingCode & RemoveLeadingCharacters(Mid(oPayment.E_Account, 9), "0")
                                                            oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                        ElseIf sClearingCode = "8000" Then
                                                            'Treat Swedbank in a special way. Use the accountno from the IBAN,
                                                            '  but remove the 5th position.
                                                            oPayment.E_Account = RemoveLeadingCharacters(Mid(oPayment.E_Account, 9), "0")
                                                            oPayment.E_Account = Left(oPayment.E_Account, 4) & Mid(oPayment.E_Account, 6)
                                                            oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                        Else
                                                            oPayment.E_Account = RemoveLeadingCharacters(Mid(oPayment.E_Account, 9), "0")
                                                            oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                        End If
                                                    End If

                                                    oPayment.E_Account = Trim(oPayment.E_Account)
                                                    If oPayment.PayType = "S" Then
                                                        oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                    Else
                                                        If oPayment.E_AccountType = BabelFiles.AccountType.NoAccountType And Not EmptyString((oPayment.E_Account)) Then
                                                            'Something is stated in the E_Account but what?
                                                            'Let's do some qualified guessing
                                                            If (InStr(1, oPayment.E_Account, "-", CompareMethod.Text) > 0) And (Len(oPayment.E_Account) = 8 Or Len(oPayment.E_Account) = 9 Or Len(oPayment.E_Account) = 10) Then
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro
                                                                oPayment.E_Account = Replace(oPayment.E_Account, "-", "", , , CompareMethod.Text)
                                                            ElseIf Len(oPayment.E_Account) = 7 Or Len(oPayment.E_Account) = 8 Or Len(oPayment.E_Account) = 9 Then
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro
                                                            ElseIf Len(oPayment.E_Account) > 7 And Len(oPayment.E_Account) < 15 Then
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                            Else
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro
                                                            End If
                                                        End If
                                                    End If

                                                    If EmptyString((oPayment.E_Account)) Then
                                                        Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf & sErrFixedText)
                                                    Else
                                                        Select Case oPayment.E_AccountType

                                                            Case BabelFiles.AccountType.SE_Bankgiro
                                                                oFile.WriteLine(":57C://SE9900")
                                                                oFile.WriteLine(":59:/" & oPayment.E_Account)
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                If EmptyString((oInvoice.Unique_Id)) Then
                                                                    sFreetext = ""
                                                                    For Each oFreeText In oInvoice.Freetexts
                                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                                    Next oFreeText
                                                                Else
                                                                    sFreetext = Trim(oInvoice.Unique_Id)
                                                                End If

                                                            Case BabelFiles.AccountType.SE_PlusGiro
                                                                oFile.WriteLine(":57C://SE9500")
                                                                oFile.WriteLine(":59:/" & oPayment.E_Account)
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                If EmptyString((oInvoice.Unique_Id)) Then
                                                                    sFreetext = ""
                                                                    For Each oFreeText In oInvoice.Freetexts
                                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                                    Next oFreeText
                                                                Else
                                                                    sFreetext = Trim(oInvoice.Unique_Id)
                                                                End If


                                                            Case BabelFiles.AccountType.SE_Bankaccount
                                                                'No :57C:
                                                                oFile.WriteLine(":59:/" & oPayment.E_Account)
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = ""
                                                                If oPayment.PayType <> "S" Then
                                                                    For Each oFreeText In oInvoice.Freetexts
                                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                                    Next oFreeText
                                                                Else
                                                                    bMessageIsMandatory = False
                                                                End If

                                                        End Select
                                                    End If

                                                    If Not EmptyString(sFreetext) Then
                                                        If Len(sFreetext) > 140 Then
                                                            If oBabel.VB_ProfileInUse Then
                                                                sFreetext = DiminishFreetext(sFreetext, 140, oPayment.E_Name & vbCrLf & oPayment.MON_InvoiceAmount)
                                                            Else
                                                                sFreetext = Left(sFreetext, 140)
                                                            End If
                                                        End If
                                                        lCounter2 = 1
                                                        For lCounter2 = 1 To 4
                                                            If Not EmptyString(sFreetext) Then
                                                                If lCounter2 = 1 Then
                                                                    oFile.WriteLine(":70:" & Left(sFreetext, 35))
                                                                Else
                                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                                End If
                                                                If Len(sFreetext) > 35 Then
                                                                    sFreetext = Mid(sFreetext, 36)
                                                                Else
                                                                    sFreetext = ""
                                                                    Exit For
                                                                End If
                                                            Else
                                                                Exit For
                                                            End If
                                                        Next lCounter2
                                                    Else
                                                        If bMessageIsMandatory Then
                                                            Err.Raise(1000, "WriteHBGlobalOnline_MT101File", "No message to beneficiary is stated. It is mandatory." & vbCrLf & vbCrLf & sErrFixedText)
                                                        End If
                                                    End If

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case vbBabel.BabelFiles.CountryName.USA

                                        End Select

                                    End If 'If oPayment.PayCode = "I"

                                    oPayment.Exported = True

                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch

                End If

            Next oBabel

        Catch ex As Exception

            'If UCase(Left(Err.Source, 5)) = "WRITE" Then
            '    Err.Raise(Err.Number, Err.Source, Err.Description)
            'Else
            '    Err.Raise(Err.Number, "WriteHBGlobalOnline_MT101File", Err.Description)
            'End If
            Throw New vbBabel.Payment.PaymentException("Function: WriteHBGlobalOnline_MT101File" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteHBGlobalOnline_MT101File = True

    End Function

    Private Function WriteSequenceARecord(ByRef oFile As Scripting.TextStream, ByRef oaccount As vbBabel.Account, ByRef oPayment As vbBabel.Payment, ByRef eCountryCode As vbBabel.BabelFiles.CountryName, ByRef sCompanyNo As String, ByRef sI_Swift As String, ByRef sExecutionDate As String, ByRef lPaymentCounter As Integer, ByRef sErrFixedText As String, ByRef sFileID As String) As Boolean
        'M 20 Sender�s Reference 16x Must be unique for each message (or chain of messages) between the sender and the bank.
        'O 21R Customer Specified Reference 16x Always �NOLI�
        'N 28D Message Index / Total 5n/5n Accepted but not acted upon
        'O 50L Instructing Party 35x Accepted but not acted upon
        'M 50H Ordering Customer /34x 4*35x Line 1: /Account to be debited for all transactions in sequence B. Line 2: customer identity in Handelsbanken, Business 4organisation number or SHB no. See country specific information
        'M 52A Account Servicing Institution 4!a2!a2!c [3!c] BIC code of the bank servicing the customer�s account to be debited, e.g. HANDDKKK. See country specific information.
        'N 51A Sending Institution Not used
        'M 30 Requested Execution Date 6!n Execution date The date on which all subsequent transactions should be initiated by the executing bank.
        'N 25 Authorisation 35x Not used
        Dim sLine As String

        sLine = ":20:" & sFileID 'Sender�s Reference
        oFile.WriteLine(sLine)
        If oPayment.PayType <> "I" Then
            oFile.WriteLine(":21R:NOLI") 'Customer Specified Reference
        Else
            'Nothing to do
        End If
        If EmptyString(oPayment.I_Account) Then
            Err.Raise(1000, , "WriteHBGlobalOnline_MT101File", "No debit account stated for ordering customer. It is mandatory." & vbCrLf & vbCrLf & "Accountnumber:" & Trim(oPayment.I_Account) & vbCrLf & sErrFixedText)
        Else
            oFile.WriteLine(":50H:/" & Trim(oPayment.I_Account))
            '    Select Case eCountryCode
            '
            '    Case HBGOCountryCode.Finland, HBGOCountryCode.Germany, HBGOCountryCode.Netherlands
            '        'Customer identity in Handelsbanken, The test that a valid customer ID exists is done in the calling function.

            'New 26.02.2008
            If EmptyString(oaccount.ContractNo) Then
                oFile.WriteLine(sCompanyNo)
            Else
                oFile.WriteLine(Trim(oaccount.ContractNo))
            End If

            '    Case Else
            '
            '    End Select
        End If
        oFile.WriteLine(":52A:" & sI_Swift)
        oFile.WriteLine(":30:" & Mid(sExecutionDate, 3))

        WriteSequenceARecord = True

    End Function
    'Private Function WriteLocalFinland(oFile As TextStream, oaccount As vbBabel.Account, oPayment As vbBabel.Payment, eCountryCode As HBGOCountryCode, sCompanyNo As String, sI_Swift As String, sExecutionDate As String) As Boolean
    ''M 20 Sender�s Reference 16x Must be unique for each message (or chain of messages) between the sender and the bank.
    ''O 21R Customer Specified Reference 16x Always �NOLI�
    ''N 28D Message Index / Total 5n/5n Accepted but not acted upon
    ''O 50L Instructing Party 35x Accepted but not acted upon
    ''M 50H Ordering Customer /34x 4*35x Line 1: /Account to be debited for all transactions in sequence B. Line 2: customer identity in Handelsbanken, Business 4organisation number or SHB no. See country specific information
    ''M 52A Account Servicing Institution 4!a2!a2!c [3!c] BIC code of the bank servicing the customer�s account to be debited, e.g. HANDDKKK. See country specific information.
    ''N 51A Sending Institution Not used
    ''M 30 Requested Execution Date 6!n Execution date The date on which all subsequent transactions should be initiated by the executing bank.
    ''N 25 Authorisation 35x Not used
    'Dim sLine As String
    '
    'On Error GoTo errWriteSequenceARecord
    '
    'sLine = ":20:" &VB6.Format(Now(), "YYYYMMDDHHMMSS") 'Sender�s Reference
    'oFile.WriteLine sLine
    'oFile.WriteLine ":21R:NOLI" 'Customer Specified Reference
    'If EmptyString(oPayment.I_Account) Then
    '    Err.Raise 1, "WriteSequenceARecord", "No debit account stated for ordering customer."
    'Else
    '    oFile.WriteLine ":50H:/" & Trim$(oPayment.I_Account)
    '    Select Case eCountryCode
    '
    '    Case HBGOCountryCode.Finland
    '        'Customer identity in Handelsbanken, The test that a valid customer ID exists is done in the calling function.
    '        oFile.WriteLine sCompanyNo
    '
    '    Case Else
    '
    '    End Select
    'End If
    'oFile.WriteLine ":52A:" & sI_Swift
    'oFile.WriteLine ":30:" & Mid$(sExecutionDate, 3)
    '
    'WriteSequenceARecord = True
    'Exit Function
    '
    'errWriteSequenceARecord:
    '
    'Err.Raise Err.Number, "WriteSequenceARecord", Err.Description
    '
    'End Function
    Private Function HBGOConvertToCorrectAmount(ByRef nAmountToConvert As Double) As String
        Dim sReturnAmount As String
        Dim sAmountToConvert As String

        sAmountToConvert = Trim(Str(nAmountToConvert))
        If Len(sAmountToConvert) = 1 Then
            sAmountToConvert = "0,0" & sAmountToConvert
        ElseIf Len(sAmountToConvert) = 2 Then
            sAmountToConvert = "0," & sAmountToConvert
        Else
            sAmountToConvert = sAmountToConvert
            sAmountToConvert = Left(sAmountToConvert, Len(sAmountToConvert) - 2) & "," & Right(sAmountToConvert, 2)
        End If

        HBGOConvertToCorrectAmount = sAmountToConvert

    End Function

    Private Function HBGOAddE_Info(ByRef oPayment As vbBabel.Payment) As String
        Dim sReturnValue As String

        sReturnValue = ""
        If Not EmptyString(oPayment.E_Name) Then
            sReturnValue = sReturnValue & Left(Trim(oPayment.E_Name), 35)
        End If
        If Not EmptyString(oPayment.E_Adr1) Then
            sReturnValue = sReturnValue & vbCrLf & Left(Trim(oPayment.E_Adr1), 35)
        End If
        If Not EmptyString(oPayment.E_Adr2) Then
            sReturnValue = sReturnValue & vbCrLf & Left(Trim(oPayment.E_Adr2), 35)
        End If
        If Not EmptyString(oPayment.E_Zip) Then
            sReturnValue = sReturnValue & vbCrLf & Left(Trim(oPayment.E_Zip) & " " & Trim(oPayment.E_City), 35)
        ElseIf Not EmptyString(oPayment.E_City) Then
            sReturnValue = sReturnValue & vbCrLf & Left(Trim(oPayment.E_City), 35)
        End If

        If Left(sReturnValue, 2) = vbCrLf Then
            sReturnValue = Mid(sReturnValue, 3)
        End If

        HBGOAddE_Info = sReturnValue

    End Function
End Module
