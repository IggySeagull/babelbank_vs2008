﻿Option Strict Off
Option Explicit On
Module WriteTotalIn


    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    Dim nFileNoRecords As Double = 0
    Dim nBatchNoRecords As Double = 0
    Dim nFileNoAvdragsRecords As Double = 0
    Dim nFileNoExtraRefRecords As Double = 0
    Dim nBatchSumAmount As Double
    Dim nNoOfInsattningsposter As Double = 0
    Dim sPayNumber As String
    Dim bFileStartWritten As Boolean
    Dim sSpecial As String = ""

    Function WriteTotalInFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String, ByVal sClientNo As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double, k As Double, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim sNewOwnref As String
        Dim sTxt As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sOldAccount As String = ""
        Dim sOldDebitAccount As String = ""
        Dim sOldCurrency As String = ""
        Dim bStartspostWritten As Boolean = False
        Dim sOldDatePayment As String = ""

        Dim iFreetextCounter As Integer
        Dim sFreetextFixed As String
        Dim sFreetextVariable As String
        Dim sTemp As String = ""
        Dim sTemp2 As String = ""
        Dim sZip As String = ""

        Try

            'XokNET 27.01.2010
            nFileNoRecords = 0
            sOldAccount = "--------------------"

            ' Did not write fileheader if we run more than once without leaving BabelBank
            If Len(Dir(sFilenameOut)) > 0 Then
                bFileStartWritten = True
            Else
                bFileStartWritten = False
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                sSpecial = oBabel.Special  ' added 20.12.2018

                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If oPayment.Cancel = False And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    ' 13.02.2019 - added  oPayment.VB_ClientNo = sClientNo - same two other longer down
                                    If oPayment.I_Account = sI_Account Or oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = vbNullString
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    If Not bFileStartWritten Then
                        ' write only once!
                        sLine = WriteTotalInFileStart(sAdditionalID)
                        oFile.WriteLine(sLine)
                        sPayNumber = "0"
                        bFileStartWritten = True
                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And Not oPayment.Exported Then

                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Or oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = vbNullString
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then
                            i = i + 1

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                                    If oPayment.Cancel = False And Not oPayment.Exported Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Or oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then
                                    j = j + 1

                                    'If sOldCurrency <> oPayment.MON_TransferCurrency Or sOldAccount <> oPayment.I_Account Then
                                    If sOldCurrency <> oPayment.MON_TransferCurrency Or sOldAccount <> oPayment.I_Account Or oPayment.DATE_Payment <> sOldDatePayment Then
                                        ' Write StartPost for new account or new currency
                                        ' 07.04.2020 - OR - new date !!!! Added date-test !
                                        sOldCurrency = oPayment.MON_TransferCurrency
                                        sOldAccount = oPayment.I_Account
                                        sOldDatePayment = oPayment.DATE_Payment

                                        bStartspostWritten = False

                                        ' Write endrecord for previous debit Bankgiro
                                        If nFileNoRecords > 1 Then
                                            sLine = WriteTotalIn_EndPost()
                                            oFile.WriteLine(sLine)
                                        End If
                                        ' write new startrecord
                                        sLine = WriteTotalInStartPost(Trim(oPayment.I_Account), oPayment.MON_TransferCurrency, oPayment.DATE_Payment)
                                        oFile.WriteLine(sLine)
                                        bStartspostWritten = True
                                    End If

                                    For Each oInvoice In oPayment.Invoices
                                        sTemp = ""
                                        If Not oInvoice.Exported Then

                                            ' text
                                            For Each oFreetext In oInvoice.Freetexts
                                                sTemp = sTemp & oFreetext.Text.Trim
                                            Next
                                            ' Remove FakturaNr:, Beløp:, KID:
                                            sTemp = Replace(sTemp, LRS(60215), "")
                                            sTemp = Replace(sTemp, LRS(60214), "")
                                            sTemp = Replace(sTemp, LRS(60060), "")
                                            sTemp = Replace(sTemp, "KID:", "")

                                            ' 02.02.2020 - moved next 6 lines down
                                            '               and added left(stemp,35) to parameter tp use in 20 record
                                            '               (first time for RagnSells DK)
                                            If oInvoice.MON_InvoiceAmount >= 0 Then
                                                sLine = WriteTotalInBetalningspost(oPayment, oInvoice, Left(sTemp, 35))
                                            Else
                                                sLine = WriteTotalInAvdragspost(oPayment, oInvoice)
                                            End If
                                            oFile.WriteLine(sLine)

                                            ' 02.03.2020 - changed from left 40 to 35, as freetextpost has space for 2 x 35
                                            sLine = WriteTotalIn_FreetextPost(Left(sTemp, 35), Mid(sTemp, 36, 35))
                                            oFile.WriteLine(sLine)

                                        End If ' If Not oInvoice.Exported Then

                                        If Not EmptyString(oPayment.E_Name) Then
                                            sTemp = oPayment.E_Name
                                            If sTemp.Length > 35 Then
                                                sTemp = sTemp.Substring(0, 35)
                                            End If
                                            sLine = WriteTotalIn_NamnPost(sTemp)
                                            oFile.WriteLine(sLine)
                                        End If

                                        If Not EmptyString(oPayment.E_City) Then
                                            sLine = WriteTotalIn_AddressPost2(oPayment.E_Zip, oPayment.E_City, oPayment.E_CountryCode)
                                            oFile.WriteLine(sLine)
                                        ElseIf Not EmptyString(oPayment.E_Adr1) Or Not EmptyString(oPayment.E_Adr2) Then
                                            sLine = WriteTotalIn_AddressPost1(oPayment.E_Adr1, oPayment.E_Adr2)
                                            oFile.WriteLine(sLine)
                                        End If
                                        oInvoice.Exported = True
                                    Next oInvoice


                                    ' Utlandsbetaling
                                    If oPayment.PayType = "I" Then
                                        sLine = WriteTotalIn_UtlandsPost(oPayment)
                                        oFile.WriteLine(sLine)
                                    End If
                                    oPayment.Exported = True
                                End If
                            Next ' payment
                        End If
                    Next 'batch
                End If
            Next 'Babelfile

            If nFileNoRecords > 0 Then
                ' Write endrecord for previous debit Bankgiro
                If nFileNoRecords > 0 Then
                    sLine = WriteTotalIn_EndPost()
                    oFile.WriteLine(sLine)
                End If

                sLine = WriteTotalInFileEnd()
                oFile.WriteLine(sLine)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteTotalInFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteTotalInFile = True

    End Function
    Function WriteTotalInFileStart(ByVal sTotalInID As String) As String
        Dim sLine As String

        ' Reset file-totals:
        nFileNoRecords = 1
        nBatchSumAmount = 0
        nBatchNoRecords = 0

        sLine = "00"        ' 1-2
        sLine = sLine & PadRight(sTotalInID, 12, " ")   '3-14
        sLine = sLine & Format(Now, "yyyyMMddhhmmssffffff")  '15-34"
        sLine = sLine & "01"                            '35-36 Filnummer - Foreløpig fixed 01
        sLine = sLine & "TL1"                           '37-39 Filtyp
        sLine = sLine & "TOTALIN   "                    '40-49
        sLine = sLine & Space(31)                       '50-80

        WriteTotalInFileStart = sLine

    End Function
    Function WriteTotalInStartPost(ByVal sAccount As String, ByVal sCurrency As String, ByVal sDate As String) As String
        Dim sLine As String

        nBatchSumAmount = 0
        nBatchNoRecords = 0
        nFileNoRecords = nFileNoRecords + 1

        sLine = "10"        ' 1-2

        sLine = sLine & PadRight(sAccount, 36, "0")     '3-38 kontonummer
        sLine = sLine & sCurrency            '39-41 Valuta
        sLine = sLine & sDate                '42-49 Dato
        sLine = sLine & Space(31)            '50-80

        WriteTotalInStartPost = sLine

    End Function
    Function WriteTotalInBetalningspost(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sText1 As String) As String
        Dim sLine As String
        nNoOfInsattningsposter = nNoOfInsattningsposter + 1
        ' Add to Batch-totals:
        nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
        nBatchNoRecords = nBatchNoRecords + 1
        nFileNoRecords = nFileNoRecords + 1

        sLine = "20"        ' 1-2
        If Not EmptyString(oInvoice.Unique_Id) Then
            sLine = sLine & PadRight(Trim(oInvoice.Unique_Id), 35, " ")          '3-37 OCR, Fakturanr
        ElseIf Not EmptyString(oInvoice.InvoiceNo) Then   ' 02.03.2020 changed from Else
            sLine = sLine & PadRight(Trim(oInvoice.InvoiceNo), 35, " ")          '3-37 OCR, Fakturanr
        Else
            ' 02.03.2020 - added Else, to put in first part of freetext, as in record 40, if no Unique_ID and no InvoiceNo
            sLine = sLine & PadRight(Trim(sText1), 35, " ")
        End If

        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")  '38-52 Belopp, i øre
        sLine = sLine & PadRight(oInvoice.REF_Own, 17, " ")                     '53-69 Løpenummer
        sLine = sLine & Space(8)                                                '70-77 Mottakende bankgironr  la stå blank foreløpig
        sLine = sLine & Space(3)                                                '78-80 Typ av isetting

        WriteTotalInBetalningspost = sLine

    End Function
    Function WriteTotalInAvdragspost(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String
        nNoOfInsattningsposter = nNoOfInsattningsposter + 1
        ' Add to Batch-totals:
        nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
        nBatchNoRecords = nBatchNoRecords + 1
        nFileNoRecords = nFileNoRecords + 1

        sLine = "25"        ' 1-2
        If Not EmptyString(oInvoice.Unique_Id) Then
            'sLine = sLine & PadLeft(Trim(oInvoice.Unique_Id), 35, "0")          '3-37 OCR, Fakturanr
            ' 21.04.2020 - skal være venstrestilt, blankutfyllt
            sLine = sLine & PadRight(Trim(oInvoice.Unique_Id), 35, " ")          '3-37 OCR, Fakturanr
        Else
            'sLine = sLine & PadLeft(Trim(oInvoice.InvoiceNo), 35, "0")          '3-37 OCR, Fakturanr
            ' 21.04.2020 - skal være venstrestilt, blankutfyllt
            sLine = sLine & PadRight(Trim(oInvoice.InvoiceNo), 35, " ")          '3-37 OCR, Fakturanr
        End If

        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")  '38-52 Belopp, i øre
        sLine = sLine & PadRight(oInvoice.REF_Own, 17, " ")                     '53-69 Løpenummer
        sLine = sLine & Space(1)                                                '70    Avdragskod
        sLine = sLine & Space(8)                                                '71-78 Mottakende bankgironr  la stå blank foreløpig
        sLine = sLine & Space(2)                                                '79-80 Typ av isetting

        WriteTotalInAvdragspost = sLine

    End Function
    Function WriteTotalIn_UtlandsPost(ByVal oPayment As Payment) As String
        Dim sLine As String

        sLine = "70"        '1-2
        nFileNoRecords = nFileNoRecords + 1

        sLine = sLine & PadLeft(Str(Math.Abs(oPayment.MON_ChargesAmount)), 15, "0")  '3-17 Bankkostnad
        sLine = sLine & PadLeft(oPayment.MON_ChargesCurrency, 3, " ")                '18-20 Valuta bankkostnad
        sLine = sLine & Space(18)                               '21-38
        sLine = sLine & PadLeft(Str(Math.Abs(oPayment.MON_OriginallyPaidAmount)), 15, "0")  '39-53 Beløp fra avsenderbank
        ' before 10.12.2019 added next line, and changed for exchangerate
        sLine = sLine & PadLeft(oPayment.MON_OriginallyPaidCurrency, 3, " ")                '54-56 Valuta for belopp fra avsendarbank
        sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 8, "0")    '57-68 Vekslingskurs - 4 siste er desimaler
        sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 4)), ",", ""), 2), 4, "0")
        sLine = sLine & Space(12)                               '69-80

        WriteTotalIn_UtlandsPost = sLine

    End Function
    Function WriteTotalIn_FreetextPost(ByVal sText1 As String, ByVal sText2 As String) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "40"        '1-2
        sLine = sLine & PadRight(sText1, 35, " ") '3-37 Information from the payer to the payee.
        sLine = sLine & PadRight(sText2, 35, " ") '38-72 Information from the payer to the payee.
        sLine = sLine & Space(8) '73-80

        WriteTotalIn_FreetextPost = sLine

    End Function
    Function WriteTotalIn_NamnPost(ByVal sName As String) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "50"        '1-2
        sLine = sLine & PadRight(sName, 35, " ") '3-37 The payer’s name
        sLine = sLine & Space(35) '38-72 Extra name field. 
        sLine = sLine & Space(8) '73-80

        WriteTotalIn_NamnPost = sLine

    End Function
    Function WriteTotalIn_AddressPost1(ByVal sAddress1 As String, ByVal sAddress2 As String) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "51"        '1-2 'Transaction code
        sLine = sLine & PadRight(sAddress1, 35, " ") '3-37 Payer's address.
        sLine = sLine & PadRight(sAddress2, 35, " ") '38-72 Extra name field. 
        sLine = sLine & Space(8) '73-80

        WriteTotalIn_AddressPost1 = sLine

    End Function
    Function WriteTotalIn_AddressPost2(ByVal sZip As String, ByVal sCity As String, ByVal sCountryCode As String) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "52"        '1-2 'Transaction code
        sLine = sLine & PadRight(sZip, 9, " ") '3-11 Postnr
        sLine = sLine & PadRight(sCity, 35, " ") '12-46 Extra name field. 
        sLine = sLine & PadRight(sCountryCode, 2, " ") '47-48 Country code Payer’s country.
        sLine = sLine & Space(32) '49-80

        WriteTotalIn_AddressPost2 = sLine

    End Function
    Function WriteTotalIn_EndPost() As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "90"        ' 1-2
        sLine = sLine & PadLeft(Str(nBatchNoRecords), 8, "0")        '3-10  Antall betalningsposter i filen
        sLine = sLine & PadLeft(Str(nBatchSumAmount), 17, "0") '11-27 Total belopp batch
        sLine = sLine & Format(Date.Today, "yyyyMMdd") & "001"  '28-38  Ref

        'pad to 80 with 0
        sLine = PadLine(sLine, 80, " ")

        nFileNoAvdragsRecords = 0
        nBatchSumAmount = 0
        WriteTotalIn_EndPost = sLine

    End Function
    Function WriteTotalInFileEnd() As String
        Dim sLine As String
        nBatchNoRecords = nBatchNoRecords + 1
        nFileNoRecords = nFileNoRecords + 1

        sLine = "99"        ' 1-2
        sLine = sLine & PadLeft(Str(nFileNoRecords), 15, "0")        '3-17  Antall betalningsposter i filen

        'pad to 80 with 0
        sLine = PadLine(sLine, 80, " ")     ' 18-80
        WriteTotalInFileEnd = sLine

    End Function

End Module