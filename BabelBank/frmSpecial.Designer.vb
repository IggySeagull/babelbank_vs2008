<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSpecial
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Check1 As System.Windows.Forms.CheckBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmbCombobox As System.Windows.Forms.ComboBox
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
    Public WithEvents lblComboBox As System.Windows.Forms.Label
	Public WithEvents lblExplaination As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Check1 = New System.Windows.Forms.CheckBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmbCombobox = New System.Windows.Forms.ComboBox
        Me.lbl2 = New System.Windows.Forms.Label
        Me.lbl1 = New System.Windows.Forms.Label
        Me.lblComboBox = New System.Windows.Forms.Label
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Check1
        '
        Me.Check1.BackColor = System.Drawing.SystemColors.Control
        Me.Check1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Check1.Enabled = False
        Me.Check1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Check1.Location = New System.Drawing.Point(392, 168)
        Me.Check1.Name = "Check1"
        Me.Check1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Check1.Size = New System.Drawing.Size(169, 25)
        Me.Check1.TabIndex = 9
        Me.Check1.Text = "Check1"
        Me.Check1.UseVisualStyleBackColor = False
        Me.Check1.Visible = False
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.Enabled = False
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(176, 192)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(193, 19)
        Me.Text2.TabIndex = 8
        Me.Text2.Text = "Text2"
        Me.Text2.Visible = False
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.Enabled = False
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(184, 160)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(177, 19)
        Me.Text1.TabIndex = 7
        Me.Text1.Text = "Text1"
        Me.Text1.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(424, 248)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "55001 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(512, 248)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 3
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmbCombobox
        '
        Me.cmbCombobox.BackColor = System.Drawing.SystemColors.Window
        Me.cmbCombobox.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbCombobox.Enabled = False
        Me.cmbCombobox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbCombobox.Location = New System.Drawing.Point(328, 128)
        Me.cmbCombobox.Name = "cmbCombobox"
        Me.cmbCombobox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbCombobox.Size = New System.Drawing.Size(145, 21)
        Me.cmbCombobox.TabIndex = 1
        Me.cmbCombobox.Visible = False
        '
        'lbl2
        '
        Me.lbl2.BackColor = System.Drawing.SystemColors.Control
        Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbl2.Enabled = False
        Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl2.Location = New System.Drawing.Point(24, 168)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl2.Size = New System.Drawing.Size(113, 25)
        Me.lbl2.TabIndex = 6
        Me.lbl2.Text = "Label2"
        Me.lbl2.Visible = False
        '
        'lbl1
        '
        Me.lbl1.BackColor = System.Drawing.SystemColors.Control
        Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbl1.Enabled = False
        Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl1.Location = New System.Drawing.Point(16, 128)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl1.Size = New System.Drawing.Size(105, 17)
        Me.lbl1.TabIndex = 5
        Me.lbl1.Text = "Label1"
        Me.lbl1.Visible = False
        '
        'lblComboBox
        '
        Me.lblComboBox.BackColor = System.Drawing.SystemColors.Control
        Me.lblComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblComboBox.Enabled = False
        Me.lblComboBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblComboBox.Location = New System.Drawing.Point(128, 128)
        Me.lblComboBox.Name = "lblComboBox"
        Me.lblComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblComboBox.Size = New System.Drawing.Size(169, 17)
        Me.lblComboBox.TabIndex = 2
        Me.lblComboBox.Text = "Label1"
        Me.lblComboBox.Visible = False
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(128, 48)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(433, 49)
        Me.lblExplaination.TabIndex = 0
        Me.lblExplaination.Text = "Explain the use of the form here"
        '
        'frmSpecial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(592, 288)
        Me.Controls.Add(Me.Check1)
        Me.Controls.Add(Me.Text2)
        Me.Controls.Add(Me.Text1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmbCombobox)
        Me.Controls.Add(Me.lbl2)
        Me.Controls.Add(Me.lbl1)
        Me.Controls.Add(Me.lblComboBox)
        Me.Controls.Add(Me.lblExplaination)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmSpecial"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
