<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCorrectOther
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmbStateBank_Code As System.Windows.Forms.ComboBox
    'Public WithEvents DTERA_Date As AxMSComCtl2.AxDTPicker
	Public WithEvents chkUrgent As System.Windows.Forms.CheckBox
	Public WithEvents txtE_Countrycode As System.Windows.Forms.TextBox
	Public WithEvents txtERA_ContractRef As System.Windows.Forms.TextBox
	Public WithEvents txtERA_DealMadeWith As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtStateBank_Text As System.Windows.Forms.TextBox
	Public WithEvents txtERA_ExchangeRateAgreed As System.Windows.Forms.TextBox
	Public WithEvents _imgExclamation_9 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_9 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_8 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_8 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_5 As System.Windows.Forms.PictureBox
	Public WithEvents lblStateBank_COde As System.Windows.Forms.Label
    Public WithEvents lblE_CountryCode As System.Windows.Forms.Label
    Public WithEvents lblBenBankInfo As System.Windows.Forms.Label
	Public WithEvents lblERA_ContractRef As System.Windows.Forms.Label
	Public WithEvents lblERA_DealMadeWith As System.Windows.Forms.Label
	Public WithEvents lblERA_Date As System.Windows.Forms.Label
	Public WithEvents lblErrMessage As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblStateBank_Text As System.Windows.Forms.Label
	Public WithEvents lblERA_ExchangeRateAgreed As System.Windows.Forms.Label
	Public WithEvents _imgExclamation_3 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_2 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_6 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_7 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_4 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_2 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_3 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_4 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_5 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_6 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_7 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_1 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_1 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents Image2 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents imgExclamation As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCorrectOther))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmbStateBank_Code = New System.Windows.Forms.ComboBox
        Me.chkUrgent = New System.Windows.Forms.CheckBox
        Me.txtE_Countrycode = New System.Windows.Forms.TextBox
        Me.txtERA_ContractRef = New System.Windows.Forms.TextBox
        Me.txtERA_DealMadeWith = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtStateBank_Text = New System.Windows.Forms.TextBox
        Me.txtERA_ExchangeRateAgreed = New System.Windows.Forms.TextBox
        Me._imgExclamation_9 = New System.Windows.Forms.PictureBox
        Me._Image2_9 = New System.Windows.Forms.PictureBox
        Me._Image2_8 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_8 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_5 = New System.Windows.Forms.PictureBox
        Me.lblStateBank_COde = New System.Windows.Forms.Label
        Me.lblE_CountryCode = New System.Windows.Forms.Label
        Me.lblBenBankInfo = New System.Windows.Forms.Label
        Me.lblERA_ContractRef = New System.Windows.Forms.Label
        Me.lblERA_DealMadeWith = New System.Windows.Forms.Label
        Me.lblERA_Date = New System.Windows.Forms.Label
        Me.lblErrMessage = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblStateBank_Text = New System.Windows.Forms.Label
        Me.lblERA_ExchangeRateAgreed = New System.Windows.Forms.Label
        Me._imgExclamation_3 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_2 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_6 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_7 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_4 = New System.Windows.Forms.PictureBox
        Me._Image2_2 = New System.Windows.Forms.PictureBox
        Me._Image2_3 = New System.Windows.Forms.PictureBox
        Me._Image2_4 = New System.Windows.Forms.PictureBox
        Me._Image2_5 = New System.Windows.Forms.PictureBox
        Me._Image2_6 = New System.Windows.Forms.PictureBox
        Me._Image2_7 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_1 = New System.Windows.Forms.PictureBox
        Me._Image2_1 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.Image2 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.imgExclamation = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.txtDate_Contract = New System.Windows.Forms.TextBox
        CType(Me._imgExclamation_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgExclamation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbStateBank_Code
        '
        Me.cmbStateBank_Code.BackColor = System.Drawing.SystemColors.Window
        Me.cmbStateBank_Code.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbStateBank_Code.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStateBank_Code.Enabled = False
        Me.cmbStateBank_Code.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbStateBank_Code.Location = New System.Drawing.Point(148, 146)
        Me.cmbStateBank_Code.Name = "cmbStateBank_Code"
        Me.cmbStateBank_Code.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbStateBank_Code.Size = New System.Drawing.Size(233, 21)
        Me.cmbStateBank_Code.TabIndex = 17
        '
        'chkUrgent
        '
        Me.chkUrgent.BackColor = System.Drawing.SystemColors.Control
        Me.chkUrgent.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUrgent.Enabled = False
        Me.chkUrgent.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUrgent.Location = New System.Drawing.Point(148, 214)
        Me.chkUrgent.Name = "chkUrgent"
        Me.chkUrgent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUrgent.Size = New System.Drawing.Size(207, 21)
        Me.chkUrgent.TabIndex = 15
        Me.chkUrgent.TabStop = False
        Me.chkUrgent.Text = "60136 - Urgent"
        Me.chkUrgent.UseVisualStyleBackColor = False
        '
        'txtE_Countrycode
        '
        Me.txtE_Countrycode.AcceptsReturn = True
        Me.txtE_Countrycode.BackColor = System.Drawing.SystemColors.Window
        Me.txtE_Countrycode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtE_Countrycode.Enabled = False
        Me.txtE_Countrycode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtE_Countrycode.Location = New System.Drawing.Point(148, 242)
        Me.txtE_Countrycode.MaxLength = 0
        Me.txtE_Countrycode.Name = "txtE_Countrycode"
        Me.txtE_Countrycode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtE_Countrycode.Size = New System.Drawing.Size(62, 20)
        Me.txtE_Countrycode.TabIndex = 4
        Me.txtE_Countrycode.Text = "<Countrycode"
        '
        'txtERA_ContractRef
        '
        Me.txtERA_ContractRef.AcceptsReturn = True
        Me.txtERA_ContractRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtERA_ContractRef.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtERA_ContractRef.Enabled = False
        Me.txtERA_ContractRef.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtERA_ContractRef.Location = New System.Drawing.Point(499, 101)
        Me.txtERA_ContractRef.MaxLength = 0
        Me.txtERA_ContractRef.Name = "txtERA_ContractRef"
        Me.txtERA_ContractRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtERA_ContractRef.Size = New System.Drawing.Size(188, 20)
        Me.txtERA_ContractRef.TabIndex = 2
        Me.txtERA_ContractRef.Text = "<ERA_ContracctRef>"
        '
        'txtERA_DealMadeWith
        '
        Me.txtERA_DealMadeWith.AcceptsReturn = True
        Me.txtERA_DealMadeWith.BackColor = System.Drawing.SystemColors.Window
        Me.txtERA_DealMadeWith.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtERA_DealMadeWith.Enabled = False
        Me.txtERA_DealMadeWith.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtERA_DealMadeWith.Location = New System.Drawing.Point(499, 78)
        Me.txtERA_DealMadeWith.MaxLength = 0
        Me.txtERA_DealMadeWith.Name = "txtERA_DealMadeWith"
        Me.txtERA_DealMadeWith.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtERA_DealMadeWith.Size = New System.Drawing.Size(188, 20)
        Me.txtERA_DealMadeWith.TabIndex = 0
        Me.txtERA_DealMadeWith.Text = "<Deal made With>"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(628, 426)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.TabStop = False
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'txtStateBank_Text
        '
        Me.txtStateBank_Text.AcceptsReturn = True
        Me.txtStateBank_Text.BackColor = System.Drawing.SystemColors.Window
        Me.txtStateBank_Text.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtStateBank_Text.Enabled = False
        Me.txtStateBank_Text.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtStateBank_Text.Location = New System.Drawing.Point(148, 175)
        Me.txtStateBank_Text.MaxLength = 0
        Me.txtStateBank_Text.Name = "txtStateBank_Text"
        Me.txtStateBank_Text.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtStateBank_Text.Size = New System.Drawing.Size(553, 20)
        Me.txtStateBank_Text.TabIndex = 3
        Me.txtStateBank_Text.Text = "<StateBank Text>"
        '
        'txtERA_ExchangeRateAgreed
        '
        Me.txtERA_ExchangeRateAgreed.AcceptsReturn = True
        Me.txtERA_ExchangeRateAgreed.BackColor = System.Drawing.SystemColors.Window
        Me.txtERA_ExchangeRateAgreed.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtERA_ExchangeRateAgreed.Enabled = False
        Me.txtERA_ExchangeRateAgreed.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtERA_ExchangeRateAgreed.Location = New System.Drawing.Point(148, 100)
        Me.txtERA_ExchangeRateAgreed.MaxLength = 0
        Me.txtERA_ExchangeRateAgreed.Name = "txtERA_ExchangeRateAgreed"
        Me.txtERA_ExchangeRateAgreed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtERA_ExchangeRateAgreed.Size = New System.Drawing.Size(201, 20)
        Me.txtERA_ExchangeRateAgreed.TabIndex = 1
        Me.txtERA_ExchangeRateAgreed.Text = "<ERA_ExchangeRateAgreed>"
        '
        '_imgExclamation_9
        '
        Me._imgExclamation_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_9.Image = CType(resources.GetObject("_imgExclamation_9.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_9, CType(9, Short))
        Me._imgExclamation_9.Location = New System.Drawing.Point(8, 368)
        Me._imgExclamation_9.Name = "_imgExclamation_9"
        Me._imgExclamation_9.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_9.TabIndex = 18
        Me._imgExclamation_9.TabStop = False
        Me._imgExclamation_9.Visible = False
        '
        '_Image2_9
        '
        Me._Image2_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_9.Image = CType(resources.GetObject("_Image2_9.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_9, CType(9, Short))
        Me._Image2_9.Location = New System.Drawing.Point(7, 369)
        Me._Image2_9.Name = "_Image2_9"
        Me._Image2_9.Size = New System.Drawing.Size(18, 18)
        Me._Image2_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_9.TabIndex = 19
        Me._Image2_9.TabStop = False
        Me._Image2_9.Visible = False
        '
        '_Image2_8
        '
        Me._Image2_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_8.Image = CType(resources.GetObject("_Image2_8.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_8, CType(8, Short))
        Me._Image2_8.Location = New System.Drawing.Point(127, 216)
        Me._Image2_8.Name = "_Image2_8"
        Me._Image2_8.Size = New System.Drawing.Size(18, 18)
        Me._Image2_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_8.TabIndex = 20
        Me._Image2_8.TabStop = False
        Me._Image2_8.Visible = False
        '
        '_imgExclamation_8
        '
        Me._imgExclamation_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_8.Image = CType(resources.GetObject("_imgExclamation_8.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_8, CType(8, Short))
        Me._imgExclamation_8.Location = New System.Drawing.Point(128, 217)
        Me._imgExclamation_8.Name = "_imgExclamation_8"
        Me._imgExclamation_8.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_8.TabIndex = 21
        Me._imgExclamation_8.TabStop = False
        Me._imgExclamation_8.Visible = False
        '
        '_imgExclamation_5
        '
        Me._imgExclamation_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_5.Image = CType(resources.GetObject("_imgExclamation_5.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_5, CType(5, Short))
        Me._imgExclamation_5.Location = New System.Drawing.Point(127, 147)
        Me._imgExclamation_5.Name = "_imgExclamation_5"
        Me._imgExclamation_5.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_5.TabIndex = 22
        Me._imgExclamation_5.TabStop = False
        Me._imgExclamation_5.Visible = False
        '
        'lblStateBank_COde
        '
        Me.lblStateBank_COde.BackColor = System.Drawing.SystemColors.Control
        Me.lblStateBank_COde.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStateBank_COde.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStateBank_COde.Location = New System.Drawing.Point(8, 148)
        Me.lblStateBank_COde.Name = "lblStateBank_COde"
        Me.lblStateBank_COde.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStateBank_COde.Size = New System.Drawing.Size(118, 16)
        Me.lblStateBank_COde.TabIndex = 14
        Me.lblStateBank_COde.Text = "60134 - NationalBank Code"
        '
        'lblE_CountryCode
        '
        Me.lblE_CountryCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblE_CountryCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblE_CountryCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblE_CountryCode.Location = New System.Drawing.Point(8, 243)
        Me.lblE_CountryCode.Name = "lblE_CountryCode"
        Me.lblE_CountryCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblE_CountryCode.Size = New System.Drawing.Size(117, 16)
        Me.lblE_CountryCode.TabIndex = 13
        Me.lblE_CountryCode.Text = "60137 - Receivers countrycode"
        '
        'lblBenBankInfo
        '
        Me.lblBenBankInfo.BackColor = System.Drawing.SystemColors.Control
        Me.lblBenBankInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBenBankInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBenBankInfo.Location = New System.Drawing.Point(8, 53)
        Me.lblBenBankInfo.Name = "lblBenBankInfo"
        Me.lblBenBankInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBenBankInfo.Size = New System.Drawing.Size(285, 18)
        Me.lblBenBankInfo.TabIndex = 12
        Me.lblBenBankInfo.Text = "60129 -Exchange Rate Contract"
        '
        'lblERA_ContractRef
        '
        Me.lblERA_ContractRef.BackColor = System.Drawing.SystemColors.Control
        Me.lblERA_ContractRef.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblERA_ContractRef.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblERA_ContractRef.Location = New System.Drawing.Point(360, 103)
        Me.lblERA_ContractRef.Name = "lblERA_ContractRef"
        Me.lblERA_ContractRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblERA_ContractRef.Size = New System.Drawing.Size(120, 16)
        Me.lblERA_ContractRef.TabIndex = 11
        Me.lblERA_ContractRef.Text = "60133 - Contract Ref."
        '
        'lblERA_DealMadeWith
        '
        Me.lblERA_DealMadeWith.BackColor = System.Drawing.SystemColors.Control
        Me.lblERA_DealMadeWith.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblERA_DealMadeWith.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblERA_DealMadeWith.Location = New System.Drawing.Point(360, 78)
        Me.lblERA_DealMadeWith.Name = "lblERA_DealMadeWith"
        Me.lblERA_DealMadeWith.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblERA_DealMadeWith.Size = New System.Drawing.Size(116, 16)
        Me.lblERA_DealMadeWith.TabIndex = 10
        Me.lblERA_DealMadeWith.Text = "60131 - Deal Made With"
        '
        'lblERA_Date
        '
        Me.lblERA_Date.BackColor = System.Drawing.SystemColors.Control
        Me.lblERA_Date.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblERA_Date.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblERA_Date.Location = New System.Drawing.Point(8, 79)
        Me.lblERA_Date.Name = "lblERA_Date"
        Me.lblERA_Date.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblERA_Date.Size = New System.Drawing.Size(117, 16)
        Me.lblERA_Date.TabIndex = 9
        Me.lblERA_Date.Text = "60130 - Contract Date"
        '
        'lblErrMessage
        '
        Me.lblErrMessage.BackColor = System.Drawing.SystemColors.Window
        Me.lblErrMessage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblErrMessage.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblErrMessage.Location = New System.Drawing.Point(31, 369)
        Me.lblErrMessage.Name = "lblErrMessage"
        Me.lblErrMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblErrMessage.Size = New System.Drawing.Size(677, 47)
        Me.lblErrMessage.TabIndex = 8
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(669, 328)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 27)
        Me._Image1_0.TabIndex = 23
        Me._Image1_0.TabStop = False
        '
        'lblStateBank_Text
        '
        Me.lblStateBank_Text.BackColor = System.Drawing.SystemColors.Control
        Me.lblStateBank_Text.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStateBank_Text.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStateBank_Text.Location = New System.Drawing.Point(8, 177)
        Me.lblStateBank_Text.Name = "lblStateBank_Text"
        Me.lblStateBank_Text.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStateBank_Text.Size = New System.Drawing.Size(120, 16)
        Me.lblStateBank_Text.TabIndex = 7
        Me.lblStateBank_Text.Text = "60135 - National Bank Text"
        '
        'lblERA_ExchangeRateAgreed
        '
        Me.lblERA_ExchangeRateAgreed.BackColor = System.Drawing.SystemColors.Control
        Me.lblERA_ExchangeRateAgreed.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblERA_ExchangeRateAgreed.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblERA_ExchangeRateAgreed.Location = New System.Drawing.Point(8, 104)
        Me.lblERA_ExchangeRateAgreed.Name = "lblERA_ExchangeRateAgreed"
        Me.lblERA_ExchangeRateAgreed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblERA_ExchangeRateAgreed.Size = New System.Drawing.Size(118, 16)
        Me.lblERA_ExchangeRateAgreed.TabIndex = 6
        Me.lblERA_ExchangeRateAgreed.Text = "60132 - Exchange Rate"
        '
        '_imgExclamation_3
        '
        Me._imgExclamation_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_3.Image = CType(resources.GetObject("_imgExclamation_3.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_3, CType(3, Short))
        Me._imgExclamation_3.Location = New System.Drawing.Point(127, 101)
        Me._imgExclamation_3.Name = "_imgExclamation_3"
        Me._imgExclamation_3.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_3.TabIndex = 24
        Me._imgExclamation_3.TabStop = False
        Me._imgExclamation_3.Visible = False
        '
        '_imgExclamation_2
        '
        Me._imgExclamation_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_2.Image = CType(resources.GetObject("_imgExclamation_2.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_2, CType(2, Short))
        Me._imgExclamation_2.Location = New System.Drawing.Point(479, 79)
        Me._imgExclamation_2.Name = "_imgExclamation_2"
        Me._imgExclamation_2.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_2.TabIndex = 25
        Me._imgExclamation_2.TabStop = False
        Me._imgExclamation_2.Visible = False
        '
        '_imgExclamation_6
        '
        Me._imgExclamation_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_6.Image = CType(resources.GetObject("_imgExclamation_6.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_6, CType(6, Short))
        Me._imgExclamation_6.Location = New System.Drawing.Point(126, 175)
        Me._imgExclamation_6.Name = "_imgExclamation_6"
        Me._imgExclamation_6.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_6.TabIndex = 26
        Me._imgExclamation_6.TabStop = False
        Me._imgExclamation_6.Visible = False
        '
        '_imgExclamation_7
        '
        Me._imgExclamation_7.BackColor = System.Drawing.Color.Transparent
        Me._imgExclamation_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_7.Image = CType(resources.GetObject("_imgExclamation_7.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_7, CType(7, Short))
        Me._imgExclamation_7.Location = New System.Drawing.Point(127, 242)
        Me._imgExclamation_7.Name = "_imgExclamation_7"
        Me._imgExclamation_7.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_7.TabIndex = 27
        Me._imgExclamation_7.TabStop = False
        Me._imgExclamation_7.Visible = False
        '
        '_imgExclamation_4
        '
        Me._imgExclamation_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_4.Image = CType(resources.GetObject("_imgExclamation_4.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_4, CType(4, Short))
        Me._imgExclamation_4.Location = New System.Drawing.Point(480, 100)
        Me._imgExclamation_4.Name = "_imgExclamation_4"
        Me._imgExclamation_4.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_4.TabIndex = 28
        Me._imgExclamation_4.TabStop = False
        Me._imgExclamation_4.Visible = False
        '
        '_Image2_2
        '
        Me._Image2_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_2.Image = CType(resources.GetObject("_Image2_2.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_2, CType(2, Short))
        Me._Image2_2.Location = New System.Drawing.Point(480, 77)
        Me._Image2_2.Name = "_Image2_2"
        Me._Image2_2.Size = New System.Drawing.Size(18, 18)
        Me._Image2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_2.TabIndex = 29
        Me._Image2_2.TabStop = False
        Me._Image2_2.Visible = False
        '
        '_Image2_3
        '
        Me._Image2_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_3.Image = CType(resources.GetObject("_Image2_3.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_3, CType(3, Short))
        Me._Image2_3.Location = New System.Drawing.Point(125, 100)
        Me._Image2_3.Name = "_Image2_3"
        Me._Image2_3.Size = New System.Drawing.Size(18, 18)
        Me._Image2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_3.TabIndex = 30
        Me._Image2_3.TabStop = False
        Me._Image2_3.Visible = False
        '
        '_Image2_4
        '
        Me._Image2_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_4.Image = CType(resources.GetObject("_Image2_4.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_4, CType(4, Short))
        Me._Image2_4.Location = New System.Drawing.Point(480, 100)
        Me._Image2_4.Name = "_Image2_4"
        Me._Image2_4.Size = New System.Drawing.Size(18, 18)
        Me._Image2_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_4.TabIndex = 31
        Me._Image2_4.TabStop = False
        Me._Image2_4.Visible = False
        '
        '_Image2_5
        '
        Me._Image2_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_5.Image = CType(resources.GetObject("_Image2_5.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_5, CType(5, Short))
        Me._Image2_5.Location = New System.Drawing.Point(127, 146)
        Me._Image2_5.Name = "_Image2_5"
        Me._Image2_5.Size = New System.Drawing.Size(18, 18)
        Me._Image2_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_5.TabIndex = 32
        Me._Image2_5.TabStop = False
        Me._Image2_5.Visible = False
        '
        '_Image2_6
        '
        Me._Image2_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_6.Image = CType(resources.GetObject("_Image2_6.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_6, CType(6, Short))
        Me._Image2_6.Location = New System.Drawing.Point(126, 177)
        Me._Image2_6.Name = "_Image2_6"
        Me._Image2_6.Size = New System.Drawing.Size(18, 18)
        Me._Image2_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_6.TabIndex = 33
        Me._Image2_6.TabStop = False
        Me._Image2_6.Visible = False
        '
        '_Image2_7
        '
        Me._Image2_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_7.Image = CType(resources.GetObject("_Image2_7.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_7, CType(7, Short))
        Me._Image2_7.Location = New System.Drawing.Point(127, 240)
        Me._Image2_7.Name = "_Image2_7"
        Me._Image2_7.Size = New System.Drawing.Size(18, 18)
        Me._Image2_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_7.TabIndex = 34
        Me._Image2_7.TabStop = False
        Me._Image2_7.Visible = False
        '
        '_imgExclamation_1
        '
        Me._imgExclamation_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_1.Image = CType(resources.GetObject("_imgExclamation_1.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_1, CType(1, Short))
        Me._imgExclamation_1.Location = New System.Drawing.Point(126, 78)
        Me._imgExclamation_1.Name = "_imgExclamation_1"
        Me._imgExclamation_1.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_1.TabIndex = 35
        Me._imgExclamation_1.TabStop = False
        Me._imgExclamation_1.Visible = False
        '
        '_Image2_1
        '
        Me._Image2_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_1.Image = CType(resources.GetObject("_Image2_1.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_1, CType(1, Short))
        Me._Image2_1.Location = New System.Drawing.Point(127, 77)
        Me._Image2_1.Name = "_Image2_1"
        Me._Image2_1.Size = New System.Drawing.Size(18, 18)
        Me._Image2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_1.TabIndex = 36
        Me._Image2_1.TabStop = False
        Me._Image2_1.Visible = False
        '
        'txtDate_Contract
        '
        Me.txtDate_Contract.AcceptsReturn = True
        Me.txtDate_Contract.BackColor = System.Drawing.SystemColors.Window
        Me.txtDate_Contract.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDate_Contract.Enabled = False
        Me.txtDate_Contract.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDate_Contract.Location = New System.Drawing.Point(148, 79)
        Me.txtDate_Contract.MaxLength = 0
        Me.txtDate_Contract.Name = "txtDate_Contract"
        Me.txtDate_Contract.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDate_Contract.Size = New System.Drawing.Size(87, 20)
        Me.txtDate_Contract.TabIndex = 86
        '
        'frmCorrectOther
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(715, 461)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtDate_Contract)
        Me.Controls.Add(Me.cmbStateBank_Code)
        Me.Controls.Add(Me.chkUrgent)
        Me.Controls.Add(Me.txtE_Countrycode)
        Me.Controls.Add(Me.txtERA_ContractRef)
        Me.Controls.Add(Me.txtERA_DealMadeWith)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtStateBank_Text)
        Me.Controls.Add(Me.txtERA_ExchangeRateAgreed)
        Me.Controls.Add(Me._imgExclamation_9)
        Me.Controls.Add(Me._Image2_9)
        Me.Controls.Add(Me._Image2_8)
        Me.Controls.Add(Me._imgExclamation_8)
        Me.Controls.Add(Me._imgExclamation_5)
        Me.Controls.Add(Me.lblStateBank_COde)
        Me.Controls.Add(Me.lblE_CountryCode)
        Me.Controls.Add(Me.lblBenBankInfo)
        Me.Controls.Add(Me.lblERA_ContractRef)
        Me.Controls.Add(Me.lblERA_DealMadeWith)
        Me.Controls.Add(Me.lblERA_Date)
        Me.Controls.Add(Me.lblErrMessage)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblStateBank_Text)
        Me.Controls.Add(Me.lblERA_ExchangeRateAgreed)
        Me.Controls.Add(Me._imgExclamation_3)
        Me.Controls.Add(Me._imgExclamation_2)
        Me.Controls.Add(Me._imgExclamation_6)
        Me.Controls.Add(Me._imgExclamation_7)
        Me.Controls.Add(Me._imgExclamation_4)
        Me.Controls.Add(Me._Image2_2)
        Me.Controls.Add(Me._Image2_3)
        Me.Controls.Add(Me._Image2_4)
        Me.Controls.Add(Me._Image2_5)
        Me.Controls.Add(Me._Image2_6)
        Me.Controls.Add(Me._Image2_7)
        Me.Controls.Add(Me._imgExclamation_1)
        Me.Controls.Add(Me._Image2_1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmCorrectOther"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "60094 - Other Information"
        CType(Me._imgExclamation_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgExclamation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtDate_Contract As System.Windows.Forms.TextBox
#End Region
End Class
