<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_003_PaymentInvoice
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_003_PaymentInvoice))
        Dim OleDBDataSource1 As DataDynamics.ActiveReports.DataSources.OleDBDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtUniqueID = New DataDynamics.ActiveReports.TextBox
        Me.SubRptFreetext = New DataDynamics.ActiveReports.SubReport
        Me.txtInvoiceAmount = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.linePageHeader = New DataDynamics.ActiveReports.Line
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.txtTotalNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblBatchfooterNoofPayments = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterNoofPayments = New DataDynamics.ActiveReports.TextBox
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        Me.grPaymentReceiverHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblReceiver = New DataDynamics.ActiveReports.Label
        Me.txtI_Adr1 = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Name = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Adr2 = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Zip = New DataDynamics.ActiveReports.TextBox
        Me.txtI_City = New DataDynamics.ActiveReports.TextBox
        Me.lblPayor = New DataDynamics.ActiveReports.Label
        Me.txtI_Adr3 = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentReceiverFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grPaymentHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtE_Name = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferredAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr1 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr2 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr3 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Zip = New DataDynamics.ActiveReports.TextBox
        Me.txtE_City = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank2 = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank1 = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Value = New DataDynamics.ActiveReports.TextBox
        Me.txtVoucherNo = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferCurrency = New DataDynamics.ActiveReports.TextBox
        Me.lblE_Account = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank2 = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank1 = New DataDynamics.ActiveReports.Label
        Me.lblDATE_Value = New DataDynamics.ActiveReports.Label
        Me.lblVoucherNo = New DataDynamics.ActiveReports.Label
        Me.lblREF_Own = New DataDynamics.ActiveReports.Label
        Me.txtREF_Own = New DataDynamics.ActiveReports.TextBox
        Me.lblBIC = New DataDynamics.ActiveReports.Label
        Me.txtBIC = New DataDynamics.ActiveReports.TextBox
        Me.lblBANK_Branch = New DataDynamics.ActiveReports.Label
        Me.txtBANK_Branch = New DataDynamics.ActiveReports.TextBox
        Me.txtPayI_Account = New DataDynamics.ActiveReports.TextBox
        Me.lblPayI_Account = New DataDynamics.ActiveReports.Label
        Me.txtE_CountryCode = New DataDynamics.ActiveReports.TextBox
        Me.lblBB_Id = New DataDynamics.ActiveReports.Label
        Me.txtBabelBank_ID = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grPaymentInternationalHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapegrPaymentInternationalHeader = New DataDynamics.ActiveReports.Shape
        Me.lblMON_OriginallyPaidAmount = New DataDynamics.ActiveReports.Label
        Me.lblMON_InvoiceAmount = New DataDynamics.ActiveReports.Label
        Me.lblMON_AccountAmount = New DataDynamics.ActiveReports.Label
        Me.lblExchangeRate = New DataDynamics.ActiveReports.Label
        Me.lblChargesAbroad = New DataDynamics.ActiveReports.Label
        Me.lblChargesDomestic = New DataDynamics.ActiveReports.Label
        Me.txtMON_OriginallyPaidAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_InvoiceAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_AccountAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtChargesAbroad = New DataDynamics.ActiveReports.TextBox
        Me.txtChargesDomestic = New DataDynamics.ActiveReports.TextBox
        Me.txtExchangeRate = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_OriginallyPaidCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_InvoiceCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_AccountCurrency = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentInternationalFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grFreetextHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtErrorText = New DataDynamics.ActiveReports.TextBox
        Me.grFreetextFooter = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.txtUniqueID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblReceiver, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Adr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Adr2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Zip, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_City, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPayor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Adr3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Zip, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_City, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVoucherNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblVoucherNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Own, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Own, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBIC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBIC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBANK_Branch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBANK_Branch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPayI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPayI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_CountryCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBB_Id, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBabelBank_ID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMON_AccountAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblExchangeRate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblChargesAbroad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblChargesDomestic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_AccountAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChargesAbroad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChargesDomestic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExchangeRate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_OriginallyPaidCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_InvoiceCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_AccountCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtErrorText, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtUniqueID, Me.SubRptFreetext, Me.txtInvoiceAmount})
        Me.Detail.Height = 0.2604167!
        Me.Detail.Name = "Detail"
        '
        'txtUniqueID
        '
        Me.txtUniqueID.CanShrink = True
        Me.txtUniqueID.Height = 0.15!
        Me.txtUniqueID.Left = 3.708!
        Me.txtUniqueID.Name = "txtUniqueID"
        Me.txtUniqueID.Style = "font-size: 8.25pt"
        Me.txtUniqueID.Text = "txtUniqueID"
        Me.txtUniqueID.Top = 0.0!
        Me.txtUniqueID.Width = 1.45!
        '
        'SubRptFreetext
        '
        Me.SubRptFreetext.CloseBorder = False
        Me.SubRptFreetext.Height = 0.15!
        Me.SubRptFreetext.Left = 0.0!
        Me.SubRptFreetext.Name = "SubRptFreetext"
        Me.SubRptFreetext.Report = Nothing
        Me.SubRptFreetext.ReportName = "SubRptFreetext"
        Me.SubRptFreetext.Top = 0.0!
        Me.SubRptFreetext.Width = 3.7!
        '
        'txtInvoiceAmount
        '
        Me.txtInvoiceAmount.CanShrink = True
        Me.txtInvoiceAmount.DataField = "InvoiceAmount"
        Me.txtInvoiceAmount.Height = 0.15!
        Me.txtInvoiceAmount.Left = 5.134!
        Me.txtInvoiceAmount.Name = "txtInvoiceAmount"
        Me.txtInvoiceAmount.OutputFormat = resources.GetString("txtInvoiceAmount.OutputFormat")
        Me.txtInvoiceAmount.Style = "font-size: 8.25pt; text-align: right"
        Me.txtInvoiceAmount.Text = "txtInvoiceAmount"
        Me.txtInvoiceAmount.Top = 0.0!
        Me.txtInvoiceAmount.Width = 0.8959999!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.rptInfoPageHeaderDate, Me.linePageHeader})
        Me.PageHeader1.Height = 0.4479167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 0.858!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Payments with invoicedetails"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 4.392!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'linePageHeader
        '
        Me.linePageHeader.Height = 0.0!
        Me.linePageHeader.Left = 0.0!
        Me.linePageHeader.LineWeight = 1.0!
        Me.linePageHeader.Name = "linePageHeader"
        Me.linePageHeader.Top = 0.375!
        Me.linePageHeader.Width = 6.5!
        Me.linePageHeader.X1 = 0.0!
        Me.linePageHeader.X2 = 6.5!
        Me.linePageHeader.Y1 = 0.375!
        Me.linePageHeader.Y2 = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.CanShrink = True
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBabelFileFooter2, Me.LineBabelFileFooter1, Me.txtTotalNoOfPayments, Me.lblTotalNoOfPayments, Me.txtTotalAmount, Me.lblTotalAmount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 3.7!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 2.8!
        Me.LineBabelFileFooter2.X1 = 3.7!
        Me.LineBabelFileFooter2.X2 = 6.5!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 3.7!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 2.8!
        Me.LineBabelFileFooter1.X1 = 3.7!
        Me.LineBabelFileFooter1.X2 = 6.5!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'txtTotalNoOfPayments
        '
        Me.txtTotalNoOfPayments.CanGrow = False
        Me.txtTotalNoOfPayments.DataField = "Grouping"
        Me.txtTotalNoOfPayments.DistinctField = "Grouping"
        Me.txtTotalNoOfPayments.Height = 0.15!
        Me.txtTotalNoOfPayments.Left = 4.6!
        Me.txtTotalNoOfPayments.Name = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalNoOfPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtTotalNoOfPayments.SummaryGroup = "grBatchHeader"
        Me.txtTotalNoOfPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalNoOfPayments.Text = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Top = 0.1!
        Me.txtTotalNoOfPayments.Width = 0.3!
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Height = 0.15!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 3.7!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 0.9!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.DataField = "MON_TransferredAmount"
        Me.txtTotalAmount.DistinctField = "Grouping"
        Me.txtTotalAmount.Height = 0.15!
        Me.txtTotalAmount.Left = 5.45!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtTotalAmount.SummaryGroup = "grBatchHeader"
        Me.txtTotalAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.0!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.15!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.95!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.5!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.txtI_Account, Me.txtDATE_Production, Me.txtClientName, Me.lblDate_Production, Me.lblClient, Me.lblI_Account, Me.txtFilename})
        Me.grBatchHeader.DataField = "Breakfield"
        Me.grBatchHeader.Height = 0.6!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.22!
        Me.txtI_Account.Width = 1.9!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.06!
        Me.txtDATE_Production.Width = 1.563!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.06!
        Me.txtClientName.Width = 1.9!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.125!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.06!
        Me.lblDate_Production.Width = 1.313!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.06!
        Me.lblClient.Width = 1.0!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.22!
        Me.lblI_Account.Width = 1.0!
        '
        'txtFilename
        '
        Me.txtFilename.DataField = "Filename"
        Me.txtFilename.Height = 0.19!
        Me.txtFilename.Left = 1.5!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Style = "font-size: 10pt; font-weight: normal"
        Me.txtFilename.Text = "txtFilename"
        Me.txtFilename.Top = 0.22!
        Me.txtFilename.Width = 1.9!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblBatchfooterAmount, Me.txtBatchfooterAmount, Me.lblBatchfooterNoofPayments, Me.txtBatchfooterNoofPayments, Me.LineBatchFooter1})
        Me.grBatchFooter.Height = 0.28!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.15!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 4.95!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.0!
        Me.lblBatchfooterAmount.Width = 0.5!
        '
        'txtBatchfooterAmount
        '
        Me.txtBatchfooterAmount.DataField = "MON_TransferredAmount"
        Me.txtBatchfooterAmount.DistinctField = "Grouping"
        Me.txtBatchfooterAmount.Height = 0.15!
        Me.txtBatchfooterAmount.Left = 5.45!
        Me.txtBatchfooterAmount.Name = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.OutputFormat = resources.GetString("txtBatchfooterAmount.OutputFormat")
        Me.txtBatchfooterAmount.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtBatchfooterAmount.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterAmount.Text = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.Top = 0.0!
        Me.txtBatchfooterAmount.Width = 1.0!
        '
        'lblBatchfooterNoofPayments
        '
        Me.lblBatchfooterNoofPayments.Height = 0.15!
        Me.lblBatchfooterNoofPayments.HyperLink = Nothing
        Me.lblBatchfooterNoofPayments.Left = 3.7!
        Me.lblBatchfooterNoofPayments.Name = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterNoofPayments.Text = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Top = 0.0!
        Me.lblBatchfooterNoofPayments.Width = 0.9!
        '
        'txtBatchfooterNoofPayments
        '
        Me.txtBatchfooterNoofPayments.DataField = "Grouping"
        Me.txtBatchfooterNoofPayments.DistinctField = "Grouping"
        Me.txtBatchfooterNoofPayments.Height = 0.15!
        Me.txtBatchfooterNoofPayments.Left = 4.6!
        Me.txtBatchfooterNoofPayments.Name = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterNoofPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtBatchfooterNoofPayments.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterNoofPayments.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterNoofPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterNoofPayments.Text = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Top = 0.0!
        Me.txtBatchfooterNoofPayments.Width = 0.3!
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 3.7!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.21!
        Me.LineBatchFooter1.Width = 2.8!
        Me.LineBatchFooter1.X1 = 3.7!
        Me.LineBatchFooter1.X2 = 6.5!
        Me.LineBatchFooter1.Y1 = 0.21!
        Me.LineBatchFooter1.Y2 = 0.21!
        '
        'grPaymentReceiverHeader
        '
        Me.grPaymentReceiverHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentReceiverHeader.CanShrink = True
        Me.grPaymentReceiverHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblReceiver, Me.txtI_Adr1, Me.txtI_Name, Me.txtI_Adr2, Me.txtI_Zip, Me.txtI_City, Me.lblPayor, Me.txtI_Adr3})
        Me.grPaymentReceiverHeader.DataField = "Grouping"
        Me.grPaymentReceiverHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentReceiverHeader.Height = 1.16!
        Me.grPaymentReceiverHeader.Name = "grPaymentReceiverHeader"
        '
        'lblReceiver
        '
        Me.lblReceiver.Height = 0.15!
        Me.lblReceiver.HyperLink = Nothing
        Me.lblReceiver.Left = 0.0!
        Me.lblReceiver.Name = "lblReceiver"
        Me.lblReceiver.Style = "font-size: 8pt"
        Me.lblReceiver.Text = "lblReceiver"
        Me.lblReceiver.Top = 0.0!
        Me.lblReceiver.Width = 4.0!
        '
        'txtI_Adr1
        '
        Me.txtI_Adr1.DataField = "I_Adr1"
        Me.txtI_Adr1.Height = 0.15!
        Me.txtI_Adr1.Left = 0.0!
        Me.txtI_Adr1.Name = "txtI_Adr1"
        Me.txtI_Adr1.Style = "font-size: 8pt"
        Me.txtI_Adr1.Text = "txtI_Adr1"
        Me.txtI_Adr1.Top = 0.3!
        Me.txtI_Adr1.Width = 4.0!
        '
        'txtI_Name
        '
        Me.txtI_Name.DataField = "I_Name"
        Me.txtI_Name.Height = 0.15!
        Me.txtI_Name.Left = 0.0!
        Me.txtI_Name.Name = "txtI_Name"
        Me.txtI_Name.Style = "font-size: 8pt"
        Me.txtI_Name.Text = "txtI_Name"
        Me.txtI_Name.Top = 0.15!
        Me.txtI_Name.Width = 4.0!
        '
        'txtI_Adr2
        '
        Me.txtI_Adr2.DataField = "I_Adr2"
        Me.txtI_Adr2.Height = 0.15!
        Me.txtI_Adr2.Left = 0.0!
        Me.txtI_Adr2.Name = "txtI_Adr2"
        Me.txtI_Adr2.Style = "font-size: 8pt"
        Me.txtI_Adr2.Text = "txtI_Adr2"
        Me.txtI_Adr2.Top = 0.45!
        Me.txtI_Adr2.Width = 4.0!
        '
        'txtI_Zip
        '
        Me.txtI_Zip.DataField = "I_Zip"
        Me.txtI_Zip.Height = 0.15!
        Me.txtI_Zip.Left = 0.0!
        Me.txtI_Zip.Name = "txtI_Zip"
        Me.txtI_Zip.Style = "font-size: 8pt"
        Me.txtI_Zip.Text = "txtI_Zip"
        Me.txtI_Zip.Top = 0.75!
        Me.txtI_Zip.Width = 0.5!
        '
        'txtI_City
        '
        Me.txtI_City.DataField = "I_City"
        Me.txtI_City.Height = 0.15!
        Me.txtI_City.Left = 0.55!
        Me.txtI_City.Name = "txtI_City"
        Me.txtI_City.Style = "font-size: 8pt"
        Me.txtI_City.Text = "txtI_City"
        Me.txtI_City.Top = 0.75!
        Me.txtI_City.Width = 3.0!
        '
        'lblPayor
        '
        Me.lblPayor.Height = 0.15!
        Me.lblPayor.HyperLink = Nothing
        Me.lblPayor.Left = 0.0!
        Me.lblPayor.Name = "lblPayor"
        Me.lblPayor.Style = "font-size: 8pt; vertical-align: top"
        Me.lblPayor.Text = "lblPayor"
        Me.lblPayor.Top = 1.0!
        Me.lblPayor.Width = 2.0!
        '
        'txtI_Adr3
        '
        Me.txtI_Adr3.DataField = "I_Adr3"
        Me.txtI_Adr3.Height = 0.15!
        Me.txtI_Adr3.Left = 0.0!
        Me.txtI_Adr3.Name = "txtI_Adr3"
        Me.txtI_Adr3.Style = "font-size: 8pt"
        Me.txtI_Adr3.Text = "txtI_Adr3"
        Me.txtI_Adr3.Top = 0.6!
        Me.txtI_Adr3.Width = 4.0!
        '
        'grPaymentReceiverFooter
        '
        Me.grPaymentReceiverFooter.CanShrink = True
        Me.grPaymentReceiverFooter.Height = 0.03125!
        Me.grPaymentReceiverFooter.Name = "grPaymentReceiverFooter"
        Me.grPaymentReceiverFooter.Visible = False
        '
        'grPaymentHeader
        '
        Me.grPaymentHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentHeader.CanShrink = True
        Me.grPaymentHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtE_Name, Me.txtMON_P_TransferredAmount, Me.txtE_Adr1, Me.txtE_Adr2, Me.txtE_Adr3, Me.txtE_Zip, Me.txtE_City, Me.txtE_Account, Me.txtREF_Bank2, Me.txtREF_Bank1, Me.txtDATE_Value, Me.txtVoucherNo, Me.txtMON_P_TransferCurrency, Me.lblE_Account, Me.lblREF_Bank2, Me.lblREF_Bank1, Me.lblDATE_Value, Me.lblVoucherNo, Me.lblREF_Own, Me.txtREF_Own, Me.lblBIC, Me.txtBIC, Me.lblBANK_Branch, Me.txtBANK_Branch, Me.txtPayI_Account, Me.lblPayI_Account, Me.txtE_CountryCode, Me.lblBB_Id, Me.txtBabelBank_ID})
        Me.grPaymentHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentHeader.Height = 1.4!
        Me.grPaymentHeader.Name = "grPaymentHeader"
        '
        'txtE_Name
        '
        Me.txtE_Name.Height = 0.15!
        Me.txtE_Name.Left = 0.0!
        Me.txtE_Name.Name = "txtE_Name"
        Me.txtE_Name.Style = "font-size: 8pt"
        Me.txtE_Name.Text = "txtE_Name"
        Me.txtE_Name.Top = 0.0!
        Me.txtE_Name.Width = 2.28!
        '
        'txtMON_P_TransferredAmount
        '
        Me.txtMON_P_TransferredAmount.DataField = "MON_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Height = 0.15!
        Me.txtMON_P_TransferredAmount.Left = 5.45!
        Me.txtMON_P_TransferredAmount.Name = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.OutputFormat = resources.GetString("txtMON_P_TransferredAmount.OutputFormat")
        Me.txtMON_P_TransferredAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_P_TransferredAmount.Text = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Top = 0.0!
        Me.txtMON_P_TransferredAmount.Width = 1.0!
        '
        'txtE_Adr1
        '
        Me.txtE_Adr1.DataField = "E_Adr1"
        Me.txtE_Adr1.Height = 0.15!
        Me.txtE_Adr1.Left = 0.0!
        Me.txtE_Adr1.Name = "txtE_Adr1"
        Me.txtE_Adr1.Style = "font-size: 8pt"
        Me.txtE_Adr1.Text = "txtE_Adr1"
        Me.txtE_Adr1.Top = 0.15!
        Me.txtE_Adr1.Width = 2.28!
        '
        'txtE_Adr2
        '
        Me.txtE_Adr2.DataField = "E_Adr2"
        Me.txtE_Adr2.Height = 0.15!
        Me.txtE_Adr2.Left = 0.0!
        Me.txtE_Adr2.Name = "txtE_Adr2"
        Me.txtE_Adr2.Style = "font-size: 8pt"
        Me.txtE_Adr2.Text = "txtE_Adr2"
        Me.txtE_Adr2.Top = 0.3!
        Me.txtE_Adr2.Width = 2.28!
        '
        'txtE_Adr3
        '
        Me.txtE_Adr3.DataField = "E_Adr3"
        Me.txtE_Adr3.Height = 0.15!
        Me.txtE_Adr3.Left = 0.0!
        Me.txtE_Adr3.Name = "txtE_Adr3"
        Me.txtE_Adr3.Style = "font-size: 8pt"
        Me.txtE_Adr3.Text = "txtE_Adr3"
        Me.txtE_Adr3.Top = 0.6000001!
        Me.txtE_Adr3.Width = 2.28!
        '
        'txtE_Zip
        '
        Me.txtE_Zip.DataField = "E_Zip"
        Me.txtE_Zip.Height = 0.15!
        Me.txtE_Zip.Left = 0.0!
        Me.txtE_Zip.Name = "txtE_Zip"
        Me.txtE_Zip.Style = "font-size: 8pt"
        Me.txtE_Zip.Text = "txtE_Zip"
        Me.txtE_Zip.Top = 0.45!
        Me.txtE_Zip.Width = 0.375!
        '
        'txtE_City
        '
        Me.txtE_City.DataField = "E_City"
        Me.txtE_City.Height = 0.15!
        Me.txtE_City.Left = 0.5!
        Me.txtE_City.Name = "txtE_City"
        Me.txtE_City.Style = "font-size: 8pt"
        Me.txtE_City.Text = "txtE_City"
        Me.txtE_City.Top = 0.45!
        Me.txtE_City.Width = 1.78!
        '
        'txtE_Account
        '
        Me.txtE_Account.DataField = "E_Account"
        Me.txtE_Account.Height = 0.15!
        Me.txtE_Account.Left = 3.25!
        Me.txtE_Account.Name = "txtE_Account"
        Me.txtE_Account.Style = "font-size: 8pt"
        Me.txtE_Account.Text = "txtE_Account"
        Me.txtE_Account.Top = 0.15!
        Me.txtE_Account.Width = 1.64!
        '
        'txtREF_Bank2
        '
        Me.txtREF_Bank2.DataField = "REF_Bank2"
        Me.txtREF_Bank2.Height = 0.15!
        Me.txtREF_Bank2.Left = 3.25!
        Me.txtREF_Bank2.Name = "txtREF_Bank2"
        Me.txtREF_Bank2.Style = "font-size: 8pt"
        Me.txtREF_Bank2.Text = "txtREF_Bank2"
        Me.txtREF_Bank2.Top = 0.3!
        Me.txtREF_Bank2.Width = 1.5!
        '
        'txtREF_Bank1
        '
        Me.txtREF_Bank1.DataField = "REF_Bank1"
        Me.txtREF_Bank1.Height = 0.15!
        Me.txtREF_Bank1.Left = 3.25!
        Me.txtREF_Bank1.Name = "txtREF_Bank1"
        Me.txtREF_Bank1.Style = "font-size: 8pt"
        Me.txtREF_Bank1.Text = "txtREF_Bank1"
        Me.txtREF_Bank1.Top = 0.45!
        Me.txtREF_Bank1.Width = 1.5!
        '
        'txtDATE_Value
        '
        Me.txtDATE_Value.Height = 0.15!
        Me.txtDATE_Value.Left = 3.25!
        Me.txtDATE_Value.Name = "txtDATE_Value"
        Me.txtDATE_Value.OutputFormat = resources.GetString("txtDATE_Value.OutputFormat")
        Me.txtDATE_Value.Style = "font-size: 8pt"
        Me.txtDATE_Value.Text = "txtDATE_Value"
        Me.txtDATE_Value.Top = 0.6!
        Me.txtDATE_Value.Width = 2.0!
        '
        'txtVoucherNo
        '
        Me.txtVoucherNo.DataField = "VoucherNo"
        Me.txtVoucherNo.Height = 0.15!
        Me.txtVoucherNo.Left = 3.25!
        Me.txtVoucherNo.Name = "txtVoucherNo"
        Me.txtVoucherNo.Style = "font-size: 8pt"
        Me.txtVoucherNo.Text = "txtVoucherNo"
        Me.txtVoucherNo.Top = 1.2!
        Me.txtVoucherNo.Width = 2.0!
        '
        'txtMON_P_TransferCurrency
        '
        Me.txtMON_P_TransferCurrency.DataField = "MON_TransferCurrency"
        Me.txtMON_P_TransferCurrency.Height = 0.15!
        Me.txtMON_P_TransferCurrency.Left = 5.0!
        Me.txtMON_P_TransferCurrency.Name = "txtMON_P_TransferCurrency"
        Me.txtMON_P_TransferCurrency.Style = "font-size: 8pt"
        Me.txtMON_P_TransferCurrency.Text = "PCur"
        Me.txtMON_P_TransferCurrency.Top = 0.0!
        Me.txtMON_P_TransferCurrency.Width = 0.4!
        '
        'lblE_Account
        '
        Me.lblE_Account.Height = 0.15!
        Me.lblE_Account.HyperLink = Nothing
        Me.lblE_Account.Left = 2.4!
        Me.lblE_Account.MultiLine = False
        Me.lblE_Account.Name = "lblE_Account"
        Me.lblE_Account.Style = "font-size: 8pt"
        Me.lblE_Account.Text = "lblE_Account"
        Me.lblE_Account.Top = 0.15!
        Me.lblE_Account.Width = 0.8!
        '
        'lblREF_Bank2
        '
        Me.lblREF_Bank2.Height = 0.15!
        Me.lblREF_Bank2.HyperLink = Nothing
        Me.lblREF_Bank2.Left = 2.4!
        Me.lblREF_Bank2.Name = "lblREF_Bank2"
        Me.lblREF_Bank2.Style = "font-size: 8pt"
        Me.lblREF_Bank2.Text = "lblREF_Bank2-ExecutionRef"
        Me.lblREF_Bank2.Top = 0.3!
        Me.lblREF_Bank2.Width = 0.8!
        '
        'lblREF_Bank1
        '
        Me.lblREF_Bank1.Height = 0.15!
        Me.lblREF_Bank1.HyperLink = Nothing
        Me.lblREF_Bank1.Left = 2.4!
        Me.lblREF_Bank1.Name = "lblREF_Bank1"
        Me.lblREF_Bank1.Style = "font-size: 8pt"
        Me.lblREF_Bank1.Text = "lblREF_Bank1-Giroref"
        Me.lblREF_Bank1.Top = 0.45!
        Me.lblREF_Bank1.Width = 0.8!
        '
        'lblDATE_Value
        '
        Me.lblDATE_Value.Height = 0.15!
        Me.lblDATE_Value.HyperLink = Nothing
        Me.lblDATE_Value.Left = 2.4!
        Me.lblDATE_Value.Name = "lblDATE_Value"
        Me.lblDATE_Value.Style = "font-size: 8pt"
        Me.lblDATE_Value.Text = "lblDATE_Value"
        Me.lblDATE_Value.Top = 0.6!
        Me.lblDATE_Value.Width = 0.8!
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Height = 0.15!
        Me.lblVoucherNo.HyperLink = Nothing
        Me.lblVoucherNo.Left = 2.4!
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Style = "font-size: 8pt"
        Me.lblVoucherNo.Text = "lblVoucherNo"
        Me.lblVoucherNo.Top = 1.2!
        Me.lblVoucherNo.Width = 0.8!
        '
        'lblREF_Own
        '
        Me.lblREF_Own.Height = 0.15!
        Me.lblREF_Own.HyperLink = Nothing
        Me.lblREF_Own.Left = 2.4!
        Me.lblREF_Own.MultiLine = False
        Me.lblREF_Own.Name = "lblREF_Own"
        Me.lblREF_Own.Style = "font-size: 8pt"
        Me.lblREF_Own.Text = "lblREF_Own"
        Me.lblREF_Own.Top = 0.75!
        Me.lblREF_Own.Width = 0.8!
        '
        'txtREF_Own
        '
        Me.txtREF_Own.DataField = "REF_Own"
        Me.txtREF_Own.Height = 0.15!
        Me.txtREF_Own.Left = 3.25!
        Me.txtREF_Own.Name = "txtREF_Own"
        Me.txtREF_Own.Style = "font-size: 8pt"
        Me.txtREF_Own.Text = "txtREF_Own"
        Me.txtREF_Own.Top = 0.75!
        Me.txtREF_Own.Width = 3.0!
        '
        'lblBIC
        '
        Me.lblBIC.Height = 0.15!
        Me.lblBIC.HyperLink = Nothing
        Me.lblBIC.Left = 2.4!
        Me.lblBIC.Name = "lblBIC"
        Me.lblBIC.Style = "font-size: 8pt"
        Me.lblBIC.Text = "lblBIC"
        Me.lblBIC.Top = 0.9!
        Me.lblBIC.Width = 0.8!
        '
        'txtBIC
        '
        Me.txtBIC.DataField = "BANK_SWIFTCode"
        Me.txtBIC.Height = 0.15!
        Me.txtBIC.Left = 3.25!
        Me.txtBIC.Name = "txtBIC"
        Me.txtBIC.Style = "font-size: 8pt"
        Me.txtBIC.Text = "txtBIC"
        Me.txtBIC.Top = 0.9!
        Me.txtBIC.Width = 1.5!
        '
        'lblBANK_Branch
        '
        Me.lblBANK_Branch.Height = 0.15!
        Me.lblBANK_Branch.HyperLink = Nothing
        Me.lblBANK_Branch.Left = 2.4!
        Me.lblBANK_Branch.Name = "lblBANK_Branch"
        Me.lblBANK_Branch.Style = "font-size: 8pt"
        Me.lblBANK_Branch.Text = "lblBANK_Branch"
        Me.lblBANK_Branch.Top = 1.05!
        Me.lblBANK_Branch.Width = 0.8099999!
        '
        'txtBANK_Branch
        '
        Me.txtBANK_Branch.Height = 0.15!
        Me.txtBANK_Branch.Left = 3.25!
        Me.txtBANK_Branch.Name = "txtBANK_Branch"
        Me.txtBANK_Branch.Style = "font-size: 8pt"
        Me.txtBANK_Branch.Text = "txtBANK_Branch"
        Me.txtBANK_Branch.Top = 1.05!
        Me.txtBANK_Branch.Width = 1.5!
        '
        'txtPayI_Account
        '
        Me.txtPayI_Account.DataField = "I_Account"
        Me.txtPayI_Account.Height = 0.15!
        Me.txtPayI_Account.Left = 3.25!
        Me.txtPayI_Account.Name = "txtPayI_Account"
        Me.txtPayI_Account.Style = "font-size: 8pt"
        Me.txtPayI_Account.Text = "txtPayI_Account"
        Me.txtPayI_Account.Top = 0.0!
        Me.txtPayI_Account.Width = 1.64!
        '
        'lblPayI_Account
        '
        Me.lblPayI_Account.Height = 0.15!
        Me.lblPayI_Account.HyperLink = Nothing
        Me.lblPayI_Account.Left = 2.4!
        Me.lblPayI_Account.MultiLine = False
        Me.lblPayI_Account.Name = "lblPayI_Account"
        Me.lblPayI_Account.Style = "font-size: 8pt"
        Me.lblPayI_Account.Text = "lblPayI_Account"
        Me.lblPayI_Account.Top = 0.0!
        Me.lblPayI_Account.Width = 0.8!
        '
        'txtE_CountryCode
        '
        Me.txtE_CountryCode.CanShrink = True
        Me.txtE_CountryCode.DataField = "E_CountryCode"
        Me.txtE_CountryCode.Height = 0.15!
        Me.txtE_CountryCode.Left = 0.0!
        Me.txtE_CountryCode.Name = "txtE_CountryCode"
        Me.txtE_CountryCode.Style = "font-size: 8pt"
        Me.txtE_CountryCode.Text = "CC"
        Me.txtE_CountryCode.Top = 0.75!
        Me.txtE_CountryCode.Width = 0.375!
        '
        'lblBB_Id
        '
        Me.lblBB_Id.Height = 0.15!
        Me.lblBB_Id.HyperLink = Nothing
        Me.lblBB_Id.Left = 0.0!
        Me.lblBB_Id.MultiLine = False
        Me.lblBB_Id.Name = "lblBB_Id"
        Me.lblBB_Id.Style = "font-size: 8pt"
        Me.lblBB_Id.Text = "BB_Id:"
        Me.lblBB_Id.Top = 1.02!
        Me.lblBB_Id.Width = 0.45!
        '
        'txtBabelBank_ID
        '
        Me.txtBabelBank_ID.CanShrink = True
        Me.txtBabelBank_ID.DataField = "BabelBank_ID"
        Me.txtBabelBank_ID.Height = 0.15!
        Me.txtBabelBank_ID.Left = 0.5100001!
        Me.txtBabelBank_ID.Name = "txtBabelBank_ID"
        Me.txtBabelBank_ID.Style = "font-size: 8pt"
        Me.txtBabelBank_ID.Text = "txtBabelBank_ID"
        Me.txtBabelBank_ID.Top = 1.01!
        Me.txtBabelBank_ID.Width = 1.5!
        '
        'grPaymentFooter
        '
        Me.grPaymentFooter.CanShrink = True
        Me.grPaymentFooter.Height = 0.0!
        Me.grPaymentFooter.Name = "grPaymentFooter"
        '
        'grPaymentInternationalHeader
        '
        Me.grPaymentInternationalHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentInternationalHeader.CanShrink = True
        Me.grPaymentInternationalHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapegrPaymentInternationalHeader, Me.lblMON_OriginallyPaidAmount, Me.lblMON_InvoiceAmount, Me.lblMON_AccountAmount, Me.lblExchangeRate, Me.lblChargesAbroad, Me.lblChargesDomestic, Me.txtMON_OriginallyPaidAmount, Me.txtMON_InvoiceAmount, Me.txtMON_AccountAmount, Me.txtChargesAbroad, Me.txtChargesDomestic, Me.txtExchangeRate, Me.txtMON_OriginallyPaidCurrency, Me.txtMON_InvoiceCurrency, Me.txtMON_AccountCurrency})
        Me.grPaymentInternationalHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentInternationalHeader.Height = 1.176389!
        Me.grPaymentInternationalHeader.KeepTogether = True
        Me.grPaymentInternationalHeader.Name = "grPaymentInternationalHeader"
        '
        'shapegrPaymentInternationalHeader
        '
        Me.shapegrPaymentInternationalHeader.Height = 1.0!
        Me.shapegrPaymentInternationalHeader.Left = 0.0!
        Me.shapegrPaymentInternationalHeader.Name = "shapegrPaymentInternationalHeader"
        Me.shapegrPaymentInternationalHeader.RoundingRadius = 9.999999!
        Me.shapegrPaymentInternationalHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapegrPaymentInternationalHeader.Top = 0.1!
        Me.shapegrPaymentInternationalHeader.Width = 5.5!
        '
        'lblMON_OriginallyPaidAmount
        '
        Me.lblMON_OriginallyPaidAmount.Height = 0.15!
        Me.lblMON_OriginallyPaidAmount.HyperLink = Nothing
        Me.lblMON_OriginallyPaidAmount.Left = 0.1!
        Me.lblMON_OriginallyPaidAmount.Name = "lblMON_OriginallyPaidAmount"
        Me.lblMON_OriginallyPaidAmount.Style = "font-size: 8pt"
        Me.lblMON_OriginallyPaidAmount.Text = "lblMON_OriginallyPaidAmount"
        Me.lblMON_OriginallyPaidAmount.Top = 0.15!
        Me.lblMON_OriginallyPaidAmount.Width = 2.0!
        '
        'lblMON_InvoiceAmount
        '
        Me.lblMON_InvoiceAmount.Height = 0.15!
        Me.lblMON_InvoiceAmount.HyperLink = Nothing
        Me.lblMON_InvoiceAmount.Left = 0.1!
        Me.lblMON_InvoiceAmount.Name = "lblMON_InvoiceAmount"
        Me.lblMON_InvoiceAmount.Style = "font-size: 8pt"
        Me.lblMON_InvoiceAmount.Text = "lblMON_InvoiceAmount"
        Me.lblMON_InvoiceAmount.Top = 0.3!
        Me.lblMON_InvoiceAmount.Width = 2.0!
        '
        'lblMON_AccountAmount
        '
        Me.lblMON_AccountAmount.Height = 0.15!
        Me.lblMON_AccountAmount.HyperLink = Nothing
        Me.lblMON_AccountAmount.Left = 0.1!
        Me.lblMON_AccountAmount.Name = "lblMON_AccountAmount"
        Me.lblMON_AccountAmount.Style = "font-size: 8pt"
        Me.lblMON_AccountAmount.Text = "lblMON_AccountAmount"
        Me.lblMON_AccountAmount.Top = 0.45!
        Me.lblMON_AccountAmount.Width = 2.0!
        '
        'lblExchangeRate
        '
        Me.lblExchangeRate.Height = 0.15!
        Me.lblExchangeRate.HyperLink = Nothing
        Me.lblExchangeRate.Left = 0.1!
        Me.lblExchangeRate.Name = "lblExchangeRate"
        Me.lblExchangeRate.Style = "font-size: 8pt"
        Me.lblExchangeRate.Text = "lblExchangeRate"
        Me.lblExchangeRate.Top = 0.6000001!
        Me.lblExchangeRate.Width = 2.0!
        '
        'lblChargesAbroad
        '
        Me.lblChargesAbroad.Height = 0.15!
        Me.lblChargesAbroad.HyperLink = Nothing
        Me.lblChargesAbroad.Left = 0.1!
        Me.lblChargesAbroad.Name = "lblChargesAbroad"
        Me.lblChargesAbroad.Style = "font-size: 8pt"
        Me.lblChargesAbroad.Text = "lblChargesAbroad"
        Me.lblChargesAbroad.Top = 0.75!
        Me.lblChargesAbroad.Width = 2.0!
        '
        'lblChargesDomestic
        '
        Me.lblChargesDomestic.Height = 0.15!
        Me.lblChargesDomestic.HyperLink = Nothing
        Me.lblChargesDomestic.Left = 0.1!
        Me.lblChargesDomestic.Name = "lblChargesDomestic"
        Me.lblChargesDomestic.Style = "font-size: 8pt"
        Me.lblChargesDomestic.Text = "lblChargesDomestic"
        Me.lblChargesDomestic.Top = 0.9!
        Me.lblChargesDomestic.Width = 2.0!
        '
        'txtMON_OriginallyPaidAmount
        '
        Me.txtMON_OriginallyPaidAmount.DataField = "MON_OriginallyPaidAmount"
        Me.txtMON_OriginallyPaidAmount.Height = 0.15!
        Me.txtMON_OriginallyPaidAmount.Left = 3.5!
        Me.txtMON_OriginallyPaidAmount.Name = "txtMON_OriginallyPaidAmount"
        Me.txtMON_OriginallyPaidAmount.OutputFormat = resources.GetString("txtMON_OriginallyPaidAmount.OutputFormat")
        Me.txtMON_OriginallyPaidAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_OriginallyPaidAmount.Text = "txtMON_OriginallyPaidAmount"
        Me.txtMON_OriginallyPaidAmount.Top = 0.15!
        Me.txtMON_OriginallyPaidAmount.Width = 1.25!
        '
        'txtMON_InvoiceAmount
        '
        Me.txtMON_InvoiceAmount.DataField = "MON_InvoiceAmount"
        Me.txtMON_InvoiceAmount.Height = 0.15!
        Me.txtMON_InvoiceAmount.Left = 3.5!
        Me.txtMON_InvoiceAmount.Name = "txtMON_InvoiceAmount"
        Me.txtMON_InvoiceAmount.OutputFormat = resources.GetString("txtMON_InvoiceAmount.OutputFormat")
        Me.txtMON_InvoiceAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_InvoiceAmount.Text = "txtMON_InvoiceAmount"
        Me.txtMON_InvoiceAmount.Top = 0.3!
        Me.txtMON_InvoiceAmount.Width = 1.25!
        '
        'txtMON_AccountAmount
        '
        Me.txtMON_AccountAmount.DataField = "MON_AccountAmount"
        Me.txtMON_AccountAmount.Height = 0.15!
        Me.txtMON_AccountAmount.Left = 3.5!
        Me.txtMON_AccountAmount.Name = "txtMON_AccountAmount"
        Me.txtMON_AccountAmount.OutputFormat = resources.GetString("txtMON_AccountAmount.OutputFormat")
        Me.txtMON_AccountAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_AccountAmount.Text = "txtMON_AccountAmount"
        Me.txtMON_AccountAmount.Top = 0.45!
        Me.txtMON_AccountAmount.Width = 1.25!
        '
        'txtChargesAbroad
        '
        Me.txtChargesAbroad.DataField = "ChargesAbroad"
        Me.txtChargesAbroad.Height = 0.15!
        Me.txtChargesAbroad.Left = 3.75!
        Me.txtChargesAbroad.Name = "txtChargesAbroad"
        Me.txtChargesAbroad.OutputFormat = resources.GetString("txtChargesAbroad.OutputFormat")
        Me.txtChargesAbroad.Style = "font-size: 8pt; text-align: right"
        Me.txtChargesAbroad.Text = "txtChargesAbroad"
        Me.txtChargesAbroad.Top = 0.75!
        Me.txtChargesAbroad.Width = 1.0!
        '
        'txtChargesDomestic
        '
        Me.txtChargesDomestic.DataField = "ChargesDomestic"
        Me.txtChargesDomestic.Height = 0.15!
        Me.txtChargesDomestic.Left = 3.75!
        Me.txtChargesDomestic.Name = "txtChargesDomestic"
        Me.txtChargesDomestic.OutputFormat = resources.GetString("txtChargesDomestic.OutputFormat")
        Me.txtChargesDomestic.Style = "font-size: 8pt; text-align: right"
        Me.txtChargesDomestic.Text = "txtChargesDomestic"
        Me.txtChargesDomestic.Top = 0.9!
        Me.txtChargesDomestic.Width = 1.0!
        '
        'txtExchangeRate
        '
        Me.txtExchangeRate.DataField = "MON_LocalExchRate"
        Me.txtExchangeRate.Height = 0.15!
        Me.txtExchangeRate.Left = 3.75!
        Me.txtExchangeRate.Name = "txtExchangeRate"
        Me.txtExchangeRate.OutputFormat = resources.GetString("txtExchangeRate.OutputFormat")
        Me.txtExchangeRate.Style = "font-size: 8pt; text-align: right"
        Me.txtExchangeRate.Text = "txtExchangeRate"
        Me.txtExchangeRate.Top = 0.6000001!
        Me.txtExchangeRate.Width = 1.0!
        '
        'txtMON_OriginallyPaidCurrency
        '
        Me.txtMON_OriginallyPaidCurrency.DataField = "MON_OriginallyPaidCurrency"
        Me.txtMON_OriginallyPaidCurrency.Height = 0.15!
        Me.txtMON_OriginallyPaidCurrency.Left = 3.1!
        Me.txtMON_OriginallyPaidCurrency.Name = "txtMON_OriginallyPaidCurrency"
        Me.txtMON_OriginallyPaidCurrency.Style = "font-size: 8pt"
        Me.txtMON_OriginallyPaidCurrency.Text = "OCur"
        Me.txtMON_OriginallyPaidCurrency.Top = 0.15!
        Me.txtMON_OriginallyPaidCurrency.Width = 0.438!
        '
        'txtMON_InvoiceCurrency
        '
        Me.txtMON_InvoiceCurrency.DataField = "MON_InvoiceCurrency"
        Me.txtMON_InvoiceCurrency.Height = 0.15!
        Me.txtMON_InvoiceCurrency.Left = 3.1!
        Me.txtMON_InvoiceCurrency.Name = "txtMON_InvoiceCurrency"
        Me.txtMON_InvoiceCurrency.Style = "font-size: 8pt"
        Me.txtMON_InvoiceCurrency.Text = "ICur"
        Me.txtMON_InvoiceCurrency.Top = 0.3!
        Me.txtMON_InvoiceCurrency.Width = 0.438!
        '
        'txtMON_AccountCurrency
        '
        Me.txtMON_AccountCurrency.DataField = "MON_AccountCurrency"
        Me.txtMON_AccountCurrency.Height = 0.15!
        Me.txtMON_AccountCurrency.Left = 3.1!
        Me.txtMON_AccountCurrency.Name = "txtMON_AccountCurrency"
        Me.txtMON_AccountCurrency.Style = "font-size: 8pt"
        Me.txtMON_AccountCurrency.Text = "ACur"
        Me.txtMON_AccountCurrency.Top = 0.45!
        Me.txtMON_AccountCurrency.Width = 0.438!
        '
        'grPaymentInternationalFooter
        '
        Me.grPaymentInternationalFooter.CanShrink = True
        Me.grPaymentInternationalFooter.Height = 0.03125!
        Me.grPaymentInternationalFooter.Name = "grPaymentInternationalFooter"
        '
        'grFreetextHeader
        '
        Me.grFreetextHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grFreetextHeader.CanShrink = True
        Me.grFreetextHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtErrorText})
        Me.grFreetextHeader.Height = 0.2604167!
        Me.grFreetextHeader.Name = "grFreetextHeader"
        '
        'txtErrorText
        '
        Me.txtErrorText.CanShrink = True
        Me.txtErrorText.Height = 0.15!
        Me.txtErrorText.Left = 0.0!
        Me.txtErrorText.Name = "txtErrorText"
        Me.txtErrorText.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.txtErrorText.Text = "txtErrorText"
        Me.txtErrorText.Top = 0.0!
        Me.txtErrorText.Visible = False
        Me.txtErrorText.Width = 6.438001!
        '
        'grFreetextFooter
        '
        Me.grFreetextFooter.CanShrink = True
        Me.grFreetextFooter.Height = 0.03125!
        Me.grFreetextFooter.Name = "grFreetextFooter"
        Me.grFreetextFooter.Visible = False
        '
        'rp_003_PaymentInvoice
        '
        Me.MasterReport = False
        OleDBDataSource1.ConnectionString = ""
        OleDBDataSource1.SQL = "Select * from"
        Me.DataSource = OleDBDataSource1
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.500501!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.grPaymentReceiverHeader)
        Me.Sections.Add(Me.grPaymentHeader)
        Me.Sections.Add(Me.grPaymentInternationalHeader)
        Me.Sections.Add(Me.grFreetextHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grFreetextFooter)
        Me.Sections.Add(Me.grPaymentInternationalFooter)
        Me.Sections.Add(Me.grPaymentFooter)
        Me.Sections.Add(Me.grPaymentReceiverFooter)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.txtUniqueID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblReceiver, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Adr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Adr2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Zip, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_City, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPayor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Adr3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Zip, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_City, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVoucherNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblVoucherNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Own, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Own, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBIC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBIC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBANK_Branch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBANK_Branch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPayI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPayI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_CountryCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBB_Id, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBabelBank_ID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMON_AccountAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblExchangeRate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblChargesAbroad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblChargesDomestic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_AccountAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChargesAbroad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChargesDomestic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExchangeRate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_OriginallyPaidCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_InvoiceCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_AccountCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtErrorText, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents linePageHeader As DataDynamics.ActiveReports.Line
    Friend WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtTotalNoOfPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBatchfooterAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblBatchfooterNoofPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBatchfooterNoofPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents grPaymentReceiverHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents lblReceiver As DataDynamics.ActiveReports.Label
    Friend WithEvents txtI_Adr1 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Name As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Adr2 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Zip As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_City As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblPayor As DataDynamics.ActiveReports.Label
    Friend WithEvents grPaymentReceiverFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents grPaymentHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents txtE_Name As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_P_TransferredAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtE_Adr1 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtE_Adr2 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtE_Adr3 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtE_Zip As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtE_City As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtE_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtREF_Bank2 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtREF_Bank1 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Value As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtVoucherNo As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_P_TransferCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblE_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents lblREF_Bank2 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblREF_Bank1 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDATE_Value As DataDynamics.ActiveReports.Label
    Friend WithEvents lblVoucherNo As DataDynamics.ActiveReports.Label
    Friend WithEvents lblREF_Own As DataDynamics.ActiveReports.Label
    Friend WithEvents txtREF_Own As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblBIC As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBIC As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblBANK_Branch As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBANK_Branch As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grPaymentFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents grPaymentInternationalHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapegrPaymentInternationalHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents lblMON_OriginallyPaidAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMON_InvoiceAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMON_AccountAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents lblExchangeRate As DataDynamics.ActiveReports.Label
    Friend WithEvents lblChargesAbroad As DataDynamics.ActiveReports.Label
    Friend WithEvents lblChargesDomestic As DataDynamics.ActiveReports.Label
    Friend WithEvents txtMON_OriginallyPaidAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_InvoiceAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_AccountAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtChargesAbroad As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtChargesDomestic As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtExchangeRate As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_OriginallyPaidCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_InvoiceCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_AccountCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grPaymentInternationalFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents grFreetextHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents SubRptFreetext As DataDynamics.ActiveReports.SubReport
    Friend WithEvents txtErrorText As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grFreetextFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents txtUniqueID As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtInvoiceAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtPayI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblPayI_Account As DataDynamics.ActiveReports.Label
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_CountryCode As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtI_Adr3 As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblBB_Id As DataDynamics.ActiveReports.Label
    Private WithEvents txtBabelBank_ID As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
End Class
