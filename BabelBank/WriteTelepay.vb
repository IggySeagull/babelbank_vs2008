Option Strict Off
Option Explicit On
Module WriteTelepay
	
    Dim oFs As Scripting.FileSystemObject
    Dim oFile As Scripting.TextStream
	Dim sLine As String ' en output-linje
	
	' Vi m� dimme noen variable fdifferentor
	'I_Enterprise is used in all  BETFOR-records.
	Private sI_EnterpriseNo As String
	Private sTransDate As String
	Private bDomestic As Boolean
	Private bTBIO As Boolean
	'This variable show if the importfile we are working with is created from the
	' accounting system, or is a returnfile. The variable is set during reading the
	' BabelFile-object.
	Private bFileFromBank As Boolean
	
	'Dim nTransactionNo
	Function WriteTelepayFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef sBranch As String, ByRef sVersion As String, ByRef SurroundWith0099 As Boolean, ByRef bThisIsTBIO As Boolean, ByRef bUnix As Boolean, ByRef nSequenceNumberStart As Double, ByRef nSequenceNumberEnd As Double, ByRef sClientNo As String, ByRef sSpecial As String) As Boolean
		Dim j, i, k As Double
		' 12.08.02 JaNp
		' Added bUnix
		' if bUnix is set to true, then only LineFeed is added at the end of each line,
		' instead of the ordinary CRLF
		
		Dim bFirstLine As Boolean
		Dim oBabel As BabelFile
		Dim oBatch As Batch
		Dim oPayment As Payment
		Dim oClient As Client
		Dim oaccount As Account
		Dim oInvoice As Invoice
		Dim sNewOwnref As String
		Dim bWriteBETFOR00 As Boolean 'Used with the sequencenumbering
		Dim bWriteBETFOR99Later As Boolean 'Used with the sequencenumbering
		Dim bLastBatchWasDomestic As Boolean 'Used to check if we change to/from domestic/intenational
		Dim bFirstBatch As Boolean 'True until we write the first BETFOR00
		Dim bFirstMassTransaction, bWriteOK As Boolean
		'Dim bCheckFilenameOut As Boolean
		Dim iCount As Short
		Dim nNoOfBETFOR, nSequenceNoTotal As Double
		Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
		Dim nClientNo As Double
		Dim bFoundClient As Boolean
		Dim xAccount As String
		Dim bExportMassRecords As Boolean ' New 25.10.01 by janp - default: No masstrans for returnfiles
		Dim bLastPaymentWasMass As Boolean
		Dim sDummy As String
		Dim nPaymentIndex As Double
        'XNET - 22.04.2013 - Must add SWIFTcode to the debitaccount
        'Next 2 variables
        Dim sOldAccountNo As String
        Dim sI_SWIFTCode As String

		'To use with summarizing masspayment
		Dim oMassPayment As Payment
		Dim oMassInvoice As Invoice
		Dim bSumMassPayment As Boolean
		Dim nMassTransferredAmount As Double
		Dim sPayment_ID As String
		Dim lMassCounter As Integer
		Dim bContinueSummarize As Boolean
		
		' lag en outputfil
		'Deleted 4/12 - Kjell
        Try

            oFs = New Scripting.FileSystemObject
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            bTBIO = bThisIsTBIO
            sI_SWIFTCode = "" 'XNET - 22.04.2013

            bWriteBETFOR00 = True
            bWriteBETFOR99Later = False
            bFirstBatch = True
            nSequenceNoTotal = 1 'new by JanP 25.10.01

            For Each oBabel In oBabelFiles
                'New 28.05.2009
                'To find all changes/examples search for
                ''LSK-STR�MMEN 2-1 e.e.o.
                bExportoBabel = False ' Not needed if no specialchecking is present
                If oBabel.BabelfileContainsPaymentsAccordingToQualifiers(iFilenameInNo, iFormat_ID, bMultiFiles, sClientNo, True, sOwnRef) Then
                    'Special test for this format - Usually we don't need to use the bExportBabel at all
                    If sSpecial = "CRENO_OCRTP" Then
                        bExportoBabel = False
                        For Each oBatch In oBabel.Batches
                            For Each oPayment In oBatch.Payments
                                If oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then
                                    bExportoBabel = True
                                End If
                            Next oPayment
                        Next oBatch
                    Else
                        bExportoBabel = True
                    End If
                End If

                'Old code
                '''''''    If oBabel.VB_FilenameInNo = iFilenameInNo Then
                '''''''
                '''''''        bExportoBabel = False
                '''''''        'Have to go through each batch-object to see if we have objects that shall
                '''''''        ' be exported to this exportfile
                '''''''        For Each oBatch In oBabel.Batches
                '''''''            For Each oPayment In oBatch.Payments
                '''''''                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                '''''''                    If Not bMultifiles Then
                '''''''                        'If oBabel.Special = "CRENO_OCRTP" Then
                '''''''                        ' Changed 23.11.06
                '''''''                        If sSpecial = "CRENO_OCRTP" Then
                '''''''                            If oPayment.MATCH_Matched = MatchStatus.Matched Then
                '''''''                                bExportoBabel = True
                '''''''                            End If
                '''''''                        Else
                '''''''                            bExportoBabel = True
                '''''''                        End If
                '''''''                    Else
                '''''''                        If oPayment.VB_ClientNo = sClientNo Then
                '''''''                            If InStr(oPayment.REF_Own, "&?") Then
                '''''''                                'Set in the part of the OwnRef that BabelBank is using
                '''''''                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                '''''''                            Else
                '''''''                                sNewOwnref = ""
                '''''''                            End If
                '''''''                            If sNewOwnref = sOwnRef Then
                '''''''                                bExportoBabel = True
                '''''''                            End If
                '''''''                        End If
                '''''''                    End If
                '''''''                End If
                '''''''                If bExportoBabel Then
                '''''''                    Exit For
                '''''''                End If
                '''''''            Next
                '''''''            If bExportoBabel Then
                '''''''                Exit For
                '''''''            End If
                '''''''        Next

                If bExportoBabel Then
                    'Set if the file was created int the accounting system or not.
                    bFileFromBank = oBabel.FileFromBank
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        ' XokNET 25.05.2011
                        ' Added next If, because it would bomb further down for empty batches
                        If oBatch.Payments.Count > 0 Then

                            'New 28.05.2009
                            'LSK-STR�MMEN 2-1 e.e.o.
                            If oBatch.BatchContainsPaymentsAccordingToQualifiers(iFormat_ID, bMultiFiles, sClientNo, nPaymentIndex, True, sOwnRef) Then
                                'Special test for this format - Usually we don't need to use the bExportBatch at all
                                If sSpecial = "CRENO_OCRTP" Then
                                    bExportoBatch = False
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then
                                            bExportoBatch = True
                                        End If
                                    Next oPayment
                                Else
                                    bExportoBatch = True
                                End If
                            End If

                            ''''''        'Old code
                            ''''''        If bExportoBabel Then
                            ''''''            'Set if the file was created int the accounting system or not.
                            ''''''            bFileFromBank = oBabel.FileFromBank
                            ''''''            'i = 0 'FIXED Moved by Janp under case 2
                            ''''''
                            ''''''
                            ''''''            'Loop through all Batch objs. in BabelFile obj.
                            ''''''            For Each oBatch In oBabel.Batches
                            ''''''                bExportoBatch = False
                            ''''''                'Have to go through the payment-object to see if we have objects thatt shall
                            ''''''                ' be exported to this exportfile
                            ''''''                For Each oPayment In oBatch.Payments
                            ''''''                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            ''''''                        If Not bMultifiles Then
                            ''''''                            'If oBabel.Special = "CRENO_OCRTP" Then
                            ''''''                            ' Changed 23.11.06
                            ''''''                            If sSpecial = "CRENO_OCRTP" Then
                            ''''''                                If oPayment.MATCH_Matched = MatchStatus.Matched Then
                            ''''''                                    bExportoBatch = True
                            ''''''                                End If
                            ''''''                            Else
                            ''''''                                bExportoBatch = True
                            ''''''                            End If
                            ''''''                        Else
                            ''''''                            If oPayment.VB_ClientNo = sClientNo Then
                            ''''''                                If InStr(oPayment.REF_Own, "&?") Then
                            ''''''                                    'Set in the part of the OwnRef that BabelBank is using
                            ''''''                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                            ''''''                                Else
                            ''''''                                    sNewOwnref = ""
                            ''''''                                End If
                            ''''''                                If sNewOwnref = sOwnRef Then
                            ''''''                                    bExportoBatch = True
                            ''''''                                End If
                            ''''''                            End If
                            ''''''                        End If
                            ''''''                        ' For PGS Sweden, we will only export International payments in TBIO-file
                            ''''''                        If UCase(sSpecial) = "PGS_SWEDEN" Then
                            ''''''                            If oPayment.PayType <> "I" Then
                            ''''''                                bExportoBatch = False
                            ''''''                            End If
                            ''''''                        End If
                            ''''''
                            ''''''                    End If
                            ''''''                    'If true exit the loop, and we have data to export
                            ''''''                    If bExportoBatch Then
                            ''''''                        Exit For
                            ''''''                    End If
                            ''''''                Next


                            If bExportoBatch Then

                                ' New by Janp 25.10.01
                                ' Optional if Mass-trans will be exported for returnfiles.
                                ' Default = False
                                ' In filesetup, test ReturnMass
                                If oBatch.VB_ProfileInUse = True Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().ReturnMass. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        bExportMassRecords = oBatch.VB_Profile.FileSetups(iFormat_ID).ReturnMass
                                    Else
                                        bExportMassRecords = True
                                    End If
                                Else
                                    bExportMassRecords = True
                                End If

                                ' New by JanP 25.10.01
                                If Not (bExportMassRecords = False And (oBatch.Payments.Item(nPaymentIndex).PayType = "M" Or oBatch.Payments.Item(nPaymentIndex).PayType = "S")) Then
                                    ' Don't put mass-payments into returnfiles (unless user has asked for it)
                                    'If bFirstBatch then
                                    ' Changed by JanP 29.10.01
                                    If oBatch.VB_ProfileInUse Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If bFirstBatch Or oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            ' First batch in file, then write betfor00
                                            bWriteBETFOR00 = True
                                        Else
                                            ' Not first batch, and masstrans, don't write betfor00
                                            bWriteBETFOR00 = False
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bWriteBETFOR00 = True
                                        Else
                                            bWriteBETFOR00 = False
                                        End If
                                    End If
                                Else
                                    ' No export of mass-trans, but check if first betfor00:
                                    If bFirstBatch Then
                                        ' First batch in file, then write betfor00
                                        bWriteBETFOR00 = True
                                    Else
                                        ' Not first batch, and masstrans, don't write betfor00
                                        bWriteBETFOR00 = False
                                    End If
                                End If


                                'Check if we shall use the original sequenceno. or not
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then
                                    'Then if it is a return-file
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        bFirstLine = True
                                        'If oBabel.Special = "CRENO_OCRTP" Then
                                        If sSpecial = "CRENO_OCRTP" Then
                                            'Always use the seqno at filesetup level
                                            If bWriteBETFOR00 Then
                                                If bFirstBatch Then
                                                    'Only for the first batch if we have one sequenceseries!
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                                    If nSequenceNoTotal > 9999 Then
                                                        nSequenceNoTotal = 0
                                                    End If
                                                End If
                                            End If
                                        End If

                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                If oBatch.SequenceNoStart = -1 Then
                                                    'FIXERROR: Babelerror No sequenceno imported
                                                Else
                                                    nSequenceNoTotal = oBatch.SequenceNoStart
                                                    ' New by JanP 25.10.01
                                                    ' If we have a file with several betfor00/99, then we must
                                                    ' check if any betfor99 is pending, and deduct 1 from sequence, for the
                                                    ' pending betfor99 (iSeqenceNoTotal is added 1 when the pending
                                                    ' BETFOR99 is written;
                                                    If bWriteBETFOR99Later Then
                                                        nSequenceNoTotal = nSequenceNoTotal - 1
                                                    End If
                                                End If
                                                'Use the seqno at filesetup level
                                            Case 1
                                                'If it isn't the first client, just keep on counting.
                                                If bWriteBETFOR00 Then
                                                    ' New by JanP 24.10.01
                                                    If bFirstBatch Then
                                                        'Only for the first batch if we have one sequenceseries!
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                                        If nSequenceNoTotal > 9999 Then
                                                            nSequenceNoTotal = 0
                                                        End If
                                                    End If
                                                End If
                                                'Use seqno per client per filesetup
                                            Case 2
                                                bFoundClient = False
                                                i = 0 ' New by Janp 25.10.01 Sets how day sequence will be counted
                                                'If oBabel.Special = "BERGESEN" Then
                                                If sSpecial = "BERGESEN" Then
                                                    'Remember to add this code in the writing of Telepay2
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                        If Trim(oClient.CompNo) = Trim(oBatch.I_EnterpriseNo) Then
                                                            nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                            If nSequenceNoTotal > 9999 Then
                                                                nSequenceNoTotal = 0
                                                            End If
                                                            nClientNo = oClient.Index
                                                            bFoundClient = True
                                                            Exit For
                                                        End If
                                                    Next oClient
                                                Else
                                                    'Normal situation
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                        For Each oaccount In oClient.Accounts
                                                            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                            If oaccount.Account = oBatch.Payments(1).I_Account Then
                                                                nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                                If nSequenceNoTotal > 9999 Then
                                                                    nSequenceNoTotal = 0
                                                                End If
                                                                nClientNo = oClient.Index
                                                                bFoundClient = True
                                                                Exit For
                                                            End If
                                                        Next oaccount
                                                        If bFoundClient Then
                                                            Exit For
                                                        End If
                                                    Next oClient
                                                End If
                                        End Select
                                    End If
                                Else
                                    'New 07.06.2006
                                    nSequenceNoTotal = nSequenceNumberStart + 1
                                    If nSequenceNoTotal > 9999 Then
                                        nSequenceNoTotal = 0
                                    End If
                                    bFirstLine = True 'new by JanP 25.10.01
                                End If
                                'Used in every applicationheader, TBII = Domestic, TBIU = International.
                                If oBatch.Payments.Count = 0 Then
                                    bDomestic = True
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = True
                                    'End If
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ElseIf oBatch.Payments(1).PayType = "I" Or bTBIO Then
                                    bDomestic = False
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = False
                                    'End If
                                Else
                                    bDomestic = True
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = True
                                    'End If
                                End If
                                'Check if it is a change between domestic and international payment, and if
                                ' the bWriteBETFOR99Later flag is true (indicates that KeepBatch is set to false in
                                ' the database
                                If bLastBatchWasDomestic <> bDomestic Then
                                    If bWriteBETFOR99Later Then
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                        nNoOfBETFOR = 0
                                    End If
                                    ' New by JanP 25.10.01
                                    If Not (bExportMassRecords = False And (oBatch.Payments.Item(nPaymentIndex).PayType = "M" Or oBatch.Payments.Item(nPaymentIndex).PayType = "S")) Then
                                        bWriteBETFOR00 = True
                                        bWriteBETFOR99Later = False
                                    Else
                                        'bWriteBETFOR00 = False  'Keep as is
                                        bWriteBETFOR99Later = False
                                    End If
                                End If

                                ' New by JanP 29.10.01
                                If oBatch.Payments.Count = 0 Then
                                    bLastBatchWasDomestic = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ElseIf oBatch.Payments(1).PayType = "I" Or bTBIO Then
                                    bLastBatchWasDomestic = False
                                Else
                                    bLastBatchWasDomestic = True
                                End If


                                'Used in every applicationheader
                                If Not oBatch.DATE_Production = "19900101" Then
                                    sTransDate = Right(oBatch.DATE_Production, 4) '(oBatch.DATE_Production, "MMDD")
                                Else
                                    'If not imported set like computerdate
                                    sTransDate = VB6.Format(Now, "MMDD")
                                End If
                                'bFirstLine is used to set the sequenceno in the aplicationheader. If true seqno=1
                                'bFirstLine = False
                                'bFirstMassTransaction is used as a flag to treat the first mass transaction specially
                                'We have to create a BETFOR21-transaction before the first mass transaction (BETFOR22).
                                bFirstMassTransaction = True

                                'Reset count of BETFOR-records
                                If bWriteBETFOR00 Then
                                    nNoOfBETFOR = 0
                                    If oBatch.VB_ProfileInUse Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType = 1 Or oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem Then
                                            ' JanP 25.10.01 added ....FromAccountingSystem=True
                                            If bFirstBatch Then
                                                bFirstLine = True
                                            Else
                                                ' Keep on with daysequence until file end
                                                bFirstLine = False
                                            End If
                                        Else
                                            ' Reset day sequence for each betfor00
                                            bFirstLine = True
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bFirstLine = True
                                        Else
                                            ' Keep on with daysequence until file end
                                            bFirstLine = False
                                        End If
                                    End If
                                End If
                                i = i + 1

                                'If i = 1 Then
                                '    bFirstLine = True
                                'End If

                                'Get the companyno either from a property passed from BabelExport, or
                                ' from the imported file
                                If sCompanyNo <> "" Then
                                    sI_EnterpriseNo = PadLeft(sCompanyNo, 11, "0")
                                Else
                                    sI_EnterpriseNo = PadLeft(oBatch.I_EnterpriseNo, 11, "0")
                                End If
                                'BETFOR00
                                If bWriteBETFOR00 Then
                                    sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo)
                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    bFirstBatch = False

                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    xAccount = oBatch.Payments(1).I_Account ' Spec. for 0099-format

                                End If
                                j = 0
                                k = 0
                                lMassCounter = 1
                                For Each oPayment In oBatch.Payments

                                    'XNET - 22.04.2013 - Special kode for SEB because they have some deviations to the TBIO fileformat
                                    If sSpecial = "SEB" And bThisIsTBIO Then
                                        If sOldAccountNo <> oPayment.I_Account Then
                                            For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                For Each oaccount In oClient.Accounts
                                                    If oaccount.Account = oPayment.I_Account Then
                                                        sI_SWIFTCode = oaccount.SWIFTAddress
                                                        bFoundClient = True
                                                        Exit For
                                                    End If
                                                Next
                                                If bFoundClient Then
                                                    sOldAccountNo = oPayment.I_Account
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    End If

                                    '---------------------------------------
                                    ' Special for betfor0099-format;
                                    ' Surround each account with BETFOR00/99
                                    ' Ref NorgesGruppen
                                    '---------------------------------------
                                    If SurroundWith0099 Then
                                        If oPayment.I_Account <> xAccount Then
                                            If nNoOfBETFOR > 0 Then
                                                sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                nNoOfBETFOR = 0

                                                sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            End If
                                            xAccount = oPayment.I_Account ' Spec. for 0099-format
                                        End If
                                    End If

                                    bExportoPayment = False
                                    bLastPaymentWasMass = False


                                    'New 28.05.2009
                                    'LSK-STR�MMEN 2-1 e.e.o.
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.PaymentIsAccordingToQualifiers(iFormat_ID, bMultiFiles, sClientNo, True, sOwnRef) Then
                                        'Special test for this format - Usually we don't need to use the bExportPayment at all
                                        If sSpecial = "CRENO_OCRTP" Then
                                            bExportoPayment = False
                                            If oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then
                                                bExportoPayment = True
                                            End If
                                        Else
                                            bExportoPayment = True

                                        End If
                                    End If

                                    'Old code
                                    '''''                        'Have to go through the payment-object to see if we have objects thatt shall
                                    '''''                        ' be exported to this exportfile
                                    '''''                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    '''''                            If Not bMultifiles Then
                                    '''''                                'If oBabel.Special = "CRENO_OCRTP" Then
                                    '''''                                If sSpecial = "CRENO_OCRTP" Then
                                    '''''                                    If oPayment.MATCH_Matched = MatchStatus.Matched Then
                                    '''''                                        bExportoPayment = True
                                    '''''                                    End If
                                    '''''                                Else
                                    '''''                                    bExportoPayment = True
                                    '''''                                End If
                                    '''''                            Else
                                    '''''                                If oPayment.VB_ClientNo = sClientNo Then
                                    '''''                                    If InStr(oPayment.REF_Own, "&?") Then
                                    '''''                                        'Set in the part of the OwnRef that BabelBank is using
                                    '''''                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                    '''''                                    Else
                                    '''''                                        sNewOwnref = ""
                                    '''''                                    End If
                                    '''''                                    If sNewOwnref = sOwnRef Then
                                    '''''                                        bExportoPayment = True
                                    '''''                                    End If
                                    '''''                                End If
                                    '''''                            End If
                                    '''''                        End If

                                    If bExportoPayment Then
                                        j = j + 1
                                        'Test what sort of transaction
                                        If oPayment.PayType = "I" Or bTBIO Then
                                            'International payment
                                            sLine = WriteBETFOR01(oPayment, nSequenceNoTotal, sCompanyNo, sSpecial)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            'XNET - 22.04.2013 - Added two new parameters
                                            sLine = WriteBETFOR02(oPayment, nSequenceNoTotal, sCompanyNo, sSpecial, sI_SWIFTCode)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            sLine = WriteBETFOR03(oPayment, nSequenceNoTotal, sCompanyNo)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            'Remove later: ExportCheck
                                            oPayment.Exported = True
                                            'Have to reset masstrans flag
                                            bFirstMassTransaction = True
                                        ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                            If oPayment.Payment_ID <> sPayment_ID Then
                                                bFirstMassTransaction = True
                                            End If
                                            If bFirstMassTransaction Then
                                                'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                If bExportMassRecords Then
                                                    'New code 10.07.2002
                                                    'Have to sumarize alle invoices/payments, to put the
                                                    'amount in Betfor21.
                                                    nMassTransferredAmount = 0
                                                    'Assumes that all BETFOR22 records in the imported file within
                                                    ' a BETFOR21 has the same PAYMENT_ID (also Invoice_ID).
                                                    sPayment_ID = oPayment.Payment_ID
                                                    bContinueSummarize = True
                                                    'Reset counter for BETFOR22-records
                                                    k = 0
                                                    Do While bContinueSummarize
                                                        If lMassCounter <= oBatch.Payments.Count Then
                                                            oMassPayment = oBatch.Payments.Item(lMassCounter)
                                                            'Have to go through the payment-object to see if the next payment also is
                                                            ' a masspayment from same account, paymentdate, filenameout and ownref
                                                            'If so add them together
                                                            If oMassPayment.VB_FilenameOut_ID = iFormat_ID And (oMassPayment.PayType = "S" Or oMassPayment.PayType = "M") Then
                                                                If oMassPayment.I_Account = sI_Account Then
                                                                    If oMassPayment.Payment_ID = sPayment_ID Then
                                                                        If InStr(oMassPayment.REF_Own, "&?") Then
                                                                            'Set in the part of the OwnRef that BabelBank is using
                                                                            sNewOwnref = Right(oMassPayment.REF_Own, Len(oMassPayment.REF_Own) - InStr(oMassPayment.REF_Own, "&?") - 1)
                                                                        Else
                                                                            sNewOwnref = ""
                                                                        End If
                                                                        If sNewOwnref = sOwnRef Then
                                                                            bSumMassPayment = True
                                                                        End If
                                                                    Else
                                                                        Exit Do
                                                                    End If
                                                                Else
                                                                    Exit Do
                                                                End If
                                                            Else
                                                                Exit Do
                                                            End If
                                                            If bSumMassPayment Then
                                                                For Each oMassInvoice In oMassPayment.Invoices
                                                                    nMassTransferredAmount = nMassTransferredAmount + oMassInvoice.MON_TransferredAmount
                                                                    lMassCounter = lMassCounter + 1
                                                                Next oMassInvoice
                                                            End If
                                                        Else
                                                            Exit Do
                                                        End If
                                                    Loop  'oMassPayment

                                                    'End new code

                                                    'BETFOR21 for Masstrans
                                                    sLine = WriteBETFOR21Mass(oPayment, nSequenceNoTotal, sCompanyNo, nMassTransferredAmount)
                                                    'First mass-transaction treated
                                                    bFirstMassTransaction = False
                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                    'Remove later: ExportCheck
                                                    oPayment.Exported = True
                                                End If
                                            End If
                                        Else
                                            'BETFOR21 for invoicetrans
                                            sLine = WriteBETFOR21Invoice(oPayment, nSequenceNoTotal, sCompanyNo)
                                            'Have to reset masstrans flag
                                            bFirstMassTransaction = True
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            'Remove later: ExportCheck
                                            oPayment.Exported = True
                                        End If
                                        'k = 0  Only one invoice pr payment for masspayments!
                                        'If its a payment to an own account we shall just create a BETFOR21, not 22 or 23
                                        ' Changed 19,08.05 BUT this goes for Domestic only. For Int., create BETFOR04!!!!
                                        If oPayment.ToOwnAccount = False Or oPayment.PayType = "I" Then
                                            For Each oInvoice In oPayment.Invoices
                                                'k will be used as a sequential number.
                                                k = k + 1
                                                If oPayment.PayType = "I" Or bTBIO Then
                                                    'Will create a BETFOR04 transaction
                                                    sLine = WriteBETFOR04(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo)
                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                                    'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                    If bExportMassRecords Then
                                                        'Will create a BETFOR22 transaction
                                                        sLine = WriteBETFOR22(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo)
                                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                        oPayment.Exported = True
                                                    End If
                                                    bLastPaymentWasMass = True
                                                Else
                                                    'Will create a BETFOR23 transaction
                                                    iCount = 0
                                                    Do While iCount - 1 < oInvoice.Freetexts.Count
                                                        If iCount = 0 Then
                                                            iCount = 1
                                                            'nNoOfBETFOR = nNoOfBETFOR - 1
                                                            'nSequenceNoTotal = nSequenceNoTotal - 1
                                                        End If
                                                        If Val(oPayment.StatusCode) > 99 Then
                                                            If Val(CStr(oBabel.BankByCode)) = BabelFiles.Bank.DnB Then 'And oPayment.Invoices.Count = 0 Then
                                                                ' Changed 14.11.2003
                                                                ' Sometimes DnB has a BETFOR23, sometimes not for deleted transactions
                                                                If oInvoice.Freetexts.Count > 0 Or oInvoice.Unique_Id <> "" Then
                                                                    If bFileFromBank Then
                                                                        oPayment.StatusCode = "02"
                                                                    Else
                                                                        oPayment.StatusCode = "00"
                                                                    End If
                                                                    sLine = WriteBETFOR23((oPayment.I_Account), oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo)
                                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                Else
                                                                    Exit For ' No KID, no Freetext, write no betfor23
                                                                End If
                                                            Else
                                                                If bFileFromBank Then
                                                                    oPayment.StatusCode = "02"
                                                                Else
                                                                    oPayment.StatusCode = "00"
                                                                End If
                                                                sLine = WriteBETFOR23((oPayment.I_Account), oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo)
                                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                            End If
                                                        Else
                                                            sLine = WriteBETFOR23((oPayment.I_Account), oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo)
                                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                        End If
                                                    Loop
                                                End If
                                            Next oInvoice
                                        End If

                                    End If
                                Next oPayment ' payment


                                ' Added by janP 25.10.01
                                'If bExportMassRecords Or bWriteBETFOR99Later Then
                                If Not (bExportMassRecords = False And bLastPaymentWasMass) Then
                                    sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                    If oBatch.VB_ProfileInUse = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                        Else
                                            bWriteBETFOR99Later = True
                                        End If
                                    Else
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    End If
                                Else
                                    '    bWriteBETFOR99Later = True
                                    ' Masspayments and bExportMassRecords=False,
                                    ' Test if we had any non-mass payments before.
                                    If nNoOfBETFOR > 0 Then
                                        ' We had invoice-records before the mass-records.
                                        ' Must write Betfor99
                                        sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    End If
                                End If
                                'We have to save the last used sequenceno. in some cases
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then
                                    'Then if it is a return-file.
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'No action
                                        'If oBabel.Special = "CRENO_OCRTP" Then
                                        If sSpecial = "CRENO_OCRTP" Then
                                            'Always use sequences at the filesetup-level
                                            If bWriteBETFOR99Later Then
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                                sDummy = ApplicationHeader(False, "-1")
                                            Else
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal - 1
                                            End If
                                            oBatch.VB_Profile.Status = 2
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                            'We have to omit BETFOR00 in the future (if we don't change between domestic/international)
                                            bWriteBETFOR00 = False
                                        End If
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                'No action
                                                'Use the seqno at filesetup level
                                            Case 1
                                                If bWriteBETFOR99Later Then
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                                    sDummy = ApplicationHeader(False, "-1")
                                                Else
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal - 1
                                                End If
                                                oBatch.VB_Profile.Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                                'We have to omit BETFOR00 in the future (if we don't change between domestic/international)
                                                bWriteBETFOR00 = False
                                                ' New by JanP 26.11.01
                                                ' Must also reduce day-sequence by one, since it has been added
                                                ' one in WriteBetfor99, which will never be used!
                                                ' ??????? WRONG ????? Moved to bWriteBETFOR99Later, up!sDummy = ApplicationHeader(False, "-1")
                                                'Use seqno per client per filesetup
                                            Case 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                                oBatch.VB_Profile.Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                        End Select
                                    End If
                                End If

                            End If
                            'bLastBatchWasDomestic = False 'Changed by JanP 29.10
                            'bFirstBatch = False  ' Changed by Janp 25.10
                        End If  ' XNET 25.05.2011 added End If
                    Next oBatch 'batch

                End If
                'Removed 28.05.2009
                'End If

            Next oBabel 'Babelfile

            'Have to write BETFOR99 if keepbatch is true
            If bWriteBETFOR99Later Then
                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                'Added 09.08.2006
                'If seqno at clientlevel and keepbatches is set to false then we we save
                ' nSequenceNoTotal - 1, but when BETFOR99 is written we must save nSequenceNoTotal
                'First, check if we have at least 1 Babelfile
                If oBabelFiles.Count > 0 Then
                    'Then check if we use a profile
                    If oBabelFiles.Item(1).VB_ProfileInUse = True Then
                        'Then if it is a send-file.
                        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = False Then

                            'Then if it is seqno at the client-level
                            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).SeqnoType = 2 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.Item().VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                oBabelFiles.Item(1).VB_Profile.Status = 2
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.Item().VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Status = 2
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.Item().VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                            End If
                        End If
                    End If

                End If
            End If

            'For those who use vbBabel
            nSequenceNumberEnd = nSequenceNoTotal

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteTelepayFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteTelepayFile = True

    End Function

    Private Function WriteBETFOR00(ByRef oBatch As Batch, ByRef bFirstLine As Boolean, ByRef nSequenceNoTotal As Double, ByRef sBranch As String, ByRef sCompanyNo As String) As String

        Dim sLine As String

        sLine = ""
        'Build AH, send Returncode
        'bFirstLine is used to set the sequenceno in the aplicationheader.
        sLine = ApplicationHeader(bFirstLine, (oBatch.StatusCode))
        sLine = sLine & "BETFOR00"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If sBranch <> "" Then
            'Uses Branch from the database
            ' New 04.11.05 to be able to redo from branch to blank branch
            If UCase(sBranch) = "INGEN" Or UCase(sBranch) = "BLANK" Then
                sBranch = Space(11)
            End If
            sLine = sLine & PadRight(sBranch, 11, " ")
        Else
            sLine = sLine & PadRight(oBatch.I_Branch, 11, " ")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & Space(6)
        If Len(oBatch.DATE_Production) = 8 And oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right(oBatch.DATE_Production, 4) 'oBatch.DATE_Production
        Else
            sLine = sLine & VB6.Format(Now, "MMDD")
        End If
        sLine = sLine & Space(10)
        sLine = sLine & "VERSJON002"
        sLine = sLine & Space(10)
        sLine = sLine & PadLeft(oBatch.OperatorID, 11, " ")
        'No use of SIGILL
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 26)
        sLine = sLine & Space(1)
        sLine = sLine & Space(143)
        'sLine = sLine & Space(25)
        ' changed 13.07.06
        If Len(oBatch.REF_Own) > 0 Then
            sLine = sLine & PadRight(oBatch.REF_Own, 15, " ") ' 13.09.06 changed from padleft
        Else
            sLine = sLine & Space(15) ' Egenreferanse - Added 13.07.06
        End If
        sLine = sLine & Space(9)


        WriteBETFOR00 = sLine

    End Function
    Private Function WriteBETFOR01(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sSpecial As String) As String

        Dim sLine As String
        'Dim sPayCode As String, iCount As Integer
        'Dim oInvoice As Invoice

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR01"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        If bTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & Right(oPayment.DATE_Payment, 6) 'Format(oPayment.DATE_Payment, "YYMMDD")
        'sLine = sLine & PadRight(oPayment.REF_Own, 30, " ")
        'XNET 22.04.2013 - Added next IF
        If sSpecial = "SEB" Then
            sLine = sLine & Left$(oPayment.REF_Own + Space(17), 17)
            sLine = sLine & Space(13)
        Else
            sLine = sLine & Left$(oPayment.REF_Own + Space(30), 30)
        End If
        sLine = sLine & PadRight(oPayment.MON_TransferCurrency, 3, " ")
        If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
            'FIXERROR: Babelerror
        Else
            sLine = sLine & oPayment.MON_InvoiceCurrency
        End If
        If oPayment.MON_ChargeMeAbroad = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        If oPayment.MON_ChargeMeDomestic = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        ' This is notification by receiving bank
        ' 07.07.06 Added Or  oPayment.ToOwnAccount
        If oPayment.NOTI_NotificationParty = 2 Or oPayment.ToOwnAccount Then
            ' new 19.01.05 for TEEKAY
            If oPayment.ToOwnAccount Then
                'sLine = sLine & PadRight("/REC/KONCERN/", 30, " ")
                ' New 07.07.06
                'If oPayment.Cargo.Special = "TEEKAY" Or _
                ''    oPayment.Cargo.Special = "DNBANOKK" Or _
                ''    oPayment.Cargo.Special = "PGSUK" _
                '' changed 23.11.06 and 18.01.2010
                If sSpecial = "TEEKAY" Or sSpecial = "DNBANOKK" Or sSpecial = "TELENORKEYPARTNER" Or sSpecial = "PGSUK" Then
                    sLine = sLine & PadRight("/REC/KONCERN/", 30, " ")
                Else
                    sLine = sLine & PadRight("/REC/INTC/", 30, " ")
                End If
            Else
                '   sLine = sLine & Space(30)
                'End If
                'Else
                ' Also notification by RECEIVING BANK
                Select Case oPayment.NOTI_NotificationType
                    Case "PHONE"
                        sLine = sLine & "PHONE" & PadRight(oPayment.NOTI_NotificationIdent, 25, " ")
                    Case "TELEX"
                        sLine = sLine & "TELEX" & PadRight(oPayment.NOTI_NotificationIdent, 25, " ")
                    Case "OTHER"
                        sLine = sLine & "OTHER" & PadRight(oPayment.NOTI_NotificationIdent, 25, " ")
                    Case Else
                        sLine = sLine & Space(30)
                End Select
            End If
        Else
            sLine = sLine & Space(30)
        End If
        'sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
        If oPayment.Priority = True Then
            sLine = sLine & "J"
        Else
            sLine = sLine & " "
        End If
        sLine = sLine & PadLeft(CStr(Int(oPayment.ERA_ExchRateAgreed)), 4, "0")
        ' Changed 21.10.05 - gave wrong results
        'sLine = sLine & PadRight(oPayment.ERA_ExchRateAgreed - Int(oPayment.ERA_ExchRateAgreed), 4, "0")
        sLine = sLine & PadLeft(CStr(oPayment.ERA_ExchRateAgreed * 10000 - (Int(oPayment.ERA_ExchRateAgreed) * 10000)), 4, "0")

        'sLine = sLine & PadLeft(oPayment.ERA_ExchRateAgreed, 8, "0")
        sLine = sLine & PadRight(oPayment.FRW_ForwardContractNo, 6, " ")
        sLine = sLine & PadLeft(CStr(Int(oPayment.FRW_ForwardContractRate)), 4, "0")
        'Changed 20.09.05, did not work correctly
        'sLine = sLine & PadRight(oPayment.FRW_ForwardContractRate - Int(oPayment.FRW_ForwardContractRate), 4, "0")
        sLine = sLine & PadLeft(CStr(oPayment.FRW_ForwardContractRate * 10000 - (Int(oPayment.FRW_ForwardContractRate) * 10000)), 4, "0")

        If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
            sLine = sLine & " "
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToPayer Then
            sLine = sLine & "2" ' 27.05.04 Changed from 0 - Error before 27.05.04!!!
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToReceiver Then
            sLine = sLine & "1"
        End If
        sLine = sLine & New String("0", 6) & Space(2)
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000"
        Else
            If oPayment.MON_TransferCurrency = oPayment.MON_AccountCurrency Then
                If oPayment.MON_TransferCurrency <> "NOK" Then
                    'If so use the local exchangerate.
                    sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 4, "0")
                    sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8)), ",", ""), 2), 8, "0")
                Else
                    sLine = sLine & "000000000000"
                End If
                ' Changed 09.09.04
                'ElseIf oPayment.MON_AccountExchRate <> oPayment.MON_LocalExchRate Then
            Else
                sLine = sLine & PadLeft(CStr(Int(oPayment.MON_AccountExchRate)), 4, "0")
                sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_AccountExchRate - Int(oPayment.MON_AccountExchRate), 8)), ",", ""), 2), 8, "0")
                'Else
                '    sLine = sLine & "000000000000"
            End If
            'sLine = sLine & PadLeft(oPayment.MON_AccountExchRate, 12, "0")  'Relle kurs, 191-202
        End If
        sLine = sLine & Space(12) 'FIXED from 16
        If oPayment.MON_AccountAmount = 0 Then 'Belastest bel�p 215-230
            sLine = sLine & New String("0", 16)
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_AccountAmount), 16, "0")
        End If
        If oPayment.MON_TransferredAmount = 0 Then
            sLine = sLine & New String("0", 16)
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 16, "0")
        End If
        sLine = sLine & Space(5)
        sLine = sLine & PadLeft(oPayment.REF_Bank2, 6, "0")
        sLine = sLine & PadRight(oPayment.ERA_DealMadeWith, 6, " ")
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)
        End If
        'FIX: This field may still be in use
        sLine = sLine & Space(1)
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & New String("0", 6)
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & PadLeft(Format(oPayment.MON_ChargesAmount, "###0"), 9, "0")
        'sLine = sLine & PadLeft(Int(oPayment.MON_ChargesAmount), 7, "0")
        'sLine = sLine & PadLeft(Mid(Replace(oPayment.MON_ChargesAmount - Int(oPayment.MON_ChargesAmount), ",", ""), 2), 2, "0")
        'sLine = sLine & PadLeft(oPayment.MON_ChargesAmount, 9, "0")
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000"
        Else
            If (oPayment.MON_LocalExchRate = 100 And oPayment.MON_TransferCurrency <> "NOK") Or oPayment.MON_LocalExchRate = 100 And oPayment.MON_TransferCurrency = "NOK" Then
                sLine = sLine & "000000000000"
            Else
                sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 4, "0")
                sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8)), ",", ""), 2), 8, "0")
            End If
        End If
        'sLine = sLine & PadLeft(oPayment.MON_LocalExchRate, 12, "0")
        sLine = sLine & Space(28)

        WriteBETFOR01 = UCase(sLine)


    End Function
    'XNET - 22.04.2013 - Added 2 new optional parameters
    Private Function WriteBETFOR02(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, Optional ByVal sSpecial As String = "", Optional ByVal sI_Swift As String = "") As String
        Dim sAccount As String
        Dim sLine As String

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR02"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If bTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ")
        sLine = sLine & PadRight(oPayment.BANK_Name, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_Adr1, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_Adr2, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_Adr3, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCodeCorrBank, 11, " ")
        If Len(oPayment.BANK_CountryCode) <> 2 Then
            'FIXERROR Babelerror, warning not compulsory?
            sLine = sLine & "  "
        Else
            sLine = sLine & oPayment.BANK_CountryCode
        End If

        ' 29.01.2008 - added test for IBANN. Strip down from IBANN to accountno for TBIO
        ' XNET 30.11.2011 -
        ' THIS NEXT If MUST BE COMPLETELY WRONG _ WHY FUCK UP E_ACCOUNT HERE _ IT IS USED IN BETFOR03
        ' HAVE COMMENTED WHOLE If-BLOCK
        '    If bTBIO Then
        '        If IsIBANNumber(oPayment.E_Account, False, False) Then
        '            If IsPaymentDomestic(oPayment) Then
        '                sAccount = oPayment.E_Account
        '                ' strip from Ibann to AccountNo
        '                    'XOKNET - 16.11.2010 - Changed the name of the function below
        '                    If Not ExtractInfoFromIBAN(oPayment) Then
        '                    'If Not ExtractAccountFromIBANN(oPayment) Then
        '                        ' something failed in the function, keep IBANN
        '                        oPayment.E_Account = sAccount
        '                    End If
        '            End If
        '        End If
        '    End If

        ' 01.03.06 - removed next 4 lines - TBIO-account is now from pos 260-294
        'If bTBIO Then
        '    sAccount = Trim$(oPayment.I_Account)
        '    sLine = sLine & PadRight(sAccount, 35, " ")
        'Else
        ' Telepay "classic"
        ' New by JanP 08.03.2002
        ' Reserved space for branchNo, 245,15
        ' In Telepay format, must add //SC, //FW, etc
        Select Case oPayment.BANK_BranchType
            Case BabelFiles.BankBranchType.Fedwire
                oPayment.BANK_BranchNo = "FW" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.SortCode
                oPayment.BANK_BranchNo = "SC" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.Bankleitzahl
                oPayment.BANK_BranchNo = "BL" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.Chips
                oPayment.BANK_BranchNo = "CH" & oPayment.BANK_BranchNo
        End Select

        sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")

        ' 01.03.06 added 7 next lines TBIO-account is now from pos 260-294
        'XNET - 22.04.2013 - Added next IF
        If sSpecial = "SEB" Then
            sAccount = Trim$(oPayment.I_Account)
            sAccount = sI_Swift & "/" & sAccount
            sLine = sLine & PadRight(sAccount, 35, " ")
        ElseIf bTBIO Then
            sAccount = Trim$(oPayment.I_Account)
            sLine = sLine & PadRight(sAccount, 35, " ")
        Else
            sLine = sLine & Space(35)
        End If
        sLine = sLine & Space(26)
        'End If
        '  removed 01.03.06 sLine = sLine & Space(41)

        WriteBETFOR02 = UCase(sLine)

    End Function
    Private Function WriteBETFOR03(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        'Dim sPayCode As String, iCount As Integer
        'Dim oInvoice As Invoice
        Dim sAccount As String


        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR03"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If bTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")


        ' 16.04.2008 - added test for IBANN. Strip down from IBANN to accountno for TBIO
        ' XNET 30.11.2011
        ' Removed If-test below - we must use IBAN, f.ex. in Finland
        '    If bTBIO Then
        '        If IsIBANNumber(oPayment.E_Account, False, False) Then
        '            If IsPaymentDomestic(oPayment) Then
        '                sAccount = oPayment.E_Account
        '                ' strip from Ibann to AccountNo
        '                    'XOKNET - 16.11.2010 - Changed the name of the function below
        '                    If Not ExtractInfoFromIBAN(oPayment) Then
        '                    'If Not ExtractAccountFromIBANN(oPayment) Then
        '                        ' something failed in the function, keep IBANN
        '                        oPayment.E_Account = sAccount
        '                    End If
        '            End If
        '        End If
        '    End If

        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
        If oPayment.E_Name = vbNullString Then
            'FIXERROR: Babelerror
        Else
            sLine = sLine & PadRight(oPayment.E_Name, 35, " ")
        End If
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ")
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ")
        sLine = sLine & PadRight(oPayment.E_Adr3, 35, " ")
        If Len(oPayment.E_CountryCode) <> 2 Then
            If Len(oPayment.BANK_CountryCode) = 2 Then
                sLine = sLine & oPayment.BANK_CountryCode
            Else
                'Maybe we should raise an error!
                sLine = sLine & "  "
            End If
        Else
            sLine = sLine & oPayment.E_CountryCode
        End If

        ' This is notification by payers bank
        If oPayment.NOTI_NotificationParty = 1 Or oPayment.NOTI_NotificationParty = 3 Then

            'Receiver shall be notified by payors bank
            If oPayment.NOTI_NotificationType = "TELEX" Then
                If oPayment.NOTI_NotificationIdent = vbNullString Or oPayment.NOTI_NotificationAttention = vbNullString Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "T"
                    'Both Telex countrycode and number are stored in Ident.
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 20, " ")
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
                End If
            ElseIf oPayment.NOTI_NotificationType = "FAX" Then
                If oPayment.NOTI_NotificationIdent = vbNullString Or oPayment.NOTI_NotificationAttention = vbNullString Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "F" & "  "
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 18, " ")
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
                End If
            Else
                sLine = sLine & Space(41)
            End If
        Else
            'No notification by payors bank
            sLine = sLine & Space(41)
        End If
        'Old code
        'sLine = sLine & PadRight(oPayment.Telex_CountryCode, 2, " ")
        'If oPayment.NOTI_NotificationParty <> vbnullstring And oPayment.TelexNo = vbnullstring Then
        'FIXERROR: Babelerror
        'Else
        '    sLine = sLine & PadRight(oPayment.TelexNo, 18, " ")
        'End If
        'If oPayment.NOTI_NotificationParty <> vbnullstring And oPayment.NOTI_NotificationAttention = vbnullstring Then
        'FIXERROR: Babelerror
        'Else
        'sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
        'End If
        sLine = sLine & Space(22)

        WriteBETFOR03 = UCase(sLine)

    End Function
    Private Function WriteBETFOR04(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        Dim iCount As Short
        Dim oFreeText As Freetext

        iCount = 0
        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR04"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If bTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")
        For Each oFreeText In oInvoice.Freetexts
            iCount = iCount + 1
            If iCount > 1 Then
                'FIXERROR: Can't be more than 1 textlines in Telepay International
            Else
                ' 18.01.2010 added KID Utland
                If Not EmptyString((oInvoice.Unique_Id)) Then
                    sLine = sLine & Left(oInvoice.Unique_Id & Space(35), 35) '81
                Else
                    sLine = sLine & Left(oFreeText.Text & Space(35), 35)
                End If
            End If
        Next oFreeText
        ' added 18.01.2010
        ' There may be KID only, no freetext!
        If oInvoice.Freetexts.Count = 0 And Not EmptyString((oInvoice.Unique_Id)) Then
            iCount = iCount + 1
            sLine = sLine & Left(oInvoice.Unique_Id & Space(35), 35) '81
        End If
        ' New 19.04.05
        ' Problem if no freetexts - will then miss 35 characters in output!
        If iCount = 0 Then
            sLine = sLine & Space(35)
        End If

        'sLine = sLine & PadRight(oInvoice.REF_Own, 35, " ")
        sLine = sLine & Left(oInvoice.REF_Own & Space(35), 35)
        sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")
        If oInvoice.MON_InvoiceAmount < 0 Then
            sLine = sLine & "K"
        Else
            sLine = sLine & "D"
        End If
        If bTBIO Then
            sLine = sLine & Space(66)
        Else
            sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 6, " ")
            sLine = sLine & PadRight(oInvoice.STATEBANK_Text, 60, " ")
        End If
        'FIX: To own account is not imported from Telepay
        ' Endret 19.01.05, ref TEEKAY
        If oPayment.ToOwnAccount Then
            sLine = sLine & "J"
        Else
            sLine = sLine & Space(1)
        End If
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 6)
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 6)
        If Not EmptyString((oInvoice.Unique_Id)) Then
            sLine = sLine & Space(45)
            sLine = sLine & "K" '293 KID Utland
        Else
            ' as pre 18.01.2010
            sLine = sLine & Space(46)
        End If
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & "000"
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
        End If
        sLine = sLine & Space(24)

        WriteBETFOR04 = UCase(sLine)


    End Function

    Private Function WriteBETFOR21Invoice(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        Dim sPayCode As String
        Dim iCount As Short
        Dim oInvoice As Invoice

        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY")
        ' "New" PayCode 603 when no account specified
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Then
            sPayCode = "603"
        End If

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR21"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & Right(oPayment.DATE_Payment, 6) 'Format(oPayment.DATE_Payment, "YYMMDD")
        'sLine = sLine & PadRight(oPayment.REF_Own, 30, " ")
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30)
        sLine = sLine & Space(1)
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Or oPayment.PayCode = "160" Then
            sLine = sLine & "00000000019"
        Else
            sLine = sLine & PadRight(oPayment.E_Account, 11, " ")
        End If
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ")
        sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ")
        sLine = sLine & PadRight(oPayment.E_Adr2, 30, " ")
        'FIXERROR: Babelerror if E_Zip longer than 4 or alfanumeric
        ' Removed 13.06.05
        'If Len(Trim$(oPayment.E_Zip)) = 0 Then
        'sLine = sLine & "0579"  ' In Telepay Zip is mandatory
        'Else
        sLine = sLine & PadRight(oPayment.E_Zip, 4, " ")
        'End If
        ' Removed 13.06.05
        'If Len(Trim$(oPayment.E_City)) = 0 Then
        '    sLine = sLine & PadRight("OSLO", 26, " ")  ' In Telepay City is mandatory
        'Else
        sLine = sLine & PadRight(oPayment.E_City, 26, " ")
        'End If
        'The amount if its a payment to own account
        If oPayment.ToOwnAccount = True Then
            iCount = 0
            For Each oInvoice In oPayment.Invoices
                iCount = iCount + 1
                If iCount > 1 Then
                    'FIXERROR: Can't be more than 1 invoice when it is a payment to own account
                Else
                    sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 15, "0")
                End If
            Next oInvoice
        Else
            sLine = sLine & New String("0", 15)
        End If
        sLine = sLine & sPayCode

        'FIX: Changed by Jan-Petter 28.06 due to no value from CodaImport
        ' Should be added in the importformat.
        ' changed again 23.06.05 by JanP because "E" was not set for CodaImport
        'If oPayment.PayType = "" Then
        '    sLine = sLine & "F"
        'Else
        If oPayment.ToOwnAccount = True Then
            sLine = sLine & "E"
        Else
            sLine = sLine & "F"
        End If
        'End If
        '------------------------------
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
            If bFileFromBank Then
                sLine = Left(sLine, 3) & "02" & Mid(sLine, 7)
            Else
                sLine = Left(sLine, 3) & "00" & Mid(sLine, 7)
            End If
        Else
            sLine = sLine & Space(1)
        End If
        'If this file shall be sent to the bank the amount shall be set to 0.
        If oPayment.StatusCode = "00" Then 'Amount = 0 Then
            sLine = sLine & New String("0", 15)
        ElseIf oPayment.StatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank Then
                sLine = sLine & New String("0", 15)
            Else
                sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If
        sLine = sLine & New String("0", 5)
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & New String("0", 6)
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & Space(26)


        WriteBETFOR21Invoice = UCase(sLine)


    End Function
    Private Function WriteBETFOR21Mass(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef nTransferredAmount As Double) As String

        Dim sLine As String
        Dim sPayCode As String ', iCount As Integer
        'Dim oInvoice As Invoice


        sLine = ""
        'FIX: Have to add some parameters, i.e. Returncode
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR21"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY", "M")
        If sPayCode = "621" Then
            ' Mass payments, use Ownref as text on receivers statement
            ' Need to put Ownref into Babels Text_E_Statement
            oPayment.REF_Own = oPayment.Text_E_Statement
        End If

        sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & Right(oPayment.DATE_Payment, 6) 'Format(oPayment.DATE_Payment, "YYMMDD")
        'sLine = sLine & PadRight(oPayment.REF_Own, 30, " ")
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30)
        sLine = sLine & Space(1)
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        If Len(Trim(oPayment.E_Account)) = 11 Then
            sLine = sLine & Space(11) 'PadRight(oPayment.E_Account, 11, " ")
        Else
            sLine = sLine & "00000000000"
        End If
        sLine = sLine & Space(30) 'PadRight(oPayment.E_Name, 30, " ")
        sLine = sLine & Space(30) 'PadRight(oPayment.E_Adr1, 30, " ")
        sLine = sLine & Space(30) 'PadRight(oPayment.E_Adr2, 30, " ")
        'FIXERROR: Babelerror if E_Zip longer than 4 or alfanumeric
        sLine = sLine & "0000" 'PadRight(oPayment.E_Zip, 4, " ")
        sLine = sLine & Space(26) 'PadRight(oPayment.E_City, 26, " ")
        'The amount if its a payment to own account
        sLine = sLine & New String("0", 15)

        sLine = sLine & sPayCode
        If oPayment.PayType = "S" Then 'Salary
            sLine = sLine & "L"
        Else
            sLine = sLine & "M"
        End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)
        End If
        If oPayment.StatusCode <> "02" Then 'No amount in betfor21 in sendfiles
            sLine = sLine & New String("0", 15)
        Else
            sLine = sLine & PadLeft(Str(nTransferredAmount), 15, "0")
            'sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If
        sLine = sLine & Space(5)
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & Space(6)
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & Space(26)


        WriteBETFOR21Mass = UCase(sLine)


    End Function

    Private Function WriteBETFOR23(ByRef sI_Account As String, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef iCount As Short, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        'Dim oFreeText As FreeText
        Dim iCountLocal As Short


        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR23"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(sI_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")
        'Can't have both Freetext and KID
        iCountLocal = 0
        If oInvoice.Freetexts.Count >= iCount Then
            For iCountLocal = 1 To 3
                If oInvoice.Freetexts.Count >= iCount Then
                    ' 20.09.05 -  Changed, in case freetext.text is not 40 pos
                    'sLine = sLine & oInvoice.Freetexts(iCount).Text
                    'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.Freetexts().Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sLine = sLine & PadRight(Left(oInvoice.Freetexts(iCount).Text, 40), 40, " ")
                    iCount = iCount + 1
                End If
            Next iCountLocal
            sLine = PadLine(sLine, 227, " ")
        ElseIf oInvoice.Unique_Id <> "" Then
            sLine = sLine & Space(120)
            sLine = sLine & PadRight(oInvoice.Unique_Id, 27, " ")
        Else
            'If no freetext is given, the add a "."
            sLine = sLine & "." & Space(146)
        End If
        'sLine = sLine & PadRight(oInvoice.REF_Own, 30, " ")
        sLine = sLine & Left(oInvoice.REF_Own & Space(30), 30)
        If iCount > 4 Then
            sLine = sLine & New String("0", 15)
        Else
            sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")
        End If
        If iCount > 4 Then
            sLine = sLine & "D"
        ElseIf oInvoice.MON_InvoiceAmount < 0 Then
            sLine = sLine & "K"
        Else
            sLine = sLine & "D"
        End If
        sLine = sLine & Space(20)
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & "000"
        ElseIf oInvoice.StatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank = True Then
                sLine = sLine & "000"
            Else
                sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
        End If
        sLine = sLine & Space(24)

        WriteBETFOR23 = UCase(sLine)


    End Function
    Private Function WriteBETFOR22(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        'Dim iCount As Integer
        'Dim oFreeText As FreeText

        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR22"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadLeft(oInvoice.Invoice_ID, 6, " ")
        sLine = sLine & PadRight(oPayment.E_Account, 11, " ")
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ")
        sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)
        End If
        'sLine = sLine & PadRight(oInvoice.REF_Own, 35, " ")
        sLine = sLine & Left(oInvoice.REF_Own & Space(35), 35)
        sLine = sLine & Space(110)
        sLine = sLine & PadRight(oInvoice.Extra1, 10, " ")
        If oInvoice.StatusCode = "00" Or Len(Trim(oInvoice.StatusCode)) = 0 Then
            sLine = sLine & "0000"
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 4, "0")
        End If
        sLine = sLine & Space(24)

        WriteBETFOR22 = UCase(sLine)


    End Function
    Private Function WriteBETFOR99(ByRef oBatch As Batch, ByRef nBETFORnumber As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sVersion As String) As String

        Dim sLine As String


        sLine = ""
        'Build AH, send Returncode
        sLine = ApplicationHeader(False, (oBatch.StatusCode))

        sLine = sLine & "BETFOR99"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & Space(11)
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & Space(6)
        If oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right(oBatch.DATE_Production, 4) 'Format(oBatch.DATE_Production, "MMDD")
        Else
            sLine = sLine & VB6.Format(Now, "MMDD")
        End If
        sLine = sLine & Space(19)
        sLine = sLine & PadLeft(Str(nBETFORnumber + 1), 5, "0")
        'FIX: No use of AEGIS or SIGILL
        sLine = sLine & Space(188)
        ' Only four chars and four digits for versioninfo
        ' changed 16.04.2008
        If Len(Replace(sVersion, ".", "")) = 5 Then
            sLine = sLine & "BAB" & Replace(sVersion, ".", "")
        Else
            sLine = sLine & "BABL" & Left(Replace(sVersion, ".", ""), 1) & Right(Replace(sVersion, ".", ""), 3)
        End If
        sLine = PadRight(sLine, 320, " ") ' Fill until 320-record is ok


        WriteBETFOR99 = (sLine)

    End Function
    Function ApplicationHeader(ByRef bFirstLine As Boolean, ByRef sStatusCode As String) As String

        Dim sAH As String
        Static nSequenceNo As Double

        If sStatusCode = "-1" Then
            ' reduce nsequenceno by 1 because of unwritten records
            '(new bay janP 26.11.01
            nSequenceNo = nSequenceNo - 1
            sAH = ""
        Else

            If bFirstLine = True Then
                nSequenceNo = 1
            Else
                nSequenceNo = nSequenceNo + 1
            End If
            If sStatusCode = "" Then
                'The imported file was not Telepay
                If Not bFileFromBank Then
                    sStatusCode = "00"
                Else
                    sStatusCode = "02"
                End If
            End If

            sAH = ""
            ' Added next "if bTBIO then" 02.10.06 to get TBIO upto Telepay2;
            If bTBIO Then
                sAH = "AH2"
            Else
                sAH = "AH1"
            End If
            sAH = sAH & sStatusCode
            If sStatusCode = "00" Then
                If bTBIO Then
                    sAH = sAH & "TBIO"
                Else
                    If bDomestic Then
                        sAH = sAH & "TBII"
                    Else
                        sAH = sAH & "TBIU"
                    End If
                End If
            Else
                If bTBIO Then
                    sAH = sAH & "TBRO"
                Else
                    If bDomestic Then
                        sAH = sAH & "TBRI"
                    Else
                        sAH = sAH & "TBRU"
                    End If
                End If
            End If
            If Trim(sTransDate) = "" Then
                sAH = sAH & VB6.Format(Now, "MMDD")
            Else
                sAH = sAH & sTransDate
            End If
            'FIX: Enter Sequenceno. per day
            sAH = sAH & PadLeft(Str(nSequenceNo), 6, "0")
            sAH = sAH & Space(8)
            'FIX: Enter User_ID
            sAH = sAH & Space(11)
            sAH = sAH & "04"
        End If
        ApplicationHeader = sAH

    End Function
	
	'UPGRADE_NOTE: WriteLine was upgraded to WriteLine_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Function WriteLine_Renamed(ByRef sLine As String, ByRef nNoOfBETFOR As Double, ByRef nSequenceNoTotal As Double, ByRef bUnix As Boolean) As Boolean
		' 12.08.02 JaNp
		' Added bUnix
		' if bUnix is set to true, then only LineFeed is added at the end of each line,
		' instead of the ordinary CRLF
		If bUnix Then
			oFile.Write(Mid(sLine, 1, 80) & vbLf)
			oFile.Write(Mid(sLine, 81, 80) & vbLf)
			oFile.Write(Mid(sLine, 161, 80) & vbLf)
			oFile.Write(Mid(sLine, 241, 80) & vbLf)
		Else
			oFile.WriteLine(Mid(sLine, 1, 80))
			oFile.WriteLine(Mid(sLine, 81, 80))
			oFile.WriteLine(Mid(sLine, 161, 80))
			oFile.WriteLine(Mid(sLine, 241, 80))
		End If
		
		nNoOfBETFOR = nNoOfBETFOR + 1
		nSequenceNoTotal = nSequenceNoTotal + 1
		If nSequenceNoTotal > 9999 Then
			nSequenceNoTotal = 0
		End If
		
		
	End Function
End Module
