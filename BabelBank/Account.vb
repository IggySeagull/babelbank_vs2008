Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("Account_NET.Account")> Public Class Account
	
	Public nIndex As Double
	Private nAccount_ID, nClient_ID As Double
	Private sGLAccount, sAccount, sGLResultsAccount As String
	Private sConvertedAccount, sContractNo, sSWIFTAddress As String
	Private sAvtaleGiroID, sAccountCountryCode As String
    Private sCurrencyCode As String ' added 16.08.2007
    Private sDebitAccountCountryCode As String
    '07.10.2015 - Added the use of dimensions
    Private sDim1 As String
    Private sDim2 As String
    Private sDim3 As String
    Private sDim4 As String
    Private sDim5 As String
    Private sDim6 As String
    Private sDim7 As String
    Private sDim8 As String
    Private sDim9 As String
    Private sDim10 As String
    Dim eStatus As Profile.CollectionStatus
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	'Old code
	'Friend Property Let Index(NewVal As Variant)
	'    nIndex = Val(NewVal)
	'End Property
	'Public Property Get Index()
	'    Index = nIndex
	'End Property
	Public Property Account_ID() As Double
		Get
			Account_ID = nAccount_ID
		End Get
		Set(ByVal Value As Double)
			nAccount_ID = Value
		End Set
	End Property
	Public Property Client_ID() As Double
		Get
			Client_ID = nClient_ID
		End Get
		Set(ByVal Value As Double)
			nClient_ID = Value
		End Set
	End Property
	Public Property Account() As String
		Get
			Account = sAccount
		End Get
		Set(ByVal Value As String)
			sAccount = Value
		End Set
	End Property
	Public Property GLAccount() As String
		Get
			GLAccount = sGLAccount
		End Get
		Set(ByVal Value As String)
			sGLAccount = Value
		End Set
	End Property
	Public Property GLResultsAccount() As String
		Get
			GLResultsAccount = sGLResultsAccount
		End Get
		Set(ByVal Value As String)
			sGLResultsAccount = Value
		End Set
	End Property
	Public Property ContractNo() As String
		Get
			ContractNo = sContractNo
		End Get
		Set(ByVal Value As String)
			sContractNo = Value
		End Set
	End Property
	Public Property ConvertedAccount() As String
		Get
			ConvertedAccount = sConvertedAccount
		End Get
		Set(ByVal Value As String)
			sConvertedAccount = Value
		End Set
	End Property
	Public Property AccountCountryCode() As String
		Get
			AccountCountryCode = sAccountCountryCode
		End Get
		Set(ByVal Value As String)
			sAccountCountryCode = Value
		End Set
	End Property
	Public Property SWIFTAddress() As String
		Get
			SWIFTAddress = sSWIFTAddress
		End Get
		Set(ByVal Value As String)
			sSWIFTAddress = Value
		End Set
	End Property
	Public Property AvtaleGiroID() As String
		Get
			AvtaleGiroID = sAvtaleGiroID
		End Get
		Set(ByVal Value As String)
			sAvtaleGiroID = Value
		End Set
	End Property
	Public Property CurrencyCode() As String
		Get
			CurrencyCode = sCurrencyCode
		End Get
		Set(ByVal Value As String)
			sCurrencyCode = Value
		End Set
    End Property
    ' XokNET 07.09.2011
    Public Property DebitAccountCountryCode() As String
        Get
            DebitAccountCountryCode = sDebitAccountCountryCode
        End Get
        Set(ByVal Value As String)
            sDebitAccountCountryCode = Value
        End Set
    End Property

    Public Property Dim1() As String
        Get
            Dim1 = sDim1
        End Get
        Set(ByVal Value As String)
            sDim1 = Value
        End Set
    End Property
    Public Property Dim2() As String
        Get
            Dim2 = sDim2
        End Get
        Set(ByVal Value As String)
            sDim2 = Value
        End Set
    End Property
    Public Property Dim3() As String
        Get
            Dim3 = sDim3
        End Get
        Set(ByVal Value As String)
            sDim3 = Value
        End Set
    End Property
    Public Property Dim4() As String
        Get
            Dim4 = sDim4
        End Get
        Set(ByVal Value As String)
            sDim4 = Value
        End Set
    End Property
    Public Property Dim5() As String
        Get
            Dim5 = sDim5
        End Get
        Set(ByVal Value As String)
            sDim5 = Value
        End Set
    End Property
    Public Property Dim6() As String
        Get
            Dim6 = sDim6
        End Get
        Set(ByVal Value As String)
            sDim6 = Value
        End Set
    End Property
    Public Property Dim7() As String
        Get
            Dim7 = sDim7
        End Get
        Set(ByVal Value As String)
            sDim7 = Value
        End Set
    End Property
    Public Property Dim8() As String
        Get
            Dim8 = sDim8
        End Get
        Set(ByVal Value As String)
            sDim8 = Value
        End Set
    End Property
    Public Property Dim9() As String
        Get
            Dim9 = sDim9
        End Get
        Set(ByVal Value As String)
            sDim9 = Value
        End Set
    End Property
    Public Property Dim10() As String
        Get
            Dim10 = sDim10
        End Get
        Set(ByVal Value As String)
            sDim10 = Value
        End Set
    End Property
    Public Property Status() As vbBabel.Profile.CollectionStatus
        Get
            Status = eStatus
        End Get
        Set(ByVal Value As vbBabel.Profile.CollectionStatus)
            If CDbl(Value) = 0 Then
                eStatus = Profile.CollectionStatus.NoChange
            End If
            If Value > eStatus Then
                eStatus = Value
            End If
        End Set
    End Property
    '********* END PROPERTY SETTINGS ***********************

    Public Function Load(ByRef iCompanyNo As Short, ByRef myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader) As Boolean

        nAccount_ID = myBBDB_AccessReader.GetInt32(0)
        nClient_ID = myBBDB_AccessReader.GetInt32(1)
        If Not myBBDB_AccessReader.IsDBNull(2) Then
            sAccount = myBBDB_AccessReader.GetString(2)
        End If
        If Not myBBDB_AccessReader.IsDBNull(3) Then
            sGLAccount = myBBDB_AccessReader.GetString(3)
        End If
        If Not myBBDB_AccessReader.IsDBNull(4) Then
            sGLResultsAccount = myBBDB_AccessReader.GetString(4)
        End If
        If Not myBBDB_AccessReader.IsDBNull(5) Then
            sContractNo = myBBDB_AccessReader.GetString(5)
        End If
        If Not myBBDB_AccessReader.IsDBNull(6) Then
            sConvertedAccount = myBBDB_AccessReader.GetString(6)
            sConvertedAccount = sConvertedAccount.Trim '08.05.2018 - Added
        End If
        If Not myBBDB_AccessReader.IsDBNull(7) Then
            sAccountCountryCode = myBBDB_AccessReader.GetString(7)
        End If
        If Not myBBDB_AccessReader.IsDBNull(8) Then
            sSWIFTAddress = myBBDB_AccessReader.GetString(8)
        End If
        If Not myBBDB_AccessReader.IsDBNull(9) Then
            sAvtaleGiroID = myBBDB_AccessReader.GetString(9)
        End If
        If Not myBBDB_AccessReader.IsDBNull(10) Then
            sCurrencyCode = myBBDB_AccessReader.GetString(10)
        End If
        If Not myBBDB_AccessReader.IsDBNull(11) Then
            sDebitAccountCountryCode = myBBDB_AccessReader.GetString(11)
        End If
        If Not myBBDB_AccessReader.IsDBNull(12) Then
            sDim1 = myBBDB_AccessReader.GetString(12)
        End If
        If Not myBBDB_AccessReader.IsDBNull(13) Then
            sDim2 = myBBDB_AccessReader.GetString(13)
        End If
        If Not myBBDB_AccessReader.IsDBNull(14) Then
            sDim3 = myBBDB_AccessReader.GetString(14)
        End If
        If Not myBBDB_AccessReader.IsDBNull(15) Then
            sDim4 = myBBDB_AccessReader.GetString(15)
        End If
        If Not myBBDB_AccessReader.IsDBNull(16) Then
            sDim5 = myBBDB_AccessReader.GetString(16)
        End If
        If Not myBBDB_AccessReader.IsDBNull(17) Then
            sDim6 = myBBDB_AccessReader.GetString(17)
        End If
        If Not myBBDB_AccessReader.IsDBNull(18) Then
            sDim7 = myBBDB_AccessReader.GetString(18)
        End If
        If Not myBBDB_AccessReader.IsDBNull(19) Then
            sDim8 = myBBDB_AccessReader.GetString(19)
        End If
        If Not myBBDB_AccessReader.IsDBNull(20) Then
            sDim9 = myBBDB_AccessReader.GetString(20)
        End If
        If Not myBBDB_AccessReader.IsDBNull(21) Then
            sDim10 = myBBDB_AccessReader.GetString(21)
        End If

        Return True

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        nAccount_ID = 0
        nClient_ID = 0
        sAccount = ""
        sContractNo = ""
        sConvertedAccount = ""
        sAccountCountryCode = ""
        SWIFTAddress = ""
        eStatus = 0
        sAvtaleGiroID = ""
        sCurrencyCode = ""
        sDebitAccountCountryCode = vbNullString
        sDim1 = ""
        sDim2 = ""
        sDim3 = ""
        sDim4 = ""
        sDim5 = ""
        sDim6 = ""
        sDim7 = ""
        sDim8 = ""
        sDim9 = ""
        sDim10 = ""
        ' When vbbabel.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
End Class
