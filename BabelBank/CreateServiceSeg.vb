Option Strict Off
Option Explicit On
Module CreateServiceSeg
	
	Public Function CreateUNA(ByRef XMLDoc As MSXML2.DOMDocument40) As String
		Dim sElementSep As String
		Dim sGroupSep As String
		Dim sDecimalSep As String
		Dim sReleaseChar As String
		Dim sSegmentTerminator As String
		Dim sSegmentString As String
		Dim elmTemp As MSXML2.IXMLDOMElement
		Dim i As Short
		
		For i = 0 To 0
			elmTemp = XMLDoc.selectSingleNode("//UNA")
			'UPGRADE_WARNING: Couldn't resolve default property of object elmTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sElementSep = elmTemp.getAttribute("elementsep")
			If Len(sElementSep) = 0 Then
				sSegmentString = "ERROR, Not able to find the component data element seperator under UNA."
				Exit For
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object elmTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sGroupSep = elmTemp.getAttribute("groupsep")
			If Len(sGroupSep) = 0 Then
				sSegmentString = "ERROR, Not able to find the data element seperator under UNA."
				Exit For
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object elmTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sDecimalSep = elmTemp.getAttribute("decimalsep")
			If Len(sDecimalSep) = 0 Then
				sSegmentString = "ERROR, Not able to find the decimal notation under UNA."
				Exit For
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object elmTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sReleaseChar = elmTemp.getAttribute("escape")
			If Len(sReleaseChar) = 0 Then
				sSegmentString = "ERROR, Not able to find the release character under UNA."
				Exit For
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object elmTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sSegmentTerminator = elmTemp.getAttribute("segmentsep")
			If Len(sSegmentTerminator) = 0 Then
				sSegmentString = "ERROR, Not able to find the segment seperator"
				Exit For
			End If
			
			sSegmentString = "UNA" & Trim(sElementSep) & Trim(sGroupSep) & Trim(sDecimalSep) & Trim(sReleaseChar) & " " & Trim(sSegmentTerminator)
		Next i
		
		CreateUNA = sSegmentString
		
	End Function
	
	
	Public Function CreateUNB(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef sApplicationReference As String, ByRef lBabelFile_ID As Integer, ByRef sInterchangeReference As String, Optional ByRef sAddToXPath As String = "", Optional ByRef sCompanyNo As String = "") As String
		Dim sSyntaxIdentifier As String
		Dim sSyntaxVersionNo As String
		Dim sSenderID As String
		Dim sRecipientID As String
		'Dim sInterchangeReference As String
		Dim sSegmentString As String
		Dim i As Short
		
		For i = 0 To 0
			sSyntaxIdentifier = GetValue(XMLDoc, "//UNB/TG010/T0001" & sAddToXPath)
			If Len(sSyntaxIdentifier) = 0 Then
				sSegmentString = "ERROR, Not able to create a Syntax identifier"
				Exit For
			End If
			sSyntaxVersionNo = GetValue(XMLDoc, "//UNB/TG010/T0002" & sAddToXPath)
			If Len(sSyntaxVersionNo) = 0 Then
				sSegmentString = "ERROR, Not able to create a Syntax Version number"
				Exit For
			End If
			If Len(sCompanyNo) > 0 Then
				sSenderID = sCompanyNo
			Else
				sSenderID = GetValue(XMLDoc, "//UNB/TG030/T0010" & sAddToXPath)
			End If
			If Len(sSenderID) = 0 Then
				sSegmentString = "ERROR, Not able to create a sender identification"
				Exit For
			End If
			sRecipientID = GetValue(XMLDoc, "//UNB/TG020/T0004" & sAddToXPath)
			If Len(sRecipientID) = 0 Then
				sSegmentString = "ERROR, Not able to create a recipent identification"
				Exit For
			End If
			'new 06.03.2008 - by Kjell to create a unique InterchangeReference
            sInterchangeReference = Right(VB6.Format(Now, "YYYYMMDDHHMMSS") & "_" & Trim(Str(lBabelFile_ID)), 14)
            'Old code
            'sInterchangeReference = "10001" 'This must be a parameter somewhere

            sSegmentString = "UNB+" & sSyntaxIdentifier & ":" & sSyntaxVersionNo & "+" & sSenderID
            sSegmentString = sSegmentString & "+" & sRecipientID & "+" & VB6.Format(Now, "YYMMDD") & ":" & VB6.Format(Now, "HHMM")
			sSegmentString = sSegmentString & "+" & sInterchangeReference & "++" & sApplicationReference & "'"
		Next i
		
		CreateUNB = sSegmentString
		
	End Function
	
	Public Function CreateUNG(ByRef XMLDoc As MSXML2.DOMDocument40, Optional ByRef sAddToXPath As String = "", Optional ByRef sCompanyNo As String = "") As String
		'FIX: Have to add this segment when in use
		MsgBox("Not able to create UNG-segment", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
	End Function
	
	Public Function CreateUNH(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef sMessageTypeIdentifier As String, ByRef lNoofMessages As Integer) As String
		Dim sMessageReferenceNo As String
		'Dim sMessageTypeIdentifier As String
		Dim sMessageTypeVersionNo As String
		Dim sMessageTypeReleaseNo As String
		Dim sControllingAgency As String
		Dim sSegmentString As String
		Dim i As Short
		
		For i = 0 To 0
			'sMessageReferenceNo = "20002" 'FIX: This must be a parameter somewhere
			'    sMessageTypeIdentifier = "CONTRL"
			sMessageTypeVersionNo = "3" 'FIX: This must be a parameter somewhere
			sMessageTypeReleaseNo = "1" 'FIX: This must be a parameter somewhere
			sControllingAgency = "UN" 'FIX: This must be a parameter somewhere
			
			sSegmentString = "UNH+" & Trim(Str(lNoofMessages)) & "+" & sMessageTypeIdentifier & ":" & sMessageTypeVersionNo
			sSegmentString = sSegmentString & ":" & sMessageTypeReleaseNo & ":" & sControllingAgency & "'"
		Next i
		
		CreateUNH = sSegmentString
		
	End Function
	
	
	
	Public Function CreateUNT(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef lNoofMessages As Integer, ByRef lNoofSegments As Integer) As String
		Dim sMessageReferenceNo As String
		Dim sSegmentString As String
		Dim i As Short
		
		For i = 0 To 0
			'sMessageReferenceNo = "20002" 'FIX: This must be a parameter somewhere
			sSegmentString = "UNT+" & Trim(Str(lNoofSegments)) & "+" & Trim(Str(lNoofMessages)) & "'"
		Next i
		
		CreateUNT = sSegmentString
		
	End Function
	
	Public Function CreateUNZ(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef lNoofMessages As Integer, ByRef sInterchangeReference As String) As String
		'Dim sInterchangeReference As String
		Dim sSegmentString As String
		Dim i As Short
		
		For i = 0 To 0
			'sInterchangeReference = "10001" 'FIX: This must be a parameter somewhere
			sSegmentString = "UNZ+" & Trim(Str(lNoofMessages)) & "+" & sInterchangeReference & "'"
		Next i
		
		CreateUNZ = sSegmentString
		
	End Function
	Public Function GetValue(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef sXPath As String) As String
		Dim nodNode As MSXML2.IXMLDOMElement
		Dim sValueChildNode As String
		Dim iCounter As Short
		Dim bChildNodeExists As Boolean
		
		'Sometimes this element isn't used. Therefor traverse through the childnodes to see if it exists
		bChildNodeExists = False
		nodNode = XMLDoc.selectSingleNode(Left(sXPath, InStrRev(sXPath, "/",  , CompareMethod.Text) - 1))
		sValueChildNode = Mid(sXPath, InStrRev(sXPath, "/",  , CompareMethod.Text) + 1)
		For iCounter = 0 To nodNode.childNodes.length - 1
			If nodNode.childNodes.Item(iCounter).nodeName = sValueChildNode Then
				bChildNodeExists = True
				Exit For
			End If
		Next iCounter
		
		If bChildNodeExists Then
			nodNode = XMLDoc.selectSingleNode(sXPath)
			GetValue = Trim(nodNode.nodeTypedValue)
		Else
            GetValue = ""
		End If
		
		'UPGRADE_NOTE: Object nodNode may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		nodNode = Nothing
		
	End Function
End Module
