Option Strict Off
Option Explicit On
Module WriteNordea_DK
    ' XokNET 03.12.2013 Several changes, take whole function
    Function WriteNordeaUnitel(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As FreeText
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim nAmount As Double
        Dim sFromAccount As String, sToAccount As String, sShortNotification As String
        Dim sAmount As String
        Dim sFromRegisterNo As String
        Dim sToRegisterNo As String
        Dim aFreetexts() As String
        Dim sTextcode As String
        Dim sISO As String
        Dim nCount As Integer, i As Integer
        Dim bFIKort As Boolean
        Dim sKortartKode As String
        Dim sBetalingsRef As String
        ' International;
        Dim sSwift As String
        Dim sStateBankCode As String
        Dim sStateBankText As String
        Dim sImportDate As String

        Dim sErrorString As String
        Dim lErrno As Long
        Dim lInvCounter As Long
        Dim bThisIsVismaIntegrator As Boolean
        '***************VISMA*****************************
        ' Testing if this is Visma Integrator
        bThisIsVismaIntegrator = IsThisVismaIntegrator()

        ' create an outputfile
        Try

            bAppendFile = False


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects to
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    '-------- fetch content of each payment ---------
                                    sFromRegisterNo = Left$(oPayment.I_Account, 4)
                                    'sFromAccount = Trim$(Mid$(oPayment.I_Account, 5))
                                    ' 25.09.2013 - take whole accountno !
                                    sFromAccount = Trim$(oPayment.I_Account)
                                    sToRegisterNo = Left$(oPayment.E_Account, 4)
                                    'sToAccount = Trim$(Mid$(oPayment.E_Account, 5))
                                    ' 25.09.2013 - take whole accountno !
                                    sToAccount = Trim$(oPayment.E_Account)
                                    sName = Trim$(Left$(oPayment.E_Name, 35))
                                    If oPayment.BANK_I_Domestic Then
                                        sOwnRef = Trim$(Left$(oPayment.REF_Own, 16))
                                    Else
                                        sOwnRef = Trim$(Left$(oPayment.REF_Own, 20))
                                    End If
                                    ' International;
                                    sSwift = oPayment.BANK_SWIFTCode
                                    If Len(sSwift) = 0 Then
                                        ' Fetch sortingcode, etc
                                        sSwift = oPayment.BANK_BranchNo
                                    End If
                                    'nCount = -1

                                    ' New 24.10.2003, re Tribe Hotel Denmark
                                    For Each oInvoice In oPayment.Invoices
                                        ' XNET 04.05.2016 Moved from 5 lines up
                                        nCount = -1

                                        ' XNET 13.05.2014
                                        ' Critical change regarding cancelled payments
                                        If Not oInvoice.Exported Then

                                            ' For FIK, we must create one payment for each invoice
                                            ' For other paymenttypes, create one payment for all invoices

                                            ' ===========================================================
                                            ' 03.12.2013 Major change - create one paymentline pr invoice
                                            ' ===========================================================
                                            '                            If (oPayment.PayCode = "301" Or oPayment.PayCode = "302") And _
                                            '                                oPayment.Invoices(1).Unique_Id <> vbNullString Then
                                            '                                ' continue with details for each invoice
                                            '                                nAmount = Val(oInvoice.MON_InvoiceAmount)
                                            '                                ReDim aFreetexts(3)
                                            '                            Else
                                            '                                'For Each oInvoice In oPayment.Invoices
                                            '                                For lInvCounter = 1 To oPayment.Invoices.Count
                                            '                                    Set oInvoice = oPayment.Invoices(lInvCounter)
                                            ' Prioritize structured InvoiceNo;
                                            If Not EmptyString(oInvoice.InvoiceNo) Then
                                                nCount = nCount + 1
                                                If nCount = 0 Then
                                                    ReDim aFreetexts(0)
                                                Else
                                                    ReDim Preserve aFreetexts(nCount)
                                                End If
                                                aFreetexts(nCount) = Trim$(oInvoice.InvoiceNo)
                                            Else

                                                For Each oFreetext In oInvoice.Freetexts
                                                    If Trim$(oFreetext.Text) <> vbNullString Then
                                                        nCount = nCount + 1
                                                        If nCount = 0 Then
                                                            ReDim aFreetexts(0)
                                                        Else
                                                            ReDim Preserve aFreetexts(nCount)
                                                        End If
                                                        aFreetexts(nCount) = Trim$(oFreetext.Text)
                                                    End If
                                                Next
                                            End If

                                            '                                Next lInvCounter
                                            '                            'End If
                                            '
                                            '                                If nCount < 4 Then
                                            '                                    For i = nCount + 1 To 3
                                            '                                        ReDim Preserve aFreetexts(i)
                                            '                                        aFreetexts(i) = vbNullString
                                            '                                    Next i
                                            '                                End If
                                            '
                                            '                                If aFreetexts(1) = vbNullString Then
                                            '                                    ' Only one freetextline, check length;
                                            '                                    If Len(aFreetexts(0)) < 21 Then
                                            '                                        sShortNotification = aFreetexts(0)
                                            '                                        aFreetexts(0) = vbNullString
                                            '                                        sTextcode = "100"
                                            '                                    Else
                                            '                                        sTextcode = "068" 'Overf�rsel
                                            '                                        sShortNotification = "1 faktura"
                                            '                                    End If
                                            '                                Else
                                            '                                    ' several textlines;
                                            '                                    sTextcode = "068" 'Overf�rsel
                                            '                                    If oPayment.Invoices.Count = 1 Then
                                            '                                        sShortNotification = Trim$(Left$(oPayment.Invoices(1).Freetexts(1).Text, 20))
                                            '                                    ElseIf oPayment.Invoices.Count > 1 Then
                                            '                                        sShortNotification = Str(Trim(oPayment.Invoices.Count)) & " fakturabetalinger"
                                            '                                    Else
                                            '                                        sShortNotification = Trim$(oPayment.I_Account)
                                            '                                    End If
                                            '                                End If
                                            '
                                            '                                ' Set textcodes based on common function for this;
                                            '                                sTextcode = bbSetPayCode(oPayment.PayCode, oPayment.PayType, "UNITELPC")
                                            '                                nAmount = 0
                                            '                                'For Each oInvoice In oPayment.Invoices
                                            '                                For lInvCounter = 1 To oPayment.Invoices.Count
                                            '                                    'nAmount = nAmount + Val(oInvoice.MON_InvoiceAmount)
                                            '                                    nAmount = nAmount + Val(oPayment.Invoices(lInvCounter).MON_InvoiceAmount)
                                            '                                Next
                                            '
                                            '                            End If  ' FIK or Not
                                            '
                                            '                            sAmount = Format(nAmount / 100, "###0.00")
                                            ' 25.09.2013 farlig, farlig - desimaltegn blir p�virket av oppsett p� PC-en;

                                            ' New 03.12.2013 (to substitute commented code above;
                                            ' ===================================================
                                            sAmount = Format(oInvoice.MON_InvoiceAmount / 100, "###0.00")
                                            ' Set textcodes based on common function for this;
                                            sTextcode = bbSetPayCode(oPayment.PayCode, oPayment.PayType, "UNITELPC")
                                            ' Textcode 102 is for "kort meddelelse", and used as standard.

                                            ' hvis det legges ut desimalkomma, s� gj�r om til punktum
                                            sAmount = Replace(sAmount, ",", ".")
                                            sStateBankCode = oInvoice.STATEBANK_Code
                                            sStateBankText = oInvoice.STATEBANK_Text
                                            If oInvoice.STATEBANK_DATE <> "19900101" Then
                                                sImportDate = Mid$(oInvoice.STATEBANK_DATE, 5, 2) & Left$(oInvoice.STATEBANK_DATE, 4) ' MMYYYY
                                            Else
                                                ' Use todays date if not Statebank_date is present
                                                sImportDate = Right$("0 " & Trim$(Str(Month(Now))), 2) & Trim(Str(Year(Now)))  'MMYYYY
                                            End If
                                            sDate = oPayment.DATE_Payment  'YYYYMMDD
                                            If oPayment.PayType <> "I" Then    ' Domestic records
                                                sISO = "DKK"
                                            Else
                                                sISO = oPayment.MON_InvoiceCurrency
                                            End If


                                            ' Start output of paymentrecord
                                            ' XokNET 27.06.2014 M� nullstille verdier, siden vi kan hoppe mellom FIK og Meldingstranser
                                            sKortartKode = ""
                                            sBetalingsRef = ""

                                            ' Change in output depending on paymenttype -
                                            ' - Bankoverf�rsel med kort meddelelse (9, Textcode =100)
                                            ' - Bankoverf�rsel med meddelelse (9, Textcode = 068)
                                            ' - F�lles indbetalingskort (44)

                                            '29.06.2021 - Changed paycode = to using the function IsFIK
                                            If IsFIK(oPayment.PayCode) And oInvoice.Unique_Id <> vbNullString Then
                                                'If (oPayment.PayCode = "301" Or oPayment.PayCode = "302") And _
                                                'oInvoice.Unique_Id <> vbNullString Then

                                                ' F�lles indbetalingskort
                                                bFIKort = True
                                                ' Forutsetter at kortartkode alltid er to f�rste i Unique_ID
                                                sKortartKode = Left$(oInvoice.Unique_Id, 2)
                                                sBetalingsRef = Mid$(oInvoice.Unique_Id, 3)

                                                sTextcode = ""
                                                sToRegisterNo = ""


                                                ' Changed 01.12.2003
                                                ' Validating FIK creates problems for users
                                                ' which are unable to recreate paymentfiles
                                                ' when an FIK error is encountered.
                                                ' Thats why we have to TURN OFF FIK-validation
                                                '''' Validation of FIKort, Kortartkode
                                                'Select Case sKortartKode
                                                'Case vbNullString
                                                '    ' Ikke Meddelelse!
                                                '    aFreetexts(0) = vbNullString
                                                '    aFreetexts(1) = vbNullString
                                                '    aFreetexts(2) = vbNullString
                                                '    aFreetexts(3) = vbNullString
                                                '    If Len(sBetalingsRef) <> 19 Then
                                                '        sErrorString = LRS(12001, "19") & vbCrLf   ' Wrong ID-length. Must be %19%
                                                '        sErrorString = sErrorString & sName & vbCrLf & sAmount
                                                '        lErrno = 12001
                                                '        Err.Raise vbObjectError + 12001, , sErrorString
                                                '    End If
                                                '
                                                'Case "04"
                                                '    ' Ikke Meddelelse!
                                                '    aFreetexts(0) = vbNullString
                                                '    aFreetexts(1) = vbNullString
                                                '    aFreetexts(2) = vbNullString
                                                '    aFreetexts(3) = vbNullString
                                                '    If Len(sBetalingsRef) <> 16 Then
                                                '        sErrorString = LRS(12001, "16") & vbCrLf   ' Wrong ID-length. Must be %19%
                                                '        sErrorString = sErrorString & sName & vbCrLf & sAmount
                                                '        lErrno = 12001
                                                '        Err.Raise vbObjectError + 12001, , sErrorString
                                                '    End If
                                                '
                                                ' Case "15"
                                                '     ' Ikke Meddelelse!
                                                '     aFreetexts(0) = vbNullString
                                                '     aFreetexts(1) = vbNullString
                                                '    aFreetexts(2) = vbNullString
                                                '    aFreetexts(3) = vbNullString
                                                '    If Len(sBetalingsRef) <> 16 Then
                                                '        sErrorString = LRS(12001, "16") & vbCrLf   ' Wrong ID-length. Must be %19%
                                                '       sErrorString = sErrorString & sName & vbCrLf & sAmount
                                                '       lErrno = 12001
                                                '       Err.Raise vbObjectError + 12001, , sErrorString
                                                '   End If

                                                'Case "71"
                                                '                                ' Ikke Meddelelse!
                                                '    aFreetexts(0) = vbNullString
                                                '    aFreetexts(1) = vbNullString
                                                '    aFreetexts(2) = vbNullString
                                                '    aFreetexts(3) = vbNullString
                                                '    If Len(sBetalingsRef) <> 15 Then
                                                '        sErrorString = LRS(12001, "15") & vbCrLf   ' Wrong ID-length. Must be %19%
                                                '        sErrorString = sErrorString & sName & vbCrLf & sAmount
                                                '        lErrno = 12001
                                                '        Err.Raise vbObjectError + 12001, , sErrorString
                                                '    End If

                                                'Case "73"
                                                '    ' Meddelelse, IKKE betalingsreferanse!
                                                '    sBetalingsRef = vbNullString
                                                '
                                                ' Case "75"
                                                '    ' Meddelelse, OG betalingsreferanse!
                                                '    If Len(sBetalingsRef) <> 16 Then
                                                '        sErrorString = LRS(12001, "16") & vbCrLf   ' Wrong ID-length. Must be %19%
                                                '        sErrorString = sErrorString & sName & vbCrLf & sAmount
                                                '        lErrno = 12001
                                                '        Err.Raise vbObjectError + 12001, , sErrorString
                                                '    End If
                                                '
                                                'End Select

                                            Else
                                                bFIKort = False
                                            End If

                                            ' Indenlandske betalinger:
                                            '45: Indenlandsk bankoverf�rsel
                                            '46: Indbetalingskort/Giro betaling
                                            '47: Check til Danmark
                                            '(56: Valutakontooverf�rsel mellem konti i Nordea Danmark)

                                            'Udenlandske betalinger:
                                            '49: Standardoverf�rsel
                                            '50: Ekspresoverf�rsel
                                            '51: Koncernoverf�rsel til udenlandsk pengeinstitut
                                            '(52: Nordea Intracompany Payment)
                                            '54: Check til udlandet
                                            'S�rlige betalingstyper:
                                            '55: Koncernoverf�rsel (indenlandsk)
                                            '43: Request for Transfer

                                            ' Analyze paymenttype;
                                            ' --------------------
                                            If oPayment.PayType <> "I" Then
                                                ' Domestic records
                                                ' ----------------
                                                If bFIKort Then
                                                    '46: Indbetalingskort/Giro betaling
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "46" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                ElseIf oPayment.E_Account = "" Then
                                                    '47: Check til Danmark
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "47" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                ElseIf oPayment.ToOwnAccount Or oPayment.PayCode = "403" Then
                                                    '55: Koncernoverf�rsel (indenlandsk)
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "55" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                Else
                                                    '45: Indenlandsk bankoverf�rsel
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "45" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                End If
                                            Else

                                                ' International
                                                ' -------------
                                                If oPayment.Priority Then
                                                    '50: Ekspresoverf�rsel
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "50" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","

                                                ElseIf oPayment.BANK_I_Domestic Then
                                                    '43: Request for Transfer
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "43" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                ElseIf oPayment.E_Account = "" Then
                                                    '54: Check til udlandet
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "54" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                ElseIf (oPayment.ToOwnAccount Or oPayment.PayCode = "403") And Left$(oPayment.BANK_SWIFTCode, 4) = "NDEA" Then
                                                    '52: Nordea Intracompany Payment
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "52" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                ElseIf oPayment.ToOwnAccount Or oPayment.PayCode = "403" Then
                                                    '51: Koncernoverf�rsel til udenlandsk pengeinstitut
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "51" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                Else
                                                    '49: Standardoverf�rsel
                                                    sLine = Chr(34) & "0" & Chr(34) & "," & Chr(34) & "49" & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & Chr(34) & ","
                                                End If
                                                ' for International payments, set countrycode receivers bank
                                                sLine = sLine & Chr(34) & Left$(oPayment.BANK_CountryCode, 2) & Chr(34) & "," '11 Modtagende banks landkode
                                            End If

                                            sLine = sLine & Chr(34) & sName & Chr(34) & "," '12 Modtagers navn
                                            sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr1) & Chr(34) & ","               '13 Modtager linje 2
                                            ' XokNET 01.10.2014 - kan ikke ha blanke adresselinjer og s� neste ibruk - m� v�re "sammenhengende"
                                            If Not EmptyString(oPayment.E_Adr2) Then
                                                sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr2) & Chr(34) & ","         '14 Modtager linje 3
                                                If EmptyString(oPayment.E_Adr3) Then
                                                    sLine = sLine & Chr(34) & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35) & Chr(34) & ","        '15 Modtager linje 4
                                                Else
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr3) & Chr(34) & ","         '15 Modtager linje 4
                                                End If
                                            Else
                                                ' tom adr2, "flytt opp";
                                                If EmptyString(oPayment.E_Adr3) Then
                                                    sLine = sLine & Chr(34) & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35) & Chr(34) & ","        '14 Modtager linje 3
                                                Else
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr3) & Chr(34) & ","         '14 Modtager linje 3
                                                End If
                                                ' blank siste adr.linje
                                                sLine = sLine & Chr(34) & "" & Chr(34) & ","         '15 Modtager linje 4
                                            End If
                                            If oPayment.PayType <> "I" Then    ' Domestic records
                                                sLine = sLine & Chr(34) & Chr(34) & ","            '16 Ikke i bruk innland
                                            Else
                                                sLine = sLine & Chr(34) & Trim(oPayment.BANK_BranchNo) & Chr(34) & ","    '16 bankkode mottakerbank, hvis ibruk
                                            End If
                                            sLine = sLine & Chr(34) & sToAccount & Chr(34) & ","   '17 Kontonummer bel�bsmodtager
                                            If oPayment.PayType <> "I" Then    ' Domestic records
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '18
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '19
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '20
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '21
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '22
                                            Else
                                                ' International payments
                                                sLine = sLine & Chr(34) & oPayment.BANK_Name & Chr(34) & ","    '18  Bank name
                                                sLine = sLine & Chr(34) & oPayment.BANK_Adr1 & Chr(34) & ","    '19  Bank Adr1
                                                sLine = sLine & Chr(34) & oPayment.BANK_Adr2 & Chr(34) & ","    '20  Bank Adr2
                                                sLine = sLine & Chr(34) & oPayment.BANK_Adr3 & Chr(34) & ","    '21  Bank Adr3
                                                sLine = sLine & Chr(34) & sSwift & Chr(34) & ","                         '22  SWIFT
                                            End If
                                            sLine = sLine & Chr(34) & aFreetexts(0) & Chr(34) & "," '23 Meddelelse 1
                                            If UBound(aFreetexts) > 0 Then
                                                sLine = sLine & Chr(34) & aFreetexts(1) & Chr(34) & "," '24 Meddelelse 2
                                            Else
                                                sLine = sLine & Chr(34) & Chr(34) & ","  '24 Meddelelse 2
                                            End If
                                            If UBound(aFreetexts) > 1 Then
                                                sLine = sLine & Chr(34) & aFreetexts(2) & Chr(34) & "," '25 Meddelelse 3
                                            Else
                                                sLine = sLine & Chr(34) & Chr(34) & ","  '24 Meddelelse 3
                                            End If
                                            If UBound(aFreetexts) > 2 Then
                                                sLine = sLine & Chr(34) & aFreetexts(3) & Chr(34) & "," '26 Meddelelse 4
                                            Else
                                                sLine = sLine & Chr(34) & Chr(34) & ","  '24 Meddelelse 4
                                            End If

                                            sLine = sLine & "0" & ","           '27 Alternativ avsender
                                            sLine = sLine & Chr(34) & Chr(34) & ","         '28
                                            sLine = sLine & Chr(34) & Chr(34) & ","         '29
                                            sLine = sLine & Chr(34) & Chr(34) & ","         '30
                                            sLine = sLine & Chr(34) & sISO & Chr(34) & ","  '31 Valutakode
                                            sLine = sLine & "0,"                             '32 Modverdi
                                            sLine = sLine & sAmount & ","    '33 Bel�p
                                            sLine = sLine & sDate & ","     '34 Betalingsdato YYMMDD
                                            sLine = sLine & ","                              '35 Ikke i bruk
                                            sLine = sLine & ","                              '36 Ikke i bruk
                                            sLine = sLine & Chr(34) & sFromAccount & Chr(34) & ","   '37 Bel�bsavsenders konto
                                            'sLine = sLine & Chr(34) & sOwnRef & Chr(34) & ","   '38 Egenref
                                            ' XokNET 13.05.2014
                                            If bThisIsVismaIntegrator Then
                                                sLine = sLine & Chr(34) & Left$(Trim$(oInvoice.CustomerNo) & "/" & Trim$(oInvoice.InvoiceNo) & " " & Trim$(oPayment.E_Name), 20) & Chr(34) & ","  '38 Egenref
                                            Else
                                                sLine = sLine & Chr(34) & Left$(Trim$(oInvoice.REF_Own), 20) & Chr(34) & ","  '38 Egenref
                                            End If

                                            sLine = sLine & ",,,,"                             '39-42, numeriske, ikke i bruk
                                            sLine = sLine & "0,"                                               ' 43 Sjekklevering

                                            If oPayment.PayType <> "I" Then    ' Domestic records
                                                sLine = sLine & Chr(34) & Chr(34) & ","             '44 Kursref, (not in use)
                                            Else
                                                sLine = sLine & Chr(34) & oPayment.ERA_DealMadeWith & Chr(34) & ","           '44 Kursref, (not in use)
                                            End If
                                            sLine = sLine & Chr(34) & sKortartKode & Chr(34) & ","  '45 Kortartkode
                                            sLine = sLine & sBetalingsRef & ","                     '46 Betalingsref, FIKort only
                                            If oPayment.PayType <> "I" Then    ' Domestic records
                                                sLine = sLine & ","                                 '47 Kurs, (not in use)
                                            Else
                                                If oPayment.ERA_ExchRateAgreed = 0 Then
                                                    sLine = sLine & ","                                 '47 Avtalt Kurs
                                                Else
                                                    sLine = sLine & oPayment.ERA_ExchRateAgreed & ","   '47 Avtalt Kurs
                                                End If
                                            End If
                                            sLine = sLine & ","                                 '48 Overf�rselsform (not in use)
                                            sLine = sLine & Chr(34) & Chr(34) & ","             '49 Medelelse til Nordea, (not in use)
                                            sLine = sLine & Chr(34) & Chr(34) & ","             '50 Meddelelse til Nordea, (not in use)
                                            If oPayment.PayType <> "I" Then    ' Domestic records
                                                sLine = sLine & sTextcode & ","                     '51 Tekstkode
                                                sLine = sLine & ","                                 '52 Samlerpostnr (not in use)
                                                ' Field 53 only if 45-payments;
                                                If xDelim(sLine, ",", 2) = "45" And sTextcode = "100" Then
                                                    sShortNotification = aFreetexts(0)
                                                    sLine = sLine & Chr(34) & sShortNotification & Chr(34)  '53 Kort fri advis
                                                Else
                                                    sLine = sLine & Chr(34) & "" & Chr(34)  '53 Kort fri advis
                                                End If

                                                ' Field 54-90 only if 45 or 46 -payments;
                                                If xDelim(sLine, ",", 2) = Chr(34) & "45" & Chr(34) Or xDelim(sLine, ",", 2) = Chr(34) & "46" & Chr(34) Then

                                                    sLine = sLine & ","
                                                    ' Upto 37 extra notificationlines:
                                                    For nCount = 4 To 40
                                                        If UBound(aFreetexts) >= nCount Then
                                                            sLine = sLine & Chr(34) & aFreetexts(nCount) & Chr(34) & "," '54-90 Lang advis, a 35 kar
                                                        Else
                                                            sLine = sLine & Chr(34) & Chr(34) & "," '54-90 Lang advis, a 35 kar
                                                        End If
                                                    Next nCount
                                                    sLine = sLine & "0,"                                 '91 Straks advis
                                                    sLine = sLine & Chr(34) & Chr(34) & ","             '92 Bel�psmodtagers ident. av bel�bsafseder
                                                    If xDelim(sLine, ",", 2) = "45" Then
                                                        sLine = sLine & Chr(34) & Trim$(oInvoice.InvoiceNo) & Chr(34)                     '93 Reference til prim�r dokument
                                                    Else
                                                        sLine = sLine & Chr(34) & Chr(34)                      '93 Reference til prim�r dokument
                                                    End If

                                                End If ' If xDelim(sLine, ",", 2) = "45" Or xDelim(sLine, ",", 2) = "46" Then
                                            Else
                                                ' International
                                                ' do not use columns 51-93
                                                For nCount = 51 To 93
                                                    sLine = sLine & ","
                                                Next nCount

                                                If oPayment.MON_ChargeMeAbroad And oPayment.MON_ChargeMeDomestic Then
                                                    sLine = sLine & Chr(34) & "A" & Chr(34) & ","     ' 94 Omkostningskode OUROUR
                                                Else
                                                    sLine = sLine & Chr(34) & "N" & Chr(34) & ""     ' 94 Omkostningskode, shared
                                                End If
                                            End If

                                            If oPayment.BANK_I_Domestic Then
                                                ' Request for transfer, felt 95-111 er forel�pig ikke implementert
                                                'sLine = sLine & ","
                                            End If

                                            If Len(sLine) > 0 Then
                                                oFile.WriteLine(sLine)
                                            End If
                                            'oPayment.Exported = True
                                            ' new 24.10.03, Tribe Hotel Denmark
                                            ' only one run for payments without FIK
                                            ' one run pr invoice for those with FIK
                                            'If oPayment.PayCode <> "301" And oPayment.PayCode <> "302" Then
                                            '    Exit For
                                            'End If
                                            ' XokNET 13.05.2014
                                            oInvoice.Exported = True

                                        End If 'If Not oInvoice.Exported Then

                                    Next ' invoice
                                End If  'bExportToPayment
                                oPayment.Exported = True
                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteNordeaUnitel" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteNordeaUnitel = True

    End Function

End Module
