Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Payments_NET.Payments")> Public Class Payments
	Implements System.Collections.IEnumerable
	
	'local variable to hold collection
	Private mCol As Collection
	Private aPaymentsSorted(,) AS Double
    Private oCargo As vbbabel.Cargo
	Public Function Add(Optional ByRef sKey As String = "") As vbbabel.Payment
		'create a new object
		Dim objNewMember As vbbabel.Payment
		objNewMember = New vbbabel.Payment
		sKey = Str(mCol.Count() + 1)
		
		'set the properties passed into the method
		If Len(sKey) = 0 Then
			mCol.Add(objNewMember)
		Else
			mCol.Add(objNewMember, sKey)
			objNewMember.Index = CObj(sKey)
		End If
		
		objNewMember.Cargo = oCargo
		
		'return the object created
		Add = objNewMember
		'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		objNewMember = Nothing
		
		
	End Function
	
	Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As vbbabel.Payment
		Get
			'used when referencing an element in the collection
			'vntIndexKey contains either the Index or Key to the collection,
			'this is why it is declared as a Variant
			'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
			Item = mCol.Item(vntIndexKey)
		End Get
	End Property
	Public ReadOnly Property Count() As Integer
		Get
			'used when retrieving the number of elements in the
			'collection. Syntax: Debug.Print x.Count
			Count = mCol.Count()
		End Get
	End Property
	
	
	'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
	'Public ReadOnly Property NewEnum() As stdole.IUnknown
		'Get
			'this property allows you to enumerate
			'this collection with the For...Each syntax
			'NewEnum = mCol._NewEnum
		'End Get
	'End Property
	
	Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
		'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        GetEnumerator = mCol.GetEnumerator
	End Function
	Friend WriteOnly Property Cargo() As vbbabel.Cargo
		Set(ByVal Value As vbbabel.Cargo)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If
			
			'Store reference
			oCargo = Value
		End Set
	End Property
	Public ReadOnly Property VB_Sorting() As Object
		Get
            VB_Sorting = aPaymentsSorted
		End Get
	End Property
	
	
    Public Sub Remove(ByRef vntIndexKey As Integer)  ' 28.12.2010 from As Object
        'used when removing an element from the collection
        'vntIndexKey contains either the Index or Key, which is why
        'it is declared as a Variant
        'Syntax: x.Remove(xyz)


        mCol.Remove(vntIndexKey)
    End Sub
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		'creates the collection when this class is created
		mCol = New Collection
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'destroys collection when this class is terminated
		'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mCol = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	
	Friend Function SetPaymentsSorted(ByRef Index As Integer, ByRef iOrigIndex As Integer, ByRef lChangeValue As Integer) As Boolean ' 11.05.10 from Int to long
        ReDim Preserve aPaymentsSorted(2, Index)
        aPaymentsSorted(0, Index) = iOrigIndex
        aPaymentsSorted(1, Index) = lChangeValue
        aPaymentsSorted(2, Index) = 0
	End Function
	Friend Function AddTotalAmount(ByRef Index As Integer, ByRef nTotalAmount As Double) As Boolean
        aPaymentsSorted(2, Index) = nTotalAmount
	End Function
	Public Function VB_AddWithObject(ByRef oExistingPayment As Payment) As vbbabel.Payment
		Dim sKey As String
		'create a new object
		sKey = Str(mCol.Count() + 1)

        'mCol.Add(oExistingPayment, sKey)
        mCol.Add(oExistingPayment)
		oExistingPayment.Index = CObj(sKey)
		
		'return the object created
		VB_AddWithObject = oExistingPayment
		
	End Function
End Class
