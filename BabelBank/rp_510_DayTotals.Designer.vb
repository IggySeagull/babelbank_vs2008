<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_510_DayTotals
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_510_DayTotals))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtIB = New DataDynamics.ActiveReports.TextBox
        Me.lblIB = New DataDynamics.ActiveReports.Label
        Me.lblImported = New DataDynamics.ActiveReports.Label
        Me.Line1Detail = New DataDynamics.ActiveReports.Line
        Me.lblTotalAfterImport = New DataDynamics.ActiveReports.Label
        Me.lblProductionToday = New DataDynamics.ActiveReports.Label
        Me.lblOCR = New DataDynamics.ActiveReports.Label
        Me.lblMatched = New DataDynamics.ActiveReports.Label
        Me.lblMatchedGL = New DataDynamics.ActiveReports.Label
        Me.Line2Detail = New DataDynamics.ActiveReports.Line
        Me.lblUB = New DataDynamics.ActiveReports.Label
        Me.lblUnMatched = New DataDynamics.ActiveReports.Label
        Me.lblDiff = New DataDynamics.ActiveReports.Label
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Line2 = New DataDynamics.ActiveReports.Line
        Me.txtImported = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalAfterImport = New DataDynamics.ActiveReports.TextBox
        Me.txtOCR = New DataDynamics.ActiveReports.TextBox
        Me.txtMatched = New DataDynamics.ActiveReports.TextBox
        Me.txtMatchedGL = New DataDynamics.ActiveReports.TextBox
        Me.txtUB = New DataDynamics.ActiveReports.TextBox
        Me.txtUnMatched = New DataDynamics.ActiveReports.TextBox
        Me.txtDiff = New DataDynamics.ActiveReports.TextBox
        Me.lblUnmatchedERP = New DataDynamics.ActiveReports.Label
        Me.txtUnmatchedERP = New DataDynamics.ActiveReports.TextBox
        Me.lblDiffERP = New DataDynamics.ActiveReports.Label
        Me.txtDiffERP = New DataDynamics.ActiveReports.TextBox
        Me.Line4 = New DataDynamics.ActiveReports.Line
        Me.Line5 = New DataDynamics.ActiveReports.Line
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        CType(Me.txtIB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblIB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblImported, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAfterImport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblProductionToday, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOCR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMatchedGL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDiff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtImported, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAfterImport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOCR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedGL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnmatchedERP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnmatchedERP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDiffERP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiffERP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtIB, Me.lblIB, Me.lblImported, Me.Line1Detail, Me.lblTotalAfterImport, Me.lblProductionToday, Me.lblOCR, Me.lblMatched, Me.lblMatchedGL, Me.Line2Detail, Me.lblUB, Me.lblUnMatched, Me.lblDiff, Me.Line1, Me.Line2, Me.txtImported, Me.txtTotalAfterImport, Me.txtOCR, Me.txtMatched, Me.txtMatchedGL, Me.txtUB, Me.txtUnMatched, Me.txtDiff, Me.lblUnmatchedERP, Me.txtUnmatchedERP, Me.lblDiffERP, Me.txtDiffERP, Me.Line4, Me.Line5})
        Me.Detail.Height = 3.635417!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtIB
        '
        Me.txtIB.DataField = "IB"
        Me.txtIB.Height = 0.19!
        Me.txtIB.Left = 1.9!
        Me.txtIB.Name = "txtIB"
        Me.txtIB.OutputFormat = resources.GetString("txtIB.OutputFormat")
        Me.txtIB.Style = "text-align: right"
        Me.txtIB.Text = "txtIB"
        Me.txtIB.Top = 0.2!
        Me.txtIB.Width = 1.7!
        '
        'lblIB
        '
        Me.lblIB.Height = 0.19!
        Me.lblIB.HyperLink = Nothing
        Me.lblIB.Left = 0.5!
        Me.lblIB.Name = "lblIB"
        Me.lblIB.Style = "font-size: 10pt"
        Me.lblIB.Text = "lblIB"
        Me.lblIB.Top = 0.2!
        Me.lblIB.Width = 1.4!
        '
        'lblImported
        '
        Me.lblImported.Height = 0.19!
        Me.lblImported.HyperLink = Nothing
        Me.lblImported.Left = 0.5!
        Me.lblImported.Name = "lblImported"
        Me.lblImported.Style = "font-size: 10pt"
        Me.lblImported.Text = "lblImported"
        Me.lblImported.Top = 0.4!
        Me.lblImported.Width = 1.4!
        '
        'Line1Detail
        '
        Me.Line1Detail.Height = 0.0!
        Me.Line1Detail.Left = 0.4!
        Me.Line1Detail.LineWeight = 1.0!
        Me.Line1Detail.Name = "Line1Detail"
        Me.Line1Detail.Top = 0.625!
        Me.Line1Detail.Width = 3.3!
        Me.Line1Detail.X1 = 3.7!
        Me.Line1Detail.X2 = 0.4!
        Me.Line1Detail.Y1 = 0.625!
        Me.Line1Detail.Y2 = 0.625!
        '
        'lblTotalAfterImport
        '
        Me.lblTotalAfterImport.Height = 0.19!
        Me.lblTotalAfterImport.HyperLink = Nothing
        Me.lblTotalAfterImport.Left = 0.5!
        Me.lblTotalAfterImport.Name = "lblTotalAfterImport"
        Me.lblTotalAfterImport.Style = "font-size: 10pt"
        Me.lblTotalAfterImport.Text = "lblTotalAfterImport"
        Me.lblTotalAfterImport.Top = 0.65!
        Me.lblTotalAfterImport.Width = 1.4!
        '
        'lblProductionToday
        '
        Me.lblProductionToday.Height = 0.19!
        Me.lblProductionToday.HyperLink = Nothing
        Me.lblProductionToday.Left = 0.5!
        Me.lblProductionToday.Name = "lblProductionToday"
        Me.lblProductionToday.Style = "font-size: 10pt"
        Me.lblProductionToday.Text = "lblProductionToday"
        Me.lblProductionToday.Top = 1.0!
        Me.lblProductionToday.Width = 1.4!
        '
        'lblOCR
        '
        Me.lblOCR.Height = 0.19!
        Me.lblOCR.HyperLink = Nothing
        Me.lblOCR.Left = 0.6!
        Me.lblOCR.Name = "lblOCR"
        Me.lblOCR.Style = "font-size: 10pt"
        Me.lblOCR.Text = "lblOCR"
        Me.lblOCR.Top = 1.25!
        Me.lblOCR.Width = 1.3!
        '
        'lblMatched
        '
        Me.lblMatched.Height = 0.19!
        Me.lblMatched.HyperLink = Nothing
        Me.lblMatched.Left = 0.6!
        Me.lblMatched.Name = "lblMatched"
        Me.lblMatched.Style = "font-size: 10pt"
        Me.lblMatched.Text = "lblMatched"
        Me.lblMatched.Top = 1.45!
        Me.lblMatched.Width = 1.3!
        '
        'lblMatchedGL
        '
        Me.lblMatchedGL.Height = 0.19!
        Me.lblMatchedGL.HyperLink = Nothing
        Me.lblMatchedGL.Left = 0.6!
        Me.lblMatchedGL.Name = "lblMatchedGL"
        Me.lblMatchedGL.Style = "font-size: 10pt"
        Me.lblMatchedGL.Text = "lblMatchedGL"
        Me.lblMatchedGL.Top = 1.65!
        Me.lblMatchedGL.Width = 1.3!
        '
        'Line2Detail
        '
        Me.Line2Detail.Height = 0.0!
        Me.Line2Detail.Left = 0.4!
        Me.Line2Detail.LineWeight = 1.0!
        Me.Line2Detail.Name = "Line2Detail"
        Me.Line2Detail.Top = 1.9!
        Me.Line2Detail.Width = 3.3!
        Me.Line2Detail.X1 = 3.7!
        Me.Line2Detail.X2 = 0.4!
        Me.Line2Detail.Y1 = 1.9!
        Me.Line2Detail.Y2 = 1.9!
        '
        'lblUB
        '
        Me.lblUB.Height = 0.19!
        Me.lblUB.HyperLink = Nothing
        Me.lblUB.Left = 0.5!
        Me.lblUB.Name = "lblUB"
        Me.lblUB.Style = "font-size: 10pt"
        Me.lblUB.Text = "lblUB"
        Me.lblUB.Top = 2.0!
        Me.lblUB.Width = 1.4!
        '
        'lblUnMatched
        '
        Me.lblUnMatched.Height = 0.19!
        Me.lblUnMatched.HyperLink = Nothing
        Me.lblUnMatched.Left = 0.5!
        Me.lblUnMatched.Name = "lblUnMatched"
        Me.lblUnMatched.Style = "font-size: 10pt"
        Me.lblUnMatched.Text = "lblUnMatched"
        Me.lblUnMatched.Top = 2.2!
        Me.lblUnMatched.Width = 1.4!
        '
        'lblDiff
        '
        Me.lblDiff.Height = 0.19!
        Me.lblDiff.HyperLink = Nothing
        Me.lblDiff.Left = 0.5!
        Me.lblDiff.Name = "lblDiff"
        Me.lblDiff.Style = "font-size: 9.75pt; font-weight: bold"
        Me.lblDiff.Text = "lblDiff"
        Me.lblDiff.Top = 2.4375!
        Me.lblDiff.Width = 1.4!
        '
        'Line1
        '
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.4!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 2.65!
        Me.Line1.Width = 3.3!
        Me.Line1.X1 = 3.7!
        Me.Line1.X2 = 0.4!
        Me.Line1.Y1 = 2.65!
        Me.Line1.Y2 = 2.65!
        '
        'Line2
        '
        Me.Line2.Height = 0.0!
        Me.Line2.Left = 0.4!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 2.68!
        Me.Line2.Width = 3.3!
        Me.Line2.X1 = 3.7!
        Me.Line2.X2 = 0.4!
        Me.Line2.Y1 = 2.68!
        Me.Line2.Y2 = 2.68!
        '
        'txtImported
        '
        Me.txtImported.DataField = "Imported"
        Me.txtImported.Height = 0.19!
        Me.txtImported.Left = 1.9!
        Me.txtImported.Name = "txtImported"
        Me.txtImported.OutputFormat = resources.GetString("txtImported.OutputFormat")
        Me.txtImported.Style = "text-align: right"
        Me.txtImported.Text = "txtImported"
        Me.txtImported.Top = 0.4!
        Me.txtImported.Width = 1.7!
        '
        'txtTotalAfterImport
        '
        Me.txtTotalAfterImport.DataField = "= IB + Imported"
        Me.txtTotalAfterImport.Height = 0.19!
        Me.txtTotalAfterImport.Left = 1.9!
        Me.txtTotalAfterImport.Name = "txtTotalAfterImport"
        Me.txtTotalAfterImport.OutputFormat = resources.GetString("txtTotalAfterImport.OutputFormat")
        Me.txtTotalAfterImport.Style = "text-align: right"
        Me.txtTotalAfterImport.Text = "txtTotalAfterImport"
        Me.txtTotalAfterImport.Top = 0.65!
        Me.txtTotalAfterImport.Width = 1.7!
        '
        'txtOCR
        '
        Me.txtOCR.DataField = "OCR"
        Me.txtOCR.Height = 0.19!
        Me.txtOCR.Left = 1.9!
        Me.txtOCR.Name = "txtOCR"
        Me.txtOCR.OutputFormat = resources.GetString("txtOCR.OutputFormat")
        Me.txtOCR.Style = "text-align: right"
        Me.txtOCR.Text = "txtOCR"
        Me.txtOCR.Top = 1.25!
        Me.txtOCR.Width = 1.7!
        '
        'txtMatched
        '
        Me.txtMatched.DataField = "Matched_BB"
        Me.txtMatched.Height = 0.19!
        Me.txtMatched.Left = 1.9!
        Me.txtMatched.Name = "txtMatched"
        Me.txtMatched.OutputFormat = resources.GetString("txtMatched.OutputFormat")
        Me.txtMatched.Style = "text-align: right"
        Me.txtMatched.Text = "txtMatched"
        Me.txtMatched.Top = 1.45!
        Me.txtMatched.Width = 1.7!
        '
        'txtMatchedGL
        '
        Me.txtMatchedGL.DataField = "Matched_GL"
        Me.txtMatchedGL.Height = 0.19!
        Me.txtMatchedGL.Left = 1.9!
        Me.txtMatchedGL.Name = "txtMatchedGL"
        Me.txtMatchedGL.OutputFormat = resources.GetString("txtMatchedGL.OutputFormat")
        Me.txtMatchedGL.Style = "text-align: right"
        Me.txtMatchedGL.Text = "txtMatchedGL"
        Me.txtMatchedGL.Top = 1.65!
        Me.txtMatchedGL.Width = 1.7!
        '
        'txtUB
        '
        Me.txtUB.DataField = "= IB + Imported - OCR - Matched_BB - Matched_GL"
        Me.txtUB.Height = 0.19!
        Me.txtUB.Left = 1.9!
        Me.txtUB.Name = "txtUB"
        Me.txtUB.OutputFormat = resources.GetString("txtUB.OutputFormat")
        Me.txtUB.Style = "text-align: right"
        Me.txtUB.Text = "txtUB"
        Me.txtUB.Top = 2.0!
        Me.txtUB.Width = 1.7!
        '
        'txtUnMatched
        '
        Me.txtUnMatched.DataField = "UnMatched_BB"
        Me.txtUnMatched.Height = 0.19!
        Me.txtUnMatched.Left = 1.9!
        Me.txtUnMatched.Name = "txtUnMatched"
        Me.txtUnMatched.OutputFormat = resources.GetString("txtUnMatched.OutputFormat")
        Me.txtUnMatched.Style = "text-align: right"
        Me.txtUnMatched.Text = "txtUnMatched"
        Me.txtUnMatched.Top = 2.2!
        Me.txtUnMatched.Width = 1.7!
        '
        'txtDiff
        '
        Me.txtDiff.DataField = "= IB + Imported - OCR - Matched_BB - Matched_GL- Unmatched_BB"
        Me.txtDiff.Height = 0.19!
        Me.txtDiff.Left = 1.9!
        Me.txtDiff.Name = "txtDiff"
        Me.txtDiff.OutputFormat = resources.GetString("txtDiff.OutputFormat")
        Me.txtDiff.Style = "font-size: 9.75pt; font-weight: bold; text-align: right"
        Me.txtDiff.Text = "txtDiff"
        Me.txtDiff.Top = 2.438!
        Me.txtDiff.Width = 1.7!
        '
        'lblUnmatchedERP
        '
        Me.lblUnmatchedERP.Height = 0.19!
        Me.lblUnmatchedERP.HyperLink = Nothing
        Me.lblUnmatchedERP.Left = 0.5!
        Me.lblUnmatchedERP.Name = "lblUnmatchedERP"
        Me.lblUnmatchedERP.Style = "font-size: 10pt"
        Me.lblUnmatchedERP.Text = "lblUnmatchedERP"
        Me.lblUnmatchedERP.Top = 2.99!
        Me.lblUnmatchedERP.Visible = False
        Me.lblUnmatchedERP.Width = 1.4!
        '
        'txtUnmatchedERP
        '
        Me.txtUnmatchedERP.Height = 0.19!
        Me.txtUnmatchedERP.Left = 1.9!
        Me.txtUnmatchedERP.Name = "txtUnmatchedERP"
        Me.txtUnmatchedERP.OutputFormat = resources.GetString("txtUnmatchedERP.OutputFormat")
        Me.txtUnmatchedERP.Style = "text-align: right"
        Me.txtUnmatchedERP.Text = "txtUnmatchedERP"
        Me.txtUnmatchedERP.Top = 2.99!
        Me.txtUnmatchedERP.Visible = False
        Me.txtUnmatchedERP.Width = 1.7!
        '
        'lblDiffERP
        '
        Me.lblDiffERP.Height = 0.19!
        Me.lblDiffERP.HyperLink = Nothing
        Me.lblDiffERP.Left = 0.5!
        Me.lblDiffERP.Name = "lblDiffERP"
        Me.lblDiffERP.Style = "font-size: 9.75pt; font-weight: bold"
        Me.lblDiffERP.Text = "lblDiffERP"
        Me.lblDiffERP.Top = 3.18!
        Me.lblDiffERP.Visible = False
        Me.lblDiffERP.Width = 1.4!
        '
        'txtDiffERP
        '
        Me.txtDiffERP.DataField = "= IB + Imported - OCR - Matched_BB - Matched_GL- Unmatched_BB"
        Me.txtDiffERP.Height = 0.19!
        Me.txtDiffERP.Left = 1.9!
        Me.txtDiffERP.Name = "txtDiffERP"
        Me.txtDiffERP.OutputFormat = resources.GetString("txtDiffERP.OutputFormat")
        Me.txtDiffERP.Style = "font-size: 9.75pt; font-weight: bold; text-align: right"
        Me.txtDiffERP.Text = "txtDiffERP"
        Me.txtDiffERP.Top = 3.18!
        Me.txtDiffERP.Visible = False
        Me.txtDiffERP.Width = 1.7!
        '
        'Line4
        '
        Me.Line4.Height = 0.0!
        Me.Line4.Left = 0.4000003!
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 3.37!
        Me.Line4.Visible = False
        Me.Line4.Width = 3.3!
        Me.Line4.X1 = 3.7!
        Me.Line4.X2 = 0.4000003!
        Me.Line4.Y1 = 3.37!
        Me.Line4.Y2 = 3.37!
        '
        'Line5
        '
        Me.Line5.Height = 0.0!
        Me.Line5.Left = 0.4000003!
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 3.41!
        Me.Line5.Visible = False
        Me.Line5.Width = 3.3!
        Me.Line5.X1 = 3.7!
        Me.Line5.X2 = 0.4000003!
        Me.Line5.Y1 = 3.41!
        Me.Line5.Y2 = 3.41!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4576389!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Daytotals"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.299305!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.299305!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.lblDate_Production, Me.txtDATE_Production, Me.lblClient, Me.txtClientName, Me.txtI_Account, Me.lblI_Account})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.6347222!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.1!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.06!
        Me.lblDate_Production.Width = 1.313!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.0625!
        Me.txtDATE_Production.Width = 1.563!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.0604167!
        Me.lblClient.Width = 1.0!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.0604167!
        Me.txtClientName.Width = 1.9!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "AccountNo"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.2504167!
        Me.txtI_Account.Width = 1.9!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.2504167!
        Me.lblI_Account.Width = 1.0!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Height = 0.0625!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'rp_510_DayTotals
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.489583!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.txtIB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblIB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblImported, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAfterImport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblProductionToday, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOCR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMatchedGL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDiff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtImported, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAfterImport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOCR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedGL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnmatchedERP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnmatchedERP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDiffERP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiffERP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtIB As DataDynamics.ActiveReports.TextBox
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Friend WithEvents lblIB As DataDynamics.ActiveReports.Label
    Friend WithEvents lblImported As DataDynamics.ActiveReports.Label
    Friend WithEvents Line1Detail As DataDynamics.ActiveReports.Line
    Friend WithEvents lblTotalAfterImport As DataDynamics.ActiveReports.Label
    Friend WithEvents lblProductionToday As DataDynamics.ActiveReports.Label
    Friend WithEvents lblOCR As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMatched As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMatchedGL As DataDynamics.ActiveReports.Label
    Friend WithEvents Line2Detail As DataDynamics.ActiveReports.Line
    Friend WithEvents lblUB As DataDynamics.ActiveReports.Label
    Friend WithEvents lblUnMatched As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDiff As DataDynamics.ActiveReports.Label
    Friend WithEvents Line1 As DataDynamics.ActiveReports.Line
    Friend WithEvents Line2 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtImported As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalAfterImport As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOCR As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMatched As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMatchedGL As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUB As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnMatched As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDiff As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblUnmatchedERP As DataDynamics.ActiveReports.Label
    Private WithEvents txtUnmatchedERP As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblDiffERP As DataDynamics.ActiveReports.Label
    Private WithEvents txtDiffERP As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line4 As DataDynamics.ActiveReports.Line
    Private WithEvents Line5 As DataDynamics.ActiveReports.Line
End Class
