﻿Option Strict Off
Option Explicit On
Module Write_OCBC

    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    Dim nBatchDebitAmount As Double
    Dim nBatchCreditAmount As Double
    Dim nFileDebitAmount As Double
    Dim nFileCreditAmount As Double
    Dim nFileAmount As Double
    Dim iNoOfRecords As Double = 0
    Dim sSpecial As String
    Dim iBatchSequence As Integer = 0

    Public Function WriteOCBC_SG_Giro(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sCompanyNo As String, ByVal sBranch As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sNewOwnref As String
        Dim oGroupPayment As vbBabel.GroupPayments
        Dim lCounter As Long = 0
        Dim bBatchWritten As Boolean = False
        ' added 29.05.2019
        Dim sOldAccount As String = ""
        Dim dOldDate As Date

        ' lag en outputfil
        Try

            oGroupPayment = New vbBabel.GroupPayments
            oGroupPayment.GroupCrossBorderPayments = True
            oGroupPayment.SetGroupingParameters(True, True, False, True, True, False)

            oFs = New Scripting.FileSystemObject
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                sSpecial = oBabel.Special

                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled
                            If oPayment.Cancel = False And Not oPayment.Exported Then
                                ' for SG debit bank only
                                If oPayment.BANK_CountryCode = "SG" Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And Not oPayment.Exported Then
                                    ' for SG debit bank only
                                    If oPayment.BANK_CountryCode = "SG" Then
                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If 'If oPayment.Cancel = False Then
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then

                            For lCounter = 1 To oBatch.Payments.Count

                                oGroupPayment.Batch = oBatch
                                oGroupPayment.MarkIdenticalPayments(True)

                                bBatchWritten = False ' Write WriteOCBC_SG_Giro_BatchStart(oBatch) further down

                                For Each oPayment In oBatch.Payments
                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects to
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        'Don't export payments that have been cancelled
                                        If oPayment.Cancel = False And Not oPayment.Exported Then
                                            ' for SG debit bank only
                                            If oPayment.BANK_CountryCode = "SG" Then
                                                If Not bMultiFiles Then
                                                    bExportoPayment = True
                                                Else
                                                    If oPayment.I_Account = sI_Account Then
                                                        If InStr(oPayment.REF_Own, "&?") Then
                                                            'Set in the part of the OwnRef that BabelBank is using
                                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                        Else
                                                            sNewOwnref = ""
                                                        End If
                                                        If sNewOwnref = sOwnRef Then
                                                            bExportoPayment = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Else
                                            'oPayment.Exported = True
                                        End If 'If oPayment.Cancel = False Then
                                    End If

                                    If bExportoPayment Then
                                        ' 29.05.2019 - write a new header pr paymentdate og debitaccount
                                        If sOldAccount <> oPayment.I_Account Then
                                            bBatchWritten = False
                                        End If
                                        If dOldDate <> StringToDate(oPayment.DATE_Payment) Then
                                            bBatchWritten = False
                                        End If

                                        If Not bBatchWritten Then
                                            ' Mut write batch here, to be able to find date for correct payment!
                                            sLine = WriteOCBC_SG_Giro_BatchStart(oBatch, oPayment)
                                            oFile.WriteLine(sLine)
                                            bBatchWritten = True
                                        End If

                                        For Each oInvoice In oPayment.Invoices
                                            sLine = WriteOCBC_SG_Giro_DetailRecord(oPayment, oInvoice)
                                            oFile.WriteLine(sLine)
                                            sLine = WriteOCBC_SG_Giro_InvRecord(oPayment, oInvoice)
                                            oFile.WriteLine(sLine)

                                        Next oInvoice
                                        oPayment.Exported = True
                                        sOldAccount = oPayment.I_Account
                                        dOldDate = StringToDate(oPayment.DATE_Payment)
                                    End If
                                Next oPayment ' payment

                                oGroupPayment.RemoveSpecialMark()
                                If oGroupPayment.AllPaymentsMarkedAsExported Then
                                    Exit For
                                End If

                            Next lCounter
                        End If

                    Next oBatch 'batch
                End If
            Next oBabel 'Babelfile


        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteOCBC_SG_Giro" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oGroupPayment Is Nothing Then
                oGroupPayment = Nothing
            End If

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteOCBC_SG_Giro = True

    End Function
    Private Function WriteOCBC_SG_Giro_BatchStart(ByVal oBatch As vbBabel.Batch, ByVal oPayment As vbBabel.Payment) As String
        ' Needs one header pr debitaccount and type of payment
        Dim sLine As String
        Dim dValueDate As Date  ' added 28.05.2019
        Dim sValueDate As String = ""

        ' set valuedate 2 bankdays ahead;
        dValueDate = StringToDate(oPayment.DATE_Payment)
        If dValueDate < DateAdd(DateInterval.Day, 2, Date.Today) Then
            dValueDate = GetBankday(Date.Today, "SG", 2)
        End If

        If oBatch.Payments(1).PayType = "S" Then
            sLine = "30"        ' 1  1-2 Transaction type, Payroll = 30
        Else
            sLine = "10"        ' 1 1-2Transaction type, Payment = 10
        End If
        sLine = sLine & Space(3) '2 3-5 Batch number, optional
        If sSpecial = "NHST_SG" Or sSpecial = "NHST_SG_LONN" Then  ' 07.04.2022 added NHST_SG_LONN
            sLine = sLine & oPayment.DATE_Payment
        Else
            sLine = sLine & Format(Date.Today, "yyyyMMdd") '3 6-13 Submission date YYYYMMDD
        End If
        sLine = sLine & "OCBCSGSGXXX"  ' 4 14-24 Originating bank code
        sLine = sLine & PadRight(oPayment.I_Account, 34, " ")    '5 25-58 Your account number

        sLine = sLine & Space(3) '6  59-61 Filler 
        sLine = sLine & Space(20)  ' 7 62-81 On behalf off (Optional)
        sLine = sLine & Space(120)  '8 82- 201 Filler
        sLine = sLine & Space(4)    '9 202-205 Charge bearer (blank)
        sLine = sLine & "GIRO"      '10 206-209 Clearing FAST or GIRO - SO FAR GIRO ONLY !!!
        sLine = sLine & Space(16)   '11 210-225 Your ref
        'sLine = sLine & Right(oPayment.DATE_Payment, 2) & Mid(oPayment.DATE_Payment, 5, 2) & Left(oPayment.DATE_Payment, 4)  '12 226-233 Value date ddmmyyyy
        sValueDate = DateToString(dValueDate)
        sLine = sLine & Right(sValueDate, 2) & Mid(sValueDate, 5, 2) & Left(sValueDate, 4)  '12 226-233 Value date ddmmyyyy
        sLine = sLine & Space(4)     '12 234-237 Value time optional
        sLine = sLine & Space(1)     '13 238 Batch booking (blank)
        sLine = sLine & Space(762)   '14 239-1000 Filler

        WriteOCBC_SG_Giro_BatchStart = sLine

    End Function
    Private Function WriteOCBC_SG_Giro_DetailRecord(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String = ""
        Dim oFreetext As Freetext
        Dim sTxt As String = ""


        If EmptyString(oInvoice.Unique_Id) Then
            For Each oFreetext In oInvoice.Freetexts
                sTxt = sTxt & " " & Trim$(oFreetext.Text)
            Next
        Else
            sTxt = oInvoice.Unique_Id
        End If
        sTxt = Trim(sTxt)

        sLine = PadRight(oPayment.BANK_SWIFTCode, 11, "X")   ' 1 1-11 bank code
        sLine = sLine & PadRight(oPayment.E_Account, 34, " ")   '2 12-45 Accountno
        sLine = sLine & PadRight(oPayment.E_Name, 140, " ")   '3 46-185 Name
        sLine = sLine & Space(3)   ' 4 186-188 Currency BLANKS
        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)).Trim, 17, "0") '5 189-205 Transaction amount
        sLine = sLine & PadRight(sTxt, 35, " ")   '6 206-240 Payment details

        If oPayment.PayType = "S" Then
            sLine = sLine & "SALA"  ' 7 241-244 Purpose code
        Else
            If Len(oPayment.PurposeCode) = 4 Then
                sLine = sLine & oPayment.PurposeCode  '7 241-244 Purpose code
            Else
                sLine = sLine & "OTHR"  ' 7 241-244 Purpose code
            End If
        End If
        If Not EmptyString(oInvoice.REF_Own) Then
            sLine = sLine & PadRight(oInvoice.REF_Own, 35, " ")   ' 8 245-279 Debtors ref
        Else
            sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")   ' 8 245-279 Debtors ref
        End If
        sLine = sLine & Space(140)   '9 280-419  Ultimate creditor name
        sLine = sLine & Space(140)   '10 420-559  Ultimate debtor name
        sLine = sLine & Space(1)     '11 560  Optional Send via
        sLine = sLine & Space(255)   '12 561-815  Optional Send details++
        sLine = sLine & Space(12)    '13 816-827  Proxytype (for PayNow only)
        sLine = sLine & Space(140)   '14 828-967  Proxyvalue (for PayNow only)
        sLine = sLine & Space(33)    '15 968-1000  Filler


        WriteOCBC_SG_Giro_DetailRecord = sLine

    End Function
    Private Function WriteOCBC_SG_Giro_InvRecord(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String = ""
        Dim oFreetext As Freetext
        Dim sTxt As String = ""


        If EmptyString(oInvoice.Unique_Id) Then
            For Each oFreetext In oInvoice.Freetexts
                sTxt = sTxt & " " & Trim$(oFreetext.Text)
            Next
        Else
            sTxt = oInvoice.Unique_Id
        End If
        sTxt = Trim(sTxt)

        sLine = "INV"       '1  1-3
        sLine = sLine & PadRight(sTxt, 97, " ")   '2 4-100 Payment details

        WriteOCBC_SG_Giro_InvRecord = sLine

    End Function


End Module
