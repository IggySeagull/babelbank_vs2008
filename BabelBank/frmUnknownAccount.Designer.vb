<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmUnknownAccount
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtFilename1Hidden As System.Windows.Forms.TextBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtFileNameOut1 As System.Windows.Forms.TextBox
	Public WithEvents txtFormatOut As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtAccountNo As System.Windows.Forms.TextBox
	Public WithEvents txtClientNo As System.Windows.Forms.TextBox
	Public WithEvents lblFilenameOut1 As System.Windows.Forms.Label
	Public WithEvents lblFormatOut As System.Windows.Forms.Label
	Public WithEvents lblAccountNo As System.Windows.Forms.Label
	Public WithEvents lblClientNo As System.Windows.Forms.Label
	Public WithEvents lblDescription As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUnknownAccount))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtFilename1Hidden = New System.Windows.Forms.TextBox
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.txtFileNameOut1 = New System.Windows.Forms.TextBox
		Me.txtFormatOut = New System.Windows.Forms.TextBox
		Me.cmdOK = New System.Windows.Forms.Button
		Me.txtAccountNo = New System.Windows.Forms.TextBox
		Me.txtClientNo = New System.Windows.Forms.TextBox
		Me.lblFilenameOut1 = New System.Windows.Forms.Label
		Me.lblFormatOut = New System.Windows.Forms.Label
		Me.lblAccountNo = New System.Windows.Forms.Label
		Me.lblClientNo = New System.Windows.Forms.Label
		Me.lblDescription = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "New Account"
		Me.ClientSize = New System.Drawing.Size(600, 268)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmUnknownAccount"
		Me.txtFilename1Hidden.AutoSize = False
		Me.txtFilename1Hidden.Size = New System.Drawing.Size(201, 19)
		Me.txtFilename1Hidden.Location = New System.Drawing.Point(376, 192)
		Me.txtFilename1Hidden.TabIndex = 11
		Me.txtFilename1Hidden.Text = "Filename1Hidden"
		Me.txtFilename1Hidden.AcceptsReturn = True
		Me.txtFilename1Hidden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFilename1Hidden.BackColor = System.Drawing.SystemColors.Window
		Me.txtFilename1Hidden.CausesValidation = True
		Me.txtFilename1Hidden.Enabled = True
		Me.txtFilename1Hidden.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFilename1Hidden.HideSelection = True
		Me.txtFilename1Hidden.ReadOnly = False
		Me.txtFilename1Hidden.Maxlength = 0
		Me.txtFilename1Hidden.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFilename1Hidden.MultiLine = False
		Me.txtFilename1Hidden.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFilename1Hidden.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFilename1Hidden.TabStop = True
		Me.txtFilename1Hidden.Visible = True
		Me.txtFilename1Hidden.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFilename1Hidden.Name = "txtFilename1Hidden"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdCancel.Text = "Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(105, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(224, 200)
		Me.cmdCancel.TabIndex = 10
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.txtFileNameOut1.AutoSize = False
		Me.txtFileNameOut1.Size = New System.Drawing.Size(353, 19)
		Me.txtFileNameOut1.Location = New System.Drawing.Point(104, 144)
		Me.txtFileNameOut1.TabIndex = 9
		Me.txtFileNameOut1.AcceptsReturn = True
		Me.txtFileNameOut1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFileNameOut1.BackColor = System.Drawing.SystemColors.Window
		Me.txtFileNameOut1.CausesValidation = True
		Me.txtFileNameOut1.Enabled = True
		Me.txtFileNameOut1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFileNameOut1.HideSelection = True
		Me.txtFileNameOut1.ReadOnly = False
		Me.txtFileNameOut1.Maxlength = 0
		Me.txtFileNameOut1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFileNameOut1.MultiLine = False
		Me.txtFileNameOut1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFileNameOut1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFileNameOut1.TabStop = True
		Me.txtFileNameOut1.Visible = True
		Me.txtFileNameOut1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFileNameOut1.Name = "txtFileNameOut1"
		Me.txtFormatOut.AutoSize = False
		Me.txtFormatOut.Size = New System.Drawing.Size(105, 19)
		Me.txtFormatOut.Location = New System.Drawing.Point(104, 120)
		Me.txtFormatOut.TabIndex = 8
		Me.txtFormatOut.AcceptsReturn = True
		Me.txtFormatOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFormatOut.BackColor = System.Drawing.SystemColors.Window
		Me.txtFormatOut.CausesValidation = True
		Me.txtFormatOut.Enabled = True
		Me.txtFormatOut.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFormatOut.HideSelection = True
		Me.txtFormatOut.ReadOnly = False
		Me.txtFormatOut.Maxlength = 0
		Me.txtFormatOut.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFormatOut.MultiLine = False
		Me.txtFormatOut.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFormatOut.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFormatOut.TabStop = True
		Me.txtFormatOut.Visible = True
		Me.txtFormatOut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFormatOut.Name = "txtFormatOut"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "OK"
		Me.cmdOK.Size = New System.Drawing.Size(105, 25)
		Me.cmdOK.Location = New System.Drawing.Point(64, 200)
		Me.cmdOK.TabIndex = 7
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.txtAccountNo.AutoSize = False
		Me.txtAccountNo.Size = New System.Drawing.Size(105, 19)
		Me.txtAccountNo.Location = New System.Drawing.Point(104, 96)
		Me.txtAccountNo.TabIndex = 1
		Me.txtAccountNo.AcceptsReturn = True
		Me.txtAccountNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtAccountNo.BackColor = System.Drawing.SystemColors.Window
		Me.txtAccountNo.CausesValidation = True
		Me.txtAccountNo.Enabled = True
		Me.txtAccountNo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAccountNo.HideSelection = True
		Me.txtAccountNo.ReadOnly = False
		Me.txtAccountNo.Maxlength = 0
		Me.txtAccountNo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAccountNo.MultiLine = False
		Me.txtAccountNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAccountNo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAccountNo.TabStop = True
		Me.txtAccountNo.Visible = True
		Me.txtAccountNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtAccountNo.Name = "txtAccountNo"
		Me.txtClientNo.AutoSize = False
		Me.txtClientNo.Size = New System.Drawing.Size(105, 19)
		Me.txtClientNo.Location = New System.Drawing.Point(104, 72)
		Me.txtClientNo.TabIndex = 0
		Me.txtClientNo.AcceptsReturn = True
		Me.txtClientNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtClientNo.BackColor = System.Drawing.SystemColors.Window
		Me.txtClientNo.CausesValidation = True
		Me.txtClientNo.Enabled = True
		Me.txtClientNo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClientNo.HideSelection = True
		Me.txtClientNo.ReadOnly = False
		Me.txtClientNo.Maxlength = 0
		Me.txtClientNo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClientNo.MultiLine = False
		Me.txtClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClientNo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtClientNo.TabStop = True
		Me.txtClientNo.Visible = True
		Me.txtClientNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtClientNo.Name = "txtClientNo"
		Me.lblFilenameOut1.Text = "Filename"
		Me.lblFilenameOut1.Size = New System.Drawing.Size(81, 17)
		Me.lblFilenameOut1.Location = New System.Drawing.Point(16, 144)
		Me.lblFilenameOut1.TabIndex = 6
		Me.lblFilenameOut1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblFilenameOut1.BackColor = System.Drawing.SystemColors.Control
		Me.lblFilenameOut1.Enabled = True
		Me.lblFilenameOut1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblFilenameOut1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblFilenameOut1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblFilenameOut1.UseMnemonic = True
		Me.lblFilenameOut1.Visible = True
		Me.lblFilenameOut1.AutoSize = False
		Me.lblFilenameOut1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblFilenameOut1.Name = "lblFilenameOut1"
		Me.lblFormatOut.Text = "FormatOut"
		Me.lblFormatOut.Size = New System.Drawing.Size(81, 17)
		Me.lblFormatOut.Location = New System.Drawing.Point(16, 120)
		Me.lblFormatOut.TabIndex = 5
		Me.lblFormatOut.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblFormatOut.BackColor = System.Drawing.SystemColors.Control
		Me.lblFormatOut.Enabled = True
		Me.lblFormatOut.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblFormatOut.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblFormatOut.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblFormatOut.UseMnemonic = True
		Me.lblFormatOut.Visible = True
		Me.lblFormatOut.AutoSize = False
		Me.lblFormatOut.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblFormatOut.Name = "lblFormatOut"
		Me.lblAccountNo.Text = "AccountNo"
		Me.lblAccountNo.Size = New System.Drawing.Size(81, 17)
		Me.lblAccountNo.Location = New System.Drawing.Point(16, 96)
		Me.lblAccountNo.TabIndex = 4
		Me.lblAccountNo.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblAccountNo.BackColor = System.Drawing.SystemColors.Control
		Me.lblAccountNo.Enabled = True
		Me.lblAccountNo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblAccountNo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblAccountNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblAccountNo.UseMnemonic = True
		Me.lblAccountNo.Visible = True
		Me.lblAccountNo.AutoSize = False
		Me.lblAccountNo.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblAccountNo.Name = "lblAccountNo"
		Me.lblClientNo.Text = "ClientNo"
		Me.lblClientNo.Size = New System.Drawing.Size(73, 17)
		Me.lblClientNo.Location = New System.Drawing.Point(16, 72)
		Me.lblClientNo.TabIndex = 3
		Me.lblClientNo.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblClientNo.BackColor = System.Drawing.SystemColors.Control
		Me.lblClientNo.Enabled = True
		Me.lblClientNo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblClientNo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblClientNo.UseMnemonic = True
		Me.lblClientNo.Visible = True
		Me.lblClientNo.AutoSize = False
		Me.lblClientNo.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblClientNo.Name = "lblClientNo"
		Me.lblDescription.Size = New System.Drawing.Size(561, 49)
		Me.lblDescription.Location = New System.Drawing.Point(24, 8)
		Me.lblDescription.TabIndex = 2
		Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
		Me.lblDescription.Enabled = True
		Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDescription.UseMnemonic = True
		Me.lblDescription.Visible = True
		Me.lblDescription.AutoSize = False
		Me.lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblDescription.Name = "lblDescription"
		Me.Controls.Add(txtFilename1Hidden)
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(txtFileNameOut1)
		Me.Controls.Add(txtFormatOut)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(txtAccountNo)
		Me.Controls.Add(txtClientNo)
		Me.Controls.Add(lblFilenameOut1)
		Me.Controls.Add(lblFormatOut)
		Me.Controls.Add(lblAccountNo)
		Me.Controls.Add(lblClientNo)
		Me.Controls.Add(lblDescription)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
