Option Strict Off
Option Explicit On

<System.Runtime.InteropServices.ProgId("BabelExport_NET.BabelExport")> Public Class BabelExport

    Dim sFILE_Name, sVB_I_Account As String
	Dim iVB_Format_ID As Short
	Dim sVB_REF_Own As String
	Dim bVB_MultiFiles As Boolean
	Dim oBabelFiles As vbBabel.BabelFiles
	Dim iVB_FilenameInNo As Short 'Which FilenameInNo shall be found oBabel (Remember that in
	' each oFileSetup has 3 fields for export filename).
	Dim sVB_ClientNo As String
	Dim sVB_CompanyNo, sVB_AdditionalID As String
	Dim sVB_Branch, sVB_AccountIDAdd As String
	Dim bFILE_OverWrite, bFILE_WarningIfExists As Boolean
	Dim sFILE_MessageIfExists As String
	Dim sVB_Version As String ' To be used in Telepay, Betfor99
	Dim sVersion, sFilenameImported As String
    Dim eBank As vbBabel.BabelFiles.Bank
	Dim nSequenceNumberStart As Double
	Dim nSequenceNumberEnd As Double
	Dim sSpecial As String
	Dim nNoOfTransactionsExported As Double
    Dim nAmountExported As Double
    Dim sLicensePath As String
    Dim bLicensePathSetFromOutside As Boolean = False

	'Errorhandling
	Private bERR_UseErrorObject As Boolean
	Private lERR_Number As Integer
	Private sERR_Message As String
	
	'Variables used when appending to a file, and to store information. Internal!
	Dim iStartposTest, iLengthTest As Short
	Dim sTestValue As String
	Dim iLineLength As Short
	Dim iStartposNoofRecords, iNoOfRecords, iLengthNoofRecords As Short
	Dim iStartposNoofLinesInEndRecord, iNoofLinesInEndRecord, iLengthNoofLinesInEndRecord As Short
	Dim nTotalAmount As Double
	Dim iStartposTotalAmount, iLengthTotalAmount As Short
	
	Dim bPostAgainstObservationAccount As Boolean
	'Export format type
    Private iExportFormat As vbBabel.BabelFiles.FileType
    ' Decides functionality, which records to export, inside selected format
    Private eTypeofRecords As vbBabel.BabelFiles.TypeofRecords
    Dim bAutoMatch As Boolean
    ' added 10.10.2008 to be able to log in reportclass
    Private bLog As Boolean
    Private oBabelLog As vbLog.vbLogging
    ' XNET 28.05.2013
    Private oExternalUpdateConnection As vbBabel.DAL
    Private oExternalReadConnection As vbBabel.DAL
    Dim bSilent As Boolean = False   ' 05.10.2018

    '********* START PROPERTY SETTINGS ***********************
    ' XNET 28.05.2013
    Public WriteOnly Property ConnectionUpdateToExternalDatabase() As vbBabel.DAL
        Set(ByVal Value As vbBabel.DAL)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If
            oExternalUpdateConnection = Value
        End Set

    End Property
    Public WriteOnly Property ConnectionReadToExternalDatabase() As vbBabel.DAL
        Set(ByVal Value As vbBabel.DAL)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If
            oExternalReadConnection = Value
        End Set

    End Property
    Public WriteOnly Property BabelLog() As vbLog.vbLogging
        Set(ByVal Value As vbLog.vbLogging)
            oBabelLog = Value
        End Set
    End Property
    Public Property LoggingActivated() As Boolean
        Get
            LoggingActivated = bLog
        End Get
        Set(ByVal Value As Boolean)
            bLog = Value
        End Set
    End Property
    Public Property Bank() As vbBabel.BabelFiles.Bank
        Get
            Bank = eBank
        End Get
        Set(ByVal value As vbBabel.BabelFiles.Bank)
            eBank = value
        End Set
    End Property

    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            'Set oBabelFiles = CreateObject ("vbBabel.BabelFiles")
            oBabelFiles = Value
        End Set
    End Property

    Public Property ExportFormat() As vbBabel.BabelFiles.FileType
        Get
            ExportFormat = iExportFormat
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.FileType)

            'Check given format
            If Value <= vbBabel.BabelFiles.FileType.Unknown Or CDbl(Value) > 150 Then
                If CDbl(Value) <= 500 Or CDbl(Value) > 506 Then
                    If CDbl(Value) < 900 Or CDbl(Value) > 1210 Then
                        If CDbl(Value) < 1501 Or CDbl(Value) > 1510 Then
                            If CDbl(Value) < 1800 Or CDbl(Value) > 1830 Then  ' Gjensidige JDE-formats from 1804
                                'Error # 10009
                                Err.Raise(10019, , LRS(10019))
                            End If
                        End If
                    End If
                End If
            End If


            iExportFormat = Value

            '--------------------------------------------
            ' Added 04.07.2008
            ' If format is PAYMULAUTACK (506), then redo format to PAYMUL (502)
            ' All necessary Autack functionality is handled outside of vbBabel, so we can treat it as normal PAYMUL in vbBabel
            If iExportFormat = vbBabel.BabelFiles.FileType.PAYMULAutack Then
                iExportFormat = vbBabel.BabelFiles.FileType.PAYMUL
            End If
            '---------------------------------------------

        End Set
    End Property
    Public WriteOnly Property TypeofRecords() As vbBabel.BabelFiles.TypeofRecords
        Set(ByVal Value As vbBabel.BabelFiles.TypeofRecords)
            ' Decides functionality, which records to export, inside selected format
            'AllRecords = 0
            'OCROnly = 1
            'BankTotals = 10

            eTypeofRecords = Value

        End Set
    End Property
	'Old code
	''Public Property Set BabelFiles(NewVal As Variant)
	''    Set oBabelFiles = NewVal
	''End Property
	''
	''Public Property Let ExportFormat(NewVal As Variant)
	''
	''    'Check given format
	''    If NewVal <= FileType.Unknown Or NewVal > 64 Then
	''        If NewVal <= 500 Or NewVal > 506 Then
	''            If NewVal < 900 Or NewVal > 1104 Then
	''                If NewVal < 1501 Or NewVal > 1510 Then
	''                    'Error # 10009
	''                    Err.Raise 10019, , LRS(10019)
	''                End If
	''            End If
	''        End If
	''    End If
	''
	''
	''    iExportFormat = NewVal
	''
	''    '--------------------------------------------
	''    ' Added 04.07.2008
	''    ' If format is PAYMULAUTACK (506), then redo format to PAYMUL (502)
	''    ' All necessary Autack functionality is handled outside of vbBabel, so we can treat it as normal PAYMUL in vbBabel
	''    If iExportFormat = FileType.PAYMULAutack Then
	''        iExportFormat = FileType.PAYMUL
	''    End If
	''    '---------------------------------------------
	''
	''End Property
	''Public Property Let TypeofRecords(NewVal As Variant)
	''    ' Decides functionality, which records to export, inside selected format
	''    'AllRecords = 0
	''    'OCROnly = 1
	''    'BankTotals = 10
	''
	''    eTypeofRecords = NewVal
	''
	''End Property
	Public WriteOnly Property VB_Version() As String
		Set(ByVal Value As String)
			sVB_Version = Value
		End Set
	End Property
	Public WriteOnly Property Version() As String
		Set(ByVal Value As String)
			sVersion = Value
		End Set
	End Property
	Public WriteOnly Property Special() As String
		Set(ByVal Value As String)
			sSpecial = Value
		End Set
	End Property
    Public WriteOnly Property LicensePath() As String
        Set(ByVal Value As String)
            sLicensePath = Value
            bLicensePathSetFromOutside = True
        End Set
    End Property
    Public WriteOnly Property FILE_Name() As String
        Set(ByVal Value As String)
            sFILE_Name = Value
        End Set
    End Property
	Public WriteOnly Property VB_Format_ID() As Short
		Set(ByVal Value As Short)
			iVB_Format_ID = Value
		End Set
	End Property
	Public WriteOnly Property VB_I_Account() As String
		Set(ByVal Value As String)
			sVB_I_Account = Value
		End Set
	End Property
	Public WriteOnly Property VB_REF_Own() As String
		Set(ByVal Value As String)
            sVB_REF_Own = Value
        End Set
	End Property
	Public WriteOnly Property VB_MultiFiles() As Boolean
		Set(ByVal Value As Boolean)
			bVB_MultiFiles = Value
		End Set
	End Property
	Public WriteOnly Property VB_FilenameInNo() As Short
		Set(ByVal Value As Short)
			iVB_FilenameInNo = Value
		End Set
	End Property
	Public WriteOnly Property VB_ClientNo() As String
		Set(ByVal Value As String)
			sVB_ClientNo = Value
		End Set
	End Property
	Public WriteOnly Property VB_CompanyNo() As String
		Set(ByVal Value As String)
			sVB_CompanyNo = Value
		End Set
	End Property
	Public WriteOnly Property VB_AdditionalID() As String
		Set(ByVal Value As String)
			'Just in use from BabelBank.
			'If the format is OCR, the ID will be use instead of Babelfile.IDENT_Receiver
			' If Direkte Remittering and a file to the bank,
			' the ID will be use instead of Babelfile.IDENT_Sender
			' If Direkte Remittering and a returnfile from the bank,
			' the ID will be use instead of Babelfile.IDENT_Receiver. May not be correct yet
			' If Erhvervsgiro and a file to the bank,
			' the ID will be use instead of Babelfile.IDENT_Sender
			sVB_AdditionalID = Value
		End Set
	End Property
	Public WriteOnly Property VB_Branch() As String
		Set(ByVal Value As String)
			sVB_Branch = Value
		End Set
	End Property
	Public WriteOnly Property VB_AccountIDAdd() As String
		Set(ByVal Value As String)
			sVB_AccountIDAdd = Value
		End Set
	End Property
	Public Property FILE_OverWrite() As Boolean
		Get
			FILE_OverWrite = bFILE_OverWrite
		End Get
		Set(ByVal Value As Boolean)
			bFILE_OverWrite = Value
		End Set
	End Property
	Public Property FILE_WarningIfExists() As Boolean
		Get
			FILE_WarningIfExists = bFILE_WarningIfExists
		End Get
		Set(ByVal Value As Boolean)
			bFILE_WarningIfExists = Value
		End Set
	End Property
	Public ReadOnly Property FILE_MessageIfExists() As String
		Get
			FILE_MessageIfExists = sFILE_MessageIfExists
		End Get
	End Property
    Public WriteOnly Property Bank_Renamed() As vbBabel.BabelFiles.Bank
        Set(ByVal Value As vbBabel.BabelFiles.Bank)
            eBank = Value
        End Set
    End Property
	'Old code
	''Public Property Let Bank(ByVal NewVal As Variant)
	''    eBank = Val(NewVal)
	''End Property
	Public WriteOnly Property ERR_UseErrorObject() As Boolean
		Set(ByVal Value As Boolean)
			bERR_UseErrorObject = Value
		End Set
	End Property
	Public ReadOnly Property ERR_Number() As Integer
		Get
			ERR_Number = lERR_Number
		End Get
	End Property
	Public ReadOnly Property ERR_Message() As String
		Get
			ERR_Message = sERR_Message
		End Get
	End Property
	Public Property VB_PostAgainstObservationAccount() As Boolean
		Get
			VB_PostAgainstObservationAccount = bPostAgainstObservationAccount
		End Get
		Set(ByVal Value As Boolean)
			bPostAgainstObservationAccount = Value
		End Set
    End Property
    Public Property Silent() As Boolean
        Get
            Silent = bSilent
        End Get
        Set(ByVal Value As Boolean)
            bSilent = Value
        End Set
    End Property
	Public Property LastUsedSequenceNo() As Double
		Get
			'The original value of this property is stored in nSequenceNumberStart, but the value to
			'  return is stored in nSequenceNumberEnd and will only be updated when the writing of a file is successful
			LastUsedSequenceNo = nSequenceNumberEnd
		End Get
		Set(ByVal Value As Double)
			nSequenceNumberStart = Value
		End Set
	End Property
	Public ReadOnly Property NoOfTransactionsExported() As Double
		Get
			'The original value of this property is stored in nSequenceNumberStart, but the value to
			'  return is stored in nSequenceNumberEnd and will only be updated when the writing of a file is successful
			NoOfTransactionsExported = nNoOfTransactionsExported
		End Get
	End Property
	Public ReadOnly Property AmountExported() As Double
		Get
			'The original value of this property is stored in nSequenceNumberStart, but the value to
			'  return is stored in nSequenceNumberEnd and will only be updated when the writing of a file is successful
			AmountExported = nAmountExported
		End Get
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'This class contains properties and methods related
	'to exporting files
	
	
	'Litt usikker p� om dette er n�dvendig
	'Dersom det er mange egenskaper som "styrer" hvordan en
	'fil skal ut
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
        sFILE_MessageIfExists = ""
        iVB_FilenameInNo = 0
        bFILE_OverWrite = False
        bFILE_WarningIfExists = True

        ' When vbbabel.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If

        bERR_UseErrorObject = True
        lERR_Number = 0
        sERR_Message = ""
        bPostAgainstObservationAccount = False
        ' Check for licenselimits:
        eTypeofRecords = vbBabel.BabelFiles.TypeofRecords.AllRecords
        nSequenceNumberStart = 0   '-1  XNET 15.05.2013 changed from -1 to 0 to start with 0001 for Visma Telepay
        nSequenceNumberEnd = -1
        nNoOfTransactionsExported = -1
        nAmountExported = -1
        sSpecial = ""
        sLicensePath = ""
        eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Friend Function CheckLicense() As Boolean
        Dim oLicense As vbBabel.License     'XNET 11.07.2013 'License information
        Dim bOK As Boolean

        bOK = True
        oLicense = New License

        '29.10.2015
        If bLicensePathSetFromOutside Then

        End If

        ' 05.10.2018 added bSilent
        If Not oLicense.LicOkToContinue(bSilent, bLog, oBabelLog) Then
            ' Stop
            bOK = False
        End If
        'done automatically oLicense.LicWrite
        'UPGRADE_NOTE: Object oLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'

        oLicense = Nothing

        CheckLicense = bOK
    End Function
    Private Function WriteCargoImportedLines(ByRef oCargo As Cargo, ByRef sFilenameOut As String, ByRef iQualifier As Short) As Object
        Dim oImportedLine As ImportedLine
        Dim oImportedLines As ImportedLines
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream

        oFs = New Scripting.FileSystemObject

        If Not oCargo Is Nothing Then
            If Not oCargo.ImportedLines Is Nothing Then
                ' New 21.09.05
                'If oCargo.ImportedLines.Count > 0 Then
                oImportedLines = oCargo.ImportedLines
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
                For Each oImportedLine In oImportedLines
                    If oImportedLine.Qualifier = iQualifier Then
                        ' Qualifier =1 says Export before collections, 2 = Export after collections
                        oFile.WriteLine(oImportedLine.Text)
                    End If
                Next oImportedLine
                'End If
            End If
        End If

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

    End Function
    Public Function Export() As Boolean
        ' Calls formatspesific functions for export

        Static bLicenseCheckedBefore As Boolean
        Static bLicenseNeverChecked As Boolean = True
        Static bWrittenBefore As Boolean = False   ' 23.11.2020 - Has file been written before?
        Dim bReturnValue As Boolean
        Dim bResult As Boolean
        Dim nInvoices As Double ' No of invoices exported
        Dim oLicense As vbBabel.License
        Dim oBabel As BabelFile
        Dim oExportEDIObject As vbBabel.CEdi2Xml 'Object 'New EDI2XML.CEdi2Xml
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim sAccountNo As String, bAccountFound As Boolean
        Dim iCheckExportFolderAndFile As Integer
        ' XOKNET 20.05.2011
        Dim oInvoice As vbBabel.Invoice
        'XNET 23.09.2011 added next line
        Static bAllExported As Boolean  ' 02.10.2017 changed to static
        Dim sTmp As String
        Dim bTBIOExists As Boolean
        Dim sTmpFilename As String = ""
        Dim bAllPaymentsExported As Boolean

        'New 04.01.2008
        'Maybe we should call the function as below, instead of todays
        'call (from ImportExport after PrepareExport before Export)
        'If so we need the Silent -property in the Export-Class
        'iCheckExportFolderAndFile = CheckFolder(sFILE_Name, ErrorMessageType.Use_MsgBox, True, "Export", True)

        Try

            nAmountExported = -1
            nNoOfTransactionsExported = -1

            ' 01.09.2016 Check licenselimit only once pr BabelBankrun
            If bLicenseNeverChecked Then
                If Not CheckLicense() Then
                    'Err.Raise 10099, , LRS(10099) ' FIX: Change number
                    Export = False
                Else
                    Export = True
                End If
                bLicenseNeverChecked = False
            Else
                Export = True
            End If

            If Export Then

                ' XNET 09.01.2012 - added special split of files for Aker Kvaerner US
                If oBabelFiles Is Nothing Then

                Else
                    If oBabelFiles.Count > 0 Then
                        If oBabelFiles(1).Special = "AKERKVAERNERUS" Then
                            If Not AkerKvaernerUS_PreProcessExport(oBabelFiles, iExportFormat) Then
                                iExportFormat = 0 ' force error
                            End If
                        End If
                    End If
                End If
                '------------------------------------

                ' 30.08.2016
                ' For Teeakay Norway (in Canada), we will need to export one Pain.001 file pr importfile
                ' "Cheat" by setting all other payments than for the actual oBabelFile as exported
                If oBabelFiles(1).Special = "TEEKAY_ISO20022" Then
                    For Each oBabelFile In oBabelFiles
                        If sFILE_Name = oBabelFile.EDI_MessageNo Then
                            ' in prepareexport, we have put the exportfilename into oBabelFile.EDI_MessageNo
                            For Each oBatch In oBabelFile.Batches
                                For Each oPayment In oBatch.Payments
                                    oPayment.Exported = False
                                Next
                            Next
                        Else
                            ' set all other as exported
                            For Each oBatch In oBabelFile.Batches
                                For Each oPayment In oBatch.Payments
                                    oPayment.Exported = True
                                Next
                            Next
                        End If
                    Next
                End If

                ' 17.12.2014
                ' ------------------------------------------------------------------------
                ' REMOVE Omitted payments, e.g. payments marked as removed in Match_Manual
                ' ------------------------------------------------------------------------
                ' This was previously done for SFEand GRIEG only, further down, now for all 
                For Each oBabelFile In oBabelFiles
                    If Not oBabelFile.Batches Is Nothing Then   ' added 04.02.2015
                        For Each oBatch In oBabelFile.Batches
                            For Each oPayment In oBatch.Payments
                                If oPayment.StatusCode = "-1" Then
                                    oPayment.Exported = True
                                End If
                            Next oPayment
                        Next oBatch
                    End If
                Next oBabelFile
                ' ------------------------------------------------------------------------

                '06.07.2015 - Added standard backpayment
                'If Not RunTime() Then
                'PrepareAndWriteBackpaymentFile(oBabelFiles)
                'End If

                ' 29.05.2020
                ' Cheat by changing output format
                If sSpecial = "RAGNSELLS_RENAME" Then
                    iExportFormat = vbBabel.BabelFiles.FileType.GenericText
                End If

                Select Case iExportFormat
                    'Select Case nFormat
                    Case vbBabel.BabelFiles.FileType.Telepay
                        bResult = WriteTelepayFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Telepay2     ', vbbabel.BabelFiles.FileType.TelepayPlus
                        ' 30.12.2008 added Telepay+
                        ' 03.02 removed Telepay+, see further down

                        '21.04.2015 - Added new code to be able to write a file with regular transactions (version2) and TBIO
                        bTBIOExists = False
                        For Each oBabelFile In oBabelFiles
                            For Each oBatch In oBabelFile.Batches
                                If oBatch.ThisIsTBIO Then
                                    bTBIOExists = True
                                    Exit For
                                End If
                            Next oBatch
                            If bTBIOExists Then Exit For
                        Next oBabelFile
                        If bTBIOExists Then
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    If Not oBatch.ThisIsTBIO Then
                                        For Each oPayment In oBatch.Payments
                                            oPayment.Exported = True
                                            For Each oInvoice In oPayment.Invoices
                                                oInvoice.Exported = True
                                            Next oInvoice
                                        Next oPayment
                                    End If
                                Next oBatch
                            Next oBabelFile
                            bResult = WriteTelepayFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, True, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial)
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    If Not oBatch.ThisIsTBIO Then
                                        For Each oPayment In oBatch.Payments
                                            oPayment.Exported = False
                                            For Each oInvoice In oPayment.Invoices
                                                oInvoice.Exported = False
                                            Next oInvoice
                                        Next oPayment
                                    End If
                                Next oBatch
                            Next oBabelFile
                        End If
                        bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                    Case vbBabel.BabelFiles.FileType.TelepayTBIO
                        bResult = WriteTelepayFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, True, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Telepay_BETFOR0099
                        ' Special format to put BETFOR00/99 around each account
                        ' Ref: NorgesGruppen
                        bResult = WriteTelepayFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, True, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.TelepayUNIX
                        bResult = WriteTelepayFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, True, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.TelepayDOIN
                        '31.10.2016 - Added this format
                        If sVersion = "DO" Then 'Always stated first if present
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.PayType = "I" Then
                                            oPayment.Exported = True
                                            For Each oInvoice In oPayment.Invoices
                                                oInvoice.Exported = True
                                            Next oInvoice
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.PayType = "I" Then
                                            oPayment.Exported = False
                                            For Each oInvoice In oPayment.Invoices
                                                oInvoice.Exported = False
                                            Next oInvoice
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                        Else
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                        End If
                    Case vbBabel.BabelFiles.FileType.Telepay_Split_TBMITBRI
                        '31.10.2016 - Added this format
                        If sVersion = "TBRI" Then 'Always stated first if present
                            If EmptyString(sFILE_Name) Then
                                'Do not export, just set Exported to True
                                For Each oBabelFile In oBabelFiles
                                    For Each oBatch In oBabelFile.Batches
                                        For Each oPayment In oBatch.Payments
                                            If oPayment.StatusCode = "01" Then
                                                oPayment.Exported = True
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = True
                                                Next oInvoice
                                            End If
                                        Next oPayment
                                    Next oBatch
                                Next oBabelFile
                                bResult = True
                            Else
                                For Each oBabelFile In oBabelFiles
                                    For Each oBatch In oBabelFile.Batches
                                        For Each oPayment In oBatch.Payments
                                            If oPayment.StatusCode <> "01" Then
                                                If oPayment.VB_ClientNo = sVB_ClientNo Then
                                                    oPayment.Exported = True
                                                    For Each oInvoice In oPayment.Invoices
                                                        oInvoice.Exported = True
                                                    Next oInvoice
                                                End If
                                            End If
                                        Next oPayment
                                    Next oBatch
                                Next oBabelFile
                                bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                                For Each oBabelFile In oBabelFiles
                                    For Each oBatch In oBabelFile.Batches
                                        For Each oPayment In oBatch.Payments
                                            If oPayment.StatusCode <> "01" Then
                                                If oPayment.VB_ClientNo = sVB_ClientNo Then
                                                    oPayment.Exported = False
                                                    For Each oInvoice In oPayment.Invoices
                                                        oInvoice.Exported = False
                                                    Next oInvoice
                                                End If
                                            End If
                                        Next oPayment
                                    Next oBatch
                                Next oBabelFile
                            End If
                        Else
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                        End If
                    Case vbBabel.BabelFiles.FileType.Telepay_Split_TBMITBRI_AND_DOIN
                        '17.12.2018 - Added this format
                        If sVersion = "TBRIDO" Then 'Mottaksretur Innland 
                            'F�rst marker betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.StatusCode <> "01" Or oPayment.PayType = "I" Then
                                            If oPayment.VB_ClientNo = sVB_ClientNo Then
                                                oPayment.Exported = True
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = True
                                                Next oInvoice
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                            'Eksporter mottaksretur innland
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                            'Fjern markering av betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            'I tillegg sett merke p� de som er eksportert
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.VB_ClientNo = sVB_ClientNo Then
                                            If oPayment.StatusCode <> "01" Or oPayment.PayType = "I" Then
                                                oPayment.Exported = False
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = False
                                                Next oInvoice
                                            Else
                                                'Sett spesialmerke p� alle som er eksportert
                                                oPayment.SpecialMark = True
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile

                        ElseIf sVersion = "TBRIIN" Then 'Mottaksretur Utland
                            'F�rst marker betalinger som ikke er mottaksretur og/eller ikke utland som eksportert (hindrer eksport)
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.StatusCode <> "01" Or oPayment.PayType <> "I" Then
                                            If oPayment.VB_ClientNo = sVB_ClientNo Then
                                                oPayment.Exported = True
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = True
                                                Next oInvoice
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                            'Eksporter mottaksretur utland
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                            'Fjern markering av betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            'I tillegg sett merke p� de som er eksportert
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.VB_ClientNo = sVB_ClientNo Then
                                            If oPayment.StatusCode <> "01" Or oPayment.PayType <> "I" Then
                                                oPayment.Exported = False
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = False
                                                Next oInvoice
                                            Else
                                                'Sett spesialmerke p� alle som er eksportert
                                                oPayment.SpecialMark = True
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                        ElseIf sVersion = "TBMIDO" Then 'Avvisningsretur Innland 
                            'F�rst marker betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.StatusCode = "00" Or oPayment.StatusCode = "01" Or oPayment.PayType = "I" Then
                                            If oPayment.VB_ClientNo = sVB_ClientNo Then
                                                oPayment.Exported = True
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = True
                                                Next oInvoice
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                            'Eksporter avvisningsretur innland
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                            'Fjern markering av betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            'I tillegg sett merke p� de som er eksportert
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.VB_ClientNo = sVB_ClientNo Then
                                            If oPayment.StatusCode = "00" Or oPayment.StatusCode = "01" Or oPayment.PayType = "I" Then
                                                oPayment.Exported = False
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = False
                                                Next oInvoice
                                            Else
                                                'Sett spesialmerke p� alle som er eksportert
                                                oPayment.SpecialMark = True
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile

                        ElseIf sVersion = "TBMIIN" Then 'Avvisningsretur Utland 
                            'F�rst marker betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.StatusCode = "00" Or oPayment.StatusCode = "01" Or oPayment.PayType <> "I" Then
                                            If oPayment.VB_ClientNo = sVB_ClientNo Then
                                                oPayment.Exported = True
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = True
                                                Next oInvoice
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                            'Eksporter avvisningsretur innland
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                            'Fjern markering av betalinger som ikke er mottaksretur og/eller ikke innland som eksportert (hindrer eksport)
                            'I tillegg sett merke p� de som er eksportert
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.VB_ClientNo = sVB_ClientNo Then
                                            If oPayment.StatusCode = "00" Or oPayment.StatusCode = "01" Or oPayment.PayType <> "I" Then
                                                oPayment.Exported = False
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.Exported = False
                                                Next oInvoice
                                            Else
                                                'Sett spesialmerke p� alle som er eksportert
                                                oPayment.SpecialMark = True
                                            End If
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                        End If
                        'Check if all payments are exported (SpecialMark = True), do not need to test for clients 
                        bAllPaymentsExported = True
                        For Each oBabelFile In oBabelFiles
                            For Each oBatch In oBabelFile.Batches
                                For Each oPayment In oBatch.Payments
                                    If Not oPayment.SpecialMark Then
                                        bAllPaymentsExported = False
                                        Exit For
                                    End If
                                Next oPayment
                                If bAllPaymentsExported = False Then Exit For
                            Next oBatch
                            If bAllPaymentsExported = False Then Exit For
                        Next oBabelFile

                        'If all payments are exported, mark them as exported
                        If bAllPaymentsExported = True Then
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        'If Not oPayment.SpecialMark Then
                                        oPayment.Exported = True
                                        For Each oInvoice In oPayment.Invoices
                                            oInvoice.Exported = True
                                        Next oInvoice
                                        'oPayment.SpecialMark = False
                                        'End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                        End If

                    Case vbBabel.BabelFiles.FileType.AutoGiro
                        bResult = WriteAutogiroReturnFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.OCR

                        If oBabelFiles(1).Cargo.Special = "NETTLEIE_TILOCR" Then
                            If bAllExported = False Then
                                ' first create a DirRem-file;
                                bResult = WriteDirRemFile(oBabelFiles, False, Trim(oBabelFiles.VB_Profile.Custom1Value), iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, "")
                                bAllExported = True
                            End If

                            ' reset Cancel from the OCR-payments;
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.Cancel = True And oPayment.MATCH_MyField = "OCR" Then
                                            oPayment.Cancel = False
                                            oPayment.Exported = False
                                            oPayment.PayCode = 510
                                        End If
                                    Next
                                Next
                            Next
                        End If

                        ' XNET 16.05.2012
                        If sSpecial = "HB_OCR" Then
                            Do
                                ' for this case, we will export one file pr account
                                ' the call to the export loops until all payments exported
                                bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, False, False, sSpecial)
                                If Not bResult Then
                                    bResult = True
                                    Exit Do
                                End If
                            Loop
                        Else
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, False, False, sSpecial)
                        End If
                        ' New 29.12.05 for Kennelklubben
                        If oBabelFiles(1).Cargo.Special = "AUTOGIRO" Then
                            ' Change filename setting .AG as filesuffix
                            sFILE_Name = Left$(sFILE_Name, InStr(sFILE_Name, ".")) + "AG"
                            bResult = WriteAutogiroEngangsFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo)
                        End If
                    Case vbBabel.BabelFiles.FileType.Dirrem
                        ' New 16.09.05
                        ' Added functionality to read/write lines independent of format, before or after collections
                        ' Check for lines before collections;
                        '''' Must test this WriteCargoImportedLines oBabelFiles(1).Batches(1).Cargo, sFILE_Name, 1  ' Before collections
                        If oBabelFiles.Count > 0 Then
                            WriteCargoImportedLines(oBabelFiles(1).Cargo, sFILE_Name, 1)  ' Before collections
                        End If


                        bResult = WriteDirRemFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, sVB_ClientNo)
                        ' Special for Hafslund -
                        ' New 06.02.06 they may have some few AutogiroFullmakt sletting. Write these in separate routine
                        'If oBabelFiles(1).Cargo.Special = "HAFSLUND" Then
                        'bResult = WriteAutoGiroFullmakt(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo)
                        'End If

                        '' 17.11.2017 - Export OCR-file for Nettleieoppgj�r;
                        'If oBabelFiles(1).Cargo.Special = "NETTLEIE_TILOCR" Then
                        '    ' reset Cancel from the OCR-payments;
                        '    For Each oBabelFile In oBabelFiles
                        '        For Each oBatch In oBabelFile.Batches
                        '            For Each oPayment In oBatch.Payments
                        '                If oPayment.Cancel = True And oPayment.MATCH_MyField = "OCR" Then
                        '                    oPayment.Cancel = False
                        '                    oPayment.Exported = False
                        '                    oPayment.PayCode = 510
                        '                End If
                        '            Next
                        '        Next
                        '    Next
                        '    '' OCR-filename must be setup in Custom1Value (Egendefintert menyvalg under Oppsett)
                        '    '' if ��� as part of filename, replace it
                        '    'sTmp = oBabelFiles.VB_Profile.Custom1Value.trim
                        '    'If sTmp.IndexOf("�") > 0 Then
                        '    '    sTmp = sTmp.Replace("�", sVB_ClientNo)
                        '    '    sTmp = sTmp.Replace("�", "")  ' remove rest of �
                        '    'End If
                        '    '' If file exists, delete it
                        '    'DeleteFilename(sTmp, True, "")
                        '    'bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sTmp, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, False, False, sSpecial)
                        'End If

                    Case vbBabel.BabelFiles.FileType.PostbOCROld
                        bResult = WritePostbOCROldFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.LHLEkstern
                        bResult = WriteLHLEkstern(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.MySoftCRM
                        bResult = WriteMySoftCRM(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        'XNET - 31.20.2011
                    Case vbBabel.BabelFiles.FileType.NRX_Ax
                        'XNET 10.10.2012 - Added next IF - should later be a separate exportformat
                        If sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then
                            bResult = WriteColumbus_Ax(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, bPostAgainstObservationAccount)
                        Else
                            ' XNET 07.09.2012, added for GRIEG
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        'If sSpecial = "GRIEG" Then
                                        '    If oPayment.StatusCode = "-1" Then
                                        '        oPayment.Exported = True
                                        '    End If
                                        'ElseIf sSpecial = "REDCROSS" Then
                                        If sSpecial = "REDCROSS" Then
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Matched Then
                                                    If oInvoice.MATCH_Final Then
                                                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                                            oPayment.Exported = True
                                                            Exit For
                                                        End If
                                                    End If
                                                End If
                                            Next oInvoice
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile

                            bResult = WriteNRX_Ax(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        End If
                    Case vbBabel.BabelFiles.FileType.SRI_AccessReturn
                        bResult = WriteSRI_AccessReturn(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.ErhvervsGiro_Udbetaling
                        bResult = WriteErhvervsgiroUtbet(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, iVB_FilenameInNo, sVB_CompanyNo, False)
                    Case vbBabel.BabelFiles.FileType.Handelsbanken_DK_Domestic
                        bResult = WriteHandelsbanken_DK_Domestic(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.OCR_Bankgirot
                        bResult = WriteOCRBankgirotFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_ClientNo, False, False)
                    Case vbBabel.BabelFiles.FileType.OCR_Bankgirot_Simulated
                        bResult = WriteOCRBankgirotFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_ClientNo, True, False)
                    Case vbBabel.BabelFiles.FileType.OCR_Bankgirot_Simulated_KID
                        bResult = WriteOCRBankgirotFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_ClientNo, True, True)
                    Case vbBabel.BabelFiles.FileType.OCR_Postgirot
                        bResult = WriteOCRPostgirotFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo)
                    Case vbBabel.BabelFiles.FileType.OCR_Simulated
                        'New code 15.12.2006 for BAMA. The exportfile must be sorted per client
                        If sSpecial = "BAMA" Then
                            If oBabelFiles.Count > 0 Then
                                For Each oClient In oBabelFiles.VB_Profile.FileSetups(oBabelFiles(1).VB_FileSetupID).Clients
                                    bAccountFound = False
                                    For Each oaccount In oClient.Accounts
                                        sAccountNo = oaccount.Account
                                        For Each oBabelFile In oBabelFiles
                                            For Each oBatch In oBabelFile.Batches
                                                For Each oPayment In oBatch.Payments
                                                    If oPayment.I_Account = sAccountNo Then
                                                        bAccountFound = True
                                                    End If
                                                    Exit For
                                                Next oPayment
                                                If bAccountFound Then
                                                    Exit For
                                                End If
                                            Next oBatch
                                            If bAccountFound Then
                                                Exit For
                                            End If
                                        Next oBabelFile
                                        If bAccountFound Then
                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then
                                        bResult = WriteOCRFile(oBabelFiles, True, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, Trim$(oClient.ClientNo), True, False, sSpecial)
                                    End If
                                Next oClient
                            End If 'If oBabelFiles.Count > 0 Then
                            ' XNET 11.06.2012 added SFE (Sogn og Fjordane Energi)
                        ElseIf sSpecial = "HAUGALAND KRAFT" Or sSpecial = "SFE" Then
                            'Don't export zero transactions.
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If Not IsOCR(oPayment.PayCode) Then
                                            If oPayment.MON_InvoiceAmount = 0 Then
                                                '21.12.2009 - Commented next IF
                                                'If oPayment.MATCH_Matched <> Matched Then
                                                'If oPayment.Invoices.Count = 1 Then
                                                oPayment.Exported = True
                                                'End If
                                                'End If
                                            End If
                                        End If
                                        'Exit For KJELL _ HVORFOR EXIT FOR HER ?
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                            'Added 01.07.2020
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, sSpecial, False)
                            'XOKNET - 06.12.2010 - Added next ElseIf
                        ElseIf sSpecial = "CONECTO_AF" Then
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, sSpecial, False)
                            'XNET - 13.12.2011 - Added next ElseIf
                        ElseIf sSpecial = "CONECTO_HAMAR" Then
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, sSpecial, False)
                            'XNET - 04.12.2018 - Added next ElseIf
                        ElseIf sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Then
                            '31.08.2022 - Added "MODHI_NORGE"
                            '27.03.2019 - Changed from 
                            'bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, True, sSpecial, False) '19.03.2019 - Changed 3 last parameter from False to True, now exporting OCR as well
                            'to
                            bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial, False)

                        ElseIf sSpecial = "SERGELNORGE" Then
                            '01.07.2020 - Added this ElseIf 
                            bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial, False)
                        ElseIf sSpecial = "AKTIV_KAPITAL" Or sSpecial = "LOWELL_NO" Then
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, sSpecial, False)
                        ElseIf sSpecial = "NORSTAT" Then
                            'bOCRTransactions = True
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, True, sSpecial)
                            'XNET - 25.04.2012 - Added next ElseIf
                        ElseIf sSpecial = "CONECTO_FAS" Then
                            bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial, False, bPostAgainstObservationAccount)
                        ElseIf sSpecial = "FJORDKRAFT" Then
                            bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)
                        ElseIf sSpecial = "BKK" Then
                            If bPostAgainstObservationAccount Then
                                bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, sSpecial, False)
                            Else
                                bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)
                            End If
                            'XokNET 10.06.2014 - Added changes for "CONECTO" to split between Procasso and Predator
                        ElseIf sSpecial = "CONECTO" Then
                            ' Pick filename for Predator-OCR from oBabelFiles.VB_Profile.Custom1Value
                            bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, Trim$(oBabelFiles.VB_Profile.Custom1Value), iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, "TILPREDATOR")
                            ' Then add a new record for each batch, totalling up the Predator-exported OCR-records.
                            ' These totals must be exported to the Procasso OCR-file, as totals with a special kid
                            bResult = ConectoAddTotalsToProcassoFile(oBabelFiles)

                            ' and then the Procasso-file
                            bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)

                            ' And create a Telepayfile with the Predator-payments;
                            bResult = ConectoPrepareTelepayToPredatorFile(oBabelFiles)
                            ' Pick filename for Predator-OCR from oBabelFiles.VB_Profile.Custom2Value
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, oBabelFiles.VB_Profile.Custom2Value, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                        Else
                            If sSpecial = "PLANNORGE" Then
                                ' update UniqueID with full KID for aKonto-postings
                                ' when aKonto, CustomerNo is set as Match_ID, but we need to prefix with "1" and add cdv-digit
                                For Each oBabelFile In oBabelFiles
                                    For Each oBatch In oBabelFile.Batches
                                        For Each oPayment In oBatch.Payments
                                            For Each oInvoice In oPayment.Invoices
                                                If oPayment.MATCH_Matched And oInvoice.MATCH_Final Then
                                                    If oInvoice.MATCH_ID.Length < 9 Then
                                                        ' this one is an aKonto, with CustomerID in MatchID
                                                        ' pad with leading 0's
                                                        oInvoice.MATCH_ID = PadLeft(oInvoice.MATCH_ID, 7, "0")
                                                        ' add leading "1"
                                                        oInvoice.MATCH_ID = "1" & oInvoice.MATCH_ID
                                                        ' add controdigit
                                                        oInvoice.MATCH_ID = oInvoice.MATCH_ID & AddCDV10digit(oInvoice.MATCH_ID)
                                                    End If
                                                End If
                                            Next
                                        Next
                                    Next
                                Next
                            End If
                            'Normal situation
                            bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, sSpecial)
                        End If
                    Case vbBabel.BabelFiles.FileType.OCR_Simulated_KID
                        bResult = WriteOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, True, sSpecial)

                        'Case vbBabel.BabelFiles.FileType.Pain001_SplitFIDKSE_AndOthers
                    Case vbBabel.BabelFiles.FileType.Pain001

                        '============================================================================
                        ' To export on XML ISO 20022 for DNB, a special licensecode DNBX is required
                        '============================================================================
                        If IsThisDNBXMLIntegrator() Then

                            ' 04.03.2016
                            ' added possibility to export DNB Integrator files as Pain.001
                            ' This format will up to September 2016 split files in (SE,DK,FI) and others
                            oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                            oExportEDIObject.BabelFiles = oBabelFiles
                            oExportEDIObject.FilesetupOut = iVB_Format_ID
                            oExportEDIObject.CompanyNo = sVB_CompanyNo
                            oExportEDIObject.Country = "XX"
                            oExportEDIObject.OutputPath = sFILE_Name
                            'oExportEDIObject.ExportFormat = GetEnumFileType(iExportFormat)
                            oExportEDIObject.BankByCode = eBank
                            oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                            oExportEDIObject.EDIVersion = sVersion
                            oExportEDIObject.VB_Version = sVB_Version
                            bResult = oExportEDIObject.Export

                        End If  'If IsThisDNBXMLIntegrator() Then

                        If sSpecial = "ASSA_ABLOY" Then
                            bResult = WriteVingcard(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, bFILE_OverWrite, bFILE_WarningIfExists, sVB_AdditionalID, sSpecial, eBank, iExportFormat, True)
                        Else
                            bResult = WriteISO_20022(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_CompanyNo, sVB_Branch, sVB_ClientNo, sSpecial, eBank, iExportFormat)
                        End If

                        ' 26.03.2019 -
                        ' Telenor Global Services sends payments from SG and US, but does not receive returnfiles from Nordea.
                        ' Because of this, we will need to produce a fake Telepay avregningsretur
                        If InStr(oBabelFiles(1).Special, "TELENORSG") > 0 Or InStr(oBabelFiles(1).Special, "TELENORUS") > 0 Then
                            ' Remove exported, and set status = 02
                            For Each oBabelFile In oBabelFiles
                                oBabelFile.StatusCode = "02"
                                For Each oBatch In oBabelFile.Batches
                                    oBatch.StatusCode = "02"
                                    For Each oPayment In oBatch.Payments
                                        ' Redo to international
                                        oPayment.PayType = "I"
                                        oPayment.Exported = False
                                        oPayment.StatusCode = "02"
                                        oPayment.DATE_Value = oPayment.DATE_Payment
                                        oPayment.MON_AccountAmount = oPayment.MON_TransferredAmount ' XNET added 24.08.2011 for returnfiles
                                        For Each oInvoice In oPayment.Invoices
                                            oInvoice.StatusCode = "02"
                                        Next oInvoice
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile

                            ' export TBIU return
                            sFILE_Name = Left$(sFILE_Name, InStrRev(sFILE_Name, ".")) + "dat"
                            sFILE_Name = Replace(Replace(sFILE_Name, "Pain", "TPAI"), "ToBank", "TPAI") ' kan ha b�de Pain og ToBank som eksportfilnavn (tror de har Pain i produklsjon)
                            bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, oExternalUpdateConnection, oExternalReadConnection)
                        End If

                    Case vbBabel.BabelFiles.FileType.Camt053
                        bResult = WriteISO_20022(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_CompanyNo, sVB_Branch, sVB_ClientNo, sSpecial, eBank, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.Camt054
                        bResult = WriteISO_20022(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_CompanyNo, sVB_Branch, sVB_ClientNo, sSpecial, eBank, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.Pain002
                        bResult = WriteISO_20022(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_CompanyNo, sVB_Branch, sVB_ClientNo, sSpecial, eBank, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.WinOrg
                        bResult = WriteWinOrg(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.HDP_Citibank
                        bResult = WriteHDP_CitibankFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo)
                    Case vbBabel.BabelFiles.FileType.Czech_payments_Citibank
                        bResult = WriteCzech_Payments_CitibankFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo)
                    Case vbBabel.BabelFiles.FileType.AutogiroEngangs
                        bResult = WriteAutogiroEngangsFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.NordeaUnitel
                        bResult = WriteNordeaUnitel(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.ShipNet
                        bResult = WriteShipNetFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.MaritechInnbetaling
                        'XNET - 30.05.2013 - Added new parameter
                        bResult = WriteMaritechFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Navision_for_SI_Data  ' Pilaro
                        bResult = WriteNavision_for_SI_Data(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, bPostAgainstObservationAccount, sSpecial)
                    Case vbBabel.BabelFiles.FileType.OCR_For_BKK
                        bResult = WriteOCRFile_Special(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, True, False, "BKK")
                    Case vbBabel.BabelFiles.FileType.BabelBank_Innbetaling
                        'If sSpecial = "SALMAR" Then
                        'TreatSalmarTerminkontrakt(oBabelFiles)
                        'End If
                        'XNET - 17.01.2013 - Added sDivision in the function
                        '11.05.2015 - Added , bPostAgainstObservationAccount
                        ' 22.11.2018 - 
                        bResult = WriteBabelBank_InnbetalingFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial, sVB_Branch, bPostAgainstObservationAccount)
                    Case vbBabel.BabelFiles.FileType.BabelBank_Innbetaling_ver2
                        bResult = WriteBabelBank_InnbetalingFile_ver2(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial, sVB_Branch, bPostAgainstObservationAccount)
                    Case vbBabel.BabelFiles.FileType.QXLOCR
                        bResult = WriteQXLOCR(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.PBS
                        bResult = WritePBSFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.DanskeBankTeleService
                        bResult = WriteDanskeBankFI(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.pgs
                        bResult = WritePGSReturn(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Visma_Global
                        bResult = WriteVisma_Global(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iVB_FilenameInNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Creno_XML
                        bResult = WriteCreno_XML(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.INS2000
                        bResult = WriteINS2000(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, False)
                    Case vbBabel.BabelFiles.FileType.NHC_Philippines
                        '09.11.2016 - Added this Case
                        bResult = WriteINS2000(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, True)
                        'XNET - 30.01.2012 - Added next Case
                    Case vbBabel.BabelFiles.FileType.eGlobal
                        bResult = WriteeGlobal(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.PINK_LAAN
                        bResult = WritePink(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords, sSpecial)
                        'Case vbbabel.BabelFiles.FileType.Agresso_Lindorff
                        'bResult = WriteAgressoLindorf(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords)
                    Case vbBabel.BabelFiles.FileType.Bellin_TRDB_RAXCM
                        bResult = WriteBellinTreasury(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID)
                    Case vbBabel.BabelFiles.FileType.NextFinancial_Incoming
                        bResult = WriteNextFinancial(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.DanskeBankLokaleDanske
                        bResult = WriteDanskeBankCMBOCMUO(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, eBank, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Leverantorsbetalningar
                        ' XOKNET 25.11.2010 special treatment for XtraPersonell
                        If sSpecial = "XTRAPERSONELL_SE" Then
                            PrepareXtraPersonellSweden(oBabelFiles)
                        End If
                        bResult = WriteLeverantorsBetalningar(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, vbBabel.BabelFiles.FileType.Leverantorsbetalningar)
                    Case vbBabel.BabelFiles.FileType.Bankdata_DK
                        bResult = WriteBankdata_DK(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.SEBScreen_International
                        bResult = WriteSEBScreen_International(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.SEBScreen_Denmark
                        bResult = WriteSEBScreen_DK(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.SEBScreen_Germany
                        bResult = WriteSEBScreen_Germany(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.SEB_Screen
                        bResult = WriteSEBScreenFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, nSequenceNumberStart, nSequenceNumberEnd, bFILE_OverWrite, bFILE_WarningIfExists)
                    Case vbBabel.BabelFiles.FileType.Patentstyret_Order
                        bResult = WritePatentstyret_Order(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, bPostAgainstObservationAccount)
                    Case vbBabel.BabelFiles.FileType.ApticIPCXML
                        bResult = WriteApticIPCXML(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, bPostAgainstObservationAccount, sSpecial, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.NavigaIncoming
                        ' For NHST - her kommer vi ved eksport av (kun) OCR-betalinger (egen profil for dette)
                        '            �vrige innbetalinger til Naviga eksporteres i caset under
                        bResult = Write_NavigaIncoming(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, bPostAgainstObservationAccount, sSpecial, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Visma_Business
                        bResult = True
                        ' For NHST, we will need to export both to Infosoft, as OCR-file, and Visma Business.
                        If sSpecial = "NHST_INNBETALINGER" Then
                            ' 20.04.2017
                            ' For NHSTs Global companies (hereafter named GLOBAL), we will need to export 4 files, 3 to Infosoft, 1 to Visma
                            ' This is marked at invoicelevel, as INFOSOFT1, INFOSOFT2, INFOSOFT3, VISMA in oInvoice.MyField2

                            ' In WriteOCRFile, we are testing to export only payments where BBRET_MyField = "INFOSOFT"
                            ' For other than GLOBAL, the exportfilename in BabelBank is set with TARGET as part of filename
                            ' (from 20.04.2017) for GLOBAL, the exportfilename is ALSO (together with TARGET) set with GLOBAL as part of the filename
                            ' GLOBAL = 1, 2 or 3
                            ' move Match_ID to Unique_ID, and add controldigit
                            For Each oBabel In oBabelFiles
                                For Each oBatch In oBabel.Batches
                                    For Each oPayment In oBatch.Payments
                                        ' 11.02.2015
                                        ' We are running several runs; one pr client
                                        ' To not add CDV several times, have to use next If before adding CDV
                                        ' 20.01.2020 - removed next IF
                                        'If oPayment.VB_ClientNo = sVB_ClientNo Then
                                        For Each oInvoice In oPayment.Invoices
                                            'If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" Then
                                            ' 20.04.2017 changed to
                                            If InStr(xDelim(oInvoice.MyField.ToUpper, "|", 1), "INFOSOFT") > 0 Then
                                                ' added next If 20.01.2020
                                                If Len(oInvoice.MATCH_ID) <> 12 Then
                                                    If Not EmptyString(oInvoice.MATCH_ID) Then
                                                        ' 19.01.2015 - changed to update Match_ID, as this one is used in export !!!
                                                        oInvoice.MATCH_ID = oInvoice.MATCH_ID & AddCDV10digit(oInvoice.MATCH_ID)
                                                        oInvoice.Unique_Id = oInvoice.MATCH_ID
                                                    End If
                                                End If

                                                ' 02.03.2015 - No creditamounts allowed in Infosoftfile;
                                                If oInvoice.MON_InvoiceAmount < 0 Then
                                                    ' warn, and cancel further processing;
                                                    sTmp = "Dette finnes kreditnotaer i filen til Infosoft. Dette er ikke tillatt." & vbCrLf
                                                    sTmp = sTmp & vbCrLf & "Klientnr: " & oPayment.VB_ClientNo
                                                    sTmp = sTmp & vbCrLf & "NHST konto: " & oPayment.I_Account & vbCrLf
                                                    sTmp = sTmp & vbCrLf & "Betaler: " & oPayment.E_Name
                                                    sTmp = sTmp & vbCrLf & "Bel�p: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,#00.00")

                                                    If oInvoice.Freetexts.Count > 0 Then
                                                        sTmp = sTmp & vbCrLf & "Tekst: " & Left(oInvoice.Freetexts(1).Text, 40)
                                                    End If
                                                    If oInvoice.Freetexts.Count > 1 Then
                                                        sTmp = sTmp & vbCrLf & "Tekst: " & Left(oInvoice.Freetexts(2).Text, 40)
                                                    End If
                                                    If oInvoice.Freetexts.Count > 2 Then
                                                        sTmp = sTmp & vbCrLf & "Tekst: " & Left(oInvoice.Freetexts(3).Text, 40)
                                                    End If

                                                    MsgBox(sTmp, MsgBoxStyle.Critical, "Ikke tillatt med kreditnota til Infosoft")
                                                    bResult = False
                                                    Exit For
                                                End If
                                            End If
                                        Next
                                        If bResult = False Then
                                            Exit For  ' Kreditnota mot Infosoft
                                        End If
                                        'End If
                                    Next
                                    If bResult = False Then
                                        Exit For  ' Kreditnota mot Infosoft
                                    End If
                                Next
                                If bResult = False Then
                                    Exit For  ' Kreditnota mot Infosoft
                                End If
                            Next

                            'If bResult And bAllExported = False Then
                            ' 27.11.2017 - to also have F14010 in a separate file:
                            If bResult Then

                                'If klient=14003 (TW), 14006 (Upstream), 14015 (Intrafish), 14032 (Recharge), skal vi ha NHST Asia-fil
                                'If sVB_ClientNo = "F14003" Or sVB_ClientNo = "F14006" Or sVB_ClientNo = "F14010" Or sVB_ClientNo = "F14015" Or sVB_ClientNo = "F14032" Then

                                ' 28.09.2017 - Export ONE Visma file only;
                                ' ----------------------------------------
                                ' remove timestamp part of filename, because we need to owerwrite, and have same filename;
                                ' filename MUST be on this form:
                                ' ������_%YMD_hms%_TARGET.txt (5 digits for client, 6 for date, 6 for time)

                                Dim iTmp As Integer
                                iTmp = sFILE_Name.IndexOf("_") + 9  ' startpost of timestamp
                                sTmpFilename = sFILE_Name.Substring(0, iTmp) + sFILE_Name.Substring(iTmp + 7)

                                sTmpFilename = sTmpFilename.Replace("F14003_", "F14063_")
                                sTmpFilename = sTmpFilename.Replace("F14006_", "F14063_")
                                'sTmpFilename = sTmpFilename.Replace("F14010_", "F14063_")
                                sTmpFilename = sTmpFilename.Replace("F14015_", "F14063_")
                                sTmpFilename = sTmpFilename.Replace("F14032_", "F14063_")
                                ' and then change filename for the "real" Vismaexport for NHST
                                sTmpFilename = sTmpFilename.Replace("TARGET", "Visma_")

                                'End If
                            End If

                            ' Prepare export to Visma of non-Infosoft invoices
                            For Each oBabel In oBabelFiles
                                For Each oBatch In oBabel.Batches
                                    For Each oPayment In oBatch.Payments
                                        For Each oInvoice In oPayment.Invoices
                                            If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" Then
                                                oInvoice.Exported = True
                                                ' added 28.06.2022, for Naviga export
                                            ElseIf xDelim(oInvoice.MyField.ToUpper, "|", 1) = "NAVIGA" Then
                                                oInvoice.Exported = True
                                            Else
                                                If oPayment.StatusCode <> "-1" Then ' 20.10.2017 removed next And - And oPayment.VB_ClientNo = sVB_ClientNo Then  ' 19.01.2015 added this if
                                                    ' We can have Omitted payments, which we NOT should set as exported = false
                                                    oInvoice.Exported = False
                                                    oPayment.Exported = False
                                                End If

                                            End If

                                        Next
                                    Next
                                Next
                            Next

                            If bResult Then
                                If sVB_ClientNo = "F14003" Or sVB_ClientNo = "F14006" Or sVB_ClientNo = "F14015" Or sVB_ClientNo = "F14032" Then
                                    If bAllExported = False Then
                                        bAllExported = True
                                        bResult = WriteVisma_Business(oBabelFiles, False, sTmpFilename, iVB_Format_ID, sVB_I_Account, "")

                                        If bResult Then
                                            NHST_Backup(sTmpFilename, oBabelFiles.VB_Profile.BackupPath)
                                        End If

                                        'End If
                                        '   pre 02.10.2017 bResult = WriteVisma_Business(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)

                                        ' For NHST, we need to export an extra Visma-file, for NHST Asia only, for postings between real bankacount for NHST Asia and
                                        ' mellomregningskonti for TradeWinds, Upstream, Recharge, Intrafish
                                        ' Find filename from Setupmenu
                                        sTmpFilename = ReplaceDateTimeStamp(oBabelFiles.VB_Profile.Custom1Value)
                                        'bResult = WriteVisma_Business_NHSTAsia(oBabelFiles, bVB_MultiFiles, sTmpFilename, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                                        bResult = WriteVisma_Business_NHSTAsia(oBabelFiles, False, sTmpFilename, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)

                                        ' 03.10.2019 - Must reset oInvoice.Exported to false for all Infosoft invoices;
                                        ' Prepare export to Visma of non-Infosoft invoices
                                        For Each oBabel In oBabelFiles
                                            For Each oBatch In oBabel.Batches
                                                For Each oPayment In oBatch.Payments
                                                    For Each oInvoice In oPayment.Invoices
														If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" Then
															oInvoice.Exported = False
															oPayment.Exported = False
														End If
														' 27.09.2022 Also exclude Naviga payments
														If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "NAVIGA" Then
															oInvoice.Exported = False
															oPayment.Exported = False
														End If

													Next
												Next
                                            Next
                                        Next
                                    End If
                                Else
                                    ' alle andre NHST Visma klienter;
                                    bResult = WriteVisma_Business(oBabelFiles, True, sTmpFilename, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                                End If

                                ' added 18.02.2015 - take backup of files exported for NHST
                                If sTmpFilename <> "" Then
                                    NHST_Backup(sTmpFilename, oBabelFiles.VB_Profile.BackupPath)
                                End If
                            End If



							' 03.11.2017 - moved next section here (Next 2 If's)
							' 27.09.2022 - do not export to Infosoft for F14080
							If sVB_ClientNo <> "F14080" Then
								sTmpFilename = Replace(sFILE_Name, "TARGET", "Infosoft_")
								If bResult Then
									bResult = WriteBabelBank_InnbetalingFile(oBabelFiles, bVB_MultiFiles, sTmpFilename, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial, "", bPostAgainstObservationAccount)
								End If

								If bResult Then
									' added 18.02.2015 - take backup of files exported for NHST
									NHST_Backup(sTmpFilename, oBabelFiles.VB_Profile.BackupPath)
								End If
							End If

							' 28.06.2022
							' for 14080, NHST Media Group AS, export to Naviga
							If sVB_ClientNo = "F14080" Then
									sTmpFilename = Replace(sFILE_Name, "TARGET", "Naviga_")
									' Must reset oInvoice.Exported to false for all Naviga invoices;
									For Each oBabel In oBabelFiles
										For Each oBatch In oBabel.Batches
											For Each oPayment In oBatch.Payments
												For Each oInvoice In oPayment.Invoices
													If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "NAVIGA" Then
														oInvoice.Exported = False
														oPayment.Exported = False
													End If
												Next
											Next
										Next
									Next
								bResult = Write_NavigaIncoming(oBabelFiles, False, sTmpFilename, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial, "")
								If bResult Then
									' added 18.02.2015 - take backup of files exported for NHST
									NHST_Backup(sTmpFilename, oBabelFiles.VB_Profile.BackupPath)
								End If
							End If

							Else
								' 30.10.2017 NOT NHST, ordinar Visma Business
								bResult = WriteVisma_Business(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                        End If  ' NHST_INNBETALINGER
                    Case vbBabel.BabelFiles.FileType.Bankdata_DK_45
                        bResult = WriteBankdata_DK_45(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Axapta_ColumbusIT
                        bResult = WriteAxapta_ColumbusITandHands(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, "ColumbusIT")
                    Case vbBabel.BabelFiles.FileType.Navision_KonicaMinolta
                        bResult = WriteGenericBabelBank_InnbetalingFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.TVaksjonen
                        bResult = WriteTVAksjonen(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Kredinor_K90
                        bResult = WriteKredinor_K90(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords)
                    Case vbBabel.BabelFiles.FileType.BOLS
                        bResult = WriteBolsFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID)
                    Case vbBabel.BabelFiles.FileType.SGFINANSBOLS
                        bResult = WriteSGFINANS_BOLSFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID)
                    Case vbBabel.BabelFiles.FileType.AquariusIncoming
                        bResult = WriteAquariusIncomingPayments(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords, nNoOfTransactionsExported, nAmountExported, bPostAgainstObservationAccount)
                    Case vbBabel.BabelFiles.FileType.AquariusIncomingOCR
                        bResult = WriteAquariusIncomingOCR(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, nNoOfTransactionsExported, nAmountExported)
                    Case vbBabel.BabelFiles.FileType.HB_GlobalOnline_MT101
                        bResult = WriteHBGlobalOnline_MT101File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, nSequenceNumberStart, nSequenceNumberEnd)
                    Case vbBabel.BabelFiles.FileType.MT101
                        bResult = WriteMT101File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, eBank, sSpecial) ', nSequenceNumberStart, nSequenceNumberEnd)
                    Case vbBabel.BabelFiles.FileType.Nacha
                        'test
                        bResult = WriteNachaUS(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo)
                    Case vbBabel.BabelFiles.FileType.Handelsbanken_Bankavstemming
                        bResult = WriteHandelsbanken_Bankavst(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo)
                        'XNET - 11.10.2011 - Added next case
                    Case vbBabel.BabelFiles.FileType.Excel
                        bResult = WriteExcelCSV_SDV(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial)

                        ' BAILockbox can be exported to three different files;
                        ' 1. Original Lockbox payments
                        ' 2. Original ACH payments
                        ' 3. Original MT940 payments
                    Case vbBabel.BabelFiles.FileType.BAILockBox
                        bResult = WriteBAILockBoxDnBNOR(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo, vbBabel.BabelFiles.FileType.BAILockBox)
                    Case vbBabel.BabelFiles.FileType.BAILockBoxACH
                        bResult = WriteBAILockBoxDnBNOR(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo, vbBabel.BabelFiles.FileType.BAILockBoxACH)
                    Case vbBabel.BabelFiles.FileType.BAILockBoxMT940
                        bResult = WriteBAILockBoxDnBNOR(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo, vbBabel.BabelFiles.FileType.BAILockBoxMT940)

                    Case vbBabel.BabelFiles.FileType.SGClientCSVNotOCR
                        bResult = WriteSGClientCSV(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, vbBabel.BabelFiles.TypeofRecords.NotOcr)
                    Case vbBabel.BabelFiles.FileType.SGClientCSVOCROnly
                        bResult = WriteSGClientCSV(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, vbBabel.BabelFiles.TypeofRecords.OCRonly)
                    Case vbBabel.BabelFiles.FileType.SI_SIRI
                        bResult = WriteSI_SIRI(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords)
                    Case vbBabel.BabelFiles.FileType.Politiets_CSV
                        bResult = WritePolitiets_CSV(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.SGFinans_Faktura
                        bResult = WriteSGFinans_Fakturafil(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.SGFinans_Kunde
                        bResult = WriteSGFinans_Kundefil(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.AIG_Account
                        bResult = WriteAIG_Kontoinfo(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID)
                    Case vbBabel.BabelFiles.FileType.ProfundoInnbet
                        bResult = WriteProfundo(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.WritePolitietsFellesForbund_Visma_Global
                        bResult = WritePolitietsFellesForbund_Global(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Skagen_fondene
                        bResult = WriteSkagen_fondene(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Eurosoft
                        bResult = WriteEurosoft(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords)
                    Case vbBabel.BabelFiles.FileType.Bluewater_Retur
                        bResult = WriteBluewaterRetur(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Silver_XML
                        bResult = WriteSilver_XML(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.NorgesForskningsrad
                        bResult = WriteNorgesForskningsrad(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords)
                    Case vbBabel.BabelFiles.FileType.AgressoGL
                        bResult = WriteAgressoGL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, eTypeofRecords, sSpecial, bPostAgainstObservationAccount)
                    Case vbBabel.BabelFiles.FileType.NorgesForskningsrad_OCR
                        bResult = True
                        'Don't do a thing - It is already exported during WriteNorgesForskningsrad
                    Case vbBabel.BabelFiles.FileType.Gjensidige_S2000_DEBMUL
                        bResult = WriteGjensidige_S2000_DEBMUL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Gjensidige_S2000_BANSTA
                        bResult = WriteGjensidige_S2000_BANSTA(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Gjensidige_S2000_FINSTA
                        bResult = WriteGjensidige_S2000_FINSTA(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.SG_SaldoFil_Kunde, vbBabel.BabelFiles.FileType.SG_SaldoFil_Aq_CSV, vbBabel.BabelFiles.FileType.SG_SaldoFil_Aq_Fixed, vbBabel.BabelFiles.FileType.SG_SaldoFil_Aq
                        bResult = WriteSGFinans_SaldoKunde(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iExportFormat, sSpecial)
                    Case vbBabel.BabelFiles.FileType.SG_FakturaHistorikkFil_Kunde, vbBabel.BabelFiles.FileType.SG_FakturaFil_Aq_CSV, vbBabel.BabelFiles.FileType.SG_FakturaHistorikFil_Aq
                        bResult = WriteSGFinans_FakturaHistorikkKunde(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.OneTimeFormat
                        'Set exportformat to f.x. BANSTA (503), set a break here and enter the routine manually
                        'No OneTimeFormat exists in the setup
                        bResult = WriteOneTimeFormat(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.Gjensidige_S2000_CREMUL
                        bResult = WriteGjensidige_S2000_CREMUL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sVB_REF_Own, bLog, oBabelLog, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Gjensidige_S2000_CONTRL
                        bResult = WriteGjensidige_S2000_CONTRL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.DnBNORFinans_CREMUL
                        bResult = WriteDnBNORFinans_CREMUL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sVB_REF_Own, bLog, oBabelLog, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Database_Innbetaling
                        bResult = WriteDatabase_Innbetaling(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, vbBabel.BabelFiles.TypeofRecords.OCRonly)
                    Case vbBabel.BabelFiles.FileType.Nordea_Liv_bankavst
                        bResult = WriteNordea_Liv_bankavst(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Axapta_Hands
                        bResult = WriteAxapta_ColumbusITandHands(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, "Hands")
                    Case vbBabel.BabelFiles.FileType.Axapta_Advania
                        bResult = WriteAxapta_ColumbusITandHands(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, "Advania")
                    Case vbBabel.BabelFiles.FileType.Alliance_Apotek
                        bResult = WriteAlliance_Apotek(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, "Hands")
                    Case vbBabel.BabelFiles.FileType.StandardSeparatedExport
                        bResult = WriteStandardSeparated(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Formula_B9000
                        bResult = WriteFormula_B900BBabelBank_InnbetalingFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iExportFormat, sSpecial)
                    Case vbBabel.BabelFiles.FileType.OCR_Santander
                        bResult = WriteSantanderOCRFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.OCR_Conecto_AF
                        bResult = WriteOCRFileAllTransactions(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.XML_Conecto
                        bResult = WriteXML_Conecto(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Vingcard_Remittering
                        bResult = WriteVingcard(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, bFILE_OverWrite, bFILE_WarningIfExists, sVB_AdditionalID, sSpecial, eBank, iExportFormat, False)
                    Case vbBabel.BabelFiles.FileType.Gjensidige_Utbytte
                        bResult = WriteGjensidigeUtbytteReturn(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.LindorffInkasso_TNT
                        bResult = WriteLindorffInkasso_TNT(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.OdinFakeExport
                        bResult = WriteOdinFakeExport(oBabelFiles)
                    Case vbBabel.BabelFiles.FileType.Autogiro_Bankgirot
                        ' No Exportformat created yet.
                        ' NOTE: In the IMPORT of this format, ReadAutogiro_Bankgirot, we have a special treatment for payments
                        ' "rejected", with codes in pos 80
                        ' 1 T�kning saknas
                        ' 2 AG-koppling saknas
                        ' 9 F�rnyad t�kning
                        ' These kind of payments are imported, but given an MON_InvoiceAmount = 0 !!!!!
                        ' Take this into consideration when creating an exportformat for Autogiro Bankgiro
                        ' XOKNET 04.11.2010
                    Case vbBabel.BabelFiles.FileType.GiroDirekt_Plusgiro
                        bResult = WriteGiroDirektFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, sVB_CompanyNo)
                        ' XOKNET 09.11.2010
                    Case vbBabel.BabelFiles.FileType.AccountControl
                        bResult = WriteAccountControl(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.InfoEasy
                        bResult = WriteInfoEasy_HBFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial)
                        ' XOKNET 09.11.2010
                    Case vbBabel.BabelFiles.FileType.DTAZV
                        bResult = Write_DTAZVFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_ClientNo, sSpecial, sVB_CompanyNo)
                        'XOKNET, 28.12.2010
                    Case vbBabel.BabelFiles.FileType.Exide_XLS
                        bResult = Write_ExideXLSFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        ' XOKNET 08.04.2011
                    Case vbBabel.BabelFiles.FileType.Excel_Export
                        ' Common export for several Excel layouts
                        bResult = Write_ExcelFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        ' XNET 27.10.2011
                    Case vbBabel.BabelFiles.FileType.CremulSplitted
                        ' Special export for splitted Cremul files (used by Oslo Kommune)
                        ' XNET 28.11.2011 changed under
                        bResult = Write_CremulSplitted(oBabelFiles, sFILE_Name, iVB_Format_ID, bFILE_OverWrite, bFILE_WarningIfExists)
                        ' XNET 07.12.2011
                    Case vbBabel.BabelFiles.FileType.Wachovia_820_Receivables
                        bResult = WriteWachovia_820_Receivables(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo, nSequenceNumberStart, nSequenceNumberEnd)
                        'XNET 30.07.2012
                    Case vbBabel.BabelFiles.FileType.Comarch_Incoming
                        bResult = WriteComarch(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        ' XNET 30.10.2012 added avtalegiro
                    Case vbBabel.BabelFiles.FileType.Avtalegiro
                        bResult = WriteAvtalegiroFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo)
                        ' XNET 31.10.2012 added DanskeBankColDebitor
                    Case vbBabel.BabelFiles.FileType.DanskeBankColDebitor
                        bResult = WriteDanskeBankColDebitor(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)

                        ' XNET 04.03.2013 added WriteNordea_SE_Plusgirot_Fakturabetalning
                    Case vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService
                        bResult = WriteNordea_SE_Plusgirot_Fakturabetalning(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_AdditionalID, sVB_ClientNo, oExternalUpdateConnection, oExternalReadConnection)
                        ' XNET 28.08.2013.2013 added WriteSEB_Sisu
                    Case vbBabel.BabelFiles.FileType.SEB_Sisu
                        bResult = WriteSEB_Sisu(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID)
                    Case vbBabel.BabelFiles.FileType.NordeaCorporateFilePayments
                        'XOKNET - 18.08.2013 Added oExternalConnection
                        bResult = WriteCorporateFilePayments(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_AdditionalID, sVB_ClientNo)
                        ' XNET 26.03.2015 added Archer
                    Case vbBabel.BabelFiles.FileType.Archer_US_CSV
                        bResult = Write_Archer_US_CSV(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID)
                        ' XOKNET 06.07.2015 Addes SPISU, which is almost same as SEB_Sisu
                    Case vbBabel.BabelFiles.FileType.SPISU
                        bResult = WriteLeverantorsBetalningar(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, vbBabel.BabelFiles.FileType.SPISU)


                    Case vbBabel.BabelFiles.FileType.eFaktura_b2b_Splitted
                        ' Special export for splitted Cremul files (used by Oslo Kommune)
                        ' XNET 28.11.2011 changed under
                        bResult = Write_eFaktura_b2b_Splitted(oBabelFiles, sFILE_Name, iVB_Format_ID, bFILE_OverWrite, bFILE_WarningIfExists)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_A1
                        bResult = WriteGjensidigeJDE_S12GL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_Basware
                        bResult = WriteGjensidigeJDE_Basware_SUGL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, "BasWare")
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_FileNet
                        ' export on two different formats - one addressformat, one that is almost equal to Basware
                        bResult = WriteGjensidigeJDE_Filenet_Address(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        bResult = WriteGjensidigeJDE_Basware_SUGL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, "FileNet")
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_P2000
                        ' export on three different formats - one addressformat, one that is almost equal to Basware and S12
                        bResult = WriteGjensidigeJDE_Filenet_Address(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        bResult = WriteGjensidigeJDE_Basware_SUGL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, "P2000")
                        bResult = WriteGjensidigeJDE_S12GL(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.GenericXML
                        bResult = WriteGenericXML(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sSpecial)
                    Case vbBabel.BabelFiles.FileType.GenericText
                        bResult = WriteGenericText(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sSpecial, bFILE_OverWrite, bLog, oBabelLog)
                    Case vbBabel.BabelFiles.FileType.Connect_WCF_Remit
                        bResult = WriteConnect_WCF_Remit_Update(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)

                        'Standard calling routine!
                        'bResult = WriteHDP_CitibankFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo)
                        ' For WriteModule - Clip from function Function StandardFileWrite() in WriteSpecial
                        ' That should give you a headstart to write a new format!
                        '  Regards to Iggy!!!!!!!!!!!!!!!!!
                    Case vbBabel.BabelFiles.FileType.Connect_WCF
                        'bResult = WriteConnect_WCF(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial)
                        If sSpecial <> "CONECT_INKASSO" Then
                            'bResult = WriteConnect_WCF_NewGLHandling(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial, bPostAgainstObservationAccount)
                        Else
                            bResult = WriteConnect_WCF_Inkasso(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, sSpecial, bPostAgainstObservationAccount)
                        End If
                    Case vbBabel.BabelFiles.FileType.Bilia_IFS
                        bResult = WriteBiliaIFS(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sSpecial, bFILE_OverWrite)
                    Case vbBabel.BabelFiles.FileType.Infotjenester
                        bResult = WriteInfotjenester_Navision(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sSpecial, bFILE_OverWrite, iVB_FilenameInNo, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Visma_Collectors_innbetaling
                        bResult = WriteVisma_Collectors_innbetaling(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial, sVB_Branch, bPostAgainstObservationAccount, iVB_FilenameInNo)
                    Case vbBabel.BabelFiles.FileType.Agresso_Patentstyret
                        bResult = WriteAgresso_Patentstyret(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, iVB_FilenameInNo, sVB_ClientNo, bPostAgainstObservationAccount)
                    Case vbBabel.BabelFiles.FileType.Ins2000_Instech_InPayment
                        bResult = WriteIns2000_InPayment(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.KAR_kontovask_Nets_AccountOwner
                        'Added 07.04.2015
                        bResult = WriteKAR_Kontovask_Nets(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, vbBabel.BabelFiles.FileType.KAR_kontovask_Nets_AccountOwner)
                    Case vbBabel.BabelFiles.FileType.KAR_kontovask_Nets_AccountPayment
                        'Added 04.05.2015
                        bResult = WriteKAR_Kontovask_Nets(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, vbBabel.BabelFiles.FileType.KAR_kontovask_Nets_AccountPayment)
                    Case vbBabel.BabelFiles.FileType.Gjensidige_kontovask
                        'Added 08.04.2015
                        bResult = WriteGjensidige_Kontovask(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.BgMax
                        bResult = WriteBGMaxFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.Fabriken_SGFEQ
                        bResult = WriteFabriken_innbetaling(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, sSpecial, bPostAgainstObservationAccount, iVB_FilenameInNo)
                    Case vbBabel.BabelFiles.FileType.NorLines
                        bResult = WriteNorLines_XML(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.MT940
                        bResult = WriteMT940File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, eBank, sSpecial) ', nSequenceNumberStart, nSequenceNumberEnd)
                    Case vbBabel.BabelFiles.FileType.DanskeBankFinland
                        bResult = WriteDanskeBankCMFI(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.DanskeBankSweden
                        bResult = WriteDanskeBankCMSI(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.BGAutogiro
                        bResult = WriteBGAutogiroFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, sSpecial)
                    Case vbBabel.BabelFiles.FileType.JPMorgan_Norway
                        bResult = WriteJPMorgan_Norway(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_CompanyNo, sVB_ClientNo, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Maritech_CSV_Import
                        bResult = WriteMaritechCSV(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iVB_FilenameInNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.NetSuite
                        bResult = Write_NetSuite_csv(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo)
                    Case vbBabel.BabelFiles.FileType.Sanitetsforeningen
                        bResult = Write_Sanitetsforeningen_csv(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_ClientNo, sVB_REF_Own, iVB_FilenameInNo)
                    Case vbBabel.BabelFiles.FileType.HSBC_SG_ACH
                        bResult = WriteHSBC_SG_ACH(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo, sVB_Branch)
                    Case vbBabel.BabelFiles.FileType.XLedger
                        bResult = WriteXLedger(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iVB_FilenameInNo, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Camt054_Bama
                        bResult = WriteISO_20022(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_CompanyNo, sVB_Branch, sVB_ClientNo, sSpecial, eBank, iExportFormat)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_UAccounts_XML
                        bResult = WriteGjensidigeJDE_UAccounts(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_Accounts_XML
                        bResult = WriteGjensidigeJDE_Accounts(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_Costcenters_XML
                        bResult = WriteGjensidigeJDE_Costcenters(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_Suppliers_XML
                        bResult = WriteGjensidigeJDE_Suppliers(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_UnderRegnskap_XML
                        bResult = WriteGjensidigeJDE_UnderRegnskap(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_Users_XML
                        bResult = WriteGjensidigeJDE_Users(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_AdvancedPermissions_XML
                        bResult = WriteGjensidigeJDE_AdvancedPermissions(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_GL02b
                        bResult = WriteGjensidigeJDE_GL02b(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.OCBC_SG
                        bResult = WriteOCBC_SG_Giro(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_CompanyNo, sVB_Branch)
                    Case vbBabel.BabelFiles.FileType.Total_IN
                        bResult = WriteTotalInFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, sVB_AdditionalID, sVB_ClientNo)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_GL02_SYS
                        bResult = WriteGjensidigeJDE_GL02_SYS(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_GL02_SYSandAP02
                        bResult = WriteGjensidigeJDE_GL02_SYS(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                        bResult = WriteGjensidigeJDE_AP02(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, bWrittenBefore)
                        bWrittenBefore = True
                    Case vbBabel.BabelFiles.FileType.GjensidigeJDE_AdvancedPermissionsXLedger_XML
                        bResult = WriteGjensidigeJDE_AdvancedPermissions_XLedger(oBabelFiles, bVB_MultiFiles, sFILE_Name)
                    Case vbBabel.BabelFiles.FileType.Betalingsservice_0602
                        bResult = WriteBetalingsservice_0602File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_AdditionalID, sVB_ClientNo, False, False, sSpecial)
                    Case vbBabel.BabelFiles.FileType.Maconomy
                        If Not eTypeofRecords = vbBabel.BabelFiles.TypeofRecords.OCRonly Then
                            ' First, write a specific file for update of payers bank account
                            bResult = WriteMaconomyBankAccountsFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iVB_FilenameInNo, sSpecial)
                        End If
                        bResult = WriteMaconomy(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo, iVB_FilenameInNo, sSpecial, eTypeofRecords)
                    Case vbBabel.BabelFiles.FileType.PowerOfficeGo
                        bResult = Write_PowerOfficeGo(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.Utleiemegleren
                        bResult = WriteUtleiemegleren(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own)
                    Case vbBabel.BabelFiles.FileType.EuroFinans_Matched
                        ' 09.06.2022 - moved export of unmatched as first process, to lower the risk of double updates in KAF in error situations
                        ' For export of unmatched, from MatchingMenu, VB_FilenameInNo is Manipulated, to 2
                        bResult = WriteEuroFinans_UnMatched(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)

                        ' 31.01.2021 - Export matched payments by updating directly into the KAF database
                        bResult = WriteEuroFinans_MatchedToKAF(oBabelFiles, iVB_Format_ID, sVB_I_Account, sVB_ClientNo)


                        '------------ EDI + XML Formats --------------------------------------------------------------------------------------------------
                    Case vbBabel.BabelFiles.FileType.TBIXML, vbBabel.BabelFiles.FileType.TBI_General, vbBabel.BabelFiles.FileType.TBI_FI, vbBabel.BabelFiles.FileType.TBI_ACH, vbBabel.BabelFiles.FileType.TBI_Salary, vbBabel.BabelFiles.FileType.TBI_Reference, vbBabel.BabelFiles.FileType.tbi_local
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "XX"
                        oExportEDIObject.OutputPath = sFILE_Name
                        'oExportEDIObject.ExportFormat = GetEnumFileType(iExportFormat)
                        oExportEDIObject.BankByCode = eBank
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        oExportEDIObject.VB_Version = sVB_Version
                        'oExportEDIObject.MappingFile = App.path & "\" & GetEnumFileType(iExportFormat) & sEDIVersion & "NOBBS.XML"
                        bResult = oExportEDIObject.Export

                    Case vbBabel.BabelFiles.FileType.TelepayPlus

                        ' first call on EDI2XML.CEdi2Xml,  to do the oCorrect/frmCorrect stuff, but NO EXPORT
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "XX"
                        oExportEDIObject.OutputPath = sFILE_Name
                        'oExportEDIObject.ExportFormat = GetEnumFileType(iExportFormat)
                        oExportEDIObject.BankByCode = eBank
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        oExportEDIObject.VB_Version = sVB_Version
                        'oExportEDIObject.MappingFile = App.path & "\" & GetEnumFileType(iExportFormat) & sEDIVersion & "NOBBS.XML"
                        bResult = oExportEDIObject.Export

                        'bResult = WriteTelepayPlusFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, iExportFormat)
                        ' XNET 20.09.2012 Major change
                        ' Have problems when mix of international and domestic. Instead of k�dding with batches, simply export Int in one run, and Dom in next run
                        ' First, export Internationals only
                        bResult = WriteTelepayPlusFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, iExportFormat, True)
                        ' Then, export Domestics only
                        bResult = WriteTelepayPlusFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank, iExportFormat, False)
                        ' XOKNET 29.02.2012 - Major change!
                        ' DNB cannot treat debits from Norwegian accounts as TelepayPlus, Ref Yngve Langgaard, DNB
                        ' Then, we have to produce "normal" Telepayfiles for those
                        ' Can be deleivered in same physical file as TelepayPlus
                        'BUT - do not fuck up NHST_LONN !!
                        ' XNET 23.10.2012 - NHST in TELEPAYPLUS; use standard routines
                        'If sSpecial <> "NHST_LONN" Then
                        ' XNET 12.09.2012 New orders from Belt - use old way, that is, always DebetNo as Telepay, no specific division
                        bResult = WriteTelepay2File(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial, eBank)
                    Case vbBabel.BabelFiles.FileType.PAYMUL
                        For Each oBabel In oBabelFiles
                            oBabel.SortCriteria1 = "DATE_Payment"
                            oBabel.SortCriteria2 = "MON_TransferCurrency"
                            oBabel.SortCriteria3 = "VB_I_Account"
                            bResult = oBabel.SortPayment()
                        Next
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "NO"
                        oExportEDIObject.OutputPath = sFILE_Name
                        'oExportEDIObject.ExportFormat = GetEnumFileType(iExportFormat)
                        oExportEDIObject.BankByCode = eBank
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        'New 18.04.2008
                        oExportEDIObject.Special = sSpecial
                        'oExportEDIObject.MappingFile = App.path & "\" & GetEnumFileType(iExportFormat) & sEDIVersion & "NOBBS.XML"
                        bResult = oExportEDIObject.Export

                    Case vbBabel.BabelFiles.FileType.CREMUL
                        '            For Each oBabel In oBabelFiles
                        '                oBabel.SortCriteria1 = "DATE_Payment"
                        '                oBabel.SortCriteria2 = "MON_TransferCurrency"
                        '                oBabel.SortCriteria3 = "VB_I_Account"
                        '                bResult = oBabel.SortPayment()
                        '            Next
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.FilenameInNo = iVB_FilenameInNo
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "NO"
                        oExportEDIObject.OutputPath = sFILE_Name
                        If sSpecial = "VISMA_FOKUS" Then
                            oExportEDIObject.BankByCode = vbBabel.BabelFiles.Bank.DnB
                        Else
                            oExportEDIObject.BankByCode = eBank
                        End If
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        oExportEDIObject.Special = sSpecial
                        oExportEDIObject.I_Account = sVB_I_Account
                        oExportEDIObject.ClientNo = sVB_ClientNo

                        bResult = oExportEDIObject.Export

                    Case vbBabel.BabelFiles.FileType.DEBMUL
                        '            For Each oBabel In oBabelFiles
                        '                oBabel.SortCriteria1 = "DATE_Payment"
                        '                oBabel.SortCriteria2 = "MON_TransferCurrency"
                        '                oBabel.SortCriteria3 = "VB_I_Account"
                        '                bResult = oBabel.SortPayment()
                        '            Next
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.FilenameInNo = iVB_FilenameInNo
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "NO"
                        oExportEDIObject.OutputPath = sFILE_Name
                        If sSpecial = "VISMA_FOKUS" Then
                            oExportEDIObject.BankByCode = vbBabel.BabelFiles.Bank.DnB
                        Else
                            oExportEDIObject.BankByCode = eBank
                        End If
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        oExportEDIObject.Special = sSpecial
                        oExportEDIObject.I_Account = sVB_I_Account
                        oExportEDIObject.ClientNo = sVB_ClientNo

                        bResult = oExportEDIObject.Export

                    Case vbBabel.BabelFiles.FileType.BANSTA
                        '            For Each oBabel In oBabelFiles
                        '                oBabel.SortCriteria1 = "DATE_Payment"
                        '                oBabel.SortCriteria2 = "MON_TransferCurrency"
                        '                oBabel.SortCriteria3 = "VB_I_Account"
                        '                bResult = oBabel.SortPayment()
                        '            Next
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.FilenameInNo = iVB_FilenameInNo
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "NO"
                        oExportEDIObject.OutputPath = sFILE_Name
                        If sSpecial = "VISMA_FOKUS" Then
                            oExportEDIObject.BankByCode = vbBabel.BabelFiles.Bank.DnB
                        Else
                            oExportEDIObject.BankByCode = eBank
                        End If
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        oExportEDIObject.Special = sSpecial
                        oExportEDIObject.I_Account = sVB_I_Account
                        oExportEDIObject.ClientNo = sVB_ClientNo

                        bResult = oExportEDIObject.Export

                    Case vbBabel.BabelFiles.FileType.CONTRL
                        '            For Each oBabel In oBabelFiles
                        '                oBabel.SortCriteria1 = "DATE_Payment"
                        '                oBabel.SortCriteria2 = "MON_TransferCurrency"
                        '                oBabel.SortCriteria3 = "VB_I_Account"
                        '                bResult = oBabel.SortPayment()
                        '            Next
                        oExportEDIObject = New vbBabel.CEdi2Xml 'CreateObject ("EDI2XML.CEdi2Xml")
                        oExportEDIObject.BabelFiles = oBabelFiles
                        oExportEDIObject.FilesetupOut = iVB_Format_ID
                        oExportEDIObject.FilenameInNo = iVB_FilenameInNo
                        oExportEDIObject.CompanyNo = sVB_CompanyNo
                        oExportEDIObject.Country = "NO"
                        oExportEDIObject.OutputPath = sFILE_Name
                        If sSpecial = "VISMA_FOKUS" Then
                            oExportEDIObject.BankByCode = vbBabel.BabelFiles.Bank.DnB
                        Else
                            oExportEDIObject.BankByCode = eBank
                        End If
                        oExportEDIObject.Format_Renamed = GetEnumFileType(iExportFormat)
                        oExportEDIObject.EDIVersion = sVersion
                        oExportEDIObject.Special = sSpecial
                        oExportEDIObject.I_Account = sVB_I_Account
                        oExportEDIObject.ClientNo = sVB_ClientNo

                        bResult = oExportEDIObject.Export

                    Case vbBabel.BabelFiles.FileType.CREMULIntern
                        If Not bFILE_OverWrite Then
                            bResult = AppendToExistingFile(sFILE_Name, iVB_Format_ID)
                        End If
                        bResult = WriteCREMULInternFile(oBabelFiles, sFILE_Name)

                    Case Else
                        Err.Raise(35027, , LRS(35027, GetEnumFileType(iExportFormat)))

                End Select

                If bResult Then

                    ' XOKNET 20.05.2011
                    ' Create returnfiles for NHST
                    ' XNET 09.06.2011 added NHST_UK (will use XML for UK for a while
                    ' XNET 27.06.2011 Contra - no need for NHST_UK here !
                    If oBabelFiles.Item(1).Special = "NHST_SG" Or oBabelFiles.Item(1).Special = "NHST_US" Then

                        ' XNET 27.06.2011
                        ' For US, there may be two runs; one for ACH (Nacha), one for rest (checks)
                        ' Create only one returnfile !

                        'XNET 23.09.2011 -
                        ' And do not create returnfile before we have exported all payments
                        ' - for US, this will be after both Nacha and TP+, so we must test
                        ' if all payments are exported !!!
                        bAllExported = True
                        If oBabelFiles.Item(1).Special = "NHST_US" And oBabelFiles.FilterType <> "RETURNED" Then
                            For Each oBabelFile In oBabelFiles
                                For Each oBatch In oBabelFile.Batches
                                    For Each oPayment In oBatch.Payments
                                        If oPayment.Exported = False Then
                                            bAllExported = False
                                        End If
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile
                        End If

                        ' XNET 23.10.2012 - removed NHST_SG which will use real returnfiler for TelepayPlus
                        'If bAllExported And (oBabelFiles.Item(1).Special = "NHST_SG" Or
                        ' 14.05.2020 - added NHST_SG again (for OCBC bank payments)
                        If bAllExported And ((oBabelFiles.Item(1).Special = "NHST_SG" Or oBabelFiles.Item(1).Special = "NHST_US") And oBabelFiles.FilterType <> "RETURNED") Then

                            ' Remove exported, and set status = 02
                            For Each oBabelFile In oBabelFiles
                                oBabelFile.StatusCode = "02"
                                For Each oBatch In oBabelFile.Batches
                                    oBatch.StatusCode = "02"
                                    For Each oPayment In oBatch.Payments
                                        oPayment.Exported = False
                                        oPayment.StatusCode = "02"
                                        oPayment.DATE_Value = oPayment.DATE_Payment
                                        oPayment.MON_AccountAmount = oPayment.MON_TransferredAmount ' XNET added 24.08.2011 for returnfiles
                                        For Each oInvoice In oPayment.Invoices
                                            oInvoice.StatusCode = "02"
                                        Next oInvoice
                                    Next oPayment
                                Next oBatch
                            Next oBabelFile

                            ' export TBIO return
                            sFILE_Name = Left$(sFILE_Name, InStrRev(sFILE_Name, ".")) + "return"
                            bResult = WriteTelepayFile(oBabelFiles, bVB_MultiFiles, sFILE_Name, iVB_Format_ID, sVB_I_Account, sVB_REF_Own, iVB_FilenameInNo, sVB_CompanyNo, sVB_Branch, sVB_Version, False, True, False, nSequenceNumberStart, nSequenceNumberEnd, sVB_ClientNo, sSpecial)
                            ' XNET 27.06.2011
                            oBabelFiles.FilterType = "RETURNED" ' returnfile created!
                        End If

                    End If
                    If Not bLicenseCheckedBefore Then
                        ' New 05.01.06, Do not count OCR for BabelBank Innbetalings-customers
                        If Not oBabelFiles.VB_Profile Is Nothing Then
                            bAutoMatch = oBabelFiles.VB_Profile.FileSetups(oBabelFiles.VB_Profile.FileSetups(iVB_Format_ID).FileSetupOut).AutoMatch
                        Else
                            bAutoMatch = False
                        End If
                        If bAutoMatch And (iExportFormat = vbBabel.BabelFiles.FileType.OCR Or iExportFormat = vbBabel.BabelFiles.FileType.OCR_Simulated_KID Or iExportFormat = vbBabel.BabelFiles.FileType.AutoGiro) Then ' Hvis Avstemming og OCR/Autogiro, og avstemming: Ikke tell, Ellers; tell

                            'Case vbbabel.BabelFiles.FileType.OCR_Simulated ' Hvis OCR_Simulated tell
                            'Case vbbabel.BabelFiles.FileType.OCR_Simulated_KID ' Hvis OCR_Simulated_KID, og avstemming: Ikke tell, Ellers (skal ikke forkomme); tell
                            '        Case vbbabel.BabelFiles.FileType.AutoGiro 'Hvis Autogiro, og avstemming: Ikke tell, Ellers; tell
                            '
                            '        Alternativt
                            '        Hvis avstemming, og merket avstem OCR-merket er false og det er en OCR-post - ikke tell
                            '        Denne er mest korrekt!
                            '
                            '        hvis avstemming:
                            '        If oFilesetup.AutoMatch Then
                            '        ofilesetup.matchocr = true ?
                            '        hvis false: ikke tell opp, dersom ocr-poster
                            '
                            '        Nytt: telle opp kun der exported = true ???
                        Else
                            ' tell opp resten !!!!

                            'Don't write to licensefile and Profile
                            'New 26.01.2007 - next 3 Ifs
                            If Not oBabelFiles Is Nothing Then
                                If oBabelFiles.Count > 0 Then
                                    If oBabelFiles.Item(1).VB_ProfileInUse Then
                                        nInvoices = CountInvoices(iVB_Format_ID)
                                        oLicense = New License
                                        oLicense.LicUpdate(nInvoices)
                                        ' done automatically oLicense.LicWrite

                                        ' 12.09.2018 <-------------------
                                        ' Re Kjell's problems at Lindorff where oLicense is not released before Pause(2) at the end of ImportExport
                                        ' Added next line
                                        oLicense.LicWrite()

                                        oLicense = Nothing
                                        bLicenseCheckedBefore = True
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    Export = False
                    Exit Function
                End If

                Export = True
            End If

            'Added code 4/12-2002 - Kjell. Next IF
            If Not oExportEDIObject Is Nothing Then
                oExportEDIObject = Nothing
            End If

            bReturnValue = True

        Catch ex As vbBabel.Payment.PaymentException ' - Testing Try ... Catch
            'ProgressStop()

            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20190612" Then
                MsgBox("EXPORT Kommet inn i riktig feilh�ndtering." & vbCrLf & "Feilmelding: " & ex.Message)
            End If

            Throw
            'Throw New vbBabel.Payment.PaymentException(ex.Message, ex.InnerException, ex.ErrorPayment, ex.LastLineRead, ex.FilenameImported) ' - Testing Try ... Catch

        Catch ex As Exception

            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20190612" Then
                MsgBox("EXPORT Kommet inn i feil feilh�ndtering." & vbCrLf & "Feilmelding: " & ex.Message)
            End If

            If bERR_UseErrorObject Then
                Throw New Exception(ex.Message, ex)
            Else
                bReturnValue = False
            End If

        Finally
            'Added code 4/12-2002 - Kjell. Next IF
            If Not oExportEDIObject Is Nothing Then
                oExportEDIObject = Nothing
            End If

        End Try

        Return bReturnValue

    End Function

    Friend Function CountInvoices(ByRef iFormat_ID As Short) As Double
        ' Counts how many invoices we have exported for this format
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim nCounter As Double

        nCounter = 0

        For Each oBabel In oBabelFiles
            ' added 08.06.2012
            ' for eFakturasplit we have no collections. But NoOfInvoices is set in
            ' oBabel.EDIMessageNO when oBabel.Special = "EFAKTURASPLIT"
            If oBabel.Special = "EFAKTURASPLIT" Then
                nCounter = CDbl(oBabel.EDI_MessageNo)
            End If
            For Each oBatch In oBabel.Batches
                ' One exeption; Telepay confirmation return (mottaksretur)
                'If Not (oBatch.FormatType = "TELEPAY" And oBatch.StatusCode = "01") Then
                ' 01.06.2011 - Also consider Telepay2, Telepay+,etc
                If Not (Microsoft.VisualBasic.Left(oBatch.FormatType, 7) = "TELEPAY" And oBatch.StatusCode = "01") Then
                    For Each oPayment In oBatch.Payments

                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            For Each oInvoice In oPayment.Invoices
                                ' XOKNET 15.01.2016 - her m� vi teste p� match_final = True
                                If oInvoice.MATCH_Final = True Then
                                    nCounter = nCounter + 1
                                End If
                            Next oInvoice
                        End If

                    Next oPayment
                End If
            Next oBatch
        Next oBabel
        CountInvoices = nCounter

    End Function

    Public Function FILE_Exists() As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim bReturnValue As Boolean

        oFs = New Scripting.FileSystemObject

        bReturnValue = False
        If oFs.FileExists(sFILE_Name) Then
            bReturnValue = True
            If bFILE_OverWrite Then
                If bFILE_WarningIfExists Then
                    ' The file <filename> exists.
                    sFILE_MessageIfExists = LRS(60003, sFILE_Name)
                End If
            Else
                If bFILE_WarningIfExists Then
                    ' The file <filename> exists. Program terminates!
                    sFILE_MessageIfExists = LRS(60004, sFILE_Name)
                End If
            End If
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        '
        'If bFILE_OverWrite Then
        '    If bFILE_WarningIfExists Then
        '        '"Skal filen " & sFileName & " overskrives?"
        '        sFILE_MessageIfExists = LRS(60010) & " " & sFileName & " " & LRS(60011) & "?"
        '    End If
        'Else
        '    If bFILE_WarningIfExists Then
        '        ' Filen <filenavn> finnes fra f�r. �nsker du at nye data skal legges til i fil?
        '        sFILE_MessageIfExists = LRS(60012) & " " & sFileName & " " & LRS(60013)
        '    End If
        'End If

        FILE_Exists = bReturnValue

    End Function
    Public Function FILE_Delete() As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim iStatus As Short
        Dim bContinue As Boolean
        Dim iCounter As Integer = 0

        oFs = New Scripting.FileSystemObject

        'Check if file exists, maybe the method ExportfileExist hasn't been run
        If oFs.FileExists(sFILE_Name) Then
            ' 20.09.2010 - Check if we are allowed to delete file
            iStatus = CheckFile(sFILE_Name, (vb_Utils.ErrorMessageType.Dont_Show), "FILE_Delete")
            If iStatus > 50 Then
                ' something wrong
                If iStatus = 52 Then
                    ' 27.11.2012
                    ' Gjensidige has problems with locked importfile, causing problems when moving file to backup
                    ' First, try to change attributes
                    IO.File.SetAttributes(sFILE_Name, IO.FileAttributes.Normal)
                    Application.DoEvents()

                    ' Try first to wait, and do a new check
                    For iCounter = 1 To 10
                        ' "sleep" for 0,5 second
                        Threading.Thread.Sleep(500)
                        iStatus = CheckFile(sFILE_Name, (vb_Utils.ErrorMessageType.Dont_Show), "FILE_Delete")
                        If iStatus <> 52 Then
                            ' file is released, jump out
                            Exit For
                        End If
                    Next iCounter
                    If iStatus = 52 Then
                        ' File is locked. Check if it is in use by another program.
                        Err.Raise(30026, "FILE_Delete", "CreateBackup:" & vbCrLf & "30026 " & LRSCommon(30026, sFILE_Name))
                    End If
                Else
                    Err.Raise(30023, "FILE_Delete", "CreateBackup:" & vbCrLf & "30023 " & LRSCommon(30023, sFILE_Name))
                End If

                bContinue = False
            Else
                ' as pre 20.09.2010
                oFs.DeleteFile((sFILE_Name))
                ' 20.09.2010 New line
                bContinue = True
            End If
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        FILE_Delete = bContinue 'True

    End Function


    Private Function AppendToExistingFile(ByRef sFilenameOut As String, ByRef iFormat_ID As Short) As Boolean
        Dim bAppendFile, bReturnValue, bFoundLastRecord As Boolean
        Dim lExistingLines, lCount As Integer
        Dim oFs As Scripting.FileSystemObject
        Dim oFileWrite, oFile, oFileRead As Scripting.TextStream
        Dim sTempFilename, sLine As String 'sLine = one output-line


        bReturnValue = True
        iNoOfRecords = 0
        iNoofLinesInEndRecord = 0
        nTotalAmount = 0

        oFs = New Scripting.FileSystemObject

        'Check if we are appending to an existing file.
        ' If so BB has to delete the last record(s) in some formats, and save the counters
        If oFs.FileExists(sFilenameOut) Then
            Select Case iFormat_ID
                Case vbBabel.BabelFiles.FileType.Telepay
                Case vbBabel.BabelFiles.FileType.OCR
                    iStartposTest = 1
                    iLengthTest = 8
                    sTestValue = "NY000089"
                    iNoofLinesInEndRecord = 1
                    iLineLength = 8 'May have to be changed, depending on the use
                    iNoOfRecords = 0
                    iStartposNoofRecords = 9
                    iLengthNoofRecords = 8
                    iNoofLinesInEndRecord = 0
                    iStartposNoofLinesInEndRecord = 17
                    iLengthNoofLinesInEndRecord = 8
                    nTotalAmount = 0
                    iStartposTotalAmount = 25
                    iLengthTotalAmount = 17
                Case vbBabel.BabelFiles.FileType.Dirrem
                Case vbBabel.BabelFiles.FileType.PostbOCROld
                Case vbBabel.BabelFiles.FileType.LHLEkstern
                Case vbBabel.BabelFiles.FileType.CREMULIntern
                Case Else
            End Select

            bAppendFile = True
            bFoundLastRecord = False
            lExistingLines = 0
            'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
            sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
            'First count the number of lines in the existing file
            Do While Not oFile.AtEndOfStream = True
                lExistingLines = lExistingLines + 1
                oFile.SkipLine()
            Loop
            oFile.Close()
            'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oFile = Nothing
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
            'Then check if one of the 5 last lines are an end-record
            ' Store the number of lines until the end-record
            Do Until oFile.Line = lExistingLines - iNoofLinesInEndRecord
                oFile.SkipLine()
            Loop
            For lCount = iNoofLinesInEndRecord To 0 Step -1
                sLine = oFile.ReadLine()
                If Len(sLine) > iLineLength Then
                    If Mid(sLine, iStartposTest, iLengthTest) = sTestValue Then
                        lExistingLines = lExistingLines - lCount
                        bFoundLastRecord = True
                        Exit For
                    End If
                End If
            Next lCount
            oFile.Close()
            'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oFile = Nothing
            If Not bFoundLastRecord Then
                'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                MsgBox("The existing file is not an " & GetEnumFileType(iFormat_ID) & "-file")
                AppendToExistingFile = False
                Exit Function
                'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFs = Nothing
            Else
                'Copy existing file to a temporary file and delete existing file
                oFs.CopyFile(sFilenameOut, sTempFilename, True)
                oFs.DeleteFile(sFilenameOut)
                'Coopy the content of the temporary file to the new file,
                ' excluding the endrecord
                oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                Do Until oFileRead.Line > lExistingLines - iNoofLinesInEndRecord
                    sLine = oFileRead.ReadLine
                    oFileWrite.WriteLine((sLine))
                Loop
                sLine = ""
                'Read the endrecord
                For lCount = 0 To iNoofLinesInEndRecord
                    sLine = sLine & oFileRead.ReadLine
                Next lCount
                'Store the neccesary counters and totalamount
                If iNoOfRecords = 0 Then
                    iNoOfRecords = Val(Mid(sLine, iStartposNoofRecords, iLengthNoofRecords))
                End If
                'Subtract the apprpriate number of lines because we have deleted the last record
                If iNoofLinesInEndRecord = 0 Then
                    iNoofLinesInEndRecord = Val(Mid(sLine, iStartposNoofLinesInEndRecord, iLengthNoofLinesInEndRecord)) - iNoofLinesInEndRecord
                End If
                If nTotalAmount = 0 Then
                    nTotalAmount = Val(Mid(sLine, iStartposTotalAmount, iLengthTotalAmount))
                End If
                'Kill'em all
                oFileRead.Close()
                'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFileRead = Nothing
                oFileWrite.Close()
                'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFileWrite = Nothing
                oFs.DeleteFile(sTempFilename)
                bReturnValue = True
            End If
        End If

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If
        If Not oFileWrite Is Nothing Then
            oFileWrite.Close()
            oFileWrite = Nothing
        End If
        If Not oFileRead Is Nothing Then
            oFileRead.Close()
            oFileRead = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        AppendToExistingFile = bReturnValue

    End Function
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		If Not oBabelFiles Is Nothing Then
			'UPGRADE_NOTE: Object oBabelFiles may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			oBabelFiles = Nothing
		End If
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
    'XNET 25.11.2010 Extra preparation for xTraPersonell Swedish domestic payments
    Private Function PrepareXtraPersonellSweden(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.BANK_SWIFTCode = "XXXXXXXXXXX" Then
                        oPayment.BANK_SWIFTCode = ""
                        ' added 24.10.2014 - because we have set a new test in WriteLeverant�rsbetalinger, stopping the Swedish Payments
                        oPayment.Cancel = False
                        oPayment.Exported = False
                    Else
                        ' Swedish payment marked with "XXXXXXXXXXX"
                        ' The others have already been exported in Telepay2 format
                        oPayment.Cancel = True
                    End If
                Next
            Next
        Next

        PrepareXtraPersonellSweden = True

    End Function
    '*****************************************************************************************
    ' XNET 23.10.2012 should not be necessary to use this one for TelepayPlus for all payments
    '*****************************************************************************************
    ' NHST Salaryfiles will be exported on three different formats -
    ' based on one input. We must prepare the content of the payments before
    ' each export
    Private Function NHST_Lonn_PreProcessExport(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iExportFormat As vbBabel.BabelFiles.FileType) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment

        On Error GoTo ErrNHST

        Select Case iExportFormat
            Case vbBabel.BabelFiles.FileType.Telepay2
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "TP" Then
                                oPayment.Exported = False
                                oPayment.ExtraD1 = "TP+" ' to be used to test in WriteTBIXML
                            Else
                                oPayment.Exported = True
                            End If
                        Next
                    Next
                Next

            Case vbBabel.BabelFiles.FileType.TBI_Salary, vbBabel.BabelFiles.FileType.TBI_General
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "TBI" Then
                                oPayment.Exported = False
                                oPayment.DnBNORTBIPayType = "SAL"
                            Else
                                ' Trick TBI-export to not export, but also to accept values;
                                If oPayment.VoucherType = "TP+" Then
                                    oPayment.DnBNORTBIPayType = "ACH" ' do not process in TBI
                                ElseIf oPayment.VoucherType = "Nacha" Then
                                    oPayment.DnBNORTBIPayType = "ACH" ' do not process in TBI
                                ElseIf oPayment.VoucherType = "TP" Then
                                    oPayment.ExtraD1 = "TP+" ' to be used to test in WriteTBIXML, and not export in XML
                                Else
                                    oPayment.DnBNORTBIPayType = "GEN" ' do not process in TBI
                                End If
                            End If
                        Next
                    Next
                Next

                ' 26.08.2011 Added Nachafiles (for US)
            Case vbBabel.BabelFiles.FileType.Nacha
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "Nacha" Then
                                oPayment.Exported = False
                                oPayment.DnBNORTBIPayType = "ACH"
                            Else
                                ' Trick TBI-export to not export, but also to accept values;
                                If oPayment.VoucherType = "TP+" Then
                                    oPayment.DnBNORTBIPayType = "ACH" ' do not process in TBI
                                ElseIf oPayment.VoucherType = "TP" Then
                                    oPayment.ExtraD1 = "TP+" ' to be used to test in WriteTBIXML, and not export in XML
                                Else
                                    ' TODO 31.01.2012 dette er feil !!!!
                                    '''''oPayment.DnBNORTBIPayType = "GEN" ' do not process in TBI
                                End If
                            End If
                        Next
                    Next
                Next

            Case vbBabel.BabelFiles.FileType.TelepayPlus
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "TP+" Then
                                oPayment.Exported = False
                                oPayment.ExtraD1 = "TP+" ' to be used to test in WriteTBIXML
                                oPayment.DnBNORTBIPayType = "SAL"
                            Else
                                If oPayment.VoucherType = "TP" Then
                                    oPayment.ExtraD1 = "TP+" ' to be used to test in WriteTBIXML, and not export in XML
                                End If
                                oPayment.Exported = True
                            End If
                        Next
                    Next
                Next

        End Select

        NHST_Lonn_PreProcessExport = True
        Exit Function

ErrNHST:
        NHST_Lonn_PreProcessExport = False

    End Function
    ' XNET 09.01.2012 - Take whole function !!!!
    ' AKER KVAERNER US - NO accounts in TelepayPlus, US Accounts in TBI and Nacha
    Private Function AkerKvaernerUS_PreProcessExport(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iExportFormat As vbBabel.BabelFiles.FileType) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment

        On Error GoTo ErrAker

        Select Case iExportFormat

            Case vbBabel.BabelFiles.FileType.TBI_General
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "TBI" Then
                                oPayment.Exported = False
                                oPayment.DnBNORTBIPayType = "GEN"
                            Else
                                ' Trick TBI-export to not export, but also to accept values;
                                If oPayment.VoucherType = "TP+" Then
                                    'oPayment.DnBNORTBIPayType = "ACH" ' do not process in TBI
                                ElseIf oPayment.VoucherType = "Nacha" Then
                                    'oPayment.DnBNORTBIPayType = "ACH" ' do not process in TBI
                                Else
                                    'oPayment.DnBNORTBIPayType = "GEN" ' do not process in TBI
                                End If
                                oPayment.Exported = True
                            End If
                        Next
                    Next
                Next

            Case vbBabel.BabelFiles.FileType.Nacha
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "Nacha" Then
                                oPayment.Exported = False
                                oPayment.DnBNORTBIPayType = "ACH"
                            Else
                                ' Trick TBI-export to not export, but also to accept values;
                                If oPayment.VoucherType = "TP+" Then
                                    'oPayment.DnBNORTBIPayType = "ACH" ' do not process in TBI
                                Else
                                    'oPayment.DnBNORTBIPayType = "GEN" ' do not process in TBI
                                    oPayment.Exported = True
                                End If
                            End If
                        Next
                    Next
                Next

            Case vbBabel.BabelFiles.FileType.TelepayPlus
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VoucherType = "TP+" Then
                                oPayment.Exported = False
                                oPayment.ExtraD1 = "TP+" ' to be used to test in WriteTBIXML
                                oPayment.DnBNORTBIPayType = "GEN"
                            Else
                                oPayment.Exported = True
                            End If
                        Next
                    Next
                Next

        End Select

        AkerKvaernerUS_PreProcessExport = True
        Exit Function

ErrAker:
        AkerKvaernerUS_PreProcessExport = False

    End Function
    Private Function TreatSalmarTerminkontrakt(ByVal oBabelfiles As vbBabel.BabelFiles)
        ' run through collections, and check if terminkontrakt
        ' if terminkontrakt, then check for �rediff
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim nTotAmount As Double = 0
        Dim aAdjustments(,) As String
        Dim sClient_ID As String = ""
        Dim oFileSetup As FileSetup
        Dim oClient As Client
        Dim oAccount As Account

        For Each oBabelFile In oBabelfiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.E_Name = "NORDEA BANK NORGE ASA" Then
                        nTotAmount = 0
                        For Each oFilesetup In oPayment.VB_Profile.FileSetups
                            For Each oClient In oFilesetup.Clients
                                For Each oaccount In oClient.Accounts
                                    If oAccount.Account = oPayment.I_Account Then
                                        sClient_ID = oClient.Client_ID
                                        Exit For
                                    End If
                                Next oaccount
                            Next oclient
                        Next ofilesetup


                        aAdjustments = GetAdjustments("1", sClient_ID)
                        ' check if invoiceamount balances;
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final Then
                                If oInvoice.MON_CurrencyAmount <> 0 Then
                                    ' calculate amount based on currencyrate and currencyamount
                                    oInvoice.MON_InvoiceAmount = oInvoice.MON_CurrencyAmount * oPayment.MON_LocalExchRate
                                    oInvoice.MON_TransferredAmount = oInvoice.MON_InvoiceAmount
                                End If
                                nTotAmount = nTotAmount + oInvoice.MON_InvoiceAmount
                            End If
                        Next
                        If oPayment.MON_InvoiceAmount <> nTotAmount Then
                            ' add �rediff;
                            oInvoice = oPayment.Invoices.Add
                            oInvoice.MON_InvoiceAmount = oPayment.MON_InvoiceAmount - nTotAmount

                            oInvoice = oPayment.Invoices.Add
                            oInvoice.MATCH_Final = True
                            oInvoice.MATCH_Matched = True
                            'XokNET, MATCH_Original is changed to False. May be critical but seems correct and is commented before
                            oInvoice.MATCH_Original = False
                            oInvoice.MATCH_ID = aAdjustments(3, 0) 'MATCH_DifferenceAccount
                            oInvoice.StatusCode = "02"
                            oInvoice.VB_Profile = oPayment.VB_Profile
                            oFreeText = oInvoice.Freetexts.Add
                            oFreeText.Qualifier = 3
                            oFreeText.Text = "�redifferanse"
                            '-- test n�ye her ---
                        End If
                        'GL-Account
                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL '2

                    End If
                Next
            Next
        Next
    End Function
    Private Sub NHST_Backup(ByVal sFilename As String, ByVal sBackupPath As String)
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bOK As Boolean = False

        oBackup = New vbBabel.BabelFileHandling
        oBackup.BackupPath = sBackupPath
        oBackup.SourceFile = sFilename
        oBackup.DeleteOriginalFile = False
        oBackup.FilesetupID = 0
        oBackup.InOut = "O"
        oBackup.FilenameNo = 0 'FilenameOut1 or 2 or 3
        oBackup.Client = ""
        oBackup.TheFilenameHasACounter = False
        oBackup.BankAccounting = "P"

        bOK = oBackup.CreateBackup()
        oBackup = Nothing

    End Sub
    Private Function ConectoAddTotalsToProcassoFile(ByVal oBabelFiles As BabelFiles) As Boolean
        ' Then add a new record for each batch, totalling up the Predator-exported OCR-records.
        ' These totals must be exported to the Procasso OCR-file, as totals with a special kid
        ' The Predatorrecords are set with oPayment.ExtraD1 = "TILPREDATOR", and has Exported = True
        Dim oBabelFile As vbbabel.BabelFile
        Dim oBatch As vbbabel.Batch
        Dim oPayment As vbbabel.Payment
        Dim oInvoice As vbbabel.Invoice
        Dim cBatchTotalPredatorAmount As Decimal
        Dim bFound As Boolean
        Dim i As Integer

        'Dim oNewPayment As vbbabel.Payment
        'Dim oTempInvoice As vbbabel.Invoice

        ' For OCR, total up pr file
        For Each oBabelFile In oBabelFiles
            If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.OCR Then
                For Each oBatch In oBabelFile.Batches
                    ' total up for all TILPREDATOR-payments in this batch;
                    cBatchTotalPredatorAmount = 0
                    For Each oPayment In oBatch.Payments
                        If oPayment.ExtraD1 = "TILPREDATOR" Then
                            cBatchTotalPredatorAmount = cBatchTotalPredatorAmount + oPayment.MON_InvoiceAmount
                            ' set as exported
                            oPayment.Exported = True
                        End If
                    Next oPayment
                    ' Then, use the first of the TILPREDATOR-payments as the Total-record;
                    For Each oPayment In oBatch.Payments
                        If oPayment.ExtraD1 = "TILPREDATOR" Then
                            ' Update amounts like total Predatoramount in Batch
                            oPayment.MON_InvoiceAmount = cBatchTotalPredatorAmount
                            oPayment.MON_TransferredAmount = cBatchTotalPredatorAmount
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.MON_InvoiceAmount = cBatchTotalPredatorAmount
                                oInvoice.MON_TransferredAmount = cBatchTotalPredatorAmount
                            Next oInvoice
                            oPayment.PayType = "D"
                            oPayment.E_Adr1 = ""
                            oPayment.E_Adr2 = ""
                            ' set specialKID
                            'oPayment.Invoices(1).Unique_Id = String(16, "6")
                            ' for "falsk" OCR, we are exporting Match_ID in the OCR-file;
                            'oPayment.Invoices(1).MATCH_ID = String(16, "6")
                            ' Vil ha flere invoices, en original og en ny matchInvoice
                            For Each oInvoice In oPayment.Invoices
                                If Not (EmptyString(oInvoice.MATCH_ID)) And oInvoice.MATCH_Final = True And oInvoice.MATCH_Matched = True And oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                    oInvoice.MATCH_ID = StrDup(16, "6")
                                    oInvoice.Exported = False
                                End If
                            Next oInvoice


                            ' Set as NOT exported, so we can export it in the Procasso OCR-file
                            oPayment.Exported = False
                            oPayment.ExtraD1 = "TILPREDATORTELEPAY"

                            ' Use the first TILPREDATOR-record in the batch only for this purpose
                            Exit For
                        End If
                    Next oPayment
                Next oBatch
            End If
        Next oBabelFile

        cBatchTotalPredatorAmount = 0
        bFound = False

        '-----------------------------------
        ' For Cremul, total up for all files
        '-----------------------------------
        For Each oBabelFile In oBabelFiles
            ' total up for all TILPREDATOR-payments in all files

            If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL Then
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.ExtraD1 = "TILPREDATOR" Then
                            ' can have some invoices to Predator, some to Procasso
                            'cBatchTotalPredatorAmount = cBatchTotalPredatorAmount + oPayment.MON_InvoiceAmount
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.Extra1 = "TILPREDATOR" Then
                                    cBatchTotalPredatorAmount = cBatchTotalPredatorAmount + oInvoice.MON_InvoiceAmount
                                End If
                            Next oInvoice
                            ' set as exported
                            oPayment.Exported = True

                        End If
                    Next oPayment
                Next oBatch
            End If
        Next oBabelFile

        bFound = False ' added 23.10.2014
        For Each oBabelFile In oBabelFiles

            If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL Then
                For Each oBatch In oBabelFile.Batches
                    ' Then, use the first of the TILPREDATOR-payments as the Total-record;

                    For Each oPayment In oBatch.Payments
                        If oPayment.ExtraD1 = "TILPREDATOR" Then

                            ' 10.10.2014 Must copy this payment, to use it as a "totalpayment" in Procassofile, with 66666666666666 in kid
                            '                    Set oNewPayment = CreateObject ("vbbabel.Payment")
                            '                    Set oNewPayment = oPayment.CopyObject
                            '                    For Each oInvoice In oPayment.Invoices
                            '                        Set oTempInvoice = CreateObject ("vbbabel.Invoice")
                            '                        Set oTempInvoice = oNewPayment.Invoices.VB_AddWithObject(oInvoice)
                            '                    Next oInvoice
                            '                    Set oNewPayment = oBatch.Payments.VB_AddWithObject(oNewPayment)

                            ' Update amounts like total Predatoramount in Batch
                            'oNewPayment.MON_InvoiceAmount = cBatchTotalPredatorAmount
                            'oNewPayment.MON_TransferredAmount = cBatchTotalPredatorAmount
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.Extra1 = "TILPREDATOR" Then
                                    oInvoice.MON_InvoiceAmount = cBatchTotalPredatorAmount
                                    oInvoice.MON_TransferredAmount = cBatchTotalPredatorAmount
                                    oInvoice.Exported = True ' to not have them in Procassofile
                                Else
                                    ' 23.10.2014 to have them in the Procassofile
                                    oInvoice.Exported = False
                                    oPayment.Exported = False
                                End If
                            Next oInvoice
                            oPayment.PayType = "D"
                            oPayment.E_Adr1 = ""
                            oPayment.E_Adr2 = ""

                            ' BEHOLD KUN EN TOTAL TILPREDATORINVOICE I TOTAL_PAYMENTEN;OG, ogs� de som er delavstemte og tilh�rer Procasso
                            For i = oPayment.Invoices.Count To 1 Step -1

                                'If Not (EmptyString(oPayment.Invoices(i).MATCH_ID)) And oPayment.Invoices(i).MATCH_Final = True And oPayment.Invoices(i).MATCH_Matched = True And oPayment.Invoices(i).MATCH_MatchType = MatchedOnInvoice And oPayment.Invoices(i).Extra1 = "TILPREDATOR" Then
                                If oPayment.Invoices(i).Extra1 = "TILPREDATOR" Then
                                    If Not bFound Then
                                        oPayment.Invoices(i).MATCH_ID = StrDup(16, "6")
                                        ' 23.10.2014
                                        oPayment.Exported = False
                                        oPayment.Invoices(i).Exported = False
                                        bFound = True
                                        oPayment.ExtraD1 = "TILPREDATORTELEPAY"
                                    Else
                                        ' Remove the "TILPREDATOR" invoices, leave rest
                                        If oPayment.Invoices(i).Extra1 = "TILPREDATOR" Then
                                            oPayment.Invoices.Remove(i)
                                        End If
                                    End If
                                Else
                                    ' leave procassoinvoices
                                    oPayment.Invoices(i).Exported = False
                                End If
                            Next i

                            ' Set as NOT exported, so we can export it in the Procasso OCR-file
                            oPayment.Exported = False
                            bFound = True
                            ' Use the first TILPREDATOR-record in the first file only for this purpose
                            ' 23.10.2014 BUT do not exit, because we need to set the payments with a mix of Predator and procasso invoices correctly
                            'Exit For
                        End If
                    Next oPayment
                    ' 23.10.2014 BUT do not exit, because we need to set the payments with a mix of Predator and procasso invoices correctly
                    'If bFound Then
                    '    Exit For
                    'End If
                Next oBatch
            End If
            If bFound Then
                Exit For
            End If

        Next oBabelFile

        ConectoAddTotalsToProcassoFile = True
    End Function
    ' XNET 12.06.2014
    Private Function ConectoPrepareTelepayToPredatorFile(ByVal oBabelFiles As BabelFiles) As Boolean
        ' Prepare a Telepayfile, with totals of payments that have been transferred to Predator
        Dim oBabelFile As vbbabel.BabelFile
        Dim oBatch As vbbabel.Batch
        Dim oPayment As vbbabel.Payment
        Dim oFreetext As FreeText
        Dim cTotalPredatorAmount As Decimal
        Dim bFound As Boolean
        Dim i As Integer

        'For Each oBabelFile In oBabelFiles
        '    For Each oBatch In oBabelFile.Batches
        '        ' total up for all TILPREDATOR-payments in this batch;
        '        For Each oPayment In oBatch.Payments
        '            If oPayment.ExtraD1 = "TILPREDATORTELEPAY" Then
        '                cTotalPredatorAmount = cTotalPredatorAmount + oPayment.MON_InvoiceAmount
        '            End If
        '        Next oPayment
        '    Next oBatch
        'Next oBabelFile

        bFound = False
        For Each oBabelFile In oBabelFiles
            ' Dette er f�r siste eksport, kun Telepay igjen.
            ' 08.09.2014 - Vi kan derfor sette all statuser til 00 for alle poster
            oBabelFile.StatusCode = "00"
            For Each oBatch In oBabelFile.Batches
                ' Use all TILPREDATOR - bunter as outgoing
                oBatch.I_Branch = "PROCASSO" ' Divisjon i Telepay
                oBatch.StatusCode = "00"

                For Each oPayment In oBatch.Payments
                    oPayment.StatusCode = "00"
                    If oPayment.ExtraD1 = "TILPREDATORTELEPAY" Then
                        oPayment.PayCode = "150"
                        oPayment.E_Account = "90011333434"
                        oPayment.E_Name = "Conecto"
                        oPayment.VB_ClientNo = "1"


                        ' lagt til 08.09.2014
                        ' bruk dagens dato;
                        oPayment.DATE_Payment = DateToString(DateSerial(Year(Now), Month(Now), Microsoft.VisualBasic.Day(Now)))
                        oBatch.DATE_Production = DateToString(DateSerial(Year(Now), Month(Now), Microsoft.VisualBasic.Day(Now)))

                        ' Update amounts like total Predatoramount in Batch
                        'oPayment.MON_InvoiceAmount = cTotalPredatorAmount
                        'oPayment.MON_TransferredAmount = cTotalPredatorAmount
                        'oPayment.Invoices(1).MON_InvoiceAmount = cTotalPredatorAmount
                        'oPayment.Invoices(1).MON_TransferredAmount = cTotalPredatorAmount
                        oPayment.Invoices(1).Unique_Id = ""
                        ' for "falsk" OCR, we are exporting Match_ID in the OCR-file;
                        oPayment.Invoices(1).MATCH_ID = ""
                        oPayment.Invoices(1).StatusCode = "00"
                        ' add freetext
                        If oPayment.Invoices(1).Freetexts.Count = 0 Then
                            oFreetext = oPayment.Invoices(1).Freetexts.Add
                        Else
                            For i = oPayment.Invoices(1).Freetexts.Count To 2 Step -1
                                oPayment.Invoices(1).Freetexts.Remove(i)
                            Next i
                            oFreetext = oPayment.Invoices(1).Freetexts(1)
                        End If
                        oFreetext.Text = "Overf�ring fra Procasso " & oPayment.DATE_Payment

                        ' 20.08.2014 - we have to keep only one invoice !!!
                        For i = oPayment.Invoices.Count To 1 Step -1
                            If oPayment.Invoices(i).Extra1 <> "TILPREDATOR" Or oPayment.Invoices(i).MATCH_Final = False Then
                                oPayment.Invoices.Remove(i)
                            Else
                                oPayment.Invoices(i).Exported = False
                                oPayment.Invoices(i).StatusCode = "00"
                            End If
                        Next i

                        ' Set as NOT exported, so we can export it in the Procasso OCR-file
                        oPayment.Exported = False
                        bFound = True
                        Exit For
                    Else
                        ' added 27.10.2014
                        ' set all other as exported
                        oPayment.Exported = True
                    End If
                Next oPayment
                'If bFound Then
                '    Exit For
                'End If
            Next oBatch
            'If bFound Then
            '    Exit For
            'End If
        Next oBabelFile

        ConectoPrepareTelepayToPredatorFile = True
    End Function
    Private Function PrepareAndWriteBackpaymentFile(ByVal oLocalBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim oLocalBabel As vbBabel.BabelFile
        Dim oLocalBatch As vbBabel.Batch
        Dim oLocalPayment As vbBabel.Payment
        Dim oLocalInvoice As vbBabel.Invoice
        Dim oLocalFreetext As vbBabel.Freetext
        Dim oLocalFilesetup As vbBabel.FileSetup
        Dim oLocalClient As vbBabel.Client
        Dim oLocalAccount As vbBabel.Account
        Dim oNewBabelFiles As vbBabel.BabelFiles
        Dim oNewBabel As vbBabel.BabelFile
        Dim bNewBabelCreated As Boolean = False
        Dim oNewBatch As vbBabel.Batch
        Dim bNewBatchCreated As Boolean = False
        Dim oNewPayment As vbBabel.Payment
        Dim oNewInvoice As vbBabel.Invoice
        Dim oNewFreetext As vbBabel.Freetext
        Dim sAccountReceiver As String = String.Empty
        Dim sMessage As String = String.Empty
        Dim bThisIsAKID As Boolean = False
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oAccount As vbBabel.Account
        Dim sExtension As String
        Dim sFilenameFromSetup As String
        Dim bAllExported As Boolean
        Dim iSeqNoOut As Integer
        Dim sErrFixedText As String

        Dim sFileNameRemittancefile As String
        Dim iFormat_ID As Integer
        Dim iVB_FilenameInNo As Integer
        Dim iVB_FilenameInNoOriginal As Integer
        Dim iFileSetupOutID As Integer
        Dim sCompanyNo As String
        Dim bAtLeastOneBackpayment As Boolean = False
        Dim bFilesetupInFound As Boolean = False
        Dim bFilesetupOutFound As Boolean = False

        Try

            'First find a Profile where the importformat says BabelBank_BackPayment
            If oLocalBabelFiles.Count > 0 Then
                If oLocalBabelFiles.Item(1).VB_ProfileInUse Then
                    For Each oLocalFilesetup In oLocalBabelFiles.Item(1).VB_Profile.FileSetups
                        If oLocalFilesetup.Format_ID = vbBabel.BabelFiles.FileType.BabelBank_BackPayment Then
                            'Ok, we have found a profile with the remittance setup

                            iFileSetupOutID = oLocalFilesetup.FileSetupOut
                            iVB_FilenameInNo = oLocalFilesetup.Index
                            sCompanyNo = oLocalFilesetup.CompanyNo
                            bFilesetupInFound = True
                            Exit For
                        End If
                    Next oLocalFilesetup

                    'Find the filesetup out to pick some information from that profile
                    For Each oLocalFilesetup In oLocalBabelFiles.Item(1).VB_Profile.FileSetups
                        If oLocalFilesetup.FileSetup_ID = iFileSetupOutID Then
                            'Ok, we have found a profile with the remittance setup

                            sFileNameRemittancefile = oLocalFilesetup.FileNameOut1
                            iFormat_ID = iFileSetupOutID 'oLocalFilesetup.FormatOutID1
                            bFilesetupOutFound = True
                            Exit For
                        End If
                    Next oLocalFilesetup

                End If


                oNewBabelFiles = New vbBabel.BabelFiles
                oNewBabel = oNewBabelFiles.Add
                oNewBabel.DATE_Production = DateToString(Now())
                oNewBabel.CreateBatches()
                'oNewBatch = New vbBabel.Batch
                oNewBatch = oNewBabel.Batches.Add()

                For Each oLocalBabel In oLocalBabelFiles
                    If Not bNewBabelCreated Then
                        bNewBabelCreated = True
                        oNewBabel.FilenameIn = oLocalBabel.FilenameIn
                        oNewBabel.Special = oLocalBabel.Special
                        oNewBabel.StatusCode = "00"
                        oNewBabel.VB_FilenameInNo = oLocalBabel.VB_FilenameInNo
                        oNewBabel.VB_FileSetupID = oLocalBabel.VB_FileSetupID
                        oNewBabel.VB_Profile = oLocalBabel.VB_Profile
                        oNewBabel.VB_ProfileInUse = oLocalBabel.VB_ProfileInUse
                        oNewBabel.VB_FilenameInNo = oLocalBabel.VB_FilenameInNo
                        iVB_FilenameInNoOriginal = oLocalBabel.VB_FilenameInNo
                    End If
                    For Each oLocalBatch In oLocalBabel.Batches
                        If Not bNewBatchCreated Then
                            bNewBatchCreated = True
                            oNewBatch.ImportFormat = oLocalBatch.ImportFormat
                            oNewBatch.VB_Profile = oLocalBatch.VB_Profile
                            oNewBatch.VB_ProfileInUse = oLocalBatch.VB_ProfileInUse
                            oNewBatch.StatusCode = "00"
                        End If
                        For Each oLocalPayment In oLocalBatch.Payments
                            If oLocalPayment.MATCH_Matched Then
                                For Each oLocalInvoice In oLocalPayment.Invoices
                                    If oLocalInvoice.MATCH_Final = True Then
                                        If oLocalInvoice.MATCH_Matched Then
                                            'MATCH_ID = RET shows that it is a backpayment
                                            If oLocalInvoice.BackPayment Then
                                                bAtLeastOneBackpayment = True
                                                oNewPayment = oNewBatch.Payments.Add
                                                oNewPayment.VB_Profile = oLocalPayment.VB_Profile
                                                oNewPayment.VB_ProfileInUse = oLocalPayment.VB_ProfileInUse
                                                oNewPayment.VB_ClientNo = oLocalPayment.VB_ClientNo
                                                oNewPayment.ImportFormat = oLocalPayment.ImportFormat
                                                oNewPayment.StatusCode = "00"
                                                oNewPayment.Exported = False
                                                oNewPayment.E_Name = oLocalPayment.E_Name
                                                oNewPayment.E_Adr1 = oLocalPayment.E_Adr1
                                                oNewPayment.E_Adr2 = oLocalPayment.E_Adr2
                                                oNewPayment.E_Adr3 = oLocalPayment.E_Adr3
                                                oNewPayment.E_City = oLocalPayment.E_City
                                                oNewPayment.E_Zip = oLocalPayment.E_Zip
                                                oNewPayment.DATE_Payment = DateToString(DateTime.Now)
                                                If Not EmptyString(oLocalInvoice.BackPaymentAccount) Then
                                                    sAccountReceiver = Trim(oLocalInvoice.BackPaymentAccount)
                                                Else
                                                    sAccountReceiver = Trim(oLocalPayment.E_Account)
                                                End If
                                                oNewPayment.E_Account = sAccountReceiver
                                                oNewPayment.I_Account = oLocalPayment.I_Account
                                                oNewPayment.MON_InvoiceCurrency = oLocalPayment.MON_InvoiceCurrency
                                                oNewInvoice = oNewPayment.Invoices.Add
                                                oNewInvoice.MON_InvoiceAmount = oLocalInvoice.MON_InvoiceAmount
                                                oNewInvoice.ImportFormat = oLocalInvoice.ImportFormat
                                                oNewInvoice.MON_TransferredAmount = 0
                                                oNewInvoice.StatusCode = "00"
                                                sMessage = String.Empty
                                                For Each oLocalFreetext In oLocalInvoice.Freetexts
                                                    If oLocalFreetext.Qualifier = 4 Then
                                                        sMessage = sMessage & Trim(oLocalFreetext.Text)
                                                    End If
                                                Next oLocalFreetext
                                                bThisIsAKID = False
                                                If Left(sMessage, 1) = "*" Then
                                                    If Len(sMessage) < 26 Then
                                                        'TODO - Move this validation to Validate_VismaCollectorsPostingPayment in bb_utils (also validate if cdv10 or 11
                                                        If vbIsNumeric(Mid(sMessage, 2), "0123456789") Then
                                                            bThisIsAKID = True
                                                        End If
                                                    End If
                                                End If
                                                If bThisIsAKID Then
                                                    oNewInvoice.Unique_Id = Mid(sMessage, 2)
                                                    oNewPayment.PayCode = "601"
                                                Else
                                                    For Each oLocalFreetext In oLocalInvoice.Freetexts
                                                        If oLocalFreetext.Qualifier = 4 Then
                                                            oNewFreetext = oNewInvoice.Freetexts.Add()
                                                            oNewFreetext.Text = oLocalFreetext.Text
                                                        End If
                                                    Next oLocalFreetext
                                                    oNewPayment.PayCode = "602"
                                                End If
                                                oNewPayment.MON_InvoiceAmount = oNewInvoice.MON_InvoiceAmount
                                                oNewPayment.VB_FilenameOut_ID = iFormat_ID
                                            End If
                                        End If
                                    End If
                                Next oLocalInvoice
                            End If
                        Next oLocalPayment
                    Next oLocalBatch
                Next oLocalBabel

                oNewBatch.AddInvoiceAmountOnBatch()
                'oNewBabel.

                sFilenameFromSetup = sFileNameRemittancefile
                ' find last part of filename from setup, use if specified
                If InStr(sFileNameRemittancefile, ".") > 0 Then
                    sExtension = Mid(sFileNameRemittancefile, InStr(sFileNameRemittancefile, "."))
                    sFilenameFromSetup = Replace(sFilenameFromSetup, sExtension, "")
                Else
                    sExtension = ".Dat"
                End If

                For Each oLocalClient In oLocalFilesetup.Clients
                    For Each oLocalAccount In oLocalClient.Accounts
                        'If ola Then
                    Next oLocalAccount
                Next oLocalClient

                'We should maybe set the iFormat_ID, but then it must also be set in oPayment
                'So far both of them are 0 so everything works well.
                If bAtLeastOneBackpayment Then
                    WriteTelepay2File(oNewBabelFiles, False, sFileNameRemittancefile, iFormat_ID, "", String.Empty, iVB_FilenameInNoOriginal, sCompanyNo, String.Empty, "", False, False, 0, iSeqNoOut, "", "RETURNPAYMENTS", vbBabel.BabelFiles.Bank.Nordea_NO)
                End If

                ' Need to traverse through each I_Account, and create a file pr account
                'For Each oFilesetup In oLocalBabel.VB_Profile.FileSetups
                '    If oFilesetup.FileSetup_ID = iVB_FilenameInNo Then
                '        For Each oClient In oFilesetup.Clients

                '            For Each oAccount In oClient.Accounts
                '                If Not EmptyString(oAccount.SWIFTAddress) Then
                '                    sFileNameRemittancefile = sFilenameFromSetup & Trim(oAccount.SWIFTAddress) & "_" & oAccount.Account & sExtension
                '                    '21.08.2014 -Added oClient.ClientNo as parameter
                '                    WriteTelepay2File(oNewBabelFiles, True, sFileNameRemittancefile, iFormat_ID, oAccount.Account, String.Empty, iVB_FilenameInNo, sCompanyNo, String.Empty, "", False, False, oClient.SeqNoTotal, iSeqNoOut, oClient.ClientNo, "RETURNPAYMENTS", vbBabel.BabelFiles.Bank.Nordea_NO)
                '                    'WriteTelepay2File(oNewBabelFiles, True, sFileNameTelepay, iFormat_ID, oAccount.Account, String.Empty, iVB_FilenameInNo, sCompanyNo, String.Empty, "", False, False, oClient.SeqNoTotal, iSeqNoOut, String.Empty, "RETURNPAYMENTS", BabelFiles.Bank.Nordea_NO)
                '                    oClient.SeqNoTotal = iSeqNoOut - 1

                '                    oClient.Status = vbBabel.Profile.CollectionStatus.Changed
                '                    oFilesetup.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
                '                    oLocalBabel.VB_Profile.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
                '                    oLocalBabel.VB_Profile.Save(1)

                '                End If
                '            Next oAccount
                '        Next oClient
                '        Exit For ' to prevent same account be exported more than once
                '    End If
                'Next oFilesetup
                bAllExported = True
                ' Check if all payments are exported;
                '21.08.2014 - New code
                For Each oNewBabel In oNewBabelFiles
                    For Each oNewBatch In oNewBabel.Batches
                        For Each oNewPayment In oNewBatch.Payments
                            '21.08.2014 - Removed this, because oNew..... consists of only backpayments
                            'For Each oNewInvoice In oNewPayment.Invoices
                            'If oNewInvoice.MATCH_'ID = "RET" Then
                            If oNewPayment.Exported = False Then
                                bAllExported = False
                                sErrFixedText = "Function: WriteVismaCollectors_PrepareAndWriteTelepayFile" & vbCrLf
                                sErrFixedText = "Betalers navn: " & oNewPayment.E_Name & vbCrLf
                                sErrFixedText = sErrFixedText & "Mottakers konto: " & oNewPayment.E_Account & vbCrLf
                                sErrFixedText = sErrFixedText & "Bel�p: " & oNewPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oNewPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf
                                sErrFixedText = sErrFixedText & "Belastningskonto: " & oNewPayment.I_Account
                            End If
                            'End If
                            'Next oNewInvoice
                        Next oNewPayment
                    Next oNewBatch
                Next oNewBabel

                'Old code
                'For Each oLocalBabel In oLocalBabelFiles
                '    For Each oLocalBatch In oLocalBabel.Batches
                '        For Each oLocalPayment In oLocalBatch.Payments
                '            For Each oLocalInvoice In oLocalPayment.Invoices
                '                If oLocalInvoice.MATCH_ID = "RET" Then
                '                    If oLocalPayment.Exported = False Then
                '                        bAllExported = False
                '                    End If
                '                End If
                '            Next oLocalInvoice
                '        Next oLocalPayment
                '    Next oLocalBatch
                'Next oLocalBabel
                If Not bAllExported Then
                    '12007: Minst en returbetaling ble ikke eksportert

                    Throw New Exception(LRS(12007) & vbCrLf & vbCrLf & sErrFixedText)
                    'MsgBox(LRS(12007) & vbCrLf & vbCrLf & sErrFixedText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "BabelBank")
                    PrepareAndWriteBackpaymentFile = False
                End If

            End If

            PrepareAndWriteBackpaymentFile = True

        Catch ex As Exception

            If oNewBatch Is Nothing Then
                oNewBatch = Nothing
            End If
            If oNewBabel Is Nothing Then
                oNewBabel = Nothing
            End If
            If oNewBabelFiles Is Nothing Then
                oNewBabelFiles = Nothing
            End If


            Throw New Exception("Function: WriteVismaCollectors_PrepareAndWriteTeleoayFile" & vbCrLf & ex.Message)

        End Try

        If Not oNewFreetext Is Nothing Then
            oNewFreetext = Nothing
        End If
        If Not oNewInvoice Is Nothing Then
            oNewInvoice = Nothing
        End If
        If Not oNewPayment Is Nothing Then
            oNewPayment = Nothing
        End If
        If Not oNewBatch Is Nothing Then
            oNewBatch = Nothing
        End If
        If Not oNewBabel Is Nothing Then
            oNewBabel = Nothing
        End If
        If Not oNewBabelFiles Is Nothing Then
            oNewBabelFiles = Nothing
        End If

    End Function
    End Class
