﻿Option Strict Off
Option Explicit On

Module WriteSwift
    'XokNET - 16.10.2014 - Added new module
    Private Enum SWIFTCountryCode
        Unknown = 0
        Denmark = 1 'HANDDKKK
        Finland = 2 'HANDFIHH
        Germany = 3 'HANDDEFF
        Great_Britain = 4 'HANDGB22
        Hong_Kong = 5 'HANDHKKK
        Luxemburg = 6 'HANDLULL
        Netherlands = 7 'HANDNL2A
        Norway = 8 'HANDNOKK
        Poland = 9 'HANDPLPW
        Singapore = 10 'HANDSGSG
        Sweden = 11 'HANDSESS
        USA = 12 'HANDUS33
        'XokNET 31.02.2012  Added next line
        France = 13 'HANDFRPP
    End Enum
    Private Enum InstructionCode
        No_Code = 0
        URGP = 1
        INTC = 2
        CORT = 3
        OTHR = 4
        EKON = 5
        CHQB = 6
    End Enum

    Function WriteMT101File(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal iFilenameInNo As Integer, ByVal sCompanyNo As String, ByVal sBranch As String, ByVal sVersion As String, ByVal eBank As BabelFiles.Bank, ByVal sSpecial As String) As Boolean

        'Copied from Handelsbank Global On-Line
        'So far only adapted for Danske Bank, domestic Finland.
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim i As Integer
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String
        Dim bSequenceAWritten As Boolean
        Dim eCountryCode As SWIFTCountryCode
        Dim eInstructionCode As InstructionCode
        Dim bDebitAccountInHandelsbanken As Boolean
        Dim sLastUsedExecutionDate As String
        Dim sToday As String
        Dim lCounter As Long, lCounter2 As Long
        Dim sAmount As String
        Dim sInfoToWriteInTag36 As String, sTemp As String
        Dim sErrFixedText As String
        Dim sKortartKode As String
        Dim sBetalerIdent As String
        Dim sKreditorNo As String
        Dim sErrorString As String
        Dim bMessageIsMandatory As Boolean
        Dim sFile_ID As String
        Dim bThisIsAPostBankAccount As Boolean
        Dim bNormalPayment As Boolean
        Dim iCharactersPerLine As Integer
        Dim sClearingCode As String
        Dim sOldPayType As String
        Dim bx As Boolean
        Dim bFirstSequenceA As Boolean
        'Dim sErrorString As String

        Try

            ' create an outputfile
            'On Error GoTo errCreateOutputFile
            bAppendFile = False

            If sSpecial = "ULEFOS_NV" Then
                MsgBox("In the start of the function; WriteMT101File")
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bSequenceAWritten = False
            sToday = Format(Now(), "yyyyMMdd")
            lCounter = 0
            bFoundClient = False
            sOldPayType = vbNullString
            bFirstSequenceA = True

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If oPayment.I_Account <> sOldAccount Then
                                        bFoundClient = False

                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Or Trim$(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                    sSwift = Trim$(oaccount.SWIFTAddress)
                                                    sOldAccount = oPayment.I_Account
                                                    If sSpecial = "ULEFOS_NV" Then
                                                        MsgBox("Found the client, SWIFTAdress: " & oaccount.SWIFTAddress)
                                                    End If
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next
                                            If bFoundClient Then
                                                'Get the companyno from a property passed from BabelExport
                                                If EmptyString(sCompanyNo) Then
                                                    Err.Raise(1, "WriteMT101File", "No Customer identity in Handelsbanken stated")
                                                End If
                                                Exit For
                                            End If


                                        Next
                                        bSequenceAWritten = False
                                    End If

                                    If Not bFoundClient Then
                                        Err.Raise(1, "WriteMT101File", "The debitaccount " & oPayment.I_Account & " is unknown to BabelBank. You must enter information about the account in the Client setup.")
                                    End If

                                    If oPayment.DATE_Payment = "19900101" Then
                                        oPayment.DATE_Payment = sToday
                                    End If

                                    If oPayment.DATE_Payment < sToday Then
                                        If sToday = sLastUsedExecutionDate Then
                                            'OK, no new sequence A
                                        Else
                                            'Change in date, write a new sequence A
                                            sLastUsedExecutionDate = sToday
                                            bSequenceAWritten = False
                                        End If
                                    ElseIf oPayment.DATE_Payment = sLastUsedExecutionDate Then
                                        'OK, no new sequence A
                                    Else
                                        'Change in date, write a new sequence A
                                        sLastUsedExecutionDate = oPayment.DATE_Payment
                                        bSequenceAWritten = False
                                    End If

                                    oPayment.BANK_I_SWIFTCode = sSwift
                                    'Create en fixed error text with info from the payment
                                    sErrFixedText = vbNullString
                                    sErrFixedText = "Beneficiary's name: " & oPayment.E_Name & vbCrLf
                                    sErrFixedText = sErrFixedText & "Beneficiary's account: " & oPayment.E_Account & vbCrLf
                                    sErrFixedText = sErrFixedText & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf
                                    sErrFixedText = sErrFixedText & "Payer's account: " & oPayment.I_Account

                                    bDebitAccountInHandelsbanken = True

                                    If Left$(sSwift, 4) <> "HAND" Then
                                        'Err.Raise 1, "WriteMT101File", "BabelBank is not yet implemented for using accounts in other banks. Contact Your dealer."
                                        'bAccountInOtherBankExists = True
                                        bDebitAccountInHandelsbanken = False
                                    End If

                                    If Not IsPaymentDomestic(oPayment, sSwift) Then
                                        If sSpecial = "ULEFOS_NV" Then
                                            MsgBox("The payment is set as international in IsPaymentDomestic")
                                        End If
                                        oPayment.PayType = "I"
                                    Else
                                        '10.03.2010 - New code to take care of salarypayments for Ulefos
                                        If oPayment.PayType <> "S" Then
                                            oPayment.PayType = "D"
                                        End If
                                    End If

                                    If Len(Trim$(sSwift)) = 8 Or Len(Trim$(sSwift)) = 11 Then
                                        If sSpecial = "ULEFOS_NV" Then
                                            MsgBox("Testing for the SWIFT-code: " & sSwift)
                                        End If
                                        Select Case UCase(Mid$(sSwift, 5, 2))
                                            Case "DK"
                                                eCountryCode = SWIFTCountryCode.Denmark
                                            Case "FI"
                                                eCountryCode = SWIFTCountryCode.Finland
                                                'XokNET - 31.01.2012 - Added next case
                                            Case "FR"
                                                eCountryCode = SWIFTCountryCode.France
                                            Case "DE"
                                                eCountryCode = SWIFTCountryCode.Germany
                                            Case "GB"
                                                eCountryCode = SWIFTCountryCode.Great_Britain
                                            Case "HK"
                                                eCountryCode = SWIFTCountryCode.Hong_Kong
                                            Case "LU"
                                                eCountryCode = SWIFTCountryCode.Luxemburg
                                            Case "NL"
                                                eCountryCode = SWIFTCountryCode.Netherlands
                                            Case "NO"
                                                eCountryCode = SWIFTCountryCode.Norway
                                            Case "PL"
                                                eCountryCode = SWIFTCountryCode.Poland
                                            Case "SG"
                                                eCountryCode = SWIFTCountryCode.Singapore
                                            Case "SE"
                                                eCountryCode = SWIFTCountryCode.Sweden
                                            Case "US"
                                                eCountryCode = SWIFTCountryCode.USA

                                            Case Else
                                                Err.Raise(1000, "WriteMT101File", "Wrong Swift code in Handelsbanken stated." & vbCrLf & vbCrLf & _
                                                   "Swiftcode:" & sSwift & vbCrLf _
                                                   & "Debit bankgiro: " & oPayment.I_Account & vbCrLf _
                                                   & sErrFixedText)

                                        End Select
                                    Else
                                        Err.Raise(1000, "WriteMT101File", "Wrong Swift code in Handelsbanken stated." & vbCrLf & vbCrLf & _
                                           "Swiftcode:" & sSwift & vbCrLf _
                                           & "Debit bankgiro: " & oPayment.I_Account & vbCrLf _
                                           & sErrFixedText)
                                    End If

                                    If sOldPayType <> oPayment.PayType Then
                                        bSequenceAWritten = False
                                        sOldPayType = oPayment.PayType
                                    End If

                                    If Not bSequenceAWritten Then
                                        If Not EmptyString(oBabel.File_id) Then
                                            sFile_ID = Right$(Trim$(oBabel.File_id) & Trim$(Str(lCounter)) & Left$(Format(Now(), "SSMMHHDDMMYY"), 16), 16)
                                        Else
                                            sFile_ID = Right$(Format(Now(), "YYMMDDHHMMSS") & Trim$(Str(lCounter)), 16)
                                        End If
                                        sLine = WriteSequenceARecord(oFile, oaccount, oPayment, eCountryCode, sCompanyNo, sSwift, sLastUsedExecutionDate, lCounter, sErrFixedText, Right(sFile_ID, 16), bFirstSequenceA, eBank)
                                        bSequenceAWritten = True
                                        bFirstSequenceA = False
                                    End If

                                    lCounter = lCounter + 1

                                    'For Financial payments, value CORT in 23E, to Svenska Handelsbanken Denmark the Beneficary account in
                                    'Tag 59 must be an IBAN
                                    'To other countries beneficary account in Handelsbaken has to be a BBAN (traditional account no)

                                    If oPayment.PayType = "I" Then
                                        'Create 1 sequence B section per invoice
                                        'For Each oInvoice In oPayment.Invoices

                                        If sSpecial = "ULEFOS_NV" Then
                                            MsgBox("Writing an international payment")
                                        End If
                                        If EmptyString(oPayment.REF_Own) Then
                                            oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                        Else
                                            oFile.WriteLine(":21:" & Right$(Trim$(oPayment.REF_Own), 16)) 'Transactionref - 16x
                                        End If

                                        'Check if we have ERA or FRW
                                        sInfoToWriteInTag36 = vbNullString
                                        If oPayment.ERA_ExchRateAgreed <> 0 Then
                                            If Not EmptyString(oPayment.ERA_DealMadeWith) Then
                                                If bDebitAccountInHandelsbanken Then
                                                    Select Case eCountryCode
                                                        Case BabelFiles.CountryName.Denmark, BabelFiles.CountryName.Finland, BabelFiles.CountryName.Norway, BabelFiles.CountryName.Germany, BabelFiles.CountryName.Great_Britain, BabelFiles.CountryName.Netherlands, BabelFiles.CountryName.Poland, BabelFiles.CountryName.Hong_Kong, BabelFiles.CountryName.Singapore, BabelFiles.CountryName.USA
                                                            oFile.WriteLine(":21F:" & Left$(Trim$(oPayment.ERA_DealMadeWith), 16)) 'F/X Deal reference - 16x - not in use
                                                            sInfoToWriteInTag36 = ":36:" & SWIFTConvertToCorrectAmount(oPayment.ERA_ExchRateAgreed)
                                                        Case Else
                                                            'Not allowed
                                                    End Select
                                                End If
                                            End If
                                        ElseIf oPayment.FRW_ForwardContractRate <> 0 Then
                                            If Not EmptyString(oPayment.FRW_ForwardContractNo) Then
                                                If bDebitAccountInHandelsbanken Then
                                                    Select Case eCountryCode
                                                        Case BabelFiles.CountryName.Denmark, BabelFiles.CountryName.Finland, BabelFiles.CountryName.Norway, BabelFiles.CountryName.Germany, BabelFiles.CountryName.Great_Britain, BabelFiles.CountryName.Netherlands, BabelFiles.CountryName.Poland, BabelFiles.CountryName.Hong_Kong, BabelFiles.CountryName.Singapore, BabelFiles.CountryName.USA
                                                            oFile.WriteLine(":21F:" & Left$(Trim$(oPayment.FRW_ForwardContractNo), 16)) 'F/X Deal reference - 16x - not in use
                                                            sInfoToWriteInTag36 = ":36:" & SWIFTConvertToCorrectAmount(oPayment.FRW_ForwardContractRate)
                                                        Case Else
                                                            'Not allowed
                                                    End Select
                                                End If
                                            End If
                                        End If

                                        'Instruction code - 4a[/30x]
                                        '• CHQB = cross border cheque payment (only Handelsbanken SE, GB, and LU)
                                        '• URGP = urgent
                                        '• INTC = Intra Company payment N.B. Beneficiary account number in Tag 59 is optional
                                        '• CORT = Financial payment N.B Beneficiary account number in Tag 59 is optional.
                                        '• OTHR/EKON = Economy payment (only Handelsbanken DK)

                                        If oPayment.ToOwnAccount Then
                                            oFile.WriteLine(":23E:INTC")
                                            eInstructionCode = InstructionCode.INTC
                                        ElseIf oPayment.Priority Then
                                            oFile.WriteLine(":23E:URGP")
                                            eInstructionCode = InstructionCode.URGP
                                        ElseIf oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                            Select Case eCountryCode
                                                Case BabelFiles.CountryName.Great_Britain, BabelFiles.CountryName.Sweden, BabelFiles.CountryName.Luxemburg
                                                    oFile.WriteLine(":23E:CHQB")
                                                    eInstructionCode = InstructionCode.CHQB
                                                Case Else
                                                    oFile.WriteLine(":23E:CORT")
                                                    eInstructionCode = InstructionCode.CORT
                                            End Select
                                        Else
                                            'No need to state a paymenttype
                                            'oFile.WriteLine ":23E:CORT"
                                        End If

                                        sLine = ":32B:"
                                        If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                            Err.Raise(1000, "WriteMT101File", "No currencycode stated. The code must be 3 positions.." & vbCrLf & vbCrLf & _
                                               "Currencycode:" & sSwift & vbCrLf _
                                               & sErrFixedText)
                                        ElseIf Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                            Err.Raise(1000, "WriteMT101File", "Wrong currencycode stated. The code must be 3 positions.." & vbCrLf & vbCrLf & _
                                               "Currencycode:" & sSwift & vbCrLf _
                                               & sErrFixedText)
                                        Else
                                            sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                        End If
                                        sAmount = SWIFTConvertToCorrectAmount(oPayment.MON_InvoiceAmount)
                                        sLine = sLine & sAmount
                                        oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount

                                        'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                        'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                        'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B

                                        ':56A: Intermediary
                                        If bDebitAccountInHandelsbanken Then
                                            If Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Then
                                                If eInstructionCode = InstructionCode.INTC Or eInstructionCode = InstructionCode.CORT Then
                                                    oFile.WriteLine(":56A:" & Trim$(oPayment.BANK_SWIFTCodeCorrBank))
                                                End If
                                            End If
                                        Else
                                            'WARNING! Other banks may also allow the use of intermediary, according to agreement
                                        End If

                                        ':57A: Account With Institution Identification of beneficiary's bank
                                        ':57C:
                                        If Not eInstructionCode = InstructionCode.CHQB Then
                                            If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                                oPayment.BANK_SWIFTCode = Trim$(oPayment.BANK_SWIFTCode)
                                                If Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11 Then
                                                    If Not EmptyString(oPayment.BANK_BranchNo) And oPayment.BANK_BranchType > BabelFiles.BankBranchType.SWIFT Then
                                                        sTemp = vbNullString
                                                        Select Case oPayment.BANK_BranchType
                                                            Case BabelFiles.BankBranchType.Bankleitzahl
                                                                sTemp = "BL"
                                                            Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                                sTemp = "AT"
                                                            Case BabelFiles.BankBranchType.Chips
                                                                sTemp = "FW"
                                                            Case BabelFiles.BankBranchType.Fedwire
                                                                sTemp = "FW"
                                                                'Casebabelfiles.bankbranchtype.MEPS
                                                                '    Not used
                                                            Case BabelFiles.BankBranchType.SortCode
                                                                sTemp = "SC"
                                                            Case BabelFiles.BankBranchType.US_ABA
                                                                sTemp = "FW"
                                                        End Select
                                                        oFile.WriteLine(":57A://" & sTemp & Trim$(oPayment.BANK_BranchNo))
                                                        oFile.WriteLine(oPayment.BANK_SWIFTCode)
                                                    Else
                                                        oFile.WriteLine(":57A:" & oPayment.BANK_SWIFTCode)
                                                    End If
                                                Else
                                                    Err.Raise(1000, "WriteMT101File", "Wrong SWIFT-code detected. The code must be either 8 or 11 positions." & vbCrLf & vbCrLf & _
                                                       "SWIFT-code:" & oPayment.BANK_SWIFTCode & vbCrLf _
                                                       & sErrFixedText)
                                                End If
                                            Else
                                                If Not EmptyString(oPayment.BANK_BranchNo) And oPayment.BANK_BranchType > BabelFiles.BankBranchType.SWIFT Then
                                                    sTemp = vbNullString
                                                    Select Case oPayment.BANK_BranchType
                                                        Case BabelFiles.BankBranchType.Bankleitzahl
                                                            sTemp = "BL"
                                                        Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                            sTemp = "AT"
                                                        Case BabelFiles.BankBranchType.Chips
                                                            sTemp = "FW"
                                                        Case BabelFiles.BankBranchType.Fedwire
                                                            sTemp = "FW"
                                                            'Casebabelfiles.bankbranchtype.MEPS
                                                            '    Not used
                                                        Case BabelFiles.BankBranchType.SortCode
                                                            sTemp = "SC"
                                                        Case BabelFiles.BankBranchType.US_ABA
                                                            sTemp = "FW"
                                                    End Select
                                                    oFile.WriteLine(":57C://" & sTemp & Trim$(oPayment.BANK_BranchNo))
                                                Else
                                                    Err.Raise(1000, "WriteMT101File", "Neither a SWIFT-code nor a branchcode is stated. You must at least state one of the codes." & vbCrLf & vbCrLf & _
                                                       "SWIFT-code:" & oPayment.BANK_SWIFTCode & vbCrLf _
                                                       & sErrFixedText)
                                                End If
                                            End If
                                        End If

                                        'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                        sTemp = vbNullString
                                        If EmptyString(oPayment.E_Account) Then
                                            If eInstructionCode = InstructionCode.CHQB Then
                                                'OK, without ac.no.
                                            ElseIf eCountryCode = SWIFTCountryCode.Sweden And (eInstructionCode = InstructionCode.CORT Or eInstructionCode = InstructionCode.INTC) Then
                                                'OK, without ac.no.
                                            Else
                                                Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                   "Accountnumber:" & oPayment.E_Account & vbCrLf _
                                                   & sErrFixedText)
                                            End If
                                        End If
                                        sLine = vbNullString
                                        If Not EmptyString(oPayment.E_Account) Then
                                            sLine = ":59:/" & Trim$(oPayment.E_Account)
                                        End If
                                        If Not EmptyString(oPayment.E_Name) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left$(Trim$(oPayment.E_Name), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                            End If
                                        End If
                                        If Not EmptyString(oPayment.E_Adr1) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left$(Trim$(oPayment.E_Adr1), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                            End If
                                        End If
                                        If Not EmptyString(oPayment.E_Adr2) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left$(Trim$(oPayment.E_Adr2), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                            End If
                                        End If
                                        If Not EmptyString(oPayment.E_Zip) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                            End If
                                        ElseIf Not EmptyString(oPayment.E_City) Then
                                            If EmptyString(sLine) Then
                                                sLine = ":59:" & Left$(Trim$(oPayment.E_City), 35)
                                            Else
                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                            End If
                                        End If
                                        oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.

                                        '-------- fetch content of each payment ---------
                                        ' Total up freetext from all invoices;
                                        sFreetext = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreetext In oInvoice.Freetexts
                                                sFreetext = sFreetext & Trim$(oFreetext.Text)
                                            Next
                                        Next
                                        If Len(sFreetext) > 140 Then
                                            If oBabel.VB_ProfileInUse Then
                                                sFreetext = DiminishFreetext(sFreetext, 140, oPayment.E_Name & vbCrLf & oPayment.MON_InvoiceAmount)
                                            Else
                                                sFreetext = Left$(sFreetext, 140)
                                            End If
                                        End If
                                        lCounter2 = 1
                                        For lCounter2 = 1 To 4
                                            If Not EmptyString(sFreetext) Then
                                                If lCounter2 = 1 Then
                                                    oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                Else
                                                    oFile.WriteLine(Left$(sFreetext, 35))
                                                End If
                                                If Len(sFreetext) > 35 Then
                                                    sFreetext = Mid$(sFreetext, 36)
                                                Else
                                                    sFreetext = vbNullString
                                                    Exit For
                                                End If
                                            Else
                                                Exit For
                                            End If
                                        Next lCounter2

                                        'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                        Select Case Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)
                                            Case "LU" 'Luxemburg
                                                'Regulatory reporting is mandatory
                                                sLine = "/" & bbGetStateBankCode(oPayment.Invoices.Item(1).STATEBANK_Code, "LU") & "/" & Trim$(oPayment.E_CountryCode)
                                                If Not EmptyString(oPayment.Invoices.Item(1).STATEBANK_Text) Then
                                                    sLine = sLine & Left$(Trim$(oPayment.Invoices.Item(1).STATEBANK_Text), 19)
                                                End If

                                            Case "NO" 'Norge
                                                'Regulatory reporting is mandatory
                                                sLine = "/" & bbGetStateBankCode(oPayment.Invoices.Item(1).STATEBANK_Code, "NO") & "/" & Trim$(oPayment.E_CountryCode)
                                                If Not EmptyString(oPayment.Invoices.Item(1).STATEBANK_Text) Then
                                                    sLine = sLine & Left$(Trim$(oPayment.Invoices.Item(1).STATEBANK_Text), 19)
                                                End If

                                            Case "SE" 'Sverige
                                                'Regulatory reporting is mandatory
                                                sLine = "/" & bbGetStateBankCode(oPayment.Invoices.Item(1).STATEBANK_Code, "SE") & "/" & Trim$(oPayment.E_CountryCode)
                                                If Not EmptyString(oPayment.Invoices.Item(1).STATEBANK_Text) Then
                                                    sLine = sLine & Left$(Trim$(oPayment.Invoices.Item(1).STATEBANK_Text), 19)
                                                End If

                                            Case Else
                                                'Regulatory reporting is not mandatory, just state the countrycode
                                                sLine = "//" & Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)

                                        End Select
                                        oFile.WriteLine(":77B:" & sLine)

                                        ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                        'Not in use

                                        'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                        'SHA = Each party pays its costs. This is the normal case
                                        'BEN = Beneficiary pays all costs
                                        'OUR = Remitter pays all costs
                                        'N.B. for Handelsbanken USA always treated As OUR

                                        If eCountryCode = SWIFTCountryCode.USA Then
                                            oFile.WriteLine(":71A:OUR")
                                        Else
                                            If oPayment.MON_ChargeMeDomestic Then
                                                If oPayment.MON_ChargeMeAbroad Then
                                                    oFile.WriteLine(":71A:OUR")
                                                Else
                                                    oFile.WriteLine(":71A:SHA")
                                                End If
                                            Else
                                                If oPayment.MON_ChargeMeAbroad Then
                                                    'Not possible but set default
                                                    oFile.WriteLine(":71A:SHA")
                                                Else
                                                    oFile.WriteLine(":71A:BEN")
                                                End If
                                            End If
                                        End If

                                        ':25A: Charges Account /34x Not used
                                        'Not in use

                                        ':36: Exchange Rate /12d Not used
                                        If Not EmptyString(sInfoToWriteInTag36) Then
                                            oFile.WriteLine(sInfoToWriteInTag36)
                                        End If


                                        'Next oInvoice

                                    Else

                                        Select Case eCountryCode

                                            Case SWIFTCountryCode.Denmark
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    If oPayment.PayType = "S" Then
                                                        oFile.WriteLine(":23E:SALY") 'Salary
                                                    Else
                                                        'Local non-urgent. Don't state a code
                                                    End If

                                                    'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume DKK
                                                        sLine = sLine & "DKK"
                                                    ElseIf Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected. The code must be 3 positions." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)

                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    'oFile.WriteLine ':57A: Account With Institution Identification of beneficiary's bank

                                                    'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                                    sTemp = vbNullString

                                                    '11.04.2008 - Don't allow IBAN
                                                    oPayment.E_Account = Trim$(oPayment.E_Account)
                                                    If ValidateIBAN(oPayment.E_Account, False) Then
                                                        'IBAN format: DKkk BBBB CCCC CCCC CC
                                                        'B = bank No., C = account No.
                                                        oPayment.E_Account = Mid$(oPayment.E_Account, 5)
                                                    End If

                                                    If Len(Trim$(oPayment.E_Account)) > 0 Then
                                                        'For some types there are no Betaleridentifikation
                                                        If Len(oInvoice.Unique_Id) > 2 Then
                                                            sTemp = Left$(oInvoice.Unique_Id, 2) & "-" & Mid$(oInvoice.Unique_Id, 3) & "-" & sTemp
                                                        ElseIf Len(oInvoice.Unique_Id) > 0 Then
                                                            sTemp = Left$(oInvoice.Unique_Id, 2) & "-" & sTemp
                                                        Else
                                                            sTemp = vbNullString
                                                        End If
                                                    Else
                                                        If Len(oInvoice.Unique_Id) > 0 Then
                                                            sTemp = Left$(oInvoice.Unique_Id, 2) & "-" & Mid$(oInvoice.Unique_Id, 3)
                                                        Else
                                                            sTemp = vbNullString
                                                        End If
                                                    End If

                                                    bMessageIsMandatory = False
                                                    sFreetext = vbNullString

                                                    If oPayment.PayType = "S" Then
                                                        'Account payment
                                                        If Not EmptyString(oPayment.E_Account) Then
                                                            oFile.WriteLine(":59:/" & Trim(oPayment.E_Account))
                                                        Else
                                                            Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                               "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                               & sErrFixedText)
                                                        End If
                                                        oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                        bMessageIsMandatory = False
                                                        sFreetext = vbNullString
                                                    ElseIf Len(sTemp) = 0 Then
                                                        'Account payment
                                                        If Not EmptyString(oPayment.E_Account) Then
                                                            oFile.WriteLine(":59:/" & Trim(oPayment.E_Account))
                                                        Else
                                                            Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                               "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                               & sErrFixedText)
                                                        End If
                                                        oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                        bMessageIsMandatory = False
                                                        sFreetext = vbNullString
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                        Next oFreetext
                                                    Else
                                                        'FI Kort
                                                        If Not ValidateFI(sTemp, sKortartKode, sBetalerIdent, sKreditorNo, BabelFiles.FileType.HB_GlobalOnline_MT101, sErrorString, True) Then
                                                            'May add some errorinfo here
                                                            'bx = AddToErrorArray(UCase(sVariableName), sErrorString, "1")
                                                        End If
                                                        Select Case sKortartKode
                                                            Case "01"
                                                                If Not EmptyString(oPayment.E_Account) Then
                                                                    oFile.WriteLine(":59:/GIRO" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                                       "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                                       & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = vbNullString
                                                                For Each oFreetext In oInvoice.Freetexts
                                                                    sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                                Next oFreetext
                                                            Case "04", "15"
                                                                If Not EmptyString(oPayment.E_Account) Then
                                                                    oFile.WriteLine(":59:/GIRO" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                                       "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                                       & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = sKortartKode & sBetalerIdent
                                                            Case "71", "75"
                                                                If Not EmptyString(oPayment.E_Account) Then
                                                                    oFile.WriteLine(":59:/FI" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                                       "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                                       & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = sKortartKode & sBetalerIdent
                                                            Case "73"
                                                                If Not EmptyString(oPayment.E_Account) Then
                                                                    oFile.WriteLine(":59:/FI" & Trim(oPayment.E_Account))
                                                                Else
                                                                    Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                                       "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                                       & sErrFixedText)
                                                                End If
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                sFreetext = vbNullString
                                                                For Each oFreetext In oInvoice.Freetexts
                                                                    sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                                Next oFreetext


                                                        End Select
                                                    End If

                                                    lCounter2 = 1

                                                    If Len(sFreetext) > 140 Then
                                                        If oBabel.VB_ProfileInUse Then
                                                            sFreetext = DiminishFreetext(sFreetext, 140, oPayment.E_Name & vbCrLf & oPayment.MON_InvoiceAmount)
                                                        Else
                                                            sFreetext = Left$(sFreetext, 140)
                                                        End If
                                                    End If

                                                    For lCounter2 = 1 To 4
                                                        If Not EmptyString(sFreetext) Then
                                                            If lCounter2 = 1 Then
                                                                oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                            Else
                                                                oFile.WriteLine(Left$(sFreetext, 35))
                                                            End If
                                                            If Len(sFreetext) > 35 Then
                                                                sFreetext = Mid$(sFreetext, 36)
                                                            Else
                                                                sFreetext = vbNullString
                                                                Exit For
                                                            End If
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case SWIFTCountryCode.Finland
                                                'Create 1 sequence B section per invoice
                                                If sSpecial = "ULEFOS_NV" Then
                                                    MsgBox("Writing a finnish payment")
                                                End If


                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use
                                                    If oPayment.Priority Then
                                                        oFile.WriteLine(":23E:URGP")
                                                    Else
                                                        oFile.WriteLine(":23E:OTHR/DMST")  'Instruction code - 4a[/30x] - not in use
                                                    End If
                                                    '10.03.2010 - New code for Ulefos regarding salary
                                                    If oPayment.PayType = "S" And oPayment.PayCode = "203" Then 'Salary
                                                        oFile.WriteLine(":23E:OTHR/SALY")
                                                    End If
                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected. The code must be 3 positions." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteMT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    '                                        If Len(Trim$(oInvoice.MON_InvoiceAmount)) = 1 Then
                                                    '                                            sAmount = "0,0" & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        ElseIf Len(Trim$(oInvoice.MON_InvoiceAmount)) = 2 Then
                                                    '                                            sAmount = "0," & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        Else
                                                    '                                            sAmount = Trim$(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                            sAmount = Left$(sAmount, Len(sAmount) - 2) & "," & Right$(sAmount, 2)
                                                    '                                        End If
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    'oFile.WriteLine ':57A: Account With Institution Identification of beneficiary's bank
                                                    sLine = ""
                                                    If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                                        If Not EmptyString(oPayment.BANK_BranchNo) And oPayment.BANK_BranchType <> BabelFiles.BankBranchType.NoBranchType Then

                                                            Select Case oPayment.BANK_BranchType
                                                                Case BabelFiles.BankBranchType.Fedwire
                                                                    sLine = ":57A://FW" & oPayment.BANK_BranchNo
                                                                Case BabelFiles.BankBranchType.SortCode
                                                                    sLine = ":57A://SC" & oPayment.BANK_BranchNo
                                                                Case BabelFiles.BankBranchType.Bankleitzahl
                                                                    sLine = ":57A://BL" & oPayment.BANK_BranchNo
                                                                Case BabelFiles.BankBranchType.Chips
                                                                    sLine = ":57A://CH" & oPayment.BANK_BranchNo
                                                                Case BabelFiles.BankBranchType.CC
                                                                    sLine = ":57A://CC" & oPayment.BANK_BranchNo
                                                                Case Else
                                                                    sLine = ":57A:"
                                                            End Select
                                                            sLine = sLine & vbCrLf & Trim(oPayment.BANK_SWIFTCode)
                                                        Else
                                                            sLine = ":57A:" & Trim(oPayment.BANK_SWIFTCode)
                                                        End If
                                                        If Not EmptyString(oPayment.BANK_Name) Then
                                                            sLine = sLine & vbCrLf & Trim(oPayment.BANK_Name)
                                                            '                                                If Not EmptyString(oPayment.BANK_Adr1) Then
                                                            '                                                    sLine = sLine & vbCrLf & Trim(oPayment.BANK_Adr1)
                                                            '                                                End If
                                                            '                                                If Not EmptyString(oPayment.BANK_Adr2) Then
                                                            '                                                    sLine = sLine & vbCrLf & Trim(oPayment.BANK_Adr2)
                                                            '                                                End If
                                                            '                                                If Not EmptyString(oPayment.BANK_Adr3) Then
                                                            '                                                    sLine = sLine & vbCrLf & Trim(oPayment.BANK_Adr3)
                                                            '                                                End If
                                                            '                                                If Not EmptyString(oPayment.BANK_CountryCode) Then
                                                            '                                                    sLine = sLine & vbCrLf & Trim(oPayment.BANK_CountryCode)
                                                            '                                                End If
                                                        End If
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    If EmptyString(oPayment.E_Account) Then
                                                        Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                           "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        sLine = ":59:/" & Trim$(oPayment.E_Account)
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr1) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr2) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Zip) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString(oPayment.E_City) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                                    '10.03.2010 - New code for Ulefos regarding salary
                                                    If oPayment.PayType = "S" And oPayment.PayCode = "203" Then 'Salary
                                                        If Not EmptyString(oPayment.e_OrgNo) Then
                                                            oFile.WriteLine(":70:" & Replace(Trim$(oPayment.e_OrgNo), "-", vbNullString))
                                                        End If
                                                    Else
                                                        If Not EmptyString(oInvoice.Unique_Id) Then
                                                            oInvoice.Unique_Id = RemoveLeadingCharacters(oInvoice.Unique_Id, "0")
                                                            If Len(Trim$(oInvoice.Unique_Id)) > 20 Then
                                                                Err.Raise(1000, "WriteMT101File", "The reference stated is too long. Maximum length is 20 digits." & vbCrLf & vbCrLf & _
                                                                   "Reference:" & Trim$(oInvoice.Unique_Id) & vbCrLf _
                                                                   & sErrFixedText)
                                                            ElseIf Not vbIsNumeric(Trim$(oInvoice.Unique_Id), "0123456789") Then
                                                                Err.Raise(1000, "WriteMT101File", "The reference stated is not valid, it must be numeric." & vbCrLf & vbCrLf & _
                                                                   "Reference:" & Trim$(oInvoice.Unique_Id) & vbCrLf _
                                                                   & sErrFixedText)
                                                            Else
                                                                oFile.WriteLine(":70:OCR" & Trim$(oInvoice.Unique_Id))
                                                            End If
                                                        Else
                                                            sFreetext = vbNullString
                                                            For Each oFreetext In oInvoice.Freetexts
                                                                sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                            Next oFreetext
                                                            lCounter2 = 1
                                                            For lCounter2 = 1 To 4
                                                                If Not EmptyString(sFreetext) Then
                                                                    If lCounter2 = 1 Then
                                                                        oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                                    Else
                                                                        oFile.WriteLine(Left$(sFreetext, 35))
                                                                    End If
                                                                    If Len(sFreetext) > 35 Then
                                                                        sFreetext = Mid$(sFreetext, 36)
                                                                    Else
                                                                        sFreetext = vbNullString
                                                                        Exit For
                                                                    End If
                                                                Else
                                                                    Exit For
                                                                End If
                                                            Next lCounter2
                                                        End If
                                                    End If

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    oFile.WriteLine(":71A:SHA")
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                                'XokNET - 31.01.2012 - Added next case
                                            Case SWIFTCountryCode.France
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use
                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    '10.03.2010 - New code for Ulefos regarding salary
                                                    If oPayment.PayType = "S" And oPayment.PayCode = "203" Then 'Salary
                                                        oFile.WriteLine(":23E:OTHR/SALY")
                                                    End If
                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Trim(oPayment.MON_InvoiceCurrency) = "EUR" Then
                                                        sLine = sLine & "EUR"
                                                    Else
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected. The code must be EUR." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    'oFile.WriteLine ':57A: Account With Institution Identification of beneficiary's bank
                                                    If EmptyString(oPayment.E_Account) Then
                                                        Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                           "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        sLine = ":59:/" & Trim$(oPayment.E_Account)
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr1) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr2) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Zip) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString(oPayment.E_City) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                                    sFreetext = vbNullString
                                                    For Each oFreetext In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                    Next oFreetext
                                                    lCounter2 = 1
                                                    For lCounter2 = 1 To 4
                                                        If Not EmptyString(sFreetext) Then
                                                            If lCounter2 = 1 Then
                                                                oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                            Else
                                                                oFile.WriteLine(Left$(sFreetext, 35))
                                                            End If
                                                            If Len(sFreetext) > 35 Then
                                                                sFreetext = Mid$(sFreetext, 36)
                                                            Else
                                                                sFreetext = vbNullString
                                                                Exit For
                                                            End If
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case SWIFTCountryCode.Germany
                                                'Create 1 sequence B section per invoice
                                                'The format requires account no and BLZ
                                                If ValidateIBAN(oPayment.E_Account, False) Then
                                                    'IBAN format: DEkk BBBB BBBB CCCC CCCC CC
                                                    'B = sort code (Bankleitzahl/BLZ), C = account No.
                                                    oPayment.BANK_BranchType = BabelFiles.BankBranchType.Bankleitzahl
                                                    oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 5, 8)
                                                    oPayment.E_Account = Mid$(oPayment.E_Account, 13)
                                                End If
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use
                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Trim$(oPayment.MON_InvoiceCurrency) <> "EUR" Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected for German local payments. The code must be EUR." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteMT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    If Left$(oPayment.BANK_BranchNo, 2) = "BL" Then
                                                        oPayment.BANK_BranchNo = Mid$(oPayment.BANK_BranchNo, 3)
                                                    End If
                                                    If oPayment.BANK_BranchType <> BabelFiles.BankBranchType.Bankleitzahl And oPayment.BANK_BranchType <> BabelFiles.BankBranchType.NoBranchType Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong branchtype stated for a domestic German payment. A BLZ code is mandatory." & vbCrLf & vbCrLf & _
                                                           sErrFixedText)
                                                    ElseIf Not ValidateBankID(BabelFiles.BankBranchType.Bankleitzahl, oPayment.BANK_BranchNo, False, sErrorString) Then
                                                        Err.Raise(1000, "WriteMT101File", sErrorString & vbCrLf & vbCrLf & _
                                                           "BLZ Code:" & Trim(oPayment.BANK_BranchNo) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        oFile.WriteLine(":57C://BL" & Trim$(oPayment.BANK_BranchNo))  'BLZ - Account With Institution Identification of beneficiary's bank
                                                    End If
                                                    If EmptyString(oPayment.E_Account) Then
                                                        Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                           "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        sLine = ":59:/" & Trim$(oPayment.E_Account)
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr1) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr2) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Zip) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString(oPayment.E_City) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                                    sFreetext = vbNullString
                                                    For Each oFreetext In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                    Next oFreetext
                                                    'Maxlength = 108
                                                    lCounter2 = 1
                                                    For lCounter2 = 1 To 4
                                                        If Not EmptyString(sFreetext) Then
                                                            If lCounter2 = 1 Then
                                                                oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                            ElseIf lCounter2 = 4 Then
                                                                oFile.WriteLine(Left$(sFreetext, 3))
                                                            Else
                                                                oFile.WriteLine(Left$(sFreetext, 35))
                                                            End If
                                                            If Len(sFreetext) > 35 Then
                                                                sFreetext = Mid$(sFreetext, 36)
                                                            Else
                                                                sFreetext = vbNullString
                                                                Exit For
                                                            End If
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case SWIFTCountryCode.Great_Britain
                                                'NOT implemented yet
                                                'Create 1 sequence B section per invoice

                                                'The format requires account no and Sortcode
                                                If ValidateIBAN(Trim$(oPayment.E_Account), False) Then
                                                    '(22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                                                    'B = alphabetical bank code, S = sort code (often a specific branch), C = account No.
                                                    oPayment.BANK_BranchType = BabelFiles.BankBranchType.SortCode
                                                    oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 9, 6)
                                                    oPayment.E_Account = Mid$(oPayment.E_Account, 15)
                                                End If

                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    If oPayment.Cheque > BabelFiles.ChequeType.noCheque Then
                                                        oPayment.Priority = False 'To be sure in a later test
                                                        bNormalPayment = False
                                                        oFile.WriteLine(":23E:CHQB") 'Instruction code - 4a[/30x]
                                                    ElseIf oPayment.Priority Then
                                                        oFile.WriteLine(":23E:URGP") 'Instruction code - 4a[/30x]
                                                        bNormalPayment = False
                                                    Else
                                                        bNormalPayment = True 'BACS
                                                        'Do not specify
                                                    End If

                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume GBP
                                                        sLine = sLine & "GBP"
                                                    ElseIf Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected for British local payments. The code must be GBP." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteMT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    If Not oPayment.Cheque Then
                                                        If oPayment.BANK_BranchType <> BabelFiles.BankBranchType.SortCode And oPayment.BANK_BranchType <> BabelFiles.BankBranchType.NoBranchType Then
                                                            Err.Raise(1000, "WriteMT101File", "Wrong branchtype stated for a domestic Britsh payment. A sort code is mandatory." & vbCrLf & vbCrLf & _
                                                               sErrFixedText)
                                                        ElseIf Not ValidateBankID(BabelFiles.BankBranchType.SortCode, oPayment.BANK_BranchNo, False, sErrorString) Then
                                                            Err.Raise(1000, "WriteMT101File", sErrorString & vbCrLf & vbCrLf & _
                                                               "BLZ Code:" & Trim(oPayment.BANK_BranchNo) & vbCrLf _
                                                               & sErrFixedText)
                                                        Else
                                                            oFile.WriteLine(":57C://SC" & Trim$(oPayment.BANK_BranchNo))  'SC - Account With Institution Identification of beneficiary's bank
                                                        End If
                                                    End If
                                                    If bNormalPayment Then 'BACS
                                                        sLine = ":59:/" & Trim$(oPayment.E_Account)
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 18)
                                                        End If
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        If EmptyString(oPayment.E_Account) And Not oPayment.Cheque Then
                                                            Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                               "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                               & sErrFixedText)
                                                        Else
                                                            If oPayment.Cheque Then
                                                                If EmptyString(oPayment.E_Name & oPayment.E_Adr1 & oPayment.E_Adr2 & oPayment.E_Adr3) Then
                                                                    Err.Raise(1000, "WriteMT101File", "Full name and address must be No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                                       "Name + address: " & Trim(oPayment.E_Name) & "-" & Trim$(oPayment.E_Adr1) & "-" & Trim$(oPayment.E_Adr2) & "-" & Trim$(oPayment.E_Adr3) & "-" & Trim$(oPayment.E_Zip) & "-" & Trim$(oPayment.E_City) & vbCrLf _
                                                                       & sErrFixedText)
                                                                End If
                                                            End If
                                                            sLine = ":59:/" & Trim$(oPayment.E_Account)
                                                            If Not EmptyString(oPayment.E_Name) Then
                                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                                            End If
                                                            If Not EmptyString(oPayment.E_Adr1) Then
                                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                                            End If
                                                            If Not EmptyString(oPayment.E_Adr2) Then
                                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                                            End If
                                                            If Not EmptyString(oPayment.E_Zip) Then
                                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                                            ElseIf Not EmptyString(oPayment.E_City) Then
                                                                sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                                            End If
                                                        End If
                                                        oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                                    End If
                                                    sFreetext = vbNullString
                                                    For Each oFreetext In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                    Next oFreetext
                                                    If bNormalPayment Then 'BACS
                                                        'Maxlength = 18
                                                        oFile.WriteLine(":70:" & Left$(sFreetext, 18))
                                                    Else
                                                        'Maxlength = 140
                                                        lCounter2 = 1
                                                        For lCounter2 = 1 To 4
                                                            If Not EmptyString(sFreetext) Then
                                                                If lCounter2 = 1 Then
                                                                    oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                                Else
                                                                    oFile.WriteLine(Left$(sFreetext, 35))
                                                                End If
                                                                If Len(sFreetext) > 35 Then
                                                                    sFreetext = Mid$(sFreetext, 36)
                                                                Else
                                                                    sFreetext = vbNullString
                                                                    Exit For
                                                                End If
                                                            Else
                                                                Exit For
                                                            End If
                                                        Next lCounter2
                                                    End If
                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case SWIFTCountryCode.Hong_Kong

                                            Case SWIFTCountryCode.Luxemburg

                                            Case SWIFTCountryCode.Netherlands
                                                'Create 1 sequence B section per invoice
                                                'The format requires account no and not IBAN
                                                If ValidateIBAN(oPayment.E_Account, False) Then
                                                    'NLkk BBBB CCCC CCCC CC
                                                    'The first 4 alphanumeric characters represent a bank
                                                    '   and the last 10 digits an account
                                                    'NL98FTSB0244721734
                                                    'oPayment.BANK_BranchType =babelfiles.bankbranchtype.SWIFT
                                                    oPayment.E_Account = RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                                                End If
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    If oPayment.PayType = "S" Or oPayment.PayType = "M" Then
                                                        oPayment.Priority = False 'To be sure in a later test
                                                        bNormalPayment = False
                                                        oFile.WriteLine(":23E:OTHR/SALY") 'Instruction code - 4a[/30x]
                                                    ElseIf oPayment.Priority Then
                                                        oFile.WriteLine(":23E:URGP") 'Instruction code - 4a[/30x]
                                                        bNormalPayment = False
                                                    Else
                                                        bNormalPayment = True
                                                        'Do not specify
                                                    End If

                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume EUR
                                                        sLine = sLine & "EUR"
                                                    ElseIf Trim$(oPayment.MON_InvoiceCurrency) <> "EUR" Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected for Dutch local payments. The code must be EUR." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                        'ElseIf UCase(Trim$(oPayment.MON_InvoiceCurrency)) <> "EUR" Then
                                                        '    Err.Raise 1, "WriteMT101File", "For a local finnish payment, the currencycode should be EUR."
                                                    Else
                                                        sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    '                                        If Len(Trim$(oInvoice.MON_InvoiceAmount)) = 1 Then
                                                    '                                            sAmount = "0,0" & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        ElseIf Len(Trim$(oInvoice.MON_InvoiceAmount)) = 2 Then
                                                    '                                            sAmount = "0," & LTrim(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                        Else
                                                    '                                            sAmount = Trim$(Str(oInvoice.MON_InvoiceAmount))
                                                    '                                            sAmount = Left$(sAmount, Len(sAmount) - 2) & "," & Right$(sAmount, 2)
                                                    '                                        End If
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)   'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used
                                                    oPayment.E_Account = Trim$(oPayment.E_Account)
                                                    If Len(oPayment.E_Account) < 3 Or Len(oPayment.E_Account) > 9 Then
                                                        Err.Raise(1000, "WriteMT101File", "No or wrong accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                           "Accountnumber:" & Trim(oPayment.E_Account) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        If Len(oPayment.E_Account) < 8 Then
                                                            bThisIsAPostBankAccount = True
                                                        Else
                                                            bThisIsAPostBankAccount = False
                                                        End If
                                                        If bThisIsAPostBankAccount Or oPayment.Priority Then
                                                            bx = RetrieveZipAndCityFromTheAddress(oPayment, True)
                                                        End If
                                                        sLine = ":59:/" & oPayment.E_Account
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Name), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr1) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr2) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
                                                        End If
                                                        'Sometimes the city is mandatory
                                                        If bThisIsAPostBankAccount Then
                                                            If EmptyString(oPayment.E_City) Then
                                                                Err.Raise(1000, "WriteMT101File", "When a payment is done to a post bankaccount, the receivers city is mandatory." & vbCrLf & vbCrLf & _
                                                                   "City:" & Trim(oPayment.E_City) & vbCrLf _
                                                                   & sErrFixedText)
                                                            End If
                                                        ElseIf oPayment.Priority Then
                                                            If EmptyString(oPayment.E_City) Then
                                                                Err.Raise(1000, "WriteMT101File", "When it is an urgent payment, the receivers city is mandatory." & vbCrLf & vbCrLf & _
                                                                   "City:" & Trim(oPayment.E_City) & vbCrLf _
                                                                   & sErrFixedText)
                                                            End If
                                                        End If
                                                        If Not EmptyString(oPayment.E_Zip) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
                                                        ElseIf Not EmptyString(oPayment.E_City) Then
                                                            sLine = sLine & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
                                                        End If
                                                    End If
                                                    oFile.WriteLine(sLine) 'Beneficiary [/34x] 4*35x Beneficiary’s account number, name and address.
                                                    If bNormalPayment And Not EmptyString(oInvoice.Unique_Id) Then
                                                        oInvoice.Unique_Id = RemoveLeadingCharacters(oInvoice.Unique_Id, "0")
                                                        If Len(Trim$(oInvoice.Unique_Id)) > 18 Or Len(Trim$(oInvoice.Unique_Id)) < 9 Then
                                                            Err.Raise(1000, "WriteMT101File", "The reference stated on the acceptgiro-payment is of wrong length. The length must be bewtween 9 and 18 digits including the form code." & vbCrLf & vbCrLf & _
                                                               "Reference:" & Trim$(oInvoice.Unique_Id) & vbCrLf _
                                                               & sErrFixedText)
                                                        ElseIf Not vbIsNumeric(Trim$(oInvoice.Unique_Id), "0123456789") Then
                                                            Err.Raise(1000, "WriteMT101File", "The reference stated on the acceptgiro-payment is not valid, it must be numeric." & vbCrLf & vbCrLf & _
                                                               "Reference:" & Trim$(oInvoice.Unique_Id) & vbCrLf _
                                                               & sErrFixedText)
                                                        Else
                                                            oFile.WriteLine(":70:" & Trim$(oInvoice.Unique_Id))
                                                        End If
                                                    Else
                                                        sFreetext = vbNullString
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                        Next oFreetext
                                                        If bNormalPayment Then
                                                            iCharactersPerLine = 32
                                                        Else
                                                            iCharactersPerLine = 35
                                                        End If
                                                        lCounter2 = 1
                                                        For lCounter2 = 1 To 4
                                                            If Not EmptyString(sFreetext) Then
                                                                If lCounter2 = 1 Then
                                                                    oFile.WriteLine(":70:" & Left$(sFreetext, iCharactersPerLine))
                                                                Else
                                                                    oFile.WriteLine(Left$(sFreetext, iCharactersPerLine))
                                                                End If
                                                                If Len(sFreetext) > iCharactersPerLine Then
                                                                    sFreetext = Mid$(sFreetext, iCharactersPerLine + 1)
                                                                Else
                                                                    sFreetext = vbNullString
                                                                    Exit For
                                                                End If
                                                            Else
                                                                Exit For
                                                            End If
                                                        Next lCounter2
                                                    End If

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case SWIFTCountryCode.Norway

                                            Case SWIFTCountryCode.Poland

                                            Case SWIFTCountryCode.Singapore

                                            Case SWIFTCountryCode.Sweden
                                                'Create 1 sequence B section per invoice
                                                For Each oInvoice In oPayment.Invoices
                                                    If EmptyString(oInvoice.REF_Own) Then
                                                        oFile.WriteLine(":21:" & Trim$(Str(lCounter))) 'Transactionref 16X
                                                    Else
                                                        oFile.WriteLine(":21:" & Right$(Trim$(oInvoice.REF_Own), 16)) 'Transactionref - 16x
                                                    End If
                                                    'oFile.WriteLine ":21F:" &  'F/X Deal reference - 16x - not in use

                                                    'oFile.WriteLine ":23E:" &  'Instruction code - 4a[/30x] - not in use
                                                    If oPayment.PayType = "S" Then
                                                        oFile.WriteLine(":23E:SALY")
                                                    End If

                                                    'Currency/Transaction Amount - 3!a15d - Currency and amount
                                                    sLine = ":32B:"
                                                    If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Assume SEK
                                                        sLine = sLine & "SEK"
                                                    ElseIf Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
                                                        Err.Raise(1000, "WriteMT101File", "Wrong currencycode detected. The code must be 3 positions." & vbCrLf & vbCrLf & _
                                                           "Currencycode:" & Trim$(oPayment.MON_InvoiceCurrency) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                    sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                                    sLine = sLine & sAmount
                                                    oFile.WriteLine(sLine)

                                                    'oFile.WriteLine ':50L: Instructing Party 35x Not used in Sequence B
                                                    'oFile.WriteLine ':50H: Ordering Customer /34 4*35x Not used in Sequence B
                                                    'oFile.WriteLine ':52A: Account Servicing Institution Not used in Sequence B
                                                    'oFile.WriteLine ':56A: Intermediary Not used

                                                    'oFile.WriteLine ':57C: Account With Institution Identification of beneficiary's bank
                                                    If ValidateIBAN(oPayment.E_Account, False) Then
                                                        'IBAN format: SEkk BBBB CCCC CCCC CCCC CCCC
                                                        'The Bs represent the bank code and the Cs the account number.
                                                        oPayment.E_Account = Trim$(oPayment.E_Account)
                                                        sClearingCode = Mid$(oPayment.E_Account, 5, 4)
                                                        If sClearingCode = "6000" Then
                                                            'Treat Handelsbanken in a special way. Clearing code is NOT stated as a part of the AccountNo in IBAN
                                                            oPayment.E_Account = sClearingCode & RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                                                            oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                        ElseIf sClearingCode = "8000" Then
                                                            'Treat Swedbank in a special way. Use the accountno from the IBAN,
                                                            '  but remove the 5th position.
                                                            oPayment.E_Account = RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                                                            oPayment.E_Account = Left$(oPayment.E_Account, 4) & Mid$(oPayment.E_Account, 6)
                                                            oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                        Else
                                                            oPayment.E_Account = RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                                                            oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                        End If
                                                    End If

                                                    oPayment.E_Account = Trim$(oPayment.E_Account)
                                                    If oPayment.PayType = "S" Then
                                                        oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                    Else
                                                        If oPayment.E_AccountType = BabelFiles.AccountType.NoAccountType And Not EmptyString(oPayment.E_Account) Then
                                                            'Something is stated in the E_Account but what?
                                                            'Let's do some qualified guessing
                                                            If (InStr(1, oPayment.E_Account, "-", vbTextCompare) > 0) And (Len(oPayment.E_Account) = 8 Or Len(oPayment.E_Account) = 9 Or Len(oPayment.E_Account) = 10) Then
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro
                                                                oPayment.E_Account = Replace(oPayment.E_Account, "-", vbNullString, , , vbTextCompare)
                                                            ElseIf Len(oPayment.E_Account) = 7 Or Len(oPayment.E_Account) = 8 Or Len(oPayment.E_Account) = 9 Then
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro
                                                            ElseIf Len(oPayment.E_Account) > 7 And Len(oPayment.E_Account) < 15 Then
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount
                                                            Else
                                                                oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro
                                                            End If
                                                        End If
                                                    End If

                                                    If EmptyString(oPayment.E_Account) Then
                                                        Err.Raise(1000, "WriteMT101File", "No accountnumber for beneficiary stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                           "Accountnumber:" & Trim$(oPayment.E_Account) & vbCrLf _
                                                           & sErrFixedText)
                                                    Else
                                                        Select Case oPayment.E_AccountType

                                                            Case BabelFiles.AccountType.SE_Bankgiro
                                                                oFile.WriteLine(":57C://SE9900")
                                                                oFile.WriteLine(":59:/" & oPayment.E_Account)
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                If EmptyString(oInvoice.Unique_Id) Then
                                                                    sFreetext = vbNullString
                                                                    For Each oFreetext In oInvoice.Freetexts
                                                                        sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                                    Next oFreetext
                                                                Else
                                                                    sFreetext = Trim$(oInvoice.Unique_Id)
                                                                End If

                                                            Case BabelFiles.AccountType.SE_PlusGiro
                                                                oFile.WriteLine(":57C://SE9500")
                                                                oFile.WriteLine(":59:/" & oPayment.E_Account)
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                If EmptyString(oInvoice.Unique_Id) Then
                                                                    sFreetext = vbNullString
                                                                    For Each oFreetext In oInvoice.Freetexts
                                                                        sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                                    Next oFreetext
                                                                Else
                                                                    sFreetext = Trim$(oInvoice.Unique_Id)
                                                                End If


                                                            Case BabelFiles.AccountType.SE_Bankaccount
                                                                'No :57C:
                                                                oFile.WriteLine(":59:/" & oPayment.E_Account)
                                                                oFile.WriteLine(HBGOAddE_Info(oPayment))
                                                                bMessageIsMandatory = True
                                                                sFreetext = vbNullString
                                                                If oPayment.PayType <> "S" Then
                                                                    For Each oFreetext In oInvoice.Freetexts
                                                                        sFreetext = sFreetext & Trim$(oFreetext.Text)
                                                                    Next oFreetext
                                                                Else
                                                                    bMessageIsMandatory = False
                                                                End If

                                                        End Select
                                                    End If

                                                    If Not EmptyString(sFreetext) Then
                                                        If Len(sFreetext) > 140 Then
                                                            If oBabel.VB_ProfileInUse Then
                                                                sFreetext = DiminishFreetext(sFreetext, 140, oPayment.E_Name & vbCrLf & oPayment.MON_InvoiceAmount)
                                                            Else
                                                                sFreetext = Left$(sFreetext, 140)
                                                            End If
                                                        End If
                                                        lCounter2 = 1
                                                        For lCounter2 = 1 To 4
                                                            If Not EmptyString(sFreetext) Then
                                                                If lCounter2 = 1 Then
                                                                    oFile.WriteLine(":70:" & Left$(sFreetext, 35))
                                                                Else
                                                                    oFile.WriteLine(Left$(sFreetext, 35))
                                                                End If
                                                                If Len(sFreetext) > 35 Then
                                                                    sFreetext = Mid$(sFreetext, 36)
                                                                Else
                                                                    sFreetext = vbNullString
                                                                    Exit For
                                                                End If
                                                            Else
                                                                Exit For
                                                            End If
                                                        Next lCounter2
                                                    Else
                                                        If bMessageIsMandatory Then
                                                            Err.Raise(1000, "WriteMT101File", "No message to beneficiary is stated. It is mandatory." & vbCrLf & vbCrLf & _
                                                               sErrFixedText)
                                                        End If
                                                    End If

                                                    'oFile.WriteLine ':77B: Regulatory Reporting 3*35x Not used
                                                    'oFile.WriteLine ':33B: Currency/Original Ordered Amount 3!a15d Not used
                                                    'oFile.WriteLine ':71A: Details of Charges 3a Accepted but not acted upon
                                                    'oFile.WriteLine ':25A: Charges Account /34x Not used
                                                    'oFile.WriteLine ':36: Exchange Rate /12d Not used

                                                Next oInvoice

                                            Case SWIFTCountryCode.USA

                                        End Select

                                    End If 'If oPayment.PayCode = "I"

                                    oPayment.Exported = True

                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch

                End If

            Next

            If Not bFirstSequenceA And (eBank = BabelFiles.Bank.Danske_Bank_dk Or eBank = BabelFiles.Bank.Danske_Bank_NO Or eBank = BabelFiles.Bank.Danske_Bank_SF) Then
                sLine = "-}"
                oFile.WriteLine(sLine)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteShipNetFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteMT101File = True

    End Function

    Private Function WriteSequenceARecord(ByVal oFile As Scripting.TextStream, ByVal oaccount As vbBabel.Account, ByVal oPayment As vbBabel.Payment, ByVal eCountryCode As SWIFTCountryCode, ByVal sCompanyNo As String, ByVal sI_Swift As String, ByVal sExecutionDate As String, ByVal lPaymentCounter As Long, ByVal sErrFixedText As String, ByVal sFileID As String, ByVal bFirstSequenceA As Boolean, ByVal eBank As BabelFiles.Bank) As Boolean
        'M 20 Sender’s Reference 16x Must be unique for each message (or chain of messages) between the sender and the bank.
        'O 21R Customer Specified Reference 16x Always ‘NOLI’
        'N 28D Message Index / Total 5n/5n Accepted but not acted upon
        'O 50L Instructing Party 35x Accepted but not acted upon
        'M 50H Ordering Customer /34x 4*35x Line 1: /Account to be debited for all transactions in sequence B. Line 2: customer identity in Handelsbanken, Business 4organisation number or SHB no. See country specific information
        'M 52A Account Servicing Institution 4!a2!a2!c [3!c] BIC code of the bank servicing the customer’s account to be debited, e.g. HANDDKKK. See country specific information.
        'N 51A Sending Institution Not used
        'M 30 Requested Execution Date 6!n Execution date The date on which all subsequent transactions should be initiated by the executing bank.
        'N 25 Authorisation 35x Not used
        Dim sLine As String

        sLine = ""
        If bFirstSequenceA And (eBank = BabelFiles.Bank.Danske_Bank_dk Or eBank = BabelFiles.Bank.Danske_Bank_NO Or eBank = BabelFiles.Bank.Danske_Bank_SF) Then
            sLine = "{1:F01DABAFIHHXXXX}{2:I101DABAFIHHXXXXN}{4:"
            oFile.WriteLine(sLine)
        End If
        sLine = ":20:" & sFileID 'Sender’s Reference
        oFile.WriteLine(sLine)
        If oPayment.PayType <> "I" Then
            'oFile.WriteLine ":21R:NOLI" 'Customer Specified Reference
        Else
            'Nothing to do
        End If
        If EmptyString(oPayment.I_Account) Then
            Err.Raise(1000, , "WriteHBGlobalOnline_MT101File", "No debit account stated for ordering customer. It is mandatory." & vbCrLf & vbCrLf & _
               "Accountnumber:" & Trim$(oPayment.I_Account) & vbCrLf _
               & sErrFixedText)
        Else
            oFile.WriteLine(":50H:/" & Trim$(oPayment.I_Account))
            '    Select Case eCountryCode
            '
            '    Case SWIFTCountryCode.Finland, SWIFTCountryCode.Germany, SWIFTCountryCode.Netherlands
            '        'Customer identity in Handelsbanken, The test that a valid customer ID exists is done in the calling function.

            'New 26.02.2008
            If EmptyString(oaccount.ContractNo) Then
                oFile.WriteLine(sCompanyNo)
            Else
                oFile.WriteLine(Trim$(oaccount.ContractNo))
            End If

            '    Case Else
            '
            '    End Select
        End If
        'oFile.WriteLine ":52A:" & sI_Swift
        oFile.WriteLine(":30:" & Mid$(sExecutionDate, 3))

        WriteSequenceARecord = True

    End Function
    'Private Function WriteLocalFinland(oFile As TextStream, oaccount As vbBabel.Account, oPayment As vbBabel.Payment, eCountryCode As SWIFTCountryCode, sCompanyNo As String, sI_Swift As String, sExecutionDate As String) As Boolean
    ''M 20 Sender’s Reference 16x Must be unique for each message (or chain of messages) between the sender and the bank.
    ''O 21R Customer Specified Reference 16x Always ‘NOLI’
    ''N 28D Message Index / Total 5n/5n Accepted but not acted upon
    ''O 50L Instructing Party 35x Accepted but not acted upon
    ''M 50H Ordering Customer /34x 4*35x Line 1: /Account to be debited for all transactions in sequence B. Line 2: customer identity in Handelsbanken, Business 4organisation number or SHB no. See country specific information
    ''M 52A Account Servicing Institution 4!a2!a2!c [3!c] BIC code of the bank servicing the customer’s account to be debited, e.g. HANDDKKK. See country specific information.
    ''N 51A Sending Institution Not used
    ''M 30 Requested Execution Date 6!n Execution date The date on which all subsequent transactions should be initiated by the executing bank.
    ''N 25 Authorisation 35x Not used
    'Dim sLine As String
    '
    'On Error GoTo errWriteSequenceARecord
    '
    'sLine = ":20:" & Format(Now(), "YYYYMMDDHHMMSS") 'Sender’s Reference
    'oFile.WriteLine sLine
    'oFile.WriteLine ":21R:NOLI" 'Customer Specified Reference
    'If EmptyString(oPayment.I_Account) Then
    '    Err.Raise 1, "WriteSequenceARecord", "No debit account stated for ordering customer."
    'Else
    '    oFile.WriteLine ":50H:/" & Trim$(oPayment.I_Account)
    '    Select Case eCountryCode
    '
    '    Case SWIFTCountryCode.Finland
    '        'Customer identity in Handelsbanken, The test that a valid customer ID exists is done in the calling function.
    '        oFile.WriteLine sCompanyNo
    '
    '    Case Else
    '
    '    End Select
    'End If
    'oFile.WriteLine ":52A:" & sI_Swift
    'oFile.WriteLine ":30:" & Mid$(sExecutionDate, 3)
    '
    'WriteSequenceARecord = True
    'Exit Function
    '
    'errWriteSequenceARecord:
    '
    'Err.Raise Err.Number, "WriteSequenceARecord", Err.Description
    '
    'End Function
    Private Function SWIFTConvertToCorrectAmount(ByVal nAmountToConvert As Double) As String
        Dim sReturnAmount As String
        Dim sAmountToConvert As String

        sAmountToConvert = Trim$(Str(nAmountToConvert))
        If Len(sAmountToConvert) = 1 Then
            sAmountToConvert = "0,0" & sAmountToConvert
        ElseIf Len(sAmountToConvert) = 2 Then
            sAmountToConvert = "0," & sAmountToConvert
        Else
            sAmountToConvert = sAmountToConvert
            sAmountToConvert = Left$(sAmountToConvert, Len(sAmountToConvert) - 2) & "," & Right$(sAmountToConvert, 2)
        End If

        SWIFTConvertToCorrectAmount = sAmountToConvert

    End Function

    Private Function HBGOAddE_Info(ByVal oPayment As vbBabel.Payment) As String
        Dim sReturnValue As String

        sReturnValue = vbNullString
        If Not EmptyString(oPayment.E_Name) Then
            sReturnValue = sReturnValue & Left$(Trim$(oPayment.E_Name), 35)
        End If
        If Not EmptyString(oPayment.E_Adr1) Then
            sReturnValue = sReturnValue & vbCrLf & Left$(Trim$(oPayment.E_Adr1), 35)
        End If
        If Not EmptyString(oPayment.E_Adr2) Then
            sReturnValue = sReturnValue & vbCrLf & Left$(Trim$(oPayment.E_Adr2), 35)
        End If
        If Not EmptyString(oPayment.E_Zip) Then
            sReturnValue = sReturnValue & vbCrLf & Left$(Trim$(oPayment.E_Zip) & " " & Trim$(oPayment.E_City), 35)
        ElseIf Not EmptyString(oPayment.E_City) Then
            sReturnValue = sReturnValue & vbCrLf & Left$(Trim$(oPayment.E_City), 35)
        End If

        If Left$(sReturnValue, 2) = vbCrLf Then
            sReturnValue = Mid$(sReturnValue, 3)
        End If

        HBGOAddE_Info = sReturnValue

    End Function

    Function WriteMT940File(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal iFilenameInNo As Integer, ByVal sCompanyNo As String, ByVal sBranch As String, ByVal sVersion As String, ByVal eBank As BabelFiles.Bank, ByVal sSpecial As String) As Boolean

        ' First created for Falck Security - to be able to export OCR-transactions on MT940 fdormat !
        ' Covers :61: and :86: records so far
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim i As Integer
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String
        Dim bSequenceAWritten As Boolean
        'Dim eCountryCode As SWIFTCountryCode
        'Dim eInstructionCode As InstructionCode
        'Dim bDebitAccountInHandelsbanken As Boolean
        Dim sLastUsedExecutionDate As String
        Dim sToday As String
        Dim lCounter As Long, lCounter2 As Long
        Dim sAmount As String
        'Dim sInfoToWriteInTag36 As String, sTemp As String
        Dim sErrFixedText As String
        'Dim sKortartKode As String
        'Dim sBetalerIdent As String
        'Dim sKreditorNo As String
        Dim sErrorString As String
        'Dim bMessageIsMandatory As Boolean
        Dim sFile_ID As String
        'Dim bThisIsAPostBankAccount As Boolean
        'Dim bNormalPayment As Boolean
        Dim iCharactersPerLine As Integer
        'Dim sClearingCode As String
        'Dim sOldPayType As String
        'Dim bx As Boolean
        Dim bFirstSequenceA As Boolean
        'Dim sErrorString As String
        Dim sCurrency As String
        Dim bOutgoing As Boolean = False  ' use this one to set if it's outgoing payments or incoming payments
        Dim nTotalAmount As Double = 0
        Dim bAccountsInfo As Boolean = True

        Try

            ' create an outputfile
            'On Error GoTo errCreateOutputFile
            bAppendFile = False

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bSequenceAWritten = False
            bFirstSequenceA = True
            sToday = Format(Now(), "yyyyMMdd")

            For Each oBabel In oBabelFiles

                ' Test format to check if outgoing or incoming.
                If oBabel.VB_Profile.FileSetups(oBabel.VB_Profile.FileSetups(iFormat_ID).FileSetupOut).Enum_ID = BabelFiles.FileType.Telepay2 Then
                    bOutgoing = True
                    ' 15.06.2021 - outgoing when DNB kontoinfo is import format
                    ' NB - Dette må sjekkes nøyere, det er ikke sikkert vi får fortegn korrekt, og vi må se om alle transer er med !!!!
                ElseIf oBabel.VB_Profile.FileSetups(oBabel.VB_Profile.FileSetups(iFormat_ID).FileSetupOut).Enum_ID = BabelFiles.FileType.DnBTBWK Then
                    bOutgoing = False
                    bAccountsInfo = True
                ElseIf oBabel.VB_Profile.FileSetups(oBabel.VB_Profile.FileSetups(iFormat_ID).FileSetupOut).Enum_ID = BabelFiles.FileType.OCR Then
                    bOutgoing = False
                Else
                    bOutgoing = False
                End If

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If oPayment.I_Account <> sOldAccount Then
                                        bFoundClient = False

                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Or Trim$(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                    sCurrency = oaccount.CurrencyCode
                                                    sSwift = Trim$(oaccount.SWIFTAddress)
                                                    sOldAccount = oPayment.I_Account
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next
                                            If bFoundClient Then
                                                Exit For
                                            End If


                                        Next
                                        bSequenceAWritten = False

                                    End If

                                    If Not bFoundClient Then
                                        ' close file first!
                                        oFile.Close()
                                        oFile = Nothing
                                        oFs = Nothing

                                        Err.Raise(1, "WriteMT940File", "The debitaccount " & oPayment.I_Account & " is unknown to BabelBank. You must enter information about the account in the Client setup.")
                                    End If


                                    If oPayment.DATE_Payment < sToday Then
                                        If sToday = sLastUsedExecutionDate Then
                                            'OK, no new sequence A
                                        Else
                                            'Change in date, write a new sequence A
                                            sLastUsedExecutionDate = sToday
                                            bSequenceAWritten = False
                                        End If
                                    ElseIf oPayment.DATE_Payment = sLastUsedExecutionDate Then
                                        'OK, no new sequence A
                                    Else
                                        'Change in date, write a new sequence A
                                        sLastUsedExecutionDate = oPayment.DATE_Payment
                                        bSequenceAWritten = False
                                    End If
                                    sDate = oPayment.DATE_Payment

                                    oPayment.BANK_I_SWIFTCode = sSwift
                                    'Create en fixed error text with info from the payment
                                    sErrFixedText = vbNullString
                                    sErrFixedText = "Beneficiary's name: " & oPayment.E_Name & vbCrLf
                                    sErrFixedText = sErrFixedText & "Beneficiary's account: " & oPayment.E_Account & vbCrLf
                                    sErrFixedText = sErrFixedText & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf
                                    sErrFixedText = sErrFixedText & "Payer's account: " & oPayment.I_Account




                                    If Not bSequenceAWritten Then
                                        If sSpecial = "HANDELSBANKEN_MT940" Then
                                            sLine = ":20:" & Format(Now(), "yyMMddHHMMss")
                                        Else
                                            sLine = ":20:" & Format(Now(), "yyyyMMddHHMMss")
                                        End If
                                        oFile.WriteLine(sLine)

                                        ' Accountsrecord
                                        ' :25:ESSENOKXXXX/97500630977
                                        If sSpecial = "HANDELSBANKEN_MT940" Then
                                            sLine = ":25:" & oPayment.I_Account.Trim
                                        Else
                                            sLine = ":25:" & sSwift & "/" & oPayment.I_Account.Trim
                                        End If
                                        oFile.WriteLine(sLine)
                                        ' :28C: Statementnumber + Pagenumber
                                        sLine = ":28C:" & oBatch.Batch_ID
                                        oFile.WriteLine(sLine)

                                        'Opening Balance (Booked funds)
                                        ':60F:C071228EUR228148,91
                                        ' Use amount from Batch
                                        sLine = ":60F:"
                                        If oBatch.MON_BalanceIN < 0 Then
                                            sLine = sLine & "D"
                                        Else
                                            sLine = sLine & "C"
                                        End If
                                        ' Date YYMMDD
                                        If oBatch.DATE_BalanceIN < (oPayment.DATE_Payment - 10) Then
                                            ' if unrealistic balancedate, take date from first payment
                                            oBatch.DATE_BalanceIN = oPayment.DATE_Payment
                                        End If
                                        sLine = sLine & Mid(oBatch.DATE_BalanceIN, 3)
                                        sLine = sLine & sCurrency
                                        ' If no BalanceIn amount ?????
                                        sAmount = SWIFTConvertToCorrectAmount(oBatch.MON_BalanceIN)
                                        sLine = sLine & sAmount
                                        oFile.WriteLine(sLine)

                                        bSequenceAWritten = True
                                        bFirstSequenceA = False
                                    End If

                                    lCounter = lCounter + 1
                                    'Create 1 sequence B section per invoice
                                    ':61:1512071207C449,00NTRFNONREF//OSL 15120760568
                                    'DOM.INCOMING(TRANSFER)
                                    ':86:/ORDP/Rødven Rolf/REMI/0054589791//OCMT/NOK449,00
                                    For Each oInvoice In oPayment.Invoices
                                        nTotalAmount = nTotalAmount + oInvoice.MON_InvoiceAmount
                                        sAmount = SWIFTConvertToCorrectAmount(oInvoice.MON_InvoiceAmount)
                                        ' 26.10.2018 - ta vekk minustegn!
                                        sAmount = Replace(sAmount, "-", "")

                                        ':61:1512071207C449,00NTRFNONREF//OSL 15120760568
                                        sLine = ":61:"
                                        sLine = sLine & Mid(sDate, 3, 6) & Mid(sDate, 5, 4)  'YYMMDDMMDD

                                        ' D/C is dependenat upon incoming or outgoing payments
                                        If bOutgoing Then
                                            If oInvoice.MON_InvoiceAmount < 0 Then
                                                sLine = sLine & "C"
                                            Else
                                                sLine = sLine & "D"
                                            End If
                                        Else
                                            If oInvoice.MON_InvoiceAmount < 0 Then
                                                sLine = sLine & "D"
                                            Else
                                                sLine = sLine & "C"
                                            End If
                                        End If
                                        sLine = sLine & sAmount
                                        ' What text to use?
                                        If sSpecial = "HANDELSBANKEN_MT940" Then
                                            ' always FTR in this case
                                            sLine = sLine & "FTR"
                                        End If
                                        sLine = sLine & oPayment.REF_Bank1  '"TRFNONREF//OSL


                                        If Not bOutgoing Then
                                            ' date 
                                            sLine = sLine & Mid(sDate, 3)  'YYMMDD
                                        End If
                                        ' ref
                                        sLine = sLine & oPayment.REF_Bank2
                                        oFile.WriteLine(sLine)
                                        ' add a text;
                                        sFreetext = ""
                                        For Each oFreetext In oInvoice.Freetexts
                                            sFreetext = sFreetext & oFreetext.Text.Trim
                                        Next
                                        If Not EmptyString(sFreetext) Then
                                            sLine = Trim(Left(sFreetext, 140))
                                            oFile.WriteLine(sLine)
                                        End If

                                        ' :86: recorden er nok veldig "Falckish". Må nok endres pr case
                                        If bOutgoing Then
                                            ':86:/REMI/Falck Secure
                                            '/BENM/Kenneth Pettersen 18226036780
                                            sLine = ":86:/REMI/" & sFreetext     ' use logic here for more advanced cases
                                            oFile.WriteLine(sLine)
                                            sLine = "/BENM/" & oPayment.E_Name.Trim & " " & oPayment.E_Account
                                            oFile.WriteLine(sLine)
                                        Else
                                            ' 86 record
                                            If sSpecial = "HANDELSBANKEN_MT940" Then
                                                ' 26.10.2018 moved ORDP after REMI
                                                ' --------------------------------
                                                ':86:/ORDP/Rødven Rolf/REMI/0054589791//OCMT/NOK449,00
                                                'sLine = ":86:/ORDP/"     ' use logic here for more advanced cases
                                                'sLine = sLine & oPayment.E_Name & "/"
                                                sLine = ":86:/REMI/"  ' use logic here for more advanced cases
                                                If Not EmptyString(oInvoice.Unique_Id) Then
                                                    sLine = sLine & oInvoice.Unique_Id.Trim '& "//OCMT/"
                                                ElseIf oInvoice.Freetexts.Count > 0 Then
                                                    sLine = sLine & oInvoice.Freetexts(1).Text.Trim & "///" & vbNewLine
                                                Else
                                                    sLine = sLine & "////"
                                                End If
                                                sLine = sLine & "/ORDP/"     ' use logic here for more advanced cases
                                                sLine = sLine & oPayment.E_Name & "/"
                                                'sLine = sLine & "//OCMT/"
                                                'sLine = sLine & oPayment.MON_InvoiceCurrency
                                                'sLine = sLine & sAmount
                                            Else
                                                ':86:/ORDP/Rødven Rolf/REMI/0054589791//OCMT/NOK449,00
                                                sLine = ":86:/ORDP/"     ' use logic here for more advanced cases
                                                sLine = sLine & oPayment.E_Name & "/"
                                                If Not EmptyString(oInvoice.Unique_Id) Then
                                                    sLine = sLine & oInvoice.Unique_Id.Trim & "//OCMT/"
                                                Else
                                                    sLine = sLine & "////"
                                                End If
                                                sLine = sLine & oPayment.MON_InvoiceCurrency
                                                sLine = sLine & sAmount
                                            End If
                                            oFile.WriteLine(sLine)
                                        End If  'if bOutgoing then

                                    Next oInvoice

                                    oPayment.Exported = True

                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch

                End If

            Next

            ' Falck will need a daytotal in 62F
            If sSpecial = "FALCK_RETUR" Or sSpecial = "FALCK_OCR" Then
                oBatch.MON_BalanceOUT = nTotalAmount
            End If

            ' Write endrecords
            ' PS: Not prepared for more than one set of :62F: and :64: - don't know if several sections are allowed?
            sLine = ":62F:"
            If oBatch.MON_BalanceOUT < 0 Then
                sLine = sLine & "D"
            Else
                sLine = sLine & "C"
            End If
            ' Date YYMMDD
            If oBatch.DATE_BalanceOUT < (sDate - 10) Then
                ' if unrealistic balancedate, take date from first payment
                oBatch.DATE_BalanceOUT = sDate
            End If

            sLine = sLine & Mid(oBatch.DATE_BalanceOUT, 3)
            ' Currency, pick from first payment (carefull!)
            sLine = sLine & sCurrency
            ' If no BalanceOUT amount ?????
            sAmount = SWIFTConvertToCorrectAmount(oBatch.MON_BalanceOUT)
            sLine = sLine & sAmount
            oFile.WriteLine(sLine)

            sLine = ":64:"
            If oBatch.MON_BalanceOUT < 0 Then
                sLine = sLine & "D"
            Else
                sLine = sLine & "C"
            End If
            ' Date YYMMDD
            If oBatch.DATE_BalanceOUT < (sDate - 10) Then
                ' if unrealistic balancedate, take date from first payment
                oBatch.DATE_BalanceOUT = sDate
            End If

            sLine = sLine & Mid(oBatch.DATE_BalanceOUT, 3)
            ' Currency, pick from first payment (carefull!)
            sLine = sLine & sCurrency
            ' If no BalanceOUT amount ?????
            sAmount = SWIFTConvertToCorrectAmount(oBatch.MON_BalanceOUT)
            sLine = sLine & sAmount
            oFile.WriteLine(sLine)

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteShipNetFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteMT940File = True

    End Function
End Module