Option Strict Off
Option Explicit On
' Johannes# 
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("Client_NET.Client")> Public Class Client
	
	Public nIndex As Double
	Private oAccounts As vbbabel.Accounts
	Private nCLientFormat_ID As Double
	Private nClient_ID As Double
	Private iFormatIn, iFormatOut As Short
	Private nSeqnoTotal As Double
	Private sOwnRefText, sName As String
	Private bOutgoing As Boolean
	Private sCountryCode, sCity, sCompNo As String
	Private sDivision, sClientNo As String
	Private iNotification2, iNotification1, iNotification3 As Short
	Private sImportKID2, sImportKID1, sImportKID3 As String
	Private sExportKID As String
	Private sNoMatchAccount As String
	Private iNoMatchAccountType As Short
	Private sRoundingsAccount As String
	Private iRoundingsAccountType As Short
	Private sChargesAccount As String
	Private iChargesAccountType As Short
	Private sAkontoPattern As String
	Private iAkontoPatternType As Short
	Private sMatchPattern As String
	Private iMatchPatternType As Short
	Private sGLPattern As String
	Private iGLPatternType As Short
	Private sVoucherNo, sVoucherType As String
	Private bVoucherNo1Changed As Boolean 'Exist only in the collection, not saved
	Private sVoucherNo2, sVoucherType2 As String
	Private bVoucherNo2Changed As Boolean 'Exist only in the collection, not saved
	Private iHowToAddVoucherNo As Short
	Private bAddVoucherNoBeforeOCR As Boolean
	Private bAddCounterToVoucherNo As Boolean
	' 16.03.06 Added new Property itmpFlag
	Private itmpFlag As Short
	Private iBB_Year As Short
	Private iBB_Period As Short
	
	Private iKIDStart, iKIDLength As Short
	Private sKIDValue As String
	Private iDBProfile_ID As Short
    Private eStatus As vbBabel.Profile.CollectionStatus

    ' added 09.07.2015 for ISO20022-usage
    Private iHistoryDelDays As Integer
    Private bSEPASingle As Boolean
    Private bUseStateBankText As Boolean
    Private bEndToEndRefFromBabelBank As Boolean
    Private sMsgID_Variable As String
    Private iValidationLevel As Integer
    Private bSavePaymentData As Boolean
    Private bAdjustForSchemaValidation As Boolean
    ' 28.12.2021
    Private sSocialSecurityNo As String = ""

    ' 12.09.2016 added 4 next 
    Dim bAdjustToBBANNorway As Boolean = False
    Dim bAdjustToBBANSweden As Boolean = False
    Dim bAdjustToBBANDenmark As Boolean = False
    Dim bUseDifferentInvoiceAndPaymentCurrency As Boolean = False

    ' 20.02.2017 added next 3
    Dim bISOSplitOCR As Boolean = False
    Dim bISOSplitOCRWithCredits As Boolean = False
    Dim bISOSRedoFreetextToStruct As Boolean = False
    ' added 24.04.2017
    Dim bISODoNotGroup As Boolean = False
    Dim bISORemoveBICInSEPA As Boolean = False
    Dim bISOUseTransferCurrency As Boolean = False  ' 25.01.2018

	'********* START PROPERTY SETTINGS ***********************
	Public Property Accounts() As vbbabel.Accounts
		Get
			If oAccounts Is Nothing Then
				oAccounts = New Accounts
			End If
			Accounts = oAccounts
		End Get
		Set(ByVal Value As vbbabel.Accounts)
			oAccounts = Value
		End Set
	End Property
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	Public Property ClientFormat_ID() As Double
		Get
			ClientFormat_ID = nCLientFormat_ID
		End Get
		Set(ByVal Value As Double)
			nCLientFormat_ID = Value
		End Set
	End Property
	Public Property Client_ID() As Double
		Get
			Client_ID = nClient_ID
		End Get
		Set(ByVal Value As Double)
			nClient_ID = Value
		End Set
	End Property
	Public Property FormatIn() As Short
		Get
			FormatIn = iFormatIn
		End Get
		Set(ByVal Value As Short)
			iFormatIn = Value
		End Set
	End Property
	Public Property FormatOut() As Short
		Get
			FormatOut = iFormatOut
		End Get
		Set(ByVal Value As Short)
			iFormatOut = Value
		End Set
	End Property
	Public Property SeqNoTotal() As Double
		Get
			SeqNoTotal = nSeqnoTotal
		End Get
		Set(ByVal Value As Double)
			nSeqnoTotal = Value
		End Set
	End Property
	Public Property OwnRefText() As String
		Get
			OwnRefText = sOwnRefText
		End Get
		Set(ByVal Value As String)
			sOwnRefText = Value
		End Set
	End Property
	Public Property Outgoing() As Boolean
		Get
			Outgoing = bOutgoing
		End Get
		Set(ByVal Value As Boolean)
			bOutgoing = Value
		End Set
	End Property
	Public Property Name() As String
		Get
			Name = sName
		End Get
		Set(ByVal Value As String)
			sName = Value
		End Set
	End Property
	Public Property City() As String
		Get
			City = sCity
		End Get
		Set(ByVal Value As String)
			sCity = Value
		End Set
	End Property
	Public Property CountryCode() As String
		Get
			CountryCode = sCountryCode
		End Get
		Set(ByVal Value As String)
			sCountryCode = Value
		End Set
	End Property
	Public Property CompNo() As String
		Get
			CompNo = sCompNo
		End Get
		Set(ByVal Value As String)
			sCompNo = Value
		End Set
	End Property
	Public Property Division() As String
		Get
			Division = sDivision
		End Get
		Set(ByVal Value As String)
			sDivision = Value
		End Set
	End Property
	Public Property ClientNo() As String
		Get
			ClientNo = sClientNo
		End Get
		Set(ByVal Value As String)
			sClientNo = Value
		End Set
	End Property
	Public Property Notification1() As Short
		Get
			Notification1 = iNotification1
		End Get
		Set(ByVal Value As Short)
			iNotification1 = Value
		End Set
	End Property
	Public Property Notification2() As Short
		Get
			Notification2 = iNotification2
		End Get
		Set(ByVal Value As Short)
			iNotification2 = Value
		End Set
	End Property
	Public Property Notification3() As Short
		Get
			Notification3 = iNotification3
		End Get
		Set(ByVal Value As Short)
			iNotification3 = Value
		End Set
	End Property
	Public Property ImportKID1() As String
		Get
			ImportKID1 = sImportKID1
		End Get
		Set(ByVal Value As String)
			sImportKID1 = Value
		End Set
	End Property
	Public Property ImportKID2() As String
		Get
			ImportKID2 = sImportKID2
		End Get
		Set(ByVal Value As String)
			sImportKID2 = Value
		End Set
	End Property
	Public Property ImportKID3() As String
		Get
			ImportKID3 = sImportKID3
		End Get
		Set(ByVal Value As String)
			sImportKID3 = Value
		End Set
	End Property
	Public Property ExportKID() As String
		Get
			ExportKID = sExportKID
		End Get
		Set(ByVal Value As String)
			sExportKID = Value
		End Set
	End Property
	Public Property NoMatchAccount() As String
		Get
			NoMatchAccount = sNoMatchAccount
		End Get
		Set(ByVal Value As String)
			sNoMatchAccount = Value
		End Set
	End Property
	Public Property NoMatchAccountType() As Short
		Get
			NoMatchAccountType = iNoMatchAccountType
		End Get
		Set(ByVal Value As Short)
			iNoMatchAccountType = Value
		End Set
	End Property
	Public Property RoundingsAccount() As String
		Get
			RoundingsAccount = sRoundingsAccount
		End Get
		Set(ByVal Value As String)
			sRoundingsAccount = Value
		End Set
	End Property
	Public Property RoundingsAccountType() As Short
		Get
			RoundingsAccountType = iRoundingsAccountType
		End Get
		Set(ByVal Value As Short)
			iRoundingsAccountType = Value
		End Set
	End Property
	Public Property ChargesAccount() As String
		Get
			ChargesAccount = sChargesAccount
		End Get
		Set(ByVal Value As String)
			sChargesAccount = Value
		End Set
	End Property
	Public Property ChargesAccountType() As Short
		Get
			ChargesAccountType = iChargesAccountType
		End Get
		Set(ByVal Value As Short)
			iChargesAccountType = Value
		End Set
	End Property
	Public Property AkontoPattern() As String
		Get
			AkontoPattern = sAkontoPattern
		End Get
		Set(ByVal Value As String)
			sAkontoPattern = Value
		End Set
	End Property
	Public Property AkontoPatternType() As Short
		Get
			AkontoPatternType = iAkontoPatternType
		End Get
		Set(ByVal Value As Short)
			iAkontoPatternType = Value
		End Set
	End Property
	Public Property MatchPattern() As String
		Get
			MatchPattern = sMatchPattern
		End Get
		Set(ByVal Value As String)
			sMatchPattern = Value
		End Set
	End Property
	Public Property MatchPatternType() As Short
		Get
			MatchPatternType = iMatchPatternType
		End Get
		Set(ByVal Value As Short)
			iMatchPatternType = Value
		End Set
	End Property
	Public Property GLPattern() As String
		Get
			GLPattern = sGLPattern
		End Get
		Set(ByVal Value As String)
			sGLPattern = Value
		End Set
	End Property
	Public Property VoucherNo() As String
		Get
			VoucherNo = sVoucherNo
		End Get
		Set(ByVal Value As String)
			sVoucherNo = Value
		End Set
	End Property
	Public Property VoucherNo1Changed() As Boolean
		Get
			VoucherNo1Changed = bVoucherNo1Changed
		End Get
		Set(ByVal Value As Boolean)
			bVoucherNo1Changed = Value
		End Set
	End Property
	Public Property AddVoucherNoBeforeOCR() As Boolean
		Get
			AddVoucherNoBeforeOCR = bAddVoucherNoBeforeOCR
		End Get
		Set(ByVal Value As Boolean)
			bAddVoucherNoBeforeOCR = Value
		End Set
	End Property
	Public Property AddCounterToVoucherNo() As Boolean
		Get
			AddCounterToVoucherNo = bAddCounterToVoucherNo
		End Get
		Set(ByVal Value As Boolean)
			bAddCounterToVoucherNo = Value
		End Set
	End Property
	Public Property VoucherType() As String
		Get
			VoucherType = sVoucherType
		End Get
		Set(ByVal Value As String)
			sVoucherType = Value
		End Set
	End Property
	Public Property VoucherNo2() As String
		Get
			VoucherNo2 = sVoucherNo2
		End Get
		Set(ByVal Value As String)
			sVoucherNo2 = Value
		End Set
	End Property
	Public Property VoucherType2() As String
		Get
			VoucherType2 = sVoucherType2
		End Get
		Set(ByVal Value As String)
			sVoucherType2 = Value
		End Set
	End Property
	Public Property VoucherNo2Changed() As Boolean
		Get
			VoucherNo2Changed = bVoucherNo2Changed
		End Get
		Set(ByVal Value As Boolean)
			bVoucherNo2Changed = Value
		End Set
	End Property
	Public Property HowToAddVoucherNo() As Short
		Get
			HowToAddVoucherNo = iHowToAddVoucherNo
		End Get
		Set(ByVal Value As Short)
			iHowToAddVoucherNo = Value
		End Set
	End Property
	Public Property tmpFlag() As Short
		Get
			tmpFlag = itmpFlag
		End Get
		Set(ByVal Value As Short)
			itmpFlag = Value
		End Set
	End Property
	Public Property GLPatternType() As Short
		Get
			GLPatternType = iGLPatternType
		End Get
		Set(ByVal Value As Short)
			iGLPatternType = Value
		End Set
	End Property
	Public Property KIDStart() As Short
		Get
			KIDStart = iKIDStart
		End Get
		Set(ByVal Value As Short)
			iKIDStart = Value
		End Set
	End Property
	Public Property KIDLength() As Short
		Get
			KIDLength = iKIDLength
		End Get
		Set(ByVal Value As Short)
			iKIDLength = Value
		End Set
	End Property
	Public Property KIDValue() As String
		Get
			KIDValue = sKIDValue
		End Get
		Set(ByVal Value As String)
			sKIDValue = Value
		End Set
	End Property
	Public Property DBProfile_ID() As Short
		Get
			DBProfile_ID = iDBProfile_ID
		End Get
		Set(ByVal Value As Short)
			iDBProfile_ID = Value
		End Set
	End Property
    Public Property Status() As vbBabel.Profile.CollectionStatus
        Get
            Status = eStatus
        End Get
        Set(ByVal Value As vbBabel.Profile.CollectionStatus)
            If CDbl(Value) = 0 Then
                eStatus = Profile.CollectionStatus.NoChange
            End If
            If CDbl(Value) = 1 Then '1=SeqNoChange
                If eStatus = Profile.CollectionStatus.NoChange Then
                    eStatus = Profile.CollectionStatus.SeqNoChange
                ElseIf eStatus = Profile.CollectionStatus.ChangeUnder Then
                    eStatus = Profile.CollectionStatus.Changed
                End If
            End If
            If CDbl(Value) = 2 Then '2=ChangeUnder
                If eStatus = Profile.CollectionStatus.NoChange Then
                    eStatus = Profile.CollectionStatus.ChangeUnder
                ElseIf eStatus = Profile.CollectionStatus.SeqNoChange Then
                    eStatus = Profile.CollectionStatus.Changed
                End If
            End If
            If Value > eStatus Then
                eStatus = Value
            End If
        End Set
    End Property
	Public Property BB_Year() As Short
		Get
			BB_Year = iBB_Year
		End Get
		Set(ByVal Value As Short)
			iBB_Year = Value
		End Set
	End Property
	Public Property BB_Period() As Short
		Get
			BB_Period = iBB_Period
		End Get
		Set(ByVal Value As Short)
			iBB_Period = Value
		End Set
    End Property
    ' added 09.07.2015
    Public Property HistoryDelDays() As Integer
        Get
            HistoryDelDays = iHistoryDelDays
        End Get
        Set(ByVal value As Integer)
            iHistoryDelDays = value
        End Set
    End Property
    ' added 28.12.2021
    Public Property SocialSecurityNo() As String
        Get
            SocialSecurityNo = sSocialSecurityNo
        End Get
        Set(ByVal Value As String)
            sSocialSecurityNo = Value
        End Set
    End Property

    Public Property UseStateBankText() As Boolean
        Get
            UseStateBankText = bUseStateBankText
        End Get
        Set(ByVal value As Boolean)
            bUseStateBankText = value
        End Set
    End Property
    Public Property SEPASingle() As Boolean
        Get
            SEPASingle = bSEPASingle
        End Get
        Set(ByVal value As Boolean)
            bSEPASingle = value
        End Set
    End Property
    Public Property EndToEndRefFromBabelBank() As Boolean
        Get
            EndToEndRefFromBabelBank = bEndToEndRefFromBabelBank
        End Get
        Set(ByVal value As Boolean)
            bEndToEndRefFromBabelBank = value
        End Set
    End Property
    Public Property SavePaymentData() As Boolean
        Get
            SavePaymentData = bSavePaymentData
        End Get
        Set(ByVal value As Boolean)
            bSavePaymentData = value
        End Set
    End Property
    Public Property MsgID_Variable() As String
        Get
            MsgID_Variable = sMsgID_Variable
        End Get
        Set(ByVal Value As String)
            sMsgID_Variable = Value
        End Set
    End Property
    Public Property ValidationLevel() As Integer
        Get
            ValidationLevel = iValidationLevel
        End Get
        Set(ByVal Value As Integer)
            iValidationLevel = Value
        End Set
    End Property
    Public Property AdjustForSchemaValidation() As Boolean
        Get
            AdjustForSchemaValidation = bAdjustForSchemaValidation
        End Get
        Set(ByVal value As Boolean)
            bAdjustForSchemaValidation = value
        End Set
    End Property
    Public Property AdjustToBBANNorway() As Boolean
        Get
            AdjustToBBANNorway = bAdjustToBBANNorway
        End Get
        Set(ByVal value As Boolean)
            bAdjustToBBANNorway = value
        End Set
    End Property
    Public Property AdjustToBBANSweden() As Boolean
        Get
            AdjustToBBANSweden = bAdjustToBBANSweden
        End Get
        Set(ByVal value As Boolean)
            bAdjustToBBANSweden = value
        End Set
    End Property
    Public Property AdjustToBBANDenmark() As Boolean
        Get
            AdjustToBBANDenmark = bAdjustToBBANDenmark
        End Get
        Set(ByVal value As Boolean)
            bAdjustToBBANDenmark = value
        End Set
    End Property
    Public Property UseDifferentInvoiceAndPaymentCurrency() As Boolean
        Get
            UseDifferentInvoiceAndPaymentCurrency = bUseDifferentInvoiceAndPaymentCurrency
        End Get
        Set(ByVal value As Boolean)
            bUseDifferentInvoiceAndPaymentCurrency = value
        End Set
    End Property
    Public Property ISOSplitOCR() As Boolean
        Get
            ISOSplitOCR = bISOSplitOCR
        End Get
        Set(ByVal value As Boolean)
            bISOSplitOCR = value
        End Set
    End Property
    Public Property ISOSplitOCRWithCredits() As Boolean
        Get
            ISOSplitOCRWithCredits = bISOSplitOCRWithCredits
        End Get
        Set(ByVal value As Boolean)
            bISOSplitOCRWithCredits = value
        End Set
    End Property
    Public Property ISOSRedoFreetextToStruct() As Boolean
        Get
            ISOSRedoFreetextToStruct = bISOSRedoFreetextToStruct
        End Get
        Set(ByVal value As Boolean)
            bISOSRedoFreetextToStruct = value
        End Set
    End Property
    Public Property ISODoNotGroup() As Boolean
        Get
            ISODoNotGroup = bISODoNotGroup
        End Get
        Set(ByVal value As Boolean)
            bISODoNotGroup = value
        End Set
    End Property
    Public Property ISORemoveBICInSEPA() As Boolean
        Get
            ISORemoveBICInSEPA = bISORemoveBICInSEPA
        End Get
        Set(ByVal value As Boolean)
            bISORemoveBICInSEPA = value
        End Set
    End Property
    Public Property ISOUseTransferCurrency() As Boolean
        Get
            ISOUseTransferCurrency = bISOUseTransferCurrency
        End Get
        Set(ByVal value As Boolean)
            bISOUseTransferCurrency = value
        End Set
    End Property



	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		nCLientFormat_ID = -1
		nClient_ID = -1
		iFormatIn = 0
		iFormatOut = 0
        sOwnRefText = ""
        bOutgoing = False
        sName = ""
        sCity = ""
        sCountryCode = ""
        sCompNo = ""
        sDivision = ""
        sClientNo = ""
        iNotification1 = -1
        iNotification2 = -1
        iNotification3 = -1
        sImportKID1 = ""
        sImportKID2 = ""
        sImportKID3 = ""
        sExportKID = ""
        sNoMatchAccount = ""
        iNoMatchAccountType = -1
        sRoundingsAccount = ""
        iRoundingsAccountType = -1
        sChargesAccount = ""
        iChargesAccountType = -1
        sAkontoPattern = ""
        iAkontoPatternType = -1
        sMatchPattern = ""
        iMatchPatternType = -1
        sGLPattern = ""
        sVoucherNo = ""
        sVoucherType = ""
        sVoucherNo2 = ""
        sVoucherType2 = ""
        iHowToAddVoucherNo = -1
        itmpFlag = 0
        bVoucherNo1Changed = False
        bVoucherNo2Changed = False
        bAddVoucherNoBeforeOCR = False
        bAddCounterToVoucherNo = False
        iBB_Year = Year(Date.Today)
        iBB_Period = Month(Date.Today)

        iGLPatternType = -1
        iKIDStart = -1
        iKIDLength = -1
        sKIDValue = ""
        iDBProfile_ID = -1
        eStatus = 0

        ' 09.07.2015
        iHistoryDelDays = 90
        bSEPASingle = False
        bUseStateBankText = False
        bEndToEndRefFromBabelBank = True
        sMsgID_Variable = ""
        SocialSecurityNo = ""
        iValidationLevel = 1
        bSavePaymentData = True
        bAdjustForSchemaValidation = False
        bAdjustToBBANNorway = False
        bAdjustToBBANSweden = False
        bAdjustToBBANDenmark = False
        bUseDifferentInvoiceAndPaymentCurrency = False

        bISOSplitOCR = False
        bISOSplitOCRWithCredits = False
        bISOSRedoFreetextToStruct = False
        bISODoNotGroup = False
        bISORemoveBICInSEPA = False
        bISOUseTransferCurrency = False

        ' When vbbabel.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object oAccounts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oAccounts = Nothing
    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub

    Public Function Load(ByRef iCompanyNo As Short, ByRef myBBDB_AccessClientReader As System.Data.OleDb.OleDbDataReader, ByRef myBBDB_AccessConnection As System.Data.OleDb.OleDbConnection) As Boolean
        Dim sMySQL As String
        Dim oaccount As Account
        Dim bAccount As Boolean
        Dim bInvoiceDesc As Boolean
        Dim bGeneralNote As Boolean
        Dim sFieldname As String
        Dim sSelectPart As String
        Dim myBBDB_AccessAccountReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessAccountCommand As System.Data.OleDb.OleDbCommand = Nothing

        nCLientFormat_ID = myBBDB_AccessClientReader.GetInt32(0)
        nClient_ID = myBBDB_AccessClientReader.GetInt32(1)
        sClientNo = myBBDB_AccessClientReader.GetString(2)
        If Not myBBDB_AccessClientReader.IsDBNull(3) Then '20.12.2017 - Added IsDBNull-test
            iFormatIn = myBBDB_AccessClientReader.GetInt32(3)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(4) Then '20.12.2017 - Added IsDBNull-test
            iFormatOut = myBBDB_AccessClientReader.GetInt32(4)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(5) Then
            nSeqnoTotal = myBBDB_AccessClientReader.GetInt32(5)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(6) Then
            sOwnRefText = Trim(myBBDB_AccessClientReader.GetString(6))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(7) Then
            bOutgoing = myBBDB_AccessClientReader.GetBoolean(7)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(8) Then
            sName = myBBDB_AccessClientReader.GetString(8)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(9) Then
            sCity = myBBDB_AccessClientReader.GetString(9)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(10) Then
            sCountryCode = myBBDB_AccessClientReader.GetString(10)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(11) Then
            sCompNo = myBBDB_AccessClientReader.GetString(11)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(12) Then
            sDivision = myBBDB_AccessClientReader.GetString(12)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(13) Then
            iNotification1 = myBBDB_AccessClientReader.GetInt16(13)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(14) Then
            iNotification2 = myBBDB_AccessClientReader.GetInt16(14)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(15) Then
            iNotification3 = myBBDB_AccessClientReader.GetInt16(15)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(16) Then
            sImportKID1 = myBBDB_AccessClientReader.GetString(16)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(17) Then
            sImportKID2 = myBBDB_AccessClientReader.GetString(17)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(18) Then
            sImportKID3 = myBBDB_AccessClientReader.GetString(18)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(19) Then
            sExportKID = myBBDB_AccessClientReader.GetString(19)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(20) Then
            sNoMatchAccount = myBBDB_AccessClientReader.GetString(20)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(21) Then
            iNoMatchAccountType = myBBDB_AccessClientReader.GetInt32(21)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(22) Then
            sRoundingsAccount = myBBDB_AccessClientReader.GetString(22)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(23) Then
            iRoundingsAccountType = myBBDB_AccessClientReader.GetInt32(23)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(24) Then
            sChargesAccount = myBBDB_AccessClientReader.GetString(24)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(25) Then
            iChargesAccountType = myBBDB_AccessClientReader.GetInt32(25)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(26) Then
            sAkontoPattern = myBBDB_AccessClientReader.GetString(26)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(27) Then
            iAkontoPatternType = myBBDB_AccessClientReader.GetInt32(27)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(28) Then
            sMatchPattern = myBBDB_AccessClientReader.GetString(28)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(29) Then
            iMatchPatternType = myBBDB_AccessClientReader.GetInt32(29)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(30) Then
            sGLPattern = myBBDB_AccessClientReader.GetString(30)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(31) Then
            iGLPatternType = myBBDB_AccessClientReader.GetInt32(31)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(32) Then
            sVoucherNo = myBBDB_AccessClientReader.GetString(32)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(33) Then
            sVoucherType = myBBDB_AccessClientReader.GetString(33)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(34) Then
            sVoucherNo2 = myBBDB_AccessClientReader.GetString(34)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(35) Then
            sVoucherType2 = myBBDB_AccessClientReader.GetString(35)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(36) Then
            iHowToAddVoucherNo = myBBDB_AccessClientReader.GetInt32(36)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(37) Then
            bAddVoucherNoBeforeOCR = myBBDB_AccessClientReader.GetBoolean(37)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(38) Then
            bAddCounterToVoucherNo = myBBDB_AccessClientReader.GetBoolean(38)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(39) Then
            iDBProfile_ID = myBBDB_AccessClientReader.GetInt32(39)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(40) Then
            iKIDStart = myBBDB_AccessClientReader.GetInt32(40)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(41) Then
            iKIDLength = myBBDB_AccessClientReader.GetInt32(41)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(42) Then
            sKIDValue = myBBDB_AccessClientReader.GetString(42)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(43) Then
            iBB_Year = myBBDB_AccessClientReader.GetInt32(43)
        Else
            iBB_Year = Year(Date.Today)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(44) Then
            iBB_Period = myBBDB_AccessClientReader.GetInt32(44)
        Else
            iBB_Period = Month(Date.Today)
        End If


        ' added 09.07.2015
        If Not myBBDB_AccessClientReader.IsDBNull(45) Then
            iHistoryDelDays = Trim(myBBDB_AccessClientReader.GetInt32(45))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(46) Then
            bSEPASingle = Trim(myBBDB_AccessClientReader.GetBoolean(46))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(47) Then
            bEndToEndRefFromBabelBank = Trim(myBBDB_AccessClientReader.GetBoolean(47))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(48) Then
            sMsgID_Variable = Trim(myBBDB_AccessClientReader.GetString(48))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(49) Then
            iValidationLevel = Trim(myBBDB_AccessClientReader.GetInt32(49))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(50) Then
            bSavePaymentData = Trim(myBBDB_AccessClientReader.GetBoolean(50))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(51) Then
            bUseStateBankText = Trim(myBBDB_AccessClientReader.GetBoolean(51))
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(52) Then
            bAdjustForSchemaValidation = myBBDB_AccessClientReader.GetBoolean(52)
        End If

        If Not myBBDB_AccessClientReader.IsDBNull(53) Then
            bAdjustToBBANNorway = myBBDB_AccessClientReader.GetBoolean(53)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(54) Then
            bAdjustToBBANSweden = myBBDB_AccessClientReader.GetBoolean(54)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(55) Then
            bAdjustToBBANDenmark = myBBDB_AccessClientReader.GetBoolean(55)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(56) Then
            bUseDifferentInvoiceAndPaymentCurrency = myBBDB_AccessClientReader.GetBoolean(56)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(57) Then
            bISOSplitOCR = myBBDB_AccessClientReader.GetBoolean(57)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(58) Then
            bISOSplitOCRWithCredits = myBBDB_AccessClientReader.GetBoolean(58)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(59) Then
            bISOSRedoFreetextToStruct = myBBDB_AccessClientReader.GetBoolean(59)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(60) Then
            bISODoNotGroup = myBBDB_AccessClientReader.GetBoolean(60)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(61) Then
            bISORemoveBICInSEPA = myBBDB_AccessClientReader.GetBoolean(61)
        End If
        If Not myBBDB_AccessClientReader.IsDBNull(62) Then
            bISOUseTransferCurrency = myBBDB_AccessClientReader.GetBoolean(62)
        End If
        ' 28.12.2021
        If Not myBBDB_AccessClientReader.IsDBNull(63) Then
            sSocialSecurityNo = Trim(myBBDB_AccessClientReader.GetString(63))
        End If

        sSelectPart = ""
        sSelectPart = "SELECT Account_ID, Client_ID, Account, GLAccount, GLResultsAccount, ContractNo, ConvertedAccount, AccountCountryCode, "
        sSelectPart = sSelectPart & "SWIFTAddr, AvtaleGiroID, CurrencyCode, DebitAccountCountryCode, Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10 "

        sMySQL = sSelectPart & "FROM Account where Company_ID=" & Str(iCompanyNo) & " and Client_ID=" & Client_ID & " ORDER BY Account_ID"

        If myBBDB_AccessAccountCommand Is Nothing Then
            myBBDB_AccessAccountCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessAccountCommand.CommandType = CommandType.Text
            myBBDB_AccessAccountCommand.Connection = myBBDB_AccessConnection
        End If

        If Not myBBDB_AccessAccountReader Is Nothing Then
            If Not myBBDB_AccessAccountReader.IsClosed Then
                myBBDB_AccessAccountReader.Close()
            End If
        End If

        myBBDB_AccessAccountCommand.CommandText = sMySQL

        myBBDB_AccessAccountReader = myBBDB_AccessAccountCommand.ExecuteReader

        oAccounts = New Accounts
        If myBBDB_AccessAccountReader.HasRows Then
            Do While myBBDB_AccessAccountReader.Read()
                'Make an Account-object
                oaccount = oAccounts.Add(myBBDB_AccessAccountReader.GetInt32(0).ToString) 'Account_ID
                bAccount = oaccount.Load(iCompanyNo, myBBDB_AccessAccountReader)
            Loop
        End If

        If Not myBBDB_AccessAccountReader Is Nothing Then
            If Not myBBDB_AccessAccountReader.IsClosed Then
                myBBDB_AccessAccountReader.Close()
            End If
            myBBDB_AccessAccountReader = Nothing
        End If
        If Not myBBDB_AccessAccountCommand Is Nothing Then
            myBBDB_AccessAccountCommand.Dispose()
            myBBDB_AccessAccountCommand = Nothing
        End If

        Load = True

    End Function
End Class
