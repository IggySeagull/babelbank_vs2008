Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_Sub_Freetext
    Dim bReportFromDatabase As Boolean
    Dim sText As String
    Dim oInvoice As vbBabel.Invoice
    Dim bEmptyReport As Boolean
    Dim nCounter As Double
    Dim sLabel As String
    Dim bShowLabel As Boolean
    Dim bGrey As Boolean = False

    Public Sub SetColorToGrey(ByVal b As Boolean)
        bGrey = b
    End Sub
    Private Sub rp_Sub_Freetext_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
    End Sub
    Private Sub rp_Sub_Freetext_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        Dim oFreetext As vbBabel.Freetext
        Dim lFreetexts As Long

        'For future use
        'If bForeColorGrey Then
        '    Me.txtFreetext.ForeColor = System.Drawing.Color.Gray
        'Else
        '    Me.txtFreetext.ForeColor = System.Drawing.Color.Black
        'End If

        If bReportFromDatabase Then
            If nCounter = 0 Then
                If InStr(Me.DataSource.sql, "Label", CompareMethod.Text) > 0 Then
                    If Not Me.Fields("Label").Value Is Nothing Then
                        If Me.Fields("Label").Value.ToString.Length > 0 Then
                            Me.txtLabel.Visible = True
                            'Me.txtLabel.Value = sLabel
                        Else
                            Me.txtLabel.Visible = False
                        End If
                    Else
                        Me.txtLabel.Visible = False
                    End If
                Else
                    Me.txtLabel.Visible = False
                End If
                Me.Fields("Freetext").Value = RetrieveIllegalCharactersInString(Me.Fields("Freetext").Value)
            Else
                'If bShowLabel Then
                'Me.txtLabel.Visible = True
                'Else
                Me.txtLabel.Visible = False
                Me.Fields("Freetext").Value = RetrieveIllegalCharactersInString(Me.Fields("Freetext").Value)

                'End If
            End If
        Else
            If bShowLabel Then
                Me.txtLabel.Value = sLabel
            Else
                Me.txtLabel.Value = ""
                Me.txtLabel.Visible = False
            End If
            Me.txtFreetext.Value = sText
        End If

        If bGrey Then
            Me.txtFreetext.ForeColor = System.Drawing.Color.Gray
        Else
            Me.txtFreetext.ForeColor = System.Drawing.Color.Black
        End If

        nCounter = nCounter + 1

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        bGrey = False
        bReportFromDatabase = True
        sText = ""
        nCounter = 0
        bshowlabel = False

    End Sub
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public WriteOnly Property Label() As String
        Set(ByVal Value As String)
            sLabel = Value
            If Not EmptyString(sLabel) Then
                bShowLabel = True
            End If
        End Set
    End Property
    Public WriteOnly Property Text() As String
        Set(ByVal Value As String)
            sText = Value
        End Set
    End Property
    Public WriteOnly Property Invoice() As vbBabel.Invoice
        Set(ByVal Value As vbBabel.Invoice)
            oInvoice = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property

End Class
