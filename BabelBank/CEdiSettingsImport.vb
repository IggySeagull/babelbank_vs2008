Option Strict Off
Option Explicit On
Friend Class CEdiSettingsImport
	
	Private mrAppAdmImport As CAppAdmImport
	Private mrXmlDoc As New MSXML2.DOMDocument40
	Private mrCurrentElement As MSXML2.IXMLDOMElement
	Private mrXMLTagSettings As New MSXML2.DOMDocument40
	Private sMappingFile As String
	Private sEDIFormat As String
	
	Private msSegmentStart As String
	Private msSegmentSep As String
	Private miSegmentLen As Short
	Private msEscapeChar As String
	Private msElementSep As String
	Private msGroupSep As String
	Private msDecimalSep As String
    Private aGroupRep() As Double ' 13.05.2020 changed from Short  
	Private aGroupMaxRep(,) AS String
	'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
	'DOTNETT REDIM Private aGroupMaxRep() As String
	Private iSegmentRepetition As Short
	Private iLevelChange As Short
	Private bGroupChange As Boolean
	Private lSegmentCount As Integer
	Private sFunctionalGroupRefNo, sMessageRefNo, sInterchangeRefNo As String
	Private iLastUsedGroup As Short
	Private lLINCounter, lSEQCounter As Integer
    Private eSendingBank As vbBabel.BabelFiles.Bank

    Private i2Pointer, i1Pointer, i3Pointer As Short
    Private i5Pointer, i4Pointer, i6Pointer As Short
    Private eleLINElementParent As MSXML2.IXMLDOMElement
    Private eleSEQElementParent As MSXML2.IXMLDOMElement
    Private eleDOCElementParent As MSXML2.IXMLDOMElement
    Private eleLINElement As MSXML2.IXMLDOMElement
    Private eleSEQElement As MSXML2.IXMLDOMElement
    Private eleDOCElement As MSXML2.IXMLDOMElement
    Private eleBasicMappingElement As MSXML2.IXMLDOMElement
    '********* START PROPERTY SETTINGS ***********************
    ' vb.net: dette ser rart ut. ser ut som om det alltid sendes en integer til denne let-en
    ' skal vel da st�:
    'Public Property Let SegmentCount(l As Long)
    '    lSegmentCount = l
    'End Property
    '18.08.2008 Change done
    Public Property SegmentCount() As Integer
        Get
            SegmentCount = lSegmentCount
        End Get
        Set(ByVal Value As Integer)
            lSegmentCount = Value
        End Set
    End Property
    Public Property MessageRefNo() As String
        Get
            MessageRefNo = sMessageRefNo
        End Get
        Set(ByVal Value As String)
            sMessageRefNo = Value
        End Set
    End Property
    Public Property FunctionalGroupRefNo() As String
        Get
            FunctionalGroupRefNo = sFunctionalGroupRefNo
        End Get
        Set(ByVal Value As String)
            sFunctionalGroupRefNo = Value
        End Set
    End Property
    Public WriteOnly Property BankByCode() As vbBabel.BabelFiles.Bank
        Set(ByVal Value As vbBabel.BabelFiles.Bank)
            'UPGRADE_WARNING: Couldn't resolve default property of object s. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            eSendingBank = Val(Value)
        End Set
    End Property
    Public Property InterchangeRefNo() As String
        Get
            InterchangeRefNo = sInterchangeRefNo
        End Get
        Set(ByVal Value As String)
            sInterchangeRefNo = Value
        End Set
    End Property
    Public ReadOnly Property CurrentElement() As MSXML2.IXMLDOMElement
        Get
            CurrentElement = mrCurrentElement
        End Get
    End Property

    Public Property DecimalSep() As String
        Get
            ' 30.05.2008 by JanP
            ' When . was set as decimalsep in the mappingfile, it was NOT read!
            ' This is because we always set it as , in initialize
            ' Then the next test is always false
            ' I have commented it out. DO WE DARE TO DO SO????

            If msDecimalSep = "" Then
                msDecimalSep = GetAttrib("//*/HEADER/ATTRIBUTES", "decimalsep")
            End If

            DecimalSep = msDecimalSep
        End Get
        Set(ByVal Value As String)
            msDecimalSep = Value
        End Set
    End Property

    Public Property ElementSep() As String
        Get
            If msElementSep = "" Then
                msElementSep = GetAttrib("//*/HEADER/ATTRIBUTES", "elementsep")
            End If
            ElementSep = msElementSep

        End Get
        Set(ByVal Value As String)
            msElementSep = Value
        End Set
    End Property

    Public Property EscapeChar() As String
        Get
            If msEscapeChar = "" Then
                msEscapeChar = GetAttrib("//*/HEADER/ATTRIBUTES", "escape")
            End If
            EscapeChar = msEscapeChar
        End Get
        Set(ByVal Value As String)
            msEscapeChar = Value
        End Set
    End Property
    'Public Property Get SegmentInfo() As String()
    '    SegmentInfo = aSegmentInfo
    'End Property
    Public ReadOnly Property LevelChange() As Short
        Get
            LevelChange = iLevelChange
        End Get
    End Property
    Public Property GroupChange() As Boolean
        Get
            GroupChange = bGroupChange
        End Get
        Set(ByVal Value As Boolean)
            bGroupChange = Value
        End Set
    End Property
    Public Property LINCounter() As Integer
        Get
            LINCounter = lLINCounter
        End Get
        Set(ByVal Value As Integer)
            lLINCounter = Value
        End Set
    End Property
    Public ReadOnly Property SEQCounter() As Integer
        Get
            SEQCounter = lSEQCounter
        End Get
    End Property
    Public ReadOnly Property XMLTagSettings() As MSXML2.DOMDocument40
        Get
            XMLTagSettings = mrXMLTagSettings
        End Get
    End Property
    Public Property SegmentSep() As String
        Get
            If msSegmentSep = "" Then
                msSegmentSep = GetAttrib("//*/HEADER/ATTRIBUTES", "segmentsep")
            End If
            SegmentSep = msSegmentSep
        End Get
        Set(ByVal Value As String)
            msSegmentSep = Value
        End Set
    End Property
    Public ReadOnly Property XMLDoc() As MSXML2.DOMDocument26
        Get
            XMLDoc = mrXmlDoc
        End Get
    End Property
    Public Property GroupSep() As String
        Get
            If msGroupSep = "" Then
                msGroupSep = GetAttrib("//*/HEADER/ATTRIBUTES", "groupsep")
            End If
            GroupSep = msGroupSep
        End Get
        Set(ByVal Value As String)
            msGroupSep = Value
        End Set
    End Property
    Public ReadOnly Property SegmentLength() As Short
        Get
            If miSegmentLen = 0 Then
                miSegmentLen = Val(GetAttrib("//*/HEADER/ATTRIBUTES", "segmentlen"))
            End If
            SegmentLength = miSegmentLen
        End Get
    End Property
    Public ReadOnly Property Start() As String
        Get
            If msSegmentStart = "" Then
                msSegmentStart = GetAttrib("//*/HEADER", "start")
            End If
            Start = msSegmentStart
        End Get
    End Property
    '********* END PROPERTY SETTINGS ***********************

    Friend Function InitStdElements(ByRef sMessageType As String) As Boolean
        'Sets some basic nodes that we use in FindNextSegment

        If sMessageType = "COACSU" Then
            eleDOCElement = mrXmlDoc.documentElement.selectSingleNode("//DOC")
            If Not eleDOCElement Is Nothing Then
                eleDOCElementParent = mrXmlDoc.documentElement.selectSingleNode("//DOC").parentNode
            End If
        ElseIf sMessageType <> "CONTRL" Then
            eleLINElement = mrXmlDoc.documentElement.selectSingleNode("//LIN")
            eleSEQElement = mrXmlDoc.documentElement.selectSingleNode("//SEQ")
            eleDOCElement = mrXmlDoc.documentElement.selectSingleNode("//DOC")
            eleLINElementParent = mrXmlDoc.documentElement.selectSingleNode("//LIN").parentNode
            eleSEQElementParent = mrXmlDoc.documentElement.selectSingleNode("//SEQ").parentNode
            'New IF 17.02.2008 to use with BANSTA
            If Not eleDOCElement Is Nothing Then
                eleDOCElementParent = mrXmlDoc.documentElement.selectSingleNode("//DOC").parentNode
            End If
        End If

        'Set the basic node
        eleBasicMappingElement = mrXmlDoc.documentElement.selectSingleNode("//GROUPS/UNB/UNG/GROUP")

        'FIX: BabelERROR: If not found

        InitStdElements = True

    End Function
    Friend Function KillStdElements() As Boolean
        'Sets some basic nodes that we use in FindNextSegment

        'UPGRADE_NOTE: Object eleLINElementParent may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleLINElementParent = Nothing
        'UPGRADE_NOTE: Object eleSEQElementParent may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleSEQElementParent = Nothing
        'UPGRADE_NOTE: Object eleDOCElementParent may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleDOCElementParent = Nothing
        'UPGRADE_NOTE: Object eleLINElement may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleLINElement = Nothing
        'UPGRADE_NOTE: Object eleSEQElement may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleSEQElement = Nothing
        'UPGRADE_NOTE: Object eleDOCElement may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleDOCElement = Nothing
        'Set the basic node
        'UPGRADE_NOTE: Object eleBasicMappingElement may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleBasicMappingElement = Nothing

        'UPGRADE_NOTE: Object mrAppAdmImport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mrAppAdmImport = Nothing
        'UPGRADE_NOTE: Object mrXmlDoc may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mrXmlDoc = Nothing

        KillStdElements = True

    End Function
    Friend Function ResetPointers() As Boolean
        i1Pointer = 0
        i2Pointer = 0
        i3Pointer = 0
        i4Pointer = 0
        i5Pointer = 0
        i6Pointer = 0
        ResetPointers = True
    End Function
    Public Function GetAttrib(ByRef sSelect As String, ByRef sName As String) As String
        Dim rElement As MSXML2.IXMLDOMElement
        Dim NodeAttribute As MSXML2.IXMLDOMNode

        rElement = mrXmlDoc.selectSingleNode(UCase(sSelect))

        NodeAttribute = rElement.Attributes.getNamedItem(sName)
        If NodeAttribute Is Nothing Then
            GetAttrib = ""
        Else
            'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            GetAttrib = NodeAttribute.nodeValue
        End If
    End Function
    Public Function GetElementNode(ByRef sSelect As String) As MSXML2.IXMLDOMElement
        Dim root As MSXML2.IXMLDOMElement
        Dim node As MSXML2.IXMLDOMElement

        root = mrXmlDoc.documentElement

        node = root.selectSingleNode(sSelect)

        GetElementNode = node
    End Function
    Private Function LoadXMLValues(ByRef sValues As String) As Boolean
        Dim bRet As Boolean
        Dim docFrag As MSXML2.IXMLDOMDocumentFragment
        Dim XMLDoc As New MSXML2.DOMDocument40
        Dim root As MSXML2.IXMLDOMElement
        Dim valNode As MSXML2.IXMLDOMElement
        Dim sErrorMsg As String = ""

        'FIX: Add errorhandling

        If sValues = "" Then
            'Impossible, but still
            err.Raise(15087, "LoadXMLValues", LRS(15087))
            '"No filepath for Values.XML are specified."
        End If

        On Error GoTo errorHandler

        bRet = XMLDoc.load(sValues)
        If Not bRet Then
            '11.02.2021 - Added ErrorHandling
            sErrorMsg = ""
            If Not EmptyString(XMLDoc.parseError.reason) Then
                sErrorMsg = XMLDoc.parseError.reason
                If Not EmptyString(XMLDoc.parseError.line) Then
                    sErrorMsg = sErrorMsg & vbCrLf & "Line: " & XMLDoc.parseError.line
                End If
                If Not EmptyString(XMLDoc.parseError.linepos) Then
                    sErrorMsg = sErrorMsg & vbCrLf & "Pos: " & XMLDoc.parseError.linepos
                End If
                If Not EmptyString(XMLDoc.parseError.url) Then
                    sErrorMsg = sErrorMsg & vbCrLf & "URL: " & XMLDoc.parseError.url
                End If
            End If
            '.ToString
            Err.Raise(15088, "LoadXMLValues", LRS(15088) & vbCrLf & sErrorMsg)
            'err.Raise 1, "LoadXMLValues", "Can't load Values.XML."
        End If

        root = XMLDoc.documentElement
        valNode = root.selectSingleNode("//VALUES")

        docFrag = mrXmlDoc.createDocumentFragment

        'UPGRADE_WARNING: Couldn't resolve default property of object valNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        docFrag.appendChild(valNode)

        'The VALUES node and subnodes are inserted as childnodes !!
        'UPGRADE_WARNING: Couldn't resolve default property of object docFrag. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        mrXmlDoc.documentElement.appendChild(docFrag)
        'mrXmlDoc.save App.path & "\zzz.xml"

        LoadXMLValues = True
        Exit Function

errorHandler:
        LoadXMLValues = False
        Err.Raise(Err.Number, "LoadXMLValues", Err.Description)

    End Function
    Friend Sub SetCurrentElement(ByRef sXpathString As String)
        'This will set the Currentnode to Group 0
        'UPGRADE_WARNING: Couldn't resolve default property of object sXpathString. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode(sXpathString)
        i1Pointer = 0
        i2Pointer = 0
        i3Pointer = 0
        i4Pointer = 0
        i5Pointer = 0
        i6Pointer = 0
    End Sub
    Friend Function Init(ByRef rAppAdmImport As CAppAdmImport, ByRef sXmlFile As String, ByRef sXMLTagSettingsFile As String, ByRef sFormat As String, Optional ByRef sValues As String = "") As Boolean
        Dim bRet As Boolean

        sMappingFile = sXmlFile
        mrAppAdmImport = rAppAdmImport
        mrXmlDoc.Load(sXmlFile)

        sEDIFormat = sFormat

        'FIX: Load specific data such as local values, currency codes document fragments here
        bRet = LoadXMLValues(sValues)

        mrXMLTagSettings.Load(sXMLTagSettingsFile)

        Init = True

    End Function
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        'msSegmentSep = ""
        miSegmentLen = 3
        'msEscapeChar = ""
        'msElementSep = ""
        eSendingBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Public Function IsGroupInUse(ByRef lGroup As Integer) As Boolean
        Dim root As MSXML2.IXMLDOMElement
        Dim node As MSXML2.IXMLDOMElement

        root = mrXmlDoc.documentElement
        node = root.selectSingleNode("//GROUPS/GROUP[" & lGroup & "]")
        If node Is Nothing Then
            Err.Raise(vbObjectError + 15020, , LRS(15020, Str(lGroup), sMappingFile))
            'Can't find segmentgroup number "lGroup" in the mappingfile: XXXXXX
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object node.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If node.getAttribute("inuse") = "1" Then
            IsGroupInUse = True
        Else
            IsGroupInUse = False
        End If
    End Function
    Private Function FindLevel(ByVal nodTemp As MSXML2.IXMLDOMElement) As Short
        Dim iReturnValue As Short

        iReturnValue = 0
        Do Until nodTemp.nodeName = "UNG" '"GROUPS"
            nodTemp = nodTemp.parentNode
            iReturnValue = iReturnValue + 1
        Loop

        FindLevel = iReturnValue

        'UPGRADE_NOTE: Object nodTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        nodTemp = Nothing

    End Function
    Private Function IsSegment(ByRef nodElement As MSXML2.IXMLDOMElement) As Boolean
        Dim bReturnValue As Boolean

        Select Case Left(nodElement.nodeName, 2)

            Case "GR"
                bReturnValue = False
            Case "TG"
                bReturnValue = False
            Case Else
                bReturnValue = True
        End Select

        IsSegment = bReturnValue

    End Function
    Private Function GroupRepetitionOK(ByRef nodTemp As MSXML2.IXMLDOMElement) As Boolean
        Dim bReturnValue As Boolean

        If Not nodTemp.nodeName = "GROUP" Then
            Err.Raise(vbObjectError + 15026, , LRS(15026, nodTemp.nodeName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)))
            ' "Internal error in EDI2XML. nodtemp.nodename is not a group"
            bReturnValue = False
        Else
            'UPGRADE_WARNING: Couldn't resolve default property of object nodTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bReturnValue = SetGroupRep(nodTemp.getAttribute("number"))
        End If

        GroupRepetitionOK = bReturnValue

    End Function
    Friend Function SetGroupRep(ByRef iGroupNo As Double, Optional ByRef bResetValues As Boolean = False) As Boolean
        ' 13.05.2020 - changed iGroupNo from Short to Double (in parameterlist above)
        Dim bReturnValue As Boolean
        Dim i As Short

        'If the array isn't defined, define it with the same length as aGroupMaxRep which is based
        ' on the mapping
        If Array_IsEmpty(aGroupRep) Then
            ReDim Preserve aGroupRep(UBound(aGroupMaxRep, 1))
        End If

        'if resetvalues = true reset all groups from the given GroupNo and forward to nil
        If bResetValues Then
            For i = iGroupNo To UBound(aGroupRep)
                aGroupRep(i) = 0
            Next i
            bReturnValue = True
        Else
            aGroupRep(iGroupNo) = aGroupRep(iGroupNo) + 1
            iLastUsedGroup = iGroupNo
            'check if the number the group is repeated is lower than the number of repetitions
            'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If aGroupRep(iGroupNo) <= Val(aGroupMaxRep(iGroupNo, 0)) Then
                bReturnValue = True
            Else
                bReturnValue = False
            End If
        End If

        SetGroupRep = bReturnValue

    End Function
    Public Function GetGroupRep(ByRef lGroupNo As Double) As Double '13.05.2020 changed from Short

        GetGroupRep = aGroupRep(lGroupNo)

    End Function
    Public Function setGroupMaxRep() As Boolean
        Dim nodlistGroupNodes As MSXML2.IXMLDOMNodeList
        Dim nodTempNode As MSXML2.IXMLDOMElement
        Dim i As Short
        'Dim iGroupNumber As Integer

        nodlistGroupNodes = mrXmlDoc.documentElement.selectNodes("//GROUP")

        ReDim Preserve aGroupMaxRep(nodlistGroupNodes.length - 1, 1)

        On Error GoTo errorHandler

        For i = 0 To nodlistGroupNodes.length - 1
            nodTempNode = nodlistGroupNodes(i)
            'iGroupNumber = Val(nodTempNode.getAttribute("number"))
            'UPGRADE_WARNING: Couldn't resolve default property of object nodTempNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(Val(), 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aGroupMaxRep(Val(nodTempNode.getAttribute("number")), 0) = nodTempNode.getAttribute("reps")
            If nodTempNode.hasChildNodes Then
                'UPGRADE_WARNING: Couldn't resolve default property of object nodTempNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(Val(), 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aGroupMaxRep(Val(nodTempNode.getAttribute("number")), 1) = nodTempNode.firstChild.nodeName
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object nodTempNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(Val(), 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aGroupMaxRep(Val(nodTempNode.getAttribute("number")), 1) = ""
            End If
        Next i

        setGroupMaxRep = True
        Exit Function

errorHandler:
        setGroupMaxRep = False

    End Function
    Private Function SegmentRepeatOK(ByRef nodTemp As MSXML2.IXMLDOMElement) As Boolean
        Dim bReturnValue As Boolean

        iSegmentRepetition = iSegmentRepetition + 1
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If iSegmentRepetition > Val(nodTemp.getAttribute("reps")) Then
            bReturnValue = False
        Else
            bReturnValue = True
        End If

        SegmentRepeatOK = bReturnValue

    End Function
    Private Function GetFirstMandatorySegInGroup() As String

        GetFirstMandatorySegInGroup = aGroupMaxRep(iLastUsedGroup, 1)

    End Function

    Private Function NumberOfSegments(ByRef nodTemp As MSXML2.IXMLDOMElement, ByVal i As Short) As Short

        For i = 0 To nodTemp.childNodes.length - 1
            '    If nodTemp.childNodes(i).nodeName = "GROUP" Then
            '        Exit For
            '    End If
        Next i

        NumberOfSegments = i - 1

    End Function
    Public Function CheckNoOfLIN(ByRef sSegment As String) As Boolean
        Dim bReturnValue As Boolean
        Dim nodTemp As MSXML2.IXMLDOMElement
        Dim i As Short
        Dim sNoOfLIN As String

        sNoOfLIN = ""
        nodTemp = mrXmlDoc.documentElement.selectSingleNode("//LIN").parentNode

        'The number of LINs are expressed in two way
        'either: CNT+LI:1, or CNT+LIN:1
        'A third way for BGMax-files from DnBNOR

        'New code
        If Mid(sSegment, 5, 1) = "2" Then
            For i = 7 To Len(sSegment) - 1
                If IsNumeric(Mid(sSegment, i, 1)) Then
                    sNoOfLIN = sNoOfLIN & Mid(sSegment, i, 1)
                Else
                    Exit For
                End If
            Next i
        ElseIf IsNumeric(Mid(sSegment, 8, 1)) Then
            For i = 8 To Len(sSegment) - 1
                If IsNumeric(Mid(sSegment, i, 1)) Then
                    sNoOfLIN = sNoOfLIN & Mid(sSegment, i, 1)
                Else
                    Exit For
                End If
            Next i
        Else
            For i = 9 To Len(sSegment) - 1
                If IsNumeric(Mid(sSegment, i, 1)) Then
                    sNoOfLIN = sNoOfLIN & Mid(sSegment, i, 1)
                Else
                    Exit For
                End If
            Next i
        End If

        'If aGroupRep(Val(nodTemp.getAttribute("number"))) <> Val(sNoOfLIN) Then
        If lLINCounter <> Val(sNoOfLIN) Then
            Err.Raise(vbObjectError + 15016, , LRS(15016, mrAppAdmImport.EDIFilePath, sNoOfLIN.Trim, lLINCounter.ToString))
            '"Incorrect number of LINs in the file msEDIFilePath. Number stated in the file: 999 Number counted in the file: 999
            '"Wrong number of LIN-segments in the message"
            bReturnValue = False
        Else
            bReturnValue = True
        End If

        'UPGRADE_NOTE: Object nodTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        nodTemp = Nothing

        CheckNoOfLIN = bReturnValue

    End Function
    Public Function CheckNoofSegments(ByRef sSegment As String) As Boolean
        Dim sNoofSegments As String
        Dim i As Short
        Dim bReturnValue As Boolean

        i = 5
        sNoofSegments = ""
        Do Until i = Len(sSegment)
            If IsNumeric(Mid(sSegment, i, 1)) Then
                sNoofSegments = sNoofSegments & (Mid(sSegment, i, 1))
            Else
                Exit Do
            End If
            i = i + 1
        Loop

        If Val(sNoofSegments) <> lSegmentCount Then
            Err.Raise(vbObjectError + 15044, , LRS(15044, sNoofSegments, Str(lSegmentCount)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath))
            ' "Wrong number of segments in the message. Number in the file, number counted"
            bReturnValue = False
        Else
            bReturnValue = True
        End If

        CheckNoofSegments = bReturnValue

    End Function
    Public Function Conditional(ByRef nodElement As MSXML2.IXMLDOMElement) As Boolean
        Dim bReturnValue As Boolean

        If nodElement.getAttribute("mandatory") = "0" Then
            If nodElement.getAttribute("inuse") = "1" Then
                bReturnValue = True
            Else
                bReturnValue = False
            End If
        Else
            bReturnValue = False
        End If

        Conditional = bReturnValue

    End Function
    Public Function FindNextSegment_REMOVE(ByRef sSegmentName As String) As MSXML2.IXMLDOMElement
        Dim i3, i1, i2, i4 As Short
        Dim i5, i6 As Short
        Dim i2SiblingCounter, i1SiblingCounter, i3SiblingCounter As Short
        Dim i5SiblingCounter, i4SiblingCounter, i6SiblingCounter As Short
        Dim bFoundOldElement, bFoundNewElement As Boolean
        'Dim i1Pointer As Integer, i2Pointer As Integer, i3Pointer As Integer
        'Dim i4Pointer As Integer, i5Pointer As Integer, i6Pointer As Integer
        Dim bFromLevelUnder2, bFromLevelUnder1, bFromLevelUnder3 As Boolean
        Dim bFromLevelUnder5, bFromLevelUnder4, bFromLevelUnder6 As Boolean
        Dim bContinue2, bContinue1, bContinue3 As Boolean
        Dim bContinue5, bContinue4, bContinue6 As Boolean
        Dim bGroupConditional As Boolean
        Dim bSegmentRepeated, bAbandon As Boolean
        Dim nodBase As MSXML2.IXMLDOMElement
        Dim eleActiveElement1 As MSXML2.IXMLDOMElement
        Dim eleActiveElement2 As MSXML2.IXMLDOMElement
        Dim eleActiveElement3 As MSXML2.IXMLDOMElement
        Dim eleActiveElement4 As MSXML2.IXMLDOMElement
        Dim eleActiveElement5 As MSXML2.IXMLDOMElement
        Dim eleActiveElement6 As MSXML2.IXMLDOMElement
        Dim bSpecialSegment As Boolean
        Dim bCheck As Boolean
        Dim iStartLevel, iEndLevel As Short
        Dim iCurrentGroupLevel, iNewGroupLevel As Short


        'i1Pointer = 0
        'i2Pointer = 0
        'i3Pointer = 0
        'i4Pointer = 0
        'i5Pointer = 0
        'i6Pointer = 0
        bFoundOldElement = False
        bFoundNewElement = False
        bGroupConditional = False
        bSegmentRepeated = False
        bAbandon = False
        bContinue1 = False
        bContinue2 = False
        bContinue3 = False
        bContinue4 = False
        bContinue5 = False
        bContinue6 = False
        bFromLevelUnder1 = False
        bFromLevelUnder2 = False
        bFromLevelUnder3 = False
        bFromLevelUnder4 = False
        bFromLevelUnder5 = False
        bFromLevelUnder6 = False
        iStartLevel = 0
        iEndLevel = 0
        bGroupChange = False
        iCurrentGroupLevel = 0
        iNewGroupLevel = 0

        Dim s As String
        s = sSegmentName

        'Used to exit the loop (we have to enter the loop to find the existing level)
        ' when the Segment is either LIN,SEQ or DOC.
        bSpecialSegment = False

        If sSegmentName = "LIN" Then
            'Set mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//LIN")
            'Resets groupvalues below LIN
            nodBase = eleLINElementParent
            'Check if at least 1 SEQ is imported in the previous LIN (had an example from Nordea with
            ' a LIN without a SEQ. Exception for the first LIN
            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If GetGroupRep(Val(nodBase.getAttribute("number"))) > 0 Then
                If lSEQCounter = 0 Then
                    Err.Raise(vbObjectError + 15092, "FindNextSegment", LRS(15092) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15030, (mrAppAdmImport.EDIFilePath)))
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bCheck = SetGroupRep(Val(nodBase.getAttribute("number")) + 1, True)

            'Add 1 to the group-counter
            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Not SetGroupRep(Val(nodBase.getAttribute("number")), False) Then
                'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(number). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Err.Raise(vbObjectError + 15021, , Replace(Replace(LRS(15021), "%1", nodBase.getAttribute("number")), "%2", mrAppAdmImport.EDIFilePath))
                ' "Group "nodBase.getAttribute("number")" repeated to many times. Filename: XXXXX"
            End If
            iEndLevel = FindLevel(nodBase)
            'UPGRADE_NOTE: Object nodBase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            nodBase = Nothing
            bSpecialSegment = True
            lLINCounter = lLINCounter + 1
            lSEQCounter = 0
            'Set FindNextSegment = mrCurrentElement
            'Exit Function
        ElseIf sSegmentName = "SEQ" Then
            'Set mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//SEQ")
            'Resets groupvalues below SEQ
            nodBase = eleSEQElementParent
            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Not SetGroupRep(Val(nodBase.getAttribute("number")) + 1, True) Then
                'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(number). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", nodBase.getAttribute("number")) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)))
                ' "Group "nodBase.getAttribute("number")" repeated to many times. Filename: XXXXX" LIN:
            End If
            'Add 1 to the group-counter
            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bCheck = SetGroupRep(Val(nodBase.getAttribute("number")), False)
            iEndLevel = FindLevel(nodBase)
            'UPGRADE_NOTE: Object nodBase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            nodBase = Nothing
            bSpecialSegment = True
            lSEQCounter = lSEQCounter + 1
            'Set FindNextSegment = mrCurrentElement
            'Exit Function
        ElseIf sSegmentName = "DOC" Then
            'Set mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//DOC")
            'Resets groupvalues below DOC
            nodBase = eleDOCElementParent
            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Not SetGroupRep(Val(nodBase.getAttribute("number")) + 1, True) Then
                'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(number). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", nodBase.getAttribute("number")) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                ' "Group "nodBase.getAttribute("number")" repeated to many times. Filename: XXXXX" LIN: SEQ:
            End If
            'Add 1 to the group-counter
            'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bCheck = SetGroupRep(Val(nodBase.getAttribute("number")), False)
            iEndLevel = FindLevel(nodBase)
            'UPGRADE_NOTE: Object nodBase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            nodBase = Nothing
            bSpecialSegment = True
            'Set FindNextSegment = mrCurrentElement
            'Exit Function
        ElseIf sSegmentName = "CNT" Then
            mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//CNT")
            iEndLevel = FindLevel(mrCurrentElement)
        End If

        'Find current grouplevel
        nodBase = mrCurrentElement.parentNode
        'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        iCurrentGroupLevel = nodBase.getAttribute("number")

        'Set the basic node
        nodBase = eleBasicMappingElement


        'Check if the new segment is the same as the last one, thus the segment is repeated and
        ' we have to check if that is legal.
        If sSegmentName = mrCurrentElement.nodeName Then
            bSegmentRepeated = True
        Else
            iSegmentRepetition = 1
        End If

        For i1 = i1Pointer To nodBase.childNodes.length - 1
            eleActiveElement1 = nodBase.childNodes(i1)
            'If we come from the level under i2Pointer must be reset
            If bContinue1 = True Then
                bContinue1 = False
                i2Pointer = 0
            End If
            If bFoundOldElement Then
                'Check if the node is in use
                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement1.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If eleActiveElement1.getAttribute("inuse") = "1" Then
                    'Check if we are dealing with a segment-node
                    If IsSegment(eleActiveElement1) Then
                        bContinue1 = False
                        '                'Check if the node is identical with the CurrentNode
                        '                If IsCurrentNode(nodBase.childNodes(i1)) Then
                        '                    bFoundElement = True
                        '                Else
                        'Check if we have found the correct node
                        If eleActiveElement1.nodeName = sSegmentName Then
                            mrCurrentElement = eleActiveElement1
                            If bSegmentRepeated Then
                                If Not SegmentRepeatOK(eleActiveElement1) Then
                                    'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                    ' always is Mandatory
                                    'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                    ' the group. Therefore we have to check if the group is repested. If so check if it can
                                    ' be repeated. If Yes then everything is OK.
                                    For i1SiblingCounter = 1 To eleActiveElement1.parentNode.childNodes.length - 1
                                        'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If Mandatory(nodBase.childNodes(i1SiblingCounter)) Then
                                            Err.Raise(vbObjectError + 15022, , Replace(LRS(15022), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                            '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                            bAbandon = True
                                            Exit For
                                        End If
                                    Next i1SiblingCounter
                                    If Not bAbandon Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement1.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If Not GroupRepetitionOK(eleActiveElement1.parentNode) Then
                                            Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                            ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                            bAbandon = True
                                        End If
                                    End If
                                End If
                            End If
                            bFoundNewElement = True
                            bContinue1 = False

                        Else
                            'Check if the node is mandatory
                            If Mandatory(eleActiveElement1) And Not bGroupConditional Then
                                Err.Raise(vbObjectError + 15023, , Replace(Replace(LRS(15023), "%1", eleActiveElement1.nodeName), "%2", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                bContinue1 = False
                            Else
                                bSegmentRepeated = False
                                bContinue1 = False
                            End If
                        End If
                        '                End If
                    Else
                        If Conditional(eleActiveElement1) Then
                            bGroupConditional = True
                        End If
                        If Not GroupRepetitionOK(eleActiveElement1) Then
                            Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                            ' "Group repeated to many times"
                            bContinue1 = False
                        Else
                            bContinue1 = True
                        End If
                    End If
                End If
            Else
                If eleActiveElement1.nodeName = mrCurrentElement.nodeName Then
                    bFoundOldElement = True
                    bContinue1 = False
                    iStartLevel = 1
                    If bSpecialSegment Then
                        bFoundNewElement = True
                        Exit For
                    End If
                    If bSegmentRepeated Then
                        'Have to subtract 1 from counter, because the segment may be repeated
                        i1 = i1 - 1
                    End If
                Else
                    bContinue1 = True
                End If
            End If
            If bContinue1 Then

                For i2 = i2Pointer To eleActiveElement1.childNodes.length - 1
                    eleActiveElement2 = eleActiveElement1.childNodes(i2)
                    'If we come from the level under i3Pointer must be reset
                    If bContinue2 = True Then
                        bContinue2 = False
                        i3Pointer = 0
                    End If
                    If bFoundOldElement Then
                        'Check if the node is in use
                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement2.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If eleActiveElement2.getAttribute("inuse") = "1" Then
                            'Check if we are dealing with a segment-node
                            If IsSegment(eleActiveElement2) Then
                                bContinue2 = False
                                'Check if we have found the correct node
                                If eleActiveElement2.nodeName = sSegmentName Then
                                    mrCurrentElement = eleActiveElement2
                                    If bSegmentRepeated Then
                                        If Not SegmentRepeatOK(eleActiveElement2) Then
                                            'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                            ' always is Mandatory
                                            'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                            ' the group. Therefore we have to check if the group is repested. If so check if it can
                                            ' be repeated. If Yes then everything is OK.
                                            For i2SiblingCounter = 1 To eleActiveElement2.parentNode.childNodes.length - 1
                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement1.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                If Mandatory(eleActiveElement1.childNodes(i2SiblingCounter)) Then
                                                    Err.Raise(vbObjectError + 15022, , Replace(LRS(15022), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                    '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                    bAbandon = True
                                                    Exit For
                                                End If
                                            Next i2SiblingCounter
                                            If Not bAbandon Then
                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement2.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                If Not GroupRepetitionOK(eleActiveElement2.parentNode) Then
                                                    Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                    ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                    bAbandon = True
                                                End If
                                            End If
                                        End If
                                    End If
                                    bFoundNewElement = True
                                    bContinue2 = False
                                Else
                                    'Check if the node is mandatory
                                    If Mandatory(eleActiveElement2) And Not bGroupConditional Then
                                        Err.Raise(vbObjectError + 15023, , Replace(Replace(LRS(15023), "%1", eleActiveElement2.nodeName), "%2", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                        ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                        bContinue2 = False
                                    Else
                                        'Reset the value
                                        bGroupConditional = False
                                        If i2 = NumberOfSegments(eleActiveElement1, i2) Then
                                            Exit For
                                        End If
                                        'FIX: Is it 100% sure that the first segment in a group is mandatory?
                                        ' If not this test isn't good enough. Then we have to continue the loop
                                        ' to check the rest of the segments.
                                        'bContinue2 = False
                                    End If
                                End If
                            Else
                                If Conditional(eleActiveElement2) Then
                                    bGroupConditional = True
                                End If
                                If Not GroupRepetitionOK(eleActiveElement2) Then
                                    Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                    ' "Group repeated to many times"
                                    bContinue2 = False
                                Else
                                    bContinue2 = True
                                End If
                            End If
                        End If
                    Else
                        If eleActiveElement2.nodeName = mrCurrentElement.nodeName Then
                            bFoundOldElement = True
                            bContinue2 = False
                            iStartLevel = 2
                            If bSpecialSegment Then
                                bFoundNewElement = True
                                Exit For
                            End If
                            If bSegmentRepeated Then
                                'Have to subtract 1 from counter, because the segment may be repeated
                                i2 = i2 - 1
                            End If
                        Else
                            bContinue2 = True
                        End If
                    End If
                    If bContinue2 Then

                        For i3 = i3Pointer To eleActiveElement2.childNodes.length - 1
                            eleActiveElement3 = eleActiveElement2.childNodes(i3)
                            'If we come from the level under i4Pointer must be reset
                            If bContinue3 = True Then
                                bContinue3 = False
                                i4Pointer = 0
                            End If
                            If bFoundOldElement Then
                                'Check if the node is in use
                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement3.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If eleActiveElement3.getAttribute("inuse") = "1" Then
                                    'Check if we are dealing with a segment-node
                                    If IsSegment(eleActiveElement3) Then
                                        bContinue3 = False
                                        'Check if we have found the correct node
                                        If eleActiveElement3.nodeName = sSegmentName Then
                                            mrCurrentElement = eleActiveElement3
                                            If bSegmentRepeated Then
                                                'The same segment occurs one more time.
                                                If Not SegmentRepeatOK(eleActiveElement3) Then
                                                    'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                    ' always is Mandatory
                                                    'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                    ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                    ' be repeated. If Yes then everything is OK.
                                                    For i3SiblingCounter = 1 To eleActiveElement3.parentNode.childNodes.length - 1
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement2.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        If Mandatory(eleActiveElement2.childNodes(i3SiblingCounter)) Then
                                                            Err.Raise(vbObjectError + 15022, , Replace(LRS(15022), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                            '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                            bAbandon = True
                                                            Exit For
                                                        End If
                                                    Next i3SiblingCounter
                                                    If Not bAbandon Then
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement3.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        If Not GroupRepetitionOK(eleActiveElement3.parentNode) Then
                                                            Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                            ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                            bAbandon = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            bFoundNewElement = True
                                            bContinue3 = False
                                        Else
                                            'Check if the node is mandatory
                                            If Mandatory(eleActiveElement3) And Not bGroupConditional Then
                                                Err.Raise(vbObjectError + 15023, , Replace(Replace(LRS(15023), "%1", eleActiveElement3.nodeName), "%2", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                bContinue3 = False
                                            Else
                                                'Reset the value
                                                bGroupConditional = False
                                                If i3 = NumberOfSegments(eleActiveElement2, i3) Then
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    Else
                                        If Conditional(eleActiveElement3) Then
                                            bGroupConditional = True
                                        End If
                                        If Not GroupRepetitionOK(eleActiveElement3) Then
                                            Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                            ' "Group repeated to many times"
                                            bContinue3 = False
                                        Else
                                            bContinue3 = True
                                        End If
                                    End If
                                End If
                            Else
                                If eleActiveElement3.nodeName = mrCurrentElement.nodeName Then
                                    bFoundOldElement = True
                                    bContinue3 = False
                                    iStartLevel = 3
                                    If bSpecialSegment Then
                                        bFoundNewElement = True
                                        Exit For
                                    End If
                                    If bSegmentRepeated Then
                                        'Have to subtract 1 from counter, because the segment may be repeated
                                        i3 = i3 - 1
                                    End If
                                Else
                                    bContinue3 = True
                                End If
                            End If
                            If bContinue3 Then

                                For i4 = i4Pointer To eleActiveElement3.childNodes.length - 1
                                    eleActiveElement4 = eleActiveElement3.childNodes(i4)
                                    'If we come from the level under i5Pointer must be reset
                                    If bContinue4 = True Then
                                        bContinue4 = False
                                        i5Pointer = 0
                                    End If
                                    If bFoundOldElement Then
                                        'Check if the node is in use
                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement4.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If eleActiveElement4.getAttribute("inuse") = "1" Then
                                            'Check if we are dealing with a segment-node
                                            If IsSegment(eleActiveElement4) Then
                                                bContinue4 = False
                                                'Check if we have found the correct node
                                                If eleActiveElement4.nodeName = sSegmentName Then
                                                    mrCurrentElement = eleActiveElement4
                                                    If bSegmentRepeated Then
                                                        If Not SegmentRepeatOK(eleActiveElement4) Then
                                                            'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                            ' always is Mandatory
                                                            'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                            ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                            ' be repeated. If Yes then everything is OK.
                                                            For i4SiblingCounter = 1 To eleActiveElement4.parentNode.childNodes.length - 1
                                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement3.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                If Mandatory(eleActiveElement3.childNodes(i4SiblingCounter)) Then
                                                                    Err.Raise(vbObjectError + 15022, , Replace(LRS(15022), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                    '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                                    bAbandon = True
                                                                    Exit For
                                                                End If
                                                            Next i4SiblingCounter
                                                            If Not bAbandon Then
                                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement4.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                If Not GroupRepetitionOK(eleActiveElement4.parentNode) Then
                                                                    Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                    ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                                    bAbandon = True
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    bFoundNewElement = True
                                                    bContinue4 = False
                                                Else
                                                    'Check if the node is mandatory
                                                    If Mandatory(eleActiveElement4) And Not bGroupConditional Then
                                                        Err.Raise(vbObjectError + 15023, , Replace(Replace(LRS(15023), "%1", eleActiveElement4.nodeName), "%2", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                        ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                        bContinue4 = False
                                                        'End If
                                                    Else
                                                        'Reset the value
                                                        bGroupConditional = False
                                                        If i4 = NumberOfSegments(eleActiveElement3, i4) Then
                                                            Exit For
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If Conditional(eleActiveElement4) Then
                                                    bGroupConditional = True
                                                End If
                                                If Not GroupRepetitionOK(eleActiveElement4) Then
                                                    Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                    ' "Group repeated to many times"
                                                    bContinue4 = False
                                                Else
                                                    bContinue4 = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        If eleActiveElement4.nodeName = mrCurrentElement.nodeName Then
                                            bFoundOldElement = True
                                            bContinue4 = False
                                            iStartLevel = 4
                                            If bSpecialSegment Then
                                                bFoundNewElement = True
                                                Exit For
                                            End If
                                            If bSegmentRepeated Then
                                                'Have to subtract 1 from counter, because the segment may be repeated
                                                i4 = i4 - 1
                                            End If
                                        Else
                                            bContinue4 = True
                                        End If
                                    End If
                                    If bContinue4 Then

                                        For i5 = i5Pointer To eleActiveElement4.childNodes.length - 1
                                            eleActiveElement5 = eleActiveElement4.childNodes(i5)
                                            'If we come from the level under i6Pointer must be reset
                                            If bContinue5 = True Then
                                                bContinue5 = False
                                                i6Pointer = 0
                                            End If
                                            If bFoundOldElement Then
                                                'Check if the node is in use
                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement5.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                If eleActiveElement5.getAttribute("inuse") = "1" Then
                                                    'Check if we are dealing with a segment-node
                                                    If IsSegment(eleActiveElement5) Then
                                                        bContinue5 = False
                                                        'Check if we have found the correct node
                                                        If eleActiveElement5.nodeName = sSegmentName Then
                                                            mrCurrentElement = eleActiveElement5
                                                            If bSegmentRepeated Then
                                                                If Not SegmentRepeatOK(eleActiveElement5) Then
                                                                    'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                                    ' always is Mandatory
                                                                    'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                                    ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                                    ' be repeated. If Yes then everything is OK.
                                                                    For i5SiblingCounter = 1 To eleActiveElement5.parentNode.childNodes.length - 1
                                                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement4.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                        If Mandatory(eleActiveElement4.childNodes(i5SiblingCounter)) Then
                                                                            Err.Raise(vbObjectError + 15022, , Replace(LRS(15022), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                            '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                                            bAbandon = True
                                                                            Exit For
                                                                        End If
                                                                    Next i5SiblingCounter
                                                                    If Not bAbandon Then
                                                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement5.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                        If Not GroupRepetitionOK(eleActiveElement5.parentNode) Then
                                                                            Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                            ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                                            bAbandon = True
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                            bFoundNewElement = True
                                                            bContinue5 = False
                                                        Else
                                                            'Check if the node is mandatory
                                                            If Mandatory(eleActiveElement5) And Not bGroupConditional Then
                                                                Err.Raise(vbObjectError + 15023, , Replace(Replace(LRS(15023), "%1", eleActiveElement5.nodeName), "%2", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                                bContinue5 = False
                                                            Else
                                                                'Reset the value
                                                                bGroupConditional = False
                                                                If i5 = NumberOfSegments(eleActiveElement4, i5) Then
                                                                    Exit For
                                                                End If
                                                            End If
                                                        End If
                                                    Else
                                                        If Conditional(eleActiveElement5) Then
                                                            bGroupConditional = True
                                                        End If
                                                        If Not GroupRepetitionOK(eleActiveElement5) Then
                                                            Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                            ' "Group repeated to many times"
                                                            bContinue5 = False
                                                        Else
                                                            bContinue5 = True
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If eleActiveElement5.nodeName = mrCurrentElement.nodeName Then
                                                    bFoundOldElement = True
                                                    bContinue5 = False
                                                    iStartLevel = 5
                                                    If bSpecialSegment Then
                                                        bFoundNewElement = True
                                                        Exit For
                                                    End If
                                                    If bSegmentRepeated Then
                                                        'Have to subtract 1 from counter, because the segment may be repeated
                                                        i5 = i5 - 1
                                                    End If
                                                Else
                                                    bContinue5 = True
                                                End If
                                            End If
                                            If bContinue5 Then

                                                For i6 = i6Pointer To eleActiveElement5.childNodes.length - 1
                                                    eleActiveElement6 = eleActiveElement5.childNodes(i6)
                                                    'If we come from the level under i7Pointer must be reset
                                                    If bContinue6 = True Then
                                                        bContinue6 = False
                                                        'i7Pointer = 0
                                                    End If
                                                    If bFoundOldElement Then
                                                        'Check if the node is in use
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement6.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        If eleActiveElement6.getAttribute("inuse") = "1" Then
                                                            'Check if we are dealing with a segment-node
                                                            If IsSegment(eleActiveElement6) Then
                                                                bContinue6 = False
                                                                'Check if we have found the correct node
                                                                If eleActiveElement6.nodeName = sSegmentName Then
                                                                    mrCurrentElement = eleActiveElement6
                                                                    If bSegmentRepeated Then
                                                                        If Not SegmentRepeatOK(eleActiveElement6) Then
                                                                            'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                                            ' always is Mandatory
                                                                            'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                                            ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                                            ' be repeated. If Yes then everything is OK.
                                                                            For i6SiblingCounter = 1 To eleActiveElement6.parentNode.childNodes.length - 1
                                                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement5.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                                If Mandatory(eleActiveElement5.childNodes(i6SiblingCounter)) Then
                                                                                    Err.Raise(vbObjectError + 15022, , Replace(LRS(15022), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                                    '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                                                    bAbandon = True
                                                                                    Exit For
                                                                                End If
                                                                            Next i6SiblingCounter
                                                                            If Not bAbandon Then
                                                                                'UPGRADE_WARNING: Couldn't resolve default property of object eleActiveElement6.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                                If Not GroupRepetitionOK(eleActiveElement6.parentNode) Then
                                                                                    Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                                    ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                                                    bAbandon = True
                                                                                End If
                                                                            End If
                                                                        End If
                                                                    End If
                                                                    bFoundNewElement = True
                                                                    bContinue6 = False
                                                                Else
                                                                    'Check if the node is mandatory
                                                                    If Mandatory(eleActiveElement6) And Not bGroupConditional Then
                                                                        Err.Raise(vbObjectError + 15023, , Replace(Replace(LRS(15023), "%1", eleActiveElement6.nodeName), "%2", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                        ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                                        bContinue6 = False
                                                                    Else
                                                                        'Reset the value
                                                                        bGroupConditional = False
                                                                        If i6 = NumberOfSegments(eleActiveElement5, i6) Then
                                                                            Exit For
                                                                        End If
                                                                    End If
                                                                End If
                                                            Else
                                                                If Conditional(eleActiveElement6) Then
                                                                    bGroupConditional = True
                                                                End If
                                                                If Not GroupRepetitionOK(eleActiveElement6) Then
                                                                    Err.Raise(vbObjectError + 15021, , Replace(LRS(15021), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                                    ' "Group repeated to many times"
                                                                    bContinue6 = False
                                                                Else
                                                                    bContinue6 = True
                                                                End If
                                                            End If
                                                        End If
                                                    Else
                                                        If eleActiveElement6.nodeName = mrCurrentElement.nodeName Then
                                                            bFoundOldElement = True
                                                            bContinue6 = False
                                                            iStartLevel = 6
                                                            If bSpecialSegment Then
                                                                bFoundNewElement = True
                                                                Exit For
                                                            End If
                                                            If bSegmentRepeated Then
                                                                'Have to subtract 1 from counter, because the segment may be repeated
                                                                i6 = i6 - 1
                                                            End If
                                                        Else
                                                            bContinue6 = True
                                                        End If
                                                    End If
                                                    If bContinue6 Then
                                                        Err.Raise(vbObjectError + 15024, , Replace(LRS(15024), "%1", Str(iLastUsedGroup)) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
                                                        ' "Too many levels"
                                                    End If

                                                    If bFoundNewElement Then
                                                        If iEndLevel = 0 Then
                                                            iEndLevel = 6
                                                        End If
                                                        Exit For
                                                    Else
                                                        bFromLevelUnder5 = True
                                                    End If
                                                    If i6 = eleActiveElement5.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup() Then
                                                        i5 = i5 - 1
                                                    End If

                                                Next i6
                                            End If
                                            If bFoundNewElement Then
                                                If iEndLevel = 0 Then
                                                    iEndLevel = 5
                                                End If
                                                Exit For
                                            Else
                                                bFromLevelUnder5 = True
                                            End If
                                            If i5 = -1 Then
                                                If bFromLevelUnder6 And Not eleActiveElement4.childNodes(0).nodeName = "GROUP" Then
                                                    bFromLevelUnder6 = False
                                                End If
                                            Else
                                                If bFromLevelUnder6 And Not eleActiveElement5.nodeName = "GROUP" Then
                                                    i5 = i5 - 1
                                                    bFromLevelUnder6 = False
                                                End If
                                                If i5 = eleActiveElement4.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup() Then
                                                    i4 = i4 - 1
                                                End If
                                            End If
                                        Next i5
                                    Else
                                        'Not continuing
                                        i5Pointer = 0
                                    End If
                                    If bFoundNewElement Then
                                        If iEndLevel = 0 Then
                                            iEndLevel = 4
                                        End If
                                        Exit For
                                    Else
                                        bFromLevelUnder4 = True
                                    End If
                                    If i4 = -1 Then
                                        If bFromLevelUnder5 And Not eleActiveElement3.childNodes(0).nodeName = "GROUP" Then
                                            bFromLevelUnder5 = False
                                        End If
                                    Else
                                        If bFromLevelUnder5 And Not eleActiveElement4.nodeName = "GROUP" Then
                                            i4 = i4 - 1
                                            bFromLevelUnder5 = False
                                        End If
                                        ' KJI: Ny Kode 20/12-2001. For � takle repetering av grupper
                                        ' If-statementet er lagt til under alle niv�er
                                        If i4 = eleActiveElement3.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup() Then
                                            'If i4 = nodBase.childNodes(i1).childNodes(i2).childNodes(i3).childNodes.length - 1 And sSegmentName = "INP" And i3 = 8 Then
                                            i3 = i3 - 1
                                        End If
                                    End If
                                Next i4
                            Else
                                'Not continuing
                                i4Pointer = 0
                            End If
                            If bFoundNewElement Then
                                If iEndLevel = 0 Then
                                    iEndLevel = 3
                                End If
                                Exit For
                            Else
                                bFromLevelUnder3 = True
                            End If
                            If i3 = -1 Then
                                If bFromLevelUnder4 And Not eleActiveElement2.childNodes(0).nodeName = "GROUP" Then
                                    bFromLevelUnder4 = False
                                End If
                            Else
                                'New code 20/5-2003 by KI
                                'Old code - next 4 lines
                                '                        If bFromLevelUnder4 And Not eleActiveElement3.nodeName = "GROUP" Then
                                '                            i3 = i3 - 1
                                '                            bFromLevelUnder4 = False
                                '                        End If
                                'New code
                                If bFromLevelUnder4 Then
                                    If Not eleActiveElement3.nodeName = "GROUP" Then
                                        i3 = i3 - 1
                                        bFromLevelUnder4 = False
                                    ElseIf eleActiveElement3.hasChildNodes Then
                                        If eleActiveElement3.firstChild.nodeName = sSegmentName Then
                                            i3 = i3 - 1
                                            bFromLevelUnder4 = False
                                        End If
                                    End If
                                End If
                                'End new code - must be implemented on the other levels
                                If i3 = eleActiveElement2.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup() Then
                                    i2 = i2 - 1
                                End If
                            End If
                        Next i3
                    Else
                        'Not continuing
                        i3Pointer = 0
                    End If
                    If bFoundNewElement Then
                        If iEndLevel = 0 Then
                            iEndLevel = 2
                        End If
                        Exit For
                    Else
                        bFromLevelUnder2 = True
                    End If
                    If i2 = -1 Then
                        If bFromLevelUnder3 And Not eleActiveElement1.childNodes(0).nodeName = "GROUP" Then
                            bFromLevelUnder3 = False
                        End If
                    Else
                        If bFromLevelUnder3 And Not eleActiveElement2.nodeName = "GROUP" Then
                            i2 = i2 - 1
                            bFromLevelUnder3 = False
                        End If
                        If i2 = eleActiveElement1.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup() Then
                            i1 = i1 - 1
                        End If
                    End If
                Next i2
            End If
            If bFoundNewElement Then
                If iEndLevel = 0 Then
                    iEndLevel = 1
                End If
                Exit For
            Else
                bFromLevelUnder1 = True
            End If
            If i1 = -1 Then
                If bFromLevelUnder2 And Not nodBase.childNodes(0).nodeName = "GROUP" Then
                    bFromLevelUnder2 = False
                End If
            Else
                If bFromLevelUnder2 And Not eleActiveElement1.nodeName = "GROUP" Then
                    i1 = i1 - 1
                    bFromLevelUnder2 = False
                End If
            End If
        Next i1

        If bSpecialSegment Then
            'We must reset the pointers to where we are in the mapping-file, because when we do jumps
            ' in the mapping the normal way to trace through the mapping doesn't function
            'The pointers are set according to the placements in the mappingfiles (not according to the groupnumbers
            'If in the mappingfile You have Group 0
            'Under group 0 You have the folloving elements
            ' 1 - UNH
            ' 2 - BGM
            ' 3 - DTM
            ' 4 - Group 1
            ' 5 - Group 2
            ' 6 - Group 3
            ' 7 - Group 4 - On the level below in this group You will find the LIN-element
            'To set the correct posinter for the LIN-level (ipointer1) set the
            ' iPointer to the number of elements prior to the LIN-element.
            ' In this example 6

            Select Case sSegmentName
                Case "LIN"
                    If sEDIFormat = "BANSTA" Then
                        If eSendingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                            i1Pointer = 7
                            i2Pointer = 0
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        ElseIf eSendingBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO Then
                            i1Pointer = 7
                            i2Pointer = 0
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        Else
                            i1Pointer = 6
                            i2Pointer = 0
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        End If
                    Else
                        i1Pointer = 6
                        i2Pointer = 0
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    End If
                    mrCurrentElement = eleLINElement
                Case "SEQ"
                    If sEDIFormat = "CREMUL" Then
                        i1Pointer = 6
                        i2Pointer = 7
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "PAYMUL" Then
                        i1Pointer = 6
                        i2Pointer = 11
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "DEBMUL" Then
                        i1Pointer = 6
                        i2Pointer = 7
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "BANSTA" Then
                        If eSendingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                            i1Pointer = 7
                            i2Pointer = 5
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        ElseIf eSendingBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO Then
                            i1Pointer = 7
                            i2Pointer = 5
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        Else
                            i1Pointer = 6
                            i2Pointer = 5
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        End If
                    End If
                    mrCurrentElement = eleSEQElement
                Case "DOC"
                    If sEDIFormat = "CREMUL" Then
                        i1Pointer = 6
                        i2Pointer = 7
                        i3Pointer = 11
                        i4Pointer = 2
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "PAYMUL" Then
                        i1Pointer = 6
                        i2Pointer = 11
                        i3Pointer = 10
                        i4Pointer = 2
                        i5Pointer = 0
                        i6Pointer = 0
                    End If
                    mrCurrentElement = eleDOCElement
            End Select
        Else
            i1Pointer = i1
            i2Pointer = i2
            i3Pointer = i3
            i4Pointer = i4
            i5Pointer = i5
            i6Pointer = i6
        End If

        If mrCurrentElement.nodeName <> sSegmentName Then
            Err.Raise(vbObjectError + 15025, , Replace(LRS(15025), "%1", sSegmentName) & vbCrLf & Replace(LRS(15030), "%1", mrAppAdmImport.EDIFilePath) & vbCrLf & Replace(LRS(15031), "%1", Str(lLINCounter)) & vbCrLf & Replace(LRS(15032), "%1", Str(lSEQCounter)))
            ' "Unexpected segment in file." & vbCrLf & "Segment: " & sSegmentName
        End If

        iLevelChange = iStartLevel - iEndLevel

        nodBase = mrCurrentElement.parentNode

        'UPGRADE_WARNING: Couldn't resolve default property of object nodBase.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        iNewGroupLevel = nodBase.getAttribute("number")
        If iCurrentGroupLevel = iNewGroupLevel Then
            bGroupChange = False
        Else
            bGroupChange = True
        End If

        'UPGRADE_NOTE: Object nodBase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        nodBase = Nothing

        'UPGRADE_NOTE: Object eleActiveElement1 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleActiveElement1 = Nothing
        'UPGRADE_NOTE: Object eleActiveElement2 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleActiveElement2 = Nothing
        'UPGRADE_NOTE: Object eleActiveElement3 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleActiveElement3 = Nothing
        'UPGRADE_NOTE: Object eleActiveElement4 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleActiveElement4 = Nothing
        'UPGRADE_NOTE: Object eleActiveElement5 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleActiveElement5 = Nothing
        'UPGRADE_NOTE: Object eleActiveElement6 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        eleActiveElement6 = Nothing

FoundChange:
        FindNextSegment_REMOVE = mrCurrentElement

    End Function

    Public Function FindNextSegment(ByVal sSegmentName As String) As MSXML2.IXMLDOMElement
        ' XNET 05.12.2013 TAKE WHOLE FUNCTION - Update LRS to use %1 correctly
        Dim i1 As Integer, i2 As Integer, i3 As Integer, i4 As Integer
        Dim i5 As Integer, i6 As Integer
        Dim i1SiblingCounter As Integer, i2SiblingCounter As Integer, i3SiblingCounter As Integer
        Dim i4SiblingCounter As Integer, i5SiblingCounter As Integer, i6SiblingCounter As Integer
        Dim bFoundOldElement As Boolean, bFoundNewElement As Boolean
        'Dim i1Pointer As Integer, i2Pointer As Integer, i3Pointer As Integer
        'Dim i4Pointer As Integer, i5Pointer As Integer, i6Pointer As Integer
        Dim bFromLevelUnder1 As Boolean, bFromLevelUnder2 As Boolean, bFromLevelUnder3 As Boolean
        Dim bFromLevelUnder4 As Boolean, bFromLevelUnder5 As Boolean, bFromLevelUnder6 As Boolean
        Dim bContinue1 As Boolean, bContinue2 As Boolean, bContinue3 As Boolean
        Dim bContinue4 As Boolean, bContinue5 As Boolean, bContinue6 As Boolean
        Dim bGroupConditional As Boolean
        Dim bSegmentRepeated As Boolean, bAbandon As Boolean
        Dim nodBase As MSXML2.IXMLDOMElement
        Dim eleActiveElement1 As MSXML2.IXMLDOMElement
        Dim eleActiveElement2 As MSXML2.IXMLDOMElement
        Dim eleActiveElement3 As MSXML2.IXMLDOMElement
        Dim eleActiveElement4 As MSXML2.IXMLDOMElement
        Dim eleActiveElement5 As MSXML2.IXMLDOMElement
        Dim eleActiveElement6 As MSXML2.IXMLDOMElement
        Dim bSpecialSegment As Boolean
        Dim bCheck As Boolean, iStartLevel As Integer, iEndLevel As Integer
        Dim iCurrentGroupLevel As Integer, iNewGroupLevel As Integer


        'i1Pointer = 0
        'i2Pointer = 0
        'i3Pointer = 0
        'i4Pointer = 0
        'i5Pointer = 0
        'i6Pointer = 0
        bFoundOldElement = False
        bFoundNewElement = False
        bGroupConditional = False
        bSegmentRepeated = False
        bAbandon = False
        bContinue1 = False
        bContinue2 = False
        bContinue3 = False
        bContinue4 = False
        bContinue5 = False
        bContinue6 = False
        bFromLevelUnder1 = False
        bFromLevelUnder2 = False
        bFromLevelUnder3 = False
        bFromLevelUnder4 = False
        bFromLevelUnder5 = False
        bFromLevelUnder6 = False
        iStartLevel = 0
        iEndLevel = 0
        bGroupChange = False
        iCurrentGroupLevel = 0
        iNewGroupLevel = 0

        Dim s As String
        s = sSegmentName

        'Used to exit the loop (we have to enter the loop to find the existing level)
        ' when the Segment is either LIN,SEQ or DOC.
        bSpecialSegment = False

        If sSegmentName = "LIN" Then
            'Set mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//LIN")
            'Resets groupvalues below LIN
            nodBase = eleLINElementParent
            'Check if at least 1 SEQ is imported in the previous LIN (had an example from Nordea with
            ' a LIN without a SEQ. Exception for the first LIN
            If GetGroupRep(Val(nodBase.getAttribute("number"))) > 0 Then
                If lSeqCounter = 0 Then
                    err.Raise(vbObjectError + 15092, "FindNextSegment", LRS(15092) _
                      & vbCrLf & LRS(15031, Str(lLINCounter)) _
                      & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath))
                End If
            End If

            bCheck = SetGroupRep(Val(nodBase.getAttribute("number")) + 1, True)

            'Add 1 to the group-counter
            If Not SetGroupRep(Val(nodBase.getAttribute("number")), False) Then
                ' XNET 05.12.2013 Update LRS to use %1 correctly
                err.Raise(vbObjectError + 15021, , LRS(15021, nodBase.getAttribute("number"), mrAppAdmImport.EDIFilePath))
                ' "Group "nodBase.getAttribute("number")" repeated to many times. Filename: XXXXX"
            End If
            iEndLevel = FindLevel(nodBase)
            nodBase = Nothing
            bSpecialSegment = True
            lLINCounter = lLINCounter + 1
            lSeqCounter = 0
            'Set FindNextSegment = mrCurrentElement
            'Exit Function
        ElseIf sSegmentName = "SEQ" Then
            'Set mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//SEQ")
            'Resets groupvalues below SEQ
            nodBase = eleSEQElementParent
            If Not SetGroupRep(Val(nodBase.getAttribute("number")) + 1, True) Then
                ' XNET 05.12.2013 Update LRS to use %1 correctly
                err.Raise(vbObjectError + 15021, , LRS(15021, nodBase.getAttribute("number")) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)))
                ' "Group "nodBase.getAttribute("number")" repeated to many times. Filename: XXXXX" LIN:
            End If
            'Add 1 to the group-counter
            bCheck = SetGroupRep(Val(nodBase.getAttribute("number")), False)
            iEndLevel = FindLevel(nodBase)
            nodBase = Nothing
            bSpecialSegment = True
            lSeqCounter = lSeqCounter + 1
            'Set FindNextSegment = mrCurrentElement
            'Exit Function
        ElseIf sSegmentName = "DOC" Then
            'Set mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//DOC")
            'Resets groupvalues below DOC
            nodBase = eleDOCElementParent
            If Not SetGroupRep(Val(nodBase.getAttribute("number")) + 1, True) Then
                ' XNET 05.12.2013 Update LRS to use %1 correctly
                err.Raise(vbObjectError + 15021, , LRS(15021, nodBase.getAttribute("number")) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                ' "Group "nodBase.getAttribute("number")" repeated to many times. Filename: XXXXX" LIN: SEQ:
            End If
            'Add 1 to the group-counter
            bCheck = SetGroupRep(Val(nodBase.getAttribute("number")), False)
            iEndLevel = FindLevel(nodBase)
            nodBase = Nothing
            bSpecialSegment = True
            'Set FindNextSegment = mrCurrentElement
            'Exit Function
        ElseIf sSegmentName = "CNT" Then
            mrCurrentElement = mrXmlDoc.documentElement.selectSingleNode("//CNT")
            iEndLevel = FindLevel(mrCurrentElement)
        End If

        'Find current grouplevel
        nodBase = mrCurrentElement.parentNode
        iCurrentGroupLevel = nodBase.getAttribute("number")

        'Set the basic node
        nodBase = eleBasicMappingElement


        'Check if the new segment is the same as the last one, thus the segment is repeated and
        ' we have to check if that is legal.
        If sSegmentName = mrCurrentElement.nodeName Then
            bSegmentRepeated = True
        Else
            iSegmentRepetition = 1
        End If

        For i1 = i1Pointer To nodBase.childNodes.length - 1
            eleActiveElement1 = nodBase.childNodes(i1)
            'If we come from the level under i2Pointer must be reset
            If bContinue1 = True Then
                bContinue1 = False
                i2Pointer = 0
            End If
            If bFoundOldElement Then
                'Check if the node is in use
                If eleActiveElement1.getAttribute("inuse") = "1" Then
                    'Check if we are dealing with a segment-node
                    If IsSegment(eleActiveElement1) Then
                        bContinue1 = False
                        '                'Check if the node is identical with the CurrentNode
                        '                If IsCurrentNode(nodBase.childNodes(i1)) Then
                        '                    bFoundElement = True
                        '                Else
                        'Check if we have found the correct node
                        If eleActiveElement1.nodeName = sSegmentName Then
                            mrCurrentElement = eleActiveElement1
                            If bSegmentRepeated Then
                                If Not SegmentRepeatOK(eleActiveElement1) Then
                                    'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                    ' always is Mandatory
                                    'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                    ' the group. Therefore we have to check if the group is repested. If so check if it can
                                    ' be repeated. If Yes then everything is OK.
                                    For i1SiblingCounter = 1 To eleActiveElement1.parentNode.childNodes.length - 1
                                        If Mandatory(nodBase.childNodes(i1SiblingCounter)) Then
                                            ' XNET 05.12.2013 Update LRS to use %1 correctly
                                            err.Raise(vbObjectError + 15022, , LRS(15022, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                            '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                            bAbandon = True
                                            Exit For
                                        End If
                                    Next i1SiblingCounter
                                    If Not bAbandon Then
                                        If Not GroupRepetitionOK(eleActiveElement1.parentNode) Then
                                            ' XNET 05.12.2013 Update LRS to use %1 correctly
                                            err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                            ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                            bAbandon = True
                                        End If
                                    End If
                                End If
                            End If
                            bFoundNewElement = True
                            bContinue1 = False

                        Else
                            'Check if the node is mandatory
                            If Mandatory(eleActiveElement1) And Not bGroupConditional Then
                                err.Raise(vbObjectError + 15023, , LRS(15023, eleActiveElement1.nodeName, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                bContinue1 = False
                            Else
                                bSegmentRepeated = False
                                bContinue1 = False
                            End If
                        End If
                        '                End If
                    Else
                        If Conditional(eleActiveElement1) Then
                            bGroupConditional = True
                        End If
                        If Not GroupRepetitionOK(eleActiveElement1) Then
                            err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                            ' "Group repeated to many times"
                            bContinue1 = False
                        Else
                            bContinue1 = True
                        End If
                    End If
                End If
            Else
                If eleActiveElement1.nodeName = mrCurrentElement.nodeName Then
                    bFoundOldElement = True
                    bContinue1 = False
                    iStartLevel = 1
                    If bSpecialSegment Then
                        bFoundNewElement = True
                        Exit For
                    End If
                    If bSegmentRepeated Then
                        'Have to subtract 1 from counter, because the segment may be repeated
                        i1 = i1 - 1
                    End If
                Else
                    bContinue1 = True
                End If
            End If
            If bContinue1 Then

                For i2 = i2Pointer To eleActiveElement1.childNodes.length - 1
                    eleActiveElement2 = eleActiveElement1.childNodes(i2)
                    'If we come from the level under i3Pointer must be reset
                    If bContinue2 = True Then
                        bContinue2 = False
                        i3Pointer = 0
                    End If
                    If bFoundOldElement Then
                        'Check if the node is in use
                        If eleActiveElement2.getAttribute("inuse") = "1" Then
                            'Check if we are dealing with a segment-node
                            If IsSegment(eleActiveElement2) Then
                                bContinue2 = False
                                'Check if we have found the correct node
                                If eleActiveElement2.nodeName = sSegmentName Then
                                    mrCurrentElement = eleActiveElement2
                                    If bSegmentRepeated Then
                                        If Not SegmentRepeatOK(eleActiveElement2) Then
                                            'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                            ' always is Mandatory
                                            'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                            ' the group. Therefore we have to check if the group is repested. If so check if it can
                                            ' be repeated. If Yes then everything is OK.
                                            For i2SiblingCounter = 1 To eleActiveElement2.parentNode.childNodes.length - 1
                                                If Mandatory(eleActiveElement1.childNodes(i2SiblingCounter)) Then
                                                    err.Raise(vbObjectError + 15022, , LRS(15022, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                    '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                    bAbandon = True
                                                    Exit For
                                                End If
                                            Next i2SiblingCounter
                                            If Not bAbandon Then
                                                If Not GroupRepetitionOK(eleActiveElement2.parentNode) Then
                                                    err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                    ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                    bAbandon = True
                                                End If
                                            End If
                                        End If
                                    End If
                                    bFoundNewElement = True
                                    bContinue2 = False
                                Else
                                    'Check if the node is mandatory
                                    If Mandatory(eleActiveElement2) And Not bGroupConditional Then
                                        err.Raise(vbObjectError + 15023, , LRS(15023, eleActiveElement2.nodeName, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                        ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                        bContinue2 = False
                                    Else
                                        'Reset the value
                                        bGroupConditional = False
                                        If i2 = NumberOfSegments(eleActiveElement1, i2) Then
                                            Exit For
                                        End If
                                        'FIX: Is it 100% sure that the first segment in a group is mandatory?
                                        ' If not this test isn't good enough. Then we have to continue the loop
                                        ' to check the rest of the segments.
                                        'bContinue2 = False
                                    End If
                                End If
                            Else
                                If Conditional(eleActiveElement2) Then
                                    bGroupConditional = True
                                End If
                                If Not GroupRepetitionOK(eleActiveElement2) Then
                                    err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                    ' "Group repeated to many times"
                                    bContinue2 = False
                                Else
                                    bContinue2 = True
                                End If
                            End If
                        End If
                    Else
                        If eleActiveElement2.nodeName = mrCurrentElement.nodeName Then
                            bFoundOldElement = True
                            bContinue2 = False
                            iStartLevel = 2
                            If bSpecialSegment Then
                                bFoundNewElement = True
                                Exit For
                            End If
                            If bSegmentRepeated Then
                                'Have to subtract 1 from counter, because the segment may be repeated
                                i2 = i2 - 1
                            End If
                        Else
                            bContinue2 = True
                        End If
                    End If
                    If bContinue2 Then

                        For i3 = i3Pointer To eleActiveElement2.childNodes.length - 1
                            eleActiveElement3 = eleActiveElement2.childNodes(i3)
                            'If we come from the level under i4Pointer must be reset
                            If bContinue3 = True Then
                                bContinue3 = False
                                i4Pointer = 0
                            End If
                            If bFoundOldElement Then
                                'Check if the node is in use
                                If eleActiveElement3.getAttribute("inuse") = "1" Then
                                    'Check if we are dealing with a segment-node
                                    If IsSegment(eleActiveElement3) Then
                                        bContinue3 = False
                                        'Check if we have found the correct node
                                        If eleActiveElement3.nodeName = sSegmentName Then
                                            mrCurrentElement = eleActiveElement3
                                            If bSegmentRepeated Then
                                                'The same segment occurs one more time.
                                                If Not SegmentRepeatOK(eleActiveElement3) Then
                                                    'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                    ' always is Mandatory
                                                    'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                    ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                    ' be repeated. If Yes then everything is OK.
                                                    For i3SiblingCounter = 1 To eleActiveElement3.parentNode.childNodes.length - 1
                                                        If Mandatory(eleActiveElement2.childNodes(i3SiblingCounter)) Then
                                                            err.Raise(vbObjectError + 15022, , LRS(15022, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                            '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                            bAbandon = True
                                                            Exit For
                                                        End If
                                                    Next i3SiblingCounter
                                                    If Not bAbandon Then
                                                        If Not GroupRepetitionOK(eleActiveElement3.parentNode) Then
                                                            err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                            ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                            bAbandon = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            bFoundNewElement = True
                                            bContinue3 = False
                                        Else
                                            'Check if the node is mandatory
                                            If Mandatory(eleActiveElement3) And Not bGroupConditional Then
                                                err.Raise(vbObjectError + 15023, , Replace(LRS(15023, eleActiveElement3.nodeName), "%2", sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                bContinue3 = False
                                            Else
                                                'Reset the value
                                                bGroupConditional = False
                                                If i3 = NumberOfSegments(eleActiveElement2, i3) Then
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    Else
                                        If Conditional(eleActiveElement3) Then
                                            bGroupConditional = True
                                        End If
                                        If Not GroupRepetitionOK(eleActiveElement3) Then
                                            err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                            ' "Group repeated to many times"
                                            bContinue3 = False
                                        Else
                                            bContinue3 = True
                                        End If
                                    End If
                                End If
                            Else
                                If eleActiveElement3.nodeName = mrCurrentElement.nodeName Then
                                    bFoundOldElement = True
                                    bContinue3 = False
                                    iStartLevel = 3
                                    If bSpecialSegment Then
                                        bFoundNewElement = True
                                        Exit For
                                    End If
                                    If bSegmentRepeated Then
                                        'Have to subtract 1 from counter, because the segment may be repeated
                                        i3 = i3 - 1
                                    End If
                                Else
                                    bContinue3 = True
                                End If
                            End If
                            If bContinue3 Then

                                For i4 = i4Pointer To eleActiveElement3.childNodes.length - 1
                                    eleActiveElement4 = eleActiveElement3.childNodes(i4)
                                    'If we come from the level under i5Pointer must be reset
                                    If bContinue4 = True Then
                                        bContinue4 = False
                                        i5Pointer = 0
                                    End If
                                    If bFoundOldElement Then
                                        'Check if the node is in use
                                        If eleActiveElement4.getAttribute("inuse") = "1" Then
                                            'Check if we are dealing with a segment-node
                                            If IsSegment(eleActiveElement4) Then
                                                bContinue4 = False
                                                'Check if we have found the correct node
                                                If eleActiveElement4.nodeName = sSegmentName Then
                                                    mrCurrentElement = eleActiveElement4
                                                    If bSegmentRepeated Then
                                                        If Not SegmentRepeatOK(eleActiveElement4) Then
                                                            'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                            ' always is Mandatory
                                                            'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                            ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                            ' be repeated. If Yes then everything is OK.
                                                            For i4SiblingCounter = 1 To eleActiveElement4.parentNode.childNodes.length - 1
                                                                If Mandatory(eleActiveElement3.childNodes(i4SiblingCounter)) Then
                                                                    err.Raise(vbObjectError + 15022, , LRS(15022, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                    '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                                    bAbandon = True
                                                                    Exit For
                                                                End If
                                                            Next i4SiblingCounter
                                                            If Not bAbandon Then
                                                                If Not GroupRepetitionOK(eleActiveElement4.parentNode) Then
                                                                    err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                    ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                                    bAbandon = True
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    bFoundNewElement = True
                                                    bContinue4 = False
                                                Else
                                                    'Check if the node is mandatory
                                                    If Mandatory(eleActiveElement4) And Not bGroupConditional Then
                                                        err.Raise(vbObjectError + 15023, , Replace(LRS(15023, eleActiveElement4.nodeName), "%2", sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                        ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                        bContinue4 = False
                                                        'End If
                                                    Else
                                                        'Reset the value
                                                        bGroupConditional = False
                                                        If i4 = NumberOfSegments(eleActiveElement3, i4) Then
                                                            Exit For
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If Conditional(eleActiveElement4) Then
                                                    bGroupConditional = True
                                                End If
                                                If Not GroupRepetitionOK(eleActiveElement4) Then
                                                    err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                    ' "Group repeated to many times"
                                                    bContinue4 = False
                                                Else
                                                    bContinue4 = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        If eleActiveElement4.nodeName = mrCurrentElement.nodeName Then
                                            bFoundOldElement = True
                                            bContinue4 = False
                                            iStartLevel = 4
                                            If bSpecialSegment Then
                                                bFoundNewElement = True
                                                Exit For
                                            End If
                                            If bSegmentRepeated Then
                                                'Have to subtract 1 from counter, because the segment may be repeated
                                                i4 = i4 - 1
                                            End If
                                        Else
                                            bContinue4 = True
                                        End If
                                    End If
                                    If bContinue4 Then

                                        For i5 = i5Pointer To eleActiveElement4.childNodes.length - 1
                                            eleActiveElement5 = eleActiveElement4.childNodes(i5)
                                            'If we come from the level under i6Pointer must be reset
                                            If bContinue5 = True Then
                                                bContinue5 = False
                                                i6Pointer = 0
                                            End If
                                            If bFoundOldElement Then
                                                'Check if the node is in use
                                                If eleActiveElement5.getAttribute("inuse") = "1" Then
                                                    'Check if we are dealing with a segment-node
                                                    If IsSegment(eleActiveElement5) Then
                                                        bContinue5 = False
                                                        'Check if we have found the correct node
                                                        If eleActiveElement5.nodeName = sSegmentName Then
                                                            mrCurrentElement = eleActiveElement5
                                                            If bSegmentRepeated Then
                                                                If Not SegmentRepeatOK(eleActiveElement5) Then
                                                                    'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                                    ' always is Mandatory
                                                                    'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                                    ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                                    ' be repeated. If Yes then everything is OK.
                                                                    For i5SiblingCounter = 1 To eleActiveElement5.parentNode.childNodes.length - 1
                                                                        If Mandatory(eleActiveElement4.childNodes(i5SiblingCounter)) Then
                                                                            err.Raise(vbObjectError + 15022, , LRS(15022, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                            '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                                            bAbandon = True
                                                                            Exit For
                                                                        End If
                                                                    Next i5SiblingCounter
                                                                    If Not bAbandon Then
                                                                        If Not GroupRepetitionOK(eleActiveElement5.parentNode) Then
                                                                            err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                            ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                                            bAbandon = True
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                            bFoundNewElement = True
                                                            bContinue5 = False
                                                        Else
                                                            'Check if the node is mandatory
                                                            If Mandatory(eleActiveElement5) And Not bGroupConditional Then
                                                                err.Raise(vbObjectError + 15023, , Replace(LRS(15023, eleActiveElement5.nodeName), "%2", sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                                bContinue5 = False
                                                            Else
                                                                'Reset the value
                                                                bGroupConditional = False
                                                                If i5 = NumberOfSegments(eleActiveElement4, i5) Then
                                                                    Exit For
                                                                End If
                                                            End If
                                                        End If
                                                    Else
                                                        If Conditional(eleActiveElement5) Then
                                                            bGroupConditional = True
                                                        End If
                                                        If Not GroupRepetitionOK(eleActiveElement5) Then
                                                            err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                            ' "Group repeated to many times"
                                                            bContinue5 = False
                                                        Else
                                                            bContinue5 = True
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If eleActiveElement5.nodeName = mrCurrentElement.nodeName Then
                                                    bFoundOldElement = True
                                                    bContinue5 = False
                                                    iStartLevel = 5
                                                    If bSpecialSegment Then
                                                        bFoundNewElement = True
                                                        Exit For
                                                    End If
                                                    If bSegmentRepeated Then
                                                        'Have to subtract 1 from counter, because the segment may be repeated
                                                        i5 = i5 - 1
                                                    End If
                                                Else
                                                    bContinue5 = True
                                                End If
                                            End If
                                            If bContinue5 Then

                                                For i6 = i6Pointer To eleActiveElement5.childNodes.length - 1
                                                    eleActiveElement6 = eleActiveElement5.childNodes(i6)
                                                    'If we come from the level under i7Pointer must be reset
                                                    If bContinue6 = True Then
                                                        bContinue6 = False
                                                        'i7Pointer = 0
                                                    End If
                                                    If bFoundOldElement Then
                                                        'Check if the node is in use
                                                        If eleActiveElement6.getAttribute("inuse") = "1" Then
                                                            'Check if we are dealing with a segment-node
                                                            If IsSegment(eleActiveElement6) Then
                                                                bContinue6 = False
                                                                'Check if we have found the correct node
                                                                If eleActiveElement6.nodeName = sSegmentName Then
                                                                    mrCurrentElement = eleActiveElement6
                                                                    If bSegmentRepeated Then
                                                                        If Not SegmentRepeatOK(eleActiveElement6) Then
                                                                            'FIX: Is this correct. This code relies on the fact that the first segment in a group
                                                                            ' always is Mandatory
                                                                            'Even is the segment itself can't be repeated, maybe this segment is the mandatory in
                                                                            ' the group. Therefore we have to check if the group is repested. If so check if it can
                                                                            ' be repeated. If Yes then everything is OK.
                                                                            For i6SiblingCounter = 1 To eleActiveElement6.parentNode.childNodes.length - 1
                                                                                If Mandatory(eleActiveElement5.childNodes(i6SiblingCounter)) Then
                                                                                    err.Raise(vbObjectError + 15022, , LRS(15022, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                                    '"Segment: " & sSegmentName & " repeated to many times.Filname LIN SEQ"
                                                                                    bAbandon = True
                                                                                    Exit For
                                                                                End If
                                                                            Next i6SiblingCounter
                                                                            If Not bAbandon Then
                                                                                If Not GroupRepetitionOK(eleActiveElement6.parentNode) Then
                                                                                    err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                                    ' "Group "iLastusedgroup" repeated to many times Filename, LIN, SEQ"
                                                                                    bAbandon = True
                                                                                End If
                                                                            End If
                                                                        End If
                                                                    End If
                                                                    bFoundNewElement = True
                                                                    bContinue6 = False
                                                                Else
                                                                    'Check if the node is mandatory
                                                                    If Mandatory(eleActiveElement6) And Not bGroupConditional Then
                                                                        err.Raise(vbObjectError + 15023, , Replace(LRS(15023, eleActiveElement6.nodeName), "%2", sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                        ' "Mandatory segment "nodBase.childNodes(i1).noedname" missing  Segment read "sSegmentName" Filename,LIN,SEQ
                                                                        bContinue6 = False
                                                                    Else
                                                                        'Reset the value
                                                                        bGroupConditional = False
                                                                        If i6 = NumberOfSegments(eleActiveElement5, i6) Then
                                                                            Exit For
                                                                        End If
                                                                    End If
                                                                End If
                                                            Else
                                                                If Conditional(eleActiveElement6) Then
                                                                    bGroupConditional = True
                                                                End If
                                                                If Not GroupRepetitionOK(eleActiveElement6) Then
                                                                    err.Raise(vbObjectError + 15021, , LRS(15021, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                                    ' "Group repeated to many times"
                                                                    bContinue6 = False
                                                                Else
                                                                    bContinue6 = True
                                                                End If
                                                            End If
                                                        End If
                                                    Else
                                                        If eleActiveElement6.nodeName = mrCurrentElement.nodeName Then
                                                            bFoundOldElement = True
                                                            bContinue6 = False
                                                            iStartLevel = 6
                                                            If bSpecialSegment Then
                                                                bFoundNewElement = True
                                                                Exit For
                                                            End If
                                                            If bSegmentRepeated Then
                                                                'Have to subtract 1 from counter, because the segment may be repeated
                                                                i6 = i6 - 1
                                                            End If
                                                        Else
                                                            bContinue6 = True
                                                        End If
                                                    End If
                                                    If bContinue6 Then
                                                        err.Raise(vbObjectError + 15024, , LRS(15024, Str(iLastUsedGroup)) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSeqCounter)))
                                                        ' "Too many levels"
                                                    End If

                                                    If bFoundNewElement Then
                                                        If iEndLevel = 0 Then
                                                            iEndLevel = 6
                                                        End If
                                                        Exit For
                                                    Else
                                                        bFromLevelUnder5 = True
                                                    End If
                                                    If i6 = eleActiveElement5.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup Then
                                                        i5 = i5 - 1
                                                    End If

                                                Next i6
                                            End If
                                            If bFoundNewElement Then
                                                If iEndLevel = 0 Then
                                                    iEndLevel = 5
                                                End If
                                                Exit For
                                            Else
                                                bFromLevelUnder5 = True
                                            End If
                                            If i5 = -1 Then
                                                If bFromLevelUnder6 And Not eleActiveElement4.childNodes(0).nodeName = "GROUP" Then
                                                    bFromLevelUnder6 = False
                                                End If
                                            Else
                                                If bFromLevelUnder6 And Not eleActiveElement5.nodeName = "GROUP" Then
                                                    i5 = i5 - 1
                                                    bFromLevelUnder6 = False
                                                End If
                                                If i5 = eleActiveElement4.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup Then
                                                    i4 = i4 - 1
                                                End If
                                            End If
                                        Next i5
                                    Else
                                        'Not continuing
                                        i5Pointer = 0
                                    End If
                                    If bFoundNewElement Then
                                        If iEndLevel = 0 Then
                                            iEndLevel = 4
                                        End If
                                        Exit For
                                    Else
                                        bFromLevelUnder4 = True
                                    End If
                                    If i4 = -1 Then
                                        If bFromLevelUnder5 And Not eleActiveElement3.childNodes(0).nodeName = "GROUP" Then
                                            bFromLevelUnder5 = False
                                        End If
                                    Else
                                        If bFromLevelUnder5 And Not eleActiveElement4.nodeName = "GROUP" Then
                                            i4 = i4 - 1
                                            bFromLevelUnder5 = False
                                        End If
                                        ' KJI: Ny Kode 20/12-2001. For � takle repetering av grupper
                                        ' If-statementet er lagt til under alle niv�er
                                        If i4 = eleActiveElement3.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup Then
                                            'If i4 = nodBase.childNodes(i1).childNodes(i2).childNodes(i3).childNodes.length - 1 And sSegmentName = "INP" And i3 = 8 Then
                                            i3 = i3 - 1
                                        End If
                                    End If
                                Next i4
                            Else
                                'Not continuing
                                i4Pointer = 0
                            End If
                            If bFoundNewElement Then
                                If iEndLevel = 0 Then
                                    iEndLevel = 3
                                End If
                                Exit For
                            Else
                                bFromLevelUnder3 = True
                            End If
                            If i3 = -1 Then
                                If bFromLevelUnder4 And Not eleActiveElement2.childNodes(0).nodeName = "GROUP" Then
                                    bFromLevelUnder4 = False
                                End If
                            Else
                                'New code 20/5-2003 by KI
                                'Old code - next 4 lines
                                '                        If bFromLevelUnder4 And Not eleActiveElement3.nodeName = "GROUP" Then
                                '                            i3 = i3 - 1
                                '                            bFromLevelUnder4 = False
                                '                        End If
                                'New code
                                If bFromLevelUnder4 Then
                                    If Not eleActiveElement3.nodeName = "GROUP" Then
                                        i3 = i3 - 1
                                        bFromLevelUnder4 = False
                                    ElseIf eleActiveElement3.hasChildNodes Then
                                        If eleActiveElement3.firstChild.nodeName = sSegmentName Then
                                            i3 = i3 - 1
                                            bFromLevelUnder4 = False
                                        End If
                                    End If
                                End If
                                'End new code - must be implemented on the other levels
                                If i3 = eleActiveElement2.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup Then
                                    i2 = i2 - 1
                                End If
                            End If
                        Next i3
                    Else
                        'Not continuing
                        i3Pointer = 0
                    End If
                    If bFoundNewElement Then
                        If iEndLevel = 0 Then
                            iEndLevel = 2
                        End If
                        Exit For
                    Else
                        bFromLevelUnder2 = True
                    End If
                    If i2 = -1 Then
                        If bFromLevelUnder3 And Not eleActiveElement1.childNodes(0).nodeName = "GROUP" Then
                            bFromLevelUnder3 = False
                        End If
                    Else
                        If bFromLevelUnder3 And Not eleActiveElement2.nodeName = "GROUP" Then
                            i2 = i2 - 1
                            bFromLevelUnder3 = False
                        End If
                        If i2 = eleActiveElement1.childNodes.length - 1 And sSegmentName = GetFirstMandatorySegInGroup Then
                            i1 = i1 - 1
                        End If
                    End If
                Next i2
            End If
            If bFoundNewElement Then
                If iEndLevel = 0 Then
                    iEndLevel = 1
                End If
                Exit For
            Else
                bFromLevelUnder1 = True
            End If
            If i1 = -1 Then
                If bFromLevelUnder2 And Not nodBase.childNodes(0).nodeName = "GROUP" Then
                    bFromLevelUnder2 = False
                End If
            Else
                If bFromLevelUnder2 And Not eleActiveElement1.nodeName = "GROUP" Then
                    i1 = i1 - 1
                    bFromLevelUnder2 = False
                End If
            End If
        Next i1

        If bSpecialSegment Then
            'We must reset the pointers to where we are in the mapping-file, because when we do jumps
            ' in the mapping the normal way to trace through the mapping doesn't function
            'The pointers are set according to the placements in the mappingfiles (not according to the groupnumbers
            'If in the mappingfile You have Group 0
            'Under group 0 You have the folloving elements
            ' 1 - UNH
            ' 2 - BGM
            ' 3 - DTM
            ' 4 - Group 1
            ' 5 - Group 2
            ' 6 - Group 3
            ' 7 - Group 4 - On the level below in this group You will find the LIN-element
            'To set the correct posinter for the LIN-level (ipointer1) set the
            ' iPointer to the number of elements prior to the LIN-element.
            ' In this example 6

            Select Case sSegmentName
                Case "LIN"
                    If sEDIFormat = "BANSTA" Then
                        If eSendingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                            i1Pointer = 7
                            i2Pointer = 0
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        ElseIf eSendingBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO Then
                            i1Pointer = 7
                            i2Pointer = 0
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        Else
                            i1Pointer = 6
                            i2Pointer = 0
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        End If
                    Else
                        i1Pointer = 6
                        i2Pointer = 0
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    End If
                    mrCurrentElement = eleLINElement
                Case "SEQ"
                    If sEDIFormat = "CREMUL" Then
                        i1Pointer = 6
                        i2Pointer = 7
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "PAYMUL" Then
                        i1Pointer = 6
                        i2Pointer = 11
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "DEBMUL" Then
                        i1Pointer = 6
                        i2Pointer = 7
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "BANSTA" Then
                        If eSendingBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
                            i1Pointer = 7
                            i2Pointer = 5
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        ElseIf eSendingBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO Then
                            i1Pointer = 7
                            i2Pointer = 5
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        Else
                            i1Pointer = 6
                            i2Pointer = 5
                            i3Pointer = 0
                            i4Pointer = 0
                            i5Pointer = 0
                            i6Pointer = 0
                        End If
                    End If
                    mrCurrentElement = eleSEQElement
                Case "DOC"
                    If sEDIFormat = "CREMUL" Then
                        i1Pointer = 6
                        i2Pointer = 7
                        i3Pointer = 11
                        i4Pointer = 2
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "PAYMUL" Then
                        i1Pointer = 6
                        i2Pointer = 11
                        i3Pointer = 10
                        i4Pointer = 2
                        i5Pointer = 0
                        i6Pointer = 0
                    ElseIf sEDIFormat = "COACSU" Then
                        i1Pointer = 9
                        i2Pointer = 0
                        i3Pointer = 0
                        i4Pointer = 0
                        i5Pointer = 0
                        i6Pointer = 0
                    End If
                    mrCurrentElement = eleDOCElement
            End Select
        Else
            i1Pointer = i1
            i2Pointer = i2
            i3Pointer = i3
            i4Pointer = i4
            i5Pointer = i5
            i6Pointer = i6
        End If

        If sSegmentName = "UNS" Then
            If sEDIFormat = "COACSU" Then
                i1Pointer = 10
                i2Pointer = 0
                i3Pointer = 0
                i4Pointer = 0
                i5Pointer = 0
                i6Pointer = 0
            End If
        End If

        If mrCurrentElement.nodeName <> sSegmentName Then
            Err.Raise(vbObjectError + 15025, , LRS(15025, sSegmentName) & vbCrLf & LRS(15030, mrAppAdmImport.EDIFilePath) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)))
            ' "Unexpected segment in file." & vbCrLf & "Segment: " & sSegmentName
        End If

        iLevelChange = iStartLevel - iEndLevel

        nodBase = mrCurrentElement.parentNode

        iNewGroupLevel = nodBase.getAttribute("number")
        If iCurrentGroupLevel = iNewGroupLevel Then
            bGroupChange = False
        Else
            bGroupChange = True
        End If

        nodBase = Nothing

        eleActiveElement1 = Nothing
        eleActiveElement2 = Nothing
        eleActiveElement3 = Nothing
        eleActiveElement4 = Nothing
        eleActiveElement5 = Nothing
        eleActiveElement6 = Nothing

FoundChange:
        FindNextSegment = mrCurrentElement

    End Function


End Class
