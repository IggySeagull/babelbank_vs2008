Option Strict Off
Option Explicit On

Friend Class frmStatistics
	Inherits System.Windows.Forms.Form
	Private sFrom As String
	Private sTo As String
	Private sPassword As String
	Private bContinue As Boolean
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 9, 160, 480, 160)
    End Sub

	' Setup, called from rp_952_Statistics
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bContinue = False
		Me.Hide()
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		sFrom = Me.txtFromDate.Text
		sTo = Me.txtToDate.Text
		
        If Val(sFrom) < 20091001 And Val(sFrom) > Year(Date.Today) * 10000 Then
            MsgBox(sFrom & " " & LRS(60164) & ",vbExclamation,lrs(60163)")
        End If
        If Val(sTo) < 20091001 And Val(sTo) > Year(Date.Today) * 10000 Then
            MsgBox(sTo & " " & LRS(60164) & ",vbExclamation,lrs(60163)")
        End If
		Me.Hide()
		bContinue = True
	End Sub
	
	Private Sub frmStatistics_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormvbStyle(Me, "")
		FormLRSCaptions(Me)
		'FormSelectAllTextboxs Me
	End Sub
	Private Sub txtFromDate_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFromDate.Enter
		Me.txtFromDate.SelectionStart = 0
		Me.txtFromDate.SelectionLength = Len(Me.txtFromDate.Text)
	End Sub
	Private Sub txtToDate_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtToDate.Enter
		Me.txtToDate.SelectionStart = 0
		Me.txtToDate.SelectionLength = Len(Me.txtToDate.Text)
		
	End Sub
	Public Function GetSetFromDate(ByRef sFromDate As String) As Object
		If Not EmptyString(sFromDate) Then
			Me.txtFromDate.Text = sFromDate
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object GetSetFromDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetSetFromDate = Me.txtFromDate.Text
	End Function
	Public Function GetSetToDate(ByRef sToDate As String) As Object
		If Not EmptyString(sToDate) Then
			Me.txtToDate.Text = sToDate
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object GetSetToDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetSetToDate = Me.txtToDate.Text
	End Function
	Public Function GetPW() As Object
		'UPGRADE_WARNING: Couldn't resolve default property of object GetPW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetPW = Me.txtPW.Text
	End Function
	'UPGRADE_NOTE: Continue was upgraded to Continue_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Function Continue_Renamed() As Object
		'UPGRADE_WARNING: Couldn't resolve default property of object Continue_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Continue_Renamed = bContinue
	End Function
End Class
