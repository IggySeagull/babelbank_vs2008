Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("BabelReport_NET.BabelReport")> Public Class BabelReport
    'Private arView As New vbBabel.arBBView
    Private arView As vbBabel.arBBView
    Private oOwnerForm As System.Windows.Forms.Form
    'Private cnProfile As ADODB.Connection
    Private oMyDal As vbBabel.DAL = Nothing
    Private oBabelLog As vbLog.vbLogging
    Private bLog As Boolean
    Private oBabelFiles As vbBabel.BabelFiles
    Private bPreview As Boolean
    Private eReportTime As vbBabel.BabelFiles.ReportTime
    Private sSpecial As String
    Private bSpecialReport As Boolean
    Private iReportNumber As Integer
    Private bPrinter As Boolean
    Private bAskForPrinter As Boolean
    Private bEmail As Boolean
    Private bEmptyReport As Boolean
    Private sExportType As String
    Private sExportFilename As String
    Private iBreakLevel As Integer
    'DetailLevel - not in use
    Private sHeading As String
    'BitmapFilename - not in use
    Private sPrinterName As String
    Private bUseLongDate As Boolean
    'ReportName  - not a part of the actual print
    Private bIncludeOCR As Boolean
    Private iReportSort As Integer
    Private bReportOnDatabase As Boolean
    Private bSavebeforeReport As Boolean
    Private sMyReportSQL As String
    'SpecialReport - not in use
    Private sEMailAddresses As String
    Private bActivateSpecialSQL As Boolean
    Private sVISMA_ConnectionString As String
    Private sVISMA_BBDB As String

    Private bBBIsInSilentMode As Boolean
    Private sTmpPDF As String ' path & name for pdf-file as email attachment

    Private iMyCompanyID As Integer 'Should be set from outside
    Private iMyFileSetupID As Integer 'Should be set from outside

    Private aEMailAddresses() As String
    Private bReportOnSelectedItems As Boolean  ' If True, then check oPayment.ToSpecialReport, and report only if = True

    'OLD DECLARATIONS
    'Private oBabelFile As vbBabel.BabelFile
    'Private oReports As vbBabel.Reports
    'Private oReport As vbBabel.Report
    'Private sEMailAdresses As String
    'Private aEMailAddresses() As String
    'Private sTmpPDF As String ' path & name for pdf-file as email attachment
    'Private sJob As String ' Preview, Print or Export
    'Private bReportRun As Boolean
    ''Private bShow510 As Boolean ' Show (or not) OCR-trans (paycode=510)
    'Private aReportArray(,) As Object
    ' '' Added 21.08.06, allow report for one client at a time (used for matchingcustomers)
    ''Private sReportClient As String
    ' '' added 12.09.07 to be able to log in reportclass

    '********* START PROPERTY SETTINGS ***********************
    ' New 09.09.05 to pass handle to owner form. Used for .Show
	Public WriteOnly Property OwnerForm() As Object
		Set(ByVal Value As Object)
            'If RunTime Then
            oOwnerForm = Value
            'End If
		End Set
	End Property
	Public WriteOnly Property BabelLog() As vbLog.vbLogging
		Set(ByVal Value As vbLog.vbLogging)
			oBabelLog = Value
		End Set
	End Property
    Public Property LoggingActivated() As Boolean
        Get
            LoggingActivated = bLog
        End Get
        Set(ByVal Value As Boolean)
            bLog = Value
        End Set
    End Property
    Public Property ReportOnSelectedItems() As Boolean
        Get
            ReportOnSelectedItems = bReportOnSelectedItems
        End Get
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public Property VB_ReportTime() As vbBabel.BabelFiles.ReportTime
        Get
            VB_ReportTime = eReportTime
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.ReportTime)
            eReportTime = Value
        End Set
    End Property
    Public Property Preview() As Boolean
        Get
            Preview = bPreview
        End Get
        Set(ByVal Value As Boolean)
            bPreview = Value
        End Set
    End Property
    Public Property Special() As String
        Get
            Special = sSpecial
        End Get
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public Property ReportNumber() As Integer
        Get
            ReportNumber = iReportNumber
        End Get
        Set(ByVal Value As Integer)
            iReportNumber = Value
        End Set
    End Property
    Public Property ToPrint() As Boolean
        Get
            ToPrint = bPrinter
        End Get
        Set(ByVal Value As Boolean)
            bPrinter = Value
        End Set
    End Property
    Public Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
        Set(ByVal Value As Boolean)
            bEmptyReport = Value
        End Set
    End Property
    Public Property AskForPrinter() As Boolean
        Get
            AskForPrinter = bAskForPrinter
        End Get
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Public Property ToEmail() As Boolean
        Get
            ToEmail = bEmail
        End Get
        Set(ByVal Value As Boolean)
            bEmail = Value
        End Set
    End Property
    Public Property ExportType() As String
        Get
            ExportType = sExportType
        End Get
        Set(ByVal Value As String)
            sExportType = Value
        End Set
    End Property
    Public Property ExportFilename() As String
        Get
            ExportFilename = sExportFilename
        End Get
        Set(ByVal Value As String)
            sExportFilename = Value
        End Set
    End Property
    Public Property BreakLevel() As Integer
        Get
            BreakLevel = iBreakLevel
        End Get
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public Property Heading() As String
        Get
            Heading = sHeading
        End Get
        Set(ByVal Value As String)
            sHeading = Value
        End Set
    End Property
    Public Property PrinterName() As String
        Get
            PrinterName = sPrinterName
        End Get
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public Property UseLongDate() As Boolean
        Get
            UseLongDate = bUseLongDate
        End Get
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public Property IncludeOCR() As Boolean
        Get
            IncludeOCR = bIncludeOCR
        End Get
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public Property ReportSort() As Integer
        Get
            ReportSort = iReportSort
        End Get
        Set(ByVal Value As Integer)
            iReportSort = Value
        End Set
    End Property
    Public Property ReportOnDatabase() As Boolean
        Get
            ReportOnDatabase = bReportOnDatabase
        End Get
        Set(ByVal Value As Boolean)
            bReportOnDatabase = Value
        End Set
    End Property
    Public Property SavebeforeReport() As Boolean
        Get
            SavebeforeReport = bSavebeforeReport
        End Get
        Set(ByVal Value As Boolean)
            bSavebeforeReport = Value
        End Set
    End Property
    Public Property ActivateSpecialSQL() As Boolean
        Get
            ActivateSpecialSQL = bActivateSpecialSQL
        End Get
        Set(ByVal Value As Boolean)
            bActivateSpecialSQL = Value
        End Set
    End Property
    Public Function AddEmailAddress(ByVal sEmailAddress As String) As Boolean
        Dim iElementsInArray As Integer

        If Array_IsEmpty(aEMailAddresses) Then
            ReDim Preserve aEMailAddresses(0)
            iElementsInArray = 0
        Else
            iElementsInArray = aEMailAddresses.GetUpperBound(0)
            iElementsInArray = iElementsInArray + 1
            ReDim Preserve aEMailAddresses(iElementsInArray)
        End If
        aEMailAddresses(iElementsInArray) = sEmailAddress
    End Function
    Public Property VISMA_ConnectionString() As String
        Get
            VISMA_ConnectionString = sVISMA_ConnectionString
        End Get
        Set(ByVal Value As String)
            sVISMA_ConnectionString = Value
        End Set
    End Property
    Public Property VISMA_BBDB() As String
        Get
            VISMA_BBDB = sVISMA_BBDB
        End Get
        Set(ByVal Value As String)
            sVISMA_BBDB = Value
        End Set
    End Property

    'TODO - Must comment all property setting from here (check their use) 
    'Public WriteOnly Property EMailAddresses() As Object()
    '    Set(ByVal Value() As Object)
    '        Dim i As Short
    '        ReDim aEMailAddresses(UBound(Value))
    '        For i = 0 To UBound(Value)
    '            aEMailAddresses(i) = Value(i)
    '        Next
    '    End Set
    'End Property
    'Public WriteOnly Property ReportArray() As Object(,)
    '    Set(ByVal Value(,) As Object)
    '        Dim i As Short
    '        Dim j As Short
    '        ReDim aReportArray(UBound(Value, 1), UBound(Value, 2))
    '        For i = 0 To UBound(Value, 2)
    '            For j = 0 To UBound(Value, 1)
    '                aReportArray(j, i) = Value(j, i)
    '            Next j
    '            'aReportArray(i) = aNewval(i)
    '        Next i
    '    End Set
    'End Property
    ''10.02.2011 - Next tree properties removed by Kjell. Not longer possible to report on oBabelFile. We don't use oReport anymore
    'Public WriteOnly Property BabelFile() As vbBabel.BabelFile
    '    Set(ByVal Value As vbBabel.BabelFile)
    '        oBabelFile = Value
    '    End Set
    'End Property
    'Public WriteOnly Property Reports() As vbBabel.Reports
    '    Set(ByVal Value As vbBabel.Reports)
    '        oReports = Value
    '    End Set
    'End Property
    'Public WriteOnly Property Report() As vbBabel.Report
    '    Set(ByVal Value As vbBabel.Report)
    '        oReport = Value
    '    End Set
    'End Property
    'Public Property Show510() As Boolean
    '    Get
    '        IncludeOCR = bIncludeOCR
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        bIncludeOCR = Value
    '    End Set
    'End Property
    '********* END PROPERTY SETTINGS ***********************

    Public Sub BBIsInSilentMode()
        bBBIsInSilentMode = True
    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        bIncludeOCR = True ' Default show OCR and Autogiro
        bBBIsInSilentMode = False
        bSpecialReport = False
        bEmptyReport = False
        sVISMA_ConnectionString = ""
        sVISMA_BBDB = ""
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Public Function RunReports(Optional ByVal iFileSetup_ID As Integer = -1) As Boolean
        Dim sMyBBDatabase As String
        Dim sMySQL As String
        'Dim sSpecialText As String


		Try
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

            iMyCompanyID = 1  'TODO: Hardcoded CompanyID
            iMyFileSetupID = iFileSetup_ID

            sMyBBDatabase = BB_DatabasePath()

            'Set cnProfile = New ADODB.Connection
            If sMyBBDatabase <> "" Then

                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                sMySQL = "SELECT * FROM Reports WHERE ReportWhen = " & Trim(Str(eReportTime)) & " AND Company_ID = " & iMyCompanyID.ToString & " AND FileSetup_ID = " & iMyFileSetupID.ToString & " ORDER BY ReportNO"

                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then
                    Do While oMyDal.Reader_ReadRecord

                        bSpecialReport = CBool(oMyDal.Reader_GetString("SpecialReport"))
                        iReportNumber = CInt(oMyDal.Reader_GetString("ReportNO"))
                        bPreview = CBool(oMyDal.Reader_GetString("Preview"))
                        bPrinter = CBool(oMyDal.Reader_GetString("Printer"))
                        bAskForPrinter = CBool(oMyDal.Reader_GetString("AskForPrinter"))
                        bEmail = CBool(oMyDal.Reader_GetString("EMail"))
                        sExportType = oMyDal.Reader_GetString("ExportType")
                        sExportFilename = oMyDal.Reader_GetString("ExportFilename")
                        'rsReports.Fields("BreakLevel").Value - not in use
                        iBreakLevel = CInt(oMyDal.Reader_GetString("ReportSort"))
                        'DetailLevel - not in use
                        sHeading = oMyDal.Reader_GetString("Heading")
                        'BitmapFilename - not in use
                        sPrinterName = oMyDal.Reader_GetString("PrinterName")
                        bUseLongDate = CBool(oMyDal.Reader_GetString("LongDate"))
                        'ReportName  - not a part of the actual print
                        bIncludeOCR = CBool(oMyDal.Reader_GetString("IncludeOCR"))
                        iReportSort = CInt(oMyDal.Reader_GetString("ReportSort"))
                        bReportOnDatabase = CBool(oMyDal.Reader_GetString("ReportOnDatabase"))
                        bSavebeforeReport = CBool(oMyDal.Reader_GetString("SaveBeforeReport"))
                        sMyReportSQL = oMyDal.Reader_GetString("ReportSQL")
                        sEMailAddresses = oMyDal.Reader_GetString("eMailAddresses")
                        bActivateSpecialSQL = CBool(oMyDal.Reader_GetString("ActivateSpecialSQL"))

                        '27.04.2022 - Added next If to be able to run report 510 even if we do not have any BabelFile
                        If eReportTime = vbBabel.BabelFiles.ReportTime.Before_End Then
                            If iReportNumber <> 510 Then '510 = DayTotals
                                If oBabelFiles Is Nothing Then
                                    RunReports = True
                                    Exit Function
                                Else
                                    If oBabelFiles.Count > 0 Then
                                        RunReports = True
                                        Exit Function
                                    End If
                                End If
                            End If
                        End If


                        If Not RunReport() Then '09.07.2014 - Added this IF
                            RunReports = False
                            Exit Function
                        End If

                    Loop
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

                RunReports = True
            Else
                RunReports = False
            End If

        Catch ex As Exception

            'OLD - Throw New Exception("Function: RunReports" & vbCrLf & ex.Message)
            Throw New vbBabel.Payment.PaymentException("Function: RunReport" & vbNewLine & LRS(40026) & ": " & iReportNumber.ToString & vbCrLf & ex.Message, ex, Nothing, "", "")

        Finally

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try


        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

    End Function

    Public Function RunReport() As Boolean

        Dim myReport As Object
        'Dim myReport As DataDynamics.ActiveReports.ActiveReport   '.rptDocument

        Dim rptDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String
        Dim sMySQLSorting As String
        Dim sAddToWhereClause As String
        Dim bReportFromDatabase As Boolean
        Dim sFilenameOut As String
        Dim iErr As Short
        Dim sErrMessage As String
        Dim bProfileInUse As Boolean
        Dim htmlExport As New DataDynamics.ActiveReports.Export.Html.HtmlExport()
        Dim pdfExport As New DataDynamics.ActiveReports.Export.Pdf.PdfExport()
        Dim rtfExport As New DataDynamics.ActiveReports.Export.Rtf.RtfExport()
        Dim txtExport As New DataDynamics.ActiveReports.Export.Text.TextExport()
        Dim tiffExport As New DataDynamics.ActiveReports.Export.Tiff.TiffExport()
        Dim xlsExport As New DataDynamics.ActiveReports.Export.Xls.XlsExport()
        Dim iMyClientID As Integer
        Dim iCounter As Integer
        Dim iPos As Integer
        Dim iPos2 As Integer
        Dim sTemp As String = ""
        Dim bReturnValue As Boolean = True
        Dim sConnectionString As String = ""
        Dim bSQLServer As Boolean
		Dim bThisIsVisma As Boolean = False
		'Dim VismaConnection As New System.Data.Odbc.OdbcConnection
		'Dim VismaCommand As New System.Data.Odbc.OdbcCommand
		'Dim VismaAdapter As New System.Data.Odbc.OdbcDataAdapter
		'---
		' 10.11.2021 - must change to oleDB as we have stopped using ODBC for Visma
		Dim VismaConnection As New System.Data.OleDb.OleDbConnection
		Dim VismaCommand As New System.Data.OleDb.OleDbCommand
		Dim VismaAdapter As New System.Data.OleDb.OleDbDataAdapter

		'---
		Dim VismaData As New System.Data.DataTable

        Dim bRunThisReport As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bClientFiles As Boolean ' 09.03.06 True if ��� or � in exportfilename
        Dim sClientNo As String ' 09.03.06 Tells which client is exported in reportfile
        Dim oClient As Client
        Dim oaccount As Account
        Dim sAccountNo As String
        Dim bAccountFound As Boolean
        Dim bNoMoreClients As Boolean
        ' 02.02.2017 added next
        Dim bLogBama As Boolean = False
        Dim bShowMessage As Boolean = True

        '21.04.2020 - Added report 1001

        'Removed 15.10.2019
        'Try

        'TODO BABELBANK - Remove next two IFs when the problem for BAMA and SISMO is soved
        If Not oBabelFiles Is Nothing Then
            If oBabelFiles.Count > 0 Then
                If oBabelFiles.Item(1).VB_ProfileInUse Then
                    If iReportNumber = 504 And oBabelFiles.VB_Profile.CompanyName = "BAMA" And bLog Then
                        bLogBama = True
                    End If
                End If
            End If
        End If
        If sSpecial = "SISMO" Or sSpecial = "FJORDKRAFT" Then '09.07.2018 - Added Fjordkraft
            bLogBama = True
        End If
        '' for errorhandling;
        'Dim iErr As Short
        'Dim sErr As String

        bSQLServer = BB_UseSQLServer()

        sTmpPDF = Left(BB_DatabasePath, InStrRev(BB_DatabasePath, "\")) & "BabelBank.pdf"

        bRunThisReport = True

        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("BabelReport.RunReport: �verst i RunReport. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
        End If

        If bRunThisReport Then

            'Check if Profile is in use
            If Not oBabelFiles Is Nothing Then
                If oBabelFiles.Count > 0 Then
                    If oBabelFiles.Item(1).VB_ProfileInUse Then
                        bProfileInUse = True
                    Else
                        bProfileInUse = False
                    End If
                Else
                    bProfileInUse = False
                End If
            Else
                bProfileInUse = False
            End If

            If bLog Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("BabelReport.RunReport: F�r kall til frmViewer. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
            End If

            If bReportOnDatabase Then

                'Start by removing old shit from the DB
                '29.09.2015 - Added next IF for Patentstyret where we don't have a BabelFiles
                If Not oBabelFiles Is Nothing Then
                    oBabelFiles.RemoveReportData()
                End If

                If bSavebeforeReport Then
                    If bIncludeOCR Then
                        'Must be set to save OCR-payments in oBabelFiles.MATCH_aveData
                        'May this interfere with matching profiles where you want an OCR-report but not match OCR-payments????
                        oBabelFiles.MATCH_MatchOCR = True
                    End If
                    If sSpecial = "SISMO" Then
                        sSpecial = ""
                    End If
                    If Not oBabelFiles.MATCH_SaveData(False, False, False, False, sSpecial, True, True) Then
                        Throw New Exception(LRSCommon(45002) & oBabelFiles.MATCH_ErrorText)
                    End If

                End If
            End If

            ' added 09.03.06 due to possible clientsplitted reports
            ' based on exportfilename with $ or �
            Do
                If bLogBama Then  ' 02.02.2017
                    oBabelLog.Heading = "BabelBank info: "
                    oBabelLog.AddLogEvent("BabelReport.RunReport: I starten av rapportloop " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                End If

                ' 17.12.2014 -
                ' at the bottom of Do/Loop, we are releaseing rptDataSource-
                ' if we are looping, like in clientreporting, must open it again;
                rptDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource()


                ' added 09.03.06
                ' check if clientfiles;
                If bProfileInUse Then
                    If Len(sExportType) > 0 And (Len(sExportFilename) - Len(Replace(sExportFilename, "�", "")) > 0 Or Len(sExportFilename) - Len(Replace(sExportFilename, "�", "")) > 0) Then
                        ' Clientno in filenames
                        bClientFiles = True
                        bNoMoreClients = True
                        ' Check which clients we have records for
                        For Each oClient In oBabelFiles.VB_Profile.FileSetups(oBabelFiles(1).VB_FileSetupID).Clients
                            bAccountFound = False
                            ' Check if this client is handled before:
                            If oClient.tmpFlag <> 9 Then
                                bNoMoreClients = False
                                sClientNo = oClient.ClientNo
                                iMyClientID = oClient.Client_ID
                                oClient.tmpFlag = 9

                                '*****************************************************************************************
                                '21.01.2020 - KI: This code beneath may cause problems in a specific case
                                'bReportOnDatabase must be True ,bSavebeforeReport must be False (probably) and it must be report per Client
                                'If you have data stored in the BB databse with valid criterias (client f.ex.) to be on the report, but
                                ' no payments in the BabelFiles object fulfill the criteria. If so no report will be created.
                                'If so the code should be something like this
                                'If bReportOnDatabase And Not bSavebeforeReport Then 'It is allways a clientreport when we are here!
                                '    bAccountFound = True
                                'Else
                                'And add an End If after the For Each Account loop
                                For Each oaccount In oClient.Accounts
                                    sAccountNo = oaccount.Account

                                    For Each oBabelFile In oBabelFiles
                                        For Each oBatch In oBabelFile.Batches
                                            For Each oPayment In oBatch.Payments
                                                If oPayment.I_Account = sAccountNo Then
                                                    bAccountFound = True
                                                    Exit For
                                                End If
                                            Next oPayment
                                            If bAccountFound Then
                                                ' set Clientno into frmViewer, where each reports can find the clientno
                                                'arView.SetClientNo(sClientNo)
                                                Exit For
                                            End If
                                        Next oBatch
                                        If bAccountFound Then
                                            Exit For
                                        End If
                                    Next oBabelFile
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oaccount
                                If bAccountFound Then
                                    Exit For
                                End If
                            End If
                        Next oClient
                        If bNoMoreClients Then

                            If bLogBama Then  ' 02.02.2017
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: Ikke flere klienter " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                            End If

                            '21.01.2020 - Added this to be able to report on clients if the data should be saved, but need to delete them before exit
                            If bReportOnDatabase Then
                                If bSavebeforeReport Then

                                    'Remove reportdata from the DB
                                    oBabelFiles.RemoveReportData()

                                End If
                            End If

                            Exit Do 'All clients in the setup are tested, exit the loop
                        Else
                            If bLogBama Then  ' 02.02.2017
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: Funnet ny klient " & oClient.ClientNo, 8) 'vbLogEventTypeInformationDetail
                            End If
                        End If
                    End If
                End If ' If bProfileInUse Then


                If bLog Then
                    oBabelLog.Heading = "BabelBank info: "
                    oBabelLog.AddLogEvent("BabelReport.RunReport: F�r ArViewer.Show. Rapport " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                End If


                If iReportNumber > 0 Then '-1 = Run before TODO: This test should be unnecessary since we use ReportWhen

                    bEmptyReport = False

                    Select Case iReportNumber

                        Case 1
                            '001 - Batches report
                            myReport = New rp_001_Batches

                        Case 2
                            '002 - Payment report (without details/invoices)
                            myReport = New rp_002_Payment

                        Case 3
                            '003 - Payment detail report (unregarding match, mathctype etc...
                            myReport = New rp_003_PaymentInvoice
                            If sSpecial = "SGFINANS_AML" Then
                                bReportOnSelectedItems = True
                            End If

                        Case 4
                            '004 - Used for reporting OCR incoming payments. Only! Not Autogiro
                            myReport = New rp_004_IncomingOCR

                        Case 5
                            '005 Report FIK incoming payments
                            myReport = New rp_005_IncomingFIK

                        Case 7
                            '007 - Special report for US ACH/BAI 
                            myReport = New rp_007_IncomingACHLockbox

                        Case 11
                            '011 - Rejected payments - Use report 003
                            myReport = New rp_003_PaymentInvoice

                        Case 12
                            '012 - CONTRL
                            myReport = New rp_012_CONTRL

                        Case 503
                            '503 - CREMUL Unmatched
                            myReport = New rp_503_Cremul_Unmatched
                            If bLogBama Then  ' 09.02.2018
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: laget ny instans av rapport " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                            End If

                        Case 504
                            '504 - CREMUL matched
                            myReport = New rp_504_Cremul_Matched
                            If bLogBama Then  ' 02.02.2017
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: laget ny instans av rapport " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                            End If

                        Case 505
                            '505 - CREMUL Control
                            myReport = New rp_505_Cremul_Control

                        Case 506
                            '506 - Matching summary
                            myReport = New rp_506_Matching_summary

                        Case 507
                            '507 - CREMUL On-account postings, use report 504
                            myReport = New rp_504_Cremul_Matched

                        Case 508
                            '508 - CREMUL GL postings, use report 504
                            myReport = New rp_504_Cremul_Matched

                        Case 510
                            '510 - DayTotals
                            myReport = New rp_510_DayTotals

                        Case 512
                            '512 - DayTotals Aggregated
                            myReport = New rp_510_DayTotals

                        Case 603 ' Added 25.08.2017
                            '603 - AML report
                            myReport = New rp_603_AML_Report

                        Case 912
                            '003 - Payment detail report (unregarding match, mathctype etc...
                            myReport = New rp_003_PaymentInvoice

                        Case 950
                            If sSpecial = "DNBNORFINANS_CREMUL" Then
                                myReport = New rp_1005_DnBNORFinans_CREMUL
                            Else
                                Throw New Exception(LRS(13012, iReportNumber.ToString))
                            End If

                        Case 952
                            '952 - Statistics
                            myReport = New rp_952_Statistics

                        Case 990
                            '990 - Client overview
                            myReport = New rp_990_Clients

                        Case 992
                            '992 - VismaDetail and VismaError
                            myReport = New rp_992_VismaDetail
                            bThisIsVisma = True

                        Case 1001
                            '992 - VismaDetail and VismaError
                            myReport = New rp_1001_Matched_KID

                        Case 1002
                            '1002 - VismaPayments
                            myReport = New rp_002_Payment
                            bThisIsVisma = True

                        Case 1010
                            '1010 - Visma total
                            myReport = New rp_1010_Visma
                            bThisIsVisma = True

                        Case Else 'TODO for real - Create a proper errormessage here
                            Throw New Exception(LRS(13012, iReportNumber.ToString))

                    End Select

                    If bSpecialReport And bProfileInUse Then
                        sMySQL = sMyReportSQL
                    Else
                        If bReportOnDatabase Then
                            If bActivateSpecialSQL Then
                                sMySQL = Replace(sMyReportSQL, "@?@", "'")
                            Else
                                sMySQL = RetrieveSQLForReports(bSQLServer, iReportNumber, iBreakLevel, bIncludeOCR, bSavebeforeReport, sClientNo)

                            End If
                        End If
                    End If

                    'Set some general parameters in the reports
                    If sMySQL Is Nothing Then
                        myReport.BreakLevel = iBreakLevel
                    Else
                        If bReportOnDatabase And bActivateSpecialSQL And sMySQL.IndexOf("AS BreakField", 0, StringComparison.CurrentCultureIgnoreCase) > -1 Then
                            myReport.BreakLevel = 999
                        Else
                            myReport.BreakLevel = iBreakLevel
                        End If
                    End If
                    myReport.ReportName = sHeading
                    myReport.Special = sSpecial
                    myReport.UseLongDate = bUseLongDate
                    myReport.ReportNumber = iReportNumber
                    myReport.ReportFromDatabase = bReportOnDatabase
                    myReport.PrintReport = bPrinter
                    myReport.PrinterName = sPrinterName
                    myReport.AskForPrinter = bAskForPrinter
                    If iReportNumber = 503 And bLogBama Then
                        myReport.babelLog = oBabelLog
                    End If

                    'IncludeOCR !!!!!
                    '23.01.2020 - Added iReportNumber = 1
                    If iReportNumber = 503 Or iReportNumber = 504 Or iReportNumber = 3 Or iReportNumber = 1 Or iReportNumber = 1001 Then
                        If bClientFiles Then
                            myReport.ClientNumber = sClientNo
                        End If
                    End If

                    If bLogBama Then  ' 02.02.2017
                        oBabelLog.Heading = "BabelBank info: "
                        oBabelLog.AddLogEvent("BabelReport.RunReport: Satt verdier i rapport " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                    End If

                    '15.07.2016 - Added reportnumber 002 
                    '27.09.2017 - Added reportnumber 503
                    If iReportNumber = 1 Or iReportNumber = 2 Or iReportNumber = 3 Or iReportNumber = 503 Then
                        If bReportOnSelectedItems Then
                            myReport.ReportOnSelectedItems = True
                        Else
                            myReport.ReportOnSelectedItems = False
                        End If
                    End If

                    If iReportNumber = 912 Then
                        myReport.ReportOnSelectedItems = True
                    End If
                    'Set connectionstring to Babel's database, even needed when we dont't report from DB to be able to add fields.

                    'bThisIsVisma = False  removed 09.02.2018 - is set for each report
                    If bLogBama Then  ' 05.10.2017
                        oBabelLog.Heading = "BabelBank info: "
                        oBabelLog.AddLogEvent("BabelReport.RunReport: Rett f�r IsThisVismaIntegrator " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                    End If
                    'If IsThisVismaIntegrator() Then  ' removed 09.02.2018 - is set for each report
                    If bThisIsVisma = True Then
                        If bLogBama Then  ' 05.10.2017
                            oBabelLog.Heading = "BabelBank info: "
                            oBabelLog.AddLogEvent("BabelReport.RunReport: Etter IsThisVismaIntegrator " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                        End If

                        'bThisIsVisma = True  removed 09.02.2018
                        If EmptyString(sVISMA_ConnectionString) Then
                            Throw New Exception(LRSCommon(30052, iReportNumber.ToString))
                        Else
                            VismaConnection.ConnectionString = sVISMA_ConnectionString
                            VismaConnection.Open()
                            'TODO VISMAPRO - We should use some parameter, it is not necessary BabelBank_vbsys
                            'VismaCommand.CommandText = "Select * FROM BabelBank_vbsys.dbo.bbClient"
                            VismaCommand.CommandText = "Select * FROM " & sVISMA_BBDB & ".dbo.bbClient"
                            VismaCommand.Connection = VismaConnection
                            'System.Data.Odbc.OdbcDataAdapter adapter = new System.Data.Odbc.OdbcDataAdapter(command);
                            VismaAdapter.SelectCommand = VismaCommand
                            VismaData.TableName = "bbClient"
                            VismaAdapter.Fill(VismaData)

                            'Dim Connection As New System.Data.Odbc.OdbcConnection(sVISMA_ConnectionString)
                            'Connection.Open()
                            'Dim Command As New System.Data.Odbc.OdbcCommand("Select FirstDate FROM BabelBank_vbsys.dbo.bbLicense", Connection)
                            ''System.Data.Odbc.OdbcDataAdapter adapter = new System.Data.Odbc.OdbcDataAdapter(command);
                            'Dim adapter = New System.Data.Odbc.OdbcDataAdapter(Command)
                            'Dim data = New System.Data.DataTable("bbLicense")
                            'adapter.Fill(data)
                            'bThisIsVisma = False
                        End If
                    Else
                        If bLogBama Then  ' 05.10.2017
                            oBabelLog.Heading = "BabelBank info: "
                            oBabelLog.AddLogEvent("BabelReport.RunReport: Etter IsThisVismaIntegrator " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                        End If

                        sConnectionString = FindBBConnectionString()
                        rptDataSource.ConnectionString = sConnectionString
                    End If
                    'Old code
                    'rptDataSource.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath() & ";Persist Security Info=False"

                    If bReportOnDatabase Then
                        rptDataSource.SQL = sMySQL
                        myReport.DataSource = rptDataSource
                        myReport.SQLServer = bSQLServer
                    Else
                        'TODO VISMAPRO - May have to have a seperate SQL for Visma
                        rptDataSource.SQL = "SELECT * FROM Company"
                        If bThisIsVisma Then
                            'myReport.DataSource = rptVISMADataSource
                            myReport.DataSource = VismaData
                        Else
                            myReport.DataSource = rptDataSource
                        End If

                        'JRO.JetEngine.RefreshCache()
                        'Application.DBEngine.Idle(dbRefreshCache)

                        '08.06.2015 - Added Report 11
                        '14.07.2016 - Added Report 992
                        '15.07.2016 - Added Report 002, 1002 and 1010
                        '22.08.2019 - Added 950
                        If iReportNumber = 1 Or iReportNumber = 2 Or iReportNumber = 3 Or iReportNumber = 4 Or iReportNumber = 11 Or iReportNumber = 503 Or iReportNumber = 504 Or iReportNumber = 508 Or iReportNumber = 912 Or iReportNumber = 950 Or iReportNumber = 990 Or iReportNumber = 992 Or iReportNumber = 1001 Or iReportNumber = 1002 Or iReportNumber = 1010 Then
                            myReport.clientnumber = sClientNo
                            myReport.BabelFiles = oBabelFiles
                            myReport.SQLServer = bSQLServer
                            If bIncludeOCR Then
                                myReport.IncludeOCR = True
                            Else
                                myReport.IncludeOCR = False
                            End If
                            If bLogBama Then  ' 02.02.2017
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: Populert myReport for " & iReportNumber.ToString, 8) 'vbLogEventTypeInformationDetail
                            End If

                        ElseIf iReportNumber = 12 Then '27.04.2017
                            'myReport.BabelFiles = oBabelFiles

                        Else
                            bRunThisReport = False
                        End If
                    End If

                    If bRunThisReport Then

                        If bLogBama Then  ' 02.02.2017
                            oBabelLog.Heading = "BabelBank info: "
                            oBabelLog.AddLogEvent("BabelReport.RunReport: F�r myReport.Run", 8) 'vbLogEventTypeInformationDetail
                        End If

                        ' 21.04.2020 - hvis vi har problemer med out of memory p� rapporter, kan vi bruke file caching
                        'If iReportNumber = 504 Then
                        'myReport.Document.CacheToDisk = True
                        'End If

                        myReport.Run()

                        If bLogBama Then  ' 02.02.2017
                            oBabelLog.Heading = "BabelBank info: "
                            oBabelLog.AddLogEvent("BabelReport.RunReport: Etter myReport.Run", 8) 'vbLogEventTypeInformationDetail
                        End If

                        If myReport.EmptyReport Then
                            bEmptyReport = True
                            If bLogBama Then  ' 02.02.2017
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: Tom rapport", 8) 'vbLogEventTypeInformationDetail
                            End If
                        Else
                            If iReportNumber = 505 Then
                                If bLog Then
                                    oBabelLog.Heading = "BabelBank error: "
                                    If Len(sExportType) > 0 And Not EmptyString(sExportFilename) Then
                                        sTemp = sExportFilename
                                    Else
                                        sTemp = ""
                                    End If
                                    oBabelLog.AddLogEvent("BabelReport.RunReport: " & LRS(13010, sTemp), 1) 'EVENTLOG_ERROR_TYPE
                                End If
                                If Not bBBIsInSilentMode Then
                                    MsgBox(Replace(LRS(13010), "%1", "") & vbCrLf & LRS(10015), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(13011))
                                Else

                                End If
                                bReturnValue = False
                            End If
                        End If

                        If bReportOnDatabase Then
                            If bSavebeforeReport Then

                                '21.01.2020 - Added this IF to be able to report on clients if the data should be saved
                                If Not bClientFiles Then
                                    'Remove reportdata from the DB
                                    oBabelFiles.RemoveReportData()
                                End If

                            End If
                        End If

                        '**********************************************************
                        '*************** SHOW REPORT ON SCREEN ********************
                        '**********************************************************
                        If bPreview And Not bBBIsInSilentMode And (Not bClientFiles Or (bClientFiles And bAccountFound)) And Not bEmptyReport Then
                            '----------------------------
                            ' PREVIEW - Send to arView
                            '----------------------------
                            If bLogBama Then  ' 02.02.2017
                                oBabelLog.Heading = "BabelBank info: "
                                oBabelLog.AddLogEvent("BabelReport.RunReport: Skal vise rapport p� skjerm.", 8) 'vbLogEventTypeInformationDetail
                            End If

                            arView = New vbBabel.arBBView

                            arView.SetDocument(myReport)
                            If iReportNumber = 950 And sSpecial = "DNBNORFINANS_CREMUL" Then
                                arView.SetPageOrientation_ToLandscape()
                            End If

                            If oOwnerForm Is Nothing Then
                                arView.ShowDialog() 'Modal
                            Else
                                arView.ShowDialog(oOwnerForm)  'Modal
                            End If

                            arView = Nothing

                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                        End If

                        '**********************************************************
                        '************ GENERATE REPORT TO PRINTER ******************
                        '**********************************************************
                        ' 28.08.2015 
                        ' New way of sending report to printer
                        ' Now uses subs in each report to do this

                        'If bPrinter And (Not bClientFiles Or (bClientFiles And bAccountFound)) And Not bEmptyReport Then
                        '    '----------------
                        '    ' Send to printer
                        '    '----------------
                        '    If bLog Then
                        '        oBabelLog.Heading = "BabelBank info: "
                        '        oBabelLog.AddLogEvent("BabelReport.RunReport: F�r start av rapport-print. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
                        '    End If

                        '    If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then

                        '        myReport.Document.Printer.Printername = sPrinterName

                        '    End If


                        '    '28.08.2015 - Changed next line
                        '    myReport.Document.Printer.Print()
                        '    'myReport.Document.Print()

                        '    If bLog Then
                        '        oBabelLog.Heading = "BabelBank info: "
                        '        oBabelLog.AddLogEvent("BabelReport.RunReport: Etter report-print. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
                        '    End If

                        'End If

                        '**********************************************************
                        '*********** GENERATE FILENAME FOR EXPORT *****************
                        '**********************************************************
                        '13.03.06 added test for emptystring( Generated error when no filename was specified

                        If bEmail Then
                            iCounter = 0
                            iPos2 = 1
                            If Not Array_IsEmpty(aEMailAddresses) Then
                                ' do nothing - aEMailAddresses is filled from "outside"
                            ElseIf Not EmptyString(sEMailAddresses) Then
                                Do While True
                                    iPos = InStr(Mid(sEMailAddresses, iPos2), ";", CompareMethod.Text)
                                    ReDim Preserve aEMailAddresses(iCounter)
                                    If iPos > 0 Then
                                        aEMailAddresses(iCounter) = Trim(Mid(sEMailAddresses, iPos2, iPos - 1))
                                        iPos2 = iPos + 1
                                        iCounter = iCounter + 1
                                    Else
                                        aEMailAddresses(iCounter) = Trim(Mid(sEMailAddresses, iPos2))
                                        Exit Do
                                    End If
                                Loop
                            Else
                                'Try to retrieve emailaddresses
                                sMySQL = "SELECT N.EMail_Adress FROM Notification N, ClientNotifications CN WHERE CN.Notification_ID = N.Notification_ID AND CN.Client_ID = " & iMyClientID.ToString & " AND CN.Company_ID = " & iMyCompanyID.ToString

                                oMyDal.SQL = sMySQL
                                iCounter = -1
                                If oMyDal.Reader_Execute() Then
                                    Do While oMyDal.Reader_ReadRecord
                                        iCounter = iCounter + 1
                                        ReDim Preserve aEMailAddresses(iCounter)
                                        aEMailAddresses(iCounter) = oMyDal.Reader_GetString("EMail_Adress")
                                    Loop
                                Else
                                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                End If
                            End If

                        End If

                        If (Len(sExportType) > 0 And Not EmptyString(sExportFilename) Or (bEmail And Not Array_IsEmpty(aEMailAddresses))) And Not bEmptyReport Then
                            If Len(sExportType) > 0 Then
                                '-------------------
                                ' Send to exportfile
                                '-------------------

                                ' Allow timestamp in exportfilename
                                ' Starts and ends with %
                                ' Use
                                ' Y Full Year (2002)
                                ' y Short Year, last two digits of year (02)
                                ' M for Months
                                ' D for Days
                                ' h for Hours
                                ' m for minutes
                                ' s for seconds
                                If Len(sExportFilename) - Len(Replace(sExportFilename, "%", "")) > 1 Then
                                    ' At least two % in filename, asume datetimestamp
                                    sFilenameOut = ReplaceDateTimeStamp((sExportFilename), True)
                                Else
                                    sFilenameOut = sExportFilename
                                End If

                                If bClientFiles And bAccountFound Then
                                    ' Find filename based on clientno/name
                                    If Len(sFilenameOut) - Len(Replace(sFilenameOut, "�", "")) > 0 Then
                                        ' replace $ with clientno
                                        sFilenameOut = Replace(sFilenameOut, "�", sClientNo, , 1)
                                        sFilenameOut = Replace(sFilenameOut, "�", "")
                                    Else
                                        sFilenameOut = Replace(sFilenameOut, "�", oClient.Name.Trim) '24.03.2017 - Added trim
                                    End If
                                End If

                                If bLogBama Then  ' 02.02.2017
                                    oBabelLog.Heading = "BabelBank info: "
                                    oBabelLog.AddLogEvent("BabelReport.RunReport: Lag fil:" & sFilenameOut, 8) 'vbLogEventTypeInformationDetail
                                End If

                                ' First, test if we are able to write to specified exportfile;
                                ' added 01.06.2007
                                ' For special report 950, there may be several files to export, and
                                ' only the filefolder is given, then test on that one;
                                ' 22.06.2009 also added report 951
                                If iReportNumber = 950 Or iReportNumber = -950 Or iReportNumber = 951 Or iReportNumber = -951 Then
                                    ' no test
                                Else
                                    ' 19.01.2010 - If still have $ or � as part of exportfilename, then we have not
                                    ' been able to replace then due to empty reports (have had no accountno to find client)
                                    ' Then, don't bother to test filenameout
                                    If InStr(sFilenameOut, "�") > 0 Or InStr(sFilenameOut, "�") > 0 Then
                                        ' no test!
                                    Else
                                        iErr = CheckFolder(Left(sFilenameOut, InStrRev(sFilenameOut, "\")), vb_Utils.ErrorMessageType.Use_ERR_Raise, True, LRS(13009), , bBBIsInSilentMode)
                                    End If
                                End If
                                If iErr > 1 Then
                                    Err.Raise(13006, LRS(13006, sFilenameOut) & " (" & Trim$(Str(iErr)) & ")", sErrMessage) ' Unable to export to file %1
                                End If

                            End If

                            ' new 09.03.06
                            If Not bClientFiles Or (bClientFiles And bAccountFound) Then

                                If bLog Then
                                    oBabelLog.Heading = "BabelBank info: "
                                    oBabelLog.AddLogEvent("BabelReport.RunReport: F�r start av rapport til PDF, RTF, etc. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
                                End If

                            End If

                            '**********************************************************
                            '******* CREATE EXPORTREPORTS (ALSO FOR EMAIL) ************
                            '**********************************************************
                            '13.03.06 added test for emptystring( Generated error when no filename was specified
                            If Len(sExportType) > 0 And Not EmptyString((sExportFilename)) Or (bEmail And Not Array_IsEmpty(aEMailAddresses)) Then

                                ' Added if 09.08.04

                                Select Case sExportType
                                    Case "PDF"
                                        ' (If E-mail, then save as PDF, and send mail)

                                        'PDFExport.AcrobatVersion = ActiveReportsPDFExport.DDACRVersion.DDACR40
                                        'PDFExport.SemiDelimitedNeverEmbedFonts = ""

                                        If pdfExport Is Nothing Then
                                            pdfExport = New DataDynamics.ActiveReports.Export.Pdf.PdfExport
                                        End If

                                        If sExportType = "EMAIL" Then
                                            ' save to temp-file, in same path as database
                                            pdfExport.Export(myReport.Document, sTmpPDF)

                                        Else
                                            pdfExport.Export(myReport.Document, sFilenameOut)

                                        End If
                                        ' 19.05.2014 - problems with locked exportfile, 
                                        ' then crashes on next .pdf.export
                                        myReport.document.dispose()
                                        myReport.dispose()
                                        myReport = Nothing
                                        pdfExport = Nothing

                                        If bLogBama Then  ' 02.02.2017
                                            oBabelLog.Heading = "BabelBank info: "
                                            oBabelLog.AddLogEvent("BabelReport.RunReport: Skrevet PDF-rapport.", 8) 'vbLogEventTypeInformationDetail
                                        End If

                                    Case "RTF"
                                        If rtfExport Is Nothing Then
                                            rtfExport = New DataDynamics.ActiveReports.Export.Rtf.RtfExport
                                        End If

                                        rtfExport.Export(myReport.Document, sFilenameOut)
                                        rtfExport = Nothing

                                    Case "TXT"
                                        If txtExport Is Nothing Then
                                            txtExport = New DataDynamics.ActiveReports.Export.Text.TextExport
                                        End If

                                        txtExport.PageDelimiter = vbCrLf & vbCrLf & vbCrLf
                                        'txtExport.Unicode = True
                                        txtExport.SuppressEmptyLines = True
                                        txtExport.Export(myReport.Document, sFilenameOut)
                                        txtExport = Nothing

                                    Case "HTML"
                                        If htmlExport Is Nothing Then
                                            htmlExport = New DataDynamics.ActiveReports.Export.Html.HtmlExport
                                        End If

                                        htmlExport.IncludePageMargins = True
                                        htmlExport.MultiPage = True
                                        htmlExport.Export(myReport.Document, sFilenameOut)

                                        htmlExport = Nothing

                                    Case "TIFF"
                                        If tiffExport Is Nothing Then
                                            tiffExport = New DataDynamics.ActiveReports.Export.Tiff.TiffExport
                                        End If

                                        tiffExport.Export(myReport.Document, sFilenameOut)
                                        tiffExport = Nothing

                                    Case "XLS"
                                        If xlsExport Is Nothing Then
                                            xlsExport = New DataDynamics.ActiveReports.Export.Xls.XlsExport
                                        End If

                                        xlsExport.Export(myReport.Document, sFilenameOut)
                                        xlsExport = Nothing

                                End Select

                                If bEmail Then
                                    If EmptyString(sFilenameOut) Then
                                        If pdfExport Is Nothing Then
                                            pdfExport = New DataDynamics.ActiveReports.Export.Pdf.PdfExport()
                                        End If

                                        pdfExport.Export(myReport.Document, sTmpPDF)
                                        pdfExport = Nothing

                                    Else
                                        ' use same file as exported
                                        sTmpPDF = sFilenameOut ' May be other than PDF-format
                                    End If

                                    ' mail report to client
                                    ' Mailadresses are set in property EMailAdresses
                                    If Not oBabelFiles.VB_Profile Is Nothing Then
                                        ReportSendMail()
                                        If bLog Then
                                            oBabelLog.Heading = "BabelBank info: "
                                            oBabelLog.AddLogEvent("BabelReport.ReportSendMail: Successfully sent mail, reportnumber " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
                                        End If

                                        ' 17.07.2018
                                        ' Remove eMails for the current client, otherwise they will stay for the next client;
                                        aEMailAddresses = Nothing
                                        ' release tmpFile;
                                        ' Do not delete, this may be report we like to archive
                                        'If Len(Dir(sTmpPDF)) > 0 Then
                                        '    Kill(sTmpPDF)
                                        'End If
                                    End If
                                End If

                                If bLog Then
                                    oBabelLog.Heading = "BabelBank info: "
                                    oBabelLog.AddLogEvent("BabelReport.RunReport: Etter rapport til PDF, RTF, etc. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
                                End If

                            End If ' ******* END CREATE EXPORTREPORTS (ALSO FOR EMAIL) ************


                        End If ' *********** END GENERATE FILENAME FOR EXPORT *****************

                    End If

                    If bLogBama Then  ' 02.02.2017
                        oBabelLog.Heading = "BabelBank info: "
                        oBabelLog.AddLogEvent("BabelReport.RunReport: Ferdig med rapport", 8) 'vbLogEventTypeInformationDetail
                    End If

                    ' release memory
                    rptDataSource = Nothing
                    If Not myReport Is Nothing Then
                        myReport.document.dispose()
                        myReport.dispose()
                        myReport = Nothing
                    End If

                    If bThisIsVisma Then
                        If Not VismaData Is Nothing Then
                            VismaData.Dispose()
                            VismaData = Nothing
                        End If
                        If Not VismaAdapter Is Nothing Then
                            VismaAdapter.Dispose()
                            VismaAdapter = Nothing
                        End If
                        If Not VismaCommand Is Nothing Then
                            VismaCommand.Dispose()
                            VismaCommand = Nothing
                        End If
                        If Not VismaConnection Is Nothing Then
                            VismaConnection.Dispose()
                            VismaConnection = Nothing
                        End If
                    End If

                    'oBabelFile = Nothing
                    'oBatch = Nothing
                    'oPayment = Nothing
                    'oClient = Nothing
                    'oaccount = Nothing


                    ' Added 09.03.06
                    If Not bClientFiles Or bAccountFound = False Then
                        Exit Do
                    End If

                End If 'If oReport.ReportNo > 0 Then

            Loop

            If bClientFiles Then
                ' reset oClient.tmpFlag when using Client separated reports
                For Each oClient In oBabelFiles.VB_Profile.FileSetups(oBabelFiles(1).VB_FileSetupID).Clients
                    oClient.tmpFlag = 0
                Next oClient
            End If ' bClientFiles Then


            '	' Need to release report from frmViewer, to prepare for next report
            '	frmViewer = Nothing
        End If ' if bRunThisReport then


        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("BabelReport.RunReport: Siste linje i RunReport. Rapport " & iReportNumber, 8) 'vbLogEventTypeInformationDetail
        End If

        RunReport = bReturnValue

        'Catch ex As Exception

        '    '    'OLD CODE - Throw New Exception("Function: RunReport" & vbNewLine & LRS(40026) & ": " & iReportNumber.ToString & vbCrLf & ex.Message)

        '    Throw New vbBabel.Payment.PaymentException("Function: RunReport" & vbNewLine & LRS(40026) & ": " & iReportNumber.ToString & vbCrLf & ex.Message & vbCrLf & sMySQL, ex, Nothing, "", "")

        'End Try

    End Function

    Private Function ReportSendMail() As Boolean
        Dim myMail As vbBabel.vbMail
        Dim sCompany As String ' Companyname, who needs support ?
        Dim sMsgNote As String
        Dim sErrDescription As String
        Dim i As Integer

        On Error GoTo errMail

        myMail = New vbBabel.vbMail

        If oBabelFiles.VB_Profile.EmailSMTP Then

            myMail.EMail_SMTP = True
            If Len(Trim(oBabelFiles.VB_Profile.EmailSMTPHost)) > 0 Then
                myMail.EMail_SMTP = True
                myMail.eMailSMTPHost = oBabelFiles.VB_Profile.EmailSMTPHost
            End If
        Else
            myMail.EMail_SMTP = False  ' MAPI
        End If

        ' Use new class/dll vbSendmail
        '-----------------------------
        myMail.eMailSender = oBabelFiles.VB_Profile.EmailSender
        myMail.eMailSenderDisplayName = oBabelFiles.VB_Profile.EmailDisplayName
        'myMail.Recipient = oProfile.EmailAutoReceiver
        'myMail.RecipientDisplayName = oProfile.EmailAutoDisplayName
        For i = 0 To UBound(aEMailAddresses)
            myMail.AddReceivers(aEMailAddresses(i))
        Next i

        'myMail.eMailReplyTo = oProfile.EmailReplyAdress
        If sSpecial = "GULFMARK" Then
            ' 07.11.2016 - Gulfmark will not have time as part of subject, and always date as DD/MM/YYYY
            ' make date DD/MM/YYYY
            myMail.Subject = "BabelBank" & " " & sHeading & " - " & Format(Date.Today, "dd/MM/yyyy")
        Else
            myMail.Subject = "BabelBank" & " " & sHeading & " - " & Now
        End If
        myMail.AddAttachment(sTmpPDF)
        'myMail.AddAttachment("C:\Slett\Clean.txt")
        'myMail.AddAttachment("C:\Slett\SQL.Dat")

        myMail.Body = LRS(60200) '"Reports from BabelBank"

        myMail.Send()
        myMail = Nothing

        ReportSendMail = True

        Exit Function

errMail:
        myMail = Nothing
        sErrDescription = Err.Description
        Err.Raise(15011, , sErrDescription)

        ReportSendMail = False

        Exit Function

    End Function

    Private Function CalculateReport506() As Object

        'TODO - This function must be changed - 
        'Regarding dimensioning the arrays as Variant
        'Regarding the arrays keeping string- double, datedimensioned values 
        'Regarding the use of frmViewer

        '      Dim sDate As String
        'Dim sAccount As String
        'Dim sFilename As String
        'Dim oBatch As Batch
        'Dim oPayment As Payment
        'Dim oInvoice As Invoice
        'Dim aDateArray(,) As Variant
        'Dim aAccountArray(,) As Variant
        'Dim aFileArray(,) As Variant
        'Dim j, i, k As Integer
        'Dim lCounter As Integer
        'Dim bDateFound, bAccountFound As Boolean


        ''sDate = oBabelFiles(1).Batches(1).Payments(1).DATE_Value
        ''Added loops 13.06.06, because of problems when all payments are filtered out
        'For	Each oBabelFile In oBabelFiles
        '	For	Each oBatch In oBabelFile.Batches
        '		For	Each oPayment In oBatch.Payments
        '			sDate = oPayment.DATE_Value
        '			' added 13.11.2007 due to possible split on clients;
        '                  If Not EmptyString((frmViewer.sClientNo)) Then

        '                      If frmViewer.CorrectReportClient(oPayment) Then
        '                          sAccount = oPayment.I_Account
        '                          Exit For
        '                      End If
        '                  Else
        '                      sAccount = oPayment.I_Account
        '                  End If
        '		Next oPayment
        '		' added 13.11.2007
        '		If Not EmptyString((frmViewer.sClientNo)) Then
        '			If Not EmptyString(sAccount) Then
        '				Exit For
        '			End If
        '		End If
        '	Next oBatch
        '	' added 13.11.2007
        '	If Not EmptyString((frmViewer.sClientNo)) Then
        '		If Not EmptyString(sAccount) Then
        '			Exit For
        '		End If
        '	End If
        'Next oBabelFile


        '      ReDim aDateArray(9, 0)
        'aDateArray(0, 0) = sDate
        'aDateArray(1, 0) = 0
        'aDateArray(2, 0) = 0
        'aDateArray(3, 0) = 0
        'aDateArray(4, 0) = 0
        'aDateArray(5, 0) = 0
        'aDateArray(6, 0) = 0
        'aDateArray(7, 0) = 0
        'aDateArray(8, 0) = 0
        'aDateArray(9, 0) = "Date"


        '      ReDim aAccountArray(9, 0)
        'aAccountArray(0, 0) = sAccount
        'aAccountArray(1, 0) = 0
        'aAccountArray(2, 0) = 0
        'aAccountArray(3, 0) = 0
        'aAccountArray(4, 0) = 0
        'aAccountArray(5, 0) = 0
        'aAccountArray(6, 0) = 0
        'aAccountArray(7, 0) = 0
        'aAccountArray(8, 0) = 0
        'aAccountArray(9, 0) = "Account"

        'sFilename = oBabelFiles(1).FilenameIn
        '      ReDim aFileArray(9, 0)
        'aFileArray(0, 0) = sFilename
        'aFileArray(1, 0) = 0
        'aFileArray(2, 0) = 0
        'aFileArray(3, 0) = 0
        'aFileArray(4, 0) = 0
        'aFileArray(5, 0) = 0
        'aFileArray(6, 0) = 0
        'aFileArray(7, 0) = 0
        'aFileArray(8, 0) = 0
        'aFileArray(9, 0) = "File"

        'For	Each oBabelFile In oBabelFiles
        '	For	Each oBatch In oBabelFile.Batches
        '		For	Each oPayment In oBatch.Payments

        '			' added 12.11.2007
        '			' Check if it is a clientseparated report, and if so, correct client
        '			If frmViewer.CorrectReportClient(oPayment) Then

        '				If oPayment.DATE_Value <> sDate Then
        '					bDateFound = False
        '					For lCounter = 0 To UBound(aDateArray, 2)
        '						If aDateArray(0, lCounter) = oPayment.DATE_Value Then
        '							i = lCounter
        '							bDateFound = True
        '							sDate = aDateArray(0, i)
        '							Exit For
        '						End If
        '					Next lCounter
        '					If Not bDateFound Then
        '						'Add a new element
        '						ReDim Preserve aDateArray(9, UBound(aDateArray, 2) + 1)
        '						sDate = oPayment.DATE_Value
        '						i = UBound(aDateArray, 2)
        '						aDateArray(0, i) = sDate
        '						aDateArray(1, i) = 0
        '						aDateArray(2, i) = 0
        '						aDateArray(3, i) = 0
        '						aDateArray(4, i) = 0
        '						aDateArray(5, i) = 0
        '						aDateArray(6, i) = 0
        '						aDateArray(7, i) = 0
        '						aDateArray(8, i) = 0
        '						aDateArray(9, i) = "Date"
        '					End If
        '				End If

        '				If oPayment.I_Account <> sAccount Then
        '					bAccountFound = False
        '					' 13.11.2007 done several changes, from Date to Account. had a serious error here!
        '					For lCounter = 0 To UBound(aAccountArray, 2)
        '						If aAccountArray(0, lCounter) = oPayment.I_Account Then
        '							' 13.11.2007 changed from i to j
        '							'i = lCounter
        '							j = lCounter
        '							bAccountFound = True
        '							sAccount = aAccountArray(0, j) ' to j from i
        '							Exit For
        '						End If
        '					Next lCounter
        '					If Not bAccountFound Then
        '						'Add a new element
        '						ReDim Preserve aAccountArray(9, UBound(aAccountArray, 2) + 1)
        '						sAccount = oPayment.I_Account
        '						j = UBound(aAccountArray, 2)
        '						aAccountArray(0, j) = sAccount
        '						aAccountArray(1, j) = 0
        '						aAccountArray(2, j) = 0
        '						aAccountArray(3, j) = 0
        '						aAccountArray(4, j) = 0
        '						aAccountArray(5, j) = 0
        '						aAccountArray(6, j) = 0
        '						aAccountArray(7, j) = 0
        '						aAccountArray(8, j) = 0
        '						aAccountArray(9, j) = "Account"
        '					End If
        '				End If

        '				If oBabelFile.FilenameIn <> sFilename Then
        '					ReDim Preserve aFileArray(9, UBound(aFileArray, 2) + 1)
        '					sFilename = oBabelFile.FilenameIn
        '					k = UBound(aFileArray, 2)
        '					aFileArray(0, k) = sFilename
        '					aFileArray(1, k) = 0
        '					aFileArray(2, k) = 0
        '					aFileArray(3, k) = 0
        '					aFileArray(4, k) = 0
        '					aFileArray(5, k) = 0
        '					aFileArray(6, k) = 0
        '					aFileArray(7, k) = 0
        '					aFileArray(8, k) = 0
        '					aFileArray(9, k) = "File"
        '				End If

        '				For	Each oInvoice In oPayment.Invoices
        '					If oInvoice.MATCH_Final Then
        '						' add to array, dependant of type and matchstatus;

        '						'15.12.2008 - Changed to IsOCR
        '						If IsOCR((oPayment.PayCode)) Then
        '							'If oPayment.PayCode = 510 Then
        '							' OCR
        '							aDateArray(1, i) = aDateArray(1, i) + 1
        '							aDateArray(2, i) = aDateArray(2, i) + oInvoice.MON_InvoiceAmount / 100
        '							aAccountArray(1, j) = aAccountArray(1, j) + 1
        '							aAccountArray(2, j) = aAccountArray(2, j) + oInvoice.MON_InvoiceAmount / 100
        '							aFileArray(1, k) = aFileArray(1, k) + 1
        '							aFileArray(2, k) = aFileArray(2, k) + oInvoice.MON_InvoiceAmount / 100
        '						Else
        '							If Not oInvoice.MATCH_Matched Then
        '								' Unmatched
        '								aDateArray(3, i) = aDateArray(3, i) + 1
        '								aDateArray(4, i) = aDateArray(4, i) + oInvoice.MON_InvoiceAmount / 100
        '								aAccountArray(3, j) = aAccountArray(3, j) + 1
        '								aAccountArray(4, j) = aAccountArray(4, j) + oInvoice.MON_InvoiceAmount / 100
        '								aFileArray(3, k) = aFileArray(3, k) + 1
        '								aFileArray(4, k) = aFileArray(4, k) + oInvoice.MON_InvoiceAmount / 100
        '							Else
        '								' Matched
        '								aDateArray(5, i) = aDateArray(5, i) + 1
        '								aDateArray(6, i) = aDateArray(6, i) + oInvoice.MON_InvoiceAmount / 100
        '								aAccountArray(5, j) = aAccountArray(5, j) + 1
        '								aAccountArray(6, j) = aAccountArray(6, j) + oInvoice.MON_InvoiceAmount / 100
        '								aFileArray(5, k) = aFileArray(5, k) + 1
        '								aFileArray(6, k) = aFileArray(6, k) + oInvoice.MON_InvoiceAmount / 100
        '							End If
        '						End If
        '						' Total
        '						aDateArray(7, i) = aDateArray(7, i) + 1
        '						aDateArray(8, i) = aDateArray(8, i) + oInvoice.MON_InvoiceAmount / 100
        '						aAccountArray(7, j) = aAccountArray(7, j) + 1
        '						aAccountArray(8, j) = aAccountArray(8, j) + oInvoice.MON_InvoiceAmount / 100
        '						aFileArray(7, k) = aFileArray(7, k) + 1
        '						aFileArray(8, k) = aFileArray(8, k) + oInvoice.MON_InvoiceAmount / 100
        '					End If
        '				Next oInvoice

        '			Else
        '				i = i
        '			End If 'If frmViewer.CorrectReportClient(oPayment) Then

        '		Next oPayment
        '	Next oBatch
        'Next oBabelFile
        '' Finished calculating dates, accounts and files

        '' Merge the three different arrays into aReportArray
        '' Also, make room for Headinglines and blank lines

        ''Old code
        ''ReDim aReportArray(9, UBound(aDateArray, 2) + 1 + UBound(aDateArray, 2) + 3 + UBound(aFileArray, 2) + 3)
        ''New code 22112005
        'Dim aReportArray(9, UBound(aDateArray, 2) + 1 + UBound(aAccountArray, 2) + 3 + UBound(aFileArray, 2) + 3) As Object

        'aReportArray(0, 0) = "Dato"
        'For i = 0 To UBound(aDateArray, 2)
        '	' Format date;
        '	aDateArray(0, i) = Right(aDateArray(0, i), 2) & "." & Mid(aDateArray(0, i), 5, 2) & "." & Left(aDateArray(0, i), 4)
        '	For j = 0 To 9
        '		aReportArray(j, i + 1) = aDateArray(j, i)
        '	Next j
        'Next i
        '' Add one blank element between date and account to make it pretty!
        'i = UBound(aDateArray, 2) + 2
        'For j = 0 To 9
        '	aReportArray(j, i) = ""
        'Next j
        '' Add a headingline
        'aReportArray(0, i + 1) = "Konto"

        'For i = 0 To UBound(aAccountArray, 2)
        '	For j = 0 To 9
        '		aReportArray(j, i + 1 + UBound(aDateArray, 2) + 3) = aAccountArray(j, i)
        '	Next j
        'Next i
        '' Add one blank element between account and file to make it pretty!
        'i = 2 + UBound(aDateArray, 2) + 2 + UBound(aAccountArray, 2) + 1
        'For j = 0 To 9
        '	aReportArray(j, i) = ""
        'Next j
        '' Add a headingline
        'aReportArray(0, i + 1) = "Filnavn"

        'For i = 0 To UBound(aFileArray, 2)
        '	' Test length of filename
        '	If Len(aFileArray(0, i)) > 18 Then
        '		aFileArray(0, i) = "..." & Right(aFileArray(0, i), 17)
        '	End If

        '	'if i > 0 then 'New file, add a

        '	For j = 0 To 9
        '		aReportArray(j, i + 1 + UBound(aDateArray, 2) + 3 + UBound(aAccountArray, 2) + 3) = aFileArray(j, i)
        '	Next j
        'Next i


    End Function

    'Private Function CalculateReport510() As Object
    '	' Fill reportarray for 510_DayTotals
    '	' Fetch data from table DayTotals

    '	Dim cnDayTotals As New ADODB.Connection
    '	Dim sMySQL As String
    '	Dim sMyFile As String
    '	Dim rsDayTotals As ADODB.Recordset
    '	'Dim aReportArray() As String
    '	Dim oDatabase As Object 'vbbabel.Database
    '	Dim sCompanyID As String
    '	Dim i As Short

    '	oDatabase = New vbbabel.Database

    '	sCompanyID = "1"
    '       ReDim aReportArray(11, 0)

    '	sMyFile = BB_DatabasePath
    '	cnDayTotals.ConnectionString = oDatabase.ConnectToProfile(sMyFile)

    '	cnDayTotals.Open()

    '	rsDayTotals = New ADODB.Recordset
    '	' Fill a recordset with this sole record from DayTotals
    '	'sMySQL = "SELECT D.Company_ID, D.ProdDate, D.Imported, D.OCR, D.Matched, D.Matched_GL, "
    '	sMySQL = "SELECT D.*, "
    '	sMySQL = sMySQL & "C.Name, C.ClientNo FROM DayTotals D, Client C WHERE D.Company_ID=" & sCompanyID
    '	sMySQL = sMySQL & " AND C.Client_ID = D.Client_ID AND C.Company_ID = D.Company_ID ORDER BY ProdDate ASC"
    '	rsDayTotals.Open(sMySQL, cnDayTotals, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockOptimistic)

    '	'rsDayTotals.MoveLast  '13.06.06 moved further down

    '	' 14.03.06
    '	' Added Client_ID as part of the array and report, to be able to present
    '	' data for each client (ref Lindorff)
    '	' Must then be prepared for more than one record, and has therefore added a Do Loop
    '	i = -1
    '	'If rsDayTotals.BBrecordCount > 0 Then
    '	If BB_RecordCount(rsDayTotals, False) > 0 Then
    '		'Changed 19.10.2006 from rsDayTotals.MoveLast to rsDayTotals.MoveFirst
    '		'rsDayTotals.MoveLast
    '		rsDayTotals.MoveFirst()
    '		Do While Not rsDayTotals.eof
    '			i = i + 1
    '			ReDim Preserve aReportArray(11, i)
    '			aReportArray(0, i) = rsDayTotals.Fields("ProdDate").Value
    '			If Not IsDbNull(rsDayTotals.Fields("IB").Value) Then
    '				aReportArray(1, i) = rsDayTotals.Fields("IB").Value / 100
    '			Else
    '				aReportArray(1, i) = 0
    '			End If
    '			If Not IsDbNull(rsDayTotals.Fields("Imported").Value) Then
    '				aReportArray(2, i) = rsDayTotals.Fields("Imported").Value / 100
    '			Else
    '				aReportArray(2, i) = 0
    '			End If
    '			aReportArray(3, i) = aReportArray(1, i) + aReportArray(2, i)
    '			If Not IsDbNull(rsDayTotals.Fields("OCR").Value) Then
    '				aReportArray(4, i) = rsDayTotals.Fields("OCR").Value / 100
    '			Else
    '				aReportArray(4, i) = 0
    '			End If
    '			If Not IsDbNull(rsDayTotals.Fields("Matched_BB").Value) Then
    '				aReportArray(5, i) = rsDayTotals.Fields("Matched_BB").Value / 100
    '			Else
    '				aReportArray(5, i) = 0
    '			End If
    '			If Not IsDbNull(rsDayTotals.Fields("Matched_GL").Value) Then
    '				aReportArray(6, i) = rsDayTotals.Fields("Matched_GL").Value / 100
    '			Else
    '				aReportArray(6, i) = 0
    '			End If
    '			aReportArray(7, i) = aReportArray(1, i) + aReportArray(2, i) - aReportArray(4, i) - aReportArray(5, i) - aReportArray(6, i)

    '			' New 14.03.06
    '			aReportArray(8, i) = rsDayTotals.Fields("ClientNo").Value & " " & rsDayTotals.Fields("Name").Value
    '			'New 02.01.2007
    '			If Not IsDbNull(rsDayTotals.Fields("AccountNo").Value) Then
    '				aReportArray(9, i) = rsDayTotals.Fields("AccountNo").Value
    '			Else
    '				aReportArray(9, i) = ""
    '			End If
    '			If Not IsDbNull(rsDayTotals.Fields("UnMatched_BB").Value) Then
    '				aReportArray(10, i) = rsDayTotals.Fields("UnMatched_BB").Value / 100
    '			Else
    '				aReportArray(10, i) = 0
    '			End If
    '			aReportArray(11, i) = aReportArray(7, i) - aReportArray(10, i) ' Difference

    '			rsDayTotals.MoveNext()
    '		Loop 
    '	End If
    '	rsDayTotals.Close()
    '	cnDayTotals.Close()

    '	oDatabase = Nothing

    'End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
