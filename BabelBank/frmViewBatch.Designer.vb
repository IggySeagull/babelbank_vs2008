﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewBatch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gridBatch = New System.Windows.Forms.DataGridView
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.cmdReport = New System.Windows.Forms.Button
        Me.cmdSearch = New System.Windows.Forms.Button
        Me.cmdShow = New System.Windows.Forms.Button
        Me.OK_Button = New System.Windows.Forms.Button
        CType(Me.gridBatch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridBatch
        '
        Me.gridBatch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gridBatch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridBatch.Location = New System.Drawing.Point(13, 60)
        Me.gridBatch.Name = "gridBatch"
        Me.gridBatch.Size = New System.Drawing.Size(969, 288)
        Me.gridBatch.TabIndex = 0
        '
        'cmdPrint
        '
        Me.cmdPrint.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdPrint.Location = New System.Drawing.Point(842, 375)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(67, 23)
        Me.cmdPrint.TabIndex = 4
        Me.cmdPrint.Text = "55003-Print"
        '
        'cmdReport
        '
        Me.cmdReport.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdReport.Location = New System.Drawing.Point(769, 375)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(67, 23)
        Me.cmdReport.TabIndex = 5
        Me.cmdReport.Text = "40026 Rapport"
        '
        'cmdSearch
        '
        Me.cmdSearch.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdSearch.Location = New System.Drawing.Point(696, 375)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(67, 23)
        Me.cmdSearch.TabIndex = 12
        Me.cmdSearch.Text = "55031-Søk"
        '
        'cmdShow
        '
        Me.cmdShow.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdShow.Location = New System.Drawing.Point(623, 375)
        Me.cmdShow.Name = "cmdShow"
        Me.cmdShow.Size = New System.Drawing.Size(67, 23)
        Me.cmdShow.TabIndex = 13
        Me.cmdShow.Text = "55030-Vis"
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(915, 375)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 14
        Me.OK_Button.Text = "55032-Lukk"
        '
        'frmViewBatch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(997, 413)
        Me.Controls.Add(Me.OK_Button)
        Me.Controls.Add(Me.cmdShow)
        Me.Controls.Add(Me.cmdSearch)
        Me.Controls.Add(Me.cmdReport)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.gridBatch)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewBatch"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Batch"
        CType(Me.gridBatch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridBatch As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents cmdShow As System.Windows.Forms.Button
    Friend WithEvents OK_Button As System.Windows.Forms.Button

End Class
