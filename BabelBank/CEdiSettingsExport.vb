Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("CEdiSettingsExport_NET.CEdiSettingsExport")> Public Class CEdiSettingsExport
	
	Private mrCurrentElement As MSXML2.IXMLDOMElement
	Private mrLINElement As MSXML2.IXMLDOMElement
	Private mrSEQElement As MSXML2.IXMLDOMElement
	Private mrDOCElement As MSXML2.IXMLDOMElement
	Private mrPRCElement As MSXML2.IXMLDOMElement
	Private lLINCounter As Integer
	Private lSEQCounter As Integer
	Private lDOCCounter As Integer
	
	Private aGroupMaxRep(,) As String
	'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
	'DOTNETT REDIM Private aGroupMaxRep() As String
	Private iRepetitionCounter, iRepetitionsAllowed As Short
	
	Private mrXMLTagSettings As New MSXML2.DOMDocument40
	Private xmlMapDoc As MSXML2.DOMDocument40
	Private lSegmentCount As Integer
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property SegmentCount() As Integer
		Get
			SegmentCount = lSegmentCount
		End Get
		Set(ByVal Value As Integer)
			lSegmentCount = Value
		End Set
	End Property
	Public WriteOnly Property MapDoc() As MSXML2.DOMDocument40
		Set(ByVal Value As MSXML2.DOMDocument40)
			xmlMapDoc = Value
		End Set
	End Property
    Public Property CurrentElement() As MSXML2.IXMLDOMElement
        Get
            CurrentElement = mrCurrentElement
        End Get
        Set(ByVal Value As MSXML2.IXMLDOMElement)
            mrCurrentElement = Value
        End Set
    End Property
	Public ReadOnly Property XMLTagSettings() As MSXML2.DOMDocument40
		Get
			XMLTagSettings = mrXMLTagSettings
		End Get
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	Friend Function setLINElement() As Boolean
		mrCurrentElement = mrLINElement
		setLINElement = True
	End Function
	Friend Function setSEQElement() As Boolean
		mrCurrentElement = mrSEQElement.parentNode
		setSEQElement = True
	End Function
	Friend Function setDOCElement() As Boolean
		mrCurrentElement = mrDOCElement
		setDOCElement = True
	End Function
	Friend Function setPRCElement() As Boolean
		mrCurrentElement = mrPRCElement
		setPRCElement = True
	End Function
	Friend Function MoveNextElement() As Boolean
		Dim bReturnValue, bMoveOn As Boolean
		Dim bGoingUp As Boolean
		
		bMoveOn = True
		bGoingUp = False
		
		Do While bMoveOn
			bMoveOn = False
			'UPGRADE_WARNING: Couldn't resolve default property of object mrCurrentElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If iRepetitionCounter >= Val(mrCurrentElement.getAttribute("reps")) Or mrCurrentElement.nodeName = "GROUP" Then
				'Can't repeat mrCurrentelement
				If mrCurrentElement.nextSibling Is Nothing Then
					If InUse(mrCurrentElement) And mrCurrentElement.nodeName = "GROUP" Then
						If mrCurrentElement.hasChildNodes Then
							mrCurrentElement = mrCurrentElement.firstChild
							bGoingUp = False
							bMoveOn = True
							iRepetitionCounter = 0
						Else
							mrCurrentElement = mrCurrentElement.parentNode
							bGoingUp = True
							bMoveOn = True
							iRepetitionCounter = 0
						End If
					Else
						mrCurrentElement = mrCurrentElement.parentNode
						bGoingUp = True
						bMoveOn = True
						iRepetitionCounter = 0
					End If
					
				ElseIf mrCurrentElement.nodeName = "GROUP" Then 
					
					
					If InUse(mrCurrentElement) Then
						'FIX Check for group repetition
						'If no more repetition then, bytt if
						If bGoingUp Then
							If mrCurrentElement.nextSibling Is Nothing Then
								mrCurrentElement = mrCurrentElement.parentNode
								bGoingUp = True
								bMoveOn = True
								iRepetitionCounter = 0
							Else
								mrCurrentElement = mrCurrentElement.nextSibling
								bGoingUp = False
								bMoveOn = True
								iRepetitionCounter = 0
							End If
						Else
							mrCurrentElement = mrCurrentElement.firstChild
							bGoingUp = False
							bMoveOn = True
							iRepetitionCounter = 0 'iRepetitionCounter + 1
						End If
					Else
						If mrCurrentElement.nextSibling Is Nothing Then
							mrCurrentElement = mrCurrentElement.parentNode
							bGoingUp = True
							bMoveOn = True
							iRepetitionCounter = 0
						Else
							mrCurrentElement = mrCurrentElement.nextSibling
							bGoingUp = False
							bMoveOn = True
							iRepetitionCounter = 0
						End If
					End If
				Else
					mrCurrentElement = mrCurrentElement.nextSibling
					iRepetitionCounter = 0
					If mrCurrentElement.nodeName = "GROUP" Then
						If InUse(mrCurrentElement) Then
							mrCurrentElement = mrCurrentElement.firstChild
							bGoingUp = False
							bMoveOn = True
						Else
							bGoingUp = False
							bMoveOn = True
						End If
						
						
						
						
					Else
						If InUse(mrCurrentElement) Then
							bReturnValue = True
							iRepetitionCounter = iRepetitionCounter + 1
						Else
							bMoveOn = True
							bGoingUp = False
						End If
					End If
				End If
				
				
				
			Else
				'May repeat currentelement
				If mrCurrentElement.Attributes.length > 3 + iRepetitionCounter Then
					'Every segment has normally 4 attributes: reps, mandatory, inuse, string
					'If it has more, then we have more than 1 possible string, named string2
					'May still repeat currentelement. We have a mapping
					If iRepetitionCounter = 0 Then
						'This segment has not been written yet
						If InUse(mrCurrentElement) Then
							' We found a possible segment
							bReturnValue = True
							iRepetitionCounter = iRepetitionCounter + 1
						Else
							'This segment is not in use. Move on.
							If mrCurrentElement.nextSibling Is Nothing Then
								mrCurrentElement = mrCurrentElement.parentNode
								bGoingUp = True
								iRepetitionCounter = 0
								bMoveOn = True
							Else
								mrCurrentElement = mrCurrentElement.nextSibling
								bGoingUp = False
								iRepetitionCounter = 0
								bMoveOn = True
							End If
						End If
					Else
						
						
						
					End If
				Else
					'The segment is not repeated, and we have to move on
					' we do this by setting the repetitioncounter to maximum.
					'UPGRADE_WARNING: Couldn't resolve default property of object mrCurrentElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					iRepetitionCounter = Val(mrCurrentElement.getAttribute("reps"))
					bMoveOn = True
					bGoingUp = False
				End If
			End If
			
		Loop 
		'iRepetitionsAllowed = Val(mrCurrentElement.getAttribute("number"))
		'iRepetitionCounter = 0
		
		MoveNextElement = bReturnValue
		
	End Function
	Friend Function MoveFirstChild() As Boolean
		mrCurrentElement = mrCurrentElement.firstChild
		MoveFirstChild = True
	End Function
    Friend Sub SetCurrentElement(ByRef sXpathString As String)
        'UPGRADE_WARNING: Couldn't resolve default property of object sXpathString. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        mrCurrentElement = xmlMapDoc.documentElement.selectSingleNode(sXpathString)
    End Sub
	Friend Function InitiateSpecialSegments() As Boolean
		lLINCounter = 0
		lSEQCounter = 0
		lDOCCounter = 0
		mrLINElement = xmlMapDoc.documentElement.selectSingleNode("//GROUP/LIN")
		mrSEQElement = xmlMapDoc.documentElement.selectSingleNode("//GROUP/SEQ")
		mrDOCElement = xmlMapDoc.documentElement.selectSingleNode("//DOC")
		mrPRCElement = xmlMapDoc.documentElement.selectSingleNode("//PRC")
		
		'FIX:  Errorhandling
		InitiateSpecialSegments = True
		
	End Function
    Friend Function Init(ByRef sXMLTagSettingsFile As String) As Boolean ' As String, Optional sValues As String) As Boolean

        'FIX: Load specific data such as local values, currency codes document fragments here
        'bRet = LoadXMLValues(sValues)

        mrXMLTagSettings.load(sXMLTagSettingsFile)

        Init = True

    End Function
	Public Function FindNextPossibleSegment() As Boolean
		Dim bFoundNextPossibleSegment As Boolean
		
		bFoundNextPossibleSegment = False
		
		Do Until bFoundNextPossibleSegment
			'First check if we have any children that is in use
			If CurrentElementHasChildrenInUse() Then
				If Not isGroupCurrentElement Then
					bFoundNextPossibleSegment = True
				Else
					'Arrived at a group, just continue
				End If
			Else
				'No children in use, check for siblings
				If CurrentElementHasMoreSiblingsInUse Then
					'bFoundNextPossibleSegment = True
					If Not isGroupCurrentElement Then
						bFoundNextPossibleSegment = True
					Else
						'Arrived at a group, just continue
					End If
				Else
					'No siblings either, we got to have a parent
					CurrentElement = CurrentElement.parentNode
				End If
			End If
		Loop 
		
		
		FindNextPossibleSegment = True
		
	End Function
	Private Function CurrentElementHasChildrenInUse() As Boolean
		Dim l As Integer
		
		'This function is based on the theory that all segment and group nodes don't start
		' with "TG", as Taggroups do.
		On Error GoTo errorHandler
		
		If mrCurrentElement.hasChildNodes = False Then
			CurrentElementHasChildrenInUse = False
			Exit Function
		Else
			For l = 1 To 100
				If Left(mrCurrentElement.childNodes(l).nodeName, 2) <> "TG" Then
					'UPGRADE_WARNING: Couldn't resolve default property of object mrCurrentElement.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					If InUse(mrCurrentElement.childNodes(l)) Then
						mrCurrentElement = mrCurrentElement.childNodes(l)
						CurrentElementHasChildrenInUse = True
						Exit Function
					End If
				End If
				If mrCurrentElement.childNodes(l).nodeName = mrCurrentElement.lastChild.nodeName Then
					CurrentElementHasChildrenInUse = False
					Exit Function
				End If
			Next l
		End If
		
		'CurrentElementHasChildrenInUse = True
		Exit Function
		
errorHandler: 
		CurrentElementHasChildrenInUse = False
		
	End Function
	Private Function CurrentElementHasMoreSiblingsInUse() As Boolean
		Dim bReturnValue As Boolean
		
		bReturnValue = False
		Do Until mrCurrentElement.nextSibling Is Nothing
			mrCurrentElement = mrCurrentElement.nextSibling
			If InUse(mrCurrentElement) Then
				bReturnValue = True
				Exit Do
			End If
		Loop 
		
		CurrentElementHasMoreSiblingsInUse = bReturnValue
		
	End Function
	Private Function isGroupCurrentElement() As Boolean
		
		'All segmentnames are 3 positions long, whilst groups are 5 or more
		If Len(mrCurrentElement.nodeName) > 3 Then
			isGroupCurrentElement = True
		Else
			isGroupCurrentElement = False
		End If
		
	End Function
	Public Function setGroupMaxRep() As Boolean
		Dim nodlistGroupNodes As MSXML2.IXMLDOMNodeList
		Dim nodTempNode As MSXML2.IXMLDOMElement
		Dim i As Short
		'Dim iGroupNumber As Integer
		
		nodlistGroupNodes = xmlMapDoc.documentElement.selectNodes("//GROUP")
		
        Dim aGroupMaxRep(nodlistGroupNodes.length - 1, 1) As String '???? AS Object
		
		On Error GoTo errorHandler
		
		For i = 0 To nodlistGroupNodes.length - 1
			nodTempNode = nodlistGroupNodes(i)
			'iGroupNumber = Val(nodTempNode.getAttribute("number"))
			'UPGRADE_WARNING: Couldn't resolve default property of object nodTempNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(Val(), 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aGroupMaxRep(Val(nodTempNode.getAttribute("number")), 0) = nodTempNode.getAttribute("reps")
			If nodTempNode.hasChildNodes Then
				'UPGRADE_WARNING: Couldn't resolve default property of object nodTempNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(Val(), 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aGroupMaxRep(Val(nodTempNode.getAttribute("number")), 1) = nodTempNode.firstChild.nodeName
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object nodTempNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object aGroupMaxRep(Val(), 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aGroupMaxRep(Val(nodTempNode.getAttribute("number")), 1) = ""
			End If
		Next i
		
		setGroupMaxRep = True
		Exit Function
		
errorHandler: 
		setGroupMaxRep = False
		
	End Function
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		
		iRepetitionCounter = 1
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class
