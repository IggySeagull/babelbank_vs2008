Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Report_NET.Report")> Public Class Report
	
	'Public nIndex As Double
	Private nIndex As Double ' Endret til Private 07.08.2008
	Private iReportNO As Short
	Private bPreview As Boolean
	Private bPrinter As Boolean
	Private bAskForPrinter As Boolean
	Private bEMail As Boolean
	Private sEMailAdresses As String
	Private sExportType As String
	Private sExportFilename As String
	Private iBreakLevel As Short
	Private iTotalLevel As Short
	Private iDetailLevel As Short
	Private sHeading As String
	Private sBitmapFilename As String
	Private sPrinterName As String
	' added 21.11.2007 - User may decide to use a long dateformat (valid for reports 001, 002, 003 pr 21.11.2007)
    Private bLongDate As Boolean
    ' added 19.01.2011
    Private sReportName As String
    ' added 20.01.2011
    Private sReportSQL As String
    Private iReportSort As Integer
    Private iReportWhen As Integer
    Private bIncludeOCR As Boolean
    ' added 25.01.2011
    Private bReportOnDatabase As Boolean
    Private bSaveBeforeReport As Boolean
    Private bSpecialReport As Boolean
    ' added 18.03.2011
    Private bClientReport As Boolean
    Private bActivateSpecialSQL As Boolean
    '********* START PROPERTY SETTINGS ***********************
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	Public Property EMail() As Boolean
		Get
			EMail = bEMail
		End Get
		Set(ByVal Value As Boolean)
			bEMail = Value
		End Set
	End Property
	Public Property EMailAdresses() As String
		Get
			EMailAdresses = sEMailAdresses
		End Get
		Set(ByVal Value As String)
			sEMailAdresses = Value
		End Set
	End Property
	Public Property Printer() As Boolean
		Get
			Printer = bPrinter
		End Get
		Set(ByVal Value As Boolean)
			bPrinter = Value
		End Set
	End Property
	Public Property LongDate() As Boolean
		Get
			LongDate = bLongDate
		End Get
		Set(ByVal Value As Boolean)
			bLongDate = Value
		End Set
	End Property
	Public Property AskForPrinter() As Boolean
		Get
			AskForPrinter = bAskForPrinter
		End Get
		Set(ByVal Value As Boolean)
			bAskForPrinter = Value
		End Set
	End Property
	Public Property Preview() As Boolean
		Get
			Preview = bPreview
		End Get
		Set(ByVal Value As Boolean)
			bPreview = Value
		End Set
	End Property
	Public Property BitmapFilename() As String
		Get
			BitmapFilename = sBitmapFilename
		End Get
		Set(ByVal Value As String)
			sBitmapFilename = Value
		End Set
	End Property
	Public Property PrinterName() As String
		Get
			PrinterName = sPrinterName
		End Get
		Set(ByVal Value As String)
			sPrinterName = Value
		End Set
	End Property
	Public Property Heading() As String
		Get
			Heading = sHeading
		End Get
		Set(ByVal Value As String)
			sHeading = Value
		End Set
	End Property
	Public Property ExportType() As String
		Get
			ExportType = sExportType
		End Get
		Set(ByVal Value As String)
			sExportType = Value
		End Set
	End Property
	Public Property ExportFilename() As String
		Get
			ExportFilename = sExportFilename
		End Get
		Set(ByVal Value As String)
			sExportFilename = Value
		End Set
	End Property
	Public Property ReportNo() As Short
		Get
			ReportNo = iReportNO
		End Get
		Set(ByVal Value As Short)
			iReportNO = Value
		End Set
	End Property
	Public Property DetailLevel() As Short
		Get
			DetailLevel = iDetailLevel
		End Get
		Set(ByVal Value As Short)
			iDetailLevel = Value
		End Set
	End Property
	Public Property TotalLevel() As Short
		Get
			TotalLevel = iTotalLevel
		End Get
		Set(ByVal Value As Short)
			iTotalLevel = Value
		End Set
	End Property
	Public Property BreakLevel() As Short
		Get
			BreakLevel = iBreakLevel
		End Get
		Set(ByVal Value As Short)
			iBreakLevel = Value
		End Set
    End Property
    ' 19.01.2011 added ReportName
    Public Property ReportName() As String
        Get
            ReportName = sReportName
        End Get
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    ' 20.01.2011 added ReportName
    Public Property ReportSQL() As String
        Get
            ReportSQL = sReportSQL
        End Get
        Set(ByVal Value As String)
            sReportSQL = Value
        End Set
    End Property
    ' 20.01.2011 added ReportSort
    Public Property ReportSort() As Integer
        Get
            ReportSort = iReportSort
        End Get
        Set(ByVal Value As Integer)
            iReportSort = Value
        End Set
    End Property
    ' 20.01.2011 added ReportSort
    Public Property ReportWhen() As Integer
        Get
            ReportWhen = iReportWhen
        End Get
        Set(ByVal Value As Integer)
            iReportWhen = Value
        End Set
    End Property
    ' 20.01.2011 added ReportSort
    Public Property IncludeOCR() As Boolean
        Get
            IncludeOCR = bIncludeOCR
        End Get
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    ' added next 3 25.01.2011
    Public Property ReportOnDatabase() As Boolean
        Get
            ReportOnDatabase = bReportOnDatabase
        End Get
        Set(ByVal Value As Boolean)
            bReportOnDatabase = Value
        End Set
    End Property
    Public Property SaveBeforeReport() As Boolean
        Get
            SaveBeforeReport = bSaveBeforeReport
        End Get
        Set(ByVal Value As Boolean)
            bSaveBeforeReport = Value
        End Set
    End Property
    Public Property SpecialReport() As Boolean
        Get
            SpecialReport = bSpecialReport
        End Get
        Set(ByVal Value As Boolean)
            bSpecialReport = Value
        End Set
    End Property
    ' added 18.03.2011
    Public Property ClientReport() As Boolean
        Get
            ClientReport = bClientReport
        End Get
        Set(ByVal Value As Boolean)
            bClientReport = Value
        End Set
    End Property
    'added 13.01.2012
    Public Property ActivateSpecialSQL() As Boolean
        Get
            ActivateSpecialSQL = bActivateSpecialSQL
        End Get
        Set(ByVal Value As Boolean)
            bActivateSpecialSQL = Value
        End Set
    End Property

    '********* END PROPERTY SETTINGS ***********************

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		
		'Defaults
		iReportNO = 0
		bPreview = True
		bPrinter = True
		bAskForPrinter = True
		bEMail = False
        sExportType = ""
        sExportFilename = ""
        iBreakLevel = 0
        iTotalLevel = 0
        iDetailLevel = 0
        sHeading = ""
        sBitmapFilename = ""
        sPrinterName = ""
		bLongDate = False
        sReportName = ""
        ' added 20.01.2011
        sReportSQL = ""
        iReportSort = 0
        iReportWhen = 0
        bIncludeOCR = False
        ' added 25.01.2011
        bReportOnDatabase = True
        bSaveBeforeReport = False
        bSpecialReport = False
        ' added 18-03-2011
        bClientReport = False
        sEMailAdresses = ""
        bActivateSpecialSQL = False

		' When vbbabel.dll is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
    Public Function Load(ByRef iCompanyNo As Short, ByRef myBBDB_AccessReportsReader As System.Data.OleDb.OleDbDataReader) As Boolean

        'FileSetup_ID = Index 0, not used here
        If Not myBBDB_AccessReportsReader.IsDBNull(1) Then
            iReportNO = myBBDB_AccessReportsReader.GetInt32(1)
        End If
        bPreview = myBBDB_AccessReportsReader.GetBoolean(2)
        bPrinter = myBBDB_AccessReportsReader.GetBoolean(3)
        bAskForPrinter = myBBDB_AccessReportsReader.GetBoolean(4)
        bEMail = myBBDB_AccessReportsReader.GetBoolean(5)
        If Not myBBDB_AccessReportsReader.IsDBNull(6) Then
            sExportType = Trim(myBBDB_AccessReportsReader.GetString(6))
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(7) Then
            sExportFilename = myBBDB_AccessReportsReader.GetString(7)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(8) Then
            iBreakLevel = myBBDB_AccessReportsReader.GetInt32(8)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(9) Then
            iTotalLevel = myBBDB_AccessReportsReader.GetInt32(9)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(10) Then
            iDetailLevel = myBBDB_AccessReportsReader.GetInt32(10)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(11) Then
            sHeading = myBBDB_AccessReportsReader.GetString(11)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(12) Then
            sBitmapFilename = myBBDB_AccessReportsReader.GetString(12)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(13) Then
            sPrinterName = myBBDB_AccessReportsReader.GetString(13)
        End If
        bLongDate = myBBDB_AccessReportsReader.GetBoolean(14)
        If Not myBBDB_AccessReportsReader.IsDBNull(15) Then
            sReportName = myBBDB_AccessReportsReader.GetString(15)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(16) Then
            sReportSQL = myBBDB_AccessReportsReader.GetString(16) 'ReportSQL
        End If
        bIncludeOCR = myBBDB_AccessReportsReader.GetBoolean(17)
        If Not myBBDB_AccessReportsReader.IsDBNull(18) Then
            iReportSort = myBBDB_AccessReportsReader.GetInt32(18)
        End If
        If Not myBBDB_AccessReportsReader.IsDBNull(19) Then
            iReportWhen = myBBDB_AccessReportsReader.GetInt32(19)
        End If
        bReportOnDatabase = myBBDB_AccessReportsReader.GetBoolean(20)
        bSaveBeforeReport = myBBDB_AccessReportsReader.GetBoolean(21)
        bSpecialReport = myBBDB_AccessReportsReader.GetBoolean(22)
        bClientReport = myBBDB_AccessReportsReader.GetBoolean(23)
        If Not myBBDB_AccessReportsReader.IsDBNull(24) Then
            sEMailAdresses = Trim(myBBDB_AccessReportsReader.GetString(24))
        End If
        bActivateSpecialSQL = myBBDB_AccessReportsReader.GetBoolean(25)


        ' Prepare some reports based on special setup

        ' Report on saved data or collections ?
        ' -------------------------------------
        Select Case iReportNO
            Case 510, 512, 12 '25.09.2019 - Added 12 (always report on DB). Gjensidige has had problems with this several times
                ' Report on database only;
                ' - 510, 512
                bReportOnDatabase = True
                bSaveBeforeReport = True

            Case 912, 990 ', 12 '25.09.2019 - Removed 12 (always report on DB, see above). Gjensidige has had problems with this several times
                ' report in collections only;
                ' - 012
                ' - 912
                ' - 990
                bReportOnDatabase = False
                bSaveBeforeReport = False

        End Select

        ' Some reports have no choice for IncludeOCR. Set to True for those    
        If iReportNO = 4 Or iReportNO = 5 Or iReportNO = 12 Or iReportNO = 505 Or iReportNO = 510 Or iReportNO = 512 Then
            bIncludeOCR = True
        End If


        Load = True

    End Function
End Class
