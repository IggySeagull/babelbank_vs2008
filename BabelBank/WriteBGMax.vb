﻿Option Strict Off
Option Explicit On
Module WriteBGMax

    '==========================================================================
    ' NB!
    ' Denne eksport kan foreløpig kun brukes til OCR-poster, da det ikke er med
    ' TK25 (Informasjonspost), TK26, TK27, TK28 (Navn/adresseposter)
    '==========================================================================
    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    ' - filenivå: sum beløp, antall records, antall transer, siste dato
    '             disse påvirkes nedover i nivåene
    Dim nFileSumAmount As Double, nFileNoRecords As Double
    Dim nFileNoAvdragsRecords As Double = 0
    Dim nFileNoExtraRefRecords As Double = 0
    Dim nBatchSumAmount As Double, nBatchNoRecords As Double
    Dim nNoOfInsattningsposter As Double = 0
    Dim sPayNumber As String
    Dim bFileStartWritten As Boolean
    Dim sSpecial As String = ""

    Function WriteBGMaxFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String, ByVal sClientNo As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double, k As Double, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim sNewOwnref As String
        Dim sTxt As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sOldAccount As String = ""
        Dim sOldDebitAccount As String = ""
        Dim sOldCurrency As String = ""
        Dim bOppningspostWritten As Boolean = False
        Dim sOldDatePayment As String = ""

        Dim iFreetextCounter As Integer
        Dim sFreetextFixed As String
        Dim sFreetextVariable As String
        Dim sTemp As String = ""
        Dim sTemp2 As String = ""
        Dim sZip As String = ""

        Try

            'XokNET 27.01.2010
            nFileNoRecords = 0
            sOldAccount = "--------------------"

            ' New 15.02.06
            ' Did not write fileheader if we run more than once without leaving BabelBank
            If Len(Dir(sFilenameOut)) > 0 Then
                bFileStartWritten = True
            Else
                bFileStartWritten = False
            End If
            '-end 15.02.06 --------------

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                sSpecial = oBabel.Special  ' added 20.12.2018

                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If oPayment.Cancel = False And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    ' 13.02.2019 - added  oPayment.VB_ClientNo = sClientNo - same two other longer down
                                    If oPayment.I_Account = sI_Account Or oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = vbNullString
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    If Not bFileStartWritten Then
                        ' write only once!
                        sLine = WriteBGMaxFileStart()  ' XOKNET 08.07.2015 added last parameter
                        oFile.WriteLine(sLine)
                        sPayNumber = "0"
                        bFileStartWritten = True
                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And Not oPayment.Exported Then

                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Or oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = vbNullString
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then
                            i = i + 1

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                                    If oPayment.Cancel = False And Not oPayment.Exported Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Or oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then
                                    j = j + 1

                                    If sOldDatePayment <> oPayment.DATE_Payment Or sOldCurrency <> oPayment.MON_TransferCurrency Or sOldAccount <> oPayment.I_Account Then
                                        sOldCurrency = oPayment.MON_TransferCurrency
                                        sOldAccount = oPayment.I_Account
                                        sOldDatePayment = oPayment.DATE_Payment

                                        bOppningspostWritten = False

                                        ' Write endrecord for previous debit Bankgiro
                                        If nFileNoRecords > 0 Then
                                            sLine = WriteBGMaxIsattningspost(sOldAccount, sOldDatePayment, sOldCurrency)
                                            oFile.WriteLine(sLine)
                                        End If
                                        ' write new startrecord
                                        sLine = WriteBGMaxOppningspost(oPayment)
                                        oFile.WriteLine(sLine)
                                        bOppningspostWritten = True
                                    End If

                                    For Each oInvoice In oPayment.Invoices
                                        sTxt = ""
                                        If sSpecial = "RAMUDDEN_INCOMING" Then
                                            ' 21.12.2018 - remove leading zeros in KID for Ramudden;
                                            If Not EmptyString(oInvoice.Unique_Id) Then
                                                oInvoice.Unique_Id = RemoveLeadingCharacters(oInvoice.Unique_Id, "0")
                                                'If oPayment.BANK_CountryCode = "FI" Then
                                                ' In Finland, test on EUR currency
                                                If oPayment.MON_InvoiceCurrency = "EUR" Then
                                                    ' remove last digit from Reference for Finland, keep invoiceno only
                                                    If Left(oInvoice.Unique_Id, 1) = "9" And (Len(oInvoice.Unique_Id) = 5 Or Len(oInvoice.Unique_Id) = 6) Then
                                                        ' 25.04.2019 - do not remove for invoices starting with 9, and 5/6 digits long
                                                    Else
                                                        oInvoice.Unique_Id = Left(oInvoice.Unique_Id, oInvoice.Unique_Id.Length - 1)
                                                    End If
                                                End If
                                            End If
                                        End If

                                        If Not oInvoice.Exported Then
                                            sLine = WriteBGMaxPayment(oPayment, oInvoice)
                                            oFile.WriteLine(sLine)
                                        End If ' If Not oInvoice.Exported Then

                                        ' 16.11.2018 - Ramudden has name,address, etc
                                        If sSpecial = "RAMUDDEN_INCOMING" Then
                                            'sFreetextVariable = ""
                                            'iFreetextCounter = 0
                                            'For Each oFreetext In oInvoice.Freetexts
                                            '    iFreetextCounter = iFreetextCounter + 1
                                            '    If iFreetextCounter = 1 Then
                                            '        sFreetextVariable = RTrim(oFreetext.Text)
                                            '    ElseIf iFreetextCounter = 2 Then
                                            '        sFreetextVariable = sFreetextVariable & " " & RTrim(oFreetext.Text)
                                            '    Else
                                            '        Exit For
                                            '    End If
                                            'Next oFreetext

                                            'If EmptyString(sFreetextVariable) Then
                                            '    sFreetextVariable = sFreetextFixed
                                            'End If

                                            '' remove KID: from text fro Ramudden;
                                            'sFreetextVariable = Replace(sFreetextVariable, "KID: ", "")

                                            'If Not EmptyString(sFreetextVariable) Then
                                            '    If sFreetextVariable.Length > 40 Then
                                            '        sFreetextVariable = sFreetextVariable.Substring(0, 40)
                                            '    End If

                                            '    sLine = WriteBGMax_SGFEQFreetext(sFreetextVariable)
                                            '    oFile.WriteLine(sLine)

                                            'End If

                                            If Not EmptyString(oPayment.E_Name) Then
                                                sTemp = oPayment.E_Name
                                                If sTemp.Length > 35 Then
                                                    sTemp = sTemp.Substring(0, 35)
                                                End If
                                                sLine = WriteBGMax_SGFEQName(sTemp)
                                                oFile.WriteLine(sLine)
                                            End If

                                            If Not EmptyString(oPayment.E_Adr1) Then
                                                sTemp = oPayment.E_Adr1.Trim
                                                If Not EmptyString(oPayment.E_City) Then
                                                    sTemp2 = oPayment.E_City.Trim
                                                Else
                                                    If Not EmptyString(oPayment.E_Adr2) Then
                                                        sTemp2 = oPayment.E_Adr2.Trim
                                                    Else
                                                        If Not EmptyString(oPayment.E_Adr3) Then
                                                            sTemp2 = oPayment.E_Adr3.Trim
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If Not EmptyString(oPayment.E_Adr2) Then
                                                    sTemp = oPayment.E_Adr2.Trim
                                                    If Not EmptyString(oPayment.E_City) Then
                                                        sTemp2 = oPayment.E_City.Trim
                                                    Else
                                                        If Not EmptyString(oPayment.E_Adr3) Then
                                                            sTemp2 = oPayment.E_Adr3.Trim
                                                        End If
                                                    End If
                                                Else
                                                    If Not EmptyString(oPayment.E_City) Then
                                                        sTemp = oPayment.E_City.Trim
                                                    End If
                                                End If
                                            End If

                                            sZip = oPayment.E_Zip.Trim

                                            If Not EmptyString(sTemp) Or Not EmptyString(sZip) Then
                                                If sTemp.Length > 35 Then
                                                    sTemp = sTemp.Substring(0, 35)
                                                End If
                                                sLine = WriteBGMax_SGFEQAddress(sTemp, sZip)
                                                oFile.WriteLine(sLine)
                                                If Not EmptyString(sTemp2) Or Not EmptyString(oPayment.E_CountryCode) Then
                                                    If Not EmptyString(oPayment.E_CountryCode) Then
                                                        sTemp = oPayment.E_CountryCode
                                                    Else
                                                        sTemp = ""
                                                    End If
                                                    sLine = WriteBGMax_SGFEQAddress2(sTemp2, sTemp)
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If
                                        End If  'If sSpecial = "RAMUDDEN" Then
                                        oInvoice.Exported = True
                                    Next oInvoice
                                    oPayment.Exported = True
                                End If
                            Next ' payment
                        End If
                    Next 'batch
                End If
            Next 'Babelfile

            If nFileNoRecords > 0 Then
                ' Write endrecord for previous debit Bankgiro
                If nFileNoRecords > 0 Then
                    sLine = WriteBGMaxIsattningspost(sOldAccount, sOldDatePayment, sOldCurrency)
                    oFile.WriteLine(sLine)
                End If

                sLine = WriteBGMaxFileEnd()
                oFile.WriteLine(sLine)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBGMaxFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBGMaxFile = True

    End Function
    Function WriteBGMaxFileStart() As String
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 0
        nBatchSumAmount = 0
        nBatchNoRecords = 0

        sLine = "01"        ' 1-2
        sLine = sLine & "BGMAX" & Space(15) '03-22
        sLine = sLine & "01"    '23-24 Layout versjon
        sLine = sLine & Format(Date.Today, "yyyyMMddhhmmssffffff")  '25-44"
        sLine = sLine & "P"       '45  P=Produksjon
        sLine = sLine & Space(35)    '46-80

        WriteBGMaxFileStart = sLine

    End Function
    Function WriteBGMaxOppningspost(ByVal oPayment As vbBabel.Payment) As String
        Dim sLine As String

        nBatchSumAmount = 0
        nBatchNoRecords = 0

        sLine = "05"        ' 1-2
        If sSpecial = "RAMUDDEN_INCOMING" Then
            sLine = sLine & PadRight(oPayment.I_Account, 10, " ")     '3-12 Første 10 i kontonummer
        Else
            sLine = sLine & PadRight(oPayment.I_Account, 10, "0")     '3-12 Første 10 i kontonummer
        End If
        sLine = sLine & Space(10)                               '13-22 Postgironr, foreløpig ikke implementert
        sLine = sLine & oPayment.MON_InvoiceCurrency            '23-25 Valuta
        sLine = sLine & Space(55)    '26-80

        WriteBGMaxOppningspost = sLine

    End Function
    Function WriteBGMaxIsattningspost(ByVal sI_Account As String, ByVal sDate As String, ByVal sCurrency As String) As String
        Dim sLine As String
        nNoOfInsattningsposter = nNoOfInsattningsposter + 1

        sLine = "15"        ' 1-2
        If sSpecial = "RAMUDDEN_INCOMING" Then
            sLine = sLine & StrDup(35, "0")          '3-37 Mottakers kontonummer, 35 siffer !!!
        Else
            sLine = sLine & PadLeft(Trim(sI_Account), 35, "0")          '3-37 Mottakers kontonummer, 35 siffer !!!
        End If
        sLine = sLine & PadRight(sDate, 8, " ")                     '38-45 Betalningsdato
        sLine = sLine & Right(Format(Date.Now, "MM"), 1) & Format(Date.Now, "ddhh")                   '46-50 Løpenr, unikt pr kontonr i løpet av et år !!
        sLine = sLine & PadLeft(Str(Math.Abs(nBatchSumAmount)), 18, "0")  '51-68 Belopp, i øre
        sLine = sLine & PadRight(sCurrency, 3, " ")                 '69-71 Valuta
        sLine = sLine & PadLeft(Str(Math.Abs(nBatchNoRecords)), 8, "0")  '72-79 antall betalinger
        sLine = sLine & Space(1)                                    '80 Typ av isetting

        WriteBGMaxIsattningspost = sLine

    End Function
    Function WriteBGMaxPayment(ByVal oPayment As Payment, ByVal oinvoice As Invoice) As String
        Dim sLine As String
        Dim oFreetext As Freetext
        Dim sRefCode As String = ""
        Dim sTxt As String = ""

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
        nBatchNoRecords = nBatchNoRecords + 1

        If oPayment.MON_InvoiceAmount < 0 Then
            ' avdragspost
            sLine = "21"        '1-2
            nFileNoAvdragsRecords = nFileNoAvdragsRecords + 1
        Else
            sLine = "20"        '1-2
            nFileNoRecords = nFileNoRecords + 1
        End If

        sLine = sLine & PadLeft(Replace(oPayment.E_Account, "-", ""), 10, "0") ' 3-12  Betalers bankgironummer

        If EmptyString(oInvoice.Unique_Id) Then
            For Each oFreetext In oInvoice.Freetexts
                sTxt = sTxt & " " & Trim$(oFreetext.Text)
                If Len(sTxt) = 0 Then
                    sRefCode = "0"
                Else
                    sRefCode = "3"
                End If
            Next

        Else
            ' pick up OCR
            sTxt = oInvoice.Unique_Id
            sRefCode = "2"
        End If
        sLine = sLine & PadRight(sTxt, 25, " ")                 '13-37  Spesifikation
        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 18, "0")  '38-55 Belopp, i øre
        sLine = sLine & sRefCode                                '56 Referenskod
        sLine = sLine & "1"                                     '57 Betalningskanalkod
        sLine = sLine & Format(Date.Now, "MMddhhmmssff")      '58-69 Løpenr, unikt pr kontonr i løpet av et år !!
        sLine = sLine & "0"                                     '70 Avvidikod
        sLine = sLine & Space(10)                               '71-80

        WriteBGMaxPayment = sLine

    End Function
    Function WriteBGMaxFileEnd() As String
        Dim sLine As String

        sLine = "70"        ' 1-2
        ' Avsändarens kontonummer
        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0")        '3-10  Antall betalningsposter i filen
        sLine = sLine & PadLeft(Str(nFileNoAvdragsRecords), 8, "0") '11-18 Antall avdragssposter i filen
        sLine = sLine & PadLeft(Str(nFileNoExtraRefRecords), 8, "0") '19-26 Antall avdragssposter i filen
        sLine = sLine & PadLeft(Str(nNoOfInsattningsposter), 8, "0") '27-34 Antall avdragssposter i filen

        'pad to 80 with 0
        sLine = PadLine(sLine, 80, " ")
        WriteBGMaxFileEnd = sLine

    End Function
    '25.06.2019 - NOT ALL - Removed most of this function, not used by SGF EQ
    '    Function WriteBGMax_SGFEQFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String) As Boolean

    '        Dim oFs As New Scripting.FileSystemObject
    '        Dim oFile As Scripting.TextStream
    '        Dim i As Double, j As Double, k As Double, l As Double
    '        Dim oBabel As BabelFile
    '        Dim oBatch As Batch
    '        Dim oPayment As Payment
    '        Dim oInvoice As Invoice
    '        Dim oFreetext As Freetext
    '        Dim sNewOwnref As String
    '        Dim sTxt As String
    '        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
    '        Dim sOldAccount As String = ""
    '        Dim sOldDebitAccount As String = ""
    '        Dim sOldCurrency As String = ""
    '        Dim bOppningspostWritten As Boolean = False
    '        Dim sOldDatePayment As String = ""
    '        Dim iFreetextCounter As Integer
    '        Dim sFreetextFixed As String
    '        Dim sFreetextVariable As String
    '        Dim sTemp As String = ""
    '        Dim sTemp2 As String = ""
    '        Dim sZip As String = ""

    '        ' lag en outputfil
    '        On Error GoTo errCreateOutputFile


    '        'XokNET 27.01.2010
    '        nFileNoRecords = 0
    '        sOldAccount = "--------------------"

    '        ' New 15.02.06
    '        ' Did not write fileheader if we run more than once without leaving BabelBank
    '        If Len(Dir(sFilenameOut)) > 0 Then
    '            bFileStartWritten = True
    '        Else
    '            bFileStartWritten = False
    '        End If
    '        '-end 15.02.06 --------------

    '        oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

    '        For Each oBabel In oBabelFiles
    '            bExportoBabel = False
    '            'Have to go through each batch-object to see if we have objects thatt shall
    '            ' be exported to this exportfile
    '            For Each oBatch In oBabel.Batches
    '                For Each oPayment In oBatch.Payments
    '                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
    '                        If oPayment.Cancel = False And Not oPayment.Exported Then
    '                            If Not bMultiFiles Then
    '                                bExportoBabel = True
    '                            Else
    '                                If oPayment.I_Account = sI_Account Then
    '                                    If InStr(oPayment.REF_Own, "&?") Then
    '                                        'Set in the part of the OwnRef that BabelBank is using
    '                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
    '                                    Else
    '                                        sNewOwnref = vbNullString
    '                                    End If
    '                                    If sNewOwnref = sOwnRef Then
    '                                        bExportoBabel = True
    '                                    End If
    '                                End If
    '                            End If
    '                        End If 'If oPayment.Cancel = False Then
    '                    End If
    '                    If bExportoBabel Then
    '                        Exit For
    '                    End If
    '                Next
    '                If bExportoBabel Then
    '                    Exit For
    '                End If
    '            Next

    '            If bExportoBabel Then

    '                If Not bFileStartWritten Then
    '                    ' write only once!
    '                    sLine = WriteBGMax_SGFEQFileStart()  ' XOKNET 08.07.2015 added last parameter
    '                    oFile.WriteLine(sLine)
    '                    sPayNumber = "0"
    '                    bFileStartWritten = True
    '                End If

    '                i = 0
    '                'Loop through all Batch objs. in BabelFile obj.
    '                For Each oBatch In oBabel.Batches
    '                    bExportoBatch = False
    '                    'Have to go through the payment-object to see if we have objects thatt shall
    '                    ' be exported to this exportfile
    '                    For Each oPayment In oBatch.Payments
    '                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
    '                            'Don't export payments that have been cancelled
    '                            If oPayment.Cancel = False And Not oPayment.Exported Then

    '                                If Not bMultiFiles Then
    '                                    bExportoBatch = True
    '                                Else
    '                                    If oPayment.I_Account = sI_Account Then
    '                                        If InStr(oPayment.REF_Own, "&?") Then
    '                                            'Set in the part of the OwnRef that BabelBank is using
    '                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
    '                                        Else
    '                                            sNewOwnref = vbNullString
    '                                        End If
    '                                        If sNewOwnref = sOwnRef Then
    '                                            bExportoBatch = True
    '                                        End If
    '                                    End If
    '                                End If
    '                            End If 'If oPayment.Cancel = False Then
    '                        End If
    '                        'If true exit the loop, and we have data to export
    '                        If bExportoBatch Then
    '                            Exit For
    '                        End If
    '                    Next

    '                    If bExportoBatch Then
    '                        i = i + 1

    '                        j = 0
    '                        For Each oPayment In oBatch.Payments
    '                            bExportoPayment = False
    '                            'Have to go through the payment-object to see if we have objects thatt shall
    '                            ' be exported to this exportfile
    '                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
    '                                'Don't export payments that have been cancelled
    '                                'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
    '                                If oPayment.Cancel = False And Not oPayment.Exported Then

    '                                    If Not bMultiFiles Then
    '                                        bExportoPayment = True
    '                                    Else
    '                                        If oPayment.I_Account = sI_Account Then
    '                                            If InStr(oPayment.REF_Own, "&?") Then
    '                                                'Set in the part of the OwnRef that BabelBank is using
    '                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
    '                                            Else
    '                                                sNewOwnref = vbNullString
    '                                            End If
    '                                            If sNewOwnref = sOwnRef Then
    '                                                bExportoPayment = True
    '                                            End If
    '                                        End If
    '                                    End If
    '                                Else
    '                                    oPayment.Exported = True
    '                                End If 'If oPayment.Cancel = False Then
    '                            End If

    '                            If bExportoPayment Then
    '                                j = j + 1

    '                                'First we have to find the freetext to the payment-lines.
    '                                iFreetextCounter = 0
    '                                sFreetextFixed = vbNullString
    '                                For Each oInvoice In oPayment.Invoices

    '                                    If oInvoice.MATCH_Original = True Then
    '                                        For Each oFreetext In oInvoice.Freetexts
    '                                            iFreetextCounter = iFreetextCounter + 1
    '                                            If iFreetextCounter = 1 Then
    '                                                sFreetextFixed = RTrim$(oFreetext.Text)
    '                                            ElseIf iFreetextCounter = 2 Then
    '                                                sFreetextFixed = sFreetextFixed & " " & RTrim$(oFreetext.Text)
    '                                            Else
    '                                                Exit For
    '                                            End If
    '                                        Next oFreetext
    '                                    End If
    '                                Next oInvoice
    '                                'sFreetextFixed = Trim$(oPayment.E_Name) & " " & sFreetextFixed

    '                                If sOldDatePayment <> oPayment.DATE_Payment Or sOldCurrency <> oPayment.MON_TransferCurrency Or sOldAccount <> oPayment.I_Account Then
    '                                    sOldCurrency = oPayment.MON_TransferCurrency
    '                                    sOldAccount = oPayment.I_Account
    '                                    sOldDatePayment = oPayment.DATE_Payment

    '                                    bOppningspostWritten = False

    '                                    ' Write endrecord for previous debit Bankgiro
    '                                    If nFileNoRecords > 0 Then
    '                                        sLine = WriteBGMax_SGFEQIsattningspost(sOldAccount, sOldDatePayment, sOldCurrency)
    '                                        oFile.WriteLine(sLine)
    '                                    End If
    '                                    ' write new startrecord
    '                                    sLine = WriteBGMax_SGFEQOppningspost(oPayment)
    '                                    oFile.WriteLine(sLine)
    '                                    bOppningspostWritten = True
    '                                End If

    '                                For Each oInvoice In oPayment.Invoices
    '                                    sTxt = ""
    '                                    If oInvoice.MATCH_Final = True Then

    '                                        If Not oInvoice.Exported Then
    '                                            sLine = WriteBGMax_SGFEQPayment(oPayment, oInvoice)
    '                                            oFile.WriteLine(sLine)

    '                                            sFreetextVariable = ""
    '                                            iFreetextCounter = 0
    '                                            For Each oFreetext In oInvoice.Freetexts
    '                                                iFreetextCounter = iFreetextCounter + 1
    '                                                If iFreetextCounter = 1 Then
    '                                                    sFreetextVariable = RTrim(oFreetext.Text)
    '                                                ElseIf iFreetextCounter = 2 Then
    '                                                    sFreetextVariable = sFreetextVariable & " " & RTrim(oFreetext.Text)
    '                                                Else
    '                                                    Exit For
    '                                                End If
    '                                            Next oFreetext

    '                                            If EmptyString(sFreetextVariable) Then
    '                                                sFreetextVariable = sFreetextFixed
    '                                            End If

    '                                            If Not EmptyString(sFreetextVariable) Then
    '                                                If sFreetextVariable.Length > 40 Then
    '                                                    sFreetextVariable = sFreetextVariable.Substring(0, 40)
    '                                                End If

    '                                                sLine = WriteBGMax_SGFEQFreetext(sFreetextVariable)
    '                                                oFile.WriteLine(sLine)

    '                                            End If

    '                                            If Not EmptyString(oPayment.E_Name) Then
    '                                                sTemp = oPayment.E_Name
    '                                                If sTemp.Length > 35 Then
    '                                                    sTemp = sTemp.Substring(0, 35)
    '                                                End If
    '                                                sLine = WriteBGMax_SGFEQName(sTemp)
    '                                                oFile.WriteLine(sLine)
    '                                            End If

    '                                            If Not EmptyString(oPayment.E_Adr1) Then
    '                                                sTemp = oPayment.E_Adr1.Trim
    '                                                If Not EmptyString(oPayment.E_City) Then
    '                                                    sTemp2 = oPayment.E_City.Trim
    '                                                Else
    '                                                    If Not EmptyString(oPayment.E_Adr2) Then
    '                                                        sTemp2 = oPayment.E_Adr2.Trim
    '                                                    Else
    '                                                        If Not EmptyString(oPayment.E_Adr3) Then
    '                                                            sTemp2 = oPayment.E_Adr3.Trim
    '                                                        End If
    '                                                    End If
    '                                                End If
    '                                            Else
    '                                                If Not EmptyString(oPayment.E_Adr2) Then
    '                                                    sTemp = oPayment.E_Adr2.Trim
    '                                                    If Not EmptyString(oPayment.E_City) Then
    '                                                        sTemp2 = oPayment.E_City.Trim
    '                                                    Else
    '                                                        If Not EmptyString(oPayment.E_Adr3) Then
    '                                                            sTemp2 = oPayment.E_Adr3.Trim
    '                                                        End If
    '                                                    End If
    '                                                Else
    '                                                    If Not EmptyString(oPayment.E_City) Then
    '                                                        sTemp = oPayment.E_City.Trim
    '                                                    End If
    '                                                End If
    '                                            End If

    '                                            sZip = oPayment.E_Zip.Trim

    '                                            If Not EmptyString(sTemp) Or Not EmptyString(sZip) Then
    '                                                If sTemp.Length > 35 Then
    '                                                    sTemp = sTemp.Substring(0, 35)
    '                                                End If
    '                                                sLine = WriteBGMax_SGFEQAddress(sTemp, sZip)
    '                                                oFile.WriteLine(sLine)
    '                                                If Not EmptyString(sTemp2) Or Not EmptyString(oPayment.E_CountryCode) Then
    '                                                    If Not EmptyString(oPayment.E_CountryCode) Then
    '                                                        sTemp = oPayment.E_CountryCode
    '                                                    Else
    '                                                        sTemp = ""
    '                                                    End If
    '                                                    sLine = WriteBGMax_SGFEQAddress2(sTemp2, sTemp)
    '                                                    oFile.WriteLine(sLine)
    '                                                End If
    '                                            End If

    '                                        End If ' If Not oInvoice.Exported Then
    '                                    End If

    '                                    oInvoice.Exported = True
    '                                Next oInvoice
    '                                oPayment.Exported = True
    '                            End If
    '                        Next ' payment
    '                    End If
    '                Next 'batch
    '            End If
    '        Next 'Babelfile

    '        If nFileNoRecords > 0 Then
    '            ' Write endrecord for previous debit Bankgiro
    '            If nFileNoRecords > 0 Then
    '                sLine = WriteBGMax_SGFEQIsattningspost(sOldAccount, sOldDatePayment, sOldCurrency)
    '                oFile.WriteLine(sLine)
    '            End If

    '            sLine = WriteBGMax_SGFEQFileEnd()
    '            oFile.WriteLine(sLine)
    '        End If

    '        If Not oFile Is Nothing Then
    '            oFile.Close()
    '        End If
    '        oFile = Nothing
    '        oFs = Nothing

    '        WriteBGMax_SGFEQFile = True

    '        Throw New Exception("Filen ble skrevet korrekt.")

    '        Exit Function

    'errCreateOutputFile:
    '        ' XOKNET 27.08.2013
    '        If Not oFile Is Nothing Then
    '            oFile.Close()
    '        End If
    '        oFile = Nothing
    '        oFs = Nothing

    '        WriteBGMax_SGFEQFile = False

    '    End Function
    '    Function WriteBGMax_SGFEQFileStart() As String
    '        Dim sLine As String

    '        ' Reset file-totals:
    '        nFileSumAmount = 0
    '        nFileNoRecords = 0
    '        nBatchSumAmount = 0
    '        nBatchNoRecords = 0

    '        sLine = "01"        ' 1-2
    '        sLine = sLine & "BGMAX" & Space(15) '03-22
    '        sLine = sLine & "01"    '23-24 Layout versjon
    '        sLine = sLine & Format(Date.Now, "yyyyMMddHHmmssffffff")  '25-44"
    '        sLine = sLine & "P"       '45  P=Produksjon
    '        sLine = sLine & Space(35)    '46-80

    '        WriteBGMax_SGFEQFileStart = sLine

    '    End Function
    '    Function WriteBGMax_SGFEQOppningspost(ByVal oPayment As vbBabel.Payment) As String
    '        Dim sLine As String

    '        nBatchSumAmount = 0
    '        nBatchNoRecords = 0

    '        sLine = "05"        ' 1-2
    '        sLine = sLine & PadRight(oPayment.I_Account, 35, " ")     '3-37 (3-12) Første 10 i kontonummer
    '        sLine = sLine & Space(10)                               '38-47 (13-22) Postgironr, foreløpig ikke implementert
    '        sLine = sLine & oPayment.MON_InvoiceCurrency            '48-50 (23-25) Valuta
    '        sLine = sLine & Space(30)    '51-80 (26-80)

    '        WriteBGMax_SGFEQOppningspost = sLine

    '    End Function
    '    Function WriteBGMax_SGFEQIsattningspost(ByVal sI_Account As String, ByVal sDate As String, ByVal sCurrency As String) As String
    '        Dim sLine As String
    '        nNoOfInsattningsposter = nNoOfInsattningsposter + 1

    '        sLine = "15"        ' 1-2
    '        sLine = sLine & PadLeft(Trim(sI_Account), 35, "0")          '3-37 Mottakers kontonummer, 35 siffer !!!
    '        sLine = sLine & PadRight(sDate, 8, " ")                     '38-45 Betalningsdato
    '        sLine = sLine & Right(Format(Date.Now, "MM"), 1) & Format(Date.Now, "ddhh")                   '46-50 Løpenr, unikt pr kontonr i løpet av et år !!
    '        sLine = sLine & PadLeft(Str(Math.Abs(nBatchSumAmount)), 18, "0")  '51-68 Belopp, i øre
    '        sLine = sLine & PadRight(sCurrency, 3, " ")                 '69-71 Valuta
    '        sLine = sLine & PadLeft(Str(Math.Abs(nBatchNoRecords)), 8, "0")  '72-79 antall betalinger
    '        sLine = sLine & Space(1)                                    '80 Typ av isetting

    '        WriteBGMax_SGFEQIsattningspost = sLine

    '    End Function
    '    Function WriteBGMax_SGFEQPayment(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
    '        Dim sLine As String
    '        Dim oFreetext As Freetext
    '        Dim sRefCode As String = ""
    '        Dim sTxt As String = ""

    '        ' Add to Batch-totals:
    '        nFileSumAmount = nFileSumAmount + oinvoice.MON_InvoiceAmount
    '        nBatchSumAmount = nBatchSumAmount + oinvoice.MON_InvoiceAmount
    '        nBatchNoRecords = nBatchNoRecords + 1

    '        If oPayment.MON_InvoiceAmount < 0 Then
    '            ' avdragspost
    '            sLine = "21"        '1-2
    '            nFileNoAvdragsRecords = nFileNoAvdragsRecords + 1
    '        Else
    '            sLine = "20"        '1-2
    '            nFileNoRecords = nFileNoRecords + 1
    '        End If

    '        sLine = sLine & PadRight(Replace(oPayment.E_Account, "-", ""), 35, " ") ' 3-37 (3-12)  Betalers bankgironummer

    '        If Not EmptyString(oInvoice.MATCH_ID) And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
    '            ' pick up OCR
    '            'sTxt = oinvoice.Unique_Id
    '            sRefCode = "2"
    '            sLine = sLine & PadRight(oInvoice.MATCH_ID, 25, " ") '38-62 (13-37)  Spesifikation
    '        Else
    '            'For Each oFreetext In oinvoice.Freetexts
    '            '    sTxt = sTxt & " " & Trim$(oFreetext.Text)
    '            'Next
    '            'If Len(sTxt) = 0 Then
    '            '    sRefCode = "0"
    '            'Else
    '            '    sRefCode = "3"
    '            'End If
    '            sRefCode = "3"
    '            If Not EmptyString(oInvoice.MATCH_ID) And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
    '                sLine = sLine & PadRight(oInvoice.MATCH_ID, 25, " ") '38-62 (13-37)  Spesifikation
    '            Else
    '                sLine = sLine & Space(25) '38-62 (13-37)  Spesifikation
    '            End If
    '        End If
    '        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 18, "0")  '63-80 (38-55) Belopp, i øre
    '        sLine = sLine & sRefCode                                '81 (56) Referenskod
    '        If IsOCR(oPayment.PayCode) Then
    '            sLine = sLine & "1"                                     '82 (57) Betalningskanalkod
    '        Else
    '            sLine = sLine & "3"                                     '82 (57) Betalningskanalkod
    '        End If
    '        'sLine = sLine & Format(Date.Now, "MMddhhmmssff")      '83-94 (58-69) Løpenr, unikt pr kontonr i løpet av et år !!
    '        sLine = sLine & PadRight(oPayment.REF_Bank1, 12, " ")      '83-94 (58-69) Løpenr, unikt pr kontonr i løpet av et år !!
    '        If Not EmptyString(oInvoice.MATCH_ID) And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
    '            If Not EmptyString(oInvoice.MyField) Then
    '                If oInvoice.MyField.Trim.Length = 1 Then
    '                    sLine = sLine & oInvoice.MyField.Trim             '95 (70) Avvidikod
    '                Else
    '                    sLine = sLine & "0"                               '95 (70) Avvidikod
    '                End If
    '            Else
    '                sLine = sLine & "0"                                   '95 (70) Avvidikod
    '            End If
    '        Else
    '            sLine = sLine & "0"                                     '95 (70) Avvidikod
    '        End If
    '        sLine = sLine & Space(10)                               '96-85

    '        WriteBGMax_SGFEQPayment = sLine

    '    End Function
    '    Function WriteBGMax_SGFEQFreetext(ByVal sText As String) As String
    '        Dim sLine As String

    '        'nBatchNoRecords = nBatchNoRecords + 1

    '        sLine = "25"        '1-2
    '        sLine = sLine & PadRight(sText, 50, " ") '3-52 Information from the payer to the payee.
    '        sLine = sLine & Space(28) '53-80

    '        WriteBGMax_SGFEQFreetext = sLine

    '    End Function
    Function WriteBGMax_SGFEQName(ByVal sName As String) As String
        Dim sLine As String

        'nBatchNoRecords = nBatchNoRecords + 1

        sLine = "26"        '1-2
        sLine = sLine & PadRight(sName, 35, " ") '3-37 The payer’s name
        sLine = sLine & Space(35) '38-72 Extra name field. 
        sLine = sLine & Space(8) '73-80

        WriteBGMax_SGFEQName = sLine

    End Function
    Function WriteBGMax_SGFEQAddress(ByVal sAddress As String, ByVal sZip As String) As String
        Dim sLine As String

        'nBatchNoRecords = nBatchNoRecords + 1

        sLine = "27"        '1-2 'Transaction code
        sLine = sLine & PadRight(sAddress, 35, " ") '3-37 Payer's address.
        sLine = sLine & PadRight(sZip, 9, " ") '38-46 Extra name field. 
        sLine = sLine & Space(34) '47-80

        WriteBGMax_SGFEQAddress = sLine

    End Function
    Function WriteBGMax_SGFEQAddress2(ByVal sAddress As String, ByVal sCountryCode As String) As String
        Dim sLine As String

        'nBatchNoRecords = nBatchNoRecords + 1

        sLine = "28"        '1-2 'Transaction code
        sLine = sLine & PadRight(sAddress, 35, " ") '3-37 Payer's local address.
        sLine = sLine & Space(35) '38-72 Payer’s country.
        sLine = sLine & PadRight(sCountryCode, 2, " ") '73-74 Country code Payer’s country.
        sLine = sLine & Space(6) '75-80

        WriteBGMax_SGFEQAddress2 = sLine

    End Function
    '    Function WriteBGMax_SGFEQFileEnd() As String
    '        Dim sLine As String

    '        sLine = "70"        ' 1-2
    '        ' Avsändarens kontonummer
    '        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0")        '3-10  Antall betalningsposter i filen
    '        sLine = sLine & PadLeft(Str(nFileNoAvdragsRecords), 8, "0") '11-18 Antall avdragssposter i filen
    '        sLine = sLine & PadLeft(Str(nFileNoExtraRefRecords), 8, "0") '19-26 Antall avdragssposter i filen
    '        sLine = sLine & PadLeft(Str(nNoOfInsattningsposter), 8, "0") '27-34 Antall avdragssposter i filen

    '        'pad to 80 with 0
    '        sLine = PadLine(sLine, 80, " ")
    '        WriteBGMax_SGFEQFileEnd = sLine

    '    End Function

End Module