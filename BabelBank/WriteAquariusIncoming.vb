Option Strict Off
Option Explicit On
' Johannes# 
Module WriteAquariusIncoming
	Private nFileSumAmount As Double
	Private nFileNoRecords As Double
    Private nFileNoTransactions As Double
    'Private bShowMessage As Boolean
	
    Function WriteAquariusIncomingPayments(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef eTypeofRecords As BabelFiles.TypeofRecords, ByRef nNoOfTransactionsExported As Double, ByRef nAmountExported As Double, ByRef bPostAgainstObservationAccount As Boolean) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oChargesFile As Scripting.TextStream
        Dim bChargesFileCreated As Boolean = False
        Dim XMLChargesDoc As System.Xml.XmlDocument
        Dim sXMLFileName As String

        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oTempInvoice As vbBabel.Invoice
        Dim oFreeText As Freetext
        Dim iFreetextCounter As Short
        Dim sFreetextFixed, sFreeTextVariable As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sLineToWrite As String
        Dim bFirstBatch As Boolean
        Dim nSequenceNoTotal As Double
        'Removed 07.03.2008, no more use of nVoucherNo
        'Dim nVoucherNo As Double
        Dim sAlphaPart As String
        Dim nDigitsLen As Short
        Dim sGLAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sVoucherSerial As String
        Dim sVoucherType As String
        Dim xClientNo As String
        Dim sQ4Text As String
        Dim bFileInitiated As Boolean
        Dim aProposedAllocation(,) As String
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aProposedAllocation() As String
        Dim bProposedAllocationRecordWritten As Boolean
        Dim sNoMatchAccount As String
        Dim bChargesExists As Boolean
        Dim nTotalChargesAmount As Double
        Dim nChargesAmountToBeUsedInThisALLOC As Double
        Dim nAmountToUseToAdjustInvoiceAmount As Double
        Dim nTempAmount, nTempAmount2 As Double
        Dim sErrorMesage As String
        Dim bErrorExists As Boolean
        Dim nTotalRoundings As Double
        Dim bRoundingsExists As Boolean
        Dim bExportAccountNo As Boolean
        Dim bThisIsAnExportPayment As Boolean
        Dim bPaymentExported As Boolean = False

        'New variables 22.08.2007
        Dim nFraction As Double
        Dim nCalculatedAllocatedAmountSAC As Double

        'New variable 03.06.2008
        Dim sTempADAccountNumber As String

        Dim bExitLoop As Boolean
        ' hvis akonto: (oinvoice.matchtype = matchtype.MatchedOnCustomer)
        ' - ikke recon-post for denne, ellers som f�r. aKonto post beregnes av Aq som diff mellom Alloc og reconposter

        Dim aAccountArray(,) As String '21.04.2017 - Implemented the use of DayTotals
        Dim aAmountArray() As Double
        Dim aGLAmountArray() As Double
        Dim bUseDayTotals As Boolean
        Dim lCounter As Long
        Dim lArrayCounter As Long
        Dim bDeductCharges As Boolean = False

        'Added next 2 27.09.2018
        Dim oMyERPChargesConnection As vbBabel.DAL
        Dim aQueryArray As String(,,)
        Dim sMyGetClientsRefSQL As String = ""
        Dim sAccountCurrencyFromSetup As String = ""
        Dim bWriteCharges As Boolean
        Dim sChargesLine As String = ""
        Dim sTemp As String = ""
        Dim sChargesClientNo As String = ""
        Dim sChargesAccountNumber As String = ""
        Dim sChargesAgreementNumber As String = ""
        'Dim nodeChargesElementList As System.Xml.XmlNodeList
        Dim nodeChargesElement As System.Xml.XmlNode
        Dim sDebtorNumber As String = ""
        Dim sClientReference As String = ""
        Dim sAvtaleType As String = ""
        Dim sChargesFilenameOut As String = ""

        '25.04.2019 - Added next 4 variables
        Dim sSGValidateCurrencySQL As String = ""
        Dim aTempQueryArray(,,) As String
        Dim lNewArrayCounter As Long

        Dim nPaymentAmount As Double = 0
        Dim nInvoiceAmountFinal As Double = 0
        Dim nInvoiceAmountMatched As Double = 0


        Try

            If Not RunTime() Then
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.I_Account = "81010788293" Then
                                If Not oPayment.Exported Then
                                    nPaymentAmount = nPaymentAmount + oPayment.MON_InvoiceAmount
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            nInvoiceAmountFinal = nInvoiceAmountFinal + oInvoice.MON_InvoiceAmount
                                            If oInvoice.MATCH_Matched Then
                                                nInvoiceAmountMatched = nInvoiceAmountMatched + oInvoice.MON_InvoiceAmount
                                            End If
                                        End If
                                    Next oInvoice
                                End If
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabel
            End If


            ' lag en outputfil
            bAppendFile = False
            bFirstBatch = True
            sOldAccountNo = ""
            sVoucherSerial = "0"
            sVoucherType = "0"
            bFileInitiated = False
            nNoOfTransactionsExported = 0
            nAmountExported = 0

            oFs = New Scripting.FileSystemObject

            '21.04.2017 - Added the use of DayTotals
            bUseDayTotals = False

            If oBabelFiles.Count > 0 Then
                If oBabelFiles(1).VB_ProfileInUse Then
                    If oBabelFiles(1).VB_Profile.ManMatchFromERP Then
                        bUseDayTotals = True
                    End If
                End If
            End If

            If bUseDayTotals Then
                'New code 21.12.2006
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim Preserve aAmountArray(UBound(aAccountArray, 2))
                ReDim Preserve aGLAmountArray(UBound(aAccountArray, 2))
            End If

            '20.02.2017 - Do not export unmatched items when bPostAgainstObservationAccount = False (first time export)
            '02.04.2020 - KKIIDD Removed next IF
            'If eTypeofRecords <> BabelFiles.TypeofRecords.OCRonly Then
            If Not bPostAgainstObservationAccount Then
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.MATCH_Matched <> BabelFiles.MatchStatus.Matched Then
                                oPayment.Exported = True
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabel
            End If
            'End If

            '21.02.2017 - Undo all postings that ain't fully matched 
            For Each oBabel In oBabelFiles
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If Not oPayment.E_Name Is Nothing Then
                            If oPayment.E_Name.Length > 5 Then
                            End If
                        End If
                        If oPayment.MATCH_Matched <> BabelFiles.MatchStatus.Matched Then
                            '26.04.2018 - EUROVEMA
                            'Her skjer feilen
                            'N�r oPayment.MatchUseOriginalAmount = True, s� adder vi en ny Invoice i AfterMatching
                            'Denne fjernes i koden under
                            oPayment.MATCH_Reset()
                            oPayment.MATCH_Undo()

                        End If
                    Next oPayment
                Next oBatch
            Next oBabel

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If Not oPayment.Exported Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                'If oPayment.I_Account = sI_Account Then
                                ' 01.10.2018 � changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If


                            '02.04.2020 - KKIIDD Removed next IF
                            'If bExportoBabel Then
                            '    If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                            '        If Not IsOCR((oPayment.PayCode)) Then
                            '            bExportoBabel = False
                            '        Else
                            '            bExportoBabel = True
                            '            Exit For
                            '        End If
                            '    Else
                            '        If IsOCR((oPayment.PayCode)) Then
                            '            bExportoBabel = False
                            '        Else
                            '            bExportoBabel = True
                            '            Exit For
                            '            '                        For Each oInvoice In oPayment.Invoices
                            '            '                            If oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then
                            '            '                                bExportoBabel = True
                            '            '                                Exit For
                            '            '                            End If
                            '            '                        Next oInvoice
                            '        End If
                            '    End If
                            'End If
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    If Not bFileInitiated Then
                        If oFs.FileExists(sFilenameOut) Then
                            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

                        Else
                            'Create a headerrecord
                            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
                            'oFile.WriteLine "H; " & Trim$(oPayment.I_Account)
                        End If
                        bFileInitiated = True

                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If

                                '02.04.2020 - KKIIDD Removed next IF
                                'If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                                '    If Not IsOCR((oPayment.PayCode)) Then
                                '        bExportoBatch = False
                                '    Else
                                '        bExportoBatch = True
                                '    End If
                                'Else
                                '    If IsOCR((oPayment.PayCode)) Then
                                '        bExportoBatch = False
                                '    Else
                                '        bExportoBatch = True
                                '        '                        For Each oInvoice In oPayment.Invoices
                                '        '                            If oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then
                                '        '                                bExportoBatch = True
                                '        '                                Exit For
                                '        '                            End If
                                '        '                        Next oInvoice
                                '    End If
                                'End If
                            End If

                            If bExportoBatch Then
                                Exit For
                            End If

                        Next oPayment


                        If bExportoBatch Then

                            j = 0
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If

                                    '02.04.2020 - KKIIDD Removed next IF
                                    'If bExportoPayment Then
                                    '    If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                                    '        If Not IsOCR((oPayment.PayCode)) Then
                                    '            bExportoPayment = False

                                    '        End If
                                    '    Else
                                    '        If IsOCR((oPayment.PayCode)) Then
                                    '            bExportoPayment = False
                                    '        Else
                                    '            bExportoPayment = True
                                    '        End If
                                    '    End If
                                    'End If

                                End If

                                'New 22.08.2007 - to prevent the export of unmatched zero-amounts
                                If bExportoPayment Then

                                    '10.09.2007 - Never export zero-payments
                                    If oPayment.MATCH_UseoriginalAmountInMatching Then
                                        If oPayment.MON_OriginallyPaidAmount = 0 Then
                                            'If oPayment.MATCH_Matched = MatchStatus.NotMatched Then
                                            'Do not export
                                            bExportoPayment = False
                                            'Set as exported
                                            oPayment.Exported = True
                                            'End If
                                        End If
                                    Else
                                        If oPayment.MON_InvoiceAmount = 0 Then
                                            'If oPayment.MATCH_Matched = MatchStatus.NotMatched Then
                                            'Do not export
                                            bExportoPayment = False
                                            'Set as exported
                                            oPayment.Exported = True
                                            'End If
                                        End If
                                    End If

                                End If

                                If bExportoPayment Then
                                    'If Trim$(oPayment.E_Name) = "KOKO" Then
                                    '    Err.Raise 123, "Test", "Tester feilh�ndtering"
                                    'End If

                                    'Find the client
                                    If oPayment.I_Account <> sOldAccountNo Then
                                        sNoMatchAccount = ""
                                        If oPayment.VB_ProfileInUse Then
                                            sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If sI_Account = oaccount.Account Or sI_Account = Trim(oaccount.ConvertedAccount) Then
                                                            sGLAccount = Trim(oaccount.GLAccount)
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True
                                                            sAccountCurrencyFromSetup = oaccount.CurrencyCode

                                                            If bUseDayTotals Then
                                                                'Find the correct client in the array
                                                                For lCounter = 0 To UBound(aAccountArray, 2)
                                                                    If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                                        lArrayCounter = lCounter
                                                                        Exit For
                                                                    End If
                                                                Next lCounter
                                                            End If

                                                            If xClientNo <> oClient.ClientNo Then
                                                                'We have to find the last used sequenceno. in some cases
                                                                'First check if we use a profile
                                                                If oBatch.VB_ProfileInUse = True Then
                                                                    'Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                                                    'Case 1
                                                                    '    nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal
                                                                    'Case 2
                                                                    ''' old, changed 10.01.06 nVoucherNo = Val(Trim$(oClient.VoucherNo)) + 1
                                                                    If Not EmptyString((oClient.VoucherType)) Then
                                                                        sVoucherType = Trim(oClient.VoucherType)
                                                                    End If
                                                                    'If Not EmptyString(oClient.VoucherNo) Then
                                                                    '    sVoucherSerial = Trim$(oClient.VoucherNo)
                                                                    'End If
                                                                    'Case Else
                                                                    '    Err.Raise 12002, "WriteAxapta_ColumbusIT", LRS(12002, "Axapta_ColumbusIT", sFilenameOut)
                                                                    'End Select

                                                                    '30.04.2019 - moved from further down where we initiate the creation of the charges file.
                                                                    oMyERPChargesConnection = New vbBabel.DAL
                                                                    oMyERPChargesConnection.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                                                                    oMyERPChargesConnection.Company_ID = oBabel.VB_Profile.Company_ID
                                                                    oMyERPChargesConnection.DBProfile_ID = oClient.DBProfile_ID
                                                                    If oClient.DBProfile_ID = 99 Then
                                                                        oMyERPChargesConnection.DBProfile_ID = 1
                                                                    Else
                                                                        oMyERPChargesConnection.DBProfile_ID = oClient.DBProfile_ID
                                                                    End If
                                                                    If Not oMyERPChargesConnection.ConnectToDB() Then
                                                                        Throw New System.Exception(oMyERPChargesConnection.ErrorMessage)
                                                                    Else
                                                                        'MsgBox("F�rste kobling mot database er OK.")
                                                                    End If

                                                                End If
                                                                xClientNo = oClient.ClientNo
                                                                sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, (oClient.Client_ID))

                                                            End If


                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If
                                    End If

                                    If Not bAccountFound Then
                                        If Not oFile Is Nothing Then
                                            oFile.Close()
                                            oFile = Nothing
                                        End If

                                        If Not oFs Is Nothing Then
                                            oFs = Nothing
                                        End If

                                        Throw New Exception("12003 - WriteAquariusIncoming" & vbCrLf & LRS(12003))
                                        '12003: Could not find account %1.Please enter the account in setup."
                                    End If

                                    '02.04.2020 - KKIIDD Removed next IF by setting 1 = 2
                                    If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly And 1 = 2 Then
                                        nNoOfTransactionsExported = nNoOfTransactionsExported + 1
                                        nAmountExported = nAmountExported + oPayment.MON_InvoiceAmount

                                        '15.06.2010 - New code - Miroslav Klose
                                        If bUseDayTotals Then
                                            'Add to totalamountexported
                                            'If oPayment.Index = 1 Then
                                            '    MsgBox("Adding an amount to the Array")
                                            'End If
                                            aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oPayment.MON_InvoiceAmount
                                            'MsgBox("Legger til i ArrayKOKO " & oPayment.MON_InvoiceAmount.ToString, MsgBoxStyle.OkOnly)

                                        End If

                                        For Each oInvoice In oPayment.Invoices

                                            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                            '    MsgBox("Calling WriteAquariusOCR, KID = " & oInvoice.Unique_Id & vbCrLf & "Navn: " & oPayment.E_Name)
                                            'End If

                                            bExitLoop = False
                                            sLineToWrite = WriteAquariusOCR(oBatch, oPayment, oInvoice, oClient, oFile, bExitLoop)
                                            '10.09.2008 - the line is now written in the function, because of client 4017
                                            'oFile.WriteLine sLineToWrite

                                            ' 07.03.2019 added next line
                                            bPaymentExported = True

                                            If bExitLoop Then
                                                Exit For
                                            End If

                                        Next oInvoice
                                    Else

                                        nNoOfTransactionsExported = nNoOfTransactionsExported + 1
                                        nAmountExported = nAmountExported + oPayment.MON_InvoiceAmount
                                        'First we have to find the freetext to the payment-lines.
                                        iFreetextCounter = 0
                                        sFreetextFixed = ""
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Original = True Then
                                                For Each oFreeText In oInvoice.Freetexts
                                                    iFreetextCounter = iFreetextCounter + 1
                                                    If iFreetextCounter = 1 Then
                                                        sFreetextFixed = Trim(oFreeText.Text)
                                                    ElseIf iFreetextCounter = 2 Then
                                                        sFreetextFixed = sFreetextFixed & " " & Trim(oFreeText.Text)
                                                    Else
                                                        Exit For
                                                    End If
                                                Next oFreeText
                                            End If
                                        Next oInvoice
                                        'Removed 02042008 - because it was also added later
                                        'sFreetextFixed = Trim$(oPayment.E_Name) & " " & sFreetextFixed
                                        'If Not EmptyString(oPayment.UserID) Then
                                        '    sFreetextFixed = Trim$(oPayment.UserID) & "-" & sFreetextFixed
                                        'End If

                                        'Check if something is posted against the obs-account
                                        '28.02.2008 - New code
                                        bExportAccountNo = True
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                                                    If oInvoice.MATCH_ClientNo = "9999999" Then
                                                        If oInvoice.CustomerNo = "9999999999" Then
                                                            '15.04.2021 - Changed this after instructions from Nordea Finance - Roar Gl�tta among others
                                                            bExportAccountNo = True
                                                            'bExportAccountNo = False
                                                            Exit For
                                                        End If
                                                    End If
                                                ElseIf oInvoice.MATCH_Matched = False Then '24.04.2016 - Changed the elseif. Old code benesth
                                                    'ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    '17.03.2021 - According to Irene the accountno should be exported
                                                    'OLD CODE - bExportAccountNo = False
                                                    bExportAccountNo = True
                                                    Exit For
                                                End If
                                            End If
                                        Next oInvoice
                                        'End new code 28.02.2008

                                        'New code 07.11.2018 - Create a separate file for charges
                                        'The criterias are are as follows:
                                        'Receivers Account = 'EUR' 
                                        'All stated amounts are equal
                                        'Charges are specified
                                        'Not some specific account

                                        'Mark the payments so it is possible to create the correct report for theese payments.

                                        bWriteCharges = False
                                        If sAccountCurrencyFromSetup = "EUR" Then
                                            'If oPayment.MON_InvoiceCurrency = sAccountCurrencyFromSetup Then
                                            If oPayment.MON_InvoiceCurrency = "EUR" Then
                                                If oPayment.MON_TransferCurrency = "EUR" Then
                                                    If oPayment.MON_ChargesCurrency = "EUR" Then
                                                        If oPayment.MON_OriginallyPaidCurrency = "EUR" Then
                                                            If oPayment.MON_InvoiceAmount = oPayment.MON_TransferredAmount Then
                                                                If oPayment.MON_InvoiceAmount = oPayment.MON_OriginallyPaidAmount Then
                                                                    If oPayment.MON_ChargesAmount > 0 Then

                                                                        If Not bChargesFileCreated Then
                                                                            XMLChargesDoc = New System.Xml.XmlDocument

                                                                            ' Load the file which states which accounts to omit from the chargesfile. 
                                                                            sXMLFileName = ""
                                                                            sXMLFileName = System.Configuration.ConfigurationManager.AppSettings("SGF_PathChargesOmit")
                                                                            'XMLChargesDoc.Load(My.Application.Info.DirectoryPath & "\mapping\SGFChargesOmit.xml")
                                                                            If EmptyString(sXMLFileName) Then
                                                                                Throw New Exception("WriteAQIncoming: Det er ikke angitt riktig n�kkel i BabelBank sin konfigurasjonsfil for fil p� avtaler som ikke skal belastes gebyr." & vbCrLf & vbCrLf & "N�kkel skal v�re: " & "SGF_PathChargesOmit")
                                                                            ElseIf Not oFs.FileExists(sXMLFileName) Then
                                                                                Throw New Exception("WriteAQIncoming: Kan ikke finne filen " & sXMLFileName & vbCrLf & " som benyttes til � finne avtaler som ikke skal belastes gebyr.")
                                                                            Else
                                                                                XMLChargesDoc.Load(sXMLFileName)
                                                                            End If

                                                                            'M� hete .csv
                                                                            If sFilenameOut.Contains(".osl") Then
                                                                                sChargesFilenameOut = sFilenameOut.Replace(".osl", ".csv")
                                                                            Else
                                                                                sChargesFilenameOut = sFilenameOut & ".csv"
                                                                            End If
                                                                            oChargesFile = oFs.OpenTextFile(AddTextToFilename(sChargesFilenameOut, "_Charges", True, False), Scripting.IOMode.ForAppending, True, 0)


                                                                            'Create a connection to Aquarius, and find the SQL
                                                                            'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                                                                            aQueryArray = GetERPDBInfo(oBabel.VB_Profile.Company_ID, 5)

                                                                            For lCounter = 0 To UBound(aQueryArray, 3)
                                                                                Select Case UCase(aQueryArray(0, 2, lCounter))
                                                                                    Case "GET_CLIENTSREF"
                                                                                        'SELECT ClientsDebtorReference FROM AgreementDetails_DEB WHERE ClientNumber = '0000464' and Agreementnumber = '002' AND DebtorNumber = '0001975848' and AccountNumber = '001'
                                                                                        'SELECT ClientsDebtorReference FROM AgreementDetails_DEB WHERE ClientNumber = 'BB_ClientNo' and Agreementnumber = 'BB_AgreementNumber' AND DebtorNumber = 'BB_CustomerNo' and AccountNumber = 'BB_AccountNumber'
                                                                                        sMyGetClientsRefSQL = Trim$(aQueryArray(0, 0, lCounter))
                                                                                End Select
                                                                            Next lCounter

                                                                            ''30.04.2019 - moved this code to where detect the client
                                                                            'oMyERPChargesConnection = New vbBabel.DAL
                                                                            'oMyERPChargesConnection.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                                                                            'oMyERPChargesConnection.Company_ID = oBabel.VB_Profile.Company_ID
                                                                            'oMyERPChargesConnection.DBProfile_ID = oClient.DBProfile_ID
                                                                            'If oClient.DBProfile_ID = 99 Then
                                                                            '    oMyERPChargesConnection.DBProfile_ID = 1
                                                                            'Else
                                                                            '    oMyERPChargesConnection.DBProfile_ID = oClient.DBProfile_ID
                                                                            'End If
                                                                            'If Not oMyERPChargesConnection.ConnectToDB() Then
                                                                            '    Throw New System.Exception(oMyERPChargesConnection.ErrorMessage)
                                                                            'Else
                                                                            '    'MsgBox("F�rste kobling mot database er OK.")
                                                                            'End If
                                                                            bChargesFileCreated = True
                                                                        End If
                                                                        bWriteCharges = True
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If

                                        If bWriteCharges Then
                                            'bWriteCharges = False
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Final = True Then
                                                    If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Or oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                                                        bWriteCharges = True
                                                        If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                                                            If oInvoice.MATCH_ClientNo = "9999999" Then
                                                                If oInvoice.CustomerNo = "9999999999" Then
                                                                    bWriteCharges = False
                                                                    oPayment.ExtraD5 = "2"
                                                                End If
                                                            End If
                                                        End If
                                                        If EmptyString(oInvoice.MATCH_ClientNo) Then
                                                            bWriteCharges = False
                                                        End If
                                                        'If oInvoice.MATCH_ClientNo.Trim.Length < 4 Then
                                                        'bWriteCharges = False
                                                        'End If
                                                        If EmptyString(xDelim((oInvoice.MyField), "-", 1)) Then
                                                            bWriteCharges = False
                                                        End If


                                                        If bWriteCharges = True Then
                                                            oPayment.ExtraD5 = ""
                                                            Exit For
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If

                                        'New code 20.02.2019
                                        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
                                            bWriteCharges = False
                                            oPayment.ExtraD5 = "2"
                                        End If

                                        If bWriteCharges Then

                                            sChargesLine = ""
                                            sChargesClientNo = ""
                                            sChargesAccountNumber = ""
                                            sChargesAgreementNumber = ""
                                            sDebtorNumber = ""
                                            sClientReference = ""
                                            sAvtaleType = ""

                                            sChargesClientNo = oInvoice.MATCH_ClientNo.Trim
                                            sChargesAgreementNumber = xDelim((oInvoice.MyField), "-", 1)
                                            sChargesAccountNumber = xDelim((oInvoice.MyField), "-", 3)
                                            sDebtorNumber = oInvoice.CustomerNo.Trim


                                            ' run the sql
                                            sTemp = sMyGetClientsRefSQL
                                            'SELECT ClientsDebtorReference AS Reference FROM AgreementDetails_DEB WHERE ClientNumber = 'BB_ClientNo' AND ServiceAgreementNumber = 'SANumber' AND Debtornumber = 'DNumber' AND AgreementNumber = 'AgNumber'                                            sTemp = Replace(sTemp, "BB_ClientNo", oInvoice.MATCH_ClientNo.Trim, , , vbTextCompare)
                                            sTemp = Replace(sTemp, "AgNumber", sChargesAgreementNumber, , , vbTextCompare)
                                            sTemp = Replace(sTemp, "BB_ClientNo", sChargesClientNo, , , vbTextCompare)
                                            sTemp = Replace(sTemp, "AcNumber", sChargesAccountNumber, , , vbTextCompare)
                                            sTemp = Replace(sTemp, "DNumber", sDebtorNumber, , , vbTextCompare)
                                            oMyERPChargesConnection.SQL = sTemp
                                            If oMyERPChargesConnection.Reader_Execute() Then
                                                If oMyERPChargesConnection.Reader_HasRows Then
                                                    Do While oMyERPChargesConnection.Reader_ReadRecord
                                                        sClientReference = oMyERPChargesConnection.Reader_GetString("ClientsDebtorReference")
                                                        sAvtaleType = oMyERPChargesConnection.Reader_GetString("AgreementTypeBSVCode")
                                                        Exit Do
                                                    Loop
                                                End If
                                            End If

                                            'Check if the postinginfo is stated in the XML among the postings not to be exported
                                            'nodeChargesElement = XMLChargesDoc.SelectSingleNode("/Avtaler")
                                            'nodeChargesElement = XMLChargesDoc.SelectSingleNode("/Avtaler/Avtale/AvtaleType")
                                            'nodeChargesElement = XMLChargesDoc.SelectSingleNode("/Avtaler/Avtale[AvtaleType='0120/0020/5000']/AvtaleType")
                                            nodeChargesElement = XMLChargesDoc.SelectSingleNode("/Avtaler/Avtale[AvtaleType='" & sAvtaleType & "']")
                                            If Not nodeChargesElement Is Nothing Then
                                                'Legges IKKE til fil, men skal p� egen rapport
                                                bWriteCharges = False
                                                oPayment.ExtraD5 = "3"
                                            End If

                                            If bWriteCharges Then
                                                oPayment.ExtraD5 = "1"
                                                sChargesLine = "5042" & ";" 'NoteType - Fixed 5042
                                                sTemp = oInvoice.MATCH_ClientNo.Trim
                                                sChargesLine = sChargesLine & sTemp.Substring(sChargesClientNo.Length - 4, 4) & ";" 'ClientNumber - Kundenummer
                                                sChargesLine = sChargesLine & sChargesAgreementNumber & ";" 'AgreementNumber - Avalenummer
                                                sChargesLine = sChargesLine & "987664398" & ";" 'ClientServiceProvider - Fixed 9 digit organizationno.
                                                sChargesLine = sChargesLine & ConvertFromAmountToString(oPayment.MON_ChargesAmount, , "") & ";" 'Amount
                                                sChargesLine = sChargesLine & oPayment.DATE_Value & ";" 'ValueDate
                                                sChargesLine = sChargesLine & "Bankgebyr " & sClientReference  'Comments, 25 posisjoner. Bankgebyr + Clientsref for debtor

                                                oChargesFile.WriteLine(sChargesLine)
                                            End If


                                            bWriteCharges = False
                                        End If


                                        'End new code 07.11.2018


                                        'First Write an InPayment record
                                        sLineToWrite = WriteInPayment_Record(oBatch, oPayment, oClient, sGLAccount, oBabel, bExportAccountNo)
                                        oFile.WriteLine(sLineToWrite)

                                        '27.04.2018 - Added next line
                                        bPaymentExported = True

                                        'When it is an export payment, just write an in-payment record
                                        bThisIsAnExportPayment = False
                                        If oPayment.ExtraD1 = "EKS" Then
                                            oPayment.Exported = True
                                            bThisIsAnExportPayment = True
                                            'Exit For
                                            '15.06.2010 - New code - Miroslav Klose
                                            If bUseDayTotals Then
                                                'Add to totalamountexported
                                                aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oPayment.MON_InvoiceAmount
                                            End If
                                        End If

                                        If Not bThisIsAnExportPayment Then

                                            'First, create an array consisting on ClientNo, DebtorNo(oInvoice.CustomerNo) which is unique
                                            '  and an amount
                                            '  create one Proposed Allocation record per ClientNo/debtor
                                            'In the same loop put keyed information in to sQ4Text
                                            j = -1
                                            sFreeTextVariable = sFreetextFixed
                                            sQ4Text = ""
                                            ReDim aProposedAllocation(7, 0)

                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount <> 0 Then
                                                    'Add the variable part of the freetext
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        If oFreeText.Qualifier > 2 Then
                                                            If oFreeText.Qualifier = 4 Then
                                                                ' Keyed into sprMatched;
                                                                sQ4Text = sQ4Text & Trim(oFreeText.Text)
                                                            End If
                                                            sFreeTextVariable = Trim(oFreeText.Text) & " " & sFreeTextVariable
                                                        End If
                                                    Next oFreeText


                                                    '                                    'If the invoice is unmatched, add the unmatched customerNo to the invoice
                                                    '                                    If oInvoice.MATCH_Matched = False Then
                                                    '                                        ' NoMatchAccount is like 9999999/001/9999999999
                                                    '                                        oInvoice.MATCH_ClientNo = xDelim(oInvoice.MATCH_ID, "/", 1)
                                                    '                                        oInvoice.CustomerNo = xDelim(oInvoice.MATCH_ID, "/", 3)
                                                    '                                        oInvoice.MyField = xDelim(oInvoice.MATCH_ID, "/", 2) & "-"  'AgreementNumber
                                                    '                                    End If
                                                    '
                                                    '                                    bAddNew = True
                                                    '                                    For i = 0 To UBound(aProposedAllocation(), 2)
                                                    '                                        If oInvoice.CustomerNo = aProposedAllocation(2, i) Then
                                                    '                                            If oInvoice.MATCH_ClientNo = aProposedAllocation(0, i) Then
                                                    '                                                If xDelim(oInvoice.MyField, "-", 1) = aProposedAllocation(1, i) Then
                                                    '                                                    bAddNew = False
                                                    '                                                    Exit For
                                                    '                                                End If
                                                    '                                            End If
                                                    '                                        End If
                                                    '                                    Next i
                                                    '                                    If bAddNew Then
                                                    '                                        j = j + 1
                                                    '                                        ReDim Preserve aProposedAllocation(7, j)
                                                    '                                        aProposedAlloc2ation(0, j) = oInvoice.MATCH_ClientNo
                                                    '                                        aProposedAllocation(1, j) = xDelim(oInvoice.MyField, "-", 1)
                                                    '                                        aProposedAllocation(2, j) = oInvoice.CustomerNo
                                                    '                                        aProposedAllocation(3, j) = Str(oInvoice.MON_InvoiceAmount)
                                                    '                                    Else
                                                    '                                        aProposedAllocation(3, i) = Str(oInvoice.MON_InvoiceAmount + Val(aProposedAllocation(3, i)))
                                                    '                                    End If
                                                End If
                                            Next oInvoice

                                            'Caluculate the 'currency rate' - to be used with calculation
                                            ' of charges and calculation of SAC Amount

                                            'New code 29.04.2008
                                            'Use nFraction in the WriteALLOC-function
                                            nFraction = 1
                                            If oPayment.MON_InvoiceCurrency = oPayment.MON_ChargesCurrency Or EmptyString((oPayment.MON_ChargesCurrency)) Then
                                                nFraction = System.Math.Round((oPayment.MON_InvoiceAmount + oPayment.MON_ChargesAmount) / oPayment.MON_OriginallyPaidAmount, 6)
                                            ElseIf oPayment.MON_OriginallyPaidCurrency = oPayment.MON_ChargesCurrency Then
                                                nFraction = System.Math.Round(oPayment.MON_InvoiceAmount / (oPayment.MON_OriginallyPaidAmount - oPayment.MON_ChargesAmount), 6)
                                            Else
                                                'Not correct but the best we can do
                                                'This situation is theoretical
                                                nFraction = System.Math.Round(oPayment.MON_InvoiceAmount / oPayment.MON_OriginallyPaidAmount, 6)

                                            End If
                                            '28.11.2017 - Added next IF too get correct exchangerate in the file to Aquarius.
                                            'SG Finans had a problem if the invoice is in Norwegian, the customer paid in EUR to a NOK account CASE A
                                            'SG Finans need 
                                            'SG Finans needs the exchangerate to be set to 1.0000 in this case, but it was not before this new IF
                                            If oPayment.MATCH_UseoriginalAmountInMatching And oPayment.MON_InvoiceCurrency <> oPayment.MON_OriginallyPaidCurrency Then
                                                'OK - Keep the calculation above
                                            Else
                                                'CASE A will end here because oPayment.MATCH_UseoriginalAmountInMatching is False, because the match must be done in NOK
                                                nFraction = 1

                                            End If
                                            'Old code
                                            'nFraction = oPayment.MON_InvoiceAmount / oPayment.MON_OriginallyPaidAmount

                                            'Prepare to post charges
                                            nTotalChargesAmount = 0
                                            If oPayment.MATCH_UseoriginalAmountInMatching And oPayment.MON_InvoiceCurrency <> oPayment.MON_OriginallyPaidCurrency Then
                                                'Prepare to post charges, to secure that the currency correspondes with the
                                                '  the amount on th invoices.
                                                If oPayment.MON_OriginallyPaidCurrency <> oPayment.MON_ChargesCurrency Then
                                                    If oPayment.MON_InvoiceCurrency = oPayment.MON_ChargesCurrency Or EmptyString((oPayment.MON_ChargesCurrency)) Then
                                                        'Dim nNetOriginallyPaidAmount As Double = 0

                                                        'nNetOriginallyPaidAmount = System.Math.Round(oPayment.MON_InvoiceAmount / nFraction, 0)
                                                        'New code 16.01.2018
                                                        nTotalChargesAmount = oPayment.MON_OriginallyPaidAmount - (System.Math.Round(oPayment.MON_InvoiceAmount / nFraction, 0))
                                                        'Recalculate the exchangerate
                                                        nFraction = System.Math.Round(oPayment.MON_InvoiceAmount / (oPayment.MON_OriginallyPaidAmount - nTotalChargesAmount), 6)

                                                        'Old code
                                                        'nTotalChargesAmount = System.Math.Round(oPayment.MON_ChargesAmount / nFraction, 0)
                                                    Else
                                                        nTotalChargesAmount = 0
                                                    End If
                                                Else
                                                    nTotalChargesAmount = oPayment.MON_ChargesAmount
                                                End If
                                            Else
                                                'Prepare to post charges
                                                If oPayment.MATCH_UseoriginalAmountInMatching Then
                                                    If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
                                                        oPayment.MATCH_UseoriginalAmountInMatching = False
                                                        nTotalChargesAmount = 0
                                                    Else
                                                        If oPayment.MON_InvoiceCurrency = oPayment.MON_ChargesCurrency Or EmptyString((oPayment.MON_ChargesCurrency)) Then
                                                            nTotalChargesAmount = oPayment.MON_ChargesAmount
                                                        Else
                                                            nTotalChargesAmount = 0
                                                        End If
                                                    End If
                                                Else
                                                    nTotalChargesAmount = 0
                                                End If
                                            End If
                                            bChargesExists = False
                                            If nTotalChargesAmount > 0 Then
                                                'For explaination see function below
                                                'UPGRADE_WARNING: Couldn't resolve default property of object AddPostingOfCharges(oPayment, nTotalChargesAmount, SGAdjCharges). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                If AddPostingOfCharges(oPayment, nTotalChargesAmount, SGAdjCharges) Then
                                                    bChargesExists = True
                                                Else
                                                    '17.01.2018 - New case with OnAccount postings and currency
                                                    'Before this change we posted the net amount of NOK and the gross amount of the currency
                                                    'W enow store the charges in currency, in the (prior unused) variable nChargesAmountToBeUsedInThisALLOC
                                                    nChargesAmountToBeUsedInThisALLOC = 0
                                                    bDeductCharges = True
                                                    If oPayment.MATCH_UseoriginalAmountInMatching And oPayment.MON_InvoiceCurrency <> oPayment.MON_OriginallyPaidCurrency Then
                                                        'Check if all postings are OnAccount
                                                        'If so we add the charges in currency to this variable.
                                                        For Each oInvoice In oPayment.Invoices
                                                            If oInvoice.MATCH_Final Then
                                                                If oInvoice.MATCH_Matched Then
                                                                    If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                                                                        'OK 
                                                                    Else
                                                                        bDeductCharges = False
                                                                    End If
                                                                Else
                                                                    bDeductCharges = False
                                                                End If
                                                            End If
                                                        Next oInvoice
                                                    Else
                                                        bDeductCharges = False
                                                    End If

                                                    If bDeductCharges Then
                                                        nChargesAmountToBeUsedInThisALLOC = nTotalChargesAmount
                                                    End If

                                                    'Old code
                                                    bChargesExists = False
                                                End If
                                            End If

                                            'Check if we have roundings
                                            nTotalRoundings = 0
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Final Then
                                                    If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                        If oInvoice.MATCH_ID = "OREDIFF" Then
                                                            nTotalRoundings = nTotalRoundings + oInvoice.MON_InvoiceAmount
                                                            oInvoice.Qualifier = BabelFiles.InvoiceQualifier.NotToBeExported
                                                        End If
                                                    End If
                                                End If
                                            Next oInvoice
                                            If Not IsEqualAmount(nTotalRoundings, 0) Then
                                                If nTotalRoundings < 0 Then
                                                    If AddPostingOfCharges(oPayment, nTotalRoundings * -1, SGCreditRounding) Then
                                                        bRoundingsExists = True
                                                    Else
                                                        bRoundingsExists = False
                                                    End If
                                                Else
                                                    If AddPostingOfCharges(oPayment, nTotalRoundings * -1, SGDebitRounding) Then
                                                        bRoundingsExists = False 'It is now an regular on-account posting
                                                    Else
                                                        bRoundingsExists = False
                                                    End If
                                                End If
                                            Else
                                                bRoundingsExists = False
                                            End If

                                            If EmptyString(sSGValidateCurrencySQL) Then
                                                'Added 25.04.2019 - to validate accountcurrency against currency in AgreementDetails_CA
                                                'Create a connection to Aquarius, and find the SQL
                                                'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                                                aTempQueryArray = GetERPDBInfo(oBabelFiles.VB_Profile.Company_ID, 5)

                                                For lNewArrayCounter = 0 To UBound(aTempQueryArray, 3)
                                                    Select Case UCase(aTempQueryArray(0, 4, lNewArrayCounter))
                                                        Case "CHECK_AGREEMENTCUR"
                                                            sSGValidateCurrencySQL = Trim$(aTempQueryArray(0, 0, lNewArrayCounter))
                                                    End Select
                                                Next lNewArrayCounter
                                            End If

                                            If EmptyString(sSGValidateCurrencySQL) Then
                                                Err.Raise(15008, "AutoMatching", "Det er ikke angitt en SQL for � validere valuta p� betaling mot AgreementDetail_CA valuta.")
                                            End If

                                            '10.09.2007 - Moved the code from this function (see above), to a seperate function
                                            bErrorExists = False
                                            aProposedAllocation = SGFinansCreateAllocArray(oPayment, sErrorMesage, False, False, True, oMyERPChargesConnection, sSGValidateCurrencySQL, bErrorExists)

                                            If bErrorExists Then
                                                bErrorExists = False
                                                ReDim aProposedAllocation(7, 0)
                                                aProposedAllocation = SGFinansCreateAllocArray(oPayment, sErrorMesage, False, False, True, oMyERPChargesConnection, sSGValidateCurrencySQL, bErrorExists)
                                                If bErrorExists Then
                                                    'New 24.07.2008
                                                    If Not EmptyString(sErrorMesage) Then
                                                        If Not oFile Is Nothing Then
                                                            oFile.Close()
                                                            oFile = Nothing
                                                        End If

                                                        If Not oFs Is Nothing Then
                                                            oFs = Nothing
                                                        End If

                                                        Throw New Exception("28731 - WriteAquariusIncomingPayments" & vbCrLf & "En feil oppstod under eksport!" & vbCrLf & "G� inn i manuell avstemming og poster om f�lgende f�lgende innbetaling." & vbCrLf & vbCrLf & "Betaler: " & oPayment.E_Name & vbCrLf & "Bel�p: " & ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ",") & vbCrLf & vbCrLf & "Melding fra valideringsmodul:" & vbCrLf & sErrorMesage)
                                                    Else
                                                        If Not oFile Is Nothing Then
                                                            oFile.Close()
                                                            oFile = Nothing
                                                        End If

                                                        If Not oFs Is Nothing Then
                                                            oFs = Nothing
                                                        End If

                                                        Throw New Exception("28731 - WriteAquariusIncomingPayments" & vbCrLf & "En feil oppstod under eksport!" & vbCrLf & "G� inn i manuell avstemming og poster om f�lgende f�lgende innbetaling." & vbCrLf & vbCrLf & "Betaler: " & oPayment.E_Name & vbCrLf & "Bel�p: " & ConvertFromAmountToString((oPayment.MON_InvoiceAmount), ".", ","))
                                                    End If
                                                End If
                                            End If


                                            '****                            bChargesWritten = False 'To be used later


                                            '22.08.2007 - Add the AllocatedAmount alias AllocatedAmountSAC to the array
                                            If oPayment.MATCH_UseoriginalAmountInMatching And oPayment.MON_InvoiceCurrency <> oPayment.MON_OriginallyPaidCurrency Then
                                                For i = 0 To UBound(aProposedAllocation, 2)
                                                    aProposedAllocation(7, i) = Trim(Str(System.Math.Round(Val(aProposedAllocation(3, i)) * nFraction, 0)))
                                                    'Test of deviation
                                                    'If i = 2 Then
                                                    '    aProposedAllocation(7, i) = Trim$(Str(Val(aProposedAllocation(7, i)) + 1))
                                                    'End If
                                                Next i

                                                'Check if we have some deviations and change a amount with the difference
                                                nCalculatedAllocatedAmountSAC = 0
                                                For i = 0 To UBound(aProposedAllocation, 2)
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object aProposedAllocation(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    nCalculatedAllocatedAmountSAC = nCalculatedAllocatedAmountSAC + Val(aProposedAllocation(7, i))
                                                Next i
                                                If nCalculatedAllocatedAmountSAC <> oPayment.MON_InvoiceAmount Then
                                                    For i = 0 To UBound(aProposedAllocation, 2)
                                                        If Val(aProposedAllocation(7, i)) <> 0 Then
                                                            aProposedAllocation(7, i) = Trim(Str(Val(aProposedAllocation(7, i)) - (nCalculatedAllocatedAmountSAC - oPayment.MON_InvoiceAmount)))
                                                            Exit For
                                                        Else
                                                            If i = UBound(aProposedAllocation, 2) Then
                                                                aProposedAllocation(7, i) = Trim(Str(Val(aProposedAllocation(7, i)) - (nCalculatedAllocatedAmountSAC - oPayment.MON_InvoiceAmount)))
                                                                Exit For
                                                            End If
                                                        End If
                                                    Next i
                                                End If

                                            Else
                                                'Just add the calculated amount
                                                For i = 0 To UBound(aProposedAllocation, 2)
                                                    aProposedAllocation(7, i) = aProposedAllocation(3, i)
                                                Next i
                                            End If

                                            ''''''                            'New 23.10.2007
                                            ''''''                            If nTotalChargesAmount > 0 Then
                                            ''''''                                nRestChargesAmount = nTotalChargesAmount
                                            ''''''                            Else
                                            ''''''                                nRestChargesAmount = 0
                                            ''''''                                bChargesWritten = True
                                            ''''''                            End If

                                            'Changed 06.06.2007
                                            'To make the note more efficient and to add the userident if the payment is reserved in BB

                                            sFreeTextVariable = ""
                                            If Not EmptyString(sQ4Text) Then
                                                sFreeTextVariable = sQ4Text & " - "
                                            End If
                                            If Not EmptyString((oPayment.UserID)) Then
                                                sFreeTextVariable = sFreeTextVariable & Trim(oPayment.UserID) & " - "
                                            End If
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                sFreeTextVariable = sFreeTextVariable & Trim(oPayment.E_Name) & " - "
                                            End If
                                            If Not EmptyString(sFreetextFixed) Then
                                                sFreeTextVariable = sFreeTextVariable & Trim(sFreetextFixed)
                                            End If
                                            'Remove trailing " - "
                                            If Right(sFreeTextVariable, 3) = " - " Then
                                                sFreeTextVariable = Left(sFreeTextVariable, Len(sFreeTextVariable) - 3)
                                            End If

                                            'Old code
                                            'sFreeTextVariable = sQ4Text & " - " & Replace(oPayment.MATCH_HowMatched, vbCrLf, " - ", , , vbTextCompare) & " - " & oPayment.E_Name & " - " & sFreeTextVariable

                                            ' Cut unreasonable blanks in freetext to present as much as possible;
                                            sFreeTextVariable = Replace(sFreeTextVariable, "   ", "")

                                            'New 12.09.2007 - Remove ' and "
                                            sFreeTextVariable = ReplaceIllegalCharactersInString(sFreeTextVariable, True)

                                            ' 14.05.2008 - Should write note here, so it's written only once for each payment, not per ALLOC as before
                                            ' 31.05.2007 - What will happen to the note on aKonto????
                                            If Not EmptyString(sFreeTextVariable) Then
                                                'Write a noterecord, changed from N to NOTE 25.05.2007
                                                ' Added Innbet;, because no subjectcolumn was present
                                                'Changed 26.03.2008. Max length reduced from 256 to 140
                                                oFile.WriteLine("NOTE;Innbet.;" & Left(sFreeTextVariable, 140))
                                            End If

                                            'Fill up the daytotal array here
                                            '21.04.2017 - New code regarding DAYTOTALS
                                            '15.06.2010 - New code - Miroslav Klose
                                            If bUseDayTotals Then
                                                For Each oInvoice In oPayment.Invoices
                                                    If oInvoice.MATCH_Final Then
                                                        If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                            'Add to totalamountexported
                                                            '09.05.2016 - Don't add this because we get a difference if it is charges (because we match on MON_OriginalAmount
                                                            ' This is illogical and chaotic!
                                                            'aGLAmountArray(lArrayCounter) = aGLAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                        Else
                                                            'Add to totalamountexported
                                                            aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                            'MsgBox("Legger til i ArrayLALA " & oInvoice.MON_InvoiceAmount.ToString, MsgBoxStyle.OkOnly)

                                                        End If
                                                    End If
                                                Next oInvoice
                                            End If


                                            For i = 0 To UBound(aProposedAllocation, 2)

                                                bProposedAllocationRecordWritten = False

                                                'Now traverse through the invoices and export all invoices with clientnos and
                                                '  debternos identical to the in in the array

                                                For Each oInvoice In oPayment.Invoices

                                                    'MsgBox("Kommet til en Invoice")

                                                    If oInvoice.MATCH_Final = True And (oInvoice.MON_InvoiceAmount <> 0 Or oInvoice.MON_DiscountAmount <> 0) Then
                                                        '08.11.2007 - External Collection: The test that oInvoice.MON_InvoiceAmount must be
                                                        '  <> 0 may be altered if we want to post a Ex Col where only interest - tax is paid. No hovedstol is paid.

                                                        'New 03.06.2008
                                                        sTempADAccountNumber = ""
                                                        If EmptyString(xDelim((oInvoice.MyField), "-", 3)) Then
                                                            sTempADAccountNumber = "001"
                                                        Else
                                                            sTempADAccountNumber = Trim(xDelim((oInvoice.MyField), "-", 3))
                                                        End If

                                                        If oInvoice.MATCH_ClientNo = aProposedAllocation(0, i) Then

                                                            If xDelim((oInvoice.MyField), "-", 1) = aProposedAllocation(1, i) Then

                                                                If oInvoice.CustomerNo = aProposedAllocation(2, i) Then

                                                                    If sTempADAccountNumber = aProposedAllocation(8, i) Then

                                                                        'New 27.11.2007 regarding charges
                                                                        If oInvoice.Qualifier <> BabelFiles.InvoiceQualifier.NotToBeExported Then

                                                                            nAmountToUseToAdjustInvoiceAmount = 0

                                                                            If Not bProposedAllocationRecordWritten Then
                                                                                ' 31.05.2007
                                                                                ' Do NOT write RECON for aKonto

                                                                                'This must be wrong. If we have only an onaccount posting on a debtor
                                                                                '   no ALLOC will be ritten therefore:
                                                                                'Changed 17.07.2007
                                                                                'Commented the IF in the next statement


                                                                                'New code 22.08.2007
                                                                                'Don't write an Allocationrecord if it is to the suspence account
                                                                                '   and the amount is zero
                                                                                'No RECONS will be written
                                                                                'UPGRADE_WARNING: Couldn't resolve default property of object aProposedAllocation(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                                                If Not (Val(aProposedAllocation(3, i)) = 0 And oInvoice.MATCH_ID = sNoMatchAccount) Then
                                                                                    'If oInvoice.MATCH_MatchType <> MatchType.MatchedOnCustomer Then
                                                                                    'Write the Proposed Allocation record

                                                                                    'Changed 07.03.2008, no more use of nVoucherNo
                                                                                    sLineToWrite = WriteProposedAllocationRecord(oPayment, oInvoice, oClient, aProposedAllocation(0, i), aProposedAllocation(1, i), aProposedAllocation(2, i), aProposedAllocation(3, i), aProposedAllocation(7, i), nChargesAmountToBeUsedInThisALLOC, nFraction)
                                                                                    '17.01.2018 - Added, see above
                                                                                    nChargesAmountToBeUsedInThisALLOC = 0
                                                                                    'Old code
                                                                                    'sLineToWrite = WriteProposedAllocationRecord(oPayment, oInvoice, oClient, nVoucherNo, aProposedAllocation(0, i), aProposedAllocation(1, i), aProposedAllocation(2, i), aProposedAllocation(3, i), aProposedAllocation(7, i), nChargesAmountToBeUsedInThisALLOC)
                                                                                    oFile.WriteLine(sLineToWrite)
                                                                                    'End If
                                                                                End If

                                                                                '14.05.2008 - Moved code further up so it will be written only once per payment
                                                                                '''''                                                                ' 31.05.2007 - What will happen to the note on aKonto????
                                                                                '''''                                                                If Not EmptyString(sFreeTextVariable) Then
                                                                                '''''                                                                    'Write a noterecord, changed from N to NOTE 25.05.2007
                                                                                '''''                                                                    ' Added Innbet;, because no subjectcolumn was present
                                                                                '''''                                                                    'Changed 26.03.2008. Max length reduced from 256 to 140
                                                                                '''''                                                                    oFile.WriteLine "NOTE;Innbet.;" & Left$(sFreeTextVariable, 140)
                                                                                '''''                                                                End If
                                                                                bProposedAllocationRecordWritten = True

                                                                            End If

                                                                            ' Do NOT write RECON for aKonto
                                                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnCustomer Then
                                                                                'Don't write the Item record if ot is an onaccount posting
                                                                                If Not EmptyString((oInvoice.MATCH_ID)) Then
                                                                                    If oInvoice.MATCH_ID <> sNoMatchAccount Then
                                                                                        'If Not oInvoice.MATCH_MatchType = MatchType.MatchedOnGL Then


                                                                                        '********************************
                                                                                        'WRITE THE INVOICE
                                                                                        '********************************
                                                                                        'New 07.11.2007 - We have to treat external collection payments specially, because we have to
                                                                                        '   adjust the amount on the RECON-record for the matching against the invoice with
                                                                                        '   the interest paid and the VAT
                                                                                        If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice And xDelim((oInvoice.MyField), "-", 5) = "HOVEDSTOL" Then
                                                                                            'Find other invoices in this payment with the same Match_ID (UNIQUEID - LI.Id)
                                                                                            '  UNIQUEID - LI.Id
                                                                                            nTempAmount = 0 'Used to correct the amount of the recon for the invoice, adjust both for interest and mva
                                                                                            nTempAmount2 = 0 'Used to calculate an amount stated as adjustmentamount the recon for the invoice, adjust only for interest
                                                                                            For Each oTempInvoice In oPayment.Invoices
                                                                                                If oTempInvoice.MATCH_ID = oInvoice.MATCH_ID Then
                                                                                                    If oTempInvoice.MATCH_Final Then
                                                                                                        If oTempInvoice.MATCH_MatchType > BabelFiles.MatchType.OpenInvoice Then
                                                                                                            If xDelim(oTempInvoice.MyField, "-", 5) = Trim(Str(SGInkassoInterest)) Then '5040
                                                                                                                nTempAmount = nTempAmount + oTempInvoice.MON_InvoiceAmount
                                                                                                                nTempAmount2 = nTempAmount2 + oTempInvoice.MON_InvoiceAmount
                                                                                                            ElseIf xDelim(oTempInvoice.MyField, "-", 5) = Trim(Str(SGInkassoVAT)) Then  '5042
                                                                                                                nTempAmount = nTempAmount + oTempInvoice.MON_InvoiceAmount
                                                                                                            End If
                                                                                                        End If
                                                                                                    End If
                                                                                                End If
                                                                                            Next oTempInvoice
                                                                                            sLineToWrite = WriteRECONRecord(oPayment, oInvoice, oClient, nTempAmount * -1, nTempAmount2)
                                                                                            oFile.WriteLine(sLineToWrite)
                                                                                        ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Or Not EmptyString(xDelim((oInvoice.MyField), "-", 5)) Then
                                                                                            sLineToWrite = WriteRECONAdjustmentRecord(oPayment, oInvoice, oClient, 0)
                                                                                            oFile.WriteLine(sLineToWrite)
                                                                                        Else
                                                                                            If oInvoice.MON_InvoiceAmount <> 0 Then
                                                                                                sLineToWrite = WriteRECONRecord(oPayment, oInvoice, oClient, nAmountToUseToAdjustInvoiceAmount)
                                                                                                oFile.WriteLine(sLineToWrite)
                                                                                            End If
                                                                                        End If

                                                                                        'Write the discount-adjustment if an discount is present
                                                                                        If Not IsEqualAmount(0, (oInvoice.MON_DiscountAmount)) Then
                                                                                            sLineToWrite = WriteRECONAdjustmentRecord(oPayment, oInvoice, oClient, 0, SGDiscount)
                                                                                            oFile.WriteLine(sLineToWrite)
                                                                                        End If

                                                                                        'End If
                                                                                    End If
                                                                                End If
                                                                            End If 'oInvoice.MATCH_MatchType <> MatchType.MatchedOnCustomer Then


                                                                            ''                                            If oInvoice.MATCH_Matched Then
                                                                            ''                                                If oInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice Then
                                                                            ''
                                                                            ''            '                                    ElseIf oInvoice.MATCH_MatchType = MatchType.OpenInvoice Then
                                                                            ''            '                                        sLineToWrite = WriteKredinor_K90_CustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, nVoucherNo)
                                                                            ''            '                                    ElseIf oInvoice.MATCH_MatchType = MatchType.MatchedOnGL Then
                                                                            ''            '                                        sLineToWrite = WriteKredinor_K90_GLRecord(oPayment, oInvoice, oClient, sFreeTextVariable, nVoucherNo)
                                                                            ''                                                Else
                                                                            ''                                                    sLineToWrite = WriteKredinor_K90_NotMatchedRecord(oPayment, oInvoice, oClient, sFreeTextVariable, nVoucherNo, oaccount)
                                                                            ''                                                End If
                                                                            ''                                            Else
                                                                            ''                                                sLineToWrite = WriteKredinor_K90_NotMatchedRecord(oPayment, oInvoice, oClient, sFreeTextVariable, nVoucherNo, oaccount)
                                                                            ''                                            End If

                                                                        End If 'if oinvoice.Qualifier <> invoicequalifier.NotToBeExported then

                                                                    End If 'If sTempADAccountNumber = aProposedAllocation(8, i) Then

                                                                End If 'If oInvoice.CustomerNo = aProposedAllocation(2, i) Then

                                                            End If 'If xDelim(oInvoice.MyField, "-", 1) = aProposedAllocation(1, i) Then

                                                        End If 'If oInvoice.MATCH_ClientNo = aProposedAllocation(0, i) Then

                                                    End If 'If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount <> 0 Then

                                                Next oInvoice
                                            Next i 'For i = 0 To UBound(aProposedAllocation(), 2)

                                        End If 'If not bThisIsAnExportPayment

                                    End If 'If eTypeofRecords <> TypeofRecords.OCRonly Then

                                    oPayment.Exported = True

                                    If bChargesExists Then
                                        RemoveTemporaryInvoices(oPayment)
                                    End If

                                End If 'bExportoPayment
                            Next oPayment ' payment

                            'We have to save the last used sequenceno. in some cases
                            'First check if we use a profile

                            'Changed 07.03.2008, no more use of nVoucherNo
                            ''                If oBatch.VB_ProfileInUse = True Then
                            ''                    oClient.VoucherNo = Trim$(Str(nVoucherNo))
                            ''                    oClient.Status = Changed
                            ''                    oBatch.VB_Profile.Status = ChangeUnder
                            ''                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = ChangeUnder
                            ''
                            ''                End If
                        End If ' bExportoBatch Then
                    Next oBatch 'batch

                End If

            Next oBabel

            '04.12.2008 - New added next IF
            'MsgBox("Skal oppdatere Day totals")
            If bUseDayTotals Then
                For lCounter = 0 To UBound(aAccountArray, 2)
                    '02.04.2020 - KKIIDD Removed next IF
                    'If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                    ''MsgBox("Skal oppdatere bel�pet: " & aAmountArray(lCounter).ToString & " p� konto " & aAccountArray(1, lCounter))
                    'If Not IsEqualAmount(aAmountArray(lCounter), 0) Then
                    'dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "OCR", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                    'End If
                    'Else
                    'MsgBox("Skal oppdatere Daytotals", MsgBoxStyle.OkOnly)
                    If Not IsEqualAmount(aAmountArray(lCounter), 0) Then
                        'MsgBox("Skal oppdatere MATCHED_BB", MsgBoxStyle.OkOnly)
                        dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "MATCHED_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                    End If
                    'MsgBox("Ferdig med MATCHED_BB", MsgBoxStyle.OkOnly)
                    If Not IsEqualAmount(aGLAmountArray(lCounter), 0) Then
                        'MsgBox("Skal oppdatere MATCHED_GL", MsgBoxStyle.OkOnly)
                        dbUpDayTotals(False, DateToString(Now), aGLAmountArray(lCounter), "MATCHED_GL", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                    Else
                        'MsgBox("Skal IKKE oppdatere MATCHED_GL", MsgBoxStyle.OkOnly)
                    End If
                    'MsgBox("Ferdig med oppdatering av Daytotals", MsgBoxStyle.OkOnly)

                    'End If
                Next lCounter
            End If

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If bChargesFileCreated Then
                If Not oChargesFile Is Nothing Then
                    oChargesFile.Close()
                    oChargesFile = Nothing
                End If
            End If

            '27.04.2018 - Added next IF to delete empty files, which SG Finans experience at the moment.
            If Not bPaymentExported Then
                If bFileInitiated Then
                    If Not oFs Is Nothing Then
                        oFs.DeleteFile((sFilenameOut))
                    End If
                End If
                If bChargesFileCreated Then
                    If Not XMLChargesDoc Is Nothing Then
                        XMLChargesDoc = Nothing
                    End If
                    If Not oMyERPChargesConnection Is Nothing Then
                        oMyERPChargesConnection = Nothing
                    End If
                    If Not oFs Is Nothing Then
                        oFs.DeleteFile(sChargesFilenameOut)
                    End If
                End If
            End If


            'Dim iResponse As Integer
            'iResponse = MsgBox("Filen " & sFilenameOut & " er eksportert." & vbCrLf & "�nsker du � avbryte?", vbYesNo, "Fil eksportert")
            'If iResponse = vbYes Then
            '    Throw New Exception("Da avbryter vi dette!")
            'Else
            '    'Continue
            'End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteAquariusIncomingPayments" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally
            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

            If Not XMLChargesDoc Is Nothing Then
                XMLChargesDoc = Nothing
            End If
            If Not oMyERPChargesConnection Is Nothing Then
                oMyERPChargesConnection = Nothing
            End If

        End Try

        WriteAquariusIncomingPayments = True

    End Function
    Private Function WriteInPayment_Record(ByRef oBatch As vbBabel.Batch, ByRef oPayment As vbBabel.Payment, ByRef oClient As vbBabel.Client, ByRef sGLAccount As String, ByVal oBabelFile As vbBabel.BabelFile, Optional ByRef bExportAccountNo As Boolean = True) As String
        Dim sLineToWrite As String
        Dim lNumericPart As Long
        Dim sAlphaPart As String

        sLineToWrite = ""
        '1   RecordType
        sLineToWrite = sLineToWrite & "PAYMENT" & ";"
        '2   Amount
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",") & ";"
        '3   HEADER BANK ACCOUNT(sg finans)
        '25.02.2005 - Must use converted account (which is passed as sI_Account if stated) for accounts in Sweden.
        If EmptyString(sGLAccount) Then
            sLineToWrite = sLineToWrite & oPayment.I_Account & ";"
        Else
            sLineToWrite = sLineToWrite & sGLAccount & ";"
        End If
        '4   HEADER BANK ACCOUNT SORT CODE
        sLineToWrite = sLineToWrite & ";"
        '5   DateReceived
        sLineToWrite = sLineToWrite & VB6.Format(Now, "YYYYMMDD") & ";"
        '6   Currency
        If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";"
        Else
            sLineToWrite = sLineToWrite & "NOK;"
        End If
        '7   PAYTYPE
        ' 5001 = Innbetaling
        ' 5004 = Indirect payment
        ' 5005 = Inkasso payment
        If oPayment.ExtraD1 = "DIR" Then
            sLineToWrite = sLineToWrite & "5004;"
        ElseIf oPayment.ExtraD1 = "INK" Then
            sLineToWrite = sLineToWrite & "5005;"
        Else
            sLineToWrite = sLineToWrite & "5001;"
        End If
        ' 25.06.2007 - Changed content from field 10 to 8, and vice versa
        '8   ExternalReferenceNumber
        If EmptyString(oPayment.REF_Bank1) Then
            ' 29.06.2007 Added test for empty bankreference, so we can fill in with 0
            sLineToWrite = sLineToWrite & "0;"
        Else
            sLineToWrite = sLineToWrite & Replace(oPayment.REF_Bank1, ";", ",", , , vbTextCompare) & ";"
        End If
        '9   ImageReferenceNumber
        sLineToWrite = sLineToWrite & oBatch.REF_Bank & ";"
        ' 25.06.2007 - Changed content from field 10 to 8, and vice versa
        '10   ReceiptNumber
        If IsOCR(oPayment.PayCode) Then '28.04.2020 - Added this IF
            If Not EmptyString(oPayment.ExtraD2) Then
                sLineToWrite = sLineToWrite & oPayment.ExtraD2.Trim & ";"
            Else
                sLineToWrite = sLineToWrite & "KID" & Trim(oPayment.Unique_PaymentID) & ";"
            End If
        Else
            sLineToWrite = sLineToWrite & "OSL" & Trim(oPayment.Unique_PaymentID) & ";" '03.04.2017 - Changed from Voucher no to OSL + Unique_PaymentID
        End If
        '11  ValueDate
        sLineToWrite = sLineToWrite & VB6.Format(StringToDate(oPayment.DATE_Value), "YYYYMMDD") & ";" ' 25.05.2007 added ";"
        '12  BANK ACCOUNT(debtor)
        'New 10.03.1006, next 3 IF-sentences

        If oPayment.PayType = "D" Then
            'New 15.08.2008 to prevent updating accountno on observationaccount
            'Changed by Irene Alstad 03.03.2021 - They want payer's accountno stated on the observationaccount
            '    Commented next If .. Else .. End If
            'If oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then
            If Len(oPayment.E_Account) = 11 Then
                If Mid(oPayment.E_Account, 5, 1) <> "9" Then
                    'New 25.06.2007 Don't write accountno for Inkasso and Direct Payments
                    If oPayment.ExtraD1 = "DIR" Then
                        sLineToWrite = sLineToWrite & ";"
                    Else
                        If oPayment.ExtraD1 = "INK" Then
                            sLineToWrite = sLineToWrite & ";"
                        Else
                            'New IF 28.02.2008
                            If bExportAccountNo Then
                                'sLineToWrite = sLineToWrite & ";"
                                'Should also remove accountno. for transactions paid from a SG-account.
                                Select Case oPayment.E_Account

                                    Case "81010787661"
                                        sLineToWrite = sLineToWrite & ";"
                                    Case Else
                                        sLineToWrite = sLineToWrite & oPayment.E_Account & ";"
                                End Select
                            Else
                                sLineToWrite = sLineToWrite & ";"
                            End If
                        End If
                    End If
                Else
                    sLineToWrite = sLineToWrite & ";"
                End If
            Else
                sLineToWrite = sLineToWrite & ";"
            End If
            'Else
            'sLineToWrite = sLineToWrite & ";"
            'End If
        Else
            'New 28.02.2008
            'Don't update accountno
            sLineToWrite = sLineToWrite & ";"

            '    'New 15.08.2008 to prevent updating accountno on observationaccount
            '    If oPayment.MATCH_Matched = MatchStatus.Matched Then
            '        If Len(oPayment.E_Account) > 5 Then
            '            sLineToWrite = sLineToWrite & oPayment.E_Account & ";"
            '        Else
            '            sLineToWrite = sLineToWrite & ";"
            '        End If
            '    Else
            '        sLineToWrite = sLineToWrite & ";"
            '    End If
        End If
        '13  BANK CODE
        sLineToWrite = sLineToWrite & ";"

        ' Added field 14 30.05.2007
        '14 If inkasso payment, then set "true", otherwise "false"
        If oPayment.ExtraD1 = "INK" Then
            sLineToWrite = sLineToWrite & "True"
        Else
            sLineToWrite = sLineToWrite & "False"
        End If

        'New 23.04.2008
        '15   BatchNumber
        sLineToWrite = sLineToWrite & ";" & oBabelFile.EDI_MessageNo & PadLeft(oBatch.Index.ToString, 3, "0")
        'Old code
        'If FindNumericPartOfString(Trim(xDelim(oPayment.VoucherNo, "-", 1)), lNumericPart, sAlphaPart) Then
        '    sLineToWrite = sLineToWrite & ";" & Left(Trim(CStr(lNumericPart)), 7)
        'Else
        '    sLineToWrite = sLineToWrite & ";" & Left(Trim(xDelim(oPayment.VoucherNo, "-", 1)), 7)
        'End If

        WriteInPayment_Record = sLineToWrite

    End Function
    Private Function WriteProposedAllocationRecord(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef oClient As vbBabel.Client, ByRef sSGClientNo As String, ByRef sAgreementNumber As String, ByRef sSGDebtorNo As String, ByRef sAmount As String, ByRef sSACAmount As String, ByRef nChargesAmount As Double, ByRef nFraction As Double) As String
        'Changed 07.03.2008, no more use of nVoucherNo
        'Old
        'Private Function WriteProposedAllocationRecord(oPayment As Payment, oInvoice As Invoice, oClient As vbbabel.Client, nVoucherNo As Double, sSGClientNo As String, sAgreementNumber As String, sSGDebtorNo As String, sAmount As String, sSACAmount As String, nChargesAmount As Double) As String
        Dim sLineToWrite As String
        Dim sTmp As String
        Dim sFraction As String
        Dim iZeroesToAdd As Short
        Dim iCounter As Short


        'Check if we have a negative allocation amount
        If Val(sAmount) - nChargesAmount < 0 Then
            Throw New Exception("41829 - WriteProposedAllocationRecord" & vbCrLf & "BabelBank is about to write an negative allocation. This is not allowed!" & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(Val(CStr(oPayment.MON_InvoiceAmount))) & vbCrLf & "Engasjementsnummer: " & sSGDebtorNo & vbCrLf & vbCrLf & "Vennligst g� inn p� manuell og omposter denne betalingen.")
        End If


        sLineToWrite = ""
        '1   RecordType
        sLineToWrite = sLineToWrite & "ALLOC" & ";"
        '2   ClientNumber - AD.ClientNumber, use a dummy clientno. for suspence 9999999
        sLineToWrite = sLineToWrite & sSGClientNo & ";"
        '3   AgreementNumber - AD.AgreementNumber - What is the AD.AgreementNumber for unmatched items?
        sLineToWrite = sLineToWrite & sAgreementNumber & ";"
        'sLineToWrite = sLineToWrite & xDelim(oInvoice.MyField, "-", 1) & ";"
        '4   DebtorNumber - AD.DebtorNumber9999999999 - Suspence
        sLineToWrite = sLineToWrite & sSGDebtorNo & ";"
        '5   AccountNumber - AD.AccountNumber, If not present, default 001
        sTmp = xDelim((oInvoice.MyField), "-", 3)
        If Not EmptyString(sTmp) And sTmp <> "N/A" Then
            sLineToWrite = sLineToWrite & sTmp & ";"
        Else
            sLineToWrite = sLineToWrite & "001;"
        End If
        '6   AllocatedAmount - Paid amount (oPayment.MON_InvoiceAmount)Must be splitted per debtor/client.
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(Val(sSACAmount), , ",") & ";"
        '7   AllocatedAmountDAC - Paid amount (oPayment.MON_InvoiceAmount)Must be splitted per debtor/client.
        'sLineToWrite = sLineToWrite & ConvertFromAmountToString(Val(sAmount), , ",") & ";"   ' 25.05.2007, added ";"
        'New 23.10.2007 - Adjust the ALLOC with charges
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(Val(sAmount) - nChargesAmount, , ",") & ";"
        '8   AllocatedAmountSAC - Paid amount (oPayment.MON_InvoiceAmount)Must be splitted per debtor/client.
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(Val(sSACAmount), , ",") & ";" ' 25.05.2007 added ";"
        ' 25.05.2007
        ' Added Currencyrate x 2
        If nFraction = 1 Or oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then

            sLineToWrite = sLineToWrite & "1,000000" & ";" 'Trasaction to SAC rate
            sLineToWrite = sLineToWrite & "1,000000" 'Transaction to DAC rate
        Else
            sFraction = Str(nFraction)
            sFraction = Trim(Replace(sFraction, ".", ","))
            If Left(sFraction, 1) = "," Then
                'Must be 0,8494 not ,8494
                sFraction = "0" & sFraction
            End If

            If InStr(1, sFraction, ",", CompareMethod.Text) > 0 Then
                iZeroesToAdd = System.Math.Abs(Len(sFraction) - InStr(1, sFraction, ",", CompareMethod.Text) - 6)
            Else
                sFraction = sFraction & "."
                iZeroesToAdd = 6
            End If
            For iCounter = 1 To iZeroesToAdd
                sFraction = sFraction & "0"
            Next iCounter

            sLineToWrite = sLineToWrite & sFraction & ";" 'Trasaction to SAC rate
            sLineToWrite = sLineToWrite & sFraction 'Transaction to DAC rate
        End If

        WriteProposedAllocationRecord = sLineToWrite

    End Function
    Private Function WriteRECONRecord(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef oClient As vbBabel.Client, ByRef nAmountToUseToAdjustInvoiceAmount As Double, Optional ByRef nAdjustmentAmount As Double = 0) As String
        Dim sLineToWrite As String
        Dim bAdjustment As Boolean

        sLineToWrite = ""
        '1   RecordType -Fixed   'RECON'
        sLineToWrite = sLineToWrite & "RECON" & ";"
        '2   NewItem
        sLineToWrite = sLineToWrite & "N" & ";"
        '3   DocumentNumber - LI.DocumentNumber, also used when creating an adjustment (i.e. discount). Must create an adjustment per invoice, and sort it.
        sLineToWrite = sLineToWrite & oInvoice.InvoiceNo & ";"
        '4   UNIQUEID - LI.Id
        If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
            sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
        Else
            sLineToWrite = sLineToWrite & ";"
        End If
        '5   RECON AMOUNT - Paid amount ''????(oPayment.MON_InvoiceAmount)0 if it is an adjustment
        'New code 07.11.2007 - Used for external collection, where we have to adjust the stated invoiceamount
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount - nAmountToUseToAdjustInvoiceAmount, , ",") & ";"
        'old code
        'sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, , ",") & ";"
        '6   AdjustmentAmount - 0 if an invoice, else adjustment amount ??
        '28.11.2008 - For inkassopaymenta we have to state the interestamount in the adjustmentfield (for some strange reason)
        If nAdjustmentAmount = 0 Then
            sLineToWrite = sLineToWrite & "0;"
        Else
            sLineToWrite = sLineToWrite & ConvertFromAmountToString(nAdjustmentAmount, , ",") & ";"
        End If
        'Old code
        'sLineToWrite = sLineToWrite & "0;"
        '7   AdjustmentCode - ???? - Use a conversion table, to show what kind of adjustment it is.
        sLineToWrite = sLineToWrite & ";"
        '8   KID
        sLineToWrite = sLineToWrite & ";"
        '9   AdjustmentDate
        sLineToWrite = sLineToWrite '


        'New fields - not implemented yet
        ' Used for adjustments, charges and maybe discount
        'If oInvoice.MATCH_MatchType = MatchType.MatchedOnGL Then
        '    '9   reconciliationIsNewAdjustment
        '    sLineToWrite = sLineToWrite & "Y;"
        '    '10   reconciliationAdjustmentCode
        '    sLineToWrite = sLineToWrite & "????;"
        '    '10   reconciliationAdjustmentAmount
        '    sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, , ",") & ";"
        'Else
        '    '9   reconciliationIsNewAdjustment
        '    sLineToWrite = sLineToWrite & "N;"
        '    '10   reconciliationAdjustmentCode
        '    sLineToWrite = sLineToWrite & ";"
        '    '10   reconciliationAdjustmentAmount
        '    sLineToWrite = sLineToWrite & "0,00;"
        'End If


        WriteRECONRecord = sLineToWrite

    End Function
    Private Function WriteRECONAdjustmentRecord(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef oClient As vbBabel.Client, ByRef nChargesAmount As Double, Optional ByRef nAdjustmentCode As Short = 0) As String
        Dim sLineToWrite As String
        Dim bCharges As Boolean
        Dim nAmountToUse As Double
        Dim sAdjustmentCode As String
        Dim bWriteUniqueID As Boolean
        'New 07.12.2007 - Set to true for all adjustmenttypes except for SGInkassoVAT
        Dim bWriteAdjustmentDate As Boolean

        'The adjustmentcode is either passed to the function or is found in the 5th posistion in the MyField
        If nAdjustmentCode <> 0 Then
            sAdjustmentCode = Trim(Str(nAdjustmentCode))
        Else
            sAdjustmentCode = xDelim((oInvoice.MyField), "-", 5)
        End If

        Select Case Val(sAdjustmentCode)

            Case SGAdjCharges
                nAmountToUse = oInvoice.MON_InvoiceAmount
                bWriteUniqueID = True
                bWriteAdjustmentDate = True

            Case SGCreditNoteUsedFromAnotherDebtorAccount
                nAmountToUse = oInvoice.MON_InvoiceAmount
                bWriteUniqueID = True
                bWriteAdjustmentDate = True

            Case SGCreditNoteTakenByDebtorForAnotherAccount
                nAmountToUse = oInvoice.MON_InvoiceAmount
                bWriteUniqueID = True '??
                bWriteAdjustmentDate = True

            Case SGCreditRounding
                nAmountToUse = oInvoice.MON_InvoiceAmount
                bWriteUniqueID = True
                bWriteAdjustmentDate = True

            Case SGDebitRounding
                nAmountToUse = oInvoice.MON_InvoiceAmount
                bWriteUniqueID = True
                bWriteAdjustmentDate = True

            Case SGInkassoInterest
                nAmountToUse = oInvoice.MON_InvoiceAmount
                '06.10.2008 - Changed to True according to instructions from HPD
                bWriteUniqueID = True
                'Old code
                'bWriteUniqueID = False
                bWriteAdjustmentDate = True
                oInvoice.InvoiceNo = "REN" & Trim(oInvoice.InvoiceNo)

            Case SGInkassoVAT
                nAmountToUse = oInvoice.MON_InvoiceAmount
                bWriteUniqueID = True
                bWriteAdjustmentDate = False
                oInvoice.InvoiceNo = "MVA" & Trim(oInvoice.InvoiceNo)

            Case SGDiscount
                'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.MON_DiscountAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nAmountToUse = oInvoice.MON_DiscountAmount
                bWriteUniqueID = True
                bWriteAdjustmentDate = True

        End Select

        'If IsEqualAmount(nChargesAmount, 0) Then
        '    bCharges = False
        '    nAmountToUse = oInvoice.MON_InvoiceAmount
        '    sAdjustmentCode = xDelim(oInvoice.MyField, "-", 5)
        'Else
        '    bCharges = True
        '    nAmountToUse = nChargesAmount * -1
        '    sAdjustmentCode = "5017"
        'End If

        sLineToWrite = ""
        '1   RecordType -Fixed   'RECON'
        sLineToWrite = sLineToWrite & "RECON" & ";"
        '2   NewItem
        sLineToWrite = sLineToWrite & "Y" & ";"
        '3   DocumentNumber - LI.DocumentNumber, also used when creating an adjustment (i.e. discount). Must create an adjustment per invoice, and sort it.
        sLineToWrite = sLineToWrite & oInvoice.InvoiceNo & ";"
        '4   UNIQUEID - LI.Id
        If bWriteUniqueID Then
            If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
                sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
            ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL And nChargesAmount = 0 Then  'For adjustments to take use of a creditnote from another customer
                sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
            Else
                sLineToWrite = sLineToWrite & ";"
            End If
        Else
            sLineToWrite = sLineToWrite & ";"
        End If
        '5   RECON AMOUNT - Paid amount ''????(oPayment.MON_InvoiceAmount)0 if it is an adjustment
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(nAmountToUse, , ",") & ";"
        '6   AdjustmentAmount - 0 if an invoice, else adjustment amount ??
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(nAmountToUse, , ",") & ";"
        '7   AdjustmentCode - ???? - Use a conversion table, to show what kind of adjustment it is.
        sLineToWrite = sLineToWrite & sAdjustmentCode & ";"
        '8   KID
        sLineToWrite = sLineToWrite & ";"
        '9   AdjustmentDate
        If bWriteAdjustmentDate Then
            sLineToWrite = sLineToWrite & VB6.Format(StringToDate((oPayment.DATE_Value)), "YYYYMMDD")
        End If

        WriteRECONAdjustmentRecord = sLineToWrite

    End Function

    Private Function WriteAquariusOCR(ByRef oBatch As Batch, ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef oClient As vbBabel.Client, ByRef oFile As Scripting.TextStream, ByRef bExitLoop As Boolean) As String
        Dim sLineToWrite As String
        Dim sKID As String
        Dim bContinue As Boolean

        bContinue = True

        bExitLoop = False

        ' Col 1
        sLineToWrite = "PAYMENT" & ";" 'Recordtype

        sKID = Trim(oInvoice.Unique_Id)

        If 1 = 2 Then

            ' XOKNET 10.10.2015
            ' 10.10.2015
            ' Ref Morten Nissen Lie, SG
            ' for KIDs starting with 940170 and 91004 will need to cut from 25 to 16, and then send to the old 16-kid-routine;
            If Left(sKID, 5) = "91004" Or Left(sKID, 5) = "94017" Then
                ' 09.11.2015 Noen er allerede p� 16 pos, ikke kutt disse
                If Len(sKID) <> 16 Then
                    sKID = Left$(sKID, 5) & Mid$(sKID, 15, 10)
                    sKID = sKID & AddCDV10digit(sKID)
                End If
            End If

            Select Case Len(sKID)
                Case 15 'Remove first digit in invoice number, and add 00 after the invoice number and calculate the cdv digit
                    sKID = Left(sKID, 5) & Mid(sKID, 7, 8) & "00"
                    sKID = sKID & AddCDV10digit(sKID)
                Case 16 'Nothing to do
                    ' XOKNET 05.11.2015 sKID already set some lines abovce
                    'sKID = Trim$(oInvoice.Unique_Id)

                    'New code12.02.2008
                    'To treat some KIDs that deviates from the standard
                    If Left(sKID, 7) = "0139166" Then
                        '9166 - Cargo Partner
                        '0139166014223400 - 9916600422340000
                        sKID = "99166" & PadLeft(Mid(sKID, 10, 6), 8, "0") & "00"
                        sKID = sKID & AddCDV10digit(sKID)
                        '23.10.2009 - From after instrudtions from Morten Nissen-Lie
                    ElseIf Left(sKID, 7) = "9129099" Then
                        'Old code - ElseIf Left$(sKID, 5) = "91290" Then
                        '1290 - IFCO
                        '9129099050297745 - 9129099050297000
                        sKID = Left(sKID, 13) & "00"
                        sKID = sKID & AddCDV10digit(sKID)
                    ElseIf Left(sKID, 7) = "0109327" Then
                        '1327 - Strandco
                        '0109327491670003 - 9132700491670000
                        sKID = "91327000" & Mid(sKID, 8, 5) & "00"
                        sKID = sKID & AddCDV10digit(sKID)
                        '28.20.2009 - Changed after instructions from Roar Gl�tta. The KID may also start with "1"
                    ElseIf Left(sKID, 1) = "0" Or Left(sKID, 1) = "1" Then
                        'Old code
                        'ElseIf Left$(sKID, 1) = "0" Then
                        If Mid(sKID, 6, 4) = "4779" Then
                            '4779 - New Phone
                            '0394947790954305 - 9477900095430000
                            sKID = "94779" & PadLeft(Mid(sKID, 10, 6), 8, "0") & "00"
                            sKID = sKID & AddCDV10digit(sKID)
                        End If
                        'ElseIf Left$(sKID, 6) = "940170" Then 'Special for Kristiansand. The code will be moved somewhere else
                        ' XOKNET 10.10.2015 added 91004
                    ElseIf Left$(sKID, 6) = "940170" Or Left$(sKID, 5) = "91004" Then 'Special for Kristiansand. The code will be moved somewhere else
                        ' XOKNET 05.11.2015 - her m� vi ogs� gj�re om fra 25 til 16 KID for basisinnbetaling
                        oInvoice.Unique_Id = sKID
                        'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                        '    MsgBox("Calling SplitKIDperInvoiceForSG, KID = " & oInvoice.Unique_Id)
                        'End If

                        If SplitKIDperInvoiceForSG(oInvoice.Unique_Id, oPayment) Then
                            bExitLoop = True 'Used to prevent raversing through the newly added invoices 
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final And oInvoice.MATCH_Matched Then
                                    sLineToWrite = "PAYMENT" & ";" 'Recordtype
                                    sKID = Trim(oInvoice.Unique_Id)
                                    ' Col 2
                                    sLineToWrite = sLineToWrite & sKID & ";" 'KID
                                    ' Col 3
                                    sLineToWrite = sLineToWrite & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), , ",") & ";" 'Amount
                                    ' Col 4
                                    sLineToWrite = sLineToWrite & "NOK;" 'Currency
                                    ' Col 5
                                    sLineToWrite = sLineToWrite & VB6.Format(Now, "YYYYMMDD") & ";" 'Date received
                                    ' Col 6
                                    sLineToWrite = sLineToWrite & Trim(oPayment.REF_Bank1) & ";" 'receiptnumber
                                    ' Col 7
                                    sLineToWrite = sLineToWrite & Trim(oBatch.REF_Bank) & ";" 'imagereferencenumber
                                    ' Col 8
                                    sLineToWrite = sLineToWrite & VB6.Format(StringToDate((oPayment.DATE_Value)), "YYYYMMDD") & ";" 'Valuedate
                                    ' Col 9
                                    If Mid(Trim(oPayment.E_Account), 5, 1) <> "9" Then
                                        If Len(Trim(oPayment.E_Account)) = 11 Then
                                            sLineToWrite = sLineToWrite & Trim(oPayment.E_Account) & ";" 'Debtors abnkaccount
                                        Else
                                            sLineToWrite = sLineToWrite & ";"
                                        End If
                                    Else
                                        sLineToWrite = sLineToWrite & ";"
                                    End If
                                    ' Col 10
                                    sLineToWrite = sLineToWrite & ";" 'Bankcode
                                    ' Col 11
                                    sLineToWrite = sLineToWrite & Trim(oPayment.I_Account) & ";" 'Header bankno(receiver)
                                    ' Col 12
                                    sLineToWrite = sLineToWrite & ";" 'Header bank sortdcode (receiver)
                                    oFile.WriteLine(sLineToWrite)

                                End If
                            Next oInvoice

                            bContinue = False
                        Else
                            bContinue = True
                        End If

                    End If
                Case 25 'Remove debtornumber and recalculate the cdv digit
                    sKID = Left(sKID, 5) & Mid(sKID, 15, 10)
                    sKID = sKID & AddCDV10digit(sKID)
                Case Else
                    'Err.Raise 77777, "Wr_AquariusOCR", "Ugyldig lengde p� KID" & vbCrLf & "KID: " & Trim$(oInvoice.Unique_Id)
            End Select

        End If

        If bContinue Then
            ' Col 2
            sLineToWrite = sLineToWrite & sKID & ";" 'KID
            ' Col 3
            sLineToWrite = sLineToWrite & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), , ",") & ";" 'Amount
            ' Col 4
            'XokNET - 25.02.2014 - Added next IF
            If oPayment.MON_InvoiceCurrency = "SEK" Then
                sLineToWrite = sLineToWrite & "SEK;" 'Currency
            Else
                sLineToWrite = sLineToWrite & "NOK;" 'Currency
            End If

            ' Col 5
            sLineToWrite = sLineToWrite & VB6.Format(Now, "YYYYMMDD") & ";" 'Date received
            ' Col 6
            sLineToWrite = sLineToWrite & Trim(oPayment.REF_Bank1) & ";" 'receiptnumber
            ' Col 7
            sLineToWrite = sLineToWrite & Trim(oBatch.REF_Bank) & ";" 'imagereferencenumber
            ' Col 8
            sLineToWrite = sLineToWrite & VB6.Format(StringToDate((oPayment.DATE_Value)), "YYYYMMDD") & ";" 'Valuedate
            ' Col 9
            If Mid(Trim(oPayment.E_Account), 5, 1) <> "9" Then
                If Len(Trim(oPayment.E_Account)) = 11 Then
                    sLineToWrite = sLineToWrite & Trim(oPayment.E_Account) & ";" 'Debtors abnkaccount
                Else
                    sLineToWrite = sLineToWrite & ";"
                End If
            Else
                sLineToWrite = sLineToWrite & ";"
            End If
            ' Col 10
            sLineToWrite = sLineToWrite & ";" 'Bankcode
            ' Col 11
            sLineToWrite = sLineToWrite & Trim(oPayment.I_Account) & ";" 'Header bankno(receiver)
            ' Col 12
            sLineToWrite = sLineToWrite & ";" 'Header bank sortdcode (receiver)
            oFile.WriteLine(sLineToWrite)
        End If


        WriteAquariusOCR = sLineToWrite

    End Function
    Function WriteAquariusIncomingOCR(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef nNoOfTransactionsExported As Double, ByRef nAmountExported As Double) As Object
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        'Dim op As Payment
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim bKeepOppdragsNr, bFoundContractNo As Boolean
        Dim sContractNo As String
        Dim sGroupNumber As String 'Used to create a false, Sentral-ID + Dagkode +
        ' Delavregningsnr + l�penummer (for grouping on the bankstatement).
        Dim bFBOsExist, bBatchStartWritten As Boolean
        Dim nFBOTransactionNo As Double
        Dim sFBOFileName As String
        Dim iStartPosFilename As Short

        Try

            ' new 19.12.02, due to problems running to OCR-export connescutive (without leaving BB.EXE)
            nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0

            oFs = New Scripting.FileSystemObject

            '----------
            bKeepOppdragsNr = True
            bAppendFile = False
            bStartRecordWritten = False

            ''Check if we are appending to an existing file.
            '' If so BB has to delete the last 89-record, and save the counters
            'If oFs.FileExists(sFilenameOut) Then
            '    bAppendFile = True
            '    bFoundRecord89 = False
            '    nExistingLines = 0
            '    'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
            '    Set oFile = oFs.OpenTextFile(sFilenameOut, ForReading, False)
            '    sTempFilename = Left$(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
            '    'First count the number of lines in the existing file
            '    Do While Not oFile.AtEndOfStream = True
            '        nExistingLines = nExistingLines + 1
            '        oFile.SkipLine
            '    Loop
            '    oFile.Close
            '    Set oFile = Nothing
            '    Set oFile = oFs.OpenTextFile(sFilenameOut, ForReading, False)
            '    'Then check if one of the 5 last lines are a 89-line
            '    ' Store the number of lines until the 89-line
            '    Do Until oFile.Line = nExistingLines - 1
            '        oFile.SkipLine
            '    Loop
            '    For iCount = 1 To 0 Step -1
            '        sLine = oFile.ReadLine()
            '        If Len(sLine) > 8 Then
            '            If Left$(sLine, 8) = "NY000089" Then
            '                nExistingLines = nExistingLines - iCount
            '                bFoundRecord89 = True
            '                Exit For
            '            End If
            '        End If
            '    Next iCount
            '    oFile.Close
            '    Set oFile = Nothing
            '    If Not bFoundRecord89 Then
            '        'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
            '        MsgBox "The existing file," & sFilenameOut & ", is not an OCR-file"
            '        WriteOCRFile = False
            '        Exit Function
            '        Set oFs = Nothing
            '    Else
            '        'Copy existing file to a temporary file and delete existing file
            '        oFs.CopyFile sFilenameOut, sTempFilename, True
            '        oFs.DeleteFile sFilenameOut
            '        'Coopy the content of the temporary file to the new file,
            '        ' excluding the 89-record
            '        Set oFileRead = oFs.OpenTextFile(sTempFilename, ForReading, False, 0)
            '        Set oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
            '        Do Until oFileRead.Line > nExistingLines - 1
            '            sLine = oFileRead.ReadLine
            '            oFileWrite.WriteLine (sLine)
            '        Loop
            '        'Store the neccesary counters and totalamount
            '        sLine = oFileRead.ReadLine
            '        nFileNoTransactions = Val(Mid$(sLine, 9, 8))
            '        'Subtract 1 because we have deleted the 89-line
            '        nFileNoRecords = Val(Mid$(sLine, 17, 8)) - 1
            '        nFileSumAmount = Val(Mid(sLine, 25, 17))
            '        'Kill'em all
            '        oFileRead.Close
            '        Set oFileRead = Nothing
            '        oFileWrite.Close
            '        Set oFileWrite = Nothing
            '        oFs.DeleteFile sTempFilename
            '    End If
            'End If

            nNoOfTransactionsExported = 0
            nAmountExported = 0

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR((oPayment.PayCode)) Then
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBabel = True
                                            End If
                                        Else
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            Exit For
                        End If 'bFalseOCR
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_AquariusOCRFileStart(oBabel)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If IsOCR((oPayment.PayCode)) Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                bKeepOppdragsNr = False ' Construct oppdragsnr
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoBatch = True
                                                End If
                                            Else
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                Exit For
                            End If 'falseOCR
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If IsOCR((oPayment.PayCode)) Then
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            'If oPayment.I_Account = sI_Account Then
                                            ' 01.10.2018 � changed above If to:
                                            If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoPayment = True
                                                    End If
                                                Else
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If 'falseOCR
                                If bExportoPayment Then
                                    j = j + 1

                                    nNoOfTransactionsExported = nNoOfTransactionsExported + 1
                                    nAmountExported = nAmountExported + oPayment.MON_InvoiceAmount

                                    For Each oInvoice In oPayment.Invoices
                                        ' Add to transactionno
                                        nTransactionNo = nTransactionNo + 1
                                        sLine = Wr_AquariusOCRPayment(oPayment, oInvoice)
                                        oFile.WriteLine((sLine))
                                        oPayment.Exported = True
                                    Next oInvoice 'invoice
                                End If

                            Next oPayment ' payment

                        End If
                    Next oBatch 'batch

                End If

            Next oBabel

            If nFileNoRecords > 0 Then
                sLine = Wr_AquariusOCRFileEnd()
                oFile.WriteLine((sLine))
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
            Else
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteAquariusIncomingOCR" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteAquariusIncomingOCR = True

    End Function
    Function Wr_AquariusOCRFileStart(ByRef oBabelFile As BabelFile) As String
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0

        sLine = "0" 'Record code
        sLine = sLine & VB6.Format(Now, "yymmddhhmm") 'Entry date, Hour of data
        sLine = sLine & "FB" 'Sending bank group
        sLine = sLine & "1" 'Currency code, mapped to Aquarius Currency Code = "NOK"
        sLine = sLine & Space(86)
        Wr_AquariusOCRFileStart = sLine

    End Function
    Public Function Wr_AquariusOCRPayment(ByRef oPayment As Payment, ByRef oInvoice As Invoice) As String
        Dim sLine As String
        Dim sKID As String


        '***************************************************************
        '*   DENNE FUNKSJONEN BENYTTES IKKE
        '***************************************************************


        nFileNoTransactions = nFileNoTransactions + 1
        nFileSumAmount = nFileSumAmount + oInvoice.MON_TransferredAmount

        sLine = "3" 'Record code 3 = KID payment
        sLine = sLine & PadLeft(oPayment.I_Account, 11, "0") 'Invoicer's account number
        If StringToDate((oPayment.DATE_Value)) > (System.DateTime.FromOADate(Now.ToOADate - 1000)) Then
            ' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "yymmdd") 'Entry date
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "yymmdd")
        End If
        If StringToDate((oPayment.DATE_Payment)) > (System.DateTime.FromOADate(Now.ToOADate - 1000)) Then
            ' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "yymmdd") 'Payment date
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "yymmdd")
        End If
        sLine = sLine & PadRight(oPayment.REF_Bank1, 16, " ") 'Filing code
        sKID = Trim(oInvoice.Unique_Id)
        Select Case Len(sKID)
            'This function is not used?????
            Case 15 'Remove first digit in invoice number, and add 00 after the invoice number and calculate the cdv digit
                sKID = Left(sKID, 5) & Mid(sKID, 7, 8) & "00"
                sKID = sKID & AddCDV10digit(sKID)
            Case 16 'Nothing to do
                sKID = Trim(oInvoice.Unique_Id)
                'New code12.02.2008
                'To treat some KIDs that deviates from the standard
                If Left(sKID, 7) = "0139166" Then
                    '9166 - Cargo Partner
                    '0139166014223400 - 9916600422340000
                    sKID = "99166" & PadLeft(Mid(sKID, 10, 6), 8, "0") & "00"
                    sKID = sKID & AddCDV10digit(sKID)
                ElseIf Left(sKID, 7) = "9129099" Then
                    'Old code - ElseIf Left$(sKID, 5) = "91290" Then
                    '1290 - New Phone
                    '9129099050297745 - 9129099050297000
                    sKID = Left(sKID, 13) & "00"
                    sKID = sKID & AddCDV10digit(sKID)
                ElseIf Left(sKID, 7) = "0109327" Then
                    '1327 - Strandco
                    '0109327491670003 - 9132700491670000
                    sKID = "91327000" & Mid(sKID, 8, 5) & "00"
                    sKID = sKID & AddCDV10digit(sKID)
                    '28.20.2009 - Changed after instructions from Roar Gl�tta. The KID may also start with "1"
                ElseIf Left(sKID, 1) = "0" Or Left(sKID, 1) = "1" Then
                    'Old code
                    'ElseIf Left$(sKID, 1) = "0" Then
                    If Mid(sKID, 6, 4) = "4779" Then
                        '4779 - IFCO
                        '0394947790954305 - 9477900095430000
                        sKID = "94779" & PadLeft(Mid(sKID, 10, 6), 8, "0") & "00"
                        sKID = sKID & AddCDV10digit(sKID)
                    End If
                End If
            Case 25 'Remove debtornumber and recalculate the cdv digit
                sKID = Left(sKID, 5) & Mid(sKID, 15, 10)
                sKID = sKID & AddCDV10digit(sKID)
            Case Else
                'Err.Raise 77777, "Wr_AquariusOCRPayment", "Ugyldig lengde p� KID" & vbCrLf & "KID: " & Trim$(oInvoice.Unique_Id)
        End Select
        sLine = sLine & PadLeft(sKID, 16, "0") 'Reference Number
        sLine = sLine & PadRight(oPayment.E_Name, 18, " ") 'Payer's name abbr.
        sLine = sLine & "1" 'Currency code, mapped to Aquarius currency Code = "NOK"
        If oInvoice.MON_TransferredAmount < 0 Then
            sLine = sLine & "-"
        Else
            sLine = sLine & "0" ' Filler
        End If
        sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_TransferredAmount)), 13, "0") ' Amount
        If Len(oPayment.E_Account) > 5 Then
            If Mid(oPayment.E_Account, 5, 1) <> "9" Then
                sLine = sLine & PadLeft(oPayment.E_Account, 11, " ") ' Payers accountnumber
            Else
                sLine = sLine & Space(11)
            End If
        Else
            sLine = sLine & Space(11)
        End If

        Wr_AquariusOCRPayment = sLine

    End Function
    Public Function Wr_AquariusOCRFileEnd() As String
        Dim sLine As String

        sLine = "9" 'Record code
        sLine = sLine & PadLeft(Str(nFileNoTransactions), 6, "0") ' No of transactions
        sLine = sLine & PadLeft(Str(nFileSumAmount), 11, "0") ' Total amount in file
        sLine = sLine & "000000" 'Number of corrections
        sLine = sLine & "00000000000" 'Amount of corrections
        sLine = sLine & "000000" 'Number of failed
        sLine = sLine & "00000000000" 'Amount of failed
        sLine = PadLine(sLine, 100, " ")

        Wr_AquariusOCRFileEnd = sLine

    End Function
    Private Function AddPostingOfCharges(ByRef oPayment As vbBabel.Payment, ByRef nTotalChargesAmount As Double, ByRef iAdjustmentType As Short) As Object
        'If charges exist, reduce the amount of 1 or more existing invoice (MatchedOnInvoice) with the eqvivalent amount
        '  of the charges (to secure that it is the samme currency, see code before calling this function).
        'Add one or more invoices for the same amount. Mark them as MatchedOnGL, since thay will not be a part of the
        '  summarization of the ALLOC-Amount. They will be created with a negative amount.
        Dim nRestChargesAmount As Double
        Dim nAmountToAdjust As Double
        Dim oInvoice As vbBabel.Invoice
        Dim oNewInvoice As vbBabel.Invoice
        Dim bReturnValue As Boolean
        Dim lCounter As Integer
        Dim bx As Boolean

        nRestChargesAmount = nTotalChargesAmount
        lCounter = 0

        For Each oInvoice In oPayment.Invoices

            If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount > 0 Then

                If oInvoice.MATCH_Matched Then

                    If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then

                        If Not EmptyString(oInvoice.MATCH_ID) Then

                            'New 02.04.2008 bt Kjell to take care of debit roundings
                            If iAdjustmentType = SGDebitRounding Then
                                'Treat this specific by adding an on-account posting
                                'Can't overpay an invoice because AQ doesn't accept this
                                'Post it on-account on the same debtor as the active invoice

                                'MsgBox("Adjustmenttype = SGDebitRounding  - Paid to much - Funker ikke.")

                                nAmountToAdjust = nRestChargesAmount * -1 'To make it positive

                                'Create an on-account posting
                                oNewInvoice = oPayment.Invoices.Add
                                oNewInvoice.MON_InvoiceAmount = nAmountToAdjust
                                oNewInvoice.MON_TransferredAmount = nAmountToAdjust
                                oNewInvoice.MATCH_Final = True
                                oNewInvoice.MATCH_Matched = True
                                'Changed 25.02.2008 by Kjell - this created an error when oPayment.Match_Undo was run
                                oNewInvoice.MATCH_Original = False
                                oNewInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer
                                oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
                                oNewInvoice.StatusCode = "02"
                                oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
                                oNewInvoice.CustomerNo = oInvoice.CustomerNo
                                'oNewInvoice.InvoiceNo = oInvoice.InvoiceNo
                                'oNewInvoice.Qualifier = InvoiceQualifier.Temporary
                                oNewInvoice.MyField = oInvoice.MyField '& "-" & Trim$(Str(iAdjustmentType))
                                oNewInvoice.VB_Profile = oPayment.VB_Profile

                                'MsgBox("Da har vi laget en ny Invoice.")

                                nRestChargesAmount = 0

                            Else

                                'MsgBox("Adjustmenttype = SGCreditRounding - Paid to little")

                                'New 23.10.2007 - Charges must be deducted, to do this add a new invoice
                                nAmountToAdjust = 0
                                If nRestChargesAmount > 0 Then
                                    If IsEqualAmount(nRestChargesAmount, 0) Then
                                        'The net amount to post on ALLOC is zero, don't withdraw charges!
                                        bReturnValue = True
                                        Exit For
                                    ElseIf nRestChargesAmount <= oInvoice.MON_InvoiceAmount Then
                                        'OK, the invoice covers the whole charges
                                        nAmountToAdjust = nRestChargesAmount
                                        nRestChargesAmount = 0
                                    Else
                                        'The ALLOC cover just a part of the charges
                                        nAmountToAdjust = oInvoice.MON_InvoiceAmount
                                        nRestChargesAmount = nRestChargesAmount - oInvoice.MON_InvoiceAmount
                                    End If
                                ElseIf nRestChargesAmount < 0 Then
                                    'Add the charges to the first (and best?) matched invoice
                                    nAmountToAdjust = nRestChargesAmount
                                    nRestChargesAmount = 0
                                End If

                                If Not IsEqualAmount(nAmountToAdjust, 0) Then

                                    lCounter = lCounter + 1
                                    oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount - nAmountToAdjust
                                    oInvoice.MON_TransferredAmount = oInvoice.MON_TransferredAmount - nAmountToAdjust
                                    oInvoice.MATCH_VATCode = "ABCDEF" & Trim(Str(lCounter))
                                    'Create an adjustment
                                    oNewInvoice = oPayment.Invoices.Add
                                    oNewInvoice.MON_InvoiceAmount = nAmountToAdjust * -1
                                    oNewInvoice.MON_TransferredAmount = nAmountToAdjust * -1
                                    oNewInvoice.MATCH_Final = True
                                    oNewInvoice.MATCH_Matched = True
                                    'Changed 25.02.2008 by Kjell - this created an error when oPayment.Match_Undo was run
                                    oNewInvoice.MATCH_Original = False
                                    'Old code
                                    'oNewInvoice.MATCH_Original = True
                                    oNewInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL
                                    oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
                                    oNewInvoice.StatusCode = "02"
                                    oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
                                    oNewInvoice.CustomerNo = oInvoice.CustomerNo
                                    oNewInvoice.InvoiceNo = oInvoice.InvoiceNo
                                    oNewInvoice.Qualifier = BabelFiles.InvoiceQualifier.Temporary
                                    oNewInvoice.MyField = oInvoice.MyField & "-" & Trim(Str(iAdjustmentType))
                                    oNewInvoice.VB_Profile = oPayment.VB_Profile
                                    If iAdjustmentType = SGAdjCharges Then
                                        oNewInvoice.MATCH_VATCode = "ABCDEF" & Trim(Str(lCounter))
                                    End If

                                End If 'If Not IsEqualAmount(nAmountToAdjust, 0) Then

                            End If 'If iAdjustmentType = SGDebitRounding Then

                            If IsEqualAmount(nRestChargesAmount, 0) Then
                                'MsgBox("Da g�r saker og ting i null.")
                                Exit For
                            End If

                        End If 'If Not EmptyString(oInvoice.MATCH_ID) Then
                    End If 'If oInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice Then
                End If 'If oInvoice.MATCH_Matched Then
            End If 'If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount > 0 Then

        Next oInvoice

        If IsEqualAmount(nRestChargesAmount, 0) Then
            bReturnValue = True
        Else
            'Remove posting of charges
            bx = ReallocatePostingOfCharges(oPayment, True)
            bReturnValue = False
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object AddPostingOfCharges. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        AddPostingOfCharges = bReturnValue

    End Function

    Private Function ReallocatePostingOfCharges(ByRef oPayment As vbBabel.Payment, ByRef bRemoveTemporaryInvoices As Boolean) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oOriginalInvoice As vbBabel.Invoice
        Dim nAmountToAdjust As Double
        Dim sId As String
        Dim bx As Boolean

        'This function is run when something goes wrong in the function ReallocatePostingOfCharges
        For Each oInvoice In oPayment.Invoices
            If oInvoice.Qualifier = BabelFiles.InvoiceQualifier.Temporary Then
                nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
                sId = oInvoice.MATCH_VATCode
                For Each oOriginalInvoice In oPayment.Invoices
                    If oOriginalInvoice.MATCH_VATCode = sId Then
                        If oOriginalInvoice.Qualifier = BabelFiles.InvoiceQualifier.Ordinary Then
                            oOriginalInvoice.MON_InvoiceAmount = oOriginalInvoice.MON_InvoiceAmount + nAmountToAdjust
                            oOriginalInvoice.MON_TransferredAmount = oOriginalInvoice.MON_TransferredAmount + nAmountToAdjust
                            oOriginalInvoice.MATCH_VATCode = ""
                        End If
                    End If
                Next oOriginalInvoice
            End If
        Next oInvoice

        If bRemoveTemporaryInvoices Then
            bx = RemoveTemporaryInvoices(oPayment)
        End If

        ReallocatePostingOfCharges = True

    End Function

    Private Function RemoveTemporaryInvoices(ByRef oPayment As vbBabel.Payment) As Boolean
        Dim lInvoiceCounter As Integer

        'Remove all temporary invoices
        For lInvoiceCounter = oPayment.Invoices.Count To 1 Step -1
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices(lInvoiceCounter).Qualifier. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oPayment.Invoices(lInvoiceCounter).Qualifier = BabelFiles.InvoiceQualifier.Temporary Then
                oPayment.Invoices.Remove((lInvoiceCounter))
            End If
        Next lInvoiceCounter

        RemoveTemporaryInvoices = True

    End Function
    Private Function SplitKIDperInvoiceForSG(ByRef sUnique_Id As String, ByRef oPayment As vbBabel.Payment) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iRecordCounter As Integer = 0
        Dim oInvoice As vbBabel.Invoice
        Dim oNewInvoice As vbBabel.Invoice
        Dim aQueryArray(,,) As String
        Dim lCompanyNo As Integer
        Dim sProvider As String
        Dim sUID, sPWD As String
        Dim sMySQL1, sMyModifiedSQL1 As String
        Dim sMySQL2, sMyModifiedSQL2 As String
        Dim lCounter As Integer
        Dim nTotalAmount As Double
        Dim nRunningTotal As Double
        Dim bReturnValue As Boolean
        Dim bContinue As Boolean
        Dim sDocumentDate As String = ""
        Dim sCustomerNo As String = ""
        Dim sTempInvoiceNo As String = ""
        Dim sTempAmount As String
        Dim sNewKID As String
        Dim bNegativeAmountExist As Boolean
        Dim nDeviation As Double

        Dim bWriteInfo As Boolean
        Dim sInfoString As String

        '10.10.2015
        Dim sClientNo As String

        'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
        '    MsgBox("I rutinen SplitKIDperInvoiceForSG, bel�p = " & oPayment.MON_InvoiceAmount.ToString)
        'End If

        sClientNo = Mid(sUnique_Id, 2, 4)   ' 4017 or 1004

        sInfoString = "KID: " & sUnique_Id

        On Error GoTo errSplitKIDperInvoiceForSG

        bReturnValue = False

        If oPayment.Invoices.Count = 1 Then

            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
            '    MsgBox("Invoices.count = 1")
            'End If

            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
            '    MsgBox("KID er n�:" & sUnique_Id)
            'End If

            lCompanyNo = 1 'TODO - Hardcoded CompanyNo
            ''Build an array consisting of the ConnectionStrings and Queries for aftermatcinh agianst the ERP-system.
            aQueryArray = GetERPDBInfo(lCompanyNo, 5)
            'sProvider = aQueryArray(0, 0, 0) ' Holds connectionstring
            'sUID = aQueryArray(0, 1, 0)
            'sPWD = aQueryArray(0, 2, 0)
            '' Change UID/PWD:
            '' Fill in UserID and Password
            'sProvider = Replace(sProvider, "=UID", "=" & sUID, , , CompareMethod.Text)
            'sProvider = Replace(sProvider, "=PWD", "=" & sPWD, , , CompareMethod.Text)
            '' Logon
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oMyDal.Company_ID = lCompanyNo
            oMyDal.DBProfile_ID = 1 'TODO - Hardcoded DBProfile_ID
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL1 = ""
            sMySQL2 = ""

            'sMySQL1 = "SELECT LI.DocumentDate AS BBRET_DocumentDate, AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency, LI.DueDate AS FFDato FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND LI.DocumentNumber = 'BB_InvoiceIdentifier' AND AD.ClientNumber = RIGHT('0000000' + '4017',7) ORDER BY AD.ClientNumber ASC, LI.DueDate ASC"
            'sMySQL2 = "SELECT AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND AD.DebtorNumber = 'BB_CustomerNo' AND AD.ClientNumber = RIGHT('0000000' + '4017',7) AND LI.DocumentDate = CONVERT(datetime, 'BB_DocumentDate')"
            '10.10.2015
            sMySQL1 = "SELECT LI.DocumentDate AS BBRET_DocumentDate, AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency, LI.DueDate AS FFDato FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND LI.DocumentNumber = 'BB_InvoiceIdentifier' AND AD.ClientNumber = RIGHT('0000000' + '" & sClientNo & "',7) ORDER BY AD.ClientNumber ASC, LI.DueDate ASC"
            sMySQL2 = "SELECT AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND AD.DebtorNumber = 'BB_CustomerNo' AND AD.ClientNumber = RIGHT('0000000' + '" & sClientNo & "',7) AND LI.DocumentDate = CONVERT(datetime, 'BB_DocumentDate')"
            nTotalAmount = 0
            nRunningTotal = 0

            For lCounter = 0 To UBound(aQueryArray, 3)

                If UCase(aQueryArray(0, 4, lCounter)) = "SAMLEFAKTURA1 9999" Then
                    'Name of the SQL's retrieve the duedate on the invoice of the KID.
                    sMySQL1 = aQueryArray(0, 0, lCounter)
                    'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                    '    MsgBox("Hentet ut SQL1: " & sMySQL1)
                    'End If

                End If
                If UCase(aQueryArray(0, 4, lCounter)) = "SAMLEFAKTURA2 9999" Then
                    'Name of the SQL's to use for checking the samlefaktura.
                    sMySQL2 = aQueryArray(0, 0, lCounter)
                    'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                    '    MsgBox("Hentet ut SQL2: " & sMySQL2)
                    'End If
                End If

            Next lCounter

            If Not EmptyString(sMySQL1) And Not EmptyString(sMySQL2) Then
                If InStr(1, sMySQL2, "BBRET_Amount", CompareMethod.Text) Then

                    sMyModifiedSQL1 = Replace(sMySQL1, "BB_InvoiceIdentifier", RemoveLeadingCharacters(Mid(sUnique_Id, 7, 7), "0"), , , CompareMethod.Text)
                    sMyModifiedSQL1 = Replace(sMyModifiedSQL1, "BB_ClientNo", sClientNo, , , CompareMethod.Text)

                    sInfoString = sInfoString & " - " & RemoveLeadingCharacters(Mid(sUnique_Id, 7, 7), "0")

                    oMyDal.SQL = sMyModifiedSQL1
                    iRecordCounter = 0
                    bContinue = False
                    'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                    '    MsgBox("Skal kj�re SQL1" & vbCrLf & "SQL: " & sMyModifiedSQL1)
                    'End If
                    If oMyDal.Reader_Execute() Then
                        Do While oMyDal.Reader_ReadRecord
                            iRecordCounter = iRecordCounter + 1
                            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                            '    MsgBox("Funnet post, antall: " & iRecordCounter.ToString)
                            'End If
                            If InStr(1, sMyModifiedSQL1, "BBRET_DocumentDate", CompareMethod.Text) > 0 Then
                                If InStr(1, sMyModifiedSQL1, "BBRET_CustomerNo", CompareMethod.Text) > 0 Then
                                    sDocumentDate = oMyDal.Reader_GetString("BBRET_DocumentDate")
                                    'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                    '    MsgBox("Funnet documentdate: " & sDocumentDate)
                                    'End If
                                    If Not oMyDal.Reader_GetString("BBRET_CustomerNo") = "" Then
                                        If Not sDocumentDate = "" Then
                                            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                            '    MsgBox("Funnet customerno: " & oMyDal.Reader_GetString("BBRET_CustomerNo"))
                                            'End If
                                            'We need to retrieve the invoiceNo
                                            If InStr(1, sMySQL2, "BBRET_InvoiceNo", CompareMethod.Text) > 0 Then
                                                If Mid(sDocumentDate, 3, 1) = "." And Mid(sDocumentDate, 6, 1) = "." And Len(sDocumentDate) = 10 Then
                                                    sDocumentDate = Mid(sDocumentDate, 7, 4) & Mid(sDocumentDate, 4, 2) & Mid(sDocumentDate, 1, 2)
                                                    sInfoString = sInfoString & " - " & "Funnet en faktura med dato " & sDocumentDate & vbCrLf
                                                    sCustomerNo = oMyDal.Reader_GetString("BBRET_CustomerNo")
                                                    'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                                    '    MsgBox("Funnet alle parametere: ")
                                                    'End If
                                                    bContinue = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Loop
                    Else
                        'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                        '    MsgBox("SP�rring feilet med feilmelding: " & vbCrLf & oMyDal.ErrorMessage)
                        'End If
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                    If iRecordCounter > 1 Then
                        bContinue = False
                        sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Funnet " & iRecordCounter.ToString & " poster."
                    End If

                    If bContinue Then
                        nTotalAmount = 0
                        bContinue = True
                        bNegativeAmountExist = False
                        iRecordCounter = 0

                        sMyModifiedSQL2 = Replace(sMySQL2, "BB_DocumentDate", sDocumentDate, , , CompareMethod.Text)
                        'sMyModifiedSQL2 = Replace(sMyModifiedSQL2, "BB_ClientNo", "4017", , , CompareMethod.Text)
                        ' 10.10.2015
                        sMyModifiedSQL2 = Replace(sMyModifiedSQL2, "BB_ClientNo", sClientNo, , , vbTextCompare)

                        sMyModifiedSQL2 = Replace(sMyModifiedSQL2, "BB_CustomerNo", sCustomerNo, , , CompareMethod.Text)
                        oMyDal.SQL = sMyModifiedSQL2
                        'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                        '    MsgBox("Skal kj�re SQL2" & vbCrLf & "SQL: " & sMyModifiedSQL2)
                        'End If

                        If oMyDal.Reader_Execute() Then
                            If oMyDal.Reader_HasRows Then
                                Do While oMyDal.Reader_ReadRecord
                                    iRecordCounter = iRecordCounter + 1
                                    sTempInvoiceNo = oMyDal.Reader_GetString("BBRET_InvoiceNo")
                                    sTempAmount = oMyDal.Reader_GetString("BBRET_Amount")

                                    If Not sTempInvoiceNo = "" Then
                                        If Not sTempAmount = "" Then
                                            nTotalAmount = nTotalAmount + CDbl(sTempAmount)
                                            If CDbl(sTempAmount) < 0 Then
                                                bNegativeAmountExist = True
                                                bContinue = False
                                                Exit Do
                                            End If
                                        Else
                                            bContinue = False
                                            Exit Do
                                        End If
                                    Else
                                        bContinue = False
                                        Exit Do
                                    End If
                                Loop
                            Else
                                sInfoString = sInfoString & "AVBRYTER PROSESS! Funnet 0 fakturaer p� denne dato."
                            End If
                        Else
                            Err.Raise(45002, "SplitKIDperInvoiceForSG", LRSCommon(45002) & oMyDal.ErrorMessage)
                        End If

                        If iRecordCounter > 1 Then
                            sInfoString = sInfoString & "Funnet " & iRecordCounter.ToString & " fakturaer."
                        Else
                            bContinue = False
                        End If

                        If bNegativeAmountExist Then
                            sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Samlefaktura inneholder kreditnota."
                            bContinue = False
                        End If

                        'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                        '    MsgBox("Totalbel�p sum fakturaer: " & nTotalAmount.ToString)
                        'End If

                        'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                        '    MsgBox("Bel�p p� innbetaling: " & oPayment.MON_InvoiceAmount.ToString)
                        'End If

                        nDeviation = 0
                        If Not IsEqualAmount(nTotalAmount, oPayment.MON_InvoiceAmount) Then
                            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                            '    MsgBox("Avvik p� bel�p er: " & (nTotalAmount - oPayment.MON_InvoiceAmount).ToString)
                            'End If

                            If System.Math.Abs(oPayment.MON_InvoiceAmount - nTotalAmount) < 100 Then
                                nDeviation = oPayment.MON_InvoiceAmount - nTotalAmount
                                nTotalAmount = oPayment.MON_InvoiceAmount
                                sInfoString = sInfoString & "�resavvik p� " & Trim(Str(nDeviation)) & " er godtatt."
                            End If
                        End If

                        If IsEqualAmount(nTotalAmount, oPayment.MON_InvoiceAmount) And iRecordCounter > 1 And bContinue Then
                            'OK, We've found the invoices
                            'Don't need to do anything if we have only 1 invoice

                            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                            '    MsgBox("Funnet match p� betalingen")
                            'End If

                            'Set the original invoce as not final
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.MATCH_Final = False
                                oInvoice.MATCH_Matched = False
                            Next oInvoice

                            'Add the new invoces from the recordset
                            oMyDal.SQL = sMyModifiedSQL2
                            If oMyDal.Reader_Execute() Then
                                Do While oMyDal.Reader_ReadRecord
                                    oNewInvoice = oPayment.Invoices.Add
                                    If nDeviation <> 0 And CDbl(oMyDal.Reader_GetString("BBRET_Amount")) + nDeviation > 0 Then
                                        oNewInvoice.MON_InvoiceAmount = CDbl(oMyDal.Reader_GetString("BBRET_Amount")) + nDeviation
                                        oNewInvoice.MON_TransferredAmount = oNewInvoice.MON_InvoiceAmount
                                        nDeviation = 0
                                    Else
                                        oNewInvoice.MON_InvoiceAmount = CDbl(oMyDal.Reader_GetString("BBRET_Amount"))
                                        oNewInvoice.MON_TransferredAmount = oNewInvoice.MON_InvoiceAmount
                                    End If
                                    nRunningTotal = nRunningTotal + oNewInvoice.MON_InvoiceAmount
                                    oNewInvoice.MATCH_Final = True
                                    oNewInvoice.MATCH_Matched = True
                                    oNewInvoice.MATCH_Original = False
                                    oNewInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice
                                    'sNewKID = "94017" & PadLeft(oMyDal.Reader_GetString("BBRET_InvoiceNo"), 8, "0") & "00"
                                    ' 10.10.2015
                                    sNewKID = "9" & sClientNo & PadLeft(oMyDal.Reader_GetString("BBRET_InvoiceNo"), 8, "0") & "00"

                                    sNewKID = sNewKID & AddCDV10digit(sNewKID)
                                    oNewInvoice.Unique_Id = sNewKID
                                    oNewInvoice.StatusCode = "02"
                                    oNewInvoice.VB_Profile = oPayment.VB_Profile

                                    'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                    '    MsgBox("Lagt inn ny faktura" & vbCrLf & "Fakturanr: " & oMyDal.Reader_GetString("BBRET_InvoiceNo") & "Bel�p: " & oMyDal.Reader_GetString("BBRET_Amount"))
                                    'End If
                                Loop
                            Else
                                Err.Raise(45002, "SplitKIDperInvoiceForSG", LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If

                            'Just check if everything is OK
                            If IsEqualAmount(nRunningTotal, oPayment.MON_InvoiceAmount) Then
                                bReturnValue = True
                                sInfoString = sInfoString & vbCrLf & "VELLYKKET UTBYING!!!!! Innbetalt bel�p: " & oPayment.MON_InvoiceAmount.ToString & " Totalt fakturert bel�p: " & nTotalAmount.ToString
                                'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                '    MsgBox("VELLYKKET UTBYING!!!!! Innbetalt bel�p: " & oPayment.MON_InvoiceAmount.ToString & " Totalt fakturert bel�p: " & nTotalAmount.ToString)
                                'End If
                            Else
                                sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Sluttkontroll avsl�rer avvik. Opphever samlefaktura."
                                oPayment.MATCH_Undo()
                                'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                                '    MsgBox("AVBRYTER PROSESS! Sluttkontroll avsl�rer avvik. Opphever samlefaktura.")
                                'End If
                            End If

                        Else

                            If bNegativeAmountExist Then
                                'Don't write anything
                            ElseIf iRecordCounter = 1 Then
                                sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Kun en faktura."
                            Else
                                sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Bel�p stemmer ikke. Innbetalt bel�p: " & oPayment.MON_InvoiceAmount.ToString & " Totalt fakturert bel�p: " & nTotalAmount.ToString
                            End If

                            'If (oPayment.E_Account = "24900511708" Or oPayment.E_Account = "95033112456") And oPayment.I_Account = "81010788293" Then
                            '    MsgBox("Feilet av en eller annen grunn. irecordcounter = " & iRecordCounter.ToString & vbCrLf & sInfoString)
                            'End If
                            'bReturnValue = False

                        End If

                    End If 'If bContinue

                End If
            End If 'If Not EmptyString(sMySQL1) And Not EmptyString(sMySQL2) Then

        End If 'If oInvoice.Count = 1 Then

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        'If Not RunTime Then
        '    Set oFile = oFs.OpenTextFile("C:\SG Finans\KID.txt", ForAppending, True, 0)
        '    oFile.WriteLine sInfoString & vbCrLf & vbCrLf
        '    oFile.Close
        '    Set oFile = Nothing
        '    Set oFs = Nothing
        'End If

        On Error GoTo 0

        SplitKIDperInvoiceForSG = bReturnValue

        Exit Function

errSplitKIDperInvoiceForSG:

        'If Not RunTime Then
        '    Set oFile = oFs.OpenTextFile("C:\SG Finans\KID.txt", ForAppending, True, 0)
        '    oFile.WriteLine sInfoString & vbCrLf & "AVBRYTER PROSESS! En uventet feil oppstod " & Err.Description
        '    Set oFile = Nothing
        '    Set oFs = Nothing
        'End If

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        SplitKIDperInvoiceForSG = False

    End Function
End Module
