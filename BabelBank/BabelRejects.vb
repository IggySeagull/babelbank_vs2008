Option Strict Off
Option Explicit On
Friend Class BabelRejects
	
	Private oBabelFile As vbbabel.BabelFile
	Private bToPrint As Boolean
	Private bToScreen As Boolean
	
	'********* START PROPERTY SETTINGS ***********************
	Public WriteOnly Property ImportObject() As vbbabel.BabelFile
		Set(ByVal Value As vbbabel.BabelFile)
			oBabelFile = Value
		End Set
	End Property
	Public WriteOnly Property ToPrint() As Boolean
		Set(ByVal Value As Boolean)
			bToPrint = Value
			'bToScreen = Not bToPrint
		End Set
	End Property
	Public WriteOnly Property ToScreen() As Boolean
		Set(ByVal Value As Boolean)
			bToScreen = Value
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	Public Function Start() As Object
		' present information about rejected payments
        Dim aRejectInfo(,) As Object
		Dim bx As Boolean
		
		' added 03.01.2008 -
		' if empty aRejectInfo, then returnvalue was set to false, which gave "Unsuccesfull running" !!
		bx = True
		aRejectInfo = VB6.CopyArray(FillRejectInfo)
		'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(0, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If aRejectInfo(0, 0) <> "" Then
            ' at least one item with rejected payment
            bx = ShowRejects(aRejectInfo)
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object Start. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Start = bx

    End Function
    Friend Function ShowRejects(ByRef aRejectInfo(,) As Object) As Boolean
        ' show with spread3
        Dim frmRejectedPayments As New frmRejectedPayments
        Dim i, j As Short
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim txtColumn As DataGridViewTextBoxColumn

        With frmRejectedPayments.gridRejected

            .ScrollBars = ScrollBars.Both
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            ' not possible to add rows manually!
            .AllowUserToAddRows = False
            .RowHeadersVisible = False
            '.MultiSelect = False
            .BorderStyle = BorderStyle.FixedSingle
            .RowTemplate.Height = 18
            .BackgroundColor = Color.White
            ' to have no marker for "selected" line
            .RowsDefaultCellStyle.SelectionBackColor = Color.White
            .RowsDefaultCellStyle.SelectionForeColor = Color.Black
            .CellBorderStyle = DataGridViewCellBorderStyle.None
            .ReadOnly = True

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60084) 'Status No
            txtColumn.Width = WidthFromSpreadToGrid(4)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60085) '"Meldingstekst"
            txtColumn.Width = WidthFromSpreadToGrid(20)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60065) '"Dato"
            txtColumn.Width = WidthFromSpreadToGrid(6)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            If oBabelFile.ImportFormat = BabelFiles.FileType.OCR Then
                txtColumn.HeaderText = LRS(60068) '"KID"
            Else
                txtColumn.HeaderText = LRS(60086) '"Navn"
            End If
            txtColumn.Width = WidthFromSpreadToGrid(15)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60060) '"Bel�p"
            txtColumn.Width = WidthFromSpreadToGrid(10)
            txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60072)
            txtColumn.Width = WidthFromSpreadToGrid(10)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60076)
            txtColumn.Width = WidthFromSpreadToGrid(8)
            .Columns.Add(txtColumn)

            frmRejectedPayments.Text = LRS(60001) & " " & Mid(oBabelFile.FilenameIn, InStrRev(oBabelFile.FilenameIn, "\") + 1)
            '"Avviste betalinger" + filnavn
            frmRejectedPayments.cmdPrint.Text = LRS(55003) '"&Utskrift"

            For i = 0 To UBound(aRejectInfo, 2)
                ' fill spread
                .Rows.Add()

                For j = 0 To 6

                    If j = 4 Then
                        ' bel�p
                        .Rows(.RowCount - 1).Cells(0).Value = VB6.Format(aRejectInfo(j, i), "##,##0.00")
                    Else
                        .Rows(.RowCount - 1).Cells(0).Value = aRejectInfo(j, i)
                    End If
                Next j

            Next i

        End With

        'Shortcut to get OCR-rejects to screen
        If oBabelFile.ImportFormat = BabelFiles.FileType.OCR Then
            bToScreen = True
        End If

        frmRejectedPayments.sHeading = LRS(60001) & " " & Mid(oBabelFile.FilenameIn, InStrRev(oBabelFile.FilenameIn, "\") + 1)

        If bToScreen Then
            frmRejectedPayments.ShowDialog()
        Else
            'UPGRADE_ISSUE: Load statement is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'

            'TODO - Should be OK by dimensioning frmRejectedPayments as new frmRejectedPayments
            'Removed next line
            frmRejectedPayments.Show()
            ' print spread
            frmRejectedPayments.RejectedPrint()
        End If


        ' New 04.08.04
        ' When using Coda, for Aureus (Narvesen, 7-Eleven, etc), we are "forcing" errorcodes
        ' in sendfile, to trigger BabelRejects.
        ' In Sendfiles for Coda, theese errorcodes must be replaced!
        If oBabelFile.ImportFormat = BabelFiles.FileType.coda Then

            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If Val(oPayment.StatusCode) > 0 Then
                        oPayment.StatusCode = "00"
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.StatusCode = "00"
                        Next oInvoice
                    End If
                Next oPayment
            Next oBatch
        End If
        '--- end new 04.08.04 ---


        ShowRejects = Not frmRejectedPayments.bCancel ' OK = True, Cancel = False


    End Function
    Friend Function FillRejectInfo() As Object()
        Dim aRejectInfo(,) As Object
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice

        Dim sStatus As String
        Dim nNo As Double
        Dim sTxt As String
        Dim bPaymentShown As Boolean


        nNo = 0
        ReDim aRejectInfo(6, 0) ' 23.03.07 added I_Account

        For Each oBatch In oBabelFile.Batches
            For Each oPayment In oBatch.Payments
                bPaymentShown = False
                sTxt = ""

                Select Case oPayment.ImportFormat

                    Case BabelFiles.FileType.Telepay, BabelFiles.FileType.coda, BabelFiles.FileType.Telepay2, BabelFiles.FileType.pgs, BabelFiles.FileType.PGS_Sweden, BabelFiles.FileType.Leverantorsbetalningar
                        sStatus = oPayment.StatusCode
                        If Val(sStatus) > 2 Or (oPayment.ImportFormat = BabelFiles.FileType.Leverantorsbetalningar And sStatus = "0000") Then
                            If oPayment.ImportFormat = BabelFiles.FileType.Leverantorsbetalningar Then
                                sTxt = BankgirotStatusTxts((oPayment.StatusCode))
                            Else
                                sTxt = TelepayStatusTxts((oPayment.StatusCode))
                            End If
                            ReDim Preserve aRejectInfo(6, nNo)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(0, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(0, nNo) = sStatus
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(1, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(1, nNo) = sTxt
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(2, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(2, nNo) = CObj(StringToDate((oPayment.DATE_Payment)))
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(3, nNo) = oPayment.E_Name
                            If oPayment.MON_TransferredAmount <> 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(4, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(4, nNo) = oPayment.MON_TransferredAmount / 100
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(4, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(4, nNo) = oPayment.MON_InvoiceAmount / 100
                            End If
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(5, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(5, nNo) = oPayment.REF_Own
                            If oPayment.ImportFormat = BabelFiles.FileType.PGS_Sweden Or BabelFiles.FileType.pgs Then ' added 09.11.2007
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(6, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(6, nNo) = oPayment.E_Account
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(6, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(6, nNo) = oPayment.I_Account
                            End If
                            bPaymentShown = True

                            nNo = nNo + 1
                        End If

                        ' do the same for invoice as well, to report payments with error in invoice only!
                        For Each oInvoice In oPayment.Invoices
                            sStatus = oInvoice.StatusCode
                            If Val(sStatus) > 2 Then
                                If Not bPaymentShown Then
                                    ' Must also show payment, even if error only in invoice
                                    If sTxt = "" Then
                                        sTxt = "(Feil i fakturalinjer, BETFOR23, ikke BETFOR21)"
                                    End If
                                    ReDim Preserve aRejectInfo(6, nNo)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(0, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(0, nNo) = sStatus
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(1, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(1, nNo) = sTxt
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(2, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(2, nNo) = CObj(StringToDate((oPayment.DATE_Payment)))
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(3, nNo) = oPayment.E_Name
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(4, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(4, nNo) = oPayment.MON_TransferredAmount / 100
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(5, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(5, nNo) = oPayment.REF_Own
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(6, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(6, nNo) = oPayment.I_Account
                                    bPaymentShown = True

                                    nNo = nNo + 1
                                End If

                                If oPayment.ImportFormat = BabelFiles.FileType.Leverantorsbetalningar Then
                                    sTxt = BankgirotStatusTxts((oPayment.StatusCode))
                                Else
                                    sTxt = TelepayStatusTxts((oInvoice.StatusCode))
                                End If
                                ReDim Preserve aRejectInfo(6, nNo)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(0, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(0, nNo) = sStatus
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(1, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(1, nNo) = sTxt
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(2, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(2, nNo) = "   - - '' - -"
                                If Trim(oInvoice.Unique_Id) <> "" Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(3, nNo) = oInvoice.Unique_Id
                                ElseIf oInvoice.Freetexts.Count > 0 Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.Freetexts(1).Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oInvoice.Freetexts(1).Text <> "" Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.Freetexts().Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        aRejectInfo(3, nNo) = oInvoice.Freetexts(1).Text
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        aRejectInfo(3, nNo) = ""
                                    End If
                                Else
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    aRejectInfo(3, nNo) = ""
                                End If
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(4, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(4, nNo) = oInvoice.MON_InvoiceAmount / 100
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(5, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(5, nNo) = oPayment.REF_Own
                                'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(6, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aRejectInfo(6, nNo) = oPayment.I_Account
                                nNo = nNo + 1
                            End If
                        Next oInvoice
                        If bPaymentShown Then
                            ' linebreak between payments:
                            ReDim Preserve aRejectInfo(6, nNo)
                            nNo = nNo + 1
                        End If

                    Case BabelFiles.FileType.OCR
                        sStatus = oPayment.StatusCode
                        If Val(sStatus) > 2 Then
                            sTxt = TelepayStatusTxts((oPayment.StatusCode))
                            ReDim Preserve aRejectInfo(6, nNo)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(0, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(0, nNo) = sStatus
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(1, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(1, nNo) = sTxt & " Ref.: " & oPayment.REF_Own & " Arkivref.: " & oPayment.REF_Bank1
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(2, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(2, nNo) = CObj(StringToDate((oPayment.DATE_Payment)))
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(3, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(3, nNo) = oPayment.Invoices.Item(1).Unique_Id
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(4, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(4, nNo) = oPayment.MON_TransferredAmount / 100
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(5, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(5, nNo) = oPayment.REF_Own
                            'UPGRADE_WARNING: Couldn't resolve default property of object aRejectInfo(6, nNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aRejectInfo(6, nNo) = oPayment.I_Account
                            bPaymentShown = True

                            nNo = nNo + 1
                        End If


                End Select

            Next oPayment
        Next oBatch

        If Not oBatch Is Nothing Then
            'UPGRADE_NOTE: Object oBatch may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oBatch = Nothing
        End If
        If Not oPayment Is Nothing Then
            'UPGRADE_NOTE: Object oPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oPayment = Nothing
        End If

        FillRejectInfo = VB6.CopyArray(aRejectInfo)
    End Function
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		bToPrint = False
		bToPrint = False
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		If Not oBabelFile Is Nothing Then
			'UPGRADE_NOTE: Object oBabelFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			oBabelFile = Nothing
		End If
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class
