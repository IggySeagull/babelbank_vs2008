﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreProcessFile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OK_Button = New System.Windows.Forms.Button
        Me.cmdErrorReport = New System.Windows.Forms.Button
        Me.cmdMail = New System.Windows.Forms.Button
        Me.cmdView = New System.Windows.Forms.Button
        Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
        Me.lblFilename = New System.Windows.Forms.Label
        Me.lblErrorsFound = New System.Windows.Forms.Label
        Me.lblProgress = New System.Windows.Forms.Label
        Me.lblFunction = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.listErrorList = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(604, 299)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 1
        Me.OK_Button.Text = "OK"
        '
        'cmdErrorReport
        '
        Me.cmdErrorReport.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdErrorReport.Location = New System.Drawing.Point(507, 299)
        Me.cmdErrorReport.Name = "cmdErrorReport"
        Me.cmdErrorReport.Size = New System.Drawing.Size(91, 23)
        Me.cmdErrorReport.TabIndex = 2
        Me.cmdErrorReport.Text = "00000-ErrorReport"
        '
        'cmdMail
        '
        Me.cmdMail.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdMail.Location = New System.Drawing.Point(410, 299)
        Me.cmdMail.Name = "cmdMail"
        Me.cmdMail.Size = New System.Drawing.Size(91, 23)
        Me.cmdMail.TabIndex = 3
        Me.cmdMail.Text = "00000-eMail"
        '
        'cmdView
        '
        Me.cmdView.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdView.Location = New System.Drawing.Point(313, 299)
        Me.cmdView.Name = "cmdView"
        Me.cmdView.Size = New System.Drawing.Size(91, 23)
        Me.cmdView.TabIndex = 4
        Me.cmdView.Text = "00000-Filebrowser"
        '
        'Line1
        '
        Me.Line1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.Line1.Name = "Line1"
        Me.Line1.X1 = 160
        Me.Line1.X2 = 676
        Me.Line1.Y1 = 291
        Me.Line1.Y2 = 290
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.Line1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(704, 331)
        Me.ShapeContainer1.TabIndex = 5
        Me.ShapeContainer1.TabStop = False
        '
        'lblFilename
        '
        Me.lblFilename.AutoSize = True
        Me.lblFilename.Location = New System.Drawing.Point(150, 65)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Size = New System.Drawing.Size(49, 13)
        Me.lblFilename.TabIndex = 6
        Me.lblFilename.Text = "Filename"
        '
        'lblErrorsFound
        '
        Me.lblErrorsFound.AutoSize = True
        Me.lblErrorsFound.Location = New System.Drawing.Point(150, 88)
        Me.lblErrorsFound.Name = "lblErrorsFound"
        Me.lblErrorsFound.Size = New System.Drawing.Size(64, 13)
        Me.lblErrorsFound.TabIndex = 7
        Me.lblErrorsFound.Text = "ErrorsFound"
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(150, 115)
        Me.lblProgress.MaximumSize = New System.Drawing.Size(2000, 0)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(81, 13)
        Me.lblProgress.TabIndex = 8
        Me.lblProgress.Text = "00000-Progress"
        '
        'lblFunction
        '
        Me.lblFunction.AutoSize = True
        Me.lblFunction.Location = New System.Drawing.Point(150, 136)
        Me.lblFunction.Name = "lblFunction"
        Me.lblFunction.Size = New System.Drawing.Size(48, 13)
        Me.lblFunction.TabIndex = 9
        Me.lblFunction.Text = "Function"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(313, 111)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(359, 22)
        Me.ProgressBar1.TabIndex = 10
        '
        'listErrorList
        '
        Me.listErrorList.FormattingEnabled = True
        Me.listErrorList.Location = New System.Drawing.Point(159, 159)
        Me.listErrorList.Name = "listErrorList"
        Me.listErrorList.Size = New System.Drawing.Size(512, 121)
        Me.listErrorList.TabIndex = 11
        '
        'frmPreProcessFile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 331)
        Me.Controls.Add(Me.listErrorList)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.lblFunction)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.lblErrorsFound)
        Me.Controls.Add(Me.lblFilename)
        Me.Controls.Add(Me.cmdView)
        Me.Controls.Add(Me.cmdMail)
        Me.Controls.Add(Me.cmdErrorReport)
        Me.Controls.Add(Me.OK_Button)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPreProcessFile"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmPreProcessFile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents cmdErrorReport As System.Windows.Forms.Button
    Friend WithEvents cmdMail As System.Windows.Forms.Button
    Friend WithEvents cmdView As System.Windows.Forms.Button
    Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents lblFilename As System.Windows.Forms.Label
    Friend WithEvents lblErrorsFound As System.Windows.Forms.Label
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents lblFunction As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents listErrorList As System.Windows.Forms.ListBox

End Class
