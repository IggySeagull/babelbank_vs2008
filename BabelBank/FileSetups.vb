Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("FileSetups_NET.FileSetups")> Public Class FileSetups
	Implements System.Collections.IEnumerable
	
	'local variable to hold collection
	Private mCol As Collection
	' Added new protperties for use in Setup (ver. 1.0.20)
	Private iSetup_CurrentSend, iSetup_CurrentReturn As Short
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property Setup_CurrentSend() As Short
		Get
			Setup_CurrentSend = iSetup_CurrentSend
		End Get
		Set(ByVal Value As Short)
			iSetup_CurrentSend = Value
		End Set
	End Property
	Public Property Setup_CurrentReturn() As Short
		Get
			Setup_CurrentReturn = iSetup_CurrentReturn
		End Get
		Set(ByVal Value As Short)
			iSetup_CurrentReturn = Value
		End Set
	End Property
	
	Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As vbBabel.FileSetup
		Get
			'used when referencing an element in the collection
			'vntIndexKey contains either the Index or Key to the collection,
			'this is why it is declared as a Variant
			'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
			Item = mCol.Item(vntIndexKey)
		End Get
	End Property
	
	
	
	Public ReadOnly Property Count() As Integer
		Get
			'used when retrieving the number of elements in the
			'collection. Syntax: Debug.Print x.Count
			Count = mCol.Count()
		End Get
	End Property
	
	
	'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
	'Public ReadOnly Property NewEnum() As stdole.IUnknown
		'Get
			'this property allows you to enumerate
			'this collection with the For...Each syntax
			'NewEnum = mCol._NewEnum
		'End Get
	'End Property
	
	Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
		'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        GetEnumerator = mCol.GetEnumerator
	End Function
	'********* END PROPERTY SETTINGS ***********************
	
	Public Function Setup_NoOfSends() As Object
		' Return no of sendprofiles involved in actual profile
		Dim oFilesetup As FileSetup
		Dim i As Short
		i = 0
		For	Each oFilesetup In mCol
			If oFilesetup.Setup_Send = True Then
				i = i + 1
			End If
		Next oFilesetup
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_NoOfSends. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_NoOfSends = i
	End Function
	Public Function Setup_NoOfReturns() As Object
		' Return no of returnprofiles involved in actual profile
		Dim oFilesetup As FileSetup
		Dim i As Short
		i = 0
		For	Each oFilesetup In mCol
			If oFilesetup.Setup_Return = True Then
				i = i + 1
			End If
		Next oFilesetup
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_NoOfReturns. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_NoOfReturns = i
	End Function
	Public Function Setup_GoToNextSend(Optional ByRef iSetNext As Object = Nothing) As Object
		' Return filesetupid for next sendprofile, -1 if no more
		Static iNextId As Short
		Dim oFilesetup As FileSetup
		
		' In case we find no next, then return -1
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GoToNextSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_GoToNextSend = -1
		
		'UPGRADE_NOTE: IsMissing() was changed to IsNothing(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"'
		If Not IsNothing(iSetNext) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object iSetNext. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			iNextId = iSetNext
			'UPGRADE_WARNING: Couldn't resolve default property of object iSetNext. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GoToNextSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Setup_GoToNextSend = iSetNext
		Else
			For	Each oFilesetup In mCol
				If oFilesetup.Setup_Send = True And Val(CStr(oFilesetup.FileSetup_ID)) > iNextId Then
					iNextId = oFilesetup.FileSetup_ID
					'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GoToNextSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Setup_GoToNextSend = oFilesetup.FileSetup_ID
				End If
			Next oFilesetup
		End If
	End Function
	Public Function Setup_GoToNextReturn(Optional ByRef iSetNext As Object = Nothing) As Object
		' Return filesetupid for next returnprofile, -1 if no more
		Static iNextId As Short
		Dim oFilesetup As FileSetup
		
		' In case we find no next, then return -1
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GoToNextReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_GoToNextReturn = -1
		
		'UPGRADE_NOTE: IsMissing() was changed to IsNothing(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"'
		If Not IsNothing(iSetNext) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object iSetNext. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			iNextId = iSetNext
			'UPGRADE_WARNING: Couldn't resolve default property of object iSetNext. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GoToNextReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Setup_GoToNextReturn = iSetNext
		Else
			For	Each oFilesetup In mCol
				If oFilesetup.Setup_Return = True And Val(CStr(oFilesetup.FileSetup_ID)) > iNextId Then
					iNextId = oFilesetup.FileSetup_ID
					'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GoToNextReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					Setup_GoToNextReturn = oFilesetup.FileSetup_ID
				End If
			Next oFilesetup
		End If
	End Function
	Public Function Setup_GotoCurrentSend() As Object
		' Return Filesetup_ID of the current sendprofile, the one we are dealing with at the moment
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GotoCurrentSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_GotoCurrentSend = iSetup_CurrentSend
		' Reset itemmarker for nextsend:
		Setup_GoToNextSend(iSetup_CurrentSend)
		
	End Function
	Public Function Setup_GotoCurrentReturn() As Object
		' Return Filesetup_ID of the current returnprofile, the one we are dealing with at the moment
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GotoCurrentReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_GotoCurrentReturn = iSetup_CurrentReturn
		' Reset itemmarker for nextreturn:
		Setup_GoToNextReturn(iSetup_CurrentReturn)
		
	End Function
	Public Function Setup_GotoInitSend() As Object
		' Return Filesetup_ID of the first initial sendprofile (normally only one)
		Dim oFilesetup As FileSetup
		
		For	Each oFilesetup In mCol
			If oFilesetup.Setup_InitSend = True Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GotoInitSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Setup_GotoInitSend = oFilesetup.FileSetup_ID
				Exit For
			End If
		Next oFilesetup
		' Reset itemmarker for nextsend:
		Setup_GoToNextSend(oFilesetup.FileSetup_ID)
		
	End Function
	Public Function Setup_GotoInitReturn() As Object
		' Return Filesetup_ID of the first initial returnprofile (usually only one)
		Dim oFilesetup As FileSetup
		
		For	Each oFilesetup In mCol
			If oFilesetup.Setup_InitReturn = True Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Setup_GotoInitReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Setup_GotoInitReturn = oFilesetup.FileSetup_ID
				Exit For
			End If
		Next oFilesetup
		' Reset itemmarker for nextreturn:
		Setup_GoToNextReturn(oFilesetup.FileSetup_ID)
		
	End Function
	Public Function Setup_SameFormat() As Object
		' Run throug all active filesetups, and check if format is the same
		Dim iEnum As BabelFiles.FileType
		Dim oFilesetup As FileSetup
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Setup_SameFormat. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Setup_SameFormat = True
		iEnum = 0
		
		For	Each oFilesetup In mCol
			If oFilesetup.Setup_Send = True Or oFilesetup.Setup_Return = True Then
				If iEnum = 0 Then
					' first filesetup
					iEnum = oFilesetup.Enum_ID
				Else
					If iEnum <> oFilesetup.Enum_ID Then
						'UPGRADE_WARNING: Couldn't resolve default property of object Setup_SameFormat. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						Setup_SameFormat = False
						Exit For
					End If
				End If
			End If
		Next oFilesetup
	End Function
	
	''Public Function Add(Key As String, Clients As Clients, Optional sKey As String) As Filename
	Public Function Add(Optional ByRef sKey As String = "") As FileSetup
		'create a new object
		Dim objNewMember As FileSetup
		objNewMember = New FileSetup
		sKey = Str(mCol.Count() + 1)
		
		'set the properties passed into the method
		''objNewMember.Key = Key
		''Set objNewMember.Filenames = Filenames
		''objNewMember.Key = Key
		''Set objNewMember.Clients = Clients
		
		If Len(sKey) = 0 Then
			mCol.Add(objNewMember)
		Else
			mCol.Add(objNewMember, sKey)
			objNewMember.Index = CObj(sKey)
		End If
		
		
		'return the object created
		Add = objNewMember
		'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		objNewMember = Nothing
		
	End Function
	
	
	Public Sub Remove(ByRef vntIndexKey As Object)
		'used when removing an element from the collection
		'vntIndexKey contains either the Index or Key, which is why
		'it is declared as a Variant
		'Syntax: x.Remove(xyz)
		
		mCol.Remove(vntIndexKey)
	End Sub
	
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		'creates the collection when this class is created
		mCol = New Collection
		'18.08.2008 - Commented some of the variables below
		'iSetup_InitSend = -1
		'iSetup_InitReturn = -1
		iSetup_CurrentSend = -1
		iSetup_CurrentReturn = -1
		Setup_GoToNextSend(-1)
		Setup_GoToNextReturn(-1)
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'destroys collection when this class is terminated
		'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mCol = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class
