﻿Option Strict Off
Option Explicit On
Module WriteGiroDirekt
    Dim sLine As String ' en output-linje
    Dim nFileSumAmount As Double, nFileNoRecords As Double
    Dim iPayNumber As Integer
    Dim bFileStartWritten As Boolean
    Public Function WriteGiroDirektFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String, ByVal sCompanyNo As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sBetalningskod As String
        Dim sTxt As String
        Dim sTmp As String
        ' XNET 10.05.2011
        Dim sUniqueID As String
        sUniqueID = ""

        Try

            oFs = New Scripting.FileSystemObject

            ' Did not write fileheader if we run more than once without leaving BabelBank
            If Len(Dir(sFilenameOut)) > 0 Then
                bFileStartWritten = True
            Else
                bFileStartWritten = False
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Only export statuscode 00 or 02. The others don't give any sense in GiroDirekt
                If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) _
                    Or oBabel.StatusCode = "02" Then
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                Else
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch
                End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!
                        '====================================================
                        ' Innledningspost MH00
                        sLine = WriteGiroDirekt_MH00(oBabel, sCompanyNo) 'MH00
                        '====================================================
                        oFile.WriteLine(sLine)

                        iPayNumber = 0
                        bFileStartWritten = True
                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next


                        If bExportoBatch Then
                            i = i + 1

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If


                                If bExportoPayment Then

                                    '======================================================
                                    'Betalning/Girering till PlusGirokonto
                                    sBetalningskod = ""
                                    sLine = WriteGiroDirekt_PI00(oPayment, sBetalningskod)
                                    oFile.WriteLine(sLine)
                                    'Avsändarens egna noteringar
                                    sLine = WriteGiroDirekt_BA00(oPayment)
                                    oFile.WriteLine(sLine)
                                    If Not EmptyString(oPayment.E_Name) Then
                                        If sBetalningskod = "02" Or sBetalningskod = "06" Or sBetalningskod = "07" Or sBetalningskod = "08" Or sBetalningskod = "09" Then
                                            ' Name/address
                                            sLine = WriteGiroDirekt_BE01(oPayment)
                                            oFile.WriteLine(sLine)
                                        End If
                                        If sBetalningskod = "02" Then
                                            sLine = WriteGiroDirekt_BE02(oPayment)
                                            oFile.WriteLine(sLine)
                                            sLine = WriteGiroDirekt_BE03(oPayment)
                                            oFile.WriteLine(sLine)
                                        End If
                                    End If
                                    '======================================================
                                    sTxt = ""
                                    ' XNET 10.05.2011
                                    sUniqueID = ""
                                    For Each oInvoice In oPayment.Invoices
                                        ' XNET 10.05.2011
                                        sUniqueID = oInvoice.Unique_Id
                                        For Each oFreeText In oInvoice.Freetexts
                                            sTxt = sTxt & oFreeText.Text
                                        Next oFreeText
                                    Next oInvoice

                                    ' NB! We do not write CNDB/CNKR records (yet)

                                    ' use BM99 if more than 12 characters freetext
                                    'If Len(sTxt) > 12 Then
                                    ' XNET 10.05.2011
                                    ' Mener at det alltid, utenom for OCR og "09" skal lages melding
                                    ' Derfor ny IF
                                    If Len(sTxt) > 0 And Len(sUniqueID) = 0 Then
                                        ' XNET 10.05.2011 changed from sTmp to sBetalningskod
                                        If sBetalningskod = "05" Then ' Bankgirot, only 25 in first line
                                            If Len(sTxt) > 25 Then
                                                ' XokNET 27.01.2011 added new parameter oPayment
                                                sLine = WriteGiroDirekt_BM99(oPayment, Left$(sTxt, 25), Mid$(sTxt, 26, 35))
                                                oFile.WriteLine(sLine)
                                                sTxt = Mid$(sTxt, 61)
                                            Else
                                                ' XokNET 27.01.2011 added new parameter oPayment
                                                sLine = WriteGiroDirekt_BM99(oPayment, Left$(sTxt, 25), "")
                                                oFile.WriteLine(sLine)
                                                sTxt = ""
                                            End If
                                        Else
                                            ' XNET 10.05.2011 added next If
                                            If Len(sTxt) < 13 And sBetalningskod = "09" Then
                                                ' do nothing, already written notification in PO01
                                                sTxt = ""
                                            Else
                                                ' other payments, 2 x 35 in each record
                                                If Len(sTxt) > 35 Then
                                                    ' XokNET 27.01.2011 added new parameter oPayment
                                                    sLine = WriteGiroDirekt_BM99(oPayment, Left$(sTxt, 35), Mid$(sTxt, 36, 35))
                                                    oFile.WriteLine(sLine)
                                                    sTxt = Mid$(sTxt, 71)
                                                Else
                                                    ' XokNET 27.01.2011 added new parameter oPayment
                                                    sLine = WriteGiroDirekt_BM99(oPayment, Left$(sTxt, 35), "")
                                                    oFile.WriteLine(sLine)
                                                    sTxt = ""
                                                End If
                                            End If
                                        End If
                                        ' upto 4 more BM99 records
                                        For i = 2 To 5
                                            If Len(Trim$(sTxt)) > 0 Then
                                                If Len(sTxt) > 35 Then
                                                    ' XokNET 27.01.2011 added new parameter oPayment
                                                    sLine = WriteGiroDirekt_BM99(oPayment, Left$(sTxt, 35), Mid$(sTxt, 36, 35))
                                                    oFile.WriteLine(sLine)
                                                    sTxt = Mid$(sTxt, 71)
                                                Else
                                                    ' XokNET 27.01.2011 added new parameter oPayment
                                                    sLine = WriteGiroDirekt_BM99(oPayment, Left$(sTxt, 35), "")
                                                    oFile.WriteLine(sLine)
                                                    sTxt = ""
                                                End If
                                            Else
                                                Exit For
                                            End If
                                        Next i
                                    End If
                                    oPayment.Exported = True
                                End If  'If bExportoPayment Then
                            Next ' payment

                        End If

                    Next 'batch
                End If
            Next 'Babelfile

            '=========================================================================
            sLine = WriteGiroDirekt_MT00(oBabelFiles(1), sAdditionalID, sCompanyNo)
            oFile.WriteLine(sLine)
            '=========================================================================

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGiroDirektFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGiroDirektFile = True

    End Function
    Function WriteGiroDirekt_MH00(ByVal oBabelFile As BabelFile, ByVal sCompanyNo As String) As String
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 0

        sLine = "MH00"              ' 1-4
        sLine = sLine & "PAYEXT"    ' 5-10
        sLine = sLine & "  "        ' 11-12
        If sCompanyNo <> "" Then
            sLine = sLine & PadRight(sCompanyNo, 10, " ")       '13-22
        Else
            sLine = sLine & PadRight(oBabelFile.IDENT_Sender, 10, " ")  '13-22
        End If
        sLine = sLine & Space(12)        ' 23-34

        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avsändarens Plusgironr
                ' Normally only one plusgirono pr file, then we can find accountno from first payment.
                sLine = sLine & PadRight(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 10, " ") '35-44

                ' Currency
                ' Assumes same currency for all payments !!!
                sLine = sLine & oBabelFile.Batches(1).Payments(1).MON_InvoiceCurrency   '45-47
                sLine = sLine & Space(6)        ' 48-53
                sLine = sLine & oBabelFile.Batches(1).Payments(1).MON_InvoiceCurrency   '54-56  Valutaficks ???
            End If
        End If

        'pad to 80 with 0
        sLine = PadLine(sLine, 80, " ")

        WriteGiroDirekt_MH00 = sLine

    End Function
    Function WriteGiroDirekt_PI00(ByVal oPayment As Payment, ByRef sTmp As String) As String
        Dim sLine As String

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = "PI00"        '1-4
        If oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro Or oPayment.PayCode = "190" Then
            ' 00 giro to Plusgirokonto
            sTmp = "00"
            sLine = sLine & "00"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Left$(oPayment.E_Account, 4) = "9962" And Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & "9962 "                                         '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        ElseIf Left$(oPayment.E_Account, 4) = "3300" Then
            'Nordea personkonto starts with 3300
            ' To Nordea account
            sTmp = "01"
            sLine = sLine & "01"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & Space(5)             '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        ElseIf EmptyString(oPayment.E_AccountType) Or oPayment.PayCode = "160" Then
            ' Utbetalingskort
            sTmp = "02"
            sLine = sLine & "02"    '5-6
            sLine = sLine & Space(5)              '7-11
            sLine = sLine & Space(13)  '12-24
        ElseIf oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro And oPayment.PayCode = "301" Then
            ' OCR til bankgiro
            sTmp = "04"
            sLine = sLine & "04"    '5-6
            sLine = sLine & Space(5)                                        '7-11
            sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
        ElseIf oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro And oPayment.PayCode <> "301" Then
            ' til bankgiro, uten OCR
            sTmp = "05"
            sLine = sLine & "05"    '5-6
            sLine = sLine & Space(5)                                        '7-11
            sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
        ElseIf oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount And oPayment.PayCode = "201" Then
            ' Pension, To bankkonto
            sTmp = "06"
            sLine = sLine & "06"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & Space(5)             '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        ElseIf oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount And oPayment.PayCode = "250" Then
            ' To bankkonto, no notification
            sTmp = "07"
            sLine = sLine & "07"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & Space(5)             '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        ElseIf oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount And oPayment.PayType = "S" Then
            ' Salary, To bankkonto
            sTmp = "08"
            sLine = sLine & "08"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & Space(5)             '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        ElseIf oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount And oPayment.PayCode = "150" Then
            ' To bankkonto, notification
            sTmp = "09"
            sLine = sLine & "09"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & Space(5)             '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        Else
            ' if no hit, set default to 09
            ' To bankkonto, notification
            sLine = sLine & "09"    '5-6
            ' assumes that there is no clearingno if accountno length is < 9
            If Len(oPayment.E_Account) > 8 Then
                sLine = sLine & Left$(oPayment.E_Account, 4) & " "              '7-11
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 13, " ")  '12-24
            Else
                sLine = sLine & Space(5)             '7-11
                sLine = sLine & PadRight(oPayment.E_Account, 13, " ")  '12-24
            End If
        End If
        ' Date
        sLine = sLine & oPayment.DATE_Payment       '25-32
        ' Amount
        sLine = sLine & PadLeft(Str(Math.Abs(oPayment.MON_InvoiceAmount)), 13, "0")  '33-45

        If sTmp = "00" Or sTmp = "04" Then
            'OCR
            If oPayment.Invoices.Count > 0 Then
                sLine = sLine & PadRight(oPayment.Invoices.Item(1).Unique_Id, 25, " ") '46-70
            End If
        ElseIf sTmp = "09" Then
            If oPayment.Invoices.Count > 0 Then
                If oPayment.Invoices.Item(1).Freetexts.Count > 0 Then
                    sLine = sLine & PadRight(Left$(oPayment.Invoices.Item(1).Freetexts.Item(1).Text, 12), 12, " ") '46-57
                End If
            End If
        End If
        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteGiroDirekt_PI00 = sLine

    End Function
    Function WriteGiroDirekt_BA00(ByVal oPayment As Payment) As String
        ' Avsändarens egna noteringar
        Dim sLine As String

        sLine = "BA00"        '1-4
        sLine = sLine & PadRight(oPayment.REF_Bank1, 18, " ")   ' 5-22
        sLine = sLine & Space(9)                                '23-31
        sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")     '32-66

        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteGiroDirekt_BA00 = sLine

    End Function
    Function WriteGiroDirekt_BE01(ByVal oPayment As Payment) As String
        ' Mottagarens navn/kontoinnehavarens navn
        Dim sLine As String

        sLine = "BE01"        '1-4
        sLine = sLine & Space(18)                               ' 5-22
        sLine = sLine & PadRight(oPayment.E_Name, 35, " ")     '23-57

        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteGiroDirekt_BE01 = sLine

    End Function
    Function WriteGiroDirekt_BE02(ByVal oPayment As Payment) As String
        ' Mottagarens ortsadress
        Dim sLine As String

        sLine = "BE02"        '1-4
        sLine = sLine & PadRight(oPayment.E_Zip, 9, " ")     ' 5-13
        sLine = sLine & PadRight(oPayment.E_City, 25, " ")   '14-38

        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteGiroDirekt_BE02 = sLine

    End Function
    Function WriteGiroDirekt_BE03(ByVal oPayment As Payment) As String
        ' Mottagarens adress
        Dim sLine As String

        sLine = "BE03"        '1-4
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ")      ' 5-39
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ")      '40-74

        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteGiroDirekt_BE03 = sLine

    End Function
    Function WriteGiroDirekt_BM99(ByVal oPayment As vbBabel.Payment, ByVal sTxt1 As String, ByVal sTxt2 As String) As String
        ' Meddelande
        Dim sLine As String

        sLine = "BM99"        '1-4
        ' XNET 27.01.2011, added next If, because max 12 chars for Kontoisetting
        If oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount And oPayment.PayCode = "150" Then
            sLine = sLine & PadRight(Left$(sTxt1, 12), 35, " ")     ' 5-39
            sLine = sLine & Space(35)      '40-74
        Else
            sLine = sLine & PadRight(sTxt1, 35, " ")      ' 5-39
            sLine = sLine & PadRight(sTxt2, 35, " ")      '40-74
        End If

        ' Pad to 80
        sLine = PadRight(sLine, 80, " ")

        WriteGiroDirekt_BM99 = sLine

    End Function
    Function WriteGiroDirekt_MT00(ByVal oBabelFile As BabelFile, ByVal sAdditionalID As String, ByVal sVB_CompanyNo As String) As String
        Dim sLine As String

        sLine = "MT00"                 ' 1-4
        sLine = sLine & Space(25)      ' 5-29
        sLine = sLine & PadLeft(Str(nFileNoRecords), 7, "0")            '30-36 antall betalningar
        sLine = sLine & PadLeft(Str(nFileSumAmount), 15, "0")           '37-51 Total amount in file

        'XNET 06.12.2010 changed from here and down
        If sAdditionalID <> "" Then
            sLine = sLine & PadLeft(sAdditionalID, 8, " ")              '52-59 Användarnavn
        Else
            sLine = sLine & Space(8)         '52-59 Användarnavn,normally not in use
        End If
        sLine = sLine & Space(6)               '60-65 Certifikat1 not in use
        sLine = sLine & Space(8)        ' 66-73 Användarnavn2, not in use so far
        sLine = sLine & Space(6)        ' 74-79 Certifikat2, not in use so far
        sLine = sLine & Space(1)        ' 80

        WriteGiroDirekt_MT00 = sLine

    End Function


End Module
