Option Strict Off
Option Explicit On
Module WriteDanskeBankTeleService
	

    Function WriteDanskeBankFI(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer

        Try
            lCounter = 0
            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        ' 01.02.05 Added paycode="510"
                        If IsOCR((oPayment.PayCode)) Then
                            'If oPayment.PayCode = "510" Then
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            ' Added 29.03.05
                            ' Do not export UDUS;
                            If oPayment.PayCode = "180" Then
                                oPayment.Exported = True
                            Else
                                oPayment.Exported = False
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments


                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                ' 01.02.05 Added paycode="510"
                                'If oPayment.PayCode = "510" Then
                                If IsOCR((oPayment.PayCode)) Then

                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                Else
                                    ' Added 08.09.05
                                    ' Do not export UDUS;
                                    If oPayment.PayCode = "180" Then
                                        oPayment.Exported = True
                                    Else
                                        oPayment.Exported = False
                                    End If

                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' 01.02.05 Added paycode="510"
                                    'If oPayment.PayCode = "510" Then
                                    If IsOCR((oPayment.PayCode)) Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If oPayment.REF_Own = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then


                                    lCounter = lCounter + 1
                                    '-------- Write content of each payment to file---------
                                    sLine = Chr(34) & "CMM" & Chr(34) & "," '1
                                    sLine = sLine & VB6.Format(Now, "YYMMDDhhmm") & "," '2 Dato/tid
                                    sLine = sLine & VB6.Format(Now, "YYMMDD") & PadLeft(LTrim(Str(lCounter)), 4, "0") & "," ' 3 Leverance ID
                                    sLine = sLine & PadLeft(oPayment.I_Name, 8, "0") & "," '4 Kreditornummer
                                    sLine = sLine & Right(oPayment.I_Account, 10) & "," '5 Kontonummer
                                    ' 27.06.05 -  New FI-Browser has no Invoice-level
                                    If oPayment.Invoices.Count > 0 Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & PadLeft(Mid(oPayment.Invoices(1).Unique_Id, 3), 19, "0") & "," ' Betalingsid (fjern kortart, 2 pos)
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().MON_InvoiceAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & Replace(VB6.Format(oPayment.Invoices(1).MON_InvoiceAmount / 100, "####0.00"), ",", ".") & "," ' Bel�p, inkl. des punktum
                                    Else
                                        ' If FI-Browser, FIK set in oPayment.Ref_Own
                                        sLine = sLine & PadLeft(Mid(oPayment.REF_Own, 3), 19, "0") & "," ' Betalingsid (fjern kortart, 2 pos)
                                        sLine = sLine & Replace(VB6.Format(oPayment.MON_InvoiceAmount / 100, "####0.00"), ",", ".") & "," ' Bel�p, inkl. des punktum
                                    End If

                                    sLine = sLine & oPayment.DATE_Value & "," ' Indbetalingsdato, YYYYMMDD
                                    sLine = sLine & oPayment.DATE_Payment ' Bokf�ringsdato, YYYYMMDD
                                    ' If Kortart 71, no more, else more fields

                                    ' 27.06.05 -  New FI-Browser has no Invoice-level
                                    If oPayment.Invoices.Count > 0 Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If Left(oPayment.Invoices(1).Unique_Id, 2) <> "71" Then
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            sLine = sLine & "," & oPayment.Invoices(1).Unique_Id
                                            sLine = sLine & oPayment.E_Name & ","
                                            sLine = sLine & oPayment.E_Adr1 & ","
                                            sLine = sLine & oPayment.E_Adr2 & ","
                                            sLine = sLine & oPayment.E_Adr3
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices(1).Freetexts. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            For i = 1 To oPayment.Invoices(1).Freetexts.Count
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Freetexts. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                sLine = sLine & "," & Left(oPayment.Invoices(1).Freetexts(i).Text, 35)
                                                If i > 56 Then
                                                    Exit For
                                                End If
                                            Next i
                                        End If
                                    Else
                                        ' No Invocelevel. FIK in oPayment.Ref_Own
                                        If Left(oPayment.REF_Own, 2) <> "71" Then
                                            sLine = sLine & "," & oPayment.REF_Own
                                            sLine = sLine & oPayment.E_Name & ","
                                            sLine = sLine & oPayment.E_Adr1 & ","
                                            sLine = sLine & oPayment.E_Adr2 & ","
                                            sLine = sLine & oPayment.E_Adr3
                                        End If
                                    End If
                                    oFile.WriteLine(sLine)
                                    oPayment.Exported = True
                                Else
                                    ' Do not export UDUS;
                                    If oPayment.PayCode = "180" Then
                                        oPayment.Exported = True
                                    Else
                                        oPayment.Exported = False
                                    End If
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDanskeBankFI" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDanskeBankFI = True

    End Function
End Module
