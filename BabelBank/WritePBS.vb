Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module WritePBS
	
	Dim sLine As String ' en output-linje
	Dim i As Double
	Dim oBatch As Batch
	' Vi m� dimme noen variable for
	Dim nSectionSumAmount As Double
	Dim nType41, nType40, nType30, nType20, nTotalType30, nTotalType40, nTotalType41 As Double
    Function WritePBSFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String) As Object
        'bFalsePBS, False = not an original PBS-file, i.e. CREMUL
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim ofreetext As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord090 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim sContractNo As String
        Dim bWriteBatch As Boolean
        Dim i40Counter As Short
        Dim i41Counter As Short

        nType20 = 0
        nType30 = 0
        nType40 = 0
        nType41 = 0
        nTotalType30 = 0
        nTotalType40 = 0
        nTotalType41 = 0
        '----------
        Try

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            sContractNo = ""

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 090-record, and save the counters

            oFs = New Scripting.FileSystemObject

            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord090 = False
                nExistingLines = 0
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 090-line
                ' Store the number of lines until the 090-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 5) = "FI090" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord090 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                If Not bFoundRecord090 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    MsgBox("The existing file is not an PBS-file")
                    'UPGRADE_WARNING: Couldn't resolve default property of object WritePBSFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    WritePBSFile = False
                    Exit Function
                    'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 090-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop

                    sLine = oFileRead.ReadLine

                    'Kill'em all
                    oFileRead.Close()
                    'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileRead = Nothing
                    oFileWrite.Close()
                    'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            bWriteBatch = True
            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'If oPayment.I_Account = sI_Account Then
                        ' 01.10.2018 � changed above If to:
                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                            If Len(oPayment.VB_ClientNo) > 0 Then
                                If oPayment.VB_ClientNo = sClientNo Then
                                    bExportoBabel = True
                                End If
                            Else
                                bExportoBabel = True
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_PBSFileStart010(oBabel, sAdditionalID)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False And IsOCR((oPayment.PayCode)) Then
                                'oPayment.PayCode = "510" Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            Else
                                If oPayment.PayCode = "180" Then
                                    oPayment.Exported = True
                                Else
                                    oPayment.Exported = False
                                End If

                            End If
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            i = i + 1

                            ' New 04.07.05 - because we may have had empty batches before
                            If bWriteBatch Then
                                sLine = Wr_PBSSectionStart020(oBatch)
                                oFile.WriteLine((sLine))
                            End If

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                ' Changed 01.02.05
                                ' Only FI-payments to export (501)
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False And IsOCR((oPayment.PayCode)) Then
                                    'oPayment.PayCode = "510" Then
                                    'If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If
                                If bExportoPayment Then
                                    'j = j + 1 moved further down 04.07.05

                                    For Each oInvoice In oPayment.Invoices
                                        ' Add to transactionno
                                        sLine = Wr_AdviceStart030(oPayment, oInvoice)
                                        oFile.WriteLine((sLine))
                                        If Left(oInvoice.Unique_Id, 2) = "73" Then
                                            ' Added 02.08.2007
                                            ' For FI 73, write Name/Address
                                            i40Counter = 0
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Name), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString((oPayment.E_Adr1)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Adr1), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString((oPayment.E_Adr2)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Adr2), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString((oPayment.E_Adr3)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Adr3), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                        End If ' Left$(oInvoice.Unique_Id, 2) = "73" Then

                                        ' for FI73 and 75, write notificationrecord(s) (Added 02.08.08
                                        If Left(oInvoice.Unique_Id, 2) = "73" Or Left(oInvoice.Unique_Id, 2) = "75" Then
                                            i41Counter = 0
                                            For Each ofreetext In oInvoice.Freetexts
                                                i41Counter = i41Counter + 1
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.Freetexts().Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                sLine = Wr_AdviceMeddelelse041(oPayment, oInvoice, oInvoice.Freetexts(i41Counter).Text, i41Counter)
                                                oFile.WriteLine((sLine))
                                            Next ofreetext
                                        End If 'Left$(oInvoice.Unique_Id, 2) = "73" Or Left$(oInvoice.Unique_Id, 2) = "75" Then

                                        oPayment.Exported = True
                                        j = j + 1
                                    Next oInvoice 'invoice
                                    ' New 27.06.05 - due to no inovoice-level from DnBNOR FI-Browser
                                    If oPayment.Invoices.Count = 0 Then
                                        sLine = Wr_AdviceStart030(oPayment, oInvoice)
                                        oFile.WriteLine((sLine))

                                        ' For FI 73, write Name/Address
                                        ' Added 02.08.2007
                                        If Left(oPayment.REF_Own, 2) = "73" Then
                                            i40Counter = 0
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Name), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString((oPayment.E_Adr1)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Adr1), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString((oPayment.E_Adr2)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Adr2), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString((oPayment.E_Adr3)) Then
                                                i40Counter = i40Counter + 1
                                                sLine = Wr_AdviceName040(oPayment, oInvoice, (oPayment.E_Adr3), i40Counter)
                                                oFile.WriteLine((sLine))
                                            End If
                                        End If 'Left$(oPayment.REF_Own, 2) = "73" Then

                                        oPayment.Exported = True
                                        j = j + 1
                                    End If
                                Else
                                    ' Do not export UDUS;
                                    If oPayment.PayCode = "180" Then
                                        oPayment.Exported = True
                                    Else
                                        oPayment.Exported = False
                                    End If

                                End If

                            Next oPayment ' payment

                            ' New 04.07.05 - because we may have empty batches before
                            If j > 0 Then
                                sLine = Wr_PBSSectionEnd080(oBatch)
                                oFile.WriteLine((sLine))
                                bWriteBatch = True
                            Else
                                bWriteBatch = False
                            End If
                            If StringToDate((oBatch.DATE_Production)) > dProductionDate Then
                                dProductionDate = StringToDate((oBatch.DATE_Production))
                            End If

                        End If
                    Next oBatch 'batch

                End If

            Next oBabel


            If nType20 > 0 Then
                sLine = Wr_PBSFileEnd090(oBabelFiles(1), sAdditionalID)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WritePBSFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WritePBSFile = True

    End Function
    Public Function Wr_AdviceStart030(ByRef oPayment As Payment, ByRef oInvoice As Invoice) As String
        Dim sLine As String

        nType30 = nType30 + 1
        sLine = "FI030"

        ' Indbetalingsdato, YYYYMMDD
        sLine = sLine & oPayment.DATE_Value

        '27.06.05 - FI-project changed. Now longer sets Invoice when called from FI-Browser
        If oPayment.Invoices.Count > 0 Then
            'Kortartkode - 2 first pos in oInvoice.Unique_ID
            sLine = sLine & Left(oInvoice.Unique_Id, 2)
            ' Betaler-id (Debitor Identification)
            sLine = sLine & PadLeft(Mid(SplitFI((oInvoice.Unique_Id), 2), 3), 19, "0")
        Else
            ' If no invoice, FIK set in oPayment.Ref_Own
            'Kortartkode - 2 first pos in FIK
            sLine = sLine & Left(oPayment.REF_Own, 2)
            ' Betaler-id (Debitor Identification)
            sLine = sLine & PadLeft(Mid(SplitFI((oPayment.REF_Own), 2), 3), 19, "0")
        End If
        ' Arkivnummer
        sLine = sLine & PadRight(oPayment.REF_Bank1, 22, " ")
        ' Bogf�ringsdato, YYYYMMDD
        sLine = sLine & oPayment.DATE_Payment
        ' Kreditbel�p
        '27.06.05 - FI-project changed. Now longer sets Invoice when called from FI-Browser
        If oPayment.Invoices.Count > 0 Then
            sLine = sLine & PadLeft(Str(oInvoice.MON_TransferredAmount), 15, "0") ' Amount
            nSectionSumAmount = nSectionSumAmount + oInvoice.MON_TransferredAmount
        Else
            ' Assumes amount from Payment equals FIK-Amount
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0") ' Amount
            nSectionSumAmount = nSectionSumAmount + oPayment.MON_TransferredAmount
        End If
        ' Sumpost reference - (we do not have this info in BabelBank for DnBNOR TBI)
        sLine = sLine & PadRight(oPayment.REF_Bank2, 22, " ")
        ' Gebyrtype
        ' Hardcode CC - no errors
        sLine = sLine & "CC"
        ' Gebyrbel�p - Hardcode zero
        sLine = sLine & "000000000"
        'Tilbagef�rsel, hadcode "N" = Ordinary payment
        sLine = sLine & "N"
        sLine = sLine & Space(15)

        Wr_AdviceStart030 = sLine

    End Function
    Public Function Wr_AdviceName040(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sNameAdr As String, ByRef nRecNo As Short) As String
        ' For FI 73 only
        Dim sLine As String

        sLine = "FI04073"
        ' Running number of nameadr records, from 01 to 05
        sLine = sLine & PadLeft(Trim(Str(nRecNo)), 2, "0")
        ' Arkivnummer
        sLine = sLine & PadRight(oPayment.REF_Bank1, 22, " ")
        sLine = sLine & PadRight(sNameAdr, 35, " ")

        sLine = sLine & Space(62)

        Wr_AdviceName040 = sLine

    End Function
    Public Function Wr_AdviceMeddelelse041(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sText As String, ByRef nRecNo As Short) As String
        Dim sLine As String

        sLine = "FI041"
        '27.06.05 - FI-project changed. Now longer sets Invoice when called from FI-Browser
        If oPayment.Invoices.Count > 0 Then
            'Kortartkode - 2 first pos in oInvoice.Unique_ID
            sLine = sLine & Left(oInvoice.Unique_Id, 2)
        Else
            ' If no invoice, FIK set in oPayment.Ref_Own
            'Kortartkode - 2 first pos in FIK
            sLine = sLine & Left(oPayment.REF_Own, 2)
        End If
        ' Running number of nameadr records, from 01 to 41
        sLine = sLine & PadLeft(Trim(Str(nRecNo)), 2, "0")
        ' Arkivnummer
        sLine = sLine & PadRight(oPayment.REF_Bank1, 22, " ")
        sLine = sLine & PadRight(sText, 35, " ")

        sLine = sLine & Space(62)

        Wr_AdviceMeddelelse041 = sLine

    End Function
    Function Wr_PBSFileStart010(ByRef oBabelFile As BabelFile, ByRef sAdditionalID As String) As String
        Dim sLine As String
        Dim sTmp As String

        sLine = "FI010"
        If UCase(oBabelFile.Special) = "VIATRAVEL" Then
            sLine = sLine & "13522197" ' dataavsender Nordea for ViaTravel
        ElseIf Len(oBabelFile.IDENT_Sender) = 0 Then
            sLine = sLine & "00000000"
        Else
            sLine = sLine & PadRight(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' dataavsender
        End If
        If UCase(oBabelFile.Special) = "VIATRAVEL" Then
            sLine = sLine & "0026524857" ' datamottaker for ViaTravel
        ElseIf sAdditionalID <> "" Then
            sLine = sLine & PadRight(sAdditionalID, 10, "0")
        Else
            sLine = sLine & PadRight(Mid(oBabelFile.IDENT_Receiver, 1, 8), 10, "0") ' datamottaker
        End If

        If Len(oBabelFile.File_id) = 0 Then
            ' create a runningnumber based on date;
            sTmp = VB6.Format(Month(Now), "00") & VB6.Format(VB.Day(Now), "00") & VB6.Format(Hour(Now), "00") & VB6.Format(Minute(Now), "00") & VB6.Format(Second(Now), "00")
            If UCase(oBabelFile.Special) = "VIATRAVEL" Then
                ' ViaTravel can handle only 5 digits (?)
                sLine = sLine & Space(5) & Left(sTmp, 5)
            Else
                sLine = sLine & PadRight(sTmp, 10, " ")
            End If
        ElseIf Len(oBabelFile.File_id) < 11 Then
            sLine = sLine & PadRight(Mid(oBabelFile.File_id, 1, 10), 10, " ") ' Leverancenummer
        Else
            sLine = sLine & Right(oBabelFile.File_id, 10)
        End If
        If UCase(oBabelFile.Special) = "VIATRAVEL" Then
            sLine = sLine & Space(5) & Left(sTmp, 5)
        Else
            sLine = sLine & "0000000000" ' LeveranceID
        End If
        sLine = sLine & VB6.Format(Now, "YYYYMMDD")
        sLine = sLine & VB6.Format(Now, "HHMM")
        sLine = sLine & "01" ' Formatversion
        sLine = sLine & "P" ' Production
        sLine = sLine & Space(70)

        Wr_PBSFileStart010 = sLine

    End Function
    Public Function Wr_PBSFileEnd090(ByRef oBabelFile As BabelFile, ByRef sAdditionalID As String) As String
        Dim sLine As String
        Dim sTmp As String

        sLine = "FI090"
        If UCase(oBabelFile.Special) = "VIATRAVEL" Then
            sLine = sLine & "13522197" ' dataavsender Nordea for ViaTravel
        ElseIf Len(oBabelFile.IDENT_Sender) = 0 Then
            sLine = sLine & "00000000"
        Else
            sLine = sLine & PadRight(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' dataavsender
        End If
        If UCase(oBabelFile.Special) = "VIATRAVEL" Then
            sLine = sLine & "0026524857" ' datamottaker for ViaTravel
        ElseIf sAdditionalID <> "" Then
            sLine = sLine & PadRight(sAdditionalID, 10, "0")
        Else
            sLine = sLine & PadRight(Mid(oBabelFile.IDENT_Receiver, 1, 8), 10, "0") ' datamottaker
        End If

        If Len(oBabelFile.File_id) = 0 Then
            ' create a runningnumber based on date;
            sTmp = VB6.Format(Month(Now), "00") & VB6.Format(VB.Day(Now), "00") & VB6.Format(Hour(Now), "00") & VB6.Format(Minute(Now), "00") & VB6.Format(Second(Now), "00")
            If UCase(oBabelFile.Special) = "VIATRAVEL" Then
                ' ViaTravel can handle only 5 digits (?)
                sLine = sLine & Space(5) & Left(sTmp, 5)
            Else
                sLine = sLine & PadRight(sTmp, 10, " ")
            End If
        ElseIf Len(oBabelFile.File_id) < 11 Then
            sLine = sLine & PadRight(Mid(oBabelFile.File_id, 1, 10), 10, " ") ' Leverancenummer
        Else
            sLine = sLine & Right(oBabelFile.File_id, 10)
        End If

        ' Antal sektioner
        sLine = sLine & PadLeft(Str(nType20), 11, "0")
        ' Antal betalingsrecords
        sLine = sLine & PadLeft(Str(nTotalType30), 11, "0")
        ' Antal tekstrecords
        sLine = sLine & PadLeft(Str(nTotalType40), 11, "0")
        ' Antal tekstrecords
        sLine = sLine & PadLeft(Str(nTotalType41), 11, "0")

        sLine = sLine & Space(51)

        Wr_PBSFileEnd090 = sLine


    End Function
    Function Wr_PBSSectionStart020(ByRef oBatch As Batch) As String
        Dim sLine As String
        Dim sKreditorNo As String

        ' Must have at least one payment with Kreditorno
        For i = 1 To oBatch.Payments.Count
            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Name. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sKreditorNo = oBatch.Payments(i).I_Name
            If Not EmptyString(sKreditorNo) Then
                Exit For
            End If
        Next

        nSectionSumAmount = 0
        nType20 = nType20 + 1
        nType30 = 0
        nType40 = 0
        nType41 = 0

        sLine = "FI020"
        ' Kreditornummer
        sLine = sLine & PadRight(sKreditorNo, 8, "0")
        ' Kontonummer
        If oBatch.Payments.Count > 0 Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & PadRight(oBatch.Payments(1).I_Account, 14, "0")
        Else
            ' Should not happen ....
            sLine = sLine & PadRight("", 14, "0")
        End If
        ' Gebyrkonto, not in use
        sLine = sLine & PadRight("", 14, "0")
        ' Sektionsnummer, not in use
        sLine = sLine & PadRight("", 10, "0")
        ' Dannelsesdato (Creation Date)
        sLine = sLine & VB6.Format(Now, "YYYYMMDD")
        ' Dannelsestid
        sLine = sLine & VB6.Format(Now, "HHMM")
        ' Formatversion
        sLine = sLine & "02"
        ' Udskriftnr, not in use
        sLine = sLine & "0000"
        ' ???, not in use
        sLine = sLine & "000"
        ' Filler
        sLine = sLine & Space(56)


        Wr_PBSSectionStart020 = sLine

    End Function
	
	Function Wr_PBSSectionEnd080(ByRef oBatch As Batch) As String
		Dim sLine As String
		Dim sKreditorNo As String
		
		nTotalType30 = nTotalType30 + nType30
		nTotalType40 = nTotalType40 + nType40
		nTotalType41 = nTotalType41 + nType41
		
		'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Name. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sKreditorNo = oBatch.Payments(1).I_Name
		
		sLine = "FI080"
		' Kreditornummer
		sLine = sLine & PadRight(sKreditorNo, 8, " ")
		' Antal betalingsrecords
		sLine = sLine & PadLeft(Str(nType30), 11, "0") ' No of paymenttranstransactions in batch
		' Opsummerede betalingsrecords
		sLine = sLine & PadLeft(Str(nSectionSumAmount), 15, "0") ' Total amount in batch
		' Opsummerede gebyrbel�b, not in use
		sLine = sLine & PadLeft("0", 15, "0") ' Total amount in batch
		' Antall 40 tekstrecords
		sLine = sLine & PadLeft(Str(nType40), 11, "0")
		' Antall 41 tekstrecords
		sLine = sLine & PadLeft(Str(nType41), 11, "0")
		sLine = sLine & Space(52)
		
		Wr_PBSSectionEnd080 = sLine
		
	End Function
End Module
