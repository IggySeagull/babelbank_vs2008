Option Strict Off
Option Explicit On
Module ReadRecords
    ' ReadRecords.bas
	' Brukes til funktioner som leser fra fil og gj�r noen kontroller
	' Inndelt i funksjoner pr format
	
	Private i As Short
	Private sImportLineBuffer As String ' one line
	Private sImportRecord As String ' one record
	Function ReadTelepayRecord(ByRef oFile As Scripting.TextStream, Optional ByRef bBreakOn80 As Boolean = False) As String ', oProfile As Profile) ' As Object)
		' Read one record from a Telepayfile
		' Usually four lines of 80 chars, in some cases one line of 320 chars
        Dim bFirstLineEmpty As Boolean

		i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False And i < 5)
            bFirstLineEmpty = False
            'Read one line

            ' New 27.03.06
            ' Added the possibilty to import lines without CR or LF, and assume 80 chars pr line
            If bBreakOn80 Then
                sImportLineBuffer = oFile.Read(80)
            Else
                sImportLineBuffer = oFile.ReadLine()

                ' 15.05.2020 - In some cases we need to import UTF-8 files.
                '              Must remove the first characters ﻿ from file.
                If InStr(sImportLineBuffer, "﻿") > 0 Then
                    sImportLineBuffer = Replace(sImportLineBuffer, "﻿", "")
                End If

                ' 25.09.2017
                ' Some Unix-based files or files from AS400 have JCL-cards in first line.
                ' FMS01NOXQD101    NOCBK001                                                       
                ' Do not import lines starting with FMS01
                If sImportLineBuffer.Length > 4 Then
                    If sImportLineBuffer.Substring(0, 5) = "FMS01" Then
                        sImportLineBuffer = oFile.ReadLine()
                    End If
                End If
                'Changed 28.03.2007 by Kjell to remove vbcr from importfile for Vingcard
                sImportLineBuffer = Replace(Replace(sImportLineBuffer, vbCr, ""), vbLf, "")
                End If
                'Some systems doesnt pad blank lines with spaces
                If Len(sImportLineBuffer) = 0 Then
                    'If it is the first line we continue reading and treats the next line as the first
                    ' line. This will also solve the problem when some files have blank lines in the
                    ' end before EOF like Jan-Petters ancient editor: BRIEF.
                    If i = 1 Then
                        bFirstLineEmpty = True
                    Else
                        sImportLineBuffer = Space(80)
                    End If
                End If
                'Test if end of file mark

                If bFirstLineEmpty Then
                    sImportLineBuffer = ""
                ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    If i = 1 Then
                        'Check first line
                        '1) Must start with AH - if not no Telepayfile:
                        If Mid(sImportLineBuffer, 1, 2) <> "AH" Then
                            'babelbank_error: Feil i format - Ikke korrekt Telepayfil!
                            sImportRecord = ""
                            If Not oFile.AtEndOfStream Then ' New test by JanP 28.02.02, due to problems at end of files
                                'Error # 10012
                                Call BabelImportError((BabelFiles.FileType.Telepay2), FilenameImported(), sImportLineBuffer, "10012")
                            End If
                        End If

                        '2) Some systems writes on line of 320 chars for each record
                        ' read only one line
                        If Len(sImportLineBuffer) = 320 Then
                            Exit Do
                        End If
                    End If

                    'Check if padded to 80 char lines:
                    If Len(sImportLineBuffer) < 80 Then
                        ' Must pad with trailing blanks
                        sImportLineBuffer = Mid(sImportLineBuffer & Space(80), 1, 80)
                    Else
                        ' Alpharma special TBI
                        ' New 12.10.04 by JanP
                        If Len(sImportLineBuffer) = 128 And Trim(Mid(sImportLineBuffer, 81, 48)) = "" Then
                            sImportLineBuffer = Mid(sImportLineBuffer, 1, 80)
                        End If
                    End If

                    ' 4 lines for each record
                    sImportRecord = sImportRecord & sImportLineBuffer
                    i = i + 1

                End If
        Loop

        '
        ' FIXed by JanP 25.10.01
        ' Next line is new - due to empty simportlinebuffer (ref Bohus)
        If Len(sImportLineBuffer) > 0 Then
            'Test if end of file mark
            If Asc(Mid(sImportLineBuffer, 1, 1)) <> 26 Then
                'Check for BETFOR - if not no Telepayfile:
                If Mid(sImportRecord, 41, 6) <> "BETFOR" Then
                    If Not oFile.AtEndOfStream Then ' New test by JanP 28.02.02, due to problems at end of files
                        'Error # 10011
                        'New_BabelImportError((BabelFiles.FileType.Telepay), FilenameImported(), sImportRecord, "10011")
                        'Throw New Exception(New_BabelImportError((BabelFiles.FileType.Telepay), "10011")) ' - Testing Try ... Catch
                        'Throw New Exception (BabelFiles.FileType.Telepay), FilenameImported(), sImportRecord, "10011") ' - Testing Try ... Catch
                        Call BabelImportError((BabelFiles.FileType.Telepay), FilenameImported(), sImportRecord, "10011")
                    End If
                End If
            End If
        End If
        ReadTelepayRecord = sImportRecord

    End Function
    ' XNET 06.07.2012 special for XELLIA
    Function ReadTelepayXelliaRecord(ByVal oFile As Scripting.TextStream, Optional ByVal bBreakOn80 As Boolean = False) As String ', oProfile As Profile) ' As Object)
        ' Read one record from a Telepayfile
        ' Special for Xellia, with extra chars at end
        'AH100TBIU1019000001                   04BETFOR000000000010410410      2969                                                      ,OW00001056,1       ,F04572NO2
        '1019          VERSJON001          00000000000 00000000000000000000000000                                                        ,OW00001056,2       ,F04572NO2
        '                                                                                                                                ,OW00001056,3       ,F04572NO2
        '                                                                                                                                ,OW00001056,4       ,F04572NO2
        'AH100TBIU1019000002                   04BETFOR0100000000104000600080042970                                                      ,OW00001056,5       ,F04572NO2
        '051020215402                           EURBENOUR                               0                                                ,OW00001056,6       ,F04572NO2

        i = 1
        sImportLineBuffer = vbNullString
        sImportRecord = vbNullString

        Do While (oFile.AtEndOfStream = False And i < 5)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            ' remove those extra columns at end;
            sImportLineBuffer = Left$(sImportLineBuffer, 80)

            If i = 1 Then
                'Check first line
                '1) Must start with AH - if not no Telepayfile:
                If Mid$(sImportLineBuffer, 1, 2) <> "AH" Then
                    'babelbank_error: Feil i format - Ikke korrekt Telepayfil!
                    sImportRecord = vbNullString
                    If Not oFile.AtEndOfStream Then ' New test by JanP 28.02.02, due to problems at end of files
                        'Error # 10012
                        Call BabelImportError(vbBabel.BabelFiles.FileType.Telepay2, FilenameImported(), sImportLineBuffer, "10012")
                    End If
                End If

            End If

            ' 4 lines for each record
            sImportRecord = sImportRecord & sImportLineBuffer
            i = i + 1

        Loop


        ReadTelepayXelliaRecord = sImportRecord

    End Function
    Function ReadTelepayPlusRecord(ByVal oFile As Scripting.TextStream) As String
        ' Read one record from a Telepay+file
        ' XNET 15.11.2010, several changes in this function
        ' Six OR ten lines of 80 chars
        Dim iNoOfLines As Integer

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        'Read first line in record
        sImportLineBuffer = oFile.ReadLine()
        'Check first line
        '1) Must start with AH - if not no Telepayfile:
        If Mid$(sImportLineBuffer, 1, 2) <> "AH" And Mid$(sImportLineBuffer, 6, 2) <> "TK" Then
            'babelbank_error: Feil i format - Ikke korrekt Telepay plus fil!
            sImportRecord = ""
            If Not oFile.AtEndOfStream Then ' New test by JanP 28.02.02, due to problems at end of files
                'Error # 10012
                Call BabelImportError(vbBabel.BabelFiles.FileType.TelepayPlus, FilenameImported(), sImportLineBuffer, "10012")
            End If
        End If
        iNoOfLines = Val(Mid$(sImportLineBuffer, 39, 2))

        Do While (oFile.AtEndOfStream = False And i < iNoOfLines + 1)
            If i > 1 Then
                'Read one line
                sImportLineBuffer = oFile.ReadLine()
            End If

            'Check if padded to 80 char lines:
            If Len(sImportLineBuffer) < 80 Then
                ' Must pad with trailing blanks
                sImportLineBuffer = Mid$(sImportLineBuffer & Space(80), 1, 80)
            End If

            ' 6 or 10 lines for each record (opposite to ordinary Telepay which has 4 records
            sImportRecord = sImportRecord & sImportLineBuffer
            i = i + 1

        Loop

        ReadTelepayPlusRecord = sImportRecord

    End Function
    Function ReadDirremRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a Dirrem-file
        ' 80 chars

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with NY00 or NY04 - if not no Dirremfile!
                If Mid(sImportLineBuffer, 1, 4) <> "NY00" And Mid(sImportLineBuffer, 1, 4) <> "NY04" Then
                    ' 16.09.05
                    ' Added possibility to import files with both Avtalegiro krav and Dir.rem
                    ' 07.01.05 Also added Autogiro
                    If Mid(sImportLineBuffer, 1, 4) <> "NY21" And Mid(sImportLineBuffer, 1, 4) <> "NY01" Then
                        'babelbank_error: Feil i format - Ikke korrekt Dirrekte remitteringsfil!
                        sImportRecord = ""
                        'Error # 10012
                        Call BabelImportError((BabelFiles.FileType.Dirrem), FilenameImported(), sImportLineBuffer, "10012")
                    End If
                End If

                'Check if padded to 80 char lines:
                If Len(sImportLineBuffer) < 80 Then
                    ' Must pad with trailing blanks
                    sImportLineBuffer = Mid(sImportLineBuffer & Space(80), 1, 80)
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadDirremRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadDirremRecord = sImportRecord

    End Function
    Function ReadBOLSRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a BOLS-file
        ' 60 chars

        'Read one line
        Do
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                ' Continue

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with 11, 13, 15, 17 or 19 - if not no BOLSfile!
                If Not Mid(sImportLineBuffer, 1, 2) = "11" And Not Mid(sImportLineBuffer, 1, 2) = "13" And Not Mid(sImportLineBuffer, 1, 2) = "15" And Not Mid(sImportLineBuffer, 1, 2) = "17" And Not Mid(sImportLineBuffer, 1, 2) = "19" Then
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError((BabelFiles.FileType.BOLS), FilenameImported(), sImportLineBuffer, "10012")
                End If

                'Check if padded to 60 char lines:
                If Len(sImportLineBuffer) < 60 Then
                    ' Must pad with trailing blanks
                    sImportLineBuffer = Mid(sImportLineBuffer & Space(60), 1, 60)
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadBOLSRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadBOLSRecord = sImportRecord

    End Function
    Function ReadSGFINANS_BOLSRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from Aquarius BOLS-file

        'Read one line
        Do
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                ' Continue

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start BOLS; or BOLSTOTAL;
                If Not Mid(sImportLineBuffer, 1, 5) = "BOLS;" And Not Mid(sImportLineBuffer, 1, 10) = "BOLSTOTAL;" Then
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError((BabelFiles.FileType.SGFINANSBOLS), FilenameImported(), sImportLineBuffer, "10012")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadSGFINANS_BOLSRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadSGFINANS_BOLSRecord = sImportRecord

    End Function
    Function ReadSGClientReportingRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)

        'Read one line
        sImportLineBuffer = ""

        Do

            'sImportLineBuffer = oFile.ReadLine()

            ' 18.09.07
            ' Dramatic change in how we read lines. Because there may be end of line or end of file chars in
            ' the importstring, we must read char by char
            Do
                If oFile.AtEndOfStream Then
                    Exit Do
                End If
                sImportLineBuffer = sImportLineBuffer & oFile.Read(1)
                ' when to end line?
                If Right(sImportLineBuffer, 2) = vbCr & vbLf Or oFile.AtEndOfStream Then
                    ' remove possible cr/lf
                    sImportLineBuffer = Replace(Replace(sImportLineBuffer, vbCr, ""), vbLf, "")
                    Exit Do
                End If
            Loop


            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                ' Continue

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start OCR, OTHER or OCRTOTAL
                If Not Mid(sImportLineBuffer, 1, 4) = "OCR;" And Not Mid(sImportLineBuffer, 1, 6) = "OTHER;" And Not Mid(sImportLineBuffer, 1, 9) = "OCRTOTAL;" Then
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError((BabelFiles.FileType.SG_ClientReporting), FilenameImported(), sImportLineBuffer, "10012")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadSGClientReportingRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadSGClientReportingRecord = sImportRecord

    End Function
    Public Function ReadPeriodiskaBetalningarFinlandRecord(ByRef oFile As Scripting.TextStream) As String
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with "1" or "9"
                If Mid(sImportLineBuffer, 1, 1) <> "1" And Mid(sImportLineBuffer, 1, 1) <> "9" And Len(sImportLineBuffer) <> 80 Then
                    'babelbank_error: Feil i format - Ikke korrekt fil!
                    sImportRecord = ""
                    'Error # 10012
                    If Not oFile.AtEndOfStream Then
                        Call BabelImportError((BabelFiles.FileType.Periodiska_Betalningar), FilenameImported(), sImportLineBuffer, "10012")
                    Else
                        sImportLineBuffer = ""
                    End If
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        ReadPeriodiskaBetalningarFinlandRecord = sImportRecord

    End Function
    Function ReadLM_Domestic_FinlandRecord(ByRef oFile As Scripting.TextStream, Optional ByRef sSpecial As String = "") As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a LM_Domestic_Finland-file

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            'If sSpecial = "MICHEMICALS" Then
            ' 04.02.05:
            ' MI Chemicals, Finland has a LM-file with CRLF only after headerrecord
            ' For the rest of the records, there are no CR or LF
            '    sImportLineBuffer = oFile.Read(300)
            ' 11.02.05 - File is OK, they do have CR/LF !!!!
            'Else
            sImportLineBuffer = oFile.ReadLine()
            'End If
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with LM02 or LM03 - if not no LM_Domestic_Finlandfile!
                If Mid(sImportLineBuffer, 1, 4) <> "LM02" And Mid(sImportLineBuffer, 1, 4) <> "LM03" Then
                    'babelbank_error: Feil i format - Ikke korrekt LM-fil!
                    sImportRecord = ""
                    'Error # 10012
                    If Not oFile.AtEndOfStream Then
                        ' For MI ligger det noe kukk i slutten av filen
                        Call BabelImportError((BabelFiles.FileType.LM_Domestic_Finland), FilenameImported(), sImportLineBuffer, "10012")
                    Else
                        sImportLineBuffer = ""
                    End If
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadLM_Domestic_FinlandRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadLM_Domestic_FinlandRecord = sImportRecord

    End Function
    Public Function ReadErhvervsgiroRecord(ByRef oFile As Scripting.TextStream, Optional ByRef sSpecial As String = "", Optional ByRef iNoOfChars As Short = 0) As Object
        ' Read record from DanskeBank ErhvervsGiroUdbetaling
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            ' 16.01.06 Added KRAFT_DK
            ' 13.09.2010 Added NOCRLF
            If sSpecial = "EMENTOR_DK" Or sSpecial = "KRAFT_DK" Or sSpecial = "NOCRLF" Then
                ' 22.03.05:
                ' Ementor, DK, has a file with no CRLF records
                ' First line in file is 126 (header), rest are 125 !!!
                sImportLineBuffer = oFile.Read(iNoOfChars)
            Else
                sImportLineBuffer = oFile.ReadLine()
            End If
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with 000, and 1, 2, 3, 4 or 5 in pos 8, and more than 120 chars
                'If Not (Mid$(sImportLineBuffer, 1, 3) = "000" And InStr(Mid$(sImportLineBuffer, 8, 1), "12345") = 0 And Len(sImportLineBuffer) > 120) Then
                If Not (Mid(sImportLineBuffer, 1, 3) = "000" And InStr(Mid(sImportLineBuffer, 8, 1), "12345") = 0 And Len(sImportLineBuffer) > 20) Then
                    'babelbank_error: Feil i format - Ikke korrekt fil!
                    sImportRecord = ""
                    'Error # 10012
                    If Not oFile.AtEndOfStream Then
                        Call BabelImportError((BabelFiles.FileType.ErhvervsGiro_Udbetaling), FilenameImported(), sImportLineBuffer, "10012")
                    Else
                        ' Hvis det ligger noe noe kukk i slutten av filen
                        sImportLineBuffer = ""
                    End If
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadErhvervsgiroRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadErhvervsgiroRecord = sImportRecord
    End Function
    Function ReadLUM_International_FinlandRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a LUM2 International Finland-file

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with LUM2
                If Mid(sImportLineBuffer, 1, 4) <> "LUM2" Then
                    'babelbank_error: Feil i format - Ikke korrekt LUM-fil!
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError((BabelFiles.FileType.LUM_International_Finland), FilenameImported(), sImportLineBuffer, "10012")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadLUM_International_FinlandRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadLUM_International_FinlandRecord = sImportRecord

    End Function

    Function ReadAutogiroRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a OCR-file
        ' 80 chars

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with NY00 or NY01 - if not no Autogiro!
                ' or NY02 (autogiro engangs) 29.12.05. Added test for NY09 to handle mix of autogiro and OCR
                ' 22.12.06 Added avtalegiro (NY21)
                If Mid(sImportLineBuffer, 1, 4) <> "NY00" And Mid(sImportLineBuffer, 1, 4) <> "NY01" And Mid(sImportLineBuffer, 1, 4) <> "NY02" And Mid(sImportLineBuffer, 1, 4) <> "NY09" And Mid(sImportLineBuffer, 1, 4) <> "NY21" Then
                    'babelbank_error: Feil i format - Ikke korrekt Dirrekte remitteringsfil!
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError((BabelFiles.FileType.AutoGiro), FilenameImported(), sImportLineBuffer, "10012")
                End If


                'Check if padded to 80 char lines:
                If Len(sImportLineBuffer) < 80 Then
                    ' Must pad with trailing blanks
                    sImportLineBuffer = Mid(sImportLineBuffer & Space(80), 1, 80)
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadAutogiroRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadAutogiroRecord = sImportRecord

    End Function
    Function ReadOCRRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a OCR-file
        ' 80 chars

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                '25.03.2010 - New code for Kredinor to deal with JCL script for Kredinor
            ElseIf Left(sImportLineBuffer, 5) = "FMS01" And Mid(sImportLineBuffer, 18, 8) = "NOKRE101" Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            ElseIf InStr(1, sImportLineBuffer, "INGEN RETUR FOR ANGITTE PARAMETERE", vbTextCompare) Then
                Exit Do
            Else
                'Check line
                '1) Must start with NY00 or NY09 - if not no OCRfile!
                ' Added NY21 (Dirrem) and NY02 29.12.05 -(Autogiro engangs)
                ' Added NY01 (autogiro) 10.11.06
                If Mid(sImportLineBuffer, 1, 4) <> "NY00" And Mid(sImportLineBuffer, 1, 4) <> "NY09" And Mid(sImportLineBuffer, 1, 4) <> "NY21" And Mid(sImportLineBuffer, 1, 4) <> "NY02" And Mid(sImportLineBuffer, 1, 4) <> "NY01" Then
                    'babelbank_error: Feil i format - Ikke korrekt OCR-fil!
                    '00 = Start file/End File, 09 = OCR, 21 = FBO
                    sImportRecord = ""
                    If Mid(sImportLineBuffer, 1, 4) = "NY04" Then
                        ' New test 29.04.05
                        ' May have DirRem return in OCR-files.
                        ' Delete these, and carry on!!!!!!!!!
                        i = 0 ' to force import of another line
                    Else
                        'Error # 10012
                        Call BabelImportError((BabelFiles.FileType.OCR), FilenameImported(), sImportLineBuffer, "10012")
                    End If
                End If

                'Check if padded to 80 char lines:
                If Len(sImportLineBuffer) < 80 Then
                    ' Must pad with trailing blanks
                    sImportLineBuffer = Mid(sImportLineBuffer & Space(80), 1, 80)
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer

                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadOCRRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadOCRRecord = sImportRecord

    End Function
    Function ReadSEK2Record(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a SEK2-file
        ' Danish Incoming payments
        ' Pengeinstitutternes standardformat
        '------------------------------------

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            ElseIf Len(sImportLineBuffer) < 5 And oFile.AtEndOfStream Then
                sImportLineBuffer = ""
                Exit Do
            Else
                'Check line
                '1) Must start with FI0
                If Mid(sImportLineBuffer, 1, 3) <> "FI0" Then
                    sImportRecord = ""
                    'Error # 10011 '  Wrong format !
                    Call BabelImportError((BabelFiles.FileType.SEK2), FilenameImported(), sImportLineBuffer, "10011")
                Else
                    ' New 06.04.05
                    ' Do not read the undocumented FI04-record
                    If Mid(sImportLineBuffer, 1, 4) = "FI04" Then
                        ' reread!
                    Else
                        Exit Do
                    End If
                End If
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadSEK2Record. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadSEK2Record = sImportLineBuffer

    End Function

    Function ReadCodaRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with PX
                If Mid(sImportLineBuffer, 1, 2) <> "PX" Then
                    'babelbank_error: Feil i format - Ikke korrekt Dirrekte remitteringsfil!
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError("CODA", FilenameImported(), sImportLineBuffer, "10012")
                End If


                'Check if padded to 80 char lines:
                If Len(sImportLineBuffer) < 559 Then
                    ' Must pad with trailing blanks
                    sImportLineBuffer = Mid(sImportLineBuffer & Space(559), 1, 559)
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer


                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadCodaRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadCodaRecord = sImportRecord

    End Function
    Function ReadPGSRecord(ByRef oFile As Scripting.TextStream) As Object
        ' Read one record from PGS special "payX"-file

        sImportLineBuffer = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                ' continue

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with PX or PM
                If Mid(sImportLineBuffer, 1, 2) <> "PX" And Mid(sImportLineBuffer, 1, 2) <> "PM" Then
                    'babelbank_error: Feil i format
                    'Error # 10011
                    Call BabelImportError("PGS", FilenameImported(), sImportLineBuffer, "10011")
                    sImportLineBuffer = ""
                Else
                    Exit Do
                End If

                ' Only one line for each record
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadPGSRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadPGSRecord = sImportLineBuffer

    End Function
    Function ReadISS_LonnsTrekkRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from ISS File for l�nnstrekk

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                If IsNumeric(Left(sImportLineBuffer, 1)) = False Or Mid(sImportLineBuffer, 13, 1) <> "0" Or IsNumeric(Mid(sImportLineBuffer, 14, 1)) = False Then
                    sImportRecord = ""
                ElseIf Val(Mid(sImportLineBuffer, 83, 9)) = 0 Then
                    ' Amounts larger than 0
                    sImportRecord = ""
                ElseIf Len(sImportLineBuffer) > 200 Then
                    ' Line is OK
                    sImportRecord = sImportLineBuffer
                    Exit Do
                End If

            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadISS_LonnsTrekkRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadISS_LonnsTrekkRecord = sImportRecord

    End Function
    Function ReadSRI_AccessRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from SRI Access textfile (Terra Invest)

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Len(Trim(sImportLineBuffer)) = 0 Then
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with ";" - if so, it's some summarylines at the end of the  file!
                If Mid(sImportLineBuffer, 1, 1) = ";" Then
                    sImportLineBuffer = ""

                End If
                ' Must start with ", second (start of account) must be digit
                If Not (Left(sImportLineBuffer, 1) = Chr(34) And IsNumeric(Mid(sImportLineBuffer, 2, 1))) Then
                    sImportLineBuffer = ""
                Else
                    ' OK
                    ' Only one line for each record
                    sImportRecord = sImportLineBuffer
                    Exit Do
                End If

            End If
        Loop


        'UPGRADE_WARNING: Couldn't resolve default property of object ReadSRI_AccessRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadSRI_AccessRecord = sImportRecord

    End Function
    Function ReadOrklaMediaExcelRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a Excelfile

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            If oFile.Line = 1 Then
                oFile.SkipLine()
            End If
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(Trim(sImportLineBuffer)) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with ";" - if so, it's some summarylines at the end of the  file!
                If Mid(sImportLineBuffer, 1, 1) = ";" Then
                    'babelbank_error: Feil i format - Ikke korrekt Dirrekte remitteringsfil!
                    sImportLineBuffer = ""
                    i = 0
                    'Error # 10012
                    'Call BabelImportError("CODA", FilenameImported(), sImportLineBuffer, "10012")
                End If
                If UCase(Left(xDelim(sImportLineBuffer, ";", 11), 1)) = "J" Then
                    sImportLineBuffer = ""
                    i = 0
                End If

                'Check if padded to 80 char lines:
                'If Len(sImportLineBuffer) < 559 Then
                ' Must pad with trailing blanks
                'sImportLineBuffer = Mid$(sImportLineBuffer & Space(559), 1, 559)
                'End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer


                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadOrklaMediaExcelRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadOrklaMediaExcelRecord = sImportRecord
    End Function
    Function ReadCompuwareRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)
        ' Read one record from sdv-file from Compuware, comma separated

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with "," - if so, it's some summarylines at the end of the  file!
                If Mid(sImportLineBuffer, 1, 1) = "," Then
                    sImportLineBuffer = ""
                    i = 0
                End If
                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadCompuwareRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadCompuwareRecord = sImportRecord
    End Function
    Function ReadBECRecord(ByRef oFile As Scripting.TextStream) As Object
        ' Read one record from DNBNOR BEC-file (incoming payments)
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportRecord = oFile.ReadLine()
            If Len(Trim(sImportRecord)) = 0 Then
                Exit Do
                'Test if end of file mark
            ElseIf Asc(Mid(sImportRecord, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with 351 -
                ' so far , we are just interested in FI 71
                ' New 31.01.05
                ' Also 356 (UDUS)
                ' or with header 200409301531119080019999 = Date (yyyyMMDDHHMMSS + AccountNo)
                ' 07.04.05 Also 355
                ' 02.08.2007 also 357 and 358 (FIK 73 and 75)
                If Mid(sImportRecord, 1, 3) = "351" Or Mid(sImportRecord, 1, 3) = "355" Or Mid(sImportRecord, 1, 3) = "356" Or Mid(sImportRecord, 1, 3) = "357" Or Mid(sImportRecord, 1, 3) = "358" Or (Left(sImportRecord, 2) = "20" And Len(sImportRecord) < 26) Then
                    Exit Do
                End If

                ' New 08.03.06 - New format
                If Mid(sImportRecord, 1, 6) = ":FI351" Or Mid(sImportRecord, 1, 3) = ":UD355" Or Mid(sImportRecord, 1, 6) = ":UD356" Or Mid(sImportRecord, 1, 6) = ":FI357" Or Mid(sImportRecord, 1, 3) = ":UD358" Or (Left(sImportRecord, 5) = "AH100" And Len(Trim(sImportRecord)) < 72) Or Mid(sImportRecord, 1, 8) = ":ACCOUNT" Or Mid(sImportRecord, 1, 5) = ":DATE" Then

                    Exit Do
                End If

            End If
        Loop

        ' Have to replace specialcharacters;
        ' Translate specialcharacters (���, ���)
        ' $ = �, } = �, � = �
        ' Kanskje !! er { = �, \ = �
        ' # = �
        ' @ = �
        If InStr(sImportRecord, "$") > 0 Then
            sImportRecord = Replace(sImportRecord, "$", "�")
        End If
        If InStr(sImportRecord, "}") > 0 Then
            sImportRecord = Replace(sImportRecord, "}", "�")
        End If
        If InStr(sImportRecord, "�") > 0 Then
            sImportRecord = Replace(sImportRecord, "�", "�")
        End If
        If InStr(sImportRecord, "\") > 0 Then
            sImportRecord = Replace(sImportRecord, "\", "�")
        End If
        If InStr(sImportRecord, "@") > 0 Then
            sImportRecord = Replace(sImportRecord, "@", "�")
        End If
        If InStr(sImportRecord, "{") > 0 Then
            sImportRecord = Replace(sImportRecord, "{", "�")
        End If
        If InStr(sImportRecord, "#") > 0 Then
            sImportRecord = Replace(sImportRecord, "#", "�")
        End If


        'UPGRADE_WARNING: Couldn't resolve default property of object ReadBECRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadBECRecord = sImportRecord
    End Function
    Function ReadSeaTruckRecord(ByRef oFile As Object) As Object
        ' Read one record from BACS, SeaTruck

        sImportLineBuffer = ""
        sImportRecord = ""

        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.AtEndOfStream. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Do While (oFile.AtEndOfStream = False)
            'Read one line
            'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                ' Keep on ...
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
                'Check line
                ' (Sometimes "BACHNUMB", sometimes BACHNUMB !!
            ElseIf Left(Replace(Mid(sImportLineBuffer, 1, 9), Chr(34), ""), 8) = "BACHNUMB" Then
                '1) Must not start with "BACHNUMB" - if so, it's headerline!

                sImportLineBuffer = ""
                ' read new line!
                '2) Must start with "
                'ElseIf Left$(sImportLineBuffer, 1) <> Chr$(34) Then
                '    sImportLineBuffer = ""
                '    ' read new line!

            Else
                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadSeaTruckRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadSeaTruckRecord = sImportRecord
    End Function
    Function ReadLeverantorsbetalningarRecord(ByRef oFile As Scripting.TextStream, Optional ByRef sSpecial As String = "") As String ', oProfile As Profile)
        ' Read one record leverantorsbetalningar
        sImportLineBuffer = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line

            '19.03.2009 - BgMax file from Norstat consists of Chr(0) between every character. Remove them
            'Remove also some other offset characters
            sImportLineBuffer = oFile.ReadLine()
            sImportLineBuffer = Replace(sImportLineBuffer, Chr(0), "", , , CompareMethod.Text)
            If Left(sImportLineBuffer, 2) = "��" Then
                sImportLineBuffer = Mid(sImportLineBuffer, 3)
            End If
            If Len(Trim(sImportLineBuffer)) = 0 Then
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with digit (TK-code), and at least 8 following digits
                '2) Linelengt must not exeed 80 chars + CRLF
                'If InStr(Left$(sImportLineBuffer, 1), "02345679") Then  ??????????
                '    sImportLineBuffer = ""

                ' 27.10.2015 - added next line to take care of problems with blank at start (like JERNIA)
                If sSpecial = "JERNIA_LB" Then '
                    ' sometimes first pos is CHR(13)
                    sImportLineBuffer = sImportLineBuffer.Trim.Replace(Chr(13), "")
                End If
                If Not IsNumeric(Left(sImportLineBuffer, 1)) Then
                    'If Not IsNumeric(Left$(sImportLineBuffer, 8)) Then
                    sImportLineBuffer = ""
                    'ElseIf Len(sImportLineBuffer) > 85 Then
                    '    sImportLineBuffer = ""
                Else
                    sImportRecord = sImportLineBuffer
                    Exit Do
                End If
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadLeverantorsbetalningarRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadLeverantorsbetalningarRecord = sImportRecord
    End Function
    Function ReadExcelRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a excelfile

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(Trim(sImportLineBuffer)) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with ";" - if so, it's some summarylines at the end of the  file!
                If Mid(sImportLineBuffer, 1, 1) = ";" Then
                    sImportLineBuffer = ""
                    i = 0
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer

                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadExcelRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadExcelRecord = sImportRecord

    End Function
    Function ReadDanskeBankTeleServiceRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from a DanskeBank TeleService file (incoming FIK-payments)

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with "CMM" -
                    If Left(sImportLineBuffer, 5) <> Chr(34) & "CMM" & Chr(34) And Left(sImportLineBuffer, 3) <> "CMM" Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadDanskeBankTeleServiceRecord = sImportLineBuffer

    End Function
    Function ReadDanskeBankLokaleDanskeRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from a DanskeBank Lokale, danske betalinger (PC)

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with "CM.." -
                    ' 09.07.2010, added Norwegian and Swedish paymenttypes
                    If (Left(sImportLineBuffer, 6) <> Chr(34) & "CMBO" & Chr(34)) And (Left(sImportLineBuffer, 6) <> Chr(34) & "CMUO" & Chr(34)) And (Left(sImportLineBuffer, 8) <> Chr(34) & "CMUONO" & Chr(34)) And (Left(sImportLineBuffer, 8) <> Chr(34) & "CMUOSE" & Chr(34)) And (Left(sImportLineBuffer, 6) <> Chr(34) & "CMNI" & Chr(34)) And (Left(sImportLineBuffer, 6) <> Chr(34) & "CMSI" & Chr(34)) Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadDanskeBankLokaleDanskeRecord = sImportLineBuffer

    End Function
    Function ReadSydbankDanskeRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from  Sydbank DK
        '"IB000000000000","20041210","
        '"IB030202000005","0001","20041217","0000006790900+"," etc ..
        '"IB030202000005","0002","

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with "IB0 -
                    If Left(sImportLineBuffer, 4) <> Chr(34) & "IB0" Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadSydbankDanskeRecord = sImportLineBuffer

    End Function
    Function ReadCyberCityRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from  CyberCity (JyskeBank)
        ' 003 = L�nn
        ' 003003545732000-05-29000000116604500                5010155155931710000189772Christina Christensen                                     0027                    *
        ' 008 = Betalingsrecord
        ' "008","Ving","","","                                   ","6798.0000","DK","21498127274046","2006-03-03","50101551559","","Faktura 25-480-987
        ' 009 = FIkortbetaling
        ' "009","Interflora-danmark","Naverland 34","Postbox 1336","2600 Glostrup                      ","345.0000","2006-03-03","50101551559","","71","000000014487714","Faktura 1448771                                                                                  ","89796245"

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with 003 "008" or "009"
                    If Left(sImportLineBuffer, 3) <> "003" And Left(sImportLineBuffer, 5) <> Chr(34) & "008" & Chr(34) And Left(sImportLineBuffer, 5) <> Chr(34) & "009" & Chr(34) Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadCyberCityRecord = sImportLineBuffer

    End Function
    Function ReadNSARecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from  NSA (JyskeBank)
        '"010","AARDAL, TONNY","FREDERIK TH HOLMSV 1","2040","NO","7101.11.56813","060530","331,83","NOK","82000171022,NSA SCAND.","Chk Nr.4098*NO*EFT","1","7101","DNB","UBNONOKK"
        '"010","ADAMSEN, �SE","NESVEIEN 18 B","1666","NO","90161021279","060530","492,75","NOK","82000171022,NSA SCAND.","Chk Nr.4099*NO*EFT","1","9016","SPAREBANK 1","RYGSNO22"
        '"010","ADOLFSEN, INGRID","HAUGE","1684","NO","1111.10.19705","060530","25,00","NOK","82000171022,NSA SCAND.","Chk Nr.4100*NO*EFT","1","1111","DNB",""

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with "001"
                    If Left(sImportLineBuffer, 5) <> Chr(34) & "010" & Chr(34) Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadNSARecord = sImportLineBuffer

    End Function
    Function ReadNordeaDK_EDIRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from  Nordea Unitel EDI-format
        'UBT03300012149755583560900000000022215020050510PEUGEOT R�DOVRE     50270001037371N100CANAL DIGITAL DANMAR70272775                                                              00004PEUGEOT R�DOVRE                    Bo Kjeldsmark Automobiler          Islevsdalsvej 75                   2610 R�dovre                       02Faktura       Rabat           Bel�b4065721/10     0,00        2.221,5000
        'UBT04400012149755583560900000000014162220050510TDC MOBILE INTERNATI008282713771000041170320500027800001TDC MOBILE INTERNATIONAL A/S       DKK0000
        'UBT04400012149755583560900000000005980020050510SONOFON GSM         008078305171000000000017436510600001SONOFON GSM                        DKK0000
        'UBT03300012149755583560900000000087468820050510KJAERULFF 1 A/S     22060370144400N100CANAL DIGITAL DANMAR70272775                                                              00003KJAERULFF 1 A/S                    VESTRE STATIONSVEJ 9               5000 Odense C                      05Faktura       Rabat           Bel�b101999/454     0,00        1.548,44102489/459     0,00        1.292,19103265/455     0,00        4.357,81103264/456     0,00        1.548,4400
        'UBT04400012149755583560900000000026375020050510TDC FORLAG A/S      008458972171000007010165770052300001TDC FORLAG A/S                     DKK0000

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with UBT
                    If Left(sImportLineBuffer, 3) <> "UBT" Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadNordeaDK_EDIRecord = sImportLineBuffer

    End Function

    Function ReadBACSforTBIRecord(ByRef oFile As Object) As Object
        ' Read one record from BACS UK Standard


        ' We accept files both with or without
        sImportLineBuffer = ""
        sImportRecord = ""

        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.AtEndOfStream. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Do While (oFile.AtEndOfStream = False)
            'Read one line
            'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                ' Keep on ...
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
                'Check line
                ' (Sometimes "BACHNUMB", sometimes BACHNUMB !!
            ElseIf Left(Replace(Mid(sImportLineBuffer, 1, 9), Chr(34), ""), 8) = "BACHNUMB" Then
                '1) Must not start with "BACHNUMB" - if so, it's headerline!

                sImportLineBuffer = ""
                ' read new line!
                '2) Must start with "
                'ElseIf Left$(sImportLineBuffer, 1) <> Chr$(34) Then
                '    sImportLineBuffer = ""
                '    ' read new line!

            Else
                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadBACSforTBIRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadBACSforTBIRecord = sImportRecord
    End Function

    Function ReadGenericRecord(ByRef oFile As Scripting.TextStream, Optional ByRef bContinueToReadWhenEmptyLine As Boolean = False) As String
        ' Read one record from file, indiffernet of format
        ' Modified by JanP 09.05.03
        sImportLineBuffer = ""

        'New code by Kjell, 10.02.2005
        'The parameter bContinue To Re... did not function well
        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) < 2 Then
                ' Read another one, if we're not at the end of the file
                ' Changed by Kjell, 13.05.2003, added the following IF-statement. Because
                '     at 3M we had passed the ENDOFSTREAM-mark
                If oFile.AtEndOfStream = False Then
                    If bContinueToReadWhenEmptyLine Then
                        'Continue
                    Else
                        Exit Do
                    End If
                Else
                    sImportLineBuffer = ""
                End If

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                ' read the rest ..
                Do While (oFile.AtEndOfStream = False)
                    sImportLineBuffer = oFile.ReadLine()
                Loop
                sImportLineBuffer = ""
                Exit Do
            Else
                'No check, generic format
                Exit Do
            End If
        Loop


        'OLD code
        'Do While (oFile.AtEndOfStream = False) And i < 2
        '    'Read one line
        '    sImportLineBuffer = oFile.ReadLine()
        '    If Len(Trim$(sImportLineBuffer)) < 2 Then
        '        ' Read another one, if we're not at the end of the file
        '        ' Changed by Kjell, 13.05.2003, added the following IF-statement. Because
        '        '     at 3M we had passed the ENDOFSTREAM-mark
        '        If oFile.AtEndOfStream = False Then
        '            sImportLineBuffer = oFile.ReadLine()
        '        Else
        '            sImportLineBuffer = ""
        '        End If
        '    End If
        '
        '    If Len(sImportLineBuffer) = 0 Then
        '        If bContinueToReadWhenEmptyLine Then
        '            'Continue
        '        Else
        '            Exit Do
        '        End If
        '
        '    'Test if end of file mark
        '    ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
        '        ' read the rest ..
        '        Do While (oFile.AtEndOfStream = False)
        '            sImportLineBuffer = oFile.ReadLine()
        '        Loop
        '        sImportLineBuffer = ""
        '        Exit Do
        '    Else
        '        'No check, generic format
        '        Exit Do
        '    End If
        'Loop

        ReadGenericRecord = sImportLineBuffer

    End Function
    Function ReadVingcardRecord(ByVal oFile As Scripting.TextStream, Optional ByVal bContinueToReadWhenEmptyLine As Boolean = False) As String
        ' Read one record from file, indiffernet of format
        ' Modified by JanP 09.05.03
        sImportLineBuffer = vbNullString

        'New code by Kjell, 10.02.2005
        'The parameter bContinue To Re... did not function well
        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim$(sImportLineBuffer)) < 2 Then
                ' Read another one, if we're not at the end of the file
                ' Changed by Kjell, 13.05.2003, added the following IF-statement. Because
                '     at 3M we had passed the ENDOFSTREAM-mark
                If oFile.AtEndOfStream = False Then
                    If bContinueToReadWhenEmptyLine Then
                        'Continue
                    Else
                        Exit Do
                    End If
                Else
                    sImportLineBuffer = vbNullString
                End If

                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                ' read the rest ..
                Do While (oFile.AtEndOfStream = False)
                    sImportLineBuffer = oFile.ReadLine()
                Loop
                sImportLineBuffer = ""
                Exit Do
            ElseIf (Mid(sImportLineBuffer, Len(sImportLineBuffer) - 2, 1) <> "." Or Not vbIsNumeric(Right(sImportLineBuffer, 2))) And Left(sImportLineBuffer, 8) <> "INVOICES" Then
                'this is a record over 2 lines, read another
                sImportLineBuffer = sImportLineBuffer & oFile.ReadLine()
                If (Mid(sImportLineBuffer, Len(sImportLineBuffer) - 2, 1) <> "." Or Not vbIsNumeric(Right(sImportLineBuffer, 2))) And Left(sImportLineBuffer, 8) <> "INVOICES" Then
                    'this is a record over 3 lines, read another
                    sImportLineBuffer = sImportLineBuffer & oFile.ReadLine()
                    Exit Do
                Else
                    Exit Do
                End If
            Else
                'No check, generic format
                Exit Do
            End If
        Loop


        'OLD code
        'Do While (oFile.AtEndOfStream = False) And i < 2
        '    'Read one line
        '    sImportLineBuffer = oFile.ReadLine()
        '    If Len(Trim$(sImportLineBuffer)) < 2 Then
        '        ' Read another one, if we're not at the end of the file
        '        ' Changed by Kjell, 13.05.2003, added the following IF-statement. Because
        '        '     at 3M we had passed the ENDOFSTREAM-mark
        '        If oFile.AtEndOfStream = False Then
        '            sImportLineBuffer = oFile.ReadLine()
        '        Else
        '            sImportLineBuffer = vbNullString
        '        End If
        '    End If
        '
        '    If Len(sImportLineBuffer) = 0 Then
        '        If bContinueToReadWhenEmptyLine Then
        '            'Continue
        '        Else
        '            Exit Do
        '        End If
        '
        '    'Test if end of file mark
        '    ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
        '        ' read the rest ..
        '        Do While (oFile.AtEndOfStream = False)
        '            sImportLineBuffer = oFile.ReadLine()
        '        Loop
        '        sImportLineBuffer = ""
        '        Exit Do
        '    Else
        '        'No check, generic format
        '        Exit Do
        '    End If
        'Loop
        ReadVingcardRecord = sImportLineBuffer

    End Function
    Function ReadDTAUSRecord(ByRef oFile As Scripting.TextStream, ByRef sImportLineBuffer As String) As String

        If Len(sImportLineBuffer) < 1000 Then
            If oFile.AtEndOfStream = False Then
                sImportLineBuffer = sImportLineBuffer & oFile.Read(2000)
                sImportLineBuffer = Replace(Replace(sImportLineBuffer, Chr(13), ""), Chr(10), "")
            Else
                If Len(sImportLineBuffer) < 128 Then
                    sImportLineBuffer = PadRight(sImportLineBuffer, 128, " ")
                End If
            End If
        End If

        ReadDTAUSRecord = sImportLineBuffer

    End Function
    Function ReadNordeaUnitelRecord(ByRef oFile As Scripting.TextStream) As String
        sImportLineBuffer = ""

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                ' Keep on ...
                'Old code, changed 14.10.2004 by Kjell, commented next elseIf
                '    ElseIf Not (Left$(sImportLineBuffer, 5) = Chr(34) & "0" & Chr(34) & "," & Chr(34)) Then  ' Chr(34) = "
                '        ' Line must start with "0","
                '        sImportLineBuffer = ""
            Else
                ' Only one line for each record
                Exit Do
            End If
        Loop
        ReadNordeaUnitelRecord = sImportLineBuffer

    End Function
    Function ReadShipNetRecord(ByRef oFile As Scripting.TextStream) As String
        sImportLineBuffer = ""

        sImportLineBuffer = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                ' Keep on ...
            ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) < 64 Then
                ' at least 64 ; (separators) required
                sImportLineBuffer = ""
            Else
                ' Only one line for each record
                Exit Do
            End If
        Loop
        ReadShipNetRecord = sImportLineBuffer

    End Function

    Function ReadCitiDirectUSFlatFileRecord(ByRef oFile As Scripting.TextStream) As String
        sImportLineBuffer = ""

        sImportLineBuffer = ""

        If oFile.AtEndOfStream Then
            sImportLineBuffer = "THE_END"
        Else
            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            'Check if CitiDirect-format
            If xDelim(sImportLineBuffer, "#", 2) <> "US" Then
                Call BabelImportError("CitiDirect US Flat File", FilenameImported(), sImportLineBuffer, "10020-022")
            End If

            If Len(Trim(sImportLineBuffer)) = 0 Then
                Call BabelImportError("CitiDirect US Flat File", FilenameImported(), sImportLineBuffer, "10034")
            ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, "#", "")) <> 86 Then
                ' at least 64 ; (separators) required
                Call BabelImportError("CitiDirect US Flat File", FilenameImported(), sImportLineBuffer, "10034")
            Else
                ' Only one line for each record
            End If
        End If

        ReadCitiDirectUSFlatFileRecord = sImportLineBuffer

    End Function
    Function ReadFirst_Union_ACHRecord(ByRef oFile As Scripting.TextStream) As String
        Static lRecordSize As Integer
        ' Read one record from file
        sImportLineBuffer = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Left(sImportLineBuffer, 1) = "1" Then
                lRecordSize = Val(Mid(sImportLineBuffer, 35, 3))
            End If
            If Len(sImportLineBuffer) <> lRecordSize Then
                ' Read another one, if we're not at the end of the file
                ' Changed by Kjell, 13.05.2003, added the following IF-statement. Because
                '     at 3M we had passed the ENDOFSTREAM-mark
                If oFile.AtEndOfStream = False Then
                    sImportLineBuffer = "ERROR"
                Else
                    sImportLineBuffer = ""
                End If
            End If

            If Len(sImportLineBuffer) = 0 Then
                Exit Do

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                ' read the rest ..
                Do While (oFile.AtEndOfStream = False)
                    sImportLineBuffer = oFile.ReadLine()
                Loop
                sImportLineBuffer = ""
                Exit Do
            Else
                'No check, generic format
                Exit Do
            End If
        Loop

        ReadFirst_Union_ACHRecord = sImportLineBuffer

    End Function
    Function ReadDnBNattsafeRecordOld(ByRef oFile As Scripting.TextStream) As String
        'Dim sImportLineBuffer As String
        Dim bReadLine As Boolean
        Dim sTmp As String
        Dim bExit As Boolean

        bReadLine = True

        Do While (oFile.AtEndOfStream = False)
            bExit = False
            ' import one line
            If bReadLine = True Then
                sImportLineBuffer = oFile.ReadLine()
            End If

            If Left(sImportLineBuffer, 3) = "INN" Or Left(sImportLineBuffer, 3) = "TOT" Then

                ' record can be broken into several lines:
                sTmp = sImportLineBuffer

                Do While (oFile.AtEndOfStream = False)

                    'Test if end of file mark
                    If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                        ' read the rest ..
                        Do While (oFile.AtEndOfStream = False)
                            sImportLineBuffer = oFile.ReadLine()
                        Loop
                        sImportLineBuffer = ""
                        Exit Do
                    ElseIf Left(sImportLineBuffer, 3) = "INN" Or Left(sImportLineBuffer, 3) = "TOT" Then
                        bReadLine = False ' already read line
                        If Len(RTrim(sImportLineBuffer)) < 80 Then
                            bExit = True
                            Exit Do ' get out, not a part of this record
                        Else
                            sImportLineBuffer = oFile.ReadLine()
                        End If

                    Else
                        sTmp = sTmp & sImportLineBuffer
                        '''sImportLineBuffer = oFile.ReadLine()
                        bExit = True
                        Exit Do
                    End If
                Loop
            End If
            If bExit Then
                Exit Do
            End If

        Loop
        ReadDnBNattsafeRecordOld = sTmp

    End Function
    Function ReadDnBNattsafeRecord(ByRef oFile As Scripting.TextStream) As String
        'Dim sImportLineBuffer As String
        'One record may consists of one or two lines
        Dim bReadLine As Boolean
        Dim sTmp As String
        Dim bExit As Boolean

        If Left(sImportLineBuffer, 3) = "INN" Or Left(sImportLineBuffer, 3) = "TOT" Then
            bReadLine = False
        Else
            bReadLine = True
        End If

        Do While (oFile.AtEndOfStream = False)
            bExit = False
            ' import one line
            If bReadLine = True Then
                sImportLineBuffer = oFile.ReadLine()
            End If

            If Left(sImportLineBuffer, 3) = "INN" Or Left(sImportLineBuffer, 3) = "TOT" Then

                ' record can be broken into several lines:
                sTmp = sImportLineBuffer

                sImportLineBuffer = oFile.ReadLine()
                If Left(sImportLineBuffer, 3) = "INN" Or Left(sImportLineBuffer, 3) = "TOT" Then
                    Exit Do
                Else
                    sTmp = sTmp & sImportLineBuffer
                    Exit Do
                End If
            Else
                bReadLine = True
            End If

        Loop
        'sImportLineBuffer = sTmp
        ReadDnBNattsafeRecord = sTmp

    End Function
    Function ReadSecuritasNattsafeRecord(ByRef oFile As Scripting.TextStream) As String
        Dim sImportLineBuffer As String
        Dim bReadLine As Boolean

        bReadLine = True

        Do While (oFile.AtEndOfStream = False)
            ' import one line
            If bReadLine = True Then
                sImportLineBuffer = oFile.ReadLine()
            End If

            If Left(sImportLineBuffer, 6) <> "Posenr" And Left(sImportLineBuffer, 3) <> "Sum" Then

                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    ' read the rest ..
                    Do While (oFile.AtEndOfStream = False)
                        sImportLineBuffer = oFile.ReadLine()
                    Loop
                    sImportLineBuffer = ""
                    Exit Do
                    Exit Do
                Else
                    Exit Do
                End If
            Else
                sImportLineBuffer = ""
            End If

        Loop
        ReadSecuritasNattsafeRecord = sImportLineBuffer

    End Function

    Function ReadMT940Record(ByRef oFile As Scripting.TextStream, Optional ByRef bResetImportRecord As Boolean = False) As String
        Dim iSoFar As Short

        '19.10.2009 - Added the next IF because sImportRecord sometimes contained info from last import
        If bResetImportRecord Then
            sImportRecord = ""
        End If

        sImportLineBuffer = ""

        If Len(sImportRecord) = 0 Then
            sImportLineBuffer = Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
            ' 29.09.2017 - If we have blank lines, then read another line;
            If sImportLineBuffer.Trim = "" Then
                sImportLineBuffer = Trim(oFile.ReadLine()) & " "
            End If
            ' 20.09.2016
            ' if we have lines with - only, then cut this
            If sImportRecord = "-" Then
                sImportRecord = ""
                If Not oFile.AtEndOfStream Then
                    sImportRecord = Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
                End If
            End If
            sImportRecord = Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
            'End If '  28.09.2017 Added '
            ' 05.11.2015 Changed If/Else/Endif, so the second ReadLine will be treated in the code below
            If sImportRecord = "-" Then
                sImportRecord = "-"
            End If

        Else '28.09.2017 - removed'
            If Left(sImportRecord, 4) = ":86:" Then
                'Do While True
                ' changed by JanP 25.10.2007
                Do While Not oFile.AtEndOfStream

                    iSoFar = Len(sImportRecord)
                    sImportRecord = sImportRecord & "#" & Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
                    ' 20.09.2016
                    ' if we have lines with - only, then cut this
                    If sImportRecord = "-" Then
                        sImportRecord = ""
                        If Not oFile.AtEndOfStream Then
                            sImportRecord = Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
                        End If
                    End If

                    If Mid(sImportRecord, iSoFar + 2, 1) = ":" Or Mid(sImportRecord, iSoFar + 2, 5) = "-}{5:" Then
                        ' XNET 15.10.2013 - we may have : as part of the text !
                        'If (Mid$(sImportRecord, iSoFar + 2, 2)) <> ":6" Then
                        ' 24.11.2014 - the test above is wrong if 86-record is followed by :20:-record!!!
                        If (Mid$(sImportRecord, iSoFar + 2, 2)) <> ":6" And (Mid$(sImportRecord, iSoFar + 2, 2)) <> ":2" Then
                            ' like : 6130931 ONZE REF.: 201321395, that is; NO :61: OR :62 after :86:
                            ' then continue to treat the line as a part of the :86: record
                        Else
                            Exit Do
                        End If
                    End If
                    ' Added next if 05.01.07
                    If oFile.AtEndOfLine Then
                        Exit Do
                    End If


                Loop
                sImportLineBuffer = Left(sImportRecord, iSoFar)
                sImportRecord = Mid(sImportRecord, iSoFar + 2)
                sImportLineBuffer = Replace(sImportLineBuffer, vbCrLf, "")
                sImportRecord = Replace(sImportRecord, vbCrLf, "")
            ElseIf Left(sImportRecord, 6) = "-}{5:}" Then
                sImportLineBuffer = sImportRecord
                sImportRecord = oFile.ReadLine()
                ' 20.09.2016
                ' if we have lines with - only, then cut this
                If sImportRecord = "-" Then
                    sImportRecord = ""
                    If Not oFile.AtEndOfStream Then
                        sImportRecord = Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
                    End If
                End If

            Else
                'Do While True
                ' changed by JanP 25.10.2007
                Do While Not oFile.AtEndOfStream
                    iSoFar = Len(sImportRecord)
                    '--------------------------------------------------------
                    'sImportRecord = sImportRecord & oFile.ReadLine()
                    sImportLineBuffer = Trim(oFile.ReadLine) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
                    ' 20.09.2016
                    ' if we have lines with - only, then cut this
                    ' 10.05.2021 - they may also be like "- " (space at end), thus, trim
                    'If sImportLineBuffer = "-" Then
                    If Trim(sImportLineBuffer) = "-" Then
                        sImportLineBuffer = ""
                        If Not oFile.AtEndOfStream Then
                            sImportLineBuffer = Trim(oFile.ReadLine()) & " "   ' 29.03.2017 added Trim, as there may be many spaces at end.
                        End If
                    End If
                    sImportRecord = sImportRecord & sImportLineBuffer
                    '----------------------------------------------------------

                    If Mid(sImportRecord, iSoFar + 1, 1) = ":" Or Mid(sImportRecord, iSoFar + 1, 5) = "-}{5:" Then
                        Exit Do
                    End If
                Loop
                sImportLineBuffer = Left(sImportRecord, iSoFar)
                sImportRecord = Mid(sImportRecord, iSoFar + 1)
                sImportLineBuffer = Replace(sImportLineBuffer, vbCrLf, "")
                sImportRecord = Replace(sImportRecord, vbCrLf, "")
            End If
        End If '28.09.2017 - Removed '

        If InStr(sImportLineBuffer, "END OF JES SPOOL FILE") > 0 Then
            ' last line with JCL-cardinfo, skip
            sImportLineBuffer = ""
        End If

        ReadMT940Record = sImportLineBuffer


    End Function
    Function ReadDnBTBUKIncomingRecord(ByRef oFile As Object) As Object
        ' Read one record from DnB TBUK Accountsinformationfile

        sImportLineBuffer = ""
        sImportRecord = ""

        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.AtEndOfStream. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Do While (oFile.AtEndOfStream = False)
            'Read one line
            'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                ' Keep on ...
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                sImportRecord = ""
                Exit Do
            ElseIf Left(sImportLineBuffer, 9) = "AH100TBUK" Then
                ' File header, carry on

            ElseIf Left(sImportLineBuffer, 6) = "940SWI" Then

                ' Each record startes with 940SWI
                Select Case Left(sImportLineBuffer, 8)
                    Case "940SWI01"
                        ' Incoming balance - use this one!
                        ' 2 lines for SWI01
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do

                    Case "940SWI02"
                        ' Transactionrecord - use this one!
                        ' 3 lines for SWI02
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                    Case "940SWI04"
                        ' Informationrecord, payers reference - use this one!
                        ' 2 lines for SWI04
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                    Case "940SWI05"
                        ' Outgoing balance - use this one!
                        ' 2 lines for SWI05
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                End Select
            Else
                ' carry on until eof
            End If

        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBTBUKIncomingRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadDnBTBUKIncomingRecord = sImportRecord
    End Function
    Function ReadDnBTBWKRecord(ByRef oFile As Object) As Object
        ' Read one record from DnBNOR TBWK Accountsinformationfile

        sImportLineBuffer = ""
        sImportRecord = ""

        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.AtEndOfStream. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Do While (oFile.AtEndOfStream = False)
            'Read one line
            'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                ' Keep on ...
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                sImportRecord = ""
                Exit Do
            ElseIf Left(sImportLineBuffer, 9) = "AH100TBWK" Then
                ' File header, carry on

            ElseIf Left(sImportLineBuffer, 6) = "940SWI" Then

                ' Each record startes with 940SWI
                Select Case Left(sImportLineBuffer, 8)
                    Case "940SWI01"
                        ' Incoming balance - use this one!
                        ' 2 lines for SWI01
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do

                    Case "940SWI02"
                        ' Transactionrecord - use this one!
                        ' 3 lines for SWI02
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                    Case "940SWI03"
                        ' Informationrecord, payers reference - use this one!
                        ' 2 lines for SWI03
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                    Case "940SWI05"
                        ' Outgoing balance - use this one!
                        ' 2 lines for SWI05
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                    Case "940SWI07"
                        ' Informationrecord, payers reference - use this one!
                        ' 2 lines for SWI07
                        sImportRecord = sImportLineBuffer
                        'UPGRADE_WARNING: Couldn't resolve default property of object oFile.ReadLine. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sImportLineBuffer = oFile.ReadLine()
                        sImportRecord = sImportRecord & sImportLineBuffer
                        Exit Do
                End Select
            Else
                ' carry on until eof
            End If

        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBTBWKRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadDnBTBWKRecord = sImportRecord
    End Function

    Function ReadHandelsbanken_DK_DomesticRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from Handelsbanken DK, Erhvervsgiro

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with "ERH -
                    If Left(sImportLineBuffer, 4) <> Chr(34) & "ERH" Then
                        sImportLineBuffer = ""
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadHandelsbanken_DK_DomesticRecord = sImportLineBuffer

    End Function
    Public Function ReadLineFromStromme(ByRef oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet, ByRef lNoOfCols As Integer, Optional ByRef lNoOfRows As Integer = 99999999) As String
        ' Return a semicolonseparated line of cells from an excel worksheet
        ' Function added 14.07.2016
        Static lRowNo As Integer
        Dim i As Integer
        Dim sLine As String = ""
        Dim sTmp As String


        ' Must reset lRowNo for each new file
        If lNoOfRows = -1 Then
            lRowNo = 0
        Else

            lRowNo = lRowNo + 1

            If lRowNo > lNoOfRows Then
                sLine = ""
            Else
                ' Siden Str�mme formatet brukes b�de for Excelfiler, og separerte filer (b�de ; og , og TAB), kan
                ' vi ikke replace ; med , der vi har kun en lang linje, uten kolonner (fra en textfil)
                If Not oExcelSheet.Cells(lRowNo, 2).Value Is Nothing Then

                    For i = 1 To lNoOfCols
                        sTmp = Replace(oExcelSheet.Cells(lRowNo, i).Value, ";", ",", , , vbTextCompare)

                        If InStr(sTmp, "E+") > 0 Then
                            ' Must be sure that it's scientific, like 1,0987654433E+15
                            If IsNumeric(Right$(sTmp, 2)) Then ' Always ends with something like E+15
                                If Mid$(sTmp, 2, 1) = "," Or Mid$(sTmp, 2, 1) = "." Then ' , pr . in second pos
                                    sTmp = Format$(sTmp, StrDup(99, "#"))
                                End If
                            End If
                        End If
                        sLine = sLine & ";" & sTmp
                    Next i
                Else
                    sLine = oExcelSheet.Cells(lRowNo, 1).Value
                End If
                ' Check if all cells are empty
                If Len(Replace(sLine, ";", "")) = 0 Then
                    sLine = ""
                Else
                    ' Remove first ;
                    '06.09.2016 - must first test if there is a ; at the start !!!
                    If Left(sLine, 1) = ";" Then
                        sLine = Mid(sLine, 2)
                    End If
                End If
            End If
        End If

        ReadLineFromStromme = sLine


    End Function
    Public Function ReadLineFromExcel(ByRef oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet, ByRef lNoOfCols As Integer, Optional ByRef lNoOfRows As Integer = 99999999) As String
        ' Return a semicolonseparated line of cells from an excel worksheet
        Static lRowNo As Integer
        Dim i As Integer
        Dim sLine As String
        Dim sTmp As String


        ' Must reset lRowNo for each new file
        If lNoOfRows = -1 Then
            lRowNo = 0
        Else

            lRowNo = lRowNo + 1

            If lRowNo > lNoOfRows Then
                sLine = ""
            Else
                For i = 1 To lNoOfCols
                    'VB.NET - Will this work or do we have to test which celltype is used (Amount, String etc...)
                    '23.09.2009 - Added replace below.
                    'UPGRADE_WARNING: Couldn't resolve default property of object oExcelSheet.Cells. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'sLine = sLine & ";" & Replace(oExcelSheet.Cells(lRowNo, i).Value, ";", ",", , , CompareMethod.Text)
                    sTmp = Replace(oExcelSheet.Cells(lRowNo, i).Value, ";", ",", , , vbTextCompare)

                    If InStr(sTmp, "E+") > 0 Then
                        ' Must be sure that it's scientific, like 1,0987654433E+15
                        If IsNumeric(Right$(sTmp, 2)) Then ' Always ends with something like E+15
                            If Mid$(sTmp, 2, 1) = "," Or Mid$(sTmp, 2, 1) = "." Then ' , pr . in second pos
                                'sTmp = Format$(sTmp, StrDup(99, "#"))
                                ' 12.05.2020 - commented previous line, and replaced with next line to do it properly for scientific cell content
                                sTmp = Format$(Convert.ToDouble(sTmp), "########################")
                            End If
                        End If
                    End If
                    sLine = sLine & ";" & sTmp
                Next i
                ' Check if all cells are empty
                If Len(Replace(sLine, ";", "")) = 0 Then
                    sLine = ""
                Else
                    ' Remove first ;
                    sLine = Mid(sLine, 2)
                End If
            End If
        End If

        ReadLineFromExcel = sLine


    End Function
    Public Function ReadCellFromExcel(ByRef oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet, ByRef lRow As Integer, ByRef lCol As Integer) As String
        ' Return a value from a specified cell
        Dim sCell As String

        'UPGRADE_WARNING: Couldn't resolve default property of object oExcelSheet.Cells. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sCell = oExcelSheet.Cells(lRow, lCol).Value
        ReadCellFromExcel = sCell

    End Function
    Function ReadNewphoneFactoringRecord(ByRef oFile As Scripting.TextStream) As String

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                If Left(sImportLineBuffer, 2) <> "H;" And Left(sImportLineBuffer, 2) <> "S;" And Left(sImportLineBuffer, 2) <> "F;" Then
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError(1067, FilenameImported(), sImportLineBuffer, "10012")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        ReadNewphoneFactoringRecord = sImportRecord

    End Function
    Function ReadNewphoneCustomerRecord(ByRef oFile As Scripting.TextStream) As String

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with H; S; or "981161408;"
                If InStr("HS", Left(sImportLineBuffer, 1)) = 0 And Mid(sImportLineBuffer, 1, 10) <> "981161408;" Then
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError(1067, FilenameImported(), sImportLineBuffer, "10012")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        ReadNewphoneCustomerRecord = sImportRecord

    End Function
    Function Read_0000778_LadeDekkserviceRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start "01" and length 302
                ' 10.02.2008 - hva er 03 og 80 records???
                If Not ((Left(sImportLineBuffer, 2) = "01" Or Left(sImportLineBuffer, 2) = "03" Or Left(sImportLineBuffer, 2) = "80") And Len(sImportLineBuffer) = 302) Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0000778_LadeDekkserviceRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & vbTab & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        Read_0000778_LadeDekkserviceRecord = sImportRecord

    End Function
    Function ReadSGFinansFactoringRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean, Optional ByRef iBuffLen As Short = 0) As String
        ' iBuffLen used for files without CRLF
        Static nLineCount As Integer
        Static bReadToCRLF As Boolean

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line, or a fixed portion of chars
            If iBuffLen > 0 And bReadToCRLF = False Then
                sImportLineBuffer = oFile.Read(iBuffLen)
            Else
                ' Normal, read one line upto CR or LF
                sImportLineBuffer = oFile.ReadLine()
                ' in some cases, we have more thant 128 chars, but ends with CRLF, then next time read hole line!
                If Len(sImportLineBuffer) > 128 Then
                    bReadToCRLF = True
                End If
            End If

            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with 1, 7, 8 or 9. added 2 09.02.2008
                If InStr("12789", Left(sImportLineBuffer, 1)) = 0 Then
                    ' may also have files with a mix of Fakturarecords and Kunderecords!
                    If Left(sImportLineBuffer, 5) <> "K9409" And Not (Left(sImportLineBuffer, 1) = "K" And Mid(sImportLineBuffer, 3, 4) = "9409") Then
                        sImportRecord = ""
                        'Error # 10010 Uventet record

                        Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "ReadSGFinansFakturaRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & " " & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                    End If
                End If
                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        ReadSGFinansFactoringRecord = sImportRecord

    End Function
    Function ReadSGFinansCustomerRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with K9409, or may also have lines like
                If Left(sImportLineBuffer, 5) <> "K9409" And Not (Left(sImportLineBuffer, 1) = "K" And Mid(sImportLineBuffer, 3, 4) = "9409") And Not (Mid(sImportLineBuffer, 2, 1) = "K" And Mid(sImportLineBuffer, 5, 4) = "9409") Then
                    ' may also have files with a mix of Fakturarecords and Kunderecords!
                    If InStr("1789", Left(sImportLineBuffer, 1)) = 0 Then
                        ' 09.02.2008 or other shit like "",
                        If Left(sImportLineBuffer, 3) <> Chr(34) & Chr(34) & "," Then
                            'or may also have lines like ;0;;;;;;;;;;;0;;; (lastline in file)
                            If Len(Replace(sImportLineBuffer, ";", "")) > 10 Then
                                sImportRecord = ""
                                'Error # 10010 Uventet record

                                ' commented out 11.02.2008 to let things through

                                'Err.Raise vbObjectError + 10010, , LRS(10010) & vbCrLf & _
                                ''LRS(64002) & vbTab & "ReadSGFinansCustomerRecord" & vbCrLf & _
                                ''LRS(20004) & " " & sClientNo & vbCrLf & _
                                ''LRS(64003) & vbTab & FilenameImported() & vbCrLf & _
                                ''LRS(64004) & Str(nLineCount) & vbCrLf & _
                                ''Left$(sImportLineBuffer, 60)
                                i = 0 ' added 11.02.2008, read one more line

                            Else
                                sImportLineBuffer = ""
                            End If
                        End If
                    End If
                End If
                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        ReadSGFinansCustomerRecord = sImportRecord

    End Function
    Function Read_0000127_SkaarTransFactoringRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with 0127
                If Left(sImportLineBuffer, 4) <> "0127" Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0000127_SkaarTransFactoringRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & " " & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        Read_0000127_SkaarTransFactoringRecord = sImportRecord

    End Function
    Function Read_0000127_SkaarTransCustomerRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start "0", 7 and 8 blank, length 185
                If Not (Left(sImportLineBuffer, 1) = "0" And Mid(sImportLineBuffer, 7, 2) = "  " And Len(sImportLineBuffer) = 185) Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0000127_SkaarTransCustomerRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & vbTab & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        Read_0000127_SkaarTransCustomerRecord = sImportRecord

    End Function
    Function Read_0001471_WebergFactoringRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with 1 + Tab + 1471
                If Not (Left(sImportLineBuffer, 1) = "1" And Mid(sImportLineBuffer, 2, 1) = Chr(9) And Mid(sImportLineBuffer, 3, 4) = "1471") Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0001471_WebergFactoringRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & " " & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        Read_0001471_WebergFactoringRecord = sImportRecord

    End Function
    Function Read_0001471_WebergCustomerRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with K   9401    1471 (TAB bewtween)
                If Not (Left(sImportLineBuffer, 1) = "K" And Mid(sImportLineBuffer, 2, 1) = Chr(9) And Mid(sImportLineBuffer, 3, 4) = "9401" And Mid(sImportLineBuffer, 7, 1) = Chr(9) And Mid(sImportLineBuffer, 8, 4) = "1471") Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0001471_WebergCustomerRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & " " & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        Read_0001471_WebergCustomerRecord = sImportRecord

    End Function
    Function Read_SG_NORFinans_FactoringRecord(ByRef oFile As Scripting.TextStream, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Kun tall, vanskelig � teste p� noe her
                If Not IsNumeric(Left(sImportLineBuffer, 34)) Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_SG_NORFinansFactoringRecord" & vbCrLf & LRS(20004) & " " & vbCrLf & LRS(64003) & " " & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        Read_SG_NORFinans_FactoringRecord = sImportRecord

    End Function
    Public Function Read_0004889_MillbaCustomerRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with 5-digit customerno in ", then space, then "
                ' "10000" "
                If Not (Left(sImportLineBuffer, 1) = Chr(34) And Mid(sImportLineBuffer, 7, 3) = Chr(34) & " " & Chr(34)) Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0004889_MillbaCustomerRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & vbTab & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        Read_0004889_MillbaCustomerRecord = sImportRecord

    End Function
    Public Function Read_0004193_T�nsbergRammeCustomerRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer
        ' "10053";"";"FOTOGRAF LEANDER";"STORGATA 7";"3611";"KONGSBERG";"32 73 14 64"
        ' "10055";"980811212";"HOLMEN FOTO A/S * *";"HOLMENSENTERET";"1379";"NESBRU";"66 84 29 00"

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with  " number and have at least five ;
                ' "10000";"
                If Not Left(sImportLineBuffer, 1) = Chr(34) And IsNumeric(Mid(sImportLineBuffer, 2, 1)) Then
                    If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) < 5 Then
                        sImportRecord = ""
                        'Error # 10010 Uventet record

                        Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0004193_T�nsbergRammeRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & vbTab & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))
                    End If
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        Read_0004193_T�nsbergRammeCustomerRecord = sImportRecord

    End Function
    Function Read_0001327_StrandCoFakturaRecord(ByRef oFile As Scripting.TextStream, ByRef sClientNo As String, ByRef bResetLineCounter As Boolean) As String
        Static nLineCount As Integer

        If bResetLineCounter Then
            nLineCount = 1
        Else
            nLineCount = nLineCount + 1
        End If

        sImportLineBuffer = ""
        sImportRecord = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' Must start with 327 or 217
                If Left(sImportLineBuffer, 3) <> "327" And Left(sImportLineBuffer, 3) <> "217" Then
                    sImportRecord = ""
                    'Error # 10010 Uventet record

                    Err.Raise(vbObjectError + 10010, , LRS(10010) & vbCrLf & LRS(64002) & vbTab & "Read_0001327_StrandCoFactoringRecord" & vbCrLf & LRS(20004) & " " & sClientNo & vbCrLf & LRS(64003) & " " & FilenameImported() & vbCrLf & LRS(64004) & Str(nLineCount) & vbCrLf & Left(sImportLineBuffer, 60))

                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        Read_0001327_StrandCoFakturaRecord = sImportRecord

    End Function
    Function Read_DnBNORUS_ACH_IncomingRecord(ByVal oFile As Scripting.TextStream, ByVal oCargo As Cargo) As String
        Dim sTmpLine As String

        sImportLineBuffer = vbNullString

        Do While Not oFile.AtEndOfStream

            ' For ACH, read two line of 80 chars pr record, except for those starting with "A"
            ' For Lockbox, only one line
            'Read two lines
            sImportLineBuffer = oFile.ReadLine()
            ' XNET 11.10.11
            ' In ACH files we may find empty lines in between - then disregard them
            If Len(Trim$(sImportLineBuffer)) = 0 Then
                ' read next
                sImportLineBuffer = oFile.ReadLine()
            End If
            sImportLineBuffer = sImportLineBuffer & Trim$(sTmpLine)

            If Left$(oCargo.REF, 3) = "ACH" Then
                'Read two lines for ACH
                If Not Left$(sImportLineBuffer, 1) = "A" And Not oFile.AtEndOfStream Then
                    ' XNET 11.10.11
                    ' In ACH files we may find empty lines in between - then disregard them
                    sTmpLine = oFile.ReadLine()
                    If Len(Trim$(sTmpLine)) = 0 Then
                        ' read next
                        sTmpLine = oFile.ReadLine()
                    End If
                    sImportLineBuffer = sImportLineBuffer & Trim$(sTmpLine)

                    'sImportLineBuffer = sImportLineBuffer & Trim$(oFile.ReadLine())
                End If
            End If ' If Left$(oCargo.REF, 3) = "ACH" Then

            ' Replace � with \ . DnBNOR sometimes gives us � when they mean \ !!!
            sImportLineBuffer = Replace(sImportLineBuffer, "�", "\")
            ' Replace ~ with \ . DnBNOR sometimes gives us ~ when they mean \ !!!
            sImportLineBuffer = Replace(sImportLineBuffer, "~", "\")
            ' Replace � with * . DnBNOR sometimes gives us � when they mean * !!!
            sImportLineBuffer = Replace(sImportLineBuffer, "�", "*")
            ' XNET 23.10.2013
            ' and � instead of *
            sImportLineBuffer = Replace(sImportLineBuffer, "�", "*")
            ' and # instead of ?
            sImportLineBuffer = Replace(sImportLineBuffer, "#", "?")

            '-------------------------------------------------------------------------
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    ' Not easy to test, but all lines have len 80
                    ' First pos A, F, 4, 5, 6, 7 or 8
                    'If Not (Len(sImportLineBuffer) = 80 And InStr("FA45678", Left$(sImportLineBuffer, 1)) > 0) Then
                    If InStr(sImportLineBuffer, "END OF JES SPOOL FILE") > 0 Then
                        ' last line with JCL-cardinfo, skip
                        sImportLineBuffer = vbNullString
                        ' XNET 11.09.2013 New file from Wells Fargo has records starting with 1, 2 and 9 as well
                    ElseIf InStr("FA45678129:", Left$(sImportLineBuffer, 1)) = 0 Then
                        ' 21.05.2008 added : for MT940
                        'babelbank_error: Feil i format -
                        sImportLineBuffer = vbNullString
                        'Error # 10010 Unexptected record
                        ' XNET 21.12.2011 changed from DnBNOR to DNB
                        Call BabelImportError("DnBUS_ACH_Incoming", FilenameImported(), sImportLineBuffer, "10010", , , , oFile.Line)
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        Read_DnBNORUS_ACH_IncomingRecord = sImportLineBuffer


    End Function

    Function ReadBankOfAmerica_UKRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a BankOfAmerica_UK record
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                If Mid(sImportLineBuffer, 1, 6) <> "SIGNON" And Mid(sImportLineBuffer, 1, 3) <> "CRF" And Mid(sImportLineBuffer, 1, 3) <> "P20" And Mid(sImportLineBuffer, 1, 3) <> "P40" And Mid(sImportLineBuffer, 1, 3) <> "P41" And Mid(sImportLineBuffer, 1, 3) <> "P42" And Mid(sImportLineBuffer, 1, 3) <> "P50" And Mid(sImportLineBuffer, 1, 3) <> "P53" And Mid(sImportLineBuffer, 1, 3) <> "P55" And Mid(sImportLineBuffer, 1, 3) <> "P56" And Mid(sImportLineBuffer, 1, 3) <> "P70" And Mid(sImportLineBuffer, 1, 3) <> "P80" Then
                    'babelbank_error: Feil i format - Ikke korrekt fil!
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError("BankOfAmerica_UK", FilenameImported(), sImportLineBuffer, "10010")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadBankOfAmerica_UKRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadBankOfAmerica_UKRecord = sImportRecord

    End Function
    Function ReadGjensidige_S2000_PAYMULRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a Dirrem-file
        ' 80 chars

        ' NB - IKKE PROGRAMMERT DETTE ER KUN EN KOPI
        i = 1
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with "0"
                If Mid(sImportLineBuffer, 1, 3) <> "000" And Mid(sImportLineBuffer, 1, 3) <> "001" And Mid(sImportLineBuffer, 1, 3) <> "010" And Mid(sImportLineBuffer, 1, 3) <> "020" And Mid(sImportLineBuffer, 1, 3) <> "021" And Mid(sImportLineBuffer, 1, 3) <> "030" And Mid(sImportLineBuffer, 1, 3) <> "040" And Mid(sImportLineBuffer, 1, 3) <> "044" And Mid(sImportLineBuffer, 1, 3) <> "045" And Mid(sImportLineBuffer, 1, 3) <> "050" And Mid(sImportLineBuffer, 1, 3) <> "060" And Mid(sImportLineBuffer, 1, 3) <> "061" And Mid(sImportLineBuffer, 1, 3) <> "070" And Mid(sImportLineBuffer, 1, 3) <> "062" And Mid(sImportLineBuffer, 1, 3) <> "071" And Mid(sImportLineBuffer, 1, 3) <> "072" And Mid(sImportLineBuffer, 1, 3) <> "078" And Mid(sImportLineBuffer, 1, 3) <> "080" And Mid(sImportLineBuffer, 1, 3) <> "084" Then
                    sImportRecord = ""
                    'Error # 10012
                    Call BabelImportError((BabelFiles.FileType.Gjensidige_S2000_PAYMUL), FilenameImported(), sImportLineBuffer, "10012", , , , oFile.Line - 1) ' reports one line to many be any reason
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadGjensidige_S2000_PAYMULRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadGjensidige_S2000_PAYMULRecord = sImportRecord

    End Function
    Function ReadDnBNOR_US_PentaRecord(ByRef oFile As Scripting.TextStream) As Object ', oProfile As Profile)  ' As Object)
        ' Read one record from a DnBNOR_US_Penta record
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line, must start with A, C, W, I, L, H, D, F
                If Mid(sImportLineBuffer, 1, 1) <> "A" And Mid(sImportLineBuffer, 1, 1) <> "C" And Mid(sImportLineBuffer, 1, 1) <> "W" And Mid(sImportLineBuffer, 1, 1) <> "I" And Mid(sImportLineBuffer, 1, 1) <> "L" And Mid(sImportLineBuffer, 1, 1) <> "H" And Mid(sImportLineBuffer, 1, 1) <> "D" And Mid(sImportLineBuffer, 1, 1) <> "F" Then
                    ' or it is a headerline with exactly 85 characters:
                    If Len(sImportLineBuffer) <> 85 Then ' 04.02.2009 changed from 75
                        'babelbank_error: Feil i format - Ikke korrekt fil!
                        sImportRecord = ""
                        'Error # 10012
                        Call BabelImportError("DnBNOR_US_Penta", FilenameImported(), sImportLineBuffer, "10010")
                    End If
                End If
                ' The format has two lines pr record, but use ReadGeneric for second line
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBNOR_US_PentaRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ReadDnBNOR_US_PentaRecord = sImportRecord

    End Function
    Public Function Read_DnV_UK_SageRecord(ByRef oFile As Scripting.TextStream, Optional ByRef iNoOfChars As Short = 0) As Object
        ' Det norske Veritas (DnV in UK - from Sage)
        sImportLineBuffer = ""
        sImportRecord = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line, 100 chars, remove chr(10) and chr(13)
            sImportLineBuffer = oFile.Read(iNoOfChars)
            ' if chr(10), then remove, but must read one more char
            If InStr(sImportLineBuffer, Chr(10)) > 0 Then
                sImportLineBuffer = Replace(sImportLineBuffer, Chr(10), "") & oFile.Read(1)
            End If
            ' if chr(13), then remove, but must read one more char
            If InStr(sImportLineBuffer, Chr(13)) > 0 Then
                sImportLineBuffer = Replace(sImportLineBuffer, Chr(13), "") & oFile.Read(1)
            End If

            If Len(sImportLineBuffer) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                ' must have DET NORSKE VERITAS from pos 47
                If Mid(sImportLineBuffer, 47, 18) = "DET NORSKE VERITAS" Then
                    ' OK
                ElseIf Mid(sImportLineBuffer, 1, 22) = "DET NORSKE VERITASOF1A" Then
                    ' eof records, read rest as one long record
                    Do While Not oFile.AtEndOfStream
                        sImportLineBuffer = sImportLineBuffer & oFile.Read(1)
                    Loop
                Else
                    'babelbank_error: Feil i format - Ikke korrekt fil!
                    sImportRecord = ""
                    'Error # 10012
                    If Not oFile.AtEndOfStream Then
                        Call BabelImportError((BabelFiles.FileType.DnV_UK_Sage), FilenameImported(), sImportLineBuffer, "10011")
                    Else
                        ' Hvis det ligger noe noe kukk i slutten av filen
                        sImportLineBuffer = ""
                    End If
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                Exit Do
            End If
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object Read_DnV_UK_SageRecord. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Read_DnV_UK_SageRecord = sImportRecord
    End Function
    Function ReadABNAmroBTL91Record(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from a ABNAmro BTL-91 file

        sImportLineBuffer = ""

        'Read one line
        sImportLineBuffer = oFile.ReadLine()

        'Check line
        '1) Must start with 11,21,22,23,24,31,or 41
        If Mid(sImportLineBuffer, 1, 2) <> "11" And Mid(sImportLineBuffer, 1, 2) <> "21" And Mid(sImportLineBuffer, 1, 2) <> "22" And Mid(sImportLineBuffer, 1, 2) <> "23" And Mid(sImportLineBuffer, 1, 2) <> "24" And Mid(sImportLineBuffer, 1, 2) <> "31" And Mid(sImportLineBuffer, 1, 2) <> "41" Then
            'babelbank_error: Feil i format - Ikke korrekt fil
            sImportRecord = ""
            'Error # 10012
            Call BabelImportError((BabelFiles.FileType.ABNAmro_BTL91), FilenameImported(), sImportLineBuffer, "10012")
        End If


        ReadABNAmroBTL91Record = sImportLineBuffer

    End Function
    Function ReadHandelsbanken_BankavstemmingRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from Handelsbanken Bankavstemmingsfile

        sImportLineBuffer = ""

        'Read one line
        sImportLineBuffer = oFile.ReadLine()

        'Check line
        If Mid(sImportLineBuffer, 1, 2) <> "S-" And Mid(sImportLineBuffer, 1, 2) <> "VS" And Mid(sImportLineBuffer, 1, 2) <> "T-" And Mid(sImportLineBuffer, 1, 2) <> "VT" And Mid(sImportLineBuffer, 1, 2) <> "HH" Then
            'babelbank_error: Feil i format - Ikke korrekt fil
            sImportRecord = ""
            'Error # 10012
            Call BabelImportError((BabelFiles.FileType.ABNAmro_BTL91), FilenameImported(), sImportLineBuffer, "10012")
        End If


        ReadHandelsbanken_BankavstemmingRecord = sImportLineBuffer

    End Function
    Function ReadABA_AustraliaRecord(ByRef oFile As Scripting.TextStream) As String
        sImportLineBuffer = ""

        'Read one line
        sImportLineBuffer = oFile.ReadLine()

        'Check line
        '1) Must start with 0, 1 or 7
        If Left(sImportLineBuffer, 1) <> "0" And Left(sImportLineBuffer, 1) <> "1" And Left(sImportLineBuffer, 1) <> "7" Then
            'babelbank_error: Feil i format - Ikke korrekt fil
            sImportRecord = ""
            'Error # 10012
            Call BabelImportError((BabelFiles.FileType.ABA_Australia), FilenameImported(), sImportLineBuffer, "10012")
        End If

        ReadABA_AustraliaRecord = sImportLineBuffer

    End Function
    Function ReadNemkoCanadaSalaryRecord(ByRef oFile As Scripting.TextStream) As String
        sImportLineBuffer = ""

        'Read one line
        sImportLineBuffer = oFile.ReadLine()

        'Check line
        '1) Must start with $ or 0
        If Left(sImportLineBuffer, 1) <> "0" And Left(sImportLineBuffer, 1) <> "$" Then
            'babelbank_error: Feil i format - Ikke korrekt fil
            sImportRecord = ""
            'Error # 10012
            Call BabelImportError((BabelFiles.FileType.NemkoCanadaSalary), FilenameImported(), sImportLineBuffer, "10012")
        End If

        ReadNemkoCanadaSalaryRecord = sImportLineBuffer

    End Function
    'XNET 04.11.2010 - added next function
    Function ReadGiroDirektRecord(ByVal oFile As Scripting.TextStream)
        Dim sTmp As String
        sImportLineBuffer = ""

        'Read one line

        sImportLineBuffer = oFile.ReadLine()
        sImportLineBuffer = Replace(sImportLineBuffer, Chr(0), "", , , vbTextCompare)

        If Len(Trim(sImportLineBuffer)) = 0 Then

            'Test if end of file mark
        ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
            sImportLineBuffer = ""
        Else
            'Check line
            ' Linelength 80
            ' 4 char recordtype
            If Len(sImportLineBuffer) > 80 Then
                sImportLineBuffer = ""
            Else
                sTmp = Left$(sImportLineBuffer, 4)
                If _
                    sTmp = "MH00" Or _
                    sTmp = "MT00" Or _
                    sTmp = "PI00" Or _
                    sTmp = "BA00" Or _
                    sTmp = "BA01" Or _
                    sTmp = "BA02" Or _
                    sTmp = "BA03" Or _
                    sTmp = "BE01" Or _
                    sTmp = "BE02" Or _
                    sTmp = "BE03" Or _
                    sTmp = "BM99" Or _
                    sTmp = "CNDB" Or _
                    sTmp = "CNKR" Then
                Else
                    sImportLineBuffer = ""
                End If
            End If
        End If

        ReadGiroDirektRecord = sImportLineBuffer
    End Function
    'XNET 01.12.2010 added function ReadISABEL_APS130_ForeignRecord
    Function ReadISABEL_APS130_ForeignRecord(ByVal oFile As Scripting.TextStream)
        Dim sTmp As String
        sImportLineBuffer = ""

        'Read one line

        sImportLineBuffer = oFile.ReadLine()
        sImportLineBuffer = Replace(sImportLineBuffer, Chr(0), "", , , vbTextCompare)

        If Len(Trim(sImportLineBuffer)) = 0 Then

            'Test if end of file mark
        ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
            sImportLineBuffer = ""
        Else
            'Check line
            ' Linelength 128 (and some 93 !!!)
            ' XNET 06.12.2010 added also <> 93
            'XNET - 17.12.2010 - Changed next line
            If Len(sImportLineBuffer) < 125 And Len(sImportLineBuffer) <> 93 Then
                sImportLineBuffer = ""
            Else
                sTmp = Left$(sImportLineBuffer, 1)
                If _
                    sTmp = "0" Or _
                    sTmp = "1" Or _
                    sTmp = "9" Then
                Else
                    sImportLineBuffer = ""
                End If
            End If
        End If

        ReadISABEL_APS130_ForeignRecord = sImportLineBuffer
    End Function
    Function Read_NachaUS_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString

        'Read one line

        sImportLineBuffer = oFile.ReadLine()
        sImportLineBuffer = Replace(sImportLineBuffer, Chr(0), vbNullString, , , vbTextCompare)

        If Len(Trim(sImportLineBuffer)) = 0 Then

            'Test if end of file mark
        ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
            sImportLineBuffer = "" 'vbNullString
        Else
            'Check line
            ' Linelength 94
            If Len(sImportLineBuffer) <> 94 Then
                sImportLineBuffer = ""  'vbNullString
            Else
                sTmp = Left$(sImportLineBuffer, 1)
                If _
                    sTmp = "1" Or _
                    sTmp = "5" Or _
                    sTmp = "6" Or _
                    sTmp = "7" Or _
                    sTmp = "8" Or _
                    sTmp = "9" Then
                    ' OK
                Else
                    ' not ok
                    sImportLineBuffer = ""  'vbNullString
                End If
            End If
        End If

        Read_NachaUS_Record = sImportLineBuffer

    End Function
    Public Function Read_GjensidigeJDE_Basware_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString

        If oFile.AtEndOfStream Then
            sImportLineBuffer = vbNullString
        Else

            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Len(Trim(sImportLineBuffer)) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                sImportLineBuffer = vbNullString
            Else
                'Check line
                'sTmp = Left$(sImportLineBuffer, 6)
                'If _
                '    sTmp = "A1LE01" Or _
                '    sTmp = "A1LE02" Or _
                '    sTmp = "A1LE03" Or _
                '    sTmp = "A1LE04" Or _
                '    sTmp = "A2LE01" Or _
                '    sTmp = "A2LE02" Or _
                '    sTmp = "A2LE03" Or _
                '    sTmp = "A2LE04" Then
                sTmp = Mid$(sImportLineBuffer, 3, 4)
                If Left(sImportLineBuffer, 1) = "A" And _
                        sTmp = "LE01" Or _
                        sTmp = "LE02" Or _
                        sTmp = "LE03" Or _
                        sTmp = "LE04" Then

                    ' OK
                Else
                    ' not ok
                    sImportLineBuffer = vbNullString
                End If
            End If

            'If Not (Left$(sImportLineBuffer, 5) = "A1LE0") Then
            If Not (Left$(sImportLineBuffer, 1) = "A" And Mid(sImportLineBuffer, 3, 3) = "LE0") Then
                ' 100009 Wrong format
                Call BabelImportError(BabelFiles.FileType.GjensidigeJDE_Basware, FilenameImported(), sImportLineBuffer, "10009", , , , oFile.Line)
            End If
        End If

        Read_GjensidigeJDE_Basware_Record = sImportLineBuffer

    End Function
    Public Function Read_GjensidigeJDE_FileNet_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString

        If oFile.AtEndOfStream Then
            sImportLineBuffer = vbNullString
        Else

            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Len(Trim(sImportLineBuffer)) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                sImportLineBuffer = vbNullString
            Else
                'Check line
                sTmp = Left$(sImportLineBuffer, 6)
                If _
                    sTmp = "05ML01" Or _
                    sTmp = "05LE01" Or _
                    sTmp = "05LE02" Then
                    ' OK
                Else
                    ' not ok
                    sImportLineBuffer = vbNullString
                End If
            End If

            If Not (Left$(sImportLineBuffer, 2) = "05") Then
                ' 100009 Wrong format
                Call BabelImportError(BabelFiles.FileType.GjensidigeJDE_FileNet_XLedger, FilenameImported(), sImportLineBuffer, "10009", , , , oFile.Line)
            End If
        End If

        Read_GjensidigeJDE_FileNet_Record = sImportLineBuffer

    End Function
    Public Function Read_GjensidigeJDE_P2000_XLedger_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString

        If oFile.AtEndOfStream Then
            sImportLineBuffer = vbNullString
        Else

            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Len(Trim(sImportLineBuffer)) = 0 Then

                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                sImportLineBuffer = vbNullString
            Else
                'Check line
                sTmp = Left$(sImportLineBuffer, 6)
                If _
                    sTmp = "22ML01" Or _
                    sTmp = "22LE01" Or _
                    sTmp = "22LE02" Or _
                    sTmp = "22LE03" Then
                    ' OK
                Else
                    ' not ok
                    sImportLineBuffer = vbNullString
                End If
            End If

            If Not (Left$(sImportLineBuffer, 2) = "22") Then
                ' 100009 Wrong format
                Call BabelImportError(BabelFiles.FileType.GjensidigeJDE_P2000_XLedger, FilenameImported(), sImportLineBuffer, "10009", , , , oFile.Line)
            End If
        End If

        Read_GjensidigeJDE_P2000_XLedger_Record = sImportLineBuffer

    End Function
    Public Function Read_GjensidigeJDE_HB01_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString
        Dim sTmp1 As String
        Dim sTmp2 As String
        Dim sTmp3 As String

        If oFile.AtEndOfStream Then
            sImportLineBuffer = vbNullString
        Else

            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Mid$(sImportLineBuffer, 3, 4) <> "HB01" Then
                sImportLineBuffer = ""
                ' 100009 Wrong format
                Call BabelImportError("GjensidigeJDE_HB01", FilenameImported(), sImportLineBuffer, "10009", , , , oFile.Line)
            End If
        End If

        Read_GjensidigeJDE_HB01_Record = sImportLineBuffer

    End Function
    Public Function Read_GjensidigeJDE_F2100_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString
        Dim sTmp1 As String
        Dim sTmp2 As String
        Dim sTmp3 As String

        If oFile.AtEndOfStream Then
            sImportLineBuffer = vbNullString
        Else

            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Not (Left$(sImportLineBuffer, 4) = "0060") Then
                sImportLineBuffer = ""
                ' 100009 Wrong format
                Call BabelImportError("GjensidigeJDE_F2100", FilenameImported(), sImportLineBuffer, "10009", , , , oFile.Line)
            End If
        End If

        Read_GjensidigeJDE_F2100_Record = sImportLineBuffer

    End Function
    Public Function Read_GjensidigeJDE_HB01_H1_Record(ByVal oFile As Scripting.TextStream) As String
        Dim sTmp As String
        sImportLineBuffer = vbNullString
        Dim sTmp1 As String
        Dim sTmp2 As String
        Dim sTmp3 As String

        If oFile.AtEndOfStream Then
            sImportLineBuffer = vbNullString
        Else

            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            'If Not (Left$(sImportLineBuffer, 8) = "20HB0110") Then
            If Not (Mid(sImportLineBuffer, 3, 4) = "HB01") Then
                sImportLineBuffer = ""
                ' 100009 Wrong format
                Call BabelImportError(BabelFiles.FileType.GjensidigeJDE_HB01_H1, FilenameImported(), sImportLineBuffer, "10009", , , , oFile.Line)
            End If
        End If

        Read_GjensidigeJDE_HB01_H1_Record = sImportLineBuffer

    End Function
    ' XokNET  12.04.2012 added DTAUS
    Function ReadDTAZVRecord(ByVal oFile As Scripting.TextStream, ByVal sImportLineBuffer As String) As String

        If Len(sImportLineBuffer) < 1000 Then
            If oFile.AtEndOfStream = False Then
                sImportLineBuffer = sImportLineBuffer & oFile.Read(2000)
                sImportLineBuffer = Replace(Replace(sImportLineBuffer, Chr(13), vbNullString), Chr(10), vbNullString)

            Else
                If Len(sImportLineBuffer) < 128 Then
                    sImportLineBuffer = PadRight(sImportLineBuffer, 128, " ")
                End If
            End If
        End If

        ReadDTAZVRecord = sImportLineBuffer

    End Function
    ' XNET 30.10.2012 new function
    Function ReadDanskeBankCollectionRecord(ByVal oFile As Scripting.TextStream) As String
        sImportLineBuffer = vbNullString

        Do While Not oFile.AtEndOfStream

            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) > 0 Then
                'Test if end of file mark
                If Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                    Exit Do
                Else
                    'Check line
                    '1) Must start with "COLLECTION" -
                    If (Left$(sImportLineBuffer, 12) <> Chr(34) & "COLLECTION" & Chr(34)) Then
                        sImportLineBuffer = vbNullString
                    Else
                        ' OK
                        Exit Do
                    End If
                End If
            End If
        Loop

        ReadDanskeBankCollectionRecord = sImportLineBuffer

    End Function
    ' XNET 30.10.2012 new function
    Function ReadAvtalegiroRecord(ByVal oFile As Scripting.TextStream) As String
        ' Read one record from avtalegiro-file
        ' 80 chars

        i = 1
        sImportLineBuffer = vbNullString
        sImportRecord = vbNullString

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            'If it is the first line we continue reading and treats the next line as the first
            ' line. This will also solve the problem when some files have blank lines in the
            ' end before EOF like Jan-Petters ancient editor: BRIEF.
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with NY00 or NY01 - if not no Autogiro!
                ' or NY02 (autogiro engangs) 29.12.05. Added test for NY09 to handle mix of autogiro and OCR
                ' 22.12.06 Added avtalegiro (NY21)
                If Mid$(sImportLineBuffer, 1, 4) <> "NY21" And Mid$(sImportLineBuffer, 1, 4) <> "NY00" Then
                    'babelbank_error: Feil i format - Ikke korrekt fil!
                    sImportRecord = vbNullString
                    'Error # 10012
                    Call BabelImportError(vbBabel.BabelFiles.FileType.Avtalegiro, FilenameImported(), sImportLineBuffer, "10012")
                End If

                'Check if padded to 80 char lines:
                If Len(sImportLineBuffer) < 80 Then
                    ' Must pad with trailing blanks
                    sImportLineBuffer = Mid$(sImportLineBuffer & Space(80), 1, 80)
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        ReadAvtalegiroRecord = sImportRecord

    End Function
    ' XNET 01.03.2013 New function
    Function ReadReadNordea_SE_FakturabetalningService(ByVal oFile As Scripting.TextStream)
        ' Read one record from a Nordea SE Plusgirofile
        ' 80 chars

        i = 1
        sImportLineBuffer = vbNullString
        sImportRecord = vbNullString

        Do While (oFile.AtEndOfStream = False) And i < 2
            'Read one line
            sImportLineBuffer = oFile.ReadLine()
            If Len(sImportLineBuffer) = 0 Then
                i = 1
                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                If InStr("012345678", Mid$(sImportLineBuffer, 1, 1)) = 0 Or Len(sImportLineBuffer) <> 80 Then
                    sImportRecord = vbNullString
                    'Error # 10012
                    Call BabelImportError(vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService, FilenameImported(), sImportLineBuffer, "10012")
                End If

                ' Only one line for each record
                sImportRecord = sImportLineBuffer
                i = i + 1
            End If
        Loop

        ReadReadNordea_SE_FakturabetalningService = sImportRecord

    End Function

    ' XokNET 15.11.2013 added next function
    Function ReadSEBSisuRecord(ByVal oFile As Scripting.TextStream)
        ' Read one record SEBSisu
        sImportLineBuffer = vbNullString

        Do While (oFile.AtEndOfStream = False)
            'Read one line

            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with digit or R , and at least 8 following digits
                '2) Linelengt must not exeed 80 chars + CRLF
                If InStr("01D23456R", Left$(sImportLineBuffer, 1)) = 0 Then
                    sImportLineBuffer = vbNullString
                ElseIf Len(sImportLineBuffer) > 85 Then
                    sImportLineBuffer = vbNullString
                Else
                    sImportRecord = sImportLineBuffer
                    Exit Do
                End If
            End If
            If EmptyString(sImportRecord) Then
                Call BabelImportError(BabelFiles.FileType.SEB_Sisu, FilenameImported(), sImportLineBuffer, "10010")  ' Unknown record
            End If
        Loop

        ReadSEBSisuRecord = sImportRecord
    End Function
    Function ReadCorporateFilePayments_Record(ByVal oFile As Scripting.TextStream)
        ' To be continued
        Do While (oFile.AtEndOfStream = False)
            'Read one line

            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                'Test if end of file mark
            ElseIf Asc(Mid$(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must not start with two chars and then two digits
                '2) Linelengt must not exeed 80 chars + CRLF
                'If Len(sImportLineBuffer) <> 80 Then
                ' 12.01.2015 test only > 80. Lines may be shorter than 80 !!
                ' 03.03.2015 AND can be longer, have added some spaces
                If Len(sImportLineBuffer) > 85 Then
                    sImportLineBuffer = vbNullString
                Else
                    sImportRecord = sImportLineBuffer
                    Exit Do
                End If
            End If
            If EmptyString(sImportRecord) Then
                Call BabelImportError(vbBabel.BabelFiles.FileType.NordeaCorporateFilePayments, FilenameImported(), sImportLineBuffer, "10010")  ' Unknown record
            End If
        Loop

        ReadCorporateFilePayments_Record = sImportRecord

    End Function
    Function ReadSwedbank_CardReconciliationRecord(ByRef oFile As Scripting.TextStream, Optional ByRef sSpecial As String = "") As String ', oProfile As Profile)
        ' Read one record Swedbank card transactions
        sImportLineBuffer = ""

        Do While (oFile.AtEndOfStream = False)
            'Read one line

            sImportLineBuffer = oFile.ReadLine()
            If Len(Trim(sImportLineBuffer)) = 0 Then
                'Test if end of file mark
            ElseIf Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            Else
                'Check line
                '1) Must start with "F"
                '2) Linelengt = 2000 chars + CRLF
                If Not Left(sImportLineBuffer, 1) = "F" Then
                    sImportLineBuffer = ""
                    '25.09.2017 - Removed the following test. A testfile from Swedbank was not 2000 positions in each line
                    'ElseIf Len(sImportLineBuffer) <> 2000 Then
                    'sImportLineBuffer = ""
                Else
                    sImportRecord = sImportLineBuffer
                    Exit Do
                End If
            End If
        Loop

        ReadSwedbank_CardReconciliationRecord = sImportRecord
    End Function
    Function ReadKTLRecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from a KTL-file
        ' Finnish Incoming payments
        '------------------------------------

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            ElseIf Len(sImportLineBuffer) < 5 And oFile.AtEndOfStream Then
                sImportLineBuffer = ""
                Exit Do
            Else
                'Check line
                ' Len = 90
                ' 11.03.2021 - we have seen a file with record length 91
                If Len(sImportLineBuffer) <> 90 And Len(sImportLineBuffer) <> 91 Then
                    sImportRecord = ""
                    'Error # 10011 '  Wrong format !
                    Call BabelImportError((BabelFiles.FileType.KTL), FilenameImported(), sImportLineBuffer, "10011")

                    '1) Must start with 0, 3, 9
                ElseIf "0359".IndexOf(Left(sImportLineBuffer, 1)) = -1 Then
                    sImportRecord = ""
                    'Error # 10011 '  Wrong format !
                    Call BabelImportError((BabelFiles.FileType.KTL), FilenameImported(), sImportLineBuffer, "10011")
                Else
                    Exit Do
                End If
            End If
        Loop

        ReadKTLRecord = sImportLineBuffer

    End Function
    Function ReadTITORecord(ByRef oFile As Scripting.TextStream) As String
        ' Read one record from a TITO-file
        ' Finnish Statement files
        '------------------------------------

        sImportLineBuffer = ""

        Do While Not oFile.AtEndOfStream
            'Read one line
            sImportLineBuffer = oFile.ReadLine()

            If Asc(Mid(sImportLineBuffer, 1, 1)) = 26 Then
                Exit Do
            ElseIf Len(sImportLineBuffer) < 5 And oFile.AtEndOfStream Then
                sImportLineBuffer = ""
                Exit Do
            Else
                'Check line

                '1) Must start with T00, T10, T11, T50, T70
                Select Case Left(sImportLineBuffer, 3)
                    Case "T00", "T10", "T11", "T40", "T50", "T70"
                        ' OK
                        Exit Do
                    Case Else
                        sImportRecord = ""
                        'Error # 10011 '  Wrong format !
                        Call BabelImportError((BabelFiles.FileType.TITO), FilenameImported(), sImportLineBuffer, "10011")
                End Select
            End If
        Loop

        ReadTITORecord = sImportLineBuffer

    End Function

End Module
