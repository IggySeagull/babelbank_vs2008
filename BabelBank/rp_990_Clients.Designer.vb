<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_990_Clients
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OleDBDataSource1 As DataDynamics.ActiveReports.DataSources.OleDBDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_990_Clients))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtClientNo = New DataDynamics.ActiveReports.TextBox
        Me.txtNoOfRecords = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.lblClientNo = New DataDynamics.ActiveReports.Label
        Me.lblClientName = New DataDynamics.ActiveReports.TextBox
        Me.lblNoOfRecords = New DataDynamics.ActiveReports.Label
        Me.lblTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.grFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        CType(Me.txtClientNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoOfRecords, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClientNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNoOfRecords, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtClientNo, Me.txtNoOfRecords, Me.txtClientName, Me.txtTotalAmount})
        Me.Detail.Height = 0.3333333!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtClientNo
        '
        Me.txtClientNo.Border.BottomColor = System.Drawing.Color.Black
        Me.txtClientNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientNo.Border.LeftColor = System.Drawing.Color.Black
        Me.txtClientNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientNo.Border.RightColor = System.Drawing.Color.Black
        Me.txtClientNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientNo.Border.TopColor = System.Drawing.Color.Black
        Me.txtClientNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientNo.DataField = "ClientNo"
        Me.txtClientNo.Height = 0.19!
        Me.txtClientNo.Left = 0.0!
        Me.txtClientNo.Name = "txtClientNo"
        Me.txtClientNo.Style = "text-align: right; "
        Me.txtClientNo.Text = "txtClientNo"
        Me.txtClientNo.Top = 0.0!
        Me.txtClientNo.Width = 1.0!
        '
        'txtNoOfRecords
        '
        Me.txtNoOfRecords.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNoOfRecords.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoOfRecords.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNoOfRecords.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoOfRecords.Border.RightColor = System.Drawing.Color.Black
        Me.txtNoOfRecords.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoOfRecords.Border.TopColor = System.Drawing.Color.Black
        Me.txtNoOfRecords.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoOfRecords.DataField = "NoOfRecords"
        Me.txtNoOfRecords.Height = 0.19!
        Me.txtNoOfRecords.Left = 3.7!
        Me.txtNoOfRecords.Name = "txtNoOfRecords"
        Me.txtNoOfRecords.Style = "text-align: right; "
        Me.txtNoOfRecords.Text = "txtNoOfRecords"
        Me.txtNoOfRecords.Top = 0.0!
        Me.txtNoOfRecords.Width = 0.6!
        '
        'txtClientName
        '
        Me.txtClientName.Border.BottomColor = System.Drawing.Color.Black
        Me.txtClientName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientName.Border.LeftColor = System.Drawing.Color.Black
        Me.txtClientName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientName.Border.RightColor = System.Drawing.Color.Black
        Me.txtClientName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientName.Border.TopColor = System.Drawing.Color.Black
        Me.txtClientName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtClientName.CanGrow = False
        Me.txtClientName.DataField = "ClientName"
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 1.2!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = ""
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.0!
        Me.txtClientName.Width = 2.5!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.CanShrink = True
        Me.txtTotalAmount.DataField = "TotalAmount"
        Me.txtTotalAmount.Height = 0.19!
        Me.txtTotalAmount.Left = 4.3!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "text-align: right; "
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.0!
        Me.txtTotalAmount.Width = 2.0!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4576389!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.RightColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.TopColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "text-align: center; font-size: 18pt; "
        Me.lblrptHeader.Text = "Clients"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Border.BottomColor = System.Drawing.Color.Black
        Me.Line3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.LeftColor = System.Drawing.Color.Black
        Me.Line3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.RightColor = System.Drawing.Color.Black
        Me.Line3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.TopColor = System.Drawing.Color.Black
        Me.Line3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.45!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.45!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right; "
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right; "
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right; "
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grHeader
        '
        Me.grHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.lblClientNo, Me.lblClientName, Me.lblNoOfRecords, Me.lblTotalAmount})
        Me.grHeader.DataField = "BreakField"
        Me.grHeader.Height = 0.45!
        Me.grHeader.Name = "grHeader"
        Me.grHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.RightColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.TopColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Height = 0.25!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'lblClientNo
        '
        Me.lblClientNo.Border.BottomColor = System.Drawing.Color.Black
        Me.lblClientNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientNo.Border.LeftColor = System.Drawing.Color.Black
        Me.lblClientNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientNo.Border.RightColor = System.Drawing.Color.Black
        Me.lblClientNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientNo.Border.TopColor = System.Drawing.Color.Black
        Me.lblClientNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientNo.Height = 0.19!
        Me.lblClientNo.HyperLink = Nothing
        Me.lblClientNo.Left = 0.0!
        Me.lblClientNo.Name = "lblClientNo"
        Me.lblClientNo.Style = "text-align: right; font-size: 10pt; "
        Me.lblClientNo.Text = "lblClientNo"
        Me.lblClientNo.Top = 0.0!
        Me.lblClientNo.Width = 1.0!
        '
        'lblClientName
        '
        Me.lblClientName.Border.BottomColor = System.Drawing.Color.Black
        Me.lblClientName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientName.Border.LeftColor = System.Drawing.Color.Black
        Me.lblClientName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientName.Border.RightColor = System.Drawing.Color.Black
        Me.lblClientName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientName.Border.TopColor = System.Drawing.Color.Black
        Me.lblClientName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblClientName.CanShrink = True
        Me.lblClientName.Height = 0.19!
        Me.lblClientName.Left = 1.2!
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.OutputFormat = resources.GetString("lblClientName.OutputFormat")
        Me.lblClientName.Style = "text-align: left; font-size: 10pt; "
        Me.lblClientName.Text = "lblClientName"
        Me.lblClientName.Top = 0.0!
        Me.lblClientName.Width = 2.5!
        '
        'lblNoOfRecords
        '
        Me.lblNoOfRecords.Border.BottomColor = System.Drawing.Color.Black
        Me.lblNoOfRecords.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNoOfRecords.Border.LeftColor = System.Drawing.Color.Black
        Me.lblNoOfRecords.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNoOfRecords.Border.RightColor = System.Drawing.Color.Black
        Me.lblNoOfRecords.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNoOfRecords.Border.TopColor = System.Drawing.Color.Black
        Me.lblNoOfRecords.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNoOfRecords.Height = 0.19!
        Me.lblNoOfRecords.HyperLink = Nothing
        Me.lblNoOfRecords.Left = 3.7!
        Me.lblNoOfRecords.Name = "lblNoOfRecords"
        Me.lblNoOfRecords.Style = "text-align: right; font-size: 10pt; "
        Me.lblNoOfRecords.Text = "lblNoOfRecords"
        Me.lblNoOfRecords.Top = 0.0!
        Me.lblNoOfRecords.Width = 0.6!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Height = 0.19!
        Me.lblTotalAmount.Left = 4.3!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "text-align: right; font-size: 10pt; "
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.0!
        Me.lblTotalAmount.Width = 2.0!
        '
        'grFooter
        '
        Me.grFooter.CanShrink = True
        Me.grFooter.Height = 0.25!
        Me.grFooter.Name = "grFooter"
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'rp_990_Clients
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        OleDBDataSource1.ConnectionString = ""
        OleDBDataSource1.SQL = "Select * from"
        Me.DataSource = OleDBDataSource1
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.49!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; " & _
                    "", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.txtClientNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoOfRecords, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClientNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNoOfRecords, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtClientNo As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtNoOfRecords As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents lblClientNo As DataDynamics.ActiveReports.Label
    Friend WithEvents lblClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblNoOfRecords As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
End Class
