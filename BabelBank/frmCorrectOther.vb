Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmCorrectOther
    Inherits System.Windows.Forms.Form

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
    End Sub

    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 9, 132, 702, 132)
        e.Graphics.DrawLine(Pens.Orange, 9, 207, 702, 207)
    End Sub

    Private Sub frmCorrectOther_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)
        'Me.lblErrMessage.BackColor = System.Drawing.Color.Cyan 'vbHighlight

    End Sub
    'Private Sub dtERA_DateNew_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
    '    'x  Display errormessage for this field, stored in control.tag;
    '    Me.lblErrMessage.Text = dtERA_DateNew.Tag
    '    PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    '    'SelectAllText
    'End Sub
    Private Sub txtDate_Contract_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        '    'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtDate_Contract.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtERA_DealMadeWith_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtERA_DealMadeWith.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtERA_DealMadeWith.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtERA_ExchangeRateAgreed_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtERA_ExchangeRateAgreed.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtERA_ExchangeRateAgreed.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtERA_ContractRef_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtERA_ContractRef.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtERA_ContractRef.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    'Private Sub txtStatebank_Code_GotFocus()
    ''x  Display errormessage for this field, stored in control.tag;
    'Me.lblErrMessage.Caption = txtStateBank_Code.Tag
    'PrepareErrorMessage   ' Icon for Error or Warning, skip first pos in Text
    'SelectAllText
    'End Sub
    Private Sub cmbStatebank_Code_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbStateBank_Code.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = cmbStateBank_Code.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        'SelectAllText - removed 06.07.2010
    End Sub
    Private Sub txtStateBank_Text_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtStateBank_Text.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtStateBank_Text.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub chkUrgent_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkUrgent.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = chkUrgent.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        ' XNET 21.08.2012, commented next, cant select text in  chkbox
        'SelectAllText
    End Sub
    Private Sub txtE_CountryCode_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtE_Countrycode.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtE_Countrycode.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Function PrepareErrorMessage() As Object
        ' Icon for Error or Warning, skip first pos in Text
        If VB.Left(Me.lblErrMessage.Text, 1) = "E" Then
            Me.Image2(9).Visible = True
            Me.imgExclamation(9).Visible = False
            Me.lblErrMessage.ForeColor = System.Drawing.Color.Red
        ElseIf VB.Left(Me.lblErrMessage.Text, 1) = "W" Then
            ' Warning
            Me.Image2(9).Visible = False
            Me.imgExclamation(9).Visible = True
            Me.lblErrMessage.ForeColor = System.Drawing.Color.Red 'vbGreen
        End If
        Me.lblErrMessage.Text = Mid(Me.lblErrMessage.Text, 2)


    End Function
End Class
