Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

<System.Runtime.InteropServices.ProgId("BabelFiles_NET.BabelFiles")> Public Class BabelFiles
    Implements System.Collections.IEnumerable

    Private oProfile As vbBabel.Profile 'Profile attached to this  object
    'local variable to hold collection
    Private mCol As Collection

    Private myBBDB_AccessConnection As System.Data.OleDb.OleDbConnection

    Private bMATCH_MatchOCR As Boolean
    Private bUseCommitAgainstBBDB As Boolean

    'New properties regarding manual matching
    Private lMATCH_NoOfOpenItems As Integer
    Private lMATCH_NoOfOpenItemsPriorToActivePayment As Integer
    Private sMATCH_ErrorText As String
    Private sMATCH_LastValidBookDate As String

    ' 20.05.05 Variables for showing payments in Match_Manual
    Private sFilter As String
    Private sClientGroupFilter As String 'Added 16.10.2013
    Private sUserIDInPayment As String
    Private sMyUserID As String
    ' 11.10.06 Added SpecialID
    Private sSpecialID As String
    Private bUseCommitRollback As Boolean   ' added 17.07.2014 to be able to Rollback if errors
    Private bClientGroupFilterIsAccount As Boolean
    Dim bSQLServer As Boolean = False '18.07.2016 - Added the use of bSQLServer in all collections

    Public Enum FileType
        Unknown = 0
        OCR = 1
        AutoGiro = 2
        Dirrem = 3
        Telepay = 4
        coda = 5
        PostbOCROld = 6
        AutogiroEngangs = 7
        TelepayTBIO = 8
        ErhvervsGiro_Udbetaling = 9
        Leverantorsbetalningar = 10
        Handelsbanken_DK_Domestic = 11
        OCR_Simulated = 12
        HDP_Citibank = 13
        NordeaUnitel = 14
        TBIXML = 15
        OCR_Bankgirot = 16
        OCR_Postgirot = 17
        Czech_payments_Citibank = 18
        DnB_TBIW_Standard = 19
        DTAUS = 20
        ShipNet = 21
        CitiDirect_US_Flat_File = 22
        DanskeBankTeleService = 23
        BACS_For_TBI = 24
        First_Union_ACH = 25
        DnBNattsafe = 26
        SecuritasNattsafe = 27
        Navision_for_SI_Data = 28
        MT940 = 29
        DanskeBankLokaleDanske = 30
        DnBTBUKIncoming = 31
        BabelBank_Innbetaling = 32
        Telepay2 = 33
        DnBNOR_TBIW_Denmark = 34
        LM_Domestic_Finland = 35
        LUM_International_Finland = 36
        PBS = 37
        bec = 38
        Excel = 39
        Sydbank = 40
        SEK2 = 41
        DnBTBWK = 42
        Bellin_TRDB_RAXCM = 43
        NordeaDK_EDI = 44
        Excel_XLS = 45
        Bankdata_DK = 46
        SEBScreen_International = 47
        SEBScreen_Denmark = 48
        SEBScreen_Germany = 49
        Bankdata_DK_4_45 = 50
        Avprickning = 51
        OCR_Bankgirot_Simulated = 52
        Nordea_Innbet_Ktoinf_utl = 53
        Bankdata_DK_45 = 54
        TellerXML = 55
        BOLS = 56
        Nacha = 57
        PTG = 58
        Periodiska_Betalningar = 59
        SEB_Screen = 60
        HB_GlobalOnline_MT101 = 61
        BankOfAmerica_UK = 62
        HSBC_iFile = 63
        BAILockBox = 64
        MT940E_Handelbanken = 65
        BabelBank_Mapped_Out = 66
        KLINK_ExcelUtbetaling = 67
        DnBNOR_UK_Standard = 68
        PayEx_CSV = 69
        Erhvervsformat_BEC_DK = 70
        TelepayPlus = 71
        Autogiro_Bankgirot = 72
        MT940E_Handelbanken_DEBANDCRED = 73
        BgMax = 74
        StandardSeparatedExport = 75
        AgressoGL = 76
        MT101 = 77
        ABNAmro_BTL91 = 78
        Handelsbanken_Bankavstemming = 79
        ABA_Australia = 80
        Australia_Excel_XLS = 81
        SAP_PAYEXT_PEXR2002 = 82
        ' XNET 02.11.2010, added Girodirekt (Plusgiro)
        GiroDirekt_Plusgiro = 83
        ' XNET 29.11.2010, added from here
        DTAZV = 84
        ' XNET 30.11.2010 added Isabel
        ISABEL_APS130_Foreign = 85
        ' XNET 13.01.2011
        SebScreen_Finland = 86
        ' XNET 08.04.2011
        Excel_Export = 87  ' Common Excel export for several layouts
        Pain001 = 88
        Bacstel = 89
        Pain002 = 90
        Pain002_Rejected = 91
        MT100 = 92
        Camt054_Outgoing = 93
        Camt054_Incoming = 94
        Camt054 = 95
        Camt053 = 96
        GenericXML = 97
        SEB_CI_Incoming = 98
        GenericText = 99
        DanskeBankCollection = 100
        DanskeBankColDebitor = 101
        Avtalegiro = 102
        SEBScreen_UK = 103
        Nordea_SE_Plusgirot_FakturabetalningService = 104
        Total_IN = 105
        ' XNET 18.06.2013
        ' Added Netx Exportformat (DirekteRemittering and Avtalegiro trekk)
        Nets_Export = 106
        ' XNET 27.08.2013
        SEB_Sisu = 107
        MT940_DebitsOnly = 108
        Camt053_Outgoing = 109
        Camt053_Incoming = 110
        NordeaCorporateFilePayments = 111

        ' 07.04.2015 - Only in .Net
        KAR_kontovask_Nets_AccountOwner = 115
        KAR_kontovask_Nets_AccountPayment = 112
        '13.05.2015 - Only in .Net
        BabelBank_Innbetaling_ver2 = 113

        ' 06.07.2015
        SPISU = 114
        BankAxess_DebitCard_XLS = 116
        Swedbank_CardReconciliation = 117
        ' 01.06.2016
        DanskeBankFinland = 118
        Commerzbank_CSV_Incoming = 119
        BGAutogiro = 120
        JPMorgan_Norway = 121
        Vipps_XML = 122
        Telepay_Split_TBMITBRI = 123
        SG_CSV_Incoming = 125
        Bambora_XML = 126
        DNBSingapore_Standard = 127
        Corporate_Banking_Interbancario = 128
        Sage_Salaries = 129  '(Same import as Phuel Oil)
        Klarna_XML = 130
        ' 4 HSBC formats first time for Vismagic solution
        HSBC_SG_ACH = 131
        HSBC_SG_INT = 132
        HSBC_GB_ACH = 133
        HSBC_GB_INT = 134
        Payex_XML = 135
        Vipps_Excel = 136
        Lindorff_Innbetaling = 137
        Excel_Incoming_DIV = 138
        DanskeBankSweden = 139
        Lindorff_Gebyr = 140
        Adyen_XML = 141
        OCBC_SG = 142
        TellerCardXML = 143
        eFaktura_e2b = 144
        KTL = 145
        TITO = 146
        PowerOfficeGo = 147
        Betalingsservice_0602 = 148
        NordeaSecureEnvelope = 149
        NavigaIncoming = 150


        CREMUL = 501
        PAYMUL = 502
        BANSTA = 503
        DEBMUL = 504
        CONTRL = 505
        PAYMULAutack = 506 ' Paymul + Autack with encrypting
        COACSU = 507 ' Paymul + Autack with encrypting
        ' "Hjelpeformater" som ikke er definert i vanlig Format-oppsett
        '---------------------------------------------------------------
        SGClientCSVOCROnly = 901
        RTV2 = 902
        NorgesForskningsrad_OCR = 903
        OCR_Bankgirot_As_Message = 904
        BAILockBoxACH = 905
        BAILockBoxMT940 = 906
        Nordea_Liv_Registrert = 907
        TellerXMLNew = 908
        RTVAltInn = 909   ' XNET - 10.02.2012 - Changed value

        ' "Systemformater"
        ' ----------------
        '25.05.2016 - Added new
        BB_AutoDetect_Outgoing_ISO20022 = 987
        '06.07.2015 - Added new format
        BabelBank_BackPayment = 988
        ' XNET 12.03.2013 - added Visma_Databaseformat
        Visma_Database = 989
        ' 14.09.2010 added specialformat for empty files
        ' XNET 07.12.2010 added 990
        BB_AutoDetect_Outgoing = 990
        EmptyFileFormat = 991
        Database_Innbetaling = 992
        SG_SaldoFil_Aq_CSV = 993
        SG_SaldoFil_Aq_Fixed = 994
        OCR_Bankgirot_Simulated_KID = 995
        OCR_Simulated_KID = 996
        OneTimeFormat = 997
        Database = 998
        ReportOnly = 999
        ' Firmaspesifike formater
        ' -----------------------
        CapsyAG = 1001
        OrklaMediaExcel = 1002
        OriflameExcel = 1003
        CREMULIntern = 1004
        Telepay_BETFOR0099 = 1005
        ISS_SDV = 1006
        LHLEkstern = 1007
        ISS_LonnsTrekk = 1008
        SRI_Access = 1009
        SRI_AccessReturn = 1010
        eCommerce_Logistics = 1011
        Compuware = 1012
        SeaTruck = 1013
        WinOrg = 1014
        TelepayUNIX = 1015
        VB_Internal = 1016
        Elkjop = 1017
        TazettAutogiro = 1018
        ISS_Paleggstrekk = 1019
        MMM_Reiseregning = 1020
        MaritechInnbetaling = 1021
        OCR_For_BKK = 1022
        Navision_SIData_Reskontro = 1023
        BACS_for_Agresso = 1024
        ControlConsultExcel = 1025
        DnBStockholmExcel = 1026
        QXLOCR = 1027
        FirstNordic = 1028
        Kvaerner_Singapore_Salary = 1029
        File_to_DnBNOR_Hamburg = 1030
        Visma_Global = 1031
        pgs = 1032
        Sandvik_Tamrock = 1033
        NCL = 1034
        Creno_XML = 1035
        RTV = 1036 'This format is not added to formats in the DB.
        PINK_LAAN = 1037
        Agresso_Lindorff = 1038
        Panfish_Scotland = 1039
        NextFinancial_Incoming = 1040
        Autocare_Norge = 1041
        StrommeSingapore = 1042
        PGSUK_Salary = 1043
        EltekSG_Salary = 1044
        Sudjaca = 1045
        Tandberg_ACH = 1046
        Visma_Business = 1047
        Sage_UK_Bacs = 1048
        DNV_Germany = 1049
        Axapta_ColumbusIT = 1050
        CyberCity = 1051
        Navision_KonicaMinolta = 1052
        NSA = 1053
        AquariusOutgoing = 1054
        TVaksjonen = 1055
        Kredinor_K90 = 1056
        SGFINANSBOLS = 1057
        AquariusIncoming = 1058
        AquariusIncomingOCR = 1059
        SG_ClientReporting = 1060
        SGClientCSVNotOCR = 1061
        SI_SIRI = 1062
        Politiets_CSV = 1063
        SGFinans_Faktura = 1064 'Fakturafilen ved innsending av faktura/kundefil
        AIG_Account = 1065
        ProfundoInnbet = 1066
        NewPhoneFactoring = 1067
        OMustadSingapore = 1068
        NewphoneCustomerFile = 1069
        SGFinans_Kunde = 1070
        WritePolitietsFellesForbund_Visma_Global = 1071
        Skagen_fondene = 1072
        SGFinansInvoicefileCopying = 1073
        EXCEL_Innbetaling = 1074
        SG_0000778_LadeDekkservice = 1075
        SG_0000127_SkaarTransFactoring = 1076
        SG_0000127_SkaarTransCustomer = 1077
        DnBNORUS_ACH_Incoming = 1078
        Eurosoft = 1079
        SG_0001327_StrandCoFactoring = 1080
        SG_DirekteBetalingsbrev = 1081
        PGS_Sweden = 1082
        Bluewater_Retur = 1083
        Silver_XML = 1084
        Gjensidige_S2000_PAYMUL = 1085
        NorgesForskningsrad = 1086
        Gjensidige_S2000_BANSTA = 1087
        Gjensidige_S2000_DEBMUL = 1088
        SG_SaldoFil_Aq = 1089
        SG_SaldoFil_Kunde = 1090
        SG_FakturaHistorikFil_Aq = 1091
        SG_FakturaHistorikkFil_Kunde = 1092
        TeekaySingapore = 1093
        SG_0004889_MillbaFactoring = 1095
        SG_0004193_T�nsbergRammeFactoring = 1096
        SG_0001471_WebergFactoring = 1097
        SG_0001471_WebergCustomer = 1098
        SG_NORFinans_Factoring = 1099
        Gjensidige_S2000_CREMUL = 1100
        SG_0001217_AutomaxFactoring = 1101 ' Imports NORFinans_Factoring and Standard (; sep) Customerfile!!
        Gjensidige_S2000_CONTRL = 1102
        SG_0000032_LadeDekkserviceSYD = 1103
        Nordea_Liv_bankavst = 1104
        Base64Importfile = 1105
        Base64Exportfile = 1106
        Biomar_UK = 1107
        Axapta_Hands = 1108
        ActavisExcel_XLS = 1109
        DnBNOR_US_Penta = 1110
        DnBNORFinans_CREMUL = 1111
        Alliance_Apotek = 1112
        RECSingapore = 1113
        DnV_UK_Sage = 1114
        Formula_B9000 = 1115
        OCR_Santander = 1116
        Seajack_Wages = 1117
        KongsbergAutomotive_BACS = 1118
        Vingcard_Remittering = 1119
        Gjensidige_Utbytte = 1120
        LindorffInkasso_TNT = 1121
        DnBNORUS_ACH_Matching = 1122
        Ulefos_Salary_FI = 1123
        INS2000 = 1124
        NemkoCanadaSalary = 1125
        OdinFakeExport = 1126
        OCR_Conecto_AF = 1127
        XML_Conecto = 1128
        ' XOKNET 29.10.10 
        DnBNOR_Connect_Test = 1129
        AccountControl = 1130
        Conecto_Dekkmann = 1131
        InfoEasy = 1132
        'XNET - 05.01.2011
        Exide_XLS = 1133
        GC_Rieber_UK = 1134
        ' XNET 22.06.2011
        AkerSolutions_UK = 1135
        ' XNET 29.06.2011
        MT942_Odin = 1136
        ' XNET 14.07.2011
        DnV_UK_BACS = 1137
        ' XNET 15.07.2011
        Morpol_UK_Bacs = 1138
        ' XNET 11.08.2011
        Givertelefon = 1139
        ' XNET 01.09.2011
        SeaCargo_UK = 1140
        ' XNET 26.10.2011
        'XNET - 07.02.2012 - Changed the line below from 1140 to 1141
        NRX_Ax = 1141
        ' XNET 27.10.2011
        CremulSplitted = 1142
        Polyeurope_UK = 1143
        ' XNET 12.12.2011
        Falck_UK = 1144
        ' XNET 12.12.2011
        Wachovia_820_Receivables = 1145
        ' XNET 05.12.2011
        eGlobal = 1146
        ' XNET 20.12.2011
        DnBNOR_US_ACH_820 = 1147
        'XNET 19.03.2012
        Handicare_UK = 1148
        'XNET 24.04.2012
        Odenberg = 1149
        'XNET 09.05.2012
        Fjorkraft_utbetaling = 1150
        ' XNET 29.05.2012
        eFaktura_b2b_Splitted = 1151
        'XNET 30.07.2012
        Comarch_Incoming = 1152
        'XNET 22.08.2012
        Vingcard_netting = 1153
        'Ikke i VB6
        Connect_WCF = 1154
        'Ikke i VB6
        Connect_WCF_Remit = 1155
        'Ikke i VB6
        Gjensidige_S2000_FINSTA = 1157

        ' XNET 21.01.2013
        Duni_Salary_UK = 1156
        ' hvor er 1157?

        ' XNET 28.02.2013
        TGS_UK = 1158
        ' XNET 05.03.2013
        Spike_UK = 1159
        ' XNET 09.04.2013
        Vroon_UK = 1160
        ' XNET 10.05.2013
        Faroe = 1161
        ' XOKNET 13.05.2013
        AndresenBil = 1162
        ' XNET 29.06.2013
        ColArt_UK = 1163
        ' XNET 17.07.2013
        SageReport = 1164
        'XNET 06.11.2012
        Elavon_Specification = 1165
        ' XNET 10.01.2013
        Rapp_Ecosse_Bacs = 1166
        'XOKNET 13.08.2013
        DRiVR_UK = 1167
        ' added 09.09.2013 for Bilia
        Bilia_IFS = 1168
        Infotjenester = 1169 'Only used in .NET
        Visma_Collectors_innbetaling = 1170 'Only used in .NET
        Vroon_Scotland = 1171
        ' XOKNET 07.07.2014
        StrommeSingaporeG3 = 1172
        ' XOKNET 21.07.2014
        ColArt_Moorbrook = 1173
        ' XOKNET 27.11.2014
        Phuel_Oil = 1174
        ' XOKNET 19.02.2015 - Only in .Net
        Agresso_Patentstyret = 1175
        ' XOKNET 19.02.2015 - Only in .Net
        Ins2000_Instech_InPayment = 1176
        ' 07.04.2015 - Only in .Net
        Gjensidige_kontovask = 1177
        Archer_US_CSV = 1178
        Patentstyret_Order = 1179
        Axapta_Advania = 1180
        HandicareBACS = 1181
        NorLines = 1182
        Pain001_SplitFIDKSE_AndOthers = 1183   ' For DNB Pain.001 to be splitted in FI,DK,SE in one file, rest in other file
        TelepayDOIN = 1184
        NHC_Philippines = 1185
        Maritech_CSV_Import = 1186
        DOF_Supplier = 1187
        DOF_Salary = 1188
        NetSuite = 1189
        MySoftCRM = 1190
        XLedger = 1191
        Elavon_2017 = 1192
        FlyWire = 1193
        SkyBridge_BACS = 1194
        Fabriken_SGFEQ = 1195
        PayPal_ActiveBrands = 1196
        Telepay_Split_TBMITBRI_AND_DOIN = 1197
        ApticIPCXML = 1198
        Musto_BACS = 1199
        Confide_Excel = 1200
        Factolink = 1201
        HelseFonna_XML_Nets = 1202
        Videotel_BACS = 1203
        Vipps_Nets = 1204
        Camt054_Bama = 1205
        Maconomy = 1206
        Sanitetsforeningen = 1207
        Utleiemegleren = 1208
        TridentSalary = 1209
        EuroFinans_Matched = 1210
        EuroFinans_UnMatched = 1211
        Modhi_Internal = 1212 '27.04.2022

        ' -----------------------------------
        'Subformats of other formats (export)
        'TBIW
        TBI_General = 1501
        TBI_FI = 1502
        TBI_ACH = 1503
        TBI_Salary = 1504
        TBI_Reference = 1505
        ' 16.03.2010 added Local
        tbi_local = 1506
        SG_FakturaFil_Aq_CSV = 1510


        ' --------------------------------------------------------------
        ' Special formats for Gjensidige JD Edwards converting
        GjensidigeJDE_Basware = 1801
        GjensidigeJDE_FileNet = 1802
        GjensidigeJDE_HB01 = 1803
        GjensidigeJDE_A1 = 1804
        ' 1805 deleted
        GjensidigeF2100_SE_DK = 1806
        GjensidigeJDE_P2000 = 1807
        GjensidigeJDE_CSV_S12 = 1808
        GjensidigeJDE_P2000_NY = 1809
        GjensidigeJDE_Basware_XML = 1810

        GjensidigeJDE_Accounts_XML = 1811
        GjensidigeJDE_Costcenters_XML = 1812
        GjensidigeJDE_Suppliers_XML = 1813
        GjensidigeJDE_UAccounts_XML = 1814
        GjensidigeJDE_UnderRegnskap_XML = 1815
        GjensidigeJDE_Users_XML = 1816
        GjensidigeJDE_AdvancedPermissions_XML = 1817
        GjensidigeJDE_HB01_H1 = 1818
        GjensidigeJDE_GL02b = 1819
        GjensidigeJDE_GL02_SYS = 1820   'XLedger GL02 SYS

        GjensidigeJDE_XLedger_Accounts_XML = 1821
        GjensidigeJDE_XLedger_Costcenters_XML = 1822
        GjensidigeJDE_XLedger_Underregnskap_XML = 1823
        GjensidigeJDE_XLedger_Suppliers_XML = 1824

        GjensidigeJDE_Basware_GL02_XML = 1825   ' 18.08.2020- added Importformat for export GjensidigeJDE_GL02_SYS = 1820
        GjensidigeJDE_FileNet_XLedger = 1826    ' 21.09.2020
        GjensidigeJDE_GL02_SYSandAP02 = 1827   'XLedger GL02 SYS
        GjensidigeJDE_F2100_SE_DK_GL02b = 1828
        GjensidigeJDE_Aditro = 1829
        GjensidigeJDE_AdvancedPermissionsXLedger_XML = 1830
        GjensidigeJDE_IDIT = 1831
        GjensidigeJDE_P2000_XLedger = 1832
        GjensidigeJDE_INPAS = 1833
        GjensidigeJDE_NICE = 1834

    End Enum

    'Line type enum
    Public Enum LineType
        UnknownLine = 0
        Batch = 1
        Header = 2
        Data = 3
    End Enum

    Public Enum SeqnoType
        FromImportfile = 0
        PerFilesetup = 1
        PerClient = 2
    End Enum

    Public Enum PrintType
        NoPrint = 0
        PrintBatches = 1
        PrintPayments = 2
        PrintInvoices = 3
        Rejects = 11
        CREMUL_Total = 501
        CREMUL_Payment = 502
        CREMUL_PaymentUnmatched = 503
        CREMUL_PaymentMatched = 504
        CREMUL_Control = 505
        CREMUL_PaymentUnmatchedWithOCR = 506
        CREMUL_PaymentMatchedWithOCR = 507
        NattsafeTotal = 601
        NattsafeDifferanse = 602
        CREMUL_NotarUnmatched = 1001
        CREMUL_NotarMatched = 1002
        StatKartSamlet = 1003
        DnBNORFinans_CREMUL = 1005
    End Enum

    Public Enum Encoding
        No_Specific_Encoding = 0
        ASCII = 1
        UTF_8WithoutBOM = 2
        UTF_8WithBOM = 3
    End Enum
    Public Enum Bank
        'If changed, remember to change in database, and
        '  in the FindMappingFilename-function in EDI2XML.
        '  and in the function GetEnumBank in BB_utils


        '****************************************************************
        '
        ' NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNB!
        ' Remember to alter some bankspecific functions when ading new banks
        ' So far IsDnB and IsSEB in bb_utils
        '
        '****************************************************************

        No_Bank_Specified = 0
        Nordea_eGateway = 1
        Nordea_CorporateAccess = 2
        CitiDirect = 1001
        Standard_Chartered = 1003
        A_Dutch_bank = 31000
        'XokNET -Changed the Enum and added NL on the next two items
        ABN_Amro_NL = 31101
        ' XOKNET 15.10.2013 added Ing Bank
        Ing_Bank_NL = 31102
        'XokNET - Added RaboBank
        Rabobank_NL = 31103
        A_Finnish_bank = 36000
        CommerzBank_PT = 35102
        Nordea_FI = 35801
        DNB_FI = 35802
        Danske_Bank_SF = 35803 ' 15.05.2015 - her var det surr mellomg PRO og Classic, har lagt inn den under med 35804, og denne manglet i Pro
        SEB_Finland = 35804
        A_Hungarian_bank = 36000
        Citibank_H = 36001
        A_Czech_bank = 42000
        Citibank_CZ = 42001
        A_British_bank = 44000 '* Not XNETet
        HSBC_UK = 44001  ' NOT XNETet!
        Danske_Bank_UK = 44002

        A_Danish_bank = 45000
        Danske_Bank_dk = 45001
        Nordea_DK = 45002
        BG_Bank = 45003
        Sydbank = 45004
        Citibank = 1002 ' Generic Citibank
        '****************************************************************
        '
        ' NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNB!
        ' Remember to alter some bankspecific functions when ading new banks
        ' So far IsDnB, IsSEB, IsNorwegianBank in bb_utils
        '
        '****************************************************************
        A_Swedish_bank = 46000
        Svenska_Handelsbanken = 46001
        Nordea_SE = 46002
        SEB_SE = 46003
        Foreningssparebanken = 46004
        DnBNOR_SE = 46005
        DanskeBank_SE = 46006

        A_Norwegian_bank = 47000
        DnB = 47001
        Nordea_NO = 47002
        Gjensidige_NOR = 47003
        Handelsbanken = 47004
        Fokus_Bank = 47005
        SEB_NO = 47006
        Danske_Bank_NO = 47007
        Sparebank1 = 47008
        Sparebanken_Midt_Norge = 47009
        BBS = 47010
        Fellesdata = 47011
        DnBNOR_INPS = 47012
        Swedbank = 47013
        BITS_NorwegianStandard = 47100

        DNB_GB = 44001

        DNB_DK = 45001
        SEB_DK = 45005
        A_German_bank = 49000
        DnBNOR_D = 49001
        CommerzBank_DE = 49002
        DNB_US = 10001

        SEB_PL = 48005

        HSBC_SG = 65001
        UBS_Bank_Switzerland = 41001
        SocieteGenerale_France = 33001
        '****************************************************************
        '
        ' NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNB!
        ' Remember to alter some bankspecific functions when ading new banks
        ' So far IsDnB, IsSEB, IsNorwegianBank in bb_utils
        '
        '****************************************************************

    End Enum

    Public Enum AccountType
        'Some countries may use different accounttypes bur they will all be stored in E_Account
        'Use the AccountType to understand which accounttype that is stated.
        'Sweden is an example.
        NoAccountType = 0
        SE_Bankgiro = 4601
        SE_Bankaccount = 4602
        SE_PlusGiro = 4603
        ' 03.09.2019
        NO_Nets = 4701

    End Enum
    Public Enum PayCode
        ' 18.07.2018
        ' We have many different paycodes, listed in function bbSetPayCode
        ' It's easier with ENums than numbers, so this is a start of a list of paycodes
        FasterPayment_GB = 178
    End Enum

    Public Enum ChequeType
        noCheque = 0
        SendToPayer = 1
        SendToReceiver = 2
    End Enum

    Public Enum BankBranchType
        NoBranchType = 0
        SWIFT = 1
        Fedwire = 2
        SortCode = 3
        Bankleitzahl = 4
        Chips = 5
        US_ABA = 6
        Bankleitzahl_Austria = 7
        MEPS = 8 ' Singapore
        BSB = 9 ' added 30.03.2010, Bank State Branch code, Australia
        CC = 10 ' added 20.04.2010, Canada Clearing
        ZASortCode = 11  ' added 27.02.2017 SouthAfrica sortcode
    End Enum

    Public Enum BackupType
        Unknown = 0
        BeforeImport = 1
        BeforeExport = 3
        AfterExport = 5
        FromSetup = 10

    End Enum
    Public Enum CountryName
        Unknown = 0
        Norway = 1 'HANDNOKK
        Sweden = 2 'HANDSESS
        Denmark = 3 'HANDDKKK
        Scandinavia = 1001
        Finland = 4 'HANDFIHH
        Iceland = 5
        Nordic = 1002
        Estonia = 6
        Latvia = 7
        Lithuania = 8
        Baltic = 1003
        Great_Britain = 9 'HANDGB22
        Ireland = 10
        Germany = 11 'HANDDEFF
        Netherlands = 12 'HANDNL2A
        Belgium = 13
        Luxemburg = 14 'HANDLULL
        France = 15 'HANDFRPP
        Poland = 16 'HANDPLPW
        Russia = 17
        Hong_Kong = 105 'HANDHKKK
        Singapore = 110 'HANDSGSG
        Canada = 211 '
        USA = 212 'HANDUS33
        Australia = 301
    End Enum
    Public Enum MatchStatus
        NotMatched = 0
        ProposedMatched = 1
        PartlyMatched = 2
        Matched = 3
    End Enum

    Public Enum MatchType
        OpenInvoice = -1
        MatchedOnInvoice = 0
        MatchedOnCustomer = 1
        MatchedOnGL = 2
        MatchedOnSupplier = 3
    End Enum

    Public Enum TypeOfStructuredInfo
        NotStructured = -1
        StructureTypeUnknown = 0
        StructureTypeInvoice = 1
        StructureTypeInvoiceRetrievedFromMessage = 2
        StructureTypeInvoiceRetrievedFromKID = 3
        StructureTypeKID = 4
        StructureTypeWrongKID = 5
        StructureTypeWrongKIDValidatedByBabelBank = 6
    End Enum

    'see also vbUtils
    'Public Const TypeGLAccount = 1
    'Public Const TypeCustomerNoAccount = 2
    'Public Const TypeKIDNumber = 3
    'Public Const TypeFromERP = 4

    Public Enum PaymentMethod
        Not_Specified = -1
        ACH_Credit = 1
        ACH_Debit = 2
        ACH_PreFormat = 3
        Book_Transfer = 4
        Drawdown_Chargewire = 5
        Drawdown_Chargewire_PreFormat = 6
        Funds_Transfer = 7
        Book_and_Funds_Transfer_PreFormat = 8
    End Enum

    Public Enum TypeofRecords
        ' Type of records to export, used mainly when exporting in matchingcases
        AllRecords = 0
        OCRonly = 1
        NotOcr = 2
        BankTotals = 10
    End Enum
    Public Enum InvoiceQualifier
        Ordinary = 0
        NotToBeExported = 3 'Should not be exported, but should be reported
        Temporary = 4 'Used as temporary invoices, may be deleted and should not be exported
        ' and not a part of any calculation. This is not yet implemented
    End Enum
    'XNET - 16.04.2013 - Added next Enum
    Public Enum TypeOfTransaction
        TransactionType_Unknown = 0
        TransactionType_Outgoing = 1
        TransactionType_Incoming = 2
        TransactionType_DirectDebit = 3
        TransactionType_eInvoice = 4
        TransactionType_OutgoingAndIncoming = 5
    End Enum
    Public Enum ReportTime
        'At what time the report should be run, stored in Report in Profile.mdb and used in BabelReport to run the correct report at the right time.
        After_Import = 1000
        Inside_SpecialRoutines = 1100
        After_SpecialRoutine = 1200
        After_OCR_Export = 2000
        After_Autogiro_Export = 2100
        Before_Export = 4000
        After_Export = 4010
        Before_End = 5000
        Customer_Specific = 9999
    End Enum
    Public Enum BreakLevel
        NoBreak = 0
        BreakOnAccountDay = 1
        BreakOnBatch = 2
        BreakOnPayment = 3
        BreakOnAccount = 4
        BreakOnClient = 5
    End Enum

    Public Enum ControlLevel
        NoControl = 0
        NormalControl = 2
        TightControl = 4
    End Enum
    Public Function Add(Optional ByRef sKey As String = "") As vbBabel.BabelFile
        'create a new object
        Dim objNewMember As vbBabel.BabelFile
        objNewMember = New vbBabel.BabelFile

        sKey = Str(mCol.Count() + 1)

        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
            objNewMember.Index = CObj(sKey)
        End If

        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing


    End Function
    Public Function VB_AddWithObject(ByRef oExistingBabelFile As BabelFile) As vbBabel.BabelFile
        Dim sKey As String
        'create a new object
        sKey = Str(mCol.Count() + 1)

        mCol.Add(oExistingBabelFile, sKey)
        oExistingBabelFile.Index = CObj(sKey)

        'return the object created
        VB_AddWithObject = oExistingBabelFile

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As vbBabel.BabelFile
        Get
            'used when referencing an element in the collection
            'vntIndexKey contains either the Index or Key to the collection,
            'this is why it is declared as a Variant
            'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property



    Public ReadOnly Property Count() As Integer
        Get
            'used when retrieving the number of elements in the
            'collection. Syntax: Debug.Print x.Count
            Count = mCol.Count()
        End Get
    End Property


    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        GetEnumerator = mCol.GetEnumerator
    End Function
    'Public Property Let Profile(oObj As Variant)
    '    If Not IsObject(oObj) Then
    '        RaiseInvalidObject
    '    End If
    '
    '    'Store copy of object
    '    'FIX: Skulle kanskje ha hatt en Set property for �
    '    'lagre referansen til objektet ??????
    '    'Dersom profilen endres underveis, kan det v�re greit
    '    '� ha en referanse, slik at profilen vil v�re oppdatert
    '    oProfile = oObj
    'End Property

    Public Property VB_Profile() As Object
        Get
            VB_Profile = oProfile
        End Get
        Set(ByVal Value As Object)

            'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oProfile = Value

        End Set
    End Property

    Public ReadOnly Property MATCH_ErrorText() As Object
        Get
            'UPGRADE_WARNING: Couldn't resolve default property of object MATCH_ErrorText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MATCH_ErrorText = CObj(sMATCH_ErrorText)
        End Get
    End Property
    Public Property MATCH_MatchOCR() As Boolean
        Get
            MATCH_MatchOCR = bMATCH_MatchOCR
        End Get
        Set(ByVal Value As Boolean)
            bMATCH_MatchOCR = Value
        End Set
    End Property
    Public ReadOnly Property MATCH_NoOfOpenItems() As Integer
        Get
            Dim oMyDal As vbBabel.DAL = Nothing
            Dim sMySQL As String = ""
            Dim bx As Boolean

            Try

                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                sMySQL = "SELECT Count(*) AS Occurance FROM Payment WHERE MATCH_Matched < 3 "
                sMySQL = sMySQL & "AND StatusCode <> '-1' AND "
                '02.11.2009 - Kjell - next line is new
                sMySQL = sMySQL & "DoNotShow = False "
                sMySQL = sMySQL & "AND Company_ID = " & oProfile.Company_ID.ToString & " AND Babelfile_ID < 11000000"
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then
                    Do While oMyDal.Reader_ReadRecord
                        lMATCH_NoOfOpenItems = oMyDal.Reader_GetString("Occurance")
                    Loop
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

            Catch ex As Exception

                If Not oMyDal Is Nothing Then
                    oMyDal.Close()
                    oMyDal = Nothing
                End If

                Throw New Exception("Function: MATCH_NoOfOpenItems" & vbCrLf & ex.Message)

            End Try

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            MATCH_NoOfOpenItems = CObj(lMATCH_NoOfOpenItems)

        End Get
    End Property

    Public ReadOnly Property MATCH_NoAmongItems(ByVal bOpenItemsOnly As Boolean) As Integer
        Get
            Dim oMyDal As vbBabel.DAL = Nothing
            Dim sMySQL As String = ""
            Dim lCounter As Integer
            Dim lReturnValue As Integer
            Dim bx As Boolean
            'This function demands that only 1 payment exists in the collection

            lReturnValue = 0

            Try
                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                sMySQL = "SELECT Babelfile_ID, Batch_ID, Payment_ID, PaymentCounter FROM Payment WHERE "
                If bOpenItemsOnly Then
                    sMySQL = sMySQL & "MATCH_Matched < 3 AND "
                Else
                    'No constraints regarding the MATCH_Matched status.
                End If
                sMySQL = sMySQL & "StatusCode <> '-1' AND "
                '02.11.2009 - Kjell - next line is new
                sMySQL = sMySQL & "DoNotShow = False AND "
                sMySQL = sMySQL & "Company_ID = " & Trim(Str(oProfile.Company_ID)) & " AND Babelfile_ID < 11000000"
                '13.07.2015 - Changed the way to move in manual matching
                sMySQL = sMySQL & " ORDER BY PaymentCounter ASC"
                'sMySQL = sMySQL & " ORDER BY BabelFile_ID ASC, Unique_PaymentID ASC"
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then
                    Do While oMyDal.Reader_ReadRecord
                        lCounter = lCounter + 1
                        If mCol.Item(1).VB_DBBabelFileIndex = CDbl(oMyDal.Reader_GetString("Babelfile_ID")) Then
                            If mCol.Item(1).Batches.Item(1).Index = CDbl(oMyDal.Reader_GetString("Batch_ID")) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().Batches. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If mCol.Item(1).Batches.Item(1).Payments.Item(1).Index = CDbl(oMyDal.Reader_GetString("Payment_ID")) Then
                                    lReturnValue = lCounter
                                    Exit Do
                                End If
                            End If
                        End If
                    Loop
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

            Catch ex As Exception

                If Not oMyDal Is Nothing Then
                    oMyDal.Close()
                    oMyDal = Nothing
                End If

                Throw New Exception("Function: MATCH_NoAmongItems" & vbCrLf & ex.Message)

            End Try

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            MATCH_NoAmongItems = lReturnValue

        End Get
    End Property

    Public Property FilterType() As String
        Get
            FilterType = sFilter
        End Get
        Set(ByVal Value As String)
            sFilter = Value
        End Set
    End Property
    Public Property ClientGroupFilter() As String
        Get
            ClientGroupFilter = sClientGroupFilter
        End Get
        Set(ByVal Value As String)
            sClientGroupFilter = Value
            If sClientGroupFilter <> String.Empty Then
                If vbIsNumeric(sClientGroupFilter, "0123456789") Then
                    bClientGroupFilterIsAccount = True
                Else
                    bClientGroupFilterIsAccount = False
                End If
            Else
                bClientGroupFilterIsAccount = False
            End If

        End Set
    End Property
    Public Property MATCH_LastValidBookDate() As String
        Get
            MATCH_LastValidBookDate = sMATCH_LastValidBookDate 'YYYYMMDD
        End Get
        Set(ByVal Value As String)
            sMATCH_LastValidBookDate = CStr(Value) 'YYYYMMDD
        End Set
    End Property
    Public Property UserIDInPayment() As String
        Get
            UserIDInPayment = sUserIDInPayment
        End Get
        Set(ByVal Value As String)
            Dim sMySQL As String = ""
            Dim sErrorString As String
            Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
            Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
            Dim iRecordsAffected As Integer

            sUserIDInPayment = Value
            'Save the UserID in the DB

            If myBBDB_AccessConnection Is Nothing Then
                ConnectToBBDatabse()
            Else
                If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                    ConnectToBBDatabse()
                End If
            End If

            If mCol.Count() = 1 Then
                If mCol.Item(1).Batches.Count = 1 Then
                    If mCol.Item(1).Batches.Item(1).Payments.Count = 1 Then
                        'OK we have only 1 payment store the UserID

                        sMySQL = "UPDATE Payment SET UserID = '" & Trim(sUserIDInPayment)
                        '13.07.2015 - Changed the way to move in manual matching
                        sMySQL = sMySQL & "' WHERE PaymentCounter = "
                        sMySQL = sMySQL & Trim(Str(mCol.Item(1).Batches.Item(1).Payments.Item(1).PaymentCounter)) '& " AND BabelFile_ID = " & Trim(Str(mCol.Item(1).VB_DBBabelFileIndex))
                        'sMySQL = sMySQL & "' WHERE Unique_PaymentID = "
                        'sMySQL = sMySQL & Trim(Str(mCol.Item(1).Batches.Item(1).Payments.Item(1).Unique_PaymentID)) & " AND BabelFile_ID = " & Trim(Str(mCol.Item(1).VB_DBBabelFileIndex))

                        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                        myBBDB_AccessCommand.CommandType = CommandType.Text

                        myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

                        myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

                        myBBDB_AccessCommand.CommandText = sMySQL

                        iRecordsAffected = myBBDB_AccessCommand.ExecuteNonQuery()

                        If iRecordsAffected <> 1 Then
                            Err.Raise(4823, "Set UserIDInPayment", "An error occured during saving of last used babelFile_ID." & vbCrLf & vbCrLf & sErrorString)
                        End If

                        myBBDB_AccessTrans.Commit()

                    End If
                End If
            End If

            If Not myBBDB_AccessCommand Is Nothing Then
                myBBDB_AccessCommand.Dispose()
                myBBDB_AccessCommand = Nothing
            End If
            If Not myBBDB_AccessTrans Is Nothing Then
                myBBDB_AccessTrans.Dispose()
                myBBDB_AccessTrans = Nothing
            End If

        End Set
    End Property
    Public Property MyUserID() As String
        Get
            MyUserID = sMyUserID
        End Get
        Set(ByVal Value As String)
            sMyUserID = Value
        End Set
    End Property
    Public Property SpecialID() As String
        Get
            SpecialID = sSpecialID
        End Get
        Set(ByVal Value As String)
            sSpecialID = Value
        End Set
    End Property
    Public Sub Remove(ByRef vntIndexKey As Integer)
        'used when removing an element from the collection
        'vntIndexKey contains either the Index or Key, which is why
        'it is declared as a Variant
        'Syntax: x.Remove(xyz)


        mCol.Remove(vntIndexKey)
    End Sub


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        'creates the collection when this class is created
        mCol = New Collection
        'UPGRADE_NOTE: Object oProfile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oProfile = Nothing
        ' When vbbabel.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If

        lMATCH_NoOfOpenItems = -1
        lMATCH_NoOfOpenItemsPriorToActivePayment = -1

        sFilter = "ANONYMOUS"
        sClientGroupFilter = ""
        bClientGroupFilterIsAccount = False
        sUserIDInPayment = ""
        sMyUserID = ""
        sMATCH_LastValidBookDate = ""
        sSpecialID = ""
        bUseCommitAgainstBBDB = True

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Private Sub Class_Terminate_Renamed()
        Dim i As Short
        'destroys collection when this class is terminated

        'reset the static variables use in CheckAccountNo
        i = CheckAccountNo("@XYZ", "", oProfile)

        mCol = Nothing
        oProfile = Nothing

    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
    Public Sub CloseConnection()

        If Not myBBDB_AccessConnection Is Nothing Then
            If myBBDB_AccessConnection.State <> ConnectionState.Closed Then
                'Debug.Print("LUKKER DATABASE I BABELFILES!")
                myBBDB_AccessConnection.Close()
            End If
            myBBDB_AccessConnection.Dispose()
            myBBDB_AccessConnection = Nothing
        End If

    End Sub
    Public Function SetProfileInUndelyingCollections() As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice

        For Each oBabelFile In mCol
            oBabelFile.VB_Profile = oProfile
            For Each oBatch In oBabelFile.Batches
                oBatch.VB_Profile = oProfile
                For Each oPayment In oBatch.Payments
                    oPayment.VB_Profile = oProfile
                    For Each oInvoice In oPayment.Invoices
                        oInvoice.VB_Profile = oProfile
                    Next oInvoice
                Next oPayment
            Next oBatch
        Next oBabelFile

    End Function
    Public Function SetCargoInUndelyingCollections() As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice

        For Each oBabelFile In mCol
            If Not oBabelFile.Cargo Is Nothing Then
                For Each oBatch In oBabelFile.Batches
                    oBatch.Cargo = oBabelFile.Cargo
                    For Each oPayment In oBatch.Payments
                        oPayment.Cargo = oBatch.Cargo
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.Cargo = oPayment.Cargo
                        Next oInvoice
                    Next oPayment
                Next oBatch
            End If
        Next oBabelFile

    End Function
    Private Sub ConnectToBBDatabse()
        Dim sConnectionString As String = ""
        '18.07.2016 - Added the use of Connectionstring to be able to log on to a SQLServer dB

        sConnectionString = FindBBConnectionString()
        'Create the connection
        myBBDB_AccessConnection = New System.Data.OleDb.OleDbConnection(sConnectionString)
        'myBBDB_AccessConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath())
        'Debug.Print("�PNER DATABASE I BABELFILES!")
        myBBDB_AccessConnection.Open()
        bSQLServer = BB_UseSQLServer()

    End Sub
    Public Function MATCH_EmptyCollections() As Object
        'Remove all Babelfile objects
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing
        mCol = New Collection
    End Function

    Public Function MATCH_SaveData(ByRef bMatchedItemsOnly As Boolean, ByRef bSaveOnlyThisPayment As Boolean, ByRef bAddToExistingPayments As Boolean, Optional ByRef bSaveOnlyNameAndAddress As Boolean = False, Optional ByRef sSpecial As String = "", Optional ByRef bUseProfile As Boolean = False, Optional ByVal bOnlyReporting As Boolean = False) As Boolean
        Dim lCounter As Integer
        Dim bReturnValue As Boolean
        Dim oBabelFile As BabelFile
        Dim oBatch As vbBabel.Batch
        Dim bSaveThisBabelfile As Boolean
        Dim sMySQL As String
        Dim lLastBabelFileID As Integer
        'Dim rsBabelFile As New ADODB.Recordset
        Dim bFileExportedMark As Boolean
        Dim sErrorString As String
        Dim bx As Boolean

        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim iRecordsAffected As Integer

        On Error GoTo ERRSaveData

        If Format(Now(), "yyyyMMdd") = "20191030" Then
            Dim s As String
            If bMatchedItemsOnly Then
                s = s & "bMatchedItemsOnly = True" & vbCrLf
            Else
                s = s & "bMatchedItemsOnly = Flase" & vbCrLf
            End If
            If bSaveOnlyThisPayment Then
                s = s & "bSaveOnlyThisPayment = True" & vbCrLf
            Else
                s = s & "bSaveOnlyThisPayment = Flase" & vbCrLf
            End If
            If bAddToExistingPayments Then
                s = s & "bAddToExistingPayments = True" & vbCrLf
            Else
                s = s & "bAddToExistingPayments = Flase" & vbCrLf
            End If

            MsgBox("Kommet til rutinen MATCH_SaveData" & vbCrLf & s & System.Environment.StackTrace)
        End If

        'Added next IF, 07.09.2010
        If bUseProfile And oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If
        'If cnProfile Is Nothing Then
        '    ConnectToProfile()
        'Else
        '    If cnProfile.State = ADODB.ObjectStateEnum.adStateClosed Then
        '        ConnectToProfile()
        '    End If
        'End If

        'BABELREINDEX
        If bAddToExistingPayments Then
            sMySQL = "SELECT BabelFile_ID FROM BabelFile Where Company_ID = " & Trim(Str(oProfile.Company_ID))
            sMySQL = sMySQL & " ORDER BY BabelFile_ID DESC"

            'rsBabelFile.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

            'If rsBabelFile.RecordCount > 0 Then
            '    rsBabelFile.MoveFirst()
            '    lLastBabelFileID = Val(rsBabelFile.Fields("BabelFile_ID").Value)
            'Else
            '    lLastBabelFileID = 0
            'End If

            'rsBabelFile.Close()

            myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessCommand.CommandType = CommandType.Text
            myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
            myBBDB_AccessCommand.CommandText = sMySQL

            myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

            If myBBDB_AccessReader.HasRows Then
                myBBDB_AccessReader.Read()
                lLastBabelFileID = myBBDB_AccessReader.GetInt32(0)
            Else
                lLastBabelFileID = 0
            End If

            myBBDB_AccessReader.Close()

            lCounter = lLastBabelFileID
            bFileExportedMark = False
        Else
            lCounter = 0
            bFileExportedMark = True
        End If

        'If Not RunTime Then
        '    'New 14.09.2006 To retrieve the last used BabelFile_ID from Company
        '    sMySQL = "SELECT LastUsedBabelFileID FROM Company Where Company_ID = " & Trim$(Str(oProfile.Company_ID))
        '
        '    rsBabelFile.Open sMySQL, cnProfile, adOpenStatic, adLockOptimistic
        '
        '    If rsBabelFile.RecordCount = 1 Then
        '        rsBabelFile.MoveFirst
        '        lLastBabelFileID = Val(rsBabelFile!LastUsedBabelFileID)
        '    Else
        '        lLastBabelFileID = 0
        '    End If
        '    rsBabelFile.Close
        '    Set rsBabelFile = Nothing
        '    lCounter = lLastBabelFileID
        'End If

        'Added 25.09.2006
        bReturnValue = True

        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommand.CommandType = CommandType.Text
        myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

        myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

        For Each oBabelFile In mCol
            'New 26.09.2008 - Don't save the babelfile if it has no batches
            If oBabelFile.Batches.Count > 0 Then
                If bMATCH_MatchOCR Then 'If we shall match the OCR, save it all.
                    bSaveThisBabelfile = True
                Else
                    bSaveThisBabelfile = False
                    For Each oBatch In oBabelFile.Batches
                        '19.07.2010
                        If oBatch.Payments.Count > 0 Then
                            ' New 01.02.05 Added 180 = UDUS to save
                            'New 27.08.2008 - changed to test against IsOCR and IsAutogiro

                            If IsOCR(oBatch.Payments.Item(1).PayCode) Or oBatch.Payments.Item(1).PayCode = "180" Or IsAutogiro(oBatch.Payments.Item(1).PayCode) Then 'OCR or Autogiro
                                'Old
                                'If oBatch.Payments.Item(1).PayCode = "510" Or oBatch.Payments.Item(1).PayCode = "180" Or oBatch.Payments.Item(1).PayCode = "611" Or oBatch.Payments.Item(1).PayCode = "612" Then 'OCR or Autogiro
                                'Assume all payments are eithe OCR or not
                                bSaveThisBabelfile = False
                            Else
                                bSaveThisBabelfile = True 'Not an OCR, save the babelfile
                                'If we have at least 1 batch without KID-transactions, save the
                                '  BabelFile. Which Batch to save is set in BabelFile.SaveData
                                Exit For
                            End If
                        Else
                            bSaveThisBabelfile = False
                        End If
                    Next oBatch
                End If
            Else
                bSaveThisBabelfile = False
            End If
            If Not oBatch Is Nothing Then
                oBatch = Nothing
            End If
            If oBabelFile.Index > lLastBabelFileID Then
                lLastBabelFileID = oBabelFile.Index
            End If
            If bSaveThisBabelfile Then
                lCounter = lCounter + 1 'Should this be outside the if?
                If sSpecial = "NFR" Then
                    bReturnValue = oBabelFile.SaveData(bSQLServer, myBBDB_AccessConnection, myBBDB_AccessCommand, bMatchedItemsOnly, lCounter, bSaveOnlyThisPayment, bMATCH_MatchOCR, bFileExportedMark, bSaveOnlyNameAndAddress, bUseCommitAgainstBBDB, bOnlyReporting)
                    ' changed 22.03.2010
                    'ElseIf Not RunTime Or sSpecial = "SISMO" Then
                ElseIf sSpecial = "SISMO" Or sSpecial = "ODIN" Then
                    'When archive is used, then the indexes are set correctly in Import/Export. Use it!
                    bReturnValue = oBabelFile.SaveData(bSQLServer, myBBDB_AccessConnection, myBBDB_AccessCommand, bMatchedItemsOnly, (oBabelFile.Index), bSaveOnlyThisPayment, bMATCH_MatchOCR, bFileExportedMark, bSaveOnlyNameAndAddress, bUseCommitAgainstBBDB, bOnlyReporting)
                ElseIf bOnlyReporting Then
                    bReturnValue = oBabelFile.SaveData(bSQLServer, myBBDB_AccessConnection, myBBDB_AccessCommand, bMatchedItemsOnly, lCounter + 11000000, bSaveOnlyThisPayment, bMATCH_MatchOCR, bFileExportedMark, bSaveOnlyNameAndAddress, bUseCommitAgainstBBDB, bOnlyReporting)
                Else
                    bReturnValue = oBabelFile.SaveData(bSQLServer, myBBDB_AccessConnection, myBBDB_AccessCommand, bMatchedItemsOnly, lCounter, bSaveOnlyThisPayment, bMATCH_MatchOCR, bFileExportedMark, bSaveOnlyNameAndAddress, bUseCommitAgainstBBDB, bOnlyReporting)
                End If
            End If
            ' 07.01.2019 - for ActiveBrands, we can have empty batches - this causes error
            If oBabelFile.Special = "ACTIVEBRANDS_KLARNA" Then
                If bReturnValue = False Then
                    If oBabelFile.Batches(1).Payments.Count = 0 Then
                        bReturnValue = True
                    End If
                End If
            End If
        Next oBabelFile

      
        myBBDB_AccessTrans.Commit()

        '25.01.2017 -Changed this code, now retrieving next BabelFile_ID from Company.LastUsedBabelFileID 
        'This is now done straight after renumerating the index of the imported BabelFile(s)

        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If
      

        MATCH_SaveData = bReturnValue

        Exit Function

ERRSaveData:
        '08.07.2015 - Commented next three line.
        'If we set mCol to Nothing, then BB will fail when we continue in manual matching
        'If Not mCol Is Nothing Then
        'mCol = Nothing
        'End If
        sMATCH_ErrorText = LRS(14001, LRS(14009)) & vbCrLf & vbCrLf '"An error occured during saving data." & vbCrLf & vbCrLf
        If Err.Number = 999 Then
            'Err is raised in BabelFile, Batch, Payment, Invoice or Freetext
            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & " " & Err.Source & vbCrLf '14002 = "Source: "
        Else
            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & " " & "BabelFiles" & vbCrLf '14002 = "Source: "
        End If

        sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & " " & Err.Description '14003 = "Description: "

        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        MATCH_SaveData = False

    End Function
    Public Function MATCH_RetrieveData(ByRef bOpenItemsOnly As Boolean, ByRef iCompanyNo As Short, ByRef bMatchedItemsOnly As Boolean, ByRef bJustNonExportedPayments As Boolean, Optional ByRef bUseProfile As Boolean = False, Optional ByRef iBabelFile_ID As Integer = 0, Optional ByRef iBatch_ID As Integer = 0, Optional ByRef iPayment_ID As Integer = 0) As Boolean
        Dim lCounter As Integer
        Dim bx As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim sSelectPart As String
        Dim sMySQL As String
        Dim bReturnValue As Boolean
        Dim sOldAccountNo As String
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oClient As vbBabel.Client
        Dim myBBDB_AccessBabelFileReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessBabelFileCommand As System.Data.OleDb.OleDbCommand = Nothing

        'On Error GoTo errorRetrieveData
        Try

            If myBBDB_AccessConnection Is Nothing Then
                ConnectToBBDatabse()
            Else
                If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                    ConnectToBBDatabse()
                End If
            End If

            mCol = Nothing
            mCol = New Collection

            sSelectPart = "SELECT BabelFile_ID, FilenameIn, File_id, IDENT_Sender, IDENT_Receiver, MON_TransferredAmount, MON_InvoiceAmount, "
            sSelectPart = sSelectPart & "AmountSetTransferred, AmountSetInvoice, No_Of_Transactions, No_Of_Records, DATE_Production, ImportFormat, "
            sSelectPart = sSelectPart & "RejectsExists, FilenameInNo, FileFromBank, Version, EDI_Format, EDI_MessageNo, FileSetup_ID, XLanguage, "
            sSelectPart = sSelectPart & "ERPSystem, Bank, StatusCode, Special"

            sMySQL = GetSelectPartInBabelfile() & " FROM Babelfile WHERE Company_ID = " & iCompanyNo.ToString
            'If one specific Babelfile_ID is stated, retrieve just that one.
            If iBabelFile_ID > 0 Then
                sMySQL = sMySQL & " AND Babelfile_ID = " & iBabelFile_ID.ToString
            End If
            If bJustNonExportedPayments Then
                sMySQL = sMySQL & " AND Exported = False"
            End If
            sMySQL = sMySQL & " AND Babelfile_ID < 11000000"

            If bSQLServer Then
                sMySQL = sMySQL.Replace("False", "0")
                sMySQL = sMySQL.Replace("false", "0")
                sMySQL = sMySQL.Replace("True", "1")
                sMySQL = sMySQL.Replace("true", "1")
            End If

            myBBDB_AccessBabelFileCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessBabelFileCommand.CommandType = CommandType.Text
            'Default er CommandTimeout = 30
            myBBDB_AccessBabelFileCommand.CommandTimeout = 120
            myBBDB_AccessBabelFileCommand.Connection = myBBDB_AccessConnection
            myBBDB_AccessBabelFileCommand.CommandText = sMySQL

            myBBDB_AccessBabelFileReader = myBBDB_AccessBabelFileCommand.ExecuteReader

            'Added 25.08.2006
            bReturnValue = True

            'Create the collection
            If myBBDB_AccessBabelFileReader.HasRows Then
                Do While myBBDB_AccessBabelFileReader.Read()
                    oBabelFile = Add()
                    If bUseProfile Then
                        oBabelFile.VB_Profile = oProfile
                    End If
                    If oBabelFile.RetrieveData(bSQLServer, myBBDB_AccessBabelFileReader, myBBDB_AccessConnection, bOpenItemsOnly, bMatchedItemsOnly, iCompanyNo, iBabelFile_ID, iBatch_ID, iPayment_ID, bUseProfile) Then
                        bReturnValue = True
                    Else
                        bReturnValue = False
                        sMATCH_ErrorText = LRS(14004) '"An unknown error occured during the retrieving data."
                        'Exit For
                    End If
                Loop
                'Removed the else clause 25.09.2006 - If there are no payments there are no payments, don't raise an error.
                'Else
                '    sMATCH_ErrorText = LRS(14013) '"No payments to retrieve." & vbcrlf & vbcrlf "No payments among the stored payments match the criterias."
            End If

            'Add oClient to each Payment if not one specific payment is to be retrieved
            If iBabelFile_ID = 0 Then
                'XokNET - 16.11.2011 - Added next IF
                'NBNBNBNBNBNBNBNBNBNB!!!!!!!!!!!!!!!
                'XNET - 23.03.2012 - Removed next IF again. Why was it added?
                'It creates problems for Odin when a new matching rule is added which is based on the indexes.
                'It may also creates other problems for Odin and other companies which needs the correct index (f.ex. companies using the archive DB)
                'It seems reasonable that we run this code in other cases.
                'Especially removing empty collections makes sense, maybe that was the idea (see comment from when the change was made 'Removes empty batches and babelfiles')
                'The function however also reindexes the collections.

                If bOpenItemsOnly Or bMatchedItemsOnly Then
                    'XNET - 28.03.2012 - Added a parameter just to remove empty collections, not reindexing
                    MATCH_ReindexCollection(False) 'Removes empty batches and babelfiles for Bring, crashes when trying to set the sVoucherNo
                End If

                'END - NBNBNBNBNBNBNBNBNBNB!!!!!!!!!!!!!!!

                For Each oBabelFile In mCol
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If sOldAccountNo <> Trim(oPayment.I_Account) Then
                                sOldAccountNo = Trim(oPayment.I_Account)
                                oClient = FindExistingClient(oProfile, (oBabelFile.VB_FileSetupID), sOldAccountNo)
                            End If
                            If Not oClient Is Nothing Then
                                oPayment.VB_Client = oClient
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
            Else
                'Retrieve useID from Payment, and store it in UserID
                If mCol.Count() > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().Batches. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sUserIDInPayment = mCol.Item(1).Batches.Item(1).Payments.Item(1).UserID
                End If
            End If

            'If Not myBBDB_AccessBabelFileReader Is Nothing Then
            '    If Not myBBDB_AccessBabelFileReader.IsClosed Then
            '        myBBDB_AccessBabelFileReader.Close()
            '    End If
            '    myBBDB_AccessBabelFileReader = Nothing
            'End If
            'If Not myBBDB_AccessBabelFileCommand Is Nothing Then
            '    myBBDB_AccessBabelFileCommand.Dispose()
            '    myBBDB_AccessBabelFileCommand = Nothing
            'End If

            'MATCH_RetrieveData = bReturnValue

        Catch ex As Exception
            Throw New Exception(LRS(14001, LRS(14010)) & vbCrLf & vbCrLf & LRS(14003) & vbCrLf & ex.Message & vbCrLf & ex.InnerException.ToString, ex.InnerException)


        Finally
            If Not myBBDB_AccessBabelFileReader Is Nothing Then
                If Not myBBDB_AccessBabelFileReader.IsClosed Then
                    myBBDB_AccessBabelFileReader.Close()
                End If
                myBBDB_AccessBabelFileReader = Nothing
            End If
            If Not myBBDB_AccessBabelFileCommand Is Nothing Then
                myBBDB_AccessBabelFileCommand.Dispose()
                myBBDB_AccessBabelFileCommand = Nothing
            End If

        End Try

        '            Exit Function

        'errorRetrieveData:
        '            If Not myBBDB_AccessBabelFileReader Is Nothing Then
        '                If Not myBBDB_AccessBabelFileReader.IsClosed Then
        '                    myBBDB_AccessBabelFileReader.Close()
        '                End If
        '                myBBDB_AccessBabelFileReader = Nothing
        '            End If
        '            If Not myBBDB_AccessBabelFileCommand Is Nothing Then
        '                myBBDB_AccessBabelFileCommand.Dispose()
        '                myBBDB_AccessBabelFileCommand = Nothing
        '            End If

        '            sMATCH_ErrorText = LRS(14001, LRS(14010)) & vbCrLf & vbCrLf '"An error occured during retrieving data."
        '            If Err.Number = 999 Then
        '                'Err is raised in BabelFile, Batch, Payment, Invoice or Freetext
        '                sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & Err.Source & vbCrLf '14002 = "Source: "
        '            Else
        '                sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & "BabelFiles" & vbCrLf '14002 = "Source: "
        '            End If

        '            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & Err.Description '14003 = "Description: "

        Return bReturnValue

    End Function
    Public Function MATCH_RetrieveOmittedData(ByRef iCompanyNo As Short) As Boolean
        Dim myBBDB_AccessReaderPayment As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommandPayment As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessReaderBabelFile As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommandBabelFile As System.Data.OleDb.OleDbCommand = Nothing
        Dim lCounter As Integer
        Dim bx As Boolean
        Dim oBabelFile As BabelFile
        Dim sMySQL As String
        Dim bReturnValue As Boolean
        Dim sOldAccountNo As String
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oClient As vbBabel.Client
        Dim lOldBabelFile_ID As Integer
        Dim lOldBatch_ID As Integer

        On Error GoTo errorRetrieveData

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        mCol = Nothing
        mCol = New Collection

        sMySQL = "Select BabelFile_ID, Batch_ID, Payment_ID from Payment WHERE Company_ID = " & Trim(Str(iCompanyNo)) & " AND StatusCode = '-1'" & " AND Babelfile_ID < 11000000"
        myBBDB_AccessCommandPayment = myBBDB_AccessConnection.CreateCommand()
        myBBDB_AccessCommandPayment.CommandType = CommandType.Text
        myBBDB_AccessCommandPayment.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommandPayment.CommandText = sMySQL

        myBBDB_AccessReaderPayment = myBBDB_AccessCommandPayment.ExecuteReader

        lOldBabelFile_ID = -1
        lOldBatch_ID = -1
        'Create the collection
        If myBBDB_AccessReaderPayment.HasRows Then
            Do While myBBDB_AccessReaderPayment.Read
                If lOldBabelFile_ID <> myBBDB_AccessReaderPayment.GetInt32(0) Then
                    If Not myBBDB_AccessReaderBabelFile.IsClosed Then
                        myBBDB_AccessReaderBabelFile.Close()
                    End If
                    oBabelFile = Add(myBBDB_AccessReaderPayment.GetInt32(0).ToString)
                    lOldBabelFile_ID = myBBDB_AccessReaderPayment.GetInt32(0)
                    lOldBatch_ID = -1
                    oBabelFile.VB_Profile = oProfile
                    sMySQL = GetSelectPartInBabelfile() & " from Babelfile WHERE Company_ID = " & iCompanyNo.ToString & " AND BabelFile_ID = " & myBBDB_AccessReaderPayment.GetInt32(0).ToString
                    myBBDB_AccessCommandBabelFile = myBBDB_AccessConnection.CreateCommand()
                    myBBDB_AccessCommandBabelFile.CommandType = CommandType.Text
                    myBBDB_AccessCommandBabelFile.Connection = myBBDB_AccessConnection
                    myBBDB_AccessCommandBabelFile.CommandText = sMySQL

                    myBBDB_AccessReaderBabelFile = myBBDB_AccessCommandBabelFile.ExecuteReader
                End If
                If lOldBatch_ID <> myBBDB_AccessReaderPayment.GetInt32(1) Then
                    lOldBatch_ID = myBBDB_AccessReaderPayment.GetInt32(1)
                    If oBabelFile.RetrieveData(bSQLServer, myBBDB_AccessReaderBabelFile, myBBDB_AccessConnection, False, False, iCompanyNo, myBBDB_AccessReaderPayment.GetInt32(0), myBBDB_AccessReaderPayment.GetInt32(1), 0, True, True) Then
                        bReturnValue = True
                    Else
                        bReturnValue = False
                        'sMATCH_ErrorText = LRS(14004) '"An unknown error occured during the retrieving data."
                        'Exit For
                    End If
                End If
            Loop
        Else
            sMATCH_ErrorText = LRS(14013) '"No payments to retrieve." & vbcrlf & vbcrlf "No payments among the stored payments match the criterias."
        End If

        For Each oBabelFile In mCol
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If sOldAccountNo <> Trim(oPayment.I_Account) Then
                        sOldAccountNo = Trim(oPayment.I_Account)
                        oClient = FindExistingClient(oProfile, (oBabelFile.VB_FileSetupID), sOldAccountNo)
                    End If
                    If Not oClient Is Nothing Then
                        oPayment.VB_Client = oClient
                    End If
                Next oPayment
            Next oBatch
        Next oBabelFile

        If Not myBBDB_AccessReaderPayment Is Nothing Then
            If Not myBBDB_AccessReaderPayment.IsClosed Then
                myBBDB_AccessReaderPayment.Close()
            End If
            myBBDB_AccessReaderPayment = Nothing
        End If
        If Not myBBDB_AccessCommandPayment Is Nothing Then
            myBBDB_AccessCommandPayment.Dispose()
            myBBDB_AccessCommandPayment = Nothing
        End If
        If Not myBBDB_AccessReaderBabelFile Is Nothing Then
            If Not myBBDB_AccessReaderBabelFile.IsClosed Then
                myBBDB_AccessReaderBabelFile.Close()
            End If
            myBBDB_AccessReaderBabelFile = Nothing
        End If
        If Not myBBDB_AccessCommandBabelFile Is Nothing Then
            myBBDB_AccessCommandBabelFile.Dispose()
            myBBDB_AccessCommandBabelFile = Nothing
        End If

        MATCH_RetrieveOmittedData = bReturnValue

        Exit Function

errorRetrieveData:
        If Not myBBDB_AccessReaderPayment Is Nothing Then
            If Not myBBDB_AccessReaderPayment.IsClosed Then
                myBBDB_AccessReaderPayment.Close()
            End If
            myBBDB_AccessReaderPayment = Nothing
        End If
        If Not myBBDB_AccessCommandPayment Is Nothing Then
            myBBDB_AccessCommandPayment.Dispose()
            myBBDB_AccessCommandPayment = Nothing
        End If
        If Not myBBDB_AccessReaderBabelFile Is Nothing Then
            If Not myBBDB_AccessReaderBabelFile.IsClosed Then
                myBBDB_AccessReaderBabelFile.Close()
            End If
            myBBDB_AccessReaderBabelFile = Nothing
        End If
        If Not myBBDB_AccessCommandBabelFile Is Nothing Then
            myBBDB_AccessCommandBabelFile.Dispose()
            myBBDB_AccessCommandBabelFile = Nothing
        End If

        sMATCH_ErrorText = LRS(14001, LRS(14010)) & vbCrLf & vbCrLf '"An error occured during retrieving data."
        If Err.Number = 999 Then
            'Err is raised in BabelFile, Batch, Payment, Invoice or Freetext
            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & Err.Source & vbCrLf '14002 = "Source: "
        Else
            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & "BabelFiles" & vbCrLf '14002 = "Source: "
        End If

        sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & Err.Description '14003 = "Description: "

    End Function
    Public Function MATCH_DeleteOmittedData(ByRef iCompanyNo As Short) As Boolean

    End Function

    Public Function FI_RetrieveData(ByRef iCompanyNo As Short) As Boolean

        'TODO: THIS FUNCTION IS NOT CONVERTED IN VB.NET

        MsgBox("The function FI_RetrieveData is not yet converted to vb.net" & vbCrLf & vbCrLf & "Please contact Visual Banking AS", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Function not available")




        '        ' Retrieve only those data needed in the DnBTBI FI_Project
        '        Dim lCounter As Integer
        '        Dim oBabelFile As BabelFile
        '        Dim sMySQL As String
        '        Dim rsBabelFile As New ADODB.Recordset
        '        Dim bReturnValue As Boolean

        '        On Error GoTo errorRetrieveData

        '        If cnProfile.State = ADODB.ObjectStateEnum.adStateClosed Then
        '            ConnectToProfile()
        '        End If

        '        oProfile = New vbBabel.Profile
        '        oProfile.Load(1)

        '        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        mCol = Nothing
        '        mCol = New Collection

        '        sMySQL = "Select * from Babelfile WHERE Company_ID = " & Trim(Str(iCompanyNo)) & " AND Babelfile_ID < 11000000"
        '        rsBabelFile.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        '        Dim ntimer As Integer

        '        'Create the collection
        '        If rsBabelFile.RecordCount > 0 Then
        '            '    ntimer = Timer
        '            For lCounter = 1 To rsBabelFile.RecordCount
        '                oBabelFile = Add(Trim(Str(rsBabelFile.Fields("BabelFile_ID").Value)))
        '                oBabelFile.VB_Profile = oProfile
        '                If oBabelFile.FI_RetrieveData(cnProfile, rsBabelFile, iCompanyNo) Then
        '                    bReturnValue = True
        '                Else
        '                    bReturnValue = False
        '                    'sMatch_errortext = LRS(14004) '"An unknown error occured during the retrieving data."
        '                    'Exit For
        '                End If
        '                rsBabelFile.MoveNext()
        '            Next lCounter
        '        Else
        '            sMATCH_ErrorText = LRS(14013) '"No payments to retrieve." & vbcrlf & vbcrlf "No payments among the stored payments match the criterias."
        '        End If
        '        ntimer = VB.Timer() - ntimer

        '        'UPGRADE_NOTE: Object rsBabelFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        rsBabelFile = Nothing

        '        FI_RetrieveData = bReturnValue

        '        Exit Function

        'errorRetrieveData:
        '        If Not rsBabelFile Is Nothing Then
        '            'UPGRADE_NOTE: Object rsBabelFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '            rsBabelFile = Nothing
        '        End If
        '        sMATCH_ErrorText = LRS(14001, LRS(14010)) & vbCrLf & vbCrLf '"An error occured during retrieving data."
        '        If Err.Number = 999 Then
        '            'Err is raised in BabelFile, Batch, Payment, Invoice or Freetext
        '            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & Err.Source & vbCrLf '14002 = "Source: "
        '        Else
        '            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & "BabelFiles" & vbCrLf '14002 = "Source: "
        '        End If

        '        sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & Err.Description '14003 = "Description: "

    End Function
    Public Function FI_FastRetrieve(ByRef iCompanyNo As Short, ByRef sAccountNo As String, ByRef lFromAmount As Integer, ByRef lToAmount As Integer, ByRef sFromDate As String, ByRef sToDate As String, ByRef sFromPayDate As String, ByRef sToPayDate As String, ByRef sPayCode As String) As Boolean
        'THIS FUNCTION IS NOT CONVERTED TO ADO.NET

        MsgBox("The function FI_FastRetrieve is not yet converted to vb.net" & vbCrLf & vbCrLf & "Please contact Visual Banking AS", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Function not available")

        '        ' Retrieve only those data needed in the DnBTBI FI_Project
        '        Dim lCounter, lFreeCounter As Integer
        '        Dim oBabelFile As BabelFile
        '        Dim oBatches As Batches
        '        Dim oBatch As Batch
        '        Dim oPayments As Payments
        '        Dim oPayment As Payment
        '        Dim oInvoice As Invoice
        '        Dim oFreeText As Freetext
        '        Dim sMySQL As String
        '        Dim rsBabelFile As New ADODB.Recordset
        '        Dim bReturnValue As Boolean
        '        Dim xBabelFile_ID As Integer
        '        Dim xBatch_ID As Integer
        '        Dim rsFreetext As New ADODB.Recordset
        '        Dim lPaymentIndex As Integer

        '        On Error GoTo errorRetrieveData

        '        If cnProfile.State = ADODB.ObjectStateEnum.adStateClosed Then
        '            ConnectToProfile()
        '        End If

        '        oProfile = New vbBabel.Profile
        '        oProfile.Load(1)

        '        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        mCol = Nothing
        '        mCol = New Collection

        '        sMySQL = "SELECT "
        '        sMySQL = sMySQL & "BF.BabelFile_ID, BF.FilenameIn, BF.FilenameInNo, BF.Special, "
        '        sMySQL = sMySQL & "B.Batch_ID, B.DATE_Production, "
        '        sMySQL = sMySQL & "P.Payment_ID, P.I_Name, P.I_Account, "
        '        sMySQL = sMySQL & "P.DATE_Value, P.DATE_Payment, "
        '        sMySQL = sMySQL & "P.MON_InvoiceAmount, P.Filenameout_ID, "
        '        sMySQL = sMySQL & "P.E_Name, P.PayCode, P.Ref_Own FROM BabelFile BF, Batch B, Payment P"
        '        sMySQL = sMySQL & " WHERE P.Company_ID = " & Trim(Str(iCompanyNo))
        '        sMySQL = sMySQL & " AND B.BabelFile_ID = BF.BabelFile_ID"
        '        sMySQL = sMySQL & " AND P.BabelFile_ID = BF.BabelFile_ID"
        '        sMySQL = sMySQL & " AND P.Batch_ID = B.Batch_ID"
        '        sMySQL = sMySQL & " AND P.I_Account ='" & sAccountNo & "'"
        '        sMySQL = sMySQL & " AND P.MON_InvoiceAmount>=" & lFromAmount
        '        sMySQL = sMySQL & " AND P.MON_InvoiceAmount<=" & lToAmount
        '        sMySQL = sMySQL & " AND P.Date_Payment>='" & sFromDate & "'"
        '        sMySQL = sMySQL & " AND P.Date_Payment<='" & sToDate & "'"
        '        sMySQL = sMySQL & " AND P.Date_Value>='" & sFromPayDate & "'"
        '        sMySQL = sMySQL & " AND P.Date_Value<='" & sToPayDate & "'"
        '        sMySQL = sMySQL & " AND P.PayCode='" & sPayCode & "'" & " AND BF.Babelfile_ID < 11000000"
        '        sMySQL = sMySQL & " ORDER BY P.BabelFile_ID, P.Batch_ID, P.Payment_ID"

        '        rsBabelFile.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        '        Dim ntimer As Integer

        '        'Create the collection
        '        If rsBabelFile.RecordCount > 0 Then
        '            '    ntimer = Timer
        '            xBabelFile_ID = 0

        '            For lCounter = 1 To rsBabelFile.RecordCount
        '                If xBabelFile_ID <> rsBabelFile.Fields("BabelFile_ID").Value Then
        '                    oBabelFile = Add(Trim(Str(rsBabelFile.Fields("BabelFile_ID").Value)))
        '                    oBabelFile.VB_Profile = oProfile
        '                    oBabelFile.FilenameIn = rsBabelFile.Fields("FilenameIn").Value
        '                    oBabelFile.VB_FilenameInNo = rsBabelFile.Fields("FilenameInNo").Value
        '                    If rsBabelFile.Fields("Special").Value Is System.DBNull.Value Then
        '                        oBabelFile.Special = rsBabelFile.Fields("Special").Value
        '                    End If
        '                    oBabelFile.Index = rsBabelFile.Fields("BabelFile_ID").Value
        '                    'oBabelFile.Special = rsBabelFile!Special
        '                    xBabelFile_ID = rsBabelFile.Fields("BabelFile_ID").Value
        '                    xBatch_ID = 0
        '                    oBatches = New Batches
        '                    oBabelFile.Batches = oBatches
        '                End If
        '                If xBatch_ID <> rsBabelFile.Fields("Batch_ID").Value Then
        '                    oBatch = oBatches.Add(Trim(Str(rsBabelFile.Fields("Batch_ID").Value)))
        '                    oBatch.VB_Profile = oProfile
        '                    oBatch.DATE_Production = rsBabelFile.Fields("DATE_Production").Value
        '                    oBatch.Batch_ID = rsBabelFile.Fields("Batch_ID").Value
        '                    oBatch.Index = rsBabelFile.Fields("Batch_ID").Value
        '                    oPayments = New Payments
        '                    oBatch.Payments = oPayments
        '                    xBatch_ID = rsBabelFile.Fields("Batch_ID").Value
        '                    lPaymentIndex = 0
        '                End If

        '                ' Add one payment for each record in recordset
        '                oPayment = oPayments.Add(Trim(Str(rsBabelFile.Fields("Payment_ID").Value)))
        '                oPayment.VB_Profile = oProfile
        '                oPayment.Payment_ID = rsBabelFile.Fields("Payment_ID").Value
        '                lPaymentIndex = lPaymentIndex + 1
        '                oPayment.Index = lPaymentIndex ' Created Errors when sorting!rsBabelFile!Payment_ID
        '                oPayment.I_Name = rsBabelFile.Fields("I_Name").Value
        '                oPayment.I_Account = rsBabelFile.Fields("I_Account").Value
        '                oPayment.DATE_Value = rsBabelFile.Fields("DATE_Value").Value
        '                oPayment.DATE_Payment = rsBabelFile.Fields("DATE_Payment").Value
        '                oPayment.MON_InvoiceAmount = CDbl(rsBabelFile.Fields("MON_InvoiceAmount").Value)
        '                oPayment.VB_FilenameOut_ID = rsBabelFile.Fields("FilenameOut_ID").Value
        '                oPayment.E_Name = rsBabelFile.Fields("E_Name").Value
        '                oPayment.PayCode = rsBabelFile.Fields("PayCode").Value
        '                oPayment.REF_Own = rsBabelFile.Fields("REF_Own").Value

        '                ' New 06.07.05 - for UDUS we must find Invoice/Freetextinfo;
        '                If rsBabelFile.Fields("PayCode").Value = "180" Then
        '                    ' Add one Invoice for each record in recordset
        '                    oInvoice = oPayment.Invoices.Add(CStr(1))
        '                    oInvoice.MON_InvoiceAmount = CDbl(rsBabelFile.Fields("MON_InvoiceAmount").Value)

        '                    ' Find all freetext for this one;
        '                    sMySQL = "SELECT xText from Freetext "
        '                    sMySQL = sMySQL & " WHERE Company_ID = " & Trim(Str(iCompanyNo))
        '                    sMySQL = sMySQL & " AND BabelFile_ID = " & rsBabelFile.Fields("BabelFile_ID").Value
        '                    sMySQL = sMySQL & " AND Batch_ID = " & rsBabelFile.Fields("Batch_ID").Value
        '                    sMySQL = sMySQL & " AND Payment_ID = " & rsBabelFile.Fields("Payment_ID").Value
        '                    sMySQL = sMySQL & " AND Invoice_ID = 1"

        '                    rsFreetext.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)
        '                    If rsFreetext.RecordCount > 0 Then
        '                        For lFreeCounter = 1 To rsFreetext.RecordCount
        '                            oFreeText = oInvoice.Freetexts.Add '(lFreeCounter)
        '                            oFreeText.Text = rsFreetext.Fields("XText").Value
        '                            rsFreetext.MoveNext()
        '                        Next lFreeCounter
        '                    End If
        '                    rsFreetext.Close()
        '                End If

        '                rsBabelFile.MoveNext()
        '            Next lCounter
        '            'Else
        '            '    sMATCH_ErrorText = LRS(14013) '"No payments to retrieve." & vbcrlf & vbcrlf "No payments among the stored payments match the criterias."
        '        End If
        '        ntimer = VB.Timer() - ntimer
        '        'UPGRADE_NOTE: Object rsFreetext may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        rsFreetext = Nothing
        '        rsBabelFile.Close()
        '        'UPGRADE_NOTE: Object rsBabelFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        rsBabelFile = Nothing

        '        FI_FastRetrieve = bReturnValue

        '        Exit Function

        'errorRetrieveData:
        '        FI_FastRetrieve = False
        '        If Not rsBabelFile Is Nothing Then
        '            'UPGRADE_NOTE: Object rsBabelFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '            rsBabelFile = Nothing
        '        End If
        '        sMATCH_ErrorText = LRS(14001, LRS(14010)) & vbCrLf & vbCrLf '"An error occured during retrieving data."
        '        If Err.Number = 999 Then
        '            'Err is raised in BabelFile, Batch, Payment, Invoice or Freetext
        '            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & Err.Source & vbCrLf '14002 = "Source: "
        '        Else
        '            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & "BabelFiles" & vbCrLf '14002 = "Source: "
        '        End If

        '        sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & Err.Description '14003 = "Description: "

    End Function
    'XNET - 28.03.2012 - Changed next line
    Public Function MATCH_ReindexCollection(Optional ByVal bReIndex As Boolean = True) As Boolean
        Dim oBabelFile As BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim lBatchCount, lBabelCount, lPaymentCount As Integer


        On Error GoTo errorMATCH_ReindexCollection

        'Remove empty batches
        For lBabelCount = mCol.Count() To 1 Step -1
            oBabelFile = mCol.Item(lBabelCount)
            For lBatchCount = oBabelFile.Batches.Count To 1 Step -1
                oBatch = oBabelFile.Batches.Item(lBatchCount)
                If oBatch.Payments.Count = 0 Then
                    oBabelFile.Batches.Remove((lBatchCount)) 'oBatch.Index)
                End If
            Next lBatchCount
            If oBabelFile.Batches.Count = 0 Then
                mCol.Remove((lBabelCount)) 'oBabelFile.Index)
            End If
        Next lBabelCount
        'Renumerate the index-field in the collection, because it has to be sequential from 1
        '  in other parts of BB. i.e. reports
        'XNET - 28.03.2012 - Added next IF
        If bReIndex Then
            lBabelCount = 0
            For Each oBabelFile In mCol
                lBabelCount = lBabelCount + 1
                lBatchCount = 0
                oBabelFile.Index = lBabelCount
                For Each oBatch In oBabelFile.Batches
                    lBatchCount = lBatchCount + 1
                    lPaymentCount = 0
                    oBatch.Index = lBatchCount
                    For Each oPayment In oBatch.Payments
                        lPaymentCount = lPaymentCount + 1
                        oPayment.Index = lPaymentCount
                    Next oPayment
                Next oBatch
            Next oBabelFile 'oBabelFile
        End If

        MATCH_ReindexCollection = True
        Exit Function

errorMATCH_ReindexCollection:

        MATCH_ReindexCollection = False
        sMATCH_ErrorText = LRS(14001, LRS(14010)) & vbCrLf & vbCrLf '"An error occured during retrieving data."

        sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & Err.Description '14003 = "Description: "

    End Function

    Public Function MATCH_FirstPayment(ByRef bFromLastImport As Boolean, Optional ByRef bOpenItemsOnly As Boolean = True, Optional ByRef bShowMarkedPayments As Boolean = True) As Boolean
        Dim sMySQL As String
        Dim bx As Boolean
        Dim bReturnValue As Boolean
        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing

        bReturnValue = False

        On Error GoTo ERRFirstPayment

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        mCol = Nothing
        mCol = New Collection

        If bFromLastImport Then
            sMySQL = "SELECT P.Babelfile_ID, P.Batch_ID, P.Payment_ID, P.PaymentCounter, B.ExportDate FROM BabelFile B, Payment P"
            If Not EmptyString(sClientGroupFilter) Then
                If Not bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & ", Client C "
                End If
            End If
            sMySQL = sMySQL & " WHERE "
            If bOpenItemsOnly Then
                sMySQL = sMySQL & "MATCH_Matched < 3 AND "
            Else
                'No constraints regarding the MATCH_Matched status.
            End If
            Select Case sFilter
                Case "ALL"
                    'No more constraints
                Case "ANONYMOUS"
                    If bSQLServer Then
                        sMySQL = sMySQL & "(len(P.UserID) = 0 OR P.UserID IS NULL) AND "
                    Else
                        sMySQL = sMySQL & "(len(trim(P.UserID)) = 0 OR P.UserID IS NULL) AND "
                    End If
                    '    Case "ANONYMOUS"
                    '        sMySQL = sMySQL & "P.UserID = ' ' AND "
                Case "MINE"
                    sMySQL = sMySQL & "P.UserID = '" & sMyUserID & "' AND "
                Case "PROPOSED"
                    ' 06.11.2017 - added new filter for Proposed matched
                    sMySQL = sMySQL & "Match_Matched = 1 AND "

                Case Else
                    ' 07.07.2010 - added specialfiltering
                    sMySQL = SpecialFilter(sMySQL)
            End Select
            sMySQL = sMySQL & "P.StatusCode <> '-1' AND "
            If Not bShowMarkedPayments Then
                If bSQLServer Then
                    sMySQL = sMySQL & "DoNotShow = 0 AND "
                Else
                    sMySQL = sMySQL & "DoNotShow = False AND "
                End If
            End If
            sMySQL = sMySQL & "B.Company_ID = " & Trim(Str(oProfile.Company_ID)) & " AND B.Company_ID = P.Company_ID AND B.Babelfile_ID = P.Babelfile_ID AND B.Babelfile_ID < 11000000"
            If Not EmptyString(sClientGroupFilter) Then
                'sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
                If bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & " AND I_Account = '" & sClientGroupFilter & "'"
                Else
                    sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo "
                    sMySQL = sMySQL & "AND ClientGroup = '" & sClientGroupFilter & "'"
                End If
            End If
            'If Not EmptyString(sClientGroupFilter) Then
            '    sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
            'End If
            '13.07.2015 - Changed the way to move in manual matching
            sMySQL = sMySQL & "  ORDER BY B.ExportDate DESC, PaymentCounter ASC"
            'sMySQL = sMySQL & " ORDER BY B.ExportDate DESC, B.BabelFile_ID ASC, P.Unique_PaymentID ASC"

        Else
            sMySQL = "SELECT Babelfile_ID, Batch_ID, Payment_ID, PaymentCounter FROM Payment P"
            If Not EmptyString(sClientGroupFilter) Then
                If Not bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & ", Client C "
                End If
            End If
            sMySQL = sMySQL & " WHERE "
            If bOpenItemsOnly Then
                sMySQL = sMySQL & "MATCH_Matched < 3 AND "
            Else
                'No constraints regarding the MATCH_Matched status.
            End If
            Select Case sFilter
                Case "ALL"
                    'No more constraints
                Case "ANONYMOUS"
                    If bSQLServer Then
                        sMySQL = sMySQL & "(len(UserID) = 0 OR UserID IS NULL) AND "
                    Else
                        sMySQL = sMySQL & "(len(trim(UserID)) = 0 OR UserID IS NULL) AND "
                    End If
                Case "MINE"
                    sMySQL = sMySQL & "UserID = '" & sMyUserID & "' AND "
                Case "PROPOSED"
                    ' 06.11.2017 - added new filter for Proposed matched
                    sMySQL = sMySQL & "Match_Matched = 1 AND "

                Case Else
                    ' 07.07.2010 - added specialfiltering
                    sMySQL = SpecialFilter(sMySQL)
            End Select
            sMySQL = sMySQL & "StatusCode <> '-1' AND "
            If Not bShowMarkedPayments Then
                If bSQLServer Then
                    sMySQL = sMySQL & "DoNotShow = 0 AND "
                Else
                    sMySQL = sMySQL & "DoNotShow = False AND "
                End If
            End If
            sMySQL = sMySQL & "P.Company_ID = " & Trim(Str(oProfile.Company_ID)) & " AND Babelfile_ID < 11000000"
            If Not EmptyString(sClientGroupFilter) Then
                'sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
                If bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & " AND I_Account = '" & sClientGroupFilter & "'"
                Else
                    sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo "
                    sMySQL = sMySQL & "AND ClientGroup = '" & sClientGroupFilter & "'"
                End If
            End If
            '13.07.2015 - Changed the way to move in manual matching
            sMySQL = sMySQL & "  ORDER BY PaymentCounter ASC"
            'sMySQL = sMySQL & " ORDER BY BabelFile_ID ASC, Unique_PaymentID ASC"
            End If

            myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessCommand.CommandType = CommandType.Text
            myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
            If bSQLServer Then
                sMySQL = sMySQL.Replace("False", "0")
                sMySQL = sMySQL.Replace("false", "0")
                sMySQL = sMySQL.Replace("True", "1")
                sMySQL = sMySQL.Replace("true", "1")
            End If
            myBBDB_AccessCommand.CommandText = sMySQL

            myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

            If myBBDB_AccessReader.HasRows Then
                myBBDB_AccessReader.Read()
                '0 = BabelFile_ID, 1 = Batch_ID, 2 = Payment_ID
                If MATCH_RetrieveData(True, oProfile.Company_ID, False, False, True, myBBDB_AccessReader.GetInt32(0), myBBDB_AccessReader.GetInt32(1), myBBDB_AccessReader.GetInt32(2)) Then
                    bReturnValue = True
                Else
                    bReturnValue = False
                End If
            Else
                sMATCH_ErrorText = LRS(14005) '"There are no open records."
                bReturnValue = False
            End If

            If Not myBBDB_AccessReader Is Nothing Then
                If Not myBBDB_AccessReader.IsClosed Then
                    myBBDB_AccessReader.Close()
                End If
                myBBDB_AccessReader = Nothing
            End If
            If Not myBBDB_AccessCommand Is Nothing Then
                myBBDB_AccessCommand.Dispose()
                myBBDB_AccessCommand = Nothing
            End If

            MATCH_FirstPayment = bReturnValue

            Exit Function

ERRFirstPayment:

            sMATCH_ErrorText = Err.Description

            If Not myBBDB_AccessReader Is Nothing Then
                If Not myBBDB_AccessReader.IsClosed Then
                    myBBDB_AccessReader.Close()
                End If
                myBBDB_AccessReader = Nothing
            End If
            If Not myBBDB_AccessCommand Is Nothing Then
                myBBDB_AccessCommand.Dispose()
                myBBDB_AccessCommand = Nothing
            End If

            MATCH_FirstPayment = False

    End Function
    Private Function SpecialFilter(ByRef sMySQL As String) As String
        ' .NET 07.07.2010 - added this function for special filtering in MatchManual

        ' - ODIN: Filter on Valuta, .Filter = "VALUTA-USD", "VALUTA-EUR", etc
        If Left(sFilter, 7) = "VALUTA-" Then
            sMySQL = sMySQL & "MON_InvoiceCurrency = '" & Right(sFilter, 3) & "' AND "
        End If

        SpecialFilter = sMySQL
    End Function
    Public Function MATCH_LastPayment(Optional ByRef bOpenItemsOnly As Boolean = True, Optional ByRef bShowMarkedPayments As Boolean = True) As Boolean
        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim sMySQL As String
        Dim bx As Boolean
        Dim bReturnValue As Boolean

        bReturnValue = False

        On Error GoTo ERRLastPayment

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        mCol = Nothing
        mCol = New Collection

        sMySQL = "SELECT Babelfile_ID, Batch_ID, Payment_ID, PaymentCounter FROM Payment P"
        If Not EmptyString(sClientGroupFilter) Then
            If Not bClientGroupFilterIsAccount Then
                sMySQL = sMySQL & ", Client C "
            End If
        End If
        sMySQL = sMySQL & " WHERE "
        If bOpenItemsOnly Then
            sMySQL = sMySQL & "MATCH_Matched < 3 AND "
        Else
            'No constraints regarding the MATCH_Matched status.
        End If
        Select Case sFilter
            Case "ALL"
                'No more constraints
            Case "ANONYMOUS"
                If bSQLServer Then
                    sMySQL = sMySQL & "(len(UserID) = 0 OR UserID IS NULL) AND "
                Else
                    sMySQL = sMySQL & "(len(trim(UserID)) = 0 OR UserID IS NULL) AND "
                End If
                'Case "ANONYMOUS"
                '    sMySQL = sMySQL & "UserID = ' ' AND "
            Case "MINE"
                sMySQL = sMySQL & "UserID = '" & sMyUserID & "' AND "
            Case "PROPOSED"
                ' 06.11.2017 - added new filter for Proposed matched
                sMySQL = sMySQL & "Match_Matched = 1 AND "

            Case Else
                ' 07.07.2010 - added specialfiltering
                sMySQL = SpecialFilter(sMySQL)
        End Select
        sMySQL = sMySQL & "StatusCode <> '-1' AND "
        If Not bShowMarkedPayments Then
            If bSQLServer Then
                sMySQL = sMySQL & "DoNotShow = 0 AND "
            Else
                sMySQL = sMySQL & "DoNotShow = False AND "
            End If
        End If
        sMySQL = sMySQL & "P.Company_ID=" & Trim(Str(oProfile.Company_ID)) & " AND Babelfile_ID < 11000000"
        If Not EmptyString(sClientGroupFilter) Then
            'sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
            If bClientGroupFilterIsAccount Then
                sMySQL = sMySQL & " AND I_Account = '" & sClientGroupFilter & "'"
            Else
                sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo "
                sMySQL = sMySQL & "AND ClientGroup = '" & sClientGroupFilter & "'"
            End If
        End If
        '13.07.2015 - Changed the way to move in manual matching
        sMySQL = sMySQL & "  ORDER BY PaymentCounter DESC"
        'sMySQL = sMySQL & "  ORDER BY BabelFile_ID DESC, Unique_PaymentID DESC"
        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
        myBBDB_AccessCommand.CommandType = CommandType.Text
        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommand.CommandText = sMySQL

        myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

        If myBBDB_AccessReader.HasRows Then
            myBBDB_AccessReader.Read()
            If MATCH_RetrieveData(True, oProfile.Company_ID, False, False, True, myBBDB_AccessReader.GetInt32(0), myBBDB_AccessReader.GetInt32(1), myBBDB_AccessReader.GetInt32(2)) Then
                bReturnValue = True
            Else
                bReturnValue = False
            End If
        Else
            sMATCH_ErrorText = LRS(14005) '"There are no open records."
            bReturnValue = False
        End If

        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If

        MATCH_LastPayment = bReturnValue

        Exit Function

ERRLastPayment:
        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If

        sMATCH_ErrorText = Err.Description

        MATCH_LastPayment = False

    End Function
    Public Function MATCH_NextPayment(Optional ByRef bOpenItemsOnly As Boolean = True, Optional ByRef bShowMarkedPayments As Boolean = True, Optional ByRef sSpecial As String = "") As Boolean
        Dim sMySQL As String
        Dim bx As Boolean
        Dim bReturnValue As Boolean
        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        'Dim nLastUsedPaymentID As Double
        Dim nLastUsedPaymentCounter As Double
        Dim nBabelFileID As Double
        Dim sLastClientNo As String

        bReturnValue = False

        On Error GoTo ERRNextPayment

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        'Check first that we are working on a specific payment, and retrieve its PaymentID
        If mCol.Count() = 1 Then
            If mCol.Item(1).Batches.Count = 1 Then
                If mCol.Item(1).Batches.Item(1).Payments.Count = 1 Then
                    '13.07.2015 - Changed the way to move in manual matching
                    nLastUsedPaymentCounter = mCol.Item(1).Batches.Item(1).Payments.Item(1).PaymentCounter
                    'nLastUsedPaymentID = mCol.Item(1).Batches.Item(1).Payments.Item(1).Unique_PaymentID
                    nBabelFileID = mCol.Item(1).VB_DBBabelFileIndex
                    'sLastClientNo = mCol.Item(1).Batches.Item(1).Payments.Item(1).vb_clientno
                    bReturnValue = True
                Else
                    sMATCH_ErrorText = LRS(14006, "MATCH_NextPayment")
                    '"An unexcpected error occured. The method MATCH_NextPayment is used in the wrong context"
                    bReturnValue = False
                End If
            Else
                sMATCH_ErrorText = LRS(14006, "MATCH_NextPayment")
                '"An unexcpected error occured. The method MATCH_NextPayment is used in the wrong context"
                bReturnValue = False
            End If
        Else
            If mCol.Count() = 0 Then
                bReturnValue = False
                sMATCH_ErrorText = LRS(14007, LRS(14011))
                '"You have reached the last open payment."
            Else
                bReturnValue = False
                sMATCH_ErrorText = LRS(14006, "MATCH_NextPayment")
                '"An unexcpected error occured. The method MATCH_NextPayment is used in the wrong context"
            End If
        End If

        If bReturnValue Then
            sMySQL = "SELECT Babelfile_ID, Batch_ID, Payment_ID, PaymentCounter, VB_ClientNo FROM Payment P"
            If Not EmptyString(sClientGroupFilter) Then
                If Not bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & ", Client C "
                End If
            End If
            sMySQL = sMySQL & " WHERE "
            If bOpenItemsOnly Then
                sMySQL = sMySQL & "MATCH_Matched < 3 AND "
            Else
                'No constraints regarding the MATCH_Matched status.
            End If
            Select Case sFilter
                Case "ALL"
                    'No more constraints
                Case "ANONYMOUS"
                    If bSQLServer Then
                        sMySQL = sMySQL & "(len(UserID) = 0 OR UserID IS NULL) AND "
                    Else
                        sMySQL = sMySQL & "(len(trim(UserID)) = 0 OR UserID IS NULL) AND "
                    End If
                Case "MINE"
                    sMySQL = sMySQL & "UserID = '" & sMyUserID & "' AND "
                Case "PROPOSED"
                    ' 06.11.2017 - added new filter for Proposed matched
                    sMySQL = sMySQL & "Match_Matched = 1 AND "
                Case Else
                    ' 07.07.2010 - added specialfiltering
                    sMySQL = SpecialFilter(sMySQL)
            End Select
            sMySQL = sMySQL & "StatusCode <> '-1' AND "
            If Not bShowMarkedPayments Then
                If bSQLServer Then
                    sMySQL = sMySQL & "DoNotShow = 0 AND "
                Else
                    sMySQL = sMySQL & "DoNotShow = False AND "
                End If
            End If
            '13.07.2015 - Changed the way to move in manual matching
            sMySQL = sMySQL & "PaymentCounter > " & nLastUsedPaymentCounter.ToString
            sMySQL = sMySQL & " AND P.Company_ID = " & oProfile.Company_ID.ToString & " AND Babelfile_ID < 11000000"


            'sMySQL = sMySQL & "((Unique_PaymentID > " & nLastUsedPaymentID.ToString
            'sMySQL = sMySQL & " AND BabelFile_ID = " & nBabelFileID.ToString
            'sMySQL = sMySQL & ") OR BabelFile_ID > " & nBabelFileID.ToString
            'sMySQL = sMySQL & ") AND P.Company_ID = " & oProfile.Company_ID.ToString & " AND Babelfile_ID < 11000000"
            If Not EmptyString(sClientGroupFilter) Then
                'sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
                If bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & " AND I_Account = '" & sClientGroupFilter & "'"
                Else
                    sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo "
                    sMySQL = sMySQL & "AND ClientGroup = '" & sClientGroupFilter & "'"
                End If
            End If
            '13.07.2015 - Changed the way to move in manual matching
            sMySQL = sMySQL & "  ORDER BY PaymentCounter ASC"
            'sMySQL = sMySQL & " ORDER BY BabelFile_ID ASC, Unique_PaymentID ASC, VB_ClientNo ASC"

            myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessCommand.CommandType = CommandType.Text
            myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
            myBBDB_AccessCommand.CommandText = sMySQL

            myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

            If myBBDB_AccessReader.HasRows Then
                'Kill old collection, create a new and populate
                mCol = Nothing
                mCol = New Collection

                '08.07.2015 - Added next code to find the 'next' payment
                myBBDB_AccessReader.Read()
                'Do While myBBDB_AccessReader.Read()
                '    If myBBDB_AccessReader.GetDecimal(3) = nLastUsedPaymentID Then
                '        If myBBDB_AccessReader.GetInt32(0) = nBabelFileID Then
                '            If myBBDB_AccessReader.GetString(4) <= sLastClientNo Then

                '            Else

                '            End If

                '        Else

                '        End If
                '    Else
                '        'OK, it is the next payment
                '    End If
                'Loop

                '0 = BabelFile_ID, 1 = Batch_ID, 2 = Payment_ID
                If MATCH_RetrieveData(True, oProfile.Company_ID, False, False, True, myBBDB_AccessReader.GetInt32(0), myBBDB_AccessReader.GetInt32(1), myBBDB_AccessReader.GetInt32(2)) Then
                    bReturnValue = True
                Else
                    bReturnValue = False
                End If
            Else
                sMATCH_ErrorText = LRS(14007, LRS(14011)) '"You have reached the last open payment."
                bReturnValue = False
            End If

        End If

        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If

        MATCH_NextPayment = bReturnValue

        Exit Function

ERRNextPayment:

        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If

        sMATCH_ErrorText = Err.Description

        MATCH_NextPayment = False

    End Function

    Public Function MATCH_PreviousPayment(Optional ByRef bOpenItemsOnly As Boolean = True, Optional ByRef bShowMarkedPayments As Boolean = True) As Boolean
        Dim sMySQL As String
        Dim bx As Boolean
        'Dim lLastUsedPaymentID As Integer
        Dim nLastUsedPaymentCounter As Double
        Dim bReturnValue As Boolean
        Dim lBabelFile_ID As Integer
        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing

        bReturnValue = False

        On Error GoTo ERRPreviousPayment

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        'Check first that we are working on a specific payment, and retrieve its PaymentCounter
        If mCol.Count() = 1 Then
            If mCol.Item(1).Batches.Count = 1 Then
                If mCol.Item(1).Batches.Item(1).Payments.Count = 1 Then
                    '13.07.2015 - Changed the way to move in manual matching
                    nLastUsedPaymentCounter = mCol.Item(1).Batches.Item(1).Payments.Item(1).PaymentCounter
                    'lLastUsedPaymentID = mCol.Item(1).Batches.Item(1).Payments.Item(1).Unique_PaymentID
                    lBabelFile_ID = mCol.Item(1).VB_DBBabelFileIndex
                    bReturnValue = True
                Else
                    bReturnValue = False
                    sMATCH_ErrorText = LRS(14006, "MATCH_PreviousPayment")
                    '"An unexcpected error occured. The method MATCH_PreviousPayment is used in the wrong context"
                End If
            Else
                bReturnValue = False
                sMATCH_ErrorText = LRS(14006, "MATCH_PreviousPayment")
                '"An unexcpected error occured. The method MATCH_PreviousPayment is used in the wrong context"
            End If
        Else
            If mCol.Count() = 0 Then
                bReturnValue = False
                sMATCH_ErrorText = LRS(14007, LRS(14012))
                '"You have reached the first open payment."
            Else
                bReturnValue = False
                sMATCH_ErrorText = LRS(14006, "MATCH_PreviousPayment")
                '"An unexcpected error occured. The method MATCH_PreviousPayment is used in the wrong context"
            End If
        End If

        If bReturnValue Then
            sMySQL = "SELECT Babelfile_ID, Batch_ID, Payment_ID, PaymentCounter FROM Payment P"
            If Not EmptyString(sClientGroupFilter) Then
                If Not bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & ", Client C "
                End If
            End If
            sMySQL = sMySQL & " WHERE "
            If bOpenItemsOnly Then
                sMySQL = sMySQL & "MATCH_Matched < 3 AND "
            Else
                'No constraints regarding the MATCH_Matched status.
            End If
            Select Case sFilter
                Case "ALL"
                    'No more constraints
                Case "ANONYMOUS"
                    If bSQLServer Then
                        sMySQL = sMySQL & "(len(UserID) = 0 OR UserID IS NULL) AND "
                    Else
                        sMySQL = sMySQL & "(len(trim(UserID)) = 0 OR UserID IS NULL) AND "
                    End If
                    '    Case "ANONYMOUS"
                    '        sMySQL = sMySQL & "UserID = ' ' AND "
                Case "MINE"
                    sMySQL = sMySQL & "UserID = '" & sMyUserID & "' AND "
                Case "PROPOSED"
                    ' 06.11.2017 - added new filter for Proposed matched
                    sMySQL = sMySQL & "Match_Matched = 1 AND "

                Case Else
                    ' 07.07.2010 - added specialfiltering
                    sMySQL = SpecialFilter(sMySQL)
            End Select
            sMySQL = sMySQL & "StatusCode <> '-1' AND "
            If Not bShowMarkedPayments Then
                If bSQLServer Then
                    sMySQL = sMySQL & "DoNotShow = 0 AND "
                Else
                    sMySQL = sMySQL & "DoNotShow = False AND "
                End If
            End If
            '13.07.2015 - Changed the way to move in manual matching
            sMySQL = sMySQL & "PaymentCounter < " & nLastUsedPaymentCounter.ToString
            sMySQL = sMySQL & " AND P.Company_ID = " & oProfile.Company_ID.ToString & " AND Babelfile_ID < 11000000"
            'sMySQL = sMySQL & "((Unique_PaymentID < " & Trim(Str(lLastUsedPaymentID))
            'sMySQL = sMySQL & " AND BabelFile_ID = " & Trim(Str(lBabelFile_ID))
            'sMySQL = sMySQL & ") OR BabelFile_ID < " & Trim(Str(lBabelFile_ID))
            'sMySQL = sMySQL & ") AND P.Company_ID = " & Trim(Str(oProfile.Company_ID)) & " AND Babelfile_ID < 11000000"
            If Not EmptyString(sClientGroupFilter) Then
                'sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
                If bClientGroupFilterIsAccount Then
                    sMySQL = sMySQL & " AND I_Account = '" & sClientGroupFilter & "'"
                Else
                    sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo "
                    sMySQL = sMySQL & "AND ClientGroup = '" & sClientGroupFilter & "'"
                End If
            End If
            'If Not EmptyString(sClientGroupFilter) Then
            '    sMySQL = sMySQL & " AND P.Company_ID = C.Company_ID AND P.VB_ClientNo = C.ClientNo AND ClientGroup = '" & sClientGroupFilter & "'"
            'End If
            '13.07.2015 - Changed the way to move in manual matching
            sMySQL = sMySQL & "  ORDER BY PaymentCounter DESC"
            'sMySQL = sMySQL & " ORDER BY BabelFile_ID DESC, Unique_PaymentID DESC"
            myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessCommand.CommandType = CommandType.Text
            myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
            myBBDB_AccessCommand.CommandText = sMySQL

            myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

            If myBBDB_AccessReader.HasRows Then
                'Kill old collection, create a new and populate
                mCol = Nothing
                mCol = New Collection
                myBBDB_AccessReader.Read()
                If MATCH_RetrieveData(True, oProfile.Company_ID, False, False, True, myBBDB_AccessReader.GetInt32(0), myBBDB_AccessReader.GetInt32(1), myBBDB_AccessReader.GetInt32(2)) Then
                    bReturnValue = True
                Else
                    bReturnValue = False
                End If
            Else
                sMATCH_ErrorText = LRS(14007, LRS(14012))
                '"You have reached the first open payment."
                bReturnValue = False
            End If

            MATCH_PreviousPayment = bReturnValue
        End If

        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If

        MATCH_PreviousPayment = bReturnValue

        Exit Function

ERRPreviousPayment:
        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If

        sMATCH_ErrorText = Err.Description

        MATCH_PreviousPayment = False

    End Function

    Public Function MATCH_SavePayment() As Boolean
        Dim bReturnValue As Boolean

        'Uses the MATCH_SaveData method. It may be better to create new methods for
        '   this task. Whats important is that the variable 'SaveOnlyThisPayment' is set to true.
        bReturnValue = MATCH_SaveData(False, True, False)

        MATCH_SavePayment = bReturnValue

    End Function
    Public Function MATCH_SaveNameAndAddress() As Boolean
        Dim bReturnValue As Boolean

        'Uses the MATCH_SaveData method. It may be better to create new methods for
        '   this task. Whats important is that the variable 'SaveOnlyThisPayment' is set to true.
        bReturnValue = MATCH_SaveData(False, True, False, True)

        MATCH_SaveNameAndAddress = bReturnValue

    End Function
    Public Function MATCH_RemoveMatchedPayments(ByRef bJustNonExportedPayments As Boolean) As Boolean
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim bReturnValue, bx As Boolean
        Dim sMySQL As String
        Dim sErrorString As String
        Dim iRecordsAffected As Integer = 0

        'Variables used in connection with testing the error that occurs for Aktiv Kapital (may be omitted later).
        Dim bProfileLoadedInFunction As Boolean
        Dim bConnectionMadeInFunction As Boolean
        Dim sMessage As String
        Dim bErrorOccured As Boolean
        Dim iCounter As Short
        Dim iResponse As Short
        'End variables errortesting.

        If Format(Now(), "yyyyMMdd") = "20200414" Then
            MsgBox("Kommet til rutinen MATCH_RemoveMatchedPayments" & System.Environment.StackTrace)
            'Dim i As Integer
            'i = "KOKO"
        End If



        bProfileLoadedInFunction = False
        bConnectionMadeInFunction = False
        bErrorOccured = False

        bReturnValue = True

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
            bProfileLoadedInFunction = True
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
            bConnectionMadeInFunction = True
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
                bConnectionMadeInFunction = True
            End If
        End If

        If bReturnValue Then
            myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
            myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
            myBBDB_AccessCommand.CommandType = CommandType.Text
            myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

            myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

            If bJustNonExportedPayments Then
                '19.07.2016 - Critical change? because of SQL Server
                'DELETE * FROM Payment P WHERE EXISTS (SELECT * FROM BabelFile B WHERE P.BabelFile_ID = B.BabelFile_ID AND B.Exported = False) AND P.MATCH_Matched = 3 AND P.Company_ID = 1
                sMySQL = "DELETE FROM Payment WHERE EXISTS (SELECT * FROM BabelFile B WHERE Payment.BabelFile_ID = B.BabelFile_ID"
                If bSQLServer Then
                    sMySQL = sMySQL & " AND B.Exported = 0) AND Payment.MATCH_Matched = "
                Else
                    sMySQL = sMySQL & " AND B.Exported = False) AND Payment.MATCH_Matched = "
                End If
                sMySQL = sMySQL & Trim(Str(MatchStatus.Matched)) & " AND Payment.Company_ID = "
                sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))
                'Old code
                'sMySQL = "DELETE FROM Payment P WHERE EXISTS (SELECT * FROM BabelFile B WHERE P.BabelFile_ID = B.BabelFile_ID"
                'sMySQL = sMySQL & " AND B.Exported = False) AND P.MATCH_Matched = "
                'sMySQL = sMySQL & Trim(Str(MatchStatus.Matched)) & " AND P.Company_ID = "
                'sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))
            Else
                sMySQL = "DELETE FROM Payment WHERE MATCH_Matched = " & Trim(Str(MatchStatus.Matched))
                sMySQL = sMySQL & " AND Company_ID = "
                sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))
            End If

            myBBDB_AccessCommand.CommandText = sMySQL

            iRecordsAffected = myBBDB_AccessCommand.ExecuteNonQuery()
            'THIS PART OF THE FUNCTION IS NOT CONVERTED TO ADO.NET

            'If Not ExecuteTheSQL(cnProfile, sMySQL, sErrorString, bUseCommitAgainstBBDB) Then
            '    'AKTIV_Kapital'
            '    sMATCH_ErrorText = LRS(14008, "Payment (MATCH_RemoveMatchedPayments)") & vbCrLf & sErrorString
            '    '"When deleting records in Freetext, the following error occured:" & vbCrLf & sErrorString
            '    If Not mCol.Count() = 0 Then
            '        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().VB_Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            '        If Left(mCol.Item(1).VB_Profile.CompanyAdr2, 3) = "AQA" Then
            '            sMessage = ""
            '            sMessage = "SQL som feilet: " & vbCrLf & sMySQL
            '            sMessage = sMessage & vbCrLf & vbCrLf & "Verdi av bProfileLoadedInFunction: "
            '            If bProfileLoadedInFunction Then
            '                sMessage = sMessage & "True"
            '            Else
            '                sMessage = sMessage & "False"
            '            End If
            '            sMessage = sMessage & vbCrLf & "Verdi av bConnectionMadeInFunction: "
            '            If bConnectionMadeInFunction Then
            '                sMessage = sMessage & "True"
            '            Else
            '                sMessage = sMessage & "False"
            '            End If
            '            sMessage = sMessage & vbCrLf & vbCrLf
            '            If Not cnProfile Is Nothing Then
            '                If cnProfile.State = ADODB.ObjectStateEnum.adStateOpen Then
            '                    sMessage = vbCrLf & sMessage & "Provider: " & cnProfile.ConnectionString
            '                    sMessage = vbCrLf & sMessage & "Antall feil: " & cnProfile.Errors.Count
            '                    sMessage = sMessage & "ConnectionString: " & cnProfile.ConnectionString
            '                Else
            '                    sMessage = sMessage & "cnProfile.State er ikke Open!!!!"
            '                End If
            '            Else
            '                sMessage = sMessage & "cnProfile er nothing!!!"
            '            End If

            '            sMessage = sMessage & vbCrLf & vbCrLf & "Pr�ver � kj�re en SELECT."

            '            'Hent ut verdier fra Connection!!!!!!!!!!

            '            MsgBox(sMessage, MsgBoxStyle.OkOnly, "Feil i ExecuteSQL")

            '            On Error GoTo TestError ' Resume Next

            '            rs = CreateObject ("ADODB.Recordset")
            '            rs.Open("SELECT COUNT(*) AS Occurance FROM PAYMENT", cnProfile)

            '            On Error GoTo 0

            '            If bErrorOccured Then
            '                sMessage = "SELECT feilet!!!"
            '                MsgBox(sMessage, MsgBoxStyle.OkOnly, "Mislykket")
            '            Else
            '                sMessage = "Vellykket SELECT, antall poster: " & CStr(rs.Fields("Occurance").Value)
            '                MsgBox(sMessage, MsgBoxStyle.OkOnly, "Vellykket!")
            '            End If

            '            If Not rs Is Nothing Then
            '                If rs.State = ADODB.ObjectStateEnum.adStateOpen Then
            '                    rs.Close()
            '                End If
            '                'UPGRADE_NOTE: Object rs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '                rs = Nothing
            '            End If

            '            bErrorOccured = False
            '            iCounter = 0
            '            Do Until iCounter > 4
            '                sErrorString = ""
            '                iCounter = iCounter + 1
            '                sMessage = "�nsker du � koble til BabelBank sin databasen p� nytt, f�r du fors�ker � slette innbetalinger i databasen?"
            '                sMessage = sMessage & vbCrLf & vbCrLf & "Gjenst�ende fors�k: " & Trim(Str(5 - iCounter)) & vbCrLf & vbCrLf
            '                If Not cnProfile Is Nothing Then
            '                    If cnProfile.State = ADODB.ObjectStateEnum.adStateOpen Then
            '                        sMessage = sMessage & "P�logging er i live og �pen!"
            '                    Else
            '                        sMessage = sMessage & "P�logging er i live men lukket!"
            '                    End If
            '                Else
            '                    sMessage = sMessage & "P�logging er ikke aktiv"
            '                End If
            '                iResponse = MsgBox(sMessage, MsgBoxStyle.YesNo, "Ny p�logging?")
            '                If iResponse = MsgBoxResult.Yes Then
            '                    If Not cnProfile Is Nothing Then
            '                        If cnProfile.State = ADODB.ObjectStateEnum.adStateOpen Then
            '                            cnProfile.Close()
            '                        End If
            '                        'UPGRADE_NOTE: Object cnProfile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '                        cnProfile = Nothing
            '                    End If
            '                    ConnectToProfile()
            '                Else
            '                    'Continue
            '                End If

            '                If Not ExecuteTheSQL(cnProfile, sMySQL, sErrorString, bUseCommitAgainstBBDB) Then
            '                    MsgBox("F�lgende feil oppstod: " & vbCrLf & sErrorString, MsgBoxStyle.OkOnly, "Feil under sletting")
            '                    bReturnValue = False
            '                Else
            '                    MsgBox("Vellykket sletting. BabelBank forsetter normal kj�ring", MsgBoxStyle.OkOnly, "Vellykket sletting!")
            '                    iCounter = 6
            '                    bReturnValue = True
            '                End If
            '            Loop
            '        Else
            '            bReturnValue = False
            '        End If
            '    Else
            '        bReturnValue = False
            '    End If
            '    'bReturnValue = False
            'End If
            myBBDB_AccessTrans.Commit()

        End If

        If bReturnValue Then
            bReturnValue = RemoveEmptyCollectionsInDB()
        End If

        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        MATCH_RemoveMatchedPayments = bReturnValue

        Exit Function

TestError:
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        bErrorOccured = True
        Resume Next

    End Function
    Public Function MATCH_RemoveOldPayments(ByRef iDelDays As Short) As Boolean
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim bReturnValue, bx As Boolean
        Dim sMySQL, sErrorString As String
        Dim sLimitDate As String
        Dim dLimitdate As Date

        On Error GoTo ERRMATCH_RemoveOldPayments

        bReturnValue = True

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        If bReturnValue Then

            dLimitdate = System.DateTime.FromOADate(StringToDate(VB6.Format(Now, "YYYYMMDD")).ToOADate - iDelDays)
            ' dLimitDate  dd.mm.yy
            sLimitDate = DateToString(dLimitdate)


            If bSQLServer Then

                'Delete all at once
                sMySQL = "DELETE FROM Payment WHERE Date_Payment < '" & sLimitDate & "'"
                sMySQL = sMySQL & " AND Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString

                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessCommand.ExecuteNonQuery()

                myBBDB_AccessTrans.Commit()
            Else
                '19.12.2017 - Changed this code. May have problem with MaxLocks so we should delete first freext then invoice, then payment
                sMySQL = "DELETE F.* FROM FREETEXT F LEFT JOIN Payment P ON F.Company_ID = P.Company_ID AND F.BabelFile_ID = P.BabelFile_ID AND F.Batch_ID = P.Batch_ID AND F.Payment_ID = P.Payment_ID WHERE P.DATE_Payment < '" & sLimitDate & "'"
                sMySQL = sMySQL & " AND F.Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString

                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessCommand.ExecuteNonQuery()

                myBBDB_AccessTrans.Commit()

                'Disconnect and reconnect
                myBBDB_AccessConnection.Close()
                myBBDB_AccessConnection = Nothing
                ConnectToBBDatabse()

                'DELETE FROM Invoice
                sMySQL = "DELETE I.* FROM INVOICE I LEFT JOIN Payment P ON I.Company_ID = P.Company_ID AND I.BabelFile_ID = P.BabelFile_ID AND I.Batch_ID = P.Batch_ID AND I.Payment_ID = P.Payment_ID WHERE P.DATE_Payment < '" & sLimitDate & "'"
                sMySQL = sMySQL & " AND I.Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString

                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessCommand.ExecuteNonQuery()

                myBBDB_AccessTrans.Commit()

                'Disconnect and reconnect
                myBBDB_AccessConnection.Close()
                myBBDB_AccessConnection = Nothing
                ConnectToBBDatabse()

                'Finally, delete from Payment
                sMySQL = "DELETE P.* FROM Payment P WHERE P.Date_Payment < '" & sLimitDate & "'"
                sMySQL = sMySQL & " AND P.Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString

                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessCommand.ExecuteNonQuery()

                myBBDB_AccessTrans.Commit()
                'If Not ExecuteTheSQL(cnProfile, sMySQL, sErrorString, bUseCommitAgainstBBDB) Then
                '    sMATCH_ErrorText = LRS(14008, "Payment (MATCH_RemoveOldPayments)") & vbCrLf & sErrorString
                '    '"When deleting records in Freetext, the following error occured:" & vbCrLf & sErrorString
                '    bReturnValue = False
                'End If
            End If
        End If

        If bReturnValue Then
            bReturnValue = RemoveEmptyCollectionsInDB()
        End If

        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        MATCH_RemoveOldPayments = bReturnValue

        Exit Function

ERRMATCH_RemoveOldPayments:
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        sMATCH_ErrorText = LRS(14008, "Payment (MATCH_RemoveOldPayments)") & vbCrLf & Err.Description
        '"When deleting records in Freetext, the following error occured:" & vbCrLf & sErrorString

        MATCH_RemoveOldPayments = False
    End Function

    Public Function RemoveEmptyCollectionsInDB() As Boolean
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim bReturnValue, bx As Boolean
        Dim sMySQL As String = ""
        Dim sErrorString As String = ""

        On Error GoTo ERRMATCH_RemoveEmptyCollectionsInDB

        bReturnValue = True

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        '19.07.2016 - Changed the next SQL to adopt to SQL Server
        sMySQL = "DELETE FROM Batch WHERE NOT EXISTS (SELECT * FROM Payment P "
        sMySQL = sMySQL & "WHERE Batch.Batch_ID = P.Batch_ID AND Batch.BabelFile_ID = P.BabelFile_ID "
        sMySQL = sMySQL & "AND Batch.Company_ID = P.Company_ID) AND Batch.Company_ID = "
        sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))
        'Old code
        'sMySQL = "DELETE * FROM Batch B WHERE NOT EXISTS (SELECT * FROM Payment P "
        'sMySQL = sMySQL & "WHERE B.Batch_ID = P.Batch_ID AND B.BabelFile_ID = P.BabelFile_ID "
        'sMySQL = sMySQL & "AND B.Company_ID = P.Company_ID) AND B.Company_ID = "
        'sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))

        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommand.CommandType = CommandType.Text
        myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction

        myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans
        myBBDB_AccessCommand.CommandText = sMySQL

        sMATCH_ErrorText = LRS(14008, "Batch")

        myBBDB_AccessCommand.ExecuteNonQuery()

        '19.07.2016 - Changed the next SQL to adopt to SQL Server
        sMySQL = "DELETE FROM Babelfile WHERE NOT EXISTS (SELECT * FROM Batch B "
        sMySQL = sMySQL & "WHERE Babelfile.BabelFile_ID = B.BabelFile_ID AND Babelfile.Company_ID = B.Company_ID) "
        sMySQL = sMySQL & "AND Babelfile.Company_ID = "
        sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))

        'Old code
        'sMySQL = "DELETE * FROM Babelfile BF WHERE NOT EXISTS (SELECT * FROM Batch B "
        'sMySQL = sMySQL & "WHERE BF.BabelFile_ID = B.BabelFile_ID AND BF.Company_ID = B.Company_ID) "
        'sMySQL = sMySQL & "AND BF.Company_ID = "
        'sMySQL = sMySQL & Trim(Str(oProfile.Company_ID))

        myBBDB_AccessCommand.CommandText = sMySQL

        sMATCH_ErrorText = LRS(14008, "BabelFile")

        myBBDB_AccessCommand.ExecuteNonQuery()

        myBBDB_AccessTrans.Commit()

        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        RemoveEmptyCollectionsInDB = bReturnValue

        Exit Function

ERRMATCH_RemoveEmptyCollectionsInDB:
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

        sMATCH_ErrorText = sMATCH_ErrorText & vbCrLf & Err.Description

        RemoveEmptyCollectionsInDB = False
    End Function

    Public Function MATCH_RemoveAll() As Boolean
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim sMySQL As String
        Dim sErrorString As String
        Dim bReturnValue, bx As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim bHaugalandKraft As Boolean = False

        Try


            If Format(Now(), "yyyyMMdd") = "20191030" Then
                MsgBox("Kommet til rutinen MATCH_RemoveAll" & vbCrLf & System.Environment.StackTrace)
                'MsgBox("Kommet til rutinen MATCH_SaveData" & vbCrLf & s & System.Environment.StackTrace)
            End If

            bReturnValue = True

            If oProfile Is Nothing Then
                oProfile = New vbBabel.Profile
                bx = oProfile.Load(1)
            End If

            If myBBDB_AccessConnection Is Nothing Then
                ConnectToBBDatabse()
            Else
                If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                    ConnectToBBDatabse()
                End If
            End If

            bHaugalandKraft = False
            If Not mCol Is Nothing Then
                If mCol.Count() > 0 Then
                    For Each oBabelFile In mCol
                        If oBabelFile.Special = "HAUGALAND KRAFT" Then
                            bHaugalandKraft = True
                        End If
                    Next
                End If
            End If

            If bHaugalandKraft Then
                If Format(Now(), "yyyyMM") = "202203" Then
                    MsgBox("Kommet til ny spesialrutine for Haugaland Kraft.")
                    'MsgBox("Kommet til rutinen MATCH_SaveData" & vbCrLf & s & System.Environment.StackTrace)
                End If

                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

                sMySQL = "DELETE FROM [Freetext] WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Freetext")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Invoice WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Invoice")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Payment WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Payment")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Batch WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Batch")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Babelfile WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Babelfile")
                myBBDB_AccessCommand.ExecuteNonQuery()

            ElseIf EmptyString(sMATCH_LastValidBookDate) Then
                'Normal situation

                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction
                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

                sMySQL = "DELETE FROM [Freetext] WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Freetext")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Invoice WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Invoice")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Payment WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Payment")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Batch WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Batch")
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE FROM Babelfile WHERE Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Babelfile")
                myBBDB_AccessCommand.ExecuteNonQuery()

                myBBDB_AccessTrans.Commit()
            Else
                'Used by SISMO so far
                myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
                myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction
                myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

                sMySQL = "DELETE FROM Payment WHERE DATE_Payment <= '" & sMATCH_LastValidBookDate
                sMySQL = sMySQL & "' AND Company_ID = "
                sMySQL = sMySQL & oProfile.Company_ID.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                sMATCH_ErrorText = LRS(14008, "Payment, Invoice or freetext")
                myBBDB_AccessCommand.ExecuteNonQuery()

                myBBDB_AccessTrans.Commit()

                bReturnValue = RemoveEmptyCollectionsInDB()

            End If

        Catch ex As Exception
            sMATCH_ErrorText = sMATCH_ErrorText & vbCrLf & ex.Message


        Finally
            If Not myBBDB_AccessCommand Is Nothing Then
                myBBDB_AccessCommand.Dispose()
                myBBDB_AccessCommand = Nothing
            End If
            If Not myBBDB_AccessTrans Is Nothing Then
                myBBDB_AccessTrans.Dispose()
                myBBDB_AccessTrans = Nothing
            End If

        End Try


        MATCH_RemoveAll = bReturnValue

    End Function
    Public Function RemoveReportData() As Boolean
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim sMySQL As String
        Dim sErrorString As String
        Dim bReturnValue, bx As Boolean
        Dim iRecordsAffected As Integer

        bReturnValue = True

        If oProfile Is Nothing Then
            oProfile = New vbBabel.Profile
            bx = oProfile.Load(1)
        End If

        '10.12.2010 - Added next IF
        If myBBDB_AccessConnection Is Nothing Then
            ConnectToBBDatabse()
        Else
            If myBBDB_AccessConnection.State <> ConnectionState.Open Then
                ConnectToBBDatabse()
            End If
        End If

        sMySQL = "DELETE FROM Babelfile WHERE Company_ID = "
        If bSQLServer Then
            sMySQL = sMySQL & oProfile.Company_ID.ToString & " AND Reporting <> 0"
        Else
            sMySQL = sMySQL & oProfile.Company_ID.ToString & " AND Reporting = True"
        End If

        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommand.CommandType = CommandType.Text
        myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction
        myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

        myBBDB_AccessCommand.CommandText = sMySQL

        iRecordsAffected = myBBDB_AccessCommand.ExecuteNonQuery()

        RemoveReportData = bReturnValue

        myBBDB_AccessTrans.Commit()

        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If

    End Function
    Private Function GetSelectPartInBabelfile() As String
        Dim sReturnstring As String = ""

        sReturnstring = "SELECT BabelFile_ID, FilenameIn, File_id, IDENT_Sender, IDENT_Receiver, MON_TransferredAmount, MON_InvoiceAmount, "
        sReturnstring = sReturnstring & "AmountSetTransferred, AmountSetInvoice, No_Of_Transactions, No_Of_Records, DATE_Production, ImportFormat, "
        sReturnstring = sReturnstring & "RejectsExists, FilenameInNo, FileFromBank, Version, EDI_Format, EDI_MessageNo, FileSetup_ID, XLanguage, "
        sReturnstring = sReturnstring & "ERPSystem, Bank, StatusCode, Special"

        Return sReturnstring

    End Function
    Public Function InitiateLicense(ByVal sLicPathFilename As String) As Boolean

        SetEnvironment("BabelLicense", sLicPathFilename)

        Return True

    End Function
    Public Function ISO20022_SaveData(ByRef bMatchedItemsOnly As Boolean, ByRef bSaveOnlyThisPayment As Boolean, ByRef bAddToExistingPayments As Boolean, Optional ByRef bSaveOnlyNameAndAddress As Boolean = False, Optional ByRef bCreateUniqueOwnRef As Boolean = True, Optional ByVal bSaveToDatabase As Boolean = True) As Boolean
        Dim lCounter As Integer
        Dim bReturnValue As Boolean
        Dim oBabelFile As BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim bSaveThisBabelfile As Boolean
        Dim sMySQL As String
        Dim sMyUpdateMergedPaymentsSQL As String
        Dim lLastBabelFileID As Integer
        'Dim rsBabelFile As New ADODB.Recordset
        Dim bFileExportedMark As Boolean
        Dim sErrorString As String
        Dim bx As Boolean
        Dim sRef_Own As String

        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim iRecordsAffected As Integer
        Dim my_AccessConnection As System.Data.OleDb.OleDbConnection
        Dim oLocalPayment As vbBabel.Payment
        Dim sConnectionString As String = "" '18.07.2016 - Added the use of connectionstring

        Dim sMessage As String
        Dim sTemp1 As String
        Dim sTemp2 As String

        Try

            sConnectionString = FindBBConnectionString()
            bSQLServer = BB_UseSQLServer()
            my_AccessConnection = New System.Data.OleDb.OleDbConnection(sConnectionString)
            'my_AccessConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath())
            my_AccessConnection.Open()

            If Not bSaveToDatabase And bCreateUniqueOwnRef Then
                '13.06.2019 - Retrieve last used Sequence used to create a Unique ID for InstrID and EndToEndID where it is added
                sMySQL = "SELECT ISOSequenceCounter FROM Company WHERE Company_ID = 1"
                myBBDB_AccessCommand = my_AccessConnection.CreateCommand()
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessCommand.Connection = my_AccessConnection
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

                If myBBDB_AccessReader.HasRows Then
                    myBBDB_AccessReader.Read()
                    '09.11.2020 - The next line may create an error for customers using SQL Server
                    '   For a handful of customer the field ISOSequenceCounter was set as Type = DOUBLE
                    '   It should have been set as Float
                    'Just change the type through management studio.
                    lLastBabelFileID = myBBDB_AccessReader.GetDouble(0)
                Else
                    lLastBabelFileID = 0
                End If

                '08.11.2016
                ' Had an error when using SQLServer without the two lines below.
                ' Not tested for Access
                myBBDB_AccessCommand = Nothing
                myBBDB_AccessReader.Close()

                lCounter = lLastBabelFileID
                bFileExportedMark = True
            ElseIf bAddToExistingPayments Then
                sMySQL = "SELECT BabelFile_ID FROM BabelFile Where Company_ID = 1"
                sMySQL = sMySQL & " ORDER BY BabelFile_ID DESC"
                myBBDB_AccessCommand = my_AccessConnection.CreateCommand()
                myBBDB_AccessCommand.CommandType = CommandType.Text
                myBBDB_AccessCommand.Connection = my_AccessConnection
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

                If myBBDB_AccessReader.HasRows Then
                    myBBDB_AccessReader.Read()
                    lLastBabelFileID = myBBDB_AccessReader.GetInt32(0)
                Else
                    lLastBabelFileID = 0
                End If

                '08.11.2016
                ' Had an error when using SQLServer without the two lines below.
                ' Not tested for Access
                myBBDB_AccessCommand = Nothing
                myBBDB_AccessReader.Close()

                lCounter = lLastBabelFileID
                bFileExportedMark = False
            Else
                lCounter = 0
                bFileExportedMark = True
            End If

            bReturnValue = True

            myBBDB_AccessCommand = my_AccessConnection.CreateCommand
            myBBDB_AccessCommand.Connection = my_AccessConnection
            myBBDB_AccessCommand.CommandType = CommandType.Text
            myBBDB_AccessTrans = my_AccessConnection.BeginTransaction

            myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

            For Each oBabelFile In mCol
                'New 26.09.2008 - Don't save the babelfile if it has no batches
                If oBabelFile.Batches.Count > 0 Then
                    If bMATCH_MatchOCR Then 'If we shall match the OCR, save it all.
                        bSaveThisBabelfile = True
                    Else
                        bSaveThisBabelfile = False
                        For Each oBatch In oBabelFile.Batches
                            '19.07.2010
                            If oBatch.Payments.Count > 0 Then
                                If IsOCR(oBatch.Payments.Item(1).PayCode) Or oBatch.Payments.Item(1).PayCode = "180" Or IsAutogiro(oBatch.Payments.Item(1).PayCode) Then 'OCR or Autogiro
                                    'Assume all payments are eithe OCR or not
                                    bSaveThisBabelfile = False
                                Else
                                    bSaveThisBabelfile = True 'Not an OCR, save the babelfile
                                    'If we have at least 1 batch without KID-transactions, save the
                                    '  BabelFile. Which Batch to save is set in BabelFile.SaveData
                                    Exit For
                                End If
                            Else
                                bSaveThisBabelfile = False
                            End If
                        Next oBatch
                    End If
                Else
                    bSaveThisBabelfile = False
                End If

                ' 11.10.2016 -
                ' added new parameter when we like to create unique refs only
                If bSaveToDatabase = False Then
                    bSaveThisBabelfile = False
                End If

                If Not oBatch Is Nothing Then
                    oBatch = Nothing
                End If
                If oBabelFile.Index > lLastBabelFileID Then
                    lLastBabelFileID = oBabelFile.Index
                End If
                'If bSaveThisBabelfile Then  ' 11.10.2016 moved further down

                lCounter = lCounter + 1 'Should this be outside the if? 13.06.2019: KI - Yes it is perfect!
                ' 11.10.2016
                ' Moved next block to TreatISO20022 in babelBank.exe
                If bCreateUniqueOwnRef Then
                    'Create the unique Own_ref here, consisting of the Babelfile_ID, Batch_id +++
                    'The BabelFile_ID will be new for each file
                    For Each oBatch In oBabelFile.Batches
                        For Each oLocalPayment In oBatch.Payments
                            sRef_Own = lCounter.ToString & "-" & oBatch.Index.ToString & "-" & oLocalPayment.Index.ToString '& "-" & oLocalPayment.REF_Own
                            If sRef_Own.Length > 35 Then
                                sRef_Own = sRef_Own.Substring(0, 35)
                            End If
                            oLocalPayment.Ref_Own_BabelBankGenerated = sRef_Own.Trim
                        Next oLocalPayment
                    Next oBatch
                End If
                If bSaveThisBabelfile Then
                    
                    bReturnValue = oBabelFile.SaveData(bSQLServer, my_AccessConnection, myBBDB_AccessCommand, bMatchedItemsOnly, lCounter, bSaveOnlyThisPayment, bMATCH_MatchOCR, bFileExportedMark, bSaveOnlyNameAndAddress, bUseCommitAgainstBBDB, False)
                End If

            Next oBabelFile

            'Added 01.07.2019
            'Check if we are going to do any merging if so (often creditnotes, do it here and store the new ID's
            'We will do the merg here and then update the saved data. Kepp the original payments, but change the Ref_own on the merged payments.

            If MergePaymentsForISO20022(Me) Then
                'This function returns true if a merger has been done
                'The function above utilizes Special
                'The rest of the code is general for all mergers

                'Ok update the payments that are merged
                For Each oBabelFile In mCol
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.SpecialMark = True Then
                                If oPayment.Exported = True Then
                                    sMyUpdateMergedPaymentsSQL = "UPDATE Payment Set Ref_Own_BabelBankGenerated = '"
                                    sMyUpdateMergedPaymentsSQL = sMyUpdateMergedPaymentsSQL & oPayment.REF_Own & "' WHERE Ref_Own_BabelBankGenerated = '"
                                    sMyUpdateMergedPaymentsSQL = sMyUpdateMergedPaymentsSQL & oPayment.Ref_Own_BabelBankGenerated & "'"

                                    myBBDB_AccessCommand.CommandText = sMyUpdateMergedPaymentsSQL

                                    iRecordsAffected = myBBDB_AccessCommand.ExecuteNonQuery()
                                    If Not iRecordsAffected = 1 Then
                                        Throw New Exception(LRSCommon(30029, iRecordsAffected) & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                    Else
                                        'MsgBox "Babelfile saving OK"
                                    End If

                                End If
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile



                'The update is finished, now delete the merged payments
                If RemoveMergedPaymentsForISO20022(Me) Then

                End If
            End If

            '13.06.2019 - Save last used Sequence used to create a Unique ID for InstrID and EndToEndID where it is added
            If Not bSaveToDatabase Then
                If bCreateUniqueOwnRef Then
                    sMySQL = "UPDATE Company SET ISOSequenceCounter = " & lCounter.ToString & " WHERE Company_ID = 1"
                    myBBDB_AccessCommand.CommandText = sMySQL

                    If Not myBBDB_AccessCommand.ExecuteNonQuery() = 1 Then
                        Err.Raise(14000, "Babelfile", sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                    Else
                        'MsgBox "Babelfile saving OK"
                    End If
                End If
            End If

            myBBDB_AccessTrans.Commit()

        Catch exx As vbBabel.Payment.PaymentException
            Throw New vbBabel.Payment.PaymentException(exx.Message, exx.InnerException, Nothing, exx.LastLineRead, exx.FilenameImported) ' - Testing Try ... Catch

        Catch ex As Exception

            If Not mCol Is Nothing Then
                mCol = Nothing
            End If
            sMATCH_ErrorText = LRS(14001, LRS(14009)) & vbCrLf & vbCrLf '"An error occured during saving data." & vbCrLf & vbCrLf
            If Err.Number = 999 Then
                'Err is raised in BabelFile, Batch, Payment, Invoice or Freetext
                sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & Err.Source & vbCrLf '14002 = "Source: "
            Else
                sMATCH_ErrorText = sMATCH_ErrorText & LRS(14002) & "BabelFiles" & vbCrLf '14002 = "Source: "
            End If

            sMATCH_ErrorText = sMATCH_ErrorText & LRS(14003) & ex.Message '14003 = "Description: "

            Throw New vbBabel.Payment.PaymentException("Method: BabelFiles.ISO20022_SaveData" & vbCrLf & ex.Message, ex, oPayment, "", "")

        Finally

            If Not myBBDB_AccessReader Is Nothing Then
                If Not myBBDB_AccessReader.IsClosed Then
                    myBBDB_AccessReader.Close()
                End If
                myBBDB_AccessReader = Nothing
            End If
            If Not myBBDB_AccessCommand Is Nothing Then
                myBBDB_AccessCommand.Dispose()
                myBBDB_AccessCommand = Nothing
            End If
            If Not myBBDB_AccessTrans Is Nothing Then
                myBBDB_AccessTrans.Dispose()
                myBBDB_AccessTrans = Nothing
            End If

            ' Close this "local" connection
            If my_AccessConnection.State <> ConnectionState.Closed Then
                my_AccessConnection.Close()
            End If
            my_AccessConnection.Dispose()
            my_AccessConnection = Nothing

        End Try

        ISO20022_SaveData = bReturnValue


    End Function
End Class
