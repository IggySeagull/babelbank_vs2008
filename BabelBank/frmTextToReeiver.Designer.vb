﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTextToReeiver
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblHeading = New System.Windows.Forms.Label
        Me.optSalary = New System.Windows.Forms.RadioButton
        Me.optTravel = New System.Windows.Forms.RadioButton
        Me.optFreetext = New System.Windows.Forms.RadioButton
        Me.txtFreetext = New System.Windows.Forms.TextBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblHeading
        '
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.Location = New System.Drawing.Point(12, 9)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.Size = New System.Drawing.Size(440, 38)
        Me.lblHeading.TabIndex = 0
        Me.lblHeading.Text = "Text to receiver"
        '
        'optSalary
        '
        Me.optSalary.AutoSize = True
        Me.optSalary.Location = New System.Drawing.Point(12, 50)
        Me.optSalary.Name = "optSalary"
        Me.optSalary.Size = New System.Drawing.Size(97, 17)
        Me.optSalary.TabIndex = 1
        Me.optSalary.TabStop = True
        Me.optSalary.Text = "Salary payment"
        Me.optSalary.UseVisualStyleBackColor = True
        '
        'optTravel
        '
        Me.optTravel.AutoSize = True
        Me.optTravel.Location = New System.Drawing.Point(12, 73)
        Me.optTravel.Name = "optTravel"
        Me.optTravel.Size = New System.Drawing.Size(104, 17)
        Me.optTravel.TabIndex = 2
        Me.optTravel.TabStop = True
        Me.optTravel.Text = "Travel Expenses"
        Me.optTravel.UseVisualStyleBackColor = True
        '
        'optFreetext
        '
        Me.optFreetext.AutoSize = True
        Me.optFreetext.Location = New System.Drawing.Point(12, 96)
        Me.optFreetext.Name = "optFreetext"
        Me.optFreetext.Size = New System.Drawing.Size(63, 17)
        Me.optFreetext.TabIndex = 3
        Me.optFreetext.TabStop = True
        Me.optFreetext.Text = "Freetext"
        Me.optFreetext.UseVisualStyleBackColor = True
        '
        'txtFreetext
        '
        Me.txtFreetext.Location = New System.Drawing.Point(138, 93)
        Me.txtFreetext.Name = "txtFreetext"
        Me.txtFreetext.Size = New System.Drawing.Size(302, 20)
        Me.txtFreetext.TabIndex = 4
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(290, 139)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 24)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(368, 139)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(72, 24)
        Me.cmdOK.TabIndex = 7
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'frmTextToReeiver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(464, 170)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtFreetext)
        Me.Controls.Add(Me.optFreetext)
        Me.Controls.Add(Me.optTravel)
        Me.Controls.Add(Me.optSalary)
        Me.Controls.Add(Me.lblHeading)
        Me.Name = "frmTextToReeiver"
        Me.Text = "Text to receiver"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHeading As System.Windows.Forms.Label
    Friend WithEvents optSalary As System.Windows.Forms.RadioButton
    Friend WithEvents optTravel As System.Windows.Forms.RadioButton
    Friend WithEvents optFreetext As System.Windows.Forms.RadioButton
    Friend WithEvents txtFreetext As System.Windows.Forms.TextBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
End Class
