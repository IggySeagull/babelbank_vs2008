Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_003_PaymentInvoice
    'Used for rp_003_PaymentInvoice
    'Used for rp_011_Rejected

    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim sClientName As String

    Dim nTotalAmount As Double
    Dim sOrderBy As String
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim bShowFilename As Boolean

    Dim oBabelFiles As vbBabel.BabelFiles
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim oFreeText As vbBabel.Freetext
    Dim bFirstTime As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean = False
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    'Specialstuff
    Dim iPatentStyretTeller As Integer
    Private Sub rp_003_PaymentInvoice_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Fields.Add("BreakField")

        If Not bReportFromDatabase Then
            Fields.Add("Grouping")
            Fields.Add("Company_ID")
            Fields.Add("Babelfile_ID")
            Fields.Add("Batch_ID")
            Fields.Add("Payment_ID")
            Fields.Add("Invoice_ID")
            Fields.Add("Filename")
            Fields.Add("StatementAmount")
            Fields.Add("Client")
            Fields.Add("DATE_Production")
            Fields.Add("BatchRef")
            Fields.Add("I_Account")
            'Fields.Add("Paycode")
            Fields.Add("MON_InvoiceAmount")
            Fields.Add("MON_InvoiceCurrency")
            Fields.Add("MON_TransferredAmount")
            Fields.Add("MON_TransferCurrency")
            Fields.Add("MON_OriginallyPaidAmount")
            Fields.Add("MON_OriginallyPaidCurrency")
            Fields.Add("MON_AccountAmount")
            Fields.Add("MON_AccountCurrency")
            Fields.Add("MON_LocalExchRate")
            Fields.Add("ChargesAbroad")
            Fields.Add("ChargesDomestic")
            Fields.Add("StatusCode")
            Fields.Add("StatusText")
            Fields.Add("E_Name")
            Fields.Add("E_Adr1")
            Fields.Add("E_Adr2")
            Fields.Add("E_Adr3")
            Fields.Add("E_Zip")
            Fields.Add("E_City")
            Fields.Add("E_Account")
            Fields.Add("E_Name")
            Fields.Add("E_CountryCode")
            Fields.Add("REF_Bank1")
            Fields.Add("Unique_ID")
            Fields.Add("I_Name")
            Fields.Add("I_Adr1")
            Fields.Add("I_Adr2")
            Fields.Add("I_Adr3")
            Fields.Add("I_Zip")
            Fields.Add("I_City")
            Fields.Add("REF_Bank1")
            Fields.Add("REF_Bank2")
            Fields.Add("REF_Own")
            Fields.Add("DATE_Value")
            Fields.Add("DATE_Payment")
            Fields.Add("PayCode")
            Fields.Add("PayType")
            Fields.Add("BANK_SWIFTCode")
            Fields.Add("BANK_BranchNo")
            Fields.Add("BANK_BranchType")
            Fields.Add("VoucherNo")
            Fields.Add("ExtraD")
            Fields.Add("Extra1")
            Fields.Add("MATCH_UseOriginalAmountInMatching")
            Fields.Add("ImportFormat")
            Fields.Add("MyField")
            Fields.Add("Unique_ID")
            Fields.Add("InvoiceStatusCode")
            Fields.Add("InvoiceNumber")
            Fields.Add("CustomerNumber")
            Fields.Add("InvoiceAmount")
            Fields.Add("Bank")
            Fields.Add("BabelBank_ID")
        End If

        'Fields.Add("PatentStyretTeller")

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtDATE_Value.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtDATE_Value.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

    End Sub

    Private Sub rp_003_PaymentInvoice_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If
    End Sub

    Private Sub rp_003_PaymentInvoice_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        bFirstTime = True
        bShowBatchfooterTotals = True

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False
                bShowFilename = False

            Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = True

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

        End Select

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtDATE_Value.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtDATE_Value.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If sReportNumber = "3" Then
            If EmptyString(sReportName) Then
                Me.lblrptHeader.Text = LRS(40034)   ' Payments with invoicedetails - "Betalinger med detaljer" 
            Else
                Me.lblrptHeader.Text = sReportName
            End If
        Else
            '011
            If EmptyString(sReportName) Then
                Me.lblrptHeader.Text = LRS(40095)   ' Rejected payments - "Rejected payments" 
            Else
                Me.lblrptHeader.Text = sReportName
            End If
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        'Set the caption of the labels

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 

        If iBreakLevel = 1 Then
            Me.lblDate_Production.Text = LRS(40044)  'bokf�ringsdato
        Else
            Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        End If
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40138) '"Own account:" 'LRS(40020) '"Mottakerkonto:" 

        'grPaymentReceiverHeader
        Me.lblReceiver.Text = LRS(48011) '"Mottaker:" 
        Me.lblPayor.Text = LRS(40108) '"Betaler:" 

        'grPaymentHeader
        Me.lblPayI_Account.Text = LRS(40138) '"Own account:"
        Me.lblE_Account.Text = LRS(40139) '"External account:"
        'Me.lblE_Account.Text = LRS(40013) '"Kontonr.:" 
        Me.lblREF_Bank1.Text = LRS(40011) '"Arkivref.:" 
        Me.lblREF_Bank2.Text = LRS(40012) '"Blankettref.:" 
        Me.lblREF_Own.Text = LRS(60072) & ":" '"Egenreferanse:" 
        Me.lblDATE_Value.Text = LRS(40003) '"Val.dato:" '
        Me.lblBIC.Text = "BIC:"
        Me.lblBANK_Branch.Text = LRS(40109) '"Branch number:" 
        Me.lblVoucherNo.Text = LRS(40068) '"Bilagsnummer:" 

        'grPaymentInternationalHeader
        Me.lblMON_OriginallyPaidAmount.Text = LRS(40037) '"Opprinnelig bel�p:" 
        Me.lblMON_InvoiceAmount.Text = LRS(40036) '"Postert bel�p:" 
        Me.lblMON_AccountAmount.Text = LRS(40038) '"Kontobel�p:" 
        Me.lblExchangeRate.Text = LRS(40035) '"Kurs:" 
        Me.lblChargesAbroad.Text = LRS(40043) '"Avsenderbankens omkostninger:" 
        Me.lblChargesDomestic.Text = LRS(40042) '"Mottakerbankens omkostninger:" 

        'Detail
        'Me.lblConcerns.Text = LRS(40010) '"Bel�pet gjelder:" 

        'grPaymentFooter

        'grBatchFooter
        Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

        ' Special cases;
        '*******VB6 Code

        ''Me.lblFileSum = LRS(40028)

        sOldI_Account = ""
        sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 

        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If
        If Not bShowFilename Then
            Me.txtFilename.Visible = False
        End If

    End Sub
    Private Sub grPaymentHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grPaymentHeader.Format
        Dim pLocation As System.Drawing.PointF

        If EmptyString(Me.txtE_City.Text) Then
            pLocation.X = 0
            pLocation.Y = 0.45
            Me.txtE_Adr3.Location = pLocation
            ' added 15.03.2019
            If sReportNumber <> "11" Then
                Me.txtE_Adr3.Visible = True
            End If
            Me.txtE_Zip.Visible = False
            Me.txtE_City.Visible = False
        Else
            Me.txtE_Adr3.Visible = False
            Me.txtE_Zip.Visible = True
            Me.txtE_City.Visible = True
        End If

    End Sub
    Private Sub rp_003_PaymentInvoice_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String
        Dim sDATE_Value As String
        Dim sDate_Payment As String

        Dim iTopAdjustment As Integer
        Dim sTemp As String
        Dim sErrorDetailsText As String
        Dim bContinue As Boolean
        Dim bContinue2 As Boolean
        Dim bExportPayment As Boolean

        ' Counters for items in the different Babel collections
        Static lBabelFiles As Long
        Static lLastUsedBabelFilesItem As Long
        Static lBatches As Long
        Static lPayments As Long
        Static lInvoices As Long
        Static lFreetexts As Long
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String
        '05.04.2016 - Added next 4 variables
        Static bBabelFileIsRejected As Boolean
        Static bBatchIsRejected As Boolean
        Static bPaymentIsRejected As Boolean
        Dim bPrint As Boolean
        Static bReport11_AlreadyDisplayed As Boolean = False
        Static bAllPaymentsACTC As Boolean = True

        iTopAdjustment = 0
        sTemp = ""

        If Not bReportFromDatabase Then

            If bFirstTime Then
                bFirstTime = False
                lBabelFiles = 1
                lBatches = 1
                lPayments = 1
                lInvoices = 1
                lFreetexts = 0
                xCreditAccount = "xxxx"
                xDate = "x"
            End If

            lFreetexts = lFreetexts + 1

            ' Spin through collections to find next suitbable record before any other
            ' proecessing is done
            ' Position to correct items in collections
            Do Until lBabelFiles > oBabelFiles.Count
                ' Try to set as few times as possible
                If oBabelFile Is Nothing Then
                    oBabelFile = oBabelFiles(lBabelFiles)
                    lLastUsedBabelFilesItem = lBabelFiles
                Else
                    'If lBabelFiles <> oBabelFile.Index Then
                    If lLastUsedBabelFilesItem <> lBabelFiles Then
                        lBatches = 1
                        lPayments = 1
                        lInvoices = 1
                        ' Added 19.05.2004
                        xCreditAccount = "xxxx"
                        xDate = "x"

                        oBabelFile = oBabelFiles(lBabelFiles)
                        lLastUsedBabelFilesItem = lBabelFiles  'oBabelFile.Index
                        '05.04.2016 - Added
                        bBabelFileIsRejected = False
                        If IsNumeric(oBabelFile.StatusCode) Then
                            If CLng(oBabelFile.StatusCode) > 2 Then
                                bBabelFileIsRejected = True
                            End If
                        End If
                    End If
                End If

                Do Until lBatches > oBabelFile.Batches.Count
                    oBatch = oBabelFile.Batches(lBatches)
                    '05.04.2016 - Added
                    bBatchIsRejected = False
                    If IsNumeric(oBatch.StatusCode) Then
                        If CLng(oBatch.StatusCode) > 2 Then
                            bBatchIsRejected = True
                        End If
                    End If

                    ' Find the next payment in this batch which is original
                    Do Until lPayments > oBatch.Payments.Count

                        oPayment = oBatch.Payments.Item(lPayments)

                        bExportPayment = False

                        'Sometimes there are restrictions on which payments to export
                        If bIncludeOCR Then
                            bExportPayment = True
                        Else
                            If IsOCR(oPayment.PayCode) Then
                                bExportPayment = False
                            Else
                                bExportPayment = True
                            End If
                        End If

                        '05.04.2016 - Added
                        If sReportNumber = "11" And bExportPayment Then
                            bExportPayment = False
                            If bBabelFileIsRejected Then
                                bExportPayment = True
                            ElseIf bBatchIsRejected Then
                                bExportPayment = True
                            Else
                                bPaymentIsRejected = False
                                If IsNumeric(oPayment.StatusCode) Then
                                    If CLng(oPayment.StatusCode) > 2 Then
                                        bExportPayment = True
                                        bPaymentIsRejected = True
                                    End If
                                Else
                                    '18.11.2016 - Added next IF to treat ISO20022 errorcodes
                                    If Not EmptyString(oPayment.StatusCode) Then
                                        If IsISO20022ReturnFile(oPayment.ImportFormat) Then
                                            bExportPayment = True
                                            bPaymentIsRejected = True
                                        End If
                                    End If
                                End If
                                If Not bExportPayment Then
                                    For Each oInvoice In oPayment.Invoices
                                        If IsNumeric(oInvoice.StatusCode) Then
                                            If CLng(oInvoice.StatusCode) > 2 Then
                                                bExportPayment = True
                                                Exit For
                                            End If
                                        End If
                                    Next oInvoice
                                End If
                            End If

                        End If

                        If bReportOnSelectedItems And bExportPayment Then
                            If oPayment.StatusCode = "-1" Or oPayment.ToSpecialReport Then
                                bExportPayment = True
                            Else
                                bExportPayment = False
                            End If
                        End If

                        ' 17.12.2014 not to report Omitted (removed) payments
                        If bReportOnSelectedItems = False Then
                            If oPayment.StatusCode = "-1" Then
                                bExportPayment = False
                            End If
                        End If

                        ' added for Salmars Feilinnbetalingskonto
                        If sSpecial = "SALMAR_FEIL" Then
                            If oPayment.StatusCode = "-1" Then
                                bExportPayment = False
                            End If
                        End If

                        '26.06.2018 - Commented next IF, and added a new one
                        'If bExportPayment Then
                        '    If Not EmptyString(sClientNumber) Then
                        '        If oPayment.VB_ClientNo = sClientNumber Then
                        '            bExportPayment = True
                        '        Else
                        '            bExportPayment = False 'Added 29.03.2017 If bReportOnSelectedItems = True we set bExportPayment = True 20 lines above, 
                        '            '   this was prior not reset to False if it was the wrong client 
                        '        End If
                        '    Else
                        '        bExportPayment = True
                        '    End If
                        'End If

                        If Not EmptyString(sClientNumber) Then
                            If Not oPayment.VB_ClientNo.Trim = sClientNumber Then
                                bExportPayment = False
                            End If
                        End If

                        If bExportPayment Then

                            ' Postiton to a, final invoice
                            ' Find the next invoice in this payment which is original;
                            Do Until lInvoices > oPayment.Invoices.Count
                                oInvoice = oPayment.Invoices.Item(lInvoices)


                                'If oInvoice.MATCH_Original Then
                                '    If lFreetexts <= oInvoice.Freetexts.Count Then
                                '        bContinue = True
                                '        oFreeText = oInvoice.Freetexts.Item(lFreetexts)
                                '        'lFreetexts = lFreetexts + 1
                                '        Exit Do
                                '    End If
                                '    If oInvoice.Freetexts.Count = 0 And lFreetexts = 1 Then
                                '        bContinue = True
                                '        Exit Do
                                '    End If

                                'End If

                                bContinue = True
                                lInvoices = lInvoices + 1
                                Exit Do
                                'lFreetexts = 1
                            Loop 'lInvoices
                        Else
                            bContinue = False
                            'lPayments = lPayments + 1
                            'lInvoices = 1
                        End If 'If frmViewer.CorrectReportClient(oPayment) Then

                        If bContinue Then
                            Exit Do
                        End If

                        lPayments = lPayments + 1
                        lInvoices = 1

                    Loop 'lPayments

                    If bContinue Then
                        Exit Do
                    End If
                    lBatches = lBatches + 1
                    lPayments = 1
                Loop ' lBatches
                If bContinue Then
                    Exit Do
                End If
                lBabelFiles = lBabelFiles + 1
                lBatches = 1
            Loop ' lBabelFiles

            If lBabelFiles > oBabelFiles.Count Then
                eArgs.EOF = True
                lBabelFiles = 0 ' to reset statics next time !

                ' 27.12.2018 - added next If, to be able to report a ACTC (OK) Pain.002 - to tell that it is OK
                If sReportNumber = "11" And bExportPayment = False And Not bReport11_AlreadyDisplayed And bAllPaymentsACTC Then
                    ' Position to first (and only?) payment
                    'lBabelFiles = 1
                    lBabelFiles = oBabelFiles.Count
                    lBatches = 1
                    lPayments = 1
                    lInvoices = 1
                    bReport11_AlreadyDisplayed = True
                    bAllPaymentsACTC = True

                Else
                    Exit Sub
                End If
            End If

            If oBabelFile.VB_ProfileInUse Then
                Me.Fields("Grouping").Value = Trim(Str(oPayment.VB_Profile.Company_ID)) & "-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                Me.Fields("Company_ID").Value = oPayment.VB_Profile.Company_ID
            Else
                Me.Fields("Grouping").Value = "1-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                Me.Fields("Company_ID").Value = "1"
            End If
            Me.Fields("Babelfile_ID").Value = oBabelFile.Index
            Me.Fields("Batch_ID").Value = oBatch.Index
            Me.Fields("Payment_ID").Value = oPayment.Index
            Me.Fields("Invoice_ID").Value = oInvoice.Index
            Me.Fields("Filename").Value = Right(oBabelFile.FilenameIn, 20)
            Me.Fields("StatementAmount").Value = oBatch.MON_InvoiceAmount
            Me.Fields("Client").Value = oPayment.VB_Client

            ' 10.02.2016 Changed to Date_Payment for Break on Account+Date
            If iBreakLevel = 1 Then
                Me.Fields("DATE_Production").Value = oPayment.DATE_Payment
            Else
                Me.Fields("DATE_Production").Value = oBabelFile.DATE_Production
            End If

            Me.Fields("BatchRef").Value = oBatch.REF_Bank
            Me.Fields("I_Account").Value = oPayment.I_Account
            'Me.Fields("Paycode").Value = oPayment.PayCode
            Me.Fields("MON_InvoiceAmount").Value = oPayment.MON_InvoiceAmount / 100 '10.02.2016
            Me.Fields("MON_InvoiceCurrency").Value = oPayment.MON_InvoiceCurrency
            If oPayment.MON_TransferredAmount = 0 Then
                Me.Fields("MON_TransferredAmount").Value = oPayment.MON_InvoiceAmount / 100
                Me.Fields("MON_TransferCurrency").Value = oPayment.MON_InvoiceCurrency
            Else
                Me.Fields("MON_TransferredAmount").Value = oPayment.MON_TransferredAmount / 100
                Me.Fields("MON_TransferCurrency").Value = oPayment.MON_TransferCurrency
            End If
            'me.txtBatchfooterAmount
            Me.Fields("MON_OriginallyPaidAmount").Value = oPayment.MON_OriginallyPaidAmount / 100
            Me.Fields("MON_OriginallyPaidCurrency").Value = oPayment.MON_OriginallyPaidCurrency
            Me.Fields("MON_AccountAmount").Value = oPayment.MON_AccountAmount / 100
            Me.Fields("MON_AccountCurrency").Value = oPayment.MON_AccountCurrency
            Me.Fields("MON_LocalExchRate").Value = oPayment.MON_LocalExchRate
            If oPayment.MON_AccountAmount = 0 Then
                Me.Fields("ChargesAbroad").Value = 0
            Else
                'Added 28.05.2015 - because outging payments like Telepay has Origanlamount set to 0
                If oPayment.MON_OriginallyPaidAmount > 0 Then
                    '24.09.2015 Changed, use of new field. May be problems in different cases, but try to use this variable
                    'Solution changed because of Norwegian Hull Club
                    Me.Fields("ChargesAbroad").Value = oPayment.MON_ChargesAmountAbroad / 100
                    'Me.Fields("ChargesAbroad").Value = (oPayment.MON_OriginallyPaidAmount - oPayment.MON_AccountAmount) / 100
                Else
                    Me.Fields("ChargesAbroad").Value = 0
                End If
            End If
            Me.Fields("ChargesDomestic").Value = oPayment.MON_ChargesAmount / 100
            Me.Fields("StatusCode").Value = oPayment.StatusCode
            Me.Fields("StatusText").Value = oPayment.StatusText
            Me.Fields("E_Name").Value = oPayment.E_Name
            Me.Fields("E_Adr1").Value = oPayment.E_Adr1
            Me.Fields("E_Adr2").Value = oPayment.E_Adr2
            Me.Fields("E_Adr3").Value = oPayment.E_Adr3
            Me.Fields("E_Zip").Value = oPayment.E_Zip
            Me.Fields("E_City").Value = oPayment.E_City
            Me.Fields("E_Account").Value = oPayment.E_Account
            'Me.Fields("E_Name").Value = oPayment.E_Name
            Me.Fields("E_CountryCode").Value = oPayment.E_CountryCode
            Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
            Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
            Me.Fields("I_Name").Value = oPayment.I_Name
            Me.Fields("I_Adr1").Value = oPayment.I_Adr1
            Me.Fields("I_Adr2").Value = oPayment.I_Adr2
            Me.Fields("I_Adr3").Value = oPayment.I_Adr3
            Me.Fields("I_Zip").Value = oPayment.I_Zip
            Me.Fields("I_City").Value = oPayment.I_City
            Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
            Me.Fields("REF_Bank2").Value = oPayment.REF_Bank2
            Me.Fields("REF_Own").Value = oPayment.REF_Own
            Me.Fields("DATE_Value").Value = oPayment.DATE_Value
            Me.Fields("DATE_Payment").Value = oPayment.DATE_Payment
            Me.Fields("PayCode").Value = oPayment.PayCode
            Me.Fields("PayType").Value = oPayment.PayType

            Me.Fields("BANK_SWIFTCode").Value = oPayment.BANK_SWIFTCode
            Me.Fields("BANK_BranchNo").Value = oPayment.BANK_BranchNo
            Me.Fields("BANK_BranchType").Value = oPayment.BANK_BranchType
            Me.Fields("VoucherNo").Value = oPayment.VoucherNo
            Me.Fields("ExtraD").Value = oPayment.ExtraD1
            Me.Fields("Extra1").Value = oInvoice.Extra1
            Me.Fields("MATCH_UseOriginalAmountInMatching").Value = oPayment.MATCH_UseoriginalAmountInMatching
            Me.Fields("ImportFormat").Value = oPayment.ImportFormat
            Me.Fields("MyField").Value = oInvoice.MyField
            Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
            Me.Fields("InvoiceStatusCode").Value = oInvoice.StatusCode
            Me.Fields("InvoiceNumber").Value = oInvoice.InvoiceNo
            Me.Fields("CustomerNumber").Value = oInvoice.CustomerNo
            Me.Fields("InvoiceAmount").Value = oInvoice.MON_InvoiceAmount / 100

            Me.Fields("Bank").Value = oBabelFile.BankByCode

            eArgs.EOF = False

        Else
            'Fix illegal characters on several fields
            Me.Fields("REF_Own").Value = RetrieveIllegalCharactersInString(Me.Fields("REF_Own").Value)
            Me.Fields("REF_Bank1").Value = RetrieveIllegalCharactersInString(Me.Fields("REF_Bank1").Value)
            Me.Fields("REF_Bank2").Value = RetrieveIllegalCharactersInString(Me.Fields("REF_Bank2").Value)
            Me.Fields("E_Name").Value = RetrieveIllegalCharactersInString(Me.Fields("E_Name").Value)
            Me.Fields("E_Adr1").Value = RetrieveIllegalCharactersInString(Me.Fields("E_Adr1").Value)
            Me.Fields("E_Adr2").Value = RetrieveIllegalCharactersInString(Me.Fields("E_Adr2").Value)
            Me.Fields("E_Adr3").Value = RetrieveIllegalCharactersInString(Me.Fields("E_Adr3").Value)
            'Me.Fields("E_OrgNo").Value = RetrieveIllegalCharactersInString(Me.Fields("E_OrgNo").Value)
            Me.Fields("E_City").Value = RetrieveIllegalCharactersInString(Me.Fields("E_City").Value)
            'Me.Fields("Text_E_Statement").Value = RetrieveIllegalCharactersInString(Me.Fields("Text_E_Statement").Value)
            'Me.Fields("Text_I_Statement").Value = RetrieveIllegalCharactersInString(Me.Fields("Text_I_Statement").Value)
            Me.Fields("I_Name").Value = RetrieveIllegalCharactersInString(Me.Fields("I_Name").Value)
            Me.Fields("I_Adr1").Value = RetrieveIllegalCharactersInString(Me.Fields("I_Adr1").Value)
            Me.Fields("I_Adr2").Value = RetrieveIllegalCharactersInString(Me.Fields("I_Adr2").Value)
            Me.Fields("I_Adr3").Value = RetrieveIllegalCharactersInString(Me.Fields("I_Adr3").Value)
            'Me.Fields("BANK_Name").Value = RetrieveIllegalCharactersInString(Me.Fields("BANK_Name").Value)
            'Me.Fields("BANK_Adr1").Value = RetrieveIllegalCharactersInString(Me.Fields("BANK_Adr1").Value)
            'Me.Fields("BANK_Adr2").Value = RetrieveIllegalCharactersInString(Me.Fields("BANK_Adr2").Value)
            'Me.Fields("BANK_Adr3").Value = RetrieveIllegalCharactersInString(Me.Fields("BANK_Adr3").Value)

        End If

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                    Me.Fields("BreakField").Value = Me.Fields("BabelFile_ID").Value.ToString

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL
                    'Me.Fields("BreakField").Value = Me.Fields("BreakField").Value

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            If sSpecial = "PATENTSTYRET_DOUBLE" Then

                iPatentStyretTeller = Me.Fields("PatentStyretTeller").Value
            End If

            'Store the keys for usage in the subreport(s)
            iCompany_ID = Me.Fields("Company_ID").Value
            iBabelfile_ID = Me.Fields("Babelfile_ID").Value
            iBatch_ID = Me.Fields("Batch_ID").Value
            iPayment_ID = Me.Fields("Payment_ID").Value
            iInvoice_ID = Me.Fields("Invoice_ID").Value
            'START WORKING HERE

            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            If EmptyString(Me.Fields("BANK_SWIFTCode").Value) Then
                Me.lblBIC.Visible = False
                Me.txtBIC.Visible = False
                iTopAdjustment = iTopAdjustment + 0.15
            End If

            If EmptyString(Me.Fields("BANK_BranchNo").Value) Then
                Me.lblBANK_Branch.Visible = False
                Me.txtBANK_Branch.Visible = False
                iTopAdjustment = iTopAdjustment + 0.15
            Else
                Me.lblBANK_Branch.Visible = True
                Me.txtBANK_Branch.Visible = True
                sTemp = Me.Fields("BANK_BranchType").Value
                Select Case sTemp
                    Case "0", "1" 'BankBranchType.NoBranchType, BankBranchType.SWIFT
                    Case "2" 'BankBranchType.Fedwire
                        Me.txtBANK_Branch.Value = "Fedwire: " & Me.Fields("BANK_BranchNo").Value
                    Case "3" 'BankBranchType.SortCode
                        Me.txtBANK_Branch.Value = "Sort Code: " & Me.Fields("BANK_BranchNo").Value
                    Case "4" 'BankBranchType.Bankleitzahl
                        Me.txtBANK_Branch.Value = "Bankleitzahl: " & Me.Fields("BANK_BranchNo").Value
                    Case "5" 'BankBranchType.Chips
                        Me.txtBANK_Branch.Value = "Chips UID No: " & Me.Fields("BANK_BranchNo").Value
                    Case "6" 'BankBranchType.US_ABA
                        Me.txtBANK_Branch.Value = "ABA: " & Me.Fields("BANK_BranchNo").Value
                    Case "7" 'BankBranchType.Bankleitzahl_Austria
                        Me.txtBANK_Branch.Value = "Bankleitzahl: " & Me.Fields("BANK_BranchNo").Value
                    Case "8" 'BankBranchType.Meps
                        Me.txtBANK_Branch.Value = "MEPS: " & Me.Fields("BANK_BranchNo").Value
                    Case "9" 'BankBranchType.BSB
                        Me.txtBANK_Branch.Value = "Bank-State-Branch : " & Me.Fields("BANK_BranchNo").Value
                    Case "10" 'BankBranchType.CC
                        Me.txtBANK_Branch.Value = "Transit Code: " & Me.Fields("BANK_BranchNo").Value

                End Select
                sTemp = ""
            End If

            'Maybe we will show the VoucherNo in the future, but until then
            'If so remeber to change the pLocationY with the variable iTopAdjustment
            Me.lblVoucherNo.Visible = False
            Me.txtVoucherNo.Visible = False

            Me.grPaymentInternationalHeader.Visible = False
            If Me.Fields("Paytype").Value = "I" Then
                If Me.Fields("Statuscode").Value = "02" Then
                    Me.grPaymentInternationalHeader.Visible = True
                End If
            End If

            'If Val(Me.Fields("StatusCode").Value) > 2 Or Me.Fields("StatusCode").Value = "ZZZ" Then
            If Me.Fields("StatusCode").Value <> "00" And Me.Fields("StatusCode").Value <> "01" And Me.Fields("StatusCode").Value <> "02" Then
                ' Show errorno and text
                Me.txtErrorText.Visible = True
                If Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.Leverantorsbetalningar Then
                    Me.txtErrorText.Value = Me.Fields("StatusCode").Value & " - " & BankgirotStatusTxts(Me.Fields("StatusCode").Value)
                ElseIf Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.BANSTA Then
                    sErrorDetailsText = ""
                    If Not EmptyString(Me.Fields("StatusText").Value) Then
                        sErrorDetailsText = vbCrLf & Trim$(Me.Fields("StatusText").Value)
                    End If
                    Me.txtErrorText.Value = Me.Fields("StatusCode").Value & " " & BANSTAStatusTxts(Me.Fields("StatusCode").Value, Me.Fields("Bank").Value) & sErrorDetailsText
                    '08.06.2015 - Added next 2 ElseIfs
                ElseIf Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.Pain002 Or Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.Pain002_Rejected Then
                    Me.txtErrorText.Value = Me.Fields("StatusCode").Value & " " & Me.Fields("StatusText").Value
                ElseIf Me.Fields("ImportFormat").Value > vbBabel.BabelFiles.FileType.Camt053 Or Me.Fields("ImportFormat").Value > vbBabel.BabelFiles.FileType.Camt053_Outgoing Or Me.Fields("ImportFormat").Value > vbBabel.BabelFiles.FileType.Camt054 Or Me.Fields("ImportFormat").Value > vbBabel.BabelFiles.FileType.Camt054_Outgoing Then
                    Me.txtErrorText.Value = Me.Fields("StatusCode").Value & " " & Me.Fields("StatusText").Value
                Else
                    Me.txtErrorText.Value = Me.Fields("StatusCode").Value & " - " & TelepayStatusTxts(Me.Fields("StatusCode").Value)
                End If
                ' added 24.05.2019
                bAllPaymentsACTC = False
            Else
                ' Errorcode may be set on invoice. Show the first one
                If Val(Me.Fields("InvoiceStatusCode").Value) > 2 Then
                    Me.txtErrorText.Visible = False
                    bAllPaymentsACTC = False
                    If Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.Leverantorsbetalningar Then
                        Me.txtErrorText.Value = Me.Fields("InvoiceStatusCode").Value & " - " & BankgirotStatusTxts(Me.Fields("InvoiceStatusCode").Value)
                    Else
                        Me.txtErrorText.Value = Me.Fields("InvoiceStatusCode").Value & " - " & TelepayStatusTxts(Me.Fields("InvoiceStatusCode").Value)
                    End If
                Else
                    Me.txtErrorText.Visible = False
                End If
            End If
            ' If KID as well as freetext, show KID;
            If sReportNumber = "11" Then
                ' 27.12.2018 - added next If
                If bExportPayment = False Then

                    ' added next If 31.12.2018
                    If bAllPaymentsACTC Then
                        ' used for Pain.002 - when we receive a file with ACTC only, we end up here
                        ' all payments accepted - show a text for this
                        bPrint = False
                        Me.Fields("E_Name").Value = LRS(40142)  '"Alle betalinger akseptert (ACTC)"

                        ' ikke vis �vrige felt;
                        Me.lblPayI_Account.Visible = False
                        Me.txtPayI_Account.Visible = False
                        Me.lblE_Account.Visible = False
                        Me.txtE_Account.Visible = False
                        Me.lblREF_Bank1.Visible = False
                        Me.txtREF_Bank1.Visible = False
                        Me.lblREF_Bank2.Visible = False
                        Me.txtREF_Bank2.Visible = False
                        Me.lblDATE_Value.Visible = False
                        Me.txtDATE_Value.Visible = False
                        Me.lblREF_Own.Visible = False
                        Me.txtREF_Own.Visible = False
                        Me.txtMON_P_TransferredAmount.Visible = False
                        Me.SubRptFreetext.Visible = False
                        Me.lblBB_Id.Visible = False
                        Me.txtBabelBank_ID.Visible = False
                        Me.txtE_Adr1.Visible = False
                        Me.txtE_Adr2.Visible = False
                        Me.txtE_Adr3.Visible = False
                        Me.txtE_City.Visible = False
                        Me.txtE_CountryCode.Visible = False
                        Me.txtE_Zip.Visible = False

                        Me.lblBIC.Visible = False
                        Me.txtBIC.Visible = False
                        Me.lblBANK_Branch.Visible = False
                        Me.txtBANK_Branch.Visible = False
                        Me.txtMON_InvoiceCurrency.Visible = False
                        Me.txtMON_P_TransferCurrency.Visible = False
                    End If
                Else
                    '05.04.2016 - Added
                    If IsNumeric(Me.Fields("InvoiceStatusCode").Value) Then
                        If CLng(Me.Fields("InvoiceStatusCode").Value) > 2 Then
                            bPrint = True
                            bAllPaymentsACTC = False
                        Else
                            bPrint = False
                        End If
                    End If
                End If
            Else
                bPrint = True
            End If
            If bPrint Then
                If Not EmptyString(Me.Fields("Unique_ID").Value) Then
                    Me.txtUniqueID.Visible = True
                    sTemp = Me.Fields("ImportFormat").Value
                    Select Case sTemp
                        Case vbBabel.BabelFiles.FileType.OCR, vbBabel.BabelFiles.FileType.AutoGiro, vbBabel.BabelFiles.FileType.OCR_Bankgirot, vbBabel.BabelFiles.FileType.CREMUL ', vbBabel.FileType.Telepay2
                            Me.txtUniqueID.Value = "KID: " & Me.Fields("Unique_ID").Value
                        Case vbBabel.BabelFiles.FileType.DanskeBankTeleService
                            Me.txtUniqueID.Value = "FIK: " & Me.Fields("Unique_ID").Value
                        Case vbBabel.BabelFiles.FileType.Telepay, vbBabel.BabelFiles.FileType.Telepay2
                            '20.11.2014 - Removed next if, because the paycode may be wrong
                            'If Me.Fields("PayCode").Value > "300" And Me.Fields("PayCode").Value < "304" Then
                            Me.txtUniqueID.Value = "KID: " & Me.Fields("Unique_ID").Value
                            'End If
                        Case Else
                            Me.txtUniqueID.Value = Me.Fields("Unique_ID").Value
                    End Select
                Else
                    ' New 11.02.05: If Structured Invoiceno, show;
                    If Not EmptyString(Me.Fields("InvoiceNumber").Value) Then
                        Me.txtUniqueID.Visible = True
                        Me.txtUniqueID.Value = LRS(60158) & " " & Me.Fields("InvoiceNumber").Value 'Invoice Number: 
                    Else
                        Me.txtUniqueID.Visible = False
                    End If
                End If
            Else
                Me.txtUniqueID.Visible = False
                Me.txtVoucherNo.Visible = False
                Me.txtInvoiceAmount.Visible = False
            End If

            ' 10.02.2016 Changed to Date_Payment for Break on Account+Date
            If iBreakLevel = 1 Then
                sDate_Payment = Me.Fields("DATE_Payment").Value
                Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDate_Payment, 4)), CInt(Mid$(sDate_Payment, 5, 2)), CInt(Right$(sDate_Payment, 2)))
            Else
                sDATE_Production = Me.Fields("DATE_Production").Value
                Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))
            End If
            sDATE_Value = Me.Fields("DATE_Value").Value
            If sDATE_Value = "19900101" Then
                sDATE_Value = Me.Fields("DATE_Payment").Value
            End If
            Me.txtDATE_Value.Value = DateSerial(CInt(Left$(sDATE_Value, 4)), CInt(Mid$(sDATE_Value, 5, 2)), CInt(Right$(sDATE_Value, 2)))

            If Me.Fields("Paycode").Value = "602" Then
                Me.txtE_Name.Value = "GIROMAIL"
            Else
                Me.txtE_Name.Value = Me.Fields("E_Name").Value
            End If

            'If Me.Fields("Bank").Value = vbBabel.Bank.DnBNOR_INPS And Me.Fields("ImportFormat").Value = vbBabel.FileType.BANSTA Then
            '    Me.txtInvoiceAmount.Visible = False
            'Else
            '    Me.txtInvoiceAmount.Visible = True
            'End If

            If Me.Fields("Bank").Value = vbBabel.BabelFiles.Bank.DnBNOR_INPS And Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.BANSTA Then
                Me.lblE_Account.Visible = False
                Me.lblI_Account.Visible = False
                Me.lblREF_Bank2.Visible = False
                Me.lblDate_Production.Visible = False
                Me.txtDATE_Production.Visible = False
                Me.lblBIC.Visible = False
                Me.lblBANK_Branch.Visible = False
                Me.lblVoucherNo.Visible = False
                Me.txtE_Name.Visible = False
                Me.txtE_Adr1.Visible = False
                Me.txtE_Adr2.Visible = False
                Me.txtE_Adr3.Visible = False
                Me.txtE_Zip.Visible = False
                Me.txtE_City.Visible = False
                Me.txtE_CountryCode.Visible = False
                Me.txtE_Account.Visible = False
                Me.txtREF_Bank2.Visible = False
                Me.txtBIC.Visible = False
                Me.txtBANK_Branch.Visible = False
                Me.txtVoucherNo.Visible = False
                Me.grPaymentInternationalHeader.Visible = False
                If Me.Fields("Special").Value = "DNBNORFINANS" Then
                    Me.lblREF_Own.Value = "Kundenr.: "
                    Me.lblDATE_Value.Value = "Produksjonsdato: "
                    'DNBNORFINANSLABELS
                    'Me.lblREF_Own.Left = 0
                    'Me.lblREF_Own.Top = 0
                    'Me.txtREF_Own.Left = 1500
                    'Me.txtREF_Own.Top = 0
                    'Me.lblDATE_Value.Left = 0
                    'Me.lblDATE_Value.Top = 195
                    'Me.txtDATE_Value.Left = 1500
                    'Me.txtDATE_Value.Top = 195
                    'Me.lblREF_Bank1.Left = 0
                    'Me.lblREF_Bank1.Top = 390
                    Me.lblREF_Bank1.Value = "Batchref.: "
                    'Me.txtREF_Bank1.Left = 1500
                    'Me.txtREF_Bank1.Top = 390
                    'Me.grPaymentHeader.Height = 650
                Else
                    Me.lblDATE_Value.Visible = False
                    Me.txtDATE_Value.Visible = False
                    Me.txtMON_P_TransferredAmount.Visible = False
                    Me.lblREF_Bank1.Visible = False
                    Me.txtREF_Bank1.Visible = False
                    'Me.lblREF_Own.Left = 20
                    'Me.lblREF_Own.Top = 20
                End If
            End If

            If sSpecial = "PATENTSTYRET_DOUBLE" Then
                Me.txtUniqueID.Visible = False
                'Me.txtInvoiceAmount.Visible = False
            End If

            '10.12.2020 - Added next info to remove label and fixed texts from the report
            If IsDBNull(Me.Fields("BabelBank_ID").Value) Then
                Me.lblBB_Id.Visible = False
                Me.txtBabelBank_ID.Visible = False
            Else
                If EmptyString(Me.Fields("BabelBank_ID").Value) Then
                    Me.lblBB_Id.Visible = False
                    Me.txtBabelBank_ID.Visible = False
                    '04.01.20201 - Added next ElseIf
                ElseIf Me.Fields("BabelBank_ID").Value = "-1" Then
                    Me.lblBB_Id.Visible = False
                    Me.txtBabelBank_ID.Visible = False
                End If
            End If

            'More SPECIAL-stuff
            If sSpecial = "SG FINANS" Or sSpecial = "SGFINANS_AML" Then
                ' 27.11.2007 Do not show if no info in i_Name:
                If IsDBNull(Me.Fields("I_Name").Value) Then
                    If IsDBNull(Me.Fields("I_Adr1").Value) Then
                        Me.grPaymentReceiverHeader.Visible = False
                    ElseIf EmptyString(Me.Fields("I_Adr1").Value) Then
                        Me.grPaymentReceiverHeader.Visible = False
                    Else
                        Me.grPaymentReceiverHeader.Visible = True
                    End If
                ElseIf IsDBNull(Me.Fields("I_Adr1").Value) Then
                    If EmptyString(Me.Fields("I_Name").Value) Then
                        Me.grPaymentReceiverHeader.Visible = False
                    Else
                        Me.grPaymentReceiverHeader.Visible = True
                    End If
                ElseIf EmptyString(Me.Fields("I_Name").Value) And EmptyString(Me.Fields("I_Adr1").Value) Then
                    Me.grPaymentReceiverHeader.Visible = False
                Else
                    Me.grPaymentReceiverHeader.Visible = True
                End If
            Else
                Me.grPaymentReceiverHeader.Visible = False
            End If

        End If
    End Sub

    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim rptFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String
        Dim lTextCounter As Long
        Dim sText As String
        Dim pSize As System.Drawing.SizeF

        lTextCounter = 1
        If sSpecial = "PATENTSTYRET_DOUBLE" Then

            pSize.Height = 0.15
            pSize.Width = 5
            Me.SubRptFreetext.Size = pSize

            childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            If bSQLServer Then
                sMySQL = "SELECT PDC.InvoiceNo, Doubletttype,  Case WHEN PDC.Doubletttype = 1 THEN 'Betalbarhet ' + PDC.InvoiceNo + ' er betalt to ganger i BabelBank' ELSE Case WHEN PDC.Doubletttype = 2 THEN 'Betalbarhet ' + PDC.InvoiceNo + ' er tidligere betalt og oppdatert i ordresystem' ELSE "
                sMySQL = sMySQL & "'Betalbarhet ' + PDC.InvoiceNo + ' er betalt to ganger i BabelBank samt ogs� tidligere betalt og oppdatert i ordresystem' END END AS Freetext, 'Dublett' AS Label "
            Else
                sMySQL = "SELECT PDC.InvoiceNo, Doubletttype, IIf(PDC.Doubletttype = 1, 'Betalbarhet ' + PDC.InvoiceNo + ' er betalt to ganger i BabelBank', IIf(PDC.Doubletttype = 2, 'Betalbarhet ' + PDC.InvoiceNo + ' er tidligere betalt og oppdatert i ordresystem', "
                sMySQL = sMySQL & "'Betalbarhet ' + PDC.InvoiceNo + ' er betalt to ganger i BabelBank samt ogs� tidligere betalt og oppdatert i ordresystem')) AS Freetext, 'Dublett' AS Label "
            End If
            sMySQL = sMySQL & "FROM PatentDoubleCheck PDC LEFT JOIN Invoice I ON PDC.Company_ID = I.Company_ID AND PDC.BabelFile_ID = I.BabelFile_ID AND PDC.Batch_ID = I.Batch_ID AND PDC.Payment_ID = I.Payment_ID AND PDC.Invoice_ID = I.Invoice_ID "
            sMySQL = sMySQL & "WHERE PDC.Doubletttype > 0"
            sMySQL = sMySQL & " AND PDC.BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            sMySQL = sMySQL & " AND PDC.Batch_ID = " & Trim$(Str(iBatch_ID))
            sMySQL = sMySQL & " AND PDC.Payment_ID = " & Trim$(Str(iPayment_ID))
            sMySQL = sMySQL & " AND PDC.Invoice_ID = " & Trim$(Str(iInvoice_ID))
            sMySQL = sMySQL & " AND PDC.PatentStyretTeller = " & iPatentStyretTeller.ToString
            'sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Freetext_ID ASC"


            'sMySQL = "SELECT 'Dette er en test' AS Freetext, 'KOKO' AS Label FROM Freetext WHERE Company_ID = " & Trim$(Str(iCompany_ID))
            'sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            'sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
            'sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
            'sMySQL = sMySQL & " AND Invoice_ID = " & Trim$(Str(iInvoice_ID))
            'sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Freetext_ID ASC"

            rptFreetext.ReportFromDatabase = True
            childFreetextDataSource.SQL = sMySQL
            rptFreetext.Label = "�rsak: "
            rptFreetext.DataSource = childFreetextDataSource
            Me.SubRptFreetext.Report = rptFreetext
        Else
            If bReportFromDatabase Then
                '26.06.2018 - Added next IF
                If sSpecial = "LINDORFF" Then
                    'If iInvoice_ID = 1 Then
                    childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
                    sMySQL = "SELECT XText AS [Freetext], 'Kommentar: ' AS Label FROM [Freetext] WHERE Company_ID = " & Trim$(Str(iCompany_ID))
                    sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
                    sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
                    sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
                    sMySQL = sMySQL & " AND Qualifier > 2 ORDER BY Freetext_ID ASC"

                    rptFreetext.ReportFromDatabase = True
                    childFreetextDataSource.SQL = sMySQL
                    'rptFreetext.Label = "Kommentar: "
                    rptFreetext.DataSource = childFreetextDataSource
                    Me.SubRptFreetext.Report = rptFreetext
                    'End If
                End If

                childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
                sMySQL = "SELECT XText AS [Freetext], '' AS Label FROM [Freetext] WHERE Company_ID = " & Trim$(Str(iCompany_ID))
                sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
                sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
                sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
                sMySQL = sMySQL & " AND Invoice_ID = " & Trim$(Str(iInvoice_ID))
                sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Freetext_ID ASC"

                rptFreetext.ReportFromDatabase = True
                childFreetextDataSource.SQL = sMySQL
                rptFreetext.Label = LRS(40010) '"Bel�pet gjelder:" 
                rptFreetext.DataSource = childFreetextDataSource
                Me.SubRptFreetext.Report = rptFreetext

            Else
                If oInvoice.Freetexts.Count > 0 Then
                    sText = ""
                    For Each oFreeText In oInvoice.Freetexts
                        sText = sText & oFreeText.Text
                        sText = sText & vbCrLf 'Add carriagereturn and line feed to the end
                        lTextCounter = lTextCounter + 1
                    Next oFreeText
                    'Remove last vbCrLf
                    If sText.Length > 1 Then
                        sText = Left(sText, Len(sText) - 2)
                    End If

                    rptFreetext.Label = LRS(40010) '"Bel�pet gjelder:" 
                    rptFreetext.Text = sText
                    rptFreetext.ReportFromDatabase = False
                    Me.SubRptFreetext.Report = rptFreetext
                Else
                    rptFreetext.ReportFromDatabase = False
                    rptFreetext.Text = ""
                    Me.SubRptFreetext.Report = rptFreetext
                End If

        End If
            End If
            If Not rptFreetext Is Nothing Then
                rptFreetext = Nothing
            End If
    End Sub

    Private Sub grFreetextHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grFreetextHeader.Format
        'Dim rptFreetext As New rp_Sub_Freetext()
        'Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        'Dim sMySQL As String

        'If bReportFromDatabase Then
        '    childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        '    sMySQL = "SELECT XText AS Freetext, '' AS Label FROM Freetext WHERE Company_ID = " & Trim$(Str(iCompany_ID))
        '    sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
        '    sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
        '    sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
        '    'sMySQL = sMySQL & " AND Invoice_ID = " & Trim$(Str(iInvoice_ID))
        '    sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Invoice_ID ASC, Freetext_ID ASC"

        '    childFreetextDataSource.SQL = sMySQL
        '    rptFreetext.DataSource = childFreetextDataSource

        'Else
        '    If EmptyString(sFreetext) Then
        '        Me.lblConcerns.Visible = False
        '    Else
        '        rptFreetext.ReportFromDatabase = False
        '        rptFreetext.Text = sFreetext
        '    End If

        'End If

        'Me.SubRptFreetext.Report = rptFreetext

        'If rptFreetext.EmptyReport Then
        '    Me.lblConcerns.Visible = False
        'End If


    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub grBatchHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grBatchHeader.Format
        Dim pLocation As System.Drawing.PointF
        Dim pSize As System.Drawing.SizeF

        If Me.txtFilename.Visible Then
            pLocation.X = 1.5
            pLocation.Y = 0.3
            Me.txtFilename.Location = pLocation
            pSize.Height = 0.19
            pSize.Width = 4.5
            Me.txtFilename.Size = pSize
            pLocation.X = 0.125
            pLocation.Y = 0.3
            Me.lblI_Account.Location = pLocation
            Me.lblI_Account.Value = LRS(20007)
            Me.txtI_Account.Visible = False
        End If

    End Sub
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class

