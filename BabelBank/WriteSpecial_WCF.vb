﻿Option Strict Off
Option Explicit On
'Imports System
'Imports System.Configuration
'Imports System.Collections.Generic
'Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Description

'using System.Text;
Module WriteSpecial_WCF
    'Removed 11.09.2019
    'Function WriteConnect_WCF_NewGLHandling(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFILE_Name As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sAdditionalID As String, ByVal sClientNo As String, ByVal sSpecial As String, ByVal bPostAgainstObservationAccount As Boolean)
    '    'XOKNET - 11.10.2010 - I guess the easiest is to convert the whole function
    '    'XOKNET - 14.12.2010 - Convert the whole function
    '    'DET ER DENNE WriteConnect_WCF SOM SKAL BENYTTES VIDERE!!!!!!!!!!!!!!!!!!!
    '    Dim ConnectClient As New vbBabel.Conecto.BabelBankServiceClient
    '    Dim BabelBalanceAccountSaveDtos() As Conecto.BabelBalanceAccountSaveDto = Nothing
    '    Dim BabelBalanceAccountSaveDto As Conecto.BabelBalanceAccountSaveDto
    '    Dim lBalanceAccountCounter As Long
    '    Dim BabelDebtorAccountSaveDtos() As Conecto.BabelDebtorAccountSaveDto
    '    Dim BabelDebtorAccountSaveDto As Conecto.BabelDebtorAccountSaveDto
    '    Dim lDebtorCounter As Long
    '    Dim BabelPaymentSaveDtos() As Conecto.BabelPaymentSaveDto
    '    Dim BabelPaymentSaveDto As Conecto.BabelPaymentSaveDto
    '    Dim lPaymentCounter As Long
    '    Dim BabelSettleSaveDtos() As Conecto.BabelSettleSaveDto
    '    Dim BabelSettleSaveDto As Conecto.BabelSettleSaveDto
    '    Dim lSettleCounter As Long

    '    Dim nodeBabelBalanceAccountSaveDtos As MSXML2.IXMLDOMNodeList
    '    Dim nodeBabelBalanceAccountSaveDto As MSXML2.IXMLDOMElement
    '    Dim nodeBabelDebtorAccountSaveDtos As MSXML2.IXMLDOMNodeList
    '    Dim nodeBabelDebtorAccountSaveDto As MSXML2.IXMLDOMElement
    '    Dim nodeBabelSettleSaveDtos As MSXML2.IXMLDOMNodeList
    '    Dim nodeBabelSettleSaveDto As MSXML2.IXMLDOMElement
    '    Dim nodeTemp As MSXML2.IXMLDOMElement

    '    Dim oFs As New Scripting.FileSystemObject
    '    Dim oFile As Scripting.TextStream
    '    Dim nTotalTransactions, nFileTransactions As Double
    '    Dim nFileSum As Double, nTotalSum As Double
    '    Dim sMessage As String
    '    Dim bAppendFile As Boolean
    '    Dim bFirstBatch As Boolean
    '    Dim sOldAccountNo As String
    '    Dim bAccountFound As String, sGLAccount As String
    '    Dim sClientName As String
    '    Dim sClientNumber As String
    '    Dim sEDIMessageNo As String
    '    Dim aClientArray() As String
    '    Dim iClientArrayCounter As Integer

    '    Dim aValidationArray(,) As String
    '    Dim sValidationError As String

    '    Dim bClientNoSet As Boolean

    '    'XOKNET - 11.10.2010 -
    '    Dim bx As Boolean
    '    Dim lNoOfDelimiters As Long
    '    Dim lCounter As Long
    '    Dim iGLIndex As Integer
    '    Dim nGLAmount As Double
    '    Dim nCreditTransactionAmount As Double
    '    Dim nSettleAmount As Double
    '    Dim bNewDebtor As Boolean
    '    Dim sOldDebtor As String
    '    Dim sNoMatchAccount As String

    '    Dim oBabel As vbBabel.BabelFile
    '    Dim oBatch As vbBabel.Batch
    '    Dim oPayment As Payment, oInvoice As vbBabel.Invoice
    '    Dim oGLInvoice As vbBabel.Invoice
    '    Dim oFreeText As vbBabel.Freetext
    '    Dim oFilesetup As vbBabel.FileSetup
    '    Dim oClient As vbBabel.Client
    '    Dim oaccount As vbBabel.Account
    '    Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
    '    Dim nTotalAmount As Double
    '    Dim lNoOfTransactions As Long
    '    Dim sFreetextFixed As String
    '    Dim sQ4Text As String
    '    Dim iFreetextCounter As Integer

    '    Dim docXMLExport As New MSXML2.DOMDocument40
    '    Dim pi As MSXML2.IXMLDOMProcessingInstruction
    '    Dim xmlSchema As MSXML2.XMLSchemaCache40
    '    Dim docXMLSchema As New MSXML2.DOMDocument40
    '    Dim nodRootElement As MSXML2.IXMLDOMElement
    '    Dim nodBankfile As MSXML2.IXMLDOMElement
    '    Dim nodStart As MSXML2.IXMLDOMElement
    '    Dim nodImportData_BalanceAccountsList As MSXML2.IXMLDOMElement
    '    Dim nodbof_BalanceAccount As MSXML2.IXMLDOMElement
    '    Dim nodBankPayment As MSXML2.IXMLDOMElement
    '    Dim nodbof_BalanceAccount_CreditorReference As MSXML2.IXMLDOMElement
    '    Dim nodCreditor As MSXML2.IXMLDOMElement
    '    Dim nodbof_BalanceAccount_Transactions As MSXML2.IXMLDOMElement
    '    Dim nodbof_BalanceAccount_TransactionsList As MSXML2.IXMLDOMElement
    '    Dim nodTransactionDebtor As MSXML2.IXMLDOMElement
    '    Dim nodDebtor As MSXML2.IXMLDOMElement
    '    Dim nodTransaction_Settles As MSXML2.IXMLDOMElement
    '    Dim nodTransaction As MSXML2.IXMLDOMElement
    '    Dim nodSettleEntry As MSXML2.IXMLDOMElement
    '    Dim nodbot_SettleDraft As MSXML2.IXMLDOMElement
    '    Dim nodGLSettle As MSXML2.IXMLDOMElement
    '    Dim nodTransactionAmount As MSXML2.IXMLDOMElement
    '    'Dim nodSettleTransaction As MSXML2.IXMLDOMElement
    '    Dim nodDebitTransaction As MSXML2.IXMLDOMElement
    '    Dim nodElement As MSXML2.IXMLDOMElement
    '    Dim newEle As MSXML2.IXMLDOMElement
    '    Dim nodImportData_BalanceAccounts As MSXML2.IXMLDOMNode
    '    Dim nodCustomerExists As MSXML2.IXMLDOMNode
    '    Dim nodCreditorExists As MSXML2.IXMLDOMNode
    '    Dim nodlistSettleDrafts As MSXML2.IXMLDOMNodeList
    '    Dim nodbot_SettleDraftInvoice As MSXML2.IXMLDOMElement
    '    Dim nInvoiceAmount As Double
    '    Dim nAmountToUse As Double
    '    Dim bRemoveActiveSettleDraft As Boolean
    '    Dim i As Integer
    '    Dim nCreditnoteAmount As Double
    '    Dim sXpath As String
    '    Dim nodTemp As MSXML2.IXMLDOMNode

    '    Dim bReturnValue As Boolean
    '    Dim iJournalID As Integer = -1
    '    Dim sReference As String = ""
    '    Dim aQueryArray(,,) As String
    '    Dim sUID As String = ""
    '    Dim sPWD As String = ""
    '    Dim sURL As String = ""

    '    Dim sErrorField As String = ""
    '    Dim sErrorValue As String = ""
    '    Dim sErrorName As String = ""
    '    Dim sErrorAmount As String = ""
    '    Dim sErrorDate As String = ""
    '    Dim sErrorTypeOfPosting As String = ""
    '    Dim sErrorInvoiceAmount As String = ""
    '    Dim sErrorMessage As String = ""

    '    Dim aAccountArray(,) As String
    '    Dim aAmountArray() As Double
    '    Dim bUseDayTotals As Boolean
    '    Dim lCounter2 As Long, lArrayCounter As Long

    '    Dim oMyERPDal As vbBabel.DAL = Nothing
    '    Dim sRetrieveBalanceSQL As String

    '    ' lag en outputfil
    '    Try

    '        bReturnValue = False
    '        bAppendFile = False
    '        bFirstBatch = True
    '        sOldAccountNo = vbNullString
    '        bAccountFound = False
    '        sGLAccount = vbNullString
    '        sClientName = vbNullString
    '        sClientNumber = vbNullString
    '        sEDIMessageNo = vbNullString
    '        sFreetextFixed = vbNullString
    '        sQ4Text = vbNullString
    '        lNoOfDelimiters = 0
    '        sOldDebtor = vbNullString

    '        nTotalAmount = 0
    '        lNoOfTransactions = 0

    '        If oBabelFiles.Count > 0 Then

    '            'New code 21.12.2006
    '            aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

    '            'Create an array equal to the client-array
    '            ReDim aAmountArray(aAccountArray.GetLength(1))

    '            'Calculate some totals
    '            For Each oBabel In oBabelFiles
    '                sEDIMessageNo = oBabel.EDI_MessageNo
    '                nTotalAmount = nTotalAmount + oBabel.MON_InvoiceAmount
    '                For Each oBatch In oBabel.Batches
    '                    For Each oPayment In oBatch.Payments
    '                        'May be used later when only matched items are to be exported
    '                        'If oPayment.MATCH_Matched = MatchStatus.Matched Then
    '                        For Each oInvoice In oPayment.Invoices
    '                            If oInvoice.MATCH_Final Then
    '                                lNoOfTransactions = lNoOfTransactions + 1
    '                            End If
    '                        Next oInvoice
    '                        'End If
    '                    Next oPayment
    '                Next oBatch
    '            Next oBabel

    '            'Initiate the XMLFiles
    '            'General
    '            docXMLExport.preserveWhiteSpace = True
    '            docXMLExport.async = False
    '            newEle = docXMLExport.createNode(MSXML2.tagDOMNodeType.NODE_ELEMENT, "ImportData", vbNullString)
    '            newEle.setAttribute("CreatedBy", "BabelBank")
    '            If EmptyString(sEDIMessageNo) Then
    '                newEle.setAttribute("ImportId", Format(Now(), "yyyy-MM-dd") & "T" & Format(Now(), "HH:mm:ss"))
    '            Else
    '                newEle.setAttribute("ImportId", sEDIMessageNo) 'This is the last ID
    '            End If
    '            newEle.setAttribute("NumberOfTransactions", Trim$(Str(lNoOfTransactions)))
    '            newEle.setAttribute("ProductionDate", Format(Now(), "yyyy-MM-dd") & "T" & Format(Now(), "HH:mm:ss"))
    '            If bPostAgainstObservationAccount Then
    '                newEle.setAttribute("TotalAmount", ConvertFromAmountToString(0, vbNullString, "."))
    '            Else
    '                newEle.setAttribute("TotalAmount", ConvertFromAmountToString(nTotalAmount, vbNullString, "."))
    '            End If

    '            'XNET - 29.02.2012 - Changed next lines
    '            newEle.setAttribute("xmlns", "clr-namespace:Conect.DomainModel.Domain;assembly=Conect.DomainModel.Domain")
    '            newEle.setAttribute("xmlns:bof", "clr-namespace:Conect.DomainModel.Domain.FileImport;assembly=Conect.DomainModel.Domain")
    '            newEle.setAttribute("xmlns:bot", "clr-namespace:Conect.DomainModel.Domain;assembly=Conect.DomainModel.Domain")
    '            newEle.setAttribute("xmlns:scg", "clr-namespace:System.Collections.Generic;assembly=mscorlib")
    '            newEle.setAttribute("xmlns:x", "http://schemas.microsoft.com/winfx/2006/xaml")

    '            'newEle.setAttribute("xmlns", "clr-namespace:BlackBox.ObjectLibrary;assembly=BlackBox.ObjectLibrary")
    '            'newEle.setAttribute("xmlns:bof", "clr-namespace:BlackBox.ObjectLibrary.FileImport;assembly=BlackBox.ObjectLibrary")
    '            'newEle.setAttribute("xmlns:bot", "clr-namespace:BlackBox.ObjectLibrary.Transactions;assembly=BlackBox.ObjectLibrary")
    '            'newEle.setAttribute("xmlns:scg", "clr-namespace:System.Collections.Generic;assembly=mscorlib")
    '            'newEle.setAttribute("xmlns:x", "http://schemas.microsoft.com/winfx/2006/xaml")
    '            docXMLExport.documentElement = newEle
    '            newEle = Nothing
    '            pi = docXMLExport.createProcessingInstruction("xml", " version='1.0' encoding='utf-8'")
    '            docXMLExport.insertBefore(pi, docXMLExport.childNodes.item(0))

    '            nodImportData_BalanceAccounts = docXMLExport.createNode(MSXML2.tagDOMNodeType.NODE_ELEMENT, "ImportData.BalanceAccounts", vbNullString)
    '            'Set nodImportData_BalanceAccounts = docXMLExport.createElement("ImportData.BalanceAccounts")
    '            docXMLExport.documentElement.appendChild(nodImportData_BalanceAccounts)
    '            nodStart = nodImportData_BalanceAccounts

    '            nodImportData_BalanceAccountsList = docXMLExport.createElement("scg:List")
    '            nodImportData_BalanceAccountsList.setAttribute("x:TypeArguments", "bof:BalanceAccount")
    '            nodImportData_BalanceAccountsList.setAttribute("Capacity", "4")
    '            nodStart.appendChild(nodImportData_BalanceAccountsList)


    '            ' Traverse through the collections
    '            '------------------

    '            For Each oBabel In oBabelFiles
    '                bExportoBabel = False
    '                'Have to go through each batch-object to see if we have objects thatt shall
    '                ' be exported to this exportfile
    '                For Each oBatch In oBabel.Batches
    '                    For Each oPayment In oBatch.Payments
    '                        If Not bMultiFiles Then
    '                            bExportoBabel = True
    '                        Else
    '                            'If oPayment.I_Account = sI_Account Then
    '                            ' 01.10.2018 – changed above If to:
    '                            If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
    '                                If Len(oPayment.VB_ClientNo) > 0 Then
    '                                    If oPayment.VB_ClientNo = sClientNo Then
    '                                        bExportoBabel = True
    '                                    End If
    '                                Else
    '                                    bExportoBabel = True
    '                                End If
    '                            End If
    '                        End If
    '                        If bExportoBabel Then
    '                            Exit For
    '                        End If
    '                    Next
    '                    If bExportoBabel Then
    '                        Exit For
    '                    End If
    '                Next oBatch

    '                If bExportoBabel Then
    '                    For Each oBatch In oBabel.Batches

    '                        bExportoBatch = False
    '                        'Have to go through the payment-object to see if we have objects thatt shall
    '                        ' be exported to this exportfile
    '                        For Each oPayment In oBatch.Payments
    '                            If Not bMultiFiles Then
    '                                bExportoBatch = True
    '                            Else
    '                                'If oPayment.I_Account = sI_Account Then
    '                                ' 01.10.2018 – changed above If to:
    '                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
    '                                    If Len(oPayment.VB_ClientNo) > 0 Then
    '                                        If oPayment.VB_ClientNo = sClientNo Then
    '                                            bExportoBatch = True
    '                                        End If
    '                                    Else
    '                                        bExportoBatch = True
    '                                    End If
    '                                End If
    '                            End If
    '                            If bExportoBatch Then
    '                                Exit For
    '                            End If
    '                        Next oPayment

    '                        'XNET 17.04.2012 - Removed this test, because 0-amounts, matched, must be exported
    '                        '            If bExportoBatch Then
    '                        '                If IsEqualAmount(oBatch.MON_InvoiceAmount, 0) Then
    '                        '                    bExportoBatch = False
    '                        '                End If
    '                        '            End If

    '                        If bExportoBatch Then

    '                            'Find the client
    '                            If oBatch.Payments.Item(1).I_Account <> sOldAccountNo Then
    '                                sNoMatchAccount = vbNullString
    '                                If oPayment.VB_ProfileInUse Then
    '                                    'sI_Account = oPayment.I_Account
    '                                    bAccountFound = False
    '                                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
    '                                        For Each oClient In oFilesetup.Clients
    '                                            For Each oaccount In oClient.Accounts
    '                                                If oBatch.Payments.Item(1).I_Account = oaccount.Account Then
    '                                                    sGLAccount = Trim$(oaccount.GLAccount)
    '                                                    sClientName = Trim$(oClient.Name)
    '                                                    sClientNumber = Trim$(oClient.ClientNo)
    '                                                    sOldAccountNo = oBatch.Payments.Item(1).I_Account
    '                                                    sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
    '                                                    bAccountFound = True

    '                                                    Exit For
    '                                                End If

    '                                                ' 11.01.2013 - linjen under er sannsynligvis feil
    '                                                'For lCounter2 = 0 To aAccountArray.GetLength(2)
    '                                                For lCounter2 = 0 To aAccountArray.GetLength(1)
    '                                                    If oPayment.I_Account = aAccountArray(1, lCounter2) Then
    '                                                        lArrayCounter = lCounter2
    '                                                        Exit For
    '                                                    End If
    '                                                Next lCounter2

    '                                            Next oaccount
    '                                            If bAccountFound Then Exit For
    '                                        Next oClient
    '                                        If bAccountFound Then Exit For
    '                                    Next oFilesetup
    '                                End If
    '                            End If

    '                            'Write the batch-level
    '                            nodbof_BalanceAccount = docXMLExport.createElement("bof:BalanceAccount")
    '                            If bPostAgainstObservationAccount Then
    '                                nodbof_BalanceAccount.setAttribute("Account", "")
    '                                sErrorField = "Batch amount"
    '                                sErrorValue = ConvertFromAmountToString(0, vbNullString, ".")
    '                                nodbof_BalanceAccount.setAttribute("Amount", ConvertFromAmountToString(0, vbNullString, "."))  'ConvertFromAmountToString(oBatch.MON_InvoiceAmount, vbNullString, ".")
    '                            Else
    '                                sErrorField = "Batch account"
    '                                sErrorValue = sGLAccount
    '                                nodbof_BalanceAccount.setAttribute("Account", sGLAccount)
    '                                sErrorField = "Batch amount"
    '                                sErrorValue = ConvertFromAmountToString(oBatch.MON_InvoiceAmount, vbNullString, ".")
    '                                nodbof_BalanceAccount.setAttribute("Amount", ConvertFromAmountToString(oBatch.MON_InvoiceAmount, vbNullString, "."))  'ConvertFromAmountToString(oBatch.MON_InvoiceAmount, vbNullString, ".")
    '                            End If
    '                            sErrorField = "Bookdate"
    '                            sErrorValue = oBatch.Payments.Item(1).DATE_Payment
    '                            nodbof_BalanceAccount.setAttribute("BookDate", Left$(oBatch.Payments.Item(1).DATE_Payment, 4) & "-" & Mid$(oBatch.Payments.Item(1).DATE_Payment, 5, 2) & "-" & Right$(oBatch.Payments.Item(1).DATE_Payment, 2))
    '                            sErrorField = "Reference"
    '                            sErrorValue = oBatch.REF_Bank
    '                            nodbof_BalanceAccount.setAttribute("Reference", oBatch.REF_Bank)
    '                            sErrorField = "Valuedate"
    '                            sErrorValue = oBatch.Payments.Item(1).DATE_Value
    '                            nodbof_BalanceAccount.setAttribute("ValueDate", Left$(oBatch.Payments.Item(1).DATE_Value, 4) & "-" & Mid$(oBatch.Payments.Item(1).DATE_Value, 5, 2) & "-" & Right$(oBatch.Payments.Item(1).DATE_Value, 2))
    '                            sErrorField = "BalanceAccountID"
    '                            sErrorValue = oBabel.Index.ToString
    '                            nodbof_BalanceAccount.setAttribute("BalanceAccountId", Trim(Str(oBabel.Index)) & "-" & Trim(Str(oBatch.Index)))
    '                            sErrorField = "AppendChild: BalanceAccount"
    '                            nodImportData_BalanceAccountsList.appendChild(nodbof_BalanceAccount)

    '                            'nodbof_BalanceAccount_CreditorReference = docXMLExport.createElement("bof:BalanceAccount.CreditorReference")
    '                            'nodbof_BalanceAccount.appendChild(nodbof_BalanceAccount_CreditorReference)

    '                            'nodCreditor = docXMLExport.createElement("Creditor")
    '                            'If Not EmptyString(oInvoice.MATCH_ClientNo) Then
    '                            '    nodCreditor.setAttribute("AccountingId", oInvoice.MATCH_ClientNo)
    '                            'Else
    '                            '    'An unmatched payment doesn't have a MATCH_ClientNo
    '                            '    nodCreditor.setAttribute("AccountingId", sClientNumber)
    '                            'End If
    '                            'nodCreditor.setAttribute "Name", sClientName
    '                            'nodbof_BalanceAccount_CreditorReference.appendChild(nodCreditor)

    '                            nodbof_BalanceAccount_Transactions = docXMLExport.createElement("bof:BalanceAccount.Transactions")
    '                            sErrorField = "AppendChild: BalanceAccount_Transactions"
    '                            nodbof_BalanceAccount.appendChild(nodbof_BalanceAccount_Transactions)

    '                            nodbof_BalanceAccount_TransactionsList = docXMLExport.createElement("scg:List")
    '                            nodbof_BalanceAccount_TransactionsList.setAttribute("x:TypeArguments", "Transaction")
    '                            nodbof_BalanceAccount_TransactionsList.setAttribute("Capacity", "4")
    '                            sErrorField = "AppendChild: BalanceAccount_TransactionsList"
    '                            nodbof_BalanceAccount_Transactions.appendChild(nodbof_BalanceAccount_TransactionsList)

    '                            For Each oPayment In oBatch.Payments

    '                                '11.10.2010 - XOKNET - Added next lines
    '                                bExportoPayment = False
    '                                'Have to go through the payment-object to see if we have objects that shall
    '                                ' be exported to this exportfile
    '                                If Not bMultiFiles Then
    '                                    bExportoPayment = True
    '                                Else
    '                                    'If oPayment.I_Account = sI_Account Then
    '                                    ' 01.10.2018 – changed above If to:
    '                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
    '                                        If Len(oPayment.VB_ClientNo) > 0 Then
    '                                            If oPayment.VB_ClientNo = sClientNo Then
    '                                                bExportoPayment = True
    '                                            End If
    '                                        Else
    '                                            bExportoPayment = True
    '                                        End If
    '                                    End If
    '                                End If

    '                                If bExportoPayment Then
    '                                    If IsEqualAmount(oPayment.MON_InvoiceAmount, 0) Then
    '                                        bExportoPayment = False
    '                                        oPayment.Exported = True
    '                                    End If
    '                                End If


    '                                If bExportoPayment Then
    '                                    '                        'Should only export matched payments??
    '                                    '                        If oPayment.MATCH_Matched <> MatchStatus.Matched Then
    '                                    '                            bExportoPayment = False
    '                                    '                        End If
    '                                End If

    '                                If bExportoPayment Then

    '                                    sErrorName = oPayment.E_Name
    '                                    sErrorAmount = ConvertFromAmountToString(oPayment.MON_InvoiceAmount, vbNullString, ".")
    '                                    sErrorDate = oPayment.DATE_Payment
    '                                    sErrorTypeOfPosting = "General"
    '                                    sErrorField = "Reference"
    '                                    sErrorValue = ""

    '                                    sErrorField = "Validate Creditnotes"
    '                                    sValidationError = ""
    '                                    aValidationArray = ValidateCreditnotes(oPayment, sValidationError, False, False, True)

    '                                    If Not EmptyString(sValidationError) Then
    '                                        Err.Raise(1462, "WriteXML_Conecto", sValidationError)
    '                                    End If

    '                                    bNewDebtor = True
    '                                    nCreditTransactionAmount = 0 'XOKNET - 23.05.2011

    '                                    sFreetextFixed = vbNullString
    '                                    'XNET - 07.10.2011 - Removed next line
    '                                    'sQ4Text = vbNullString
    '                                    For Each oInvoice In oPayment.Invoices
    '                                        If oInvoice.MATCH_Original = True Then
    '                                            For Each oFreeText In oInvoice.Freetexts
    '                                                iFreetextCounter = iFreetextCounter + 1
    '                                                If iFreetextCounter = 1 Then
    '                                                    sFreetextFixed = Trim$(oFreeText.Text)
    '                                                ElseIf iFreetextCounter = 2 Then
    '                                                    sFreetextFixed = sFreetextFixed & " " & Trim$(oFreeText.Text)
    '                                                Else
    '                                                    Exit For
    '                                                End If
    '                                            Next oFreeText
    '                                        End If
    '                                        'XNET - 07.10.2011 - Removed next IF
    '                                        '                            If oInvoice.MATCH_Final And oInvoice.MON_InvoiceAmount <> 0 Then
    '                                        '                                For Each oFreeText In oInvoice.Freetexts
    '                                        '                                    If oFreeText.Qualifier = 4 Then
    '                                        '                                        ' Keyed into sprMatched;
    '                                        '                                        sQ4Text = sQ4Text & Trim$(oFreeText.Text)
    '                                        '                                    End If
    '                                        '                                Next oFreeText
    '                                        '                            End If
    '                                    Next oInvoice
    '                                    If Not EmptyString(oPayment.E_Name) Then
    '                                        sFreetextFixed = Trim$(oPayment.E_Name) & " - " & sFreetextFixed
    '                                    End If
    '                                    'XNET - 07.10.2011 - Removed next IF
    '                                    '                        If Not EmptyString(sQ4Text) Then
    '                                    '                            sFreetextFixed = sQ4Text & " " & sFreetextFixed
    '                                    '                        End If

    '                                    'XOKNET - 24.05.2011 - Added next For Each
    '                                    'Check if the posting of the payment incude a backpayment

    '                                    sErrorField = "Check for backpayment"
    '                                    sErrorValue = ""

    '                                    For Each oInvoice In oPayment.Invoices
    '                                        'Backpayment is marked as OnAccount payment (matched on customer)
    '                                        If oInvoice.MATCH_Final And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
    '                                            'The match_ID is set as UTBET
    '                                            If Trim(oInvoice.MyField) = "3" Then
    '                                                'Reset some values to make a correct XML-file
    '                                                oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice
    '                                                oInvoice.MyField = "3"
    '                                                If Not bPostAgainstObservationAccount Then '23.11.2012
    '                                                    oInvoice.MATCH_ID = "0"
    '                                                End If
    '                                            End If
    '                                        End If
    '                                    Next oInvoice

    '                                    'XOKNET - 11.10.2010 -
    '                                    'XNET - Added an IF - ASHLEY BARNES

    '                                    'GLHandling, commented next IF
    '                                    'If Not (RunTime()) Then
    '                                    '    bx = TreatConectoGLPostings(oPayment, True)
    '                                    'Else
    '                                    '    bx = TreatConectoGLPostings(oPayment, True)
    '                                    'End If

    '                                    'FIRST check if the payment is fully matched
    '                                    'If not write the whole payment amount as unmatched, not each invoice
    '                                    If oPayment.MATCH_Matched <> BabelFiles.MatchStatus.Matched Then

    '                                        sErrorTypeOfPosting = "Unmatched"
    '                                        nodTransaction = docXMLExport.createElement("Transaction")
    '                                        'Amount is set further down
    '                                        'nodTransaction.setAttribute "Amount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                        sErrorField = "BankReference"
    '                                        sErrorValue = oPayment.REF_Bank1
    '                                        nodTransaction.setAttribute("BankReference", oPayment.REF_Bank1)
    '                                        sErrorField = "VoucherDate"
    '                                        sErrorValue = oPayment.DATE_Payment
    '                                        nodTransaction.setAttribute("VoucherDate", Left$(oPayment.DATE_Payment, 4) & "-" & Mid$(oPayment.DATE_Payment, 5, 2) & "-" & Right$(oPayment.DATE_Payment, 2)) '2010-09-15
    '                                        'XNET - 07.10.2011 - Added next IF and changed the code
    '                                        If sQ4Text = "" Then
    '                                            sErrorField = "Description"
    '                                            sErrorValue = Left(sFreetextFixed, 50)
    '                                            nodTransaction.setAttribute("Description", sFreetextFixed)
    '                                        Else
    '                                            sErrorField = "Description"
    '                                            sErrorValue = Left(sQ4Text & " - " & sFreetextFixed, 50)
    '                                            nodTransaction.setAttribute("Description", sQ4Text & " - " & sFreetextFixed)
    '                                        End If
    '                                        If Len(oPayment.E_Account) = "11" Then
    '                                            If Not Mid$(oPayment.E_Account, 5, 1) = "9" Then
    '                                                sErrorField = "FromBankAccountNumber"
    '                                                sErrorValue = oPayment.E_Account
    '                                                nodTransaction.setAttribute("FromBankAccountNumber", oPayment.E_Account)
    '                                            Else
    '                                                sErrorField = "FromBankAccountNumber"
    '                                                sErrorValue = ""
    '                                                nodTransaction.setAttribute("FromBankAccountNumber", vbNullString)
    '                                            End If
    '                                        Else
    '                                            sErrorField = "FromBankAccountNumber"
    '                                            sErrorValue = ""
    '                                            nodTransaction.setAttribute("FromBankAccountNumber", vbNullString)
    '                                        End If
    '                                        If Not EmptyString(oPayment.Unique_ERPID) Then
    '                                            sErrorField = "UniqueERPID"
    '                                            sErrorValue = Trim(oPayment.Unique_ERPID)
    '                                            nodTransaction.setAttribute("UniqueERPID", Trim(oPayment.Unique_ERPID))
    '                                        Else
    '                                            'nodTransaction.setAttribute("UniqueERPID", "KOKO")
    '                                            Err.Raise(10894, "WriteXML_Conecto", "Could not find the unique paymentID that is mandatory on the payment." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                        End If
    '                                        If IsOCR(oPayment.PayCode) Then
    '                                            If oPayment.Invoices.Count = 1 Then
    '                                                sErrorField = "Kid"
    '                                                sErrorValue = oInvoice.Unique_Id
    '                                                nodTransaction.setAttribute("Kid", oInvoice.Unique_Id)
    '                                            Else
    '                                                sErrorField = "Kid"
    '                                                sErrorValue = vbNullString
    '                                                nodTransaction.setAttribute("Kid", vbNullString)
    '                                            End If
    '                                        Else
    '                                            sErrorField = "Kid"
    '                                            sErrorValue = vbNullString
    '                                            nodTransaction.setAttribute("Kid", vbNullString)
    '                                        End If
    '                                        'nodTransaction.setAttribute("Amount", "0.00")
    '                                        sErrorField = "Amount"
    '                                        sErrorValue = ConvertFromAmountToString(oPayment.MON_InvoiceAmount * -1, "", ".")
    '                                        nodTransaction.setAttribute("Amount", ConvertFromAmountToString(oPayment.MON_InvoiceAmount * -1, "", "."))
    '                                        'nodCustomerExists.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Math.Round(Val(nodCustomerExists.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((nSettleAmount * -1), 0), 0), vbNullString, ".")
    '                                        'xXAmount = 0
    '                                        sErrorField = "Payment_ID"
    '                                        sErrorValue = Trim(Str(oPayment.Index))
    '                                        nodTransaction.setAttribute("Payment_ID", Trim(Str(oPayment.Index)))

    '                                        sErrorField = "Creditor"
    '                                        sErrorValue = sClientNumber
    '                                        nodTransaction.setAttribute("Creditor", sClientNumber)
    '                                        'nodTransaction.setAttribute "RegistrationDate", Format(Now(), "YYYY-MM-DD") & "T" & Format(Now(), "hh:mm:ss") '2010-09-15
    '                                        'SettleAmount and SettleAmountTotal - Removed
    '                                        'nodTransaction.setAttribute "SettleAmount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                        'nodTransaction.setAttribute "SettleAmountTotal", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                        sErrorField = "appendChild: Transaction"
    '                                        nodbof_BalanceAccount_TransactionsList.appendChild(nodTransaction)

    '                                        nodTransactionDebtor = docXMLExport.createElement("Transaction.Debtor")
    '                                        sErrorField = "appendChild: TransactionDebtor"
    '                                        nodTransaction.appendChild(nodTransactionDebtor)

    '                                        nodDebtor = docXMLExport.createElement("Debtor")
    '                                        'XNET - 07.10.2011 - Changed according to info from Lars Hiim, this is the external customerno (payers custno in creditors system).
    '                                        If bPostAgainstObservationAccount Then
    '                                            sErrorField = "CustomerNumber"
    '                                            sErrorValue = sNoMatchAccount
    '                                            nodDebtor.setAttribute("CustomerNumber", sNoMatchAccount)
    '                                        Else
    '                                            sErrorField = "CustomerNumber"
    '                                            sErrorValue = sNoMatchAccount
    '                                            nodDebtor.setAttribute("CustomerNumber", sNoMatchAccount)
    '                                        End If
    '                                        'nodDebtor.setAttribute "AccountingId", oInvoice.CustomerNo
    '                                        sErrorField = "appendChild: Debtor"
    '                                        nodTransactionDebtor.appendChild(nodDebtor)

    '                                        nodTransaction_Settles = docXMLExport.createElement("Transaction.Settles")
    '                                        sErrorField = "appendChild: Transaction_Settles"
    '                                        nodTransaction.appendChild(nodTransaction_Settles)

    '                                        'Set nodSettleEntry = docXMLExport.createElement("bot:SettleDraft")
    '                                        'nodSettleEntry.setAttribute "x:TypeArguments", "bt:Settle"
    '                                        'nodSettleEntry.setAttribute "Capacity", "4"
    '                                        'nodTransaction_Settles.appendChild nodSettleEntry


    '                                        nodCustomerExists = nodTransaction

    '                                        'nCreditTransactionAmount = nCreditTransactionAmount + oPayment.MON_InvoiceAmount

    '                                    Else

    '                                        aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oPayment.MON_InvoiceAmount

    '                                        'SECONDLY, write the Invoices and On account posts
    '                                        For Each oInvoice In oPayment.Invoices

    '                                            If oInvoice.MATCH_Final And oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL And oInvoice.MON_InvoiceAmount > 0 Then

    '                                                sErrorTypeOfPosting = "Invoice/On account"
    '                                                sErrorInvoiceAmount = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ",")

    '                                                'Add a default clientno to the invoice if it's not stated
    '                                                If EmptyString(oInvoice.MATCH_ClientNo) Then
    '                                                    oInvoice.MATCH_ClientNo = sClientNumber
    '                                                End If

    '                                                'nodbof_BalanceAccount_TransactionsList is the bottom-node written under oBatch

    '                                                sQ4Text = vbNullString
    '                                                If oInvoice.MON_InvoiceAmount <> 0 Then
    '                                                    For Each oFreeText In oInvoice.Freetexts
    '                                                        If oFreeText.Qualifier = 4 Then
    '                                                            ' Keyed into sprMatched;
    '                                                            sQ4Text = sQ4Text & Trim$(oFreeText.Text)
    '                                                        End If
    '                                                    Next oFreeText
    '                                                    If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "611" Then
    '                                                        If Not EmptyString(oInvoice.Unique_Id) Then
    '                                                            If sQ4Text = "" Then
    '                                                                sQ4Text = "KID:" & Trim$(oInvoice.Unique_Id)
    '                                                            Else
    '                                                                sQ4Text = sQ4Text & " - " & Trim$(oInvoice.Unique_Id)
    '                                                            End If
    '                                                        End If
    '                                                    End If
    '                                                End If

    '                                                If oPayment.MATCH_Matched <> BabelFiles.MatchStatus.Matched Then
    '                                                    oInvoice.CustomerNo = sNoMatchAccount
    '                                                End If

    '                                                nodCustomerExists = Nothing

    '                                                'Check if we find a payment for this creditor and debtor already
    '                                                sErrorField = "XPATH: Try to find Creditor/Debtor"
    '                                                sXpath = ""
    '                                                sXpath = "Transaction[@Payment_ID = '"
    '                                                sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                sXpath = sXpath & "' and @Creditor = '"
    '                                                sXpath = sXpath & Trim(Str(oInvoice.MATCH_ClientNo)) '& "']"
    '                                                sXpath = sXpath & "' and Transaction.Debtor/Debtor[@CustomerNumber = '"
    '                                                If bPostAgainstObservationAccount Then
    '                                                    sXpath = sXpath & sNoMatchAccount
    '                                                Else
    '                                                    sXpath = sXpath & oInvoice.CustomerNo
    '                                                End If
    '                                                sXpath = sXpath & "']]"

    '                                                nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)

    '                                                'OK examples
    '                                                'Set nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode("Transaction[@Payment_ID = '1' and @VoucherDate = '2009-05-05']")
    '                                                'Set nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode("Transaction[@Payment_ID = '1' and Transaction.Debtor/Debtor]")
    '                                                'Set nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode("Transaction[@Payment_ID = '1' and Transaction.Debtor/Debtor[@CustomerNumber = '10046']]")
    '                                                '/UNB/UNG/GROUP/U
    '                                                If nodCustomerExists Is Nothing Then
    '                                                    bNewDebtor = True
    '                                                    sOldDebtor = oInvoice.CustomerNo
    '                                                Else
    '                                                    nodTransaction_Settles = Nothing
    '                                                    nodTransaction_Settles = nodCustomerExists.selectSingleNode("Transaction.Settles")
    '                                                    If nodTransaction_Settles Is Nothing Then
    '                                                        Err.Raise(10894, "WriteXML_Conecto", "Could not find the node Transaction.Settles." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    End If
    '                                                End If

    '                                                If bNewDebtor Then

    '                                                    bNewDebtor = False
    '                                                    nCreditTransactionAmount = 0

    '                                                    nodTransaction = docXMLExport.createElement("Transaction")
    '                                                    'Amount is set further down
    '                                                    'nodTransaction.setAttribute "Amount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                                    sErrorField = "BankReference"
    '                                                    sErrorValue = oPayment.REF_Bank1
    '                                                    nodTransaction.setAttribute("BankReference", oPayment.REF_Bank1)
    '                                                    sErrorField = "VoucherDate"
    '                                                    sErrorValue = oPayment.DATE_Payment
    '                                                    nodTransaction.setAttribute("VoucherDate", Left$(oPayment.DATE_Payment, 4) & "-" & Mid$(oPayment.DATE_Payment, 5, 2) & "-" & Right$(oPayment.DATE_Payment, 2)) '2010-09-15

    '                                                    'XNET - 07.10.2011 - Added next IF and changed the code
    '                                                    If sQ4Text = "" Then
    '                                                        sErrorField = "Description"
    '                                                        sErrorValue = Left(sFreetextFixed, 50)
    '                                                        nodTransaction.setAttribute("Description", sFreetextFixed)
    '                                                    Else
    '                                                        sErrorField = "Description"
    '                                                        sErrorValue = Left(sQ4Text & " - " & sFreetextFixed, 50)
    '                                                        nodTransaction.setAttribute("Description", sQ4Text & " - " & sFreetextFixed)
    '                                                    End If
    '                                                    If Len(oPayment.E_Account) = "11" Then
    '                                                        If Not Mid$(oPayment.E_Account, 5, 1) = "9" Then
    '                                                            sErrorField = "FromBankAccountNumber"
    '                                                            sErrorValue = oPayment.E_Account
    '                                                            nodTransaction.setAttribute("FromBankAccountNumber", oPayment.E_Account)
    '                                                        Else
    '                                                            sErrorField = "FromBankAccountNumber"
    '                                                            sErrorValue = ""
    '                                                            nodTransaction.setAttribute("FromBankAccountNumber", vbNullString)
    '                                                        End If
    '                                                    Else
    '                                                        sErrorField = "FromBankAccountNumber"
    '                                                        sErrorValue = ""
    '                                                        nodTransaction.setAttribute("FromBankAccountNumber", vbNullString)
    '                                                    End If
    '                                                    If Not EmptyString(oPayment.Unique_ERPID) Then
    '                                                        sErrorField = "UniqueERPID"
    '                                                        sErrorValue = Trim(oPayment.Unique_ERPID)
    '                                                        nodTransaction.setAttribute("UniqueERPID", Trim(oPayment.Unique_ERPID))
    '                                                    Else
    '                                                        'sErrorField = "UniqueERPID"
    '                                                        'sErrorValue = "KOKO"
    '                                                        'nodTransaction.setAttribute("UniqueERPID", "KOKO")
    '                                                        Err.Raise(10894, "WriteXML_Conecto", "Could not find the unique paymentID that is mandatory on the payment." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    End If
    '                                                    'nodTransaction.setAttribute "DocumentNumber", oPayment.VoucherNo
    '                                                    'nodTransaction.setAttribute "DueDate", "0001-01-01"
    '                                                    If IsOCR(oPayment.PayCode) Then
    '                                                        If oPayment.Invoices.Count = 1 Then
    '                                                            sErrorField = "Kid"
    '                                                            sErrorValue = oInvoice.Unique_Id
    '                                                            nodTransaction.setAttribute("Kid", oInvoice.Unique_Id)
    '                                                        Else
    '                                                            sErrorField = "Kid"
    '                                                            sErrorValue = oInvoice.Unique_Id
    '                                                            nodTransaction.setAttribute("Kid", vbNullString)
    '                                                        End If
    '                                                    Else
    '                                                        sErrorField = "Kid"
    '                                                        sErrorValue = oInvoice.Unique_Id
    '                                                        nodTransaction.setAttribute("Kid", vbNullString)
    '                                                    End If
    '                                                    sErrorField = "Amount"
    '                                                    sErrorValue = "0.00"
    '                                                    nodTransaction.setAttribute("Amount", "0.00")
    '                                                    'xXAmount = 0
    '                                                    sErrorField = "Payment_ID"
    '                                                    sErrorValue = Trim(Str(oPayment.Index))
    '                                                    nodTransaction.setAttribute("Payment_ID", Trim(Str(oPayment.Index)))

    '                                                    sErrorField = "Kid"
    '                                                    sErrorValue = oInvoice.Unique_Id
    '                                                    nodTransaction.setAttribute("Creditor", oInvoice.MATCH_ClientNo)
    '                                                    'nodTransaction.setAttribute "RegistrationDate", Format(Now(), "YYYY-MM-DD") & "T" & Format(Now(), "hh:mm:ss") '2010-09-15
    '                                                    'SettleAmount and SettleAmountTotal - Removed
    '                                                    'nodTransaction.setAttribute "SettleAmount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                                    'nodTransaction.setAttribute "SettleAmountTotal", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                                    sErrorField = "AppendChild: Transaction"
    '                                                    nodbof_BalanceAccount_TransactionsList.appendChild(nodTransaction)

    '                                                    sErrorField = "AppendChild: Transaction.Debtor"
    '                                                    nodTransactionDebtor = docXMLExport.createElement("Transaction.Debtor")
    '                                                    nodTransaction.appendChild(nodTransactionDebtor)

    '                                                    nodDebtor = docXMLExport.createElement("Debtor")
    '                                                    'XNET - 07.10.2011 - Changed according to info from Lars Hiim, this is the external customerno (payers custno in creditors system).
    '                                                    If bPostAgainstObservationAccount Then
    '                                                        sErrorField = "CustomerNumber"
    '                                                        sErrorValue = sNoMatchAccount
    '                                                        nodDebtor.setAttribute("CustomerNumber", sNoMatchAccount)
    '                                                    Else
    '                                                        sErrorField = "CustomerNumber"
    '                                                        sErrorValue = oInvoice.CustomerNo
    '                                                        nodDebtor.setAttribute("CustomerNumber", oInvoice.CustomerNo)
    '                                                    End If
    '                                                    'nodDebtor.setAttribute "AccountingId", oInvoice.CustomerNo
    '                                                    sErrorField = "AppendChild: Debtor"
    '                                                    nodTransactionDebtor.appendChild(nodDebtor)

    '                                                    nodTransaction_Settles = docXMLExport.createElement("Transaction.Settles")
    '                                                    sErrorField = "AppendChild: Transaction_Settles"
    '                                                    nodTransaction.appendChild(nodTransaction_Settles)

    '                                                    'Set nodSettleEntry = docXMLExport.createElement("bot:SettleDraft")
    '                                                    'nodSettleEntry.setAttribute "x:TypeArguments", "bt:Settle"
    '                                                    'nodSettleEntry.setAttribute "Capacity", "4"
    '                                                    'nodTransaction_Settles.appendChild nodSettleEntry


    '                                                    nodCustomerExists = nodTransaction

    '                                                    nCreditTransactionAmount = nCreditTransactionAmount + oInvoice.MON_InvoiceAmount
    '                                                Else
    '                                                    nCreditTransactionAmount = nCreditTransactionAmount + oInvoice.MON_InvoiceAmount
    '                                                End If

    '                                                If (oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice And oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched) Or _
    '                                                (bPostAgainstObservationAccount = True And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer And oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched) Then
    '                                                    nSettleAmount = 0


    '                                                    nSettleAmount = nSettleAmount + oInvoice.MON_InvoiceAmount

    '                                                    nodbot_SettleDraft = docXMLExport.createElement("bot:SettleDraft")
    '                                                    'Added after adjustments are written
    '                                                    'nodbot_SettleDraft.setAttribute "Amount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")

    '                                                    'KOKO
    '                                                    'If bPostAgainstObservationAccount Then
    '                                                    'nodbot_SettleDraft.setAttribute("ExcludeFromTotalSum", "True")
    '                                                    'Else
    '                                                    sErrorField = "ExcludeFromTotalSum"
    '                                                    sErrorValue = "False"
    '                                                    nodbot_SettleDraft.setAttribute("ExcludeFromTotalSum", "False")
    '                                                    'End If
    '                                                    If Trim(oInvoice.MyField) = "0" Or Trim(oInvoice.MyField) = "3" Then 'XOKNET 24.05.2011 - Added Or oInvoice.MyField = "3" Then
    '                                                        sErrorField = "IsReminder"
    '                                                        sErrorValue = "False"
    '                                                        nodbot_SettleDraft.setAttribute("IsReminder", "False")
    '                                                    Else
    '                                                        sErrorField = "IsReminder"
    '                                                        sErrorValue = "True"
    '                                                        nodbot_SettleDraft.setAttribute("IsReminder", "True")
    '                                                    End If
    '                                                    If bPostAgainstObservationAccount Then
    '                                                        sErrorField = "CreditNotaSettleId"
    '                                                        sErrorValue = Trim(oPayment.MATCH_MatchingIDAgainstObservationAccount)
    '                                                        nodbot_SettleDraft.setAttribute("CreditNotaSettleId", Trim(oPayment.MATCH_MatchingIDAgainstObservationAccount))
    '                                                        sErrorField = "DebtorIDForCreditNotes"
    '                                                        sErrorValue = oInvoice.CustomerNo
    '                                                        nodbot_SettleDraft.setAttribute("DebtorIDForCreditNotes", oInvoice.CustomerNo)
    '                                                    End If
    '                                                    'XokNET - 31.01.2011 - Changed the line below after instructions from Lars
    '                                                    If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then 'Only when bPostAgainstObservationAccount (an earlier IF)
    '                                                        If bPostAgainstObservationAccount Then
    '                                                            sErrorField = "MoveToDebtorId"
    '                                                            sErrorValue = Trim$(oInvoice.MATCH_ID)
    '                                                            nodbot_SettleDraft.setAttribute("MoveToDebtorId", Trim$(oInvoice.MATCH_ID))
    '                                                            sErrorField = "Deductable"
    '                                                            nodbot_SettleDraft.setAttribute("Deductable", "No")
    '                                                        Else
    '                                                            'Should never enter this Else part because we will never match OnAccount after automatic matching (against FA)
    '                                                            sErrorField = "SettledTransactionId"
    '                                                            sErrorValue = Trim$(oInvoice.MATCH_ID)
    '                                                            nodbot_SettleDraft.setAttribute("SettledTransactionId", Trim$(oInvoice.MATCH_ID))
    '                                                            sErrorField = "Deductable"
    '                                                            nodbot_SettleDraft.setAttribute("Deductable", "No")
    '                                                        End If
    '                                                    Else
    '                                                        If bPostAgainstObservationAccount And Trim(oInvoice.MyField) = "3" Then '23.11.2012 - Backpayment mus have a MoveToDebtorID 
    '                                                            sErrorField = "MoveToDebtorId"
    '                                                            sErrorValue = Trim$(oInvoice.MATCH_ID)
    '                                                            nodbot_SettleDraft.setAttribute("MoveToDebtorId", Trim$(oInvoice.MATCH_ID))
    '                                                            sErrorField = "Deductable"
    '                                                            nodbot_SettleDraft.setAttribute("Deductable", "No")
    '                                                        Else
    '                                                            sErrorField = "SettledTransactionId"
    '                                                            sErrorValue = Trim$(oInvoice.MATCH_ID)
    '                                                            nodbot_SettleDraft.setAttribute("SettledTransactionId", Trim$(oInvoice.MATCH_ID))
    '                                                            sErrorField = "Deductable"
    '                                                            nodbot_SettleDraft.setAttribute("Deductable", "Yes")
    '                                                        End If
    '                                                    End If
    '                                                    'nodbot_SettleDraft.setAttribute "MainTransactionId", Trim$(oInvoice.MATCH_ID)

    '                                                    '19.12.2011 - Added next IF because we expierienced that RestAmountType was equal to ""
    '                                                    If EmptyString(oInvoice.MyField) Then
    '                                                        sErrorField = "RestAmountType"
    '                                                        sErrorValue = "0"
    '                                                        nodbot_SettleDraft.setAttribute("RestAmountType", "0")
    '                                                    Else
    '                                                        sErrorField = "RestAmountType"
    '                                                        sErrorValue = Trim(oInvoice.MyField)
    '                                                        nodbot_SettleDraft.setAttribute("RestAmountType", Trim(oInvoice.MyField))
    '                                                    End If
    '                                                    sErrorField = "AppendChild: SettleDraft"
    '                                                    nodTransaction_Settles.appendChild(nodbot_SettleDraft)

    '                                                    'Add the settle amount. It may have been adjusted with GL-postings
    '                                                    ' XNET 30.04.2012
    '                                                    sErrorField = "Amount (Settle)"
    '                                                    sErrorValue = ConvertFromAmountToString(Math.Round(nSettleAmount, 2), vbNullString, ".")
    '                                                    nodbot_SettleDraft.setAttribute("Amount", ConvertFromAmountToString(Math.Round(nSettleAmount, 2), vbNullString, "."))

    '                                                    'Change the amount stated on the transaction (amount paid to this customer in this payment).
    '                                                    'The amount on the credit level for the last credit transaction
    '                                                    'xXAmount = Val(nodCustomerExists.Attributes.getNamedItem("XAmount").nodeValue) * 100 + (nSettleAmount * -1)
    '                                                    'Adjust the debtoramount
    '                                                    'XNET 30.04.2012
    '                                                    If Not bPostAgainstObservationAccount Then
    '                                                        sErrorField = "Amount(Customer)"
    '                                                        sErrorValue = "To be added: " & ConvertFromAmountToString(Math.Round((nSettleAmount * -1), 0), ".", ",")
    '                                                        nodCustomerExists.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Math.Round(Val(nodCustomerExists.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((nSettleAmount * -1), 0), 0), vbNullString, ".")
    '                                                        'Adjust amount on the batch level
    '                                                        'XNET 30.04.2012
    '                                                        'nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Math.Round(Val(nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((nSettleAmount), 0), 0), vbNullString, ".") '* -1
    '                                                        'Set nodTransactionAmount = nodCustomerExists.selectSingleNode("")
    '                                                    End If

    '                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer And oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then
    '                                                    'xXAmount = Val(nodCustomerExists.Attributes.getNamedItem("XAmount").nodeValue) * 100 + (oInvoice.MON_InvoiceAmount * -1)
    '                                                    If Not bPostAgainstObservationAccount Then
    '                                                        sErrorField = "Amount(Customer - On Account)"
    '                                                        sErrorValue = "To be added: " & ConvertFromAmountToString(Math.Round((oInvoice.MON_InvoiceAmount * -1), 0), ".", ",")
    '                                                        nodCustomerExists.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString((Val(nodCustomerExists.attributes.getNamedItem("Amount").nodeValue)) * 100 + (oInvoice.MON_InvoiceAmount * -1), vbNullString, ".")
    '                                                        sErrorField = "Deductable"
    '                                                        nodbot_SettleDraft.setAttribute("Deductable", "No")
    '                                                        'Adjust amount on the batch level
    '                                                        'XNET 30.04.2012
    '                                                        'nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Math.Round(Val(nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((oInvoice.MON_InvoiceAmount), 0), 0), vbNullString, ".") '* -1
    '                                                    End If

    '                                                ElseIf oPayment.MATCH_Matched < BabelFiles.MatchStatus.Matched Then
    '                                                    If Not bPostAgainstObservationAccount Then
    '                                                        sErrorField = "Amount(Customer - Not matched)"
    '                                                        sErrorValue = "To be added: " & ConvertFromAmountToString(Math.Round((oInvoice.MON_InvoiceAmount * -1), 0), ".", ",")
    '                                                        'XNET 30.04.2012
    '                                                        nodCustomerExists.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Math.Round(Val(nodCustomerExists.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((oInvoice.MON_InvoiceAmount * -1), 0), 0), vbNullString, ".")
    '                                                        'Adjust amount on the batch level
    '                                                        'nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Math.Round(Val(nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((oInvoice.MON_InvoiceAmount), 0), 0), vbNullString, ".") '* -1
    '                                                    End If

    '                                                End If

    '                                            End If

    '                                        Next oInvoice

    '                                        'Add correct amount on the credit level for the last credit transaction
    '                                        'nodTransaction.setAttribute "Amount", ConvertFromAmountToString(nCreditTransactionAmount * -1, vbNullString, ".")

    '                                        '                        If nCreditTransactionAmount < 0 Then
    '                                        '                            nodTransaction.setAttribute "CreditAmount", "0"
    '                                        '                            nodTransaction.setAttribute "DebitAmount", ConvertFromAmountToString(Abs(nCreditTransactionAmount), vbNullString, ".")
    '                                        '                        Else
    '                                        '                            nodTransaction.setAttribute "CreditAmount", ConvertFromAmountToString(nCreditTransactionAmount, vbNullString, ".")
    '                                        '                            nodTransaction.setAttribute "DebitAmount", "0"
    '                                        '                        End If

    '                                        'Now, write the creditnotes
    '                                        For Each oInvoice In oPayment.Invoices
    '                                            If oInvoice.MATCH_Final And oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL And oInvoice.MON_InvoiceAmount < 0 Then

    '                                                sErrorTypeOfPosting = "Creditnote"
    '                                                sErrorInvoiceAmount = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ",")

    '                                                sQ4Text = vbNullString
    '                                                If oInvoice.MON_InvoiceAmount <> 0 Then
    '                                                    For Each oFreeText In oInvoice.Freetexts
    '                                                        If oFreeText.Qualifier = 4 Then
    '                                                            ' Keyed into sprMatched;
    '                                                            sQ4Text = sQ4Text & Trim$(oFreeText.Text)
    '                                                        End If
    '                                                    Next oFreeText
    '                                                    If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "611" Then
    '                                                        If Not EmptyString(oInvoice.Unique_Id) Then
    '                                                            If sQ4Text = "" Then
    '                                                                sQ4Text = "KID:" & Trim$(oInvoice.Unique_Id)
    '                                                            Else
    '                                                                sQ4Text = sQ4Text & " - " & Trim$(oInvoice.Unique_Id)
    '                                                            End If
    '                                                        End If
    '                                                    End If
    '                                                End If

    '                                                nCreditnoteAmount = oInvoice.MON_InvoiceAmount * -1

    '                                                Do While nCreditnoteAmount > 0.01
    '                                                    'First try to find invoices posted on the same creditor and debtor within this balanceposting (batch)
    '                                                    nodCreditorExists = Nothing

    '                                                    sErrorField = "XPATH: Try to find the Creditor/Debtor"
    '                                                    sXpath = ""
    '                                                    sXpath = "Transaction[@Payment_ID = '"
    '                                                    sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                    sXpath = sXpath & "' and @Creditor = '"
    '                                                    'oInvoice.MATCH_ClientNo = "35570"
    '                                                    'oInvoice.CustomerNo = "96127"
    '                                                    sXpath = sXpath & Trim(Str(oInvoice.MATCH_ClientNo)) '& "']"
    '                                                    If bPostAgainstObservationAccount Then
    '                                                        sXpath = sXpath & "' and Transaction.Settles/SettleDraft[@Amount != '0.00']"
    '                                                    Else
    '                                                        sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                    End If
    '                                                    If bPostAgainstObservationAccount Then
    '                                                        sXpath = sXpath & " and Transaction.Settles/SettleDraft[@DebtorIDForCreditNotes = '"
    '                                                        sXpath = sXpath & oInvoice.CustomerNo
    '                                                    Else
    '                                                        sXpath = sXpath & " and Transaction.Debtor/Debtor[@CustomerNumber = '"
    '                                                        sXpath = sXpath & oInvoice.CustomerNo
    '                                                    End If
    '                                                    sXpath = sXpath & "']" & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']]"

    '                                                    nodCreditorExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)

    '                                                    If nodCreditorExists Is Nothing Then
    '                                                        'No invoices to eat on the same creditor and debtor, try to find invoices on the creditor

    '                                                        sErrorField = "XPATH: Try to find a Creditor"
    '                                                        If bPostAgainstObservationAccount Then
    '                                                            sXpath = ""
    '                                                            sXpath = "Transaction[@Payment_ID = '"
    '                                                            sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                            sXpath = sXpath & "' and @Creditor = '"
    '                                                            sXpath = sXpath & Trim(Str(oInvoice.MATCH_ClientNo)) '& "']"
    '                                                            'sXpath = sXpath & "' and Transaction.Settles/SettleDraft[@Deductable = 'True']]"
    '                                                            sXpath = sXpath & "' and Transaction.Settles/SettleDraft[@Amount != '0.00' and @Deductable = 'Yes' and @ExcludeFromTotalSum = 'False']]"



    '                                                            'sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"
    '                                                            'sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False'] and Transaction.Settles/SettleDraft[ @SettledTransactionId ]]"

    '                                                            'sXpath = sXpath & " and Transaction.Settles/SettleDraft[@SettledTransactionId != 'False'] and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"

    '                                                            'sXpath = sXpath & " and Transaction.Settles/SettleDraft[@Deductable = 'True'] and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"


    '                                                            ''sXpath = ""
    '                                                            ''sXpath = "Transaction[@Payment_ID = '"
    '                                                            ''sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                            ''sXpath = sXpath & "' and @Creditor = '"
    '                                                            ''sXpath = sXpath & Trim(Str(oInvoice.MATCH_ClientNo)) '& "']"
    '                                                            ''sXpath = sXpath & "' and Transaction.Settles/SettleDraft[@Amount != '0.00']"
    '                                                            ''sXpath = sXpath & " and Transaction.Settles/SettleDraft[@Deductable = 'True'] and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"
    '                                                            'IKKE MoveToDebtorId
    '                                                            'author[degree and not(publication)]
    '                                                            'Find elements that have a given attribute Synatx: XML_element_name [ @attribute_name ] 

    '                                                            'KOFEILKO
    '                                                        Else
    '                                                            sXpath = ""
    '                                                            sXpath = "Transaction[@Payment_ID = '"
    '                                                            sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                            sXpath = sXpath & "' and @Creditor = '"
    '                                                            sXpath = sXpath & Trim(Str(oInvoice.MATCH_ClientNo)) '& "']"
    '                                                            sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                            sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']]"
    '                                                        End If

    '                                                        nodCreditorExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)

    '                                                        If nodCreditorExists Is Nothing Then
    '                                                            'No invoices to eat on the same creditor, just ick a random creditor
    '                                                            sErrorField = "XPATH: Find the first invoice"
    '                                                            If bPostAgainstObservationAccount Then

    '                                                                sXpath = ""
    '                                                                sXpath = "Transaction[@Payment_ID = '"
    '                                                                sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                                sXpath = sXpath & "' and Transaction.Settles/SettleDraft[@Amount != '0.00']"
    '                                                                'sXpath = sXpath & " and Transaction.Settles/SettleDraft[@RestAmountType = '0']]"
    '                                                                sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']]"
    '                                                            Else
    '                                                                sXpath = ""
    '                                                                sXpath = "Transaction[@Payment_ID = '"
    '                                                                sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                                sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                                sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']]"
    '                                                            End If

    '                                                            nodCreditorExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)
    '                                                        End If

    '                                                    End If

    '                                                    If nodCreditorExists Is Nothing Then
    '                                                        Err.Raise(10894, "WriteXML_Conecto", "Could not find a valid BalanceAccount node when posting creditnotes." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    Else
    '                                                        nodbof_BalanceAccount = nodCreditorExists
    '                                                        'KOKOnodbof_BalanceAccount_TransactionsList = nodCreditorExists.selectSingleNode("BalanceAccount.Transactions/List")
    '                                                    End If

    '                                                    'Move to the Transaction.Settles node
    '                                                    nodTransaction_Settles = Nothing
    '                                                    sErrorField = "XPATH: Select the settles"
    '                                                    nodTransaction_Settles = nodCreditorExists.selectSingleNode("Transaction.Settles")
    '                                                    If nodTransaction_Settles Is Nothing Then
    '                                                        Err.Raise(10894, "WriteXML_Conecto", "Could not find the node Transaction.Settles when writing creditnotes." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    End If

    '                                                    'Create a list of the settledrafts on this transaction
    '                                                    'Set nodlistSettleDrafts = nodTransaction_Settles.selectNodes("SettleDraft")
    '                                                    sErrorField = "XPATH: Select a list of the SettleDraft"
    '                                                    nodlistSettleDrafts = nodTransaction_Settles.selectNodes("SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']")
    '                                                    'Find the first SettleDraft which we may use for deducting the invoice

    '                                                    bRemoveActiveSettleDraft = False
    '                                                    For i = 0 To nodlistSettleDrafts.length - 1
    '                                                        nodbot_SettleDraftInvoice = nodlistSettleDrafts.item(i)
    '                                                        ' XNET 30.04.2012
    '                                                        nInvoiceAmount = Math.Round(Val(nodbot_SettleDraftInvoice.attributes.getNamedItem("Amount").nodeValue) * 100, 0)

    '                                                        If nInvoiceAmount > nCreditnoteAmount Then
    '                                                            nAmountToUse = nCreditnoteAmount
    '                                                        Else
    '                                                            nAmountToUse = nInvoiceAmount
    '                                                            bRemoveActiveSettleDraft = True
    '                                                        End If

    '                                                        nCreditnoteAmount = nCreditnoteAmount - nAmountToUse

    '                                                        If nAmountToUse > 0 Then
    '                                                            Exit For
    '                                                        End If

    '                                                    Next i

    '                                                    nodbot_SettleDraft = docXMLExport.createElement("bot:SettleDraft")
    '                                                    'Added after adjustments are written
    '                                                    'nodbot_SettleDraft.setAttribute "Amount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                                    sErrorField = "ExcludeFromTotalSum"
    '                                                    sErrorValue = "True"
    '                                                    nodbot_SettleDraft.setAttribute("ExcludeFromTotalSum", "True")
    '                                                    sErrorField = "IsReminder"
    '                                                    sErrorValue = "False"
    '                                                    nodbot_SettleDraft.setAttribute("IsReminder", "False")
    '                                                    sErrorField = "CreditNotaSettleId"
    '                                                    sErrorValue = Trim$(oInvoice.MATCH_ID)
    '                                                    nodbot_SettleDraft.setAttribute("CreditNotaSettleId", Trim$(oInvoice.MATCH_ID)) ', nodbot_SettleDraftInvoice.attributes.getNamedItem("SettledTransactionId").nodeValue)
    '                                                    sErrorField = "SettledTransactionId"
    '                                                    sErrorValue = nodbot_SettleDraftInvoice.attributes.getNamedItem("SettledTransactionId").nodeValue
    '                                                    nodbot_SettleDraft.setAttribute("SettledTransactionId", nodbot_SettleDraftInvoice.attributes.getNamedItem("SettledTransactionId").nodeValue) ', Trim$(oInvoice.MATCH_ID))
    '                                                    sErrorField = "RestAmountType"
    '                                                    sErrorValue = Trim(oInvoice.MyField)
    '                                                    nodbot_SettleDraft.setAttribute("RestAmountType", Trim(oInvoice.MyField))
    '                                                    sErrorField = "AppendChild: SettleDraft"
    '                                                    nodTransaction_Settles.appendChild(nodbot_SettleDraft)

    '                                                    ' XNET 30.04.2012
    '                                                    sErrorField = "Amount (Settle)"
    '                                                    sErrorValue = ConvertFromAmountToString(Math.Round(nAmountToUse, 2), vbNullString, ".")
    '                                                    nodbot_SettleDraft.setAttribute("Amount", ConvertFromAmountToString(Math.Round(nAmountToUse, 0), "", "."))

    '                                                    If bRemoveActiveSettleDraft Then
    '                                                        'Set koko = nodlistSettleDrafts.Item(CLng(i))
    '                                                        sErrorField = "XPATH: Remove a SettleDraft"
    '                                                        nodTemp = nodlistSettleDrafts.item(CLng(i)).parentNode.removeChild(nodlistSettleDrafts.item(CLng(i)))
    '                                                        'nodlistSettleDrafts.Reset
    '                                                        nodTemp = Nothing
    '                                                        'Set nodlistSettleDrafts = nodTransaction_Settles.selectNodes("SettleDraft")
    '                                                        'nodlistSettleDrafts.Item(CLng(i)).parentNode.removeChild (nodlistSettleDrafts.Item(CLng(i)))
    '                                                        'nodTransaction_Settles.removeChild (nodlistSettleDrafts.Item(CLng(i)))
    '                                                        'nodbot_SettleDraftInvoice.parentNode.removeChild (nodbot_SettleDraftInvoice)
    '                                                    Else
    '                                                        'Adjust the amount on the settle of the invoice
    '                                                        ' XNET 30.04.2012
    '                                                        'If Not bPostAgainstObservationAccount Then
    '                                                        sErrorField = "Amount(SettleDraft)"
    '                                                        sErrorValue = "To be added: " & ConvertFromAmountToString(Math.Round((nAmountToUse * -1), 0), ".", ",")
    '                                                        nodbot_SettleDraftInvoice.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Val(nodbot_SettleDraftInvoice.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((nAmountToUse * -1), 0), vbNullString, ".")
    '                                                        'End If
    '                                                    End If

    '                                                    'Change the amount stated on the transaction (amount paid to this customer in this payment).
    '                                                    'The amount on the credit level for the last credit transaction

    '                                                    ' XKJELL - det er her det blir feil for Conecto
    '                                                    ' nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue returnerer f.ex. 1.36424205265939E-.12
    '                                                    ' Jeg har tatt en spansk en for å få det til å funke -
    '                                                    ' Første If-en er sannsynligvis ikke nødvendig.
    '                                                    ' Det viktige tror jeg er Round i Else-en
    '                                                    'If InStr(nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue, "E-.") > 0 Then
    '                                                    '    nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(nAmountToUse, vbNullString, ".")
    '                                                    'Else
    '                                                    'nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString((Val(nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue)) * 100 + (nAmountToUse), vbNullString, ".")
    '                                                    'XNET ca 20.04.2012
    '                                                    If Not bPostAgainstObservationAccount Then
    '                                                        sErrorField = "Amount(Creditor)"
    '                                                        sErrorValue = "To be added: " & ConvertFromAmountToString(Math.Round((nAmountToUse * -1), 0), ".", ",")
    '                                                        nodCreditorExists.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round((Val(nodCreditorExists.attributes.getNamedItem("Amount").nodeValue)) * 100 + (nAmountToUse), 0), vbNullString, ".")
    '                                                    End If
    '                                                    'End If
    '                                                    'Adjust amount on the batch level- not influenced by creditnotes????
    '                                                    ' XKJELL - det er her det opprinnelig ble  feil for Conecto
    '                                                    ' nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue returnerer f.ex. 1.36424205265939E-.12
    '                                                    ' Her tester vi på om det er en slik spesialverdi
    '                                                    ' Slår sannsynligvis ikke til, da det er løst i kodelinjene over med Round.
    '                                                    ' VI MÅ SE PÅ DETTE SAMMEN
    '                                                    'If InStr(nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue, "E-.") > 0 Then
    '                                                    '    nodbof_BalanceAccount.Attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString((nAmountToUse * -1), vbNullString, ".")
    '                                                    'Else
    '                                                    'XNET ca 20.04.2012
    '                                                    'nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round((Val(nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue)) * 100 + (nAmountToUse * -1), 0), vbNullString, ".") '* -1
    '                                                    'End If

    '                                                Loop
    '                                            End If

    '                                        Next oInvoice

    '                                        '******************************* GL Handling new code
    '                                        'Now, write the GL-posts
    '                                        For Each oInvoice In oPayment.Invoices
    '                                            If oInvoice.MATCH_Final And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then

    '                                                sErrorTypeOfPosting = "GL Posting"
    '                                                sErrorInvoiceAmount = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ",")
    '                                                sQ4Text = vbNullString
    '                                                If oInvoice.MON_InvoiceAmount <> 0 Then
    '                                                    For Each oFreeText In oInvoice.Freetexts
    '                                                        If oFreeText.Qualifier = 4 Then
    '                                                            ' Keyed into sprMatched;
    '                                                            sQ4Text = sQ4Text & Trim$(oFreeText.Text)
    '                                                        End If
    '                                                    Next oFreeText
    '                                                End If

    '                                                nGLAmount = oInvoice.MON_InvoiceAmount * -1

    '                                                Do While Not IsEqualAmount(nGLAmount, 0.0)
    '                                                    'First move to a debitor with a positiv amount
    '                                                    nodCreditorExists = Nothing

    '                                                    sErrorField = "XPATH: Find debtor"
    '                                                    If bPostAgainstObservationAccount Then


    '                                                        ''Check if we find a payment for this creditor and debtor already
    '                                                        'sXpath = ""
    '                                                        'sXpath = "Transaction[@Payment_ID = '"
    '                                                        'sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                        'sXpath = sXpath & "' and @Creditor = '"
    '                                                        'sXpath = sXpath & Trim(Str(oInvoice.MATCH_ClientNo)) '& "']"
    '                                                        'sXpath = sXpath & "' and Transaction.Debtor/Debtor[@CustomerNumber = '"
    '                                                        'If bPostAgainstObservationAccount Then
    '                                                        '    sXpath = sXpath & sNoMatchAccount
    '                                                        'Else
    '                                                        '    sXpath = sXpath & oInvoice.CustomerNo
    '                                                        'End If
    '                                                        'sXpath = sXpath & "']]"


    '                                                        sXpath = ""
    '                                                        sXpath = "Transaction[@Payment_ID = '"
    '                                                        sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                        sXpath = sXpath & "' and Transaction.Settles/SettleDraft[@Amount != '0.00']"
    '                                                        'sXpath = sXpath & " and Transaction.Settles/SettleDraft[@RestAmountType = '0']]"
    '                                                        sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']]"
    '                                                    Else
    '                                                        sXpath = ""
    '                                                        sXpath = "Transaction[@Payment_ID = '"
    '                                                        sXpath = sXpath & Trim(Str(oPayment.Index)) '& "']"
    '                                                        sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                        sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']]"
    '                                                    End If

    '                                                    nodCreditorExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)

    '                                                    'sXpath = ""
    '                                                    'sXpath = "BalanceAccount[@BalanceAccountId = '"
    '                                                    'sXpath = sXpath & Trim(Str(oBabel.Index)) & "-" & Trim(Str(oBatch.Index))
    '                                                    'sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                    'sXpath = sXpath & " and BalanceAccount.Transactions/List/Transaction[@Payment_ID = '"
    '                                                    'sXpath = sXpath & Trim(Str(oPayment.Index))
    '                                                    'sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                    'sXpath = sXpath & "] and BalanceAccount.Transactions/List/Transaction/Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"

    '                                                    'nodCreditorExists = nodImportData_BalanceAccountsList.selectSingleNode(sXpath)

    '                                                    If nodCreditorExists Is Nothing Then
    '                                                        Err.Raise(10894, "WriteXML_Conecto", "Could not find a valid BalanceAccount node when posting GL." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    Else
    '                                                        'OK
    '                                                        'nodbof_BalanceAccount = nodCreditorExists
    '                                                        'KOKOnodbof_BalanceAccount_TransactionsList = nodCreditorExists.selectSingleNode("BalanceAccount.Transactions/List")
    '                                                    End If

    '                                                    'sXpath = ""
    '                                                    'sXpath = "Transaction[@Payment_ID = '"
    '                                                    'sXpath = sXpath & Trim(Str(oPayment.Index))
    '                                                    'sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                    'sXpath = sXpath & " and Transaction.Debtor/Debtor[@CustomerNumber = '"
    '                                                    'sXpath = sXpath & oInvoice.CustomerNo
    '                                                    'sXpath = sXpath & "'] and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"

    '                                                    'nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)

    '                                                    'If nodCustomerExists Is Nothing Then
    '                                                    '    'Can't deduct the creditnote from the same customer find another, with the same creditor (fordringshaver)
    '                                                    '    sXpath = ""
    '                                                    '    sXpath = "Transaction[@Payment_ID = '"
    '                                                    '    sXpath = sXpath & Trim(Str(oPayment.Index))
    '                                                    '    sXpath = sXpath & "' and @Amount != '0.00'"
    '                                                    '    sXpath = sXpath & " and Transaction.Settles/SettleDraft[@ExcludeFromTotalSum = 'False']]"

    '                                                    '    nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)

    '                                                    '    If nodCustomerExists Is Nothing Then
    '                                                    '        Err.Raise(10894, "WriteXML_Conecto", "Could not find a valid Transaction node when posting to GL." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    '    End If

    '                                                    'End If
    '                                                    'Move to the Transaction.Settles node
    '                                                    sErrorField = "XPATH: Find settles"
    '                                                    nodTransaction_Settles = Nothing
    '                                                    nodTransaction_Settles = nodCreditorExists.selectSingleNode("Transaction.Settles")
    '                                                    If nodTransaction_Settles Is Nothing Then
    '                                                        Err.Raise(10894, "WriteXML_Conecto", "Could not find the node Transaction.Settles when posting to GL." & vbCrLf & vbCrLf & "Payer: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", "."))
    '                                                    End If

    '                                                    'Create a list of the settledrafts on this transaction
    '                                                    'Set nodlistSettleDrafts = nodTransaction_Settles.selectNodes("SettleDraft")
    '                                                    sErrorField = "XPATH: Find SettleDraft"
    '                                                    nodlistSettleDrafts = nodTransaction_Settles.selectNodes("SettleDraft[@ExcludeFromTotalSum = 'False' and @Deductable = 'Yes']")
    '                                                    'Find the first SettleDraft which we may use for posting the GL posting

    '                                                    bRemoveActiveSettleDraft = False
    '                                                    If nGLAmount > 0 Then 'Not necessary to find an invoice when it is paid to much (nGLAmount is negative)
    '                                                        For i = 0 To nodlistSettleDrafts.length - 1
    '                                                            nodbot_SettleDraftInvoice = nodlistSettleDrafts.item(i)
    '                                                            ' XNET 30.04.2012
    '                                                            sErrorField = "XPATH: Validate amount"
    '                                                            nInvoiceAmount = Math.Round(Val(nodbot_SettleDraftInvoice.attributes.getNamedItem("Amount").nodeValue) * 100, 0)

    '                                                            If nInvoiceAmount > nGLAmount Then
    '                                                                nAmountToUse = nGLAmount
    '                                                            Else
    '                                                                nAmountToUse = nInvoiceAmount
    '                                                                bRemoveActiveSettleDraft = True
    '                                                            End If

    '                                                            nGLAmount = nGLAmount - nAmountToUse

    '                                                            If nAmountToUse > 0 Then
    '                                                                Exit For
    '                                                            End If

    '                                                        Next i
    '                                                    Else
    '                                                        nAmountToUse = nGLAmount
    '                                                        nGLAmount = 0
    '                                                    End If

    '                                                    nodbot_SettleDraft = docXMLExport.createElement("bot:SettleDraft")
    '                                                    'Added after adjustments are written
    '                                                    'nodbot_SettleDraft.setAttribute "Amount", ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
    '                                                    If nAmountToUse > 0 Then
    '                                                        sErrorField = "ExcludeFromTotalSum"
    '                                                        sErrorValue = "True"
    '                                                        nodbot_SettleDraft.setAttribute("ExcludeFromTotalSum", "True")
    '                                                    Else
    '                                                        sErrorField = "ExcludeFromTotalSum"
    '                                                        sErrorValue = "False"
    '                                                        nodbot_SettleDraft.setAttribute("ExcludeFromTotalSum", "False")
    '                                                    End If
    '                                                    sErrorField = "IsReminder"
    '                                                    sErrorValue = "False"
    '                                                    nodbot_SettleDraft.setAttribute("IsReminder", "False")
    '                                                    'nodbot_SettleDraft.setAttribute("CreditNotaSettleId", nodbot_SettleDraftInvoice.attributes.getNamedItem("SettledTransactionId").nodeValue)
    '                                                    sErrorField = "Deductable"
    '                                                    nodbot_SettleDraft.setAttribute("Deductable", "No")
    '                                                    If nAmountToUse > 0 Then
    '                                                        sErrorField = "SettledTransactionId"
    '                                                        sErrorValue = nodbot_SettleDraftInvoice.attributes.getNamedItem("SettledTransactionId").nodeValue
    '                                                        nodbot_SettleDraft.setAttribute("SettledTransactionId", nodbot_SettleDraftInvoice.attributes.getNamedItem("SettledTransactionId").nodeValue)
    '                                                    Else
    '                                                        ''Not necessary to match against an invoice when it is paid to much (nGLAmount/nAmountToUse is negative)
    '                                                        sErrorField = "SettledTransactionId"
    '                                                        sErrorValue = ""
    '                                                        nodbot_SettleDraft.setAttribute("SettledTransactionId", "")
    '                                                        If bPostAgainstObservationAccount Then '23.11.2012 - Must add a debtor which to post the income on, pick the first
    '                                                            sErrorField = "MoveToDebtorId"
    '                                                            sErrorValue = nodlistSettleDrafts.item(0).attributes.getNamedItem("DebtorIDForCreditNotes").nodeValue
    '                                                            nodbot_SettleDraft.setAttribute("MoveToDebtorId", nodlistSettleDrafts.item(0).attributes.getNamedItem("DebtorIDForCreditNotes").nodeValue)
    '                                                            sErrorField = "CreditNotaSettleId"
    '                                                            sErrorValue = Trim(oPayment.MATCH_MatchingIDAgainstObservationAccount)
    '                                                            nodbot_SettleDraft.setAttribute("CreditNotaSettleId", Trim(oPayment.MATCH_MatchingIDAgainstObservationAccount))
    '                                                        End If
    '                                                    End If
    '                                                    'nodbot_SettleDraft.setAttribute("SettledTransactionId", Trim$(oInvoice.MATCH_ID))
    '                                                    'nodbot_SettleDraft.setAttribute("RestAmountType", Trim(oInvoice.MyField))
    '                                                    sErrorField = "RestAmountType"
    '                                                    sErrorValue = Trim$(oInvoice.MATCH_ID)
    '                                                    nodbot_SettleDraft.setAttribute("RestAmountType", Trim$(oInvoice.MATCH_ID))
    '                                                    sErrorField = "AppendChild: SettleDraft"
    '                                                    nodTransaction_Settles.appendChild(nodbot_SettleDraft)

    '                                                    ' XNET 30.04.2012
    '                                                    sErrorField = "Amount (Settle)"
    '                                                    sErrorValue = ConvertFromAmountToString(Math.Round(nAmountToUse * -1, 0), ".", ",")
    '                                                    nodbot_SettleDraft.setAttribute("Amount", ConvertFromAmountToString(Math.Round(nAmountToUse * -1, 0), "", "."))

    '                                                    If bRemoveActiveSettleDraft Then
    '                                                        'Set koko = nodlistSettleDrafts.Item(CLng(i))
    '                                                        sErrorField = "XPATH: Remove settledraft"
    '                                                        nodTemp = nodlistSettleDrafts.item(CLng(i)).parentNode.removeChild(nodlistSettleDrafts.item(CLng(i)))
    '                                                        'nodlistSettleDrafts.Reset
    '                                                        nodTemp = Nothing
    '                                                        'Set nodlistSettleDrafts = nodTransaction_Settles.selectNodes("SettleDraft")
    '                                                        'nodlistSettleDrafts.Item(CLng(i)).parentNode.removeChild (nodlistSettleDrafts.Item(CLng(i)))
    '                                                        'nodTransaction_Settles.removeChild (nodlistSettleDrafts.Item(CLng(i)))
    '                                                        'nodbot_SettleDraftInvoice.parentNode.removeChild (nodbot_SettleDraftInvoice)
    '                                                    Else
    '                                                        'Adjust the amount on the settle of the invoice
    '                                                        ' XNET 30.04.2012
    '                                                        If nAmountToUse > 0 Then
    '                                                            sErrorField = "Amount Settle(to be adjusted)"
    '                                                            sErrorValue = ConvertFromAmountToString(Math.Round(nAmountToUse * -1, 0), ".", ",")
    '                                                            nodbot_SettleDraftInvoice.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round(Val(nodbot_SettleDraftInvoice.attributes.getNamedItem("Amount").nodeValue) * 100, 0) + Math.Round((nAmountToUse * -1), 0), vbNullString, ".")
    '                                                        End If
    '                                                    End If

    '                                                    'Change the amount stated on the transaction (amount paid to this customer in this payment).
    '                                                    'The amount on the credit level for the last credit transaction

    '                                                    ' XKJELL - det er her det blir feil for Conecto
    '                                                    ' nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue returnerer f.ex. 1.36424205265939E-.12
    '                                                    ' Jeg har tatt en spansk en for å få det til å funke -
    '                                                    ' Første If-en er sannsynligvis ikke nødvendig.
    '                                                    ' Det viktige tror jeg er Round i Else-en
    '                                                    'If InStr(nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue, "E-.") > 0 Then
    '                                                    '    nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(nAmountToUse, vbNullString, ".")
    '                                                    'Else
    '                                                    'nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString((Val(nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue)) * 100 + (nAmountToUse), vbNullString, ".")
    '                                                    'XNET ca 20.04.2012
    '                                                    If Not bPostAgainstObservationAccount Then
    '                                                        sErrorField = "Amount Creditor(to be adjusted)"
    '                                                        sErrorValue = ConvertFromAmountToString(Math.Round(nAmountToUse * -1, 0), ".", ",")
    '                                                        nodCreditorExists.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round((Val(nodCreditorExists.attributes.getNamedItem("Amount").nodeValue)) * 100 + (nAmountToUse), 0), vbNullString, ".")
    '                                                    End If
    '                                                    'End If
    '                                                    'Adjust amount on the batch level- not influenced by creditnotes????
    '                                                    ' XKJELL - det er her det opprinnelig ble  feil for Conecto
    '                                                    ' nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue returnerer f.ex. 1.36424205265939E-.12
    '                                                    ' Her tester vi på om det er en slik spesialverdi
    '                                                    ' Slår sannsynligvis ikke til, da det er løst i kodelinjene over med Round.
    '                                                    ' VI MÅ SE PÅ DETTE SAMMEN
    '                                                    'If InStr(nodCustomerExists.Attributes.getNamedItem("Amount").nodeValue, "E-.") > 0 Then
    '                                                    '    nodbof_BalanceAccount.Attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString((nAmountToUse * -1), vbNullString, ".")
    '                                                    'Else
    '                                                    'XNET ca 20.04.2012
    '                                                    'nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue = ConvertFromAmountToString(Math.Round((Val(nodbof_BalanceAccount.attributes.getNamedItem("Amount").nodeValue)) * 100 + (nAmountToUse * -1), 0), vbNullString, ".") '* -1
    '                                                    'End If
    '                                                Loop
    '                                            End If

    '                                        Next oInvoice

    '                                    End If

    '                                    'END ******************************** GLHandling

    '                                    oPayment.Exported = True

    '                                    'XOKNET - 11.10.2010
    '                                End If
    '                            Next oPayment

    '                            'Next iClientArrayCounter

    '                        End If

    '                    Next oBatch

    '                End If

    '            Next oBabel

    '            'Write to Totallevel
    '            'Set nodElement = nodTotal.selectSingleNode("child::*[name()='TotalTransactionCount']")
    '            'nodElement.nodeTypedValue = Trim$(Str(nTotalTransactions))
    '            'Set nodElement = nodTotal.selectSingleNode("child::*[name()='TotalSum']")
    '            'nodElement.nodeTypedValue = ConvertFromAmountToString(nTotalSum / 100)


    '            'Delete all helpnodes Payment_ID
    '            '    sXpath = ""
    '            '    sXpath = "Transaction[@Payment_ID = '"
    '            '    sXpath = sXpath & Trim(Str(oPayment.Index))
    '            '    sXpath = sXpath & "' and @Amount != '0.00']"
    '            '
    '            '    Set nodCustomerExists = nodbof_BalanceAccount_TransactionsList.selectSingleNode(sXpath)
    '            sErrorField = "XPATH: Select transactionlist"
    '            sErrorValue = ""
    '            sErrorName = ""
    '            sErrorAmount = ""
    '            sErrorDate = ""
    '            sErrorTypeOfPosting = ""
    '            sErrorInvoiceAmount = ""

    '            nodlistSettleDrafts = Nothing
    '            nodlistSettleDrafts = docXMLExport.selectNodes("ImportData/ImportData.BalanceAccounts/List/BalanceAccount/BalanceAccount.Transactions/List/Transaction")

    '            sErrorField = "XPATH: Remove payment_ID"
    '            For i = 0 To nodlistSettleDrafts.length - 1
    '                'nodlistSettleDrafts
    '                'nodlistSettleDrafts.Item(i).rem
    '                nodTransaction = nodlistSettleDrafts.item(i)
    '                nodTransaction.removeAttribute("Payment_ID")
    '                'Dim k As MSXML2.IXMLDOMElement
    '                'Dim newEle As MSXML2.IXMLDOMElement
    '                'Dim nodImportData_BalanceAccounts As MSXML2.IXMLDOMNode
    '                'newEle.re

    '            Next i

    '            sErrorField = "Save XML-file"
    '            docXMLExport.save(sFILE_Name)
    '            pi = Nothing

    '            'Export to Connect via WCF!

    '            'Retrieve endpoint-address

    '            'Build an array consisting of the ConnectionStrings and Queries for 'other SQL' agianst the ERP-system.
    '            sErrorField = "Find WCF-connection"
    '            aQueryArray = GetERPDBInfo(oBabelFiles.Item(1).VB_Profile.Company_ID, 5)

    '            For lCounter = 0 To aQueryArray.GetUpperBound(0)
    '                If UCase(aQueryArray(lCounter, 4, 0)) = "CONECTO_WCF" Then
    '                    sURL = aQueryArray(lCounter, 0, 0)
    '                    sUID = aQueryArray(lCounter, 1, 0)
    '                    sPWD = aQueryArray(lCounter, 2, 0)
    '                End If
    '            Next lCounter

    '            ' Increase binding max sizes
    '            sErrorField = "Set values in binding"
    '            If TypeOf ConnectClient.Endpoint.Binding Is ServiceModel.BasicHttpBinding Then
    '                Dim binding As ServiceModel.BasicHttpBinding = _
    '                        CType(ConnectClient.Endpoint.Binding, ServiceModel.BasicHttpBinding)
    '                Dim max As Integer = Integer.MaxValue ' around 5M size allowed  
    '                binding.MaxReceivedMessageSize = max 'Increase binding max sizes
    '                binding.MaxBufferPoolSize = max
    '                binding.MaxBufferSize = max 'Increase binding max sizes
    '                binding.ReaderQuotas.MaxStringContentLength = max 'Increase binding max sizes
    '                binding.ReaderQuotas.MaxDepth = max 'Increase binding max sizes
    '                binding.ReaderQuotas.MaxArrayLength = max 'Increase binding max sizes
    '                binding.ReaderQuotas.MaxBytesPerRead = max 'Increase binding max sizes
    '                binding.ReaderQuotas.MaxNameTableCharCount = max 'Increase binding max sizes
    '                'binding. .MaxNameTableCharCount = max 'Increase binding max sizes
    '                binding.SendTimeout = New TimeSpan(3, 0, 0, 0, 0) 'Increase the clientside timeout (BB will now wait 3 hours before raising an error?
    '                binding.ReceiveTimeout = New TimeSpan(3, 0, 0, 0, 0)
    '            End If

    '            sReference = CreateUniqueReferenceFromBabelFiles(oBabelFiles)

    '            sErrorField = "Set WCF-credentials"
    '            ConnectClient.ClientCredentials.UserName.UserName = sUID '"Admin"
    '            ConnectClient.ClientCredentials.UserName.Password = sPWD '"Cranberry987"

    '            'Change information in the serviceagreement (appconfig)

    '            'For Each endpoint As ServiceEndpoint In Description.Endpoints
    '            '    For Each operation As OperationDescription In endpoint.Contract.Operations
    '            '        Dim operationBehavior As DataContractSerializerOperationBehavior = operation.Behaviors.Find(Of DataContractSerializerOperationBehavior)()
    '            '        If operationBehavior Is Nothing Then
    '            '            operationBehavior = New DataContractSerializerOperationBehavior(operation)
    '            '            operation.Behaviors.Add(operationBehavior)
    '            '        End If
    '            '        operationBehavior.DataContractResolver = New TypeResolver()
    '            '        operationBehavior.MaxItemsInObjectGraph = Integer.MaxValue
    '            '    Next
    '            'Next

    '            'For Each endpoint As ServiceEndpoint In Description.Endpoints
    '            sErrorField = "Change WCF-operations in Endpoint"
    '            For Each operation As OperationDescription In ConnectClient.Endpoint.Contract.Operations
    '                Dim operationBehavior As DataContractSerializerOperationBehavior = operation.Behaviors.Find(Of DataContractSerializerOperationBehavior)()
    '                If operationBehavior Is Nothing Then
    '                    operationBehavior = New DataContractSerializerOperationBehavior(operation)
    '                    operation.Behaviors.Add(operationBehavior)
    '                End If
    '                'operationBehavior.DataContractResolver = New TypeResolver()
    '                operationBehavior.MaxItemsInObjectGraph = Integer.MaxValue
    '            Next
    '            'Next

    '            'ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress("https://195.159.147.217/EX_BabelBank/service.svc")
    '            'ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress("https://hiilar-pc.conecto.no/BabelBankService/service.svc")
    '            'ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress("https://test2services.conecto.no/EX_BabelBank/service.svc")

    '            sErrorField = "Set WCF-endpoint address"
    '            ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(sURL)

    '            sErrorField = "WCF: Kontrollerer at service kjører"
    '            bFirstBatch = ConnectClient.IsAlive()

    '            If Not RunTime() Then
    '                Err.Raise(1, "WriteConnect_WCF_NewGLHandling", "Du er nå pålogget en service hos Conecto fra utviklingsmiljøet. Kjøring avbrytes!")
    '            End If

    '            sXpath = ""
    '            sXpath = "ImportData/ImportData.BalanceAccounts/List/BalanceAccount"

    '            sErrorField = "XPATH Select Balance accounts"
    '            nodeBabelBalanceAccountSaveDtos = docXMLExport.selectNodes(sXpath)

    '            If nodeBabelBalanceAccountSaveDtos.length > 0 Then
    '                sErrorField = "WCF: CreatePaymentJournal"
    '                iJournalID = ConnectClient.CreatePaymentJournal(sReference)

    '                lBalanceAccountCounter = -1

    '                'For ikoko = 1 To 100

    '                sXpath = "ImportData/ImportData.BalanceAccounts/List/BalanceAccount"

    '                sErrorField = "XPATH Select Balance accounts"
    '                nodeBabelBalanceAccountSaveDtos = docXMLExport.selectNodes(sXpath)

    '                For Each nodeBabelBalanceAccountSaveDto In nodeBabelBalanceAccountSaveDtos
    '                    lBalanceAccountCounter = lBalanceAccountCounter + 1

    '                    sErrorTypeOfPosting = "BalanceAccountSaveDto"

    '                    BabelBalanceAccountSaveDto = New Conecto.BabelBalanceAccountSaveDto
    '                    sErrorField = "WCF: Account"
    '                    sErrorValue = nodeBabelBalanceAccountSaveDto.getAttribute("Account")
    '                    BabelBalanceAccountSaveDto.Account = nodeBabelBalanceAccountSaveDto.getAttribute("Account")
    '                    sErrorField = "WCF: Amount"
    '                    sErrorValue = nodeBabelBalanceAccountSaveDto.getAttribute("Amount")
    '                    BabelBalanceAccountSaveDto.Amount = Val(nodeBabelBalanceAccountSaveDto.getAttribute("Amount")) * -1
    '                    sErrorField = "WCF: BookDate"
    '                    sErrorValue = nodeBabelBalanceAccountSaveDto.getAttribute("BookDate")
    '                    BabelBalanceAccountSaveDto.BookDate = nodeBabelBalanceAccountSaveDto.getAttribute("BookDate")
    '                    'BabelBalanceAccountSaveDto.ProcessedDateTime = ??
    '                    sErrorField = "WCF: Reference"
    '                    sErrorValue = nodeBabelBalanceAccountSaveDto.getAttribute("Reference")
    '                    BabelBalanceAccountSaveDto.Reference = nodeBabelBalanceAccountSaveDto.getAttribute("Reference")
    '                    sErrorField = "WCF: ValueDate"
    '                    sErrorValue = nodeBabelBalanceAccountSaveDto.getAttribute("ValueDate")
    '                    BabelBalanceAccountSaveDto.ValueDate = nodeBabelBalanceAccountSaveDto.getAttribute("ValueDate")
    '                    sErrorField = "XPATH: Select transactions"
    '                    sXpath = ""
    '                    sXpath = "BalanceAccount.Transactions/List/Transaction"
    '                    nodeBabelDebtorAccountSaveDtos = nodeBabelBalanceAccountSaveDto.selectNodes(sXpath)

    '                    ReDim BabelDebtorAccountSaveDtos(0)
    '                    lDebtorCounter = -1
    '                    For Each nodeBabelDebtorAccountSaveDto In nodeBabelDebtorAccountSaveDtos
    '                        lDebtorCounter = lDebtorCounter + 1

    '                        sErrorName = Left(nodeBabelDebtorAccountSaveDto.getAttribute("Description"), 50)

    '                        BabelDebtorAccountSaveDto = New Conecto.BabelDebtorAccountSaveDto
    '                        sErrorTypeOfPosting = "DebtorAccountSaveDto"
    '                        sErrorField = "WCF: DebtorAmount"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("Amount")
    '                        sErrorAmount = sErrorValue
    '                        BabelDebtorAccountSaveDto.Amount = Val(nodeBabelDebtorAccountSaveDto.getAttribute("Amount")) * -1
    '                        sErrorField = "WCF: BankReference"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("BankReference")
    '                        BabelDebtorAccountSaveDto.BankReference = nodeBabelDebtorAccountSaveDto.getAttribute("BankReference")
    '                        'sXpath = ""
    '                        'sXpath = "BalanceAccount.CreditorReference/Creditor"
    '                        'nodeTemp = nodeBabelBalanceAccountSaveDto.selectSingleNode(sXpath)
    '                        'BabelDebtorAccountSaveDto.CreditorPartyId = nodeTemp.getAttribute("AccountingId")
    '                        sErrorField = "WCF: Creditor"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("Creditor")
    '                        BabelDebtorAccountSaveDto.CreditorPartyId = nodeBabelDebtorAccountSaveDto.getAttribute("Creditor")
    '                        sErrorField = "XPATH: Select debtor"
    '                        sXpath = ""
    '                        sXpath = "Transaction.Debtor/Debtor"
    '                        nodeTemp = nodeBabelDebtorAccountSaveDto.selectSingleNode(sXpath)
    '                        'BabelDebtorAccountSaveDto.DebtorPartyId ?? Lars ønsker denne
    '                        sErrorField = "WCF: CustomerNumber"
    '                        sErrorValue = nodeTemp.getAttribute("CustomerNumber")
    '                        BabelDebtorAccountSaveDto.CustomerNumber = nodeTemp.getAttribute("CustomerNumber")
    '                        sErrorField = "WCF: UniqueERPID"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("UniqueERPID")
    '                        BabelDebtorAccountSaveDto.PaymentId = nodeBabelDebtorAccountSaveDto.getAttribute("UniqueERPID")

    '                        BabelPaymentSaveDto = New Conecto.BabelPaymentSaveDto
    '                        sErrorTypeOfPosting = "PaymentSaveDto"

    '                        sErrorField = "WCF: Amount(Payment)"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("Amount")
    '                        BabelPaymentSaveDto.Amount = Val(nodeBabelDebtorAccountSaveDto.getAttribute("Amount")) * -1
    '                        'BabelPaymentSaveDto.CaseId = "" 'Will be used in Collection
    '                        sErrorField = "WCF: Description"
    '                        sErrorValue = Left(nodeBabelDebtorAccountSaveDto.getAttribute("Description"), 50)
    '                        BabelPaymentSaveDto.Description = nodeBabelDebtorAccountSaveDto.getAttribute("Description")
    '                        sErrorField = "WCF: FromBankAccountNumber"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("FromBankAccountNumber")
    '                        BabelPaymentSaveDto.FromBankAccountNumber = nodeBabelDebtorAccountSaveDto.getAttribute("FromBankAccountNumber")
    '                        sErrorField = "WCF: Kid"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("Kid")
    '                        BabelPaymentSaveDto.Kid = nodeBabelDebtorAccountSaveDto.getAttribute("Kid")
    '                        sErrorField = "WCF: VoucherDate"
    '                        sErrorValue = nodeBabelDebtorAccountSaveDto.getAttribute("VoucherDate")
    '                        sErrorDate = sErrorValue
    '                        BabelPaymentSaveDto.VoucherDate = nodeBabelDebtorAccountSaveDto.getAttribute("VoucherDate")

    '                        sErrorField = "XPATH: SettleDraft"
    '                        sXpath = ""
    '                        sXpath = "Transaction.Settles/SettleDraft"
    '                        nodeBabelSettleSaveDtos = nodeBabelDebtorAccountSaveDto.selectNodes(sXpath)
    '                        lSettleCounter = -1

    '                        ReDim BabelSettleSaveDtos(0)

    '                        For Each nodeBabelSettleSaveDto In nodeBabelSettleSaveDtos
    '                            lSettleCounter = lSettleCounter + 1

    '                            BabelSettleSaveDto = New Conecto.BabelSettleSaveDto
    '                            sErrorTypeOfPosting = "SettleSaveDto"

    '                            sErrorField = "WCF: Amount(Settle)"
    '                            sErrorValue = nodeBabelSettleSaveDto.getAttribute("Amount")
    '                            sErrorInvoiceAmount = sErrorValue
    '                            BabelSettleSaveDto.Amount = Val(nodeBabelSettleSaveDto.getAttribute("Amount")) * -1
    '                            If bPostAgainstObservationAccount Then
    '                                sErrorField = "WCF: ExcludeFromTotalSum"
    '                                sErrorValue = "True"
    '                                BabelSettleSaveDto.ExcludeFromTotalSum = "True" 'nodeBabelSettleSaveDto.getAttribute("ExcludeFromTotalSum")
    '                            Else
    '                                sErrorField = "WCF: ExcludeFromTotalSum"
    '                                sErrorValue = nodeBabelSettleSaveDto.getAttribute("ExcludeFromTotalSum")
    '                                BabelSettleSaveDto.ExcludeFromTotalSum = nodeBabelSettleSaveDto.getAttribute("ExcludeFromTotalSum")
    '                            End If
    '                            '27.01.2014 - Added not emptystring
    '                            If Not IsDBNull(nodeBabelSettleSaveDto.getAttribute("CreditNotaSettleId")) Then
    '                                '26.01.2017 - Splitted the original IF in 2 IFs
    '                                If Not EmptyString(nodeBabelSettleSaveDto.getAttribute("CreditNotaSettleId")) Then
    '                                    sErrorField = "WCF: ExistingCreditTransactionId"
    '                                    sErrorValue = nodeBabelSettleSaveDto.getAttribute("CreditNotaSettleId")
    '                                    BabelSettleSaveDto.ExistingCreditTransactionId = nodeBabelSettleSaveDto.getAttribute("CreditNotaSettleId")
    '                                    sErrorField = "WCF: DoSettleOnTwoExistingTransactions"
    '                                    sErrorValue = "True"
    '                                    BabelSettleSaveDto.DoSettleOnTwoExistingTransactions = True
    '                                End If
    '                            End If
    '                            sErrorField = "WCF: IsReminder"
    '                            sErrorValue = nodeBabelSettleSaveDto.getAttribute("IsReminder")
    '                            BabelSettleSaveDto.IsReminder = nodeBabelSettleSaveDto.getAttribute("IsReminder")
    '                            sErrorField = "WCF: RestAmountType"
    '                            sErrorValue = nodeBabelSettleSaveDto.getAttribute("RestAmountType")
    '                            BabelSettleSaveDto.RestAmountType = nodeBabelSettleSaveDto.getAttribute("RestAmountType")
    '                            If Not IsDBNull(nodeBabelSettleSaveDto.getAttribute("MoveToDebtorId")) Then
    '                                If Not EmptyString(nodeBabelSettleSaveDto.getAttribute("MoveToDebtorId")) Then
    '                                    sErrorField = "WCF: MoveToDebtorCustmerNumber"
    '                                    sErrorValue = nodeBabelSettleSaveDto.getAttribute("MoveToDebtorId")
    '                                    BabelSettleSaveDto.MoveToDebtorCustmerNumber = nodeBabelSettleSaveDto.getAttribute("MoveToDebtorId")
    '                                    sErrorField = "WCF: DoSettleOnTwoExistingTransactions"
    '                                    sErrorValue = "False"
    '                                    BabelSettleSaveDto.DoSettleOnTwoExistingTransactions = False
    '                                Else
    '                                    'Keep the default value
    '                                End If
    '                            Else
    '                                'BabelSettleSaveDto.SettleTransactionId is nothing
    '                            End If
    '                            'BabelSettleSaveDto.MoveToDebtorId = nodeBabelSettleSaveDto.getAttribute("MoveToDebtorId")
    '                            If Not IsDBNull(nodeBabelSettleSaveDto.getAttribute("SettledTransactionId")) Then
    '                                If Not EmptyString(nodeBabelSettleSaveDto.getAttribute("SettledTransactionId")) Then
    '                                    sErrorField = "WCF: SettleTransactionId"
    '                                    sErrorValue = nodeBabelSettleSaveDto.getAttribute("SettledTransactionId")
    '                                    BabelSettleSaveDto.SettleTransactionId = nodeBabelSettleSaveDto.getAttribute("SettledTransactionId")
    '                                Else
    '                                    'Keep the default value
    '                                End If
    '                            Else
    '                                'BabelSettleSaveDto.SettleTransactionId is nothing
    '                            End If

    '                            ReDim Preserve BabelSettleSaveDtos(lSettleCounter)
    '                            BabelSettleSaveDtos(lSettleCounter) = BabelSettleSaveDto

    '                        Next nodeBabelSettleSaveDto

    '                        BabelPaymentSaveDto.Settles = BabelSettleSaveDtos

    '                        ReDim BabelPaymentSaveDtos(0) 'Always one?
    '                        BabelPaymentSaveDtos(0) = BabelPaymentSaveDto

    '                        BabelDebtorAccountSaveDto.Payments = BabelPaymentSaveDtos

    '                        ReDim Preserve BabelDebtorAccountSaveDtos(lDebtorCounter) 'Always one?
    '                        BabelDebtorAccountSaveDtos(lDebtorCounter) = BabelDebtorAccountSaveDto

    '                    Next nodeBabelDebtorAccountSaveDto

    '                    BabelBalanceAccountSaveDto.DebtorAccounts = BabelDebtorAccountSaveDtos

    '                    ReDim Preserve BabelBalanceAccountSaveDtos(lBalanceAccountCounter)
    '                    BabelBalanceAccountSaveDtos(lBalanceAccountCounter) = BabelBalanceAccountSaveDto

    '                Next nodeBabelBalanceAccountSaveDto

    '                'Next ikoko

    '                sErrorField = "WCF: SaveBalanceAccountsForFactoring"
    '                sErrorValue = ""
    '                sErrorName = ""
    '                sErrorAmount = ""
    '                sErrorDate = ""
    '                sErrorTypeOfPosting = ""
    '                sErrorInvoiceAmount = ""
    '                ConnectClient.SaveBalanceAccountsForFactoring(BabelBalanceAccountSaveDtos, iJournalID)

    '                sErrorField = "WCF: PostBalanceAccountsInJournal"
    '                ConnectClient.PostBalanceAccountsInJournal(iJournalID)
    '            End If

    '            'PaymentBalanceAccountSaveDto();
    '            '    paymentBalanceAccountSaveDto.DebtorAccounts = new List<DebtorAccountSaveDto>();
    '            '    paymentBalanceAccountSaveDto.Amount = -1000;

    '            '    DebtorAccountSaveDto debtorAccountSaveDto = new DebtorAccountSaveDto();
    '            '    debtorAccountSaveDto.Amount = -1000;
    '            '    debtorAccountSaveDto.CreditorPartyId = invoiceTransaction.CreditorPartyId;
    '            '    debtorAccountSaveDto.DebtorPartyId = invoiceTransaction.DebtorPartyId;
    '            '    debtorAccountSaveDto.Payments = new List<PaymentSaveDto>();
    '            '    paymentBalanceAccountSaveDto.DebtorAccounts.Add(debtorAccountSaveDto);

    '            '    PaymentSaveDto paymentSaveDto = new PaymentSaveDto();
    '            '    paymentSaveDto.Amount = -1000;
    '            '    paymentSaveDto.FromBankAccountNumber = "1234";
    '            '    debtorAccountSaveDto.Payments.Add(paymentSaveDto);

    '            'If (addSette) Then
    '            '    {
    '            '        SettleSaveDto settleSaveDto = new SettleSaveDto();
    '            '        settleSaveDto.Amount = -1000;
    '            '        settleSaveDto.SettleDebetTransactionId = invoiceTransaction.TransactionId;
    '            '        settleSaveDto.RestAmountType = 0;
    '            '        paymentSaveDto.Settles = new List<SettleSaveDto>();
    '            '        paymentSaveDto.Settles.Add(settleSaveDto);
    '            '    }
    '            '    If (addPostToLoss) Then
    '            '    {
    '            '        SettleSaveDto settleSaveDto = new SettleSaveDto();
    '            '        settleSaveDto.Amount = 1;
    '            '        settleSaveDto.SettleDebetTransactionId = invoiceTransaction.TransactionId;
    '            '        settleSaveDto.RestAmountType = (int)ObjectLibraryEnums.RestAmountType.Loss;
    '            '        if (paymentSaveDto.Settles == null) 
    '            '            paymentSaveDto.Settles = new List<SettleSaveDto>();

    '            '        paymentSaveDto.Settles.Add(settleSaveDto);
    '            '    }

    '            'pi = Nothing

    '            sErrorField = "Function: dbUpDayTotals"
    '            If Not RunTime() Then
    '                For lCounter = 0 To UBound(aAccountArray, 2)
    '                    dbUpDayTotals(False, DateToString(Now()), aAmountArray(lCounter), "Matched_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
    '                Next lCounter

    '                '26.01.2017
    '                dbUpDayTotals(False, DateToString(Now), 0, "UnMatched_BB", "", oBabelFiles.VB_Profile.Company_ID)

    '                'Retrieve the balance from the unmatched account in Conect
    '                sErrorField = "SQL: RetrieveBalanceFromConect"
    '                oMyERPDal = New vbBabel.DAL
    '                oMyERPDal.Company_ID = 1 'TOD: Hardcoded CompanyNo
    '                oMyERPDal.DBProfile_ID = 1 'TOD: Hardcoded DBProfile_ID
    '                oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
    '                If Not oMyERPDal.ConnectToDB() Then
    '                    Throw New System.Exception(oMyERPDal.ErrorMessage)
    '                End If

    '                For lCounter = 0 To aQueryArray.GetUpperBound(0)
    '                    If UCase(aQueryArray(lCounter, 4, 1)) = "GET_BALANCE_OBS" Then
    '                        sRetrieveBalanceSQL = aQueryArray(0, 0, 1)
    '                        'sUID = aQueryArray(lCounter, 1, 0)
    '                        'sPWD = aQueryArray(lCounter, 2, 0)
    '                        Exit For
    '                    End If
    '                Next lCounter

    '                For lCounter = 0 To UBound(aAccountArray, 2)
    '                    bAccountFound = False
    '                    For Each oFilesetup In oBabelFiles.VB_Profile.FileSetups
    '                        For Each oClient In oFilesetup.Clients
    '                            For Each oaccount In oClient.Accounts
    '                                If aAccountArray(1, lCounter) = oaccount.Account Then
    '                                    sClientNumber = Trim$(oClient.ClientNo)
    '                                    bAccountFound = True

    '                                    Exit For
    '                                End If

    '                            Next oaccount
    '                            If bAccountFound Then Exit For
    '                        Next oClient
    '                        If bAccountFound Then Exit For
    '                    Next oFilesetup
    '                    oMyERPDal.SQL = Replace(sRetrieveBalanceSQL, "BB_ClientNo", Trim$(sClientNumber), , , vbTextCompare)
    '                    If oMyERPDal.Reader_Execute() Then

    '                        Do While oMyERPDal.Reader_ReadRecord

    '                            dbUpDayTotals(False, DateToString(Now()), CDbl(oMyERPDal.Reader_GetString("Balance")), "MATCHED_GL", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
    '                            Exit Do
    '                        Loop
    '                    End If
    '                Next lCounter
    '            End If


    '        Else
    '            dbUpDayTotals(False, DateToString(Now), 0, "UnMatched_BB", "", oBabelFiles.VB_Profile.Company_ID)
    '            'No Babelfile, nothing to do
    '        End If

    '    Catch ex As Exception

    '        sErrorMessage = "Felt/Kommando: " & sErrorField
    '        If Not EmptyString(sErrorValue) Then
    '            sErrorMessage = sErrorMessage & vbCrLf & "Verdi: " & sErrorValue
    '        End If
    '        If Not EmptyString(sErrorTypeOfPosting) Then
    '            sErrorMessage = sErrorMessage & vbCrLf & "Delrutine: " & sErrorTypeOfPosting
    '        End If
    '        If Not EmptyString(sErrorName) Then
    '            sErrorMessage = sErrorMessage & vbCrLf & "Navn: " & sErrorName
    '        End If
    '        If Not EmptyString(sErrorAmount) Then
    '            sErrorMessage = sErrorMessage & vbCrLf & "Betalt beløp: " & sErrorAmount
    '        End If
    '        If Not EmptyString(sErrorDate) Then
    '            sErrorMessage = sErrorMessage & vbCrLf & "Betalingsdato: " & sErrorDate
    '        End If
    '        If Not EmptyString(sErrorInvoiceAmount) Then
    '            sErrorMessage = sErrorMessage & vbCrLf & "Settlebeløp: " & sErrorInvoiceAmount
    '        End If

    '        Throw New vbBabel.Payment.PaymentException("Function: WriteConnect_WCF_NewGLHandling" & vbCrLf & ex.Message & vbCrLf & sErrorMessage, ex, oPayment, "", sFILE_Name)

    '    Finally

    '        If Not oMyERPDal Is Nothing Then
    '            oMyERPDal.Close()
    '            oMyERPDal = Nothing
    '        End If

    '    End Try

    '    WriteConnect_WCF_NewGLHandling = True

    'End Function
    Function WriteConnect_WCF_Inkasso(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFILE_Name As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sAdditionalID As String, ByVal sClientNo As String, ByVal sSpecial As String, ByVal bPostAgainstObservationAccount As Boolean)
        'XOKNET - 11.10.2010 - I guess the easiest is to convert the whole function
        'XOKNET - 14.12.2010 - Convert the whole function
        'DET ER DENNE WriteConnect_WCF SOM SKAL BENYTTES VIDERE!!!!!!!!!!!!!!!!!!!
        Dim ConnectClient As New vbBabel.Conecto.BabelBankServiceClient
        Dim BabelBalanceAccountSaveDtos() As Conecto.BabelBalanceAccountSaveDto = Nothing
        Dim BabelBalanceAccountSaveDto As Conecto.BabelBalanceAccountSaveDto
        Dim lBalanceAccountCounter As Long
        Dim BabelDebtorAccountSaveDtos() As Conecto.BabelDebtorAccountSaveDto
        Dim BabelDebtorAccountSaveDto As Conecto.BabelDebtorAccountSaveDto
        Dim lDebtorCounter As Long
        Dim BabelPaymentSaveDtos() As Conecto.BabelPaymentSaveDto
        Dim BabelPaymentSaveDto As Conecto.BabelPaymentSaveDto
        Dim lPaymentCounter As Long

        Dim nTotalTransactions, nFileTransactions As Double
        Dim nFileSum As Double, nTotalSum As Double
        Dim sMessage As String
        Dim bFirstBatch As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As String, sGLAccount As String
        Dim sClientName As String
        Dim sClientNumber As String
        Dim sEDIMessageNo As String
        Dim aClientArray() As String
        Dim iClientArrayCounter As Integer

        Dim bClientNoSet As Boolean
        Dim bFirstTime As Boolean = True
        Dim bPostingsDone As Boolean = False

        'XOKNET - 11.10.2010 -
        Dim bx As Boolean
        Dim lCounter As Long
        Dim sNoMatchAccount As String

        Dim oBabel As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As Payment, oInvoice As vbBabel.Invoice
        Dim oGLInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim nTotalAmount As Double
        Dim lNoOfTransactions As Long
        Dim sFreetextFixed As String
        Dim sQ4Text As String
        Dim iFreetextCounter As Integer

        Dim bReturnValue As Boolean
        Dim iJournalID As Integer = -1
        Dim sReference As String = ""
        Dim aQueryArray(,,) As String
        Dim sUID As String = ""
        Dim sPWD As String = ""
        Dim sURL As String = ""

        Dim sErrorField As String = ""
        Dim sErrorValue As String = ""
        Dim sErrorName As String = ""
        Dim sErrorAmount As String = ""
        Dim sErrorDate As String = ""
        Dim sErrorTypeOfPosting As String = ""
        Dim sErrorInvoiceAmount As String = ""
        Dim sErrorMessage As String = ""

        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim bUseDayTotals As Boolean
        Dim lCounter2 As Long, lArrayCounter As Long

        ' lag en outputfil
        Try

            bReturnValue = False
            bFirstBatch = True
            sOldAccountNo = vbNullString
            bAccountFound = False
            sGLAccount = vbNullString
            sClientName = vbNullString
            sClientNumber = vbNullString
            sEDIMessageNo = vbNullString
            sFreetextFixed = vbNullString
            sQ4Text = vbNullString

            nTotalAmount = 0
            lNoOfTransactions = 0

            'Build an array consisting of the ConnectionStrings and Queries for 'other SQL' agianst the ERP-system.
            sErrorField = "Find WCF-connection"
            aQueryArray = GetERPDBInfo(oBabelFiles.Item(1).VB_Profile.Company_ID, 5)

            For lCounter = 0 To aQueryArray.GetUpperBound(0)
                If UCase(aQueryArray(lCounter, 4, 0)) = "CONECTO_WCF" Then
                    sURL = aQueryArray(lCounter, 0, 0)
                    sUID = aQueryArray(lCounter, 1, 0)
                    sPWD = aQueryArray(lCounter, 2, 0)
                End If
            Next lCounter

            If EmptyString(sURL) Then
                Err.Raise(1892, "WriteConect_WCF_Inkasso", "Det er ikke angitt en WCF-kobling med navn CONECTO_WCF")
            End If

            ' Increase binding max sizes
            sErrorField = "Set values in binding"
            If TypeOf ConnectClient.Endpoint.Binding Is ServiceModel.BasicHttpBinding Then
                Dim binding As ServiceModel.BasicHttpBinding = _
                        CType(ConnectClient.Endpoint.Binding, ServiceModel.BasicHttpBinding)
                Dim max As Integer = Integer.MaxValue ' around 5M size allowed  
                binding.MaxReceivedMessageSize = max 'Increase binding max sizes
                binding.MaxBufferPoolSize = max
                binding.MaxBufferSize = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxStringContentLength = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxDepth = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxArrayLength = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxBytesPerRead = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxNameTableCharCount = max 'Increase binding max sizes
                'binding. .MaxNameTableCharCount = max 'Increase binding max sizes
                binding.SendTimeout = New TimeSpan(3, 0, 0, 0, 0) 'Increase the clientside timeout (BB will now wait 3 hours before raising an error?
                binding.ReceiveTimeout = New TimeSpan(3, 0, 0, 0, 0)
            End If

            sReference = CreateUniqueReferenceFromBabelFiles(oBabelFiles)

            sErrorField = "Set WCF-credentials"
            ConnectClient.ClientCredentials.UserName.UserName = sUID '"Admin"
            ConnectClient.ClientCredentials.UserName.Password = sPWD '"Cranberry987"

            sErrorField = "Change WCF-operations in Endpoint"
            For Each operation As OperationDescription In ConnectClient.Endpoint.Contract.Operations
                Dim operationBehavior As DataContractSerializerOperationBehavior = operation.Behaviors.Find(Of DataContractSerializerOperationBehavior)()
                If operationBehavior Is Nothing Then
                    operationBehavior = New DataContractSerializerOperationBehavior(operation)
                    operation.Behaviors.Add(operationBehavior)
                End If
                operationBehavior.MaxItemsInObjectGraph = Integer.MaxValue
            Next

            sErrorField = "Set WCF-endpoint address"
            ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(sURL)

            sErrorField = "WCF: Kontrollerer at service kjører"
            bFirstBatch = ConnectClient.IsAlive()

            lBalanceAccountCounter = -1

            If oBabelFiles.Count > 0 Then

                'New code 21.12.2006
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim aAmountArray(aAccountArray.GetLength(1))

                'Calculate some totals
                For Each oBabel In oBabelFiles
                    sEDIMessageNo = oBabel.EDI_MessageNo
                    nTotalAmount = nTotalAmount + oBabel.MON_InvoiceAmount
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            'May be used later when only matched items are to be exported
                            'If oPayment.MATCH_Matched = MatchStatus.Matched Then
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    lNoOfTransactions = lNoOfTransactions + 1
                                End If
                            Next oInvoice
                            'End If
                        Next oPayment
                    Next oBatch
                Next oBabel


                ' Traverse through the collections
                '------------------

                For Each oBabel In oBabelFiles
                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                'If oPayment.I_Account = sI_Account Then
                                ' 01.10.2018 – changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then

                        If bFirstTime Then
                            iJournalID = ConnectClient.CreateJournalForDebtCollection(sReference)
                            bFirstTime = False
                        End If

                        For Each oBatch In oBabel.Batches

                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 – changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment

                            If bExportoBatch Then

                                lBalanceAccountCounter = lBalanceAccountCounter + 1

                                'Find the client
                                If oBatch.Payments.Item(1).I_Account <> sOldAccountNo Then
                                    sNoMatchAccount = vbNullString
                                    If oPayment.VB_ProfileInUse Then
                                        'sI_Account = oPayment.I_Account
                                        bAccountFound = False
                                        For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                            For Each oClient In oFilesetup.Clients
                                                For Each oaccount In oClient.Accounts
                                                    If oBatch.Payments.Item(1).I_Account = oaccount.Account Then
                                                        sGLAccount = Trim$(oaccount.GLAccount)
                                                        sClientName = Trim$(oClient.Name)
                                                        sClientNumber = Trim$(oClient.ClientNo)
                                                        sOldAccountNo = oBatch.Payments.Item(1).I_Account
                                                        sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                        bAccountFound = True

                                                        Exit For
                                                    End If

                                                    ' 11.01.2013 - linjen under er sannsynligvis feil
                                                    'For lCounter2 = 0 To aAccountArray.GetLength(2)
                                                    For lCounter2 = 0 To aAccountArray.GetLength(1)
                                                        If oPayment.I_Account = aAccountArray(1, lCounter2) Then
                                                            lArrayCounter = lCounter2
                                                            Exit For
                                                        End If
                                                    Next lCounter2

                                                Next oaccount
                                                If bAccountFound Then Exit For
                                            Next oClient
                                            If bAccountFound Then Exit For
                                        Next oFilesetup
                                    End If
                                End If

                                'Write the batch-level
                                BabelBalanceAccountSaveDto = New Conecto.BabelBalanceAccountSaveDto
                                sErrorField = "WCF: Account"
                                If bPostAgainstObservationAccount Then
                                    sErrorValue = ""
                                    BabelBalanceAccountSaveDto.Account = ""
                                    sErrorField = "WCF: Amount"
                                    sErrorValue = 0
                                    BabelBalanceAccountSaveDto.Amount = 0 'ConvertFromAmountToString(0, vbNullString, ".")
                                Else
                                    sErrorValue = sGLAccount
                                    BabelBalanceAccountSaveDto.Account = sGLAccount
                                    sErrorField = "WCF: Amount"
                                    sErrorValue = ConvertFromAmountToString(oBatch.MON_InvoiceAmount, vbNullString, ".")
                                    BabelBalanceAccountSaveDto.Amount = oBatch.MON_InvoiceAmount / 100
                                End If
                                sErrorField = "WCF: BookDate"
                                sErrorValue = Left$(oBatch.Payments.Item(1).DATE_Payment, 4) & "-" & Mid$(oBatch.Payments.Item(1).DATE_Payment, 5, 2) & "-" & Right$(oBatch.Payments.Item(1).DATE_Payment, 2)
                                BabelBalanceAccountSaveDto.BookDate = Left$(oBatch.Payments.Item(1).DATE_Payment, 4) & "-" & Mid$(oBatch.Payments.Item(1).DATE_Payment, 5, 2) & "-" & Right$(oBatch.Payments.Item(1).DATE_Payment, 2)
                                sErrorField = "WCF: Reference"
                                sErrorValue = oBatch.REF_Bank
                                BabelBalanceAccountSaveDto.Reference = oBatch.REF_Bank
                                sErrorField = "WCF: ValueDate"
                                sErrorValue = Left$(oBatch.Payments.Item(1).DATE_Value, 4) & "-" & Mid$(oBatch.Payments.Item(1).DATE_Value, 5, 2) & "-" & Right$(oBatch.Payments.Item(1).DATE_Value, 2)
                                BabelBalanceAccountSaveDto.ValueDate = Left$(oBatch.Payments.Item(1).DATE_Value, 4) & "-" & Mid$(oBatch.Payments.Item(1).DATE_Value, 5, 2) & "-" & Right$(oBatch.Payments.Item(1).DATE_Value, 2)

                                ReDim BabelDebtorAccountSaveDtos(0)
                                lDebtorCounter = -1

                                For Each oPayment In oBatch.Payments

                                    '11.10.2010 - XOKNET - Added next lines
                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects that shall
                                    ' be exported to this exportfile
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 – changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then
                                        If IsEqualAmount(oPayment.MON_InvoiceAmount, 0) Then
                                            bExportoPayment = False
                                            oPayment.Exported = True
                                        End If
                                    End If


                                    If bExportoPayment Then

                                        sErrorName = oPayment.E_Name
                                        sErrorAmount = ConvertFromAmountToString(oPayment.MON_InvoiceAmount, vbNullString, ".")
                                        sErrorDate = oPayment.DATE_Payment
                                        sErrorTypeOfPosting = "General"
                                        sErrorField = "Reference"
                                        sErrorValue = ""

                                        sFreetextFixed = vbNullString
                                        'XNET - 07.10.2011 - Removed next line
                                        'sQ4Text = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Original = True Then
                                                For Each oFreeText In oInvoice.Freetexts
                                                    iFreetextCounter = iFreetextCounter + 1
                                                    If iFreetextCounter = 1 Then
                                                        sFreetextFixed = Trim$(oFreeText.Text)
                                                    ElseIf iFreetextCounter = 2 Then
                                                        sFreetextFixed = sFreetextFixed & " " & Trim$(oFreeText.Text)
                                                    Else
                                                        Exit For
                                                    End If
                                                Next oFreeText
                                            End If
                                        Next oInvoice

                                        If Not EmptyString(oPayment.E_Name) Then
                                            sFreetextFixed = Trim$(oPayment.E_Name) & " - " & sFreetextFixed
                                        End If

                                        For Each oInvoice In oPayment.Invoices
                                            'Backpayment is marked as OnAccount payment (matched on customer)
                                            If oInvoice.MATCH_Final And oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                                                'The match_ID is set as UTBET
                                                If Trim(oInvoice.MyField) = "3" Then
                                                    'Reset some values to make a correct XML-file
                                                    oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice
                                                    oInvoice.MyField = "3"
                                                    If Not bPostAgainstObservationAccount Then '23.11.2012
                                                        oInvoice.MATCH_ID = "0"
                                                    End If
                                                End If
                                            End If
                                        Next oInvoice

                                        'XOKNET - 11.10.2010 -
                                        'XNET - Added an IF - ASHLEY BARNES

                                        'GLHandling, commented next IF
                                        'If Not (RunTime()) Then
                                        '    bx = TreatConectoGLPostings(oPayment, True)
                                        'Else
                                        '    bx = TreatConectoGLPostings(oPayment, True)
                                        'End If

                                        'FIRST check if the payment is fully matched
                                        'If not write the whole payment amount as unmatched, not each invoice
                                        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then

                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Final Then
                                                    If oInvoice.MATCH_Matched Then
                                                        bPostingsDone = True
                                                        lDebtorCounter = lDebtorCounter + 1
                                                        BabelDebtorAccountSaveDto = New Conecto.BabelDebtorAccountSaveDto
                                                        sErrorTypeOfPosting = "DebtorAccountSaveDto"
                                                        sErrorField = "WCF: DebtorAmount"
                                                        sErrorValue = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
                                                        sErrorAmount = sErrorValue
                                                        BabelDebtorAccountSaveDto.Amount = oInvoice.MON_InvoiceAmount * -1 / 100
                                                        sErrorField = "WCF: BankReference"
                                                        sErrorValue = oPayment.REF_Bank1
                                                        BabelDebtorAccountSaveDto.BankReference = oPayment.REF_Bank1
                                                        If bPostAgainstObservationAccount Then
                                                            sErrorField = "WCF: Creditor"
                                                            sErrorValue = oInvoice.MATCH_ClientNo
                                                            BabelDebtorAccountSaveDto.CreditorPartyId = oInvoice.MATCH_ClientNo 'This is the creditorno for the case (moving to, not the observationaccount)
                                                            sErrorField = "CustomerNumber"
                                                            sErrorValue = oInvoice.CustomerNo
                                                            BabelDebtorAccountSaveDto.CustomerNumber = oInvoice.CustomerNo 'This is the customerno for the case (moving to, not the observationaccount)
                                                            sErrorField = "MovedFromTransactionId"
                                                            sErrorValue = Trim(oPayment.MATCH_MatchingIDAgainstObservationAccount)
                                                            BabelDebtorAccountSaveDto.MovedFromTransactionId = Trim(oPayment.MATCH_MatchingIDAgainstObservationAccount)
                                                        Else
                                                            sErrorField = "WCF: Creditor"
                                                            sErrorValue = oInvoice.MATCH_ClientNo
                                                            BabelDebtorAccountSaveDto.CreditorPartyId = oInvoice.MATCH_ClientNo
                                                            sErrorField = "CustomerNumber"
                                                            sErrorValue = oInvoice.CustomerNo
                                                            BabelDebtorAccountSaveDto.CustomerNumber = oInvoice.CustomerNo
                                                        End If
                                                        sErrorField = "WCF: UniqueERPID"
                                                        sErrorValue = Trim(oPayment.Unique_ERPID)
                                                        BabelDebtorAccountSaveDto.PaymentId = Trim(oPayment.Unique_ERPID)

                                                        If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
                                                            BabelPaymentSaveDto = New Conecto.BabelPaymentSaveDto
                                                            sErrorTypeOfPosting = "PaymentSaveDto"
                                                            sErrorField = "WCF: Amount(Payment)"
                                                            sErrorValue = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, vbNullString, ".")
                                                            BabelPaymentSaveDto.Amount = oInvoice.MON_InvoiceAmount * -1 / 100
                                                            sErrorField = "CaseId"
                                                            sErrorValue = oInvoice.MATCH_ID
                                                            BabelPaymentSaveDto.CaseId = oInvoice.MATCH_ID
                                                            sErrorField = "WCF: Description"
                                                            If sQ4Text = "" Then
                                                                sErrorValue = Left(sFreetextFixed, 50)
                                                                BabelPaymentSaveDto.Description = Left(sFreetextFixed, 50)
                                                            Else
                                                                sErrorValue = Left(sQ4Text & " - " & sFreetextFixed, 50)
                                                                BabelPaymentSaveDto.Description = Left(sQ4Text & " - " & sFreetextFixed, 50)
                                                            End If
                                                            If Len(oPayment.E_Account) = "11" Then
                                                                If Not Mid$(oPayment.E_Account, 5, 1) = "9" Then
                                                                    sErrorField = "FromBankAccountNumber"
                                                                    sErrorValue = oPayment.E_Account
                                                                    BabelPaymentSaveDto.FromBankAccountNumber = oPayment.E_Account
                                                                Else
                                                                    sErrorField = "FromBankAccountNumber"
                                                                    sErrorValue = ""
                                                                    BabelPaymentSaveDto.FromBankAccountNumber = ""
                                                                End If
                                                            Else
                                                                sErrorField = "FromBankAccountNumber"
                                                                sErrorValue = ""
                                                                BabelPaymentSaveDto.FromBankAccountNumber = ""
                                                            End If
                                                            If IsOCR(oPayment.PayCode) Then
                                                                If oPayment.Invoices.Count = 1 Then
                                                                    sErrorField = "WCF: Kid"
                                                                    sErrorValue = oInvoice.Unique_Id
                                                                    BabelPaymentSaveDto.Kid = oInvoice.Unique_Id
                                                                Else
                                                                    sErrorField = "WCF: Kid"
                                                                    sErrorValue = vbNullString
                                                                    BabelPaymentSaveDto.Kid = vbNullString
                                                                End If
                                                            Else
                                                                sErrorField = "WCF: Kid"
                                                                sErrorValue = vbNullString
                                                                BabelPaymentSaveDto.Kid = vbNullString
                                                            End If
                                                            sErrorValue = Left$(oPayment.DATE_Payment, 4) & "-" & Mid$(oPayment.DATE_Payment, 5, 2) & "-" & Right$(oPayment.DATE_Payment, 2)
                                                            sErrorDate = sErrorValue
                                                            BabelPaymentSaveDto.VoucherDate = Left$(oPayment.DATE_Payment, 4) & "-" & Mid$(oPayment.DATE_Payment, 5, 2) & "-" & Right$(oPayment.DATE_Payment, 2)

                                                            ReDim BabelPaymentSaveDtos(0) 'Always one
                                                            BabelPaymentSaveDtos(0) = BabelPaymentSaveDto

                                                            BabelDebtorAccountSaveDto.Payments = BabelPaymentSaveDtos
                                                        End If


                                                        ReDim Preserve BabelDebtorAccountSaveDtos(lDebtorCounter)
                                                        BabelDebtorAccountSaveDtos(lDebtorCounter) = BabelDebtorAccountSaveDto

                                                    Else


                                                    End If
                                                End If
                                            Next oInvoice



                                        Else
                                            lDebtorCounter = lDebtorCounter + 1
                                            bPostingsDone = True
                                            BabelDebtorAccountSaveDto = New Conecto.BabelDebtorAccountSaveDto
                                            sErrorTypeOfPosting = "DebtorAccountSaveDto"
                                            sErrorField = "WCF: DebtorAmount"
                                            sErrorValue = ConvertFromAmountToString(oPayment.MON_InvoiceAmount, vbNullString, ".")
                                            sErrorAmount = sErrorValue
                                            BabelDebtorAccountSaveDto.Amount = oPayment.MON_InvoiceAmount * -1 / 100
                                            sErrorField = "WCF: BankReference"
                                            sErrorValue = oPayment.REF_Bank1
                                            BabelDebtorAccountSaveDto.BankReference = oPayment.REF_Bank1
                                            sErrorField = "WCF: Creditor"
                                            sErrorValue = sClientNo
                                            BabelDebtorAccountSaveDto.CreditorPartyId = sClientNo
                                            sErrorField = "CustomerNumber"
                                            sErrorValue = sNoMatchAccount
                                            BabelDebtorAccountSaveDto.CustomerNumber = sNoMatchAccount
                                            sErrorField = "WCF: UniqueERPID"
                                            sErrorValue = Trim(oPayment.Unique_ERPID)
                                            BabelDebtorAccountSaveDto.PaymentId = Trim(oPayment.Unique_ERPID)

                                            ReDim Preserve BabelDebtorAccountSaveDtos(lDebtorCounter)
                                            BabelDebtorAccountSaveDtos(lDebtorCounter) = BabelDebtorAccountSaveDto


                                        End If
                                        oPayment.Exported = True

                                        'XOKNET - 11.10.2010
                                    End If
                                Next oPayment

                                'Next iClientArrayCounter

                                BabelBalanceAccountSaveDto.DebtorAccounts = BabelDebtorAccountSaveDtos

                                ReDim Preserve BabelBalanceAccountSaveDtos(lBalanceAccountCounter)
                                BabelBalanceAccountSaveDtos(lBalanceAccountCounter) = BabelBalanceAccountSaveDto

                            End If

                        Next oBatch

                    End If

                Next oBabel

                If bPostingsDone Then
                    ConnectClient.ImportBalanceAccountForDebtCollection(BabelBalanceAccountSaveDtos, iJournalID)
                    ConnectClient.PostBalanceAccountsJournalForDebtCollection(iJournalID)
                End If


                '    sErrorField = "Function: dbUpDayTotals"
                '    If Not RunTime() Then
                '        For lCounter = 0 To UBound(aAccountArray, 2)
                '            dbUpDayTotals(False, DateToString(Now()), aAmountArray(lCounter), "Matched_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                '        Next lCounter

                '        dbUpDayTotals(False, DateToString(Now), 0, "UnMatched_BB", "", oBabelFiles.VB_Profile.Company_ID)

                '        'Retrieve the balance from the unmatched account in Conect
                '        sErrorField = "SQL: RetrieveBalanceFromConect"
                '        oMyERPDal = New vbBabel.DAL
                '        oMyERPDal.Company_ID = 1 'TOD: Hardcoded CompanyNo
                '        oMyERPDal.DBProfile_ID = 1 'TOD: Hardcoded DBProfile_ID
                '        oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                '        If Not oMyERPDal.ConnectToDB() Then
                '            Throw New System.Exception(oMyERPDal.ErrorMessage)
                '        End If

                '        For lCounter = 0 To aQueryArray.GetUpperBound(0)
                '            If UCase(aQueryArray(lCounter, 4, 1)) = "GET_BALANCE_OBS" Then
                '                sRetrieveBalanceSQL = aQueryArray(0, 0, 1)
                '                'sUID = aQueryArray(lCounter, 1, 0)
                '                'sPWD = aQueryArray(lCounter, 2, 0)
                '                Exit For
                '            End If
                '        Next lCounter

                '        For lCounter = 0 To UBound(aAccountArray, 2)
                '            bAccountFound = False
                '            For Each oFilesetup In oBabelFiles.VB_Profile.FileSetups
                '                For Each oClient In oFilesetup.Clients
                '                    For Each oaccount In oClient.Accounts
                '                        If aAccountArray(1, lCounter) = oaccount.Account Then
                '                            sClientNumber = Trim$(oClient.ClientNo)
                '                            bAccountFound = True

                '                            Exit For
                '                        End If

                '                    Next oaccount
                '                    If bAccountFound Then Exit For
                '                Next oClient
                '                If bAccountFound Then Exit For
                '            Next oFilesetup
                '            oMyERPDal.SQL = Replace(sRetrieveBalanceSQL, "BB_ClientNo", Trim$(sClientNumber), , , vbTextCompare)
                '            If oMyERPDal.Reader_Execute() Then

                '                Do While oMyERPDal.Reader_ReadRecord

                '                    dbUpDayTotals(False, DateToString(Now()), CDbl(oMyERPDal.Reader_GetString("Balance")), "MATCHED_GL", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                '                    Exit Do
                '                Loop
                '            End If
                '        Next lCounter
                '    End If


            Else
                dbUpDayTotals(False, DateToString(Now), 0, "UnMatched_BB", "", oBabelFiles.VB_Profile.Company_ID)
                'No Babelfile, nothing to do
            End If

        Catch ex As Exception

            sErrorMessage = "Felt/Kommando: " & sErrorField
            If Not EmptyString(sErrorValue) Then
                sErrorMessage = sErrorMessage & vbCrLf & "Verdi: " & sErrorValue
            End If
            If Not EmptyString(sErrorTypeOfPosting) Then
                sErrorMessage = sErrorMessage & vbCrLf & "Delrutine: " & sErrorTypeOfPosting
            End If
            If Not EmptyString(sErrorName) Then
                sErrorMessage = sErrorMessage & vbCrLf & "Navn: " & sErrorName
            End If
            If Not EmptyString(sErrorAmount) Then
                sErrorMessage = sErrorMessage & vbCrLf & "Betalt beløp: " & sErrorAmount
            End If
            If Not EmptyString(sErrorDate) Then
                sErrorMessage = sErrorMessage & vbCrLf & "Betalingsdato: " & sErrorDate
            End If
            If Not EmptyString(sErrorInvoiceAmount) Then
                sErrorMessage = sErrorMessage & vbCrLf & "Settlebeløp: " & sErrorInvoiceAmount
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteConnect_WCF_Inkasso" & vbCrLf & ex.Message & vbCrLf & vbCrLf & sErrorMessage, ex, oPayment, "", sFILE_Name)

        Finally

        End Try

        WriteConnect_WCF_Inkasso = True

    End Function
    'myBinding.Name = "bpPROVIDERBinding"
    'myBinding.CloseTimeout = System.TimeSpan.FromSeconds(60)
    'myBinding.OpenTimeout = System.TimeSpan.FromSeconds(60)
    'myBinding.ReceiveTimeout = System.TimeSpan.FromSeconds(300)
    'myBinding.SendTimeout = System.TimeSpan.FromSeconds(300)
    'myBinding.AllowCookies = False
    'myBinding.BypassProxyOnLocal = False
    'myBinding.HostNameComparisonMode = System.ServiceModel.HostNameComparisonMode.StrongWildcard
    'myBinding.MaxBufferSize = 50000000
    'myBinding.MaxBufferPoolSize = 524288
    'myBinding.MaxReceivedMessageSize = 50000000
    'myBinding.MessageEncoding = System.ServiceModel.WSMessageEncoding.Text
    'myBinding.TextEncoding = System.Text.Encoding.UTF8
    'myBinding.TransferMode = System.ServiceModel.TransferMode.Buffered
    'myBinding.UseDefaultWebProxy = True
    'myBinding.ReaderQuotas.MaxDepth = 32
    'myBinding.ReaderQuotas.MaxStringContentLength = 8192
    'myBinding.ReaderQuotas.MaxArrayLength = 16384
    'myBinding.ReaderQuotas.MaxBytesPerRead = 4096
    'myBinding.ReaderQuotas.MaxNameTableCharCount = 16384
    'myBinding.Security.Mode = System.ServiceModel.BasicHttpSecurityMode.Transport
    'myBinding.Security.Transport.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.Basic
    'myBinding.Security.Transport.ProxyCredentialType = System.ServiceModel.HttpProxyCredentialType.None
    'myBinding.Security.Transport.Realm = "somerealm"
    'myBinding.Security.Message.ClientCredentialType = System.ServiceModel.BasicHttpMessageCredentialType.UserName
    'myBinding.Security.Message.AlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Default

    Private Function SetupConectoService() As Conecto.BabelBankServiceClient 'FunWebService.FunServiceClient
        'Dim AppSettings As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings

        'Using host As New System.ServiceModel.ServiceHost(GetType(CalculatorService))
        '    With host
        '        .AddServiceEndpoint(GetType(ICalculator), _
        '                                binding1, _
        '                                baseAddress)
        '    End With
        'End Using

        ''Using host As New System.ServiceModel.ServiceHost(GetType(CalculatorService))
        ''    With host
        ''        .AddServiceEndpoint(GetType(ICalculator), _
        ''                                binding1, _
        ''                                baseAddress)
        ''    End With
        ''End Using



        'Dim i As Integer = 0
        'While i < AppSettings.Count
        '    Console.WriteLine( _
        '        "#{0} Key: {1} Value: {2}", _
        '        i, AppSettings.GetKey(i), AppSettings(i))
        '    System.Math.Max( _
        '        System.Threading.Interlocked. _
        '        Increment(i), i - 1)
        'End While

        ''create endpoint
        'Dim ep As New ServiceModel.EndpointAddress(New Uri(AppSettings("BasicHttpBinding_IBabelBankService")), ServiceModel.EndpointIdentity.CreateUpnIdentity(AppSettings("address")))
        ''Dim ep As New EndpointAddress(New Uri(AppSettings("URL_FunService")), EndpointIdentity.CreateUpnIdentity(AppSettings("Identity_FunService")))

        ''create proxy with new endpoint
        'Dim service As New Conecto.BabelBankServiceClient 'SugargliderBilling.SugargliderServiceClient("wsHttp", ep)
        ''allow client to impersonate user        
        'service.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation
        ''return our shiny new service
        'Return service
        ''...and I've added this to my config file:<addkey= "FunIdentity " value= "Service_Prod@happyfunland.com.au "/>
    End Function
    Function WriteConnect_WCF_Remit_Update(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim ConnectClient As New Conecto.BabelBankServiceClient
        Dim oAvregningsObjects() As Conecto.BabelRemitInfoSaveDto
        Dim oAvregningsObject As Conecto.BabelRemitInfoSaveDto
        Dim iAvregnCounter As Integer = -1
        Dim oAvvisningObjects() As Conecto.BabelRemitInfoSaveDto
        Dim oAvvisningObject As Conecto.BabelRemitInfoSaveDto
        Dim iAvvisningsCounter As Integer = -1
        Dim iMottakArray() As Integer
        Dim iMottakArrayCounter As Integer = -1
        Dim bExportoBatch As Boolean
        Dim bExportobabelfile As Boolean
        Dim bExportoPayment As Boolean
        Dim sErrorCode As String
        Dim bErrorInBabelfile As Boolean = False
        Dim bErrorInBatch As Boolean = False
        Dim bErrorInPayment As Boolean = False
        Dim bIsAlive As Boolean = False
        Dim sErrorField As String

        Dim sConnection As String = ""

        Dim lCounter As Long
        Dim aQueryArray(,,) As String
        Dim sUID As String = ""
        Dim sPWD As String = ""
        Dim sURL As String = ""

        Try

            'Build an array consisting of the ConnectionStrings and Queries for aftermatcinh agianst the ERP-system.
            aQueryArray = GetERPDBInfo(1, 3) 'TODO: Hardcoded companyno

            sConnection = Trim(sFilenameOut)
            If Left(sConnection, 4) = "DSN=" Then
                sConnection = UCase(Trim(Mid(sConnection, 5)))
            End If

            For lCounter = 0 To aQueryArray.GetUpperBound(0)
                If UCase(aQueryArray(lCounter, 4, 0)) = sConnection Then
                    sURL = aQueryArray(lCounter, 0, 0)
                    sUID = aQueryArray(lCounter, 1, 0)
                    sPWD = aQueryArray(lCounter, 2, 0)
                End If
            Next lCounter

            sErrorField = "Binding"

            ' Increase binding max sizes
            If TypeOf ConnectClient.Endpoint.Binding Is ServiceModel.BasicHttpBinding Then
                Dim binding As ServiceModel.BasicHttpBinding = _
                        CType(ConnectClient.Endpoint.Binding, ServiceModel.BasicHttpBinding)
                Dim max As Integer = Integer.MaxValue ' around 5M size allowed  
                binding.MaxReceivedMessageSize = max 'Increase binding max sizes
                binding.MaxBufferPoolSize = max
                binding.MaxBufferSize = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxStringContentLength = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxDepth = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxArrayLength = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxBytesPerRead = max 'Increase binding max sizes
                binding.ReaderQuotas.MaxNameTableCharCount = max 'Increase binding max sizes
                'binding. .MaxNameTableCharCount = max 'Increase binding max sizes
                binding.SendTimeout = New TimeSpan(3, 0, 0, 0, 0) 'Increase the clientside timeout (BB will now wait 3 minutes before raising an error?
                binding.ReceiveTimeout = New TimeSpan(3, 0, 0, 0, 0)
            End If

            ConnectClient.ClientCredentials.UserName.UserName = sUID '"Admin"
            ConnectClient.ClientCredentials.UserName.Password = sPWD '"Cranberry987"

            For Each operation As System.ServiceModel.Description.OperationDescription In ConnectClient.Endpoint.Contract.Operations
                Dim operationBehavior As System.ServiceModel.Description.DataContractSerializerOperationBehavior = operation.Behaviors.Find(Of System.ServiceModel.Description.DataContractSerializerOperationBehavior)()
                If operationBehavior Is Nothing Then
                    operationBehavior = New System.ServiceModel.Description.DataContractSerializerOperationBehavior(operation)
                    operation.Behaviors.Add(operationBehavior)
                End If
                'operationBehavior.DataContractResolver = New TypeResolver()
                operationBehavior.MaxItemsInObjectGraph = Integer.MaxValue
            Next

            ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(sURL)
            'ConnectClient.ClientCredentials.UserName.UserName = "DEV"
            'ConnectClient.ClientCredentials.UserName.Password = "king"

            sErrorField = "IsAlive Not OK"

            bIsAlive = ConnectClient.IsAlive()

            sErrorField = "IsAliveOK"

            If Not RunTime() Then
                Throw New Exception("Function: WriteConnect_WCF_Remit_Update" & vbCrLf & "Du er nå pålogget en service hos Conecto fra utviklingsmiljøet. Kjøring avbrytes!")
            End If

            For Each oBabelFile In oBabelFiles

                bExportobabelfile = False
                bErrorInBabelfile = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportobabelfile = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    bExportobabelfile = True
                                End If
                            End If
                        End If
                        If bExportobabelfile Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportobabelfile Then
                        Exit For
                    End If
                Next oBatch

                If bExportobabelfile Then

                    If Val(oBabelFile.StatusCode) > 2 Then
                        bErrorInBabelfile = True
                        sErrorCode = oBabelFile.StatusCode
                    Else
                        bErrorInBabelfile = False
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabelFile.Batches
                        bExportoBatch = False
                        bErrorInBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        bExportoBatch = True
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            If bErrorInBabelfile Then
                                bErrorInBatch = True
                            Else
                                If Val(oBatch.StatusCode) > 2 Then
                                    bErrorInBatch = True
                                    sErrorCode = oBatch.StatusCode
                                Else
                                    bErrorInBatch = False
                                End If
                            End If

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                bErrorInPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            bExportoPayment = True
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If bErrorInBatch Then
                                        bErrorInPayment = True
                                    Else
                                        If Val(oPayment.StatusCode) > 2 Then
                                            bErrorInPayment = True
                                            sErrorCode = oPayment.StatusCode
                                        ElseIf oPayment.Cancel Then
                                            bErrorInPayment = True
                                            sErrorCode = "SLETT"
                                        Else
                                            bErrorInPayment = False
                                        End If
                                    End If

                                    For Each oInvoice In oPayment.Invoices

                                        If bErrorInPayment Then
                                            'No need to check the invoice
                                            iAvvisningsCounter = iAvvisningsCounter + 1
                                            oAvvisningObject = New Conecto.BabelRemitInfoSaveDto
                                            oAvvisningObject.TransactionId = CInt(oInvoice.REF_Own)
                                            If sErrorCode = "SLETT" Then
                                                oAvvisningObject.ErrorMessage = "Transaksjonen er slettet av Conecto"
                                            Else
                                                oAvvisningObject.ErrorMessage = TelepayStatusTxts(sErrorCode)
                                            End If

                                            ReDim Preserve oAvvisningObjects(iAvvisningsCounter)

                                            oAvvisningObjects(iAvvisningsCounter) = oAvvisningObject
                                        Else
                                            Select Case oInvoice.StatusCode

                                                Case "01"
                                                    iMottakArrayCounter = iMottakArrayCounter + 1
                                                    ReDim Preserve iMottakArray(iMottakArrayCounter)
                                                    iMottakArray(iMottakArrayCounter) = CInt(oInvoice.REF_Own)

                                                Case "02"
                                                    iAvregnCounter = iAvregnCounter + 1
                                                    oAvregningsObject = New Conecto.BabelRemitInfoSaveDto
                                                    oAvregningsObject.TransactionId = CInt(oInvoice.REF_Own)
                                                    oAvregningsObject.Amount = oInvoice.MON_TransferredAmount / 100
                                                    oAvregningsObject.BankReference = oInvoice.REF_Bank
                                                    oAvregningsObject.RemittedDate = StringToDate(oPayment.DATE_Value)

                                                    ReDim Preserve oAvregningsObjects(iAvregnCounter)

                                                    oAvregningsObjects(iAvregnCounter) = oAvregningsObject
                                                    'iAvregnArrayCounter = iAvregnArrayCounter + 1
                                                    'ReDim Preserve iAvregnArray(iAvregnArrayCounter)
                                                    'iAvregnArray(iAvregnArrayCounter) = CInt(oInvoice.REF_Own)

                                                Case Else
                                                    iAvvisningsCounter = iAvvisningsCounter + 1
                                                    oAvvisningObject = New Conecto.BabelRemitInfoSaveDto
                                                    oAvvisningObject.TransactionId = CInt(oInvoice.REF_Own)
                                                    oAvvisningObject.ErrorMessage = TelepayStatusTxts(oInvoice.StatusCode)

                                                    ReDim Preserve oAvvisningObjects(iAvvisningsCounter)

                                                    oAvvisningObjects(iAvvisningsCounter) = oAvvisningObject

                                            End Select
                                        End If


                                    Next

                                    oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabelFile

            If iMottakArrayCounter > -1 Then
                If oBabelFile.Special = "CONECTO_WCR_REMIT_INKASSO" Then
                    sErrorField = "SetTransactionStatusConfirmedReceivedByBank - Inkasso"
                    ConnectClient.SetTransactionStatusConfirmedReceivedByBank(iMottakArray, True)
                Else
                    sErrorField = "SetTransactionStatusConfirmedReceivedByBank - Procredit"
                    ConnectClient.SetTransactionStatusConfirmedReceivedByBank(iMottakArray, False)
                End If
            End If
            If iAvregnCounter > -1 Then
                If oBabelFile.Special = "CONECTO_WCR_REMIT_INKASSO" Then
                    sErrorField = "SetTransactionStatusSuccessfullyRemitted - Inkasso"
                    ConnectClient.SetTransactionStatusSuccessfullyRemitted(oAvregningsObjects, True)
                Else
                    sErrorField = "SetTransactionStatusSuccessfullyRemitted - Procredit"
                    ConnectClient.SetTransactionStatusSuccessfullyRemitted(oAvregningsObjects, False)
                End If
            End If
            If iAvvisningsCounter > -1 Then
                If oBabelFile.Special = "CONECTO_WCR_REMIT_INKASSO" Then
                    sErrorField = "SetTransactionStatusRemitationFailed - Inkasso"
                    ConnectClient.SetTransactionStatusRemitationFailed(oAvvisningObjects, True)
                Else
                    sErrorField = "SetTransactionStatusRemitationFailed - Procredit"
                    ConnectClient.SetTransactionStatusRemitationFailed(oAvvisningObjects, False)
                End If
            End If

        Catch ex As Exception

            Throw New Exception("Function: WriteConnect_WCF_Remit_Update" & vbCrLf & sErrorField & vbCrLf & ex.Message)

        Finally

            If Not ConnectClient Is Nothing Then
                ConnectClient = Nothing
            End If

        End Try

        WriteConnect_WCF_Remit_Update = True

    End Function
End Module
