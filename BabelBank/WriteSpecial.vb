Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Specialized

Module WriteSpecial
	' This module is a collection of several specialformats
    Dim nCounter As Double
    Dim iType40 As Integer
    Dim iType30 As Integer


	'Dim nNoOfTransactions As Double
	
    Function WriteLHLEkstern(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean

        ' 03.01.05
        ' Used not only for LHL
        ' Test on oBabelFile.Special
        ' R�de Kors: REDCROSS
        ' Kirkens N�dhjelp: CHURCHAID
        '
        '
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        'Dim op As Payment
        'Dim nTransactionNo As Double
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        'Dim nExistingLines As Double, sTempFilename As String, iCount As Integer
        'Dim bFoundRecord3 As Boolean, oFileWrite As TextStream, oFileRead As TextStream
        Dim bAppendFile As Boolean, dDate As Date ', sIAccountNo As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim sAdr1 As String, sAdr2 As String, sZip As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sId As String
        Dim sFreetext As String
        Dim sRef1 As String, sRef2 As String
        Dim sBirthNumber As String
        Dim sGLAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sNoMatchAccount As String
        Dim lBatchcounter As Long
        Dim bErrorInMaalgruppeExists As Boolean
        Dim iFreetextCounter As Integer
        Dim lCounter As Long

        '
        ' create an outputfile
        Try

            If UCase(oBabelFiles.Item(1).Special) = "KN" Then


                ' must sort on I_Account
                For Each oBabel In oBabelFiles
                    oBabel.SortCriteria1 = "VB_I_Account"
                    oBabel.SortCriteria2 = "DATE_Payment"
                    oBabel.SortCriteria3 = "MON_INVOICEAMOUNT"
                    oBabel.SortPayment()
                Next
            End If


            bAppendFile = False
            sOldAccountNo = vbNullString
            sGLAccount = vbNullString
            '05.04.2006 commented next line, due to error for LHL. When sI_Account is set to vbnullstring
            '  no data is exported in a client situation
            'sI_Account = vbNullString
            bAccountFound = False
            lBatchcounter = 0
            bErrorInMaalgruppeExists = False

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR(oPayment.PayCode) Then
                            'Nothing to do, don't export
                        Else
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If IsOCR(oPayment.PayCode) Then
                                'Nothing to do, don't export
                            Else
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            lBatchcounter = lBatchcounter + 1
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If IsOCR(oPayment.PayCode) Then
                                    'Nothing to do, don't export
                                Else
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If oPayment.REF_Own = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If UCase(oBabel.Special) = "REDCROSS" Or UCase(oBabel.Special) = "KN" Or UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then 'XNET - 28.06.2011 - Added NF
                                        If oPayment.I_Account <> sOldAccountNo Then
                                            If oPayment.VB_ProfileInUse Then
                                                bAccountFound = False
                                                For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                    For Each oClient In oFilesetup.Clients
                                                        For Each oaccount In oClient.Accounts
                                                            If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                                                sGLAccount = Trim$(oaccount.GLAccount)
                                                                sOldAccountNo = oPayment.I_Account
                                                                bAccountFound = True

                                                                Exit For
                                                            End If
                                                        Next oaccount
                                                        If bAccountFound Then Exit For
                                                    Next oClient
                                                    If bAccountFound Then Exit For
                                                Next oFilesetup
                                                If bAccountFound Then
                                                    sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                Else
                                                    Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                                    '12003: Could not find account %1.Please enter the account in setup."
                                                End If
                                            End If
                                        End If
                                    End If 'If UCase(oBabel.Special) = "REDCROSS" Then

                                    '-------- fetch content of each payment ---------
                                    sName = oPayment.E_Name
                                    sAdr1 = vbNullString   'vbNullString
                                    sAdr2 = vbNullString   'vbNullString
                                    ' If Adr2="-", empty Adr2;
                                    If Trim$(oPayment.E_Adr2) = "-" Then
                                        oPayment.E_Adr2 = vbNullString
                                    End If

                                    If Len(Trim$(oPayment.E_Adr2)) = 0 Then
                                        ' empty adr2, put opayment.e_adr1 into eksportfiles adr2
                                        sAdr2 = oPayment.E_Adr1  'Special for LHL, adr2 is the mainadresse
                                    Else
                                        ' both addresslines in use
                                        sAdr1 = oPayment.E_Adr1
                                        sAdr2 = oPayment.E_Adr2
                                    End If

                                    sZip = oPayment.E_Zip
                                    nAmount = oPayment.MON_TransferredAmount

                                    'sArchiveRef = oPayment.REF_Bank1
                                    ' Skal benyttes til avstemming bank

                                    If UCase(oBabel.Special) = "REDCROSS" Or UCase(oBabel.Special) = "KN" Or UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then 'XNET - 28.06.2011 - Added NF

                                        dDate = StringToDate(oPayment.DATE_Payment)
                                        '12.09.2006 - new code to write the correct archiveref for bankreconciliation
                                        sArchiveRef = Trim$(oBatch.REF_Bank)
                                        For lCounter = 1 To Len(sArchiveRef)
                                            If Left$(sArchiveRef, 1) = "*" Then
                                                sArchiveRef = Mid$(sArchiveRef, 2)
                                            Else
                                                Exit For
                                            End If
                                        Next lCounter
                                        If oBatch.Payments.Count = 1 And Not EmptyString(oPayment.REF_Bank1) Then
                                            sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                            sRef1 = oBatch.REF_Bank
                                        Else
                                            sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                            sRef1 = oPayment.REF_Bank1
                                        End If
                                        sRef2 = oPayment.REF_Bank2
                                        'Old code
                                        '''''                            If oBatch.Payments.Count = 1 And Not EmptyString(oPayment.REF_Bank1) Then
                                        '''''                                sArchiveRef = oPayment.REF_Bank1
                                        '''''                                For lCounter = 1 To Len(sArchiveRef)
                                        '''''                                    If Left$(sArchiveRef, 1) = "*" Then
                                        '''''                                        sArchiveRef = Mid$(sArchiveRef, 2)
                                        '''''                                    Else
                                        '''''                                        Exit For
                                        '''''                                    End If
                                        '''''                                Next lCounter
                                        '''''                                sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                        '''''                                sRef1 = oBatch.REF_Bank
                                        '''''                            Else
                                        '''''                                sArchiveRef = oBatch.REF_Bank
                                        '''''                                For lCounter = 1 To Len(sArchiveRef)
                                        '''''                                    If Left$(sArchiveRef, 1) = 0 Then
                                        '''''                                        sArchiveRef = Mid$(sArchiveRef, 2)
                                        '''''                                    Else
                                        '''''                                        Exit For
                                        '''''                                    End If
                                        '''''                                Next lCounter
                                        '''''                                sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                        '''''                                sRef1 = oPayment.REF_Bank1
                                        '''''                            End If
                                        '''''                            sRef2 = oPayment.REF_Bank2

                                    Else

                                        dDate = StringToDate(oPayment.DATE_Payment)
                                        If Trim$(oBatch.REF_Bank) = "*90000000" Then
                                            sArchiveRef = Replace(oBatch.REF_Bank, "*", "0")
                                            sArchiveRef = Left$(sArchiveRef, Len(sArchiveRef) - Len(Trim$(Str(lBatchcounter)))) & Trim$(Str(lBatchcounter))
                                        Else
                                            sArchiveRef = Replace(oBatch.REF_Bank, "*", "0")
                                        End If
                                        sRef1 = oPayment.REF_Bank1
                                        sRef2 = oPayment.REF_Bank2
                                    End If

                                    sFreetext = vbNullString
                                    If UCase(oBabel.Special) = "REDCROSS" Or UCase(oBabel.Special) = "KN" Or UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then  'XNET - 28.06.2011 - Added NF

                                        iFreetextCounter = 0
                                        sFreetext = vbNullString
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Original = True Then
                                                For Each oFreeText In oInvoice.Freetexts
                                                    If oFreeText.Qualifier < 3 Then
                                                        iFreetextCounter = iFreetextCounter + 1
                                                        If iFreetextCounter = 1 Then
                                                            sFreetext = RTrim$(oFreeText.Text)
                                                        ElseIf iFreetextCounter = 2 Then
                                                            sFreetext = sFreetext & " " & RTrim$(oFreeText.Text)
                                                        Else
                                                            Exit For
                                                        End If
                                                    End If
                                                Next oFreeText
                                            End If
                                        Next oInvoice
                                        sFreetext = sFreetext & " - " & Trim$(oPayment.E_Name)

                                        For Each oInvoice In oPayment.Invoices
                                            If Not bErrorInMaalgruppeExists Then
                                                'M�lgruppe, RK_ID, IB_EX_ID
                                                'Changed 15.02.2006
                                                If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                    If EmptyString(oInvoice.InvoiceNo) Then
                                                        bErrorInMaalgruppeExists = True
                                                        ' Type post
                                                        '-------
                                                    ElseIf EmptyString(oInvoice.MyField) Then
                                                        bErrorInMaalgruppeExists = True
                                                    End If
                                                End If
                                            End If
                                        Next
                                    Else
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                sFreetext = sFreetext & " " & Trim$(oFreeText.Text)
                                            Next
                                        Next
                                        sFreetext = Mid$(sFreetext, 2)
                                    End If

                                    If UCase(oBabel.Special) = "CHURCHAID" Or UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then 'XNET - 28.06.2011 - Added NF
                                        'XNET 28.06.2011 - Added next line
                                        oPayment.StatusSplittedFreetext = 0 'To be sure the birthnumber is extracted
                                        sBirthNumber = ExtractBirthNumber(oPayment) ' Run through freetext to catch F�dselsnr
                                        If Len(sBirthNumber) > 0 Then
                                            'XNET - 28.06.2011 - Added next IF - WHAT IS THIS???????? - We write oPayment.e_orgno
                                            If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then
                                                oPayment.e_OrgNo = sBirthNumber
                                            Else
                                                ' If valid birthnumber, set as adr2
                                                sAdr1 = sBirthNumber
                                            End If
                                        End If
                                    End If

                                    If oBabel.Special = "REDCROSS" Or oBabel.Special = "KN" Or UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then  'XNET - 28.06.2011 - Added NF
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MON_InvoiceAmount <> 0 Then
                                                    sLine = WriteLHLTransaction(UCase(oBabel.Special), sName, sAdr1, sAdr2, sZip, nAmount, dDate, TransformDnBNORReference(sArchiveRef), sId, oPayment.I_Account, oPayment.E_Account, TransformDnBNORReference(sRef1), TransformDnBNORReference(sRef2), sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oInvoice)
                                                    If Len(sLine) > 0 Then
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If
                                            End If
                                        Next oInvoice
                                    Else
                                        sLine = WriteLHLTransaction(UCase(oBabel.Special), sName, sAdr1, sAdr2, sZip, nAmount, dDate, sArchiveRef, sId, oPayment.I_Account, oPayment.E_Account, sRef1, sRef2, sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oPayment.Invoices.Item(1))
                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine(sLine)
                                        End If
                                    End If
                                    oPayment.Exported = True
                                End If

                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

            If oBabelFiles.Count > 0 Then
                If UCase(oBabelFiles.Item(1).Special) = "REDCROSS" Then
                    If bErrorInMaalgruppeExists Then
                        MsgBox("Det finnes eksporterte poster hvor m�lgruppe 999 er lagt inn.", vbInformation + vbOKOnly, "Feil i m�lgruppe")
                    End If
                End If
                If UCase(oBabelFiles.Item(1).Special) = "KN" Then
                    If bErrorInMaalgruppeExists Then
                        ' ikke viktig � varsle
                        ''''$MsgBox "Det finnes eksporterte poster hvor m�lgruppe 99999 er lagt inn.", vbInformation + vbOKOnly, "Feil i m�lgruppe"
                    End If
                End If
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteLHLEkstern" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteLHLEkstern = True

    End Function

    Public Function WriteLHLTransaction(ByVal sSpecial As String, ByVal sName As String, ByVal sAdr1 As String, ByVal sAdr2 As String, ByVal sZip As String, ByVal nAmount As Double, ByVal dDate As Date, ByVal sArchiveRef As String, ByVal sId As String, ByVal sToAccount As String, ByVal sFromAccount As String, ByVal sRef1 As String, ByVal sRef2 As String, ByVal sFreetext As String, ByVal sPayCode As String, ByVal oPayment As vbBabel.Payment, ByVal sGLAccount As String, ByVal sNoMatchAccount As String, ByVal oInvoice As vbBabel.Invoice) As String
        ' 03.01.05
        ' Used not only for LHL
        ' Test on sSpecial
        ' R�de Kors: REDCROSS
        ' Kirkens N�dhjelp: CHURCHAID
        '
        '
        Dim sLine As String
        Dim aNames() As String
        Dim sASString As String 'Store AS, ASA, A.S, etc, and add later
        Dim sJrString As String ' Check Jr. Sen. etc
        Dim i As Long
        Dim sMalGruppe As String
        Dim bTheInvoiceIsMatched As Boolean
        Dim sFreeTextVariable As String
        Dim oFreeText As vbBabel.Freetext
        Dim sAmount As String
        Dim bAbandon As Boolean

        'New 29.12.2005 -  REDCROSS is now treated in TreatSpecial
        If sSpecial <> "REDCROSS" And sSpecial <> "KN" And sSpecial <> "NORSK_FOLKEHJELP" Then  'XNET - 28.06.2011 - Added NF

            sMalGruppe = "69014"  ' Personer

            ' Remove hyphens from name
            sName = Replace(sName, "'", vbNullString)

            ' New tests 10.11.04
            If InStr(UCase(sName), "[") > 0 Then
                sName = Replace(sName, "[", "�")
            End If
            If InStr(UCase(sAdr1), "[") > 0 Then
                sAdr1 = Replace(sAdr1, "[", "�")
            End If
            If InStr(UCase(sAdr2), "[") > 0 Then
                sAdr2 = Replace(sAdr2, "[", "�")
            End If
            If InStr(UCase(sName), "]") > 0 Then
                sName = Replace(sName, "]", "�")
            End If
            If InStr(UCase(sAdr1), "]") > 0 Then
                sAdr1 = Replace(sAdr1, "]", "�")
            End If
            If InStr(UCase(sAdr2), "]") > 0 Then
                sAdr2 = Replace(sAdr2, "]", "�")
            End If
            If InStr(UCase(sName), "\") > 0 Then
                sName = Replace(sName, "\", "�")
            End If
            If InStr(UCase(sAdr1), "\") > 0 Then
                sAdr1 = Replace(sAdr1, "\", "�")
            End If
            If InStr(UCase(sAdr2), "\") > 0 Then
                sAdr2 = Replace(sAdr2, "\", "�")
            End If
            If InStr(UCase(sName), "@") > 0 Then
                sName = Replace(sName, "@", "�")
            End If
            If InStr(UCase(sAdr1), "@") > 0 Then
                sAdr1 = Replace(sAdr1, "@", "�")
            End If
            If InStr(UCase(sAdr2), "@") > 0 Then
                sAdr2 = Replace(sAdr2, "@", "�")
            End If
            If InStr(UCase(sName), "$") > 0 Then
                sName = Replace(sName, "$", "�")
            End If
            If InStr(UCase(sAdr1), "$") > 0 Then
                sAdr1 = Replace(sAdr1, "$", "�")
            End If
            If InStr(UCase(sAdr2), "$") > 0 Then
                sAdr2 = Replace(sAdr2, "$", "�")
            End If



            ' Check on AS, A.S, ASA etc;

            ' Start of namestring:
            If Left$(UCase(sName), 3) = "AS " Then
                sASString = "AS"
                sName = Replace(UCase(sName), "AS ", vbNullString)
            ElseIf Left$(UCase(sName), 3) = "AL " Then
                sASString = "AL"
                sName = Replace(UCase(sName), "AL ", vbNullString)
            ElseIf Left$(UCase(sName), 4) = "ASA " Then
                sASString = "ASA"
                sName = Replace(UCase(sName), "ASA ", vbNullString)
            ElseIf UCase(Left$(UCase(sName), 4)) = "ANS " Then
                sASString = "ANS"
                sName = Replace(UCase(sName), "ANS ", vbNullString)
            ElseIf Left$(UCase(sName), 4) = "A.S " Then
                sASString = "A.S"
                sName = Replace(UCase(sName), "A.S ", vbNullString)
            ElseIf Left$(UCase(sName), 4) = "A.L " Then
                sASString = "A.L"
                sName = Replace(UCase(sName), "A.L ", vbNullString)
            ElseIf Left$(UCase(sName), 4) = "A/S " Then
                sASString = "A/S"
                sName = Replace(UCase(sName), "A/S ", vbNullString)
            ElseIf Left$(UCase(sName), 4) = "A/L " Then
                sASString = "A/L"
                sName = Replace(UCase(sName), "A/L ", vbNullString)

                ' End of namestring
            ElseIf Right$(UCase(sName), 3) = " AS" Then
                sASString = "AS"
                sName = Replace(UCase(sName), " AS", vbNullString)
            ElseIf Right$(UCase(sName), 3) = " AL" Then
                sASString = "AL"
                sName = Replace(UCase(sName), " AL", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " ASA" Then
                sASString = "ASA"
                sName = Replace(UCase(sName), " ASA", vbNullString)
            ElseIf UCase(Right$(UCase(sName), 4)) = " ANS" Then
                sASString = "ANS"
                sName = Replace(UCase(sName), " ANS", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " A.S" Then
                sASString = "A.S"
                sName = Replace(UCase(sName), " A.S", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " A/S" Then
                sASString = "A/S"
                sName = Replace(UCase(sName), " A/S", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " A.L" Then
                sASString = "A.L"
                sName = Replace(UCase(sName), " A.L", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " A/L" Then
                sASString = "A/L"
                sName = Replace(UCase(sName), " A/L", vbNullString)

            Else
                sASString = vbNullString
            End If

            If Right$(sName, 4) = " Jr." Then
                sJrString = "Jr."
                sName = Replace(sName, " Jr.", vbNullString)
            ElseIf Right$(sName, 4) = " JR." Then
                sJrString = "JR."
                sName = Replace(sName, " JR.", vbNullString)
            ElseIf Right$(sName, 3) = " Jr" Then
                sJrString = "Jr"
                sName = Replace(sName, " Jr", vbNullString)
            ElseIf Right$(sName, 3) = " JR" Then
                sJrString = "JR"
                sName = Replace(sName, " JR", vbNullString)
            ElseIf Right$(sName, 4) = " Sen" Then
                sJrString = "Sen"
                sName = Replace(sName, " Sen", vbNullString)
            ElseIf Right$(sName, 4) = " sen" Then
                sJrString = "sen"
                sName = Replace(sName, " sen", vbNullString)
            ElseIf Right$(sName, 5) = " Sen." Then
                sJrString = "Sen."
                sName = Replace(sName, " Sen.", vbNullString)
            ElseIf Right$(sName, 5) = " sen." Then
                sJrString = "sen."
                sName = Replace(sName, " sen.", vbNullString)
            ElseIf Right$(sName, 4) = " Sr." Then
                sJrString = "Sr."
                sName = Replace(sName, " Sr.", vbNullString)
            ElseIf Left$(sName, 4) = "Hr. " Then
                sJrString = vbNullString
                sName = Replace(sName, "Hr. ", vbNullString)
            ElseIf Left$(sName, 3) = "Hr " Then
                sJrString = vbNullString
                sName = Replace(sName, "Hr ", vbNullString)
            ElseIf Left$(sName, 5) = "Herr " Then
                sJrString = vbNullString
                sName = Replace(sName, "Herr ", vbNullString)
            ElseIf Left$(sName, 4) = "Fr. " Then
                sJrString = vbNullString
                sName = Replace(sName, "Fr. ", vbNullString)
            ElseIf Left$(sName, 3) = "Fr " Then
                sJrString = vbNullString
                sName = Replace(sName, "Fr ", vbNullString)
            ElseIf Left$(sName, 4) = "Frk " Then
                sJrString = vbNullString
                sName = Replace(sName, "Frk ", vbNullString)
            ElseIf Left$(sName, 5) = "Frk. " Then
                sJrString = vbNullString
                sName = Replace(sName, "Frk. ", vbNullString)
            ElseIf Left$(sName, 4) = "Fru " Then
                sJrString = vbNullString
                sName = Replace(sName, "Fru ", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " D.Y" Then
                sJrString = vbNullString
                sName = Replace(UCase(sName), " D.Y ", vbNullString)
            ElseIf Right$(UCase(sName), 4) = " D.E" Then
                sJrString = vbNullString
                sName = Replace(UCase(sName), " D.E ", vbNullString)
            Else
                sJrString = vbNullString
            End If

            ' for Kirkens N�dhjelp, we need to find M�lgruppe:
            If sSpecial = "CHURCHAID" Then
                ' Find M�lgruppe Skole
                If InStr(UCase(sName), " SKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " SKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " BARNESKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " BARNESKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " UNGDOMSSKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " UNGDOMSSKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " VGS") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " SENTRALSKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " SENTRALSKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " H�GSKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " H�GSKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " S�NDAGSSKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " S�NDAGSSKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " S�NDAGSKOLE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " S�NDAGSKULE") > 0 Then
                    sMalGruppe = "69017"
                ElseIf InStr(UCase(sName), " UNIVERSITET") > 0 Then
                    sMalGruppe = "69017"

                    ' Find M�lgruppe Menighet:
                ElseIf InStr(UCase(sName), " MENIGHET") > 0 Then
                    sMalGruppe = "69016"
                ElseIf InStr(UCase(sName), " SOKN") > 0 Then
                    sMalGruppe = "69016"
                ElseIf InStr(UCase(sName), " BABTISTMENIGHET") > 0 Then
                    sMalGruppe = "69016"
                ElseIf InStr(UCase(sName), " FILADELFIA") > 0 Then
                    sMalGruppe = "69016"
                ElseIf InStr(UCase(sName), "FILADELFIA ") > 0 Then
                    sMalGruppe = "69016"
                ElseIf InStr(UCase(sName), " DOMKIRKE") > 0 Then
                    sMalGruppe = "69016"
                ElseIf InStr(UCase(sName), "DOMKIRKE ") > 0 Then
                    sMalGruppe = "69016"

                    ' Find M�lgruppe bedrifter
                ElseIf InStr(UCase(sName), " KOMMUNE") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "KOMMUNE ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " FYLKESKOMMUNE") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "FYLKESKOMMUNE ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "KIRKELIG ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " KIRKELIG") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "FELLESR�D ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " FELLESR�D") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "HOVEDORGAN ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " HOVEDORGAN") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "SPAREBANK ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " SPAREBANK") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "BONDELAG ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " BONDELAG") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "MOTORKLUBB ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " MOTORKLUBB") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "FORBUND ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " FORBUND") > 0 Then
                    sMalGruppe = "69015"
                ElseIf Right$(UCase(sName), 7) = "FORBUND" Then  ' Noen m� testes i slutten av navnet
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "FORLAG ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " FORLAG") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "FORENING ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " FORENING") > 0 Then
                    sMalGruppe = "69015"
                ElseIf Right$(UCase(sName), 8) = "FORENING" Then  ' Noen m� testes i slutten av navnet
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "STIFTELSE ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " STIFTELSE") > 0 Then
                    sMalGruppe = "69015"
                ElseIf Right$(UCase(sName), 9) = "STIFTELSE" Then  ' Noen m� testes i slutten av navnet
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "FAGFORENING ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " FAGFORENING") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "BARNEHAGE ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " BARNEHAGE") > 0 Then
                    sMalGruppe = "69015"
                ElseIf Right$(UCase(sName), 9) = "BARNEHAGE" Then  ' Noen m� testes i slutten av navnet
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "ROTARY ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " ROTARY") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "EFTF ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " EFTF") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "IDRETTSLAG ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " IDRETTSLAG") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "ANSATTE ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " ANSATTE") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), "BYDEL ") > 0 Then
                    sMalGruppe = "69015"
                ElseIf InStr(UCase(sName), " BYDEL") > 0 Then
                    sMalGruppe = "69015"
                ElseIf Not EmptyString(sASString) Then
                    sMalGruppe = "69015"

                    ' Anonyme (lenger ned i koden testes det p� blanke navn)
                ElseIf UCase(sName) = "NN" > 0 Then
                    sMalGruppe = "69018"
                ElseIf UCase(sName) = "N N" > 0 Then
                    sMalGruppe = "69018"
                End If
            End If


            If Len(sASString) > 0 Then
                ' Keep name in same order, but must capitalize
                aNames = Split(Trim(sName))
                sName = vbNullString
                ' Capitalize first letter in all parts of name, uncap all others;
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    sName = sName & aNames(i) & " "
                Next

                ' AS, ASA, etc., keep in same order
                sName = Left$(PadRight(sName & " " & sASString, 40, " "), 40)
            Else

                ' new 30.10.2003
                ' Some names in CREMUL is a mix of name + adress,
                ' e.g. NAD+PL+++Inge Hegvik,Sagav.14,3050 Mj�n+SAGAVN.14+3050 MJ�NDALEN++0000
                ' Common; it is both a comma in name, and zip is part of name
                If InStr(sName, ",") > 0 Then
                    If InStr(sName, sZip) > 0 Then
                        ' remove from first comma in name;
                        sName = Left$(sName, InStr(sName, ",") - 1)
                    ElseIf InStr(UCase(sAdr1), UCase(Trim$(Mid$(sName, InStr(sName, ",") + 1)))) > 0 Or _
                        InStr(UCase(sAdr2), UCase(Trim$(Mid$(sName, InStr(sName, ",") + 1)))) > 0 Then
                        ' NAD+PL+++Merethe Hannestad,Stockflethsg+STOCKFLETHSG 56 A+0461 OSLO
                        ' where comma in name and part of adr2 is in name
                        sName = Left$(sName, InStr(sName, ",") - 1)
                    End If
                End If
                '----
                'If InStr(sName, ",") > 0 Then
                If InStr(sName, ",") > 0 Then
                    If sMalGruppe <> "69016" And sMalGruppe <> "69017" Then
                        ' If comma in name, or Menighet/Skole keep name as is, except from comma
                        sName = Replace(sName, ",", vbNullString)
                        ' Keep name in same order, but must capitalize
                        aNames = Split(Trim(sName))
                        sName = vbNullString
                        ' Capitalize first letter in all parts of name, uncap all others;
                        For i = 0 To UBound(aNames)
                            aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                            ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                            If InStr(aNames(i), "-") > 0 Then
                                aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                            End If
                            ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                            If InStr(aNames(i), "/") > 0 Then
                                aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                            End If

                            sName = sName & aNames(i) & " "
                        Next
                    End If
                Else
                    ' New 24.02.05 for Kirkens N�dhjelp;
                    If sMalGruppe <> "69015" And sMalGruppe <> "69016" And sMalGruppe <> "69017" Then
                        ' New 04.01.05 for Kirkens N�dhjelp
                        sMalGruppe = "69014"  ' Personer
                        'Name must be split in lastname + space + firstname
                        ' Strip name from . and ,
                        sName = Replace(Replace(sName, ".", vbNullString), ",", vbNullString)

                        aNames = Split(Trim(sName))

                        ' Capitalize first letter in all parts of name, uncap all others;
                        For i = 0 To UBound(aNames)
                            aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                            ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                            If InStr(aNames(i), "-") > 0 Then
                                aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                            End If
                            ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                            If InStr(aNames(i), "/") > 0 Then
                                aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                            End If

                        Next

                        Select Case UBound(aNames)
                            Case -1
                                ' Empty name
                                If sSpecial = "REDCROSS" Or sSpecial = "KN" Then
                                    ' Differ between paymenttypes
                                    If sPayCode = "602" Then
                                        ' Manual payments
                                        sName = "MANUELL"
                                    Else
                                        sName = "NN"
                                    End If
                                ElseIf sSpecial = "CHURCHAID" Then
                                    sMalGruppe = "69018"
                                    sName = "NN"
                                    If sPayCode = "602" Then
                                        sName = "NN" 'test
                                    End If
                                Else
                                    sName = "Ukjent"
                                End If
                            Case 0
                                ' En streng i navnefeltet
                                sName = aNames(0)
                            Case 1
                                ' To strenger i navnefeltet, siste f�rst
                                sName = aNames(1) + " " + aNames(0)
                            Case 2
                                ' Tre, siste f�rst!
                                sName = aNames(2) + " " + aNames(0) + " " + aNames(1)
                            Case 3
                                ' Fire, siste f�rst!
                                sName = aNames(3) + " " + aNames(0) + " " + aNames(1) + " " + aNames(2)
                            Case Else
                                ' Flere enn fire , siste f�rst!
                                sName = aNames(UBound(aNames)) + " " + aNames(0) + " " + aNames(1) + " " + aNames(2)
                        End Select
                    End If
                End If
                If sName <> "Ukjent" Then
                    sName = sName + " " & sJrString
                    sName = PadRight(Left$(sName, 40), 40, " ")
                End If
            End If

            ' New 08.12.03
            ' Replace specialchars for �,�,�

            If InStr(UCase(sName), Chr(162)) > 0 Then
                sName = Replace(sName, Chr(162), "�")
            End If
            If InStr(UCase(sAdr1), Chr(162)) > 0 Then
                sAdr1 = Replace(sAdr1, Chr(162), "�")
            End If
            If InStr(UCase(sAdr2), Chr(162)) > 0 Then
                sAdr2 = Replace(sAdr2, Chr(162), "�")
            End If
            If InStr(UCase(sName), Chr(187)) > 0 Then
                sName = Replace(sName, Chr(187), "�")
            End If
            If InStr(UCase(sAdr1), Chr(187)) > 0 Then
                sAdr1 = Replace(sAdr1, Chr(187), "�")
            End If
            If InStr(UCase(sAdr2), Chr(187)) > 0 Then
                sAdr2 = Replace(sAdr2, Chr(187), "�")
            End If


        End If

        If sSpecial = "REDCROSS" Or sSpecial = "KN" Or sSpecial = "NORSK_FOLKEHJELP" Then  'XNET - 28.06.2011 - Added NF
            'For Each oinvoice In oPayment.Invoices
            bAbandon = False

            'New 17.02.2006
            'Add the variable part of the freetext

            'XNET 12.12.2011 - Added next IF
            If sSpecial = "NORSK_FOLKEHJELP" Then
                sFreeTextVariable = ""
            Else
                sFreeTextVariable = sFreetext
            End If
            For Each oFreeText In oInvoice.Freetexts
                If oFreeText.Qualifier > 3 Then
                    sFreeTextVariable = RTrim$(oFreeText.Text) & " " & sFreeTextVariable
                End If
            Next oFreeText

            'End new


            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                'sNoMatchAccount
                If UCase(Trim$(oInvoice.CustomerNo)) = UCase(Trim$(sNoMatchAccount)) Then
                    bTheInvoiceIsMatched = False
                ElseIf EmptyString(oInvoice.CustomerNo) Then
                    bTheInvoiceIsMatched = False
                Else
                    bTheInvoiceIsMatched = True
                End If
                If bTheInvoiceIsMatched Then
                    ''''''                If oPayment.I_Account = "16440865058" And oInvoice.CustomerNo = oInvoice.MATCH_ID Then
                    ''''''                    ' added 22.04.2009 for PayEx/Artisti KID-records with deviations
                    ''''''                    ' this is the deviation-record
                    ''''''                    ' Then the ps_id is int Match_ID, which must be moved to .InvoiceNo
                    ''''''                    oInvoice.InvoiceNo = oInvoice.MATCH_ID
                    ''''''                    sLine = Space(8)
                    ''''''                Else
                    sLine = PadLeft(oInvoice.CustomerNo, 8, " ")
                    ''''''                End If
                Else
                    sLine = Space(8)
                End If

                sLine = sLine & PadRight(sName, 40, " ")
                ' Adr1
                '-----
                ' Remove commas and points from adr1;
                If InStr(sAdr1, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr1 = Replace(Replace(sAdr1, ".", vbNullString), ",", vbNullString)
                    sAdr1 = Replace(Replace(sAdr1, ".", " "), ",", " ")
                    ' not two spaces!!
                    sAdr1 = Replace(sAdr1, "  ", " ")
                End If

                ' Capitalize first letter in each part;
                aNames = Split(Trim(sAdr1))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                sAdr1 = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    sAdr1 = sAdr1 & aNames(i) & " "

                Next
                sLine = sLine + Left$(PadRight(sAdr1, 30, " "), 30)


                ' Adr2
                '-----
                ' Remove commas and points from adr2;
                If InStr(sAdr2, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr2 = Replace(Replace(sAdr2, ".", vbNullString), ",", vbNullString)
                    sAdr2 = Replace(Replace(sAdr2, ".", " "), ",", " ")
                    ' not two spaces!!
                    sAdr2 = Replace(sAdr2, "  ", " ")
                End If
                ' Capitalize first letter in each part;
                aNames = Split(Trim(sAdr2))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                sAdr2 = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    sAdr2 = sAdr2 & aNames(i) & " "
                Next
                sLine = sLine + Left$(PadRight(sAdr2, 30, " "), 30)

                sLine = sLine + Space(30)

                ' Zip
                '----
                sLine = sLine + Left$(PadLeft(sZip, 4, "0"), 4)

                'XNET - 28.06.2011 - added next IF
                If sSpecial = "NORSK_FOLKEHJELP" Then
                    If oPayment.ExtraD1 = "P" Then
                        If oPayment.ExtraD2 = "K" Then
                            sLine = sLine + "K"  ' Kj�nn
                        ElseIf oPayment.ExtraD2 = "M" Then
                            sLine = sLine + "M"  ' Kj�nn
                        Else
                            sLine = sLine + "I"  ' Kj�nn
                        End If
                    Else
                        sLine = sLine + "I"  ' Kj�nn
                    End If
                Else
                    sLine = sLine + "I"  ' Kj�nn
                End If
                If sSpecial = "NORSK_FOLKEHJELP" And oPayment.ExtraD3 = "UPDATENAME" Then
                    sLine = sLine + "J"  'Endringskode
                Else
                    sLine = sLine + " "  'Endringskode
                End If

                ' added next if 18.12.2008 - Special for Kirkens N�dhjelp Teller;
                'If sSpecial = "KN" And oPayment.ExtraD3 = "Teller" Then
                '    ' for Teller-payments we fill in ext.id into Match_ID
                '    sLine = sLine + PadLeft(oInvoice.MATCH_ID, 8, " ") 'BetalingsID -rk_Id, ib_ex_id eller ia_id

                'XNET - 28.06.2011 - Added next IF
                If sSpecial = "NORSK_FOLKEHJELP" And oInvoice.MATCH_Matched = False Then
                    oInvoice.InvoiceNo = "" 'Sometimes there are stated an invoiceno
                End If

                If (sSpecial = "REDCROSS" Or sSpecial = "KN") And oInvoice.MyField = "ISWAP" Then
                    ' for PayEx-payments we fill in ext.id into Match_ID
                    sLine = sLine + PadLeft(oInvoice.MATCH_ID, 8, " ") 'BetalingsID -rk_Id, ib_ex_id eller ia_id
                ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
                    sLine = sLine + PadLeft(oInvoice.InvoiceNo, 8, " ") 'BetalingsID -rk_Id, ib_ex_id eller ia_id
                Else
                    If sSpecial = "REDCROSS" Then
                        sLine = sLine & "     999"
                        'XNET - 28.06.2011 - Added next ElseIf
                    ElseIf sSpecial = "NORSK_FOLKEHJELP" Then
                        sLine = sLine & "  610009"
                    Else
                        sLine = sLine & "   99999"
                    End If
                End If

                ' Type post
                '-------
                If Not EmptyString(oInvoice.MyField) Then
                    sLine = sLine & Left$(oInvoice.MyField, 1)
                Else
                    sLine = sLine & "M"
                End If

                'New code 10.03.2006
                ' Amount - kroneamount
                '-------
                ' Amount - �reamount
                '--------------------
                sAmount = Trim$(Str(oInvoice.MON_InvoiceAmount))
                If Len(sAmount) > 2 Then
                    sLine = sLine & PadLeft(Left$(sAmount, Len(sAmount) - 2), 9, " ")
                    sLine = sLine & Right$(sAmount, 2)
                ElseIf Len(sAmount) = 1 Then
                    sLine = sLine & PadLeft("0", 9, " ")
                    sLine = sLine & "0" & sAmount
                ElseIf Len(sAmount) = 2 Then
                    sLine = sLine & PadLeft("0", 9, " ")
                    sLine = sLine & sAmount
                Else
                    bAbandon = True
                End If

                ''                'Old code
                ''                ' Amount - kroneamount
                ''                '-------
                ''                sLine = sLine & PadLeft(Left$(Trim$(Str(oInvoice.MON_InvoiceAmount)), Len(Trim$(Str(oInvoice.MON_InvoiceAmount))) - 2), 9, " ")
                ''
                ''                ' Amount - �reamount
                ''                '--------------------
                ''                sLine = sLine & Right$(Trim$(Str(oInvoice.MON_InvoiceAmount)), 2)

                ' Betalingsm�te
                '---------------
                sLine = sLine & "B"

                'Persontype
                'Add if company or not
                If Not EmptyString(oPayment.ExtraD1) Then
                    sLine = sLine & Left$(oPayment.ExtraD1, 1)
                Else
                    sLine = sLine & "P"
                End If

                'Type bedrift, 167-168
                '----------------------
                If sSpecial = "KN" Then
                    ' always blank for KN
                    sLine = sLine & "  "
                Else
                    If Not EmptyString(oPayment.ExtraD2) Then
                        'XNET - 28.06.2011 - added next IF
                        If sSpecial = "NORSK_FOLKEHJELP" Then
                            sLine = sLine & PadLeft(Trim$(oPayment.ExtraD2), 2, " ")
                        Else
                            sLine = sLine & " " & Left$(oPayment.ExtraD2, 1)
                        End If
                    Else
                        If Left$(oPayment.ExtraD1, 1) = "A" Then
                            'XNET - 28.06.2011
                            If sSpecial = "NORSK_FOLKEHJELP" Then
                                sLine = sLine & " 2"
                            Else
                                sLine = sLine & " 1"
                            End If
                        Else
                            sLine = sLine & "  "
                        End If
                    End If
                End If

                ' Date
                '-----
                If dDate > (System.DateTime.FromOADate(Now.ToOADate - 1000)) Then
                    ' hvis vi har lagret en dato, bruk denne
                    sLine = sLine & Format(dDate, "dd.mm.yyyy")
                Else
                    ' vi har ikke lagret dato, sett inn dagens dato
                    sLine = sLine & Format(Now, "dd.mm.yyyy")
                End If

                'Telefonnummer
                '--------------
                'XNET - 11.08.2001 - Added next IF
                If oPayment.PayCode = "649" Then
                    sLine = sLine & PadRight(oPayment.REF_Own, 15, " ")
                Else
                    sLine = sLine & Space(15) 'For givertelefon
                End If

                'E-post
                '-----------
                sLine = sLine & Space(40) '

                'Archiveref
                '-------------
                sLine = sLine & PadRight(sArchiveRef, 13, " ")

                'Kontonummer - Hovedbokskonto bank
                sLine = sLine & PadRight(sGLAccount, 9, " ")
                'Use sFromAccount to find the HB

                ' Fromaccount
                '--------------
                If sSpecial = "KN" Or sSpecial = "NORSK_FOLKEHJELP" Then 'XNET - 28.06.2011 - added NF
                    ' For Kirkens N�dhjelp, update with crypted fromaccount
                    sLine = sLine & PadRight$(BBEncrypt(sFromAccount, 95), 11, " ")
                Else
                    sLine = sLine & PadRight$(sFromAccount, 11, " ")
                End If

                If sSpecial = "KN" And oPayment.ExtraD3 = "Teller" Then
                    ' for Teller, set ordreID as text
                    If oInvoice.Freetexts.Count > 0 Then
                        sFreeTextVariable = Trim$(oInvoice.Freetexts.Item(1).Text)
                    End If
                Else
                    ' Add first 80 of freetext
                    sFreeTextVariable = Replace(Replace(sFreeTextVariable, Chr(124), "1"), Chr(34), vbNullString)
                End If
                If sSpecial = "REDCROSS" And oInvoice.MyField = "ISWAP" Then
                    'NRX doesn't want any freetext written on payEx. It will overwrite the freetext today.
                    'Changed 12.02.2010 - Added ORDR in front of the text
                    sLine = sLine & PadRight("ORDR" & oInvoice.InvoiceNo, 80, " ")
                    'Old code
                    'sLine = sLine & PadRight(oInvoice.InvoiceNo, 80, " ")
                Else
                    'XNET - 28.06.2011 - Changed next line
                    sLine = sLine & PadRight(Replace(Left$(Replace(Replace(sFreeTextVariable, vbCr, vbNullString), vbLf, vbNullString), 80), "|", " "), 80, " ")
                End If

                ' Add unique ref (ref1)
                sLine = sLine & PadRight$(sRef1, 30, " ")
                ' Add unique ref (ref2)
                sLine = sLine & PadRight$(sRef2, 30, " ")

                If sSpecial = "KN" Then
                    ' Kirkens N�dhjelp, med "ordentlig avstemming"
                    ' NOT COMPLETE; snakk med Kristin i MySoft om posisjoner
                    'Har snakket med Kristin, det er OK. Vi beholder det blanke feltet.

                    sLine = sLine & Space(1) ' mangler av en eller annen grunnen 1 pos f�r kontonr i pos 408
                    ' -   Nytt felt; pos 408-418: Mottakers kontonummer
                    sLine = sLine & PadRight(oPayment.I_Account, 11, " ")

                    ' -   Nytt felt; post 419-429: F�dselsnr,
                    If oPayment.e_OrgNo = "00000000000" Then
                        sLine = sLine & Space(11)
                    Else
                        sLine = sLine & PadRight(oPayment.e_OrgNo, 11, " ")
                    End If

                    'XNET - 28.06.2011
                ElseIf sSpecial = "NORSK_FOLKEHJELP" Then
                    sLine = sLine & Space(1) ' mangler av en eller annen grunnen 1 pos f�r kontonr i pos 408
                    ' -   Nytt felt; pos 408-418: Mottakers kontonummer - Don't use
                    sLine = sLine & Space(11)

                    ' -   Nytt felt; post 419-429: F�dselsnr,
                    If oPayment.e_OrgNo = "00000000000" Or EmptyString(oPayment.e_OrgNo) Then
                        sLine = sLine & Space(11)
                    Else
                        sLine = sLine & PadRight(oPayment.e_OrgNo, 11, " ")
                    End If

                End If

                ' Add Teller provisjon if Teller-record;
                'If sSpecial = "KN" And oPayment.ExtraD3 = "Teller" Then
                '    sAmount = Trim$(Str(oPayment.MON_InvoiceAmount - oPayment.MON_TransferredAmount))
                '    If Len(sAmount) > 2 Then
                '        sLine = sLine & PadLeft(Left$(sAmount, Len(sAmount) - 2), 9, " ")
                '        sLine = sLine & Right$(sAmount, 2)
                '    ElseIf Len(sAmount) = 1 Then
                '        sLine = sLine & Space(10) & sAmount
                '    Else
                '        sLine = sLine & Space(10) & "0"
                '    End If
                'Else
                sLine = sLine & Space(10) & "0"
                'End If

                sLine = sLine & " " ' Extra space at end of line

                If bAbandon Then
                    WriteLHLTransaction = vbNullString
                Else
                    WriteLHLTransaction = sLine
                End If
            End If 'If oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then
            'Next oinvoice
        Else
            'Other companies
            If sName <> "Ukjent" Then
                ' Don't export records without names1
                sLine = Space(8)

                sLine = sLine & sName

                ' Adr1
                '-----
                ' Remove commas and points from adr1;
                If InStr(sAdr1, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr1 = Replace(Replace(sAdr1, ".", vbNullString), ",", vbNullString)
                    sAdr1 = Replace(Replace(sAdr1, ".", " "), ",", " ")
                    ' not two spaces!!
                    sAdr1 = Replace(sAdr1, "  ", " ")
                End If

                ' Capitalize first letter in each part;
                aNames = Split(Trim(sAdr1))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                sAdr1 = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    sAdr1 = sAdr1 & aNames(i) & " "

                Next
                sLine = sLine + Left$(PadRight(sAdr1, 30, " "), 30)


                ' Adr2
                '-----
                ' Remove commas and points from adr2;
                If InStr(sAdr2, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr2 = Replace(Replace(sAdr2, ".", vbNullString), ",", vbNullString)
                    sAdr2 = Replace(Replace(sAdr2, ".", " "), ",", " ")
                    ' not two spaces!!
                    sAdr2 = Replace(sAdr2, "  ", " ")
                End If
                ' Capitalize first letter in each part;
                aNames = Split(Trim(sAdr2))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                sAdr2 = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    sAdr2 = sAdr2 & aNames(i) & " "
                Next
                sLine = sLine + Left$(PadRight(sAdr2, 30, " "), 30)

                ' Zip
                '----
                sLine = sLine + Left$(PadLeft(sZip, 4, "0"), 4)

                sLine = sLine + "I"  ' Kj�nn
                sLine = sLine + " "  'Endringskode
                If sSpecial = "CHURCHAID" Then
                    ' Two sets of M�lgruppe, one for Account 80, and one for 100
                    ' If Account 100 (1602.40.26535), then translate
                    If sToAccount = "16024026535" Then
                        Select Case sMalGruppe
                            Case "69014"
                                sMalGruppe = "60012" ' Personer
                            Case "69015"
                                sMalGruppe = "60013" ' Bedrifter
                            Case "69016"
                                sMalGruppe = "60014" ' Menigheter
                            Case "69017"
                                sMalGruppe = "60015" ' Skoler
                            Case "69018"
                                sMalGruppe = "60016" ' Anonyme
                        End Select
                    End If
                    sLine = sLine + PadRight(sMalGruppe, 8, " ") 'Space(8) 'M�lgr.ident
                Else
                    sLine = sLine + Space(8) 'M�lgr.ident
                End If
                sLine = sLine + Space(5) ' U.�lgr.

                ' Amount
                '-------
                sLine = sLine + Right$(PadLeft(Format(nAmount / 100, "###0.00"), 10, " "), 10)

                ' Date
                '-----
                sLine = sLine + "B"     'Bet m�te, B=Bank
                If dDate > (System.DateTime.FromOADate(Now.ToOADate - 1000)) Then
                    ' hvis vi har lagret en dato, bruk denne
                    sLine = sLine & Format(dDate, "dd mm yy")
                Else
                    ' vi har ikke lagret dato, sett inn dagens dato
                    sLine = sLine & Format(Now, "dd mm yy")
                End If
                sLine = sLine & " " ' Extra space at end of line

                If sSpecial <> "CHURCHAID" Then
                    ' New 29.10.03
                    ' Added ArkivRef at end of line
                    sLine = sLine & PadRight$(sArchiveRef, 12, " ")
                End If

                ' Special for R�de Kors
                ' sSpecial = "REDCROSS"
                If sSpecial = "REDCROSS" Then
                    ' Add fromaccount
                    sLine = sLine & PadRight$(sFromAccount, 35, " ")
                    ' Add toaccount
                    sLine = sLine & PadRight$(sToAccount, 11, " ")
                    ' Add unique ref (ref1)
                    sLine = sLine & PadRight$(sRef1, 30, " ")
                    ' Add unique ref (ref2)
                    sLine = sLine & PadRight$(sRef2, 30, " ")
                    'Add if company or not
                    If Len(sASString) > 0 Then
                        sLine = sLine & "B"
                    Else
                        sLine = sLine & "P"
                    End If
                    ' Add first 250 of freetext
                    sLine = sLine & PadRight(Left$(sFreetext, 250), 250, " ")
                End If

                ' FIX: temp - test for Kirkens N�dhjelp
                ''''''''        sLine = sLine & PadRight$(sId, 16, " ")
                ''''''''        sLine = sLine & PadRight$(sFromAccount, 11, " ")

                WriteLHLTransaction = sLine
            Else
                WriteLHLTransaction = vbNullString
            End If
        End If 'If sSpecial = "REDCROSS"
    End Function
    Function WriteMySoftCRM(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean

        ' 03.01.05
        ' Used by Norsk Folkehjelp
        '
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, dDate As Date
        Dim sName As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sId As String
        Dim sFreetext As String
        Dim sRef1 As String, sRef2 As String
        Dim sBirthNumber As String
        Dim sGLAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sNoMatchAccount As String
        Dim lBatchcounter As Long
        Dim bErrorInMaalgruppeExists As Boolean
        Dim iFreetextCounter As Integer
        Dim lCounter As Long
        Dim aAccountArray(,) As String
        Dim iArrayCounter As Integer
        Dim sAccountToExport As String = ""

        '
        ' create an outputfile
        Try

            bAppendFile = False
            sOldAccountNo = vbNullString
            sGLAccount = vbNullString
            bAccountFound = False
            lBatchcounter = 0
            bErrorInMaalgruppeExists = False

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            'New code 13.06.2017 - sort the payments per I_Account
            ' Do this by traversing through all accounts 
            'In other parts of the code search for sAccountToExport
            aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

            For iArrayCounter = 0 To aAccountArray.GetUpperBound(1)
                sAccountToExport = aAccountArray(1, iArrayCounter)
                For Each oBabel In oBabelFiles

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If Not oPayment.Exported Then
                                If oPayment.I_Account = sAccountToExport Then
                                    If IsOCR(oPayment.PayCode) Then
                                        'Nothing to do, don't export
                                    Else
                                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                            If Not bMultiFiles Then
                                                bExportoBabel = True
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If oPayment.REF_Own = sOwnRef Then
                                                        bExportoBabel = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                    If bExportoBabel Then
                                        Exit For
                                    End If
                                Else
                                    'DO NOT EXPORT
                                End If
                            End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next

                    If bExportoBabel Then

                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If Not oPayment.Exported Then
                                    If oPayment.I_Account = sAccountToExport Then
                                        If IsOCR(oPayment.PayCode) Then
                                            'Nothing to do, don't export
                                        Else
                                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                                If Not bMultiFiles Then
                                                    bExportoBatch = True
                                                Else
                                                    If oPayment.I_Account = sI_Account Then
                                                        If oPayment.REF_Own = sOwnRef Then
                                                            bExportoBatch = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                        'If true exit the loop, and we have data to export
                                        If bExportoBatch Then
                                            Exit For
                                        End If
                                    Else
                                        'DO NOT EXPORT YET
                                    End If
                                End If
                            Next

                            If bExportoBatch Then

                                lBatchcounter = lBatchcounter + 1
                                For Each oPayment In oBatch.Payments

                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If Not oPayment.Exported Then
                                        If oPayment.I_Account = sAccountToExport Then
                                            If IsOCR(oPayment.PayCode) Then
                                                'Nothing to do, don't export
                                            Else
                                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                                    If Not bMultiFiles Then
                                                        bExportoPayment = True
                                                    Else
                                                        If oPayment.I_Account = sI_Account Then
                                                            If oPayment.REF_Own = sOwnRef Then
                                                                bExportoPayment = True
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If

                                        Else
                                            'DO NOT EXPORT YET
                                        End If
                                    End If

                                    If bExportoPayment Then

                                        If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then
                                            If oPayment.I_Account <> sOldAccountNo Then
                                                If oPayment.VB_ProfileInUse Then
                                                    bAccountFound = False
                                                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                        For Each oClient In oFilesetup.Clients
                                                            For Each oaccount In oClient.Accounts
                                                                If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                                                    sGLAccount = Trim$(oaccount.GLAccount)
                                                                    sOldAccountNo = oPayment.I_Account
                                                                    bAccountFound = True

                                                                    Exit For
                                                                End If
                                                            Next oaccount
                                                            If bAccountFound Then Exit For
                                                        Next oClient
                                                        If bAccountFound Then Exit For
                                                    Next oFilesetup
                                                    If bAccountFound Then
                                                        sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                    Else
                                                        Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                                        '12003: Could not find account %1.Please enter the account in setup."
                                                    End If
                                                End If
                                            End If
                                        End If 'If UCase(oBabel.Special) = "REDCROSS" Then

                                        '-------- fetch content of each payment ---------
                                        sName = oPayment.E_Name
                                        nAmount = oPayment.MON_TransferredAmount

                                        'sArchiveRef = oPayment.REF_Bank1
                                        ' Skal benyttes til avstemming bank

                                        If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then

                                            dDate = StringToDate(oPayment.DATE_Payment)
                                            '12.09.2006 - new code to write the correct archiveref for bankreconciliation
                                            sArchiveRef = Trim$(oBatch.REF_Bank)
                                            For lCounter = 1 To Len(sArchiveRef)
                                                If Left$(sArchiveRef, 1) = "*" Then
                                                    sArchiveRef = Mid$(sArchiveRef, 2)
                                                Else
                                                    Exit For
                                                End If
                                            Next lCounter
                                            If oBatch.Payments.Count = 1 And Not EmptyString(oPayment.REF_Bank1) Then
                                                sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                                sRef1 = oBatch.REF_Bank
                                            Else
                                                sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                                sRef1 = oPayment.REF_Bank1
                                            End If
                                            sRef2 = oPayment.REF_Bank2

                                        Else

                                            dDate = StringToDate(oPayment.DATE_Payment)
                                            If Trim$(oBatch.REF_Bank) = "*90000000" Then
                                                sArchiveRef = Replace(oBatch.REF_Bank, "*", "0")
                                                sArchiveRef = Left$(sArchiveRef, Len(sArchiveRef) - Len(Trim$(Str(lBatchcounter)))) & Trim$(Str(lBatchcounter))
                                            Else
                                                sArchiveRef = Replace(oBatch.REF_Bank, "*", "0")
                                            End If
                                            sRef1 = oPayment.REF_Bank1
                                            sRef2 = oPayment.REF_Bank2
                                        End If

                                        sFreetext = vbNullString
                                        If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then

                                            iFreetextCounter = 0
                                            sFreetext = vbNullString
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Original = True Then
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        If oFreeText.Qualifier < 3 Then
                                                            iFreetextCounter = iFreetextCounter + 1
                                                            If iFreetextCounter = 1 Then
                                                                sFreetext = RTrim$(oFreeText.Text)
                                                            ElseIf iFreetextCounter = 2 Then
                                                                sFreetext = sFreetext & " " & RTrim$(oFreeText.Text)
                                                            Else
                                                                Exit For
                                                            End If
                                                        End If
                                                    Next oFreeText
                                                End If
                                            Next oInvoice
                                            sFreetext = sFreetext & " - " & Trim$(oPayment.E_Name)

                                            For Each oInvoice In oPayment.Invoices
                                                If Not bErrorInMaalgruppeExists Then
                                                    'M�lgruppe, RK_ID, IB_EX_ID
                                                    'Changed 15.02.2006
                                                    If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                        If EmptyString(oInvoice.InvoiceNo) Then
                                                            bErrorInMaalgruppeExists = True
                                                            ' Type post
                                                            '-------
                                                        ElseIf EmptyString(oInvoice.MyField) Then
                                                            bErrorInMaalgruppeExists = True
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        Else
                                            For Each oInvoice In oPayment.Invoices
                                                For Each oFreeText In oInvoice.Freetexts
                                                    sFreetext = sFreetext & " " & Trim$(oFreeText.Text)
                                                Next
                                            Next
                                            sFreetext = Mid$(sFreetext, 2)
                                        End If

                                        If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then
                                            'XNET 28.06.2011 - Added next line
                                            oPayment.StatusSplittedFreetext = 0 'To be sure the birthnumber is extracted
                                            sBirthNumber = ExtractBirthNumber(oPayment) ' Run through freetext to catch F�dselsnr
                                            If Len(sBirthNumber) > 0 Then
                                                'XNET - 28.06.2011 - Added next IF - WHAT IS THIS???????? - We write oPayment.e_orgno
                                                If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then
                                                    oPayment.e_OrgNo = sBirthNumber
                                                End If
                                            End If
                                        End If

                                        If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Final Then
                                                    If oInvoice.MON_InvoiceAmount <> 0 Then
                                                        sLine = WriteMySoftCRMTransaction(UCase(oBabel.Special), sName, nAmount, dDate, TransformDnBNORReference(sArchiveRef), sId, oPayment.I_Account, oPayment.E_Account, TransformDnBNORReference(sRef1), TransformDnBNORReference(sRef2), sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oInvoice)
                                                        If Len(sLine) > 0 Then
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                End If
                                            Next oInvoice
                                        Else
                                            sLine = WriteMySoftCRMTransaction(UCase(oBabel.Special), sName, nAmount, dDate, sArchiveRef, sId, oPayment.I_Account, oPayment.E_Account, sRef1, sRef2, sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oPayment.Invoices.Item(1))
                                            If Len(sLine) > 0 Then
                                                oFile.WriteLine(sLine)
                                            End If
                                        End If
                                        oPayment.Exported = True
                                    End If

                                Next ' payment
                            End If
                        Next 'batch
                    End If

                Next
            Next iArrayCounter

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteMySoftCRM" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteMySoftCRM = True

    End Function

    Public Function WriteMySoftCRMTransaction(ByVal sSpecial As String, ByVal sName As String, ByVal nAmount As Double, ByVal dDate As Date, ByVal sArchiveRef As String, ByVal sId As String, ByVal sToAccount As String, ByVal sFromAccount As String, ByVal sRef1 As String, ByVal sRef2 As String, ByVal sFreetext As String, ByVal sPayCode As String, ByVal oPayment As vbBabel.Payment, ByVal sGLAccount As String, ByVal sNoMatchAccount As String, ByVal oInvoice As vbBabel.Invoice) As String
        ' 03.01.05
        ' Used by Norsk Folkehjelp
        '
        Dim sLine As String
        Dim i As Long
        Dim sMalGruppe As String
        Dim bTheInvoiceIsMatched As Boolean
        Dim sFreeTextVariable As String
        Dim oFreeText As vbBabel.Freetext
        Dim sAmount As String
        Dim bAbandon As Boolean

        'For Each oinvoice In oPayment.Invoices
        bAbandon = False

        'New 17.02.2006
        'Add the variable part of the freetext

        'XNET 12.12.2011 - Added next IF
        If sSpecial = "NORSK_FOLKEHJELP" Then
            sFreeTextVariable = ""
        Else
            sFreeTextVariable = sFreetext
        End If
        For Each oFreeText In oInvoice.Freetexts
            If oFreeText.Qualifier > 3 Then
                sFreeTextVariable = RTrim$(oFreeText.Text) & " " & sFreeTextVariable
            End If
        Next oFreeText

        If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
            'sNoMatchAccount
            If UCase(Trim$(oInvoice.CustomerNo)) = UCase(Trim$(sNoMatchAccount)) Then
                bTheInvoiceIsMatched = False
            ElseIf EmptyString(oInvoice.CustomerNo) Then
                bTheInvoiceIsMatched = False
            Else
                bTheInvoiceIsMatched = True
            End If
            If bTheInvoiceIsMatched Then
                sLine = oInvoice.CustomerNo.Trim & ";"
            Else
                sLine = ";"
            End If

            sLine = sLine & Chr(34) & sName.Trim & Chr(34) & ";"

            ' Betalings ID
            'Type post
            '---------------
            If Not EmptyString(oInvoice.MATCH_ID) Then
                sLine = sLine + oInvoice.MATCH_ID.Trim & ";"
                '13.06.2017 - Added next IF to write A if gift
                If oInvoice.MyField.Trim = "M" Then
                    sLine = sLine & Chr(34) & "A" & Chr(34) & ";"
                Else
                    sLine = sLine & Chr(34) & "K" & Chr(34) & ";"
                End If
            Else
                sLine = sLine + "999999" & ";"
                sLine = sLine & Chr(34) & "A" & Chr(34) & ";"
            End If

            'New code 10.03.2006
            ' Amount - kroneamount
            '-------
            ' Amount - �reamount
            '--------------------
            sAmount = Trim$(Str(oInvoice.MON_InvoiceAmount))
            sLine = sLine & sAmount & ";"

            ' Betalingsm�te
            '---------------
            sLine = sLine & Chr(34) & "B" & Chr(34) & ";" 'Type post

            ' Dato
            '-----
            If dDate > (System.DateTime.FromOADate(Now.ToOADate - 1000)) Then
                ' hvis vi har lagret en dato, bruk denne
                sLine = sLine & Format(dDate, "dd.MM.yyyy") & ";"
            Else
                ' vi har ikke lagret dato, sett inn dagens dato
                sLine = sLine & Format(Now, "dd.MM.yyyy") & ";"
            End If

            'Archiveref
            '-------------
            sLine = sLine & Chr(34) & sArchiveRef.Trim & Chr(34) & ";"

            ' Fromaccount
            '--------------
            sLine = sLine & sFromAccount & ";"

            ' Bilagstekst
            '--------------
            ' Add first 80 of freetext
            sFreeTextVariable = Replace(Replace(sFreeTextVariable, Chr(124), "1"), Chr(34), vbNullString)
            sLine = sLine & sFreeTextVariable & ";"

            ' Mottakers bankkontonummer
            '--------------
            sLine = sLine & oPayment.I_Account & ";"

            ' BB_ID
            '--------------
            sLine = sLine & oPayment.Unique_PaymentID

            If bAbandon Then
                WriteMySoftCRMTransaction = vbNullString
            Else
                WriteMySoftCRMTransaction = sLine
            End If
        End If 'If oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then
        'Next oinvoice
    End Function

    Function WriteWinOrg(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        'Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim dDate As Date
        Dim sName As String
        Dim sAdr3, sAdr1, sAdr2, sZip As String
        Dim nAmount As Double
        Dim sREF_Bank1, sREF_Bank2 As String
        Dim sE_Account As String ' Payers account
        Dim sSequenceNo As String

        '
        ' create an outputfile
        Try
            bAppendFile = False

            sSequenceNo = Month(Now) & VB.Day(Now) & Hour(Now) & Minute(Now) & Second(Now)

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    ' new 17.10.02
                                    ' Export to WinOrg only payments with Invoice.Match_Matched = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices(1).MATCH_Matched. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oPayment.Invoices(1).MATCH_Matched Then

                                        '-------- fetch content of each payment ---------
                                        dDate = StringToDate((oPayment.DATE_Payment))
                                        sName = oPayment.E_Name
                                        sAdr1 = "" '""
                                        sAdr2 = "" '""
                                        sAdr3 = ""
                                        ' Addresspriority;
                                        ' 1) c/o address
                                        ' 2) Postboks
                                        ' 3) Street address
                                        ' Test for c/o:
                                        If InStr(UCase(oPayment.E_Adr1), "C/O") > 0 Then
                                            sAdr1 = oPayment.E_Adr1
                                            sAdr2 = oPayment.E_Adr2
                                            sAdr3 = oPayment.E_Adr3
                                        ElseIf InStr(UCase(oPayment.E_Adr2), "C/O") > 0 Then
                                            sAdr1 = oPayment.E_Adr2
                                            sAdr2 = oPayment.E_Adr1
                                            sAdr3 = oPayment.E_Adr3
                                        ElseIf InStr(UCase(oPayment.E_Adr3), "C/O") > 0 Then
                                            sAdr1 = oPayment.E_Adr3
                                            sAdr2 = oPayment.E_Adr1
                                            sAdr3 = oPayment.E_Adr2
                                            ' no c/o address, test for Postboks
                                        ElseIf InStr(UCase(oPayment.E_Adr1), "OSTBOKS") > 0 Then
                                            sAdr1 = oPayment.E_Adr1
                                            sAdr2 = oPayment.E_Adr2
                                            sAdr3 = oPayment.E_Adr3
                                        ElseIf InStr(UCase(oPayment.E_Adr2), "OSTBOKS") > 0 Then
                                            sAdr1 = oPayment.E_Adr2
                                            sAdr2 = oPayment.E_Adr1
                                            sAdr3 = oPayment.E_Adr3
                                        ElseIf InStr(UCase(oPayment.E_Adr3), "OSTBOKS") > 0 Then
                                            sAdr1 = oPayment.E_Adr3
                                            sAdr2 = oPayment.E_Adr1
                                            sAdr3 = oPayment.E_Adr2
                                        Else
                                            sAdr1 = oPayment.E_Adr1
                                            sAdr2 = oPayment.E_Adr2
                                            sAdr3 = oPayment.E_Adr3
                                        End If

                                        sZip = oPayment.E_Zip
                                        nAmount = oPayment.MON_TransferredAmount


                                        sE_Account = oPayment.E_Account
                                        sI_Account = oPayment.I_Account
                                        sREF_Bank1 = oPayment.REF_Bank1
                                        sREF_Bank2 = oPayment.REF_Bank2

                                        '-------------------------------------------------
                                        sLine = WriteWinOrgTransaction(sName, sAdr1, sAdr2, sAdr3, sZip, sE_Account, sI_Account, nAmount, dDate, sSequenceNo, sREF_Bank1, sREF_Bank2, oPayment)
                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine((sLine))
                                        End If
                                    End If

                                    'Only mark non-KID payments as exported,
                                    ' and also Gebyr /COM/648
                                    'If oPayment.PayCode <> "510" And _
                                    'oPayment.PayCode <> "518" And _
                                    'oPayment.PayCode <> "648" Then
                                    ' 29.05.04 Ogs� filtrer vekk Wrong KID
                                    ' 08.06.04 Ogs� filtrer vekk COM (648)
                                    ' Changed 08.06.04 - Set all as exported!
                                    oPayment.Exported = True
                                    'End If
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteWinOrg" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteWinOrg = True

    End Function
    Public Function WriteWinOrgTransaction(ByRef sName As String, ByRef sAdr1 As String, ByRef sAdr2 As String, ByRef sAdr3 As String, ByRef sZip As String, ByRef sE_Account As String, ByRef sI_Account As String, ByRef nAmount As Double, ByRef dDate As Date, ByRef sSequenceNo As String, ByRef sREF_Bank1 As String, ByRef sREF_Bank2 As String, ByRef oPayment As Payment) As String

        Dim sLine As String
        Dim aNames() As String
        Dim sASString As String 'Store AS, ASA, A.S, etc, and add later
        'Dim sJrString As String ' Check Jr. Sen. etc
        Dim i As Integer
        Dim sFirstName, sLastName As String
        'Dim sKampanje As String, sAktivitet As String
        Dim sAnonym, sMelding As String
        Dim sBirthNumber As String
        Dim oInvoice As Invoice

        ' Remove hyphens from name
        sName = Replace(sName, "'", "")

        ' Check on AS, A.S, ASA etc;
        ' Start of namestring:
        If Left(UCase(sName), 3) = "AS " Then
            sASString = "AS"
            sName = Replace(UCase(sName), "AS ", "")
        ElseIf Left(UCase(sName), 3) = "AL " Then
            sASString = "AL"
            sName = Replace(UCase(sName), "AL ", "")
        ElseIf Left(UCase(sName), 4) = "ASA " Then
            sASString = "ASA"
            sName = Replace(UCase(sName), "ASA ", "")
        ElseIf UCase(Left(UCase(sName), 4)) = "ANS " Then
            sASString = "ANS"
            sName = Replace(UCase(sName), "ANS ", "")
        ElseIf Left(UCase(sName), 4) = "A.S " Then
            sASString = "A.S"
            sName = Replace(UCase(sName), "A.S ", "")
        ElseIf Left(UCase(sName), 4) = "A.L " Then
            sASString = "A.L"
            sName = Replace(UCase(sName), "A.L ", "")
        ElseIf Left(UCase(sName), 4) = "A/S " Then
            sASString = "A/S"
            sName = Replace(UCase(sName), "A/S ", "")
        ElseIf Left(UCase(sName), 4) = "K/S " Then
            sASString = "K/S"
            sName = Replace(UCase(sName), "K/S ", "")
        ElseIf Left(UCase(sName), 4) = "A/L " Then
            sASString = "A/L"
            sName = Replace(UCase(sName), "A/L ", "")

            ' End of namestring
        ElseIf Right(UCase(sName), 3) = " AS" Then
            sASString = "AS"
            sName = Replace(UCase(sName), " AS", "")
        ElseIf Right(UCase(sName), 3) = " AL" Then
            sASString = "AL"
            sName = Replace(UCase(sName), " AL", "")
        ElseIf Right(UCase(sName), 4) = " ASA" Then
            sASString = "ASA"
            sName = Replace(UCase(sName), " ASA", "")
        ElseIf UCase(Right(UCase(sName), 4)) = " ANS" Then
            sASString = "ANS"
            sName = Replace(UCase(sName), " ANS", "")
        ElseIf Right(UCase(sName), 4) = " A.S" Then
            sASString = "A.S"
            sName = Replace(UCase(sName), " A.S", "")
        ElseIf Right(UCase(sName), 4) = " A/S" Then
            sASString = "A/S"
            sName = Replace(UCase(sName), " A/S", "")
        ElseIf Right(UCase(sName), 4) = " K/S" Then
            sASString = "K/S"
            sName = Replace(UCase(sName), " K/S", "")
        ElseIf Right(UCase(sName), 4) = " A.L" Then
            sASString = "A.L"
            sName = Replace(UCase(sName), " A.L", "")
        ElseIf Right(UCase(sName), 4) = " A/L" Then
            sASString = "A/L"
            sName = Replace(UCase(sName), " A/L", "")
        Else
            sASString = ""
        End If


        If Len(sASString) > 0 Then
            ' Keep name in same order, but must capitalize
            aNames = Split(Trim(sName))
            sName = ""
            ' Capitalize first letter in all parts of name, uncap all others;
            For i = 0 To UBound(aNames)
                aNames(i) = UCase(Left(aNames(i), 1)) & LCase(Mid(aNames(i), 2))
                ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                If InStr(aNames(i), "-") > 0 Then
                    aNames(i) = Left(aNames(i), InStr(aNames(i), "-")) & UCase(Mid(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "-") + 2))
                End If
                ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                If InStr(aNames(i), "/") > 0 Then
                    aNames(i) = Left(aNames(i), InStr(aNames(i), "/")) & UCase(Mid(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "/") + 2))
                End If

                sName = sName & aNames(i) & " "
            Next

            ' AS, ASA, etc., keep in same order
            ' Lastname only for company (?)
            sLastName = Left(PadRight(sName & " " & sASString, 40, " "), 40) '
        Else
            If InStr(sName, ",") > 0 Then
                ' If comma in name, keep name as is, except from comma
                sName = Replace(sName, ",", "")
                ' Keep name in same order, but must capitalize
                aNames = Split(Trim(sName))
                sName = ""
                ' Capitalize first letter in all parts of name, uncap all others;
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left(aNames(i), 1)) & LCase(Mid(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left(aNames(i), InStr(aNames(i), "-")) & UCase(Mid(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left(aNames(i), InStr(aNames(i), "/")) & UCase(Mid(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "/") + 2))
                    End If
                    ' Split into Lastname and firstname;
                    If i = 0 Then
                        ' Lastname is given first in this case, therefore;
                        sLastName = aNames(i)
                    Else
                        sFirstName = sFirstName & aNames(i) & " "
                    End If
                Next

            Else
                'Name must be split in lastname + space + firstname
                ' Strip name from . and ,
                sName = Replace(Replace(sName, ".", ""), ",", "")

                aNames = Split(Trim(sName))

                ' Capitalize first letter in all parts of name, uncap all others;
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left(aNames(i), 1)) & LCase(Mid(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left(aNames(i), InStr(aNames(i), "-")) & UCase(Mid(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left(aNames(i), InStr(aNames(i), "/")) & UCase(Mid(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                Next

                Select Case UBound(aNames)
                    Case -1
                        ' Empty name
                        sName = "Ukjent"
                        '        Case 0
                        ' Tomt i navnefeltet
                        '            sName = "Mangler navn"
                    Case 0
                        ' En streng i navnefeltet
                        sLastName = aNames(0)
                    Case 1
                        ' To strenger i navnefeltet, siste f�rst
                        sLastName = aNames(1)
                        sFirstName = aNames(0)
                    Case 2
                        ' Tre, siste f�rst!
                        sLastName = aNames(2)
                        sFirstName = aNames(0) & " " & aNames(1)
                    Case 3
                        ' Fire, siste f�rst!
                        sLastName = aNames(3)
                        sFirstName = aNames(0) & " " & aNames(1) & " " & aNames(2)
                    Case Else
                        ' Flere enn fire , siste f�rst!
                        sLastName = aNames(UBound(aNames))
                        sFirstName = aNames(0) & " " & aNames(1) & " " & aNames(2)
                End Select
            End If
        End If

        sLine = sSequenceNo & ";"

        If sName <> "Ukjent" Then
            ' Don't export records without names1
            sLine = sLine & Trim(sFirstName) & ";" & Trim(sLastName) & ";"
        Else
            sLine = sLine & "Mangler navn;;"
        End If

        ' Adr1
        '-----
        ' Remove commas and points from adr2;
        sAdr1 = Replace(Replace(sAdr1, ".", ""), ",", "")
        ' Capitalize first letter in each part;
        aNames = Split(Trim(sAdr1))
        ' Capitalize first letter in all parts of Adr2, uncap all others;
        sAdr1 = ""
        For i = 0 To UBound(aNames)
            aNames(i) = UCase(Left(aNames(i), 1)) & LCase(Mid(aNames(i), 2))
            ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
            If InStr(aNames(i), "-") > 0 Then
                aNames(i) = Left(aNames(i), InStr(aNames(i), "-")) & UCase(Mid(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "-") + 2))
            End If
            sAdr1 = sAdr1 & aNames(i) & " "
        Next

        sLine = sLine & sAdr1 & ";"


        ' Adr2
        '-----
        ' Remove commas and points from adr2;
        sAdr2 = Replace(Replace(sAdr2, ".", ""), ",", "")
        ' Capitalize first letter in each part;
        aNames = Split(Trim(sAdr2))
        ' Capitalize first letter in all parts of Adr2, uncap all others;
        sAdr2 = ""
        For i = 0 To UBound(aNames)
            aNames(i) = UCase(Left(aNames(i), 1)) & LCase(Mid(aNames(i), 2))
            ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
            If InStr(aNames(i), "-") > 0 Then
                aNames(i) = Left(aNames(i), InStr(aNames(i), "-")) & UCase(Mid(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "-") + 2))
            End If
            sAdr2 = sAdr2 & aNames(i) & " "
        Next
        sLine = sLine & sAdr2 & ";"

        ' Adr3
        '-----
        ' Remove commas and points from adr3;
        sAdr3 = Replace(Replace(sAdr3, ".", ""), ",", "")
        ' Capitalize first letter in each part;
        aNames = Split(Trim(sAdr3))
        ' Capitalize first letter in all parts of Adr3, uncap all others;
        sAdr3 = ""
        For i = 0 To UBound(aNames)
            aNames(i) = UCase(Left(aNames(i), 1)) & LCase(Mid(aNames(i), 2))
            ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
            If InStr(aNames(i), "-") > 0 Then
                aNames(i) = Left(aNames(i), InStr(aNames(i), "-")) & UCase(Mid(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid(aNames(i), InStr(aNames(i), "-") + 2))
            End If
            sAdr3 = sAdr3 & aNames(i) & " "
        Next
        sLine = sLine & sAdr3 & ";"

        ' Zip
        '----
        sLine = sLine & sZip & ";"

        ' Payers account
        '---------------
        sLine = sLine & sE_Account & ";"

        ' Birthnumber (f�dselsnummer)
        '----------------------------
        ' ExtractBirthNumber handles only invoices with MATCH_Matched = False
        ' In WinOrg actual inovoices have MATCH_Matched set to True
        ' Therefore ..., cheat with
        For Each oInvoice In oPayment.Invoices
            oInvoice.MATCH_Matched = False
        Next oInvoice
        sBirthNumber = ExtractBirthNumber(oPayment) ' Run through freetext to catch F�dselsnr
        sLine = sLine & sBirthNumber & ";"
        For Each oInvoice In oPayment.Invoices
            oInvoice.MATCH_Matched = True ' Reset to previous state
        Next oInvoice

        ' Date
        '-----
        If dDate > (System.Date.FromOADate(Now.ToOADate - 1000)) Then
            ' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(dDate, "yyyymmdd") & ";"
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "yyyymmdd") & ";"
        End If

        ' Amount
        '-------
        sLine = sLine & VB6.Format(nAmount / 100, "###0.00") & ";"

        ' Kampanje
        '---------
        'Not in use, commented by Kjell 30-12-2004
        'sKampanje = ExtractKampanje(oPayment.Invoices) ' Run through freetext to catch Kampanje
        'sLine = sLine & sKampanje & ";"
        sLine = sLine & ";"

        ' Aktivitet
        '----------
        'Not in use, commented by Kjell 30-12-2004
        'sAktivitet = ExtractAktivitet(oPayment.Invoices)  ' Run through freetext to catch Aktivitet
        'sLine = sLine & sAktivitet & ";"
        sLine = sLine & ";"

        ' Anonym
        '---------
        sAnonym = ExtractAnonymous((oPayment.Invoices)) ' Let etter anonym, nn, anonymous, etc
        sLine = sLine & sAnonym & ";"

        ' Notification
        '--------------
        ' Noen bruker ; i fritekst. Bytt til :
        sMelding = Replace(ExtractNotification((oPayment.Invoices)), ";", ":") ' Vis fullstending melding
        sLine = sLine & sMelding & ";"

        ' Receivers bankaccount
        '-----------------------
        sLine = sLine & sI_Account & ";"

        ' Blankettnr
        '-----------
        sLine = sLine & sREF_Bank2 & ";"

        ' Arkivreferanse
        '---------------
        sLine = sLine & sREF_Bank1

        WriteWinOrgTransaction = sLine
        'Else
        'WriteWinOrgTransaction = ""
        'End If

    End Function
    'Private Function ExtractKampanje(oInvoices As Invoices) As String
    ''Not in use, commented by Kjell 30-12-2004
    'Dim sKampanje As String
    '
    'ExtractKampanje = sKampanje
    'End Function
    'Private Function ExtractAktivitet(oInvoices As Invoices) As String
    ''Not in use, commented by Kjell 30-12-2004
    'Dim sAktivitet As String
    '
    'ExtractAktivitet = sAktivitet
    'End Function
    Private Function ExtractAnonymous(ByRef oInvoices As Invoices) As String
        Dim bAnonymous As Boolean
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        For Each oInvoice In oInvoices
            For Each oFreeText In oInvoice.Freetexts
                If InStr(UCase(oFreeText.Text), "ANONYM") > 0 Then
                    bAnonymous = True
                    Exit For
                End If
            Next oFreeText
            If bAnonymous Then
                Exit For
            End If
        Next oInvoice

        If bAnonymous Then
            ExtractAnonymous = "J"
        End If
    End Function
    Private Function ExtractNotification(ByRef oInvoices As Invoices) As String
        Dim sReturn As String
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        For Each oInvoice In oInvoices
            For Each oFreeText In oInvoice.Freetexts
                sReturn = sReturn & oFreeText.Text
            Next oFreeText
        Next oInvoice

        ExtractNotification = Trim(sReturn)
    End Function

    Function WriteSRI_AccessReturn(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        'Dim nExistingLines As Double, sTempFilename As String, iCount As Integer
        'Dim bFoundRecord3 As Boolean, oFileWrite As TextStream, oFileRead As TextStream
        Dim bAppendFile As Boolean ', dDate As Date, sIAccountNo As String
        Dim sTelebankRef, sValDate, sAccountNo As String
        '
        ' create an outputfile
        Try
            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    '-------- fetch content of each payment ---------
                                    'UPGRADE_WARNING: Str has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                                    sValDate = Left(Str(StringToDate((oPayment.DATE_Payment)).ToOADate) & Space(4), 10)
                                    'sValDate = Left$(Str(oPayment.DATE_Payment) + Space(4), 10)
                                    sTelebankRef = oPayment.REF_Bank1
                                    sAccountNo = oPayment.E_Account
                                    '-------------------------------------------------
                                    ' Write one line, fixed columns, for each record
                                    sLine = sValDate & "  " & sTelebankRef & "  " & sAccountNo ' & "  " & oPayment.MON_TransferredAmount

                                    If Len(sLine) > 0 Then
                                        oFile.WriteLine((sLine))
                                    End If
                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSRI_AccessReturn" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSRI_AccessReturn = True

    End Function

    Function StandardFileWrite() As Object

        'Option Explicit
        '
        'Dim oFs As New FileSystemObject
        'Dim oFile As TextStream
        'Dim sLine As String ' en output-linje
        '
        'Function WriteFormatFile(oBabelFiles As BabelFiles, bMultiFiles As Boolean, sFilenameOut As String, iFormat_ID As Integer, sI_Account As String, sOwnRef As String, iFilenameInNo As Integer) As Boolean
        '
        'Dim oBabel As BabelFile
        'Dim oBatch As Batch
        'Dim oPayment As Payment
        '
        'Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        'Dim bFileFromBank As Boolean
        'Dim sNewOwnref As String
        '
        'On Error GoTo errCreateOutputFile
        '
        'Set oFile = oFs.OpenTextFile(sFilenameOut, ForAppending, True, 0)
        '
        'For Each oBabel In oBabelFiles
        '    If oBabel.VB_FilenameInNo = iFilenameInNo Then
        '
        '        bExportoBabel = False
        '        'Have to go through each batch-object to see if we have objects that shall
        '        ' be exported to this exportfile
        '        For Each oBatch In oBabel.Batches
        '            For Each oPayment In oBatch.Payments
        '                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
        '                    If Not bMultiFiles Then
        '                       bExportoBabel = True
        '                    Else
        '                        If oPayment.I_Account = sI_Account Then
        '                            If InStr(oPayment.REF_Own, "&?") Then
        '                                'Set in the part of the OwnRef that BabelBank is using
        '                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
        '                            Else
        '                                sNewOwnref = ""
        '                            End If
        '                            If sNewOwnref = sOwnRef Then
        '                                bExportoBabel = True
        '                            End If
        '                        End If
        '                    End If
        '                End If
        '                If bExportoBabel Then
        '                    Exit For
        '                End If
        '            Next oPayment
        '            If bExportoBabel Then
        '                Exit For
        '            End If
        '        Next oBatch
        '
        '        If bExportoBabel Then
        '            'Set if the file was created int the accounting system or not.
        '            bFileFromBank = oBabel.FileFromBank
        '            'i = 0 'FIXED Moved by Janp under case 2
        '
        '
        '            'Loop through all Batch objs. in BabelFile obj.
        '            For Each oBatch In oBabel.Batches
        '                bExportoBatch = False
        '                'Have to go through the payment-object to see if we have objects thatt shall
        '                ' be exported to this exportfile
        '                For Each oPayment In oBatch.Payments
        '                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
        '                        If Not bMultiFiles Then
        '                            bExportoBatch = True
        '                        Else
        '                            If oPayment.I_Account = sI_Account Then
        '                                If InStr(oPayment.REF_Own, "&?") Then
        '                                    'Set in the part of the OwnRef that BabelBank is using
        '                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
        '                                Else
        '                                    sNewOwnref = ""
        '                                End If
        '                                If sNewOwnref = sOwnRef Then
        '                                    bExportoBatch = True
        '                                End If
        '                            End If
        '                        End If
        '                    End If
        '                    'If true exit the loop, and we have data to export
        '                    If bExportoBatch Then
        '                        Exit For
        '                    End If
        '                Next
        '
        '
        '                If bExportoBatch Then
        '
        ''                    i = i + 1
        ''
        ''                    'Get the companyno either from a property passed from BabelExport, or
        ''                    ' from the imported file
        ''                    If sCompanyNo <> "" Then
        ''                        sI_EnterpriseNo = sCompanyNo
        ''                    Else
        ''                        sI_EnterpriseNo = oBatch.I_EnterpriseNo
        ''                    End If
        ''                    'BETFOR00
        ''                    If bWriteRecord10 Then
        ''                        sLine = WriteRecord10(sI_Account, bEuroAccount, oBatch.Payments(1).ERA_ExchRateAgreed, oBatch.Payments(1).ERA_DealMadeWith, oBatch.Payments(1).MON_TransferCurrency, sFilenameOut)
        ''                        oFile.WriteLine (sLine)
        ''                        bWriteRecord10 = False
        ''                        lNumberofRecords = lNumberofRecords + 1
        ''                        'New record10 for this vriables
        ''                        sLastAgreedRate = oBatch.Payments(1).ERA_ExchRateAgreed
        ''                        sLastAccount = sI_Account
        ''                        sLastCurrencyCode = oBatch.Payments(1).MON_TransferCurrency
        ''                    End If
        '
        '                    For Each oPayment In oBatch.Payments
        '
        '
        '                        'Have to go through the payment-object to see if we have objects thatt shall
        '                        ' be exported to this exportfile
        '                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
        '                            If Not bMultiFiles Then
        '                                bExportoPayment = True
        '                            Else
        '                                If oPayment.I_Account = sI_Account Then
        '                                    If InStr(oPayment.REF_Own, "&?") Then
        '                                        'Set in the part of the OwnRef that BabelBank is using
        '                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
        '                                    Else
        '                                        sNewOwnref = ""
        '                                    End If
        '                                    If sNewOwnref = sOwnRef Then
        '                                        bExportoPayment = True
        '                                    End If
        '                                End If
        '                            End If
        '                        End If
        '
        '                        If bExportoPayment Then
        '
        '
        '
        '                            oPayment.Exported = True
        '
        '                        End If 'bExportoPayment
        '
        '                    Next oPayment
        '
        '                End If 'bExportoBatch
        '
        '            Next oBatch
        '
        '        End If 'bExportoBabel
        '
        '    End If 'oBabel.VB_FilenameInNo = iFilenameInNo
        '
        'Next oBabel
        '
        'If nFileNoRecords > 0 Then
        '    oFile.Close
        '    Set oFile = Nothing
        'Else
        '    oFile.Close
        '    Set oFile = Nothing
        '    oFs.DeleteFile (sFilenameOut)
        'End If
        '
        'Set oFs = Nothing
        '
        'WriteFormatFile = True
        'Exit Function
        '
        'errCreateOutputFile:
        '    oFile.Close
        '    Set oFile = Nothing
        '    Set oFs = Nothing
        '
        '    WriteFormatFile = False
        '
        '
        'End Function


    End Function
    Function WriteQXLOCR(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean


        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    '-------- Write content of each payment to file---------
                                    If oPayment.Invoices.Count > 0 Then
                                        ' Kr�kkete m�te � sikre at vi alltid f�r bel�p som desimalpunktum, og to sifre tilsutt

                                        'New code 05.10.2006, new KID length 10
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If Len(Trim(oPayment.Invoices(1).Unique_Id)) = 8 Then '7 first is customerno
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oFile.WriteLine((Left(Trim(oPayment.Invoices(1).Unique_Id), 7) & "," & Trim(Str(Int(oPayment.MON_TransferredAmount / 100)) & "." & Right(Str(oPayment.MON_TransferredAmount), 2))))
                                        Else '9 first is customerno
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oFile.WriteLine((Left(Trim(oPayment.Invoices(1).Unique_Id), 9) & "," & Trim(Str(Int(oPayment.MON_TransferredAmount / 100)) & "." & Right(Str(oPayment.MON_TransferredAmount), 2))))
                                        End If
                                    Else
                                        ' Error, no invoice. Should not happen
                                        oFile.WriteLine(("0000000" & "," & Trim(Str(Int(oPayment.MON_TransferredAmount / 100)) & "." & Right(Str(oPayment.MON_TransferredAmount), 2))))
                                    End If
                                    oPayment.Exported = True
                                Else
                                    oPayment.Exported = False
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteQXLOCR" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteQXLOCR = True

    End Function


    Function WritePGSReturn(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim oTempBatch As Batch
        Dim oTempPayment As Payment
        Dim i As Short

        Try

            oFs = New Scripting.FileSystemObject

            ' New 26.01.05
            ' Must first add all BabelFile into one BabelFile
            If oBabelFiles.Count > 1 Then
                oBabel = oBabelFiles(1)
                ' Add from rest of babelfiles;
                For i = 2 To oBabelFiles.Count
                    For Each oBatch In oBabelFiles(i).Batches
                        oTempBatch = oBatch
                        'oBatch = oBabel.Batches.Add
                        'Set oBatch = oTempbatch
                        oBatch = oBabel.Batches.VB_AddWithObject(oTempBatch)
                    Next oBatch
                Next i
            End If
            ' Then remove all but first BabelFile
            For i = oBabelFiles.Count To 2 Step -1
                oBabelFiles.Remove(i)
            Next i
            ' Then add all payments into one batch!!!!
            If oBabelFiles(1).Batches.Count > 1 Then
                oBabel = oBabelFiles(1)
                oBatch = oBabel.Batches(1)
                ' Add from rest of batches;
                For i = 2 To oBabel.Batches.Count
                    'UPGRADE_WARNING: Couldn't resolve default property of object oBabel.Batches(i).Payments. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For Each oPayment In oBabel.Batches(i).Payments
                        oTempPayment = oPayment
                        oPayment = oBatch.Payments.VB_AddWithObject(oTempPayment)
                    Next oPayment
                Next i
            End If
            ' Then remove all but first Batch
            For i = oBabelFiles(1).Batches.Count To 2 Step -1
                oBabelFiles(1).Batches.Remove(i)
            Next i


            ' New 24.01.05
            ' New version of Peoplesoft must have payments sorted by accountno
            For Each oBabel In oBabelFiles
                oBabel.SortCriteria1 = "VB_I_Account"
                oBabel.SortCriteria2 = "DATE_Payment"
                oBabel.SortCriteria3 = "MON_INVOICECURRENCY"
                oBabel.SortPayment()
            Next oBabel


            bAppendFile = False


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            ' Fileheader;
            ' -----------
            sLine = ""
            sLine = sLine & "000" ' ECFILEROWID
            sLine = sLine & "02 " ' ECACTIONID
            ' SWIFTaddress set in Profile, Special
            sLine = sLine & PadRight(Trim(oBabelFiles(1).Special), 20, " ") ' BNK_ID_NBR BankID for DNBNOR
            'sLine = sLine & PadRight("DNBANOKK", 20, " ") ' BNK_ID_NBR BankID for DNBNOR
            'sLine = sLine & PadRight("NDEANOKK", 20, " ") ' BNK_ID_NBR BankID for Nordea
            sLine = sLine & Trim(Str(Year(Now))) & "/" & VB6.Format(Month(Now), "0#") & "/" & VB6.Format(VB.Day(Now), "0#") ' ASOFDATE Fildato
            sLine = sLine & PadRight(VB6.Format(Now, "HH:MM:SS") & ":00", 15, " ") ' ASOFTIME Filklokkeslett
            sLine = sLine & PadRight("PGS", 20, " ") ' RECEIVER
            sLine = sLine & Space(24)
            oFile.WriteLine((sLine))

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    '
                                    ' New 11.03.05
                                    ' Export ONLY status = 02
                                    ' Had a problem with rejected payments pre this test!!!
                                    If oPayment.StatusCode = "02" Then

                                        '-------- fetch content of each payment ---------
                                        sLine = ""
                                        sLine = sLine & "002" ' ECFILEROWID
                                        sLine = sLine & "02 " ' ECACTIONID
                                        sLine = sLine & PadRight(Trim(oBabel.Special), 20, " ") ' BNK_ID_NBR BankID for DNBNOR
                                        'sLine = sLine & PadRight("DNBANOKK", 20, " ") ' BNK_ID_NBR BankID for DNBNOR
                                        'sLine = sLine & PadRight("NDEANOKK", 20, " ") ' BNK_ID_NBR BankID for Nordea
                                        'sLine = sLine & PadRight(oPayment.I_Account, 17, " ") 'BANK_ACCOUNT_NUM Belastningskonto
                                        ' XokNET 16.08.2011 -
                                        ' Changed from 17 to 20 for accountlength
                                        ' (- and rest of fields shifted three positions, but no codechange required for that)
                                        sLine = sLine & PadRight(oPayment.I_Account, 20, " ") 'BANK_ACCOUNT_NUM Belastningskonto
                                        sLine = sLine & PadRight("EFT", 20, " ") ' BANK_TRANS_CODE

                                        ' 21.01.05 Changed from Invoiceamount to AccountAmount
                                        If oPayment.MON_AccountAmount <> 0 Then
                                            sLine = sLine & Replace(PadLeft(VB6.Format(oPayment.MON_AccountAmount / 100, "#############0.00"), 17, " "), ",", ".") 'RECON_TRAN_AMT Amount with decimalpoint
                                        Else
                                            sLine = sLine & Replace(PadLeft(VB6.Format(oPayment.MON_InvoiceAmount / 100, "#############0.00"), 17, " "), ",", ".") 'RECON_TRAN_AMT Amount with decimalpoint
                                        End If

                                        If oPayment.DATE_Value <> "19900101" Then
                                            sLine = sLine & Left(oPayment.DATE_Value, 4) & "/" & Mid(oPayment.DATE_Value, 5, 2) & "/" & Right(oPayment.DATE_Value, 2) 'RECON_BANK_DT Valuedate
                                        Else
                                            sLine = sLine & Left(oPayment.DATE_Payment, 4) & "/" & Mid(oPayment.DATE_Payment, 5, 2) & "/" & Right(oPayment.DATE_Payment, 2) 'RECON_BANK_DT Valuedate
                                        End If
                                        sLine = sLine & PadRight(oPayment.REF_Own, 10, " ") ' RECON_REF_ID Paymentref
                                        sLine = sLine & Space(60)

                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine((sLine))
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If

                                    oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WritePGSReturn" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WritePGSReturn = True

    End Function

    Function WriteBellinTreasury(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short) As Boolean
        ' Treasuryfile for import into Bellins TRDB RAXCM
        ' Used for Europark. Importfile is a DnBNOR Statementfile

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer
        Dim sTmp As String
        Dim sTmpDate As String

        Try

            lCounter = 0
            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        ' 01.02.05 Added paycode="510"
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    End If
                                End If

                                If bExportoPayment Then


                                    lCounter = lCounter + 1
                                    '-------- Write content of each payment to file---------
                                    ' Cheating - So far Only Europark uses this format,
                                    ' for info from DnBNOR Norway
                                    sTmp = "DNBANOKK"
                                    '1) Account id
                                    sLine = sTmp & "/" & oPayment.I_Account & ";" ' Swift+Account
                                    '2) Account statementno
                                    'sLine = sLine & oPayment.REF_Bank2 & ";"
                                    ' Due to problems in DnBNOR we have to simulate a statementNo
                                    ' use MMDD from oBatch.DATE_Production
                                    sLine = sLine & Mid(oBatch.DATE_Production, 5, 4) & "/01;"

                                    sTmpDate = oBatch.DATE_Production '- 1
                                    ' mm/dd/yyyy - Startdate, always day before batch productiondate
                                    '3) Start date of statement
                                    sLine = sLine & Mid(sTmpDate, 5, 2) & "/" & Mid(sTmpDate, 7, 2) & "/" & Left(sTmpDate, 4) & ";"
                                    '4) Starting balance
                                    sLine = sLine & CDbl(oBatch.MON_BalanceIN / 100) & ";"
                                    sTmpDate = oBatch.DATE_Production
                                    ' mm/dd/yyyy - Enddate
                                    '5) Enddate of statement
                                    sLine = sLine & Mid(sTmpDate, 5, 2) & "/" & Mid(sTmpDate, 7, 2) & "/" & Left(sTmpDate, 4) & ";"
                                    '6) Closing balance
                                    sLine = sLine & CDbl(oBatch.MON_BalanceOUT / 100) & ";"
                                    sTmpDate = oPayment.DATE_Value
                                    '7) Valuedate
                                    sLine = sLine & Mid(sTmpDate, 5, 2) & "/" & Mid(sTmpDate, 7, 2) & "/" & Left(sTmpDate, 4) & ";"
                                    sTmpDate = oPayment.DATE_Payment
                                    '8) Booking date
                                    sLine = sLine & Mid(sTmpDate, 5, 2) & "/" & Mid(sTmpDate, 7, 2) & "/" & Left(sTmpDate, 4) & ";"
                                    '9) Currency
                                    sLine = sLine & oPayment.MON_AccountCurrency & ";"
                                    '10) Amount
                                    sLine = sLine & CDbl(oPayment.MON_AccountAmount / 100) & ";"
                                    '11) SWIFTnumber, not present
                                    sLine = sLine & ";"
                                    '12) Natinal Bankcode, not present
                                    sLine = sLine & ";"
                                    '13) BC - not present
                                    sLine = sLine & ";"
                                    '14) BC supplemental - not present
                                    sLine = sLine & ";"

                                    sTmp = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sTmp = sTmp & Trim(oFreeText.Text) & " "
                                        Next oFreeText
                                    Next oInvoice
                                    '15) Comments
                                    sLine = sLine & Left(sTmp, 255) & ";"
                                    '16) Payment reference - not present
                                    sLine = sLine & ";"
                                    '17) Reference - not present
                                    sLine = sLine & ";"
                                    '18) Booking key - not present
                                    sLine = sLine & ";"
                                    '19) Reference - not present
                                    sLine = sLine & ";"
                                    '20) Bank reference
                                    sLine = sLine & Left(oPayment.REF_Bank1, 16) & ";"
                                    '21-27) Not present
                                    sLine = sLine & ";;;;;;"


                                    oFile.WriteLine(sLine)
                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBellinTreasury" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBellinTreasury = True

    End Function


    Function WriteBatchInfo(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        ' 24.10.05
        ' Used for tests to write the content of the collections
        '
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sTmpDate As String
        Dim sREF As String
        Dim lCounter As Integer
        Dim sFreetext, sFreeTextVariable As String
        Dim iFreetextCounter As Short

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                For Each oBatch In oBabel.Batches

                    For Each oPayment In oBatch.Payments
                        If Not IsOCR((oPayment.PayCode)) Then

                            sTmpDate = oPayment.DATE_Payment
                            sLine = oPayment.I_Account

                            sLine = sLine & Mid(sTmpDate, 7, 2) & "." & Mid(sTmpDate, 5, 2) & "." & Left(sTmpDate, 4)
                            sLine = sLine & PadLeft(CStr(oPayment.MON_InvoiceAmount), 15, " ")
                            If oBatch.Payments.Count = 1 And Not EmptyString((oPayment.REF_Bank1)) Then
                                sREF = oPayment.REF_Bank1
                                For lCounter = 1 To Len(sREF)
                                    If Left(sREF, 1) = "*" Then
                                        sREF = Mid(sREF, 2)
                                    Else
                                        Exit For
                                    End If
                                Next lCounter
                                sLine = sLine & PadLeft(sREF, 15, " ")
                            Else
                                sREF = oBatch.REF_Bank
                                For lCounter = 1 To Len(sREF)
                                    If CDbl(Left(sREF, 1)) = 0 Then
                                        sREF = Mid(sREF, 2)
                                    Else
                                        Exit For
                                    End If
                                Next lCounter
                                sLine = sLine & PadLeft(sREF, 15, " ")
                            End If

                            sLine = sLine & "Nyref:" & Trim(oBatch.REF_Bank)
                            sLine = sLine & PadLeft(oPayment.E_Name, 35, " ")
                            sFreetext = ""
                            iFreetextCounter = 0
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Original = True Then
                                    For Each oFreeText In oInvoice.Freetexts
                                        If oFreeText.Qualifier < 3 Then
                                            iFreetextCounter = iFreetextCounter + 1
                                            If iFreetextCounter = 1 Then
                                                sFreetext = RTrim(oFreeText.Text)
                                            ElseIf iFreetextCounter = 2 Then
                                                sFreetext = sFreetext & " " & RTrim(oFreeText.Text)
                                            Else
                                                Exit For
                                            End If
                                        End If
                                    Next oFreeText
                                End If
                            Next oInvoice
                            sFreetext = sFreetext & oPayment.E_Name

                            oFile.WriteLine(sLine)
                        End If
                        '        For Each oInvoice In oPayment.Invoices
                        '            'If oInvoice.MATCH_Final Then
                        '
                        '                sFreeTextVariable = sFreetext
                        '                For Each oFreeText In oInvoice.Freetexts
                        '                    If oFreeText.Qualifier > 3 Then
                        '                        sFreeTextVariable = RTrim$(oFreeText.Text) & " " & sFreeTextVariable
                        '                    End If
                        '                Next oFreeText
                        '                oFile.WriteLine sLine & sFreeTextVariable
                        '            'End If
                        '        Next oInvoice


                        '        For Each oInvoice In oPayment.Invoices
                        '            For Each oFreeText In oInvoice.Freetexts
                        '                If oFreeText.Qualifier <> 3 Then
                        '                    sFreetext = sFreetext & " " & Trim$(oFreeText.Text)
                        '                End If
                        '            Next
                        '        Next

                        'sLine = sLine & sFreetextVariable

                        'sLine = sLine & PadLeft$(oBatch.MON_InvoiceAmount, 15, " ")
                        'sLine = sLine & PadLeft$(oBatch.REF_Bank, 15, " ")
                        'sLine = sLine & PadLeft$(oBatch.Payments.Item(1).REF_Bank1, 15, " ")
                        'sLine = sLine & PadLeft$(oBatch.Payments.Item(1).REF_Bank2, 15, " ")

                        'oFile.WriteLine sLine
                        'For Each oPayment In oBatch.Payments
                        'oFile.WriteLine "###" & oPayment.E_Name
                        'oPayment.Exported = True
                        'For Each oInvoice In oPayment.Invoices
                        '    For Each oFreeText In oInvoice.Freetexts
                        '        oFile.WriteLine oFreeText.Text
                        '    Next oFreeText
                        'Next oInvoice
                        'Next

                    Next oPayment
                Next oBatch
            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBatchInfo" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

    End Function

    Function WriteItAll(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        ' 24.10.05
        ' Used for tests to write the content of the collections
        '
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sTmpDate As String
        Dim sFreetext As String
        Dim sBabelInfo, sBatchInfo As String
        Dim sPaymentInfo, sInvoiceInfo As String

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                sBabelInfo = "BABELFILE: "
                sBabelInfo = sBabelInfo & oBabel.FilenameIn & ", "
                sBabelInfo = sBabelInfo & oBabel.File_id & ", "
                sBabelInfo = sBabelInfo & oBabel.IDENT_Sender & ", "
                sBabelInfo = sBabelInfo & oBabel.IDENT_Receiver & ", "
                sBabelInfo = sBabelInfo & oBabel.MON_TransferredAmount & ", "
                sBabelInfo = sBabelInfo & oBabel.MON_InvoiceAmount & ", "
                sBabelInfo = sBabelInfo & oBabel.No_Of_Transactions & ", "
                sBabelInfo = sBabelInfo & oBabel.No_Of_Records & ", "
                sBabelInfo = sBabelInfo & oBabel.DATE_Production & ", "
                sBabelInfo = sBabelInfo & oBabel.ImportFormat & ", "
                sBabelInfo = sBabelInfo & oBabel.RejectsExists & ", "
                sBabelInfo = sBabelInfo & oBabel.VB_FilenameInNo & ", "
                sBabelInfo = sBabelInfo & oBabel.FileFromBank & ", "
                sBabelInfo = sBabelInfo & oBabel.Version & ", "
                sBabelInfo = sBabelInfo & oBabel.EDI_Format & ", "
                sBabelInfo = sBabelInfo & oBabel.EDI_MessageNo & ", "
                sBabelInfo = sBabelInfo & oBabel.VB_FileSetupID & ", "
                sBabelInfo = sBabelInfo & oBabel.Index & ", "
                'sBabelInfo = sBabelInfo & oBabel.Language & ", "
                'sBabelInfo = sBabelInfo & oBabel.ERPSystem & ", "
                sBabelInfo = sBabelInfo & oBabel.BankByCode & ", "
                sBabelInfo = sBabelInfo & oBabel.StatusCode & ", "
                sBabelInfo = sBabelInfo & oBabel.Special & ", "
                oFile.WriteLine(sBabelInfo)

                For Each oBatch In oBabel.Batches
                    sBatchInfo = "BATCH: "
                    sBatchInfo = sBatchInfo & oBatch.Batch_ID & ", "
                    sBatchInfo = sBatchInfo & oBatch.SequenceNoStart & ", "
                    sBatchInfo = sBatchInfo & oBatch.SequenceNoEnd & ", "
                    sBatchInfo = sBatchInfo & oBatch.StatusCode & ", "
                    sBatchInfo = sBatchInfo & oBatch.Version & ", "
                    sBatchInfo = sBatchInfo & oBatch.OperatorID & ", "
                    sBatchInfo = sBatchInfo & oBatch.FormatType & ", "
                    sBatchInfo = sBatchInfo & oBatch.I_EnterpriseNo & ", "
                    sBatchInfo = sBatchInfo & oBatch.I_Branch & ", "
                    sBatchInfo = sBatchInfo & oBatch.DATE_Production & ", "
                    sBatchInfo = sBatchInfo & oBatch.REF_Own & ", "
                    sBatchInfo = sBatchInfo & oBatch.REF_Bank & ", "
                    sBatchInfo = sBatchInfo & oBatch.MON_TransferredAmount & ", "
                    sBatchInfo = sBatchInfo & oBatch.MON_InvoiceAmount & ", "
                    sBatchInfo = sBatchInfo & oBatch.AmountSetTransferred & ", "
                    sBatchInfo = sBatchInfo & oBatch.AmountSetInvoice & ", "
                    sBatchInfo = sBatchInfo & oBatch.No_Of_Transactions & ", "
                    sBatchInfo = sBatchInfo & oBatch.No_Of_Records & ", "
                    sBatchInfo = sBatchInfo & oBatch.ImportFormat & ", "
                    oFile.WriteLine(sBatchInfo)

                    For Each oPayment In oBatch.Payments

                        sPaymentInfo = "PAYMENT: "

                        sPaymentInfo = sPaymentInfo & oPayment.Payment_ID & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.StatusCode & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Cancel & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.DATE_Payment & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_TransferredAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.AmountSetInvoice & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.AmountSetTransferred & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_TransferCurrency & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_OriginallyPaidAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_OriginallyPaidCurrency & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.REF_Own & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.REF_Bank1 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.REF_Bank2 & ", "
                        'sPaymentInfo = sPaymentInfo & oPayment.FormatType & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_Account & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_AccountPrefix & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_AccountSuffix & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_Name & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_Adr1 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_Adr2 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_Adr3 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_Zip & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_City & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.E_CountryCode & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.DATE_Value & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Priority & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Client & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Text_E_Statement & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Text_I_Statement & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Cheque & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ToOwnAccount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.PayCode & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.PayType & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Account & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Name & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Adr1 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Adr2 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Adr3 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_Zip & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_City & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.I_CountryCode & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Structured & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.VB_ClientNo & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraD1 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraD2 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraD3 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraD4 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraD5 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_LocalAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_LocalCurrency & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_AccountAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_AccountCurrency & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_InvoiceAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_InvoiceCurrency & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_EuroAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_AccountExchRate & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_LocalExchRate & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_LocalExchRateSet & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_EuroExchRate & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_ChargesAmount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_ChargesCurrency & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_ChargeMeDomestic & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MON_ChargeMeAbroad & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ERA_ExchRateAgreed & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ERA_DealMadeWith & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.FRW_ForwardContractRate & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.FRW_ForwardContractNo & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.NOTI_NotificationMessageToBank & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.NOTI_NotificationType & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.NOTI_NotificationParty & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.NOTI_NotificationAttention & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.NOTI_NotificationIdent & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_SWIFTCode & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_BranchNo & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_Name & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_Adr1 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_Adr2 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_Adr3 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_CountryCode & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.BANK_SWIFTCodeCorrBank & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraI1 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraI2 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ExtraI3 & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.ImportFormat & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.VB_FilenameOut_ID & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.StatusSplittedFreetext & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Exported & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.VoucherNo & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.VoucherType & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MATCH_Matched & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Unique_PaymentID & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.Unique_ERPID & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.PaymentCounter & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MATCH_MatchingIDAgainstObservationAccount & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.UserID & ", "
                        sPaymentInfo = sPaymentInfo & oPayment.MATCH_HowMatched & ", "
                        oFile.WriteLine(sPaymentInfo)

                        For Each oInvoice In oPayment.Invoices
                            sFreetext = "FREETEXT: "
                            For Each oFreeText In oInvoice.Freetexts
                                sFreetext = sFreetext & oFreeText.Text
                            Next oFreeText

                            sInvoiceInfo = "INVOICE:"
                            sInvoiceInfo = sInvoiceInfo & oInvoice.Invoice_ID & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.StatusCode & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.Cancel & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MON_InvoiceAmount & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MON_TransferredAmount & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MON_AccountAmount & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MON_LocalAmount & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.AmountSetTransferred & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.AmountSetInvoice & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.REF_Own & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.REF_Bank & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.InvoiceNo & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.CustomerNo & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.Unique_Id & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.STATEBANK_Code & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.STATEBANK_Text & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.STATEBANK_DATE & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.Extra1 & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MATCH_Original & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MATCH_Final & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MATCH_Matched & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MyField & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MATCH_MatchType & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MATCH_ID & ", "
                            sInvoiceInfo = sInvoiceInfo & oInvoice.MATCH_PartlyPaid & ", "
                            oFile.WriteLine(sInvoiceInfo)

                            oFile.WriteLine(sFreetext)
                            '        sTmpDate = oBatch.Payments.Item(1).DATE_Value
                            '        sLine = Mid$(sTmpDate, 5, 2) & "." & Mid$(sTmpDate, 7, 2) & "." & Left$(sTmpDate, 4)
                            '        sLine = sLine & PadLeft$(oBatch.MON_InvoiceAmount, 15, " ")
                            '        sLine = sLine & PadLeft$(oBatch.REF_Bank, 15, " ")
                            '        sLine = sLine & PadLeft$(oBatch.Payments.Item(1).REF_Bank1, 15, " ")
                            '        sLine = sLine & PadLeft$(oBatch.Payments.Item(1).REF_Bank2, 15, " ")
                            '
                            '        oFile.WriteLine sLine
                            '        For Each oPayment In oBatch.Payments
                            '            oFile.WriteLine "###" & oPayment.E_Name
                            '            oPayment.Exported = True
                            '            For Each oInvoice In oPayment.Invoices
                            '                For Each oFreeText In oInvoice.Freetexts
                            '                    oFile.WriteLine oFreeText.Text
                            '                Next oFreeText
                        Next oInvoice
                    Next oPayment

                Next oBatch
            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteItAll" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

    End Function
    Function WriteOneTimeFormat(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef iFormat As BabelFiles.FileType) As Boolean
        '-------------------------------------------------------------------------
        ' Generic format covering several exportformats for BabelBank Innbetaling
        ' 15.05.06: Navision for KonicaMinolta
        '       Dato; Bilagstype; Kontotype; Konto; Tekst ; Bel�p; Motpost; Motpostype
        '-------------------------------------------------------------------------

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim iFreetextCounter As Short
        Dim sFreetextFixed, sFreeTextVariable As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sLineToWrite As String
        Dim bFirstBatch As Boolean
        Dim nSequenceNoTotal As Double
        Dim bGLRecordWritten As Boolean
        Dim sVoucherNo As String
        Dim sAlphaPart As String
        Dim nDigitsLen As Short
        Dim sGLAccount As String
        Dim sOldAccountNo, sTempClientNo As String
        Dim bAccountFound As Boolean

        Try

            ' lag en outputfil
            bAppendFile = False
            bGLRecordWritten = False
            bFirstBatch = True
            sOldAccountNo = ""
            sTempClientNo = ""
            sOldAccountNo = ""

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'Check for omitted payments
                        If oPayment.StatusCode <> "-1" Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                'If oPayment.I_Account = sI_Account Then� 01.10.2018 � changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'Check for omitted payments
                            If oPayment.StatusCode <> "-1" Then
                                'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            bGLRecordWritten = False
                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'Check for omitted payments
                                If oPayment.StatusCode <> "-1" Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If
                                If bExportoPayment Then

                                    'Intrum Justitia AS
                                    'navn;postboks:gateadresse;postnummer;poststed;bel�p
                                    'TUSSA 24 AS;;LANGEMYRA 6;6150;�RSTA;150,00

                                    sLineToWrite = ""
                                    If EmptyString((oPayment.E_Name)) Then
                                        sLineToWrite = sLineToWrite
                                    End If
                                    sLineToWrite = Trim(oPayment.E_Name) & ";"
                                    sLineToWrite = sLineToWrite & Trim(oPayment.E_Adr2) & ";" 'Postboks
                                    sLineToWrite = sLineToWrite & Trim(oPayment.E_Adr1) & ";" 'Gateadresse
                                    If EmptyString((oPayment.E_Zip)) Then
                                        sLineToWrite = sLineToWrite
                                    End If
                                    sLineToWrite = sLineToWrite & Trim(oPayment.E_Zip) & ";" 'Postnummer
                                    If EmptyString((oPayment.E_City)) Then
                                        sLineToWrite = sLineToWrite
                                    End If
                                    sLineToWrite = sLineToWrite & Trim(oPayment.E_City) & ";" 'Poststed
                                    sLineToWrite = sLineToWrite & VB6.Format(oPayment.MON_InvoiceAmount / 100, "##0.00") 'Bel�p

                                    oFile.WriteLine(sLineToWrite)

                                    '''' *****************************************************
                                    '''' The next commented lines are to be used if it is matched payments
                                    '''' The programming to write the specific recordtypes are NOT done
                                    '''' *****************************************************
                                    ''''                        'Find the client
                                    '''                        If oPayment.I_Account <> sOldAccountNo Then
                                    '''                            If oPayment.VB_ProfileInUse Then
                                    '''                                bAccountFound = False
                                    '''                                For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                    '''                                    For Each oClient In oFilesetup.Clients
                                    '''                                        For Each oaccount In oClient.Accounts
                                    '''                                            If oPayment.I_Account = oaccount.Account Then
                                    '''                                                sGLAccount = Trim$(oaccount.GLAccount)
                                    '''                                                sOldAccountNo = oPayment.I_Account
                                    '''                                                bAccountFound = True
                                    '''
                                    '''                                                Exit For
                                    '''                                            End If
                                    '''                                        Next oaccount
                                    '''                                    If bAccountFound Then Exit For
                                    '''                                    Next oClient
                                    '''                                If bAccountFound Then Exit For
                                    '''                                Next oFilesetup
                                    '''                            End If
                                    '''                        End If


                                    ''''                        If Not bAccountFound Then
                                    ''''                            Err.Raise 12003, "WriteGenericBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account)
                                    ''''                            '12003: Could not find account %1.Please enter the account in setup."
                                    ''''                        End If
                                    ''''
                                    ''''                        'Write GL-line if not previously written
                                    ''''                        If Not bGLRecordWritten Then
                                    ''''                            sLineToWrite = WriteOneTimeBankRecord(oBatch, oPayment, oClient, sGLAccount, iFormat)
                                    ''''                            oFile.WriteLine sLineToWrite
                                    ''''                            bGLRecordWritten = True
                                    ''''                        End If
                                    ''''
                                    ''''                        'First we have to find the freetext to the payment-lines.
                                    ''''                        iFreetextCounter = 0
                                    ''''                        sFreetextFixed = ""
                                    ''''                        For Each oInvoice In oPayment.Invoices
                                    ''''                            If oInvoice.MATCH_Original = True Then
                                    ''''                                For Each oFreeText In oInvoice.Freetexts
                                    ''''                                    iFreetextCounter = iFreetextCounter + 1
                                    ''''                                    If iFreetextCounter = 1 Then
                                    ''''                                        sFreetextFixed = RTrim$(oFreeText.Text)
                                    ''''                                    ElseIf iFreetextCounter = 2 Then
                                    ''''                                        sFreetextFixed = sFreetextFixed & " " & RTrim$(oFreeText.Text)
                                    ''''                                    Else
                                    ''''                                        Exit For
                                    ''''                                    End If
                                    ''''                                Next oFreeText
                                    ''''                            End If
                                    ''''                        Next oInvoice
                                    ''''                        sFreetextFixed = Trim$(oPayment.E_Name) & " " & sFreetextFixed
                                    ''''
                                    ''''                        For Each oInvoice In oPayment.Invoices
                                    ''''                            If oInvoice.MATCH_Final = True Then
                                    ''''                                'Add the variable part of the freetext
                                    ''''                                sFreeTextVariable = sFreetextFixed
                                    ''''                                For Each oFreeText In oInvoice.Freetexts
                                    ''''                                    If oFreeText.Qualifier > 2 Then
                                    ''''                                        sFreeTextVariable = RTrim$(oFreeText.Text) & " " & sFreeTextVariable
                                    ''''                                    End If
                                    ''''                                Next oFreeText
                                    ''''
                                    ''''                                If oInvoice.MATCH_Matched Then
                                    ''''                                    If oInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice Then
                                    ''''                                        sLineToWrite = WriteOneTimeInvoiceRecord(oPayment, oInvoice, oClient, sFreeTextVariable, iFormat)
                                    ''''                                    ElseIf oInvoice.MATCH_MatchType = MatchType.OpenInvoice Then
                                    ''''                                        sLineToWrite = WriteOneTimeCustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, iFormat)
                                    ''''                                    ElseIf oInvoice.MATCH_MatchType = MatchType.MatchedOnGL Then
                                    ''''                                        sLineToWrite = WriteOneTimeGLRecord(oPayment, oInvoice, oClient, sFreeTextVariable, iFormat)
                                    ''''                                    ElseIf oInvoice.MATCH_MatchType = MatchType.MatchedOnSupplier Then
                                    ''''                                        sLineToWrite = WriteOneTimeSupplierRecord(oPayment, oInvoice, oClient, sFreeTextVariable, iFormat)
                                    ''''                                    Else
                                    ''''                                        sLineToWrite = WriteOneTimeCustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, iFormat)
                                    ''''                                    End If
                                    ''''                                Else
                                    ''''                                    sLineToWrite = WriteOneTimeNotMatchedRecord(oPayment, oInvoice, oClient, sFreeTextVariable, iFormat)
                                    ''''                                End If
                                    ''''
                                    ''''                                oFile.WriteLine sLineToWrite
                                    ''''
                                    ''''                            End If 'oInvoice.MATCH_Final = True
                                    ''''                        Next oInvoice

                                    oPayment.Exported = True
                                End If 'bExportoPayment
                            Next oPayment ' payment
                        End If

                        'We have to save the last used sequenceno. in some cases
                        'First check if we use a profile
                        If oBatch.VB_ProfileInUse = True Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                'Always use the seqno at filesetup level
                                Case 1
                                    'Only for the first batch if we have one sequenceseries!
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                    oBatch.VB_Profile.Status = 2
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                Case 2
                                    sVoucherNo = sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0")
                                    oClient.VoucherNo = sVoucherNo
                                    oClient.Status = Profile.CollectionStatus.Changed
                                    oBatch.VB_Profile.Status = Profile.CollectionStatus.ChangeUnder
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = Profile.CollectionStatus.ChangeUnder

                                Case Else
                                    Err.Raise(12002, "WriteGenericBabelBank_InnbetalingsFile", LRS(12002, "Navision", sFilenameOut))

                            End Select

                        End If

                    Next oBatch 'batch


                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteOneTimeFormat" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteOneTimeFormat = True

    End Function
    Function WriteSGClientCSV(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef eTypeofRecords As BabelFiles.TypeofRecords) As Boolean
        ' For SG Finans
        ' Creates a TAB-separated CSV-file for clientreporting
        '-----------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer
        Dim sTmp As String
        Dim sTmpDate As String

        Try

            lCounter = 0
            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            ' First, create a headerline;
            '----------------------------
            sLine = "Kontonr" & Chr(9) & "Navn" & Chr(9) & "Adresse1" & Chr(9) & "Adresse2" & Chr(9) & "Bokf.dato" & Chr(9) & "Val.dato" & Chr(9) & "Forfall" & Chr(9) & "Kode" & Chr(9) & "Tekst" & Chr(9) & "Bilagsnr" & Chr(9) & "Transbel�p" & Chr(9) & "Valutakode" & Chr(9) & "Valutabel�p" & Chr(9) & "Refnr." & Chr(9) & "Selger" & Chr(9) & "Prosjekt" & Chr(9) & "Fritekst"
            oFile.WriteLine(sLine)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                ' 2008.03.25 Changed, because we are using SGs account, This caused trouble
                                'If oPayment.I_Account = sI_Account Then
                                If oPayment.VB_ClientNo = sClientNo Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    ' 2008.03.25 Changed, because we are using SGs account, This caused trouble
                                    'If oPayment.I_Account = sI_Account Then
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        ' 2008.03.25 Changed, because we are using SGs account, This caused trouble
                                        'If oPayment.I_Account = sI_Account Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If


                                If bExportoPayment And oPayment.OmitExport = False Then
                                    ' Changed 23.05.2007
                                    ' Export OCR payments, or payments without OCR
                                    ' added 28.09.2007 Do not export payments with Cancel = True to OCR, but to OTHER
                                    If (eTypeofRecords = BabelFiles.TypeofRecords.NotOcr And (Not IsOCR((oPayment.PayCode)) Or oPayment.Cancel)) Or (eTypeofRecords = BabelFiles.TypeofRecords.OCRonly And (IsOCR((oPayment.PayCode)) And oPayment.Cancel = False)) Then

                                        For Each oInvoice In oPayment.Invoices
                                            lCounter = lCounter + 1
                                            sLine = ""

                                            '1  Kontonr, ClientDebtorReference
                                            sLine = sLine & oInvoice.CustomerNo & Chr(9)
                                            '2  Navn, DebtorPartyName.
                                            sLine = sLine & oPayment.E_Name & Chr(9)
                                            '3  Adresse1, Not in use
                                            sLine = sLine & oPayment.E_Adr1 & Chr(9)
                                            '4  Adresse2
                                            sLine = sLine & oPayment.E_Adr1 & Chr(9)
                                            '5  Bokf.dato
                                            sLine = sLine & oPayment.DATE_Payment & Chr(9)
                                            '6  Val.dato
                                            sLine = sLine & oPayment.DATE_Value & Chr(9)
                                            '7  Forfall, Not in use
                                            sLine = sLine & "" & Chr(9)
                                            '8  Kode, ClearingTransactionType
                                            'sLine = sLine & oPayment.PayCode & Chr(9)
                                            ' changed 14.06.2007
                                            If IsOCR((oPayment.PayCode)) Then
                                                sLine = sLine & "OCR" & Chr(9)
                                            Else
                                                sLine = sLine & "OTHER" & Chr(9)
                                            End If
                                            '9  Tekst, ClearingTransactionTypeDescription
                                            sLine = sLine & oInvoice.REF_Bank & Chr(9)
                                            '10  Bilagsnr, BatchNumber
                                            sLine = sLine & oPayment.VoucherNo & Chr(9)
                                            '11 Transbel�p, reconciledAmountDAC
                                            ' 22.08.2007 Divided with 100
                                            ' 28.02.2008 Multiply with -
                                            'sLine = sLine &VB6.Format(oPayment.MON_InvoiceAmount / -100, "###00.00") & Chr(9)
                                            ' 28.02.2008 Replace decimalcomma with decimalpoint always, as the old format from Alpha
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_InvoiceAmount / -100, "####0.00"), ",", ".") & Chr(9)

                                            '12 Valutakode, transactionCurrencyCode
                                            sLine = sLine & oPayment.MON_InvoiceCurrency & Chr(9)
                                            '13 Valutabel�p, AmountTransactionCurrency
                                            ' 22.08.2007 Divided with 100
                                            'sLine = sLine &VB6.Format(oPayment.MON_TransferredAmount / 100, "###00.00") & Chr(9)
                                            ' 30.10.2007 Using MON_OriginallyPaidAmount instead. Gives us amount in paid currency
                                            ' 28.02.2008 Multiply with -
                                            ' 28.02.2008 Replace decimalcomma with decimalpoint always, as the old format from Alpha
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_OriginallyPaidAmount / -100, "###00.00"), ",", ".") & Chr(9)

                                            '14 Refnr., DocumentNumber
                                            sLine = sLine & oInvoice.InvoiceNo & Chr(9)
                                            '15 Selger, SellersNumber
                                            sLine = sLine & oPayment.ExtraD1 & Chr(9)
                                            '16 Prosjekt, ProjectNumber
                                            sLine = sLine & oPayment.ExtraD2 & Chr(9)
                                            '17 Fritekst, Comments
                                            If oInvoice.Freetexts.Count > 0 Then
                                                sLine = sLine & oInvoice.Freetexts(1).Text
                                            Else
                                                sLine = sLine & ""
                                            End If

                                            oFile.WriteLine(sLine) '& vbCrLf
                                        Next oInvoice
                                        ' Mark only non-ocr as expored, otherwise OCR will not be exported in OCR-file
                                        If Not IsOCR((oPayment.PayCode)) Then
                                            oPayment.Exported = True
                                        End If
                                    End If
                                End If

                            Next oPayment ' oPayment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSGClientCSV" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSGClientCSV = True

    End Function
    Function WriteSG_DirekteBetalingsbrev(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef eTypeofRecords As BabelFiles.TypeofRecords) As Boolean
        ' For SG Finans. NB; BENYTTES IKKE I PROD
        ' Creates a ;-separated CSV-file to use when printing Direkte Betalingsbrev
        '--------------------------------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer
        'Dim sTmp As String
        'Dim sTmpDate As String

        Try
            lCounter = 0
            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    ' Export payments where  oPayment.ExtraD1 = "DIR"
                                    If UCase(oPayment.ExtraD1) = "DIR" And oPayment.MATCH_Matched > BabelFiles.MatchStatus.ProposedMatched Then ' Direkte Betalinger
                                        For Each oInvoice In oPayment.Invoices
                                            ' DebitorNo is in oInvoice.CustomerNo
                                            If oInvoice.MATCH_Final And oInvoice.MATCH_Matched Then
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Or oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
                                                    lCounter = lCounter + 1
                                                    sLine = ""


                                                    '1  Report ID (rapport id)
                                                    sLine = sLine & "2825;"
                                                    '2  Report Name
                                                    sLine = sLine & "Direct Payment Letter;" '>>>>>>??????
                                                    '3  Client Name (kunde navn)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 9) & ";"
                                                    '4  Client number (kunde nummer)
                                                    sLine = sLine & oInvoice.MATCH_ClientNo & ";"
                                                    '5  Client org no, not in use
                                                    sLine = sLine & ";"
                                                    '6  Debtor Name (debitor navn)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 1) & ";"
                                                    '7  National Language Code
                                                    sLine = sLine & "no_NO;"
                                                    '8  Debtor Address Line 1 (debitor addresselinje 1)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 2) & ";"
                                                    '9  Debtor Address Line 2 (debitor addresselinje 2)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 3) & ";"
                                                    '10  Debtor Address Line 3 (debitor addresselinje 3)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 4) & ";"
                                                    '11 City
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 6) & ";"
                                                    '12 State
                                                    sLine = sLine & ";"
                                                    '13 Post code
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 5) & ";"
                                                    '14 Country
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 7) & ";"
                                                    '15 Special Deliver Information, AgreementTypeBSVCode (5000, etc)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 10) & ";"
                                                    '16 Debtor No/ Acc Ref (debitor ) (Clients CustomerNo)
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 8) & ";"
                                                    '17 Total Sum(totalbel�p)
                                                    sLine = sLine & oInvoice.MON_InvoiceAmount & ";" ' >>>>>>>???? Vil alltid hele paymentbel�pet var DirekteBetaling (ikke deler av det?)
                                                    '18 Datetime [todays datetime] (dato)[dagens dato]
                                                    sLine = sLine & VB6.Format(Now, "DD.MM.YY") & ";"
                                                    '19 (-) Invoice Due Date (forfallsdato)
                                                    sLine = sLine & ";"
                                                    '20 (-) Invoice No (faktura no)
                                                    sLine = sLine & ";"
                                                    '21 (-) Invoice Date (faktura dato)
                                                    sLine = sLine & ";"
                                                    '22 (-) Invoice Amount (faktura bel�p)
                                                    sLine = sLine & ";"
                                                    '23 (-) Fee(gebyr / renter)
                                                    sLine = sLine & ";"
                                                    '24 (-) Fee from date (rentedato fra)
                                                    sLine = sLine & ";"
                                                    '25 (-) Fee to date (rentedate til)
                                                    sLine = sLine & ";"
                                                    '26 (-) Fee days (antall rentedager)
                                                    sLine = sLine & ";"
                                                    '27 SWIFT
                                                    sLine = sLine & "DABANO22;"
                                                    '28 IBAN No  >>>>>>>>>>>>>>>??????? Har vi IBANN for innbetalingskonto i "v�r" database?
                                                    sLine = sLine & xDelim((oInvoice.Extra1), Chr(9), 10) & ";"
                                                    '29 (-) debitor org no
                                                    sLine = sLine & ";"
                                                    '30 (-) KID
                                                    sLine = sLine & ";"
                                                    '31 (-) Due Date (forfallsdato)
                                                    sLine = sLine & ";"
                                                    '32 Currency
                                                    sLine = sLine & oPayment.MON_InvoiceCurrency & ";"
                                                    '33 Service Provider Organisation No (organisasjonsnummer)
                                                    sLine = sLine & "0098766439;"
                                                    '34 Service Provider Name (factoring selskap)
                                                    sLine = sLine & "Nordea Finance AS;"
                                                    '35 Service Provider Accountable (factoringselskap kundeansvarlig)
                                                    sLine = sLine & ";"
                                                    '36 SC language code
                                                    sLine = sLine & "no_NO;"
                                                    '37 Service Provider Address Line 1 (factoring selskap adresselinje 1)
                                                    sLine = sLine & "Strandveien 18;"
                                                    '38 Service Provider Address Line 2 (factoring selskap adresselinje 2)
                                                    sLine = sLine & "Postboks 105;"
                                                    '39 Service Provider Address Line 3 (factoring selskap adresselinje 3)
                                                    sLine = sLine & ";"
                                                    '40 Service Provider City (factoring selskap by)
                                                    sLine = sLine & "Lysaker;"
                                                    '41 Service Provider State(factoring selskap stat)
                                                    sLine = sLine & ";"
                                                    '42 Service Provider Postcode (factoring selskap postkode)
                                                    sLine = sLine & "1325;"
                                                    '43 Service Provider Country (factoring selskap land)
                                                    sLine = sLine & "Norge;"
                                                    '44 (-)Special Deliver Information
                                                    sLine = sLine & ";"
                                                    '45 (-)Service Provider Bank (factoring selskap bank)
                                                    sLine = sLine & ";"
                                                    '46 Service Provider Account No (factoring selskap konto no)
                                                    sLine = sLine & oPayment.I_Account & ";"
                                                    '47 (-) Accountable Phone(kundekonsulent telefon)
                                                    sLine = sLine & ";"
                                                    '48 (-) Accountable Fax (kundekonsulent fax)
                                                    sLine = sLine & ";"
                                                    '49 (-) Accountable Email (kundekonsulent email)
                                                    sLine = sLine & ";"

                                                    oFile.WriteLine(sLine) '& vbCrLf
                                                End If 'oInvoice.MATCH_MatchType = MatchType.MatchedOnCustomer Or oInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice Then
                                            End If 'If oInvoice.MATCH_Final And oInvoice.MATCH_Matched Then
                                        Next oInvoice

                                    End If 'If UCase(oPayment.ExtraD1) = "DIR" And oPayment.MATCH_Matched > MatchStatus.ProposedMatched Then ' Direkte Betalinger
                                End If

                            Next oPayment ' oPayment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSG_DirekteBetalingsbrev" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSG_DirekteBetalingsbrev = True

    End Function
    Function WritePolitiets_CSV(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' Exports a semiclon separated file with OCR and other payments for Politiets Fellesforbund

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sText As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean

        Try

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    '-------- Write content of each payment to file---------
                                    If oPayment.DATE_Payment <> "19900101" Then
                                        ' Skip specials like autogiro, etc
                                        If oPayment.Invoices.Count > 0 Then



                                            ' Posttype, Mottakerkonto;Val.Dato,Bokf.dato,Betalers konto;Navn;Adr1;Adr2,Postnr,Sted;KID;Bel�p;Valuta;Melding
                                            For Each oInvoice In oPayment.Invoices
                                                sLine = ""
                                                If IsOCR((oPayment.PayCode)) Then
                                                    sLine = sLine & "O;"
                                                Else
                                                    sLine = sLine & "I;"
                                                End If
                                                sLine = sLine & oPayment.I_Account & ";" ' 2 Mottakerkonto
                                                sLine = sLine & oPayment.DATE_Value & ";" ' 3 Val.Dato
                                                sLine = sLine & oPayment.DATE_Payment & ";" ' 4 Bokf.Dato
                                                ' Do not use Bankenes hjelpekonto!
                                                If Mid(oPayment.E_Account, 5, 1) = "9" Then
                                                    sLine = sLine & "Hjelpekonto;"
                                                Else
                                                    sLine = sLine & oPayment.E_Account & ";" ' 5 Betalers konto
                                                End If
                                                sLine = sLine & oPayment.E_Name & ";" ' 6 Betalers Navn
                                                sLine = sLine & oPayment.E_Adr1 & ";" ' 7 Betalers adr1
                                                sLine = sLine & oPayment.E_Adr2 & ";" ' 8 Betalers adr2
                                                sLine = sLine & oPayment.E_Zip & ";" ' 9 Betalers postnr
                                                sLine = sLine & oPayment.E_City & ";" ' 10 Betalers poststed
                                                sLine = sLine & Trim(oInvoice.Unique_Id) & ";" ' 11 KID
                                                sLine = sLine & Trim(Str(Int(oInvoice.MON_TransferredAmount))) & ";" ' 12 Bel�p i �re   & "." & Right$(Str(oInvoice.MON_TransferredAmount), 2)) & ";" ' Bel�p
                                                sLine = sLine & oPayment.MON_TransferCurrency & ";" ' 13 Valuta

                                                sText = ""
                                                For Each oFreeText In oInvoice.Freetexts
                                                    sText = sText & Trim(oFreeText.Text) & " "
                                                Next oFreeText
                                                sLine = sLine & Left(sText, 100) ' 14 Melding fra mottaker
                                                If Len(sLine) > 0 Then
                                                    oFile.WriteLine((sLine))
                                                End If


                                            Next oInvoice
                                        Else
                                            ' Error, no invoice. Should not happen
                                            sLine = ""
                                            If IsOCR((oPayment.PayCode)) Then
                                                sLine = sLine & "O;"
                                            Else
                                                sLine = sLine & "I;"
                                            End If
                                            sLine = sLine & oPayment.I_Account & ";" ' 2 Mottakerkonto
                                            sLine = sLine & oPayment.DATE_Value & ";" ' 3 Val.Dato
                                            sLine = sLine & oPayment.DATE_Payment & ";" ' 4 Bokf.Dato
                                            ' Do not use Bankenes hjelpekonto!
                                            If Mid(oPayment.E_Account, 5, 1) = "9" Then
                                                sLine = sLine & "Hjelpekonto;"
                                            Else
                                                sLine = sLine & oPayment.E_Account & ";" ' 5 Betalers konto
                                            End If
                                            sLine = sLine & oPayment.E_Name & ";" ' 6 Betalers Navn
                                            sLine = sLine & oPayment.E_Adr1 & ";" ' 7 Betalers adr1
                                            sLine = sLine & oPayment.E_Adr2 & ";" ' 8 Betalers adr2
                                            sLine = sLine & oPayment.E_Zip & ";" ' 9 Betalers postnr
                                            sLine = sLine & oPayment.E_City & ";" ' 10 Betalers poststed
                                            sLine = sLine & ";" ' 11 KID
                                            sLine = sLine & Trim(Str(Int(oPayment.MON_TransferredAmount))) & ";" ' 12 Bel�p i �re   & "." & Right$(Str(oInvoice.MON_TransferredAmount), 2)) & ";" ' Bel�p
                                            sLine = sLine & oPayment.MON_TransferCurrency & ";" ' 13 Valuta

                                            sText = ""
                                            sLine = sLine & "" ' 14 Melding fra mottaker
                                            If Len(sLine) > 0 Then
                                                oFile.WriteLine((sLine))
                                            End If

                                        End If
                                        oPayment.Exported = True
                                    Else
                                        oPayment.Exported = True
                                    End If
                                Else
                                    oPayment.Exported = False
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WritePolitiets_CSV" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WritePolitiets_CSV = True

    End Function

    Function WriteAIG_Kontoinfo(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short) As Boolean
        ' Used for AIG. Importfile is a DnBNOR Statementfile, + Telepay Avregn returnfile

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sName As String
        Dim aNames() As String
        Dim bThisIsAccountInfoItem As Boolean
        Dim bNameOK As Boolean

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer
        Dim sTmp As String
        Dim sTmpDate As String

        Try

            lCounter = 0
            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    End If
                                End If

                                If bExportoPayment Then

                                    If Not RunTime() Then

                                        If EmptyString((oPayment.E_Account)) Then
                                            bThisIsAccountInfoItem = True
                                        Else
                                            bThisIsAccountInfoItem = False
                                        End If
                                        sTmpDate = oPayment.DATE_Payment

                                        For Each oInvoice In oPayment.Invoices
                                            'A - Counter (no in use)
                                            sLine = ";"
                                            'B - Not in use
                                            sLine = sLine & ";"
                                            'C - Not in use
                                            sLine = sLine & ";"
                                            'D - Customno from ownref in oInvoice
                                            If bThisIsAccountInfoItem Then
                                                sLine = sLine & sTmpDate & ";"
                                            Else
                                                sLine = sLine & Mid(Trim(oInvoice.REF_Own), 4, 6) & ";"
                                            End If
                                            'E - Last name - Always used last part om name
                                            bNameOK = False
                                            aNames = Split(Trim(oPayment.E_Name))
                                            If Not Array_IsEmpty(aNames) Then
                                                If UBound(aNames) > -1 Then
                                                    sLine = sLine & UCase(aNames(UBound(aNames))) & ";"
                                                    bNameOK = True
                                                End If
                                            End If
                                            If Not bNameOK Then
                                                sTmp = ""
                                                For Each oFreeText In oInvoice.Freetexts
                                                    sTmp = Trim(oFreeText.Text)
                                                    If Not EmptyString(sTmp) Then
                                                        Exit For
                                                    End If
                                                Next oFreeText
                                                sLine = sLine & Replace(sTmp, ";", "", , , CompareMethod.Text) & ";"
                                            End If

                                            'F - First name (all but the last)
                                            sName = ""
                                            For lCounter = 0 To UBound(aNames) - 1
                                                If lCounter = 0 Then
                                                    sName = sName & UCase(aNames(lCounter))
                                                Else
                                                    sName = sName & " " & UCase(aNames(lCounter))
                                                End If
                                            Next lCounter
                                            sLine = sLine & sName & ";"
                                            'G - Paymentnumber in Nice (Not in use)
                                            sLine = sLine & ";"
                                            'H - Amount in (Not in use)
                                            sLine = sLine & ";"
                                            'I - Amount in (Not in use)
                                            sLine = sLine & ";"
                                            'J - Amount
                                            If oPayment.Invoices.Count = 1 Then
                                                sTmp = ConvertFromAmountToString((oPayment.MON_InvoiceAmount), "", ",")
                                                sLine = sLine & sTmp & ";"
                                            Else
                                                sTmp = "-" & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), "", ",")
                                                sLine = sLine & sTmp & ";"
                                            End If
                                            'K - Something (Not in use)
                                            If bThisIsAccountInfoItem Then
                                                sLine = sLine & oPayment.REF_Bank2 & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            'L - Saksnummer - first part
                                            sLine = sLine & xDelim((oPayment.VoucherNo), "-", 1) & ";" 'Retrieved in TreatAIG
                                            'M - Saksnummer - second part
                                            sLine = sLine & xDelim((oPayment.VoucherNo), "-", 2) & ";" 'Retrieved in TreatAIG
                                            'N - Period (Not in use)
                                            sLine = sLine & ";"
                                            'O - CustomerNo (Not in use)
                                            sLine = sLine & ";"
                                            'P - Bookdate
                                            sLine = sLine & sTmpDate & ";"
                                            'Q - Source
                                            If bThisIsAccountInfoItem Then
                                                sLine = sLine & Right(Trim(oPayment.I_Account), 5) & ";"
                                            Else
                                                If oPayment.Cancel Then
                                                    sLine = sLine & "Kansellert" & Right(oPayment.I_Account, 5) & ";"
                                                Else
                                                    sLine = sLine & "Utbetalt" & Right(oPayment.I_Account, 5) & ";"
                                                End If
                                            End If
                                            'R - Abs. amount (not in use)
                                            sLine = sLine & ";"
                                            'S - Counter2 (not in use)
                                            sLine = sLine & ";"
                                            'T - Utbetanvisning
                                            If bThisIsAccountInfoItem Then
                                                sLine = sLine & "Kontoinformasjon"
                                            Else
                                                If oPayment.E_Account = "00000000019" Then
                                                    sLine = sLine & "Utbetalingsanvisning"
                                                Else
                                                    sLine = sLine & "Til konto"
                                                End If
                                            End If

                                        Next oInvoice
                                        oFile.WriteLine(sLine)


                                    Else
                                        lCounter = lCounter + 1
                                        '-------- Write content of each payment to file---------
                                        sLine = ""

                                        '1 Bookdate
                                        sTmpDate = oPayment.DATE_Payment
                                        sLine = sLine & Mid(sTmpDate, 5, 2) & "/" & Mid(sTmpDate, 7, 2) & "/" & Left(sTmpDate, 4) & ";"

                                        '2) AIG Account no
                                        sLine = sLine & oPayment.I_Account & ";"

                                        '3 Recivers name if present
                                        sLine = sLine & oPayment.E_Name & ";"

                                        '4 Receivers account
                                        sLine = sLine & oPayment.E_Account & ";"

                                        '5 Amount
                                        sLine = sLine & CDbl(oPayment.MON_InvoiceAmount / 100) & ";"

                                        '6 Ref Own
                                        'Changed 26.01.2007. In TreatAIG we try to retrieve the saksnummer
                                        'sLine = sLine & oPayment.REF_Own & ";"
                                        sLine = sLine & oPayment.VoucherNo & ";"

                                        '7 T for Telepay
                                        If CDbl(oPayment.PayCode) > 149 And CDbl(oPayment.PayCode) < 200 Then
                                            sLine = sLine & "T" & ";"
                                        Else
                                            sLine = sLine & "" & ";"
                                        End If

                                        '8) Bank reference
                                        sLine = sLine & Left(oPayment.REF_Bank1, 16) & ";"

                                        '9 Notification
                                        'Changed 26.01.2007. Added the replace
                                        sTmp = ""
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                sTmp = sTmp & Trim(oFreeText.Text) & " "
                                            Next oFreeText
                                        Next oInvoice
                                        sLine = sLine & Replace(Left(sTmp, 255), ";", "") & ";"

                                        oFile.WriteLine(sLine)

                                    End If

                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteAIG_Kontoinfo" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteAIG_Kontoinfo = True

    End Function
    Function WriteProfundo(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String ', sIAccountNo As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim sZip, sAdr2, sAdr1, sAdr3, sCity As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sId As String
        Dim sFreetext As String
        Dim sRef1, sRef2 As String
        Dim sBirthNumber As String
        Dim sGLAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sNoMatchAccount As String
        Dim lBatchcounter As Integer
        Dim bErrorInMaalgruppeExists As Boolean
        Dim iFreetextCounter As Short
        Dim lCounter As Integer

        Try

            bAppendFile = False
            sOldAccountNo = ""
            sGLAccount = ""
            bAccountFound = False
            lBatchcounter = 0
            bErrorInMaalgruppeExists = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'If IsOCR(oPayment.PayCode) Then
                            'Nothing to do, don't export
                            'Else
                            ' Also export KID !!
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            lBatchcounter = lBatchcounter + 1
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If


                                If bExportoPayment Then

                                    '-------- fetch content of each payment ---------
                                    sName = oPayment.E_Name
                                    sAdr1 = "" '""
                                    sAdr2 = "" '""
                                    ' If Adr2="-", empty Adr2;
                                    If Trim(oPayment.E_Adr2) = "-" Then
                                        oPayment.E_Adr2 = ""
                                    End If

                                    ' both addresslines in use
                                    sAdr1 = oPayment.E_Adr1
                                    sAdr2 = oPayment.E_Adr2



                                    nAmount = oPayment.MON_TransferredAmount
                                    sDate = oPayment.DATE_Payment

                                    sFreetext = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sFreetext = sFreetext & " " & Trim(oFreeText.Text)
                                        Next oFreeText
                                    Next oInvoice
                                    sFreetext = Mid(sFreetext, 2)

                                    If oPayment.PayType = "I" Then
                                        ' international
                                        sAdr3 = oPayment.E_Adr3
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = WriteProfundoTransaction(sName, sAdr1, sAdr2, sAdr3, sZip, (oPayment.E_CountryCode), nAmount, sDate, (oPayment.E_Account), (oPayment.I_Account), (oPayment.PayCode), sFreetext, oPayment.Invoices(1).Unique_Id)
                                    Else
                                        sZip = oPayment.E_Zip
                                        sCity = oPayment.E_City
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = WriteProfundoTransaction(sName, sAdr1, sAdr2, sCity, sZip, (oPayment.E_CountryCode), nAmount, sDate, (oPayment.E_Account), (oPayment.I_Account), (oPayment.PayCode), sFreetext, oPayment.Invoices(1).Unique_Id)
                                    End If
                                    If Len(sLine) > 0 Then
                                        oFile.WriteLine((sLine))
                                    End If
                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteProfundo" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteProfundo = True

    End Function
    Public Function WriteProfundoTransaction(ByRef sName As String, ByRef sAdr1 As String, ByRef sAdr2 As String, ByRef sAdr3 As String, ByRef sZip As String, ByRef sCountryCode As String, ByRef nAmount As Double, ByRef sDate As String, ByRef sFromAccount As String, ByRef sToAccount As String, ByRef sPayCode As String, ByRef sFreetext As String, ByRef sKID As String) As String

        Dim sLine As String
        Dim aNames() As String
        Dim sASString As String 'Store AS, ASA, A.S, etc, and add later
        Dim sJrString As String ' Check Jr. Sen. etc
        Dim i As Integer
        Dim sMalGruppe As String
        Dim bTheInvoiceIsMatched As Boolean
        Dim sFreeTextVariable As String
        Dim oFreeText As vbBabel.Freetext
        Dim sAmount As String
        Dim bAbandon As Boolean

        ' Remove hyphens from name
        sName = Replace(sName, "'", "")

        ' New tests 10.11.04
        If InStr(UCase(sName), "[") > 0 Then
            sName = Replace(sName, "[", "�")
        End If
        If InStr(UCase(sAdr1), "[") > 0 Then
            sAdr1 = Replace(sAdr1, "[", "�")
        End If
        If InStr(UCase(sAdr2), "[") > 0 Then
            sAdr2 = Replace(sAdr2, "[", "�")
        End If
        If InStr(UCase(sName), "]") > 0 Then
            sName = Replace(sName, "]", "�")
        End If
        If InStr(UCase(sAdr1), "]") > 0 Then
            sAdr1 = Replace(sAdr1, "]", "�")
        End If
        If InStr(UCase(sAdr2), "]") > 0 Then
            sAdr2 = Replace(sAdr2, "]", "�")
        End If
        If InStr(UCase(sName), "\") > 0 Then
            sName = Replace(sName, "\", "�")
        End If
        If InStr(UCase(sAdr1), "\") > 0 Then
            sAdr1 = Replace(sAdr1, "\", "�")
        End If
        If InStr(UCase(sAdr2), "\") > 0 Then
            sAdr2 = Replace(sAdr2, "\", "�")
        End If
        If InStr(UCase(sName), "@") > 0 Then
            sName = Replace(sName, "@", "�")
        End If
        If InStr(UCase(sAdr1), "@") > 0 Then
            sAdr1 = Replace(sAdr1, "@", "�")
        End If
        If InStr(UCase(sAdr2), "@") > 0 Then
            sAdr2 = Replace(sAdr2, "@", "�")
        End If
        If InStr(UCase(sName), "$") > 0 Then
            sName = Replace(sName, "$", "�")
        End If
        If InStr(UCase(sAdr1), "$") > 0 Then
            sAdr1 = Replace(sAdr1, "$", "�")
        End If
        If InStr(UCase(sAdr2), "$") > 0 Then
            sAdr2 = Replace(sAdr2, "$", "�")
        End If
        If InStr(UCase(sName), Chr(162)) > 0 Then
            sName = Replace(sName, Chr(162), "�")
        End If
        If InStr(UCase(sAdr1), Chr(162)) > 0 Then
            sAdr1 = Replace(sAdr1, Chr(162), "�")
        End If
        If InStr(UCase(sAdr2), Chr(162)) > 0 Then
            sAdr2 = Replace(sAdr2, Chr(162), "�")
        End If
        If InStr(UCase(sName), Chr(187)) > 0 Then
            sName = Replace(sName, Chr(187), "�")
        End If
        If InStr(UCase(sAdr1), Chr(187)) > 0 Then
            sAdr1 = Replace(sAdr1, Chr(187), "�")
        End If
        If InStr(UCase(sAdr2), Chr(187)) > 0 Then
            sAdr2 = Replace(sAdr2, Chr(187), "�")
        End If





        ' new 30.10.2003
        ' Some names in CREMUL is a mix of name + adress,
        ' e.g. NAD+PL+++Inge Hegvik,Sagav.14,3050 Mj�n+SAGAVN.14+3050 MJ�NDALEN++0000
        ' Common; it is both a comma in name, and zip is part of name
        'If InStr(sName, ",") > 0 Then
        '    If InStr(sName, sZip) > 0 Then
        '        ' remove from first comma in name;
        '        sName = Left$(sName, InStr(sName, ",") - 1)
        '    ElseIf InStr(UCase(sAdr1), UCase(Trim$(Mid$(sName, InStr(sName, ",") + 1)))) > 0 Or _
        ''        InStr(UCase(sAdr2), UCase(Trim$(Mid$(sName, InStr(sName, ",") + 1)))) > 0 Then
        '        ' NAD+PL+++Merethe Hannestad,Stockflethsg+STOCKFLETHSG 56 A+0461 OSLO
        '        ' where comma in name and part of adr2 is in name
        '        sName = Left$(sName, InStr(sName, ",") - 1)
        '    End If
        'End If


        If sName <> "Ukjent" Then
            ' Don't export records without names1
            sLine = ""

            sLine = sLine & PadRight(sName, 30, " ")

            ' Adr1
            '-----
            sLine = sLine & Left(PadRight(sAdr1, 30, " "), 30)

            ' Adr2
            '-----
            sLine = sLine & Left(PadRight(sAdr2, 30, " "), 30)

            ' Adr3/City
            '-----
            sLine = sLine & Left(PadRight(sAdr3, 30, " "), 30)

            ' Zip
            '----
            sLine = sLine & Left(PadLeft(sZip, 4, "0"), 4)


            ' Landkode
            '---------
            If EmptyString(sCountryCode) Then
                sCountryCode = "NO"
            End If
            sLine = sLine & PadRight(sCountryCode, 2, " ")

            ' Frakonto
            sLine = sLine & Left(PadLeft(sFromAccount, 11, "0"), 11)

            ' Amount
            '-------
            sLine = sLine & Right(PadLeft(VB6.Format(nAmount / 100, "###0.00"), 12, " "), 12)

            ' tilkonto
            sLine = sLine & Left(PadLeft(sToAccount, 11, "0"), 11)

            ' Date
            '-----
            sLine = sLine & sDate 'YYYYMMDD

            'KID
            '---
            sLine = sLine & PadRight(sKID, 25, " ")

            ' PayCode from Cremul;
            '---------------------
            sLine = sLine & bbGetPayCode(sPayCode, "CREMUL")

            ' Fritekst
            '---------
            sLine = sLine & PadRight(sFreetext, 80, " ")


            WriteProfundoTransaction = sLine
        Else
            WriteProfundoTransaction = ""
        End If


    End Function
    Function WriteBluewaterRetur(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' Avregningsretur som ; separert fil til Visma Global
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean

        Try

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    '-------- Write content of each payment to file---------
                                    ' kan v�re slettetranser, da er det ingen valuteringsdato!
                                    If oPayment.DATE_Value = "19900101" Then
                                        oFile.WriteLine(Trim(Str(Int(oPayment.MON_TransferredAmount / 100)) & "," & Right(Str(oPayment.MON_TransferredAmount), 2)) & ";" & oPayment.DATE_Payment & ";" & Trim(oPayment.REF_Own) & ";S")
                                    Else
                                        oFile.WriteLine(Trim(Str(Int(oPayment.MON_TransferredAmount / 100)) & "," & Right(Str(oPayment.MON_TransferredAmount), 2)) & ";" & oPayment.DATE_Value & ";" & Trim(oPayment.REF_Own) & ";")
                                    End If
                                    oPayment.Exported = True
                                Else
                                    oPayment.Exported = False
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBluewaterRetur" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBluewaterRetur = True

    End Function

    Function WriteGjensidige_S2000_DEBMUL(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim sLine As String
        'Dim sPayCode As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sOldDate, sOldAccount, sOldCurrency As String
        Dim nAmount As Double
        Dim sMessageNo As String
        Dim oFilesetup As vbBabel.FileSetup
        Dim sSenderID As String = ""
        Dim sReceiverID As String = ""

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Only export statuscode  02. The others don't give any sense in DEBMUL
                If oBabel.StatusCode = "02" Then
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch
                Else
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch
                End If 'If oBabel.StatusCode = "02" Then

                If bExportoBabel Then
                    sSenderID = ""
                    sReceiverID = ""
                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabel.Batches.Item(1).Payments.Item(1).VB_FilenameOut_ID Then
                            If Not EmptyString(oFilesetup.CompanyNo) Then
                                sReceiverID = oFilesetup.CompanyNo
                                sSenderID = oFilesetup.AdditionalNo
                                Exit For
                            End If
                        End If
                    Next oFilesetup
                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteGjensidige_000(oBabel, sSenderID, sReceiverID)
                        'sLine = WriteGjensidige_000(oBabel)
                        oFile.WriteLine((sLine))
                        If oBabel.Batches.Count > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object oBabel.Batches(1).Payments. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If oBabel.Batches(1).Payments.Count > 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBabel.Batches().Payments. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sMessageNo = Left(oBabel.Batches(1).Payments(1).REF_Own, 6)
                                ' changed MeldingsID 16.11.2007 Reversed
                                'sMessageNo =VB6.Format(Date, "YYMMDD") &VB6.Format(Time, "HHMM") & "0001"
                            End If
                        End If
                        sLine = WriteGjensidige_010(oBabel, "DEBMUL", sMessageNo)
                        oFile.WriteLine((sLine))

                        bFileStartWritten = True
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then

                                    ' Write 020, 021 and 030 for each payment, due to the nature of Telepay returnfiles
                                    ' Try to write them only when shift in date or currency or I_account
                                    ' For International, one 020 for each payment!

                                    If oPayment.I_Account <> sOldAccount Or oPayment.DATE_Payment <> sOldDate Or oPayment.MON_InvoiceCurrency <> sOldCurrency Or oPayment.PayType = "I" Then
                                        ' Calculate amount in next batch
                                        'nAmount = CalculateGjensidigeBatchAmount(oBatch, oPayment)
                                        nAmount = CalculateGjensidige_BatchAmount(oBabelFiles, oBabel, oBatch, oPayment)
                                        ' Called a 020-loop in S2000 DEBMUL
                                        sLine = WriteGjensidige_020(oBatch, oPayment, nAmount)
                                        oFile.WriteLine((sLine))
                                        sLine = WriteGjensidige_021(oBatch, oPayment)
                                        oFile.WriteLine((sLine))
                                        sLine = WriteGjensidige_030(oBatch, oPayment)
                                        oFile.WriteLine((sLine))
                                    End If

                                    sOldAccount = oPayment.I_Account
                                    sOldDate = oPayment.DATE_Payment
                                    sOldCurrency = oPayment.MON_InvoiceCurrency

                                    ' Next loop is called a 040-loop
                                    sLine = WriteGjensidige_040(oPayment)
                                    oFile.WriteLine((sLine))
                                    If oPayment.PayType = "I" Then
                                        ' For International, write 3 different payment-records ????Always 3 ?????
                                        sLine = WriteGjensidige_042_36(oPayment)
                                        oFile.WriteLine((sLine))
                                        sLine = WriteGjensidige_042_60(oPayment)
                                        oFile.WriteLine((sLine))
                                        sLine = WriteGjensidige_042_98(oPayment)
                                        oFile.WriteLine((sLine))
                                    Else
                                        ' For domestic write one 042_60
                                        sLine = WriteGjensidige_042_60(oPayment)
                                        oFile.WriteLine((sLine))
                                    End If

                                    sLine = WriteGjensidige_050(oPayment)
                                    oFile.WriteLine((sLine))

                                    ' I f�lge Mats s� er ikke 052 og 053 med
                                    'If oPayment.PayType = "I" Then
                                    '    sLine = WriteGjensidige_052(oPayment)
                                    '    oFile.WriteLine (sLine)
                                    '    sLine = WriteGjensidige_053(oPayment)
                                    '    oFile.WriteLine (sLine)
                                    'End If

                                    oPayment.Exported = True

                                End If

                            Next oPayment ' payment

                        End If

                    Next oBatch 'batch


                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidige_S2000_DEBMUL" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidige_S2000_DEBMUL = True

    End Function
    Function WriteGjensidige_S2000_BANSTA(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sMessageNo As String
        Dim bErrorsExists As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim sSenderID As String = ""
        Dim sReceiverID As String = ""

        Try

            bErrorsExists = False ' added 02.01.2008

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Only export statuscode  > 0. The others don't give any sense in BANSTA
                'If Val(oBabel.StatusCode) > 0 Then  ' changed 03.12.2007, SEB uses 00 as returncode for those not errormarked
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Only export statuscode  > 0. The others don't give any sense in BANSTA
                            'If Val(oPayment.StatusCode) > 2 Then
                            ' changed 02.01.2008, to be able to trap errors at invoicelevel
                            ' Changed again 15.01.2008

                            '16.05.2017 - Added the next IF
                            If IsNumeric(oPayment.StatusCode) Then
                                If Val(oPayment.StatusCode) > 2 Then
                                    bErrorsExists = True
                                    bExportoBabel = True
                                    Exit For
                                End If
                            Else
                                bErrorsExists = True
                                bExportoBabel = True
                            End If

                            For Each oInvoice In oPayment.Invoices
                                '16.05.2017 - Added the next IF
                                If IsNumeric(oInvoice.StatusCode) Then
                                    If Val(oInvoice.StatusCode) > 2 Then
                                        bErrorsExists = True
                                        Exit For
                                    End If
                                Else
                                    bErrorsExists = True
                                    Exit For
                                End If
                            Next oInvoice
                            If bErrorsExists Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            Else
                                ' Run BANSTA as own profile, must then exportmark other transactions
                                oPayment.Exported = True
                            End If 'If Val(opayment.StatusCode) > 2 Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch
                'Else
                '    For Each oBatch In oBabel.Batches
                '        For Each oPayment In oBatch.Payments
                '            oPayment.Exported = True
                '        Next oPayment
                '    Next oBatch
                'End If ''Only export statuscode  > 0. The others don't give any sense in BANSTA

                If bExportoBabel Then
                    sSenderID = ""
                    sReceiverID = ""
                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabel.Batches.Item(1).Payments.Item(1).VB_FilenameOut_ID Then
                            If Not EmptyString(oFilesetup.CompanyNo) Then
                                sReceiverID = oFilesetup.CompanyNo
                                sSenderID = oFilesetup.AdditionalNo
                                Exit For
                            End If
                        End If
                    Next oFilesetup

                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteGjensidige_000(oBabel, sSenderID, sReceiverID)
                        oFile.WriteLine((sLine))
                        If oBabel.Batches.Count > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object oBabel.Batches(1).Payments. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If oBabel.Batches(1).Payments.Count > 0 Then
                                'sMessageNo = Left$(oBabel.Batches(1).Payments(1).REF_Own, 6)
                                ' changed MeldingsID 16.11.2007
                                '21.02.2008
                                If EmptyString((oBabel.EDI_MessageNo)) Then
                                    sMessageNo = VB6.Format(Date.Today, "YYMMDD") & VB6.Format(TimeOfDay, "HHMM") & "0001"
                                Else
                                    sMessageNo = Trim(oBabel.EDI_MessageNo)
                                End If
                            End If
                        End If
                        sLine = WriteGjensidige_010(oBabel, "BANSTA", sMessageNo)
                        oFile.WriteLine((sLine))

                        bFileStartWritten = True
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            bErrorsExists = False
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Only export statuscode  > 0. The others don't give any sense in BANSTA
                                ' changed 15.01.2008
                                '16.05.2017 - Added the next IF
                                If IsNumeric(oPayment.StatusCode) Then
                                    If Val(oPayment.StatusCode) > 2 Then
                                        bErrorsExists = True
                                    End If
                                Else
                                    bErrorsExists = True
                                End If

                                For Each oInvoice In oPayment.Invoices
                                    '16.05.2017 - Added the next IF
                                    If IsNumeric(oInvoice.StatusCode) Then
                                        If Val(oInvoice.StatusCode) > 2 Then
                                            bErrorsExists = True
                                            Exit For
                                        End If
                                    Else
                                        bErrorsExists = True
                                        Exit For
                                    End If
                                Next oInvoice
                                If bErrorsExists Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                Else
                                    ' Run BANSTA as own profile, must then exportmark other transactions
                                    oPayment.Exported = True
                                End If 'If bErrorsExists Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                bErrorsExists = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Only export statuscode  > 0. The others don't give any sense in BANSTA
                                    ' changed 15.01.2008
                                    '16.05.2017 - Added the next IF
                                    If IsNumeric(oPayment.StatusCode) Then
                                        If Val(oPayment.StatusCode) > 2 Then
                                            bErrorsExists = True
                                        End If
                                    Else
                                        bErrorsExists = True
                                    End If
                                    ' changed 02.01.2008, to be able to trap errors at invoicelevel
                                    For Each oInvoice In oPayment.Invoices
                                        '16.05.2017 - Added the next IF
                                        If IsNumeric(oInvoice.StatusCode) Then
                                            If Val(oInvoice.StatusCode) > 2 Then
                                                bErrorsExists = True
                                                Exit For
                                            End If
                                        Else
                                            bErrorsExists = True
                                            Exit For
                                        End If
                                    Next oInvoice
                                    If bErrorsExists Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If bErrorsExists Then
                                End If

                                If bExportoPayment Then

                                    ' Next loop is called a 040-loop
                                    sLine = WriteGjensidige_040(oPayment)
                                    oFile.WriteLine((sLine))

                                    'sLine = WriteGjensidige_072(oPayment) '25.06.2012 - Old code
                                    sLine = WriteGjensidige_073(oPayment)
                                    oFile.WriteLine((sLine))

                                    oPayment.Exported = True

                                End If

                            Next oPayment ' payment

                        End If

                    Next oBatch 'batch


                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidige_S2000_BANSTA" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidige_S2000_BANSTA = True

    End Function
    Function WriteGjensidige_S2000_FINSTA(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim sOldAccountNo As String = ""

        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sMessageNo As String
        Dim bErrorsExists As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim sSenderID As String = ""
        Dim sReceiverID As String = ""
        Dim bWriteRecord010 As Boolean = False
        Dim sOldBalanceDate As String = ""
        Dim sOldBalanceAccount As String = ""

        Try

            bErrorsExists = False ' added 02.01.2008

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.VB_ClientNo = sClientNo Then
                                    If InStr(oPayment.REF_Own, "&?") Then
                                        'Set in the part of the OwnRef that BabelBank is using
                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                    Else
                                        sNewOwnref = ""
                                    End If
                                    If sNewOwnref = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    sSenderID = ""
                    sReceiverID = ""
                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabel.Batches.Item(1).Payments.Item(1).VB_FilenameOut_ID Then
                            If Not EmptyString(oFilesetup.CompanyNo) Then
                                sReceiverID = oFilesetup.CompanyNo
                                sSenderID = oFilesetup.AdditionalNo
                                Exit For
                            End If
                        End If
                    Next oFilesetup

                    If Not bFileStartWritten Then
                        If oBabel.Batches.Count > 0 Then
                            If oBabel.Batches(1).Payments.Count > 0 Then
                                'sMessageNo = Left$(oBabel.Batches(1).Payments(1).REF_Own, 6)
                                ' changed MeldingsID 16.11.2007
                                '21.02.2008
                                If EmptyString((oBabel.EDI_MessageNo)) Then
                                    sMessageNo = VB6.Format(Date.Today, "YYMMDD") & VB6.Format(TimeOfDay, "HHMM") & "0001"
                                Else
                                    sMessageNo = Trim(oBabel.EDI_MessageNo)
                                End If
                            End If
                        End If
                        'sLine = WriteGjensidige_010(oBabel, "KONTOU", sMessageNo)
                        'oFile.WriteLine((sLine))

                        bFileStartWritten = True
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            bErrorsExists = False
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then

                            oPayment = oBatch.Payments.Item(1)

                            If sOldBalanceDate <> oBatch.DATE_BalanceIN Then
                                bWriteRecord010 = True
                                sOldBalanceDate = oBatch.DATE_BalanceIN
                            End If
                            If sOldBalanceAccount <> oPayment.I_Account Then
                                bWriteRecord010 = True
                                sOldBalanceAccount = oPayment.I_Account
                            End If

                            If bWriteRecord010 Then
                                '06.02.2013 - Mats wants a 000-record before each 010-record.
                                sLine = WriteGjensidige_000(oBabel, sSenderID, sReceiverID)
                                oFile.WriteLine((sLine))
                                sLine = WriteGjensidige_010_FINSTA(oBabel, "KONTOUT", sMessageNo, oBatch.DATE_BalanceIN)
                                oFile.WriteLine((sLine))
                                bWriteRecord010 = False
                            End If

                            If oPayment.I_Account <> sOldAccountNo Then
                                'Write the balance
                                sOldAccountNo = oPayment.I_Account
                                sLine = WriteGjensidige_020_FINSTA(oBatch, oPayment.MON_AccountCurrency)
                                oFile.WriteLine((sLine))
                                sLine = WriteGjensidige_030(oBatch, oPayment)
                                oFile.WriteLine((sLine))
                            End If

                            'Write info from the batch-level
                            sLine = WriteGjensidige_040_FINSTA(oBatch, oPayment)
                            oFile.WriteLine((sLine))

                            For Each oPayment In oBatch.Payments
                                oPayment.Exported = True
                            Next oPayment

                        End If
                    Next oBatch 'batch

                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidige_S2000_FINSTA" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidige_S2000_FINSTA = True

    End Function
    Function WriteGjensidige_S2000_CONTRL(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sMessageNo As String
        Dim bErrorsExists As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim sSenderID As String = ""
        Dim sReceiverID As String = ""

        Try

            'New code 18.04.2012
            'Transform statuscode from standard Pain.002 to the codes used by Gjensidige (based on CONTRL)
            If oBabelFiles.Count > 0 Then
                '02.02.2017 - Added the format Pain002_Rejected to the IF below
                If oBabelFiles.Item(1).ImportFormat = BabelFiles.FileType.Pain002 Or oBabelFiles.Item(1).ImportFormat = BabelFiles.FileType.Pain002_Rejected Then
                    For Each oBabel In oBabelFiles
                        oBabel.EDI_MessageNo = oBabel.File_id 'Move message ID of the file beeing responded to to message_ID
                        If oBabel.StatusCode = "01" Or oBabel.StatusCode = "02" Then '02.02.2015 added statusCode = "01" because of change in importroutine
                            oBabel.StatusCode = "7"
                        Else
                            oBabel.StatusCode = "4"
                        End If
                        For Each oBatch In oBabel.Batches
                            If oBatch.StatusCode = "01" Or oBatch.StatusCode = "02" Then '02.02.2015 added statusCode = "01" because of change in importroutine
                                oBatch.StatusCode = "7"
                            Else
                                oBatch.StatusCode = "4"
                            End If
                            For Each oPayment In oBatch.Payments '02.02.2015 added statusCode = "01" because of change in importroutine
                                If oPayment.StatusCode = "01" Or oPayment.StatusCode = "02" Then
                                    oPayment.StatusCode = "7"
                                Else
                                    'oPayment.StatusCode = "4"
                                End If
                            Next oPayment
                        Next oBatch
                    Next oBabel
                End If
            End If

            bErrorsExists = False ' added 02.01.2008

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Only export statuscode  > 0. The others don't give any sense in CONTRL
                            'If Val(oPayment.StatusCode) > 2 Then
                            ' changed 02.01.2008, to be able to trap errors at invoicelevel
                            ' Changed again 15.01.2008
                            '12.02.2015 - added And oPayment.StatusCode <> "7" because of change in Pain.002 import
                            '06.03.2015 - Changed back
                            If Val(oPayment.StatusCode) > 2 Then 'And oPayment.StatusCode <> "7" Then
                                bErrorsExists = True
                                bExportoBabel = True
                                Exit For
                            End If
                            For Each oInvoice In oPayment.Invoices
                                '12.02.2015 - added And oPayment.StatusCode <> "7" because of change in Pain.002 import
                                If Val(oInvoice.StatusCode) > 2 Then 'And oPayment.StatusCode <> "7" Then
                                    bErrorsExists = True
                                    Exit For
                                End If
                            Next oInvoice
                            If bErrorsExists Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            Else
                                ' Run CONTRL as own profile, must then exportmark other transactions
                                oPayment.Exported = True
                            End If 'If Val(opayment.StatusCode) > 2 Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    sSenderID = ""
                    sReceiverID = ""
                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabel.Batches.Item(1).Payments.Item(1).VB_FilenameOut_ID Then
                            If Not EmptyString(oFilesetup.CompanyNo) Then
                                sReceiverID = oFilesetup.CompanyNo
                                sSenderID = oFilesetup.AdditionalNo
                                Exit For
                            End If
                        End If
                    Next oFilesetup

                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteGjensidige_000(oBabel, sSenderID, sReceiverID)
                        oFile.WriteLine((sLine))

                        bFileStartWritten = True
                    End If

                    sLine = WriteGjensidige_001(oBabel, sSenderID, sReceiverID)
                    oFile.WriteLine((sLine))
                    sLine = WriteGjensidige_010(oBabel, "CONTRL", "")
                    oFile.WriteLine((sLine))

                    'Mark the 'dummy'-payment as exported
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch

                    '25.02.2008 - No need to traverse through the batch-object, because this information is not used
                    ''''        'Loop through all Batch objs. in BabelFile obj.
                    ''''        For Each oBatch In oBabel.Batches
                    ''''            bExportoBatch = False
                    ''''            'Have to go through the payment-object to see if we have objects thatt shall
                    ''''            ' be exported to this exportfile
                    ''''            For Each oPayment In oBatch.Payments
                    ''''                bErrorsExists = False
                    ''''                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                    ''''                    'Only export statuscode  > 0. The others don't give any sense in CONTRL
                    ''''                    ' changed 15.01.2008
                    ''''                    If Val(oPayment.StatusCode) > 2 Then
                    ''''                        bErrorsExists = True
                    ''''                    End If
                    ''''                    For Each oInvoice In oPayment.Invoices
                    ''''                        If Val(oInvoice.StatusCode) > 2 Then
                    ''''                            bErrorsExists = True
                    ''''                            Exit For
                    ''''                        End If
                    ''''                    Next oInvoice
                    ''''                    If bErrorsExists Then
                    ''''                        If Not bMultiFiles Then
                    ''''                            bExportoBatch = True
                    ''''                        Else
                    ''''                            If oPayment.VB_ClientNo = sClientNo Then
                    ''''                                If InStr(oPayment.REF_Own, "&?") Then
                    ''''                                    'Set in the part of the OwnRef that BabelBank is using
                    ''''                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                    ''''                                Else
                    ''''                                    sNewOwnref = ""
                    ''''                                End If
                    ''''                                If sNewOwnref = sOwnRef Then
                    ''''                                    bExportoBatch = True
                    ''''                                End If
                    ''''                            End If
                    ''''                        End If
                    ''''                    Else
                    ''''                        ' Run CONTRL as own profile, must then exportmark other transactions
                    ''''                        oPayment.Exported = True
                    ''''                    End If 'If bErrorsExists Then
                    ''''                End If
                    ''''                'If true exit the loop, and we have data to export
                    ''''                If bExportoBatch Then
                    ''''                    Exit For
                    ''''                End If
                    ''''            Next
                    ''''
                    ''''
                    ''''            If bExportoBatch Then
                    ''''
                    ''''                For Each oPayment In oBatch.Payments
                    ''''                    bExportoPayment = False
                    ''''                    bErrorsExists = False
                    ''''                    'Have to go through the payment-object to see if we have objects thatt shall
                    ''''                    ' be exported to this exportfile
                    ''''                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                    ''''                        'Only export statuscode  > 0. The others don't give any sense in CONTRL
                    ''''                        ' changed 15.01.2008
                    ''''                        If Val(oPayment.StatusCode) > 2 Then
                    ''''                            bErrorsExists = True
                    ''''                        End If
                    ''''                        ' changed 02.01.2008, to be able to trap errors at invoicelevel
                    ''''                        For Each oInvoice In oPayment.Invoices
                    ''''                            If Val(oInvoice.StatusCode) > 2 Then
                    ''''                                bErrorsExists = True
                    ''''                                Exit For
                    ''''                            End If
                    ''''                        Next oInvoice
                    ''''                        If bErrorsExists Then
                    ''''                            If Not bMultiFiles Then
                    ''''                                bExportoPayment = True
                    ''''                            Else
                    ''''                                If oPayment.VB_ClientNo = sClientNo Then
                    ''''                                    If InStr(oPayment.REF_Own, "&?") Then
                    ''''                                        'Set in the part of the OwnRef that BabelBank is using
                    ''''                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                    ''''                                    Else
                    ''''                                        sNewOwnref = ""
                    ''''                                    End If
                    ''''                                    If sNewOwnref = sOwnRef Then
                    ''''                                        bExportoPayment = True
                    ''''                                    End If
                    ''''                                End If
                    ''''                            End If
                    ''''                        Else
                    ''''                            oPayment.Exported = True
                    ''''                        End If 'If bErrorsExists Then
                    ''''                    End If
                    ''''
                    ''''                    If bExportoPayment Then
                    ''''
                    ''''                        ' Next loop is called a 040-loop
                    ''''                        sLine = WriteGjensidige_040(oPayment)
                    ''''                        oFile.WriteLine (sLine)
                    ''''
                    ''''                        sLine = WriteGjensidige_072(oPayment)
                    ''''                        oFile.WriteLine (sLine)
                    ''''
                    ''''                        oPayment.Exported = True
                    ''''
                    ''''                    End If
                    ''''
                    ''''                Next ' payment
                    ''''
                    ''''            End If
                    ''''
                    ''''        Next 'batch


                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidige_S2000_CONTRL" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidige_S2000_CONTRL = True

    End Function
    Function WriteGjensidige_S2000_CREMUL(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef sOwnRef As String, ByRef bLog As Boolean, ByRef oBabelLog As vbLog.vbLogging, ByRef sSpecial As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice, oInvoice2 As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim sOldDate, sOldAccount, sOldCurrency As String
        Dim nAmount As Double
        Dim sMessageNo As String
        Dim lFreetextCounter As Integer
        Dim sTemp As String
        Dim iCounter, iAddressFieldUsed As Short
        Dim bHeaderRecordsWritten, bBatchRecordsWritten As Boolean
        Dim bUsePaymentInfo As Boolean
        Dim bUseKID As Boolean
        Dim bAddSpecialText As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim sSenderID As String = ""
        Dim sReceiverID As String = ""

        Try

            bHeaderRecordsWritten = False
            bBatchRecordsWritten = False

            ' added logging 10.10.2008
            ' test if we can open file
            'If sSpecial = "GJENSIDIGE_TEST" Then
            '    Dim sTmp As String
            '    oBabelLog.Heading = "BabelBank info: "
            '    oBabelLog.AddLogEvent "I WriteGjensidige_S2000_CREMUL, F�r OpenTextFile" & sTmp, 8
            '
            '    ' 10.10.2008 tester om vi har tilgang til disken vi skal skrive til
            '
            '    sTmp = sFilenameOut
            '    ' ta vekk siste del
            '    sTmp = Left$(sTmp, InStrRev(sTmp, "\") - 1)
            '    If Not oFs.FolderExists(sTmp) Then
            '    '''If Dir(sTmp) = "" Then
            '        oBabelLog.Heading = "BabelBank info: "
            '        oBabelLog.AddLogEvent "I WriteGjensidige_S2000_CREMUL, F�r OpenTextFile - Finner ikke " & sTmp, 8
            '    End If
            '    ' Sjekk ogs� om filen finnes fra f�r;
            '    If oFs.FileExists(sFilenameOut) Then
            '        oBabelLog.Heading = "BabelBank info: "
            '        oBabelLog.AddLogEvent "I WriteGjensidige_S2000_CREMUL, Filen finnes fra f�r " & sFilenameOut, 8
            '    End If
            'End If

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled
                            If oPayment.Cancel = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    sSenderID = ""
                    sReceiverID = ""
                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabel.Batches.Item(1).Payments.Item(1).VB_FilenameOut_ID Then
                            If Not EmptyString(oFilesetup.CompanyNo) Then
                                sReceiverID = oFilesetup.CompanyNo
                                sSenderID = oFilesetup.AdditionalNo
                                Exit For
                            End If
                        End If
                    Next oFilesetup

                    'Old code, changed 10.03.2008

                    '        If Not bFileStartWritten Then
                    '            ' write only once!
                    '            ' When merging Client-files, skip this one except for first client!
                    '            sLine = WriteGjensidige_000(oBabel, "938741700")
                    '            oFile.WriteLine (sLine)
                    ''            If oBabel.Batches.Count > 0 Then
                    ''                If oBabel.Batches(1).Payments.Count > 0 Then
                    ''                    sMessageNo = oBabel.Batches(1).REF_Own
                    ''                End If
                    ''            End If
                    '            '21.02.2008
                    '            If EmptyString(oBabel.EDI_MessageNo) Then
                    '                sMessageNo =VB6.Format(Date, "YYMMDD") &VB6.Format(Time, "HHMM") & "0001"
                    '            Else
                    '                sMessageNo = Trim$(oBabel.EDI_MessageNo)
                    '            End If
                    '            sLine = WriteGjensidige_010(oBabel, "CREMUL", sMessageNo)
                    '            oFile.WriteLine (sLine)
                    '
                    '            bFileStartWritten = True
                    '        End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                ' Write 020, 021, 030-records for each batch
                                ' Called a 020-loop in S2000 CREMUL

                                'Old code 10.03.2008
                                '                    sLine = WriteGjensidige_CREMUL_020(oBatch, oBatch.Payments(1), oBatch.MON_InvoiceAmount)
                                '                    oFile.WriteLine (sLine)
                                '                    sLine = WriteGjensidige_021(oBatch, oBatch.Payments(1), "CREMUL")
                                '                    oFile.WriteLine (sLine)
                                '                    sLine = WriteGjensidige_030(oBatch, oBatch.Payments(1))
                                '                    oFile.WriteLine (sLine)

                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            bBatchRecordsWritten = False
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects that shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then

                                    'New code 10.03.2008
                                    If oPayment.I_Account <> sOldAccount Then
                                        bHeaderRecordsWritten = False
                                        sOldAccount = oPayment.I_Account
                                    End If

                                    If Not bHeaderRecordsWritten Then
                                        'Also changed 10.03.2008
                                        sLine = WriteGjensidige_000(oBabel, sSenderID, sReceiverID)
                                        'sLine = WriteGjensidige_000(oBabel)
                                        'Old code
                                        'sLine = WriteGjensidige_000(oBabel, "938741700")
                                        oFile.WriteLine((sLine))

                                        sMessageNo = oBatch.REF_Own
                                        '21.02.2008
                                        If EmptyString(sMessageNo) Then
                                            sMessageNo = VB6.Format(Date.Today, "YYMMDD") & VB6.Format(TimeOfDay, "HHMM") & "0001"
                                        End If
                                        sLine = WriteGjensidige_010(oBabel, "CREMUL", sMessageNo)
                                        oFile.WriteLine((sLine))
                                        bHeaderRecordsWritten = True
                                    End If

                                    If Not bBatchRecordsWritten Then
                                        'Reset at the batchlevel
                                        ' Write 020, 021, 030-records for each batch
                                        ' Called a 020-loop in S2000 CREMUL
                                        sLine = WriteGjensidige_CREMUL_020(oBatch, oPayment, (oBatch.MON_InvoiceAmount))
                                        oFile.WriteLine((sLine))
                                        sLine = WriteGjensidige_021(oBatch, oPayment, "CREMUL")
                                        oFile.WriteLine((sLine))
                                        sLine = WriteGjensidige_030(oBatch, oPayment)
                                        oFile.WriteLine((sLine))
                                        bBatchRecordsWritten = True
                                    End If

                                    'End new code
                                    '08.07.2008 - First analyze if we shall use paymentinformation
                                    ' or invoiceinformation (retrieved a KID)
                                    bUsePaymentInfo = True
                                    bUseKID = False
                                    bAddSpecialText = False
                                    If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "801" Then '16.06.2021 added oPayment.PayCode = "801"
                                        bUsePaymentInfo = True
                                        bUseKID = True
                                    Else
                                        If oPayment.Invoices.Count > 1 Then
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Matched Then
                                                    If Not EmptyString((oInvoice.Unique_Id)) Then
                                                        '21.22.2012 - Added next IF first part is new (<0)
                                                        If oInvoice.MON_InvoiceAmount < 0 Then
                                                            bUsePaymentInfo = True
                                                            bUseKID = False
                                                            bAddSpecialText = False
                                                            Exit For
                                                        Else
                                                            bUsePaymentInfo = False
                                                            bUseKID = True
                                                            bAddSpecialText = True
                                                        End If
                                                    End If
                                                End If
                                            Next oInvoice
                                        ElseIf oPayment.Invoices.Count > 0 Then
                                            bUsePaymentInfo = True
                                            If oPayment.Invoices.Item(1).MATCH_Matched Then
                                                If Not EmptyString((oPayment.Invoices.Item(1).Unique_Id)) Then
                                                    bUseKID = True
                                                    If oPayment.Invoices.Item(1).Freetexts.Count > 0 Then
                                                        bUsePaymentInfo = True
                                                    Else
                                                        bAddSpecialText = True
                                                    End If
                                                Else
                                                    bUseKID = False
                                                End If
                                            Else
                                                bUseKID = False
                                            End If
                                        Else
                                            bUsePaymentInfo = True
                                            bUseKID = False
                                        End If
                                    End If

                                    For Each oInvoice In oPayment.Invoices

                                        ' Next loop is called a 040-loop

                                        ' XXX EMIL - er ref i 040 viktig. vi skiller ikke p� om vi bruker ABO eller ikke
                                        sLine = WriteGjensidige_CREMUL_040(oBatch, oPayment)
                                        oFile.WriteLine((sLine))

                                        ' Bank Referencerecord
                                        sLine = WriteGjensidige_041(oPayment)
                                        oFile.WriteLine((sLine))

                                        'If oPayment.PayType = "I" Then
                                        '    ' For International, write 3 different payment-records ????Always 3 ?????
                                        '    sLine = WriteGjensidige_042_36(oPayment)
                                        '    oFile.WriteLine (sLine)
                                        '    sLine = WriteGjensidige_042_60(oPayment)
                                        '    oFile.WriteLine (sLine)
                                        '    sLine = WriteGjensidige_042_98(oPayment)
                                        '    oFile.WriteLine (sLine)
                                        'Else
                                        '    ' For domestic write one 042_98  ??????
                                        '    sLine = WriteGjensidige_042_98(oPayment)
                                        '    oFile.WriteLine (sLine)
                                        'End If

                                        'sLine = WriteGjensidige_CREMUL_042_98(oPayment)
                                        'oFile.WriteLine (sLine)

                                        If bUsePaymentInfo Then
                                            If oPayment.MON_OriginallyPaidAmount <> 0 Then
                                                sLine = WriteGjensidige_CREMUL_042_119(oPayment)
                                                oFile.WriteLine((sLine))
                                            End If
                                        End If

                                        If bUsePaymentInfo Then
                                            sLine = WriteGjensidige_042_60(oPayment)
                                            oFile.WriteLine((sLine))
                                        Else
                                            sLine = WriteGjensidige_042_60(oPayment, (oInvoice.MON_InvoiceAmount))
                                            oFile.WriteLine((sLine))
                                        End If

                                        ' Bank ordrenr (oPayment.REF_Bank2, AEK)
                                        sLine = WriteGjensidige_043(oPayment)
                                        oFile.WriteLine((sLine))

                                        sLine = WriteGjensidige_050(oPayment)
                                        oFile.WriteLine((sLine))

                                        ' XXXX EMIL - i f�lge Cremul dok er det 055 og 056 som brukes
                                        ' for bankswift og banknavn, ikke 052 og 053 som for de andre
                                        ' hva er riktig, og brukes de ????

                                        ' I f�lge Mats s� er ikke 052 og 053 med
                                        'If oPayment.PayType = "I" Then
                                        '    sLine = WriteGjensidige_052(oPayment)
                                        '    oFile.WriteLine (sLine)
                                        '    sLine = WriteGjensidige_053(oPayment)
                                        '    oFile.WriteLine (sLine)
                                        'End If

                                        ' Vi leser f�rst fra Group 14 NAD ("strukturert" navn), men hvis dette
                                        ' ikke finnes, bruker vi "bbs_Navn", fra Group15, FTX. Det er dette som skiller
                                        ' 060-records og 071-records i S2000 Cremul.
                                        ' Skal vi velge � bruke kun 071 ???

                                        ' Namerecords, 60-62
                                        ' 25.02.2008 - Sometimes the zipcode is a part of a genral address line
                                        ' Try to find it and move the content to zip/city
                                        sTemp = ""
                                        iAddressFieldUsed = 0
                                        If EmptyString((oPayment.E_Zip)) Then
                                            If Not EmptyString((oPayment.E_Adr3)) Then
                                                sTemp = Trim(oPayment.E_Adr3)
                                                iAddressFieldUsed = 3
                                            End If
                                            If EmptyString(sTemp) Then
                                                If Not EmptyString((oPayment.E_Adr2)) Then
                                                    sTemp = Trim(oPayment.E_Adr2)
                                                    iAddressFieldUsed = 2
                                                End If
                                            End If
                                            If EmptyString(sTemp) Then
                                                If Not EmptyString((oPayment.E_Adr1)) Then
                                                    sTemp = Trim(oPayment.E_Adr1)
                                                    iAddressFieldUsed = 1
                                                End If
                                            End If
                                            If Not EmptyString(sTemp) Then
                                                If vbIsNumeric(Left(sTemp, 5), "01234567890 ") Then
                                                    'OK move the content to zip and city
                                                    For iCounter = 1 To 6
                                                        If Not vbIsNumeric(Mid(sTemp, iCounter, 1), "01234567890 ") Then
                                                            Exit For
                                                        End If
                                                    Next iCounter
                                                    oPayment.E_Zip = Replace(Left(sTemp, iCounter - 1), " ", "")
                                                    oPayment.E_City = Trim(Mid(sTemp, iCounter))
                                                    If iAddressFieldUsed = 1 Then
                                                        oPayment.E_Adr1 = ""
                                                    ElseIf iAddressFieldUsed = 2 Then
                                                        oPayment.E_Adr2 = ""
                                                    ElseIf iAddressFieldUsed = 3 Then
                                                        oPayment.E_Adr3 = ""
                                                    End If
                                                Else
                                                    'Nothing to do
                                                End If
                                            End If
                                        End If
                                        lFreetextCounter = 0
                                        '25.02.2008 - New IF
                                        If Not EmptyString((oPayment.E_Zip)) Then
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                sLine = WriteGjensidige_060((oPayment.E_Name))
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString(oPayment.E_Adr1 & oPayment.E_Adr2 & oPayment.E_Adr3) Then
                                                sLine = WriteGjensidige_061(oPayment)
                                                oFile.WriteLine((sLine))
                                            End If
                                            If Not EmptyString(oPayment.E_Zip & oPayment.E_City & oPayment.E_CountryCode) Then
                                                sLine = WriteGjensidige_062(oPayment)
                                                oFile.WriteLine((sLine))
                                            End If
                                        Else
                                            '25.02.2008 - Write address info as freetext
                                            If Not EmptyString((oPayment.E_Name)) Then
                                                sLine = WriteGjensidige_071((oPayment.E_Name))
                                                oFile.WriteLine((sLine))
                                                lFreetextCounter = lFreetextCounter + 1
                                            End If
                                            If Not EmptyString((oPayment.E_Adr1)) Then
                                                sLine = WriteGjensidige_071((oPayment.E_Adr1))
                                                oFile.WriteLine((sLine))
                                                lFreetextCounter = lFreetextCounter + 1
                                            End If
                                            If Not EmptyString((oPayment.E_Adr2)) Then
                                                sLine = WriteGjensidige_071((oPayment.E_Adr2))
                                                oFile.WriteLine((sLine))
                                                lFreetextCounter = lFreetextCounter + 1
                                            End If
                                            If Not EmptyString((oPayment.E_Adr3)) Then
                                                sLine = WriteGjensidige_071((oPayment.E_Adr3))
                                                oFile.WriteLine((sLine))
                                                lFreetextCounter = lFreetextCounter + 1
                                            End If
                                        End If

                                        'Freetext 71
                                        If bUsePaymentInfo Then
                                            For Each oInvoice2 In oPayment.Invoices
                                                For Each oFreeText In oInvoice2.Freetexts
                                                    If Not EmptyString((oFreeText.Text)) Then
                                                        lFreetextCounter = lFreetextCounter + 1
                                                        sLine = WriteGjensidige_071((oFreeText.Text))
                                                        oFile.WriteLine((sLine))
                                                        If lFreetextCounter > 20 Then
                                                            Exit For
                                                        End If
                                                    End If
                                                Next oFreeText
                                                If lFreetextCounter > 20 Then
                                                    Exit For
                                                End If
                                            Next oInvoice2
                                        Else
                                            For Each oFreeText In oInvoice.Freetexts
                                                If Not EmptyString((oFreeText.Text)) Then
                                                    lFreetextCounter = lFreetextCounter + 1
                                                    sLine = WriteGjensidige_071((oFreeText.Text))
                                                    oFile.WriteLine((sLine))
                                                    If lFreetextCounter > 20 Then
                                                        Exit For
                                                    End If
                                                End If
                                            Next oFreeText
                                            sLine = PadRight("071", 73, " ")
                                            oFile.WriteLine((sLine))
                                            sLine = PadRight("071Totalbel�p p� innbetaling: " & ConvertFromAmountToString((oPayment.MON_InvoiceAmount), , ","), 73, " ")
                                            oFile.WriteLine((sLine))
                                            sLine = PadRight("071", 73, " ")
                                            oFile.WriteLine((sLine))
                                            For Each oInvoice2 In oPayment.Invoices
                                                For Each oFreeText In oInvoice2.Freetexts
                                                    If Not EmptyString((oFreeText.Text)) Then
                                                        lFreetextCounter = lFreetextCounter + 1
                                                        sLine = WriteGjensidige_071((oFreeText.Text))
                                                        oFile.WriteLine((sLine))
                                                        If lFreetextCounter > 20 Then
                                                            Exit For
                                                        End If
                                                    End If
                                                Next oFreeText
                                                If lFreetextCounter > 20 Then
                                                    Exit For
                                                End If
                                            Next oInvoice2
                                        End If

                                        'Old code
                                        'If IsOCR(oPayment.PayCode) Then
                                        'New code 09.07.2008
                                        If bUseKID Then
                                            ' write a 080 record with KID
                                            If Not EmptyString((oInvoice.Unique_Id)) Then
                                                If bAddSpecialText = True Then
                                                    sLine = PadRight("071Scannet etter KID", 73, " ")
                                                    oFile.WriteLine((sLine))
                                                End If

                                                sLine = WriteGjensidige_080(oInvoice, (oInvoice.MyField))
                                                oFile.WriteLine((sLine))
                                            End If
                                        End If

                                        ' XXXX EMIL, skal 083-record, bel�p, skrives en gang pr
                                        ' invoice, innenfor en payment?
                                        If bUsePaymentInfo Then
                                            For Each oInvoice2 In oPayment.Invoices
                                                ' write a 083 record with invoiceamount
                                                sLine = WriteGjensidige_083(oInvoice2)
                                                oFile.WriteLine((sLine))
                                            Next oInvoice2
                                        Else
                                            sLine = WriteGjensidige_083(oInvoice)
                                            oFile.WriteLine((sLine))
                                        End If

                                        If bUsePaymentInfo Then
                                            Exit For
                                        End If

                                    Next oInvoice

                                    oPayment.Exported = True

                                End If

                            Next oPayment ' payment

                        End If

                    Next oBatch 'batch


                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidige_S2000_CREMUL" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidige_S2000_CREMUL = True

    End Function

    Function WriteGjensidige_000(ByRef oBabelFile As BabelFile, Optional ByRef sIdentSender As String = "", Optional ByRef sIdentReceiver As String = "") As String
        ' 00000088114 971049944
        Dim sLine As String

        sLine = "000" ' RecType 1-3
        If EmptyString(sIdentReceiver) Then
            '07.03.2013 - Changed next line to PadRight after mail from Mats
            sLine = sLine & PadRight(Mid(oBabelFile.IDENT_Receiver, 1, 9), 9, " ") ' for CREMUL, 938741700
            'Old code
            'sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Receiver, 1, 9), 9, " ") ' for CREMUL, 938741700
        Else
            '25.02.2008 - Changed, to use all 9 positions, because of CONTRL
            '07.03.2013 - Changed next line to PadRight after mail from Mats
            sLine = sLine & PadRight(sIdentReceiver, 9, " ") '  dataavsender 4-12
            'Old code
            'sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") & " " ' dataavsender 4-12
        End If
        If EmptyString(sIdentSender) Then
            sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 9), 9, " ") ' for CREMUL, 938741700
        Else
            '25.02.2008 - Changed, to use all 9 positions, because of CONTRL
            sLine = sLine & PadLeft(sIdentSender, 9, " ") ' dataavsender 4-12
            'Old code
            'sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") & " " ' dataavsender 4-12
        End If


        'Old code, sIdentSender was then a parameter with a fixed ID
        ' returfil fra bank/BBS
        'If Not EmptyString(sIdentSender) Then
        '    sLine = sLine & PadRight(sIdentSender, 9, " ")      ' for CREMUL, 938741700
        'ElseIf EmptyString(oBabelFile.IDENT_Sender) Then
        '    sLine = sLine & "00088114 "
        'Else
        '    '25.02.2008 - Changed, to use all 9 positions, because of CONTRL
        '    sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 9), 9, "0") ' dataavsender 4-12
        '    'Old code
        '    'sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") & " " ' dataavsender 4-12
        'End If
        'sLine = sLine & "971049944"                                         ' Datamottaker 13-21
        sLine = sLine & Space(5) ' Avgivende system, 22-26, not in use
        WriteGjensidige_000 = sLine

    End Function
    Function WriteGjensidige_001(ByRef oBabelFile As BabelFile, Optional ByRef sIdentSender As String = "", Optional ByRef sIdentReceiver As String = "") As String 'Used in CONTRL
        '00100088114 00008080 S207082901    7
        Dim sLine As String

        sLine = "001" ' RecType 1-3
        If EmptyString(sIdentSender) Then
            sLine = sLine & PadRight(oBabelFile.IDENT_Sender, 9, " ") ' Utvekslingsavsender 4-12
        Else
            sLine = sLine & PadRight(sIdentSender, 9, " ") ' Utvekslingsavsender 4-12
        End If
        If EmptyString(sIdentReceiver) Then
            sLine = sLine & PadRight(oBabelFile.IDENT_Receiver, 9, " ") ' EDI-avsender 13-21
        Else
            sLine = sLine & PadRight(sIdentReceiver, 9, " ") ' EDI-avsender 13-21
        End If
        sLine = sLine & PadRight(oBabelFile.EDI_MessageNo, 14, " ") ' Utvekslingsmottaker? 22-35
        sLine = sLine & PadRight(oBabelFile.StatusCode, 2, " ") ' Statuskode, 36-37
        WriteGjensidige_001 = sLine

    End Function
    Function WriteGjensidige_010(ByRef oBabelFile As BabelFile, ByRef sDocumentType As String, ByRef sMessageNo As String) As String
        ' 010DEBMUL07082813000001                     20070828
        Dim sLine As String

        sLine = "010" ' RecType, 1-3
        sLine = sLine & PadRight(sDocumentType, 6, " ") ' Dokumenttype, 4-9
        If sDocumentType <> "CONTRL" Then
            ' M� gj�re noe for � fylle opp EDI_Messageno n�r vi leser Telepay
            sLine = sLine & PadRight(sMessageNo, 35, " ") ' Meldingsnr, 10-44
            If oBabelFile.DATE_Production = "19900101" Then
                sLine = sLine & VB6.Format(Date.Today, "YYYYMMDD")
            Else
                sLine = sLine & oBabelFile.DATE_Production '????????????? - fylles ikke inn for Telepay!                         ' Meldingsdato, 45-52
            End If

            If sDocumentType = "CREMUL" Then
                sLine = sLine & oBabelFile.EDI_MessageNo ' Forsendelsesnr, 53-68
                sLine = sLine & oBabelFile.DATE_Production ' Meldingsdato, 69-76
            End If
        Else
            sLine = PadRight(sLine, 52, " ")
        End If
        WriteGjensidige_010 = sLine

    End Function
    Function WriteGjensidige_010_FINSTA(ByRef oBabelFile As BabelFile, ByRef sDocumentType As String, ByRef sMessageNo As String, Optional ByVal sBalanceDate As String = "") As String
        ' 010DEBMUL07082813000001                     20070828
        Dim sLine As String

        sLine = "010" ' RecType, 1-3
        sLine = sLine & PadRight(sDocumentType, 6, " ") ' Dokumenttype, 4-9
        If oBabelFile.DATE_Production = "19900101" Then
            sLine = sLine & VB6.Format(Date.Today, "YYMMDD")
        Else
            sLine = sLine & Mid(oBabelFile.DATE_Production, 3) '????????????? - fylles ikke inn for Telepay!                         ' Meldingsdato, 45-52
        End If
        sLine = sLine & Space(35) '10-44
        If sBalanceDate = "19900101" Then
            sLine = sLine & VB6.Format(Date.Today, "YYYYMMDD")
        Else
            sLine = sLine & sBalanceDate '' Meldingsdato, 45-52
        End If

        WriteGjensidige_010_FINSTA = sLine

    End Function
    Function WriteGjensidige_020(ByRef oBatch As Batch, ByRef oPayment As Payment, ByRef nAmount As Double) As String
        ' 020        0708280202                            851,06            NOK
        Dim sLine As String

        sLine = "020" ' RecType, 1-3
        sLine = sLine & Space(8) ' Effektueringsdato, not in use, 4-11
        sLine = sLine & PadRight(Left(oPayment.REF_Own, 10), 35, " ") ' Oppdrags ID, utledet av de 10 f�rste i opayment.ref_own, 12-46
        sLine = sLine & Space(3) ' Type betaling, 47-49, not in use
        'sLine = sLine & PadRight(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), 18, " ") ' Betalingsbel�p, venstrejustert, 50-67
        sLine = sLine & PadRight(ConvertFromAmountToString(nAmount, , ","), 18, " ") ' Betalingsbel�p, venstrejustert, 50-67
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") ' Bel�psvaluta, 68-70
        sLine = sLine & Space(3) ' Fakturavaluta, 71-73
        sLine = sLine & Space(3) ' Betalingstype, 74-76, not in use
        sLine = sLine & Space(3) ' Betalingsvaluta, 77-79

        WriteGjensidige_020 = sLine

    End Function
    Function WriteGjensidige_020_FINSTA(ByRef oBatch As Batch, ByRef sCurrency As String) As String
        ' 020        0708280202                            851,06            NOK
        Dim sLine As String

        sLine = "020" ' RecType, 1-3
        sLine = sLine & Space(18) ' Not in use, 4-21
        sLine = sLine & PadRight(ConvertFromAmountToString(oBatch.MON_BalanceIN, , ","), 18, " ") ' Startsaldo, venstrejustert, 22-39
        sLine = sLine & Space(10) ' Not in use, 40-49
        sLine = sLine & PadRight(ConvertFromAmountToString(oBatch.MON_BalanceOUT, , ","), 18, " ") ' Startsaldo, venstrejustert, 50-67
        sLine = sLine & sCurrency 'Valutakode, 68-70

        WriteGjensidige_020_FINSTA = sLine

    End Function
    Function WriteGjensidige_040_FINSTA(ByRef oBatch As Batch, ByRef oPayment As vbBabel.Payment) As String
        ' 020        0708280202                            851,06            NOK
        Dim sLine As String

        sLine = "040" ' RecType, 1-3
        sLine = sLine & PadRight(ConvertFromAmountToString(oPayment.MON_AccountAmount, , ","), 18, " ") ' Startsaldo, venstrejustert, 4-21
        If oPayment.PayType = "I" Then
            If Not EmptyString(oPayment.REF_Own) Then
                sLine = sLine & PadRight(oPayment.REF_Own, 14, " ") 'Reference, 22-35
            Else
                sLine = sLine & Mid(oPayment.DATE_Payment, 3) 'Bokf�ringsdat, 22-27
                sLine = sLine & Space(8) ' Not in use, 28-35
            End If
        Else
            sLine = sLine & Mid(oPayment.DATE_Payment, 3) 'Bokf�ringsdat, 22-27
            sLine = sLine & Space(8) ' Not in use, 28-35
        End If
        sLine = sLine & oPayment.MON_AccountCurrency 'Valutakode, 36-38
        sLine = sLine & Space(26) ' Not in use, 39-64
        'sLine = sLine & PadRight(oBatch.REF_Bank, 36, " ") '65-100
        sLine = sLine & PadRight(oPayment.REF_Own, 36, " ") '65-100 '27.02.2013 - Changed according to mail from Mats

        WriteGjensidige_040_FINSTA = sLine

    End Function
    Function WriteGjensidige_CREMUL_020(ByRef oBatch As Batch, ByRef oPayment As Payment, ByRef nAmount As Double) As String
        ' 020                                           230        9588071,57
        Dim sLine As String

        ' XXXXXXXXXX EMIL Hvordan BUNTER vi batcher - er det en pr valuta, pr konto?
        ' XXXXXXXXXX EMIL Er det en pr betalingstype ? (230, 233, 234 ???)

        sLine = "020" ' RecType, 1-3
        sLine = sLine & Space(8) ' Effektueringsdato, not in use, 4-11
        sLine = sLine & Space(35) ' Oppdrags ID, ikke i bruk
        If oPayment.PayCode = "801" Then
            sLine = sLine & "230"
        Else
            sLine = sLine & bbGetPayCode((oPayment.PayCode), "CREMUL") ' Type betaling, 47-49, 230, 233, 234 etc
        End If
        sLine = sLine & PadLeft(ConvertFromAmountToString(nAmount, , ","), 18, " ") ' Betalingsbel�p, venstrejustert, 50-67
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") ' Bel�psvaluta, 68-70  ???? Ikke dokumentert for CREMUL
        sLine = sLine & Space(3) ' Fakturavaluta, 71-73
        sLine = sLine & Space(3) ' Betalingstype, 74-76, not in use
        sLine = sLine & Space(3) ' Betalingsvaluta, 77-79

        WriteGjensidige_CREMUL_020 = sLine

    End Function
    Function WriteGjensidige_021(ByRef oBatch As Batch, ByRef oPayment As Payment, Optional ByRef sDocumentType As String = "") As String
        ' 02120070828        IN                      0708280202
        Dim sLine As String

        sLine = "021" ' RecType, 1-3
        sLine = sLine & oPayment.DATE_Payment ' Bokf�ringsdato, 4-11
        ' XXXXXXX EMIL In use for CREMUL-only, but it probably doesn't matter if it is present also for the other formats
        sLine = sLine & oPayment.DATE_Value ' Valuteringsdato, 12-19, (not in use)
        If oPayment.PayType = "I" Then
            sLine = sLine & "IN " ' Type betaling, IN eller DO, 20-22
        Else
            sLine = sLine & "DO " ' Type betaling, IN eller DO, 20-22
        End If
        sLine = sLine & Space(21) ' filer_21, 23-43
        If sDocumentType = "CREMUL" Then
            '01.03.2013 - Added next IF. Mats wants to cahnge the reference stated in record021 with the one stated in record040 for CREMUL, incoming international payments 
            If oPayment.ImportFormat = vbBabel.BabelFiles.FileType.Camt054_Incoming And oPayment.PayType = "I" Then
                sLine = sLine & PadRight(Trim(oPayment.REF_Bank1), 35, " ") ' Bankref, egentlig egenref som i 020, 44-78
            Else
                If EmptyString(oBatch.REF_Own) Then
                    sLine = sLine & PadRight(Trim(oBatch.REF_Bank), 35, " ") ' Bankref, egentlig egenref som i 020, 44-78
                Else
                    sLine = sLine & PadRight(Trim(oBatch.REF_Own), 35, " ") ' Bankref, egentlig egenref som i 020, 44-78
                End If
            End If
            'Old code
            'If EmptyString(oBatch.REF_Own) Then
            '    sLine = sLine & PadRight(Trim(oBatch.REF_Bank), 35, " ") ' Bankref, egentlig egenref som i 020, 44-78
            'Else
            '    sLine = sLine & PadRight(Trim(oBatch.REF_Own), 35, " ") ' Bankref, egentlig egenref som i 020, 44-78
            'End If
        Else
            sLine = sLine & PadRight(Left(oPayment.REF_Own, 10), 35, " ") ' Bankref, egentlig egenref som i 020, 44-78
        End If
        sLine = sLine & Space(16) ' Filler_16, 79-94

        WriteGjensidige_021 = sLine

    End Function
    Function WriteGjensidige_030(ByRef oBatch As Batch, ByRef oPayment As Payment) As String
        ' 03097500707864
        Dim sLine As String

        sLine = "030" ' RecType, 1-3
        sLine = sLine & PadRight(oPayment.I_Account, 35, " ") ' GF konto, 4-38

        WriteGjensidige_030 = sLine

    End Function
    Function WriteGjensidige_040(ByRef oPayment As Payment) As String
        ' 040                  07082802020001                             STHNEI0I200708280802575909000001
        Dim sLine As String

        sLine = "040" ' RecType, 1-3
        sLine = sLine & Space(18) ' Trans.bel�p, 4-21, not in use
        sLine = sLine & PadRight(oPayment.REF_Own, 35, " ") ' TransID, 22-56
        sLine = sLine & Space(1) ' Sjekk, 57, not in use
        sLine = sLine & Space(1) ' Hastebetaling, 58, not in use
        sLine = sLine & Space(3) ' Type betaling, 59-61, not in use
        sLine = sLine & Space(3) ' Betalingskanal, 62-64, not in use
        '21.11.2012 - Added IF to get � unique ID
        If oPayment.PayType = "I" Then
            sLine = sLine & PadRight(Trim(oPayment.REF_Bank1) & Trim(oPayment.REF_Bank2), 35, " ") ' Bankref, 65-99
        Else
            sLine = sLine & PadRight(Trim(oPayment.REF_Bank1) & Trim(oPayment.REF_Own), 35, " ") ' Bankref, 65-99
        End If
        sLine = sLine & Space(3) ' Type banktjeneste, 100-102, not in use
        sLine = sLine & Space(6) ' Filler, 103-108

        WriteGjensidige_040 = sLine

    End Function
    Function WriteGjensidige_CREMUL_040(ByRef oBatch As vbBabel.Batch, ByRef oPayment As Payment) As String
        ' 040                  9176020810
        Dim sLine As String

        sLine = "040" ' RecType, 1-3
        sLine = sLine & Space(18) ' Trans.bel�p, 4-21, not in use
        '01.03.2013 - Added next IF. Mats wants to cahnge the reference stated in record021 with the one stated in record040 for CREMUL, incoming international payments 
        If oPayment.ImportFormat = vbBabel.BabelFiles.FileType.Camt054_Incoming And oPayment.PayType = "I" Then
            If EmptyString(oBatch.REF_Own) Then
                sLine = sLine & PadRight(Trim(oBatch.REF_Bank), 35, " ") 'TransID, 22-56
            Else
                sLine = sLine & PadRight(Trim(oBatch.REF_Own), 35, " ") 'TransID, 22-56
            End If
        Else
            sLine = sLine & PadRight(oPayment.REF_Bank1, 35, " ") ' TransID, 22-56
        End If
        'Old code
        'sLine = sLine & PadRight(oPayment.REF_Bank1, 35, " ") ' TransID, 22-56
        sLine = sLine & Space(57) ' Filler

        WriteGjensidige_CREMUL_040 = sLine

    End Function
    Function WriteGjensidige_041(ByRef oPayment As Payment) As String
        ' 041235145616
        Dim sLine As String

        sLine = "041" ' RecType, 1-3
        sLine = sLine & PadRight(oPayment.REF_Bank2, 35, " ") ' Arkivreferanse
        sLine = sLine & Space(35) ' Egenreferanse
        sLine = sLine & Space(35) ' Fremmedreferanse

        WriteGjensidige_041 = sLine

    End Function
    Function WriteGjensidige_042_36(ByRef oPayment As Payment) As String
        ' 04236 1000,00           NOKSEK0,8510      SEK
        Dim sLine As String
        Dim dCurrencyRate As Double
        Dim sCurrencyRate As String

        sLine = "042" ' RecType, 1-3
        sLine = sLine & "36 " ' Bel�pstype, 4-6
        sLine = sLine & PadRight(ConvertFromAmountToString((oPayment.MON_TransferredAmount), , ","), 18, " ") ' Bel�p, venstrejustert, 7-24, overf�rt bel�p
        sLine = sLine & "NOK" ' Referansevaluta, 25-27, her alltid NOK
        sLine = sLine & oPayment.MON_TransferCurrency ' Maalvaluta, 28-30, betalingsvaluta
        ' Valutakurs, 31-42, kurs mot NOK, fire desimaler
        dCurrencyRate = CalculateCurrencyUnits((oPayment.MON_AccountExchRate), (oPayment.MON_TransferCurrency), 1, 1)
        dCurrencyRate = System.Math.Round(dCurrencyRate, 4)
        sCurrencyRate = Trim(Str(dCurrencyRate))
        sCurrencyRate = Replace(sCurrencyRate, ".", ",")
        sLine = sLine & PadRight(sCurrencyRate, 12, " ")
        'sLine = sLine & PadRight(Mid$(Replace(Round(oPayment.MON_AccountExchRate - Int(oPayment.MON_AccountExchRate), 8), ",", ""), 2), 12, " ")
        sLine = sLine & oPayment.MON_TransferCurrency ' Valuta, 43-45
        sLine = sLine & Space(11) ' Filler_11, 47-57

        WriteGjensidige_042_36 = sLine

    End Function
    Private Function WriteGjensidige_CREMUL_042_98(ByRef oPayment As Payment) As String
        ' 04298 3171,00                                20320070829
        Dim sLine As String
        Dim dCurrencyRate As Double
        Dim sCurrencyRate As String

        sLine = "042" ' RecType, 1-3
        sLine = sLine & "98 " ' Bel�pstype, 4-6
        sLine = sLine & PadRight(ConvertFromAmountToString((oPayment.MON_TransferredAmount), , ","), 18, " ") ' Bel�p, venstrejustert, 7-24, overf�rt bel�p
        If oPayment.PayType = "I" Then
            sLine = sLine & oPayment.MON_InvoiceCurrency 'XXXXX EMIL               ' Referansevaluta, 25-27
            sLine = sLine & oPayment.MON_TransferCurrency ' Maalvaluta, 28-30, betalingsvaluta

            ' Valutakurs, 31-42, kurs mot NOK, fire desimaler, oppgis alltid i en enhet
            dCurrencyRate = CalculateCurrencyUnits((oPayment.MON_AccountExchRate), (oPayment.MON_TransferCurrency), 1, 1)
            dCurrencyRate = System.Math.Round(dCurrencyRate, 4)
            sCurrencyRate = Trim(Str(dCurrencyRate))
            sCurrencyRate = Replace(sCurrencyRate, ".", ",")
            sLine = sLine & PadRight(sCurrencyRate, 12, " ")
            sLine = sLine & oPayment.MON_TransferCurrency ' XXXXX EMIL hvilken valuta her???                       ' Valuta, 43-45
        Else
            ' Domestic
            sLine = sLine & Space(3) ' Referansevaluta, 25-27
            sLine = sLine & Space(3) ' Maalvaluta, 28-30, betalingsvaluta
            sLine = sLine & Space(12)
            sLine = sLine & Space(3) ' Valuta, 43-45
        End If
        sLine = sLine & "203" 'datotype, XXXXX EMIL alltid 203???
        sLine = sLine & oPayment.DATE_Payment ' dato 49,8

        WriteGjensidige_CREMUL_042_98 = sLine

    End Function
    Private Function WriteGjensidige_CREMUL_042_119(ByRef oPayment As Payment) As String
        ' 04298 3171,00                                20320070829
        Dim sLine As String
        Dim dCurrencyRate As Double
        Dim sCurrencyRate As String

        sLine = "042" ' RecType, 1-3
        sLine = sLine & "119" ' Bel�pstype, 4-6
        sLine = sLine & PadRight(ConvertFromAmountToString((oPayment.MON_OriginallyPaidAmount), , ","), 18, " ") ' Bel�p, venstrejustert, 7-24, overf�rt bel�p
        sLine = sLine & PadRight(oPayment.MON_OriginallyPaidCurrency, 3, " ") ' Referansevaluta, 25-27
        If Len(oPayment.MON_AccountCurrency) <> 3 Then
            sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") ' Maalvaluta, 28-30, betalingsvaluta
        Else
            sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") ' Maalvaluta, 28-30, betalingsvaluta
        End If

        ' Valutakurs, 31-42, kurs mot NOK, fire desimaler, oppgis alltid i en enhet
        If oPayment.PayType = "I" Then
            dCurrencyRate = CalculateCurrencyUnits((oPayment.MON_AccountExchRate), (oPayment.MON_TransferCurrency), 1, 1)
        Else
            dCurrencyRate = CalculateCurrencyUnits(1, (oPayment.MON_TransferCurrency), 1, 1)
        End If
        dCurrencyRate = System.Math.Round(dCurrencyRate, 4)
        sCurrencyRate = Trim(Str(dCurrencyRate))
        sCurrencyRate = Replace(sCurrencyRate, ".", ",")
        sLine = sLine & PadRight(sCurrencyRate, 12, " ")
        sLine = sLine & PadRight(oPayment.MON_OriginallyPaidCurrency, 3, " ") ' Valuta, 43-45
        sLine = sLine & "203" 'datotype, XXXXX EMIL alltid 203???
        sLine = sLine & oPayment.DATE_Payment ' dato 49,8

        WriteGjensidige_CREMUL_042_119 = sLine

    End Function
    Function WriteGjensidige_042_60(ByRef oPayment As Payment, Optional ByRef nAmountToUse As Double = 0) As String
        ' 04260 851,06                              NOK
        Dim sLine As String

        sLine = "042" ' RecType, 1-3
        sLine = sLine & "60 " ' Bel�pstype, 4-6
        ' ????? use invoiceamount ????????
        If IsEqualAmount(nAmountToUse, 0) Then
            If oPayment.MON_AccountAmount <> 0 Then
                sLine = sLine & PadRight(ConvertFromAmountToString((oPayment.MON_AccountAmount), , ","), 18, " ")
            Else
                sLine = sLine & PadRight(ConvertFromAmountToString((oPayment.MON_InvoiceAmount), , ","), 18, " ") ' Bel�p, venstrejustert, 7-24, overf�rt bel�p
            End If
        Else
            sLine = sLine & PadRight(ConvertFromAmountToString(nAmountToUse, , ","), 18, " ")
        End If
        sLine = sLine & Space(3) ' Referansevaluta, 25-27, her alltid NOK
        sLine = sLine & Space(3) ' Maalvaluta, 28-30, betalingsvaluta
        sLine = sLine & Space(12) ' Valutakurs, 31-42
        ' ?????? Always NOK??????????
        sLine = sLine & "NOK" ' Valuta, 43-45
        sLine = sLine & Space(11) ' Filler_11, 47-57

        WriteGjensidige_042_60 = sLine

    End Function
    Function WriteGjensidige_042_98(ByRef oPayment As Payment) As String
        ' 04298 1000,00                             SEK
        Dim sLine As String

        sLine = "042" ' RecType, 1-3
        sLine = sLine & "98 " ' Bel�pstype, 4-6
        ' ????? use accountamount ????????
        sLine = sLine & PadRight(ConvertFromAmountToString((oPayment.MON_InvoiceAmount), , ","), 18, " ") ' Bel�p, venstrejustert, 7-24, overf�rt bel�p
        sLine = sLine & Space(3) ' Referansevaluta, 25-27, her alltid NOK
        sLine = sLine & Space(3) ' Maalvaluta, 28-30, betalingsvaluta
        sLine = sLine & Space(12) ' Valutakurs, 31-42
        sLine = sLine & oPayment.MON_InvoiceCurrency ' Valuta, 43-45
        sLine = sLine & Space(11) ' Filler_11, 47-57

        WriteGjensidige_042_98 = sLine

    End Function
    Private Function WriteGjensidige_043(ByRef oPayment As Payment) As String
        ' 04372400123283
        ' Used for Cremul - BetalingOrdreNr, from Gr.11 RFF C506_1154, Gr.11 RFF C506_1153 er lik "AEK"
        Dim sLine As String

        sLine = "043" ' RecType, 1-3
        If Not EmptyString((oPayment.REF_Bank1)) Then
            sLine = sLine & PadRight(oPayment.REF_Bank1, 35, " ") ' Kundekonto, 4-38
        End If

        WriteGjensidige_043 = sLine

    End Function
    Function WriteGjensidige_050(ByRef oPayment As Payment) As String
        ' 050SE2960000000000037133268
        Dim sLine As String

        sLine = "050" ' RecType, 1-3
        If oPayment.E_Account = "00000000019" Then
            sLine = sLine & Space(35) ' Kundekonto, 4-38
        Else
            sLine = sLine & PadRight(oPayment.E_Account, 35, " ") ' Kundekonto, 4-38
        End If
        sLine = sLine & PadRight(oPayment.E_Name, 35, " ") ' Kundenavn, 39-73
        sLine = sLine & Space(35) ' Filler_35, 74-98

        WriteGjensidige_050 = sLine

    End Function
    Function WriteGjensidige_052(ByRef oPayment As Payment) As String
        ' 052NWBKGB2L
        Dim sLine As String

        sLine = "052" ' RecType, 1-3
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ") ' Mottakers Swift, 4-14
        sLine = sLine & PadRight(oPayment.BANK_BranchNo, 17, " ") '?????????????      ' Mottaker bank branch, 15-31
        sLine = sLine & Space(35) ' Filler_35, 74-98

        WriteGjensidige_052 = sLine

    End Function
    Function WriteGjensidige_053(ByRef oPayment As Payment) As String
        Dim sLine As String

        sLine = "053" ' RecType, 1-3
        sLine = sLine & PadRight(oPayment.BANK_Name, 70, " ") ' Mottakers banknavn, 4-73

        WriteGjensidige_053 = sLine

    End Function
    Private Function WriteGjensidige_060(ByRef sE_Name As String) As String
        ' Used for Cremul - namerecords
        Dim sLine As String

        sLine = "060" ' RecType, 1-3
        sLine = PadRight(sLine & Trim(sE_Name), 70, " ") ' Name 1 4-35, Name 2 36-70

        WriteGjensidige_060 = sLine

    End Function
    Private Function WriteGjensidige_061(ByRef oPayment As Payment) As String
        ' Used for Cremul - namerecords
        Dim sLine As String

        sLine = "061" ' RecType, 1-3
        If Not EmptyString((oPayment.E_Adr1)) Then
            sLine = sLine & PadRight(Trim(oPayment.E_Adr1), 35, " ") ' Address 1 4-38
            If Not EmptyString((oPayment.E_Adr2)) Then
                sLine = sLine & PadRight(Trim(oPayment.E_Adr2), 35, " ") ' Address 2 39-73
            ElseIf Not EmptyString((oPayment.E_Adr3)) Then
                sLine = sLine & PadRight(Trim(oPayment.E_Adr3), 35, " ") ' Address 2 39-73
            End If
        Else
            If Not EmptyString((oPayment.E_Adr2)) Then
                sLine = sLine & PadRight(Trim(oPayment.E_Adr2), 35, " ") ' Address 1 4-38
                If Not EmptyString((oPayment.E_Adr3)) Then
                    sLine = sLine & PadRight(Trim(oPayment.E_Adr3), 35, " ") ' Address 2 39-73
                End If
            Else
                If Not EmptyString((oPayment.E_Adr3)) Then
                    sLine = sLine & PadRight(Trim(oPayment.E_Adr3), 35, " ") ' Address 1 4-38
                End If
            End If
        End If
        sLine = PadRight(sLine, 73, " ")

        WriteGjensidige_061 = sLine

    End Function
    Private Function WriteGjensidige_062(ByRef oPayment As Payment) As String 'CREMUL
        ' Used for Cremul - namerecords
        Dim sLine As String

        sLine = "062" ' RecType, 1-3
        sLine = sLine & PadRight(Trim(oPayment.E_City), 35, " ") 'Poststed  4-38
        sLine = sLine & PadRight(Trim(oPayment.E_Zip), 9, " ") 'Postnummer 39-47
        sLine = sLine & PadRight(Trim(oPayment.E_CountryCode), 3, " ") 'Landkode 48-50

        sLine = PadRight(sLine, 50, " ")

        WriteGjensidige_062 = sLine

    End Function

    Private Function WriteGjensidige_071(ByRef sString As String) As String
        ' Used for Cremul - namerecords
        Dim sLine As String

        sLine = "071" ' RecType, 1-3
        sLine = sLine & PadRight(sString, 70, " ")

        WriteGjensidige_071 = sLine

    End Function
    Function WriteGjensidige_072(ByRef oPayment As Payment) As String
        Dim sLine As String
        Dim oInvoice As Invoice

        '072          45

        sLine = "072" ' RecType, 1-3
        sLine = sLine & Space(10)
        ' Use statuscodes from Telepay (so far)
        If Val(oPayment.StatusCode) > 2 Then
            sLine = sLine & oPayment.StatusCode 'Gjensidige_S2000_Avvikskode(oPayment)
        Else
            For Each oInvoice In oPayment.Invoices
                If Val(oInvoice.StatusCode) > 0 Then
                    sLine = sLine & oInvoice.StatusCode
                    ' gor Gjensidige, only on invoice pr payment
                    Exit For
                End If
            Next oInvoice
        End If
        WriteGjensidige_072 = sLine

    End Function
    Function WriteGjensidige_073(ByRef oPayment As Payment) As String
        '    Gj�r d� en anpassning i denne recordtype  :
        '�	Avvisningskode er p� 3 pos ut�kes til 5 pos. Starter i posisjon 11.
        '�	Avvisningstekst er nytt felt. Legges direkte etter koden. Starter d� i posisjon 16 og er p� 65 pos langt. 
        Dim sLine As String
        Dim sTmp As String = ""
        Dim sErrorCode As String = ""
        Dim sErrorMessage As String = ""
        Dim iPos As Integer = 0
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim bContinue As Boolean

        '073       EC043BENEFICIARY NAME OR ADDRESS IS MISSING

        '16.05.2017 - Added new code to add freetext
        If oPayment.ImportFormat <> vbBabel.BabelFiles.FileType.Telepay2 Then
            For Each oInvoice In oPayment.Invoices
                bContinue = False
                If Not IsNumeric(oInvoice.StatusCode) Then
                    bContinue = True
                ElseIf Val(oInvoice.StatusCode) > 0 Then
                    bContinue = True
                End If

                If bContinue Then
                    For Each oFreetext In oInvoice.Freetexts
                        sTmp = sTmp & oFreetext.Text
                    Next
                    iPos = InStr(sTmp, "  ", CompareMethod.Text)
                    If iPos > 0 Then
                        sErrorCode = Left(sTmp, iPos - 1)
                        sErrorMessage = Trim(Mid(sTmp, iPos + 1))
                    Else
                        sErrorCode = ""
                        sErrorMessage = sTmp
                    End If
                    'sLine = sLine & PadRight(sErrorCode, 10, " ")
                    'sLine = sLine & PadRight(sErrorMessage, 65, " ")
                    ' gor Gjensidige, only on invoice pr payment
                    Exit For
                End If
            Next oInvoice
            If EmptyString(sErrorMessage) Then
                sErrorMessage = oPayment.StatusText
            End If
        End If

        sLine = "073" ' RecType, 1-3
        'sLine = sLine & Space(7)
        ' Use statuscodes from Telepay (so far)
        If oPayment.ImportFormat = vbBabel.BabelFiles.FileType.Pain002_Rejected Then
            If Val(oPayment.StatusCode) > 2 Then
                For Each oInvoice In oPayment.Invoices
                    If EmptyString(sErrorCode) Then
                        sErrorCode = "99"
                    End If
                    sLine = sLine & PadRight(sErrorCode, 10, " ")
                    sLine = sLine & PadRight(sErrorMessage, 65, " ")


                    'If Val(oInvoice.StatusCode) > 0 Then
                    '    For Each oFreetext In oInvoice.Freetexts
                    '        sTmp = sTmp & oFreetext.Text
                    '    Next
                    '    iPos = InStr(sTmp, "  ", CompareMethod.Text)
                    '    If iPos > 0 Then
                    '        sErrorCode = Left(sTmp, iPos - 1)
                    '        sErrorMessage = Trim(Mid(sTmp, iPos + 1))
                    '    Else
                    '        sErrorCode = "99"
                    '        sErrorMessage = sTmp
                    '    End If
                    '    sLine = sLine & PadRight(sErrorCode, 10, " ")
                    '    sLine = sLine & PadRight(sErrorMessage, 65, " ")
                    '    ' For Gjensidige, only on invoice pr payment
                    '    Exit For
                    'End If
                Next oInvoice
            End If
        Else

            '16.05.2017 - Added the next IF
            If IsNumeric(oPayment.StatusCode) Then
                If Val(oPayment.StatusCode) > 2 Then
                    sErrorCode = oPayment.StatusCode.Trim 'Gjensidige_S2000_Avvikskode(oPayment)
                Else
                    For Each oInvoice In oPayment.Invoices
                        '16.05.2017 - Added the next IF
                        If IsNumeric(oInvoice.StatusCode) Then
                            If Val(oInvoice.StatusCode) > 0 Then
                                sErrorCode = oInvoice.StatusCode.Trim
                                ' gor Gjensidige, only on invoice pr payment
                                Exit For
                            End If
                        Else
                            sErrorCode = "99"
                        End If
                    Next oInvoice
                End If
            Else
                sErrorCode = "99"
            End If
            If EmptyString(sErrorMessage) Then
                sErrorMessage = oPayment.StatusText
            End If
            sLine = sLine & PadRight(sErrorCode, 10, " ")
            sLine = sLine & PadRight(sErrorMessage, 65, " ")
        End If
        WriteGjensidige_073 = sLine

    End Function

    Private Function WriteGjensidige_080(ByRef oInvoice As Invoice, ByRef sDocumentType As String) As String
        ' Used for Cremul - KID
        Dim sLine As String

        sLine = "080" ' RecType, 1-3
        sLine = sLine & Space(35) ' filler, 4-38
        sLine = sLine & PadRight(oInvoice.Unique_Id, 35, " ") ' FakturaReferanse, 39,73
        sLine = sLine & "999" ' Type dokument, 74-76, her alltid 999
        sLine = sLine & Space(3) ' Filler, 77-79

        WriteGjensidige_080 = sLine

    End Function
    Private Function WriteGjensidige_083(ByRef oInvoice As Invoice) As String
        ' Used for Cremul - InvoiceAmount
        Dim sLine As String

        sLine = "083" ' RecType, 1-3
        sLine = sLine & PadRight(ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), , ","), 18, " ") ' Bel�p, 4-21
        sLine = sLine & Space(35) ' Dokumentdato, 22-56
        sLine = sLine & "12 " ' Betalingstype, 57-59, EMIL her alltid 12 ????
        sLine = sLine & Space(3) ' Datotype, 60-62

        WriteGjensidige_083 = sLine

    End Function
    Private Function CalculateGjensidigeBatchAmount(ByRef oBatch As Batch, ByRef oPayment As Payment) As Double
        ' run through all payments up to next shift in i_account, date or currency
        Dim nPaymentItem As Integer
        Dim i As Integer
        Dim sOldDate, sOldAccount, sOldCurrency As String
        Dim nAmount As Double

        nPaymentItem = oPayment.Index
        oPayment = oBatch.Payments(nPaymentItem)
        sOldAccount = oPayment.I_Account
        sOldDate = oPayment.DATE_Payment
        sOldCurrency = oPayment.MON_InvoiceCurrency

        For i = nPaymentItem To oBatch.Payments.Count
            oPayment = oBatch.Payments(i)
            If oPayment.I_Account <> sOldAccount Or oPayment.DATE_Payment <> sOldDate Or oPayment.MON_InvoiceCurrency <> sOldCurrency Then
                Exit For
            End If
            nAmount = nAmount + oPayment.MON_InvoiceAmount
        Next i
        oPayment = oBatch.Payments(nPaymentItem) ' Reset to first payment in batch
        CalculateGjensidigeBatchAmount = nAmount
    End Function
    Private Function CalculateGjensidige_BatchAmount(ByRef oBabelFiles As BabelFiles, ByRef oBabelFile As BabelFile, ByRef oBatch As Batch, ByRef oPayment As Payment) As Double
        ' run through all payments up to next shift in i_account, date or currency
        Dim nBabelFileItem As Integer
        Dim nBatchItem As Integer
        Dim nPaymentItem As Integer
        Dim i As Integer
        Dim sOldDate, sOldAccount, sOldCurrency As String
        Dim nAmount As Double
        Dim bJumpOut As Boolean
        Dim bReachedCurrentPayment As Boolean

        nBabelFileItem = oBabelFile.Index
        nBatchItem = oBatch.Index
        nPaymentItem = oPayment.Index
        bReachedCurrentPayment = False

        sOldAccount = oPayment.I_Account
        sOldDate = oPayment.DATE_Payment
        sOldCurrency = oPayment.MON_InvoiceCurrency
        bJumpOut = False

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If oBabelFile.Index = nBabelFileItem And oBatch.Index = nBatchItem And oPayment.Index = nPaymentItem Then
                        bReachedCurrentPayment = True
                    End If

                    If bReachedCurrentPayment Then
                        ' do not start totaling before we have reached the "current" payment
                        If oPayment.I_Account = sOldAccount And oPayment.DATE_Payment = sOldDate And oPayment.MON_InvoiceCurrency = sOldCurrency Then
                            If oPayment.StatusCode = "02" Then
                                If Not oPayment.Cancel Then
                                    nAmount = nAmount + oPayment.MON_InvoiceAmount
                                End If
                            End If
                        Else
                            ' XXXX EMIL - What to do here?
                            ' If break in account, date or currency, jump out ?
                            ' Jump out if we are going to have a mix, and thus several batches
                            bJumpOut = True
                            Exit For
                        End If
                    End If
                Next oPayment
                If bJumpOut Then
                    Exit For
                End If
            Next oBatch
            If bJumpOut Then
                Exit For
            End If
        Next oBabelFile
        ' Reset to where we were before calculating totals
        oBabelFile = oBabelFiles(nBabelFileItem)
        oBatch = oBabelFile.Batches(nBatchItem)
        oPayment = oBatch.Payments(nPaymentItem) ' Reset to first payment in batch

        CalculateGjensidige_BatchAmount = nAmount
    End Function
    Private Function Gjensidige_S2000_Avvikskode(ByRef oPayment As Payment) As String
        ' Find S2000 Avvikskode from BabelBank avvikskode
        Dim sCode As String
        ' Avvikskoder i BANSTA
        'N02    19    utbetaling til konto omgjort til utbet.anvisning pga. feil i kontonr.feltet
        'N03          melding om kreditering blir ikke sendt grunnet utenlandsadresse.
        '102    34    feil i navn/adr . Ikke mulig � sende giroutbetaling grunnet feil i navn/adr.felt
        '104    18    avvist, mottakers konto krever kid
        '105    17     avvist, feil registrert kidnr.
        '107    44    feil landkode
        '45           avvist grunnet avsluttet konto hos mottaker
        '000 ???      �vrige feil ???????????????????

        Select Case oPayment.StatusCode
            Case "19"
                sCode = "N02"
            Case "34"
                sCode = "102"
            Case "18"
                sCode = "104"
            Case "17"
                sCode = "105"
            Case "44"
                sCode = "107"
            Case Else
                sCode = "000"
        End Select

        Gjensidige_S2000_Avvikskode = sCode
    End Function
    Function WriteSGFinans_SaldoKunde(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef iFiletype As BabelFiles.FileType, ByRef sSpecial As String) As Boolean
        ' For SG Finans
        ' Creates TAB-separated or fixed formatfile for Saldofile clientreporting to mail
        ' 20.02.2008
        ' Added two new fields, at the end; AvtaleNr and Avtalenr
        ' 28.02.2008
        ' Added two new formattypes; CSV-separated and Fixed
        '--------------------------------------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer
        Dim sTmp As String
        Dim sTmpDate As String
        Dim sDelimiter As String
        Dim sDecimalSign As String

        Try

            lCounter = 0
            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            'If bFixedFormat Then
            '    ' Fixed, no delimiter, no header
            '    sDelimiter = ""
            'Else

            ' for SG Revisjonsrapport, we like ; sep files exported, but can't set that as exportformat
            '28.07.09 Moved headerinfo down, to be able to find correct customername


            If iFiletype = BabelFiles.FileType.SG_SaldoFil_Aq_Fixed Then
                ' Fixed, no delimiter, no header
                sDelimiter = ""
            ElseIf iFiletype = BabelFiles.FileType.SG_SaldoFil_Aq_CSV Then
                ' ; separated, no headerline
                sDelimiter = ";"
            Else
                ' TAB-delimited format, with headerline
                sDelimiter = Chr(9)

                If sSpecial <> "SG_REVISJON_SALDO" Then
                    ' 21.04.2009
                    ' Revisjonsrapport saldo does not use all columns

                    ' First, create a headerline;
                    '----------------------------
                    sLine = Chr(34) & "Transtype" & Chr(34) & sDelimiter & Chr(34) & "Debitor nr." & Chr(34) & sDelimiter
                    sLine = sLine & Chr(34) & "Navn" & Chr(34) & sDelimiter & Chr(34) & "Kredittid" & Chr(34) & sDelimiter
                    sLine = sLine & Chr(34) & "Saldo totalt" & Chr(34) & sDelimiter & sDelimiter
                    sLine = sLine & Chr(34) & "Ikke forfalt" & Chr(34) & sDelimiter & sDelimiter
                    sLine = sLine & Chr(34) & "Forfalt 1-15" & Chr(34) & sDelimiter & sDelimiter & Chr(34) & "Forfalt 16-30" & Chr(34) & sDelimiter
                    sLine = sLine & sDelimiter & Chr(34) & "Forfalt 31-60" & Chr(34) & sDelimiter & sDelimiter
                    sLine = sLine & Chr(34) & "Forfalt 61-90" & Chr(34) & sDelimiter & sDelimiter
                    sLine = sLine & Chr(34) & "Forfalt 90-" & Chr(34) & sDelimiter & sDelimiter
                    sLine = sLine & Chr(34) & "Betalingstid" & Chr(34) & sDelimiter & Chr(34) & "Kreditlimit" & Chr(34) & sDelimiter
                    sLine = sLine & Chr(34) & "Sperrekode" & Chr(34) & sDelimiter & Chr(34) & "Siste purredato" & Chr(34) & sDelimiter
                    sLine = sLine & Chr(34) & "Antall P3" & Chr(34) & sDelimiter & Chr(34) & "Antall Inkasso" & Chr(34) & sDelimiter
                    sLine = sLine & Chr(34) & "DecisionScore" & Chr(34) & sDelimiter & Chr(34) & "Valuta" & Chr(34) & sDelimiter
                    sLine = sLine & Chr(34) & "Avtalenr." & Chr(34)
                    ' added 03.11.2009
                    sLine = sLine & sDelimiter & Chr(34) & "Avtalenr." & Chr(34)
                    'XNET 08.05.2012 Added new field Rentefakturaer for Ventist�l (6401)
                    If sSpecial = "SG_SALDO_6401" Then
                        sLine = sLine & sDelimiter & Chr(34) & "Rentefakturaer" & Chr(34)
                    End If

                    oFile.WriteLine(sLine)
                End If
            End If

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                'If oPayment.I_Account = sI_Account Then
                                ' 01.10.2018 � changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                If sSpecial = "SG_REVISJON_SALDO" Then

                                    iFiletype = BabelFiles.FileType.SG_SaldoFil_Aq_CSV

                                    ' First, fileheading
                                    sLine = "Nordea Finance AS, org.nr. 987 664 398"
                                    oFile.WriteLine(sLine)

                                    sLine = "Aldersfordelt saldoliste"
                                    oFile.WriteLine(sLine)
                                    sLine = "Kunde " & sClientNo & " " & oPayment.I_Name
                                    oFile.WriteLine(sLine)
                                    ' find last day in previous month;
                                    'sTmp = Str(DateSerial(Year(Date), Month(Date), 1) - 1)
                                    'sLine = "Periode per " & sTmp
                                    ' changed 04.11.2009 - take period from recordset
                                    sLine = "Periode per " & Trim(Str(oBabel.Period))
                                    oFile.WriteLine(sLine)
                                    sLine = "Dato for datauttrekk: " & oPayment.VoucherNo
                                    oFile.WriteLine(sLine)
                                    sLine = "Produksjonsdato: " & VB6.Format(Date.Today, "dd.mm.yyyy")
                                    oFile.WriteLine(sLine)
                                    sLine = "" 'blank linje
                                    oFile.WriteLine(sLine)

                                    ' Then, create a headerline;
                                    '----------------------------
                                    sDelimiter = ";"
                                    sLine = Chr(34) & "Debitor nr." & Chr(34) & sDelimiter
                                    ' added 16.12.2009
                                    sLine = sLine & Chr(34) & "Org nr." & Chr(34) & sDelimiter

                                    sLine = sLine & Chr(34) & "Navn" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Saldo" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Ikke forfalt" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Forfalt 0-30" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Forfalt 31-60" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Forfalt 61-90" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Forfalt 90+" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Rating D&B" & Chr(34) & sDelimiter
                                    sLine = sLine & Chr(34) & "Eng.nr" & Chr(34)

                                    oFile.WriteLine(sLine)
                                End If

                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If


                                If bExportoPayment And oPayment.OmitExport = False Then
                                    If oPayment.PayCode = "720" Then ' 720 = Saldoposter
                                        ' Assumes only one invoice pr payment

                                        lCounter = lCounter + 1
                                        sLine = ""

                                        If sSpecial <> "SG_REVISJON_SALDO" Then
                                            'S1 Transaksjonstype, always "02"
                                            sLine = sLine & "02" & sDelimiter
                                        End If


                                        'S2 DebitorNo
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().CustomerNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & PadLeft(oPayment.Invoices(1).CustomerNo, 9, "0") & sDelimiter

                                        If sSpecial = "SG_REVISJON_SALDO" Then
                                            'S1 Transaksjonstype, always "02"
                                            sLine = sLine & oPayment.e_OrgNo & sDelimiter
                                        End If

                                        'S3 DebitorName
                                        sLine = sLine & PadRight(oPayment.E_Name, 30, " ") & sDelimiter

                                        If sSpecial <> "SG_REVISJON_SALDO" Then
                                            'S4 Gj.snitt kredittid
                                            sLine = sLine & PadLeft(oPayment.ExtraI1, 3, "0") & sDelimiter
                                        End If

                                        ' All amounts in "hele kroner"
                                        ' XNET 11.05.2012 added sspecial="SG_SALDO_6401"
                                        If iFiletype = BabelFiles.FileType.SG_SaldoFil_Kunde Or iFiletype = BabelFiles.FileType.SG_SaldoFil_Aq_Fixed Or sSpecial = "SG_SALDO_6401" Then
                                            ' TAB sep or Fixed
                                            ' ----------------

                                            'S5 Saldo totalt
                                            sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_InvoiceAmount / 100), "000######") & sDelimiter

                                            ' S6 Fortegn for S5
                                            sLine = sLine & IIf(oPayment.MON_InvoiceAmount < 0, "-", " ") & sDelimiter

                                            'S7 Saldo ikke forfallt
                                            sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_TransferredAmount / 100), "000######") & sDelimiter

                                            ' S8 Fortegn for S7
                                            sLine = sLine & IIf(oPayment.MON_TransferredAmount < 0, "-", " ") & sDelimiter

                                            'S9 Saldo forfalt 1 - 15 dager
                                            sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_AccountAmount / 100), "000######") & sDelimiter

                                            ' S10 Fortegn for S9
                                            sLine = sLine & IIf(oPayment.MON_AccountAmount < 0, "-", " ") & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' 21.04.2009
                                                ' Revisjonsrapport saldo has 1-30 days, not splitted in 1-15 and 16-30 as rest

                                                'S11 Saldo forfalt 16 - 30 dager
                                                sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_LocalAmount / 100), "000######") & sDelimiter

                                                ' S12 Fortegn for S11
                                                sLine = sLine & IIf(oPayment.MON_LocalAmount < 0, "-", " ") & sDelimiter
                                            End If

                                            'S13 Saldo forfalt 31 - 60 dager
                                            sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_EuroAmount / 100), "000######") & sDelimiter

                                            ' S14 Fortegn for S13
                                            sLine = sLine & IIf(oPayment.MON_EuroAmount < 0, "-", " ") & sDelimiter

                                            'S15 Saldo forfalt 61 - 90 dager
                                            sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_ChargesAmount / 100), "000######") & sDelimiter

                                            ' S16 Fortegn for S15
                                            sLine = sLine & IIf(oPayment.MON_ChargesAmount < 0, "-", " ") & sDelimiter

                                            'S17 Saldo forfalt over 90 dager
                                            sLine = sLine & VB6.Format(System.Math.Abs(oPayment.MON_OriginallyPaidAmount / 100), "000######") & sDelimiter

                                            ' S18 Fortegn for S17
                                            sLine = sLine & IIf(oPayment.MON_OriginallyPaidAmount < 0, "-", " ") & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' 21.04.2009
                                                ' Revisjonsrapport saldo does not use all columns

                                                'S19 Gj.snitt betalingstid
                                                sLine = sLine & PadLeft(oPayment.ExtraI2, 3, " ") & sDelimiter

                                                'S20 Kreditlimit
                                                ' not in use
                                                sLine = sLine & New String("0", 9) & sDelimiter

                                                'S21 Sperrekode
                                                ' not in use
                                                sLine = sLine & "00" & sDelimiter

                                                'S22 Siste purredato
                                                'sLine = sLine & String(6, " ") & sDelimiter
                                                ' 05.03.2008 added!
                                                sLine = sLine & PadRight(oPayment.ExtraI3, 6, " ") & sDelimiter

                                                'S23 Antall P3
                                                ' sLine = sLine & "000" & sDelimiter
                                                ' 05.03.2008 added!
                                                sLine = sLine & PadLeft(oPayment.ExtraD1, 3, "0") & sDelimiter

                                                'S24 Antall inkasso
                                                ' sLine = sLine & "000" & sDelimiter
                                                ' 05.03.2008 added!
                                                sLine = sLine & PadLeft(oPayment.ExtraD2, 3, "0") & sDelimiter

                                            End If

                                            'S25 Kreditrating
                                            ' not in use
                                            ' in use from 03.03.2009
                                            sLine = sLine & PadRight(oPayment.ExtraD3, 3, " ") & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' 21.04.2009
                                                ' Revisjonsrapport saldo does not use all columns

                                                'S26 Valuta
                                                sLine = sLine & oPayment.MON_InvoiceCurrency & sDelimiter

                                                'S27 Avtalenr.
                                                sLine = sLine & PadLeft(oPayment.VoucherType, 3, "0") & sDelimiter

                                                ' added 03.11.2009
                                                'S28 Org.no
                                                sLine = sLine & PadLeft(oPayment.e_OrgNo, 11, "0")

                                            End If

                                            'XNET 08.05.2012 Added new field Rentefakturaer for Venistaal (6401)
                                            If sSpecial = "SG_SALDO_6401" Then
                                                ' Rentefakturaer
                                                sLine = sLine & sDelimiter & Format(Math.Abs(oPayment.MON_AccountExchRate), "000######") & sDelimiter
                                            End If

                                        ElseIf iFiletype = BabelFiles.FileType.SG_SaldoFil_Aq_CSV Then
                                            ' ; sep, csv-file
                                            ' ---------------
                                            If sSpecial = "SG_REVISJON_SALDO" Then
                                                ' decimalsign for revisionreport is , (comma)
                                                sDecimalSign = ","
                                            Else
                                                ' decimalsign for all reports BUT revisionreport is . (point)
                                                sDecimalSign = "."
                                            End If

                                            'S5 Saldo totalt
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_InvoiceAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' S6 Fortegn for S5, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If

                                            'S7 Saldo ikke forfallt
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_TransferredAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' S8 Fortegn for S7, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If

                                            'S9 Saldo forfalt 1 - 15 dager
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_AccountAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' S10 Fortegn for S9, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' 21.04.2009
                                                ' Revisjonsrapport saldo has 1-30 days, not splitted in 1-15 and 16-30 as rest
                                                'S11 Saldo forfalt 16 - 30 dager
                                                sLine = sLine & Replace(VB6.Format(oPayment.MON_LocalAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                                ' S12 Fortegn for S11, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If
                                            'S13 Saldo forfalt 31 - 60 dager
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_EuroAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' S14 Fortegn for S13, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If

                                            'S15 Saldo forfalt 61 - 90 dager
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_ChargesAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' S16 Fortegn for S15, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If

                                            'S17 Saldo forfalt over 90 dager
                                            sLine = sLine & Replace(VB6.Format(oPayment.MON_OriginallyPaidAmount / 100, "######.00"), ",", sDecimalSign) & sDelimiter

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' S18 Fortegn for S17, not in use for csv
                                                sLine = sLine & sDelimiter
                                            End If

                                            If sSpecial <> "SG_REVISJON_SALDO" Then
                                                ' 21.04.2009
                                                ' Revisjonsrapport saldo does not use all columns

                                                'S19 Gj.snitt betalingstid
                                                sLine = sLine & PadLeft(oPayment.ExtraI2, 3, " ") & sDelimiter

                                                'S20 Kreditlimit
                                                ' not in use
                                                sLine = sLine & sDelimiter

                                                'S21 Sperrekode
                                                ' not in use
                                                sLine = sLine & sDelimiter

                                                'S22 Siste purredato
                                                ' 05.03.2008 added!
                                                sLine = sLine & oPayment.ExtraI3 & sDelimiter

                                                'S23 Antall P3
                                                ' 05.03.2008 added!
                                                sLine = sLine & oPayment.ExtraD1 & sDelimiter

                                                'S24 Antall inkasso
                                                ' 05.03.2008 added!
                                                sLine = sLine & oPayment.ExtraD2 & sDelimiter

                                            End If

                                            'S25 Kreditrating
                                            ' not in use
                                            ' in use from 03.03.2009
                                            sLine = sLine & oPayment.ExtraD3 & sDelimiter

                                            If sSpecial = "SG_REVISJON_SALDO" Then
                                                ' Eng.nr.
                                                sLine = sLine & oPayment.REF_Own
                                            End If

                                            'XNET 08.05.2012 Added new field Rentefakturaer for Venistaal (6401)
                                            If sSpecial = "SG_SALDO_6401" Then
                                                ' Rentefakturaer
                                                sLine = sLine & sDelimiter & Replace(Format(oPayment.MON_AccountExchRate, "######.00"), ",", sDecimalSign) & sDelimiter
                                            End If

                                        End If

                                        ' 2008.03.12 Removed Currency and Agreementnumber for CSV (in place for Tab and Fixed)
                                        'XokNET 15.06.2011 - PROBLEMS with characterset at SG Basefarm
                                        ' translates some specialchars to ?
                                        ' BUT instr does not recognize then as "?", so we must scan the string for asc(63),
                                        ' and if found, replace this with nothing
                                        ' If not, oFile.WriteLine BOMBS!
                                        For lCounter = 1 To Len(sLine)
                                            If lCounter > Len(sLine) Then
                                                Exit For
                                            End If
                                            If Asc(Mid(sLine, lCounter, 1)) = 63 Then
                                                ' replace with ?
                                                sLine = Mid(sLine, 1, lCounter - 1) & "?" & Mid(sLine, lCounter + 1)
                                            End If
                                        Next lCounter

                                        oFile.WriteLine(sLine) '& vbCrLf

                                        oPayment.Exported = True
                                    End If
                                End If

                            Next oPayment ' oPayment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSGFinans_SaldoKunde" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSGFinans_SaldoKunde = True

    End Function
    Function WriteSGFinans_FakturaHistorikkKunde(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef iFiletype As BabelFiles.FileType) As Boolean
        ' For SG Finans
        ' Creates a ;-separated CSV-file for FakturaHistorikkfile clientreporting to mail
        '--------------------------------------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim lCounter, i As Integer
        Dim sTmp As String
        Dim sTmpDate As String
        Dim sDelimiter As String
        Dim sSurround As String
        Dim sDecimalSign As String

        Try

            lCounter = 0

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            ' decimalsign for all reports BUT revisionreport is . (point)
            sDecimalSign = "."

            If iFiletype = BabelFiles.FileType.SG_FakturaHistorikFil_Aq Then
                ' Revisjonsrapport, Open Items
                ' ; separated, no headerline
                sDelimiter = ";"
                sSurround = "" ' No surround
                ' decimalsign for revisionreport is , (comma)
                sDecimalSign = ","

                '28.07.09 moved header down

            ElseIf iFiletype = BabelFiles.FileType.SG_FakturaFil_Aq_CSV Then
                ' ; separated, no headerline
                sDelimiter = ";"
                sSurround = "" ' No surround
            Else
                ' TAB-delimited format, with headerline
                sDelimiter = Chr(9)
                sSurround = Chr(34) ' " " as surround for TAB

                ' First, create a headerline; (for TAB-sep only)
                '-----------------------------------------------
                sLine = "Debitornr" & Chr(9) & "Navn" & Chr(9) & "Fakturanr" & Chr(9) & "Type" & Chr(9) & "Fakturadato" & Chr(9) & "Forfallsdato" & Chr(9) & "Siste purredato" & Chr(9) & "Inkassodato" & Chr(9) & "Purrekode" & Chr(9) & "Fin.kode" & Chr(9) & "Purrestopp" & Chr(9) & "Selgernr (K)" & Chr(9) & "Selgernr (F)" & Chr(9) & "Prosjekt" & Chr(9) & "Fakturabel�p" & Chr(9) & "Restbel�p"
                oFile.WriteLine(sLine)
            End If

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                'If oPayment.I_Account = sI_Account Then
                                ' 01.10.2018 � changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                If iFiletype = BabelFiles.FileType.SG_FakturaHistorikFil_Aq Then
                                    ' Revisjonsrapport, Open Items
                                    ' Fileheading
                                    sLine = "Nordea Finance AS, org.nr. 987 664 398"
                                    oFile.WriteLine(sLine)

                                    sLine = "�pen post liste"
                                    oFile.WriteLine(sLine)
                                    sLine = "Kunde " & sClientNo & " " & oPayment.I_Name
                                    oFile.WriteLine(sLine)
                                    ' find last day in previous month;
                                    'sTmp = Str(DateSerial(Year(Date), Month(Date), 1) - 1)
                                    'sLine = "Periode per " & sTmp
                                    ' changed 04.11.2009 - take period from recordset
                                    sLine = "Periode per " & Trim(Str(oBabel.Period))

                                    oFile.WriteLine(sLine)
                                    sLine = "Dato for datauttrekk: " & oPayment.VoucherNo
                                    oFile.WriteLine(sLine)
                                    sLine = "Produksjonsdato: " & VB6.Format(Date.Today, "dd.mm.yyyy")
                                    oFile.WriteLine(sLine)
                                    sLine = "" 'blank linje
                                    oFile.WriteLine(sLine)

                                    ' Columnheading
                                    ' 16.12.2009 added e_orgno
                                    sLine = "Debitornr" & sDelimiter & "Org nr." & sDelimiter & "Navn" & sDelimiter & "Dok.nr" & sDelimiter & "Dok.dato" & sDelimiter & "Ff-dato" & sDelimiter & "Siste purredato" & sDelimiter & "Ink.dato" & sDelimiter & "Purrestatus" & sDelimiter & "Fin.kode" & sDelimiter & "Selgernr fakt" & sDelimiter & "Fakt.bel�p" & sDelimiter & "Restbel�p" & sDelimiter & "Engasjementsnr"
                                    oFile.WriteLine(sLine)
                                End If
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If


                                If bExportoPayment And oPayment.OmitExport = False Then
                                    If oPayment.PayCode = "705" Then ' 705 = FakturaHistorikkposter
                                        ' Assumes only one invoice pr payment

                                        lCounter = lCounter + 1
                                        sLine = ""

                                        'F3 Kundenr
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().CustomerNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & sSurround & oPayment.Invoices(1).CustomerNo & sSurround & sDelimiter

                                        ' 16.12.2009 - new column, OrgID
                                        If oBabel.Special = "SG_REVISJON_OPENITEMS" Then
                                            sLine = sLine & sSurround & oPayment.e_OrgNo & sSurround & sDelimiter
                                        End If

                                        'F4 DebitorName
                                        sLine = sLine & sSurround & Trim(oPayment.E_Name) & sSurround & sDelimiter

                                        'F5 FakturaNo
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().InvoiceNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & sSurround & oPayment.Invoices(1).InvoiceNo & sSurround & sDelimiter

                                        If oBabel.Special <> "SG_REVISJON_OPENITEMS" Then
                                            'F6 Type 0=Faktura, 3=Kreditnota, 4=Rentenota, 5=Inn/utbetaling
                                            sLine = sLine & sSurround & oPayment.ExtraD1 & sSurround & sDelimiter
                                        End If

                                        'F7 Fakturadato, DD.MM.YY
                                        ' 09.04.2008 - Changed to YYMMDD according to documentation
                                        If oPayment.DATE_Payment > "20070101" Then
                                            If oBabel.Special = "SG_REVISJON_OPENITEMS" Then
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.DATE_Payment)), "DD.MM.YYYY") & sSurround & sDelimiter
                                            Else
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.DATE_Payment)), "YYMMDD") & sSurround & sDelimiter
                                            End If
                                        Else
                                            sLine = sLine & sSurround & "" & sSurround & sDelimiter
                                        End If
                                        'F8 Forfalsdato, YYMMDD
                                        ' 09.04.2008 - Changed to YYMMDD according to documentation
                                        If oPayment.DATE_Value > "20070101" Then
                                            If oBabel.Special = "SG_REVISJON_OPENITEMS" Then
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.DATE_Value)), "DD.MM.YYYY") & sSurround & sDelimiter
                                            Else
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.DATE_Value)), "YYMMDD") & sSurround & sDelimiter
                                            End If
                                        Else
                                            sLine = sLine & sSurround & "" & sSurround & sDelimiter
                                        End If

                                        'F9 Siste purredato
                                        ' 09.04.2008 - Changed to YYMMDD according to documentation
                                        If oPayment.ERA_Date > "20070101" Then
                                            If oBabel.Special = "SG_REVISJON_OPENITEMS" Then
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.ERA_Date)), "DD.MM.YYYY") & sSurround & sDelimiter
                                            Else
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.ERA_Date)), "YYMMDD") & sSurround & sDelimiter
                                            End If
                                        Else
                                            sLine = sLine & sSurround & "" & sSurround & sDelimiter
                                        End If

                                        'F10 Inkassodato
                                        ' 09.04.2008 - Changed to YYMMDD according to documentation
                                        If oPayment.ExtraD2 > "20070101" Then
                                            If oBabel.Special = "SG_REVISJON_OPENITEMS" Then
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.ExtraD2)), "DD.MM.YYYY") & sSurround & sDelimiter
                                            Else
                                                sLine = sLine & sSurround & VB6.Format(StringToDate((oPayment.ExtraD2)), "YYMMDD") & sSurround & sDelimiter
                                            End If
                                        Else
                                            sLine = sLine & sSurround & "" & sSurround & sDelimiter
                                        End If

                                        'F11 Purrekode 0=Ikke purret, 1-3=Purrebrev #, 4=Sent inkasso.
                                        sLine = sLine & sSurround & oPayment.ExtraD3 & sSurround & sDelimiter

                                        'F12 Finansieringskode 0=Finansierbar, 1=IFF, 2=IFF p.g.a. alder.
                                        sLine = sLine & sSurround & oPayment.ExtraD4 & sSurround & sDelimiter

                                        If oBabel.Special <> "SG_REVISJON_OPENITEMS" Then
                                            'F13 Purrestopp 0=Ikke purrestopp/reklamasjon,3=Stoppet rente-beregning,5=Skal sendes til inkasso,6=Reklamasjon m/renteberegning,7=Reklamasjon u/renteberegning,8=Purrestopp m/renteberegning,9=Purrestopp u/renteberegning
                                            sLine = sLine & sSurround & oPayment.ExtraD5 & sSurround & sDelimiter
                                        End If

                                        If oBabel.Special <> "SG_REVISJON_OPENITEMS" Then
                                            'F14 Selgernr kont
                                            sLine = sLine & sSurround & oPayment.REF_Bank1 & sSurround & sDelimiter
                                        End If

                                        'F15 Selgernr faktura
                                        sLine = sLine & sSurround & oPayment.REF_Bank2 & sSurround & sDelimiter

                                        If oBabel.Special <> "SG_REVISJON_OPENITEMS" Then
                                            'F16 Prosjektnr
                                            sLine = sLine & sSurround & oPayment.REF_Own & sSurround & sDelimiter
                                        End If

                                        'F17 Fakturabel�p
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().MON_InvoiceAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & Replace(VB6.Format(oPayment.Invoices(1).MON_InvoiceAmount / 100, "#####.00"), ",", sDecimalSign) & sDelimiter

                                        'F18 Restbel�p
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().MON_TransferredAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = sLine & Replace(VB6.Format(oPayment.Invoices(1).MON_TransferredAmount / 100, "#####.00"), ",", sDecimalSign)

                                        If oBabel.Special = "SG_REVISJON_OPENITEMS" Then
                                            'For Revisjonsrapport: DebtorNumber
                                            sLine = sLine & sDelimiter & sSurround & oPayment.REF_Own & sSurround & sDelimiter
                                            ' added 03.11.2009
                                            'F20 Org.no receiver
                                            ' removed 16.12.2009
                                            'sLine = sLine & sSurround & PadLeft(oPayment.e_OrgNo, 11, "0") & sSurround

                                        End If
                                        'XokNET 15.06.2011 - PROBLEMS with characterset at SG Basefarm
                                        ' translates some specialchars to ?
                                        ' BUT instr does not recognize then as "?", so we must scan the string for asc(63),
                                        ' and if found, replace this with nothing
                                        ' If not, oFile.WriteLine BOMBS!
                                        For lCounter = 1 To Len(sLine)
                                            If lCounter > Len(sLine) Then
                                                Exit For
                                            End If
                                            If Asc(Mid(sLine, lCounter, 1)) = 63 Then
                                                ' replace with ?
                                                sLine = Mid(sLine, 1, lCounter - 1) & "?" & Mid(sLine, lCounter + 1)
                                            End If
                                        Next lCounter

                                        oFile.WriteLine(sLine) '& vbCrLf

                                        oPayment.Exported = True
                                    End If
                                End If

                            Next oPayment ' oPayment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSGFinans_FakturaHistorikkKunde" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSGFinans_FakturaHistorikkKunde = True

    End Function
    Function WriteDnBNORFinans_CREMUL(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef sOwnRef As String, ByRef bLog As Boolean, ByRef oBabelLog As vbLog.vbLogging, ByRef sSpecial As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice, oInvoice2 As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim oNewFreetext As vbBabel.Freetext
        Dim sFreetext As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim sOldDate, sOldAccount, sOldCurrency As String
        Dim nAmount As Double
        Dim sAmountToUse, sLastCharacter As String
        Dim sMessageNo As String
        Dim lFreetextCounter As Integer
        Dim sTemp As String
        Dim lCounter As Integer
        Dim sDateValue, sDatePayment As String
        Dim sNewOwnref As String
        Dim sDateTimeStamp As String
        Dim lPaymentCounter As Integer
        Dim bErrorInThisFunction As Boolean ' True = Error in this function, False = Error in function called from this function
        Dim bLindorff As Boolean
        Dim dInvoiceCounter As Double
        Dim bCreateOnlyOneRecordtype1 As Boolean

        Try

            bErrorInThisFunction = True

            lPaymentCounter = 0
            dInvoiceCounter = 0
            sDateValue = ""
            sDatePayment = ""

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled
                            If oPayment.Cancel = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects that shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                'If bExportoPayment Then
                                '    If IsOCR(oPayment.PayCode) Then
                                '        bExportoPayment = False
                                '        oPayment.Exported = True
                                '    End If
                                'End If

                                'For domestic payments 629
                                '                    If oPayment.PayCode = "629" Then
                                '                        For Each oInvoice In oPayment.Invoices
                                '                            If oInvoice.TypeOfStructuredInfo = StructureTypeKID Then
                                '                                bExportoPayment = False
                                '                                oPayment.Exported = True
                                '                                Exit For
                                '                            End If
                                '                        Next oInvoice
                                '                    End If

                                'For domestic payments KID + 629 which contains KID
                                '                    If oPayment.PayCode = "629" Then
                                '                        bExportoPayment = False
                                '                        oPayment.Exported = True
                                '                        For Each oInvoice In oPayment.Invoices
                                '                            If oInvoice.TypeOfStructuredInfo = StructureTypeKID Then
                                '                                bExportoPayment = True
                                '                                Exit For
                                '                            End If
                                '                        Next oInvoice
                                '                    End If

                                If bExportoPayment Then

                                    oPayment.Exported = True

                                    lPaymentCounter = lPaymentCounter + 1
                                    'If Not RunTime() Then
                                    'sDateTimeStamp = "2009-05-08-12.01.54" & "." & PadLeft(Trim(Str(lPaymentCounter)), 6, "0")
                                    'Else
                                    sDateTimeStamp = VB6.Format(Now, "YYYY-MM-DD-hh.mm.ss") & "." & PadLeft(Trim(Str(lPaymentCounter)), 6, "0")
                                    'End If

                                    'If we have zeroamount invoices don't create more than one record 1
                                    bCreateOnlyOneRecordtype1 = False
                                    If oPayment.Invoices.Count > 1 Then
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MON_InvoiceAmount = 0 Then
                                                If EmptyString(oInvoice.Unique_Id) And EmptyString(oInvoice.InvoiceNo) Then
                                                    bCreateOnlyOneRecordtype1 = True
                                                    Exit For
                                                End If
                                            End If
                                        Next oInvoice
                                    End If

                                    If bCreateOnlyOneRecordtype1 Then
                                        'Remove all but 1 invoice. Keep all freetext
                                        For lCounter = oPayment.Invoices.Count To 2 Step -1
                                            If Not oPayment.Invoices.Item(lCounter) Is Nothing Then
                                                For Each oFreeText In oPayment.Invoices.Item(lCounter).Freetexts
                                                    oNewFreetext = oPayment.Invoices.Item(1).Freetexts.Add
                                                    oNewFreetext.Qualifier = oFreeText.Qualifier
                                                    oNewFreetext.Text = oFreeText.Text
                                                Next oFreeText
                                                oPayment.Invoices.Remove((lCounter))
                                            End If
                                        Next lCounter
                                        oPayment.Invoices.Item(1).MON_TransferredAmount = oPayment.MON_TransferredAmount
                                        oPayment.Invoices.Item(1).MON_InvoiceAmount = oPayment.MON_InvoiceAmount
                                        If oPayment.MATCH_PossibleNumberOfInvoices > 0 Then
                                            oPayment.Invoices.Item(1).TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoiceRetrievedFromMessage
                                        Else
                                            oPayment.Invoices.Item(1).TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.NotStructured
                                        End If
                                    End If

                                    For Each oInvoice In oPayment.Invoices

                                        '18.02.2009 - Special treatment of in-payments from LINDORFF -
                                        '  THEY ARE MARKED/MATCHED IN AUTOBOOK
                                        If oInvoice.CustomerNo = "LINDORFF" Then
                                            'First, remove some information added in autobook
                                            oPayment.MATCH_HowMatched = ""
                                            oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched
                                            oInvoice.MATCH_ID = ""
                                            oInvoice.MATCH_Matched = False
                                            'oInvoice.CustomerNo = ""

                                            bErrorInThisFunction = False
                                            bLindorff = RetrieveLindorffInfo(oPayment)
                                            bErrorInThisFunction = True
                                        Else
                                            bLindorff = False
                                        End If

                                        sAmountToUse = Trim(TranslateToSignedAmount(oInvoice.MON_InvoiceAmount, "DNBNORFINANS"))

                                        sLine = ""

                                        bErrorInThisFunction = False
                                        dInvoiceCounter = dInvoiceCounter + 1
                                        sLine = WriteDnBNORFinansRecordtype1(oBatch, oPayment, oInvoice, sAmountToUse, sDateTimeStamp, dInvoiceCounter)
                                        bErrorInThisFunction = True
                                        oFile.WriteLine(sLine)

                                        sLine = ""

                                        bErrorInThisFunction = False
                                        dInvoiceCounter = dInvoiceCounter + 1
                                        sLine = WriteDnBNORFinansRecordtype2(oPayment, oInvoice, sAmountToUse, sDateTimeStamp, dInvoiceCounter, False, "")
                                        bErrorInThisFunction = True
                                        If Not EmptyString(sLine) Then
                                            oFile.WriteLine(sLine)
                                        Else
                                            dInvoiceCounter = dInvoiceCounter - 1
                                        End If

                                        'If oPayment.PayCode = "601" Or oPayment.PayCode = "510" Then
                                        ' 28.08.2019 - MAJOR DIFFERENCES IN PAYCODES between Cremul and Camt.054.
                                        '               We have to open for all paycodes here ........
                                        '               THUS - have removed the above If
                                        If oPayment.Invoices.Count = 1 Then
                                            If oPayment.Invoices.Item(1).CustomerNo <> "LINDORFF" Then
                                                If oPayment.Invoices.Item(1).TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoiceRetrievedFromMessage Then
                                                    oPayment.MATCH_FirstInvoice()
                                                    For lCounter = 1 To oPayment.MATCH_PossibleNumberOfInvoices
                                                        If lCounter > 1 Then
                                                            oPayment.MATCH_NextInvoice()
                                                        End If
                                                        dInvoiceCounter = dInvoiceCounter + 1
                                                        sLine = WriteDnBNORFinansRecordtype2(oPayment, oInvoice, sAmountToUse, sDateTimeStamp, dInvoiceCounter, True, oPayment.MATCH_InvoiceNumber)
                                                        oFile.WriteLine(sLine)
                                                    Next lCounter
                                                End If
                                            End If
                                        ElseIf oPayment.Invoices.Count > 1 Then
                                            '27.05.2009 - Must treat the payments with several invoices specific
                                            If oInvoice.CustomerNo <> "LINDORFF" Then
                                                If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoiceRetrievedFromMessage Then
                                                    oPayment.MATCH_FirstInvoice()
                                                    For lCounter = 1 To oPayment.MATCH_PossibleNumberOfInvoices
                                                        If CDbl(oPayment.MATCH_InvoiceIndex) = oInvoice.Index Then
                                                            dInvoiceCounter = dInvoiceCounter + 1
                                                            sLine = WriteDnBNORFinansRecordtype2(oPayment, oInvoice, sAmountToUse, sDateTimeStamp, dInvoiceCounter, True, oPayment.MATCH_InvoiceNumber)
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                        oPayment.MATCH_NextInvoice()
                                                    Next lCounter
                                                End If
                                            End If
                                        End If
                                        'End If

                                        sFreetext = ""
                                        sLine = ""

                                        For Each oFreeText In oInvoice.Freetexts
                                            'sFreetext = sFreetext & oFreeText.Text
                                            ' 22.08.2019 - changed freetext, as we do not populate Faktnr: and Bel�p: in Camt.054 import
                                            If Not EmptyString(oInvoice.Unique_Id) Then
                                                ' 25.11.2019 - do our best for not adding Bel�p: twice
                                                If InStr(sFreetext, "Bel�p:") = 0 And oInvoice.Freetexts.Count = 1 Then
                                                    sFreetext = sFreetext & oFreeText.Text & " Bel�p: " & PadLeft(ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ","), 18, " ")
                                                Else
                                                    sFreetext = sFreetext & oFreeText.Text
                                                End If
                                            Else
                                                If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice Then
                                                    ' 25.11.2019 - Faktnr: and Bel�p already added in Camt.054C import for structured
                                                    'sFreetext = sFreetext & "Faktnr: " & oFreeText.Text & " Bel�p: " & PadLeft(ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ","), 18, " ")
                                                    ' 25.11.2019 - in some cases we have already added "Faktnr" - do not add twice
                                                    'If InStr(oFreeText.Text, "Faktnr:") = 0 Then
                                                    '    sFreetext = sFreetext & "Faktnr: " & oFreeText.Text
                                                    'Else
                                                    '    sFreetext = sFreetext & " " & oFreeText.Text
                                                    'End If
                                                    'If InStr(sFreetext, "Bel�p:") = 0 Then
                                                    '    sFreetext = sFreetext & " Bel�p: " & PadLeft(ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ","), 18, " ")
                                                    'Else
                                                    '    sFreetext = sFreetext & " " & PadLeft(ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ","), 18, " ")
                                                    'End If
                                                    sFreetext = sFreetext & " " & oFreeText.Text

                                                ElseIf oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID Then
                                                    sFreetext = sFreetext & "KID: " & oFreeText.Text & " Bel�p: " & PadLeft(ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ","), 18, " ")
                                                Else
                                                    ' 18.09.2019 - add Bel�p only once!
                                                    If oFreeText.Index = oInvoice.Freetexts.Count Then
                                                        sFreetext = sFreetext & oFreeText.Text & " Bel�p: " & PadLeft(ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ","), 18, " ")
                                                    Else
                                                        sFreetext = sFreetext & oFreeText.Text
                                                    End If
                                                End If
                                            End If
                                        Next oFreeText

                                        Do While Not EmptyString(sFreetext)

                                            If Not EmptyString(sFreetext) Then
                                                sFreetext = ChangeCharactersDnBNORFinans(sFreetext)
                                                bErrorInThisFunction = False
                                                'THORESEN Write recordtype 2
                                                'sLine = WriteDnBNORFinansRecordtype3Old(oPayment, oInvoice)
                                                dInvoiceCounter = dInvoiceCounter + 1
                                                sLine = WriteDnBNORFinansRecordtype3(oPayment, sDateTimeStamp, dInvoiceCounter, Left(sFreetext, 350))
                                                bErrorInThisFunction = True
                                                oFile.WriteLine(sLine)
                                                sFreetext = Mid(sFreetext, 350)
                                            End If
                                        Loop

                                    Next oInvoice

                                    '''''                        End If 'If bCreateOnlyOneRecordtype1 Then
                                Else
                                    'Don't export

                                End If

                            Next oPayment

                        End If

                    Next oBatch


                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDnBNORFinans_CREMUL" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDnBNORFinans_CREMUL = True

    End Function
    Private Function WriteDnBNORFinansStartOfRecord(ByRef oPayment As vbBabel.Payment, ByRef sDateTimeStamp As String, ByRef dInvoiceCounter As Double, ByRef sTransType As String) As String
        Dim sLine As String

        'On Error GoTo errWriteDnBNORFinansStartOfRecord

        sLine = ""

        sLine = Trim(oPayment.MON_InvoiceCurrency) & " " 'Payment currency X(3) 1-3
        sLine = sLine & PadRight(oPayment.I_Account, 35, " ") & " " 'FF-Bank-Acct-no X(35) 5-39
        sLine = sLine & sDateTimeStamp & " " 'Timestamp X(26) 41-66
        sLine = sLine & PadLeft(Trim(Str(dInvoiceCounter)), 9, "0") & " " 'DOC-SEQ-no 9(9) 68-76
        sLine = sLine & sTransType & " " 'Record-type X 78-78

        WriteDnBNORFinansStartOfRecord = sLine

        '        Exit Function

        'errWriteDnBNORFinansStartOfRecord:

        '        Err.Raise(Err.Number, "WriteDnBNORFinansStartOfRecord", Err.Description)

        '        WriteDnBNORFinansStartOfRecord = ""

    End Function
    Private Function WriteDnBNORFinansRecordtype1(ByRef oBatch As vbBabel.Batch, ByRef oPayment As vbBabel.Payment, ByRef oInvoice As vbBabel.Invoice, ByRef sAmountToUse As String, ByRef sDateTimeStamp As String, ByRef dInvoiceCounter As Double) As String
        Dim sLine As String
        Dim sDateValue As String
        Dim sDatePayment As String
        Dim bErrorInThisFunction As Boolean
        Dim sTransType As String

        'On Error GoTo errWriteDnBNORFinansRecordtype1
        sLine = ""
       
        bErrorInThisFunction = False
        sLine = WriteDnBNORFinansStartOfRecord(oPayment, sDateTimeStamp, dInvoiceCounter, "1") '1-79
        bErrorInThisFunction = True

        If Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "NO" Then
            If oPayment.PayType = "I" Then
                sTransType = "60"
            Else
                Select Case oPayment.PayCode

                    Case "510" 'KID
                        ' 18.11.2019 
                        ' Etter samtale med Oddbj�rn s� bestemmes det at det aldri skal bli Type = 30 p� betalinger fra DNB, kun Nets
                        If oPayment.I_AccountType <> BabelFiles.AccountType.NO_Nets Then
                            sTransType = "50"
                        Else
                            sTransType = "30"
                            iType30 = iType30 + 1
                        End If

                    Case "511" 'Set in TreatSpecial. Originally a 629, but all invoices has a correct KID
                        sTransType = "40" 'Added 08.05.2009
                        iType40 = iType40 + 1

                    Case "518" 'Wrong KID
                        sTransType = "31"

                    Case "611" 'Autogiro
                        sTransType = "32"

                    Case "601" 'Electronic payments
                        ' 03.09.2019 - added use of oPayment.I_accountType = "NETS"
                        'If oPayment.Cargo.Bank = BabelFiles.Bank.DnB Then
                        If oPayment.I_AccountType <> BabelFiles.AccountType.NO_Nets Then
                            sTransType = "50"
                        Else
                            sTransType = "33"
                        End If

                    Case "602" 'Manual payments
                        sTransType = "34"

                    Case "629" 'Structured payment
                        '20.05.2009 - Next IF new
                        ' 03.09.2019 - added use of oPayment.I_accountType = "NETS"
                        'If oPayment.Cargo.Bank = BabelFiles.Bank.DnB Then
                        If oPayment.I_AccountType <> BabelFiles.AccountType.NO_Nets Then
                            sTransType = "50"
                        Else
                            If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice Or oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoiceRetrievedFromKID Or oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoiceRetrievedFromMessage Then
                                '15.05.2009 - Changed after instructions from DnBNOR Finans, Eva Theodorsen
                                sTransType = "40"
                                'sTransType = "41"
                                iType40 = iType40 + 1
                            Else
                                sTransType = "40"
                                iType40 = iType40 + 1
                            End If
                        End If

                    Case Else
                        ' Nytt 30.08.2019
                        ' Her kommer "resten" av de elektroniske betalingene.
                        ' 03.09.2019 - added use of oPayment.I_accountType = "NETS"
                        'If oPayment.Cargo.Bank = BabelFiles.Bank.DnB Then
                        If oPayment.I_AccountType <> BabelFiles.AccountType.NO_Nets Then
                            sTransType = "50"
                        Else
                            sTransType = "33"
                        End If

                End Select
            End If

        ElseIf Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "SE" Then
            If EmptyString(oInvoice.Unique_Id) Then
                '15.05.2009 - Changed after instructions from DnBNOR Finans, Eva Theodorsen
                sTransType = "70" 'Transtype 9(2) Last to digits, 70 = Sweden
                'sTransType = "71" 'Transtype 9(2) Last to digits, 71 = Sweden
            Else
                sTransType = "70" 'Transtype 9(2) Last to digits, 70 = OCR Sweden
            End If
        ElseIf Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "DK" Then
            sTransType = "60"
        ElseIf Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "FI" Then
            sTransType = "60"
        ElseIf Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "DE" Then
            ' 16.08.2019 - added Germany (ref Oddbj�rn Pedersen)
            sTransType = "60"
        ElseIf Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "GB" Then
            ' 16.08.2019 - added Germany (ref Oddbj�rn Pedersen)
            sTransType = "60"
        Else
            sTransType = "60"
        End If

        sLine = sLine & PadLeft(sTransType, 3, " ") & " " 'Trans-type 9(3), 80-82
        sDateValue = Left(oPayment.DATE_Value, 4) & "-" & Mid(oPayment.DATE_Value, 5, 2) & "-" & Right(oPayment.DATE_Value, 2)
        sDatePayment = Left(oPayment.DATE_Payment, 4) & "-" & Mid(oPayment.DATE_Payment, 5, 2) & "-" & Right(oPayment.DATE_Payment, 2)
        sLine = sLine & sDateValue & " " 'BBS-Date X(10) YYYY-MM-DD, 84-93
        sLine = sLine & sDatePayment & " " 'Bank-Alloc-Date X(10) YYYY-MM-DD, 95-104
        sLine = sLine & sDateValue & " " 'Item-Value-Date X(10) YYYY-MM-DD, 106-115
        'Next line changed due to structured info
        sLine = sLine & PadLeft(sAmountToUse, 15, CStr(0)) & " " 'Payment-Amount s9(13)V9(2), 117-131
        '18.02.2009 Changed from invoiceamount to OriginalAmount and Currency - Next 2
        sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(oPayment.MON_OriginallyPaidAmount, "DNBNORFINANS")), 15, CStr(0)) & " " 'Bank-Amount s9(13)V9(2), 133-147
        sLine = sLine & PadLeft(oPayment.MON_OriginallyPaidCurrency, 3, " ") & " " 'ItemCurrency X(3), 149-151
        ' 27.09.2019 added next If
        ' 17.10.2019 - removed, ref Oddbj�rn Pedersen
        'If oPayment.MON_AccountExchRate = 0 Then
        '    oPayment.MON_AccountExchRate = 1
        'End If
        sLine = sLine & PadLeft(CStr(oPayment.MON_AccountExchRate), 11, CStr(0)) & " " 'Rate of exchange s9(6)V9(5), 153-163
        sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(oPayment.MON_ChargesAmount, "DNBNORFINANS")), 13, CStr(0)) & " " 'Bank-charge s9(11)V9(2), 165-177
        sLine = sLine & PadRight(oPayment.E_Account, 35, " ") & " " 'Pay-bank-acct-no X(35) 179-213
        If oInvoice.CustomerNo = "LINDORFF" Then
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 4)) Then
                sLine = sLine & PadRight(xDelim(oInvoice.Extra1, ";", 4), 8, " ") & " " ' By-acctno X(8) 215-222 Debitornr fra Lindorff fra fritekst eller hentes fra KID (se mappingfil)
            Else
                sLine = sLine & Space(8) & " " ' By-acctno X(8) 215-222 Debitornr fra Lindorff fra fritekst eller hentes fra KID (se mappingfil)
            End If
        ElseIf Not EmptyString(oInvoice.MATCH_ClientNo) Then
            sLine = sLine & PadRight(oInvoice.MATCH_ClientNo, 8, " ") & " " ' By-acctno X(8) 215-222 Debitornr fra Lindorff fra fritekst eller hentes fra KID (se mappingfil)
        Else
            sLine = sLine & Space(8) & " " ' By-acctno X(8) 215-222 Debitornr fra Lindorff fra fritekst eller hentes fra KID (se mappingfil)
        End If
        sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.E_Name), 35, " ") & " " 'Pay-name X(35) 224-258
        sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.E_Adr1), 35, " ") & " " 'Pay-sup-adress1 X(35) 260-294
        sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.E_Adr2), 35, " ") & " " 'Pay-sup-adress2 X(35) 296-330
        sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.E_Adr3), 35, " ") & " " 'Pay-postal-adress X(35) 332-366
        sLine = sLine & PadRight(oPayment.E_Zip, 9, " ") & " " 'Pay-postcode X(9) 368-376
        sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.E_City), 35, " ") & " " 'Pay-city X(35) 378-412
        sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.E_CountryCode), 3, " ") & " " 'Pay-country X(3) 414-416
        sLine = sLine & PadRight(RemoveLeadingCharacters(oPayment.e_OrgNo, "0"), 17, " ") & " " 'Pay-enterp-no X(17) 418-434
        ' 27.09.2019 byttet fra REF_Bank2 til 1
        sLine = sLine & PadRight(oPayment.REF_Bank1, 25, " ") & " " 'Pay-reference X(25) 436-460
        '????????????????????? - OK inntil videre
        sLine = sLine & Space(25) & " " 'Payers-reference X(25) 462-486
        'New 16.03.2009 - next IF. Sometimes a 999 is stated from Sweden, nut it is no KID
        If oPayment.I_CountryCode = "SE" Then
            If EmptyString(oInvoice.Unique_Id) Then
                sLine = sLine & Space(25) & " " '488-512
            Else
                If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID Then
                    sLine = sLine & PadRight(oInvoice.Unique_Id, 25, " ") & " " 'By-id X(25) 488-512 KID
                Else
                    sLine = sLine & Space(25) & " " '488-512
                End If
            End If
        Else
            If EmptyString(oInvoice.Unique_Id) Then
                sLine = sLine & Space(25) & " " '488-512
            Else
                sLine = sLine & PadRight(oInvoice.Unique_Id, 25, " ") & " " 'By-id X(25) 488-512 KID
            End If
        End If
        If oInvoice.CustomerNo = "LINDORFF" Then
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 2)) Then
                sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(xDelim(oInvoice.Extra1, ";", 2)), 35, " ") & " " 'CL-Acctname X(35) 514-548
            Else
                sLine = sLine & Space(35) & " " 'CL-Acctname X(35) 514-548
            End If
        Else
            sLine = sLine & PadRight(ChangeCharactersDnBNORFinans(oPayment.I_Name), 35, " ") & " " 'CL-Acctname X(35) 514-548
        End If
        ' 27.09.2019 byttet fra REF_Bank1 til 2
        sLine = sLine & PadLeft(Right(Replace(oPayment.REF_Bank2, "*", ":"), 10), 10, "0") & " " 'Archive-ref 9(10) 550-559
        sLine = sLine & PadLeft(oBatch.REF_Bank, 15, "0") & " " 'Bank-booking-id 9(15) 561-575
        If oInvoice.CustomerNo = "LINDORFF" Then
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 5)) Then
                sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(CDbl(Replace(xDelim(oInvoice.Extra1, ";", 5), ",", "")), "DNBNORFINANS")), 13, "0") & " " 'Inkasso-hoved s9(11)V9(2) 577-589
            Else
                sLine = sLine & "0000000000000" & " " 'Inkasso-hoved s9(11)V9(2) 577-589
            End If
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 6)) Then
                sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(CDbl(Replace(xDelim(oInvoice.Extra1, ";", 6), ",", "")), "DNBNORFINANS")), 13, "0") & " " 'Inkasso-rente s9(11)V9(2) 591-603
            Else
                sLine = sLine & "0000000000000" & " " 'Inkasso-rente s9(11)V9(2) 591-603
            End If
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 7)) Then
                sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(CDbl(Replace(xDelim(oInvoice.Extra1, ";", 7), ",", "")), "DNBNORFINANS")), 13, "0") & " " 'Inkasso-prov s9(11)V9(2) 605-617
            Else
                sLine = sLine & "0000000000000" & " " 'Inkasso-prov s9(11)V9(2) 605-617
            End If
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 8)) Then
                sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(CDbl(Replace(xDelim(oInvoice.Extra1, ";", 8), ",", "")), "DNBNORFINANS")), 13, "0") & " " 'Inkasso-gebyr s9(11)V9(2) 619-631
            Else
                sLine = sLine & "0000000000000" & " " 'Inkasso-gebyr s9(11)V9(2) 619-631
            End If
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 9)) Then
                sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(CDbl(Replace(xDelim(oInvoice.Extra1, ";", 9), ",", "")), "DNBNORFINANS")), 13, "0") & " " 'Inkasso-mva s9(11)V9(2) 633-645
            Else
                sLine = sLine & "0000000000000" & " " 'Inkasso-mva s9(11)V9(2) 633-645
            End If
            If Not EmptyString(xDelim(oInvoice.Extra1, ";", 1)) Then
                sLine = sLine & PadRight(Replace(xDelim(oInvoice.Extra1, ";", 1), ",", ""), 9, " ") & " " 'Inkasso-saksnr X(9) 647-655
            Else
                sLine = sLine & "         " & " " 'Inkasso-saksnr X(9) 647-655
            End If

        Else
            sLine = sLine & "0000000000000" & " " 'Inkasso-hoved s9(11)V9(2) 577-589
            sLine = sLine & "0000000000000" & " " 'Inkasso-rente s9(11)V9(2) 591-603
            sLine = sLine & "0000000000000" & " " 'Inkasso-prov s9(11)V9(2) 605-617
            sLine = sLine & "0000000000000" & " " 'Inkasso-gebyr s9(11)V9(2) 619-631
            sLine = sLine & "0000000000000" & " " 'Inkasso-mva s9(11)V9(2) 633-645
            sLine = sLine & "         " & " " 'Inkasso-saksnr X(9) 647-655
        End If
        '18.02.2009 - Changed from space.
        sLine = sLine & PadLeft(Trim(TranslateToSignedAmount(oPayment.MON_InvoiceAmount, "DNBNORFINANS")), 15, CStr(0)) & " " 'Paym-Tot-Amount s9(13)V9(2), 657-671
        sLine = sLine & "  " & " " 'Branch X(2) 673-674
        sLine = sLine & " " & " " 'Diff-mark X(1) 676-676
        '13.03.2009 - added this IF-test. Before the field was a blank.
        If oPayment.Invoices.Count > 1 Then
            sLine = sLine & "J" & " " 'Konstruert X(1) 678-678
        Else
            sLine = sLine & "N" & " " 'Konstruert X(1) 678-678
        End If
        '20.04.2009 - changed
        If oInvoice.CustomerNo = "LINDORFF" Then
            sLine = sLine & PadLeft(Trim(Str(BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice)), 2, " ") 'Mapping-ind X(2) 680-681
        Else
            'If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice And EmptyString(oInvoice.InvoiceNo) Then
            ' 26.09.2019 Endret, for ogs� � f� med seg verdi = 0 der TypeOfStructuredInfo ikke er satt (er da -1)
            If oInvoice.TypeOfStructuredInfo <= BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice And EmptyString(oInvoice.InvoiceNo) Then
                sLine = sLine & PadLeft(Trim(Str(BabelFiles.TypeOfStructuredInfo.StructureTypeUnknown)), 2, " ") 'Mapping-ind X(2) 680-681
            Else
                sLine = sLine & PadLeft(Trim(Str(CDbl(oInvoice.TypeOfStructuredInfo))), 2, " ") 'Mapping-ind X(2) 680-681
            End If
        End If
        'sLine = sLine & "  " 'Mapping-ind X(2) 680-681

        WriteDnBNORFinansRecordtype1 = sLine

        '        Exit Function

        'errWriteDnBNORFinansRecordtype1:

        '        If bErrorInThisFunction Then
        '            Err.Raise(Err.Number, "WriteDnBNORFinansRecordtype1", Err.Description)
        '        Else
        '            Err.Raise(Err.Number, Err.Source, Err.Description)
        '        End If

        '        WriteDnBNORFinansRecordtype1 = CStr(False)


    End Function

    Private Function WriteDnBNORFinansRecordtype2(ByRef oPayment As vbBabel.Payment, ByRef oInvoice As vbBabel.Invoice, ByRef sAmountToUse As String, ByRef sDateTimeStamp As String, ByRef dInvoiceCounter As Double, ByRef bPossibleInvoice As Boolean, ByRef sPossibleInvoiceNumber As String) As String
        Dim sLine As String
        Dim oFreeText As vbBabel.Freetext
        Dim bErrorInThisFunction As Boolean
        '13.03.2009 - Added the use of bInfoWriiten. If no info written return a blank line
        Dim bInfoWritten As Boolean
        Dim sInvoiceNo As String

        'On Error GoTo errWriteDnBNORFinansRecordtype2

        bInfoWritten = False

        sLine = ""

        bErrorInThisFunction = False
        sLine = WriteDnBNORFinansStartOfRecord(oPayment, sDateTimeStamp, dInvoiceCounter, "2") ''1-79
        bErrorInThisFunction = True

        'New 16.03.2009 - next IF. Sometimes a 999 is stated from Sweden, nut it is no KID
        '02.04.2009 - Changed the first part of the IF
        If bPossibleInvoice Then
            'sLine = sLine & PadLeft(sPossibleInvoiceNumber, 8, "0") & " " 'Item-ID-NO X(8) 80-87
            'sLine = sLine & "00000" & " " 'Item-NO s9(5) 89-93
            'sLine = sLine & "0000000000000" & " " 'Item-Inv-Amount s9(11)V9(2) 95-107
            'sLine = sLine & "00000" & " " 'CL-Acctno X(5), 109-113
            'sLine = sLine & " " 'Paym-history X(1), 115-115
            '08.11.2017 - Fakturanummer utvidet til 20 tegn
            sLine = sLine & PadLeft(sPossibleInvoiceNumber, 20, "0") & " " 'Item-ID-NO X(8) 80-99
            sLine = sLine & "00000" & " " 'Item-NO s9(5) 101-105
            sLine = sLine & "0000000000000" & " " 'Item-Inv-Amount s9(11)V9(2) 107-119
            sLine = sLine & "00000" & " " 'CL-Acctno X(5), 121-125
            sLine = sLine & " " 'Paym-history X(1), 127-127

            WriteDnBNORFinansRecordtype2 = sLine
        Else
            If oPayment.I_CountryCode = "SE" Then
                '26.05.2009
                'New code - removed Or oInvoice.TypeOfStructuredInfo = StructureTypeInvoiceRetrievedFromMessage, because
                ' it created an recordtype 2 with no invoicenumber
                If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice Or oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoiceRetrievedFromKID Then
                    'Old code
                    'If oInvoice.TypeOfStructuredInfo = StructureTypeInvoice Or oInvoice.TypeOfStructuredInfo = StructureTypeInvoiceRetrievedFromKID Or oInvoice.TypeOfStructuredInfo = StructureTypeInvoiceRetrievedFromMessage Then

                    '25.03.2009 -Changed, Add zeroes at the front instead of spaces to the right
                    'sLine = sLine & PadLeft(oInvoice.InvoiceNo, 8, "0") & " " 'Item-ID-NO X(8) 80-87
                    '08.11.2017 - Fakturanummer utvidet til 20 tegn
                    sLine = sLine & PadLeft(oInvoice.InvoiceNo, 20, "0") & " " 'Item-ID-NO X(8) 80-99

                    'sLine = sLine & PadRight(oInvoice.InvoiceNo, 8, " ") & " " 'Item-ID-NO X(8) 80-87
                    bInfoWritten = True
                ElseIf EmptyString(oInvoice.Unique_Id) Then
                    'sLine = sLine & PadLeft(oInvoice.InvoiceNo, 8, "0") & " " 'Item-ID-NO X(8) 80-87
                    '08.11.2017 - Fakturanummer utvidet til 20 tegn
                    sLine = sLine & PadLeft(oInvoice.InvoiceNo, 20, "0") & " " 'Item-ID-NO X(8) 80-99

                    If Not EmptyString(oInvoice.InvoiceNo) Then
                        bInfoWritten = True
                    End If
                Else
                    If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeWrongKIDValidatedByBabelBank Then
                        '08.06.2009 - Changed after instructions from DnBNOR Finans
                        'Don't create a record 2 if no invoice is stated
                        If Len(oInvoice.Unique_Id) > 20 Then  '8 Then 08.11.2017 Changed from > 8 til > 20
                            oFreeText = oInvoice.Freetexts.Add
                            oFreeText.Text = Trim(oInvoice.Unique_Id)
                        Else
                            '25.03.2009 -Changed, Add zeroes at the front instead of spaces to the right
                            'sLine = sLine & PadLeft(oInvoice.InvoiceNo, 8, "0") & " " 'Item-ID-NO X(8) 80-87
                            '08.11.2017 - Fakturanummer utvidet til 20 tegn
                            sLine = sLine & PadLeft(oInvoice.InvoiceNo, 20, "0") & " " 'Item-ID-NO X(8) 80-99

                            'sLine = sLine & PadRight(oInvoice.Unique_Id, 8, " ") & " " 'Item-ID-NO X(8) 80-87
                        End If
                    ElseIf oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID Then
                        'sLine = sLine & PadLeft(oInvoice.InvoiceNo, 8, "0") & " " 'Item-ID-NO X(8) 80-87 Part of KID should be retrieved from aKIDDesc
                        '08.11.2017 - Fakturanummer utvidet til 20 tegn
                        sLine = sLine & PadLeft(oInvoice.InvoiceNo, 20, "0") & " " 'Item-ID-NO X(8) 80-99

                        bInfoWritten = True
                    Else
                        'sLine = sLine & "00000000" & " " ''Item-ID-NO X(8) 80-87
                        sLine = sLine & "00000000000000000000" & " " ''Item-ID-NO X(8) 80-99  ' 08.11.2017 Utvidet til 20
                    End If
                End If
            Else
                If oInvoice.TypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice Then

                    sInvoiceNo = CheckForValidCharacters(oInvoice.InvoiceNo, False, False, False, True, "") 'Remove all except digits
                    'If Len(sInvoiceNo) > 4 Then 'Must be at least 5 digits
                    If InStr(1, oInvoice.InvoiceNo, sInvoiceNo, CompareMethod.Text) > 0 Then 'Check if the digits are found in sequence in the original invoiceno
                        sInvoiceNo = sInvoiceNo 'Use the new invoiceno (or identical to the original if it consists of only digits)
                    Else
                        'Sometimes the year (probably) are stated at the end. Remove this and try again
                        If Right(oInvoice.InvoiceNo, 2) = VB6.Format(Now, "YY") Then
                            sInvoiceNo = CheckForValidCharacters(Left(oInvoice.InvoiceNo, Len(oInvoice.InvoiceNo) - 2), False, False, False, True, "") 'Remove all except digits
                            'If Len(sInvoiceNo) > 4 Then
                            If InStr(1, oInvoice.InvoiceNo, sInvoiceNo, CompareMethod.Text) > 0 Then
                                sInvoiceNo = sInvoiceNo
                            Else
                                sInvoiceNo = oInvoice.InvoiceNo
                            End If
                            'Else
                            '    sInvoiceNo = oInvoice.InvoiceNo
                            'End If
                        Else
                            sInvoiceNo = oInvoice.InvoiceNo
                        End If
                    End If
                    'Else
                    '    sInvoiceNo = oInvoice.InvoiceNo
                    'End If
                    'RemoveCharacters

                    '25.03.2009 -Changed, Add zeroes at the front instead of spaces to the right
                    'sLine = sLine & PadLeft(sInvoiceNo, 8, "0") & " " 'Item-ID-NO X(8) 80-87
                    sLine = sLine & PadLeft(sInvoiceNo, 20, "0") & " " 'Item-ID-NO X(8) 80-99  08.11.2017
                    'sLine = sLine & PadRight(oInvoice.InvoiceNo, 8, " ") & " " 'Item-ID-NO X(8) 80-87 Part of KID should be retrieved from aKIDDesc
                Else
                    'New 02.02.2009 - extracted in TreatSpecial
                    'sLine = sLine & PadLeft(oInvoice.InvoiceNo, 8, "0") & " " 'Item-ID-NO X(8) 80-87 Part of KID should be retrieved from aKIDDesc
                    ' 08.11.2017 to 20 pos
                    sLine = sLine & PadLeft(oInvoice.InvoiceNo, 20, "0") & " " 'Item-ID-NO X(8) 80 - 99 Part of KID should be retrieved from aKIDDesc
                    'sLine = sLine & Mid$(oInvoice.Unique_Id, 7, 8) 'Item-ID-NO X(8) 2-9 Part of KID should be retrieved from aKIDDesc
                End If
                If Not EmptyString(oInvoice.InvoiceNo) Then
                    bInfoWritten = True
                End If

            End If

            '??????????????? OK, so far, used by DnBNOR Finans
            sLine = sLine & "00000" & " " 'Item-NO s9(5) 89-93 - 101-106 ????

            '25.03.2009 - Always write the invoiceamount
            sLine = sLine & PadLeft(sAmountToUse, 13, "0") & " " 'Item-Inv-Amount s9(11)V9(2) 95-107 - Dette er vel 107-119 ???

            'Old code
            'If oInvoice.TypeOfStructuredInfo = StructureTypeInvoice Then
            '    sLine = sLine & PadLeft(sAmountToUse, 13, "0") & " " 'Item-Inv-Amount s9(11)V9(2) 95-107
            'Else
            '    sLine = sLine & "0000000000000" & " " 'Item-Inv-Amount s9(11)V9(2) 95-107
            'End If
            If oInvoice.CustomerNo = "LINDORFF" Then
                If Not EmptyString(xDelim(oInvoice.Extra1, ";", 3)) Then
                    sLine = sLine & PadRight(xDelim(oInvoice.Extra1, ";", 3), 5, " ") & " " 'CL-Acctno X(5), 109-113
                    bInfoWritten = True
                Else
                    sLine = sLine & "00000" & " " 'CL-Acctno X(5), 109-113
                End If
            Else
                '25.03.2009 - State customerNo here, retrieved from some types of KID
                sLine = sLine & PadLeft(oInvoice.CustomerNo, 5, "0") & " " 'CL-Acctno X(5), 109-113
                'Old code
                'sLine = sLine & "     " & " " 'CL-Acctno X(5), 109-113
            End If
            sLine = sLine & " " 'Paym-history X(1), 115-115

            If bInfoWritten Then
                WriteDnBNORFinansRecordtype2 = sLine
            Else
                WriteDnBNORFinansRecordtype2 = ""
            End If
        End If

        '        Exit Function

        'errWriteDnBNORFinansRecordtype2:

        '        If bErrorInThisFunction Then
        '            Err.Raise(Err.Number, "WriteDnBNORFinansRecordtype2", Err.Description)
        '        Else
        '            Err.Raise(Err.Number, Err.Source, Err.Description)
        '        End If

        '        WriteDnBNORFinansRecordtype2 = CStr(False)


    End Function

    Private Function WriteDnBNORFinansRecordtype3(ByRef oPayment As vbBabel.Payment, ByRef sDateTimeStamp As String, ByRef dInvoiceCounter As Double, ByRef sFreetext As String) As String
        Dim sLine As String
        Dim bErrorInThisFunction As Boolean

        'On Error GoTo errWriteDnBNORFinansRecordtype3

        sLine = ""

        bErrorInThisFunction = False
        sLine = WriteDnBNORFinansStartOfRecord(oPayment, sDateTimeStamp, dInvoiceCounter, "3") '1-79
        sLine = sLine & PadRight(sFreetext, 350, " ") '80-429
        bErrorInThisFunction = True

        WriteDnBNORFinansRecordtype3 = sLine

        '        Exit Function

        'errWriteDnBNORFinansRecordtype3:

        '        If bErrorInThisFunction Then
        '            Err.Raise(Err.Number, "WriteDnBNORFinansRecordtype3", Err.Description)
        '        Else
        '            Err.Raise(Err.Number, Err.Source, Err.Description)
        '        End If

        '        WriteDnBNORFinansRecordtype3 = CStr(False)

    End Function

    Private Function ChangeCharactersDnBNORFinans(ByRef sString As String) As String

        'On Error GoTo errChangeCharactersDnBNORFinans

        sString = Replace(sString, "�", "�") '91

        sString = Replace(sString, Chr(157), "�")

        sString = Replace(sString, "�", "�") '9B

        sString = Replace(sString, Chr(143), "�")

        sString = Replace(sString, "�", "�") '86



        ChangeCharactersDnBNORFinans = sString

        '        Exit Function

        'errChangeCharactersDnBNORFinans:

        '        Err.Raise(Err.Number, "ChangeCharactersDnBNORFinans", Err.Description)

        '        ChangeCharactersDnBNORFinans = ""

    End Function

    Private Function RetrieveLindorffInfo(ByRef oPayment As vbBabel.Payment) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim iCounter, iCounter2 As Short
        Dim iPosition, iLength As Short
        Dim aMessage(10, 0) As String
        Dim sTemp, sTemp2 As String
        Dim sSaksnummer As String
        Dim sSaksnavn As String
        Dim sCL_AccountNo As String
        Dim sBY_AccountNo As String
        Dim sHovedstol As String
        Dim sRenter As String
        Dim sProvisjon As String
        Dim sGebyr As String
        Dim sMVA As String

        sSaksnummer = ""
        sSaksnavn = ""
        sCL_AccountNo = ""
        sBY_AccountNo = ""
        sHovedstol = ""
        sRenter = ""
        sProvisjon = ""
        sGebyr = ""
        sMVA = ""

        Try

            If oPayment.Invoices.Count = 1 Then
                For Each oInvoice In oPayment.Invoices
                    iCounter = 0
                    For iCounter = 1 To oInvoice.Freetexts.Count

                        oFreeText = oInvoice.Freetexts.Item(iCounter)

                        aMessage(iCounter - 1, 0) = oFreeText.Text

                    Next iCounter
                Next oInvoice

                For iCounter = 0 To UBound(aMessage)
                    'First line may be added by BB as structured info
                    If Left(aMessage(iCounter, 0), 6) = "Bel�p:" Then
                        'Nothing to do
                    Else
                        If EmptyString(aMessage(iCounter, 0)) Then
                            'Nothing to do
                        ElseIf Left(aMessage(iCounter, 0), 3) = "SAK" Then
                            sTemp = Trim(Mid(aMessage(iCounter, 0), 4, 12))
                            sSaksnummer = ""
                            For iCounter2 = 1 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789") Then
                                    sSaksnummer = sSaksnummer & Mid(sTemp, iCounter2, 1)
                                Else
                                    'Find the name of the skyldner. It's stated after the '-' after the saksnr.
                                    sSaksnavn = Trim(Mid(aMessage(iCounter, 0), InStr(3 + Len(sSaksnummer), aMessage(iCounter, 0), "-") + 1))
                                    Exit For
                                End If
                            Next iCounter2
                        ElseIf Left(aMessage(iCounter, 0), 3) = "REF" Then
                            iPosition = InStr(4, aMessage(iCounter, 0), "-")
                            If iPosition > 4 Then   ' added this If 30.08.2019
                                sTemp = Mid(aMessage(iCounter, 0), 4, iPosition - 4)
                            Else
                                ' 30.08.2019 - er dette OK?
                                sTemp = ""
                            End If
                            For iCounter2 = 1 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789") Then
                                    sCL_AccountNo = sCL_AccountNo & Mid(sTemp, iCounter2, 1)
                                End If
                            Next iCounter2
                            If iPosition > 4 Then   ' added this If 30.08.2019
                                sTemp = Trim(Mid(aMessage(iCounter, 0), iPosition + 1))
                            Else
                                sTemp = ""
                            End If
                            For iCounter2 = 1 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789") Then
                                    sBY_AccountNo = sBY_AccountNo & Mid(sTemp, iCounter2, 1)
                                Else
                                    Exit For 'By_account must be numeric
                                End If
                            Next iCounter2
                            ElseIf Left(Trim(aMessage(iCounter, 0)), 6) = "AVDRAG" Or Left(Trim(aMessage(iCounter, 0)), 5) = "RETUR" Then
                                sTemp = Trim(aMessage(iCounter, 0)) & Trim(aMessage(iCounter + 1, 0))
                                'Remove the text AVDRAG and RETUR
                                sTemp = Replace(sTemp, "AVDRAG", "", , , CompareMethod.Text)
                                sTemp = Replace(sTemp, "RETUR", "", , , CompareMethod.Text)
                                iPosition = 0
                                'The downpayment of the hovedstol is marked with H.
                                iPosition = InStr(1, sTemp, "H.", CompareMethod.Binary)
                                If iPosition > 0 Then
                                    sTemp2 = ""
                                    For iCounter2 = iPosition + 2 To Len(sTemp)
                                        If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                            sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                        Else
                                            Exit For
                                        End If
                                    Next iCounter2
                                    sHovedstol = Replace(sTemp2, " ", "")
                                    If sHovedstol = "-" Then
                                        sHovedstol = ""
                                    End If
                                    'Remove hovedstol from the text
                                    sTemp = Replace(sTemp, sHovedstol, "", iPosition + 1, 1, CompareMethod.Text)
                                    sTemp = Replace(sTemp, "H.", "", , , CompareMethod.Binary)
                                End If
                                'The downpayment of the interest is marked with R.
                                iPosition = InStr(1, sTemp, "R.", CompareMethod.Binary)
                                If iPosition > 0 Then
                                    sTemp2 = ""
                                    For iCounter2 = iPosition + 2 To Len(sTemp)
                                        If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                            sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                        Else
                                            Exit For
                                        End If
                                    Next iCounter2
                                    sRenter = Replace(sTemp2, " ", "")
                                    If sRenter = "-" Then
                                        sRenter = ""
                                    End If
                                    'Remove interest from the text
                                    sTemp = Replace(sTemp, sRenter, "", iPosition + 1, 1, CompareMethod.Text)
                                    sTemp = Replace(sTemp, "R.", "", , , CompareMethod.Binary)
                                End If
                                'The downpayment of the provision is marked with P.
                                iPosition = InStr(1, sTemp, "P.", CompareMethod.Binary)
                                If iPosition > 0 Then
                                    sTemp2 = ""
                                    For iCounter2 = iPosition + 2 To Len(sTemp)
                                        If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                            sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                        Else
                                            Exit For
                                        End If
                                    Next iCounter2
                                    sProvisjon = Replace(sTemp2, " ", "")
                                    If sProvisjon = "-" Then
                                        sProvisjon = ""
                                    End If
                                    'Remove provision from the text
                                    sTemp = Replace(sTemp, sProvisjon, "", iPosition + 1, 1, CompareMethod.Text)
                                    sTemp = Replace(sTemp, "P.", "", , , CompareMethod.Binary)
                                End If
                                'The downpayment of the charges is marked with G.
                                iPosition = InStr(1, sTemp, "G.", CompareMethod.Binary)
                                If iPosition > 0 Then
                                    sTemp2 = ""
                                    For iCounter2 = iPosition + 2 To Len(sTemp)
                                        If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                            sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                        Else
                                            Exit For
                                        End If
                                    Next iCounter2
                                    sGebyr = Replace(sTemp2, " ", "")
                                    If sGebyr = "-" Then
                                        sGebyr = ""
                                    End If
                                    'Remove charges from the text
                                    sTemp = Replace(sTemp, sGebyr, "", iPosition + 1, 1, CompareMethod.Text)
                                    sTemp = Replace(sTemp, "G.", "", , , CompareMethod.Binary)
                                End If
                                'The downpayment of the VAT is marked with M.
                                iPosition = InStr(1, sTemp, "M.", CompareMethod.Binary)
                                If iPosition > 0 Then
                                    sTemp2 = ""
                                    For iCounter2 = iPosition + 2 To Len(sTemp)
                                        If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                            sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                        Else
                                            Exit For
                                        End If
                                    Next iCounter2
                                    sMVA = Replace(sTemp2, " ", "")
                                    If sMVA = "-" Then
                                        sMVA = ""
                                    End If
                                    'Remove VAT from the text
                                    sTemp = Replace(sTemp, sMVA, "", iPosition + 1, 1, CompareMethod.Text)
                                    sTemp = Replace(sTemp, "M.", "", , , CompareMethod.Binary)
                                End If

                                iCounter = iCounter + 1

                            End If
                        End If

                Next iCounter

                For Each oInvoice In oPayment.Invoices
                    oInvoice.Extra1 = sSaksnummer & ";" & sSaksnavn & ";" & sCL_AccountNo & ";" & sBY_AccountNo & ";" & sHovedstol & ";" & sRenter & ";" & sProvisjon & ";" & sGebyr & ";" & sMVA
                    oInvoice.CustomerNo = "LINDORFF"
                Next oInvoice

            End If

            RetrieveLindorffInfo = True

        Catch ex As Exception

            '07.10.2009 - Don't raise an error
            'Just continue
            RetrieveLindorffInfo = False

        Finally

        End Try

    End Function
    Function WriteStandardSeparated(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sSpecial As String) As Boolean
        '-------------------------------------------------------------------------
        ' XokNET 06.10.2011
        ' Different exportformats for separated files, used for different customers.
        ' Use Special = .. to distinguish between different outputformats
        '-------------------------------------------------------------------------

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        ' XNET 06.10.2011 added next line
        Dim oInvoice As Invoice
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sSeparator As String
        Dim sLine As String
        ' XNET 06.10.2011
        Dim sTmp As String

        Try

            ' create an outputfile
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            ' HEADER SECTION
            '---------------

            ' different layouts for each customer
            Select Case sSpecial
                Case "VESTBY_KOMMUNE"
                    ' DATO;BELOP;KID
                    '
                    sSeparator = ";"
                    oFile.WriteLine("DATO;BELOP;KID")


            End Select

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    ' different layouts for each customer
                                    Select Case sSpecial
                                        Case "VESTBY_KOMMUNE"
                                            ' Export from OCR-file.
                                            ' Exportformat:
                                            '
                                            ' DATO;BELOP;KID
                                            ' 09.03.2009;3048;000236019329
                                            ' 09.03.2009;9721;000036017267
                                            '
                                            sLine = vbNullString
                                            If oPayment.Invoices.Count > 0 Then
                                                ' Imports OCR, assumes only one Invoice pr payment !!!
                                                ' Date
                                                sLine = Format(StringToDate(oPayment.DATE_Payment), "dd.mm.yyyy") & sSeparator
                                                ' Amount, NO �rer !!!
                                                sLine = sLine & Trim$(Str(Int(oPayment.Invoices.Item(1).MON_InvoiceAmount / 100))) & sSeparator
                                                ' KID
                                                sLine = sLine & Trim$(oPayment.Invoices.Item(1).Unique_Id)
                                                oFile.WriteLine(sLine)
                                                oPayment.Exported = True
                                            End If

                                            ' XNET 06.10.2011, added OMV returnfiles
                                        Case "OMV"
                                            ' Export from Telepay avregningsretur
                                            ' Exportformat:
                                            ' Supplier code;Supplier name;Date paid;Transaction amount;Currency code;Amount in NOK;Invoice nr
                                            ' 240467;NorSea AS;20110830;2575500,00;NOK;2575500,00;144487
                                            sSeparator = ";"
                                            ' Can be more than one invoice pr payment
                                            sLine = vbNullString
                                            For Each oInvoice In oPayment.Invoices

                                                ' Supplier code and invoiceno in oInvoice.REF_Own, like 240467;144487
                                                If Not EmptyString(oInvoice.REF_Own) Then
                                                    sTmp = Trim$(oInvoice.REF_Own)
                                                    sLine = xDelim(sTmp, ";", 1) & sSeparator  ' Suppliercode
                                                Else
                                                    ' should not happen, bu in case
                                                    sLine = sSeparator
                                                End If
                                                ' Supplier name
                                                sLine = sLine & oPayment.E_Name & sSeparator
                                                ' Date paid
                                                sLine = sLine & oPayment.DATE_Payment & sSeparator
                                                ' Transactionamount, comma as decimalsep
                                                sLine = sLine & Format(oInvoice.MON_TransferredAmount / 100, "##0.00") & sSeparator
                                                ' Currencycode
                                                sLine = sLine & oPayment.MON_TransferCurrency & sSeparator
                                                ' Amount in NOK
                                                If oPayment.PayType = "I" Then
                                                    ' for Int payments, we must calculate NOK-amount pr invoice!
                                                    sLine = sLine & Format((oPayment.MON_AccountAmount * (oInvoice.MON_InvoiceAmount / oPayment.MON_InvoiceAmount)) / 100, "##0.00") & sSeparator
                                                Else
                                                    sLine = sLine & Format(oInvoice.MON_InvoiceAmount / 100, "##0.00") & sSeparator
                                                End If
                                                ' InvoiceNo
                                                ' Supplier code and invoiceno in oInvoice.REF_Own, like 240467;144487
                                                If Not EmptyString(oInvoice.REF_Own) Then
                                                    sTmp = Trim$(oInvoice.REF_Own)
                                                    sLine = sLine & xDelim(sTmp, ";", 2)   ' InvocieNo
                                                Else
                                                    ' should not happen, bu in case
                                                    sLine = sLine
                                                End If

                                                oFile.WriteLine(sLine)

                                            Next oInvoice
                                            oPayment.Exported = True
                                    End Select

                                Else
                                    oPayment.Exported = False
                                End If

                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteStandardSeparated" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteStandardSeparated = True

    End Function
    Function WriteGjensidigeUtbytteReturn(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim sText As String
        Dim sDate As String
        Dim sTemp As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        '
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                ' XNET 31.03.2011, changed line below
                                'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    ' Write one line, ; separated for each payment
                                    ' only one invoice pr payment
                                    ' New format programmed 06.05.2010
                                    sLine = vbNullString
                                    ' 1 Part-ref        10  Kundens referanse hos Gjensdige
                                    ' 2 Sendt-dato      10  Dato for n�r utbetalingene ble sendt til VB
                                    ' 3 Utbet-bel�p     9   Bel�p i kroner med ledende nuller
                                    ' 4 Kontonummer-til 11  Kundens kontonr
                                    ' 5 Status          1   U = utbetalt, F = feil
                                    ' 6 Tekst           20  Hvis status = F, skal det v�re en tekst her
                                    If oPayment.ImportFormat = BabelFiles.FileType.BANSTA Then
                                        sLine = sLine & PadLeft(xDelim(oBatch.REF_Own, "-", 1), 10, "0") & ";" '1 Part ref, first part of Ref_Own
                                        sLine = sLine & PadLeft(xDelim(oBatch.REF_Own, "-", 2), 10, "0") & ";" '2 Sendt dato, second part of Ref_Own
                                        sLine = sLine & PadLeft(xDelim(oBatch.REF_Own, "-", 3), 1, "1") & ";" '3 Utbet kode, third part of Ref_Own
                                    ElseIf oPayment.ImportFormat = BabelFiles.FileType.Pain002_Rejected Or oPayment.ImportFormat = BabelFiles.FileType.Pain002 Then
                                        sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 1), 10, "0") & ";" '1 Part ref, first part of Ref_Own
                                        sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 2), 10, "0") & ";" '2 Sendt dato, second part of Ref_Own
                                        sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 3), 1, "1") & ";" '3 Utbet kode, third part of Ref_Own
                                        'ElseIf oPayment.ImportFormat = BabelFiles.FileType.Pain002 Then
                                        '    sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 1), 10, "0") & ";" '1 Part ref, first part of Ref_Own
                                        '    sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 2), 10, "0") & ";" '2 Sendt dato, second part of Ref_Own
                                        '    sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 3), 1, "1") & ";" '3 Utbet kode, third part of Ref_Own
                                    ElseIf oPayment.ImportFormat = BabelFiles.FileType.Camt054_Outgoing Then
                                        sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 1), 10, "0") & ";" '1 Part ref, first part of Ref_Own
                                        sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 2), 10, "0") & ";" '2 Sendt dato, second part of Ref_Own
                                        sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 3), 1, "1") & ";" '3 Utbet kode, third part of Ref_Own
                                    Else
                                        ' DEBMUL
                                        ' OR Telepay return (XNET 30.03.2011)
                                        If oPayment.Invoices.Count > 0 Then
                                            sLine = sLine & PadLeft(xDelim(oPayment.Invoices.Item(1).REF_Own, "-", 1), 10, "0") & ";" '1 Part ref, first part of Ref_Own
                                            sLine = sLine & PadLeft(xDelim(oPayment.Invoices.Item(1).REF_Own, "-", 2), 10, "0") & ";" '2 Sendt dato, second part of Ref_Own
                                            sLine = sLine & PadLeft(xDelim(oPayment.Invoices.Item(1).REF_Own, "-", 3), 1, "1") & ";" '3 Utbet kode, third part of Ref_Own
                                        Else
                                            ' should not be possible
                                            sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 1), 10, "0") & ";" '1 Part ref, first part of Ref_Own
                                            sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 2), 10, "0") & ";" '2 Sendt dato, second part of Ref_Own
                                            sLine = sLine & PadLeft(xDelim(oPayment.REF_Own, "-", 3), 1, "1") & ";" '3 Utbet kode, third part of Ref_Own
                                        End If
                                    End If

                                    sLine = sLine & PadLeft(Trim$(Str(oPayment.MON_InvoiceAmount / 100)), 9, "0") & ";"        '3 Bel�p
                                    sLine = sLine & oPayment.E_Account & ";"                                  '4 Kontonummer-til
                                    ' Avvikskoder i BANSTA
                                    '47 Payor ' account number unknown
                                    '51 Currency code not possible
                                    '72 Beneficiary 's financial information incorrect
                                    '107 Transaction duplicates previous transaction
                                    If EmptyString(oPayment.StatusCode) Or oPayment.StatusCode = "02" Then
                                        sLine = sLine & "U;"                                                      '4 Status
                                        sLine = sLine & ";"                                                       '5 Feiltekst
                                    Else
                                        If oPayment.ImportFormat = BabelFiles.FileType.Pain002_Rejected Or oPayment.ImportFormat = BabelFiles.FileType.Pain002 Then
                                            sText = ""
                                            For Each oInvoice In oPayment.Invoices
                                                For Each oFreetext In oInvoice.Freetexts
                                                    sText = sText & oFreetext.Text
                                                Next oFreetext
                                            Next oInvoice
                                            sLine = sLine & "F;"                                                    '4 Status
                                            'Export the actual errormessage and not the 
                                            'sLine = sLine & Replace(sText, ";", ",") & ";"                               '5 Feiltekst
                                            ' 16.11.2018 - export ErrorCode, not text, from Mats M�ller
                                            sLine = sLine & oInvoice.StatusCode & ";"                               '5 Feiltekst
                                        Else
                                            sLine = sLine & "F;"                                                    '4 Status
                                            sLine = sLine & oPayment.StatusCode & ";"                               '5 Feiltekst
                                        End If
                                    End If
                                    ' 11.05.2010 added paydate
                                    If oPayment.ImportFormat = BabelFiles.FileType.Pain002_Rejected Or oPayment.ImportFormat = BabelFiles.FileType.Pain002 Then
                                        sTemp = xDelim(oPayment.REF_Own, "-", 2) '2 Sendt dato, second part of Ref_Own
                                        If Len(sTemp) = 10 Then
                                            sDate = Right(sTemp, 4)
                                            sDate = sDate & Mid(sTemp, 4, 2)
                                            sDate = sDate & Left(sTemp, 2)
                                            sLine = sLine & sDate
                                        Else
                                            sLine = sLine & oPayment.DATE_Payment
                                        End If
                                    Else
                                        sLine = sLine & oPayment.DATE_Payment
                                    End If

                                    If Len(sLine) > 0 Then
                                        oFile.WriteLine(sLine)
                                    End If
                                    oPayment.Exported = True
                                End If

                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeUtbytteReturn" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeUtbytteReturn = True

    End Function

    Function WriteLindorffInkasso_TNT(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' Export KID- and extracted invoice transactions for Lindorff Inkasso for kunden TNT
        ' ----------------------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        '
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'If oPayment.VB_FilenameOut_ID = iFormat_ID And IsOCR(oPayment.PayCode) Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And (IsOCR((oPayment.PayCode)) Or oPayment.MATCH_Matched) Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    For Each oInvoice In oPayment.Invoices
                                        'If oInvoice.MATCH_Matched Then
                                        ' changed 29.04.2010 - also export KID
                                        If oInvoice.MATCH_Matched Or IsOCR((oPayment.PayCode)) Then

                                            ' Write one line, ; separated for each payment
                                            ' only one invoice pr payment
                                            sLine = ""
                                            sLine = sLine & "RD" '1 Recordtype RD, 1-2
                                            If Not EmptyString((oInvoice.Unique_Id)) Then
                                                sLine = sLine & PadRight(oInvoice.Unique_Id, 25, " ") '2 KID, 3-27
                                            Else
                                                sLine = sLine & Space(25) '2 KID, 3-27
                                            End If
                                            sLine = sLine & "TNT" & Space(9) '3 Kundenr TNT, 27-38
                                            sLine = sLine & oPayment.DATE_Payment '4 Dato, YYYYMMDD 39-46
                                            sLine = sLine & PadLeft(CStr(oInvoice.MON_InvoiceAmount), 10, " ") '5 Bel�p, 10 (2) 47-56
                                            If Not EmptyString((oInvoice.Unique_Id)) Then
                                                sLine = sLine & PadRight(Mid(oInvoice.Unique_Id, 8, 7), 10, " ") '6 Kunder, 57-66
                                                sLine = sLine & PadRight(Mid(oInvoice.Unique_Id, 1, 7), 10, " ") '7 Fakturanr 67-76
                                            Else
                                                sLine = sLine & Space(10) '6 Kunder, 57-66
                                                sLine = sLine & PadRight(Mid(oInvoice.InvoiceNo, 1, 7), 10, " ") '7 Fakturanr 67-76
                                            End If
                                            If EmptyString((oPayment.MON_InvoiceCurrency)) Then
                                                oPayment.MON_InvoiceCurrency = "NOK"
                                            End If
                                            sLine = sLine & oPayment.MON_InvoiceCurrency '8 Valutakode, 77-79
                                            sLine = sLine & PadRight(oPayment.E_Account, 11, " ") '9 Betalers konto, 80-90

                                            If Len(sLine) > 0 Then
                                                oFile.WriteLine((sLine))
                                            End If
                                        End If
                                    Next oInvoice

                                    oPayment.Exported = True
                                ElseIf oPayment.VB_FilenameOut_ID = iFormat_ID And IsOCR((oPayment.PayCode)) = False Then
                                    ' mark non kids as exported
                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteLindorffInkasso_TNT" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteLindorffInkasso_TNT = True

    End Function
    ' XNET 08.11.2010 added new function for export to AccountControl (used by Cominor)
    Function WriteAccountControl(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean
        ' ----------------------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sTxt As String
        '
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'If oPayment.VB_FilenameOut_ID = iFormat_ID And IsOCR(oPayment.PayCode) Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then    'And (IsOCR(oPayment.PayCode) Or oPayment.MATCH_Matched) Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    sTxt = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sTxt = sTxt & Trim$(oFreeText.Text)
                                        Next oFreeText
                                    Next oInvoice

                                    ' only one invoice pr payment
                                    sLine = ""
                                    sLine = sLine & "P"                                                 '1 Recordtype "P"=Payment  1-1
                                    sLine = sLine & PadRight(oPayment.I_Account, 20, " ")               '2 Kontonr                 2-21
                                    sLine = sLine & oPayment.DATE_Payment                               '3 Dato, YYYYMMDD         22-29
                                    sLine = sLine & PadLeft(oPayment.MON_InvoiceAmount, 12, " ")        '4 Bel�p, 12 (2)          30-41
                                    sLine = sLine & PadRight(Mid$(oPayment.MATCH_InvoiceNumber, 1, 10), 10, " ") '5 Faktnr/ID1/Sj�f�rnr 42-51
                                    sLine = sLine & PadRight(Mid$(oPayment.MATCH_MyField, 1, 10), 10, " ") '6 ID2, MyField,variabel use 52-61
                                    sLine = sLine & PadRight(Mid$(oPayment.E_Name, 1, 30), 30, " ")     '7 Navn                   62-91
                                    sLine = sLine & PadRight(sTxt, 100, " ")                            '6 fritext                92-191

                                    If Len(sLine) > 0 Then
                                        oFile.WriteLine(sLine)
                                    End If

                                    oPayment.Exported = True
                                End If
                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteAccountControl" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteAccountControl = True

    End Function
    Function WriteNRX_Ax(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean

        ' 03.01.05
        ' Used by R�de Kors, Special = REDCROSS
        '

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, dDate As Date ', sIAccountNo As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim sAdr1 As String, sAdr2 As String, sZip As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sId As String
        Dim sFreetext As String
        Dim sRef1 As String, sRef2 As String
        Dim sBirthNumber As String
        Dim sGLAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sNoMatchAccount As String
        Dim lBatchcounter As Long
        Dim bErrorInMaalgruppeExists As Boolean
        Dim iFreetextCounter As Integer
        Dim lCounter As Long
        Dim bGLRecordWritten As Boolean

        '
        Try

            bAppendFile = False
            sOldAccountNo = vbNullString
            sGLAccount = vbNullString
            bGLRecordWritten = False
            bAccountFound = False
            lBatchcounter = 0
            bErrorInMaalgruppeExists = False

            ' XNET 29.08.2012 - added specialhanding for Grieg - remove Forskuddsposter, and reduce bankamount
            If oBabelFiles(1).Special = "GRIEG" Then
                Grieg_PrepareExport(oBabelFiles)
            ElseIf oBabelFiles(1).Special = "REDCROSS" Then 'Added 05.11.2014 for LHL
                REDCROSS_PrepareExportRemoveExportedPayments(oBabelFiles)
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'XNET - 06.03.2012 - removed next IF (OCR may be exported!)
                        'If IsOCR(oPayment.PayCode) Then
                        'Nothing to do, don't export
                        'Else
                        'XNET - 09.05.2012 - Added next IF
                        If Not oPayment.Exported Then
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        'End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'XNET - 06.03.2012 - removed next IF (OCR may be exported!)
                            'If IsOCR(oPayment.PayCode) Then
                            'Nothing to do, don't export
                            'Else
                            'XNET - 09.05.2012 - Added next IF
                            If Not oPayment.Exported Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            bGLRecordWritten = False
                            lBatchcounter = lBatchcounter + 1
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'XNET - 06.03.2012 - removed next IF (OCR may be exported!)
                                'If IsOCR(oPayment.PayCode) Then
                                'Nothing to do, don't export
                                'Else
                                'XNET - 09.05.2012 - Added next IF
                                If Not oPayment.Exported Then
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If oPayment.REF_Own = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                'End If

                                If bExportoPayment Then

                                    If oPayment.I_Account <> sOldAccountNo Then
                                        If oPayment.VB_ProfileInUse Then
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                                            sGLAccount = Trim$(oaccount.GLAccount)
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True
                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                            If bAccountFound Then
                                                sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                            Else
                                                Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                                '12003: Could not find account %1.Please enter the account in setup."
                                            End If
                                        End If
                                    End If

                                    '                        'XNET - 28.02.2012 - Moved code below
                                    '                        'Write GL-line if not previously written
                                    '                        If Not bGLRecordWritten Then
                                    '                            sLine = WriteNRX_AxBankRecord(oBatch, oPayment, oClient, sGLAccount)
                                    '                            oFile.WriteLine sLine
                                    '                            bGLRecordWritten = True
                                    '                        End If

                                    '-------- fetch content of each payment ---------
                                    sName = oPayment.E_Name
                                    sAdr1 = vbNullString   'vbNullString
                                    sAdr2 = vbNullString   'vbNullString
                                    ' If Adr2="-", empty Adr2;
                                    If Trim$(oPayment.E_Adr2) = "-" Then
                                        oPayment.E_Adr2 = vbNullString
                                    End If

                                    If Len(Trim$(oPayment.E_Adr2)) = 0 Then
                                        ' empty adr2, put opayment.e_adr1 into eksportfiles adr2
                                        sAdr2 = oPayment.E_Adr1  'Special for LHL, adr2 is the mainadresse
                                    Else
                                        ' both addresslines in use
                                        sAdr1 = oPayment.E_Adr1
                                        sAdr2 = oPayment.E_Adr2
                                    End If

                                    sZip = oPayment.E_Zip
                                    nAmount = oPayment.MON_TransferredAmount

                                    'sArchiveRef = oPayment.REF_Bank1
                                    ' Skal benyttes til avstemming bank

                                    dDate = StringToDate(oPayment.DATE_Payment)
                                    '12.09.2006 - new code to write the correct archiveref for bankreconciliation
                                    sArchiveRef = Trim$(oBatch.REF_Bank)
                                    For lCounter = 1 To Len(sArchiveRef)
                                        If Left$(sArchiveRef, 1) = "*" Then
                                            sArchiveRef = Mid$(sArchiveRef, 2)
                                        Else
                                            Exit For
                                        End If
                                    Next lCounter
                                    If oBatch.Payments.Count = 1 And Not EmptyString(oPayment.REF_Bank1) Then
                                        sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                        sRef1 = oBatch.REF_Bank
                                    Else
                                        sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                        sRef1 = oPayment.REF_Bank1
                                    End If
                                    sRef2 = oPayment.REF_Bank2

                                    sFreetext = vbNullString

                                    iFreetextCounter = 0
                                    sFreetext = vbNullString
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier < 3 Then
                                                    iFreetextCounter = iFreetextCounter + 1
                                                    If iFreetextCounter = 1 Then
                                                        sFreetext = RTrim$(oFreeText.Text)
                                                    ElseIf iFreetextCounter = 2 Then
                                                        sFreetext = sFreetext & " " & RTrim$(oFreeText.Text)
                                                    Else
                                                        Exit For
                                                    End If
                                                End If
                                            Next oFreeText
                                        End If
                                    Next oInvoice
                                    sFreetext = sFreetext & " - " & Trim$(oPayment.E_Name)

                                    For Each oInvoice In oPayment.Invoices
                                        If Not bErrorInMaalgruppeExists Then
                                            'M�lgruppe, RK_ID, IB_EX_ID
                                            'Changed 15.02.2006
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                If EmptyString(oInvoice.InvoiceNo) Then
                                                    bErrorInMaalgruppeExists = True
                                                    ' Type post
                                                    '-------
                                                ElseIf EmptyString(oInvoice.MyField) Then
                                                    bErrorInMaalgruppeExists = True
                                                End If
                                            End If
                                        End If
                                    Next

                                    'TODO CHECK WITH ESPEN IF THIS SHOULD BE DONE!
                                    '                        If UCase(oBabel.Special) = "CHURCHAID" Or UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then 'XNET - 28.06.2011 - Added NF
                                    '                            'XNET 28.06.2011 - Added next line
                                    '                            oPayment.StatusSplittedFreetext = 0 'To be sure the birthnumber is extracted
                                    '                            sBirthNumber = ExtractBirthNumber(oPayment) ' Run through freetext to catch F�dselsnr
                                    '                            If Len(sBirthNumber) > 0 Then
                                    '                                'XNET - 28.06.2011 - Added next IF - WHAT IS THIS???????? - We write oPayment.e_orgno
                                    '                                If UCase(oBabel.Special) = "NORSK_FOLKEHJELP" Then
                                    '                                    oPayment.e_OrgNo = sBirthNumber
                                    '                                Else
                                    '                                    ' If valid birthnumber, set as adr2
                                    '                                    sAdr1 = sBirthNumber
                                    '                                End If
                                    '                            End If
                                    '                        End If

                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MON_InvoiceAmount <> 0 Then
                                                ' XNET 15.05.2012 - added GL-record
                                                ' Kan dette under v�re generelt, uten � �delegge NRX?
                                                sLine = ""
                                                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                                    If UCase(oBabel.Special) <> "REDCROSS" Then
                                                        sLine = WriteNRX_AxGLRecord(UCase(oBabel.Special), oPayment, oInvoice, oClient)
                                                    End If
                                                Else
                                                    ' as pre 15.05.2012
                                                    sLine = WriteNRX_AxTransaction(UCase(oBabel.Special), sName, sAdr1, sAdr2, sZip, nAmount, dDate, TransformDnBNORReference(sArchiveRef), sId, oPayment.I_Account, oPayment.E_Account, TransformDnBNORReference(sRef1), TransformDnBNORReference(sRef2), sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oInvoice)
                                                End If
                                                If Len(sLine) > 0 Then
                                                    oFile.WriteLine(sLine)
                                                End If

                                            End If
                                        End If
                                    Next oInvoice
                                    oPayment.Exported = True
                                End If

                            Next ' payment

                            'XNET - 28.02.2012 - Moved code from above

                            'Write GL-line after each record
                            If Not bGLRecordWritten Then
                                ' XNET 29.03.2012 added sSpecial as first parameter (same as in _AxTransaction)
                                sLine = WriteNRX_AxBankRecord(UCase(oBabel.Special), oBatch, oClient, sGLAccount)
                                'XNET - 22.05.2012 - Added next IF for NRX in case there only is a GL-posting in the batch
                                If Not EmptyString(sLine) Then
                                    oFile.WriteLine(sLine)
                                End If
                                bGLRecordWritten = True

                            End If
                        End If
                    Next 'batch
                End If

            Next

            '06.11.2014 - Removed this for LHL
            'If oBabelFiles.Count > 0 Then
            '    If UCase(oBabelFiles.Item(1).Special) = "REDCROSS" Then
            '        If bErrorInMaalgruppeExists Then
            '            MsgBox("Det finnes eksporterte poster hvor m�lgruppe 999 er lagt inn.", vbInformation + vbOKOnly, "Feil i m�lgruppe")
            '        End If
            '    End If
            'End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteNRX_Ax" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteNRX_Ax = True

    End Function
    'XNET - 26.10.2011 - Added next function
    'XNET - 07.02.2012 - Whole function again
    ' XNET 29.03.2012 added sSpecial as first parameter (same as in _AxTransaction)
    'Private Function WriteNRX_AxBankRecord(oBatch As vbbabel.Batch, oClient As vbbabel.Client, sGLAccount As String) As String
    Private Function WriteNRX_AxBankRecord(ByVal sSpecial As String, ByVal oBatch As vbBabel.Batch, ByVal oClient As vbBabel.Client, ByVal sGLAccount As String) As String
        'Nr  Felt - Type - Kommentar - Felt i Axapta - Felt i BB
        '1   Kontotype - Str - F = Finans - AccountType - Batch
        '2   Kontonummer - Str - Bankkonto - AccountID - Payment.I_Account
        '3   Transaksjonsdato - Date - Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY - TransDate - Payment.DateValue
        '4   Bel�p - Real - Bel�p i valuta. - (minus) settes for negative bel�p.Desimalskille settes til punktum - AmountCurDebit - Batch.MonInvoiceAmount
        '5   Valuta - Str - Standard ISO-koder (NOK, EUR etc..) - CurrencyCode - Payment.MonInvoiceCurrency
        '6   Valutakurs - Real - Valutakurs p� innbetaling, ved NOK settes kurs = 100. Desimalskille settes til punktum - ExchRate - 100
        '7   Tekst - Str 60 posisjoner - Txt - Payment.PayType?
        '8   Betalings-referanse - Str - Referanse til kontoutdrag - PaymentReference - Batch.RefBank

        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim oLocalPayment As vbBabel.Payment
        Dim oLocalInvoice As vbBabel.Invoice
        Dim nAmountToUse As Double
        Dim iCounter As Integer
        Dim sI_Account As String
        Dim sDATE_Value As String
        Dim sDate_Payment As String
        Dim sMON_InvoiceCurrency As String
        Dim sPayCode As String

        iCounter = 0
        nAmountToUse = 0
        sI_Account = ""
        sDATE_Value = ""
        sDate_Payment = ""
        sMON_InvoiceCurrency = ""
        sPayCode = ""
        For Each oLocalPayment In oBatch.Payments
            iCounter = iCounter + 1
            If iCounter = 1 Then
                sI_Account = Trim(oLocalPayment.I_Account)
                sDATE_Value = oLocalPayment.DATE_Value
                sDate_Payment = oLocalPayment.DATE_Payment
                sMON_InvoiceCurrency = oLocalPayment.MON_InvoiceCurrency
                sPayCode = oLocalPayment.PayCode
            End If
            If sSpecial = "REDCROSS" Then
                For Each oLocalInvoice In oLocalPayment.Invoices
                    If oLocalInvoice.MATCH_Final Then
                        If oLocalInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                            nAmountToUse = nAmountToUse + oLocalInvoice.MON_InvoiceAmount
                        End If
                    End If
                Next oLocalInvoice
            Else
                nAmountToUse = oBatch.MON_InvoiceAmount
            End If
        Next oLocalPayment

        sLineToWrite = vbNullString

        'If nAmountToUse > 0 Then
        ' XNET 29.08.2012 - in special cases, with credits and forskudd, Grieg may have "negative" bankamount"
        If nAmountToUse > 0 Or (nAmountToUse < 0 And sSpecial = "GRIEG") Then
            If sSpecial = "GRIEG" Then
                ' XOKNET 15.06.2012 - changed from F to B
                sLineToWrite = "B;;;" 'Kontotype
            Else
                ' as pre 15.06.2012
                sLineToWrite = "F;;;" 'Kontotype
            End If

            ' XOKNET 29.03.2012 - Grieg must have GL-account for bank, not "real accountnumber"
            If sSpecial = "GRIEG" Then
                sLineToWrite = sLineToWrite & sGLAccount & ";;" ' GL konto for bank
            Else
                sLineToWrite = sLineToWrite & sI_Account & ";;" ' Kontonummer
            End If ' End XOKNET 29.03.2012
            sDateToUse = vbNullString
            If Not sDATE_Value = "19900101" Then
                sDateToUse = Right(sDATE_Value, 2) & "." & Mid(sDATE_Value, 5, 2) & "." & Left(sDATE_Value, 4)
            Else 'Use payment_date
                If Not sDate_Payment = "19900101" Then
                    sDateToUse = Right(sDate_Payment, 2) & "." & Mid(sDate_Payment, 5, 2) & "." & Left(sDate_Payment, 4)
                Else
                    sDateToUse = CStr(Format(Now(), "DDMMYYYY"))
                End If
            End If
            sLineToWrite = sLineToWrite & sDateToUse & ";" 'Transaksjonsdato
            sLineToWrite = sLineToWrite & ConvertFromAmountToString(nAmountToUse, "", ".") & ";" 'Bel�p
            sLineToWrite = sLineToWrite & sMON_InvoiceCurrency & ";" 'Valuta
            sLineToWrite = sLineToWrite & "100;;" ' Exch.rate against basevaluta
            sLineToWrite = sLineToWrite & sPayCode & ";" ' Txt - Add VoucherNo?
            sLineToWrite = sLineToWrite & oBatch.REF_Bank 'Betalings-referanse
        End If

        WriteNRX_AxBankRecord = sLineToWrite

    End Function
    'XNET - 15.05.2012 - Added next function
    Private Function WriteNRX_AxGLRecord(ByVal sSpecial As String, ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client) As String
        'Nr  Felt - Type - Kommentar - Felt i Axapta - Felt i BB
        '1   Kontotype - Str - F = Finans - AccountType - Batch
        '2   Kontonummer - Str - Bankkonto - AccountID - Payment.I_Account
        '3   Transaksjonsdato - Date - Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY - TransDate - Payment.DateValue
        '4   Bel�p - Real - Bel�p i valuta. - (minus) settes for negative bel�p.Desimalskille settes til punktum - AmountCurDebit - Batch.MonInvoiceAmount
        '5   Valuta - Str - Standard ISO-koder (NOK, EUR etc..) - CurrencyCode - Payment.MonInvoiceCurrency
        '6   Valutakurs - Real - Valutakurs p� innbetaling, ved NOK settes kurs = 100. Desimalskille settes til punktum - ExchRate - 100
        '7   Tekst - Str 60 posisjoner - Txt - Payment.PayType?
        '8   Betalings-referanse - Str -

        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim oLocalPayment As vbBabel.Payment
        Dim oLocalInvoice As vbBabel.Invoice
        Dim nAmountToUse As Double
        Dim iCounter As Integer
        'Dim sI_Account As String
        'Dim sDATE_Value As String
        'Dim sDate_Payment As String
        'Dim sMON_InvoiceCurrency As String
        'Dim sPayCode As String
        Dim sFreetext As String

        'iCounter = 0
        'nAmountToUse = 0
        'sI_Account = ""
        'sDATE_Value = ""
        'sDate_Payment = ""
        'sMON_InvoiceCurrency = ""
        'sPayCode = ""
        'For Each oLocalPayment In oBatch.Payments
        '    iCounter = iCounter + 1
        '    If iCounter = 1 Then
        '        sI_Account = Trim(oLocalPayment.I_Account)
        '        sDATE_Value = oLocalPayment.DATE_Value
        '        sDate_Payment = oLocalPayment.DATE_Payment
        '        sMON_InvoiceCurrency = oLocalPayment.MON_InvoiceCurrency
        '        sPayCode = oLocalPayment.PayCode
        '    End If
        '    For Each oLocalInvoice In oLocalPayment.Invoices
        '        If oLocalInvoice.MATCH_Final Then
        '            If oLocalInvoice.MATCH_MatchType <> MatchedOnGL Then
        '                nAmountToUse = nAmountToUse + oLocalInvoice.MON_InvoiceAmount
        '            End If
        '        End If
        '    Next oLocalInvoice
        'Next oLocalPayment

        sLineToWrite = vbNullString
        sLineToWrite = "F;;;" 'Kontotype
        ' XNET 29.03.2012 - Grieg must have GL-account for bank, not "real accountnumber"
        sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";;" ' Kontonummer
        'End If ' End XNET 29.03.2012
        'sDateToUse = vbNullString
        'If Not sDATE_Value = "19900101" Then
        '    sDateToUse = Right(sDATE_Value, 2) & "." & Mid(sDATE_Value, 5, 2) & "." & Left(sDATE_Value, 4)
        'Else 'Use payment_date
        '    If Not sDate_Payment = "19900101" Then
        '        sDateToUse = Right(sDate_Payment, 2) & "." & Mid(sDate_Payment, 5, 2) & "." & Left(sDate_Payment, 4)
        '    Else
        '        sDateToUse = CStr(Format(Now(), "DDMMYYYY"))
        '    End If
        'End If
        sDateToUse = Right(oPayment.DATE_Payment, 2) & "." & Mid(oPayment.DATE_Payment, 5, 2) & "." & Left(oPayment.DATE_Payment, 4)
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Transaksjonsdato
        'XOKNET 06.06.2012 - feil fortegn p� hovedboksposter for Grieg
        sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".") & ";" 'Bel�p
        sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        sLineToWrite = sLineToWrite & "100;;" ' Exch.rate against basevaluta
        sFreetext = ""
        For iCounter = 1 To oInvoice.Freetexts.Count
            sFreetext = sFreetext & oInvoice.Freetexts(iCounter).Text
        Next iCounter
        sLineToWrite = sLineToWrite & Left$(sFreetext, 60) & ";" ' Txt - Add VoucherNo?
        sLineToWrite = sLineToWrite & ""   ' Trenger vi noe her ?  oBatch.REF_Bank 'Betalings-referanse

        WriteNRX_AxGLRecord = sLineToWrite

    End Function
    'XNET - 26.10.2011 - Added next function
    'XNET - 07.02.2012 - Whole function (again)
    Public Function WriteNRX_AxTransaction(ByVal sSpecial As String, ByVal sName As String, ByVal sAdr1 As String, ByVal sAdr2 As String, ByVal sZip As String, ByVal nAmount As Double, ByVal dDate As Date, ByVal sArchiveRef As String, ByVal sId As String, ByVal sToAccount As String, ByVal sFromAccount As String, ByVal sRef1 As String, ByVal sRef2 As String, ByVal sFreetext As String, ByVal sPayCode As String, ByVal oPayment As vbBabel.Payment, ByVal sGLAccount As String, ByVal sNoMatchAccount As String, ByVal oInvoice As vbBabel.Invoice) As String
        'Nr  Felt        Type    Kommentar              Felt i Axapta   Felt i BabelBank
        '1   Kontotype   Str     K = Kunde              AccountType     Payment
        '2   Linjetype   Str     K = KID, m = M�lgruppe ,N = Ny relasjon, S = Eurobate(SMS)   Tolking av innbetalingsposter   Invoice.MyField
        '3   Betalingsreferanse  Str  BabelBank referanse Referansen vil vises i Axapta   PaymReference   Payment.VoucherNo
        '4   Kontonummer Str     Kundenummer            AccountNum      Invoice.CustomerNo
        '5   Dimensjon 3 Str     M�lgruppe - fylles ut n�r linjetype = M og N (S?)   Dimension3
        '6   TransDate   Date    Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY    TransDate   Payment.DateValue
        '7   Bel�p, Greit med minus (SMS) Real Bel�p i valuta. - (minus) settes for negative bel�p
        '    Desimalskille settes til punktum    AmountCurCredit/AmountCurDebet - avhengig av fortegn    Invoice.MonInvoiceAmount
        '8   Valuta      Str     Standard ISO-koder (NOK, EUR etc..) CurrencyCode    Invoice.MonInvoiceCurrency
        '9   Valutakurs  Real    Valutakurs p� innbetaling, ved NOK settes kurs = 100 Desimalskille settes til punktum    ExchRate  "100"
        '10  KID         Str     Legge inn KID om linjetype = K ellers blank    PaymId  Invoice.MatchId
        '11  Tekst       Str 60 posisjoner (Viktig informasjon)
        '    - Bankreferanse
        '    - Navn p� betaler
        '    - Kommentarer   Txt ???
        '12  BetalingsNotat Str  Fritekstfelt, legge p� all tekst som er lagt inn manuelt i BabelBank
        '    - Sjekkes med RK om ytterligere tekst skal eksporteres  PaymentNotes    FreeTxt.Xtext
        '13  Fornavn     Str     F�rste del av navn     FirstName       Payment.Ename
        '14  Mellomnavn  Str     Alt mellom f�rst og siste del av navn   MiddleName  Payment.Ename
        '15  Etternavn   Str     Siste del av navn      LastName        Payment.Ename
        '16  Adresse1    Str     1. linje i adresse     Street (linje1) Payment.EAdr1
        '17  Adresse2    Str     2. linje i adresse     Street (linje2) Payment.EAdr2
        '18  Adresse3    Str     3. linje i adresse     Street (linje3) Payment.EAdr3
        '19  Postnr      Str     Postnummer             ZipCode         Payment.EZip
        '20  Poststed    Str     Poststed               City            Payment.ECity
        '21  Betalingstype B=Bank, T=Givertelefon, X=PayEx (eller annen Internett-l�sning)
        '22  Kontonummer betaler Str Bankkontonummer kunde   BankAccount Payment.EAccount

        'XNET - 18.10.2013 - Added comment
        'For NRX
        '23  Telefonnummer
        '24  Type - P=Person, A=Bedrift


        '    Kj�nn   Str M, K, I. I skrives ikke over M eller K.
        '- sjekkes med RK ?      ???
        '    Adresseboktype  Str Organisasjon eller Person
        'Ytterligere detaljer?
        'P=Person, A=Bedrift, L=Lokalforening, D=Distrikt. Vi �nsker oss ogs� nye Persontyper som ogs� m� gjenspeiles i PERSON-bildet: S=Skoler, barnehager etc., F=Foreninger (ikke egne), O=Offentlige etater. DirPartyTable.Type  ???
        '    Endringskode    Str Dersom det her st�r Ja skal navn og adresse oppdateres selv om kunde eksisterer.
        'Vi kommer i utgangspunktet ikke til � ta denne i bruk, men det kan bli aktuelt senere.  ??  ???
        '    Betalingsmetode Str B=Bank
        'T=Givertelefon X=PayEx (eller annen Internett-l�sning
        '- Sjekkes med RK    ??  ???

        '
        Dim sLineToWrite As String
        Dim sLineType As String
        Dim s As String
        Dim aNames() As String
        Dim i As Long
        Dim bTheInvoiceIsMatched As Boolean
        Dim sFreeTextVariable As String
        Dim oFreeText As vbBabel.Freetext
        Dim sAmount As String
        Dim sDateToUse As String

        'New 17.02.2006
        'Add the variable part of the freetext
        sFreeTextVariable = sFreetext
        For Each oFreeText In oInvoice.Freetexts
            If oFreeText.Qualifier > 3 Then
                sFreeTextVariable = RTrim$(oFreeText.Text) & " " & sFreeTextVariable
            End If
        Next oFreeText

        sLineToWrite = vbNullString

        If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
            If UCase(Trim$(oInvoice.CustomerNo)) = UCase(Trim$(sNoMatchAccount)) Then
                bTheInvoiceIsMatched = False
            ElseIf EmptyString(oInvoice.CustomerNo) Then
                bTheInvoiceIsMatched = False
            Else
                bTheInvoiceIsMatched = True
            End If

            ' XNET 21.06.2012 more changes for Grieg !!!!
            If sSpecial = "GRIEG" And oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                bTheInvoiceIsMatched = False
            End If
            ' XNET 07.09.2012 more changes for Grieg !!!!
            If sSpecial = "GRIEG" And IsOCR(oPayment.PayCode) Then
                bTheInvoiceIsMatched = True
            End If

            sLineToWrite = "K;" 'Kontotype
            ''Linjetype   Str     K = KID, m = M�lgruppe ,N = Ny relasjon, S = Eurobate(SMS)
            sLineType = ""
            If Not EmptyString(oInvoice.MyField) And bTheInvoiceIsMatched Then
                sLineType = Left$(oInvoice.MyField, 1)
                If sLineType = "R" Then
                    sLineType = "K" 'KID
                ElseIf sLineType = "I" Then
                    sLineType = "K" 'KID
                End If
            Else
                ' XNET 16.04.2012 - Lagt inn If Grieg ...
                If sSpecial = "GRIEG" Then
                    If bTheInvoiceIsMatched Then
                        ' XNET 03.09.2012 - more changes for Grieg, again ..........
                        'sLineType = "K"
                        sLineType = "F"
                    Else
                        sLineType = "N"
                    End If
                Else
                    sLineType = "N"
                End If
            End If
            sLineToWrite = sLineToWrite & sLineType & ";"
            '3   Betalingsreferanse  Str  BabelBank referanse Referansen vil vises i Axapta   PaymReference   Payment.VoucherNo
            sLineToWrite = sLineToWrite & Trim(oPayment.VoucherNo) & ";"
            '4   Kontonummer Str     Kundenummer            AccountNum      Invoice.CustomerNo
            If sSpecial <> "GRIEG" And sLineType = "N" Then
                sLineToWrite = sLineToWrite & ";"
            Else
                '        sLineToWrite = sLineToWrite & Trim(oInvoice.CustomerNo) & ";"
                ' XNET 05.06.2012 - changed by JanP to be able to export Not Matched
                If sSpecial = "GRIEG" And Not bTheInvoiceIsMatched Then
                    ' unmatched - have created KID for unmatched in special aftermatching GRIEG_ADDKID
                    sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
                Else
                    sLineToWrite = sLineToWrite & Trim(oInvoice.CustomerNo) & ";"
                End If
            End If
            '5   Dimensjon 3 Str     M�lgruppe - fylles ut n�r linjetype = M og N (S?)   Dimension3
            If sLineType = "M" And bTheInvoiceIsMatched Then
                sLineToWrite = sLineToWrite & Trim(oInvoice.InvoiceNo) & ";" 'alternatively oInvoice.MATCH_ID - Vil ikke funke hvis man endrer i sprMatched fra I til M
            ElseIf sLineType = "N" Then
                sLineToWrite = sLineToWrite & Trim(oInvoice.InvoiceNo) & ";" 'alternatively oInvoice.MATCH_ID
            Else
                sLineToWrite = sLineToWrite & ";"
            End If
            '6   TransDate   Date    Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY    TransDate   Payment.DateValue
            sDateToUse = vbNullString
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = Right(oPayment.DATE_Value, 2) & "." & Mid(oPayment.DATE_Value, 5, 2) & "." & Left(oPayment.DATE_Value, 4)
            Else 'Use payment_date
                If Not oPayment.DATE_Payment = "19900101" Then
                    sDateToUse = Right(oPayment.DATE_Payment, 2) & "." & Mid(oPayment.DATE_Payment, 5, 2) & "." & Left(oPayment.DATE_Payment, 4)
                Else
                    sDateToUse = CStr(Format(Now(), "DD.MM.YYYY"))
                End If
            End If
            sLineToWrite = sLineToWrite & sDateToUse & ";"
            '7   Bel�p, Greit med minus (SMS) Real Bel�p i valuta. - (minus) settes for negative bel�p
            '    Desimalskille settes til punktum    AmountCurCredit/AmountCurDebet - avhengig av fortegn    Invoice.MonInvoiceAmount
            sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".") & ";" 'Bel�p
            '8   Valuta      Str     Standard ISO-koder (NOK, EUR etc..) CurrencyCode    Invoice.MonInvoiceCurrency
            If Len(Trim(oPayment.MON_InvoiceCurrency)) = 3 Then
                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency) & ";"
            Else
                sLineToWrite = sLineToWrite & "NOK;"
            End If
            '9   Valutakurs  Real    Valutakurs p� innbetaling, ved NOK settes kurs = 100 Desimalskille settes til punktum    ExchRate  "100"
            sLineToWrite = sLineToWrite & "100;"
            '10  KID         Str     Legge inn KID om linjetype = K ellers blank    PaymId  Invoice.MatchId
            If sLineType = "K" And bTheInvoiceIsMatched Then
                sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
                ' XNET 03.09.2012 - more changes for Grieg, again ..........
            ElseIf sLineType = "F" Then
                sLineToWrite = sLineToWrite & Trim(oInvoice.InvoiceNo) & ";"
            Else
                ' XNET 16.04.2012 - Lagt inn If sspecial Grieg ...
                If sSpecial = "GRIEG" And IsOCR(oPayment.PayCode) Then
                    ' For Grieg, KID-payments, add KID here
                    sLineToWrite = sLineToWrite & oInvoice.Unique_Id & ";"
                ElseIf sSpecial = "GRIEG" And Not bTheInvoiceIsMatched Then
                    ' unmatched - have created KID for unmatched in special aftermatching GRIEG_ADDKID
                    ' XNET 18.06.2012 �yvind in Columbus will need 0 here, not customerNo
                    sLineToWrite = sLineToWrite & "0;"
                Else
                    sLineToWrite = sLineToWrite & ";"
                End If
            End If
            '11  Tekst       Str 60 posisjoner (Viktig informasjon)
            '    - Bankreferanse
            '    - Navn p� betaler
            '    - Kommentarer   Txt ???
            s = ""
            If Not EmptyString(oPayment.REF_Bank1) Then
                s = "Ref: " & oPayment.REF_Bank1
                ' XNET 18.06.2012
                ' �yvind i Columbus sier at innhold fra felt 12 ikke leses inn i Axapta,og at vi m� samle alt i felt 11
                If sSpecial = "GRIEG" Then
                    s = s + "-" & sFreeTextVariable
                    sFreeTextVariable = ""
                End If
            End If
            sLineToWrite = sLineToWrite & s & ";"
            '12  BetalingsNotat Str  Fritekstfelt, legge p� all tekst som er lagt inn manuelt i BabelBank
            '    - Sjekkes med RK om ytterligere tekst skal eksporteres  PaymentNotes    FreeTxt.Xtext
            sLineToWrite = sLineToWrite & sFreeTextVariable & ";"

            ' XNET 29.03.2012 - For Grieg, blank names and adresses
            If sSpecial = "GRIEG" Then
                '13-21 er blanke
                sLineToWrite = sLineToWrite & ";;;;;;;;;"
            Else
                '13  Fornavn     Str     F�rste del av navn     FirstName       Payment.Ename
                ' Capitalize first letter in each part;
                aNames = Split(Trim(oPayment.E_Name))
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If
                Next
                sLineToWrite = sLineToWrite & aNames(0) & ";"
                '14  Mellomnavn  Str     Alt mellom f�rst og siste del av navn   MiddleName  Payment.Ename
                s = ""
                For i = 1 To UBound(aNames) - 1
                    s = s & aNames(i) & " "
                Next i
                s = Trim(s)
                sLineToWrite = sLineToWrite & s & ";"
                '15  Etternavn   Str     Siste del av navn      LastName        Payment.Ename
                sLineToWrite = sLineToWrite & aNames(UBound(aNames)) & ";"
                '16  Adresse1    Str     1. linje i adresse     Street (linje1) Payment.EAdr1
                s = oPayment.E_Adr1
                If InStr(s, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr1 = Replace(Replace(sAdr1, ".", vbNullString), ",", vbNullString)
                    s = Replace(Replace(s, ".", " "), ",", " ")
                    ' not two spaces!!
                    s = Replace(s, "  ", " ")
                End If

                ' Capitalize first letter in each part;
                aNames = Split(Trim(s))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                s = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    s = s & aNames(i) & " "
                Next
                sLineToWrite = sLineToWrite & s & ";"
                '17  Adresse2    Str     2. linje i adresse     Street (linje2) Payment.EAdr2
                s = oPayment.E_Adr2
                If InStr(s, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr1 = Replace(Replace(sAdr1, ".", vbNullString), ",", vbNullString)
                    s = Replace(Replace(s, ".", " "), ",", " ")
                    ' not two spaces!!
                    s = Replace(s, "  ", " ")
                End If

                ' Capitalize first letter in each part;
                aNames = Split(Trim(s))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                s = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    s = s & aNames(i) & " "
                Next
                sLineToWrite = sLineToWrite & s & ";"
                '18  Adresse3    Str     3. linje i adresse     Street (linje3) Payment.EAdr3
                s = oPayment.E_Adr3
                If InStr(s, ".") > 0 Then
                    ' changed 03.11.2003
                    'sAdr1 = Replace(Replace(sAdr1, ".", vbNullString), ",", vbNullString)
                    s = Replace(Replace(s, ".", " "), ",", " ")
                    ' not two spaces!!
                    s = Replace(s, "  ", " ")
                End If

                ' Capitalize first letter in each part;
                aNames = Split(Trim(s))
                ' Capitalize first letter in all parts of Adr2, uncap all others;
                s = vbNullString
                For i = 0 To UBound(aNames)
                    aNames(i) = UCase(Left$(aNames(i), 1)) & LCase(Mid$(aNames(i), 2))
                    ' Uppercase also after -, ex Jan-Petter, not Jan-petter!
                    If InStr(aNames(i), "-") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "-")) & UCase(Mid$(aNames(i), InStr(aNames(i), "-") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "-") + 2))
                    End If
                    ' Uppercase also after /, ex V/Jan-Petter, not V/jan-petter!
                    If InStr(aNames(i), "/") > 0 Then
                        aNames(i) = Left$(aNames(i), InStr(aNames(i), "/")) & UCase(Mid$(aNames(i), InStr(aNames(i), "/") + 1, 1)) & LCase(Mid$(aNames(i), InStr(aNames(i), "/") + 2))
                    End If

                    s = s & aNames(i) & " "
                Next
                sLineToWrite = sLineToWrite & s & ";"
                '19  Postnr      Str     Postnummer             ZipCode         Payment.EZip
                sLineToWrite = sLineToWrite & Trim(oPayment.E_Zip) & ";"
                '20  Poststed    Str     Poststed               City            Payment.ECity
                sLineToWrite = sLineToWrite & Trim(oPayment.E_City) & ";"


                '21  Betalingskanal
                'B=Bank
                'T=Givertelefon
                'X=PayEx (eller annen Internett-l�sning)
                ' 19.09.2018 - LHL has reported an error when using B here.
                ' Remove it. Can't use Special, because it is REDCROSS, which is used for other cases
                If oPayment.VB_Profile.CompanyName = "Landforeningen for hjerte- og lunge" Then
                    sLineToWrite = sLineToWrite & "" & ";"
                Else
                    sLineToWrite = sLineToWrite & "B" & ";"
                End If
            End If ' If sSpecial = "GRIEG" XNET 29.03.2012 ends here
            '22  Kontonummer betaler Str Bankkontonummer kunde   BankAccount Payment.EAccount
            If Mid(oPayment.E_Account, 5, 1) <> "9" Then
                sLineToWrite = sLineToWrite & Trim(oPayment.E_Account)
            Else
                sLineToWrite = sLineToWrite
            End If

            'XOKNET - 22.08.2013 - Added field 23 for REDCROSS
            '23 Type bedrift, REDCROSS only
            '----------------------
            'XOKNET - 22.08.2013 - Added field 23 for REDCROSS
            '23 Type bedrift, REDCROSS only
            '----------------------
            'XNET - Changed next IF, copy all of it
            If sSpecial = "REDCROSS" Then
                If Trim(oPayment.ExtraD1) = "A" Then
                    sLineToWrite = sLineToWrite & ";A;"
                Else
                    sLineToWrite = sLineToWrite & ";P;"
                End If
                If oPayment.ImportFormat = vbBabel.BabelFiles.FileType.Givertelefon Then
                    sLineToWrite = sLineToWrite & Trim(oPayment.REF_Own)
                Else
                    sLineToWrite = sLineToWrite
                End If
            End If

            '    If (sSpecial = "REDCROSS" Or sSpecial = "KN") And oInvoice.MyField = "ISWAP" Then
            '        ' for PayEx-payments we fill in ext.id into Match_ID
            '        sLine = sLine + PadLeft(oInvoice.MATCH_ID, 8, " ") 'BetalingsID -rk_Id, ib_ex_id eller ia_id
            '    ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
            '        sLine = sLine + PadLeft(oInvoice.InvoiceNo, 8, " ") 'BetalingsID -rk_Id, ib_ex_id eller ia_id
            '    Else
            '        If sSpecial = "REDCROSS" Then
            '            sLine = sLine & "     999"
            '        'XokNET - 28.06.2011 - Added next ElseIf
            '        ElseIf sSpecial = "NORSK_FOLKEHJELP" Then
            '            sLine = sLine & "  610009"
            '        Else
            '            sLine = sLine & "   99999"
            '        End If
            '    End If


            '    'Persontype
            '    'Add if company or not
            '    If Not EmptyString(oPayment.ExtraD1) Then
            '        sLine = sLine & Left$(oPayment.ExtraD1, 1)
            '    Else
            '        sLine = sLine & "P"
            '    End If
            '
            '    'Type bedrift, 167-168
            '    '----------------------
            '    If Not EmptyString(oPayment.ExtraD2) Then
            '        sLine = sLine & " " & Left$(oPayment.ExtraD2, 1)
            '    Else
            '        If Left$(oPayment.ExtraD1, 1) = "A" Then
            '            sLine = sLine & " 1"
            '        Else
            '            sLine = sLine & "  "
            '        End If
            '    End If


            '    'Telefonnummer
            '    '--------------
            '    'XokNET - 11.08.2001 - Added next IF
            '    If oPayment.PayCode = "649" Then
            '        sLine = sLine & PadRight(oPayment.REF_Own, 15, " ")
            '    Else
            '        sLine = sLine & Space(15) 'For givertelefon
            '    End If


            '    If sSpecial = "REDCROSS" And oInvoice.MyField = "ISWAP" Then
            '        'NRX doesn't want any freetext written on payEx. It will overwrite the freetext today.
            '        'Changed 12.02.2010 - Added ORDR in front of the text
            '        sLine = sLine & PadRight("ORDR" & oInvoice.InvoiceNo, 80, " ")
            '        'Old code
            '        'sLine = sLine & PadRight(oInvoice.InvoiceNo, 80, " ")
            '    Else
            '        'XokNET - 28.06.2011 - Changed next line
            '        sLine = sLine & PadRight(Replace(Left$(Replace(Replace(sFreeTextVariable, vbCr, vbNullString), vbLf, vbNullString), 80), "|", " "), 80, " ")
            '    End If

        End If 'If oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then

        WriteNRX_AxTransaction = sLineToWrite

    End Function
    'XNET - 10.10.2012 - Added next function
    Function WriteColumbus_Ax(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal bPostAgainstObservationAccount As Boolean) As Boolean

        ' 03.01.05
        ' Used by Grieg Logistics, Special = GRIEG
        ' Used by Vingcard Elsafe, Special = VINGCARD

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oDummyPayment As vbBabel.Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, dDate As Date ', sIAccountNo As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim sAdr1 As String, sAdr2 As String, sZip As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sId As String
        Dim sFreetext As String
        Dim sRef1 As String, sRef2 As String
        Dim sPayCode As String
        Dim sBirthNumber As String
        Dim sGLAccount As String
        Dim sGLAccountDim1 As String
        Dim sGLAccountDim2 As String
        Dim sGLAccountDim3 As String
        Dim sGLAccountDim4 As String
        Dim sGLAccountDim5 As String
        Dim sGLAccountDim6 As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sNoMatchAccount As String
        Dim sNoMatchAccountType As String
        Dim lBatchcounter As Long
        Dim iFreetextCounter As Integer
        Dim lCounter As Long
        Dim bGLRecordWritten As Boolean
        Dim sNoMatchAccountAR As String
        Dim sDim1NoMatchAR As String
        Dim sDim2NoMatchAR As String
        Dim sDim3NoMatchAR As String
        Dim sDim4NoMatchAR As String
        Dim sDim5NoMatchAR As String
        Dim sDim6NoMatchAR As String
        Dim sNoMatchAccountAP As String ' = "2471"
        Dim sDim1NoMatchAP As String
        Dim sDim2NoMatchAP As String
        Dim sDim3NoMatchAP As String
        Dim sDim4NoMatchAP As String
        Dim sDim5NoMatchAP As String
        Dim sDim6NoMatchAP As String
        Dim sAccountCurrency As String
        Dim sTransactionDate As String
        Dim lArrayCounter As Long
        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim sSpecial As String


        Try

            bAppendFile = False
            sOldAccountNo = vbNullString
            sGLAccount = vbNullString
            bGLRecordWritten = False
            bAccountFound = False
            lBatchcounter = 0
            sNoMatchAccountAP = ""
            sAccountCurrency = ""
            sTransactionDate = Format(Now(), "dd.MM.yyyy")
            sNoMatchAccountType = ""

            sSpecial = UCase(oBabelFiles(1).Special)

            ' XNET 29.08.2012 - added specialhanding for Grieg - remove Forskuddsposter, and reduce bankamount
            If sSpecial = "GRIEG" Then
                Grieg_PrepareExport(oBabelFiles)
            ElseIf sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then
                Vingcard_AddUnmatched(oBabelFiles)
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim Preserve aAmountArray(UBound(aAccountArray, 2))

            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'XOKNET - 06.03.2012 - removed next IF (OCR may be exported!)
                        'If IsOCR(oPayment.PayCode) Then
                        'Nothing to do, don't export
                        'Else
                        'XOKNET - 09.05.2012 - Added next IF
                        If Not oPayment.Exported Then
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        'End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'XOKNET - 06.03.2012 - removed next IF (OCR may be exported!)
                            'If IsOCR(oPayment.PayCode) Then
                            'Nothing to do, don't export
                            'Else
                            'XOKNET - 09.05.2012 - Added next IF
                            If Not oPayment.Exported Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            bGLRecordWritten = False
                            lBatchcounter = lBatchcounter + 1
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'XOKNET - 06.03.2012 - removed next IF (OCR may be exported!)
                                'If IsOCR(oPayment.PayCode) Then
                                'Nothing to do, don't export
                                'Else
                                'XOKNET - 09.05.2012 - Added next IF
                                If Not oPayment.Exported Then
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If oPayment.REF_Own = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                'End If

                                If bExportoPayment Then

                                    If oPayment.I_Account <> sOldAccountNo Then
                                        If oPayment.VB_ProfileInUse Then
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                                            sGLAccount = oaccount.GLAccount.Trim
                                                            sGLAccountDim1 = oaccount.Dim1.Trim
                                                            sGLAccountDim2 = oaccount.Dim2.Trim
                                                            sGLAccountDim3 = oaccount.Dim3.Trim
                                                            sGLAccountDim4 = oaccount.Dim4.Trim
                                                            sGLAccountDim5 = oaccount.Dim5.Trim
                                                            sGLAccountDim6 = oaccount.Dim6.Trim
                                                            sOldAccountNo = oPayment.I_Account
                                                            sAccountCurrency = oaccount.CurrencyCode
                                                            bAccountFound = True
                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                            If bAccountFound Then
                                                'Find the correct account in the array
                                                For lCounter = 0 To UBound(aAccountArray, 2)
                                                    If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                        lArrayCounter = lCounter
                                                        Exit For
                                                    End If
                                                Next lCounter
                                            End If

                                            If oBabel.VB_ProfileInUse Then
                                                If bAccountFound Then
                                                    sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                    sNoMatchAccountType = GetNoMatchAccountType(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                    sNoMatchAccountAR = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID, sDim1NoMatchAR, sDim2NoMatchAR, sDim3NoMatchAR, sDim4NoMatchAR, sDim5NoMatchAR, sDim6NoMatchAR)
                                                    sNoMatchAccountAP = GetAccountFromName(oBabel.VB_Profile.Company_ID, oClient.Client_ID, "UNSETTLED AP BB", sDim1NoMatchAP, sDim2NoMatchAP, sDim3NoMatchAP, sDim4NoMatchAP, sDim5NoMatchAP, sDim6NoMatchAP)
                                                Else
                                                    Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                                    '12003: Could not find account %1.Please enter the account in setup."
                                                End If
                                            End If
                                        End If
                                    End If

                                    '-------- fetch content of each payment ---------
                                    sName = Replace(oPayment.E_Name, ";", ",")   ' 22.03.2018 ISINGRUD GLEMT DATO
                                    sAdr1 = vbNullString   'vbNullString
                                    sAdr2 = vbNullString   'vbNullString
                                    ' If Adr2="-", empty Adr2;
                                    If Trim$(oPayment.E_Adr2) = "-" Then
                                        oPayment.E_Adr2 = vbNullString
                                    End If

                                    If Len(Trim$(oPayment.E_Adr2)) = 0 Then
                                        ' empty adr2, put opayment.e_adr1 into eksportfiles adr2
                                        sAdr2 = Replace(oPayment.E_Adr1, ";", ",")  'Special for LHL, adr2 is the mainadresse
                                    Else
                                        ' both addresslines in use
                                        sAdr1 = Replace(oPayment.E_Adr1, ";", ",")
                                        sAdr2 = Replace(oPayment.E_Adr2, ";", ",")
                                    End If

                                    sZip = Replace(oPayment.E_Zip, ";", ",")
                                    nAmount = oPayment.MON_TransferredAmount

                                    'sArchiveRef = oPayment.REF_Bank1
                                    ' Skal benyttes til avstemming bank

                                    dDate = StringToDate(oPayment.DATE_Payment)
                                    '12.09.2006 - new code to write the correct archiveref for bankreconciliation
                                    sArchiveRef = Trim$(oBatch.REF_Bank)
                                    For lCounter = 1 To Len(sArchiveRef)
                                        If Left$(sArchiveRef, 1) = "*" Then
                                            sArchiveRef = Mid$(sArchiveRef, 2)
                                        Else
                                            Exit For
                                        End If
                                    Next lCounter
                                    If oBatch.Payments.Count = 1 And Not EmptyString(oPayment.REF_Bank1) Then
                                        sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                        sRef1 = oBatch.REF_Bank
                                    Else
                                        sLine = sLine & PadLeft(sArchiveRef, 15, " ")
                                        sRef1 = oPayment.REF_Bank1
                                    End If
                                    sRef2 = oPayment.REF_Bank2

                                    sFreetext = vbNullString

                                    iFreetextCounter = 0
                                    sFreetext = vbNullString
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreetext In oInvoice.Freetexts
                                                If oFreetext.Qualifier < 3 Then
                                                    iFreetextCounter = iFreetextCounter + 1
                                                    If iFreetextCounter = 1 Then
                                                        sFreetext = RTrim$(oFreetext.Text)
                                                    ElseIf iFreetextCounter = 2 Then
                                                        sFreetext = sFreetext & " " & RTrim$(oFreetext.Text)
                                                    Else
                                                        Exit For
                                                    End If
                                                End If
                                            Next oFreetext
                                        End If
                                    Next oInvoice
                                    sFreetext = sFreetext & " - " & Trim$(oPayment.E_Name)

                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MON_InvoiceAmount <> 0 Then
                                                ' XOKNET 15.05.2012 - added GL-record
                                                ' Kan dette under v�re generelt, uten � �delegge NRX?
                                                sLine = ""
                                                If oInvoice.MATCH_Matched = False Then
                                                    If sNoMatchAccountType = "2" Then 'GL
                                                        sLine = WriteColumbus_AxGLRecord(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate)
                                                    Else
                                                        sLine = WriteColumbus_AxTransaction(UCase(oBabel.Special), sName, sAdr1, sAdr2, sZip, nAmount, dDate, TransformDnBNORReference(sArchiveRef), sId, oPayment.I_Account, oPayment.E_Account, TransformDnBNORReference(sRef1), TransformDnBNORReference(sRef2), sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oInvoice, sTransactionDate)
                                                    End If
                                                ElseIf oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                                    sLine = WriteColumbus_AxGLRecord(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate)
                                                    'Add to totalamountexported
                                                    aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oPayment.MON_InvoiceAmount
                                                Else
                                                    ' as pre 15.05.2012
                                                    sLine = WriteColumbus_AxTransaction(UCase(oBabel.Special), sName, sAdr1, sAdr2, sZip, nAmount, dDate, TransformDnBNORReference(sArchiveRef), sId, oPayment.I_Account, oPayment.E_Account, TransformDnBNORReference(sRef1), TransformDnBNORReference(sRef2), sFreetext, oPayment.PayCode, oPayment, sGLAccount, sNoMatchAccount, oInvoice, sTransactionDate)
                                                    'Add to totalamountexported
                                                    aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oPayment.MON_InvoiceAmount
                                                End If
                                                If Len(sLine) > 0 Then
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If
                                        End If
                                    Next oInvoice

                                    'Write the debet posting against the observation account (move the amount from this account)
                                    If bPostAgainstObservationAccount Then
                                        If oPayment.PayCode = "901" Then
                                            sLine = WriteColumbus_AxBankRecord(UCase(oBabel.Special), oBatch, oClient, oPayment.MON_InvoiceCurrency & sNoMatchAccountAP, sAccountCurrency, sTransactionDate, oPayment, bPostAgainstObservationAccount, sFreetext, sDim1NoMatchAP, sDim2NoMatchAP, sDim3NoMatchAP, sDim4NoMatchAP, sDim5NoMatchAP, sDim6NoMatchAP)
                                        ElseIf oPayment.PayCode = "902" Then
                                            sLine = WriteColumbus_AxBankRecord(UCase(oBabel.Special), oBatch, oClient, oPayment.MON_InvoiceCurrency & sNoMatchAccountAR, sAccountCurrency, sTransactionDate, oPayment, bPostAgainstObservationAccount, sFreetext, sDim1NoMatchAR, sDim2NoMatchAR, sDim3NoMatchAR, sDim4NoMatchAR, sDim5NoMatchAR, sDim6NoMatchAR)
                                        Else
                                            If sSpecial = "VINGCARD_UK" Then
                                                sLine = WriteColumbus_AxBankRecord(UCase(oBabel.Special), oBatch, oClient, sNoMatchAccountAR, sAccountCurrency, sTransactionDate, oPayment, bPostAgainstObservationAccount, sFreetext, sDim1NoMatchAR, sDim2NoMatchAR, sDim3NoMatchAR, sDim4NoMatchAR, sDim5NoMatchAR, sDim6NoMatchAR, True)
                                            Else
                                                sLine = WriteColumbus_AxBankRecord(UCase(oBabel.Special), oBatch, oClient, sAccountCurrency & sNoMatchAccountAR, sAccountCurrency, sTransactionDate, oPayment, bPostAgainstObservationAccount, sFreetext, sDim1NoMatchAR, sDim2NoMatchAR, sDim3NoMatchAR, sDim4NoMatchAR, sDim5NoMatchAR, sDim6NoMatchAR)
                                            End If
                                        End If

                                        If Not EmptyString(sLine) Then
                                            oFile.WriteLine(sLine)
                                        End If
                                    Else
                                        If (sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK") And oPayment.PayCode <> "901" And oPayment.PayCode <> "902" Then
                                            sLine = WriteColumbus_AxBankRecord(sSpecial, oBatch, oClient, sGLAccount, sAccountCurrency, sTransactionDate, oPayment, bPostAgainstObservationAccount, "", sGLAccountDim1, sGLAccountDim2, sGLAccountDim3, sGLAccountDim4, sGLAccountDim5, sGLAccountDim6)
                                            If Not EmptyString(sLine) Then
                                                oFile.WriteLine(sLine)
                                            End If
                                        End If
                                    End If

                                    '                        'Write the debet posting against the observation account (move the amount from this account)
                                    '                        If bPostAgainstObservationAccount Then
                                    '                            If oPayment.PayCode = "901" Then
                                    '                                sLine = WriteColumbus_AxGLRecord(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate, bPostAgainstObservationAccount, sNoMatchAccountAP)
                                    '                            Else
                                    '                                sLine = WriteColumbus_AxGLRecord(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate, bPostAgainstObservationAccount, sNoMatchAccountAR)
                                    '                            End If
                                    '
                                    '                            If Not EmptyString(sLine) Then
                                    '                                oFile.WriteLine sLine
                                    '                            End If
                                    '                        End If
                                    '
                                    '                        If sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" And oPayment.PayCode <> "901" And oPayment.PayCode <> "902" Then
                                    '                            sLine = WriteColumbus_AxBankRecord(sSpecial, oBatch, oClient, sGLAccount, sAccountCurrency, sTransactionDate, oPayment)
                                    '                            If Not EmptyString(sLine) Then
                                    '                                oFile.WriteLine sLine
                                    '                            End If
                                    '                        End If

                                    sPayCode = oPayment.PayCode

                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment

                            'XOKNET - 28.02.2012 - Moved code from above

                            'Write GL-line after each record
                            If Not bPostAgainstObservationAccount Then
                                If sSpecial <> "VINGCARD" And sSpecial <> "VINGCARD_UK" Or (sPayCode = "901" Or sPayCode = "902") Then 'Written for each payment
                                    If Not bGLRecordWritten Then
                                        ' XOKNET 29.03.2012 added sSpecial as first parameter (same as in _AxTransaction)
                                        sLine = WriteColumbus_AxBankRecord(sSpecial, oBatch, oClient, sGLAccount, sAccountCurrency, sTransactionDate, oDummyPayment, bPostAgainstObservationAccount, "", sGLAccountDim1, sGLAccountDim2, sGLAccountDim3, sGLAccountDim4, sGLAccountDim5, sGLAccountDim6)
                                        'XOKNET - 22.05.2012 - Added next IF for NRX in case there only is a GL-posting in the batch
                                        If Not EmptyString(sLine) Then
                                            oFile.WriteLine(sLine)
                                        End If
                                        bGLRecordWritten = True
                                    End If
                                End If
                            End If

                        End If
                    Next 'batch
                End If

            Next

            '04.12.2008 - added next IF
            If oBabelFiles(1).Special = "VINGCARD" Or oBabelFiles(1).Special = "VINGCARD_UK" Then
                For lCounter = 0 To UBound(aAccountArray, 2)
                    dbUpDayTotals(False, DateToString(Now()), aAmountArray(lCounter), "Matched_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                Next lCounter
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteColumbus_Ax" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteColumbus_Ax = True

    End Function
    'XNET - 10.10.2012 - Added next function
    Private Function WriteColumbus_AxBankRecord(ByVal sSpecial As String, ByVal oBatch As vbBabel.Batch, ByVal oClient As vbBabel.Client, ByVal sGLAccount As String, ByVal sAccountCurrency As String, ByVal sTransactionDate As String, ByVal oPayment As vbBabel.Payment, ByVal bPostAgainstObservationAccount As Boolean, ByVal sFreetext As String, ByVal sDim1 As String, ByVal sDim2 As String, ByVal sDim3 As String, ByVal sDim4 As String, ByVal sDim5 As String, ByVal sDim6 As String, Optional ByVal bUseFAsTransactionType As Boolean = False) As String
        'Nr  Felt - Type - Kommentar - Felt i Axapta - Felt i BB
        '1   Kontotype - Str - F = Finans - AccountType - Batch
        '2   Kontonummer - Str - Bankkonto - AccountID - Payment.I_Account
        '3   Transaksjonsdato - Date - Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY - TransDate - Payment.DateValue
        '4   Bel�p - Real - Bel�p i valuta. - (minus) settes for negative bel�p.Desimalskille settes til punktum - AmountCurDebit - Batch.MonInvoiceAmount
        '5   Valuta - Str - Standard ISO-koder (NOK, EUR etc..) - CurrencyCode - Payment.MonInvoiceCurrency
        '6   Valutakurs - Real - Valutakurs p� innbetaling, ved NOK settes kurs = 100. Desimalskille settes til punktum - ExchRate - 100
        '7   Tekst - Str 60 posisjoner - Txt - Payment.PayType?
        '8   Betalings-referanse - Str - Referanse til kontoutdrag - PaymentReference - Batch.RefBank

        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim oLocalPayment As vbBabel.Payment
        Dim oLocalInvoice As vbBabel.Invoice
        Dim nAmountToUse As Double
        Dim iCounter As Integer
        Dim sI_Account As String
        Dim sDATE_Value As String
        Dim sDate_Payment As String
        Dim sMON_InvoiceCurrency As String
        Dim sMON_LocalExchangeRate As String
        Dim sPayCode As String

        iCounter = 0
        nAmountToUse = 0
        sI_Account = ""
        sDATE_Value = ""
        sDate_Payment = ""
        sMON_InvoiceCurrency = ""
        sMON_LocalExchangeRate = ""
        sPayCode = ""
        If sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then

            If oPayment Is Nothing Then
                sI_Account = Trim(oBatch.Payments.Item(1).I_Account)
                sDATE_Value = oBatch.Payments.Item(1).DATE_Value
                sDate_Payment = oBatch.Payments.Item(1).DATE_Payment
                sPayCode = oBatch.Payments.Item(1).PayCode
                If oBatch.Payments.Item(1).PayCode = "901" Or oBatch.Payments.Item(1).PayCode = "902" Then
                    'Nettingfile
                    sMON_InvoiceCurrency = sAccountCurrency
                    sMON_LocalExchangeRate = 100
                    nAmountToUse = 0
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayCode = "901" Then
                            nAmountToUse = nAmountToUse - oPayment.MON_LocalAmount
                        Else
                            nAmountToUse = nAmountToUse + oPayment.MON_LocalAmount
                        End If
                    Next oPayment
                Else
                    sMON_InvoiceCurrency = oPayment.MON_InvoiceCurrency
                    sMON_LocalExchangeRate = Trim(Str(oPayment.MON_LocalExchRate))
                    nAmountToUse = oPayment.MON_LocalAmount
                End If
            Else
                sI_Account = Trim(oPayment.I_Account)
                sDATE_Value = oPayment.DATE_Value
                sDate_Payment = oPayment.DATE_Payment
                sPayCode = oPayment.PayCode
                sMON_InvoiceCurrency = oPayment.MON_InvoiceCurrency
                sMON_LocalExchangeRate = Trim(Str(oPayment.MON_LocalExchRate))
                nAmountToUse = oPayment.MON_InvoiceAmount
                '01.02.2013 - This will be used to post the payment 'away' from the observation account for outgoing payments.
                If oPayment.PayCode = "901" Then
                    nAmountToUse = nAmountToUse * -1
                End If
            End If

            '    If oBatch.Payments.Count > 0 Then
            '        If oBatch.Payments.Item(1).PayCode = "901" Or oBatch.Payments.Item(1).PayCode = "902" Then
            '            'Nettingfile
            '            nAmountToUse = 0
            '            For Each oLocalPayment In oBatch.Payments
            '                If oLocalPayment.PayCode = "901" Then
            '                    nAmountToUse = nAmountToUse - oLocalPayment.MON_LocalAmount
            '                Else
            '                    nAmountToUse = nAmountToUse + oLocalPayment.MON_LocalAmount
            '                End If
            '            Next oLocalPayment
            '        End If
            '    End If
        Else
            For Each oLocalPayment In oBatch.Payments
                iCounter = iCounter + 1
                If iCounter = 1 Then
                    sI_Account = Trim(oLocalPayment.I_Account)
                    sDATE_Value = oLocalPayment.DATE_Value
                    sDate_Payment = oLocalPayment.DATE_Payment
                    sMON_InvoiceCurrency = oLocalPayment.MON_InvoiceCurrency
                    sMON_LocalExchangeRate = Trim(Str(oLocalPayment.MON_LocalExchRate))
                    sPayCode = oLocalPayment.PayCode
                    If sPayCode = "901" Or sPayCode = "902" Then
                        sMON_InvoiceCurrency = sAccountCurrency
                        sMON_LocalExchangeRate = 100
                    End If
                End If
                nAmountToUse = oBatch.MON_InvoiceAmount
            Next oLocalPayment
        End If


        sLineToWrite = vbNullString

        'If nAmountToUse > 0 Then
        ' XNET 29.08.2012 - in special cases, with credits and forskudd, Grieg may have "negative" bankamount"
        If nAmountToUse <> 0 Then
            If bUseFAsTransactionType Then 'Added 19.05.2018 for Vingcard_UK
                sLineToWrite = "F;;;" 'Kontotype
            Else
                sLineToWrite = "B;;;" 'Kontotype
            End If

            ' XOKNET 29.03.2012 - Grieg must have GL-account for bank, not "real accountnumber"
            '15.06.2015 - Added next IF for Vingcard
            If sGLAccount.IndexOf("-") > 0 Then
                sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 1) & ";;" ' GL konto for bank
            Else
                sLineToWrite = sLineToWrite & sGLAccount & ";;" ' GL konto for bank
            End If
            If sSpecial = "VINGCARD_UK" Then '19.05.2018 - Added this IF
                sDateToUse = vbNullString 'Transactiondate
                If Not sDate_Payment = "19900101" Then
                    sDateToUse = Right(sDate_Payment, 2) & "." & Mid(sDate_Payment, 5, 2) & "." & Left(sDate_Payment, 4)
                Else 'Use payment_date
                    If Not sDATE_Value = "19900101" Then
                        sDateToUse = Right(sDATE_Value, 2) & "." & Mid(sDATE_Value, 5, 2) & "." & Left(sDATE_Value, 4)
                    Else
                        sDateToUse = sTransactionDate
                    End If
                End If
                sLineToWrite = sLineToWrite & sDateToUse & ";" 'Transaksjonsdato
            Else
                sLineToWrite = sLineToWrite & sTransactionDate & ";" 'Transaksjonsdato
            End If

            sLineToWrite = sLineToWrite & ConvertFromAmountToString(nAmountToUse, "", ".") & ";" 'Bel�p
            sLineToWrite = sLineToWrite & Replace(sMON_InvoiceCurrency, ";", ",") & ";" 'Valuta
            If sMON_LocalExchangeRate = "0" Or sMON_LocalExchangeRate = "100" Then
                sLineToWrite = sLineToWrite & "100;;"
            Else
                sLineToWrite = sLineToWrite & sMON_LocalExchangeRate & ";;"
            End If
            'sLineToWrite = sLineToWrite & "100;;" ' Exch.rate against basevaluta

            '20.11.2002 changed lines below
            If sPayCode = "901" Or sPayCode = "902" Then
                '01.02.2013 - Changed from "Netting" to sFreetext when posting aginst observationaccount
                If bPostAgainstObservationAccount Then
                    sLineToWrite = sLineToWrite & Replace(sFreetext, ";", ",") & ";" '& ";;;;;;;;;;;" 'Betalings-referanse '01.02.2013
                Else
                    sLineToWrite = sLineToWrite & "Netting" & ";" '& ";;;;;;;;;;;" 'Betalings-referanse '01.02.2013
                End If
            Else
                If bPostAgainstObservationAccount Then
                    sLineToWrite = sLineToWrite & Replace(sFreetext, ";", ",") & ";" '& ";;;;;;;;;;;" 'Betalings-referanse
                Else
                    sLineToWrite = sLineToWrite & Replace(oBatch.REF_Bank, ";", ",") & ";" '& ";;;;;;;;;;;" 'Betalings-referanse
                End If
            End If
            sLineToWrite = sLineToWrite & sPayCode & ";;;;;;;;;;;" ' Txt - Add VoucherNo?
            'Old code
            '    sLineToWrite = sLineToWrite & sPayCode & ";" ' Txt - Add VoucherNo?
            '    If sPayCode = "901" Or sPayCode = "902" Then
            '        sLineToWrite = sLineToWrite & "Netting" & ";;;;;;;;;;;" 'Betalings-referanse
            '    Else
            '        sLineToWrite = sLineToWrite & oBatch.REF_Bank & ";;;;;;;;;;;" 'Betalings-referanse
            '    End If


            sLineToWrite = sLineToWrite & ";"  ' Form�l/Purpose (felt 23)
            sDateToUse = vbNullString 'Transactiondate
            If Not sDate_Payment = "19900101" Then
                sDateToUse = Right(sDate_Payment, 2) & "." & Mid(sDate_Payment, 5, 2) & "." & Left(sDate_Payment, 4)
            Else 'Use payment_date
                If Not sDATE_Value = "19900101" Then
                    sDateToUse = Right(sDATE_Value, 2) & "." & Mid(sDATE_Value, 5, 2) & "." & Left(sDATE_Value, 4)
                Else
                    sDateToUse = CStr(Format(Now(), "DDMMYYYY"))
                End If
            End If
            sLineToWrite = sLineToWrite & sDateToUse  ' DocumentDate (felt 24)
            If sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then
                '15.06.2015 - Added this section for Vingcard to deal with dimensions
                If sGLAccount.IndexOf("-") > 0 Then
                    sLineToWrite = sLineToWrite & ";" & xDelim(sGLAccount, "-", 2) & ";" ' Dim1 (felt 25)
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 3) & ";" ' Dim2 (felt 26)
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 4) & ";" ' Dim3 (felt 27)
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 5) & ";" ' Dim4 (felt 28)
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 6) & ";" ' Dim5 (felt 29)
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 7) ' Dim6 (felt 30)
                Else
                    'sLineToWrite = sLineToWrite & ";;;;;;" ' Dimensions
                    sLineToWrite = sLineToWrite & ";" & sDim1 & ";" ' Dim1 (felt 25)
                    sLineToWrite = sLineToWrite & sDim2 & ";" ' Dim2 (felt 26)
                    sLineToWrite = sLineToWrite & sDim3 & ";" ' Dim3 (felt 27)
                    sLineToWrite = sLineToWrite & sDim4 & ";" ' Dim4 (felt 28)
                    sLineToWrite = sLineToWrite & sDim5 & ";" ' Dim5 (felt 29)
                    sLineToWrite = sLineToWrite & sDim6 ' Dim6 (felt 30)
                End If
            End If
        End If

        WriteColumbus_AxBankRecord = sLineToWrite

    End Function
    'XNET - 10.10.2012 - Added next function
    Private Function WriteColumbus_AxGLRecord(ByVal sSpecial As String, ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sTransactionDate As String, Optional ByVal bPostAgainstObservationAccount As Boolean = False, Optional ByVal sGLAccount As String = "") As String
        'Nr  Felt - Type - Kommentar - Felt i Axapta - Felt i BB
        '1   Kontotype - Str - F = Finans - AccountType - Batch
        '2   Kontonummer - Str - Bankkonto - AccountID - Payment.I_Account
        '3   Transaksjonsdato - Date - Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY - TransDate - Payment.DateValue
        '4   Bel�p - Real - Bel�p i valuta. - (minus) settes for negative bel�p.Desimalskille settes til punktum - AmountCurDebit - Batch.MonInvoiceAmount
        '5   Valuta - Str - Standard ISO-koder (NOK, EUR etc..) - CurrencyCode - Payment.MonInvoiceCurrency
        '6   Valutakurs - Real - Valutakurs p� innbetaling, ved NOK settes kurs = 100. Desimalskille settes til punktum - ExchRate - 100
        '7   Tekst - Str 60 posisjoner - Txt - Payment.PayType?
        '8   Betalings-referanse - Str -

        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim oLocalPayment As vbBabel.Payment
        Dim oLocalInvoice As vbBabel.Invoice
        Dim nAmountToUse As Double
        Dim iCounter As Integer
        Dim sFreetext As String

        'iCounter = 0
        'nAmountToUse = 0
        'sI_Account = ""
        'sDATE_Value = ""
        'sDate_Payment = ""
        'sMON_InvoiceCurrency = ""
        'sPayCode = ""
        'For Each oLocalPayment In oBatch.Payments
        '    iCounter = iCounter + 1
        '    If iCounter = 1 Then
        '        sI_Account = Trim(oLocalPayment.I_Account)
        '        sDATE_Value = oLocalPayment.DATE_Value
        '        sDate_Payment = oLocalPayment.DATE_Payment
        '        sMON_InvoiceCurrency = oLocalPayment.MON_InvoiceCurrency
        '        sPayCode = oLocalPayment.PayCode
        '    End If
        '    For Each oLocalInvoice In oLocalPayment.Invoices
        '        If oLocalInvoice.MATCH_Final Then
        '            If oLocalInvoice.MATCH_MatchType <> MatchedOnGL Then
        '                nAmountToUse = nAmountToUse + oLocalInvoice.MON_InvoiceAmount
        '            End If
        '        End If
        '    Next oLocalInvoice
        'Next oLocalPayment

        sLineToWrite = vbNullString
        sLineToWrite = "F;;;" 'Kontotype
        ' XOKNET 29.03.2012 - Grieg must have GL-account for bank, not "real accountnumber"
        If bPostAgainstObservationAccount Then
            sLineToWrite = sLineToWrite & sGLAccount & ";" ' Kontonummer
        Else
            sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 1) & ";" 'oInvoice.MATCH_ID & ";" ' Kontonummer
        End If
        If bPostAgainstObservationAccount Then
            sLineToWrite = sLineToWrite & ";" 'Avdeling/Department
        Else
            If Not EmptyString(xDelim(oInvoice.MATCH_ID, "-", 2)) Then
                sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 2) & ";" 'Avdeling/Department
            Else
                sLineToWrite = sLineToWrite & ";" 'Avdeling/Department
            End If
        End If
        'End If ' End XOKNET 29.03.2012
        'sDateToUse = vbNullString
        'If Not sDATE_Value = "19900101" Then
        '    sDateToUse = Right(sDATE_Value, 2) & "." & Mid(sDATE_Value, 5, 2) & "." & Left(sDATE_Value, 4)
        'Else 'Use payment_date
        '    If Not sDate_Payment = "19900101" Then
        '        sDateToUse = Right(sDate_Payment, 2) & "." & Mid(sDate_Payment, 5, 2) & "." & Left(sDate_Payment, 4)
        '    Else
        '        sDateToUse = CStr(Format(Now(), "DDMMYYYY"))
        '    End If
        'End If
        If sSpecial = "VINGCARD_UK" Then '19.05.2018 - Added this IF
            sDateToUse = vbNullString 'Transactiondate
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = Right(oPayment.DATE_Payment, 2) & "." & Mid(oPayment.DATE_Payment, 5, 2) & "." & Left(oPayment.DATE_Payment, 4)
            Else 'Use payment_date
                If Not oPayment.DATE_Value = "19900101" Then
                    sDateToUse = Right(oPayment.DATE_Value, 2) & "." & Mid(oPayment.DATE_Value, 5, 2) & "." & Left(oPayment.DATE_Value, 4)
                Else
                    sDateToUse = sTransactionDate
                End If
            End If
            sLineToWrite = sLineToWrite & sDateToUse & ";" 'Transaksjonsdato
        Else
            sLineToWrite = sLineToWrite & sTransactionDate & ";" 'Transaksjonsdato
        End If
        'XOKNET 06.06.2012 - feil fortegn p� hovedboksposter for Grieg
        If bPostAgainstObservationAccount Then
            If oPayment.PayCode = "901" Then
                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oPayment.MON_InvoiceAmount * -1, "", ".") & ";" 'Bel�p
            Else
                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, "", ".") & ";" 'Bel�p
            End If
        Else
            'XNET - 06.11.2012 - Added next IF
            If oPayment.PayCode = "901" Then
                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, "", ".") & ";" 'Bel�p
            Else
                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".") & ";" 'Bel�p
            End If
        End If
        sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta

        If oPayment.MON_LocalExchRate = "0" Or oPayment.MON_LocalExchRate = "100" Then
            sLineToWrite = sLineToWrite & "100;;"
        Else
            sLineToWrite = sLineToWrite & Trim(Str(oPayment.MON_LocalExchRate)) & ";;"
        End If

        sFreetext = ""
        If bPostAgainstObservationAccount Then
            For Each oLocalInvoice In oPayment.Invoices
                If oLocalInvoice.MATCH_Original Then
                    For iCounter = 1 To oLocalInvoice.Freetexts.Count
                        sFreetext = sFreetext & oLocalInvoice.Freetexts(iCounter).Text
                    Next iCounter
                End If
            Next oLocalInvoice
        Else
            For iCounter = 1 To oInvoice.Freetexts.Count
                sFreetext = sFreetext & oInvoice.Freetexts(iCounter).Text
            Next iCounter
        End If
        'sFreetext = Trim(oPayment.VoucherNo) & " " & sFreetext
        sFreetext = sFreetext & " " & Trim(oPayment.VoucherNo)
        sFreetext = Replace(sFreetext, ";", ",")
        sLineToWrite = sLineToWrite & Left$(Replace(sFreetext, ";", ","), 60) & ";" ' Txt - Add VoucherNo?
        sLineToWrite = sLineToWrite & ";;;;;;;;;;;"   ' Trenger vi noe her ?  oBatch.REF_Bank 'Betalings-referanse

        If bPostAgainstObservationAccount Then
            sLineToWrite = sLineToWrite & ";" 'Form�l/Purpose
        Else
            If Not EmptyString(xDelim(oInvoice.MATCH_ID, "-", 3)) Then
                sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 3) & ";" 'Form�l/Purpose
            Else
                sLineToWrite = sLineToWrite & ";" 'Form�l/Purpose
            End If
        End If

        sDateToUse = vbNullString 'Transactiondate
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = Right(oPayment.DATE_Payment, 2) & "." & Mid(oPayment.DATE_Payment, 5, 2) & "." & Left(oPayment.DATE_Payment, 4)
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = Right(oPayment.DATE_Value, 2) & "." & Mid(oPayment.DATE_Value, 5, 2) & "." & Left(oPayment.DATE_Value, 4)
            Else
                sDateToUse = CStr(Format(Now(), "DDMMYYYY"))
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse  ' DocumentDate (felt 24)

        If sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then
            '15.06.2015 - Added this section for Vingcard to deal with dimensions
            If bPostAgainstObservationAccount Then
                If sGLAccount.IndexOf("-") > 0 Then
                    sLineToWrite = sLineToWrite & ";" & xDelim(sGLAccount, "-", 2) & ";" ' Dim1
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 3) & ";" ' Dim2
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 4) & ";" ' Dim3
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 5) & ";" ' Dim4
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 6) & ";" ' Dim5
                    sLineToWrite = sLineToWrite & xDelim(sGLAccount, "-", 7) ' Dim6
                Else
                    sLineToWrite = sLineToWrite & ";;;;;;" ' Dimensions
                End If
            Else
                '03.07.2015 - Temporary code.
                'Right now the dimension must be keyed in manually when posting,
                'By adding the IF, you may use the old way to state a dimension by using - as a delimiter
                If oInvoice.MATCH_ID.IndexOf("-") > 0 Then
                    sLineToWrite = sLineToWrite & ";" & xDelim(oInvoice.MATCH_ID, "-", 2) & ";" ' Dim1
                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 3) & ";" ' Dim2
                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 4) & ";" ' Dim3
                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 5) & ";" ' Dim4
                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 6) & ";" ' Dim5
                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 7) ' Dim6
                Else
                    sLineToWrite = sLineToWrite & ";" & oInvoice.Dim1.Trim & ";" ' Dim1
                    sLineToWrite = sLineToWrite & oInvoice.Dim2.Trim & ";" ' Dim2
                    sLineToWrite = sLineToWrite & oInvoice.Dim3.Trim & ";" ' Dim3
                    sLineToWrite = sLineToWrite & oInvoice.Dim4.Trim & ";" ' Dim4
                    sLineToWrite = sLineToWrite & oInvoice.Dim5.Trim & ";" ' Dim5
                    sLineToWrite = sLineToWrite & oInvoice.Dim6.Trim ' Dim6
                End If
            End If

        End If

            WriteColumbus_AxGLRecord = sLineToWrite

    End Function

    'XNET - 10.10.2012 - Added next function
    Public Function WriteColumbus_AxTransaction(ByVal sSpecial As String, ByVal sName As String, ByVal sAdr1 As String, ByVal sAdr2 As String, ByVal sZip As String, ByVal nAmount As Double, ByVal dDate As Date, ByVal sArchiveRef As String, ByVal sId As String, ByVal sToAccount As String, ByVal sFromAccount As String, ByVal sRef1 As String, ByVal sRef2 As String, ByVal sFreetext As String, ByVal sPayCode As String, ByVal oPayment As vbBabel.Payment, ByVal sGLAccount As String, ByVal sNoMatchAccount As String, ByVal oInvoice As vbBabel.Invoice, ByVal sTransactionDate As String) As String
        'Nr  Felt        Type    Kommentar              Felt i Axapta   Felt i BabelBank
        '1   Kontotype   Str     K = Kunde              AccountType     Payment
        '2   Linjetype   Str     K = KID, m = M�lgruppe ,N = Ny relasjon, S = Eurobate(SMS)   Tolking av innbetalingsposter   Invoice.MyField
        '3   Betalingsreferanse  Str  BabelBank referanse Referansen vil vises i Axapta   PaymReference   Payment.VoucherNo
        '4   Kontonummer Str     Kundenummer            AccountNum      Invoice.CustomerNo
        '5   Dimensjon 3 Str     M�lgruppe - fylles ut n�r linjetype = M og N (S?)   Dimension3
        '6   TransDate   Date    Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY    TransDate   Payment.DateValue
        '7   Bel�p, Greit med minus (SMS) Real Bel�p i valuta. - (minus) settes for negative bel�p
        '    Desimalskille settes til punktum    AmountCurCredit/AmountCurDebet - avhengig av fortegn    Invoice.MonInvoiceAmount
        '8   Valuta      Str     Standard ISO-koder (NOK, EUR etc..) CurrencyCode    Invoice.MonInvoiceCurrency
        '9   Valutakurs  Real    Valutakurs p� innbetaling, ved NOK settes kurs = 100 Desimalskille settes til punktum    ExchRate  "100"
        '10  KID         Str     Legge inn KID om linjetype = K ellers blank    PaymId  Invoice.MatchId
        '11  Tekst       Str 60 posisjoner (Viktig informasjon)
        '    - Bankreferanse
        '    - Navn p� betaler
        '    - Kommentarer   Txt ???
        '12  BetalingsNotat Str  Fritekstfelt, legge p� all tekst som er lagt inn manuelt i BabelBank
        '    - Sjekkes med RK om ytterligere tekst skal eksporteres  PaymentNotes    FreeTxt.Xtext
        '13  Fornavn     Str     F�rste del av navn     FirstName       Payment.Ename
        '14  Mellomnavn  Str     Alt mellom f�rst og siste del av navn   MiddleName  Payment.Ename
        '15  Etternavn   Str     Siste del av navn      LastName        Payment.Ename
        '16  Adresse1    Str     1. linje i adresse     Street (linje1) Payment.EAdr1
        '17  Adresse2    Str     2. linje i adresse     Street (linje2) Payment.EAdr2
        '18  Adresse3    Str     3. linje i adresse     Street (linje3) Payment.EAdr3
        '19  Postnr      Str     Postnummer             ZipCode         Payment.EZip
        '20  Poststed    Str     Poststed               City            Payment.ECity

        '    Kj�nn   Str M, K, I. I skrives ikke over M eller K.
        '- sjekkes med RK ?      ???
        '    Adresseboktype  Str Organisasjon eller Person
        'Ytterligere detaljer?
        'P=Person, A=Bedrift, L=Lokalforening, D=Distrikt. Vi �nsker oss ogs� nye Persontyper som ogs� m� gjenspeiles i PERSON-bildet: S=Skoler, barnehager etc., F=Foreninger (ikke egne), O=Offentlige etater. DirPartyTable.Type  ???
        '    Endringskode    Str Dersom det her st�r Ja skal navn og adresse oppdateres selv om kunde eksisterer.
        'Vi kommer i utgangspunktet ikke til � ta denne i bruk, men det kan bli aktuelt senere.  ??  ???
        '    Betalingsmetode Str B=Bank
        'T=Givertelefon X=PayEx (eller annen Internett-l�sning
        '- Sjekkes med RK    ??  ???
        '21  Betalingstype B=Bank, T=Givertelefon, X=PayEx (eller annen Internett-l�sning)

        '22  Kontonummer betaler Str Bankkontonummer kunde   BankAccount Payment.EAccount

        '
        Dim sLineToWrite As String
        Dim sLineType As String
        Dim s As String
        Dim aNames() As String
        Dim i As Long
        Dim bTheInvoiceIsMatched As Boolean
        Dim sFreeTextVariable As String
        Dim oFreetext As vbBabel.Freetext
        Dim sAmount As String
        Dim sDateToUse As String
        Dim sAccountType As String

        sAccountType = ""

        'New 17.02.2006
        'Add the variable part of the freetext
        If oPayment.PayCode = "901" Or oPayment.PayCode = "902" Then 'Outgoing payment for Vingcard XNET 10.10.2012
            sFreeTextVariable = sFreetext
        Else
            sFreeTextVariable = ""
        End If
        For Each oFreetext In oInvoice.Freetexts
            If oFreetext.Qualifier > 3 Then
                sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
            End If
        Next oFreetext

        sLineToWrite = vbNullString

        If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
            If UCase(Trim$(oInvoice.CustomerNo)) = UCase(Trim$(sNoMatchAccount)) Then
                bTheInvoiceIsMatched = False
            ElseIf EmptyString(oInvoice.CustomerNo) Then
                bTheInvoiceIsMatched = False
            Else
                bTheInvoiceIsMatched = True
            End If

            ' XOKNET 21.06.2012 more changes for Grieg !!!!
            If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                bTheInvoiceIsMatched = False
            End If
            ' XNET 07.09.2012 more changes for Grieg !!!!
            If IsOCR(oPayment.PayCode) Then
                bTheInvoiceIsMatched = True
            End If

            If oPayment.PayCode = "901" Then 'Outgoing payment for Vingcard XNET 10.10.2012
                sLineToWrite = "L;" 'Kontotype
                sAccountType = "L"
            Else
                sLineToWrite = "K;" 'Kontotype
                sAccountType = "K"
            End If
            ''Linjetype   Str     K = KID, m = M�lgruppe ,N = Ny relasjon, S = Eurobate(SMS)
            sLineType = ""
            If Not EmptyString(oInvoice.MyField) And bTheInvoiceIsMatched Then
                sLineType = Left$(oInvoice.MyField, 1)
                If sLineType = "R" Then
                    sLineType = "K" 'KID
                ElseIf sLineType = "I" Then
                    sLineType = "K" 'KID
                End If
            Else
                If bTheInvoiceIsMatched Then
                    ' XNET 03.09.2012 - more changes for Grieg, again ..........
                    'sLineType = "K"
                    sLineType = "F"
                Else
                    sLineType = "N"
                End If
            End If
            sLineToWrite = sLineToWrite & sLineType & ";"
            '3   Betalingsreferanse  Str  BabelBank referanse Referansen vil vises i Axapta   PaymReference   Payment.VoucherNo
            sLineToWrite = sLineToWrite & Trim(oPayment.VoucherNo) & ";"
            '4   Kontonummer Str     Kundenummer            AccountNum      Invoice.CustomerNo
            If Not bTheInvoiceIsMatched Then
                ' unmatched - have created KID for unmatched in special aftermatching GRIEG_ADDKID or WriteSpecial.VINGCARD_ADDUNMATCHED
                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer And sSpecial = "VINGCARD_UK" Then
                    '20.12.2021 - Added for ASSA Abloy DDB
                    sLineToWrite = sLineToWrite & Trim(oInvoice.CustomerNo) & ";"
                Else
                    sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
                End If
            Else
                sLineToWrite = sLineToWrite & Trim(oInvoice.CustomerNo) & ";"
            End If
            '5   Dimensjon 3 Str     M�lgruppe - fylles ut n�r linjetype = M og N (S?)   Dimension3
            If sLineType = "M" And bTheInvoiceIsMatched Then
                sLineToWrite = sLineToWrite & Trim(oInvoice.InvoiceNo) & ";" 'alternatively oInvoice.MATCH_ID - Vil ikke funke hvis man endrer i sprMatched fra I til M
            ElseIf sLineType = "N" Then
                sLineToWrite = sLineToWrite & Trim(oInvoice.InvoiceNo) & ";" 'alternatively oInvoice.MATCH_ID
            Else
                sLineToWrite = sLineToWrite & ";"
            End If
            '6   TransDate   Date    Benytt valuteringsdato fra BabelBank. Format: DDMMYYYY    TransDate   Payment.DateValue
            If sSpecial = "VINGCARD_UK" Then '19.05.2018 - Added this IF
                sDateToUse = vbNullString 'Transactiondate
                If Not oPayment.DATE_Payment = "19900101" Then
                    sDateToUse = Right(oPayment.DATE_Payment, 2) & "." & Mid(oPayment.DATE_Payment, 5, 2) & "." & Left(oPayment.DATE_Payment, 4)
                Else 'Use payment_date
                    If Not oPayment.DATE_Value = "19900101" Then
                        sDateToUse = Right(oPayment.DATE_Value, 2) & "." & Mid(oPayment.DATE_Value, 5, 2) & "." & Left(oPayment.DATE_Value, 4)
                    Else
                        sDateToUse = sTransactionDate
                    End If
                End If
                sLineToWrite = sLineToWrite & sDateToUse & ";" 'Transaksjonsdato
            Else
                sLineToWrite = sLineToWrite & sTransactionDate & ";" 'Transaksjonsdato
            End If
            'sLineToWrite = sLineToWrite & sTransactionDate & ";"
            '7   Bel�p, Greit med minus (SMS) Real Bel�p i valuta. - (minus) settes for negative bel�p
            '    Desimalskille settes til punktum    AmountCurCredit/AmountCurDebet - avhengig av fortegn    Invoice.MonInvoiceAmount
            If sAccountType = "L" Then
                'Payable
                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, "", ".") & ";" 'Bel�p
            Else
                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".") & ";" 'Bel�p
            End If
            '8   Valuta      Str     Standard ISO-koder (NOK, EUR etc..) CurrencyCode    Invoice.MonInvoiceCurrency
            If Len(Trim(oPayment.MON_InvoiceCurrency)) = 3 Then
                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency) & ";"
            Else
                sLineToWrite = sLineToWrite & "NOK;"
            End If
            '9   Valutakurs  Real    Valutakurs p� innbetaling, ved NOK settes kurs = 100 Desimalskille settes til punktum    ExchRate  "100"
            If oPayment.MON_LocalExchRate = 0 Or oPayment.MON_LocalExchRate = 100 Then
                sLineToWrite = sLineToWrite & "100;"
            Else
                sLineToWrite = sLineToWrite & Trim(Str(oPayment.MON_LocalExchRate)) & ";"
            End If
            '10  KID         Str     Legge inn KID om linjetype = K ellers blank    PaymId  Invoice.MatchId
            If sLineType = "K" And bTheInvoiceIsMatched Then
                sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ";"
                ' XNET 03.09.2012 - more changes for Grieg, again ..........
            ElseIf sLineType = "F" Then
                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice And EmptyString(oInvoice.InvoiceNo) Then
                    sLineToWrite = sLineToWrite & Trim(oInvoice.MATCH_ID) & ";" 'Posting against a payment or .....
                Else
                    sLineToWrite = sLineToWrite & Trim(oInvoice.InvoiceNo) & ";"
                End If
            Else
                ' XOKNET 16.04.2012 - Lagt inn If sspecial Grieg ...
                If IsOCR(oPayment.PayCode) Then
                    ' For Grieg, KID-payments, add KID here
                    sLineToWrite = sLineToWrite & oInvoice.Unique_Id & ";"
                ElseIf Not bTheInvoiceIsMatched Then
                    ' unmatched - have created KID for unmatched in special aftermatching GRIEG_ADDKID
                    ' XOKNET 18.06.2012 �yvind in Columbus will need 0 here, not customerNo
                    sLineToWrite = sLineToWrite & "0;"
                Else
                    sLineToWrite = sLineToWrite & ";"
                End If
            End If
            '11  Tekst       Str 60 posisjoner (Viktig informasjon)
            '    - Bankreferanse
            '    - Navn p� betaler
            '    - Kommentarer   Txt ???
            s = ""
            s = sFreeTextVariable
            If Not EmptyString(s) Then
                s = s & " " & Trim(oPayment.VoucherNo)
            Else
                s = Trim(oPayment.VoucherNo)
            End If
            '    If Not EmptyString(oPayment.REF_Bank1) Then
            '        s = "Ref: " & oPayment.REF_Bank1
            '        ' XOKNET 18.06.2012
            '        ' �yvind i Columbus sier at innhold fra felt 12 ikke leses inn i Axapta,og at vi m� samle alt i felt 11
            '        s = s + "-" & sFreeTextVariable
            '        sFreeTextVariable = ""
            '    End If
            s = Replace(s, ";", ",")
            sLineToWrite = sLineToWrite & s & ";"
            '12  BetalingsNotat Str  Fritekstfelt, legge p� all tekst som er lagt inn manuelt i BabelBank
            '    - Sjekkes med RK om ytterligere tekst skal eksporteres  PaymentNotes    FreeTxt.Xtext
            'sLineToWrite = sLineToWrite & sFreeTextVariable & ";"
            sLineToWrite = sLineToWrite & ";"

            ' Blank names and adresses
            '13-21 er blanke
            sLineToWrite = sLineToWrite & ";;;;;;;;;"
            '22  Kontonummer betaler Str Bankkontonummer kunde   BankAccount Payment.EAccount
            If Mid(oPayment.E_Account, 5, 1) <> "9" Then
                sLineToWrite = sLineToWrite & Trim(oPayment.E_Account) & ";"
            Else
                sLineToWrite = sLineToWrite & ";"
            End If
            sLineToWrite = sLineToWrite & ";"  ' Form�l/Purpose

            sDateToUse = vbNullString
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = Right(oPayment.DATE_Payment, 2) & "." & Mid(oPayment.DATE_Payment, 5, 2) & "." & Left(oPayment.DATE_Payment, 4)
            Else 'Use payment_date
                If Not oPayment.DATE_Value = "19900101" Then
                    sDateToUse = Right(oPayment.DATE_Value, 2) & "." & Mid(oPayment.DATE_Value, 5, 2) & "." & Left(oPayment.DATE_Value, 4)
                Else
                    sDateToUse = CStr(Format(Now(), "DDMMYYYY"))
                End If
            End If
            sLineToWrite = sLineToWrite & sDateToUse ' DocumentDate (felt 24)

            If sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then
                '15.06.2015 - Added this section for Vingcard to deal with dimensions
                sLineToWrite = sLineToWrite & ";" & oInvoice.Dim1.Trim & ";" ' Dim1 (felt 25)
                sLineToWrite = sLineToWrite & oInvoice.Dim2.Trim & ";" ' Dim2 (felt 26)
                sLineToWrite = sLineToWrite & oInvoice.Dim3.Trim & ";" ' Dim3 (felt 27)
                sLineToWrite = sLineToWrite & oInvoice.Dim4.Trim & ";" ' Dim4 (felt 28)
                sLineToWrite = sLineToWrite & oInvoice.Dim5.Trim & ";" ' Dim5 (felt 29)
                sLineToWrite = sLineToWrite & oInvoice.Dim6.Trim ' Dim6 (felt 30)

            End If

        End If 'If oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then

        WriteColumbus_AxTransaction = sLineToWrite

    End Function
    ' XNET 29.08.2012 - added specialhanding for Grieg - remove Forskuddsposter, and reduce bankamount
    Sub Grieg_PrepareExport(ByVal oBabelFiles As vbBabel.BabelFiles)
        Dim dForskuddsAmountBatch As Double
        Dim dForskuddsAmountPayment As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim i As Integer
        Dim b As Integer
        Dim p As Integer
        Dim bf As Integer

        For bf = oBabelFiles.Count To 1 Step -1
            oBabel = oBabelFiles(bf)
            For b = oBabel.Batches.Count To 1 Step -1
                dForskuddsAmountBatch = 0
                oBatch = oBabel.Batches(b)
                For p = oBatch.Payments.Count To 1 Step -1
                    dForskuddsAmountPayment = 0
                    oPayment = oBatch.Payments(p)
                    For i = oPayment.Invoices.Count To 1 Step -1
                        oInvoice = oPayment.Invoices(i)
                        If oInvoice.MATCH_ID = "192000" Then
                            ' delete, and reduce bankamount
                            dForskuddsAmountPayment = dForskuddsAmountPayment + oInvoice.MON_InvoiceAmount
                            dForskuddsAmountBatch = dForskuddsAmountBatch + oInvoice.MON_InvoiceAmount
                            oPayment.Invoices.Remove(i)
                        End If
                    Next i
                    oPayment.MON_InvoiceAmount = oPayment.MON_InvoiceAmount - dForskuddsAmountPayment
                    If oPayment.Invoices.Count = 0 Then
                        oBatch.Payments.Remove(p)
                    End If
                Next p
                oBatch.MON_InvoiceAmount = oBatch.MON_InvoiceAmount - dForskuddsAmountBatch
                If oBatch.Payments.Count = 0 Then
                    oBabel.Batches.Remove(b)
                End If
            Next b
            If oBabel.Batches.Count = 0 Then
                oBabelFiles.Remove(bf)
            End If
        Next bf

    End Sub
    Sub REDCROSS_PrepareExportRemoveExportedPayments(ByVal oBabelFiles As vbBabel.BabelFiles)
        Dim dRemovedAmountBatch As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim b As Integer
        Dim p As Integer
        Dim bf As Integer

        For bf = oBabelFiles.Count To 1 Step -1
            oBabel = oBabelFiles(bf)
            For b = oBabel.Batches.Count To 1 Step -1
                dRemovedAmountBatch = 0
                oBatch = oBabel.Batches(b)
                For p = oBatch.Payments.Count To 1 Step -1
                    oPayment = oBatch.Payments(p)
                    If oPayment.Exported = True Then
                        dRemovedAmountBatch = dRemovedAmountBatch + oPayment.MON_InvoiceAmount
                        oBatch.Payments.Remove(p)
                    End If
                Next p
                oBatch.MON_InvoiceAmount = oBatch.MON_InvoiceAmount - dRemovedAmountBatch
                If oBatch.Payments.Count = 0 Then
                    oBabel.Batches.Remove(b)
                End If
            Next b
            If oBabel.Batches.Count = 0 Then
                oBabelFiles.Remove(bf)
            End If
        Next bf

    End Sub
    'XNET - 10.10.2012 - New Sub
    Sub Vingcard_AddUnmatched(ByVal oBabelFiles As vbBabel.BabelFiles)
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim sOldAccountNo As String
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim sNoMatchAccountAR As String
        Dim sNoMatchAccountARDim1 As String
        Dim sNoMatchAccountARDim2 As String
        Dim sNoMatchAccountARDim3 As String
        Dim sNoMatchAccountARDim4 As String
        Dim sNoMatchAccountARDim5 As String
        Dim sNoMatchAccountARDim6 As String
        Dim sNoMatchAccountAP As String
        Dim sNoMatchAccountAPDim1 As String
        Dim sNoMatchAccountAPDim2 As String
        Dim sNoMatchAccountAPDim3 As String
        Dim sNoMatchAccountAPDim4 As String
        Dim sNoMatchAccountAPDim5 As String
        Dim sNoMatchAccountAPDim6 As String
        Dim bAccountFound As Boolean

        sOldAccountNo = ""
        bAccountFound = False

        For Each oBabel In oBabelFiles
            For Each oBatch In oBabel.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.MATCH_Matched <> vbBabel.BabelFiles.MatchStatus.Matched Then
                        If oPayment.I_Account <> sOldAccountNo Then
                            If oPayment.VB_ProfileInUse Then
                                bAccountFound = False
                                For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                                'sGLAccount = Trim$(oAccount.GLAccount)
                                                'sOldAccountNo = oPayment.I_Account
                                                bAccountFound = True

                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then Exit For
                                    Next oClient
                                    If bAccountFound Then Exit For
                                Next oFilesetup
                                If bAccountFound Then
                                    sNoMatchAccountAR = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID, sNoMatchAccountARDim1, sNoMatchAccountARDim2, sNoMatchAccountARDim3, sNoMatchAccountARDim4, sNoMatchAccountARDim5, sNoMatchAccountARDim6)
                                    sNoMatchAccountAP = GetAccountFromName(oBabel.VB_Profile.Company_ID, oClient.Client_ID, "UNSETTLED AP BB", sNoMatchAccountAPDim1, sNoMatchAccountAPDim2, sNoMatchAccountAPDim3, sNoMatchAccountAPDim4, sNoMatchAccountAPDim5, sNoMatchAccountAPDim6)
                                Else
                                    Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                    '12003: Could not find account %1.Please enter the account in setup."
                                End If
                            End If
                        End If
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final Then
                                If oInvoice.MATCH_Matched = False Then
                                    If oPayment.PayCode = "901" Then
                                        oInvoice.MATCH_ID = sNoMatchAccountAP
                                        oInvoice.Dim1 = sNoMatchAccountAPDim1
                                        oInvoice.Dim2 = sNoMatchAccountAPDim2
                                        oInvoice.Dim3 = sNoMatchAccountAPDim3
                                        oInvoice.Dim4 = sNoMatchAccountAPDim4
                                        oInvoice.Dim5 = sNoMatchAccountAPDim5
                                        oInvoice.Dim6 = sNoMatchAccountAPDim6
                                    Else
                                        oInvoice.MATCH_ID = sNoMatchAccountAR
                                        oInvoice.Dim1 = sNoMatchAccountARDim1
                                        oInvoice.Dim2 = sNoMatchAccountARDim2
                                        oInvoice.Dim3 = sNoMatchAccountARDim3
                                        oInvoice.Dim4 = sNoMatchAccountARDim4
                                        oInvoice.Dim5 = sNoMatchAccountARDim5
                                        oInvoice.Dim6 = sNoMatchAccountARDim6
                                    End If
                                End If
                            End If
                        Next oInvoice
                    End If
                Next oPayment
            Next oBatch
        Next oBabel

    End Sub

    'XNET - 30.07.2011 - Added next function
    Function WriteComarch(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double
        Dim iPos As Integer
        Dim sExtension As String
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean ', sIAccountNo As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sRef1 As String, sRef2 As String
        Dim sBabelFile_ID As String
        Dim lBatchcounter As Long
        Dim lCounter As Long

        Try

            bAppendFile = False
            lBatchcounter = 0

            iPos = InStrRev(sFilenameOut, ".")
            sExtension = Mid(sFilenameOut, iPos + 1)
            'Use a temporary filename until the witing of the file is finished.
            'Rename it back to correct extension when the export is completed.
            oFile = oFs.OpenTextFile(Left(sFilenameOut, iPos) & "pending", Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If Not oPayment.Exported Then
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next

                    If bExportoBabel Then
                        'The file_ID or EDIMessageNo is a part of the filename. Make a check
                        If oBabel.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL Then
                            sBabelFile_ID = oBabel.EDI_MessageNo
                        Else
                            If Not EmptyString(oBabel.File_id) Then
                                sBabelFile_ID = Trim(oBabel.File_id)
                            Else
                                sBabelFile_ID = Format(Now(), "yyyyMMddHHmmss")
                            End If
                        End If
                        If InStr(1, sFilenameOut, sBabelFile_ID, vbTextCompare) = 0 Then
                            'The file_ID is not a part of the filename, don't export this babelfile
                            bExportoBabel = False
                        End If
                    End If

                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    nCounter = 0

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If Not oPayment.Exported Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            lBatchcounter = lBatchcounter + 1
                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If Not oPayment.Exported Then
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If oPayment.REF_Own = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    '                        If oPayment.I_Account <> sOldAccountNo Then
                                    '                            If oPayment.VB_ProfileInUse Then
                                    '                                bAccountFound = False
                                    '                                For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                    '                                    For Each oClient In oFilesetup.Clients
                                    '                                        For Each oaccount In oClient.Accounts
                                    '                                            If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                    '                                                sOldAccountNo = oPayment.I_Account
                                    '                                                bAccountFound = True
                                    '                                                Exit For
                                    '                                            End If
                                    '                                        Next oaccount
                                    '                                    If bAccountFound Then Exit For
                                    '                                    Next oClient
                                    '                                If bAccountFound Then Exit For
                                    '                                Next oFilesetup
                                    '                                If Not bAccountFound Then
                                    '                                    Err.Raise 12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account)
                                    '                                    '12003: Could not find account %1.Please enter the account in setup."
                                    '                                End If
                                    '                            End If
                                    '                        End If

                                    '12.09.2006 - new code to write the correct archiveref for bankreconciliation
                                    sArchiveRef = Trim$(oBatch.REF_Bank)

                                    If oBatch.Payments.Count = 1 And EmptyString(oPayment.REF_Bank1) Then
                                        sRef1 = oBatch.REF_Bank
                                    Else
                                        sRef1 = oPayment.REF_Bank1
                                    End If
                                    sRef2 = oPayment.REF_Bank2

                                    sLine = WriteComarch_Transaction(TransformDnBNORReference(sArchiveRef), TransformDnBNORReference(sRef1), TransformDnBNORReference(sRef2), oPayment, sBabelFile_ID, sFilenameOut)
                                    oFile.WriteLine(sLine)

                                End If

                            Next ' payment

                        End If
                    Next 'batch
                End If

            Next

            oFile.Close()
            oFile = Nothing

            'Use a temporary filename until the witing of the file is finished.
            'Rename it back to correct extension when the export is completed.
            oFs.MoveFile(Left(sFilenameOut, iPos) & "pending", sFilenameOut)

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteComarch" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteComarch = True

    End Function
    'XNET - 30.07.2012 - Whole function
    Public Function WriteComarch_Transaction(ByVal sArchiveRef As String, ByVal sRef1 As String, ByVal sRef2 As String, ByVal oPayment As vbBabel.Payment, ByVal sBabelFile_ID As String, ByVal sFilename As String) As String
        'The function assumes that there are only 1 KID on each payment (not more than 1 invoice).

        Dim sLineToWrite As String
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim sDateValue As String
        Dim sDatePayment As String
        Dim sAddress As String
        Dim sFreetext As String
        Dim sTmp As String
        Dim iFreetextCounter As Integer
        ' 1. BankFileID         String  M   File identifier, constant for all records in single file.
        ' 2. FileName           String  M   Name of payment file delivered from BabelBank, constant for all records in single file
        ' 3. BankPaymentID      Number  M   Payment record identifier, unique for all records in single file
        ' 4. PayMethod          Boolean M   True =OCR False=CREMUL
        ' 5. ValueDate          Date    M   Payment registration date on Telia's bank account in format: YYYY-MM-DD HH24:MI:SS Norwegian time
        ' 6. IssueDate          Date    M   Payment issue date(bookdate), will used as issue date for payment document creation in BS, format: YYYY-MM-DD HH24:MI:SS Norwegian time
        ' 7. KID                        M/O KID identifier, mandatory if <ocr> = true. KID number uniquely identifies payer and will be used to match payment with customer.
        ' 8. Amount             Number  M   Payment amount with 2 decimal places
        ' 9. Currency           String  O   Only NOK is handled. No exchange between currencies
        '10. BankRef            Number  O   Bank reference number - for information only
        '11. ArchiveRef         Number  O   Archive reference number - for information only
        '12. PayerName          String  O   Payer name
        '13. PayerZip           String  O   Payer's zip code
        '14. PayerAddress       String  O   Payer's address
        '15. PayerAccount       Number  O   Payer's bank account number
        '16. PayTitle           String (10000) O   Free text field for entering payment information
        '17. Batch Id           Number  O   Field storing payment batch identifier. Stored in BS for informational purposes and used in Adra export.
        '18. Transaction type        Number  M (for ocr) O(for CREMUL)    Field 3 in 'Transaction record OCR specification - Field storing information about payment transaction type.
        'Possible values for OCR:
        '10 - Transaction from giro debited account;
        '11 - Transaction from standing orders
        '12 - Transaction from direct debit remittance
        '13 - Transaction from BTG (Business Terminal Giro)
        '14 - Transaction from counter giro
        '15 - Transaction from Avtalegiro
        '16 - Transaction from telegiro
        '17 - Transaction from giro - paid in cash
        '18 - Reversing with KID
        '19 - Purchase with KID
        '20 - Reversing with free text
        '21 - Purchase with free text
        'Possible codes for CREMUL:
        '230 - OCR payment valid KID
        '231 - OCR payment invalid KID
        '232 - Direct Debit
        '233 - Electronic payment with message
        '234 - Manual payment
        '240 - Structured payment
        'Values from this field will be loaded to Comarch BS as payment details for informational purposes only. Field can be used by NGT to prepare some reports on customer/account level. Field will not be used and does not affect payment matching algorithm.

        sFreetext = vbNullString

        sDateValue = ""
        sDatePayment = ""
        If oPayment.DATE_Value = "19900101" Then
            If oPayment.DATE_Payment = "19900101" Then
                sDateValue = Format(Now(), "yyyyMMdd")
                sDatePayment = sDateValue
            Else
                sDatePayment = oPayment.DATE_Payment
                sDateValue = sDatePayment
            End If
        Else
            sDateValue = oPayment.DATE_Value
            If oPayment.DATE_Payment = "19900101" Then
                sDatePayment = sDateValue
            Else
                sDatePayment = oPayment.DATE_Payment
            End If
        End If
        sDateValue = Left(sDateValue, 4) & "-" & Mid(sDateValue, 5, 2) & "-" & Right(sDateValue, 2)
        sDatePayment = Left(sDatePayment, 4) & "-" & Mid(sDatePayment, 5, 2) & "-" & Right(sDatePayment, 2)

        sAddress = ""
        If Not EmptyString(oPayment.E_Adr1) Then
            sAddress = Trim(oPayment.E_Adr1) & " - "
        End If
        If Not EmptyString(oPayment.E_Adr2) Then
            sAddress = sAddress & Trim(oPayment.E_Adr2) & " - "
        End If
        If Not EmptyString(oPayment.E_Adr3) Then
            sAddress = sAddress & Trim(oPayment.E_Adr3) & " - "
        End If
        If Not EmptyString(oPayment.E_Zip) Then
            sAddress = sAddress & Trim(oPayment.E_Zip) & " - "
        End If
        If Not EmptyString(oPayment.E_City) Then
            sAddress = sAddress & Trim(oPayment.E_City) & " - "
        End If
        If Not EmptyString(sAddress) Then
            sAddress = Left(sAddress, Len(sAddress) - 3) 'Remove last " - ".
        End If

        If IsOCR(oPayment.PayCode) And oPayment.Invoices.Count > 1 Then
            Err.Raise("More than 1 KID is stated on this OCR-payment. This is not yet implemented in the solution." & vbCrLf _
                & "Payers name: " & oPayment.E_Name & vbCrLf _
                & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","))
        End If

        nCounter = nCounter + 1

        'For Each oInvoice In oPayment.Invoices
        '    If oInvoice.MON_InvoiceAmount <> 0 Then

        sLineToWrite = vbNullString

        sLineToWrite = sBabelFile_ID & ";" '1. BankFileID
        sLineToWrite = sLineToWrite & sFilename & ";" '2. FileName           String  M   Name of payment file delivered from BabelBank, constant for all records in single file
        sLineToWrite = sLineToWrite & Trim(Str(nCounter)) & ";" ' 3. BankPaymentID      Number  M   Payment record identifier, unique for all records in single file
        If IsOCR(oPayment.PayCode) Then
            sLineToWrite = sLineToWrite & True & ";" ' 4. PayMethod          Boolean M   True =OCR False=CREMUL
        Else
            sLineToWrite = sLineToWrite & False & ";"
        End If
        sLineToWrite = sLineToWrite & sDateValue & "12:00:00" & ";" ' 5. ValueDate          Date    M   Payment registration date on Telia's bank account in format: YYYY-MM-DD HH24:MI:SS Norwegian time
        sLineToWrite = sLineToWrite & sDatePayment & "12:00:00" & ";" ' 6. IssueDate          Date    M   Payment issue date(bookdate), will used as issue date for payment document creation in BS, format: YYYY-MM-DD HH24:MI:SS Norwegian time
        If IsOCR(oPayment.PayCode) Then
            sLineToWrite = sLineToWrite & oPayment.Invoices.Item(1).Unique_Id & ";" ' 7. KID                        M/O KID identifier, mandatory if <ocr> = true. KID number uniquely identifies payer and will be used to match payment with customer.
        Else
            sLineToWrite = sLineToWrite & ";"
        End If
        sLineToWrite = sLineToWrite & Trim(Str(oPayment.MON_InvoiceAmount)) & ";" ' 8. Amount             Number  M   Payment amount with 2 decimal places
        If EmptyString(oPayment.MON_InvoiceCurrency) Then
            sLineToWrite = sLineToWrite & "NOK" & ";" ' 9. Currency           String  O   Only NOK is handled. No exchange between currencies
        Else
            If oPayment.MON_InvoiceCurrency <> "NOK" Then
                Err.Raise("Illegal currency, " & oPayment.MON_InvoiceCurrency & ", stated on the payment." & vbCrLf _
                    & "Payers name: " & oPayment.E_Name & vbCrLf _
                    & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","))
            Else
                sLineToWrite = sLineToWrite & "NOK" & ";"
            End If
        End If
        If Not EmptyString(sRef1) Then
            sLineToWrite = sLineToWrite & sRef1 & ";" '10. BankRef            Number  O   Bank reference number - for information only
        Else
            sLineToWrite = sLineToWrite & sRef2 & ";"
        End If
        sLineToWrite = sLineToWrite & sArchiveRef & ";" '11. ArchiveRef         Number  O   Archive reference number - for information only
        sLineToWrite = sLineToWrite & Trim(oPayment.E_Name) & ";" '12. PayerName          String  O   Payer name
        sLineToWrite = sLineToWrite & oPayment.E_Zip & ";" '13. PayerZip           String  O   Payer's zip code
        sLineToWrite = sLineToWrite & sAddress & ";" '14. PayerAddress       String  O   Payer's address
        If Mid(oPayment.E_Account, 5, 1) = "9" Then
            sLineToWrite = sLineToWrite & ";"
        Else
            sLineToWrite = sLineToWrite & Trim(oPayment.E_Account) & ";" '15. PayerAccount       Number  O   Payer's bank account number
        End If
        iFreetextCounter = 0
        For Each oInvoice In oPayment.Invoices
            For Each oFreeText In oInvoice.Freetexts
                If oFreeText.Qualifier < 3 Then
                    iFreetextCounter = iFreetextCounter + 1
                    If iFreetextCounter = 1 Then
                        sFreetext = RTrim$(oFreeText.Text)
                    ElseIf iFreetextCounter = 2 Then
                        sFreetext = sFreetext & " " & RTrim$(oFreeText.Text)
                    Else
                        Exit For
                    End If
                End If
            Next oFreeText
        Next oInvoice
        sFreetext = Replace(sFreetext, ";", ",")
        sLineToWrite = sLineToWrite & sFreetext & ";" '16. PayTitle           String (10000) O   Free text field for entering payment information
        sLineToWrite = sLineToWrite & sArchiveRef & oPayment.DATE_Payment & ";" '17. Batch Id           Number  O   Field storing payment batch identifier. Stored in BS for informational purposes and used in Adra export.
        If IsOCR(oPayment.PayCode) Then
            sLineToWrite = sLineToWrite & bbGetPayCode(oPayment.PayCode, "OCR") & ";" '18. Transaction type        Number  M (for ocr) O(for CREMUL)    Field 3 in 'Transaction record OCR specification - Field storing information about payment transaction type.
        Else
            sTmp = bbGetPayCode(oPayment.PayCode, "CREMUL")
            If Not IsNumeric(sTmp) Then
                sTmp = "999"
            End If
            sLineToWrite = sLineToWrite & sTmp & ";"
        End If

        '    End If
        'Next oInvoice

        oPayment.Exported = True

        WriteComarch_Transaction = sLineToWrite

    End Function
    Public Function WriteGenericXML(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String, ByRef iVB_Format_ID As Integer, ByRef sSpecial As String) As Boolean
        ' Export a generic XML-file
        ' The import must use GenericXML, and then the XMLDocument is stored i oBabelFile

        Dim XMLDoc As System.Xml.XmlDocument
        Dim oBabelFile As vbBabel.BabelFile
        Dim bDecryptedFileAlreadyExported As Boolean = False
        Dim sStringToWrite As String = ""
        Dim lPos1 As Long
        Dim lPos2 As Long
        Dim iLength As Integer
        Dim sStringToReplace As String = ""
        Dim sNewCompanyNo As String = ""
        Dim oFileSetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        '21.02.2022 - Added the use of sSpecial = "CHANGE_XMLID"

        Try

            For Each oBabelFile In oBabelFiles
                XMLDoc = oBabelFile.GenericXML

                sFILE_Name = oBabelFile.EDI_FilenameCONTRLMessage '21.01.2021 - Now retrieving the exportfilename from her

                If sSpecial = "CHANGE_XMLID" Then
                    For Each oFileSetup In oBabelFile.VB_Profile.FileSetups
                        If oFileSetup.FileSetup_ID = iVB_Format_ID Then
                            For Each oClient In oFileSetup.Clients
                                sNewCompanyNo = oClient.CompNo
                            Next oClient
                        End If
                    Next oFileSetup
                End If
                ' do any specialtreatment here

                '    ' 11.01.2020 - we can use this one to "unpack" returnfiles with SecureEnvelope;
                ' if bDecryptedFileAlreadyExported returns with True, then ExportDecryptedFile has exported the file decrypted

                '21.01.2021 - Changed this to be able to export more than one file
                'Old code
                'ExportDecryptedFile(oBabelFiles(1).FilenameIn, sFILE_Name, bDecryptedFileAlreadyExported)
                'New code
                ExportDecryptedFile(oBabelFile.FilenameIn, sFILE_Name, bDecryptedFileAlreadyExported)


                If Not bDecryptedFileAlreadyExported Then
                    ' export file
                    sStringToWrite = XMLDoc.OuterXml
                    If sSpecial = "CHANGE_XMLID" And Not EmptyString(sNewCompanyNo) Then
                        lPos1 = sStringToWrite.IndexOf("<OrgId><Othr><Id>") + 17
                        lPos2 = sStringToWrite.IndexOf("</Id>", CInt(lPos1))
                        sStringToReplace = sStringToWrite.Substring(lPos1, lPos2 - lPos1)
                        sStringToWrite = sStringToWrite.Replace(sStringToReplace, sNewCompanyNo)
                    End If
                    Using sw As New System.IO.StreamWriter(sFILE_Name) ', False, System.Text.Encoding.UTF8)
                        ' save callstack to the fil
                        'sw.Write(XMLDoc.OuterXml) ' 21.02.2022 - Old code
                        sw.Write(sStringToWrite)
                        sw.Close()
                    End Using
                End If

                XMLDoc = Nothing

            Next oBabelFile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGenericXML" & vbCrLf & ex.Message, ex)

        Finally

            XMLDoc = Nothing

        End Try
        WriteGenericXML = True

    End Function
    Public Function WriteGenericText(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String, ByRef iVB_Format_ID As Integer, ByRef sSpecial As String, ByRef bFILE_OverWrite As Boolean, ByRef bLog As Boolean, ByRef oBabelLog As vbLog.vbLogging) As Boolean        ' Export a generic Text-file
        ' The import must use GenericText, and then the textfilecontent is stored i oBabelFile

        Dim sFilecontent As String = ""
        Dim sChangedFileContent As String = ""
        Dim oBabelFile As vbBabel.BabelFile

        Try
            For Each oBabelFile In oBabelFiles
                sFilecontent = oBabelFile.GenericText

                sFILE_Name = oBabelFile.EDI_FilenameCONTRLMessage '21.01.2021 - Now retrieving the exportfilename from her

                ' do any specialtreatment here

                ' 21.03.2013 - added option to change characters that may cause problems for receiving bank
                If InStr(sSpecial, "CHECKCHARACTERS") > 0 Then
                    ' her m� vi finne en bedre l�sning enn den under
                    sFilecontent = CheckForValidCharacters(sFilecontent, True, True, True, True, ":+,? '")
                End If

                ' 21.03.2013 - added option to create "cut80"-files
                If InStr(sSpecial, "CUT80") > 0 Then
                    ' remove existing CR and LF
                    sFilecontent = Replace(Replace(sFilecontent, vbCr, ""), vbLf, "")
                    ' then cut to 80 pr line
                    Do
                        If Len(sFilecontent) < 80 Then
                            sChangedFileContent = sChangedFileContent & vbCrLf & sFilecontent
                            Exit Do
                        Else
                            sChangedFileContent = sChangedFileContent & vbCrLf & Left(sFilecontent, 80)
                        End If
                        sFilecontent = Mid(sFilecontent, 81)
                    Loop
                    sFilecontent = Mid(sChangedFileContent, 3)  ' take away first CR/LF
                End If 'If sSpecial = "CUT80" Then

                ' export file
                ' Changed to "ISO-8859-1"
                Using sw As New System.IO.StreamWriter(sFILE_Name, Not bFILE_OverWrite, System.Text.Encoding.GetEncoding("ISO-8859-1")) ', False, System.Text.Encoding.UTF8)

                    ' save callstack to the file
                    sw.Write(sFilecontent)
                    sw.Close()
                End Using

            Next oBabelFile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGenericText" & vbCrLf & ex.Message, ex)

        Finally

        End Try

        WriteGenericText = True

    End Function
    Function WriteBiliaIFS(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' 09.09.2013 - For Bilia: Exports a semiclon separated file for IFS Hovedbok, based on Telepay Returnfiles as input

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean

        Try

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            ' Write a "bankline" for each batch;
                            ' NO, write a bankline pr payment! - moved down !

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    ' Bankinfo pr payment
                                    ' Only 4 columns with "real" info;
                                    '  3 Account            1104 (Bank)
                                    ' 19 Amount             Creditamount   
                                    ' 24 Text               Bankref, from batch.ref_bank
                                    ' 31 TransactionDate    Paymentdate
                                    sLine = ";;"
                                    sLine = sLine & "1104;"                                                 '  3 Account            1104 Bank
                                    sLine = sLine & ";;;;;;;;;;;;;;;"
                                    sLine = sLine & Trim(Str(oPayment.MON_InvoiceAmount * -1 / 100)) & ";"    ' 19 Amount               Creditamount   
                                    sLine = sLine & ";;;;"
                                    sLine = sLine & Trim(oPayment.REF_Bank1) & ";"                                  ' 24 Text               Invoiceno from Ref_Own 
                                    sLine = sLine & ";;;;;;"
                                    sLine = sLine & Left(oPayment.DATE_Payment, 4) & "-" & Mid(oPayment.DATE_Payment, 5, 2) & "-" & Right(oPayment.DATE_Payment, 2)   ' 31 TransactionDate    Paymentdate, 2013-09-30

                                    If Len(sLine) > 0 Then
                                        oFile.WriteLine((sLine))
                                    End If

                                    '-------- Write content of each payment to file---------
                                    ' Only 4 columns with "real" info;
                                    '  3 Account            1072 (Nordea Finans)
                                    ' 19 Amount             Debitamount   
                                    ' 24 Text               Invoiceno from Ref_Own
                                    ' 31 TransactionDate    Paymentdate


                                    For Each oInvoice In oPayment.Invoices
                                        ' For each real payment to Nordea Finans;
                                        sLine = ";;"
                                        sLine = sLine & "1072;"                                                 '  3 Account            1072 (Nordea)
                                        sLine = sLine & ";;;;;;;;;;;;;;;"
                                        sLine = sLine & Trim(Str(oInvoice.MON_InvoiceAmount / 100)) & ";"    ' 19 Amount             Debit or Creditamount   
                                        sLine = sLine & ";;;;"
                                        sLine = sLine & Trim(oInvoice.REF_Own) & ";"                                  ' 24 Text               Invoiceno from Ref_Own 
                                        sLine = sLine & ";;;;;;"
                                        sLine = sLine & Left(oPayment.DATE_Payment, 4) & "-" & Mid(oPayment.DATE_Payment, 5, 2) & "-" & Right(oPayment.DATE_Payment, 2)  ' 31 TransactionDate    Paymentdate, 2013-09-30

                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine((sLine))
                                        End If


                                    Next oInvoice
                                End If
                                oPayment.Exported = True
                            Next oPayment
                        End If 'If bExportoBatch Then
                    Next oBatch 'batch
                End If  'If bExportoBabel Then
            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBiliaIFS" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBiliaIFS = True

    End Function
    'Only in .NET
    'Function WriteInfotjenester_Navision(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sSpecial As String, ByRef bFILE_OverWrite As Boolean, ByRef iFilenameInNo As Short, ByRef sClientNo As String) As Boolean
    Function WriteInfotjenester_Navision(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sClientNo As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim sFreetext As String = ""
        Dim oClient As Client
        Dim oAccount As Account
        Dim sOldAccount As String = ""
        Dim bFoundClient As Boolean
        Dim sGLAccount As String
        Dim sLine As String
        Dim bExportoBatch As Boolean
        Dim bExportoBabel As Boolean
        Dim bExportoPayment As Boolean
        Dim sText As String
        Dim nDummy As Double

        Try

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False

                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                If oBabel.BabelfileContainsPaymentsAccordingToQualifiers(iFilenameInNo, iFormat_ID, bMultiFiles, sClientNo, True) Then
                    bExportoBabel = True
                End If

                If bExportoBabel Then

                    For Each oBatch In oBabel.Batches
                        'Loop through all Batch objs. in BabelFile obj.

                        If oBatch.Payments.Count > 0 Then

                            bExportoBatch = False
                            If oBatch.BatchContainsPaymentsAccordingToQualifiers(iFormat_ID, bMultiFiles, sClientNo, nDummy, True) Then
                                bExportoBatch = True
                            End If

                            If bExportoBatch Then
                                If oBatch.Payments(1).I_Account <> sOldAccount Then
                                    sOldAccount = oBatch.Payments(1).I_Account
                                    For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                        For Each oAccount In oClient.Accounts
                                            If oAccount.Account = oBatch.Payments(1).I_Account Then
                                                sGLAccount = Trim(oAccount.GLAccount)
                                                bFoundClient = True
                                                Exit For
                                            End If
                                        Next oAccount
                                        If bFoundClient Then
                                            Exit For
                                        End If
                                    Next oClient
                                End If

                                ' Write a "bankline" for each batch;
                                sLine = ""
                                sLine = "B;"
                                sLine = sLine & sGLAccount & ";"
                                sLine = sLine & ConvertFromAmountToString(oBatch.MON_InvoiceAmount, "", ",") & ";"
                                sLine = sLine & oBatch.Payments(1).MON_InvoiceCurrency & ";"
                                sLine = sLine & oBatch.Payments(1).DATE_Payment & ";"
                                sLine = sLine & Replace(oBatch.REF_Bank, ";", "")
                                oFile.WriteLine(sLine)

                                For Each oPayment In oBatch.Payments

                                    bExportoPayment = False
                                    If oPayment.PaymentIsAccordingToQualifiers(iFormat_ID, bMultiFiles, sClientNo, True) Then
                                        bExportoPayment = True
                                    End If

                                    If bExportoPayment Then
                                        For Each oInvoice In oPayment.Invoices
                                            sLine = ""
                                            sLine = "T;"
                                            sLine = sLine & Trim(oPayment.I_Account) & ";"
                                            sLine = sLine & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, "", ",") & ";"
                                            sLine = sLine & oPayment.MON_InvoiceCurrency & ";"
                                            sLine = sLine & oPayment.DATE_Payment & ";"
                                            If Not EmptyString(oInvoice.Unique_Id) Then
                                                sLine = sLine & Trim(oInvoice.Unique_Id) & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            If Not EmptyString(oInvoice.InvoiceNo) Then
                                                sLine = sLine & Trim(oInvoice.InvoiceNo) & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            sText = ""
                                            sText = Replace(oPayment.E_Name, ";", ",")
                                            If Not EmptyString(sText) Then
                                                sText = sText & " - "
                                            End If
                                            sFreetext = ""
                                            For Each oFreetext In oInvoice.Freetexts
                                                sFreetext = sFreetext & Trim(oFreetext.Text) & " "
                                            Next
                                            sText = sText & sFreetext
                                            If Not EmptyString(oPayment.E_Adr1) Then
                                                sText = sText & " - " & Trim(oPayment.E_Adr1)
                                            End If
                                            If Not EmptyString(oPayment.E_Adr2) Then
                                                sText = sText & " - " & Trim(oPayment.E_Adr2)
                                            End If
                                            If Not EmptyString(oPayment.E_Zip) Then
                                                sText = sText & " - " & Trim(oPayment.E_Zip)
                                            End If
                                            If Not EmptyString(oPayment.E_City) Then
                                                sText = sText & " - " & Trim(oPayment.E_City)
                                            End If
                                            sText = Replace(sText, ";", ",")
                                            sLine = sLine & sText

                                            oFile.WriteLine(sLine)

                                        Next oInvoice
                                    End If
                                    oPayment.Exported = True
                                Next oPayment
                            End If 'If bExportoBatch Then

                        End If 'If oBatch.Payments.Count > 0 Then
                    Next oBatch 'batch
                End If  'If bExportoBabel Then
            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteInfotjenester_Navision" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteInfotjenester_Navision = True

    End Function
    Function WritePatentstyret_Order(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal bPostAgainstObservationAccount As Boolean) As Boolean
        'KJELL - skal disse hardcodes ???
        Const constPatentstyretRoundings As String = "7772"
        Const constPatentstyretFee As String = "7771"
        Const constPatentstyretAcceptedDeviance As String = "7790"
        Const constPatentstyretReImbursement As String = "8143"

        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oDummyPayment As vbBabel.Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim dDate As Date
        Dim sName As String
        Dim sAdr1 As String, sAdr2 As String, sZip As String
        Dim nAmount As Double
        Dim sArchiveRef As String
        Dim sText As String
        Dim sId As String
        Dim sFreetext As String
        Dim sFixedErrorText As String = ""
        Dim sRef1 As String, sRef2 As String
        Dim sPayCode As String
        Dim sBirthNumber As String
        Dim sGLAccount As String
        Dim sGLResultsAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sNoMatchAccount As String
        Dim iNoMatchAccountType As String
        Dim sCompanyCode As String
        Dim sVoucherType As String
        Dim iFreetextCounter As Integer
        Dim lCounter As Long
        Dim sNoMatchAccountAR As String
        Dim sAccountCurrency As String
        Dim sTransactionDate As String
        Dim lArrayCounter As Long
        Dim lArrayCounter2 As Long
        Dim iUpdayTotalsArrayCounter As Integer
        Dim aAccountArray(,) As String
        Dim aARAmountArray() As Double
        Dim aGLAmountArray() As Double
        Dim sSpecial As String

        Dim aOrderArray As String(,)
        Dim aPayableArray As String(,)
        Dim bFirstItem As Boolean
        Dim bOrdersExist As Boolean
        Dim bAddNewItem As Boolean
        Dim bBatchWritten As Boolean
        Dim bFound As Boolean

        Dim apiNS As String
        Dim xsiNS As String
        Dim agrlibNS As String
        Dim xsi As String
        Dim defaultNS As String
        '09.09.2015 - Added code using new variable sPayeeAddress
        Dim sPayeeAddress As String = ""

        '28.08.2015 - New variables to update articles in Agresso
        Dim aArticles As Object(,)
        Dim oERPDal As vbBabel.DAL = Nothing
        Dim aQueryArray(,,) As String
        Dim sRetrievAccountingStringSQL As String
        Dim sMySQL As String
        Dim sArticleAccount As String
        Dim sArticleDim1 As String
        Dim sArticleDim4 As String

        'Dim oOrderUpdate As BB_Special.PatentstyretUpdate 'OVERGANG TIL VS2017 - Fjern kommentar p� denne linjen
        Dim bContinue As Boolean = True
        Dim sErrorText As String

        '13.05.2016 - Added variable
        Dim nCardFee As Double
        '- legg inn bruk av bContinue i hele funksjonen

        Try

            sOldAccountNo = vbNullString
            sGLAccount = vbNullString
            bBatchWritten = False
            bAccountFound = False
            sAccountCurrency = ""
            sTransactionDate = Format(Now(), "dd.MM.yyyy")
            iNoMatchAccountType = -1

            sSpecial = UCase(oBabelFiles(1).Special)

            aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

            'Create an array equal to the client-array
            ReDim Preserve aARAmountArray(UBound(aAccountArray, 2))
            ReDim Preserve aGLAmountArray(UBound(aAccountArray, 2))

            '28.08.2015 - Connect to the ERP (used to retrieve correct accounting string for articles
            aQueryArray = GetERPDBInfo(1, 5)

            For lCounter = 0 To UBound(aQueryArray, 3)
                Select Case UCase(aQueryArray(0, 4, lCounter))

                    Case "RETRIEVE_ARTICLE"
                        sRetrievAccountingStringSQL = Trim$(aQueryArray(0, 0, lCounter))
                End Select
            Next lCounter

            If Not EmptyString(sRetrievAccountingStringSQL) Then

                ' connect to erp system
                oERPDal = New vbBabel.DAL
                oERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                oERPDal.CreateArrayForReturnvalues(aQueryArray)
                oERPDal.Company_ID = 1 'TODO - Hardcoded Company_ID
                oERPDal.DBProfile_ID = 1 'TODO - Hardcoded DBProfile_ID
                If Not oERPDal.ConnectToDB() Then
                    Throw New System.Exception(oERPDal.ErrorMessage)
                End If
            Else
                Throw New System.Exception("Det m� angis en SQL for � hente ut korrekt konteringsstreng for artikler.")
            End If

            If oBabelFiles.Count > 0 Then

                For Each oBabel In oBabelFiles
                    If oBabel.Special = "PATENTSTYRET_FASE2" Then
                        If Patentstyret_ValidateDoublePayments(oBabel.VB_Profile, bPostAgainstObservationAccount, oBabelFiles) Then
                            'OK, report empty
                        Else
                            If MsgBox("Vil du fortsette eksporten av data?", MsgBoxStyle.YesNo, "BabelBank eksport") = MsgBoxResult.Yes Then
                                bContinue = True
                            Else
                                Err.Raise(8264, "WritePatentstyret_Order", "BabelBank eksport avbrutt av bruker.")
                            End If
                        End If
                    End If
                    Exit For
                Next oBabel

                sErrorText = "Patentstyret_SetNamespaces"
                If Patentstyret_SetNamespaces(apiNS, agrlibNS, xsiNS, xsi, defaultNS) Then

                Else
                    bContinue = False
                End If

                Dim settings As System.Xml.XmlWriterSettings = New System.Xml.XmlWriterSettings()
                settings.Indent = True

                If bContinue Then
                    'Using writer As System.Xml.XmlWriter = rootNode.CreateNavigator.AppendChild   'XmlWriter.Create("C:\xmlfile.xml", settings)
                    Using writer As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(sFilenameOut, settings)

                        sErrorText = "WriteAgresso_Patentstyret_StartDocument"
                        If Not WriteAgresso_Patentstyret_StartDocument(writer, defaultNS, agrlibNS, xsiNS, xsi) Then

                        End If

                        Dim prefix As String = writer.LookupPrefix(agrlibNS)

                        'OVERGANG TIL VS2017 - neste 2 linjer
                        'System.Net.ServicePointManager.Expect100Continue = True
                        'System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12

                        For Each oBabel In oBabelFiles

                            bExportoBabel = False
                            sErrorText = "Sjekker hvilke poster som skal eksporteres."
                            'Have to go through each batch-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oBatch In oBabel.Batches
                                For Each oPayment In oBatch.Payments
                                    If Not oPayment.Exported Then
                                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                            If Not bMultiFiles Then
                                                bExportoBabel = True
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If oPayment.REF_Own = sOwnRef Then
                                                        bExportoBabel = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                    If bExportoBabel Then
                                        Exit For
                                    End If
                                Next
                                If bExportoBabel Then
                                    Exit For
                                End If
                            Next

                            If bExportoBabel Then

                                'Loop through all Batch objs. in BabelFile obj.
                                For Each oBatch In oBabel.Batches
                                    bExportoBatch = False
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    For Each oPayment In oBatch.Payments
                                        If Not oPayment.Exported Then
                                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                                If Not bMultiFiles Then
                                                    bExportoBatch = True
                                                Else
                                                    If oPayment.I_Account = sI_Account Then
                                                        If oPayment.REF_Own = sOwnRef Then
                                                            bExportoBatch = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                        'If true exit the loop, and we have data to export
                                        If bExportoBatch Then
                                            Exit For
                                        End If
                                    Next

                                    If bExportoBatch Then
                                        aArticles = Nothing
                                        If oPayment.VB_ProfileInUse Then
                                            sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        ' 02.01.2015 added Or sI_Account = oaccount.ConvertedAccount
                                                        ' because we also needs NHST Asias real bankaccounts in clientsetup for NHST Asia
                                                        ' then we must cheat by adding NHST Asia accounts for the other clients, and set the account as ConvertedAccount
                                                        If sI_Account = oaccount.Account Or sI_Account = oaccount.ConvertedAccount Then
                                                            sGLAccount = Trim(oaccount.GLAccount.Trim)
                                                            sGLResultsAccount = oaccount.GLResultsAccount.Trim
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True

                                                            sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                            iNoMatchAccountType = GetNoMatchAccountType(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                            sCompanyCode = oClient.ClientNo
                                                            sVoucherType = oClient.VoucherType2

                                                            '19.11.2015 - Find the correct element in the accountarray
                                                            iUpdayTotalsArrayCounter = 0
                                                            For iUpdayTotalsArrayCounter = 0 To aAccountArray.GetUpperBound(1)
                                                                If aAccountArray(1, iUpdayTotalsArrayCounter) = sI_Account Then
                                                                    Exit For
                                                                End If
                                                            Next iUpdayTotalsArrayCounter

                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If
                                        'End find the client

                                        'If Not bPostAgainstObservationAccount Then
                                        If Not bBatchWritten Then
                                            sErrorText = "WriteAgresso_Patentstyret_BatchID"
                                            If Not WriteAgresso_Patentstyret_BatchID(writer, agrlibNS, prefix, oBabel) Then

                                            Else
                                                bBatchWritten = True
                                            End If
                                            'Write GL Bank-record
                                        End If
                                        'End If

                                        If bContinue Then
                                            sErrorText = "WriteAgresso_Patentstyret_Voucher"
                                            If Not WriteAgresso_Patentstyret_Voucher(writer, defaultNS, agrlibNS, prefix, oPayment, sVoucherType, sCompanyCode) Then
                                            Else
                                                'bContinue = False
                                            End If
                                        End If

                                        If bContinue Then
                                            If Not bPostAgainstObservationAccount Then
                                                sErrorText = "WriteAgresso_Patentstyret_Transaction"
                                                If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oBatch.Payments.Item(1).MON_InvoiceCurrency, xDelim(sGLAccount, "-", 1), xDelim(sGLAccount, "-", 2), xDelim(sGLAccount, "-", 3), xDelim(sGLAccount, "-", 4), xDelim(sGLAccount, "-", 5), "", False, "CREMUL", oBatch.REF_Bank, ConvertFromAmountToString(oBatch.MON_InvoiceAmount, "", ".")) Then
                                                    bContinue = False
                                                Else
                                                    bContinue = True
                                                End If
                                                writer.WriteEndElement() 'Transaction
                                            End If
                                        End If

                                        If bContinue Then '********
                                            For Each oPayment In oBatch.Payments

                                                bExportoPayment = False
                                                'Have to go through the payment-object to see if we have objects thatt shall
                                                ' be exported to this exportfile
                                                If Not oPayment.Exported Then
                                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                                        If Not bMultiFiles Then
                                                            bExportoPayment = True
                                                        Else
                                                            If oPayment.I_Account = sI_Account Then
                                                                If oPayment.REF_Own = sOwnRef Then
                                                                    bExportoPayment = True
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                                If bExportoPayment Then

                                                    sFixedErrorText = CreateStandardErrorText(oPayment)

                                                    sPayeeAddress = ""
                                                    nCardFee = 0 'Added 13.05.2016
                                                    '19.11.2015 - Added CRlF between fields
                                                    If Not EmptyString(oPayment.E_Adr1) Then
                                                        sPayeeAddress = oPayment.E_Adr1.Trim
                                                    End If
                                                    If Not EmptyString(oPayment.E_Adr2) Then
                                                        If EmptyString(sPayeeAddress) Then
                                                            sPayeeAddress = oPayment.E_Adr2.Trim
                                                        Else
                                                            sPayeeAddress = sPayeeAddress & vbCrLf & oPayment.E_Adr2.Trim
                                                        End If
                                                    End If
                                                    If Not EmptyString(oPayment.E_Adr3) Then
                                                        If EmptyString(sPayeeAddress) Then
                                                            sPayeeAddress = oPayment.E_Adr3.Trim
                                                        Else
                                                            sPayeeAddress = sPayeeAddress & vbCrLf & oPayment.E_Adr3.Trim
                                                        End If
                                                    End If
                                                    If Not EmptyString(oPayment.E_Zip) Then
                                                        If EmptyString(sPayeeAddress) Then
                                                            sPayeeAddress = oPayment.E_Zip.Trim
                                                        Else
                                                            sPayeeAddress = sPayeeAddress & vbCrLf & oPayment.E_Zip.Trim
                                                        End If
                                                    End If
                                                    If Not EmptyString(oPayment.E_City) Then
                                                        If EmptyString(sPayeeAddress) Then
                                                            sPayeeAddress = oPayment.E_City.Trim
                                                        Else
                                                            sPayeeAddress = sPayeeAddress & vbCrLf & oPayment.E_City.Trim
                                                        End If
                                                    End If

                                                    'Old code
                                                    'sPayeeAddress = Trim(Trim(oPayment.E_Adr1) & " " & Trim(oPayment.E_Adr2))
                                                    'sPayeeAddress = Trim(sPayeeAddress & " " & Trim(oPayment.E_Adr3))
                                                    'sPayeeAddress = Trim(sPayeeAddress & " " & Trim(oPayment.E_Zip))
                                                    'sPayeeAddress = Trim(sPayeeAddress & " " & Trim(oPayment.E_City))
                                                    If bPostAgainstObservationAccount Then
                                                        sErrorText = "WriteAgresso_Patentstyret_Transaction (Observasjon)"
                                                        If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oPayment.MON_InvoiceCurrency, xDelim(sNoMatchAccount, "-", 1), xDelim(sNoMatchAccount, "-", 2), xDelim(sNoMatchAccount, "-", 3), xDelim(sNoMatchAccount, "-", 4), xDelim(sNoMatchAccount, "-", 5), "", False, oPayment.Unique_PaymentID.ToString & " - " & Trim$(oPayment.E_Name), "CREMUL", ConvertFromAmountToString(oPayment.MON_InvoiceAmount, "", ".")) Then

                                                        End If
                                                        writer.WriteEndElement() 'Transaction
                                                    End If

                                                    'If oPayment.I_Account <> sOldAccountNo Then
                                                    '    If oPayment.VB_ProfileInUse Then
                                                    '        bAccountFound = False
                                                    '        For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                    '            For Each oClient In oFilesetup.Clients
                                                    '                For Each oaccount In oClient.Accounts
                                                    '                    If Trim$(oPayment.I_Account) = Trim$(oaccount.Account) Then
                                                    '                        sGLAccount = Trim$(oaccount.GLAccount)
                                                    '                        sOldAccountNo = oPayment.I_Account
                                                    '                        sAccountCurrency = oaccount.CurrencyCode
                                                    '                        bAccountFound = True
                                                    '                        Exit For
                                                    '                    End If
                                                    '                Next oaccount
                                                    '                If bAccountFound Then Exit For
                                                    '            Next oClient
                                                    '            If bAccountFound Then Exit For
                                                    '        Next oFilesetup
                                                    '        If bAccountFound Then
                                                    '            'Find the correct account in the array
                                                    '            For lCounter = 0 To UBound(aAccountArray, 2)
                                                    '                If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                    '                    lArrayCounter = lCounter
                                                    '                    Exit For
                                                    '                End If
                                                    '            Next lCounter
                                                    '        End If

                                                    '        If oBabel.VB_ProfileInUse Then
                                                    '            If bAccountFound Then
                                                    '                sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                    '                sNoMatchAccountType = GetNoMatchAccountType(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                    '                sNoMatchAccountAR = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                    '            Else
                                                    '                Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                                    '                '12003: Could not find account %1.Please enter the account in setup."
                                                    '            End If
                                                    '        End If
                                                    '    End If
                                                    'End If

                                                    '-------- fetch content of each payment ---------
                                                    sName = oPayment.E_Name
                                                    sAdr1 = vbNullString   'vbNullString
                                                    sAdr2 = vbNullString   'vbNullString
                                                    ' If Adr2="-", empty Adr2;
                                                    If Trim$(oPayment.E_Adr2) = "-" Then
                                                        oPayment.E_Adr2 = vbNullString
                                                    End If

                                                    If Len(Trim$(oPayment.E_Adr2)) = 0 Then
                                                        ' empty adr2, put opayment.e_adr1 into eksportfiles adr2
                                                        sAdr2 = oPayment.E_Adr1  'Special for LHL, adr2 is the mainadresse
                                                    Else
                                                        ' both addresslines in use
                                                        sAdr1 = oPayment.E_Adr1
                                                        sAdr2 = oPayment.E_Adr2
                                                    End If

                                                    sZip = oPayment.E_Zip
                                                    nAmount = oPayment.MON_TransferredAmount

                                                    dDate = StringToDate(oPayment.DATE_Payment)
                                                    '12.09.2006 - new code to write the correct archiveref for bankreconciliation
                                                    sArchiveRef = Trim$(oBatch.REF_Bank)
                                                    For lCounter = 1 To Len(sArchiveRef)
                                                        If Left$(sArchiveRef, 1) = "*" Then
                                                            sArchiveRef = Mid$(sArchiveRef, 2)
                                                        Else
                                                            Exit For
                                                        End If
                                                    Next lCounter
                                                    If oBatch.Payments.Count = 1 And Not EmptyString(oPayment.REF_Bank1) Then
                                                        sRef1 = oBatch.REF_Bank
                                                    Else
                                                        sRef1 = oPayment.REF_Bank1
                                                    End If
                                                    sRef2 = oPayment.REF_Bank2

                                                    sFreetext = vbNullString

                                                    iFreetextCounter = 0
                                                    sFreetext = vbNullString
                                                    For Each oInvoice In oPayment.Invoices
                                                        If oInvoice.MATCH_Original = True Then
                                                            For Each oFreetext In oInvoice.Freetexts
                                                                If oFreetext.Qualifier < 3 Then
                                                                    iFreetextCounter = iFreetextCounter + 1
                                                                    If iFreetextCounter = 1 Then
                                                                        sFreetext = RTrim$(oFreetext.Text)
                                                                    ElseIf iFreetextCounter = 2 Then
                                                                        sFreetext = sFreetext & " " & RTrim$(oFreetext.Text)
                                                                    Else
                                                                        Exit For
                                                                    End If
                                                                End If
                                                            Next oFreetext
                                                        End If
                                                    Next oInvoice
                                                    sFreetext = sFreetext & " - " & Trim$(oPayment.E_Name)
                                                    '10.09.2015 - Added UniqueID
                                                    'sFreetext = oPayment.Unique_PaymentID.ToString & " - " & sFreetext

                                                    'Create Arrays for postinginformation
                                                    sErrorText = "Lager arrayer med posteringsinformasjon"
                                                    'Reset the arrays
                                                    ReDim aOrderArray(13, 0)
                                                    ReDim aPayableArray(3, 0)
                                                    '0 - OrderID
                                                    '1 - Payee
                                                    '2 - TotalPaid
                                                    '3 - PaidDate
                                                    '4 - TransId
                                                    '5 - PayeeBankAccount
                                                    '6 - TotalFee
                                                    '7 - Rounding
                                                    '8 - AcceptedDeviance
                                                    '9 - ReImbursement
                                                    '10 - Type Of Update - MyField2
                                                    '11 - Payee Address
                                                    '12 - SantCustomerID
                                                    '13 - Shows if a Order already is updated in an earlier report True = Skal oppdateres, False = Sakl IKKE oppdateres
                                                    '    ORDER - Update an order
                                                    '    PAYABLE - Create a new order using the payables listed in aPayableArray
                                                    '    ORDERPAYABLE - Partpayment of an order. Update the order with th selected payables listed in aPayableArray
                                                    ' - TransIdDate? Dagens dato?
                                                    '0 - OrdreId
                                                    '1 - PayableId for PAYABLE-update, OrderPayableId for ORDERPAYABLEUPDATE (Retrieved from MyField)
                                                    '2 - TotalPaid 
                                                    '3 - DeleteLatePayment, True/False
                                                    bFirstItem = True
                                                    bOrdersExist = False
                                                    For Each oInvoice In oPayment.Invoices
                                                        If oInvoice.MATCH_Final Then
                                                            If oInvoice.MATCH_Matched Then
                                                                If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                                    If bFirstItem Then
                                                                        aOrderArray(0, 0) = oInvoice.MATCH_ID 'oInvoice.CustomerNo
                                                                        aOrderArray(1, 0) = oPayment.E_Name 'Payee
                                                                        aOrderArray(2, 0) = oInvoice.MON_InvoiceAmount.ToString 'TotalPaid
                                                                        aOrderArray(3, 0) = oPayment.DATE_Payment 'PaidDate
                                                                        aOrderArray(4, 0) = oPayment.Unique_PaymentID 'TransId
                                                                        aOrderArray(5, 0) = oPayment.E_Account 'PayeeBankAccount 
                                                                        aOrderArray(10, 0) = oInvoice.MyField2.Trim
                                                                        aOrderArray(11, 0) = sPayeeAddress
                                                                        aOrderArray(13, 0) = "True"
                                                                        'Added 11.09.2015
                                                                        If Not EmptyString(oInvoice.MyField3) Then
                                                                            aOrderArray(12, 0) = oInvoice.MyField3.Trim
                                                                        End If
                                                                        aPayableArray(0, 0) = oInvoice.CustomerNo
                                                                        aPayableArray(1, 0) = oInvoice.MyField 'oInvoice.InvoiceNo
                                                                        aPayableArray(2, 0) = oInvoice.MON_InvoiceAmount.ToString 'TotalPaid skal alltid v�re lik TotalAmount (m� kontrolleres)
                                                                        aPayableArray(3, 0) = oInvoice.MyField2
                                                                        bFirstItem = False
                                                                        bOrdersExist = True
                                                                    Else
                                                                        'Check if the order is already present in the array
                                                                        bAddNewItem = True
                                                                        For lArrayCounter = 0 To aOrderArray.GetUpperBound(1)
                                                                            If aOrderArray(0, lArrayCounter) = oInvoice.MATCH_ID Then
                                                                                bAddNewItem = False
                                                                                '05.11.2015  Commented next IF. Probably no use for this check
                                                                                'It created problems when one or more PAYABLE was matched and at lest one was marked
                                                                                ' to remove respitt (NO_CHARGES)
                                                                                'If oInvoice.MyField2.Trim <> aOrderArray(10, lArrayCounter) Then
                                                                                'Throw New Exception("Funnet forskjellige type oppdateringer p� innbetalingen.")
                                                                                'Else
                                                                                aOrderArray(2, lArrayCounter) = (CDbl(aOrderArray(2, lArrayCounter)) + oInvoice.MON_InvoiceAmount).ToString 'TotalPaid
                                                                                'Added 11.09.2015
                                                                                If Not EmptyString(oInvoice.MyField3) Then
                                                                                    If EmptyString(aOrderArray(12, lArrayCounter)) Then
                                                                                        aOrderArray(12, lArrayCounter) = oInvoice.MyField3.Trim
                                                                                    End If
                                                                                End If
                                                                                'End If
                                                                                Exit For
                                                                            Else
                                                                                'Just continue
                                                                            End If
                                                                        Next lArrayCounter
                                                                        If bAddNewItem Then
                                                                            ReDim Preserve aOrderArray(13, lArrayCounter)
                                                                            aOrderArray(0, lArrayCounter) = oInvoice.MATCH_ID
                                                                            aOrderArray(1, lArrayCounter) = oPayment.E_Name 'Payee
                                                                            aOrderArray(2, lArrayCounter) = oInvoice.MON_InvoiceAmount.ToString 'TotalPaid
                                                                            aOrderArray(3, lArrayCounter) = oPayment.DATE_Payment 'PaidDate
                                                                            aOrderArray(4, lArrayCounter) = oPayment.Unique_PaymentID 'TransId
                                                                            aOrderArray(5, lArrayCounter) = oPayment.E_Account 'PayeeBankAccount 
                                                                            aOrderArray(11, lArrayCounter) = sPayeeAddress
                                                                            aOrderArray(13, lArrayCounter) = "True"
                                                                            'Added 11.09.2015
                                                                            If Not EmptyString(oInvoice.MyField3) Then
                                                                                aOrderArray(12, lArrayCounter) = oInvoice.MyField3.Trim
                                                                            End If
                                                                        End If

                                                                        'Now the payablearray
                                                                        bAddNewItem = True
                                                                        For lArrayCounter2 = 0 To aPayableArray.GetUpperBound(1)
                                                                            If aPayableArray(0, lArrayCounter2) = oInvoice.CustomerNo Then
                                                                                If aPayableArray(1, lArrayCounter2) = oInvoice.MyField Then 'oInvoice.InvoiceNo Then
                                                                                    bAddNewItem = False
                                                                                    Exit For
                                                                                Else
                                                                                    'Just continue
                                                                                End If
                                                                            Else
                                                                                'Just continue
                                                                            End If
                                                                        Next lArrayCounter2
                                                                        If bAddNewItem Then
                                                                            ReDim Preserve aPayableArray(3, lArrayCounter2)
                                                                            aPayableArray(0, lArrayCounter2) = oInvoice.CustomerNo
                                                                            aPayableArray(1, lArrayCounter2) = oInvoice.MyField 'oInvoice.InvoiceNo
                                                                            aPayableArray(2, lArrayCounter2) = oInvoice.MON_InvoiceAmount.ToString
                                                                            aPayableArray(3, lArrayCounter2) = oInvoice.MyField2
                                                                        Else
                                                                            aPayableArray(2, lArrayCounter2) = (CDbl(aPayableArray(2, lArrayCounter2)) + oInvoice.MON_InvoiceAmount).ToString
                                                                        End If
                                                                    End If

                                                                End If
                                                            End If
                                                        End If
                                                    Next oInvoice

                                                    For Each oInvoice In oPayment.Invoices
                                                        If oInvoice.MATCH_Final Then
                                                            If oInvoice.MATCH_Matched Then
                                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                                    '6 - TotalFee
                                                                    '7 - Rounding
                                                                    '8 - AcceptedDeviance
                                                                    '9 - ReImbursement
                                                                    If Not EmptyString(oInvoice.CustomerNo) Then
                                                                        'Hopefully a valid ordernumber is stated in the CustomerNoField
                                                                        'Find the corresponding entry in the Orderarray
                                                                        bFound = False
                                                                        For lArrayCounter = 0 To aOrderArray.GetUpperBound(1)
                                                                            If aOrderArray(0, lArrayCounter) = oInvoice.CustomerNo.Trim Then
                                                                                bFound = True
                                                                                Exit For
                                                                            Else
                                                                                'Just continue
                                                                            End If
                                                                        Next lArrayCounter
                                                                        If Not bFound Then
                                                                            'If no corresponding orderref is found among the matched orders, use the first one
                                                                            lArrayCounter = 0
                                                                        End If
                                                                    Else
                                                                        'No orderref is stated, use the first one for the adjustment
                                                                        lArrayCounter = 0
                                                                    End If
                                                                    Select Case oInvoice.MATCH_ID.Trim

                                                                        Case constPatentstyretFee
                                                                            aOrderArray(6, lArrayCounter) = (CDbl(aOrderArray(6, lArrayCounter)) + oInvoice.MON_InvoiceAmount).ToString

                                                                        Case constPatentstyretRoundings
                                                                            aOrderArray(7, lArrayCounter) = (CDbl(aOrderArray(7, lArrayCounter)) + oInvoice.MON_InvoiceAmount).ToString

                                                                        Case constPatentstyretAcceptedDeviance
                                                                            aOrderArray(8, lArrayCounter) = (CDbl(aOrderArray(8, lArrayCounter)) + oInvoice.MON_InvoiceAmount).ToString

                                                                        Case constPatentstyretReImbursement
                                                                            aOrderArray(9, lArrayCounter) = (CDbl(aOrderArray(9, lArrayCounter)) + oInvoice.MON_InvoiceAmount).ToString

                                                                    End Select

                                                                End If
                                                            End If
                                                        End If
                                                    Next oInvoice

                                                    'OVERGANG TIL VS2017 - Fjern kommentar p� kode fram til teksten OVERGANG SLUTT TIL VS2017

                                                    'If bOrdersExist Then
                                                    '    sErrorText = "Initierer oOrderUpdate"
                                                    '    oOrderUpdate = New BB_Special.PatentstyretUpdate
                                                    '    oOrderUpdate.FixedErrorText = CreateStandardErrorText(oPayment)
                                                    '    oOrderUpdate.PassOrders = aOrderArray
                                                    '    oOrderUpdate.PassPayables = aPayableArray
                                                    '    'oOrderUpdate.PassArticles = aArticles '10.09.2015 - Commented

                                                    '    Dim sClientBaseAddress As String = ""
                                                    '    Dim sLoginAddress As String = ""
                                                    '    Dim sLoginUser As String = ""
                                                    '    Dim sLoginPassword As String = ""

                                                    '    '' Set Login user
                                                    '    sLoginUser = ConfigurationSettings.AppSettings("USER")
                                                    '    If Not EmptyString(sLoginUser) Then
                                                    '        If Not RunTime() Then
                                                    '            oOrderUpdate.LoginUser = "Kjell.Isingrud@visualbanking.net"
                                                    '        Else
                                                    '            oOrderUpdate.LoginUser = sLoginUser
                                                    '        End If
                                                    '    End If
                                                    '    ' Set Login password
                                                    '    sLoginPassword = ConfigurationSettings.AppSettings("PASSWORD")
                                                    '    sLoginPassword = CheckAndDecrypt(sLoginPassword)

                                                    '    If Not EmptyString(sLoginPassword) Then
                                                    '        If Not RunTime() Then
                                                    '            oOrderUpdate.LoginPassWord = "Visual00%%"
                                                    '        Else
                                                    '            oOrderUpdate.LoginPassWord = sLoginPassword
                                                    '        End If
                                                    '    End If

                                                    '    ' Set URL-addresses;
                                                    '    ' Set Base address
                                                    '    If Not RunTime() And Date.Today <> StringToDate("20160519") Then
                                                    '        sClientBaseAddress = "https://ws-test.patentstyret.no/"
                                                    '    Else
                                                    '        sClientBaseAddress = ConfigurationSettings.AppSettings("URL1")
                                                    '    End If
                                                    '    If Not EmptyString(sClientBaseAddress) And sClientBaseAddress <> "xxx.xxx.xxx.xxx" Then
                                                    '        oOrderUpdate.ClientBaseAddress = sClientBaseAddress
                                                    '    End If

                                                    '    ' Set Login address
                                                    '    If Not RunTime() And Date.Today <> StringToDate("20200323") Then
                                                    '        sLoginAddress = "https://login-test.patentstyret.no/core"
                                                    '    Else
                                                    '        sLoginAddress = ConfigurationSettings.AppSettings("Patent_Server")
                                                    '        'sLoginAddress = ConfigurationSettings.AppSettings("URL2")
                                                    '    End If
                                                    '    If Not EmptyString(sLoginAddress) And sLoginAddress <> "xxx.xxx.xxx.xxx" Then
                                                    '        oOrderUpdate.LoginAddress = sLoginAddress
                                                    '    End If
                                                    '    sErrorText = "oOrderUpdate.RunRequestToken()"

                                                    '    oOrderUpdate.RunRequestToken()

                                                    '    sErrorText = "oOrderUpdate.DoTheUpdate"
                                                    '    If oOrderUpdate.DoTheUpdate Then
                                                    '        'MsgBox("OK")
                                                    '        '/////////////////////////////////////////////////////////////
                                                    '        'NEW 25.08.2015
                                                    '        'For new orders added, set orderRef into oInvoice.CustomerNo
                                                    '        aArticles = oOrderUpdate.PassArticles
                                                    '        nCardFee = oOrderUpdate.CardFee '13.05.2016 - Added
                                                    '        For Each oInvoice In oPayment.Invoices
                                                    '            'KJELL - er denne neste riktig?
                                                    '            If oInvoice.CustomerNo = "NY_ORDRE" Then
                                                    '                oInvoice.CustomerNo = oOrderUpdate.ReturnedOrderRef
                                                    '            End If
                                                    '        Next
                                                    '        '////////////////////////////////////////////////////////////

                                                    '    Else
                                                    '        MsgBox("Feil i oOrderUpdate.DoTheUpdate")
                                                    '        ' -- hva gj�r vi her ????????
                                                    '    End If

                                                    'End If

                                                    'OVERGANG SLUTT TIL VS2017

                                                    For Each oInvoice In oPayment.Invoices
                                                        If oInvoice.MATCH_Final Then
                                                            If oInvoice.MON_InvoiceAmount <> 0 Then

                                                                sFixedErrorText = CreateStandardErrorText(oPayment, oInvoice)

                                                                ' XOKNET 15.05.2012 - added GL-record

                                                                If oInvoice.MATCH_Matched = False Then
                                                                    If iNoMatchAccountType = 2 Then 'GL
                                                                        sErrorText = "WriteAgresso_Patentstyret_Transaction (GL)"
                                                                        If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oPayment.MON_InvoiceCurrency, xDelim(sNoMatchAccount, "-", 1), xDelim(sNoMatchAccount, "-", 2), xDelim(sNoMatchAccount, "-", 3), xDelim(sNoMatchAccount, "-", 4), xDelim(sNoMatchAccount, "-", 5), "", False, oPayment.Unique_PaymentID.ToString & " - " & sFreetext, oBatch.REF_Bank, ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".")) Then

                                                                        End If
                                                                        writer.WriteEndElement() 'Transaction
                                                                        'TODO: Lag denne funksjonen, b�r ogs� benyttes fra WriteAgresso_Patentstyret
                                                                        'sLine = WriteAgresso_PatentstyretPost(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate)
                                                                    Else
                                                                        'TODO: Lag denne funksjonen, b�r ogs� benyttes fra WriteAgresso_Patentstyret
                                                                        'sLine = WriteAgresso_PatentstyretPost(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate)
                                                                    End If
                                                                ElseIf oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                                                    sErrorText = "WriteAgresso_Patentstyret_Transaction (GL)"
                                                                    sText = ""
                                                                    For Each oFreetext In oInvoice.Freetexts
                                                                        If oFreetext.Qualifier = 4 Then
                                                                            sText = sText & oFreetext.Text
                                                                        End If
                                                                    Next oFreetext
                                                                    If Not EmptyString(sText) Then
                                                                        sText = oPayment.Unique_PaymentID.ToString & " - " & sText & " - " & sFreetext
                                                                    Else
                                                                        sText = oPayment.Unique_PaymentID.ToString & " - " & sFreetext
                                                                    End If
                                                                    If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oPayment.MON_InvoiceCurrency, oInvoice.MATCH_ID, oInvoice.Dim1, oInvoice.Dim2, oInvoice.Dim3, oInvoice.Dim4, "", False, sText, oBatch.REF_Bank, ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".")) Then

                                                                    End If
                                                                    writer.WriteEndElement() 'Transaction
                                                                    'Add to totalamountexported
                                                                    '19.11.2015 - Changed form lArrayCounter to iUpdayTotalsArrayCounter
                                                                    aGLAmountArray(iUpdayTotalsArrayCounter) = aGLAmountArray(iUpdayTotalsArrayCounter) + oInvoice.MON_InvoiceAmount
                                                                Else

                                                                    'Retrieve correct articles and update an Array which will be used to update a GL result account.

                                                                    'Matched against the order-mdule, update a GL result account
                                                                    'sErrorText = "WriteAgresso_Patentstyret_Transaction (GL Result)"
                                                                    'If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oPayment, xDelim(sGLResultsAccount, "-", 1), xDelim(sGLResultsAccount, "-", 2), xDelim(sGLResultsAccount, "-", 3), False, "CREMUL", oBatch.REF_Bank, ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, "", ".")) Then

                                                                    'End If
                                                                    'writer.WriteEndElement() 'Transaction

                                                                    'Add to totalamountexported
                                                                    '19.11.2015 - Changed form lArrayCounter to iUpdayTotalsArrayCounter
                                                                    aARAmountArray(iUpdayTotalsArrayCounter) = aARAmountArray(iUpdayTotalsArrayCounter) + oInvoice.MON_InvoiceAmount
                                                                End If
                                                            End If
                                                        End If
                                                    Next oInvoice

                                                    '13.05.2016 - Added posting of cardcharges
                                                    If nCardFee > 0 Then
                                                        sErrorText = "WriteAgresso_Patentstyret_Transaction (GL CardFee)"
                                                        sText = oPayment.Unique_PaymentID.ToString & " - " & "CardFee - " & oPayment.E_Name
                                                        If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oPayment.MON_InvoiceCurrency, constPatentstyretFee, "440", "", "", "", "", False, sText, oBatch.REF_Bank, ConvertFromAmountToString(nCardFee * -1, "", ".")) Then

                                                        End If
                                                        writer.WriteEndElement() 'Transaction
                                                    End If


                                                    'Write the debet and credit posting against the observation account (move the amount from this account)
                                                    If bPostAgainstObservationAccount Then
                                                        'TODO: Lag denne funksjonen, b�r ogs� benyttes fra WriteAgresso_Patentstyret
                                                        'sLine = WriteAgresso_PatentstyretPost(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate)

                                                        'TODO: Lag denne funksjonen, b�r ogs� benyttes fra WriteAgresso_Patentstyret
                                                        'sLine = WriteAgresso_PatentstyretPost(UCase(oBabel.Special), oPayment, oInvoice, oClient, sTransactionDate)

                                                    End If

                                                    sPayCode = oPayment.PayCode

                                                    oPayment.Exported = True
                                                End If


                                                If aArticles Is Nothing Then

                                                Else
                                                    For i = 0 To aArticles.GetUpperBound(1)

                                                        'SELECT A.KONTO AS BBRET_AccountNo, A.DIM1 AS BBRET_Dim1, A.DIM4 AS BBRET_Dim4 FROM ORDERS O, PAYABLES P, PAYABLEITEMS PI, OM_Articles A WHERE O.Refnr = 'BB_MyText' AND O.OrderID = P.OrderID AND P.OrderPayableId = PI.OrderPayableId AND PI.FeeCode = A.ARTICLE_ID
                                                        sMySQL = Replace(sRetrievAccountingStringSQL, "BB_MyText", aArticles(0, i))

                                                        sArticleAccount = ""
                                                        sArticleDim1 = ""
                                                        sArticleDim4 = ""

                                                        ' run the sql
                                                        oERPDal.SQL = sMySQL
                                                        If oERPDal.Reader_Execute() Then
                                                            If oERPDal.Reader_HasRows Then
                                                                Do While oERPDal.Reader_ReadRecord
                                                                    ' We found an entry in the view, retrieve data
                                                                    sArticleAccount = oERPDal.Reader_GetString("BBRET_AccountNo")
                                                                    sArticleDim1 = oERPDal.Reader_GetString("BBRET_Dim1")
                                                                    sArticleDim4 = oERPDal.Reader_GetString("BBRET_Dim4")

                                                                    Exit Do
                                                                Loop
                                                            Else
                                                                sArticleAccount = "2991"
                                                                sArticleDim1 = ""
                                                                sArticleDim4 = ""
                                                            End If
                                                        Else
                                                            Throw New Exception(LRSCommon(45002) & oERPDal.ErrorMessage)
                                                        End If

                                                        If EmptyString(sArticleAccount) Then
                                                            sArticleAccount = "2991"
                                                        End If

                                                        'Matched against the order-mdule, update a GL result account
                                                        If Not WriteAgresso_Patentstyret_Transaction(writer, defaultNS, agrlibNS, prefix, oBatch.Payments.Item(1).MON_InvoiceCurrency, sArticleAccount, sArticleDim1, "", "", sArticleDim4, "", False, oPayment.Unique_PaymentID.ToString & " - " & "CREMUL", oBatch.REF_Bank, ConvertFromAmountToString(CDbl(aArticles(1, i)) * -100, "", ".")) Then

                                                        End If
                                                        writer.WriteEndElement() 'Transaction
                                                    Next i
                                                    aArticles = Nothing
                                                End If

                                            Next oPayment ' payment 10.09.2015 - Moved from before the posting of articles in Agresso.

                                            writer.WriteEndElement() 'Voucher

                                        End If
                                    End If
                                Next 'batch
                            End If

                        Next oBabel
                    End Using
                End If  ' if bContinue 
            End If

            If oBabelFiles.Count > 0 Then
                sErrorText = "dbUpdayTotals"
                If oBabelFiles(1).VB_ProfileInUse Then
                    If oBabelFiles(1).VB_Profile.ManMatchFromERP Then
                        For lCounter = 0 To UBound(aAccountArray, 2)
                            dbUpDayTotals(False, DateToString(Now), aARAmountArray(lCounter), "MATCHED_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                            dbUpDayTotals(False, DateToString(Now), aGLAmountArray(lCounter), "MATCHED_GL", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                        Next lCounter
                    End If
                End If
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WritePatentstyret_Order" & vbCrLf & ex.Message & vbCrLf & vbCrLf & vbCrLf & sErrorText, ex, oPayment, "", sFilenameOut)

        Finally

        End Try

        WritePatentstyret_Order = True

    End Function

    Private Function Patentstyret_ValidateDoublePayments(ByVal oProfile As vbBabel.Profile, ByVal bPostAgainstObservationAccount As Boolean, ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim sMySQL As String
        Dim sMyINSERTSQL As String
        Dim sMyERPSQLGetPayableID As String = "SELECT PayableID, ApplicationId, TotalAmount*100 AS IAmount FROM Payables WHERE OrderId = Match_Id"
        '13.05.206 - Added AND PaidStateCode <> 3
        Dim sMyERPSQLCheckPayableID As String = "SELECT PayableID FROM Payables WHERE PayableId = Match_Id AND PaidStateCode <> 0 AND OrderPayableId IS NULL"
        '02.06.2016 - Added next line
        Dim sE_AccountNo As String
        'PaidStateCode = 3, signifies paid with card
        Dim sMyERPSQLCheckPayableIDCard As String = "SELECT PayableID FROM Payables WHERE PayableId = Match_Id AND PaidStateCode <> 0 AND PaidStateCode <> 3 AND OrderPayableId IS NULL"
        Dim oMyBBDal As vbBabel.DAL = Nothing
        Dim oMyBBDalINSERT As vbBabel.DAL = Nothing
        Dim sCompanyID As String = "1"
        Dim oMyERPDal As vbBabel.DAL = Nothing
        Dim sMatch_ID As String
        Dim bTestMode As Boolean = False
        Dim j As Integer
        Dim oBabelReport As vbBabel.BabelReport
        Dim lPatentStyretCounter As Long
        Dim bReturnvalue As Boolean = False
        Dim oFileSetup As vbBabel.FileSetup

        'Connect to BBDB
        oMyBBDal = New vbBabel.DAL
        oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyBBDal.ConnectToDB() Then
            Throw New System.Exception(oMyBBDal.ErrorMessage)
        End If

        'and a second connection
        oMyBBDalINSERT = New vbBabel.DAL
        oMyBBDalINSERT.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyBBDalINSERT.ConnectToDB() Then
            Throw New System.Exception(oMyBBDalINSERT.ErrorMessage)
        End If

        'Connect to ERPDB
        oMyERPDal = New vbBabel.DAL
        oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
        oMyERPDal.Company_ID = sCompanyID
        oMyERPDal.DBProfile_ID = 1 'TODO: Hardcoded DBProfile_ID
        If Not oMyERPDal.ConnectToDB() Then
            Throw New System.Exception(oMyERPDal.ErrorMessage)
        End If

        If bTestMode Then
            sMyERPSQLGetPayableID = "SELECT PayableID FROM Payables WHERE OrderPayableId = Match_Id"
            sMyERPSQLCheckPayableID = "SELECT PayableID FROM Payables WHERE PayableId = Match_Id AND PaidState <> 0"
        End If

        sMySQL = "DELETE FROM PatentDoubleCheck"
        oMyBBDal.SQL = sMySQL

        If oMyBBDal.ExecuteNonQuery() Then
            'OK, table exist
        Else
            sMySQL = "CREATE TABLE PatentDoubleCheck ( " & _
                "[Company_Id] Integer NOT NULL, " & _
                "[BabelFile_Id] Double NOT NULL, " & _
                "[Batch_Id] Double NOT NULL, " & _
                "[Payment_ID] Double NOT NULL, " & _
                "[Invoice_ID] Double NOT NULL, " & _
                "[CustomerNo] Text(25) WITH Compression, " & _
                "[InvoiceNo] Text(25) WITH Compression, " & _
                "[Match_ID] Text(35) WITH Compression, " & _
                "[DoublettType] Integer, " & _
                "CONSTRAINT [pk_Index] PRIMARY KEY (Company_Id, BabelFile_Id, Batch_ID, Payment_ID, Invoice_Id))"

            oMyBBDal.SQL = sMySQL
            oMyBBDal.ExecuteNonQuery()

            sMySQL = "CREATE INDEX MyTable_idx1 on PatentDoubleCheck(Match_ID)"
            oMyBBDal.SQL = sMySQL
            oMyBBDal.ExecuteNonQuery()

        End If


        sMySQL = "SELECT E_Account FROM PatentDoubleCheck"
        oMyBBDal.SQL = sMySQL

        If Not oMyBBDal.Reader_Execute Then
            sMySQL = "ALTER TABLE PatentDoubleCheck ADD COLUMN E_Account CHAR (35) DEFAULT " & Chr(34) & " " & Chr(34) & "NOT NULL"
            oMyBBDal.SQL = sMySQL
            oMyBBDal.ExecuteNonQuery()
        End If


        '19.02.2016 - Added AND I.MATCH_Type = 0
        sMySQL = "SELECT P.BabelFile_ID, P.Batch_ID, P.Payment_ID, I.Invoice_ID, P.MON_InvoiceAmount, P.E_Name, I.MON_InvoiceAmount AS IAmount, I.Match_ID, I.CustomerNo, I.InvoiceNo, I.MyField, P.E_Account AS E_Account "
        sMySQL = sMySQL & "FROM Payment P LEFT JOIN Invoice I ON P.Company_ID = I.Company_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Batch_ID = I.Batch_ID and P.Payment_ID = I.Payment_ID "
        sMySQL = sMySQL & "WHERE P.Company_ID = " & sCompanyID & " AND P.MATCH_Matched > 0 AND I.MATCH_Final = True AND I.MATCH_Matched = True AND I.MATCH_Type = 0"
        oMyBBDal.SQL = sMySQL

        If oMyBBDal.Reader_Execute() Then

            Do While oMyBBDal.Reader_ReadRecord
                sMatch_ID = ""
                If bTestMode Then
                    If oMyBBDal.Reader_GetString("CustomerNo") <> "NY_ORDRE" Then
                        'Retrieve the original PayableID from the views based on the OrderPayableID
                        If oMyBBDal.Reader_GetString("CustomerNo").Length > 0 Then
                            oMyERPDal.SQL = Replace(sMyERPSQLGetPayableID, "Match_ID", oMyBBDal.Reader_GetString("CustomerNo"), , , CompareMethod.Text)
                            If oMyERPDal.Reader_Execute() Then

                                If oMyERPDal.Reader_HasRows Then
                                    ' one record only
                                    oMyERPDal.Reader_ReadRecord()
                                    sMatch_ID = oMyERPDal.Reader_GetString("PayableID")
                                Else
                                    sMatch_ID = oMyBBDal.Reader_GetString("CustomerNo")
                                End If
                            End If
                        End If
                    Else
                        sMatch_ID = oMyBBDal.Reader_GetString("Match_ID")
                    End If
                Else
                    If oMyBBDal.Reader_GetString("CustomerNo") <> "NY_ORDRE" Then
                        'Retrieve the original PayableID from the views based on the OrderPayableID

                        If oMyBBDal.Reader_GetString("Match_ID").Length > 0 Then
                            oMyERPDal.SQL = Replace(sMyERPSQLGetPayableID, "Match_ID", oMyBBDal.Reader_GetString("Match_ID"), , , CompareMethod.Text)
                            If oMyERPDal.Reader_Execute() Then

                                If oMyERPDal.Reader_HasRows Then
                                    ' one record only
                                    lPatentStyretCounter = 0
                                    Do While oMyERPDal.Reader_ReadRecord()
                                        lPatentStyretCounter = lPatentStyretCounter + 1
                                        sMatch_ID = oMyERPDal.Reader_GetString("PayableID")

                                        'Save the info in the table PatentDoubleCheck
                                        sMyINSERTSQL = "INSERT INTO PatentDoubleCheck(Company_Id, BabelFile_Id, Batch_Id, Payment_ID, Invoice_ID, "
                                        sMyINSERTSQL = sMyINSERTSQL & "CustomerNo , InvoiceNo, Match_ID, DoublettType, Amount, E_Account) "
                                        sMyINSERTSQL = sMyINSERTSQL & "VALUES(" & sCompanyID & ", " & oMyBBDal.Reader_GetString("BabelFile_ID") & ", " & oMyBBDal.Reader_GetString("Batch_ID")
                                        sMyINSERTSQL = sMyINSERTSQL & ", " & oMyBBDal.Reader_GetString("Payment_ID") & ", " & oMyBBDal.Reader_GetString("Invoice_ID")
                                        sMyINSERTSQL = sMyINSERTSQL & ", '" & oMyBBDal.Reader_GetString("CustomerNo") & "', '" & oMyERPDal.Reader_GetString("ApplicationID")
                                        sMyINSERTSQL = sMyINSERTSQL & "', '" & sMatch_ID & "', 0, " & oMyERPDal.Reader_GetString("IAmount") & ", '" & oMyBBDal.Reader_GetString("E_Account") & "')"

                                        If Not RunTime() Then
                                            sMyINSERTSQL = "INSERT INTO PatentDoubleCheck(Company_Id, BabelFile_Id, Batch_Id, Payment_ID, Invoice_ID, CustomerNo, InvoiceNo, Match_ID, DoublettType, Amount, E_Account) VALUES(1, 2, 3, 1, 2, 'XR005782529', 'Sant', '57825', 0, 2020000, '50011610886')"
                                        End If

                                        oMyBBDalINSERT.SQL = sMyINSERTSQL

                                        If oMyBBDalINSERT.ExecuteNonQuery Then

                                            If oMyBBDalINSERT.RecordsAffected > 0 Then
                                                'OK
                                            Else

                                            End If

                                        Else

                                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDalINSERT.ErrorMessage)

                                        End If

                                    Loop

                                End If
                            End If
                        End If
                    Else
                        sMatch_ID = oMyBBDal.Reader_GetString("MyField")

                        'Save the info in the table PatentDoubleCheck
                        sMyINSERTSQL = "INSERT INTO PatentDoubleCheck(Company_Id, BabelFile_Id, Batch_Id, Payment_ID, Invoice_ID, "
                        sMyINSERTSQL = sMyINSERTSQL & "CustomerNo , InvoiceNo, Match_ID, DoublettType, Amount) "
                        sMyINSERTSQL = sMyINSERTSQL & "VALUES(" & sCompanyID & ", " & oMyBBDal.Reader_GetString("BabelFile_ID") & ", " & oMyBBDal.Reader_GetString("Batch_ID")
                        sMyINSERTSQL = sMyINSERTSQL & ", " & oMyBBDal.Reader_GetString("Payment_ID") & ", " & oMyBBDal.Reader_GetString("Invoice_ID")
                        sMyINSERTSQL = sMyINSERTSQL & ", '" & oMyBBDal.Reader_GetString("CustomerNo") & "', '" & oMyBBDal.Reader_GetString("InvoiceNo")
                        sMyINSERTSQL = sMyINSERTSQL & "', '" & sMatch_ID & "', 0, " & oMyBBDal.Reader_GetString("IAmount") & ")"

                        oMyBBDalINSERT.SQL = sMyINSERTSQL

                        If oMyBBDalINSERT.ExecuteNonQuery Then

                            If oMyBBDalINSERT.RecordsAffected > 0 Then
                                'OK
                            Else

                            End If

                        Else

                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDalINSERT.ErrorMessage)

                        End If

                    End If
                End If

            Loop
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDal.ErrorMessage)

        End If

        'Reconnect to BBDB
        If Not oMyBBDal Is Nothing Then
            oMyBBDal.Close()
            oMyBBDal = Nothing

            oMyBBDal = New vbBabel.DAL
            oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyBBDal.ConnectToDB() Then
                Throw New System.Exception(oMyBBDal.ErrorMessage)
            End If
        End If

        'and reconnect a second connection
        If Not oMyBBDalINSERT Is Nothing Then
            oMyBBDalINSERT.Close()
            oMyBBDalINSERT = Nothing
            oMyBBDalINSERT = New vbBabel.DAL
            oMyBBDalINSERT.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyBBDalINSERT.ConnectToDB() Then
                Throw New System.Exception(oMyBBDalINSERT.ErrorMessage)
            End If
        End If

        'First check if we have an identical match within the PatentDoubleCheck-table
        If bTestMode Then
            sMySQL = "SELECT CustomerNo AS Match_ID FROM (SELECT CustomerNo, COUNT(*) AS Occ FROM Invoice II GROUP BY II.CustomerNo) WHERE Occ > 1"
        Else
            sMySQL = "SELECT Match_ID FROM (SELECT Match_ID, COUNT(*) AS Occ FROM PatentDoubleCheck GROUP BY Match_ID) WHERE Occ > 1"
        End If
        oMyBBDal.SQL = sMySQL

        If oMyBBDal.Reader_Execute() Then
            If oMyBBDal.Reader_HasRows Then
                Do While oMyBBDal.Reader_ReadRecord
                    sMatch_ID = ""
                    sMatch_ID = oMyBBDal.Reader_GetString("Match_ID")
                    If sMatch_ID.Length > 0 Then
                        sMyINSERTSQL = "UPDATE PatentDoubleCheck SET DoublettType = 1 WHERE Match_ID = '" & sMatch_ID & "'"
                        oMyBBDalINSERT.SQL = sMyINSERTSQL

                        If oMyBBDalINSERT.ExecuteNonQuery Then

                            If oMyBBDalINSERT.RecordsAffected > 0 Then
                                'OK
                            Else

                            End If

                        Else

                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDalINSERT.ErrorMessage)

                        End If
                    End If
                Loop
            End If
        End If

        'Reconnect to BBDB
        If Not oMyBBDal Is Nothing Then
            oMyBBDal.Close()
            oMyBBDal = Nothing

            oMyBBDal = New vbBabel.DAL
            oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyBBDal.ConnectToDB() Then
                Throw New System.Exception(oMyBBDal.ErrorMessage)
            End If
        End If

        'and reconnect a second connection
        If Not oMyBBDalINSERT Is Nothing Then
            oMyBBDalINSERT.Close()
            oMyBBDalINSERT = Nothing
            oMyBBDalINSERT = New vbBabel.DAL
            oMyBBDalINSERT.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyBBDalINSERT.ConnectToDB() Then
                Throw New System.Exception(oMyBBDalINSERT.ErrorMessage)
            End If
        End If

        'Secondly check if any of the matched payments already are matched
        sMySQL = "SELECT DISTINCT Match_ID, E_Account FROM PatentDoubleCheck"
        oMyBBDal.SQL = sMySQL

        If oMyBBDal.Reader_Execute() Then
            If oMyBBDal.Reader_HasRows Then
                Do While oMyBBDal.Reader_ReadRecord

                    If oMyBBDal.Reader_GetString("Match_ID").Length > 0 Then
                        sMatch_ID = oMyBBDal.Reader_GetString("Match_ID")
                        sE_AccountNo = oMyBBDal.Reader_GetString("E_Account")
                        'Added 02.06.2016 for Bankaxess and Swedbank
                        If sE_AccountNo = "14300500820" Or sE_AccountNo = "98760598765" Then '14300500820 = SWEDBANK, ??? = BANKAXESS
                            oMyERPDal.SQL = Replace(sMyERPSQLCheckPayableIDCard, "Match_ID", oMyBBDal.Reader_GetString("Match_ID"), , , CompareMethod.Text)
                        Else
                            oMyERPDal.SQL = Replace(sMyERPSQLCheckPayableID, "Match_ID", oMyBBDal.Reader_GetString("Match_ID"), , , CompareMethod.Text)
                        End If
                        If oMyERPDal.Reader_Execute() Then

                            If oMyERPDal.Reader_HasRows Then
                                ' The Payment is already paid, update the Doublettmark
                                sMyINSERTSQL = "UPDATE PatentDoubleCheck SET DoublettType = DoublettType + 2 WHERE Match_ID = '" & oMyBBDal.Reader_GetString("Match_ID") & "'"
                                oMyBBDalINSERT.SQL = sMyINSERTSQL

                                If oMyBBDalINSERT.ExecuteNonQuery Then

                                    If oMyBBDalINSERT.RecordsAffected > 0 Then
                                        'OK
                                    Else

                                    End If

                                Else

                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDalINSERT.ErrorMessage)

                                End If
                            End If
                        End If
                    End If
                Loop
            End If
        End If


        ' activate report, call report 003
        oFilesetup = oProfile.FileSetups(2)
        For j = 1 To oFilesetup.Reports.Count
            If oFilesetup.Reports(j).ReportNo = 3 Then ' Specialreport

                oBabelReport = New vbBabel.BabelReport
                oBabelReport.Special = "PATENTSTYRET_DOUBLE"
                oBabelReport.BabelFiles = Nothing
                oBabelReport.ReportOnSelectedItems = False
                oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
                If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
                    bReturnvalue = False
                Else
                    bReturnvalue = oBabelReport.EmptyReport
                End If

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                Exit For
            End If
        Next j

        If bPostAgainstObservationAccount Then '19.11.2015 - Added this IF

            'The export is initiated from the menu after manual matching

            'OK, nothing to do

        Else
            'The export is initiated from auomatic matching

            'Just set the mathced items as proposed matched

            'Reconnect to BBDB
            'If Not oMyBBDal Is Nothing Then
            '    oMyBBDal.Close()
            '    oMyBBDal = Nothing

            '    oMyBBDal = New vbBabel.DAL
            '    oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            '    If Not oMyBBDal.ConnectToDB() Then
            '        Throw New System.Exception(oMyBBDal.ErrorMessage)
            '    End If
            'End If

            'sMySQL = "SELECT P.Company_ID AS Company_ID, P.BabelFile_ID AS BabelFile_ID, P.Batch_ID AS Batch_ID, P.Payment_ID AS Payment_ID, P.MON_InvoiceAmount AS MON_InvoiceAmount, P.Unique_PaymentID, P.MATCH_Matched "
            'sMySQL = sMySQL & "FROM (((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
            'sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
            'sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID) "
            'sMySQL = sMySQL & "INNER JOIN PatentDoubleCheck PDC ON PDC.Company_ID = I.Company_ID AND PDC.BabelFile_ID = I.BabelFile_ID AND PDC.Batch_ID = I.Batch_ID AND PDC.Payment_ID = I.Payment_ID AND PDC.Invoice_ID = I.Invoice_ID "
            'sMySQL = sMySQL & "WHERE (BA.MON_InvoiceAmount >= 0 And I.MATCH_Final = True) AND PDC.DoublettType > 0 "
            'sMySQL = sMySQL & "GROUP BY P.Company_ID, P.BabelFile_ID, P.Batch_ID, P.Payment_ID,  P.MON_InvoiceAmount, P.Unique_PaymentID, P.MATCH_Matched "
            'sMySQL = sMySQL & "ORDER BY P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC"
            'oMyBBDal.SQL = sMySQL

            'If oMyBBDal.Reader_Execute() Then
            '    If oMyBBDal.Reader_HasRows Then
            '        Do While oMyBBDal.Reader_ReadRecord
            '            If CInt(oMyBBDal.Reader_GetString("Match_ID")) = BabelFiles.MatchStatus.Matched Then

            '                'First, we must set it to proposed matched in the BBDB

            '                'Secondly


            '            End If
            '        Loop
            '    End If
            'End If
        End If

        Return bReturnvalue

    End Function
    'XOKNET 26.03.2015 added function
    Function Write_Archer_US_CSV(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sSpecial As String) As Boolean
        ' Export Incoming payments from lockbox and ACH-files
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As FreeText
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sSeparator As String
        Dim sLine As String
        Dim sTmp As String
        Dim nSequenceNumber As Double

        Try

            ' create an outputfile
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nSequenceNumber = 0
            ' heading;
            ' XOKNET 20.08.2015 added two new fields
            sLine = "NUMBER; DEPOSIT DATE;COMPANY NUMBER;COMPANY NAME;LOCKBOX SITE;LOCKBOX NUMBER;BATCH NUMBER;ITEM NUMBER;CHECK AMOUNT;CHECK NUMBER;REMITTER NAME;INVOICE NUMBER;INVOICE AMOUNT;COMPANY.ID/CHECK R/T;ORIG.DFI ID/CHECK DDA ACCT"
            oFile.WriteLine(sLine)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                ' XOKNET 25.09.2015 -
                                ' do not export credits
                                If oPayment.VoucherType = "26" Or oPayment.VoucherType = "27" Or oPayment.VoucherType = "28" Or oPayment.VoucherType = "29" Or oPayment.VoucherType = "36" Or oPayment.VoucherType = "37" Or oPayment.VoucherType = "38" Or oPayment.VoucherType = "39" Or oPayment.VoucherType = "46" Or oPayment.VoucherType = "47" Or oPayment.VoucherType = "48" Or oPayment.VoucherType = "49" Or oPayment.VoucherType = "55" Or oPayment.VoucherType = "56" Then
                                    bExportoPayment = False
                                    oPayment.Exported = True
                                End If

                                If bExportoPayment Then

                                    ' Exportformat:
                                    ' XOKNET 20.08.2015 added two new fields
                                    'Number;Deposit Date;Company Numbers;Company Name;Lockbox Number;Last Update Time;Last Update Date;Batch Number;Item Number;Check Amount;Remitter Name;Invoice Number;Invoice Amount;DFI.ID/CHECK R/T;DFI ACCOUNT/CHECK DDA ACCT"


                                    sSeparator = ";"
                                    ' Can be more than one invoice pr payment
                                    sLine = vbNullString
                                    For Each oInvoice In oPayment.Invoices
                                        If oPayment.Invoices.Count = 1 Or oInvoice.MATCH_Original = False Or oPayment.PayCode = 665 Then
                                            nSequenceNumber = nSequenceNumber + 1

                                            '1. Number
                                            sLine = Trim(Str(Int(nSequenceNumber))) & sSeparator
                                            '2. Deposit Date
                                            sTmp = oBatch.DATE_Production
                                            'sLine = sLine & Mid(sTmp, 5, 2) & Mid(sTmp, 7, 2) & Left(sTmp, 4) & sSeparator ' MMDDYYYY
                                            ' XOKNET 25.08.2015
                                            sLine = sLine & Mid(sTmp, 5, 2) & "/" & Mid(sTmp, 7, 2) & "/" & Left(sTmp, 4) & sSeparator ' MM/DD/YYYY

                                            '3. Company Numbers;
                                            sLine = sLine & Trim(oPayment.I_Account) & sSeparator
                                            '4. Company Name;
                                            sLine = sLine & Trim(oBatch.I_Branch) & sSeparator
                                            '5. Lockbox Number;
                                            sLine = sLine & Trim(oPayment.VoucherNo) & sSeparator
                                            '6. Last Update Time;
                                            sLine = sLine & Trim(oPayment.ExtraD5) & sSeparator
                                            '7. Last Update Date;
                                            sTmp = oPayment.DATE_Payment
                                            'sLine = sLine & Mid(sTmp, 5, 2) & Mid(sTmp, 7, 2) & Left(sTmp, 4) & sSeparator ' MMDDYYYY
                                            ' XOKNET 25.08.2015
                                            sLine = sLine & Mid(sTmp, 5, 2) & "/" & Mid(sTmp, 7, 2) & "/" & Left(sTmp, 4) & sSeparator ' MM/DD/YYYY
                                            '8. Batch Number;
                                            sLine = sLine & Trim(oBatch.Batch_ID) & sSeparator
                                            '9. Check Amount;
                                            sLine = sLine & LTrim(Str(oPayment.MON_InvoiceAmount / 100)) & sSeparator
                                            '10. Item Number;
                                            If oPayment.DnBNORTBIPayType = "LBX" Then
                                                sLine = sLine & Trim(oPayment.REF_Bank1) & sSeparator
                                            Else
                                                'sLine = sLine & Trim(oPayment.ExtraD3) & sSeparator
                                                ' XOKNET 05.06.2015
                                                ' Changed for ACH to show ACH Trace Number from Record 6 position 80-94
                                                sLine = sLine & Trim(oPayment.REF_Bank1) & sSeparator
                                            End If
                                            '11. Remitter Name;
                                            sLine = sLine & Trim(oPayment.E_Name) & sSeparator
                                            '12. Invoice Number;
                                            If Not EmptyString(oInvoice.InvoiceNo) Then
                                                sLine = sLine & Trim(oInvoice.InvoiceNo) & sSeparator
                                            Else
                                                sTmp = ""
                                                ' structured text with Qualifier=3
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier = 3 Then
                                                        sTmp = sTmp & oFreetext.Text
                                                    End If
                                                Next oFreetext
                                                ' remove ; from freetext
                                                sTmp = Replace(sTmp, ";", "")
                                                sLine = sLine & sTmp & sSeparator
                                            End If
                                            '13. Invoice Amount
                                            sLine = sLine & LTrim(Str(oInvoice.MON_TransferredAmount / 100)) & sSeparator

                                            ' XOKNET 20.08.2015 added two new fields
                                            If oPayment.DnBNORTBIPayType = "LBX" Then
                                                ' Lockbox
                                                ' 14. Check R/T
                                                sLine = sLine & Trim(oPayment.BANK_BranchNo) & sSeparator
                                                '15. Check DDA Acct
                                                sLine = sLine & Trim(oPayment.REF_Own)
                                            Else
                                                ' ACH
                                                ' 14. CompanyID (pos 41 - 50 in record 5)
                                                sLine = sLine & Trim(oPayment.ExtraD1) & sSeparator
                                                ' 15. DFI Accountno ( pos 80 - 87 in record 5)
                                                sLine = sLine & Trim(oPayment.BANK_BranchNo)
                                            End If

                                            oFile.WriteLine(sLine)
                                        End If
                                    Next oInvoice
                                    oPayment.Exported = True

                                End If

                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: Write_Archer_US_CSV" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Write_Archer_US_CSV = True

    End Function

End Module
