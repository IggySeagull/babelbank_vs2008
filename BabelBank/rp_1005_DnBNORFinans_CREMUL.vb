Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_1005_DnBNORFinans_CREMUL

    Private oBabelFiles As BabelFiles
    ' Declare all objects to be used in actual report:
    Dim oBabelFile As BabelFile
    Dim oBatch As Batch
    Dim oPayment As Payment
    Dim oInvoice As Invoice
    Dim cTotalAmount As Double
    Dim cInvoiceTotal As Double
    Dim nNoOfPayments As Double

    Dim sSpecial As String
    Dim sReportName As String
    Dim sClientNumber As String
    Dim bEmptyReport As Boolean = False
    Dim bUseLongDate As Boolean
    Dim bReportFromDatabase As Boolean
    Dim iBreakLevel As Integer
    Dim sReportNumber As String
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean

    'Dim bInsertedBlankLine As Boolean
    Private Sub rp_1005_DnBNORFinans_CREMUL_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize

        'Declare all fields to be used in actual report:
        Fields.Add("BreakField")
        If Not bReportFromDatabase Then
            Fields.Add("Filename")
            Fields.Add("DATE_Book")
            Fields.Add("AmountStatement")
            Fields.Add("Client")
            Fields.Add("I_Account")
            Fields.Add("AccountRef")
            Fields.Add("E_Account")
            Fields.Add("Unique_ID")
            Fields.Add("REF_Bank1")
            Fields.Add("REF_Bank2")
            Fields.Add("DATE_Value")
            Fields.Add("E_Name")
            Fields.Add("MON_InvoiceCurrency")
            Fields.Add("MON_InvoiceAmount")
            Fields.Add("NoOfPayments")
            Fields.Add("TotalAmount")
        End If

    End Sub
    Private Sub rp_1005_DnBNORFinans_CREMUL_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        ' Counters for items in the different Babel collections
        Static iBabelFiles As Integer
        Static iLastUsedBabelFilesItem As Integer
        Static iBatches As Integer
        Static iPayments As Integer
        Static iInvoices As Integer
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String
        Static bBabelComment As Boolean   ' Freetext with qualifier > 2 have been displayed for this invoice

        Dim oFileSetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oAccount As vbBabel.Account
        Dim sClientNo As String
        Dim sBranch As String
        Dim bContinue As Boolean
        Dim bFoundClient As Boolean
        Dim eof As Boolean

        eof = False
        iPayments = iPayments + 1

        If iBabelFiles = 0 Then
            iBabelFiles = 1
            iBatches = 1
            iPayments = 1
            iInvoices = 1
            xCreditAccount = "xxxx"
            xDate = "x"
        End If

        ' Spin through collections to find next suitbable record before any other
        ' proecessing is done
        ' Position to correct items in collections
        Do Until iBabelFiles > oBabelFiles.Count
            eArgs.EOF = False
            ' Try to set as few times as possible
            If oBabelFile Is Nothing Then
                oBabelFile = oBabelFiles(iBabelFiles)
                iLastUsedBabelFilesItem = iBabelFiles
            Else
                'If iBabelFiles <> oBabelFile.Index Then
                If iLastUsedBabelFilesItem <> iBabelFiles Then
                    iBatches = 1
                    iPayments = 1
                    iInvoices = 1
                    ' Added 19.05.2004
                    xCreditAccount = "xxxx"
                    xDate = "x"

                    oBabelFile = oBabelFiles(iBabelFiles)
                    iLastUsedBabelFilesItem = iBabelFiles  'oBabelFile.Index
                End If
            End If

            Do Until iBatches > oBabelFile.Batches.Count
                oBatch = oBabelFile.Batches(iBatches)
                ' 18.09.2019 - flyttet lenger ned Me.lblBatchRefBank.Text = "Kontoref: " & oBatch.REF_Bank

                ' Find the next payment in this batch which is original
                Do Until iPayments > oBatch.Payments.Count
                    oPayment = oBatch.Payments.Item(iPayments)
                    ' Show ONLY OCR-payments (510)
                    'Changed by Kjell 26.04.2005 because there are more sorts of OCR's
                    'If oPayment.PayCode <> "510" Then
                    'If Val(oPayment.PayCode) < 510 Or Val(oPayment.PayCode) > 517 Then

                    'Normal situation
                    'iPayments = iPayments + 1
                    'iInvoices = oPayment.Invoices.Count + 1 ' force another tour in loop
                    'bContinue = False
                    'Else
                    bContinue = True
                    iInvoices = 1
                    'End If

                    bFoundClient = False
                    If Not EmptyString(sClientNumber) Then
                        If oPayment.VB_ClientNo = sClientNumber Then
                            bFoundClient = True
                        End If
                    Else
                        bFoundClient = True
                    End If

                    ' Check if it is a clientseparated report, and if so, correct client
                    If bFoundClient Then

                        ' Postiton to a, final invoice
                        ' Find the next invoice in this payment which is original;
                        Do Until iInvoices > oPayment.Invoices.Count
                            oInvoice = oPayment.Invoices.Item(iInvoices)

                            If oInvoice.MATCH_Original Then
                                bContinue = True
                                Exit Do
                            End If
                            iInvoices = iInvoices + 1
                        Loop 'iInvoices
                    Else
                        bContinue = False
                        iPayments = iPayments + 1
                        iInvoices = 1
                    End If 'If frmViewer.CorrectReportClient(oPayment) Then

                    If bContinue Then
                        Exit Do
                    End If

                Loop 'iPayments
                If bContinue Then
                    Exit Do
                End If
                iBatches = iBatches + 1
                iPayments = 1
            Loop ' iBatches
            If bContinue Then
                Exit Do
            End If
            iBabelFiles = iBabelFiles + 1
            iBatches = 1
        Loop ' iBabelFiles

        If iBabelFiles > oBabelFiles.Count Then
            eArgs.EOF = True
            iBabelFiles = 0 ' to reset statics next time !

            Exit Sub
        End If

        '    If oBabelFile.DATE_Production <> "19900101" Then
        '        Fields("fldProdDate").Value = CStr(StringToDate(oBabelFile.DATE_Production))
        '    ElseIf oBabelFile.Batches(iBatches).DATE_Production <> "19900101" Then
        '        Fields("fldProdDate").Value = CStr(StringToDate(oBabelFile.Batches(iBatches).DATE_Production))
        '    Else
        '        ' Show todays date
        'Fields("fldProdDate").Value = CStr(Date)
        '    End If


        ' grBatch data
        '-------------
        If Not EmptyString(oBabelFile.FilenameIn) Then
            If oBabelFile.FilenameIn.Length > 30 Then
                Me.Fields("Filename").Value = "..." & oBabelFile.FilenameIn.Substring(oBabelFile.FilenameIn.Length - 30)
            Else
                Me.Fields("Filename").Value = oBabelFile.FilenameIn
            End If
        End If
        'Me.Fields("fldFileIndex").Value = iBabelFiles '


        ' Footer data:
        ' ------------
        'Fields("fldRunDate").Value = Now

        If oPayment.I_Account <> xCreditAccount Then
            'xAccount = oPayment.I_Account
            sClientNo = ""
            sBranch = ""
            For Each oFileSetup In oBabelFile.VB_Profile.FileSetups
                For Each oClient In oFileSetup.Clients
                    For Each oAccount In oClient.Accounts
                        If oPayment.I_Account = oAccount.Account Then
                            sClientNo = oClient.ClientNo
                            sBranch = oClient.Name
                            Exit For
                        End If
                    Next
                    If sClientNo <> vbNullString Then
                        Exit For
                    End If
                Next
            Next
            If sClientNo = vbNullString And sBranch = vbNullString Then
                lblClient.Visible = False
            Else
                lblClient.Visible = True
                Fields("Client").Value = sClientNo & " " & sBranch
            End If
        End If

        Me.Fields("I_Account").Value = oPayment.I_Account

        Me.Fields("AccountRef").Value = "Kontoref: " & iBatches.ToString & oPayment.REF_Own
        Me.Fields("DATE_Book").Value = CStr(StringToDate(oPayment.DATE_Payment))

        ' Create a "BreakField", fldBreak
        Me.Fields("BreakField").Value = Str(iBabelFiles) & Fields("DATE_Book").Value & Fields("I_Account").Value

        If oPayment.I_Account <> xCreditAccount Or oPayment.DATE_Payment <> xDate Then
            xCreditAccount = oPayment.I_Account
            xDate = oPayment.DATE_Payment
            PreGroupTotals(oPayment.I_Account, xDate)
            Me.Fields("TotalAmount").Value = cTotalAmount / 100
            Me.Fields("AmountStatement").Value = cTotalAmount / 100
            Me.Fields("NoOfPayments").Value = nNoOfPayments
        End If

        bBabelComment = False  ' have not, yet, displayed comments(freetexts) added by BabelBank

        'Fields("fldPaymentIndex").Value = Val(Trim(Str(iBatches)) & Trim(Str(iPayments))) ' force a brake here -iPayments ' Group break

        'Details data
        '----------------
        Me.Fields("E_Account").Value = Trim(oPayment.E_Account)
        If IsOCR(oPayment.PayCode) Then
            ' Show KID
            Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
        Else
            Me.Fields("Unique_ID").Value = vbNullString
        End If
        Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
        Me.Fields("REF_Bank2").Value = oPayment.REF_Bank2
        ' 18.09.2019 - moved this one here to have correct bankref in footer
        ' Me.lblBatchRefBank.Text = "Kontoref: " & oBatch.REF_Bank

        Me.Fields("Date_Value").Value = Format(StringToDate(oPayment.DATE_Value), "Short Date")

        Me.Fields("E_Name").Value = Left$(oPayment.E_Name, 30)

        'Fields("fldNameOrRef").Value = Str(iBatches) & oPayment.REF_Own

        If oPayment.MON_InvoiceAmount <> 0 Then
            Me.Fields("MON_InvoiceAmount").Value = oPayment.MON_InvoiceAmount / 100
        Else
            Me.Fields("MON_InvoiceAmount").Value = oPayment.MON_TransferredAmount / 100
        End If

        Me.Fields("MON_InvoiceCurrency").Value = oPayment.MON_InvoiceCurrency
        'Fields("fldValDate").Value = CStr(StringToDate(oPayment.DATE_Value))

    End Sub

    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format


    End Sub

    Private Sub rp_1005_DnBNORFinans_CREMUL_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        '----------------
        ' Set breaklevel;
        '----------------
        ' 1, BREAK_ON_ACCOUNT : Break on productiondate + CreditAccount
        ' 2, BREAK_ON_BATCH   : Break on productiondate + CreditAccount + iBatches
        ' 3, BREAK_ON_PAYMENT : Break on productiondate + CreditAccount + iBatches + iPayments
        ' Default;
        'iBreakOn = BREAK_ON_PAYMENT 'BAMA
        'iBreakOn = BREAK_ON_BATCH   ' LHL
        ' If set, fetch breakelevel from frmViewer
        'If frmViewer.iBreakLevel > 0 Then
        '    iBreakOn = frmViewer.iBreakLevel
        'End If


        ' Setup of pagesize, etc
        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Landscape

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40040)   ' Payments
        Else
            Me.lblrptHeader.Text = sReportName
        End If

        ''31.07.2008 - KOKO - Added frmViewer before iBreakLevel
        'If frmViewer.iBreakLevel = BREAK_ON_PAYMENT Then
        '    ' Test new page pr payment
        '    Me.Detail.NewPage = ddNPBefore
        'End If

        ' Localize capitions for headings, etc
        Me.lblFilename.Text = LRS(40001)
        Me.lblDate_Book.Text = LRS(40044)
        Me.lblAmountStatement.Text = LRS(40004)
        Me.lblClient.Text = LRS(40030)  'Client
        Me.lblI_Account.Text = LRS(40020)
        'lblPage.Caption = LRS(40009)
        Me.lblNoOfPayments.Text = LRS(40014)
        Me.lblBabelfooterAmount.Text = LRS(40017)

        Me.lblREF_Bank1.Text = "Bank_Ref1"
        Me.lblREF_Bank2.Text = "Bank_Ref2"
        Me.lblDateValue.Text = LRS(40003)
        Me.lblKID.Text = LRS(40046)
        Me.lblE_Name.Text = LRS(40071)
        Me.lblPcur.Text = LRS(40007)
        Me.lblAmount.Text = LRS(40021)
        Me.lblBatchfooterAmount.Text = LRS(40047)
        Me.lblE_Account.Text = LRS(40048)

        'If frmViewer.iTotalLevel = 1 Then
        '    ' Hide totals
        '    Me.txtFileSumAmount.Visible = False
        '    Me.lblFileSum.Visible = False
        'End If

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Book.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:dd.MM.yyyy HH:mm:ss}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Book.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If


    End Sub
    Private Sub rp_1005_DnBNORFinans_CREMUL_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportEnd
        'If Not oBabelFiles Is Nothing Then
        '    Set oBabelFiles = Nothing
        'End If
        'If Not oBabelFile Is Nothing Then
        '    Set oBabelFile = Nothing
        'End If
        If Not oBatch Is Nothing Then
            oBatch = Nothing
        End If
        If Not oPayment Is Nothing Then
            oPayment = Nothing
        End If
        If Not oInvoice Is Nothing Then
            oInvoice = Nothing
        End If
        'frmViewer.SetBabelFilesToNothing
        cTotalAmount = 0
        nNoOfPayments = 0

    End Sub
    Private Sub PreGroupTotals(ByVal sAccount As String, ByVal sDate As String)
        Dim oTempBatch As Batch
        Dim oTempPayment As Payment

        ' totals to show in group header
        cTotalAmount = 0
        nNoOfPayments = 0

        For Each oTempBatch In oBabelFile.Batches
            For Each oTempPayment In oTempBatch.Payments
                If oTempPayment.I_Account = sAccount And oTempPayment.DATE_Payment = sDate Then

                    'Changed by Kjell 26.04.2005 because there are more sorts of OCR's
                    'If oPayment.PayCode = "510" Then
                    'If Val(oPayment.PayCode) > 509 Or Val(oPayment.PayCode) < 518 Then
                    'If IsOCR(oTempPayment.PayCode) Then
                    nNoOfPayments = nNoOfPayments + 1
                    If oTempPayment.MON_TransferredAmount <> 0 Then
                        cTotalAmount = cTotalAmount + oTempPayment.MON_TransferredAmount
                    Else
                        cTotalAmount = cTotalAmount + oTempPayment.MON_InvoiceAmount
                    End If
                    'End If
                End If
            Next
        Next

    End Sub

    Private Sub rp_1005_DnBNORFinans_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If
    End Sub
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property

    Private Sub grBabelFileFooter_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grBabelFileFooter.Format

    End Sub
End Class
