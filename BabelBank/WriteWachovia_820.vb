﻿Option Explicit On
Module WriteWachovia_820

    ' XokNET 20.12.2011 - New function, and changes after that

    ' Wachovia 820 receivables (Nacha)
    ' Used by:
    ' - Actavis, US (a DNB Client)
    '---------------------------------------------------------------------
    Dim sLine As String ' en output-linje
    Dim BatchSumAmount As Double, BatchNoRecords As Double
    Dim nFileSumAmount As Double
    Dim dFileFirstDate As Date
    Dim iBatchNumber As Integer

    Dim sSegmentSeparator As String
    Dim sIncomingSegmentSeparator As String
    Dim iTransactionSetControlnumber As Integer
    Dim iNoOfSegmentsInST As Integer
    Dim iNoOfTransactions As Integer
    Dim iNoOfFunctionalGroups As Integer
    Dim iGroupControlNumber As Integer
    Dim sSpecial As String
    Public Function WriteWachovia_820_Receivables(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sCompanyNo As String, ByVal nSequenceNumberStart As Double, ByVal nSequenceNumberEnd As Double) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim i As Integer
        Dim bFileStartWritten As Boolean
        Dim sFreetext As String
        Dim sOriginalFreetext As String
        Dim bStreaming As Boolean ' this can be set in the profile, TODO later
        Dim sDate As String

        Try

            iTransactionSetControlnumber = 0
            iGroupControlNumber = 0
            iNoOfTransactions = 0
            ' -----------------
            ' Streaming or not?
            ' -----------------
            bStreaming = True  'False
            ' this can be set in the profile, TODO later
            If bStreaming Then
                ' If Streaming, then all output is one line. Then segments are separated by ~
                sSegmentSeparator = "~"
            Else
                ' Non streaming has each segment in one line, then CRLF
                sSegmentSeparator = vbCr + vbLf
            End If
            sIncomingSegmentSeparator = "\" ' from imported file

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            iBatchNumber = 0

            For Each oBabel In oBabelFiles
                If EmptyString(sCompanyNo) Then
                    sCompanyNo = oBabel.VB_Profile.CompanyNo
                End If
                nSequenceNumberStart = oBabel.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1

                sSpecial = UCase(oBabel.Special)
                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments

                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled, or previously exported
                            If oPayment.Cancel = False And oPayment.Exported = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next


                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' ---------------------------
                        ' File start
                        ' ---------------------------
                        ' write only once!
                        ' add 1 to sequence, used in 'ISA13   I12 Interchange Control Number
                        nSequenceNumberStart = nSequenceNumberStart + 1
                        sLine = Write_ISA(oBabel, nSequenceNumberStart)
                        oFile.Write(sLine)

                        bFileStartWritten = True
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And oPayment.Exported = False Then

                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then
                            ' GS for each new batch ?
                            sLine = Write_GS(oBabel, nSequenceNumberStart) 'XNET
                            oFile.Write(sLine)

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False

                                'Have to go through the payment-object to see if we have objects that shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False And oPayment.Exported = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then

                                    '-----------------------------------------------------------
                                    sLine = Write_ST(nSequenceNumberStart)
                                    oFile.Write(sLine)

                                    ' Different attitudes for CTX and CCD.
                                    ' If CTX, then just "dump" the segment as they are in oFreetexts
                                    If oBatch.Version = "CTX" Then
                                        ' from here we have all imported text in oFreetext
                                        ' each segment, from BPR and to end has it's own oFreetext
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If Left$(oFreeText.Text, 4) = sIncomingSegmentSeparator & "BPR" Then
                                                    iNoOfTransactions = iNoOfTransactions + 1
                                                End If
                                                sLine = Write_820Segment(oFreeText.Text)
                                                oFile.Write(sLine)
                                            Next
                                        Next

                                    Else
                                        ' But for CCD we must construct the segments

                                        sLine = Write_BPR(oBatch, oPayment)
                                        oFile.Write(sLine)
                                        sLine = Write_TRN(oPayment)
                                        oFile.Write(sLine)

                                        ' ------------------------------------------------
                                        ' Date / time
                                        ' Set both bookdate and valuedate ????
                                        ' ------------------------------------------------
                                        sLine = Write_DTM(oPayment, "bookdate")
                                        oFile.Write(sLine)
                                        sLine = Write_DTM(oPayment, "valuedate")
                                        oFile.Write(sLine)
                                        ' ------------------------------------------------
                                        ' Name/address -
                                        ' Only Name, do not use the other address-segments
                                        ' ------------------------------------------------
                                        sLine = Write_N1(oPayment, "PE")     ' Receiver
                                        oFile.Write(sLine)
                                        sLine = Write_N1(oPayment, "PR")     ' Payer
                                        oFile.Write(sLine)
                                        '-----------------------------------------------------------
                                    End If

                                    ' ------------------------
                                    ' End of payment
                                    ' ------------------------
                                    sLine = Write_SE(nSequenceNumberStart)
                                    oFile.Write(sLine)

                                    oPayment.Exported = True
                                End If
                            Next ' payment

                            ' ------------------------
                            ' End of batch
                            ' ------------------------
                            sLine = Write_GE()
                            oFile.Write(sLine)

                        End If
                        sDate = oBatch.DATE_Production
                    Next 'batch
                End If

                nSequenceNumberEnd = nSequenceNumberStart ' update last sequence
                ' Save last used sequence
                oBabel.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNumberEnd - 1
                oBabel.VB_Profile.Status = 2
                oBabel.VB_Profile.FileSetups(iFormat_ID).Status = 1

            Next 'Babelfile

            ' ------------------------
            ' End of file
            ' ------------------------
            sLine = Write_IEA(nSequenceNumberEnd)
            oFile.Write(sLine)

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteWachovia_820_Receivables" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteWachovia_820_Receivables = True

    End Function
    Private Function Write_ISA(ByVal oBabelFile As vbBabel.BabelFile, ByVal nSequenceNumberStart As Double) As String
        Dim sLine As String
        ' XNET 20.03.2012 - several changes, take whole function
        iNoOfFunctionalGroups = 0

        sLine = "ISA|00|"           'Hardcode 00 for ISA01   I01 Authorization Information Qualifier
        sLine = sLine & Space(10) & "|" 'ISA02   I02 Authorization Information, spaces so far
        sLine = sLine & "00|"       'ISA03   I03 Security Information Qualifier, hardcoded 00
        sLine = sLine & Space(10) & "|" 'ISA04   I04 Security Information, spaces so far
        sLine = sLine & "17|"       'ISA05   I05 Interchange ID Qualifier, hardcoded 01
        sLine = sLine & "02600005610|"      'ISA06   I06 Interchange Sender ID - 048463400P
        sLine = sLine & "01|"       'ISA07   I05 Interchange ID Qualifier, hardcode 01
        sLine = sLine & PadRight(Trim$(oBabelFile.VB_Profile.AdditionalNo), 15, " ") & "|"  'ISA08   I07 Interchange Receiver ID
        'sLine = sLine & Right(oBabelFile.DATE_Production, 6) & "|"  'ISA09   I08 Interchange Date (YYMMDD)
        sLine = sLine & Format(Now, "YYMMDD") & "|" 'ISA09   I08 Interchange Date (YYMMDD)
        sLine = sLine & Format(Now, "hhnn") & "|" 'ISA10   I09 Interchange Time
        sLine = sLine & "U|"        'ISA11   I10 Interchange Control Standards Identifier, hardcoded U
        sLine = sLine & "00401|"    'ISA12   I11 Interchange Control Version Number,harcoded 00401
        sLine = sLine & LTrim(Str(nSequenceNumberStart)) & "|"  'ISA13   I12 Interchange Control Number
        sLine = sLine & "0|"        'ISA14   I13 Acknowledgment Requested, hardcoded 0
        sLine = sLine & "P|"        'ISA15   I14 Usage Indicator P = Production
        sLine = sLine & ">|"        'ISA16   I15 Component Element Separator


        sLine = sLine & sSegmentSeparator
        Write_ISA = sLine

    End Function
    Private Function Write_GS(ByVal oBabelFile As vbBabel.BabelFile, ByVal nSequenceNumberStart As Double) As String
        Dim sLine As String
        ' XNET 20.03.2012 - several changes, take whole function
        iGroupControlNumber = iGroupControlNumber + 1

        iNoOfFunctionalGroups = iNoOfFunctionalGroups + 1
        sLine = "GS|RA|"            'GS01    479 Functional Identifier Code, hardcoded RA
        sLine = sLine & "026006510|"      'GS02    142 Application Sender's Code
        sLine = sLine & PadRight(Trim$(oBabelFile.VB_Profile.AdditionalNo), 15, " ") & "|"  'GS03    124 Application Receiver's Code
        sLine = sLine & Format(Now, "YYMMDD") & "|" 'GS04    373 Date (CCYYMMDD)
        sLine = sLine & Format(Now, "hhnn") & "|"  'GS05    337 Time
        sLine = sLine & LTrim(Str(nSequenceNumberStart)) & "|" 'GS06    28  Group Control Number
        sLine = sLine & "X|"        'GS07    455 Responsible Agency Code, hardcode X
        sLine = sLine & "004010"     'GS08    480 Version / Release / Industry Identifier Code, hardcoded 00401

        sLine = sLine & sSegmentSeparator
        Write_GS = sLine

    End Function
    Private Function Write_ST(ByVal nSequenceNumberStart As Double) As String
        ' Transaction Set Header
        ' ----------------------
        Dim sLine As String
        iTransactionSetControlnumber = iTransactionSetControlnumber + 1
        iNoOfSegmentsInST = 1

        sLine = "ST|820|"
        sLine = sLine & Right$("0000" & LTrim(Str(nSequenceNumberStart)), 4) & Right$("00000" & LTrim(Str(iTransactionSetControlnumber)), 5)
        sLine = sLine & sSegmentSeparator
        Write_ST = sLine

    End Function
    Private Function Write_820Segment(ByVal sText As String) As String
        ' General function to write a segment in the 820-file
        Dim sLine As String
        Dim sIncomingElementSeparator As String
        Dim sExportElementSeparator As String

        sIncomingElementSeparator = "*"
        sExportElementSeparator = "|"
        iNoOfSegmentsInST = iNoOfSegmentsInST + 1

        ' Incoming segment like
        ' \BPR*X*20065.9*C*ACH*CTX*01*031100209*DA*
        ' Export segment like
        ' BPR|X|15904.32|C|ACH|CCD|01|122000030|||3562056614||01|031000503|DA|2000041710470|20111107

        ' remove segmentsep from incoming;
        sText = Replace(sText, sIncomingSegmentSeparator, "")
        sLine = Replace(sText, sIncomingElementSeparator, sExportElementSeparator)

        sLine = sLine & sSegmentSeparator
        Write_820Segment = sLine
    End Function
    Private Function Write_GE() As String
        ' Functional Group Trailer
        ' ------------------------
        Dim sLine As String
        iNoOfSegmentsInST = 1

        sLine = "GE|"
        sLine = sLine & LTrim(Str(iNoOfTransactions)) & "|"   'GE01    97  Number of Transaction Sets Included
        sLine = sLine & LTrim(Str(iGroupControlNumber)) & "|" 'GE02    28  Group Control Number
        sLine = sLine & sSegmentSeparator
        Write_GE = sLine

    End Function
    Private Function Write_SE(ByVal nSequenceNumberStart As Double) As String
        ' Transaction Set Trailer
        ' -----------------------
        Dim sLine As String

        'XNET 23.03.2012 - added next line
        iNoOfSegmentsInST = iNoOfSegmentsInST + 1
        sLine = "SE|"
        sLine = sLine & LTrim(Str(iNoOfSegmentsInST)) & "|"  'SE01    96  Number of Included Segments
        sLine = sLine & Right$("0000" & LTrim(Str(nSequenceNumberStart)), 4) & Right$("00000" & LTrim(Str(iTransactionSetControlnumber)), 5)  'SE02    329 Transaction Set Control Number

        sLine = sLine & sSegmentSeparator
        Write_SE = sLine

    End Function
    Private Function Write_IEA(ByVal nSequenceNumberStart As Double) As String
        ' Interchange Control Trailer
        ' -----------------------
        Dim sLine As String

        sLine = "IEA|"
        sLine = sLine & LTrim(Str(iNoOfFunctionalGroups)) & "|" 'IEA01   I16 Number of Included Functional Groups
        sLine = sLine & LTrim(Str(nSequenceNumberStart)) & "|"   'IEA02   I12 Interchange Control Number  M   N0 9/9

        sLine = sLine & sSegmentSeparator
        Write_IEA = sLine

    End Function
    Private Function Write_BPR(ByVal oBatch As vbBabel.Batch, ByVal oPayment As vbBabel.Payment) As String
        ' Beginning Segment for Payment Order/Remittance Advice
        ' -----------------------------------------------------
        Dim sLine As String
        iNoOfSegmentsInST = iNoOfSegmentsInST + 1

        sLine = "BPR|"
        ' Dependant upon RMR or not
        If oPayment.ExtraD4 = "RMR" Then
            sLine = sLine & "C|"        'BPR01   305 Transaction Handling Code
        Else
            sLine = sLine & "X|"
        End If
        sLine = sLine & Replace(Str(oPayment.MON_InvoiceAmount / 100), ",", ".") & "|" 'BPR02   782 Monetary Amount
        sLine = sLine & "C|"        'BPR03   478 Credit/Debit Flag Code, assume credits only
        sLine = sLine & "ACH|"      'BPR04   591 Payment Method Code, assume ACH
        sLine = sLine & oBatch.Version & "|"    'BPR05   812 Payment Format Code (CCD, CTX, PPD)
        sLine = sLine & "01|"       'BPR06   506 (DFI) ID Number Qualifier
        sLine = sLine & oPayment.BANK_BranchNo & "|" 'BPR07   507 (DFI) Identification Number
        If Not EmptyString(oPayment.E_Account) Then
            sLine = sLine & "DA|"   'BPR08   569 Account Number Qualifier
            sLine = sLine & oPayment.E_Account & "|" 'BPR09   508 Account Number
        Else
            ' unkonown payers account
            sLine = sLine & "||"
        End If
        sLine = sLine & oPayment.ExtraD1 & "|"      'BPR10   509 Originating Company Identifier
        sLine = sLine & "|"         'BPR11   not in use
        sLine = sLine & "01|"       'BPR12   506 (DFI) ID Number Qualifier
        sLine = sLine & "123456789|" 'BPR13   507 (DFI) Identification Number, DNB Routing no
        sLine = sLine & "DA|"        'BPR14   569 Account Number Qualifier, DA or SG
        sLine = sLine & oPayment.I_Account & "|"    'BPR15   508 Account Number (receiver)
        sLine = sLine & oPayment.DATE_Payment 'BPR16   373 Date

        sLine = sLine & sSegmentSeparator
        Write_BPR = sLine

    End Function
    Private Function Write_TRN(ByVal oPayment As vbBabel.Payment) As String
        ' Trace
        ' -----
        Dim sLine As String
        iNoOfSegmentsInST = iNoOfSegmentsInST + 1

        sLine = "TRN|1|"
        sLine = sLine & oPayment.REF_Bank1    'TRN02   127 Reference Identification (use Tracenumber, 80,15 from record 6 in ACH)

        sLine = sLine & sSegmentSeparator
        Write_TRN = sLine

    End Function
    Private Function Write_N1(ByVal oPayment As vbBabel.Payment, ByVal sEntityQualifier As String) As String
        ' Name
        ' ----
        Dim sLine As String
        iNoOfSegmentsInST = iNoOfSegmentsInST + 1
        ' TODO - we may need more elements here

        sLine = "N1|"
        If sEntityQualifier = "PE" Then
            ' Payee, receiver
            sLine = sLine & "PE|"       'N101    98  Entity Identifier Code
            If Not EmptyString(oPayment.I_Name) Then
                sLine = sLine & oPayment.I_Name   'N102    93  Name
            Else
                sLine = sLine & oPayment.VB_Profile.CompanyName   'N102    93  Name
            End If
        Else
            ' Payer
            sLine = sLine & "PR|"       'N101    98  Entity Identifier Code
            sLine = sLine & oPayment.E_Name   'N102    93  Name
        End If

        sLine = sLine & sSegmentSeparator
        Write_N1 = sLine

    End Function
    Private Function Write_DTM(ByVal oPayment As vbBabel.Payment, ByVal sDatetype As String) As String
        ' Date Time -  can be used at different levels/loops
        ' --------------------------------------------------
        Dim sLine As String
        Dim sTmp As String
        iNoOfSegmentsInST = iNoOfSegmentsInST + 1

        sLine = "DTM|"
        If sDatetype = "bookdate" Then
            ' ' is 044 OK ???????????
            sLine = sLine & "044|"  'DTM01   374 Date/Time Qualifier
            sLine = sLine & oPayment.DATE_Payment  'DTM02   373 Date
        Else
            ' ' is 007 OK ???????????
            sLine = sLine & "007|"  'DTM01   374 Date/Time Qualifier
            sLine = sLine & oPayment.DATE_Value  'DTM02   373 Date
        End If

        sLine = sLine & sSegmentSeparator
        Write_DTM = sLine

    End Function


End Module
