<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_505_Cremul_Control
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_505_Cremul_Control))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtMatched = New DataDynamics.ActiveReports.TextBox
        Me.txtDiff = New DataDynamics.ActiveReports.TextBox
        Me.txtUnMatched = New DataDynamics.ActiveReports.TextBox
        Me.txtRef = New DataDynamics.ActiveReports.TextBox
        Me.txtAccount = New DataDynamics.ActiveReports.TextBox
        Me.txtName = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.linePageHeader = New DataDynamics.ActiveReports.Line
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.lblMatched = New DataDynamics.ActiveReports.Label
        Me.lblUnMatched = New DataDynamics.ActiveReports.Label
        Me.lblAccount = New DataDynamics.ActiveReports.Label
        Me.lblRef = New DataDynamics.ActiveReports.Label
        Me.lblDiff = New DataDynamics.ActiveReports.Label
        Me.lblName = New DataDynamics.ActiveReports.Label
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblBatchfooterNoofPayments = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterNoofPayments = New DataDynamics.ActiveReports.TextBox
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        CType(Me.txtMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblRef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDiff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtMatched, Me.txtDiff, Me.txtUnMatched, Me.txtRef, Me.txtAccount, Me.txtName})
        Me.Detail.Height = 0.1979167!
        Me.Detail.Name = "Detail"
        '
        'txtMatched
        '
        Me.txtMatched.CanGrow = False
        Me.txtMatched.DataField = "MATCHEDAmount"
        Me.txtMatched.Height = 0.15!
        Me.txtMatched.Left = 2.2!
        Me.txtMatched.Name = "txtMatched"
        Me.txtMatched.OutputFormat = resources.GetString("txtMatched.OutputFormat")
        Me.txtMatched.Style = "font-size: 8pt; text-align: right"
        Me.txtMatched.Text = "txtMatched"
        Me.txtMatched.Top = 0.0!
        Me.txtMatched.Width = 1.0!
        '
        'txtDiff
        '
        Me.txtDiff.CanGrow = False
        Me.txtDiff.Height = 0.15!
        Me.txtDiff.Left = 5.5!
        Me.txtDiff.Name = "txtDiff"
        Me.txtDiff.OutputFormat = resources.GetString("txtDiff.OutputFormat")
        Me.txtDiff.Style = "font-size: 8pt; text-align: right"
        Me.txtDiff.Text = "txtDiff"
        Me.txtDiff.Top = 0.0!
        Me.txtDiff.Width = 0.95!
        '
        'txtUnMatched
        '
        Me.txtUnMatched.CanGrow = False
        Me.txtUnMatched.DataField = "UNMATCHEDAmount"
        Me.txtUnMatched.Height = 0.15!
        Me.txtUnMatched.Left = 3.3!
        Me.txtUnMatched.Name = "txtUnMatched"
        Me.txtUnMatched.OutputFormat = resources.GetString("txtUnMatched.OutputFormat")
        Me.txtUnMatched.Style = "font-size: 8pt; text-align: right"
        Me.txtUnMatched.Text = "txtUnMatched"
        Me.txtUnMatched.Top = 0.0!
        Me.txtUnMatched.Width = 1.0!
        '
        'txtRef
        '
        Me.txtRef.CanGrow = False
        Me.txtRef.DataField = "REF_Bank"
        Me.txtRef.Height = 0.15!
        Me.txtRef.Left = 1.4!
        Me.txtRef.Name = "txtRef"
        Me.txtRef.Style = "font-size: 8pt"
        Me.txtRef.Text = "txtRef"
        Me.txtRef.Top = 0.0!
        Me.txtRef.Width = 0.8!
        '
        'txtAccount
        '
        Me.txtAccount.CanGrow = False
        Me.txtAccount.DataField = "PaymentAmount"
        Me.txtAccount.Height = 0.15!
        Me.txtAccount.Left = 4.4!
        Me.txtAccount.Name = "txtAccount"
        Me.txtAccount.OutputFormat = resources.GetString("txtAccount.OutputFormat")
        Me.txtAccount.Style = "font-size: 8pt; text-align: right"
        Me.txtAccount.Text = "txtAccount"
        Me.txtAccount.Top = 0.0!
        Me.txtAccount.Width = 1.0!
        '
        'txtName
        '
        Me.txtName.CanGrow = False
        Me.txtName.DataField = "Name"
        Me.txtName.Height = 0.15!
        Me.txtName.Left = 0.0!
        Me.txtName.Name = "txtName"
        Me.txtName.Style = "font-size: 8pt"
        Me.txtName.Text = "txtName"
        Me.txtName.Top = 0.0!
        Me.txtName.Width = 1.4!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.rptInfoPageHeaderDate, Me.linePageHeader})
        Me.PageHeader1.Height = 0.4479167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.3!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Matching Control"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'linePageHeader
        '
        Me.linePageHeader.Height = 0.0!
        Me.linePageHeader.Left = 0.0!
        Me.linePageHeader.LineWeight = 1.0!
        Me.linePageHeader.Name = "linePageHeader"
        Me.linePageHeader.Top = 0.375!
        Me.linePageHeader.Width = 6.5!
        Me.linePageHeader.X1 = 0.0!
        Me.linePageHeader.X2 = 6.5!
        Me.linePageHeader.Y1 = 0.375!
        Me.linePageHeader.Y2 = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.CanShrink = True
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBabelFileFooter2, Me.LineBabelFileFooter1, Me.txtTotalAmount, Me.lblTotalAmount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 4.9!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 1.6!
        Me.LineBabelFileFooter2.X1 = 4.9!
        Me.LineBabelFileFooter2.X2 = 6.5!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 4.9!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 1.6!
        Me.LineBabelFileFooter1.X1 = 4.9!
        Me.LineBabelFileFooter1.X2 = 6.5!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.Height = 0.15!
        Me.txtTotalAmount.Left = 5.45!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.0!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.15!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.95!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.5!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.txtI_Account, Me.txtDATE_Production, Me.txtClientName, Me.lblDate_Production, Me.lblClient, Me.lblI_Account, Me.lblMatched, Me.lblUnMatched, Me.lblAccount, Me.lblRef, Me.lblDiff, Me.lblName})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.8!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.45!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.05!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.3004167!
        Me.txtI_Account.Width = 1.9!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.125!
        Me.txtDATE_Production.Width = 1.563!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.1104167!
        Me.txtClientName.Width = 1.9!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.125!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.125!
        Me.lblDate_Production.Width = 1.313!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.125!
        Me.lblClient.Width = 1.0!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.3004167!
        Me.lblI_Account.Width = 1.0!
        '
        'lblMatched
        '
        Me.lblMatched.Height = 0.19!
        Me.lblMatched.HyperLink = Nothing
        Me.lblMatched.Left = 2.8!
        Me.lblMatched.Name = "lblMatched"
        Me.lblMatched.Style = ""
        Me.lblMatched.Text = "lblMatched"
        Me.lblMatched.Top = 0.55!
        Me.lblMatched.Width = 0.906!
        '
        'lblUnMatched
        '
        Me.lblUnMatched.Height = 0.19!
        Me.lblUnMatched.HyperLink = Nothing
        Me.lblUnMatched.Left = 3.7!
        Me.lblUnMatched.Name = "lblUnMatched"
        Me.lblUnMatched.Style = ""
        Me.lblUnMatched.Text = "lblUnMatched"
        Me.lblUnMatched.Top = 0.55!
        Me.lblUnMatched.Width = 0.906!
        '
        'lblAccount
        '
        Me.lblAccount.Height = 0.19!
        Me.lblAccount.HyperLink = Nothing
        Me.lblAccount.Left = 4.9!
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Style = ""
        Me.lblAccount.Text = "lblAccount"
        Me.lblAccount.Top = 0.55!
        Me.lblAccount.Width = 0.906!
        '
        'lblRef
        '
        Me.lblRef.Height = 0.19!
        Me.lblRef.HyperLink = Nothing
        Me.lblRef.Left = 1.4!
        Me.lblRef.Name = "lblRef"
        Me.lblRef.Style = ""
        Me.lblRef.Text = "lblRef"
        Me.lblRef.Top = 0.55!
        Me.lblRef.Width = 0.906!
        '
        'lblDiff
        '
        Me.lblDiff.Height = 0.19!
        Me.lblDiff.HyperLink = Nothing
        Me.lblDiff.Left = 5.8!
        Me.lblDiff.Name = "lblDiff"
        Me.lblDiff.Style = ""
        Me.lblDiff.Text = "lblDiff"
        Me.lblDiff.Top = 0.55!
        Me.lblDiff.Width = 0.7!
        '
        'lblName
        '
        Me.lblName.Height = 0.19!
        Me.lblName.HyperLink = Nothing
        Me.lblName.Left = 0.0!
        Me.lblName.Name = "lblName"
        Me.lblName.Style = ""
        Me.lblName.Text = "lblName"
        Me.lblName.Top = 0.55!
        Me.lblName.Width = 0.906!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblBatchfooterAmount, Me.txtBatchfooterAmount, Me.lblBatchfooterNoofPayments, Me.txtBatchfooterNoofPayments, Me.LineBatchFooter1})
        Me.grBatchFooter.Height = 0.25!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.15!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 4.95!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.0!
        Me.lblBatchfooterAmount.Width = 0.5!
        '
        'txtBatchfooterAmount
        '
        Me.txtBatchfooterAmount.DataField = "InvoiceAmount"
        Me.txtBatchfooterAmount.Height = 0.15!
        Me.txtBatchfooterAmount.Left = 5.45!
        Me.txtBatchfooterAmount.Name = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.OutputFormat = resources.GetString("txtBatchfooterAmount.OutputFormat")
        Me.txtBatchfooterAmount.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterAmount.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterAmount.Text = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.Top = 0.0!
        Me.txtBatchfooterAmount.Width = 1.0!
        '
        'lblBatchfooterNoofPayments
        '
        Me.lblBatchfooterNoofPayments.Height = 0.15!
        Me.lblBatchfooterNoofPayments.HyperLink = Nothing
        Me.lblBatchfooterNoofPayments.Left = 3.7!
        Me.lblBatchfooterNoofPayments.Name = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterNoofPayments.Text = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Top = 0.0!
        Me.lblBatchfooterNoofPayments.Width = 0.95!
        '
        'txtBatchfooterNoofPayments
        '
        Me.txtBatchfooterNoofPayments.DataField = "Invoice_ID"
        Me.txtBatchfooterNoofPayments.DistinctField = "Invoice_ID"
        Me.txtBatchfooterNoofPayments.Height = 0.15!
        Me.txtBatchfooterNoofPayments.Left = 4.65!
        Me.txtBatchfooterNoofPayments.Name = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterNoofPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.txtBatchfooterNoofPayments.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterNoofPayments.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterNoofPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterNoofPayments.Text = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Top = 0.0!
        Me.txtBatchfooterNoofPayments.Width = 0.25!
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 3.7!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.21!
        Me.LineBatchFooter1.Width = 2.8!
        Me.LineBatchFooter1.X1 = 3.7!
        Me.LineBatchFooter1.X2 = 6.5!
        Me.LineBatchFooter1.Y1 = 0.21!
        Me.LineBatchFooter1.Y2 = 0.21!
        '
        'rp_505_Cremul_Control
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.500501!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.txtMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAccount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblRef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDiff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtMatched As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDiff As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnMatched As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtRef As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAccount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents linePageHeader As DataDynamics.ActiveReports.Line
    Friend WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBatchfooterAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblBatchfooterNoofPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBatchfooterNoofPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Private WithEvents lblRef As DataDynamics.ActiveReports.Label
    Private WithEvents lblMatched As DataDynamics.ActiveReports.Label
    Private WithEvents lblUnMatched As DataDynamics.ActiveReports.Label
    Private WithEvents lblAccount As DataDynamics.ActiveReports.Label
    Private WithEvents lblDiff As DataDynamics.ActiveReports.Label
    Private WithEvents txtName As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblName As DataDynamics.ActiveReports.Label
End Class
