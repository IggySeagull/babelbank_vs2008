Option Strict Off
Option Explicit On
Module WriteBols
	
	Dim sLine As String ' en output-linje
	Dim oBatch As Batch
	Dim nBatchDebitAmount As Double
	Dim nBatchCreditAmount As Double
	Dim nFileDebitAmount As Double
	Dim nFileCreditAmount As Double
	Dim nFileAmount As Double
	Dim iSequenceNo As Double
	Dim sSpecial As String
    Function WriteBolsFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sAdditionalID As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim k, i, j, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim sPayCode As String
        Dim nLine As Short
        Dim nCol As Short
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sNewOwnref As String

        Dim bReturnValue As Boolean

        ' lag en outputfil
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                sSpecial = oBabel.Special

                'Only export statuscode 00 or 02. The others don't give any sense in BOLS
                'If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) _
                'Or oBabel.StatusCode = "02" Then
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled
                            If oPayment.Cancel = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch
                'Else
                '    For Each oBatch In oBabel.Batches
                '        For Each oPayment In oBatch.Payments
                '            oPayment.Exported = True
                '        Next oPayment
                '    Next oBatch
                'End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteBOLSFileStart(oBabel)
                        oFile.WriteLine((sLine))
                        bFileStartWritten = True
                        'UPGRADE_WARNING: Couldn't resolve default property of object oBabel.Batches(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sLine = WriteBOLSBatchStart(oBabel.Batches(1))
                        oFile.WriteLine((sLine))

                        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20190612" Then
                            i = "KOKO"
                        End If

                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            i = i + 1

                            ' 16.02.07 - removed next 2 lines. Writes only ONE BatchStart pr file
                            'sLine = WriteBOLSBatchStart(oBatch)
                            'oFile.WriteLine (sLine)

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then
                                    j = j + 1

                                    sLine = WriteBOLSPayment(oPayment)
                                    oFile.WriteLine((sLine))
                                    oPayment.Exported = True
                                End If
                            Next oPayment ' payment

                            ' 16.02.07 - removed next 2 lines. Writes only ONE BatchEnd pr file
                            'sLine = WriteBOLSBatchEnd()
                            'oFile.WriteLine (sLine)
                        End If

                    Next oBatch 'batch
                End If
            Next oBabel 'Babelfile

            sLine = WriteBOLSBatchEnd()
            oFile.WriteLine((sLine))

            sLine = WriteBOLSFileEnd()
            ' Changed 12.08.2007
            ' Danske Bank will not have a CRLF at the end of last line!
            If oBabelFiles(1).Special = "SGBOLS" Then
                oFile.Write((sLine))
            Else
                oFile.WriteLine((sLine))
            End If

            bReturnValue = True

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBolsFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

            bReturnValue = False

        Finally
            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return bReturnValue

    End Function
    Function WriteBOLSFileStart(ByRef oBabelFile As BabelFile) As String
        Dim sLine As String
        Dim sTmp As String

        ' Reset file-totals:
        nFileDebitAmount = 0
        nFileCreditAmount = 0
        iSequenceNo = 1

        sLine = "11" ' 1. Layoutkode
        sLine = sLine & PadLeft(CStr(iSequenceNo), 6, "0") ' 2. Sekvensnummer
        If sSpecial = "SGBOLS" Then
            sLine = sLine & "FACTOR" ' 3. Datasett ID
        Else
            sLine = sLine & "FACTOR" ' 3. Datasett ID
        End If
        sLine = sLine & "000" ' 4. Avleverende sentral, NB! ikke ifyllt enn�!
        sLine = sLine & "000" ' 5. Mottakende sentral, NB! ikke ifyllt enn�!
        sTmp = oBabelFile.DATE_Production
        sLine = sLine & Right(sTmp, 2) & Mid(sTmp, 5, 2) & Mid(sTmp, 3, 2) ' 6. Behandlingsdato, DDMMYY
        sLine = sLine & "01" ' 7. Datasettnummer NB! ikke aktivt ibruk enn�
        sLine = sLine & "0" ' 8.Clearingkode, NB! ikke ifyllt enn�!
        If sSpecial = "SGBOLS" Then
            ' ser ut som SG Finans gjentar datoen her
            sLine = sLine & Right(sTmp, 2) & Mid(sTmp, 5, 2) & Mid(sTmp, 3, 2)
        Else
            sLine = sLine & "000000" ' 9. Autorisasjonskode, NB! ikke ifyllt enn�!
        End If
        sLine = sLine & "000000" ' 10. Oppgj�rsdato

        'pad to 60 with space
        sLine = PadLine(sLine, 60, "0")

        WriteBOLSFileStart = sLine

    End Function
    Function WriteBOLSBatchStart(ByRef oBatch As Batch) As String
        ' Bunthode record

        Dim sLine As String
        ' Reset Batch-totals:
        nBatchDebitAmount = 0
        nBatchCreditAmount = 0
        iSequenceNo = iSequenceNo + 1

        sLine = "13" ' 1. Layoutkode
        sLine = sLine & PadLeft(CStr(iSequenceNo), 6, "0") ' 2. Sekvensnummer
        If sSpecial = "SGBOLS" Then
            sLine = sLine & "8601" ' 3. HBFE, Registnr bank
        ElseIf EmptyString((oBatch.I_Branch)) Then
            sLine = sLine & PadLeft(Left(oBatch.I_Branch, 4), 4, "0") ' 3. HBFE, Registnr bank
        Else
            ' Find in Companysetup, AdditionalNo
            sLine = sLine & PadRight(oBatch.VB_Profile.AdditionalNo, 4, "0") ' 3. HBFE, Registnr bank
        End If
        sLine = sLine & "00" ' 4. Bunttype, NB! Ikke aktivt ibruk enn�
        sLine = sLine & "000" ' 5. Buntnr, NB! Ikke aktivt ibruk enn�

        sLine = PadLine(sLine, 60, "0")
        WriteBOLSBatchStart = sLine

    End Function
    Function WriteBOLSPayment(ByRef oPayment As Payment) As String

        Dim sLine As String
        Dim sPayCode As String
        Dim sTmp As String

        iSequenceNo = iSequenceNo + 1

        sLine = "15" ' 1. Layoutkode
        sLine = sLine & PadLeft(CStr(iSequenceNo), 6, "0") ' 2. Sekvensnummer
        sLine = sLine & PadLeft(oPayment.E_Account, 11, "0") ' 3. Kontonummer
        If sSpecial = "SGBOLS" Then
            ' SG skal ikke? bruke signede bel�p??
            sLine = sLine & PadLeft(CStr(oPayment.MON_InvoiceAmount), 11, "0") ' 4. Bel�p
        Else
            sLine = sLine & PadLeft(TranslateToSignedAmount((oPayment.MON_InvoiceAmount)), 11, "0") ' 4. Bel�p
        End If
        sLine = sLine & "000" ' 5. Tekstkode, NB! Ikke aktivt ibruk enn�
        sTmp = oPayment.DATE_Payment
        sLine = sLine & Right(sTmp, 2) & Mid(sTmp, 5, 2) & Mid(sTmp, 3, 2) ' 6. Bokf�ringsdato, DDMMYY

        sLine = sLine & New String("0", 11) ' 7. ID Kode, NB! Ikke aktivt ibruk enn�
        sLine = sLine & New String("0", 9) ' 8. Arkivref, NB! Ikke aktivt ibruk enn�
        sLine = sLine & "0" ' 9. BehandlingsKode, NB! Ikke aktivt ibruk enn�

        ' Add to Batch-totals:
        If oPayment.MON_InvoiceAmount < 0 Then
            nBatchDebitAmount = nBatchDebitAmount + System.Math.Abs(oPayment.MON_InvoiceAmount)
        Else
            nBatchCreditAmount = nBatchCreditAmount + oPayment.MON_InvoiceAmount
        End If

        sLine = PadLine(sLine, 60, "0") ' filler

        WriteBOLSPayment = sLine

    End Function
    Function WriteBOLSBatchEnd() As String '(oBatch) As String
        Dim sLine As String

        ' Add to File-totals:
        nFileDebitAmount = nFileDebitAmount + nBatchDebitAmount
        nFileCreditAmount = nFileCreditAmount + nBatchCreditAmount

        iSequenceNo = iSequenceNo + 1

        sLine = "17" ' 1. Layoutkode
        sLine = sLine & PadLeft(CStr(iSequenceNo), 6, "0") ' 2. Sekvensnummer
        If sSpecial = "SGBOLS" Then
            ' SG skal ikke? bruke signede bel�p??
            sLine = sLine & PadLeft(CStr(nBatchDebitAmount), 13, "0") ' 3. Sumbel�p debet
            sLine = sLine & PadLeft(CStr(nBatchCreditAmount), 13, "0") ' 4. Sumbel�p kredit
        Else
            sLine = sLine & PadLeft(TranslateToSignedAmount(nBatchDebitAmount), 13, "0") ' 3. Sumbel�p debet
            sLine = sLine & PadLeft(TranslateToSignedAmount(nBatchCreditAmount), 13, "0") ' 4. Sumbel�p kredit
        End If
        sLine = PadLine(sLine, 60, "0")
        WriteBOLSBatchEnd = sLine

    End Function
    Function WriteBOLSFileEnd() As String '(oBabelFile As BabelFile) As String
        Dim sLine As String

        iSequenceNo = iSequenceNo + 1

        sLine = "19" ' 1. Layoutkode
        sLine = sLine & PadLeft(CStr(iSequenceNo), 6, "0") ' 2. Sekvensnummer
        If sSpecial = "SGBOLS" Then
            ' SG skal ikke? bruke signede bel�p??
            sLine = sLine & PadLeft(CStr(nFileDebitAmount), 13, "0") ' 3. Sumbel�p debet
            sLine = sLine & PadLeft(CStr(nFileCreditAmount), 13, "0") ' 4. Sumbel�p kredit
        Else
            sLine = sLine & PadLeft(TranslateToSignedAmount(nFileDebitAmount), 13, "0") ' 3. Sumbel�p debet
            sLine = sLine & PadLeft(TranslateToSignedAmount(nFileCreditAmount), 13, "0") ' 4. Sumbel�p kredit
        End If

        ' pad with 0 to 80
        sLine = PadLine(sLine, 60, "0")

        WriteBOLSFileEnd = sLine

    End Function


    '=========================================================================================================
    Function WriteSGFINANS_BOLSFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sAdditionalID As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim k, i, j, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim sPayCode As String
        Dim nLine As Short
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sNewOwnref As String

        ' XNET 10.09.2013, set to 0 to be certain
        iSequenceNo = 0

        ' lag en outputfil
        On Error GoTo errCreateOutputFile

        oFs = New Scripting.FileSystemObject

        oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

        For Each oBabel In oBabelFiles
            bExportoBabel = False
            sSpecial = oBabel.Special

            'Only export statuscode 00 or 02. The others don't give any sense in SGFINANS_BOLS
            'If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) _
            'Or oBabel.StatusCode = "02" Then
            'Have to go through each batch-object to see if we have objects thatt shall
            ' be exported to this exportfile
            For Each oBatch In oBabel.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                        'Don't export payments that have been cancelled
                        If oPayment.Cancel = False Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If InStr(oPayment.REF_Own, "&?") Then
                                        'Set in the part of the OwnRef that BabelBank is using
                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                    Else
                                        sNewOwnref = ""
                                    End If
                                    If sNewOwnref = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If 'If oPayment.Cancel = False Then
                    End If
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oPayment
                If bExportoBabel Then
                    Exit For
                End If
            Next oBatch
            'Else
            '    For Each oBatch In oBabel.Batches
            '        For Each oPayment In oBatch.Payments
            '            oPayment.Exported = True
            '        Next oPayment
            '    Next oBatch
            'End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

            If bExportoBabel Then

                i = 0
                'Loop through all Batch objs. in BabelFile obj.
                For Each oBatch In oBabel.Batches
                    bExportoBatch = False
                    'Have to go through the payment-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled
                            If oPayment.Cancel = False Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        'If true exit the loop, and we have data to export
                        If bExportoBatch Then
                            Exit For
                        End If
                    Next oPayment


                    If bExportoBatch Then
                        i = i + 1
                        j = 0
                        For Each oPayment In oBatch.Payments
                            bExportoPayment = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                Else
                                    oPayment.Exported = True
                                End If 'If oPayment.Cancel = False Then
                            End If

                            If bExportoPayment Then
                                j = j + 1

                                sLine = WriteSGFINANS_BOLSPayment(oPayment)
                                oFile.WriteLine((sLine))
                                oPayment.Exported = True
                            End If
                        Next oPayment ' payment

                    End If

                Next oBatch 'batch
            End If
        Next oBabel 'Babelfile

        sLine = WriteSGFINANS_BOLSFileEnd()
        oFile.WriteLine((sLine))

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        WriteSGFINANS_BOLSFile = True


        Exit Function

errCreateOutputFile:
        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        WriteSGFINANS_BOLSFile = False

    End Function
	Function WriteSGFINANS_BOLSPayment(ByRef oPayment As Payment) As String
		
		Dim sLine As String
		Dim sPayCode As String
		Dim sTmp As String
		
		iSequenceNo = iSequenceNo + 1
		
		' 13.12.06
		' Different formats for send and returnfiler;
		If oPayment.StatusCode = "00" Then
			sLine = "BOLS" & ";" ' 1. Recordtype
			sLine = sLine & oPayment.DATE_Payment & ";" ' 2. Bokf�ringsdato, YYYYMMDD
			sLine = sLine & oPayment.E_Account & ";" ' 3. Kontonummer
			sLine = sLine & LTrim(Str(oPayment.MON_InvoiceAmount)) ' 4. Bel�p
		Else
			' Returnfile to Aquarius
			sLine = "BOLSRETURN" & ";" ' 1. Recordtype
			sLine = sLine & oPayment.DATE_Payment & ";" ' 2. Valuedate, YYYYMMDD
			sLine = sLine & oPayment.DATE_Payment & ";" ' 3. Transactiondate, YYYYMMDD
			sLine = sLine & oPayment.E_Account & ";" ' 4. Kontonummer
			sLine = sLine & LTrim(Str(System.Math.Abs(oPayment.MON_InvoiceAmount))) & ";" ' 5. Bel�p
			' endret 08.11.2007 etter samtale med Rolf
			'If oPayment.MON_InvoiceAmount < 0 Then
			If oPayment.MON_InvoiceAmount > 0 Then
				sLine = sLine & "4010/0010" '6. Fortegn
			Else
				sLine = sLine & "3010/0010" '6. Fortegn
			End If
		End If
		' Add to File-totals:
		' 25.05.2007 - use "non-sense total"
		'nFileAmount = nFileAmount + oPayment.MON_InvoiceAmount
		nFileAmount = nFileAmount + System.Math.Abs(oPayment.MON_InvoiceAmount)
		
		WriteSGFINANS_BOLSPayment = sLine
		
	End Function
	Function WriteSGFINANS_BOLSFileEnd() As String
		Dim sLine As String
		
		sLine = "BOLSTOTAL" & ";" ' 1. Recordtype
		sLine = sLine & LTrim(Str(nFileAmount)) & ";" ' 2. Sumbel�p
		sLine = sLine & LTrim(Str(iSequenceNo)) ' 3. Antall BOLS-transer
		
		
		WriteSGFINANS_BOLSFileEnd = sLine
		
	End Function
End Module
