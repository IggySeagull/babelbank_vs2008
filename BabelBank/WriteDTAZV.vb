﻿Option Strict Off
Option Explicit On
Module WriteDTAZV
    Dim nFileSumAmount As Double
    Dim nFileNoRecords As Integer
    Dim lPositionsWritten As Long

    'XOKNET - 29.11.2010 - New module added
    'XOKNET - 13.12.2010 - Whole module
    Function Write_DTAZVFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sClientNo As String, ByVal sSpecial As String, ByVal sCompanyNo As String) As Boolean
        ' DTAZV - Germany - Cross border payments - Data exchange between customer And Bank - Version 2006

        '01.12.2010 - This export file is created for Thule/DnBNOR to be used together with Prang.
        'In this project there should be no validation of the information, and there alle error handling is omitted.
        'However ther are a few errormessages that is commented and may be used in other cases.
        'Prang, for some strange reason, needs a seperate file per paymentday therefore an For ... Each is surronded the whole function.
        'The test if it is a Sepa payment is also omitted.
        'Also the field Q8 - (First) execution date of file is not correct!
        'Reporting to Bundesbank is not implemented, and other transactionstypes are implemented but not tested.

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.FreeText
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim i As Integer
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim bFileStartWritten As Boolean
        Dim sString As String
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim sBLZCodeReceivingInstitution As String
        Dim sCustomerNoPayer As String
        Dim nSequenceNoTotal As Double
        Dim sBLZDebitAccount As String
        Dim sCurrencyDebitAccount As String
        Dim sAccountNumberToDebit As String
        Dim dFirstPaymentDate As Date
        Dim lCounter As Long
        Dim bThisFileWillBeReadByPrang As Boolean
        Dim sPaymentDateToExport As String
        Dim bAtLeastOnePaymentExported As Boolean
        Dim bx As Boolean

        ' create an outputfile
        Try
            oFs = New Scripting.FileSystemObject

            bAppendFile = False
            sOldAccount = ""
            nSequenceNoTotal = 0
            sBLZDebitAccount = ""
            sCurrencyDebitAccount = ""
            sAccountNumberToDebit = ""
            sPaymentDateToExport = ""
            bFoundClient = False
            dFirstPaymentDate = Date.Today  'Now()
            bFileStartWritten = False
            lPositionsWritten = 0
            bx = False

            If sSpecial = "THULE" Then
                bThisFileWillBeReadByPrang = True
            Else
                bThisFileWillBeReadByPrang = False
            End If

            If Not bThisFileWillBeReadByPrang Then
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, Scripting.Tristate.TristateFalse)
            End If

            If bThisFileWillBeReadByPrang Then
                'Move all paymentdates tearlier than Now() to Now()
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If StringToDate(oPayment.DATE_Payment) < Date.Today Then '''Now() Then
                                oPayment.DATE_Payment = DateToString(Date.Today)    'Now())
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabel
            End If
            bFileStartWritten = False

            For lCounter = 1 To 300
                bAtLeastOnePaymentExported = False
                For Each oBabel In oBabelFiles

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                If Not oPayment.Exported Then
                                    Exit For
                                Else
                                    bExportoBabel = False
                                End If
                            End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next


                    If bExportoBabel Then

                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    If Not oPayment.Exported Then
                                        Exit For
                                    Else
                                        bExportoBatch = False
                                    End If
                                End If
                            Next


                            If bExportoBatch Then


                                For Each oPayment In oBatch.Payments

                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    ' added 16.12.2008 - do not export payments with Exported = True
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If

                                    If oPayment.Exported Then
                                        bExportoPayment = False
                                    Else

                                    End If

                                    If bThisFileWillBeReadByPrang Then
                                        If oPayment.DATE_Payment = sPaymentDateToExport Or sPaymentDateToExport = "" Then
                                            'OK - Export the payment
                                            '(sPaymentDateToExport = "" signifies the start of a new file/date
                                        Else
                                            bExportoPayment = False
                                        End If
                                    End If

                                    If bExportoPayment Then

                                        bAtLeastOnePaymentExported = True

                                        If oPayment.I_Account <> sOldAccount Then
                                            bFoundClient = False

                                            If EmptyString(oPayment.VB_Profile.FileSetups(iFormat_ID).CompanyNo) Then
                                                Err.Raise(17431, "Write_DTAZVFile", "No BLZ-code for receiving financial institution is stated.")
                                            Else
                                                sBLZCodeReceivingInstitution = Trim$(oPayment.VB_Profile.FileSetups(iFormat_ID).CompanyNo)
                                            End If
                                            If EmptyString(oPayment.VB_Profile.FileSetups(iFormat_ID).AdditionalNo) Then
                                                Err.Raise(17432, "Write_DTAZVFile", "No Customer identity (order number agreed with the financial institution receiving the file) stated")
                                            Else
                                                sCustomerNoPayer = Trim$(oPayment.VB_Profile.FileSetups(iFormat_ID).AdditionalNo)
                                            End If

                                            For Each oClient In oPayment.VB_Profile.FileSetups(iFormat_ID).Clients
                                                For Each oaccount In oClient.Accounts
                                                    If Trim$(oaccount.Account) = oPayment.I_Account Or Trim$(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                        If EmptyString(oaccount.ContractNo) Then
                                                            Err.Raise(17433, "Write_DTAZVFile", "The debitaccount " & oPayment.I_Account & " has no BLZ-code. You may enter information about the account in the Client setup.")
                                                        Else
                                                            sBLZDebitAccount = Trim$(oaccount.ContractNo)
                                                        End If
                                                        If EmptyString(oaccount.CurrencyCode) Then
                                                            Err.Raise(17435, "Write_DTAZVFile", "The debitaccount " & oPayment.I_Account & " has no Currency. You may enter information about the account in the Client setup.")
                                                        Else
                                                            sCurrencyDebitAccount = Trim$(oaccount.CurrencyCode)
                                                        End If
                                                        If EmptyString(oaccount.ConvertedAccount) Then
                                                            sAccountNumberToDebit = Trim$(oaccount.Account)
                                                        Else
                                                            sAccountNumberToDebit = Trim$(oaccount.ConvertedAccount)
                                                        End If
                                                        sOldAccount = oPayment.I_Account
                                                        bFoundClient = True
                                                        Exit For
                                                    End If
                                                Next
                                                'If bFoundClient Then
                                                '    'Get the companyno from a property passed from BabelExport
                                                '    If EmptyString(sCompanyNo) Then
                                                '        Err.Raise 1, "WriteHBGlobalOnline_MT101File", "No Customer identity in Handelsbanken stated"
                                                '    End If
                                                '    Exit For
                                                'End If

                                            Next
                                        End If

                                        If Not bFoundClient Then
                                            Err.Raise(17434, "Write_DTAZVFile", "The debitaccount " & oPayment.I_Account & " is unknown to BabelBank. You must enter information about the account in the Client setup.")
                                        End If

                                        If Not bFileStartWritten Then

                                            If bThisFileWillBeReadByPrang Then
                                                sPaymentDateToExport = oPayment.DATE_Payment
                                                oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
                                                oFile = oFs.OpenTextFile(AddTextToFilename(sFilenameOut, oPayment.DATE_Payment, False, True), Scripting.IOMode.ForAppending, True, 0)
                                                lPositionsWritten = 0
                                            End If

                                            nSequenceNoTotal = oBabel.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                            If nSequenceNoTotal > 99 Then
                                                nSequenceNoTotal = 1
                                            End If

                                            sString = WriteQRecord_FileHeader(oBatch, sCompanyNo, sBLZCodeReceivingInstitution, sCustomerNoPayer, nSequenceNoTotal, dFirstPaymentDate, sSpecial)

                                            bx = WriteDTAZVRecord(oFile, sString)
                                            bFileStartWritten = True
                                        End If

                                        'If oPayment.DATE_Payment > Now() Then
                                        'If oPayment.DATE_Payment > DateToString(Date.Today) Then
                                        'If dFirstPaymentDate + 15 < StringToDate(oPayment.DATE_Payment) Then
                                        '    'Err.raise "The paymentdate must be maximum 15 days later than the first paymentdate"
                                        'Else
                                        '    'OK
                                        'End If
                                        'End If
                                        sString = WriteTRecord_SingleRecord(oPayment, sBLZDebitAccount, sCurrencyDebitAccount, sAccountNumberToDebit, sSpecial)
                                        bx = WriteDTAZVRecord(oFile, sString)
                                        oPayment.Exported = True

                                    End If 'If bExportoPayment
                                Next ' payment
                            End If 'If bExportoBatch Then
                        Next 'batch
                    End If 'If bExportBabel

                    oBabel.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                    oBabel.VB_Profile.Status = 2
                    oBabel.VB_Profile.FileSetups(iFormat_ID).Status = 1

                Next oBabel

                If bAtLeastOnePaymentExported Then
                    sString = WriteZRecord_Trailer()
                    bx = WriteDTAZVRecord(oFile, sString)
                End If

                bFileStartWritten = False


                If Not bThisFileWillBeReadByPrang Then
                    lCounter = 301 ' To jump out of the loop
                End If

                If Not bAtLeastOnePaymentExported Then
                    'No payments exported during this loop of lCounter.
                    'Then all payments that shall be exported are exported.
                    'Get out of the loop
                    lCounter = 301
                End If

                sPaymentDateToExport = ""

                If bAtLeastOnePaymentExported Then
                    oFile.Close()
                    oFile = Nothing
                    oFs = Nothing
                End If

            Next lCounter

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: Write_DTAZVFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Write_DTAZVFile = True

    End Function
    Private Function WriteQRecord_FileHeader(ByVal oBatch As vbBabel.Batch, ByVal sCompanyNo As String, ByVal sBLZCodeReceivingInstitution As String, ByVal sCustomerNoPayer As String, ByVal nSequenceNoTotal As Double, ByVal dFirstPaymentDate As Date, ByVal sSpecial As String) As String
        Dim sLine As String

        sLine = ""

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 0

        '1-4 M Length of record - Q1
        sLine = "0256"
        '5-5 M - Type of record - Constant "Q" - Q2
        sLine = sLine & "Q"
        ' 6-13 M - German bank code (BLZ) - Financial institution receiving the file - Q3
        sLine = sLine & PadLeft(sBLZCodeReceivingInstitution, 8, "0")
        ' 14-23 M - Customer number - Order number agreed with the financial institution receiving the file (where necessary: account number) - Q4
        sLine = sLine & PadLeft(sCustomerNoPayer, 10, "0")
        '' 24-163 - 4*35 - Name and address of principal -
        'Lines 1 and 2 : Name
        'Line 3 : Street/PO Box
        'Line 4 : City / town - Q5
        sLine = sLine & PadRight(ValidateCharacters(oBatch.VB_Profile.CompanyName), 35, " ")
        sLine = sLine & Space(35)
        sLine = sLine & PadRight(ValidateCharacters(oBatch.VB_Profile.CompanyAdr1), 35, " ")
        sLine = sLine & PadRight(ValidateCharacters(oBatch.VB_Profile.CompanyCity), 35, " ")
        ' 164-169 M - Date of generation - Format: YYMMDD - Q6
        sLine = sLine & VB6.Format(Date.Today, "YYMMDD")
        ' 170-171 M - Serial number - Daily serial number - Q7
        sLine = sLine & PadLeft(Trim$(Str(nSequenceNoTotal)), 2, "0")
        ' 172-177 M - (First) execution date of file - Format: YYMMDD; Same or up to maximum of 5 calendar days after the date of field Q6. - Q8
        '************************************************************************
        'TODO - THIS IS NOT CORRECT!
        '************************************************************************
        sLine = sLine & VB6.Format(dFirstPaymentDate, "YYMMDD")
        ' 178-178 M - To be sent to reporting authorities - Should the institution receiving the file send the report data of the following payment orders to the Deutsche Bundesbank?
        '(see explanations in Appendix 3) 'J' Yes 'N' No - Q9
        sLine = sLine & "N"
        ' 179-180 O/M - Federal state number - Absolutely required if reporting data of payment orders are to be sent to Deutsche Bundesbank ('J' in field Q9). - Q10
        sLine = sLine & "00"
        ' 181-188 O/M - Principal's company number / bank code - See description of field Q10 - Q11
        sLine = sLine & "00000000"
        'sLine = sLine & ' 189-256 N - Reserve - Q12
        sLine = sLine & Space(68)


        WriteQRecord_FileHeader = sLine

    End Function
    Private Function WriteTRecord_SingleRecord(ByVal oPayment As vbBabel.Payment, ByVal sBLZDebitAccount As String, ByVal sCurrencyDebitAccount As String, ByVal sAccountNumberToDebit As String, ByVal sSpecial As String) As String
        Dim sLine As String
        Dim bAddressOfBankMustBeStated As Boolean
        Dim bThisIsAChequePayment As Boolean
        Dim bThisIsAEUPayment As Boolean
        Dim bThisIsAEUEPayment As Boolean
        Dim sErrorMessage As String
        Dim sAmount As String
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.FreeText
        Dim sFreetext As String
        Dim bFirstFreetext As Boolean
        Dim iInstructionCode As Integer
        Dim sTmp As String

        bAddressOfBankMustBeStated = True
        bThisIsAChequePayment = False
        If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.noCheque Then
            bThisIsAChequePayment = False
        Else
            bThisIsAChequePayment = True
        End If
        'EU standard payment, ie cross-border payment under Article 2 a) i) of Regulation (EC)No 2560/2001 of the European Parliament and of the Council of the
        'European Union on cross-border payments in euro, which are in euro up to an amount of EUR 50,000 and in which, pursuant to Article 5 (2),
        'the IBAN of the beneficiary and the BIC of the bank of the beneficiary are mentioned.
        bThisIsAEUPayment = False
        'As above but Express
        bThisIsAEUEPayment = False
        sErrorMessage = ""
        sAmount = ""
        sFreetext = ""
        sTmp = ""
        bFirstFreetext = True
        iInstructionCode = 0

        If IsSEPAPayment(oPayment, , False) Then
            If oPayment.Priority Then
                bThisIsAEUEPayment = True
            Else
                bThisIsAEUPayment = True
            End If
        End If

        sLine = ""

        '1-4 M Length of record - T1
        sLine = "0768"
        '5-5 M - Type of record - Constant "T" - T2
        sLine = sLine & "T"
        ' 6-13 M - German bank code (BLZ) - Bank code of the bank section maintaining the account, to which order amount is to be debited (field T4b) - T3
        sLine = sLine & PadLeft(sBLZDebitAccount, 8, "0")
        ' 14-16 M - ISO currency code - For account to which order amount is to be debited - For EU and EUE payments only EUR permissible - T4a
        sLine = sLine & sCurrencyDebitAccount
        ' 17-26 M - Account number - Account to be debited with order amount - T4b
        'sLine = sLine & PadLeft(sAccountNumberToDebit, 10, "0")
        ' 06.09.2012: Take LAST 10 digits, in case we pass on an IBAN accountnumber
        sLine = sLine & PadLeft(Right$(sAccountNumberToDebit, 10), 10, "0")
        ' 27-32 O - Execution date of individual payment if deviating from field Q8 - T5
        'Format: YYMMDD; immediately or by the date specified in field Q8 but no later than 15 calendar days after the date in field Q6; if the date is absent in T5,
        'the date in Q8 is assumed to be the execution date - T5
        If StringToDate(oPayment.DATE_Payment) > Date.Today Then
            sLine = sLine & VB6.Format(StringToDate(oPayment.DATE_Payment), "YYMMDD")
        Else
            sLine = sLine & VB6.Format(Date.Today, "yyMMdd")
        End If
        ' 33-40 O/M - German bank code (BLZ) - Bank code of bank section maintaining the account to be debited with fees and expenses.
        '(completed only if this account is different from order amount account - For EUE payments only EUR permissible - T6
        sLine = sLine & "00000000"
        ' 41-43 O/M - ISO currency code - Currency code of the account to be debited with fees and expenses
        '(completed only if this account is different from order amount account) - T7a
        sLine = sLine & Space(3)
        ' 44-53 O/M - Account number - Account number of the account to be debited with fees and expenses (completed only if this account is different from order amount account) - T7b
        sLine = sLine & "0000000000"
        ' 54-64 O/M - Bank Identifier Code (BIC) of financial institution of beneficiary or other ID, eg Chips ID -
        'If the payment is made to a German financial institution, alternatively, also the German bank code of the beneficiary financial institution,
        'in which case three slashes should precede the bank code (not to be completed for cheque drawings, ie for payment type codes 20-23 and 30-33 in field T22) - T8
        If Not bThisIsAChequePayment Then
            If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                sLine = sLine & PadRight(Trim$(UCase(oPayment.BANK_SWIFTCode)), 11, " ")
                bAddressOfBankMustBeStated = False
            Else
                If Not EmptyString(oPayment.BANK_BranchNo) Then
                    If oPayment.BANK_BranchNo = vbBabel.BabelFiles.BankBranchType.Bankleitzahl Then
                        ' XNET 12.04.2012 - changed from Swift to BranchNo
                        'sLine = sLine & PadRight("///" & Trim$(UCase(oPayment.BANK_SWIFTCode)), 11, " ")
                        sLine = sLine & PadRight("///" & Trim$(UCase(oPayment.BANK_BranchNo)), 11, " ")
                        bAddressOfBankMustBeStated = False
                    Else
                        ' XNET 12.04.2012 - changed from Swift to BranchNo
                        'sLine = sLine & PadRight(Trim$(oPayment.BANK_SWIFTCode), 11, " ")
                        sLine = sLine & PadRight(Trim$(oPayment.BANK_BranchNo), 11, " ")
                        bAddressOfBankMustBeStated = False
                    End If
                Else
                    sLine = sLine & Space(11)
                End If
            End If
        Else
            sLine = sLine & Space(11)
        End If
        ' 65-67 O/M - Country code of beneficiary's bank - Two-letter ISO-alpha country code as per country index for the balance of payments statistics; left aligned;
        'third place blank (mandatory field if field T8 is not completed; not to be completed for cheque drawings, ie for payment type codes 20-23 and 30–33 in field T22) - T9a
        If Not bThisIsAChequePayment Then
            If EmptyString(oPayment.BANK_SWIFTCode) Then
                If EmptyString(oPayment.BANK_CountryCode) Then
                    'err.raise 'When the Bank Identifier Code (BIC) of financial institution of beneficiary or other ID, eg Chips ID, is not stated, the receiving bank's country code is mandatory
                    sLine = sLine & "   "
                Else
                    sLine = sLine & PadRight(oPayment.BANK_CountryCode, 3, " ")
                End If
            Else
                sLine = sLine & "   "
            End If
        Else
            sLine = sLine & Space(3)
        End If
        ' 68-207 4*35 O/M - Address of beneficiary's bank - Mandatory field if field T8 does not contain BIC address or – for payments to a German credit institution –
        'it does not contain the German bank code; if address is not known, enter “UNBEKANNT"
        'Lines 1 and 2: Name 68-102 and 103-137
        'Line 3: Street 138-172
        'Line 4: City 173-207 (not to be completed for cheque drawings, i.e. for payment type codes 20-23 and 3033 in field T22) - T9b
        'sLine = sLine &
        ' XokNET 02.02.2012 - Andres Gunkel, DNB DE says that if standard EU-payment, then bankname can't be stated
        'If not bThisIsAChequePayment Then
        If bThisIsAChequePayment = False And bThisIsAEUPayment = False Then
            If bAddressOfBankMustBeStated Then
                If EmptyString(oPayment.BANK_Name) Then
                    sLine = sLine & PadRight("UNBEKANNT", 35, " ")
                    sLine = sLine & Space(35)
                    sLine = sLine & Space(35)
                    sLine = sLine & Space(35)
                Else
                    sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Name)), 35, " ")
                    sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Adr1)), 35, " ")
                    sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Adr2)), 35, " ")
                    sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Adr3)), 35, " ")
                End If
            Else
                sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Name)), 35, " ")
                sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Adr1)), 35, " ")
                sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Adr2)), 35, " ")
                sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.BANK_Adr3)), 35, " ")
            End If
        Else
            sLine = sLine & Space(140)
        End If

        ' 208-210 M - Country code for country of beneficiary or cheque recipient - Two-letter ISO-alpha country code as per country index for the balance of payments statistics;
        'left aligned, third place blank - T10a
        If EmptyString(oPayment.E_CountryCode) Then
            'err.raise 'Country code for country of beneficiary or cheque recipient is mandatory
        End If
        sLine = sLine & PadRight(oPayment.E_CountryCode, 3, " ")
        '211-350 4*35 M - Beneficiary/cheque recipient - For payment orders: beneficiary For cheque drawings: cheque recipient
        'Lines 1 and 2: Name 211-245 and 246-280
        ''Line 3: Street 281-315
        ''Line 4: City/Country 316-350 - T10b
        ''For EU and EUE payments: Mentioning the cheque recipient is not possible
        If EmptyString(oPayment.E_Name) Then
            'Err.raise Name of beneficiary/cheque recipient is mandatory
            sLine = sLine & Space(140)
        Else
            sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.E_Name)), 35, " ")
            sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.E_Adr1)), 35, " ")
            sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.E_Adr2)), 35, " ")
            If EmptyString(oPayment.E_City) Then
                sLine = sLine & PadRight(ValidateCharacters(Trim$(oPayment.E_Adr3)), 35, " ")
            Else
                sLine = sLine & PadRight(oPayment.E_Zip & ValidateCharacters(Trim$(oPayment.E_City)) & " " & oPayment.E_CountryCode, 35, " ")
            End If

        End If
        '351-420 2*35 O/M - Order mark - Completed only for cheque drawings (ie for the payment type codes 20-23 and 30-33 in field T22)
        'and if different from content of lines 1 and 2 in field T10b -T11
        sLine = sLine & Space(70)
        ' 421-455 O/M - Account number or IBAN of beneficiary - IBAN or account number of the beneficiary, left aligned, beginning with slash.
        '(Not to be completed for cheque drawings, ie for payment type codes 20-23 and 30-33 in field T22) - TT12
        ''For EU and EUE payments: Only IBAN permitted
        If Not bThisIsAChequePayment Then
            If bThisIsAEUPayment Or bThisIsAEUEPayment Then
                If IsIBANNumber(oPayment.E_Account, False, False, sErrorMessage) Then
                    'sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
                    ' XNET 27.12.2010 - added / in front of E_Account if missing, thus added next If / End If
                    If Left$(oPayment.E_Account, 1) <> "/" Then
                        sLine = sLine & PadRight("/" & oPayment.E_Account, 35, " ")
                    Else
                        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
                    End If
                Else
                    'sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
                    ' XNET 27.12.2010 - added / in front of E_Account if missing, thus added next If / End If
                    If Left$(oPayment.E_Account, 1) <> "/" Then
                        sLine = sLine & PadRight("/" & oPayment.E_Account, 35, " ")
                    Else
                        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
                    End If
                    'Err.raise For EU or EUE payments the accountnumberstated must be IBAN & vbcrlf & serrormessage
                End If
            Else
                'sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
                ' XNET 27.12.2010 - added / in front of E_Account if missing, thus added next If / End If
                If Left$(oPayment.E_Account, 1) <> "/" Then
                    sLine = sLine & PadRight("/" & oPayment.E_Account, 35, " ")
                Else
                    sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
                End If

            End If
        Else
            sLine = sLine & Space(35)
        End If
        '456-458 M - Order currency - ISO code of currency payable - T13
        'For EU and EUE payments: Only “EUR” permissible
        If Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
            sLine = sLine & Space(3)
            'Err.raise No currency stated on the payment. The field is mandatory.
        Else
            If bThisIsAEUPayment Or bThisIsAEUEPayment Then
                If Trim$(oPayment.MON_InvoiceCurrency) <> "EUR" Then
                    'Err.raise For EU or EUE payments the payment currency must be EUR.
                    sLine = sLine & PadRight("oPayment.MON_InvoiceCurrency", 3, " ")
                Else
                    sLine = sLine & "EUR"
                End If
            Else
                sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
            End If
        End If

        If bThisIsAEUPayment Then
            If oPayment.MON_InvoiceAmount > 5000000 Then
                'err.raise For EU payments only payments up to max. EUR 50,000 is permissible.
            End If
        End If
        sAmount = Trim$(Str(oPayment.MON_InvoiceAmount))
        ' 459-472 M - Amount (digits before decimal point) - Right aligned - T14a
        'For EU payments: Only amounts up to max. EUR 50,000 permissible
        sLine = sLine & PadLeft(Left$(sAmount, Len(sAmount) - 2), 14, "0")

        nFileSumAmount = nFileSumAmount + Val(Left$(sAmount, Len(sAmount) - 2))

        ' 473-475 M - Amount (digits after decimal point) - Left aligned - T14b
        sLine = sLine & PadRight(Right(sAmount, 2), 3, "0")
        ' 476-615 4*35 O - Details of payment - T15
        'sLine = sLine &
        For Each oInvoice In oPayment.Invoices
            For Each oFreeText In oInvoice.Freetexts
                If bFirstFreetext Then
                    sFreetext = RTrim$(oFreeText.Text)
                    bFirstFreetext = False
                Else
                    sFreetext = sFreetext & " " & oFreeText.Text
                End If
            Next oFreeText
        Next oInvoice
        sLine = sLine & PadRight(ValidateCharacters(sFreetext), 140, " ")
        ' 616-617 O - Instruction code 1 (as per Appendix 2) - Not to be completed for check drawings, (ie for payment type codes 20-23 and 30-33 in field T22) - T16
        'For EUE payments: Only instruction codes ‘10’, ‘11’ and ‘12’ from Appendix 2 permissible
        '* = Implemented (maybe not correctly but a fair try).
        '02 - CHQB - Pay beneficiary customer only by cheque. The optional account number line in field 59 (MT103) must not be used - cannot be used together with 04, 11, 12
        '04 - HOLD - Beneficiary customer/claimant will call; pay upon identification. - 02, 11, 12
        '*06 - PHON - Please advise account with institution by phone. - 07
        '*07 - TELE - Please advise account with institution by the most efficient means of telecommunication. - 06
        '*09 - PHOB - Please advise/contact beneficiary/claimant by phone. - 10
        '*10 - TELB - Please advise/contact beneficiary/claimant by the most efficient means of telecommunication - 09
        '*11 - CORT - Payment is made in settlement of a trade, eg, foreign exchange deal, securities transaction. - 02, 04
        '*12 - INTC - The payment is an intra-company payment, ie, a payment between two companies belonging to the same group. - 02, 04
        '91 -        Euro equivalent payment: (usage permitted only in field T 19, see Appendix 2a)
        'ONLY 1 Possibility to use an instructioncode is implemented ***************************************
        If Not bThisIsAChequePayment Then
            If oPayment.ToOwnAccount Or oPayment.PayCode = "403" Then
                sLine = sLine & "12"
                iInstructionCode = 12
            ElseIf oPayment.ERA_ExchRateAgreed > 0 Then
                sLine = sLine & "11"
                iInstructionCode = 11
            ElseIf oPayment.FRW_ForwardContractRate > 0 Then
                sLine = sLine & "11"
                iInstructionCode = 11
            Else
                If oPayment.NOTI_NotificationType <> "" Then
                    Select Case oPayment.NOTI_NotificationType

                        Case "TELEX"
                            If oPayment.NOTI_NotificationParty = 2 And Not bThisIsAEUEPayment Then
                                sLine = sLine & "07"
                                iInstructionCode = 7
                            Else
                                sLine = sLine & "10"
                                iInstructionCode = 10
                            End If
                        Case "PHONE"
                            If bThisIsAEUEPayment Then
                                iInstructionCode = 10
                            Else
                                If oPayment.NOTI_NotificationParty = 2 Then
                                    sLine = sLine & "06"
                                    iInstructionCode = 6
                                Else
                                    sLine = sLine & "09"
                                    iInstructionCode = 9
                                End If
                            End If
                        Case "OTHER"
                            If oPayment.NOTI_NotificationParty = 2 And Not bThisIsAEUEPayment Then
                                sLine = sLine & "07"
                                iInstructionCode = 7
                            Else
                                sLine = sLine & "10"
                                iInstructionCode = 10
                            End If
                        Case "FAX"
                            If oPayment.NOTI_NotificationParty = 2 And Not bThisIsAEUEPayment Then
                                sLine = sLine & "07"
                                iInstructionCode = 7
                            Else
                                sLine = sLine & "10"
                                iInstructionCode = 10
                            End If
                        Case "EMAIL"
                            If oPayment.NOTI_NotificationParty = 2 And Not bThisIsAEUEPayment Then
                                sLine = sLine & "07"
                                iInstructionCode = 7
                            Else
                                sLine = sLine & "10"
                                iInstructionCode = 10
                            End If

                        Case Else
                            If oPayment.NOTI_NotificationParty = 2 And Not bThisIsAEUEPayment Then
                                sLine = sLine & "07"
                                iInstructionCode = 7
                            Else
                                sLine = sLine & "10"
                                iInstructionCode = 10
                            End If

                    End Select

                Else
                    sLine = sLine & "00"
                End If
            End If
        Else
            sLine = sLine & "00"
        End If
        'sLine = sLine & ' 618-619 O - Instruction code 2 (as per Appendix 2) - Not to be completed for check drawings, (ie for payment type codes 20-23 and 30-33 in field T22) - T17
        ''For EUE payments: Only instruction codes ‘10’, ‘11’ and ‘12’ from Appendix 2 permissible
        'NOT IMPLEMENTED *****************************
        sLine = sLine & "00"
        ' 620-621 O - Instruction code 3 (as per Appendix 2) - Not to be completed for check drawings, (ie for payment type codes 20-23 and 30-33 in field T22) - T18
        ''For EUE payments: Only instruction codes ‘10’, ‘11’ and ‘12’ from Appendix 2 permissible
        'NOT IMPLEMENTED *****************************
        sLine = sLine & "00"
        ' 622-623 O/M - Instruction code 3 (as per Appendix 2 and 2a) - Enter ‘91‘ in the case of "euro-equivalent payments“ (see Appendix 2a)
        ''For cheque drawings (ie for payment type codes 20-23 and 30-33 in field T22), only ‘91’ possible - T19
        ''For EUE payments: Only instruction codes ‘10’, ‘11’ and ‘12’ from Appendix 2 permissible
        'NOT IMPLEMENTED *****************************
        sLine = sLine & "00"

        ' 624-648 O - Additional information on instruction code - For example, telex, telephone number, cable address.
        '(Not to be completed for cheque drawings, ie for payment type codes 20-23 and 30-33 in field T22) - T20
        'For EUE payments: Only permissible for instruction code ‘10’ from Appendix 2
        sTmp = Trim$(oPayment.NOTI_NotificationAttention) & Trim$(oPayment.NOTI_NotificationIdent) & ValidateCharacters(Trim$(oPayment.NOTI_NotificationMessageToBank))

        If Not bThisIsAChequePayment Then
            If Not EmptyString(sTmp) And iInstructionCode > 0 Then
                If bThisIsAEUEPayment Then
                    If iInstructionCode = 10 Then
                        sLine = sLine & PadRight(sTmp, 25, " ")
                    Else
                        sLine = sLine & Space(25)
                    End If
                Else
                    sLine = sLine & PadRight(sTmp, 25, " ")
                End If
            Else
                sLine = sLine & Space(25)
            End If
        Else
            sLine = sLine & Space(25)
        End If

        ' 649-650 O/M - Fee rule -
        ''00 = fees debited to principal third-party fees and expenses debited to beneficiary
        ''01 = all fees and expenses debited to principal
        ''02 = all fees and expenses debited to beneficiary
        ''(For cheque drawings, ie for payment type codes 20-23 and 30-33 in field T22, only ‘00’ is possible) - T21
        ''For EU payments: Only ‘00’ permitted
        If bThisIsAChequePayment Then
            sLine = sLine & "00"
        ElseIf bThisIsAEUEPayment Then
            sLine = sLine & "00"
        ElseIf bThisIsAEUPayment Then
            sLine = sLine & "00"
        Else
            If oPayment.MON_ChargeMeAbroad Then
                If oPayment.MON_ChargeMeDomestic Then
                    sLine = sLine & "01"
                Else
                    'Not possible but you never know
                    sLine = sLine & "00"
                End If
            Else
                If oPayment.MON_ChargeMeDomestic Then
                    sLine = sLine & "00"
                Else
                    sLine = sLine & "02"
                End If
            End If
        End If

        ' 651-652 M - Code for type of payment - As per Appendix 1 Payments which do not contain either ‘11’ or ‘13’ as payment type code are considered general payments. - T22
        'For EU payments: Only payment type code ‘13’ from Appendix 1 permissible
        'For EUE payments: Only payment type code ‘11’ from Appendix 1 permissible
        '00 = Standard transmission (eg letter, standard SWIFT)
        '10 = Telex payment or urgent SWIFT
        '11 = Urgent payment in euro on same day (EUE payment)1
        '13 = EU standard payment, ie cross-border payment under Article 2 a) i) of Regulation (EC)No 2560/2001 of the European Parliament and of the Council of the
        'European Union on cross-border payments in euro, which are in euro up to an amount of EUR 50,000 and in which, pursuant to Article 5 (2),
        'the IBAN of the beneficiary and the BIC of the bank of the beneficiary are mentioned.
        '15 = Cross-border transfer, in accordance with a bilateral agreement with the bank
        '20 = Cheque drawing, any form of dispatch
        '21 = Cheque drawing, sent by registered mail
        '22 = Cheque drawing, sent by special delivery
        '23 = Cheque drawing, sent by registered /express mail
        '30 = Cheque drawing to principal, any form of dispatch
        '31 = Cheque drawing to principal, sent by registered mail
        '32 = Cheque drawing to principal, sent by special delivery
        '33 = Cheque drawing to principal, sent by registered /express mail
        If bThisIsAEUEPayment Then
            sLine = sLine & "11"
        ElseIf bThisIsAEUPayment Then
            ' XOKNET 10.09.2012 - Andreas in DNB DE says 13 is wrong, so we use 00 instead
            'sLine = sLine & "13"
            sLine = sLine & "00"
        ElseIf bThisIsAChequePayment Then
            If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer Then
                sLine = sLine & "30"
            Else
                sLine = sLine & "20"
            End If
        Else
            If oPayment.Priority Then
                sLine = sLine & "10"
            Else
                sLine = sLine & "00"
            End If
        End If

        ' 653-679 O - Variable text only for principal's settlement purposes - Can be completed at principal's discretion (eg reference number).
        'This is not forwarded; use T15 for data to be forwarded. No more than 16 bytes are transmitted to the electronic account statement.
        '(only after consultation with the bank)- T23
        sLine = sLine & PadRight(ValidateCharacters(oPayment.REF_Own), 27, " ")
        ' 680-714 O/M - Name and telephone number and name of deputy, if any - Person to contact at principal's company if paying bank/reporting authority has
        'questions relating to payment order.
        'Then, if principal is not the party liable for payment: ‘INVF’, followed directly (without space) by:
        'the federal state number (2 digits) and the company code or German bank code (8 digits) of party liable for payment - T24
        'For EU payments: Contact person at principal’s company for any queries from commissioned bank
        'NOT IMPLEMENTED *****************************
        sLine = sLine & Space(35)
        ' 715-715 O - Reporting code - Only completed if the payment order data to be reported to the Deutsche Bundesbank are to be limited to statistical data;
        '(these are the data records V, W and Q (excluding field Q4) and the fields 3, 5, 8, 9a, 9b, 10a, 10b, 13, 14a, 14b, 15, 16, 17, 18, 19 and 24 - 27 of data record T).
        'In this case, enter: ‘1’ - T25
        'NOT IMPLEMENTED *****************************
        sLine = sLine & "0"
        ' 716-766 N - Reserve - T26
        sLine = sLine & Space(51)
        ' 767-768 M - Extension identifier - 00 = No further report parts 01 - 08 = Number of report parts (recordtype V and W), 256 bytes each - T26
        'NOT IMPLEMENTED *****************************
        sLine = sLine & "00"

        'nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        WriteTRecord_SingleRecord = sLine

    End Function

    Private Function WriteVRecord_ReportingData() As String
        Dim sLine As String

        sLine = ""

        'NOT IMPLEMENTED *****************************

        'sLine = " 256" '1-4 M ????? Length of record - V1
        'sLine = sLine & "V" '5-5 M - Type of record - Constant "V" - V2
        'sLine = sLine & ' 6-32 M - Designation of merchanting goods purchased - V3
        'sLine = sLine & ' 33-34 M - Chapter number of goods index for purchased merchanting goods - As per classification of goods for the German foreign trade statistics- V4a
        'sLine = sLine & ' 35-41 M - Constant "0000000” - V4b
        'sLine = sLine & ' 42-48 M - Country of purchase merchanting - Brief description as per country index for the balance of payments statistics- V5
        'sLine = sLine & ' 49-51 M - Country code for country of purchase merchanting - Two-letter ISO alpha country code as per country index for the balance of payments statistics;
        ''left aligned; third digit is a space. - V6
        'sLine = sLine & ' 52-63 M - Purchase price merchanting (no decimal places) -
        ''To be given in order currency (see field T13); for euro equivalent payments give the value in euro and enter ‘91’ in field T19. - V7
        'sLine = sLine & ' 64-64 M - Sale of merchanting goods to non-residents (direct merchanting) - Yes (= J) / No (= N) - V8
        'sLine = sLine & ' 65-65 M - Code for sale of merchanting goods to residents (indirect merchanting) - Yes (= J) / No (= N) - V9
        'sLine = sLine & ' 66-66 N - Reserve - V10
        'sLine = sLine & ' 67-67 M - Code: merchanting goods not sold in storage in foreign country - Yes (= J) / No (= N) - V11
        'sLine = sLine & ' 68-94 O/M - Designation of merchanting goods sold - To be completed only for direct merchanting (J in field V8) and if not identical with field V3 - V12
        'sLine = sLine & ' 95-96 O/M - Chapter number of goods index for merchanting goods sold - As per classification of goods for the German foreign trade statistics;
        ''to be completed only for direct merchanting '(J in field V8) and if field V13a is not identical with field V4a - V13a
        'sLine = sLine & ' 97-103 M - Constant "0000000” - V13b
        'sLine = sLine & ' 104-107 O/M - Due date for sales proceeds of merchanting sales - Only for direct merchanting (J in field V8); format: YYMM - V14
        'sLine = sLine & ' 108-114 O/M - Purchasing country merchanting - Short name as per country index for balance of payments statistics;
        ''to be completed only for direct merchanting (J in field V8) - V15
        'sLine = sLine & ' 115-117 O/M - Country code of purchasing country - Two-letter ISO alpha country code as per country index for the balance of payments statistics;
        ''left aligned; third digit is a space; to be completed only if direct merchanting (J in field V8); format: YYMM - V16
        'sLine = sLine & ' 118-129 O/M - Selling price merchanting (no decimal places) - To be completed only if direct merchanting (J in field V8),
        ''to be given in order currency (see field T13); for euro equivalent payments give the value in euro and enter ‘91’ in field T19 - V17
        'sLine = sLine & ' 130-169 O/M - Additional information merchanting - Name and domicile of subsequent buyer in the case of indirect merchanting (J in field V9) - V18
        'sLine = sLine & ' 170-256 N - Reserve - V10

        WriteVRecord_ReportingData = sLine

    End Function
    Private Function WriteWRecord_ReportingData() As String
        Dim sLine As String

        sLine = ""

        'NOT IMPLEMENTED *****************************

        'sLine = " 256" '1-4 M ????? Length of record - W1
        'sLine = sLine & "W" '5-5 M - Type of record - Constant "W" - W2
        'sLine = sLine & ' 6-6 M - Type of transaction - Services, transfers = ‘2’ Financial transactions and capital yield = ‘4’ - W3
        'sLine = sLine & ' 7-9 M - Code number - As per coding list (Annex LV to the Foreign Trade and Payments Regulation) - W4
        'sLine = sLine & ' 10-16 M - Country - Short name as per country index for the balance of payments statistics (see Appendix 3, part E) - W5
        'sLine = sLine & ' 17-19 M - Country code - Two-letter ISO alpha country code as per country index for the balance of payments statistics;
        ''(Appendix 3, part E); left aligned; third digit is a space - W6
        'sLine = sLine & ' 20-26 M - Investment country/ financial transactions - Short name as per country index for the balance of payments statistics - W7
        'sLine = sLine & ' 27-29 M - Country code/ investment country -
        ''Two-letter ISO alpha country code as per country index for the balance of payments statistics; left aligned; third digit is a space - W8
        'sLine = sLine & ' 30-41 M - Amount for services, transfers and financial transactions (no decimal places) -
        ''To be given in order currency (see field T13); for euro equivalent payments give the value in euro and enter ‘91’ in field T19. - W9
        'sLine = sLine & ' 42-181 M - Details of underlying transaction - Important features of underlying transaction - W10
        'sLine = sLine & ' 182-256 N - Reserve - W11

        WriteWRecord_ReportingData = sLine

    End Function
    Private Function WriteZRecord_Trailer() As String
        Dim sLine As String


        sLine = ""

        ' 1-4 M Length of record - W1
        sLine = "0256"
        ' 5-5 M - Type of record - Constant "Z" - Z2
        sLine = sLine & "Z"
        ' 6-20 M - Sum total of all amounts (no decimal places) - Sum of all amounts in field T14a (all currencies) - Z3
        sLine = sLine & PadLeft(Trim$(Str(nFileSumAmount)), 15, "0")
        ' 21-35 M - Number of T data records - Z4
        sLine = sLine & PadLeft(Trim$(Str(nFileNoRecords)), 15, "0")
        ' 36-256 N - Reserve - Z5
        sLine = sLine & Space(221)

        WriteZRecord_Trailer = sLine

    End Function
    Private Function WriteDTAZVRecord(ByVal oFile As Scripting.TextStream, ByVal sString As String) As Boolean

        'XNET - 30.12.2010 - Added next IF
        If Not RunTime() Then
            If Val(Left(sString, 4)) <> Len(sString) Then
                Err.Raise(1234, "WriteDTAZWRecord", "Feil lengde på record" & vbCrLf & sString)
            End If
        End If

        If lPositionsWritten + Len(sString) > 32000 Then
            oFile.WriteLine(Left$(sString, 32000 - lPositionsWritten))
            oFile.Write(Mid$(sString, 32000 + 1 - lPositionsWritten))
            lPositionsWritten = Len(Mid$(sString, 32000 + 1 - lPositionsWritten))
        Else
            lPositionsWritten = lPositionsWritten + Len(sString)
            If lPositionsWritten = 32000 Then
                oFile.WriteLine(sString)
                lPositionsWritten = 0
            Else
                oFile.Write(sString)
            End If
        End If

        WriteDTAZVRecord = True

    End Function

    Private Function ValidateCharacters(ByVal s As String) As String
        Dim sReturnValue As String

        s = UCase(s)

        sReturnValue = CheckForValidCharacters(s, False, True, False, True, " .,-/+", True)

        ValidateCharacters = sReturnValue

    End Function

End Module
