﻿Public Class frmViewFilter
    Dim grid As DataGridView
    Dim oBabelFiles As BabelFiles
    Const ShowAll = 1
    Const ShowNextOnly = 0
    Private Sub frmViewSearch_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim working_area As Rectangle = SystemInformation.WorkingArea
        Dim x As Integer = working_area.Left + working_area.Width - Me.Width
        Dim y As Integer = working_area.Top + working_area.Height - Me.Height - 50

        Me.Location = New Point(x, y)  'Position to lower right corner
        FormLRSCaptions(Me)
        Me.txtSearchFor.Focus()
    End Sub
    Public Sub passGrid(ByVal g As DataGridView)
        grid = g
    End Sub
    Public Sub SetBabelFilesInViewSearch(ByVal obj As BabelFiles)
        ' pass BabelFilesbject to frmViewPaymentDetail
        oBabelFiles = obj
    End Sub

    Private Sub DoTheSearch(ByVal iMode As Integer)
        Dim sSearchFor As String
        Dim x As Integer
        Dim y As Integer = grid.CurrentCell.ColumnIndex + 1
        Dim iCurrentCol As Integer = grid.CurrentCell.ColumnIndex
        Dim iCurrentRow As Integer = grid.CurrentRow.Index
        Static iLastX As Integer = 0
        Static iLastY As Integer = 0
        Dim c As DataGridViewCell
        Dim bFound As Boolean = False
        Dim sCellValue As String = ""

        If Me.chkWrap.Checked Or iMode = ShowAll Or chkFilter.Checked Then
            x = 0
            y = 0
        Else
            x = grid.CurrentRow.Index
        End If

        sSearchFor = UCase(Trim(Me.txtSearchFor.Text))
        If iLastX > 0 Then
            ' reset font from last found
            grid.Rows(iLastX).Cells(iLastY).Style.Font = (New Font(Me.Font, FontStyle.Regular))
            iLastX = 0
            iLastY = 0
        End If

        While x < grid.Rows.Count
            While y < grid.Rows(x).Cells.Count
                bFound = False
                c = grid.Rows(x).Cells(y)
                If c.Visible Then
                    If Not c.Value Is DBNull.Value Or Nothing Then
                        sCellValue = UCase(CType(c.Value, String))
                        If Me.chkFromStartOfCell.Checked Then
                            ' search from start of columncontent
                            If Strings.Left(sCellValue, Len(sSearchFor)) = sSearchFor Then
                                bFound = True
                            End If
                        Else
                            ' search inside field
                            If InStr(sCellValue, sSearchFor) > 0 Then
                                bFound = True
                            End If
                        End If
                        If bFound And (Me.chkCurrenctColumn.Checked = False Or (y = iCurrentCol)) Then
                            ' positon to cell
                            grid.CurrentCell = grid(y, x)
                            grid.Rows(x).Cells(y).Selected = True
                            iLastX = x
                            iLastY = y
                            ' change to bold
                            grid.Rows(x).Cells(y).Style.Font = (New Font(Me.Font, FontStyle.Bold))
                            grid.Focus()
                            If iMode = ShowNextOnly Or chkFilter.Checked Then
                                Exit While
                            End If
                        Else
                            bFound = False
                        End If
                    End If
                End If
                System.Math.Min(System.Threading.Interlocked.Increment(y), y - 1)
            End While
            If bFound Then
                If Not Me.chkFilter.Checked Then  ' do not jump out if filter or show all
                    If iMode = ShowNextOnly Then
                        Exit While
                    End If
                Else
                    ' TODO
                    '---------
                    'CountMatchedItems filtered itens
                    'CountMatchedItems(amount)

                    'batch: click i batch checkbox skal ikke hoppe til payment
                    '    CheckBox i columnsheader for å merke alle
                    'sjekk checkboxer ved hopp til payment

                    grid.Rows(x).Visible = True
                    ' to report, set a marker that this one will be reported
                    oBabelFiles(Me.grid.Rows(x).Cells(0)).Batches(Me.grid.Rows(x).Cells(1)).Payments(Me.grid.Rows(x).Cells(2)).ToSpecialReport = True
                End If
            Else
                If Me.chkFilter.Checked Then
                    grid.Rows(x).Visible = False
                    ' to report, set a marker that this one will NOT be reported
                    oBabelFiles(Me.grid.Rows(x).Cells(0).Value).Batches(Me.grid.Rows(x).Cells(1).Value).Payments(Me.grid.Rows(x).Cells(2).Value).ToSpecialReport = False
                End If
            End If
            System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
            y = 0
        End While
        ' position back if not found
        If Not bFound Then
            iLastX = 0
            iLastY = 0
            If Not Me.chkFilter.Checked Then  ' do not jump out if filterThen
                grid.CurrentCell = grid(iCurrentCol, iCurrentRow)
                grid.Rows(iCurrentRow).Cells(iCurrentCol).Selected = True
            Else
                'grid.CurrentCell = grid(y, x) ' last found
                'grid.Rows(x).Cells(y).Selected = True
            End If
            ' TODO position grid to show selected line on middle
            grid.Focus()
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        DoTheSearch(0)  ' next only
    End Sub

    Private Sub cmdAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAll.Click
        ' if filter, show the filter in label
        If Me.chkFilter.Checked Then
            Me.lblFilter.Text = "Filter: " & Replace(Me.lblFilter.Text, "Filter:", "") & " " & Trim(Me.txtSearchFor.Text)
        End If
        DoTheSearch(1)  ' all
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Private Sub txtSearch_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtSearchFor.KeyUp
        Try
            If e.KeyCode = Keys.Enter Then
                cmdNext_Click(sender, e)
            End If
            If e.KeyCode = Keys.Escape Then
                Me.Close()
                Me.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub chkFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFilter.CheckedChanged
        If chkFilter.Checked Then
            Me.cmdAll.Text = "0000Sett filter"
            Me.cmdNext.Visible = False
            Me.cmdRemoveFilter.Visible = True
        Else
            Me.cmdAll.Text = "0000Søk alle"
            Me.cmdNext.Visible = True
            Me.cmdRemoveFilter.Visible = False
            cmdRemoveFilter_Click(sender, e)
        End If
    End Sub
    Private Sub cmdRemoveFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveFilter.Click
        ' run through all lines, and set visible = true
        Dim x, y As Integer
        Dim iCurrentCol As Integer = grid.CurrentCell.ColumnIndex
        Dim iCurrentRow As Integer = grid.CurrentRow.Index

        Me.lblFilter.Text = ""
        If grid.CurrentCell.ColumnIndex < 0 Then
            iCurrentCol = 0
        Else
            iCurrentCol = grid.CurrentCell.ColumnIndex
        End If
        If grid.CurrentRow.Index < 0 Then
            iCurrentRow = 0
        Else
            iCurrentRow = grid.CurrentRow.Index
        End If

        While x < grid.Rows.Count
            grid.Rows(x).Visible = True
            y = 0
            While Y < grid.Rows(x).Cells.Count
                grid.Rows(x).Cells(y).Style.Font = (New Font(Me.Font, FontStyle.Regular))
                System.Math.Min(System.Threading.Interlocked.Increment(y), y - 1)
            End While
            System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
        End While
        ' position back if not found
        grid.CurrentCell = grid(iCurrentCol, iCurrentRow)
        grid.Rows(iCurrentRow).Cells(iCurrentCol).Selected = True
        grid.Focus()

    End Sub
End Class