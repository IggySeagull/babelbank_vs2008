Option Strict Off
Option Explicit On
'.Printing.Compatibility.VB6
Friend Class frmTextView
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdPrint_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPrint.Click
        'Dim Printer As New Printer
        'On Error GoTo PrintError
        'Printer.Font = VB6.FontChangeName(Printer.Font, "Arial")
        'Printer.Font = VB6.FontChangeSize(Printer.Font, 10)
        'Printer.Print(vbCrLf)
        'Printer.Print(vbCrLf)
        'Printer.Print(Me.Text)
        'Printer.Print(vbCrLf)
        'Printer.Print(Me.txtFile.Text)
        'Printer.EndDoc()
		
		Exit Sub
		
PrintError: 
		' LRS(10018) ' 10018: Unable to print to default printer
		Err.Raise(Err.Number,  , LRS(10018) & vbCrLf & Err.Description)
		
	End Sub
	
	Private Sub frmTextView_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		' Localize:
		cmdPrint.Text = LRS(55003)
		cmdClose.Text = LRS(55007)
		FormvbStyle(Me, "") ' Make vb-style
		
	End Sub
	Private Sub cmdClose_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdClose.Click
		Me.Close()
    End Sub

End Class
