﻿Option Strict Off
Option Explicit On
Imports System.Data.Common
Imports System.Net
'Imports Microsoft.Identity.Client

Public Class DAL

    Private ConnERP As System.Data.Common.DbConnection
    Private readerCommand As System.Data.Common.DbCommand = Nothing
    Private readerERP As System.Data.Common.DbDataReader = Nothing
    Private readerWCF As ReaderWCF = Nothing
    Private myCommand As DbCommand = Nothing
    Private MyTrans As DbTransaction = Nothing
    Private cmdParms As DbParameter()

    Private bTestMode As Boolean
    Private eTypeOfDB As DatabaseType = DatabaseType.BBDB
    Private sERPUID As String
    Private sERPPWD As String
    Private sERPClientNo As String
    Private sERPConnectionString As String
    Private eProviderType As vbBabel.DAL.ProviderType
    Private sMySQL As String
    Private bReader_HasRows As Boolean = False
    Private iRecordsAffected As Integer
    Private iRecordCounter As Integer

    Private aReturnValues(,) As String
    Private iYIndex As Integer

    Private iCompany_ID As Integer
    Private iDBProfile_ID As Integer

    Private TEST_bUseCommitFromOutside As Boolean = False
    Private TEST_bTransactionSet As Boolean = False

    Private sErrorMessage As String = ""
    Private oException As System.Exception = Nothing
    Private lErrorNumber As Long = 0
	Private bShowErrorMessage As Boolean = False
	Private eMatchingType As vbBabel.DAL.MatchingType = MatchingType.Not_Specified
    Private iFieldType As Integer = FieldType_Unknown
    Private bDebug As Boolean = False
    Private bThisIsOracle As Boolean = False

    Private bSQLServer As Boolean
    Private sNameOfSearch As String = ""  ' added 27.12.2017 for WCF use
    Private sCompanyName As String = ""  ' added 27.12.2017 for WCF use
    Private sCompanyCode As String = ""  ' added 09.03.2021 for WCF use - if we need a "technical" code for the web service
    Private iConnectionTimeOut As Integer
    'Private sSQLServerTest As String

    Public Const constBBRET_ClientNo As Integer = 1
    Public Const constBBRET_CustomerNo As Integer = 2
    Public Const constBBRET_InvoiceNo As Integer = 3
    Public Const constBBRET_Amount As Integer = 4
    Public Const constBBRET_Currency As Integer = 5
    Public Const constBBRET_MatchID As Integer = 6
    Public Const constBBRET_MyField As Integer = 7
    Public Const constBBRET_Name As Integer = 8
    Public Const constBBRET_Address As Integer = 9
    Public Const constBBRET_Address2 As Integer = 10
    Public Const constBBRET_ZIP As Integer = 11
    Public Const constBBRET_Freetext As Integer = 12
    Public Const constBBRET_DiscountAmount As Integer = 13
    Public Const constBBRET_DiscountDate As Integer = 14
    Public Const constBBRET_AKontoID As Integer = 15
    Public Const constBBRET_GLID As Integer = 16
    Public Const constBBRET_VATCode As Integer = 17
    Public Const constBBRET_String1 As Integer = 21
    Public Const constBBRET_String2 As Integer = 22
    Public Const constBBRET_String3 As Integer = 23
    Public Const constBBRET_Double1 As Integer = 24
    Public Const constBBRET_Double2 As Integer = 25
    Public Const constBBRET_Date1 As Integer = 26
    Public Const constBBRET_MyField2 As Integer = 27
    Public Const constBBRET_MyField3 As Integer = 28
    Public Const constBBRET_Dim1 As Integer = 29
    Public Const constBBRET_Dim2 As Integer = 30
    Public Const constBBRET_Dim3 As Integer = 31
    Public Const constBBRET_Dim4 As Integer = 32
    Public Const constBBRET_Dim5 As Integer = 33
    Public Const constBBRET_Dim6 As Integer = 34
    Public Const constBBRET_Dim7 As Integer = 35
    Public Const constBBRET_Dim8 As Integer = 36
    Public Const constBBRET_Dim9 As Integer = 37
    Public Const constBBRET_Dim10 As Integer = 38

    Public Const Reader_NoRecords As Integer = 0
    Public Const Reader_OneRecord As Integer = 1
    Public Const Reader_MultippelRecords As Integer = 2

    Public Const FieldType_Unknown As Integer = 0
    Public Const FieldType_String As Integer = 1
    Public Const FieldType_Integer As Integer = 11
    Public Const FieldType_Double As Integer = 12
    Public Const FieldType_Short As Integer = 13
    Public Const FieldType_Decimal As Integer = 14
    Public Const FieldType_BigInt As Integer = 15
    Public Const FieldType_Boolean As Integer = 21
    Public Const FieldType_Byte As Integer = 22
    Public Const FieldType_Date As Integer = 31
    Public Const FieldType_Value As Integer = 41

    Private sBBRET_CustomerNo As String
    Private sBBRET_ClientNo As String
    Private sBBRET_InvoiceNo As String
    Private nBBRET_Amount As Double
    Private sBBRET_Currency As String
    Private sBBRET_MatchID As String
    Private sBBRET_MyField As String
    Private sBBRET_MyField2 As String
    Private sBBRET_MyField3 As String
    Private sBBRET_Dim1 As String
    Private sBBRET_Dim2 As String
    Private sBBRET_Dim3 As String
    Private sBBRET_Dim4 As String
    Private sBBRET_Dim5 As String
    Private sBBRET_Dim6 As String
    Private sBBRET_Dim7 As String
    Private sBBRET_Dim8 As String
    Private sBBRET_Dim9 As String
    Private sBBRET_Dim10 As String
    Private sBBRET_Name As String
    Private sBBRET_Address As String
    Private sBBRET_Address2 As String
    Private sBBRET_ZIP As String
    Private sBBRET_Freetext As String
    Private nBBRET_DiscountAmount As Double
    Private sBBRET_DiscountDate As String
    Private sBBRET_AKontoID As String
    Private sBBRET_GLID As String
    Private sBBRET_VATCode As String
    Private sBBRET_String1 As String
    Private sBBRET_String2 As String
    Private sBBRET_String3 As String
    Private nBBRET_Double1 As Double
    Private nBBRET_Double2 As Double
    Private dBBRET_Date1 As Date

    Private sTemp_CustomerNo As String
    Private sTemp_ClientNo As String
    Private sTemp_InvoiceNo As String
    Private nTemp_Amount As Double
    Private sTemp_Currency As String
    Private sTemp_MatchID As String
    Private sTemp_MyField As String
    Private sTemp_MyField2 As String
    Private sTemp_MyField3 As String
    Private sTemp_Dim1 As String
    Private sTemp_Dim2 As String
    Private sTemp_Dim3 As String
    Private sTemp_Dim4 As String
    Private sTemp_Dim5 As String
    Private sTemp_Dim6 As String
    Private sTemp_Dim7 As String
    Private sTemp_Dim8 As String
    Private sTemp_Dim9 As String
    Private sTemp_Dim10 As String
    Private sTemp_Name As String
    Private sTemp_Address As String
    Private sTemp_Address2 As String
    Private sTemp_ZIP As String
    Private sTemp_Freetext As String
    Private nTemp_DiscountAmount As Double
    Private sTemp_DiscountDate As String
    Private sTemp_AKontoID As String
    Private sTemp_GLID As String
    Private sTemp_VATCode As String
    Private sTemp_String1 As String
    Private sTemp_String2 As String
    Private sTemp_String3 As String
    Private nTemp_Double1 As Double
    Private nTemp_Double2 As Double
    Private dTemp_Date1 As Date

    Private sMarked_CustomerNo As String
    Private sMarked_ClientNo As String
    Private sMarked_InvoiceNo As String
    Private nMarked_Amount As Double
    Private sMarked_Currency As String
    Private sMarked_MatchID As String
    Private sMarked_MyField As String
    Private sMarked_MyField2 As String
    Private sMarked_MyField3 As String
    Private sMarked_Dim1 As String
    Private sMarked_Dim2 As String
    Private sMarked_Dim3 As String
    Private sMarked_Dim4 As String
    Private sMarked_Dim5 As String
    Private sMarked_Dim6 As String
    Private sMarked_Dim7 As String
    Private sMarked_Dim8 As String
    Private sMarked_Dim9 As String
    Private sMarked_Dim10 As String
    Private sMarked_Name As String
    Private sMarked_Address As String
    Private sMarked_Address2 As String
    Private sMarked_ZIP As String
    Private sMarked_Freetext As String
    Private nMarked_DiscountAmount As Double
    Private sMarked_DiscountDate As String
    Private sMarked_AKontoID As String
    Private sMarked_GLID As String
    Private sMarked_VATCode As String
    Private sMarked_String1 As String
    Private sMarked_String2 As String
    Private sMarked_String3 As String
    Private nMarked_Double1 As Double
    Private nMarked_Double2 As Double
    Private dMarked_Date1 As Date

    Private bFirstRecordRead As Boolean
    Private bLastRecordRead As Boolean
    Private lNumberOfRecordsRead As Long

    'To be removed
    '    Private ixArray As Integer

    Public Enum ProviderType
        OleDb = 0
        Odbc = 1
        OracleClient = 2
        SqlClient = 3
        SqlServerCe_3_5 = 4
        WCF = 5
    End Enum
    Public Enum DatabaseType
        BBDB = 0
        ERPDB = 1
        ArchiveDB = 2
    End Enum
    Public Enum MatchingType
        Not_Specified = 0
        Automatic = 1
        Manual = 2
        After = 3
        Validation = 4
    End Enum
    Public WriteOnly Property TypeOfMatching() As MatchingType
        'Default false
        Set(ByVal Value As MatchingType)
            eMatchingType = Value
        End Set
    End Property
    Public WriteOnly Property TypeOfDB() As DatabaseType
        'Default false
        Set(ByVal Value As DatabaseType)
            eTypeOfDB = Value
        End Set
    End Property
    Public ReadOnly Property ErrorMessage() As String
        Get
            If lErrorNumber <> 0 Then
                ErrorMessage = lErrorNumber.ToString & ": " & sErrorMessage
            Else
                ErrorMessage = sErrorMessage
            End If
        End Get
    End Property
    Public ReadOnly Property InnerException() As System.Exception
        Get
            'InnerException = New System.Exception
            InnerException = oException
        End Get
    End Property
    Public ReadOnly Property ErrorNumber() As Long
        Get
            ErrorNumber = lErrorNumber
        End Get
    End Property
    Public WriteOnly Property Debug() As Boolean
        'Default false
        Set(ByVal Value As Boolean)
            bDebug = Value
        End Set
    End Property
    ' added 27.12.2017 for WCF use
    Public WriteOnly Property NameOfSearch() As String
        Set(ByVal Value As String)
            sNameOfSearch = Value
        End Set
    End Property
    ' added 27.12.2017 for WCF use
    Public WriteOnly Property NameOfCompany() As String
        Set(ByVal Value As String)
            sCompanyName = Value
        End Set
    End Property
    Public WriteOnly Property CodeOfCompany() As String
        Set(ByVal Value As String)
            sCompanyCode = Trim(Value)
        End Set
    End Property
    '08.03.2021 - Added Connection TimeOut
    Public WriteOnly Property ConnectionTimeOut() As Integer
        'Default for connectionTimeOut on Kjell's PC when connecting to SQL Server for Visma is 30!
        'The value 0 will signify use defaultvalue
        Set(ByVal Value As Integer)
            iConnectionTimeOut = Value
        End Set
    End Property
    Public WriteOnly Property ShowErrorMessage() As Boolean
        'Default false
        Set(ByVal Value As Boolean)
            bShowErrorMessage = Value
        End Set
    End Property
    Public WriteOnly Property TestMode() As Boolean
        Set(ByVal Value As Boolean)
            bTestMode = Value
        End Set
    End Property
	Public Property Provider() As vbBabel.DAL.ProviderType
		Get
			Provider = eProviderType
		End Get
		Set(ByVal Value As vbBabel.DAL.ProviderType)
			eProviderType = Value
		End Set
	End Property
	Public Property Connectionstring() As String
        Get
            Connectionstring = sERPConnectionString
        End Get
        Set(ByVal Value As String)
            sERPConnectionString = Value
        End Set
    End Property
    Public WriteOnly Property UserID() As String
        Set(ByVal Value As String)
            sERPUID = Value
        End Set
    End Property
    Public WriteOnly Property Password() As String
        Set(ByVal Value As String)
            sERPPWD = Value
        End Set
    End Property
    Public WriteOnly Property ERPClientNo() As String
        Set(ByVal Value As String)
            sERPClientNo = Value
        End Set
    End Property
    Public ReadOnly Property SQLServer() As Boolean
        Get
            SQLServer = bSQLServer
        End Get
    End Property
    Public Property SQL() As String
        Get
            SQL = sMySQL
        End Get
        Set(ByVal Value As String)

            sMySQL = Value

            If bSQLServer Then 'SQQLSERVER
                sMySQL = sMySQL.Replace("False", "0")
                sMySQL = sMySQL.Replace("false", "0")
                sMySQL = sMySQL.Replace("True", "1")
                sMySQL = sMySQL.Replace("true", "1")
            End If

        End Set
    End Property
    Public WriteOnly Property Company_ID() As Integer
        Set(ByVal Value As Integer)
            iCompany_ID = Value
        End Set
    End Property
    Public WriteOnly Property DBProfile_ID() As Integer
        Set(ByVal Value As Integer)
            iDBProfile_ID = Value
        End Set
    End Property
    Public WriteOnly Property TEST_UseCommitFromOutside() As Boolean
        Set(ByVal Value As Boolean)
            TEST_bUseCommitFromOutside = Value
        End Set
    End Property
    Public ReadOnly Property Reader_HasRows() As Boolean
        Get
            Reader_HasRows = bReader_HasRows
        End Get
    End Property
    Public ReadOnly Property Reader_Count() As Integer

        Get
            If Not bReader_HasRows Then
                Reader_Count = Reader_NoRecords
            ElseIf lNumberOfRecordsRead = 1 Then
                Reader_Count = Reader_OneRecord
            Else
                Reader_Count = Reader_MultippelRecords
            End If
        End Get
    End Property
    Public ReadOnly Property Reader_FieldCount() As Integer
        Get
            If eProviderType = ProviderType.WCF Then
                '21.06.2021 _ Added next If
                If Not readerWCF Is Nothing Then
                    If Not readerWCF.HasRows Then
                        Reader_FieldCount = -1
                    Else
                        Reader_FieldCount = readerWCF.FieldCount - 1
                    End If
                Else
                    Reader_FieldCount = -1
                End If
            Else
                If Not readerERP.HasRows Then
                    Reader_FieldCount = -1
                Else
                    Reader_FieldCount = readerERP.FieldCount - 1
                End If
            End If
        End Get
    End Property
    Public ReadOnly Property RecordsAffected() As Integer
        Get
            RecordsAffected = iRecordsAffected
        End Get
    End Property
    Public ReadOnly Property Reader_RecordCounter() As Integer
        Get
            Reader_RecordCounter = iRecordCounter
        End Get
    End Property
    Public ReadOnly Property ClientNo() As String
        Get
            ClientNo = sBBRET_ClientNo
        End Get
    End Property
    Public ReadOnly Property ClientNoInUse() As Boolean
        Get
            If aReturnValues(constBBRET_ClientNo, iYIndex) > -1 Then
                ClientNoInUse = True
            Else
                ClientNoInUse = False
            End If
        End Get
    End Property
    Public ReadOnly Property CustomerNo() As String
        Get
            CustomerNo = sBBRET_CustomerNo
        End Get
    End Property
    Public ReadOnly Property InvoiceNo() As String
        Get
            InvoiceNo = sBBRET_InvoiceNo
        End Get
    End Property
    Public ReadOnly Property Amount() As Double
        Get
            Amount = nBBRET_Amount
        End Get
    End Property
    Public ReadOnly Property Currency() As String
        Get
            Currency = sBBRET_Currency
        End Get
    End Property
    Public ReadOnly Property CurrencyInUse() As Boolean
        Get
            If aReturnValues(constBBRET_Currency, iYIndex) > -1 Then
                CurrencyInUse = True
            Else
                CurrencyInUse = False
            End If
        End Get
    End Property
    Public ReadOnly Property MatchID() As String
        Get
            MatchID = sBBRET_MatchID
        End Get
    End Property
    Public ReadOnly Property MatchIDInUse() As Boolean
        Get
            If aReturnValues(constBBRET_MatchID, iYIndex) > -1 Then
                MatchIDInUse = True
            Else
                MatchIDInUse = False
            End If
        End Get
    End Property
    Public ReadOnly Property MyField() As String
        Get
            MyField = sBBRET_MyField
        End Get
    End Property
    Public ReadOnly Property MyField2() As String
        Get
            MyField2 = sBBRET_MyField2
        End Get
    End Property
    Public ReadOnly Property MyField3() As String
        Get
            MyField3 = sBBRET_MyField3
        End Get
    End Property
    Public ReadOnly Property Dim1() As String
        Get
            Dim1 = sBBRET_Dim1
        End Get
    End Property
    Public ReadOnly Property Dim2() As String
        Get
            Dim2 = sBBRET_Dim2
        End Get
    End Property
    Public ReadOnly Property Dim3() As String
        Get
            Dim3 = sBBRET_Dim3
        End Get
    End Property
    Public ReadOnly Property Dim4() As String
        Get
            Dim4 = sBBRET_Dim4
        End Get
    End Property
    Public ReadOnly Property Dim5() As String
        Get
            Dim5 = sBBRET_Dim5
        End Get
    End Property
    Public ReadOnly Property Dim6() As String
        Get
            Dim6 = sBBRET_Dim6
        End Get
    End Property
    Public ReadOnly Property Dim7() As String
        Get
            Dim7 = sBBRET_Dim7
        End Get
    End Property
    Public ReadOnly Property Dim8() As String
        Get
            Dim8 = sBBRET_Dim8
        End Get
    End Property
    Public ReadOnly Property Dim9() As String
        Get
            Dim9 = sBBRET_Dim9
        End Get
    End Property
    Public ReadOnly Property Dim10() As String
        Get
            Dim10 = sBBRET_Dim10
        End Get
    End Property
    Public ReadOnly Property Name() As String
        Get
            Name = sBBRET_Name
        End Get
    End Property
    Public ReadOnly Property Address() As String
        Get
            Address = sBBRET_Address
        End Get
    End Property
    Public ReadOnly Property Adr2() As String
        Get
            Adr2 = sBBRET_Address2
        End Get
    End Property
    Public ReadOnly Property ZIP() As String
        Get
            ZIP = sBBRET_ZIP
        End Get
    End Property
    Public ReadOnly Property Freetext() As String
        Get
            Freetext = sBBRET_Freetext
        End Get
    End Property
    Public ReadOnly Property DiscountAmount() As Double
        Get
            DiscountAmount = nBBRET_DiscountAmount
        End Get
    End Property
    Public ReadOnly Property DiscountAmountInUse() As Boolean
        Get
            If aReturnValues(constBBRET_DiscountAmount, iYIndex) > -1 Then
                DiscountAmountInUse = True
            Else
                DiscountAmountInUse = False
            End If
        End Get
    End Property
    Public ReadOnly Property DiscountDate() As String
        Get
            DiscountDate = sBBRET_DiscountDate
        End Get
    End Property
    Public ReadOnly Property DiscountDateInUse() As Boolean
        Get
            If aReturnValues(constBBRET_DiscountDate, iYIndex) > -1 Then
                DiscountDateInUse = True
            Else
                DiscountDateInUse = False
            End If
        End Get
    End Property
    Public ReadOnly Property AKontoID() As String
        Get
            AKontoID = sBBRET_AKontoID
        End Get
    End Property
    Public ReadOnly Property AkontoIDInUse() As Boolean
        Get
            If aReturnValues(constBBRET_AKontoID, iYIndex) > -1 Then
                AkontoIDInUse = True
            Else
                AkontoIDInUse = False
            End If
        End Get
    End Property
    Public ReadOnly Property GLID() As String
        Get
            GLID = sBBRET_GLID
        End Get
    End Property
    Public ReadOnly Property VATCode() As String
        Get
            VATCode = sBBRET_VATCode
        End Get
    End Property
    Public ReadOnly Property String1() As String
        Get
            String1 = sBBRET_String1
        End Get
    End Property
    Public ReadOnly Property String2() As String
        Get
            String2 = sBBRET_String2
        End Get
    End Property
    Public ReadOnly Property String3() As String
        Get
            String3 = sBBRET_String3
        End Get
    End Property
    Public ReadOnly Property Double1() As Double
        Get
            Double1 = nBBRET_Double1
        End Get
    End Property
    Public ReadOnly Property Double2() As Double
        Get
            Double2 = nBBRET_Double2
        End Get
    End Property
    Public ReadOnly Property Date1() As Date
        Get
            Date1 = dBBRET_Date1
        End Get
    End Property
    Public ReadOnly Property MarkedClientNo() As String
        Get
            MarkedClientNo = sMarked_ClientNo
        End Get
    End Property
    Public ReadOnly Property MarkedCustomerNo() As String
        Get
            MarkedCustomerNo = sMarked_CustomerNo
        End Get
    End Property
    Public ReadOnly Property MarkedInvoiceNo() As String
        Get
            MarkedInvoiceNo = sMarked_InvoiceNo
        End Get
    End Property
    Public ReadOnly Property MarkedAmount() As Double
        Get
            MarkedAmount = nMarked_Amount
        End Get
    End Property
    Public ReadOnly Property MarkedCurrency() As String
        Get
            MarkedCurrency = sMarked_Currency
        End Get
    End Property
    Public ReadOnly Property MarkedMatchID() As String
        Get
            MarkedMatchID = sMarked_MatchID
        End Get
    End Property
    Public ReadOnly Property MarkedMyField() As String
        Get
            MarkedMyField = sMarked_MyField
        End Get
    End Property
    Public ReadOnly Property MarkedMyField2() As String
        Get
            MarkedMyField2 = sMarked_MyField2
        End Get
    End Property
    Public ReadOnly Property MarkedMyField3() As String
        Get
            MarkedMyField3 = sMarked_MyField3
        End Get
    End Property
    Public ReadOnly Property MarkedDim1() As String
        Get
            MarkedDim1 = sMarked_Dim1
        End Get
    End Property
    Public ReadOnly Property MarkedDim2() As String
        Get
            MarkedDim2 = sMarked_Dim2
        End Get
    End Property
    Public ReadOnly Property MarkedDim3() As String
        Get
            MarkedDim3 = sMarked_Dim3
        End Get
    End Property
    Public ReadOnly Property MarkedDim4() As String
        Get
            MarkedDim4 = sMarked_Dim4
        End Get
    End Property
    Public ReadOnly Property MarkedDim5() As String
        Get
            MarkedDim5 = sMarked_Dim5
        End Get
    End Property
    Public ReadOnly Property MarkedDim6() As String
        Get
            MarkedDim6 = sMarked_Dim6
        End Get
    End Property
    Public ReadOnly Property MarkedDim7() As String
        Get
            MarkedDim7 = sMarked_Dim7
        End Get
    End Property
    Public ReadOnly Property MarkedDim8() As String
        Get
            MarkedDim8 = sMarked_Dim8
        End Get
    End Property
    Public ReadOnly Property MarkedDim9() As String
        Get
            MarkedDim9 = sMarked_Dim9
        End Get
    End Property
    Public ReadOnly Property MarkedDim10() As String
        Get
            MarkedDim10 = sMarked_Dim10
        End Get
    End Property
    Public ReadOnly Property MarkedName() As String
        Get
            MarkedName = sMarked_Name
        End Get
    End Property
    Public ReadOnly Property MarkedAddress() As String
        Get
            MarkedAddress = sMarked_Address
        End Get
    End Property
    Public ReadOnly Property MarkedAdr2() As String
        Get
            MarkedAdr2 = sMarked_Address2
        End Get
    End Property
    Public ReadOnly Property MarkedZIP() As String
        Get
            MarkedZIP = sMarked_ZIP
        End Get
    End Property
    Public ReadOnly Property MarkedFreetext() As String
        Get
            MarkedFreetext = sMarked_Freetext
        End Get
    End Property
    Public ReadOnly Property MarkedDiscountAmount() As Double
        Get
            MarkedDiscountAmount = nMarked_DiscountAmount
        End Get
    End Property
    Public ReadOnly Property MarkedDiscountDate() As String
        Get
            MarkedDiscountDate = sMarked_DiscountDate
        End Get
    End Property
    Public ReadOnly Property MarkedAKontoID() As String
        Get
            MarkedAKontoID = sMarked_AKontoID
        End Get
    End Property
    Public ReadOnly Property MarkedGLID() As String
        Get
            MarkedGLID = sMarked_GLID
        End Get
    End Property
    Public ReadOnly Property MarkedVATCode() As String
        Get
            MarkedVATCode = sMarked_VATCode
        End Get
    End Property
    Public ReadOnly Property MarkedString1() As String
        Get
            MarkedString1 = sMarked_String1
        End Get
    End Property
    Public ReadOnly Property MarkedString2() As String
        Get
            MarkedString2 = sMarked_String2
        End Get
    End Property
    Public ReadOnly Property MarkedString3() As String
        Get
            MarkedString3 = sMarked_String3
        End Get
    End Property
    Public ReadOnly Property MarkedDouble1() As Double
        Get
            MarkedDouble1 = nMarked_Double1
        End Get
    End Property
    Public ReadOnly Property MarkedDouble2() As Double
        Get
            MarkedDouble2 = nMarked_Double2
        End Get
    End Property
    Public ReadOnly Property MarkedDate1() As Date
        Get
            MarkedDate1 = dMarked_Date1
        End Get
    End Property
    'Public Property XArray() As Integer
    '    Get
    '        XArray = ixArray
    '    End Get
    '    Set(ByVal Value As Integer)
    '        ixArray = Value
    '    End Set
    'End Property

    Public Function CreateArrayForReturnvalues(ByVal aArray(,,) As String) As Boolean
        Dim iLocalXCounter As Integer
        Dim iLocalYCounter As Integer

        ReDim aReturnValues(38, UBound(aArray, 3))   'DING

        For iLocalXCounter = 0 To UBound(aReturnValues, 1)
            For iLocalYCounter = 0 To UBound(aReturnValues, 2)
                aReturnValues(iLocalXCounter, iLocalYCounter) = -1
            Next
        Next

    End Function

    Public Function ConnectToDB() As Boolean
        Dim factory As System.Data.Common.DbProviderFactory
        Dim bx As Boolean
        Dim iLoginAttempts As Short
        Dim sProviderName As String
        Dim bErrorDuringConnection As Boolean
        Dim sErrDescription As String
        Dim sOrigConnectionString As String

        'Close the connection if it's open.
        Try

            If eProviderType = ProviderType.WCF Then
                'Nothing to do, no connection necessary
                ' 14.03.2018 - can give Companyname as part of connectionstring for WCF - ;Company=
                '               must be at the end of connectionstring
                If InStr(sERPConnectionString, ";Company=") > 0 Then
                    sCompanyName = Mid(sERPConnectionString, InStr(sERPConnectionString, ";Company=") + 9)
                End If
                If InStr(sERPConnectionString, "PWD=") > 0 Then
                    sERPPWD = Mid(sERPConnectionString, InStr(sERPConnectionString, "PWD=") + 4)
                    If InStr(sERPPWD, ";") > 0 Then
                        sERPPWD = Left(sERPPWD, InStr(sERPPWD, ";") - 1)
                    End If
                End If


            Else
                If Not ConnERP Is Nothing Then
                    If ConnERP.State <> ConnectionState.Closed Then
                        ConnERP.Close()
                    End If
                    ConnERP.Dispose()
                End If

                Me.Reader_Close()
                Me.Command_Close()

                If EmptyString(sERPConnectionString) Then
                    If eTypeOfDB = DatabaseType.ERPDB Then
                        sERPConnectionString = FindBBConnectionString()

                        RetrieveERPDBProfileInfo(sERPConnectionString)
                        'RetrieveERPDBProfileInfo("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath())

                        sOrigConnectionString = sERPConnectionString

                        sERPConnectionString = Replace(sERPConnectionString, "=UID", "=" & Trim(sERPUID))
                        sERPConnectionString = Replace(sERPConnectionString, "=PWD", "=" & Trim(sERPPWD))
                    ElseIf eTypeOfDB = DatabaseType.BBDB Then
                        bSQLServer = BB_UseSQLServer()
                        sERPConnectionString = FindBBConnectionString()

                    ElseIf eTypeOfDB = DatabaseType.ArchiveDB Then

                        '05.11.2019 - Changed line below, 
                        sERPConnectionString = FindBBConnectionString()
                        RetrieveArchiveInfo(sERPConnectionString)
                        'Old code
                        'RetrieveArchiveInfo("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath(False, False))
                        ' TODO - ACCESS2016 - JPS 27.03.2018 - Ikke tilpasset Access 2016 eller SQL Server

                        sOrigConnectionString = sERPConnectionString

                        sERPConnectionString = Replace(sERPConnectionString, "=UID", "=" & Trim(sERPUID))
                        sERPConnectionString = Replace(sERPConnectionString, "=PWD", "=" & Trim(sERPPWD))

                    End If
                Else
                    'Connection information set from calling function
                    sERPConnectionString = Replace(sERPConnectionString, "=UID", "=" & Trim(sERPUID))
                    sERPConnectionString = Replace(sERPConnectionString, "=PWD", "=" & Trim(sERPPWD))
                End If
                'bx = ERP_MakeConnectionNew(aQueryArray(ixArray, 0, 0), aQueryArray(ixArray, 1, 0), aQueryArray(ixArray, 2, 0), aQueryArray(ixArray, 5, 0), Val(aQueryArray(ixArray, 6, 0)), Val(aQueryArray(ixArray, 7, 0)), Val(aQueryArray(ixArray, 8, 0)))

                Select Case eProviderType
                    Case vbBabel.DAL.ProviderType.OleDb
                        sProviderName = "System.Data.OleDb"
                    Case vbBabel.DAL.ProviderType.Odbc
                        sProviderName = "System.Data.Odbc"
                    Case vbBabel.DAL.ProviderType.OracleClient
                        sProviderName = "System.Data.OracleClient"
                    Case vbBabel.DAL.ProviderType.SqlClient
                        sProviderName = "System.Data.SqlClient"
                    Case vbBabel.DAL.ProviderType.SqlServerCe_3_5
                        sProviderName = "System.Data.SqlServerCe.3.5"
                    Case ProviderType.WCF
                        Return True
                    Case Else
                        '" No valid databaseprovider is stated."
                        Throw New ArgumentException(LRS(70006))

                End Select

                'Dim factory As System.Data.Common.DbProviderFactory = System.Data.Common.DbProviderFactories.GetFactory(sProviderName)
                'System.Data.Odbc
                'System.Data.OleDb
                'System.Data.OracleClient
                'System.Data.SqlClient
                'System.Data.SqlServerCe.3.5

                factory = System.Data.Common.DbProviderFactories.GetFactory(sProviderName)
                'ConnERP.
                'Debug.Print("Åpner Connection!")

                ConnERP = factory.CreateConnection()
                bErrorDuringConnection = True
                ConnERP.ConnectionString = sERPConnectionString
                ConnERP.Open()

                End If

                Return True

                Exit Function

        Catch ex As Exception

            If Not ConnERP Is Nothing Then
                ConnERP.Dispose()
            End If

            '70002: Couldn't connect to the database. - 70004 = Connectionstring:
            ' 19.02.2020 - vi bør vel her vise den conn.string som brukes, sOrigConnection kan være tom
            ' 28.04.2020 - IKKE VIS PWD!!!
            Dim sConnStringFreeOfPWD As String = ""
            If InStr(sERPConnectionString, "PWD=") > 0 Then
                sConnStringFreeOfPWD = Left(sERPConnectionString, InStr(sERPConnectionString, "PWD=") - 1) & "PWD=XXXXXXXXXX"
            Else
                sConnStringFreeOfPWD = sERPConnectionString
            End If
            'sErrDescription = LRS(70001) & vbCrLf & vbCrLf & LRS(70004) & vbCrLf & sERPConnectionString  ''''sOrigConnectionString
            sErrDescription = LRS(70001) & vbCrLf & vbCrLf & LRS(70004) & vbCrLf & sConnStringFreeOfPWD

            If bErrorDuringConnection Then
                'The connection reported the following error:
                sErrDescription = sErrDescription & vbCrLf & LRS(70002) & vbCrLf & ex.Message
            Else
                'Error during the building of the connectionstring.
                sErrDescription = sErrDescription & vbCrLf & LRS(70003)
            End If

            If bTestMode Then
                '70005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
                sErrDescription = sErrDescription & vbCrLf & LRS(70005)
            Else

            End If

            If bShowErrorMessage Then
                MsgBox(sErrDescription, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = sErrDescription
                oException = ex
                Return False
            End If

        End Try

        Return True

    End Function
    'Public Function CreateParameter(ByVal name As String, ByVal type As DbType, ByVal value As Object, ByVal direction As ParameterDirection) As IDataParameter

    '    Dim param As DbParameter = Nothing

    '    Dim cmd As DbCommand = ConnERP.CreateCommand

    '    param = cmd.CreateParameter()

    '    If Not param Is Nothing Then
    '        param.ParameterName = name
    '        param.DbType = type
    '        param.Direction = direction
    '        param.Value = value
    '    End If

    '    Return param
    'End Function

    Public Function Reader_Execute(Optional ByRef iArrayIndex As Integer = -1) As Boolean
        Dim iNoOfAttempts As Short
        Dim bx As Boolean
        Dim bReturnValue As Boolean
        Dim sWCF_NameCompany As String
        Dim sWCF_NameSQL As String
        Dim iPos As String
        Dim sSearch As String = ""


        bReturnValue = True
        iRecordCounter = 0

        If iArrayIndex > 0 Then
            iYIndex = iArrayIndex
        Else
            iYIndex = -1
        End If

		Try

			If eProviderType = ProviderType.WCF Then
				iPos = InStr(SQL, "-", CompareMethod.Text)
				'If EmptyString(sNameOfSearch) Then
				' 27.12.2017 Companyname and searchname can be set with property into DAL, or as before as part of the "sql"
				' REMOVE THIS PART !!!
				'sWCF_NameCompany = xDelim(SQL, "-", 1)  'Left(SQL, iPos - 1)
				'sWCF_NameSQL = xDelim(SQL, "-", 2)      'Mid(SQL, iPos + 1)
				'sSearch = xDelim(SQL, "-", 3)
				'Else


				sWCF_NameSQL = sNameOfSearch
				sSearch = SQL

				If sCompanyName = "" Then
					If InStr(sERPConnectionString, ";Company=") > 0 Then
						sCompanyName = Mid(sERPConnectionString, InStr(sERPConnectionString, ";Company=") + 9)
					End If
				End If
				If sERPPWD = "" Then
					If InStr(sERPConnectionString, "PWD=") > 0 Then
						sERPPWD = Mid(sERPConnectionString, InStr(sERPConnectionString, "PWD=") + 4)
						If InStr(sERPPWD, ";") > 0 Then
							sERPPWD = Left(sERPPWD, InStr(sERPPWD, ";") - 1)
						End If
					End If
				End If
				sWCF_NameCompany = sCompanyName
				If InStr(sWCF_NameCompany, "Grieg") > 0 Then
					sWCF_NameCompany = "Grieg"
				End If

				Select Case sWCF_NameCompany

					Case "VB_DEMO"
						RunWCF_VB_Demo(sWCF_NameSQL)

					Case "Active Brands_BCNY"
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						RunWCF_ActiveBrands_BCNY(sWCF_NameSQL, sSearch)  ' we will need the complete "search", as we have parameters

					Case "Active Brands AS"
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						RunWCF_ActiveBrands(sWCF_NameSQL, sSearch)  ' we will need the complete "search", as we have parameters

					Case "Active Brands US", "ECom USA"
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						RunWCF_ActiveBrandsUS(sWCF_NameSQL, sSearch)  ' we will need the complete "search", as we have parameters

					Case "Storm Geo", "Grieg"
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0
						'If Not RunTime() Then
						'    ' ##############################################
						'    ' HUSK at vi nå kjører mot XLedger DEMO database i utviklingsmiljøet
						'    RunWCF_XLedgerDEMO(sWCF_NameSQL, sSearch)  ' we will need the complete "search", as we have parameters
						'    ' ##############################################
						'Else
						RunWCF_XLedger(sWCF_NameSQL, sSearch)  ' we will need the complete "search", as we have parameters
						'End If

					Case "ABAX", "ABAX_NO", "ABAX_SE", "ABAX Danmark AS", "ABAX Denmark AS", "ABAX_NL", "ABAX_PL", "ABAX_DK", "ABAX_FI"  ' etc.
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						' å sikkert ha et navn pr abax-land
						RunWCF_ABAX(sWCF_NameSQL, sSearch, sWCF_NameCompany)  ' we will need the complete "search", as we have parameters

					Case "Seaborn"
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						RunWCF_Seaborn(sWCF_NameSQL, sSearch, sWCF_NameCompany)

					Case "PatentStyret", "PATENTSTYRET_ORDREDATABASE"
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						RunWCF_Patentstyret(sWCF_NameSQL, sSearch, sWCF_NameCompany)

					Case "NHST"   ' Naviga
						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						RunWCF_Naviga(sWCF_NameSQL, sSearch, sWCF_NameCompany)  ' we will need the complete "search", as we have parameters

					Case Else

						bReader_HasRows = False
						bFirstRecordRead = False
						bLastRecordRead = False
						lNumberOfRecordsRead = 0

						' IN setup, as "SQL"-text, put in Methodname + parameters, so we can fire off the complete, interpreted "sql" text
						'RunWCF_Generic()
						'---


				End Select
			Else
				If Not readerERP Is Nothing Then
					If Not readerERP.IsClosed Then
						readerERP.Close()
					End If
				End If

				sErrorMessage = ""
				oException = Nothing
				lErrorNumber = 0
				iNoOfAttempts = 0

				'15.10.2007 Added the use of bUseoriginalAmountInMatching

				If ConnERP.State = ConnectionState.Closed Then
					ConnectToDB()
					readerCommand = ConnERP.CreateCommand()
					'08.03.2021 - Added next If
					If iConnectionTimeOut <> 0 Then
						readerCommand.CommandTimeout = iConnectionTimeOut
					End If
					readerCommand.CommandType = CommandType.Text
					readerCommand.Connection = ConnERP
				End If

				bReader_HasRows = False
				bFirstRecordRead = False
				bLastRecordRead = False
				lNumberOfRecordsRead = 0

				' Create the command.
				If readerCommand Is Nothing Then
					readerCommand = ConnERP.CreateCommand()
					'08.03.2021 - Added next If
					If iConnectionTimeOut <> 0 Then
						readerCommand.CommandTimeout = iConnectionTimeOut
					End If
					readerCommand.CommandType = CommandType.Text
					readerCommand.Connection = ConnERP
				End If
				readerCommand.CommandText = sMySQL

				'Andre parametre som kan være interessante
				'myCommand.CommandType = CommandType.StoredProcedure
				'myCommand.Cancel() - Prøver å stoppe ekskveringen av en DbCommand (SQL-en)
				'myCommand.CreateParameter - ParameterDirection til StoredProcedure?
				'myCommand.ExecuteNonQuery - Kjører SQL som ikke returnerer felter, 'INSERT/UPDATE, CHANGE/ADD TABLES etc...
				'myCommand.ExecuteScalar - Typisk SELECT COUNT(*) ....! Executes the query and returns the first column of the first row in the result set returned by the query.

				' Retrieve the data.
				If eTypeOfDB = DatabaseType.ERPDB And Not RunTime() Then
					If Not sERPConnectionString.Contains("Provider=Microsoft.Jet.OLEDB.4.0") Then
						readerERP = readerCommand.ExecuteReader
						If readerERP.HasRows Then
							bReader_HasRows = True
						End If
					Else
						bReader_HasRows = False
					End If
					'readerERP = New System.Data.Common.DbDataReader
				Else
					readerERP = readerCommand.ExecuteReader
					If readerERP.HasRows Then
						bReader_HasRows = True
					End If
				End If

			End If

		Catch exx As Exception
			If bShowErrorMessage Then
				MsgBox(exx.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
			Else
				'30.08.2022 – Added StackTrace                       
				If exx.InnerException Is Nothing Then
					sErrorMessage = exx.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
				Else
					If exx.InnerException.StackTrace Is Nothing Then
						sErrorMessage = exx.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
					Else
						sErrorMessage = exx.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL & vbCrLf & "Inner Exception: " & exx.InnerException.StackTrace
					End If
				End If
				oException = exx
			End If
			bReturnValue = False
		Finally

		End Try
        'End Using


        Reader_Execute = bReturnValue

        Exit Function


    End Function
    Public Sub TEST_BeginTrans()
        'Test at ConnERP er åpen
        If eProviderType <> ProviderType.WCF Then
            MyTrans = ConnERP.BeginTransaction
        End If
    End Sub
    Public Sub TEST_CommitTrans()
        If eProviderType <> ProviderType.WCF Then
            MyTrans.Commit()
            TEST_bTransactionSet = False
        End If
    End Sub
    Public Sub TEST_Rollback()
        If eProviderType <> ProviderType.WCF Then
            MyTrans.Rollback()
            TEST_bTransactionSet = False
        End If
    End Sub
    ''' <param name="cmdType">Set the Transact-SQL statement or stored procedure to execute at the data source.</param>

    ''' <param name="cmdText">The text of the query.</param>

    ''' <param name="cmdParms">Set Array of Parameter</param>

    ''' <returns>The number of rows affected.</returns>

    ''' <remarks></remarks>

    Public Function ExecuteNonQuery() As Boolean

        Dim iReturnValue As Integer = 0

        Try

            sErrorMessage = ""
            oException = Nothing
            iRecordsAffected = 0
            lErrorNumber = 0

            If Not ConnERP Is Nothing Then  ' added this if 27.02.2015

                If ConnERP.State = ConnectionState.Closed Then
                    ConnectToDB()
                End If

                If myCommand Is Nothing Then
                    myCommand = ConnERP.CreateCommand
                    myCommand.Connection = ConnERP
                    '08.03.2021 - Added next If
                    If iConnectionTimeOut <> 0 Then
                        myCommand.CommandTimeout = iConnectionTimeOut
                    End If
                    myCommand.CommandType = CommandType.Text
                    If Not TEST_bUseCommitFromOutside Then
                        myCommand.Transaction = MyTrans
                    End If
                End If

                myCommand.CommandText = sMySQL
                myCommand.Parameters.Clear()

                'Check if any parameters are set - Not implemented
                If Not (IsNothing(cmdParms)) Then
                    Dim param As DbParameter

                    For Each param In cmdParms
                        myCommand.Parameters.Add(param)
                    Next
                End If

                If Not TEST_bUseCommitFromOutside Then
                    MyTrans = ConnERP.BeginTransaction
                    myCommand.Transaction = MyTrans
                Else
                    If Not TEST_bTransactionSet Then
                        myCommand.Transaction = MyTrans
                        TEST_bTransactionSet = True
                    End If
                End If

				iReturnValue = myCommand.ExecuteNonQuery()
				iRecordsAffected = iReturnValue
                myCommand.Parameters.Clear()
                If Not TEST_bUseCommitFromOutside Then
                    MyTrans.Commit()
                End If
            End If

            Return True

        Catch ex As DbException
            If Not TEST_bUseCommitFromOutside Then
                MyTrans.Rollback()
            End If
            iRecordsAffected = 0

            If bShowErrorMessage Then
                MsgBox(ex.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = "Databasefeil: " & ex.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
                oException = ex
                lErrorNumber = ex.ErrorCode
            End If

            Return False

        Catch exx As Exception
            If Not TEST_bUseCommitFromOutside Then
                MyTrans.Rollback()
            End If
            iRecordsAffected = 0

            If bShowErrorMessage Then
                MsgBox(exx.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = exx.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
                oException = exx
            End If

            Return False

        Finally

            'myCommand = Nothing
            'cmdParms = Nothing

        End Try
    End Function
    Public Function Reader_GetString(ByVal sFieldName As String, Optional ByVal bTrim As Boolean = True) As String
        Dim iFieldNumber As Integer = -1
        Dim sReturnValue As String = ""

        Try
            If eProviderType = ProviderType.WCF Then
                If ColumnWCFExist(readerWCF, sFieldName) Then
                    iFieldNumber = readerWCF.GetOrdinal(sFieldName)
                    sReturnValue = GetWCFReaderItemAsString(readerWCF, sFieldName)
                Else
                    If Not RunTime() Then
                        MsgBox("Variabelen " & sFieldName & " finnes ikke i tabellen." & vbCrLf & "Husk at funksjonen er case-sensitiv", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "DEBUG INFO")
                    End If
                End If
            Else
                If ColumnExist(readerERP, sFieldName) Then
                    iFieldNumber = readerERP.GetOrdinal(sFieldName)
                    sReturnValue = GetReaderItemAsString(readerERP, iFieldNumber)
                Else
                    If Not RunTime() Then
                        MsgBox("Variabelen " & sFieldName & " finnes ikke i tabellen." & vbCrLf & "Husk at funksjonen er case-sensitiv", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "DEBUG INFO")
                    End If
                End If
            End If

            If bTrim Then
                sReturnValue = sReturnValue.Trim
            End If

        Catch
            sReturnValue = False
            Err.Raise(11101, "DAL.Reader_GetString", "Error getting string!" & vbNewLine & "sFieldName: " & sFieldName)
        End Try

        Return sReturnValue
    End Function
    Public Function Reader_GetField(ByVal iFieldNo As Integer) As String
        Dim sFieldName As String = ""
        If eProviderType = ProviderType.WCF Then
            sFieldName = readerWCF.GetName(iFieldNo)
            Return GetWCFReaderItemAsString(readerWCF, sFieldName)
        Else

            Return GetReaderItemAsString(readerERP, iFieldNo)
        End If

    End Function
    Public Function Reader_GetFieldName(ByVal iFieldNo As Integer) As String
        If eProviderType = ProviderType.WCF Then
            Return readerWCF.GetName(iFieldNo)
        Else
            Return readerERP.GetName(iFieldNo)
        End If
    End Function
    Public Function Reader_GetFieldType(ByVal iFieldNo As Integer) As String

        Try
            If readerERP.HasRows Then
                If eProviderType = ProviderType.WCF Then
                    Return GetWCFFieldType(readerWCF.GetDataTypeName(iFieldNo))
                Else
                    Return GetFieldType(readerERP.GetDataTypeName(iFieldNo))
                End If
            Else
                Return FieldType_Unknown
            End If

        Catch
            Err.Raise(11102, "DAL.Reader_GetFieldType", "Error retrieving fieldtype!" & vbNewLine & "iFieldNo: " & iFieldNo.ToString)
        End Try

    End Function
    Public Function Reader_ReadRecord() As Boolean
        Dim bReturnValue As Boolean = False

        Try

            If bDebug Then
                MsgBox("QWQW - I Reader_ReadRecord.")
            End If
            If eProviderType = ProviderType.WCF Then
                If Not readerWCF Is Nothing Then
                    Do While readerWCF.Read()
                        If bDebug Then
                            MsgBox("QWQW - Har kjørt readerWCF.Read.")
                        End If
                        bFirstRecordRead = True
                        bReturnValue = True
                        lNumberOfRecordsRead = lNumberOfRecordsRead + 1
                        Exit Do
                    Loop
                Else
                    bReturnValue = False
                End If

            Else
                If Not readerERP Is Nothing Then
                    Do While readerERP.Read()
                        If bDebug Then
                            MsgBox("QWQW - Har kjørt readerERP.Read.")
                        End If
                        bFirstRecordRead = True
                        bReturnValue = True
                        lNumberOfRecordsRead = lNumberOfRecordsRead + 1
                        Exit Do
                    Loop
                Else
                    bReturnValue = False
                End If
            End If

            Return bReturnValue

        Catch ex As Exception
            If bDebug Then
                MsgBox("QWQW - Feil i Reader_ReadRecord: " & ex.Message)
            End If
            Err.Raise(11103, "DAL.Reader_ReadRecord", "Errormessage: " & ex.Message & vbNewLine & "lNumberOfRecordsRead: " & lNumberOfRecordsRead.ToString)
        End Try

    End Function
    Public Function Reader_ReadERPRecord() As Boolean
        Dim sTemp As String
        Dim bReturnValue As Boolean = False
        Dim bRecordRead As Boolean = False

        Try
            If bDebug Then
                MsgBox("QWQW - I rutinen Reader_ReadERPRecord.")
            End If

            'If Not readerERP Is Nothing Then
            If Not (readerERP Is Nothing And readerWCF Is Nothing) Then
                If bDebug Then
                    MsgBox("QWQW - readerERP/WCF er noe")
                End If

                If Not bLastRecordRead Then

                    If lNumberOfRecordsRead > 0 Then 'Will also fill the returnvalues the very first time but it doesn't matter, may be prevented by adding AND lNumberOfRecordsRead > 0
                        If bDebug Then
                            MsgBox("QWQW - lnumberofrecordsread > 0, skal fylle inn variabler")
                        End If
                        sBBRET_CustomerNo = sTemp_CustomerNo
                        sBBRET_ClientNo = sTemp_ClientNo
                        sBBRET_InvoiceNo = sTemp_InvoiceNo
                        nBBRET_Amount = nTemp_Amount
                        sBBRET_Currency = sTemp_Currency
                        sBBRET_MatchID = sTemp_MatchID
                        sBBRET_MyField = sTemp_MyField
                        sBBRET_MyField2 = sTemp_MyField2
                        sBBRET_MyField3 = sTemp_MyField3
                        sBBRET_Dim1 = sTemp_Dim1
                        sBBRET_Dim2 = sTemp_Dim2
                        sBBRET_Dim3 = sTemp_Dim3
                        sBBRET_Dim4 = sTemp_Dim4
                        sBBRET_Dim5 = sTemp_Dim5
                        sBBRET_Dim6 = sTemp_Dim6
                        sBBRET_Dim7 = sTemp_Dim7
                        sBBRET_Dim8 = sTemp_Dim8
                        sBBRET_Dim9 = sTemp_Dim9
                        sBBRET_Dim10 = sTemp_Dim10
                        sBBRET_Name = sTemp_Name
                        sBBRET_Address = sTemp_Address
                        sBBRET_Address2 = sTemp_Address2
                        sBBRET_ZIP = sTemp_ZIP
                        sBBRET_Freetext = sTemp_Freetext
                        nBBRET_DiscountAmount = nTemp_DiscountAmount
                        sBBRET_DiscountDate = sTemp_DiscountDate
                        sBBRET_AKontoID = sTemp_AKontoID
                        sBBRET_GLID = sTemp_GLID
                        sBBRET_VATCode = sTemp_VATCode
                        sBBRET_String1 = sTemp_String1
                        sBBRET_String2 = sTemp_String2
                        sBBRET_String3 = sTemp_String3
                        nBBRET_Double1 = nTemp_Double1
                        nBBRET_Double2 = nTemp_Double2
                        dBBRET_Date1 = dTemp_Date1

                        If bDebug Then
                            MsgBox("QWQW - variabler fyllt inn")
                        End If

                        bReturnValue = True
                    End If


                    If eProviderType = ProviderType.WCF Then
                        '--- wcf
                        '- lag kode for å lese ut data her

                        Do While readerWCF.Read()
                            If bDebug Then
                                MsgBox("QWQW - Leser ny post")
                            End If

                            If iYIndex > -1 Then
                                If aReturnValues(0, iYIndex) = -1 Then 'Not initiated
                                    'Not initiated for this query/rule
                                    If bDebug Then
                                        MsgBox("QWQW - Skal initiere denne spørringen")
                                    End If

                                    PopulateQueryArray()
                                    If bDebug Then
                                        MsgBox("QWQW - Ferdig med initiering")
                                    End If

                                End If
                                'Exit Do
                            End If

                            If Not bFirstRecordRead Then
                                If bDebug Then
                                    MsgBox("QWQW - Leser første record")
                                    MsgBox("QWQW - iYIndex = " & iYIndex.ToString)
                                    MsgBox("QWQW - aReturnValues(0, iYIndex) = " & aReturnValues(0, iYIndex).ToString)
                                    MsgBox("QWQW - constBBRET_CustomerNo = " & constBBRET_CustomerNo.ToString)
                                End If

                                sBBRET_CustomerNo = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_CustomerNo, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Lest kundenummer")
                                End If
                                sBBRET_ClientNo = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_ClientNo, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Lest klientnummer")
                                End If
                                sBBRET_InvoiceNo = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_InvoiceNo, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Leser fakturanummer")
                                End If
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Amount, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_Amount = 0
                                Else
                                    nBBRET_Amount = CDbl(sTemp)
                                End If
                                If bDebug Then
                                    MsgBox("QWQW - Lest beløp")
                                End If
                                sBBRET_Currency = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Currency, iYIndex)))
                                sBBRET_MatchID = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MatchID, iYIndex)))
                                sBBRET_MyField = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MyField, iYIndex)))
                                sBBRET_MyField2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MyField2, iYIndex)))
                                sBBRET_MyField3 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MyField3, iYIndex)))
                                sBBRET_Dim1 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim1, iYIndex)))
                                sBBRET_Dim2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim2, iYIndex)))
                                sBBRET_Dim3 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim3, iYIndex)))
                                sBBRET_Dim4 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim4, iYIndex)))
                                sBBRET_Dim5 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim5, iYIndex)))
                                sBBRET_Dim6 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim6, iYIndex)))
                                sBBRET_Dim7 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim7, iYIndex)))
                                sBBRET_Dim8 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim8, iYIndex)))
                                sBBRET_Dim9 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim9, iYIndex)))
                                sBBRET_Dim10 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim10, iYIndex)))
                                sBBRET_Name = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Name, iYIndex)))
                                sBBRET_Address = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Address, iYIndex)))
                                sBBRET_Address2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Address2, iYIndex)))
                                sBBRET_ZIP = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_ZIP, iYIndex)))
                                sBBRET_Freetext = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Freetext, iYIndex)))
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_DiscountAmount, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_DiscountAmount = 0
                                Else
                                    nBBRET_DiscountAmount = CDbl(sTemp)
                                End If
                                sBBRET_DiscountDate = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_DiscountDate, iYIndex)))
                                sBBRET_AKontoID = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_AKontoID, iYIndex)))
                                sBBRET_GLID = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_GLID, iYIndex)))
                                sBBRET_VATCode = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_VATCode, iYIndex)))
                                sBBRET_String1 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_String1, iYIndex)))
                                sBBRET_String2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_String2, iYIndex)))
                                sBBRET_String3 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_String3, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Lest string 3")
                                End If

                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Double1, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_Double1 = 0
                                Else
                                    nBBRET_Double1 = CDbl(sTemp)
                                End If
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Double2, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_Double2 = 0
                                Else
                                    nBBRET_Double2 = CDbl(sTemp)
                                End If
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Date1, iYIndex)))
                                If sTemp = "" Then
                                    dBBRET_Date1 = Now()
                                Else
                                    dBBRET_Date1 = sTemp 'The string must be converted
                                End If

                                'Don't set bRecordRead to True because if the Reader has only 1 row the variable bLastRecordRead must be set to True (see code below)
                                bFirstRecordRead = True
                                lNumberOfRecordsRead = lNumberOfRecordsRead + 1
                                If bDebug Then
                                    MsgBox("QWQW - Ferdig med å lese første record")
                                End If


                            Else

                                If bDebug Then
                                    MsgBox("QWQW - Leser nok en record")
                                End If
                                sTemp_CustomerNo = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_CustomerNo, iYIndex)))
                                sTemp_ClientNo = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_ClientNo, iYIndex)))
                                sTemp_InvoiceNo = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_InvoiceNo, iYIndex)))
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Amount, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_Amount = 0
                                Else
                                    nTemp_Amount = CDbl(sTemp)
                                End If
                                sTemp_Currency = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Currency, iYIndex)))
                                sTemp_MatchID = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MatchID, iYIndex)))
                                sTemp_MyField = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MyField, iYIndex)))
                                sTemp_MyField2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MyField2, iYIndex)))
                                sTemp_MyField3 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_MyField3, iYIndex)))
                                sTemp_Dim1 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim1, iYIndex)))
                                sTemp_Dim2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim2, iYIndex)))
                                sTemp_Dim3 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim3, iYIndex)))
                                sTemp_Dim4 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim4, iYIndex)))
                                sTemp_Dim5 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim5, iYIndex)))
                                sTemp_Dim6 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim6, iYIndex)))
                                sTemp_Dim7 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim7, iYIndex)))
                                sTemp_Dim8 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim8, iYIndex)))
                                sTemp_Dim9 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim9, iYIndex)))
                                sTemp_Dim10 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Dim10, iYIndex)))
                                sTemp_Name = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Name, iYIndex)))
                                sTemp_Address = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Address, iYIndex)))
                                sTemp_Address2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Address2, iYIndex)))
                                sTemp_ZIP = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_ZIP, iYIndex)))
                                sTemp_Freetext = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Freetext, iYIndex)))
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_DiscountAmount, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_DiscountAmount = 0
                                Else
                                    nTemp_DiscountAmount = CDbl(sTemp)
                                End If
                                sTemp_DiscountDate = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_DiscountDate, iYIndex)))
                                sTemp_AKontoID = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_AKontoID, iYIndex)))
                                sTemp_GLID = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_GLID, iYIndex)))
                                sTemp_VATCode = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_VATCode, iYIndex)))
                                sTemp_String1 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_String1, iYIndex)))
                                sTemp_String2 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_String2, iYIndex)))
                                sTemp_String3 = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_String3, iYIndex)))
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Double1, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_Double1 = 0
                                Else
                                    nTemp_Double1 = CDbl(sTemp)
                                End If
                                sTemp = ""
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Double2, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_Double2 = 0
                                Else
                                    nTemp_Double2 = CDbl(sTemp)
                                End If
                                sTemp = GetWCFReaderItemAsString(readerWCF, (aReturnValues(constBBRET_Date1, iYIndex)))
                                If sTemp = "" Then
                                    dTemp_Date1 = Now()
                                Else
                                    dTemp_Date1 = sTemp 'The string must be converted
                                End If

                                bRecordRead = True
                                lNumberOfRecordsRead = lNumberOfRecordsRead + 1

                                If bDebug Then
                                    MsgBox("QWQW - Ferdig med å lese en ny record")
                                End If


                                Exit Do
                            End If

                            bReturnValue = True

                        Loop
                    Else
                        ' Not WCF
                        Do While readerERP.Read()
                            If bDebug Then
                                MsgBox("QWQW - Leser ny post")
                            End If

                            If iYIndex > -1 Then
                                If aReturnValues(0, iYIndex) = -1 Then 'Not initiated
                                    'Not initiated for this query/rule
                                    If bDebug Then
                                        MsgBox("QWQW - Skal initiere denne spørringen")
                                    End If

                                    PopulateQueryArray()
                                    If bDebug Then
                                        MsgBox("QWQW - Ferdig med initiering")
                                    End If

                                End If
                                'Exit Do
                            End If

                            If Not bFirstRecordRead Then
                                If bDebug Then
                                    MsgBox("QWQW - Leser første record")
                                    MsgBox("QWQW - iYIndex = " & iYIndex.ToString)
                                    MsgBox("QWQW - aReturnValues(0, iYIndex) = " & aReturnValues(0, iYIndex).ToString)
                                    MsgBox("QWQW - constBBRET_CustomerNo = " & constBBRET_CustomerNo.ToString)
                                End If

                                sBBRET_CustomerNo = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_CustomerNo, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Lest kundenummer")
                                End If
                                sBBRET_ClientNo = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_ClientNo, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Lest klientnummer")
                                End If
                                sBBRET_InvoiceNo = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_InvoiceNo, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Leser fakturanummer")
                                End If
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Amount, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_Amount = 0
                                Else
                                    nBBRET_Amount = CDbl(sTemp)
                                End If
                                If bDebug Then
                                    MsgBox("QWQW - Lest beløp")
                                End If
                                sBBRET_Currency = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Currency, iYIndex)))
                                sBBRET_MatchID = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MatchID, iYIndex)))
                                sBBRET_MyField = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MyField, iYIndex)))
                                sBBRET_MyField2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MyField2, iYIndex)))
                                sBBRET_MyField3 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MyField3, iYIndex)))
                                sBBRET_Dim1 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim1, iYIndex)))
                                sBBRET_Dim2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim2, iYIndex)))
                                sBBRET_Dim3 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim3, iYIndex)))
                                sBBRET_Dim4 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim4, iYIndex)))
                                sBBRET_Dim5 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim5, iYIndex)))
                                sBBRET_Dim6 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim6, iYIndex)))
                                sBBRET_Dim7 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim7, iYIndex)))
                                sBBRET_Dim8 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim8, iYIndex)))
                                sBBRET_Dim9 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim9, iYIndex)))
                                sBBRET_Dim10 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim10, iYIndex)))
                                sBBRET_Name = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Name, iYIndex)))
                                sBBRET_Address = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Address, iYIndex)))
                                sBBRET_Address2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Address2, iYIndex)))
                                sBBRET_ZIP = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_ZIP, iYIndex)))
                                sBBRET_Freetext = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Freetext, iYIndex)))
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_DiscountAmount, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_DiscountAmount = 0
                                Else
                                    nBBRET_DiscountAmount = CDbl(sTemp)
                                End If
                                sBBRET_DiscountDate = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_DiscountDate, iYIndex)))
                                sBBRET_AKontoID = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_AKontoID, iYIndex)))
                                sBBRET_GLID = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_GLID, iYIndex)))
                                sBBRET_VATCode = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_VATCode, iYIndex)))
                                sBBRET_String1 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_String1, iYIndex)))
                                sBBRET_String2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_String2, iYIndex)))
                                sBBRET_String3 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_String3, iYIndex)))
                                If bDebug Then
                                    MsgBox("QWQW - Lest string 3")
                                End If

                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Double1, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_Double1 = 0
                                Else
                                    nBBRET_Double1 = CDbl(sTemp)
                                End If
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Double2, iYIndex)))
                                If sTemp = "" Then
                                    nBBRET_Double2 = 0
                                Else
                                    nBBRET_Double2 = CDbl(sTemp)
                                End If
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Date1, iYIndex)))
                                If sTemp = "" Then
                                    dBBRET_Date1 = Now()
                                Else
                                    dBBRET_Date1 = sTemp 'The string must be converted
                                End If

                                'Don't set bRecordRead to True because if the Reader has only 1 row the variable bLastRecordRead must be set to True (see code below)
                                bFirstRecordRead = True
                                lNumberOfRecordsRead = lNumberOfRecordsRead + 1
                                If bDebug Then
                                    MsgBox("QWQW - Ferdig med å lese første record")
                                End If


                            Else

                                If bDebug Then
                                    MsgBox("QWQW - Leser nok en record")
                                End If
                                sTemp_CustomerNo = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_CustomerNo, iYIndex)))
                                sTemp_ClientNo = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_ClientNo, iYIndex)))
                                sTemp_InvoiceNo = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_InvoiceNo, iYIndex)))
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Amount, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_Amount = 0
                                Else
                                    nTemp_Amount = CDbl(sTemp)
                                End If
                                sTemp_Currency = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Currency, iYIndex)))
                                sTemp_MatchID = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MatchID, iYIndex)))
                                sTemp_MyField = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MyField, iYIndex)))
                                sTemp_MyField2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MyField2, iYIndex)))
                                sTemp_MyField3 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_MyField3, iYIndex)))
                                sTemp_Dim1 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim1, iYIndex)))
                                sTemp_Dim2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim2, iYIndex)))
                                sTemp_Dim3 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim3, iYIndex)))
                                sTemp_Dim4 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim4, iYIndex)))
                                sTemp_Dim5 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim5, iYIndex)))
                                sTemp_Dim6 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim6, iYIndex)))
                                sTemp_Dim7 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim7, iYIndex)))
                                sTemp_Dim8 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim8, iYIndex)))
                                sTemp_Dim9 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim9, iYIndex)))
                                sTemp_Dim10 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Dim10, iYIndex)))
                                sTemp_Name = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Name, iYIndex)))
                                sTemp_Address = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Address, iYIndex)))
                                sTemp_Address2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Address2, iYIndex)))
                                sTemp_ZIP = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_ZIP, iYIndex)))
                                sTemp_Freetext = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Freetext, iYIndex)))
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_DiscountAmount, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_DiscountAmount = 0
                                Else
                                    nTemp_DiscountAmount = CDbl(sTemp)
                                End If
                                sTemp_DiscountDate = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_DiscountDate, iYIndex)))
                                sTemp_AKontoID = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_AKontoID, iYIndex)))
                                sTemp_GLID = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_GLID, iYIndex)))
                                sTemp_VATCode = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_VATCode, iYIndex)))
                                sTemp_String1 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_String1, iYIndex)))
                                sTemp_String2 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_String2, iYIndex)))
                                sTemp_String3 = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_String3, iYIndex)))
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Double1, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_Double1 = 0
                                Else
                                    nTemp_Double1 = CDbl(sTemp)
                                End If
                                sTemp = ""
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Double2, iYIndex)))
                                If sTemp = "" Then
                                    nTemp_Double2 = 0
                                Else
                                    nTemp_Double2 = CDbl(sTemp)
                                End If
                                sTemp = GetReaderItemAsString(readerERP, (aReturnValues(constBBRET_Date1, iYIndex)))
                                If sTemp = "" Then
                                    dTemp_Date1 = Now()
                                Else
                                    dTemp_Date1 = sTemp 'The string must be converted
                                End If

                                bRecordRead = True
                                lNumberOfRecordsRead = lNumberOfRecordsRead + 1

                                If bDebug Then
                                    MsgBox("QWQW - Ferdig med å lese en ny record")
                                End If


                                Exit Do
                            End If

                            bReturnValue = True

                        Loop
                    End If

                    If Not bRecordRead Then 'Only set when the second record is read. This variable is False when the Reader has only 1 row
                        bLastRecordRead = True
                    End If

                Else
                    If bDebug Then
                        MsgBox("QWQW - LastrecordRead er sann")
                    End If
                    bReturnValue = False
                End If

            Else
                bReturnValue = False
            End If

        Catch ex As Exception
            If bDebug Then
                MsgBox("QWQW - Feil i Reader_ReadERPRecord: " & ex.Message)
            End If
            Err.Raise(11104, "DAL.Reader_ReadERPRecord", "Errormessage:" & ex.Message & vbNewLine & "sTemp_InvoiceNo: " & sTemp_InvoiceNo & " nTemp_Amount: " & nTemp_Amount.ToString)
        End Try

        Reader_ReadERPRecord = bReturnValue

    End Function
    Public Sub Reader_Close()
        If eProviderType = ProviderType.WCF Then
            readerWCF = Nothing

        Else
            If Not readerERP Is Nothing Then
                If Not readerERP.IsClosed Then
                    readerERP.Close()
                End If
                readerERP = Nothing
            End If
            If Not readerCommand Is Nothing Then
                readerCommand.Dispose()
                readerCommand = Nothing
            End If
        End If
        iRecordCounter = 0

    End Sub
    Public Sub Command_Close()

        If Not myCommand Is Nothing Then
            myCommand.Dispose()
        End If
        If Not MyTrans Is Nothing Then
            MyTrans.Dispose()
        End If
        'What to do with this? cmdParms

    End Sub
    Public Sub Reader_MarkThisRow()

        sMarked_CustomerNo = sBBRET_CustomerNo
        sMarked_ClientNo = sBBRET_ClientNo
        sMarked_InvoiceNo = sBBRET_InvoiceNo
        nMarked_Amount = nBBRET_Amount
        sMarked_Currency = sBBRET_Currency
        sMarked_MatchID = sBBRET_MatchID
        sMarked_MyField = sBBRET_MyField
        sMarked_MyField2 = sBBRET_MyField2
        sMarked_MyField3 = sBBRET_MyField3
        sMarked_Dim1 = sBBRET_Dim1
        sMarked_Dim2 = sBBRET_Dim2
        sMarked_Dim3 = sBBRET_Dim3
        sMarked_Dim4 = sBBRET_Dim4
        sMarked_Dim5 = sBBRET_Dim5
        sMarked_Dim6 = sBBRET_Dim6
        sMarked_Dim7 = sBBRET_Dim7
        sMarked_Dim8 = sBBRET_Dim8
        sMarked_Dim9 = sBBRET_Dim9
        sMarked_Dim10 = sBBRET_Dim10
        sMarked_Name = sBBRET_Name
        sMarked_Address = sBBRET_Address
        sMarked_Address2 = sBBRET_Address2
        sMarked_ZIP = sBBRET_ZIP
        sMarked_Freetext = sBBRET_Freetext
        nMarked_DiscountAmount = nBBRET_DiscountAmount
        sMarked_DiscountDate = sBBRET_DiscountDate
        sMarked_AKontoID = sBBRET_AKontoID
        sMarked_GLID = sBBRET_GLID
        sMarked_VATCode = sBBRET_VATCode
        sMarked_String1 = sBBRET_String1
        sMarked_String2 = sBBRET_String2
        sMarked_String3 = sBBRET_String3
        nMarked_Double1 = nBBRET_Double1
        nMarked_Double2 = nBBRET_Double2
        dMarked_Date1 = dBBRET_Date1

    End Sub
    Public Sub Reader_UseMarkedPayment()

        sBBRET_CustomerNo = sMarked_CustomerNo
        sBBRET_ClientNo = sMarked_ClientNo
        sBBRET_InvoiceNo = sMarked_InvoiceNo
        nBBRET_Amount = nMarked_Amount
        sBBRET_Currency = sMarked_Currency
        sBBRET_MatchID = sMarked_MatchID
        sBBRET_MyField = sMarked_MyField
        sBBRET_MyField2 = sMarked_MyField2
        sBBRET_MyField3 = sMarked_MyField3
        sBBRET_Dim1 = sMarked_Dim1
        sBBRET_Dim2 = sMarked_Dim2
        sBBRET_Dim3 = sMarked_Dim3
        sBBRET_Dim4 = sMarked_Dim4
        sBBRET_Dim5 = sMarked_Dim5
        sBBRET_Dim6 = sMarked_Dim6
        sBBRET_Dim7 = sMarked_Dim7
        sBBRET_Dim8 = sMarked_Dim8
        sBBRET_Dim9 = sMarked_Dim9
        sBBRET_Dim10 = sMarked_Dim10
        sBBRET_Name = sMarked_Name
        sBBRET_Address = sMarked_Address
        sBBRET_Address2 = sMarked_Address2
        sBBRET_ZIP = sMarked_ZIP
        sBBRET_Freetext = sMarked_Freetext
        nBBRET_DiscountAmount = nMarked_DiscountAmount
        sBBRET_DiscountDate = sMarked_DiscountDate
        sBBRET_AKontoID = sMarked_AKontoID
        sBBRET_GLID = sMarked_GLID
        sBBRET_VATCode = sMarked_VATCode
        sBBRET_String1 = sMarked_String1
        sBBRET_String2 = sMarked_String2
        sBBRET_String3 = sMarked_String3
        nBBRET_Double1 = nMarked_Double1
        nBBRET_Double2 = nMarked_Double2
        dBBRET_Date1 = dMarked_Date1

    End Sub
    Private Sub PopulateQueryArray()
        'The Array is built like this
        'BBRET_ClientNo = 1
        'BBRET_CustomerNo = 2
        'BBRET_InvoiceNo = 3
        'BBRET_Amount = 4
        'BBRET_Currency = 5
        'BBRET_MatchID = 6
        'BBRET_MyField = 7
        'BBRET_Name = 8
        'BBRET_Address = 9
        'BBRET_Adr2 = 10
        'BBRET_ZIP = 11
        'BBRET_Freetext = 12
        'BBRET_DiscountAmount = 13
        'BBRET_DiscountDate = 14
        'BBRET_AKontoID = 15
        'BBRET_GLID = 16
        'BBRET_VATCode = 17
        'BBRET_String1 = 21
        'BBRET_String2 = 22
        'BBRET_String3 = 23
        'BBRET_Double1 = 24
        'BBRET_Double2 = 25
        'BBRET_Date1 = 26
        'BBRET_MyField2 = 27
        'BBRET_MyField3 = 28
        'BBRET_Dim1 = 29
        'BBRET_Dim2 = 31
        'BBRET_Dim3 = 32
        'BBRET_Dim4 = 33
        'BBRET_Dim5 = 34
        'BBRET_Dim6 = 35
        'BBRET_Dim7 = 36
        'BBRET_Dim8 = 37
        'BBRET_Dim9 = 38
        'BBRET_Dim10 = 39

        Try
            If bDebug Then
                MsgBox("QWQW - I runtinen PopulateQuerArray")
            End If

            aReturnValues(0, iYIndex) = 1 'Signifies initiated

            If eProviderType = ProviderType.WCF Then

                If WCF_ColumnExist(readerWCF, "BBRET_ClientNo") Then
                    aReturnValues(constBBRET_ClientNo, iYIndex) = readerWCF.GetOrdinal("BBRET_ClientNo")
                End If
                If bDebug Then
                    MsgBox("QWQW - Lagt inn ClientNo i arrayen.")
                End If

                If WCF_ColumnExist(readerWCF, "BBRET_CustomerNo") Then
                    aReturnValues(constBBRET_CustomerNo, iYIndex) = readerWCF.GetOrdinal("BBRET_CustomerNo")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_InvoiceNo") Then
                    aReturnValues(constBBRET_InvoiceNo, iYIndex) = readerWCF.GetOrdinal("BBRET_InvoiceNo")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Amount") Then
                    aReturnValues(constBBRET_Amount, iYIndex) = readerWCF.GetOrdinal("BBRET_Amount")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Currency") Then
                    aReturnValues(constBBRET_Currency, iYIndex) = readerWCF.GetOrdinal("BBRET_Currency")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_MatchID") Then
                    aReturnValues(constBBRET_MatchID, iYIndex) = readerWCF.GetOrdinal("BBRET_MatchID")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_MyField") Then
                    aReturnValues(constBBRET_MyField, iYIndex) = readerWCF.GetOrdinal("BBRET_MyField")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_MyField2") Then
                    aReturnValues(constBBRET_MyField2, iYIndex) = readerWCF.GetOrdinal("BBRET_MyField2")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_MyField3") Then
                    aReturnValues(constBBRET_MyField3, iYIndex) = readerWCF.GetOrdinal("BBRET_MyField3")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim1") Then
                    aReturnValues(constBBRET_Dim1, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim1")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim2") Then
                    aReturnValues(constBBRET_Dim2, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim2")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim3") Then
                    aReturnValues(constBBRET_Dim3, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim3")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim4") Then
                    aReturnValues(constBBRET_Dim4, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim4")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim5") Then
                    aReturnValues(constBBRET_Dim5, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim5")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim6") Then
                    aReturnValues(constBBRET_Dim6, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim6")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim7") Then
                    aReturnValues(constBBRET_Dim7, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim7")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim8") Then
                    aReturnValues(constBBRET_Dim8, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim8")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim9") Then
                    aReturnValues(constBBRET_Dim9, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim9")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Dim10") Then
                    aReturnValues(constBBRET_Dim10, iYIndex) = readerWCF.GetOrdinal("BBRET_Dim10")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Name") Then
                    aReturnValues(constBBRET_Name, iYIndex) = readerWCF.GetOrdinal("BBRET_Name")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Address") Then
                    aReturnValues(constBBRET_Address, iYIndex) = readerWCF.GetOrdinal("BBRET_Address")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Adr2") Then
                    aReturnValues(constBBRET_Address2, iYIndex) = readerWCF.GetOrdinal("BBRET_Adr2")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_ZIP") Then
                    aReturnValues(constBBRET_ZIP, iYIndex) = readerWCF.GetOrdinal("BBRET_ZIP")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Freetext") Then
                    aReturnValues(constBBRET_Freetext, iYIndex) = readerWCF.GetOrdinal("BBRET_Freetext")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_DiscountAmount") Then
                    aReturnValues(constBBRET_DiscountAmount, iYIndex) = readerWCF.GetOrdinal("BBRET_DiscountAmount")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_DiscountDate") Then
                    aReturnValues(constBBRET_DiscountDate, iYIndex) = readerWCF.GetOrdinal("BBRET_DiscountDate")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_AKontoID") Then
                    aReturnValues(constBBRET_AKontoID, iYIndex) = readerWCF.GetOrdinal("BBRET_AKontoID")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_GLID") Then
                    aReturnValues(constBBRET_GLID, iYIndex) = readerWCF.GetOrdinal("BBRET_GLID")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_VATCode") Then
                    aReturnValues(constBBRET_VATCode, iYIndex) = readerWCF.GetOrdinal("BBRET_VATCode")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_String1") Then
                    aReturnValues(constBBRET_String1, iYIndex) = readerWCF.GetOrdinal("BBRET_String1")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_String2") Then
                    aReturnValues(constBBRET_String2, iYIndex) = readerWCF.GetOrdinal("BBRET_String2")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_String3") Then
                    aReturnValues(constBBRET_String3, iYIndex) = readerWCF.GetOrdinal("BBRET_String3")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Double1") Then
                    aReturnValues(constBBRET_Double1, iYIndex) = readerWCF.GetOrdinal("BBRET_Double1")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Double2") Then
                    aReturnValues(constBBRET_Double2, iYIndex) = readerWCF.GetOrdinal("BBRET_Double2")
                End If
                If WCF_ColumnExist(readerWCF, "BBRET_Date1") Then
                    aReturnValues(constBBRET_Date1, iYIndex) = readerWCF.GetOrdinal("BBRET_Date1")
                End If

            Else
                If ColumnExist(readerERP, "BBRET_ClientNo") Then
                    aReturnValues(constBBRET_ClientNo, iYIndex) = readerERP.GetOrdinal("BBRET_ClientNo")
                End If
                If bDebug Then
                    MsgBox("QWQW - Lagt inn ClientNo i arrayen.")
                End If

                If ColumnExist(readerERP, "BBRET_CustomerNo") Then
                    aReturnValues(constBBRET_CustomerNo, iYIndex) = readerERP.GetOrdinal("BBRET_CustomerNo")
                End If
                If ColumnExist(readerERP, "BBRET_InvoiceNo") Then
                    aReturnValues(constBBRET_InvoiceNo, iYIndex) = readerERP.GetOrdinal("BBRET_InvoiceNo")
                End If
                If ColumnExist(readerERP, "BBRET_Amount") Then
                    aReturnValues(constBBRET_Amount, iYIndex) = readerERP.GetOrdinal("BBRET_Amount")
                End If
                If ColumnExist(readerERP, "BBRET_Currency") Then
                    aReturnValues(constBBRET_Currency, iYIndex) = readerERP.GetOrdinal("BBRET_Currency")
                End If
                If ColumnExist(readerERP, "BBRET_MatchID") Then
                    aReturnValues(constBBRET_MatchID, iYIndex) = readerERP.GetOrdinal("BBRET_MatchID")
                End If
                If ColumnExist(readerERP, "BBRET_MyField") Then
                    aReturnValues(constBBRET_MyField, iYIndex) = readerERP.GetOrdinal("BBRET_MyField")
                End If
                If ColumnExist(readerERP, "BBRET_MyField2") Then
                    aReturnValues(constBBRET_MyField2, iYIndex) = readerERP.GetOrdinal("BBRET_MyField2")
                End If
                If ColumnExist(readerERP, "BBRET_MyField3") Then
                    aReturnValues(constBBRET_MyField3, iYIndex) = readerERP.GetOrdinal("BBRET_MyField3")
                End If
                If ColumnExist(readerERP, "BBRET_Dim1") Then
                    aReturnValues(constBBRET_Dim1, iYIndex) = readerERP.GetOrdinal("BBRET_Dim1")
                End If
                If ColumnExist(readerERP, "BBRET_Dim2") Then
                    aReturnValues(constBBRET_Dim2, iYIndex) = readerERP.GetOrdinal("BBRET_Dim2")
                End If
                If ColumnExist(readerERP, "BBRET_Dim3") Then
                    aReturnValues(constBBRET_Dim3, iYIndex) = readerERP.GetOrdinal("BBRET_Dim3")
                End If
                If ColumnExist(readerERP, "BBRET_Dim4") Then
                    aReturnValues(constBBRET_Dim4, iYIndex) = readerERP.GetOrdinal("BBRET_Dim4")
                End If
                If ColumnExist(readerERP, "BBRET_Dim5") Then
                    aReturnValues(constBBRET_Dim5, iYIndex) = readerERP.GetOrdinal("BBRET_Dim5")
                End If
                If ColumnExist(readerERP, "BBRET_Dim6") Then
                    aReturnValues(constBBRET_Dim6, iYIndex) = readerERP.GetOrdinal("BBRET_Dim6")
                End If
                If ColumnExist(readerERP, "BBRET_Dim7") Then
                    aReturnValues(constBBRET_Dim7, iYIndex) = readerERP.GetOrdinal("BBRET_Dim7")
                End If
                If ColumnExist(readerERP, "BBRET_Dim8") Then
                    aReturnValues(constBBRET_Dim8, iYIndex) = readerERP.GetOrdinal("BBRET_Dim8")
                End If
                If ColumnExist(readerERP, "BBRET_Dim9") Then
                    aReturnValues(constBBRET_Dim9, iYIndex) = readerERP.GetOrdinal("BBRET_Dim9")
                End If
                If ColumnExist(readerERP, "BBRET_Dim10") Then
                    aReturnValues(constBBRET_Dim10, iYIndex) = readerERP.GetOrdinal("BBRET_Dim10")
                End If
                If ColumnExist(readerERP, "BBRET_Name") Then
                    aReturnValues(constBBRET_Name, iYIndex) = readerERP.GetOrdinal("BBRET_Name")
                End If
                If ColumnExist(readerERP, "BBRET_Address") Then
                    aReturnValues(constBBRET_Address, iYIndex) = readerERP.GetOrdinal("BBRET_Address")
                End If
                If ColumnExist(readerERP, "BBRET_Adr2") Then
                    aReturnValues(constBBRET_Address2, iYIndex) = readerERP.GetOrdinal("BBRET_Adr2")
                End If
                If ColumnExist(readerERP, "BBRET_ZIP") Then
                    aReturnValues(constBBRET_ZIP, iYIndex) = readerERP.GetOrdinal("BBRET_ZIP")
                End If
                If ColumnExist(readerERP, "BBRET_Freetext") Then
                    aReturnValues(constBBRET_Freetext, iYIndex) = readerERP.GetOrdinal("BBRET_Freetext")
                End If
                If ColumnExist(readerERP, "BBRET_DiscountAmount") Then
                    aReturnValues(constBBRET_DiscountAmount, iYIndex) = readerERP.GetOrdinal("BBRET_DiscountAmount")
                End If
                If ColumnExist(readerERP, "BBRET_DiscountDate") Then
                    aReturnValues(constBBRET_DiscountDate, iYIndex) = readerERP.GetOrdinal("BBRET_DiscountDate")
                End If
                If ColumnExist(readerERP, "BBRET_AKontoID") Then
                    aReturnValues(constBBRET_AKontoID, iYIndex) = readerERP.GetOrdinal("BBRET_AKontoID")
                End If
                If ColumnExist(readerERP, "BBRET_GLID") Then
                    aReturnValues(constBBRET_GLID, iYIndex) = readerERP.GetOrdinal("BBRET_GLID")
                End If
                If ColumnExist(readerERP, "BBRET_VATCode") Then
                    aReturnValues(constBBRET_VATCode, iYIndex) = readerERP.GetOrdinal("BBRET_VATCode")
                End If
                If ColumnExist(readerERP, "BBRET_String1") Then
                    aReturnValues(constBBRET_String1, iYIndex) = readerERP.GetOrdinal("BBRET_String1")
                End If
                If ColumnExist(readerERP, "BBRET_String2") Then
                    aReturnValues(constBBRET_String2, iYIndex) = readerERP.GetOrdinal("BBRET_String2")
                End If
                If ColumnExist(readerERP, "BBRET_String3") Then
                    aReturnValues(constBBRET_String3, iYIndex) = readerERP.GetOrdinal("BBRET_String3")
                End If
                If ColumnExist(readerERP, "BBRET_Double1") Then
                    aReturnValues(constBBRET_Double1, iYIndex) = readerERP.GetOrdinal("BBRET_Double1")
                End If
                If ColumnExist(readerERP, "BBRET_Double2") Then
                    aReturnValues(constBBRET_Double2, iYIndex) = readerERP.GetOrdinal("BBRET_Double2")
                End If
                If ColumnExist(readerERP, "BBRET_Date1") Then
                    aReturnValues(constBBRET_Date1, iYIndex) = readerERP.GetOrdinal("BBRET_Date1")
                End If
            End If

        Catch ex As Exception
            Err.Raise(11105, "DAL.PopulateQueryArray", "Message: " & ex.Message & vbNewLine & "sTemp_InvoiceNo: " & sTemp_InvoiceNo & " nTemp_Amount: " & nTemp_Amount.ToString)
        End Try

    End Sub
    Private Function ColumnExist(ByVal reader As System.Data.Common.DbDataReader, ByVal columnName As String) As Boolean
        Dim i As Integer

        Try
            For i = 0 To reader.FieldCount - 1
                If reader.GetName(i).Equals(columnName, StringComparison.CurrentCultureIgnoreCase) Then '04.03.2013 - Added parameter , StringComparison.CurrentCultureIgnoreCase
                    Return True 'Not IsDBNull(reader.GetValue(i))
                    'Return Not IsDBNull(reader(columnName))
                End If
            Next

            Return False

        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function WCF_ColumnExist(ByVal readerWCF As ReaderWCF, ByVal columnName As String) As Boolean
        ' Check if a Column exists in the readerWCF object - based on definitions from xml mappingfile
        Dim i As Integer

        ' status: råskrevet
        ' testet: nix

        Try
            For i = 0 To readerWCF.FieldCount - 1
                If readerWCF.GetName(i).Equals(columnName, StringComparison.CurrentCultureIgnoreCase) Then
                    ' also checks if it is "displayable. in the definitons file, DisplaySequence = -1 for fields which we will not display
                    Return True 'Not IsDBNull(reader.GetValue(i))
                    'Return Not IsDBNull(reader(columnName))
                End If
            Next

            Return False

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub New()

        sERPConnectionString = ""
        eProviderType = vbBabel.DAL.ProviderType.OleDb
        sERPUID = ""
        sERPPWD = ""
        iConnectionTimeOut = 0
        bTestMode = False
        sMySQL = ""
        bReader_HasRows = False
        iYIndex = -1
    End Sub
    Public Function GetReaderItemAsString(ByVal Reader As System.Data.Common.DbDataReader, ByVal iFieldNo As Integer) As String
        'For definitions se: http://msdn.microsoft.com/en-us/library/4e5xt97a.aspx
        'However it doesn't fit for OleDb/Access
        Dim sTypeName As String
        Dim iType As Integer = FieldType_Unknown

        Try

            If bDebug Then
                MsgBox("QWQW - I funksjonen GetReaderItemAsString")
            End If

            If iFieldNo > -1 Then
                sTypeName = Reader.GetDataTypeName(iFieldNo)
                iType = GetFieldType(sTypeName)
				If Reader.IsDBNull(iFieldNo) Then
					Select Case iType
						'NBNBNBNB: If a new type is added remeber to add it if the field is NULL to return "0" instead of ""
						Case FieldType_Integer, FieldType_Double, FieldType_Short, FieldType_Decimal, FieldType_BigInt, FieldType_Byte
							GetReaderItemAsString = "0"
						Case FieldType_Boolean
							GetReaderItemAsString = "False"
						Case Else
							GetReaderItemAsString = ""
					End Select
				Else
					Select Case iType
                        Case FieldType_String
                            GetReaderItemAsString = Reader.GetString(iFieldNo)
                        Case FieldType_Byte
                            GetReaderItemAsString = Reader.GetByte(iFieldNo).ToString
                        Case FieldType_Short
                            GetReaderItemAsString = Reader.GetInt16(iFieldNo).ToString
                        Case FieldType_Integer
                            GetReaderItemAsString = Reader.GetInt32(iFieldNo).ToString
                        Case FieldType_BigInt
                            GetReaderItemAsString = Reader.GetInt64(iFieldNo).ToString
                        Case FieldType_Double
                            GetReaderItemAsString = Reader.GetDouble(iFieldNo).ToString
                        Case FieldType_Decimal
                            GetReaderItemAsString = Reader.GetDecimal(iFieldNo).ToString
                        Case FieldType_Boolean
                            If Reader.GetBoolean(iFieldNo) = True Then
                                GetReaderItemAsString = "True"
                            Else
                                GetReaderItemAsString = "False"
                            End If
                        Case FieldType_Date

                            GetReaderItemAsString = Reader.GetDateTime(iFieldNo).ToString 'Not Tested
                        Case FieldType_Value
                            GetReaderItemAsString = Reader.GetValue(iFieldNo).ToString
                        Case Else
                            Throw New Exception(LRS(70008, iType.ToString) & vbCrLf & vbCrLf & LRS(10016))

                            '"BabelBank is not able to retrieve fields of type: " & Trim(Str(iType)) _
                            '   & vbCrLf & vbCrLf & "Please contact Your dealer")

                    End Select

                End If
            Else
                Return ""
            End If

        Catch ex As Exception

            If bShowErrorMessage Then
                MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = LRS(70007) & ex.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
                Return ""
            End If

        End Try

    End Function
    Private Function GetFieldType(ByVal s As String) As Integer

        Try

            Select Case s.ToUpper   ' 08.10.2014 added ToUpper, 10.09.15 lagt inn NVARCHAR2

                Case "UNIQUEIDENTIFIER", "DBTYPE_R4"
                    Return FieldType_Value
                    '03.02.2021 - Added UNIQUEIDENTIFIER for SQL Server Aptic for kunden Nordea Finance. Usikker på om dette er riktig? Funker ihvertfall

                    ' 27.09.2021 - added DBTYPE_CHAR, found in new Visma SQLOLEDB connection
				Case "WVARCHAR", "DBTYPE_WVARCHAR", "DBTYPE_WCHAR", "DBTYPE_WLONGVARCHAR", "NVARCHAR", "NVARCHAR2", "VARCHAR", "CHAR", "VARCHAR2", "SQL_VARCHAR", "DBTYPE_VARCHAR", "UDD_BANKNUMBER", "NCHAR", "NTEXT", "DBTYPE_CHAR"  '01.12.2015 added NCHAR
					'Access: DBTYPE_WVARCHAR, DBTYPE_WCHAR, DBTYPE_WLONGVARCHAR,
					'DBTYPE_WLONGVARCHAR = memo
					'SQL Server: nvarchar, ntext
					'20.06.2017 - Added type ntext for SQL Server (SG Finans)
					Return FieldType_String
                Case "NUMERIC", "DECIMAL", "MONEY", "DBTYPE_NUMERIC", "DBTYPE_VARNUMERIC", "NUMBER" '01.12.2015 added MONEY '08.11.2013 - Added numeric for Progress (Collectors)
                    ' VBDISCUSS 08.06.2017 KJELL - NUMBER tryner hos NHST som Decimal, men funker som Double. - Må pga dette endres tilbake. Vi må se hva dette er
                    '22.06.2016 - Added DBTYPE_NUMERIC, DBTYPE_VARNUMERIC for SISMO, Oracle
                    '06.10.2015 - Added "NUMBER" for Fjordkraft - Microsoft Oracle ODBC
                    'NBNBNBNB: If a new type is added remeber to add it if the field is NULL to return "0" instead of ""
                    'Access: 
                    'SQL Server: decimal
                    Return FieldType_Decimal
                Case "DOUBLE", "DBTYPE_R8", "FLOAT", "SQL_DOUBLE", "DOUBLE PRECISION" '
                    ' NUMBER Flyttet tilbake 18.10.2017 pga Fjordkraft
                    ', "NUMBER" '06.10.2015 - Removed "NUMBER - AND 08.06.2017 added again"
                    'NBNBNBNB: If a new type is added remeber to add it if the field is NULL to return "0" instead of ""
                    'Access: DBTYPE_R8
                    'SQL Server: 
                    If bThisIsOracle Then 'Added the use of bThisIsOracle return return different values depending on the type of DB. (for BKK/Fjordkraft (Oracle) and NHST(?)
                        Return FieldType_Decimal
                    Else
                        Return FieldType_Double
                    End If
                Case "TINYINT", "DBTYPE_UI1"
                    '24.11.2015 - Added TINYINT
                    '06.09.2018 - Added DBTYPE_UI1 for SQL Server (Lindorff)
                    Return FieldType_Byte
                Case "DBTYPE_I2", "SMALLINT" 'Short
                    '24.11.2015 - Added SMALLINT
                    'Access: DBTYPE_I2
                    Return FieldType_Short
                Case "DBTYPE_I4", "INT", "INTEGER", "TINYINT", "SQL_VARIANT", "SYSTEM.INT32", "INT32", "SQL_INTEGER", "INT IDENTITY" '08.11.2013 - Added integer for Progress (Collectors)
                    'NBNBNBNB: If a new type is added remeber to add it if the field is NULL to return "0" instead of ""
                    'Access: DBTYPE_I4
                    'SQL Server: int
                    Return FieldType_Integer
                Case "BIGINT", "DBTYPE_CY", "DBTYPE_I8", "BIGINT IDENTITY"
                    '24.11.2015 - Added BIGINT IDENTITY
                    'Access: 
                    'SQL Server: bigint
                    '25.02.2015 - Added DBTYPE_CY
                    Return FieldType_BigInt
                Case "DBTYPE_BOOL", "BIT", "SQL_BIT", "YESNO" '"BOOLEAN" '08.11.2013 - Added bit for Progress (Collectors)
                    'Access: DBTYPE_BOOL
                    Return FieldType_Boolean
                Case "DBTYPE_DATE", "DATETIME", "FIN_DATE", "DATE", "SQL_TYPE_DATE", "SQL_TYPE_TIMESTAMP", "TIMESTAMP", "DBTYPE_DBTIMESTAMP", "SMALLDATETIME"
                    '22.06.2016 - Added DBTYPE_DBTIMESTAMP for SISMO, Oracle
                    'Access: DBTYPE_DATE, Added TIMESTAMP for Oracle
                    Return FieldType_Date

                Case Else

                    If s.ToUpper.Substring(0, 4) = "FIN_" Or s = "UDD_paycodes" Then
                        ' added 28.08.2014 for AeroGulf, running SQL-server
                        Return FieldType_String

                    Else
                        Throw New Exception(LRS(70008, s) & vbCrLf & vbCrLf & LRS(10016))
                        '"BabelBank is not able to retrieve fields of type: " & s _
                        '& vbCrLf & vbCrLf & "Please contact Your dealer")
                    End If
            End Select

        Catch ex As Exception
            If bShowErrorMessage Then
                MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = LRS(70007) & ex.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
                Return FieldType_Unknown
            End If

        End Try

    End Function
    Private Function RetrieveERPDBProfileInfo(ByVal sConnectionString As String) As Boolean
        Dim myBBConnection As System.Data.Common.DbConnection
        Dim myDBProfileCommand As System.Data.Common.DbCommand
        Dim myDBProfileReader As System.Data.Common.DbDataReader

        'Theese variables are not yet used in the connection/retrieving of data from ERP
        Dim iERPDBProfile_ID As String = ""
        Dim sERPName As String
        Dim bERPRetrieveRSWithOpen = True
        Dim iERPCursorType As Integer
        Dim iERPLockType As Integer
        Dim iERPCursorLocation As Integer

        Dim sErrorString As String
        Dim sMySQL As String
        Dim iDataBaseType As Integer

        Try

            If iCompany_ID < 0.1 Then
                Throw New System.Exception(LRS(70009)) '70009: No Company_ID stated." & vbCrLf & "BabelBank is not able to retrieve the connection information to the ERP database."
            End If

            If iDBProfile_ID < 0.1 Then
                Throw New System.Exception(LRS(70010)) '"No connection are associated with this company")
            End If

            Dim factory As System.Data.Common.DbProviderFactory = System.Data.Common.DbProviderFactories.GetFactory("System.Data.OleDb")

            myBBConnection = factory.CreateConnection()
            myBBConnection.ConnectionString = sConnectionString
            myBBConnection.Open()

            sMySQL = "Select Constring1, Constring2, UID, PWD, DBProfile_ID, Name, RetrieveRSWithOpen, CursorType, LockType, CursorLocation, ProviderType, DatabaseType FROM DB_Profile WHERE Company_ID = " & iCompany_ID.ToString & " AND DBProfile_ID = " & iDBProfile_ID
            myDBProfileCommand = myBBConnection.CreateCommand()
            myDBProfileCommand.CommandText = sMySQL
            myDBProfileCommand.CommandType = CommandType.Text

            myDBProfileReader = myDBProfileCommand.ExecuteReader

            If myDBProfileReader.HasRows Then

                Do While myDBProfileReader.Read()

                    'Element 0 - ConnectionString
                    sERPConnectionString = myDBProfileReader.GetString(0) & myDBProfileReader.GetString(1)

                    'Element 1 - UID
                    If sERPUID = "" Then 'May be set from the calling function
                        sERPUID = myDBProfileReader.GetString(2)
                    Else
                        'Keep the UID set from calling function
                    End If
                    'Element 2 - PWD
                    If sERPPWD = "" Then 'May be set from the calling function
                        sERPPWD = BBDecrypt(myDBProfileReader.GetString(3), 3)
                    Else
                        'Keep the UID set from calling function
                    End If
                    'Element 3 - DBProfile_ID
                    iERPDBProfile_ID = myDBProfileReader.GetInt32(4)
                    'Element 4 - Name
                    sERPName = myDBProfileReader.GetString(5)
                    'Element 5 - RetrieveRSWithOpen
                    If Not myDBProfileReader.IsDBNull(6) Then  ' 13.12.2021 added If
                        bERPRetrieveRSWithOpen = myDBProfileReader.GetBoolean(6) = True
                    Else
                        bERPRetrieveRSWithOpen = False
                    End If

                    'Element 6 - CursorType
                    'NO USE OF CURSOR IMPLEMENTED IN ADO.NET
                    If Not myDBProfileReader.IsDBNull(7) Then
                        iERPCursorType = myDBProfileReader.GetInt32(7)
                        'Else
                        '    iERPCursorType = ADODB.CursorTypeEnum.adOpenStatic
                    End If
                    'Element 7 - LockType
                    If Not myDBProfileReader.IsDBNull(8) Then
                        iERPLockType = myDBProfileReader.GetInt32(8)
                        'Else
                        '    iERPLockType = ADODB.LockTypeEnum.adLockOptimistic
                    End If

                    'Element 8 - CursorLocation
                    If Not myDBProfileReader.IsDBNull(9) Then
                        iERPCursorLocation = myDBProfileReader.GetInt32(9)
                        'Else
                        '    iERPCursorLocation = ADODB.CursorLocationEnum.adUseClient 'TODO, can't use ADODB
                    End If

                    'Element 9 - ProviderType
                    eProviderType = myDBProfileReader.GetInt32(10)

                    ' 08.12.2017 testing;
                    If Not RunTime() Then
                        If eProviderType = ProviderType.WCF Then
                            'bDebug = True
                        End If
                    End If

                    iDataBaseType = myDBProfileReader.GetInt32(11)
                    If iDataBaseType = 2 Then
                        bThisIsOracle = True
                    End If

                Loop

            Else
                Throw New System.Exception(LRS(70011)) '"No connectionstring to the ERP system are stated.")

            End If 'myODBCReader.HasRows

            myDBProfileReader.Close()
            myDBProfileReader = Nothing
            myDBProfileCommand.Dispose()

            myBBConnection.Close()
            myBBConnection.Dispose()

        Catch ex As Exception
            If Not myDBProfileReader Is Nothing Then
                myDBProfileReader.Close()
                myDBProfileReader = Nothing
            End If
            If Not myDBProfileCommand Is Nothing Then
                myDBProfileCommand.Dispose()
            End If
            If Not myBBConnection Is Nothing Then
                myBBConnection.Close()
                myBBConnection = Nothing
            End If

            sErrorString = ""

            sErrorString = LRS(70001)

            'The connection reported the following error:
            sErrorString = sErrorString & vbCrLf & vbCrLf & LRS(70002) & vbCrLf & ex.Message

            If bShowErrorMessage Then
                MsgBox(sErrorString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = sErrorString
            End If

        End Try

        If Not myDBProfileReader Is Nothing Then
            myDBProfileReader.Close()
            myDBProfileReader = Nothing
        End If
        If Not myDBProfileCommand Is Nothing Then
            myDBProfileCommand.Dispose()
        End If
        If Not myBBConnection Is Nothing Then
            myBBConnection.Close()
            myBBConnection = Nothing
        End If

    End Function
    Private Function RetrieveArchiveInfo(ByVal sConnectionString As String) As Boolean
        Dim myBBConnection As System.Data.Common.DbConnection
        Dim myDBArchiveCommand As System.Data.Common.DbCommand
        Dim myDBArchiveReader As System.Data.Common.DbDataReader

        'Theese variables are not yet used in the connection/retrieving of data from ERP
        Dim iERPDBProfile_ID As String = ""
        Dim sERPName As String
        Dim bERPRetrieveRSWithOpen = True
        Dim iERPCursorType As Integer
        Dim iERPLockType As Integer
        Dim iERPCursorLocation As Integer

        Dim sErrorString As String
        Dim sMySQL As String

        Try

            If iCompany_ID < 0.1 Then
                Throw New System.Exception(LRS(70009)) '70009: No Company_ID stated." & vbCrLf & "BabelBank is not able to retrieve the connection information to the ERP database."
            End If

            Dim factory As System.Data.Common.DbProviderFactory = System.Data.Common.DbProviderFactories.GetFactory("System.Data.OleDb")

            myBBConnection = factory.CreateConnection()
            myBBConnection.ConnectionString = sConnectionString
            myBBConnection.Open()

            sMySQL = "SELECT ArchiveConnString, "
            sMySQL = sMySQL & "ArchiveUID, ArchivePWD, ArchiveProvider "
            sMySQL = sMySQL & "FROM Company WHERE Company_ID = " & iCompany_ID.ToString

            myDBArchiveCommand = myBBConnection.CreateCommand()
            myDBArchiveCommand.CommandText = sMySQL
            myDBArchiveCommand.CommandType = CommandType.Text

            myDBArchiveReader = myDBArchiveCommand.ExecuteReader

            If myDBArchiveReader.HasRows Then

                Do While myDBArchiveReader.Read()

                    'Element 0 - ConnectionString
                    sERPConnectionString = myDBArchiveReader.GetString(0) 'ArchiveConnString

                    'Element 1 - UID
                    If sERPUID = "" Then 'May be set from the calling function
                        '27.09.2017 - Added next IF
                        If myDBArchiveReader.IsDBNull(1) Then
                            sERPUID = ""
                        Else
                            sERPUID = myDBArchiveReader.GetString(1) 'ArchiveUID
                        End If
                    Else
                        'Keep the UID set from calling function
                    End If
                    'Element 2 - PWD
                    If sERPPWD = "" Then 'May be set from the calling function
                        If myDBArchiveReader.IsDBNull(2) Then
                            sERPUID = ""
                        Else
                            sERPPWD = BBDecrypt(myDBArchiveReader.GetString(2), 3)
                        End If
                    Else
                        'Keep the UID set from calling function
                    End If
                    'Element 3 - ProviderType
                    eProviderType = myDBArchiveReader.GetInt32(3)

                    bERPRetrieveRSWithOpen = True

                Loop

            Else
                Throw New System.Exception(LRS(70011)) '"No connectionstring to the ERP system are stated.")

            End If 'myODBCReader.HasRows

            myDBArchiveReader.Close()
            myDBArchiveReader = Nothing
            myDBArchiveCommand.Dispose()

            myBBConnection.Close()
            myBBConnection.Dispose()

        Catch ex As Exception
            If Not myDBArchiveReader Is Nothing Then
                myDBArchiveReader.Close()
                myDBArchiveReader = Nothing
            End If
            If Not myDBArchiveCommand Is Nothing Then
                myDBArchiveCommand.Dispose()
            End If
            If Not myBBConnection Is Nothing Then
                myBBConnection.Close()
                myBBConnection = Nothing
            End If

            sErrorString = ""

            sErrorString = LRS(70001)

            'The connection reported the following error:
            sErrorString = sErrorString & vbCrLf & vbCrLf & LRS(70002) & vbCrLf & ex.Message

            If bShowErrorMessage Then
                MsgBox(sErrorString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = sErrorString
            End If

        End Try

        If Not myDBArchiveReader Is Nothing Then
            myDBArchiveReader.Close()
            myDBArchiveReader = Nothing
        End If
        If Not myDBArchiveCommand Is Nothing Then
            myDBArchiveCommand.Dispose()
        End If
        If Not myBBConnection Is Nothing Then
            myBBConnection.Close()
            myBBConnection = Nothing
        End If

    End Function
    Public Sub BB_SetVersion(ByVal sVersion As String)
        sMySQL = "UPDATE Version SET VersionNo = '" & Trim(sVersion) & "'"

        If Me.ExecuteNonQuery Then

            If Me.RecordsAffected = 1 Then
                'OK
            Else
                Throw New Exception("No records affected!" & vbCrLf & Me.ErrorMessage)
            End If

        Else

            Throw New Exception(LRSCommon(45002) & Me.ErrorMessage)

        End If

    End Sub
    Public Sub Close()

        sERPUID = ""
        sERPPWD = ""

        Command_Close()
        Reader_Close()

        readerWCF = Nothing

        If Not ConnERP Is Nothing Then
            If ConnERP.State <> ConnectionState.Closed Then
                'Debug.Print("Lukker Connection!")
                ConnERP.Close()
            End If
            ConnERP.Dispose()
            ConnERP = Nothing
        End If

    End Sub
    Private Function RunWCF_Generic() As Boolean
        ' call the appropriate WEBService;


    End Function
    '----- WCF Functions -----
    Private Function ColumnWCFExist(ByVal readerWCF As ReaderWCF, ByVal columnName As String) As Boolean
        ' Check if a specific columnname exists
        Dim i As Integer

        ' status: råskrevet
        ' testet: nix

        Try
            For i = 0 To readerWCF.FieldCount - 1
                'If readerWCF.GetName(i) Then
                ' 23.08.2019 - changed, because otherwise we get error about Boolean returnvalue
                If readerWCF.GetName(i) <> "" Then
                    Return True
                End If
            Next

            Return False

        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function GetWCFFieldType(ByVal s As String) As Integer
        ' Returns fieldtype based on definitions in .xml file

        ' status: råskrevet
        ' testet: nix

        Try

            Select Case s.ToUpper   ' 08.10.2014 added ToUpper, 10.09.15 lagt inn NVARCHAR2

                Case "STRING"
                    Return FieldType_String
                Case "DOUBLE"
                    Return FieldType_Double
                Case "INT", "INTEGER"
                    Return FieldType_Integer
                Case "BOOLEAN", "BIT"
                    Return FieldType_Boolean
                Case "DATE"
                    Return FieldType_Date

                Case Else
                    Throw New Exception(LRS(70008, s) & vbCrLf & vbCrLf & LRS(10016))
                    '"BabelBank is not able to retrieve fields of type: " & s & vbCrLf & vbCrLf & "Please contact Your dealer")
            End Select

        Catch ex As Exception
            If bShowErrorMessage Then
                MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = LRS(70007) & ex.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
                Return FieldType_Unknown
            End If

        End Try

    End Function
    Public Function GetWCFReaderItemAsString(ByVal ReaderWCF As ReaderWCF, ByVal sFieldName As String) As String
        ' Returns value of iFieldNo from current xml-"record"
        Dim sTypeName As String
        Dim iType As Integer = FieldType_Unknown
        GetWCFReaderItemAsString = ""

        Try

            If bDebug Then
                MsgBox("QWQW - I funksjonen GetWCFReaderItemAsString")
            End If

            'If iFieldNo > -1 Then
            'sTypeName = ReaderWCF.GetDataTypeName(iFieldNo)
            'iType = GetWCFFieldType(sTypeName)
            ' innertext from xml always returns string

            ' if passed with fieldNo, find fieldname!
            If IsNumeric(sFieldName) Then
                If sFieldName = "-1" Then
                    ' field does not exist
                    sFieldName = ""
                Else
                    sFieldName = ReaderWCF.GetName(sFieldName)
                End If
            End If
            If Not EmptyString(sFieldName) Then
                ' special treat of BBRET_Filter
                ' ------------------------------
                If sFieldName = "BBRET_Filter" Then
                    'Dim iFieldNo As Integer = 0
                    Dim sFormat As String = ""
                    Dim sValue As String = ""
                    'iFieldNo = ReaderWCF.GetOrdinal(sFieldName)
                    ' Pick which field to use as filter
                    sFormat = ReaderWCF.GetFormat(ReaderWCF.GetOrdinal(sFieldName))
                    If Not EmptyString(sFormat) Then
                        ' no Format set
                        ' get value from this field
                        sValue = ReaderWCF.GetString(sFormat)
                        ' so far, assume that we have an amountsfield, like invoice rest amount
                        'If CDbl(sValue) > 0 Then
                        ' 27.04.2021 - must check for different to 0, otherwise will have problems with creditnotes
                        If CDbl(sValue) <> 0 Then
                            GetWCFReaderItemAsString = 1
                        Else
                            GetWCFReaderItemAsString = 0
                        End If
                    Else
                        ' get value direct from field BBRET_Filter
                        GetWCFReaderItemAsString = ReaderWCF.GetString(sFieldName)
                    End If
                Else
                    sFieldName = ReaderWCF.GetERPName(sFieldName)   ' Get the "real" ERP fieldname (from aFields(0,
                    GetWCFReaderItemAsString = ReaderWCF.GetString(sFieldName)
                End If
            End If

            'Select Case iType
            '    Case FieldType_String
            '        GetWCFReaderItemAsString = ReaderWCF.GetString(sFieldName)
            '    Case FieldType_Integer
            '        GetWCFReaderItemAsString = ReaderWCF.GetInt32(iFieldNo).ToString
            '    Case FieldType_Double
            '        GetWCFReaderItemAsString = ReaderWCF.GetDouble(iFieldNo).ToString
            '    Case FieldType_Boolean
            '        If ReaderWCF.GetBoolean(iFieldNo) = True Then
            '            GetWCFReaderItemAsString = "True"
            '        Else
            '            GetWCFReaderItemAsString = "False"
            '        End If
            '    Case FieldType_Date
            '        GetWCFReaderItemAsString = ReaderWCF.GetDateTime(iFieldNo).ToString 'Not Tested
            '    Case Else
            '        Throw New Exception(LRS(70008, iType.ToString) & vbCrLf & vbCrLf & LRS(10016))

            '        '"BabelBank is not able to retrieve fields of type: " & Trim(Str(iType)) _
            '        '   & vbCrLf & vbCrLf & "Please contact Your dealer")

            'End Select

            'End If

        Catch ex As Exception

            If bShowErrorMessage Then
                MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(70007))
            Else
                sErrorMessage = LRS(70007) & ex.Message & vbCrLf & vbCrLf & "SQL:" & sMySQL
                Return ""
            End If

        End Try

    End Function

    '===============================================================================================================
    ' Specialized WCF Methods
    '===============================================================================================================
    Friend Function RunWCF_ActiveBrands(ByVal sNameOfRule As String, ByVal sWCF As String) As Boolean
		'' For Active Brands, NAV 2017

        'Dim sWebService As String = ""  ' Name of webservice to run
        'Dim sField As String = ""       ' Which field to search in
        'Dim sCriteria As String = ""    ' Value to search for
        'Dim sXMLString As String = ""
        'Dim lCounter As Long = 0

        'Dim sCustomer_No As String = ""
        'Dim sInvoice_No As String = ""
        'Dim sAmount As String = ""
        'Dim sMatch_Id As String = ""
        'Dim sCurrency As String = ""
        'Dim sName As String = ""
        'Dim sAddress As String = ""
        'Dim sCity As String = ""
        'Dim sZip As String = ""
        'Dim sDueDate As String = ""
        'Dim bOpen As Boolean = True
        'Dim sOriginalAmount As String = ""
        'Dim sDiscountAmount As String = ""
        'Dim dDiscountDate As Date
        'Dim dNetAmount As Double = 0
        'Dim sCardType As String = ""
        'Dim sGlobalDimension1 As String = ""
        'Dim sYourRef As String = ""
        'Dim sBB_Amount As String = ""
        'Dim bFillUpXMLString As Boolean = True

        'Try
        '	' WCF-en må settes opp slik:
        '	' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
        '	' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
        '	' --------------------------------------------------------------
        '	sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
        '	sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
        '	sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567

        '	' for amounts, we will need to divide. do this here
        '	If sField = "Remaining_Amount" Then
        '		If sCriteria.IndexOf("/") > -1 Then
        '			sCriteria = (CDbl(xDelim(sCriteria, "/", 1)) / CDbl(xDelim(sCriteria, "/", 2))).ToString
        '			sCriteria = sCriteria.Replace(",", ".")  ' must have decimal point
        '		End If
        '	End If

        '	' remove special chars which can not be part of criteria
        '	sCriteria = sCriteria.Replace("(", "").Replace(")", "")

        '	readerWCF = New ReaderWCF
        '	readerWCF.NameOfCompany = sCompanyName

        '	Select Case sWebService
        '		' One mappingfile pr WEBservice (NOT pr rule)
        '		Case "BBGet_InvoiceItems"   '"Søk på fakturanummer", "Avstem på faktura"
        '			'-------------------------------------------------------------------

        '			'WEBService specific dimmings;
        '			Dim BB_GetInvoiceItems_Service As New ActiveBrandsWCF.BB_GetInvoiceItems_Service

        '			Dim BBFilter1 As New ActiveBrandsWCF.BB_GetInvoiceItems_Filter
        '			Dim bbFilters() As ActiveBrandsWCF.BB_GetInvoiceItems_Filter   ' one or more BBFilter objects
        '			Dim aReturn() As ActiveBrandsWCF.BB_GetInvoiceItems

        '			' TESTING ONLY
        '			' ============
        '			' Åpne xml-fil
        '			'Dim sFilename As String = ""
        '			'If Not RunTime() Then
        '			'    sFilename = "C:\Slett\ActiveBrands\WEBServices\Soap_Reply_GetInvoiceNumber_Mini.xml"
        '			'Else
        '			'    sFilename = My.Application.Info.DirectoryPath & "\WCF\Soap_Reply_GetInvoiceNumber_Mini.xml"
        '			'End If


        '			' then load definitions from xml-file for this webservice;
        '			' -------------------------------------------------------
        '			readerWCF.LoadXMLDefintions(sWebService)

        '			' TESTING ONLY
        '			' ============
        '			' Import soap-"cheat" xml object into ReaderWCF object
        '			' readerWCF.LoadFromFile(sFilename, "/ISO:Returns/ISO:Result")

        '			' PROD -
        '			' Run the WEBService
        '			' ------------------
        '			Select Case sField
        '				Case "Remaining_Amount"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.GrossAmount   '   .Remaining_Amount

        '				Case "Document_No"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Document_No

        '				Case "Customer_Name"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Name  '    .Customer_Name
        '					' 15.10.2019 - Remove " AS", " A/S", " LTD", " GMBH", " OY", "& CO" " &CO"
        '					sCriteria = sCriteria.ToUpper & " "
        '					sCriteria = Replace(sCriteria, " AS ", "")
        '					sCriteria = Replace(sCriteria, " A/S ", "")
        '					sCriteria = Replace(sCriteria, " LTD ", "")
        '					sCriteria = Replace(sCriteria, " LIMITED ", "")
        '					sCriteria = Replace(sCriteria, " GMBH ", "")
        '					sCriteria = Replace(sCriteria, " OY ", "")
        '					sCriteria = Replace(sCriteria, " OYJ ", "")
        '					sCriteria = Replace(sCriteria, " & CO ", "")
        '					sCriteria = Replace(sCriteria, " &CO ", "")
        '					sCriteria = Replace(sCriteria, " SRL ", "")
        '					sCriteria = Replace(sCriteria, " AB ", "")
        '					sCriteria = Replace(sCriteria, " OG ", "")
        '					sCriteria = Replace(sCriteria, " INC ", "")
        '					sCriteria = Replace(sCriteria, " AG ", "")
        '					sCriteria = Replace(sCriteria, " APS ", "")
        '					sCriteria = "@*" & Trim(sCriteria) & "*"  ' Søk med wildcard og @ for case insenstive

        '				Case "Customer_Address"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Address   '.Customer_Address
        '					sCriteria = "@*" & sCriteria.ToUpper & "*"   ' Søk med wildcard, og @ for case insenstive

        '				Case "Customer_No"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Customer_No

        '				Case "ExternalDocumentNo"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.ExternalDocumentNo

        '				Case "DCCaseNo"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.DCCaseNo

        '				Case "YourReference"
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.YourReference

        '			End Select
        '			BBFilter1.Criteria = sCriteria

        '			' '' Filter2 for companycode (???)
        '			Dim BBFilter2 As New ActiveBrandsWCF.BB_GetInvoiceItems_Filter
        '			BBFilter2.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Document_No
        '			BBFilter2.Criteria = ""  ' Not in use here

        '			If BBFilter2.Criteria = "" Then
        '				bbFilters = New ActiveBrandsWCF.BB_GetInvoiceItems_Filter(0) {BBFilter1}
        '			Else
        '				'Dim bbFilters() As ActiveBrandsWCF.BB_GetInvoiceItems_Filter = New ActiveBrandsWCF.BB_GetInvoiceItems_Filter(0) {BBFilter1}, {BBFilter2}
        '				bbFilters = New ActiveBrandsWCF.BB_GetInvoiceItems_Filter(1) {BBFilter1, BBFilter2}
        '			End If


        '			BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD, "skigutane")
        '			' Calling the actual WEBService
        '			aReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        '			' Dim returnfields for this specific WEBService 
        '			' ---------------------------------------------
        '			'---> Convert the array returndata to an XML object
        '			'<?xml version="1.0" encoding="utf-8"?>
        '			'<Returns>
        '			'  <Result>
        '			'    <BBRET_InvoiceNo>10989445</BBRET_InvoiceNo>
        '			'    <BBRET_Amount>442000</BBRET_Amount>
        '			'    <BBRET_CustomerNo>1473900</BBRET_CustomerNo>
        '			'    <BBRET_MatchID>10989445</BBRET_MatchId>
        '			'  </Result>
        '			'</Returns>

        '			sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '			For lCounter = 0 To aReturn.GetUpperBound(0)
        '				sCustomer_No = aReturn(lCounter).Customer_No
        '				sInvoice_No = aReturn(lCounter).Document_No
        '				sAmount = aReturn(lCounter).GrossAmount
        '				' D=Debitamount (+), C=CreditAmount(-)
        '				sAmount = sAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '				' need to multiply amount with 100
        '				' 12.10.2018 - NEI - vent med dette til vi fyller opp !!!!
        '				'sAmount = (CDbl(sAmount) * 100).ToString
        '				sMatch_Id = aReturn(lCounter).Document_No

        '				sCurrency = aReturn(lCounter).Currency_Code
        '				If EmptyString(sCurrency) Then
        '					sCurrency = "NOK"
        '				End If
        '				sName = RemoveIllegalXMLCharacters(aReturn(lCounter).Name)   '.Customer_Name
        '				sAddress = RemoveIllegalXMLCharacters(aReturn(lCounter).Address)    '.Customer_Address
        '				sCity = aReturn(lCounter).City  '.Customer_City
        '				sZip = aReturn(lCounter).PostCode   '.Customer_Post_Code
        '				sDueDate = aReturn(lCounter).Due_Date.ToString
        '				bOpen = aReturn(lCounter).Open
        '				'sOriginalAmount = aReturn(lCounter).Original_Amount
        '				' D=Debitamount (+), C=CreditAmount(-)
        '				'sOriginalAmount = sOriginalAmount.Replace("C", "-").Replace("D", "")   ' remove C or D

        '				sDiscountAmount = aReturn(lCounter).Original_Pmt_Disc_Possible   '.Pmt_Disc_Given_LCY
        '				' D=Debitamount (+), C=CreditAmount(-)
        '				sDiscountAmount = sDiscountAmount.Replace("C", "-").Replace("D", "")   ' remove C or D

        '				dDiscountDate = aReturn(lCounter).Pmt_Discount_Date

        '				' 14.05.2018 
        '				' added field for kontantrabatt - GlobalDimension1
        '				sGlobalDimension1 = aReturn(lCounter).GlobalDimension1

        '				' 04.06.2019
        '				' added field for OrdreNr from YourRef, to be used for Adyen
        '				'sYourRef = aReturn(lCounter).YourReference
        '				' 23.10.2019 - Take away "illegal" chars from XML-string
        '				sYourRef = CheckForValidCharacters(aReturn(lCounter).YourReference, True, True, True, True, "")

        '				' Specialformatting of amounts, dates, etc. here;
        '				' -----------------------------------------------

        '				' NEW 12.10.2018:
        '				' NAV viser kontantrabatt også for en del fakturaer hvor rabatt dato forlengst er passert.
        '				' Dette gjør at vi får feil beløp i BBRET_Amount.
        '				' La BabelBank avgjøre om det skal være kontantrabatt eller ikke.
        '				' Beregn en margin på 5 dager etter rabattforfall
        '				' Vi har ikke noe payment-date her, men bruker dagens dato
        '				If DateAdd(DateInterval.Day, 5, dDiscountDate) < Date.Today Then
        '					sDiscountAmount = 0
        '				End If

        '				' 28.10.2019 - For YourRef searches, in Automatch, also check if amount is matching.
        '				'               In "ReturnOrder" case there can be 2/3 with same YourRef
        '				bFillUpXMLString = True
        '				If aReturn.GetUpperBound(0) > 0 Then
        '					If sField = "YourReference" Then
        '						' set BB_Amount as 4th element in sWCF
        '						sBB_Amount = xDelim(sWCF, ",", 4, Chr(34))
        '						If Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString = sBB_Amount Then
        '							bFillUpXMLString = True
        '						Else
        '							bFillUpXMLString = False
        '						End If
        '					End If
        '				End If

        '				If bFillUpXMLString Then
        '					sXMLString = sXMLString & "<Result>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '					' 12.10.2018 - pga endringer i WEBServicen eller NAV, får vi beløpene ut helt annerledes enn tidligere.
        '					'               derfor er de to neste linjene endret
        '					' ---------> MERK AT DETTE MÅ REVERSERES NÅR DET BLIR KORREKT FRA NAV.
        '					'            Dette er tatt opp med Meriem 17.10.2018, og hun vil se på dette.
        '					sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
        '					sXMLString = sXMLString & "    <Brutto>" & Format(CDbl(sAmount), "##,##0.00") & "</Brutto>" & vbCrLf
        '					'sXMLString = sXMLString & "    <BBRET_Amount>" & sAmount & "</BBRET_Amount>" & vbCrLf
        '					'sXMLString = sXMLString & "    <Brutto>" & Format((CDbl(sAmount) / 100 + CDbl(sDiscountAmount)), "##,##0.00") & "</Brutto>" & vbCrLf

        '					sXMLString = sXMLString & "    <KRabatt>" & Format(CDbl(sDiscountAmount), "##,##0.00") & "</KRabatt>" & vbCrLf
        '					sXMLString = sXMLString & "    <RabattDato>" & Format(dDiscountDate, "dd.MM.yy") & "</RabattDato>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '					sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '					sXMLString = sXMLString & "    <BBRET_Dim2>" & sGlobalDimension1 & "</BBRET_Dim2>" & vbCrLf
        '					' 04.06.2019 added YourRef (16)
        '					sXMLString = sXMLString & "    <BBRET_MyField>" & sYourRef & "</BBRET_MyField>" & vbCrLf
        '					sXMLString = sXMLString & "</Result>" & vbCrLf
        '				End If
        '			Next
        '			sXMLString = sXMLString & "</Returns>"

        '			' 24.10.2018 -
        '			' I en overgangsperiode, hvis søk på ExternalDocumentNo, og vi ikke finner noe, søk også på YourReference;
        '			'      ================
        '			' FJERNES CA 01.05.2019
        '			' ======================
        '			If sField = "ExternalDocumentNo" Then
        '				If lCounter = 0 Then
        '					' søk etter YourRef kun hvis vi ikke finner noe i "ExternalDocumentNo"
        '					' ----------------------------------------------------------------------
        '					BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.YourReference
        '					BBFilter1.Criteria = sCriteria
        '					bbFilters = New ActiveBrandsWCF.BB_GetInvoiceItems_Filter(0) {BBFilter1}

        '					BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD, "skigutane")
        '					' Calling the actual WEBService
        '					aReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        '					sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '					For lCounter = 0 To aReturn.GetUpperBound(0)
        '						sCustomer_No = aReturn(lCounter).Customer_No
        '						sInvoice_No = aReturn(lCounter).Document_No
        '						sAmount = aReturn(lCounter).GrossAmount
        '						' D=Debitamount (+), C=CreditAmount(-)
        '						sAmount = sAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '						sMatch_Id = aReturn(lCounter).Document_No

        '						sCurrency = aReturn(lCounter).Currency_Code
        '						If EmptyString(sCurrency) Then
        '							sCurrency = "NOK"
        '						End If
        '						sName = RemoveIllegalXMLCharacters(aReturn(lCounter).Name)   '.Customer_Name
        '						sAddress = RemoveIllegalXMLCharacters(aReturn(lCounter).Address)    '.Customer_Address
        '						sCity = aReturn(lCounter).City  '.Customer_City
        '						sZip = aReturn(lCounter).PostCode   '.Customer_Post_Code
        '						sDueDate = aReturn(lCounter).Due_Date.ToString
        '						bOpen = aReturn(lCounter).Open
        '						sDiscountAmount = aReturn(lCounter).Original_Pmt_Disc_Possible   '.Pmt_Disc_Given_LCY
        '						' D=Debitamount (+), C=CreditAmount(-)
        '						sDiscountAmount = sDiscountAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '						dDiscountDate = aReturn(lCounter).Pmt_Discount_Date
        '						sGlobalDimension1 = aReturn(lCounter).GlobalDimension1

        '						' Vi har ikke noe payment-date her, men bruker dagens dato
        '						If DateAdd(DateInterval.Day, 5, dDiscountDate) < Date.Today Then
        '							sDiscountAmount = 0
        '						End If
        '						sXMLString = sXMLString & "<Result>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '						' 12.10.2018 - pga endringer i WEBServicen eller NAV, får vi beløpene ut helt annerledes enn tidligere.
        '						'               derfor er de to neste linjene endret
        '						' ---------> MERK AT DETTE MÅ REVERSERES NÅR DET BLIR KORREKT FRA NAV.
        '						'            Dette er tatt opp med Meriem 17.10.2018, og hun vil se på dette.
        '						sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
        '						sXMLString = sXMLString & "    <Brutto>" & Format(CDbl(sAmount), "##,##0.00") & "</Brutto>" & vbCrLf
        '						sXMLString = sXMLString & "    <KRabatt>" & Format(CDbl(sDiscountAmount), "##,##0.00") & "</KRabatt>" & vbCrLf
        '						sXMLString = sXMLString & "    <RabattDato>" & Format(dDiscountDate, "dd.MM.yy") & "</RabattDato>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '						sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '						sXMLString = sXMLString & "    <BBRET_Dim2>" & sGlobalDimension1 & "</BBRET_Dim2>" & vbCrLf
        '						' 04.06.2019 added YourRef
        '						sXMLString = sXMLString & "    <BBRET_MyField>" & sYourRef & "</BBRET_MyField>" & vbCrLf
        '						sXMLString = sXMLString & "</Result>" & vbCrLf

        '					Next
        '					sXMLString = sXMLString & "</Returns>"
        '					' Fjernes ca 01.05.2019 til hit----------------------------------------------------------------------
        '				End If
        '			End If

        '			' Load XML returnstring into readerWCF XMLdoc object
        '			readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

        '			If readerWCF.RecordCount > 0 Then
        '				bReader_HasRows = True
        '				bFirstRecordRead = False
        '				bLastRecordRead = False
        '			End If

        '		Case "BBGet_Transactions"   '"Søk på Tellerposter
        '			' SKAL FORELØPIG IKKE BRUKES !!! Butikkdrift skal beholdes i "gamle" NAV
        '			'-------------------------------------------------------------------

        '			'WEBService specific dimmings;
        '			'Dim BB_GetTransactions_Service As New ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Service

        '			'Dim BBFilter1_Transactions As New ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Filter
        '			'Dim bbFilters_Transactions() As ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Filter
        '			'Dim aReturn_Transactions() As ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions   '   .BB_GetInvoiceItems

        '			'' then load definitions from xml-file for this webservice;
        '			'' -------------------------------------------------------
        '			'readerWCF.LoadXMLDefintions(sWebService)


        '			'' PROD -
        '			'' Run the WEBService
        '			'' ------------------
        '			'Select Case sField
        '			'    Case "Amount"
        '			'        'BBFilter1.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Amount

        '			'        ' legg til flere spørringer her
        '			'End Select
        '			'BBFilter1_Transactions.Criteria = sCriteria

        '			'' '' Filter2 for companycode (???)
        '			'' TÒDO - do we need companycode?
        '			'Dim BBFilter2_Transactions As New ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Filter
        '			'BBFilter2_Transactions.Field = ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Fields.Document_No
        '			'BBFilter2_Transactions.Criteria = ""  ' Not in use here

        '			'If BBFilter2_Transactions.Criteria = "" Then
        '			'    bbFilters_Transactions = New ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Filter(0) {BBFilter1_Transactions}
        '			'Else
        '			'    bbFilters_Transactions = New ActiveBrandsWCF_BB_GetTransactions.BB_GetTransactions_Filter(1) {BBFilter1_Transactions, BBFilter2_Transactions}
        '			'End If


        '			'BB_GetTransactions_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD, "skigutane")
        '			'' Calling the actual WEBService
        '			'aReturn_Transactions = BB_GetTransactions_Service.ReadMultiple(bbFilters_Transactions, Nothing, 0)


        '			'sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '			'For lCounter = 0 To aReturn_Transactions.GetUpperBound(0)
        '			'    sInvoice_No = aReturn_Transactions(lCounter).Document_No
        '			'    sAmount = aReturn_Transactions(lCounter).Amount
        '			'    ' need to multiply amount with 100
        '			'    sAmount = (CDbl(sAmount) * 100).ToString
        '			'    sCurrency = aReturn_Transactions(lCounter).Currency_Code
        '			'    sDueDate = aReturn_Transactions(lCounter).Posting_Date
        '			'    sCustomer_No = aReturn_Transactions(lCounter).Customer_No
        '			'    'sCardType = aReturn(lCounter).CardType
        '			'    sMatch_Id = aReturn_Transactions(lCounter).Document_No

        '			'    If EmptyString(sCurrency) Then
        '			'        sCurrency = "NOK"
        '			'    End If

        '			'    ' Specialformatting of amounts, dates, etc. here;
        '			'    ' -----------------------------------------------
        '			'    sXMLString = sXMLString & "<Result>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <BBRET_Amount>" & sAmount & "</BBRET_Amount>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <CardType>" & sDueDate & "</CardType>" & vbCrLf
        '			'    sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '			'    sXMLString = sXMLString & "</Result>" & vbCrLf

        '			'Next
        '			'sXMLString = sXMLString & "</Returns>"

        '			'' Load XML returnstring into readerWCF XMLdoc object
        '			'readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

        '			'If readerWCF.RecordCount > 0 Then
        '			'    bReader_HasRows = True
        '			'    bFirstRecordRead = False
        '			'    bLastRecordRead = False
        '			'End If

        '			'Case "BB_Validate"   'Valider kundenummer
        '			'    '-------------------------------------------------------------------

        '			'    'WEBService specific dimmings;
        '			'    Dim BB_Validate_Service As New ActiveBrandsWCF_BB_Validate.BB_Validate

        '			'    Dim BBFilter1_Validate As New ActiveBrandsWCF_BB_Validate.BB_Validate_Filter
        '			'    Dim bbFilters_Validate() As ActiveBrandsWCF_BB_Validate.BB_Validate_Filter
        '			'    Dim aReturn() As ActiveBrandsWCF_BB_Validate.BB_Validate

        '			'    ' then load definitions from xml-file for this webservice;
        '			'    ' -------------------------------------------------------
        '			'    readerWCF.LoadXMLDefintions(sWebService)


        '			'    ' PROD -
        '			'    ' Run the WEBService
        '			'    ' ------------------
        '			'    Select Case sField
        '			'        Case "Customer_No"
        '			'            BBFilter1_Validate.Field = ActiveBrandsWCF_BB_Validate.BB_Validate_Fields.No


        '			'    End Select
        '			'    BBFilter1_Validate.Criteria = sCriteria
        '			'    bbFilters_Validate = New ActiveBrandsWCF_BB_Validate.BB_Validate_Filter(0) {BBFilter1_Validate}


        '			'    'BB_Validate_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD, "skigutane")
        '			'    ' Calling the actual WEBService
        '			'    'aReturn = BB_Validate_Service.  .ReadMultiple(bbFilters, Nothing, 0)

        '			'    sName = BB_Validate_Service.Name

        '			'    ' test om vi får noen verdi i navn, hvis ikke, gjør ingenting!
        '			'    If Not EmptyString(sName) Then
        '			'        sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '			'        'For lCounter = 0 To aReturn.GetUpperBound(0)
        '			'        sName = aReturn(lCounter).Name   '  Customer_Name

        '			'        ' Specialformatting of amounts, dates, etc. here;
        '			'        ' -----------------------------------------------
        '			'        sXMLString = sXMLString & "<Result>" & vbCrLf
        '			'        sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '			'        sXMLString = sXMLString & "</Result>" & vbCrLf

        '			'        'Next
        '			'        sXMLString = sXMLString & "</Returns>"

        '			'        ' Load XML returnstring into readerWCF XMLdoc object
        '			'        readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

        '			'        If readerWCF.RecordCount > 0 Then
        '			'            bReader_HasRows = True
        '			'            bFirstRecordRead = False
        '			'            bLastRecordRead = False
        '			'        End If
        '			'    End If

        '	End Select

        'Catch ex As Exception
        '	Err.Raise(11151, "DAL.RunWCF_ActiveBrands", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)

        'End Try

	End Function
	Friend Function RunWCF_ActiveBrands_BCNY(ByVal sNameOfRule As String, ByVal sWCF As String) As Boolean
        ' '' For Active Brands, BC Test

        '      Dim sWebService As String = ""  ' Name of webservice to run
        '      Dim sField As String = ""       ' Which field to search in
        '      Dim sCriteria As String = ""    ' Value to search for
        '      Dim sCriteria2 As String
        '      Dim sXMLString As String = ""
        '      Dim lCounter As Long = 0

        '      Dim sCustomer_No As String = ""
        '      Dim sInvoice_No As String = ""
        '      Dim sAmount As String = ""
        '      Dim sMatch_Id As String = ""
        '      Dim sCurrency As String = ""
        '      Dim sName As String = ""
        '      Dim sAddress As String = ""
        '      Dim sCity As String = ""
        '      Dim sZip As String = ""
        '      Dim sDueDate As String = ""
        '      Dim bOpen As Boolean = True
        '      Dim sOriginalAmount As String = ""
        '      Dim sDiscountAmount As String = ""
        '      Dim dDiscountDate As Date
        '      Dim dNetAmount As Double = 0
        '      Dim sCardType As String = ""
        '      Dim sGlobalDimension1 As String = ""
        '      Dim sYourRef As String = ""
        '      Dim sBB_Amount As String = ""
        '      Dim bFillUpXMLString As Boolean = True

        '      Try
        '          ' WCF-en må settes opp slik:
        '          ' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
        '          ' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
        '          ' --------------------------------------------------------------
        '          sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
        '          sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
        '          sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
        '          sCriteria2 = xDelim(sWCF, ",", 4, Chr(34))       ' 1234567

        '          ' for amounts, we will need to divide. do this here
        '          If sField = "Remaining_Amount" Then
        '              ' Bruker vi komma som desimaldeler, havner ører i sCriteria2
        '              sCriteria = sCriteria & "." & sCriteria2
        '              If sCriteria.IndexOf("/") > -1 Then
        '                  ' 20.06.2022 - hvis desimalpunktum, gjør om til komma
        '                  sCriteria = Replace(sCriteria, ".", ",")
        '                  sCriteria = (CDbl(xDelim(sCriteria, "/", 1)) / CDbl(xDelim(sCriteria, "/", 2))).ToString
        '              End If
        '              sCriteria = sCriteria.Replace(",", ".")  ' must have decimal point
        '          End If

        '          ' remove special chars which can not be part of criteria
        '          sCriteria = sCriteria.Replace("(", "").Replace(")", "")

        '          readerWCF = New ReaderWCF
        '          readerWCF.NameOfCompany = sCompanyName

        '          Select Case sWebService
        '              ' One mappingfile pr WEBservice (NOT pr rule)
        '              Case "BBGet_InvoiceItems"   '"Søk på fakturanummer", "Avstem på faktura"
        '                  '-------------------------------------------------------------------

        '                  'WEBService specific dimmings;
        '                  'Dim BB_GetInvoiceItems_Service As New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Service
        '                  Dim BB_GetInvoiceItems_Service As New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Binding

        '                  Dim BBFilter1 As New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter
        '                  Dim bbFilters() As ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter   ' one or more BBFilter objects
        '                  Dim aReturn() As ActiveBrandsBC_Ny.BB_GetInvoiceItems

        '                  ' TESTING ONLY
        '                  ' ============
        '                  ' Åpne xml-fil
        '                  'Dim sFilename As String = ""
        '                  'If Not RunTime() Then
        '                  '    sFilename = "C:\Slett\ActiveBrands\WEBServices\Soap_Reply_GetInvoiceNumber_Mini.xml"
        '                  'Else
        '                  '    sFilename = My.Application.Info.DirectoryPath & "\WCF\Soap_Reply_GetInvoiceNumber_Mini.xml"
        '                  'End If


        '                  ' then load definitions from xml-file for this webservice;
        '                  ' -------------------------------------------------------
        '                  readerWCF.LoadXMLDefintions(sWebService)

        '                  ' TESTING ONLY
        '                  ' ============
        '                  ' Import soap-"cheat" xml object into ReaderWCF object
        '                  ' readerWCF.LoadFromFile(sFilename, "/ISO:Returns/ISO:Result")

        '                  ' PROD -
        '                  ' Run the WEBService
        '                  ' ------------------
        '                  Select Case sField
        '                      Case "Remaining_Amount"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.GrossAmount   '   .Remaining_Amount

        '                      Case "Document_No"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.Document_No

        '                      Case "Customer_Name"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.Name  '    .Customer_Name
        '                          ' 15.10.2019 - Remove " AS", " A/S", " LTD", " GMBH", " OY", "& CO" " &CO"
        '                          sCriteria = sCriteria.ToUpper & " "
        '                          sCriteria = Replace(sCriteria, " AS ", "")
        '                          sCriteria = Replace(sCriteria, " A/S ", "")
        '                          sCriteria = Replace(sCriteria, " LTD ", "")
        '                          sCriteria = Replace(sCriteria, " LIMITED ", "")
        '                          sCriteria = Replace(sCriteria, " GMBH ", "")
        '                          sCriteria = Replace(sCriteria, " OY ", "")
        '                          sCriteria = Replace(sCriteria, " OYJ ", "")
        '                          sCriteria = Replace(sCriteria, " & CO ", "")
        '                          sCriteria = Replace(sCriteria, " &CO ", "")
        '                          sCriteria = Replace(sCriteria, " SRL ", "")
        '                          sCriteria = Replace(sCriteria, " AB ", "")
        '                          sCriteria = Replace(sCriteria, " OG ", "")
        '                          sCriteria = Replace(sCriteria, " INC ", "")
        '                          sCriteria = Replace(sCriteria, " AG ", "")
        '                          sCriteria = Replace(sCriteria, " APS ", "")
        '                          sCriteria = "@*" & Trim(sCriteria) & "*"  ' Søk med wildcard og @ for case insenstive

        '                      Case "Customer_Address"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.Address   '.Customer_Address
        '                          sCriteria = "@*" & sCriteria.ToUpper & "*"   ' Søk med wildcard, og @ for case insenstive

        '                      Case "Customer_No"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.Customer_No

        '                      Case "ExternalDocumentNo"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.ExternalDocumentNo

        '                      Case "DCCaseNo"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.DCCaseNo

        '                      Case "YourReference"
        '                          BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.YourReference

        '                  End Select
        '                  BBFilter1.Criteria = sCriteria

        '                  ' '' Filter2 for companycode (???)
        '                  Dim BBFilter2 As New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter
        '                  BBFilter2.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.Document_No
        '                  BBFilter2.Criteria = ""  ' Not in use here

        '                  If BBFilter2.Criteria = "" Then
        '                      bbFilters = New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter(0) {BBFilter1}
        '                  Else
        '                      'Dim bbFilters() As ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter = New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter(0) {BBFilter1}, {BBFilter2}
        '                      bbFilters = New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter(1) {BBFilter1, BBFilter2}
        '                  End If

        '                  'SKIGUTANE\\NCBEAS

        '                  '/yqEiyRciUlcOQKucoBgZlDQ9dUoRzQ+pUR0S/bA8ao=

        '                  BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD)   ', "skigutane")
        '                  'BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential("NCBEAS", "/yqEiyRciUlcOQKucoBgZlDQ9dUoRzQ+pUR0S/bA8ao=", "SKIGUTANE")    ', "skigutane")

        '                  ' Calling the actual WEBService
        '                  aReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        '                  ' Dim returnfields for this specific WEBService 
        '                  ' ---------------------------------------------
        '                  '---> Convert the array returndata to an XML object
        '                  '<?xml version="1.0" encoding="utf-8"?>
        '                  '<Returns>
        '                  '  <Result>
        '                  '    <BBRET_InvoiceNo>10989445</BBRET_InvoiceNo>
        '                  '    <BBRET_Amount>442000</BBRET_Amount>
        '                  '    <BBRET_CustomerNo>1473900</BBRET_CustomerNo>
        '                  '    <BBRET_MatchID>10989445</BBRET_MatchId>
        '                  '  </Result>
        '                  '</Returns>

        '                  sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '                  For lCounter = 0 To aReturn.GetUpperBound(0)
        '                      sCustomer_No = aReturn(lCounter).Customer_No
        '                      sInvoice_No = aReturn(lCounter).Document_No
        '                      sAmount = aReturn(lCounter).GrossAmount
        '                      ' D=Debitamount (+), C=CreditAmount(-)
        '                      sAmount = sAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '                      ' need to multiply amount with 100
        '                      ' 12.10.2018 - NEI - vent med dette til vi fyller opp !!!!
        '                      'sAmount = (CDbl(sAmount) * 100).ToString
        '                      sMatch_Id = aReturn(lCounter).Document_No

        '                      sCurrency = aReturn(lCounter).Currency_Code
        '                      If EmptyString(sCurrency) Then
        '                          sCurrency = "NOK"
        '                      End If
        '                      sName = RemoveIllegalXMLCharacters(aReturn(lCounter).Name)   '.Customer_Name
        '                      sAddress = RemoveIllegalXMLCharacters(aReturn(lCounter).Address)    '.Customer_Address
        '                      sCity = aReturn(lCounter).City  '.Customer_City
        '                      sZip = aReturn(lCounter).PostCode   '.Customer_Post_Code
        '                      sDueDate = aReturn(lCounter).Due_Date.ToString
        '                      bOpen = aReturn(lCounter).Open
        '                      'sOriginalAmount = aReturn(lCounter).Original_Amount
        '                      ' D=Debitamount (+), C=CreditAmount(-)
        '                      'sOriginalAmount = sOriginalAmount.Replace("C", "-").Replace("D", "")   ' remove C or D

        '                      sDiscountAmount = aReturn(lCounter).Original_Pmt_Disc_Possible   '.Pmt_Disc_Given_LCY
        '                      ' D=Debitamount (+), C=CreditAmount(-)
        '                      sDiscountAmount = sDiscountAmount.Replace("C", "-").Replace("D", "")   ' remove C or D

        '                      dDiscountDate = aReturn(lCounter).Pmt_Discount_Date

        '                      ' 14.05.2018 
        '                      ' added field for kontantrabatt - GlobalDimension1
        '                      sGlobalDimension1 = aReturn(lCounter).GlobalDimension1

        '                      ' 04.06.2019
        '                      ' added field for OrdreNr from YourRef, to be used for Adyen
        '                      'sYourRef = aReturn(lCounter).YourReference
        '                      ' 23.10.2019 - Take away "illegal" chars from XML-string
        '                      sYourRef = CheckForValidCharacters(aReturn(lCounter).YourReference, True, True, True, True, "")

        '                      ' Specialformatting of amounts, dates, etc. here;
        '                      ' -----------------------------------------------

        '                      ' NEW 12.10.2018:
        '                      ' NAV viser kontantrabatt også for en del fakturaer hvor rabatt dato forlengst er passert.
        '                      ' Dette gjør at vi får feil beløp i BBRET_Amount.
        '                      ' La BabelBank avgjøre om det skal være kontantrabatt eller ikke.
        '                      ' Beregn en margin på 5 dager etter rabattforfall
        '                      ' Vi har ikke noe payment-date her, men bruker dagens dato
        '                      If DateAdd(DateInterval.Day, 5, dDiscountDate) < Date.Today Then
        '                          sDiscountAmount = 0
        '                      End If

        '                      ' 28.10.2019 - For YourRef searches, in Automatch, also check if amount is matching.
        '                      '               In "ReturnOrder" case there can be 2/3 with same YourRef
        '                      bFillUpXMLString = True
        '                      If aReturn.GetUpperBound(0) > 0 Then
        '                          If sField = "YourReference" Then
        '                              ' set BB_Amount as 4th element in sWCF
        '                              sBB_Amount = xDelim(sWCF, ",", 4, Chr(34))
        '                              If Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString = sBB_Amount Then
        '                                  bFillUpXMLString = True
        '                              Else
        '                                  bFillUpXMLString = False
        '                              End If
        '                          End If
        '                      End If

        '                      If bFillUpXMLString Then
        '                          sXMLString = sXMLString & "<Result>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '                          ' 12.10.2018 - pga endringer i WEBServicen eller NAV, får vi beløpene ut helt annerledes enn tidligere.
        '                          '               derfor er de to neste linjene endret
        '                          ' ---------> MERK AT DETTE MÅ REVERSERES NÅR DET BLIR KORREKT FRA NAV.
        '                          '            Dette er tatt opp med Meriem 17.10.2018, og hun vil se på dette.
        '                          sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
        '                          sXMLString = sXMLString & "    <Brutto>" & Format(CDbl(sAmount), "##,##0.00") & "</Brutto>" & vbCrLf
        '                          'sXMLString = sXMLString & "    <BBRET_Amount>" & sAmount & "</BBRET_Amount>" & vbCrLf
        '                          'sXMLString = sXMLString & "    <Brutto>" & Format((CDbl(sAmount) / 100 + CDbl(sDiscountAmount)), "##,##0.00") & "</Brutto>" & vbCrLf

        '                          sXMLString = sXMLString & "    <KRabatt>" & Format(CDbl(sDiscountAmount), "##,##0.00") & "</KRabatt>" & vbCrLf
        '                          sXMLString = sXMLString & "    <RabattDato>" & Format(dDiscountDate, "dd.MM.yy") & "</RabattDato>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '                          sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Dim2>" & sGlobalDimension1 & "</BBRET_Dim2>" & vbCrLf
        '                          ' 04.06.2019 added YourRef (16)
        '                          sXMLString = sXMLString & "    <BBRET_MyField>" & sYourRef & "</BBRET_MyField>" & vbCrLf
        '                          sXMLString = sXMLString & "</Result>" & vbCrLf
        '                      End If
        '                  Next
        '                  sXMLString = sXMLString & "</Returns>"

        '                  ' 24.10.2018 -
        '                  ' I en overgangsperiode, hvis søk på ExternalDocumentNo, og vi ikke finner noe, søk også på YourReference;
        '                  '      ================
        '                  ' FJERNES CA 01.05.2019
        '                  ' ======================
        '                  'If sField = "ExternalDocumentNo" Then
        '                  '    If lCounter = 0 Then
        '                  '        ' søk etter YourRef kun hvis vi ikke finner noe i "ExternalDocumentNo"
        '                  '        ' ----------------------------------------------------------------------
        '                  '        BBFilter1.Field = ActiveBrandsBC_Ny.BB_GetInvoiceItems_Fields.YourReference
        '                  '        BBFilter1.Criteria = sCriteria
        '                  '        bbFilters = New ActiveBrandsBC_Ny.BB_GetInvoiceItems_Filter(0) {BBFilter1}

        '                  '        BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD)   ', "skigutane")
        '                  '        ' Calling the actual WEBService
        '                  '        aReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        '                  '        sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '                  '        For lCounter = 0 To aReturn.GetUpperBound(0)
        '                  '            sCustomer_No = aReturn(lCounter).Customer_No
        '                  '            sInvoice_No = aReturn(lCounter).Document_No
        '                  '            sAmount = aReturn(lCounter).GrossAmount
        '                  '            ' D=Debitamount (+), C=CreditAmount(-)
        '                  '            sAmount = sAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '                  '            sMatch_Id = aReturn(lCounter).Document_No

        '                  '            sCurrency = aReturn(lCounter).Currency_Code
        '                  '            If EmptyString(sCurrency) Then
        '                  '                sCurrency = "NOK"
        '                  '            End If
        '                  '            sName = RemoveIllegalXMLCharacters(aReturn(lCounter).Name)   '.Customer_Name
        '                  '            sAddress = RemoveIllegalXMLCharacters(aReturn(lCounter).Address)    '.Customer_Address
        '                  '            sCity = aReturn(lCounter).City  '.Customer_City
        '                  '            sZip = aReturn(lCounter).PostCode   '.Customer_Post_Code
        '                  '            sDueDate = aReturn(lCounter).Due_Date.ToString
        '                  '            bOpen = aReturn(lCounter).Open
        '                  '            sDiscountAmount = aReturn(lCounter).Original_Pmt_Disc_Possible   '.Pmt_Disc_Given_LCY
        '                  '            ' D=Debitamount (+), C=CreditAmount(-)
        '                  '            sDiscountAmount = sDiscountAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '                  '            dDiscountDate = aReturn(lCounter).Pmt_Discount_Date
        '                  '            sGlobalDimension1 = aReturn(lCounter).GlobalDimension1

        '                  '            ' Vi har ikke noe payment-date her, men bruker dagens dato
        '                  '            If DateAdd(DateInterval.Day, 5, dDiscountDate) < Date.Today Then
        '                  '                sDiscountAmount = 0
        '                  '            End If
        '                  '            sXMLString = sXMLString & "<Result>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '                  '            ' 12.10.2018 - pga endringer i WEBServicen eller NAV, får vi beløpene ut helt annerledes enn tidligere.
        '                  '            '               derfor er de to neste linjene endret
        '                  '            ' ---------> MERK AT DETTE MÅ REVERSERES NÅR DET BLIR KORREKT FRA NAV.
        '                  '            '            Dette er tatt opp med Meriem 17.10.2018, og hun vil se på dette.
        '                  '            sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <Brutto>" & Format(CDbl(sAmount), "##,##0.00") & "</Brutto>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <KRabatt>" & Format(CDbl(sDiscountAmount), "##,##0.00") & "</KRabatt>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <RabattDato>" & Format(dDiscountDate, "dd.MM.yy") & "</RabattDato>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '                  '            sXMLString = sXMLString & "    <BBRET_Dim2>" & sGlobalDimension1 & "</BBRET_Dim2>" & vbCrLf
        '                  '            ' 04.06.2019 added YourRef
        '                  '            sXMLString = sXMLString & "    <BBRET_MyField>" & sYourRef & "</BBRET_MyField>" & vbCrLf
        '                  '            sXMLString = sXMLString & "</Result>" & vbCrLf

        '                  '        Next
        '                  '        sXMLString = sXMLString & "</Returns>"
        '                  '        ' Fjernes ca 01.05.2019 til hit----------------------------------------------------------------------
        '                  '    End If
        '                  'End If

        '                  ' Load XML returnstring into readerWCF XMLdoc object
        '                  readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

        '                  If readerWCF.RecordCount > 0 Then
        '                      bReader_HasRows = True
        '                      bFirstRecordRead = False
        '                      bLastRecordRead = False
        '                  End If

        '          End Select

        '      Catch ex As Exception
        '          Err.Raise(11151, "DAL.RunWCF_ActiveBrands_BCNY", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)

        '      End Try

	End Function
	Friend Function RunWCF_ActiveBrandsUS(ByVal sNameOfRule As String, ByVal sWCF As String) As Boolean
        ' '' For ECom US Active Brands USA, NAV 2017

        '      Dim sWebService As String = ""  ' Name of webservice to run
        '      Dim sField As String = ""       ' Which field to search in
        '      Dim sCriteria As String = ""    ' Value to search for
        '      Dim sXMLString As String = ""
        '      Dim lCounter As Long = 0

        '      Dim sCustomer_No As String = ""
        '      Dim sInvoice_No As String = ""
        '      Dim sAmount As String = ""
        '      Dim sMatch_Id As String = ""
        '      Dim sCurrency As String = ""
        '      Dim sName As String = ""
        '      Dim sAddress As String = ""
        '      Dim sCity As String = ""
        '      Dim sZip As String = ""
        '      Dim sDueDate As String = ""
        '      Dim bOpen As Boolean = True
        '      Dim sOriginalAmount As String = ""
        '      Dim sDiscountAmount As String = ""
        '      Dim dDiscountDate As Date
        '      Dim dNetAmount As Double = 0
        '      Dim sCardType As String = ""
        '      Dim sGlobalDimension1 As String = ""
        '      Dim sYourRef As String = ""
        '      Dim sBB_Amount As String = ""
        '      Dim bFillUpXMLString As Boolean = True

        '      Try
        '          ' WCF-en må settes opp slik:
        '          ' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
        '          ' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
        '          ' --------------------------------------------------------------
        '          sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
        '          sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
        '          sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567

        '          ' for amounts, we will need to divide. do this here
        '          If sField = "Remaining_Amount" Then
        '              If sCriteria.IndexOf("/") > -1 Then
        '                  sCriteria = (CDbl(xDelim(sCriteria, "/", 1)) / CDbl(xDelim(sCriteria, "/", 2))).ToString
        '                  sCriteria = sCriteria.Replace(",", ".")  ' must have decimal point
        '              End If
        '          End If

        '          ' remove special chars which can not be part of criteria
        '          sCriteria = sCriteria.Replace("(", "").Replace(")", "")

        '          readerWCF = New ReaderWCF
        '          readerWCF.NameOfCompany = sCompanyName

        '          Select Case sWebService
        '              ' One mappingfile pr WEBservice (NOT pr rule)
        '              Case "BBGet_InvoiceItems"   '"Søk på fakturanummer", "Avstem på faktura"
        '                  '-------------------------------------------------------------------

        '                  'WEBService specific dimmings;
        '                  Dim BB_GetInvoiceItems_Service As New ActiveBrandsUS.BB_GetInvoiceItems_Service

        '                  Dim BBFilter1 As New ActiveBrandsUS.BB_GetInvoiceItems_Filter
        '                  Dim bbFilters() As ActiveBrandsUS.BB_GetInvoiceItems_Filter   ' one or more BBFilter objects
        '                  Dim aReturn() As ActiveBrandsUS.BB_GetInvoiceItems

        '                  ' then load definitions from xml-file for this webservice;
        '                  ' -------------------------------------------------------
        '                  readerWCF.LoadXMLDefintions(sWebService)

        '                  ' Run the WEBService
        '                  ' ------------------
        '                  Select Case sField
        '                      Case "Remaining_Amount"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.GrossAmount   '   .Remaining_Amount

        '                      Case "Document_No"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.Document_No

        '                      Case "Customer_Name"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.Name  '    .Customer_Name
        '                          ' 15.10.2019 - Remove " AS", " A/S", " LTD", " GMBH", " OY", "& CO" " &CO"
        '                          sCriteria = sCriteria.ToUpper & " "
        '                          sCriteria = Replace(sCriteria, " AS ", "")
        '                          sCriteria = Replace(sCriteria, " A/S ", "")
        '                          sCriteria = Replace(sCriteria, " LTD ", "")
        '                          sCriteria = Replace(sCriteria, " LIMITED ", "")
        '                          sCriteria = Replace(sCriteria, " GMBH ", "")
        '                          sCriteria = Replace(sCriteria, " OY ", "")
        '                          sCriteria = Replace(sCriteria, " OYJ ", "")
        '                          sCriteria = Replace(sCriteria, " & CO ", "")
        '                          sCriteria = Replace(sCriteria, " &CO ", "")
        '                          sCriteria = Replace(sCriteria, " SRL ", "")
        '                          sCriteria = Replace(sCriteria, " AB ", "")
        '                          sCriteria = Replace(sCriteria, " OG ", "")
        '                          sCriteria = Replace(sCriteria, " INC ", "")
        '                          sCriteria = Replace(sCriteria, " INC. ", "")
        '                          sCriteria = Replace(sCriteria, " AG ", "")
        '                          sCriteria = Replace(sCriteria, " APS ", "")
        '                          sCriteria = "@*" & Trim(sCriteria) & "*"  ' Søk med wildcard og @ for case insenstive

        '                      Case "Customer_Address"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.Address   '.Customer_Address
        '                          sCriteria = "@*" & sCriteria.ToUpper & "*"   ' Søk med wildcard, og @ for case insenstive

        '                      Case "Customer_No"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.Customer_No

        '                      Case "ExternalDocumentNo"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.ExternalDocumentNo

        '                      Case "DCCaseNo"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.DCCaseNo

        '                      Case "YourReference"
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.YourReference

        '                  End Select
        '                  BBFilter1.Criteria = sCriteria

        '                  ' '' Filter2 for companycode (???)
        '                  Dim BBFilter2 As New ActiveBrandsUS.BB_GetInvoiceItems_Filter
        '                  BBFilter2.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.Document_No
        '                  BBFilter2.Criteria = ""  ' Not in use here

        '                  If BBFilter2.Criteria = "" Then
        '                      bbFilters = New ActiveBrandsUS.BB_GetInvoiceItems_Filter(0) {BBFilter1}
        '                  Else
        '                      'Dim bbFilters() As ActiveBrandsUS.BB_GetInvoiceItems_Filter = New ActiveBrandsUS.BB_GetInvoiceItems_Filter(0) {BBFilter1}, {BBFilter2}
        '                      bbFilters = New ActiveBrandsUS.BB_GetInvoiceItems_Filter(1) {BBFilter1, BBFilter2}
        '                  End If


        '                  BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD)    ', "skigutane")
        '                  ' Calling the actual WEBService
        '                  aReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        '                  ' Dim returnfields for this specific WEBService 
        '                  ' ---------------------------------------------
        '                  '---> Convert the array returndata to an XML object
        '                  '<?xml version="1.0" encoding="utf-8"?>
        '                  '<Returns>
        '                  '  <Result>
        '                  '    <BBRET_InvoiceNo>10989445</BBRET_InvoiceNo>
        '                  '    <BBRET_Amount>442000</BBRET_Amount>
        '                  '    <BBRET_CustomerNo>1473900</BBRET_CustomerNo>
        '                  '    <BBRET_MatchID>10989445</BBRET_MatchId>
        '                  '  </Result>
        '                  '</Returns>

        '                  sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '                  For lCounter = 0 To aReturn.GetUpperBound(0)
        '                      sCustomer_No = aReturn(lCounter).Customer_No
        '                      sInvoice_No = aReturn(lCounter).Document_No
        '                      sAmount = aReturn(lCounter).GrossAmount
        '                      ' D=Debitamount (+), C=CreditAmount(-)
        '                      sAmount = sAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '                      sMatch_Id = aReturn(lCounter).Document_No

        '                      sCurrency = aReturn(lCounter).Currency_Code
        '                      If EmptyString(sCurrency) Then
        '                          sCurrency = "USD"
        '                      End If
        '                      sName = RemoveIllegalXMLCharacters(aReturn(lCounter).Name)   '.Customer_Name
        '                      sAddress = RemoveIllegalXMLCharacters(aReturn(lCounter).Address)    '.Customer_Address
        '                      sCity = aReturn(lCounter).City  '.Customer_City
        '                      sZip = aReturn(lCounter).PostCode   '.Customer_Post_Code
        '                      sDueDate = aReturn(lCounter).Due_Date.ToString
        '                      bOpen = aReturn(lCounter).Open
        '                      ' D=Debitamount (+), C=CreditAmount(-)
        '                      sDiscountAmount = aReturn(lCounter).Original_Pmt_Disc_Possible   '.Pmt_Disc_Given_LCY
        '                      ' D=Debitamount (+), C=CreditAmount(-)
        '                      sDiscountAmount = sDiscountAmount.Replace("C", "-").Replace("D", "")   ' remove C or D

        '                      dDiscountDate = aReturn(lCounter).Pmt_Discount_Date

        '                      ' 14.05.2018 
        '                      ' added field for kontantrabatt - GlobalDimension1
        '                      sGlobalDimension1 = aReturn(lCounter).GlobalDimension1

        '                      ' 04.06.2019
        '                      ' added field for OrdreNr from YourRef, to be used for Adyen
        '                      'sYourRef = aReturn(lCounter).YourReference
        '                      ' 23.10.2019 - Take away "illegal" chars from XML-string
        '                      sYourRef = CheckForValidCharacters(aReturn(lCounter).YourReference, True, True, True, True, "")

        '                      ' Specialformatting of amounts, dates, etc. here;
        '                      ' -----------------------------------------------

        '                      ' NEW 12.10.2018:
        '                      ' NAV viser kontantrabatt også for en del fakturaer hvor rabatt dato forlengst er passert.
        '                      ' Dette gjør at vi får feil beløp i BBRET_Amount.
        '                      ' La BabelBank avgjøre om det skal være kontantrabatt eller ikke.
        '                      ' Beregn en margin på 5 dager etter rabattforfall
        '                      ' Vi har ikke noe payment-date her, men bruker dagens dato
        '                      If DateAdd(DateInterval.Day, 5, dDiscountDate) < Date.Today Then
        '                          sDiscountAmount = 0
        '                      End If

        '                      ' 28.10.2019 - For YourRef searches, in Automatch, also check if amount is matching.
        '                      '               In "ReturnOrder" case there can be 2/3 with same YourRef
        '                      bFillUpXMLString = True
        '                      If aReturn.GetUpperBound(0) > 0 Then
        '                          If sField = "YourReference" Then
        '                              ' set BB_Amount as 4th element in sWCF
        '                              sBB_Amount = xDelim(sWCF, ",", 4, Chr(34))
        '                              If Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString = sBB_Amount Then
        '                                  bFillUpXMLString = True
        '                              Else
        '                                  bFillUpXMLString = False
        '                              End If
        '                          End If
        '                      End If

        '                      If bFillUpXMLString Then
        '                          sXMLString = sXMLString & "<Result>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '                          ' 12.10.2018 - pga endringer i WEBServicen eller NAV, får vi beløpene ut helt annerledes enn tidligere.
        '                          '               derfor er de to neste linjene endret
        '                          ' ---------> MERK AT DETTE MÅ REVERSERES NÅR DET BLIR KORREKT FRA NAV.
        '                          '            Dette er tatt opp med Meriem 17.10.2018, og hun vil se på dette.
        '                          sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
        '                          sXMLString = sXMLString & "    <Brutto>" & Format(CDbl(sAmount), "##,##0.00") & "</Brutto>" & vbCrLf
        '                          'sXMLString = sXMLString & "    <BBRET_Amount>" & sAmount & "</BBRET_Amount>" & vbCrLf
        '                          'sXMLString = sXMLString & "    <Brutto>" & Format((CDbl(sAmount) / 100 + CDbl(sDiscountAmount)), "##,##0.00") & "</Brutto>" & vbCrLf

        '                          sXMLString = sXMLString & "    <KRabatt>" & Format(CDbl(sDiscountAmount), "##,##0.00") & "</KRabatt>" & vbCrLf
        '                          sXMLString = sXMLString & "    <RabattDato>" & Format(dDiscountDate, "dd.MM.yy") & "</RabattDato>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '                          sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_Dim2>" & sGlobalDimension1 & "</BBRET_Dim2>" & vbCrLf
        '                          sXMLString = sXMLString & "    <BBRET_MyField>" & sYourRef & "</BBRET_MyField>" & vbCrLf
        '                          sXMLString = sXMLString & "</Result>" & vbCrLf
        '                      End If
        '                  Next
        '                  sXMLString = sXMLString & "</Returns>"

        '                  ' 24.10.2018 -
        '                  ' I en overgangsperiode, hvis søk på ExternalDocumentNo, og vi ikke finner noe, søk også på YourReference;
        '                  '      ================
        '                  ' FJERNES CA 01.05.2019
        '                  ' ======================
        '                  If sField = "ExternalDocumentNo" Then
        '                      If lCounter = 0 Then
        '                          ' søk etter YourRef kun hvis vi ikke finner noe i "ExternalDocumentNo"
        '                          ' ----------------------------------------------------------------------
        '                          BBFilter1.Field = ActiveBrandsUS.BB_GetInvoiceItems_Fields.YourReference
        '                          BBFilter1.Criteria = sCriteria
        '                          bbFilters = New ActiveBrandsUS.BB_GetInvoiceItems_Filter(0) {BBFilter1}

        '                          BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential(sERPUID, sERPPWD, "skigutane")
        '                          ' Calling the actual WEBService
        '                          aReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        '                          sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
        '                          For lCounter = 0 To aReturn.GetUpperBound(0)
        '                              sCustomer_No = aReturn(lCounter).Customer_No
        '                              sInvoice_No = aReturn(lCounter).Document_No
        '                              sAmount = aReturn(lCounter).GrossAmount
        '                              ' D=Debitamount (+), C=CreditAmount(-)
        '                              sAmount = sAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '                              sMatch_Id = aReturn(lCounter).Document_No

        '                              sCurrency = aReturn(lCounter).Currency_Code
        '                              If EmptyString(sCurrency) Then
        '                                  sCurrency = "NOK"
        '                              End If
        '                              sName = RemoveIllegalXMLCharacters(aReturn(lCounter).Name)   '.Customer_Name
        '                              sAddress = RemoveIllegalXMLCharacters(aReturn(lCounter).Address)    '.Customer_Address
        '                              sCity = aReturn(lCounter).City  '.Customer_City
        '                              sZip = aReturn(lCounter).PostCode   '.Customer_Post_Code
        '                              sDueDate = aReturn(lCounter).Due_Date.ToString
        '                              bOpen = aReturn(lCounter).Open
        '                              sDiscountAmount = aReturn(lCounter).Original_Pmt_Disc_Possible   '.Pmt_Disc_Given_LCY
        '                              ' D=Debitamount (+), C=CreditAmount(-)
        '                              sDiscountAmount = sDiscountAmount.Replace("C", "-").Replace("D", "")   ' remove C or D
        '                              dDiscountDate = aReturn(lCounter).Pmt_Discount_Date
        '                              sGlobalDimension1 = aReturn(lCounter).GlobalDimension1

        '                              ' Vi har ikke noe payment-date her, men bruker dagens dato
        '                              If DateAdd(DateInterval.Day, 5, dDiscountDate) < Date.Today Then
        '                                  sDiscountAmount = 0
        '                              End If
        '                              sXMLString = sXMLString & "<Result>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
        '                              ' 12.10.2018 - pga endringer i WEBServicen eller NAV, får vi beløpene ut helt annerledes enn tidligere.
        '                              '               derfor er de to neste linjene endret
        '                              ' ---------> MERK AT DETTE MÅ REVERSERES NÅR DET BLIR KORREKT FRA NAV.
        '                              '            Dette er tatt opp med Meriem 17.10.2018, og hun vil se på dette.
        '                              sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount) - CDbl(sDiscountAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
        '                              sXMLString = sXMLString & "    <Brutto>" & Format(CDbl(sAmount), "##,##0.00") & "</Brutto>" & vbCrLf
        '                              sXMLString = sXMLString & "    <KRabatt>" & Format(CDbl(sDiscountAmount), "##,##0.00") & "</KRabatt>" & vbCrLf
        '                              sXMLString = sXMLString & "    <RabattDato>" & Format(dDiscountDate, "dd.MM.yy") & "</RabattDato>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
        '                              sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
        '                              sXMLString = sXMLString & "    <BBRET_Dim2>" & sGlobalDimension1 & "</BBRET_Dim2>" & vbCrLf
        '                              ' 04.06.2019 added YourRef
        '                              sXMLString = sXMLString & "    <BBRET_MyField>" & sYourRef & "</BBRET_MyField>" & vbCrLf
        '                              sXMLString = sXMLString & "</Result>" & vbCrLf

        '                          Next
        '                          sXMLString = sXMLString & "</Returns>"
        '                          ' Fjernes ca 01.05.2019 til hit----------------------------------------------------------------------
        '                      End If
        '                  End If

        '                  ' Load XML returnstring into readerWCF XMLdoc object
        '                  readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

        '                  If readerWCF.RecordCount > 0 Then
        '                      bReader_HasRows = True
        '                      bFirstRecordRead = False
        '                      bLastRecordRead = False
        '                  End If
        '          End Select

        '      Catch ex As Exception
        '          Err.Raise(11151, "DAL.RunWCF_ActieBrandsUS", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)

        '      End Try

	End Function
	Friend Function RunWCF_XLedger(ByVal sNameOfRule As String, ByVal sWCF As String) As Boolean
		' For Storm Geo - XLedger
		' Grieg Logistics, flere Grieg selskaper

		Dim sWebService As String = ""  ' Name of webservice to run
		Dim sField As String = ""       ' Which field to search in
		Dim sCriteria As String = ""    ' Value to search for
		Dim sReturnField As String = ""   ' If we need only one value to return
		Dim sWebService2 As String = ""  ' Name of webservice to run
		Dim sField2 As String = ""       ' Which field to search in
		Dim sCriteria2 As String = ""    ' Value to search for
		Dim sReturnField2 As String = "" ' If we need only one value to return
		Dim sXMLString As String = ""
		Dim lCounter As Long = 0
		Dim sTmp1 As String = ""
		Dim sTmp2 As String = ""
		Dim aCriterias() As String      ' returned values from "inner" WEBService

		Dim sApp As String = "XLEDGER"
		Dim iEntity As Integer = 3252  ' find a variable for this, like ClientNo or so ???????
		Dim myExport As XLedger_Export.ExportSoapClient
		Dim sXML As String = ""
		Dim sTotalXML As String = ""
		Dim sFilter As String = ""
		Dim sBaseCurrency As String = ""

		Dim dSearchDate As Date
		Dim i As Integer

		' 17.08.2020 - changed next two to static, so we don't need to logon for each query
		Static sKey As String = ""
		Static mySoapClient As XLedger_Authentication.AuthenticationSoapClient

		Try

			iEntity = Val(sERPClientNo)        ' pass from outside, use clientno

			If iEntity = 0 Then
				' 07.12.2020 - add an extra field for EntityCode - to use in testing
				iEntity = Val(xDelim(sWCF, ",", 5, Chr(34)))
				If iEntity = 0 Then
					iEntity = 3252   ' Storm Geo AS
					'iEntity = 3389  ' Storm Geo Ltd
					iEntity = 25629  ' Grieg Logistics
				End If
			End If
			' Regnskap føres i NOK for StormGeo AS, i GBP for StormGeo Ltd.
			If iEntity = 3389 Then
				sBaseCurrency = "GBP"
			Else
				sBaseCurrency = "NOK"
			End If

			' WCF-en må settes opp slik:
			' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
			' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
			' --------------------------------------------------------------
			sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
			sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
			sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
			sReturnField = xDelim(sWCF, ",", 4, Chr(34))       ' 


			' We can have "nested" WEBServices, meaning that we must first consume one WEBService first, to return a value, 
			' and use this value in the "outer" WEBService, like
			'         "GetCustomerTransactionsData","SubLedgerID", ("GetCustomers2","Description","LIKE BB_MyText*","SubLedgerID")
			' First, consume GetCustomers2, using Description as filter, and return SubLedgerID
			' Then use value of SubLedgerID as filter in GetCustomerTransactionsData
			If InStr(sCriteria, "(") > 0 Then
				' yes, we have an "inner" WEBService to consume first
				' we need to find sCriteria properly
				sCriteria = Mid(sWCF, InStr(sWCF, "("))  '"(GetCustomers2","Description","LIKE BB_MyText*","SubLedgerID")
				sCriteria = Replace(Replace(sCriteria, ")", ""), "(", "")  ' remove ()
				sWebService2 = xDelim(sCriteria, ",", 1, Chr(34))     ' BBGet_InvoiceItems
				sField2 = xDelim(sCriteria, ",", 2, Chr(34))          ' Document_No
				sCriteria2 = xDelim(sCriteria, ",", 3, Chr(34))       ' 1234567
				sReturnField2 = xDelim(sCriteria, ",", 4, Chr(34))       ' SubLedgerID
			End If
			readerWCF = New ReaderWCF
			readerWCF.NameOfCompany = sCompanyName

			'Dim mySoapClient As XLedger_Authentication.AuthenticationSoapClient
			'Dim sKey As String = ""
			'Dim sUID As String = sUID   '"jan-petter@visualbanking.net"
			'Dim sPWD As String = sPWD   '"skeid2002"
			'' TESTING ONLY:
			'sUID = "jan-petter@visualbanking.net"
			'sPWD = "skeid2002"

			'MsgBox("Før authentication")
			'            MsgBox("sERPUID=" & sERPUID & vbNewLine & "sERPPWD=" & sERPPWD)
			'17.08.2020 - Reuse mySoapClient!!
			If EmptyString(sKey) Then
				mySoapClient = New XLedger_Authentication.AuthenticationSoapClient
				sKey = mySoapClient.LogonKey(sERPUID, sERPPWD, sApp)
				'MsgBox("sKey=" & sKey)
			End If

			If Not EmptyString(sWebService2) Then

				Select Case sWebService2
					' One mappingfile pr WEBservice (NOT pr rule)
					Case "GetCustomers2"
						'---------------

						' load definitions from xml-file for this webservice;
						' -------------------------------------------------------
						readerWCF.LoadXMLDefintions(sWebService2)
						myExport = New XLedger_Export.ExportSoapClient

						Dim xlxmlHent As ChkCert = New ChkCert
						myExport.ClientCredentials.ClientCertificate.Certificate = xlxmlHent.CtrW.Certificate

						' set filter
						' ----------

						' for amounts with decimals, we need to manipulate amounts here
						If InStr(sCriteria, "-/-") > 0 Then
							sTmp1 = xDelim(sCriteria, "/", 1)
							sTmp2 = xDelim(sCriteria, "/", 2)
							sCriteria = Replace((CDbl(sTmp1) / CDbl(sTmp2)).ToString, ",", ".")  ' needs . as decimalsep.
						End If
						If InStr(sCriteria, "LIKE") > 0 Then
							sFilter = "[" & sField2 & "] LIKE " & Chr(34) & Replace(sCriteria2, "LIKE", "").Trim & Chr(34)
						Else
							sFilter = "[" & sField2 & "] = " & Chr(34) & sCriteria2 & Chr(34)
						End If

						' Run the "inner" WEBService
						' --------------------------
						' returns a xml-string
						sXML = myExport.GetCustomers2Data(sERPUID, sKey, sApp, iEntity, "", sFilter, XLedger_Export.DataOption.All)

						If Len(sXML) > 50 Then
							' Load XML returnstring into readerWCF XMLdoc object
							readerWCF.LoadFromXMLString(sXML, "/ISO:Customers/ISO:Customer2")
							If readerWCF.RecordCount > 0 Then
								bReader_HasRows = True
								bFirstRecordRead = False
								bLastRecordRead = False
							End If

							ReDim aCriterias(0)
							' Can have more than one return"record"
							Do While Reader_ReadRecord()
								' Must read the record;
								'Reader_ReadRecord()
								' if one field to return, then find value, and pass into sCriteria
								sCriteria = readerWCF.GetString(sReturnField2)
								If aCriterias(0) Is Nothing Then
									aCriterias(0) = sCriteria
								Else
									ReDim Preserve aCriterias(aCriterias.GetUpperBound(0) + 1)
									aCriterias(aCriterias.GetUpperBound(0)) = sCriteria
								End If
							Loop

							' need to reset, and activate new ReaderWCF for next WEBService;
							readerWCF = New ReaderWCF
							readerWCF.NameOfCompany = sCompanyName
							' 17.08.2020 - No need to reset mySoapClient/sKey ???
							'mySoapClient = New XLedger_Authentication.AuthenticationSoapClient
							'sKey = mySoapClient.LogonKey(sERPUID, sERPPWD, sApp)
						Else
							sCriteria = ""
						End If  '  If Len(sXML) > 50 Then

				End Select

			Else
				' only one criteria, and now WEBService2
				ReDim aCriterias(0)
				aCriterias(0) = sCriteria
			End If   'If Not EmptyString(sWebService2) Then

			Select Case sWebService
				' One mappingfile pr WEBservice (NOT pr rule)
				Case "GetCustomerTransactionsData"
					'-------------------------------------------------------------------
					' run as many times as we have criterias (from WEBService2 - if in use)
					' if no WEBService2, then we have only one criteria
					'MsgBox("Case GetCustomerTransactionsData")
					If Not aCriterias Is Nothing Then
						For lCounter = 0 To aCriterias.GetUpperBound(0)

							sCriteria = aCriterias(lCounter)

							'MsgBox("sCriteria=" & sCriteria)
							If lCounter = 0 Then
								'MsgBox("LoadDefinitions")
								' only for first criteria:
								' load definitions from xml-file for this webservice;
								' -------------------------------------------------------
								readerWCF.LoadXMLDefintions(sWebService)
								myExport = New XLedger_Export.ExportSoapClient

								Dim xlxmlHent As ChkCert = New ChkCert
								myExport.ClientCredentials.ClientCertificate.Certificate = xlxmlHent.CtrW.Certificate
							End If

							' set filter
							' ----------
							'Select Case sField
							'Case "InvoiceNo"
							' fields
							' - InvoiceNo
							' - Remaining (Amount)
							' for amounts with decimals, we need to manipulate amounts here
							If InStr(sCriteria, "/") > 0 Then
								sTmp1 = xDelim(sCriteria, "/", 1)
								sTmp2 = xDelim(sCriteria, "/", 2)
								sCriteria = Replace((CDbl(sTmp1) / CDbl(sTmp2)).ToString, ",", ".")  ' needs . as decimalsep.
							End If

							' 14.04.2021
							' If invoice no, then replace spaces, to be able to search for likes "SO 12345"
							If sField = "InvoiceNo" Then
								sCriteria = Replace(sCriteria, " ", "")
							End If

							' 29.01.2021 byttet ut SO med SO eller GL eller AR 
							'sFilter = "[trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " AND [" & sField & "] = " & Chr(34) & sCriteria & Chr(34)
							sFilter = "([trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " OR  [trsourceCode] =" & Chr(34) & "GL" & Chr(34) & " OR [trsourceCode] = " & Chr(34) & "AR" & Chr(34) & ") AND  [" & sField & "] = " & Chr(34) & sCriteria & Chr(34)
							'sFilter = "[trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " AND [" & sField & "] = " & Chr(34) & "976.50" & Chr(34)
							'MsgBox("sFilter=" & sFilter)
							'End Select

							' PROD -
							' Run the WEBService
							' ------------------
							' returns a xml-string
							sXML = myExport.GetCustomerTransactionsData(sERPUID, sKey, sApp, iEntity, "", sFilter, XLedger_Export.DataOption.Open)

							'If Not RunTime() Then
							'    ' 13.04.2021 TEST siden vi får feil ved kjøring
							'    sXML = "<CustomerTransactions>  <CustomerTransaction>    <OwnerCode>25656</OwnerCode>    <Owner>Grieg Shipbrokers KS</Owner>    <TransactionID>103607228</TransactionID>    <TransactionKey>103607228</TransactionKey>    <TrSourceID>4e6f98a1-4158-44d8-a28d-62e0a75f0473</TrSourceID>    <TrSourceKey>598</TrSourceKey>    <TrSource>GL</TrSource>    <TrNo>7</TrNo>    <TrItemNo>1</TrItemNo>    <PeriodID>0c12e25b-a133-44c9-a7dd-a7e9e22e6862</PeriodID>    <PeriodKey>5148</PeriodKey>    <Period>Des/2020</Period>   <FiscalYear>2020</FiscalYear>    <PeriodNo>12</PeriodNo>    <PostedDate>2020-12-31T00:00:00+01:00</PostedDate>    <InvoiceDate>2020-11-17T00:00:00+01:00</InvoiceDate>    <ClientKey>21776926</ClientKey>    <Client>Macedonian Enterprises Inc</Client>    <SubledgerID>00000000-0000-0000-0000-000000000000</SubledgerID>    <SubledgerKey>21777305</SubledgerKey>    <SubledgerCode>13233</SubledgerCode>    <Subledger>Macedonian Enterprises Inc</Subledger>    <LedgerType>Kundereskontro</LedgerType>    <PaymentDate>1900-01-01T00:00:00+01:00</PaymentDate>    <DueDate>2020-12-18T00:00:00+01:00</DueDate>    <OriginalDueDate>1900-01-01T00:00:00+01:00</OriginalDueDate>    <InvoiceNo>SO20204589</InvoiceNo>    <ExtIdentifier />    <ExtOrderRef />    <OurRef />    <YourReference />    <PayTermsID>3d926511-eef5-4bde-b93e-e578f5db6e38</PayTermsID>    <PayTermsKey>443075</PayTermsKey>    <PayTerms>Fakturadato</PayTerms>    <ServiceTypeKey>0</ServiceTypeKey>    <ServiceTypeCode />    <ServiceType />    <PayMethodID>00000000-0000-0000-0000-000000000000</PayMethodID>    <PayMethodKey>0</PayMethodKey>    <PayMethod />    <CollectionID>e038523e-262f-4f4f-8944-f6775098636f</CollectionID>    <CollectionKey>446031</CollectionKey>    <CollectionCode>NS1</CollectionCode>    <BillStreetAddress />    <BillZipCode />    <BillPlace />    <BillState />    <BillCountry />    <ShipStreetAddress />    <ShipZipCode />    <ShipPlace />    <ShipState />    <ShipCountry />    <LastPaymentDate>2021-03-18T00:00:00+01:00</LastPaymentDate>    <ReminderDate>1900-01-01T00:00:00+01:00</ReminderDate>    <Reminder>0.0000</Reminder>    <AccountID>cb86e167-8feb-45a7-9d7f-99aba86e8a23</AccountID>    <AccountKey>92715</AccountKey>    <AccountCode>1500</AccountCode>    <Account>Kundefordringer (1500)</Account>    <TaxRuleID>00000000-0000-0000-0000-000000000000</TaxRuleID>    <TaxRuleKey>0</TaxRuleKey>    <TaxRule />    <Text>Faktura SO20204589</Text>    <GLObject1ID>548495cc-bdba-479d-91fb-daa02d8d604a</GLObject1ID>    <GLObject1Key>41</GLObject1Key>    <GLObject1>Koststed</GLObject1>    <GLObjectValue1ID>ec990fc0-5e9d-4a42-9fc3-a59f83c9c7e4</GLObjectValue1ID>    <GLObjectValue1Key>21243850</GLObjectValue1Key>    <GLObjectValue1>40 - Gass</GLObjectValue1>    <GLObjectValue1Code>40</GLObjectValue1Code>    <GLObject2ID>00000000-0000-0000-0000-000000000000</GLObject2ID>    <GLObject2Key>0</GLObject2Key>    <GLObject2 />    <GLObjectValue2ID>00000000-0000-0000-0000-000000000000</GLObjectValue2ID>    <GLObjectValue2Key>0</GLObjectValue2Key>    <GLObjectValue2 />    <GLObjectValue2Code />    <GLObject3ID>00000000-0000-0000-0000-000000000000</GLObject3ID>    <GLObject3Key>0</GLObject3Key>    <GLObject3 />    <GLObjectValue3ID>00000000-0000-0000-0000-000000000000</GLObjectValue3ID>    <GLObjectValue3Key>0</GLObjectValue3Key>    <GLObjectValue3 />    <GLObjectValue3Code />    <GLObject4ID>00000000-0000-0000-0000-000000000000</GLObject4ID>    <GLObject4Key>0</GLObject4Key>    <GLObject4 />    <GLObjectValue4ID>00000000-0000-0000-0000-000000000000</GLObjectValue4ID>    <GLObjectValue4Key>0</GLObjectValue4Key>    <GLObjectValue4 />    <GLObjectValue4Code />    <GLObject5ID>00000000-0000-0000-0000-000000000000</GLObject5ID>    <GLObject5Key>0</GLObject5Key>    <GLObject5 />    <GLObjectValue5ID>00000000-0000-0000-0000-000000000000</GLObjectValue5ID>    <GLObjectValue5Key>0</GLObjectValue5Key>    <GLObjectValue5 />    <GLObjectValue5Code />    <XGLID>00000000-0000-0000-0000-000000000000</XGLID>    <XGLKey>0</XGLKey>    <XGL />   <BankAccount />    <BankAccountCode />    <Currency>USD</Currency>    <Remaining>0.0000</Remaining>    <Invoice>6000.0000</Invoice>    <ExchangeRate>9.0627</ExchangeRate>    <Amount>54376.2000</Amount>    <MatchID>2058</MatchID>    <Created>2021-02-22T22:57:01+01:00</Created>    <Modified>2021-03-19T11:14:29.59+01:00</Modified>    <Archive>0</Archive>  </CustomerTransaction></CustomerTransactions> "
							'End If
							' Trap Error here, by testing on start of sXML
							If Left(sXML, 6) = "Error:" Then
								Err.Raise(11251, "DAL.RunWCF_XLedger", "Message: " & vbNewLine & "sXMLString: " & sXML)
							End If
							If sXML = "Invalid User Access" Or sXML = "Ikke tilgang til rolle" Then
								Err.Raise(11251, "DAL.RunWCF_XLedger", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & " - BabelBank is not allowed to logon to XLedger")
							End If


							'
							' TESTING ONLY
							' ======================================================================================================
							' Import soap-"cheat" xml object into ReaderWCF object
							'Dim sFilename As String = ""
							'If Not RunTime() Then
							'    sFilename = "C:\Slett\StormGeo_XLedger\xmlresult.xml"
							'Else
							'    sFilename = My.Application.Info.DirectoryPath & "\WCF\xmlresult.xml"
							'End If
							''readerWCF.LoadFromFile(sFilename, "/ISO:CustomerTransactions/ISO:CustomerTransaction")
							'Dim oFs As Scripting.FileSystemObject 'Object
							'Dim oFile As Scripting.TextStream 'Object

							'oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
							'oFile = oFs.OpenTextFile(sFilename, 1)

							'If oFile.AtEndOfStream = False Then
							'    'Read first line
							'    sXML = oFile.Read(50000)
							'End If
							'oFile.Close()
							'oFile = Nothing
							'oFs = Nothing
							'======================== END TESTING ================================================
							'--------------------------------------------------------------------------------------

							' sxml looks like this:
							' <CustomerTransactions>   <CustomerTransaction>     <OwnerCode>3389</OwnerCode>     <Owner>0794 - (3+) StormGeo Ltd</Owner>     <TransactionID>15867</TransactionID>     <TrSourceID>5b1f7948-fe95-47b9-8621-4a96b9e319e1</TrSourceID>     <TrSource>SO</TrSource>     <TrNo>28</TrNo>     <TrItemNo>0</TrItemNo>     <PeriodID>dbc3ecbe-0e38-42a5-8320-0dbd30b32d3c</PeriodID>     <Period>Mar/2017</Period>     <FiscalYear>2017</FiscalYear>     <PeriodNo>3</PeriodNo>     <PostedDate>2017-03-27T00:00:00+02:00</PostedDate>     <InvoiceDate>2017-03-27T00:00:00+02:00</InvoiceDate>     <ClientID>cde5452b-dc8f-456b-9fe0-5bfa81530fe5</ClientID>     <Client>Abermed Ltd.</Client>     <SubledgerID>7c323822-81e0-4149-8fbb-3a802338587d</SubledgerID>     <SubledgerCode>30002</SubledgerCode>     <Subledger>Abermed Ltd.</Subledger>     <LedgerType>Kundereskontro</LedgerType>     <PaymentDate>1900-01-01T00:00:00+01:00</PaymentDate>     <DueDate>2017-04-26T00:00:00+02:00</DueDate>     <OriginalDueDate>2017-04-26T00:00:00+02:00</OriginalDueDate>     <InvoiceNo>6191</InvoiceNo>     <ExtIdentifier />     <ExtOrderRef />     <OurRef>Alan Binley</OurRef>     <YourReference />     <PayTermsID>dcf5ffb7-74a7-462f-9656-acdaf8f33e27</PayTermsID>     <PayTerms>Netto 30 dager</PayTerms>     <PayMethodID>2daf3b9c-0c6b-4092-b375-0b554c7d3a1c</PayMethodID>     <PayMethod>Per faktura</PayMethod>     <CollectionID>377da315-36da-4777-91b8-97062c6fe2b4</CollectionID>     <CollectionCode>SG01</CollectionCode>     <BillStreetAddress>International SOS, Forest Grove House, Foresterhill Health &amp; Research Complex</BillStreetAddress>     <BillZipCode>AB25 2ZP</BillZipCode>     <BillPlace>Aberdeen</BillPlace>     <BillState />     <BillCountry>Storbritannia  </BillCountry>     <ShipStreetAddress>International SOS Forest Grove House Foresterhill Health &amp; Research Complex Foresterhill Road</ShipStreetAddress>     <ShipZipCode>AB25 2ZP</ShipZipCode>     <ShipPlace>Aberdeen</ShipPlace>     <ShipState />     <ShipCountry>Storbritannia  </ShipCountry>     <LastPaymentDate>2017-06-27T00:00:00+02:00</LastPaymentDate>     <ReminderDate>2017-06-22T00:00:00+02:00</ReminderDate>     <Reminder>0.0000</Reminder>     <AccountID>3a731f12-83e0-43cf-8d33-bfa2e9436233</AccountID>     <AccountCode>1500</AccountCode>     <Account>1500 - Trade debtors / debtors control account</Account>     <TaxRuleID>00000000-0000-0000-0000-000000000000</TaxRuleID>     <TaxRule />     <Text />     <GLObject1ID>548495cc-bdba-479d-91fb-daa02d8d604a</GLObject1ID>     <GLObject1>Koststed</GLObject1>     <GLObjectValue1ID>7f285496-8824-4c05-8974-a48bb23a92d6</GLObjectValue1ID>     <GLObjectValue1>9006 Aviation Training</GLObjectValue1>     <GLObjectValue1Code>9006</GLObjectValue1Code>     <GLObject2ID>d0765b06-e3ae-4aa5-b8a2-e3d59af776fc</GLObject2ID>     <GLObject2 />     <GLObjectValue2ID>d0765b06-e3ae-4aa5-b8a2-e3d59af776fc</GLObjectValue2ID>     <GLObjectValue2 />     <GLObjectValue2Code />     <XGLID>d0765b06-e3ae-4aa5-b8a2-e3d59af776fc</XGLID>     <XGL />     <BankAccount>GBP DNB (DNB GBP)</BankAccount>     <BankAccountCode>DNB GBP</BankAccountCode>     <Currency>GBP</Currency>     <Remaining>0.0000</Remaining>     <Invoice>660.0000</Invoice>     <ExchangeRate>1</ExchangeRate>     <Amount>660.0000</Amount>     <MatchID>10019</MatchID>     <Created>2017-03-27T11:00:50+02:00</Created>     <Modified>2017-03-27T11:00:50+02:00</Modified>   </CustomerTransaction> </CustomerTransactions>

							' Specialformatting of amounts, dates, etc. here;
							' -----------------------------------------------


							' if more than one result from WEBService2 (inner WEBService), then we need to "add" sXML's together before
							' feeding it into readerWCF object
							'If aCriterias.GetUpperBound(0) > 0 Then
							'If lCounter < aCriterias.GetUpperBound(0) Then

							' sometimes returns only headerinfo, if no "records" are found
							If Len(sXML) < 50 Then
								sXML = ""
							End If
							' remove "startpart" of sXML
							sXML = Replace(sXML, "<CustomerTransactions>", "")
							'sXML = Replace(sXML, "<CustomerTransaction>", "")

							' remove endpart of sXML
							sXML = Replace(sXML, "</CustomerTransactions>", "")
							'sXML = Replace(sXML, "</CustomerTransaction>", "")

							'End If
							'If lCounter <> 0 And Len(sTotalXML) > 0 Then

							'End If
							sTotalXML = sTotalXML & sXML
							'Else
							'   sTotalXML = sXML
							'End If
							If lCounter = aCriterias.GetUpperBound(0) Then
								' add "startpart" to sXML
								sTotalXML = "<CustomerTransactions>" & sTotalXML

								' add "endpart" to sXML;
								sTotalXML = sTotalXML & "</CustomerTransactions>"

								' we are at the last run, all criterias are handled
								' Load XML returnstring into readerWCF XMLdoc object
								readerWCF.LoadFromXMLString(sTotalXML, "/ISO:CustomerTransactions/ISO:CustomerTransaction")

								If readerWCF.RecordCount > 0 Then
									bReader_HasRows = True
									bFirstRecordRead = False
									bLastRecordRead = False
								End If
							End If
						Next lCounter
						'MsgBox("sTotalXML=" & vbNewLine & sTotalXML)
					End If 'If Not aCriterias(0) Is Nothing Then

					' One mappingfile pr WEBservice (NOT pr rule)
				Case "GetExchangeRateData"

					If Not aCriterias Is Nothing Then
						For lCounter = 0 To aCriterias.GetUpperBound(0)

							sCriteria = aCriterias(lCounter)
							' For Exchangerates, we need two criterias, separated by /
							' Currencycode and Date
							sCriteria2 = xDelim(sCriteria, "/", 2)   ' 20190823
							' redo to 2019-08-23
							sCriteria2 = Left(sCriteria2, 4) & "-" & Mid(sCriteria2, 5, 2) & "-" & Right(sCriteria2, 2)
							sCriteria = xDelim(sCriteria, "/", 1)

							If lCounter = 0 Then
								' only for first criteria:
								' load definitions from xml-file for this webservice;
								' -------------------------------------------------------
								readerWCF.LoadXMLDefintions(sWebService)
								myExport = New XLedger_Export.ExportSoapClient

								Dim xlxmlHent As ChkCert = New ChkCert
								myExport.ClientCredentials.ClientCertificate.Certificate = xlxmlHent.CtrW.Certificate
							End If

							For i = 0 To -30 Step -1  ' 18.06.2020 - Try some days earlier if no currency rate is found for exact date
								' set filter
								' ----------
								' sCriteria is always a currency code, like USD or EUR
								' [CurrencyFromCode] = "USD" AND [CurrencyToCode] = "NOK"
								' 03.03.2020 - changed to >= for fromdate, in case we do not have a exch.rate for the exact date
								dSearchDate = DateAdd(DateInterval.Day, -1, StringToDate(Replace(sCriteria2, "-", "")))
								sCriteria2 = DateToString(dSearchDate)

								'sCriteria2 = "2020-06-01"
								' redo to 2019-08-23
								sCriteria2 = Left(sCriteria2, 4) & "-" & Mid(sCriteria2, 5, 2) & "-" & Right(sCriteria2, 2)

								sFilter = "[CurrencyFromCode] = " & Chr(34) & sCriteria & Chr(34) & " AND [CurrencyToCode] = " & Chr(34) & sBaseCurrency & Chr(34) & " AND [DateFrom] >= " & Chr(34) & sCriteria2 & Chr(34) '& " AND [DateTo] = " & Chr(34) & "2018-11-30" & Chr(34)
								'' testing sFilter = "[CurrencyFromCode] = " & Chr(34) & "USD" & Chr(34) & " AND [CurrencyToCode] = " & Chr(34) & "NOK" & Chr(34) & " AND [DateFrom] >= " & Chr(34) & "2021-08-17" & Chr(34)
								' DEMO -
								' Run the WEBService
								' ------------------
								' returns a xml-string with currencyrates
								sXML = myExport.GetExchangeRateData(sERPUID, sKey, sApp, iEntity, "", "", sFilter, XLedger_Export.DataOption.Open)
								If Left(sXML, 6) = "Error:" Then
									Err.Raise(11251, "DAL.RunWCF_XLedger", "Message: " & vbNewLine & "sXMLString: " & sXML)
								End If
								If sXML = "Invalid User Access" Then
									Err.Raise(11251, "DAL.RunWCF_Xledger", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & "BabelBank is not allowed to logon to XLedger")
								End If
								' check if we have found a currency code for the sCriteria2 date:
								If Len(sXML) > 20 Then
									Exit For
								End If
							Next i

							' TESTING ONLY
							' ======================================================================================================
							' Import soap-"cheat" xml object into ReaderWCF object
							'Dim sFilename As String = ""
							'If Not RunTime() Then
							'    sFilename = "C:\Slett\StormGeo_XLedger\xmlresult.xml"
							'Else
							'    sFilename = My.Application.Info.DirectoryPath & "\WCF\xmlresult.xml"
							'End If
							''readerWCF.LoadFromFile(sFilename, "/ISO:CustomerTransactions/ISO:CustomerTransaction")
							'Dim oFs As Scripting.FileSystemObject 'Object
							'Dim oFile As Scripting.TextStream 'Object

							'oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
							'oFile = oFs.OpenTextFile(sFilename, 1)

							'If oFile.AtEndOfStream = False Then
							'    'Read first line
							'    sXML = oFile.Read(50000)
							'End If
							'oFile.Close()
							'oFile = Nothing
							'oFs = Nothing
							'======================== END TESTING ================================================
							'--------------------------------------------------------------------------------------

							' sxml looks like this: ??????
							' <DailyExchangeRates> <DailyExchangeRate>  ......


							' sometimes returns only headerinfo, if no "records" are found
							If Len(sXML) < 50 Then
								sXML = ""
							End If
							' remove "startpart" of sXML
							sXML = Replace(sXML, "<GetExchangeRateData>", "")
							' remove endpart of sXML
							sXML = Replace(sXML, "</GetExchangeRateData>", "")
							sTotalXML = sTotalXML & sXML

							If lCounter = aCriterias.GetUpperBound(0) Then
								' add "startpart" to sXML
								sTotalXML = "<GetExchangeRateData>" & sTotalXML

								' add "endpart" to sXML;
								sTotalXML = sTotalXML & "</GetExchangeRateData>"

								' we are at the last run, all criterias are handled
								' Load XML returnstring into readerWCF XMLdoc object
								readerWCF.LoadFromXMLString(sTotalXML, "/ISO:GetExchangeRateData/ISO:ExchangeRates/ISO:ExchangeRate")

								If readerWCF.RecordCount > 0 Then
									bReader_HasRows = True
									bFirstRecordRead = False
									bLastRecordRead = False
								End If
							End If
						Next lCounter
					End If 'If Not aCriterias(0) Is Nothing Then
			End Select

		Catch ex As Exception
			Err.Raise(11151, "DAL.RunWCF_XLedger", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXML)

		End Try

	End Function
	Friend Function RunWCF_ABAX(ByVal sNameOfRule As String, ByVal sWCF As String, ByVal sCompanyName As String) As Boolean
		' For ABAX Business Central ("ny" NAV)

		' Vi bruker RestAPI (IKKE Soap)

		Dim sWebService As String = ""  ' Name of webservice to run
		Dim sField As String = ""       ' Which field to search in
		Dim sCriteria As String = ""    ' Value to search for
		Dim sCriteria2 As String = ""    ' Value to search for
		Dim sXMLString As String = ""
		Dim lCounter As Long = 0

		Dim sCustomer_No As String = ""
		Dim sInvoice_No As String = ""
		Dim sAmount As String = ""
		Dim sOrigAmount As String = ""
		Dim sMatch_Id As String = ""
		Dim sCurrency As String = ""
		Dim sName As String = ""
		Dim sAddress As String = ""
		Dim sCity As String = ""
		Dim sZip As String = ""
		Dim sDueDate As String = ""
		Dim sPostingDate As String = ""
		Dim bOpen As Boolean = True
		Dim sOriginalAmount As String = ""
		Dim dNetAmount As Double = 0
		Dim sBB_Amount As String = ""
		Dim bFillUpXMLString As Boolean = True

		Dim aReturn() As String
		Dim bContinue As Boolean = True

		Dim bDebug As Boolean = False

		Try
			' WCF-en må settes opp slik:
			' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
			' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
			' --------------------------------------------------------------
			sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
			sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
			sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
			sCriteria2 = xDelim(sWCF, ",", 4, Chr(34))       ' 1234567
			If xDelim(sWCF, ",", 5, Chr(34)) = "debug" Then
				bDebug = True
			End If

			' for amounts, we will need to divide. do this here
			If sField = "amount" Then

				' hvis bruker har brukt desimalpunktum, må vi gjøre om pga CDbl. Satser på bare Norge/Norden, slik at komma brukes i CDbl
				sCriteria = Replace(sCriteria, ".", ",")
				If sCriteria.IndexOf("/") > -1 And sCriteria.IndexOf(",") < 1 Then
					sCriteria = (CDbl(xDelim(sCriteria, "/", 1)) / CDbl(xDelim(sCriteria, "/", 2))).ToString
				Else
					' remove / 100
					sCriteria = Left(sCriteria, InStr(sCriteria, "/") - 1)
				End If
				sCriteria = sCriteria.Replace(",", ".")  ' must have decimal point
			End If

			' remove special chars which can not be part of criteria
			sCriteria = sCriteria.Replace("(", "").Replace(")", "")

			readerWCF = New ReaderWCF
			readerWCF.NameOfCompany = sCompanyName

			Select Case sWebService
				' One mappingfile pr WEBservice (NOT pr rule)
				Case "customers"   ' finnes invoices og customers
					'-------------------------------------------------------------------
					readerWCF.LoadXMLDefintions(sWebService)

					Dim bcInstance As String = "bc160"
					Dim apiPublisher As String = "pilaro"
					Dim apiGroup As String = "babel"
					Dim apiVersion As String = "v1.0"
					Dim sFilter As String = ""
					Dim sResult As String = ""
					Dim sCompanyString As String = ""
					Dim urlCompany As String = ""
					' needs conpany code for webservice
					Select Case sCompanyName
						Case "ABAX_NO"
							urlCompany = "BC_ABAX_NO"
							' test
							sCompanyString = "f95dd429-73d5-ea11-80c0-0050569e9619"
						Case "ABAX_FI"
							urlCompany = "BC_ABAX_FI"
							sCompanyString = "ef2fa500-f8f0-ea11-80c1-0050569e9619"
							sCompanyString = "ef2fa500-f8f0-ea11-80c1-0050569e9619"
						Case "ABAX_SE"
							urlCompany = "BC_ABAX_SE"
							sCompanyString = "1e6cf7fa-f8f0-ea11-80c1-0050569e9619"
						Case "ABAX Danmark AS", "ABAX Denmark AS", "ABAX_DK"
							urlCompany = "BC_ABAX_DK"
							' Testdata for Danmark
							'sCompanyString = "28a494f8-20d6-ea11-80c0-0050569e9619"
							sCompanyString = "18fcb6ba-f7f0-ea11-80c1-0050569e9619"
							' Firma Test Abax Danmark AS: 
							'sCompanyString = "fed6e512-21d6-ea11-80c0-0050569e9619"
						Case "ABAX_NL"
							urlCompany = "BC_ABAX_NL"
							sCompanyString = "ad904fb6-7d3b-eb11-80c5-0050569e9619"
						Case "ABAX_PL"
							urlCompany = "BC_ABAX_PL"
							sCompanyString = "599a25bb-f8f0-ea11-80c1-0050569e9619"

					End Select

					' 09.03.2021 -
					' For å slippe å hardcode sCompanyString, kan vi sette denne i brukertilpasset meny,
					' slik at vi kan teste med ulike koder dersom vi er usikre - slik vi er hos ABAX
					If Not EmptyString(sCompanyCode) Then
						sCompanyString = sCompanyCode
					End If
					' Test ABAX AS - med grunnoppsett
					'sCompanyString = "f95dd429-73d5-ea11-80c0-0050569e9619"
					' TODO ################## Hvor skal vi hente CompanyString fra? Bør være i et oppsett !!!
					' ABAX AS
					'sCompanyString = "4044c136-51b1-ea11-80bd-0050569e5255"

					Dim sDomain As String = "domain"


					' then load definitions from xml-file for this webservice;
					' -------------------------------------------------------
					' LAG en mappingfil for amounts servicen
					readerWCF.LoadXMLDefintions(sWebService)

					' Run the WEBService
					' ------------------
					Select Case sField
						Case "documentNo+amount"
							sFilter = "documentno eq " & sCriteria & " amount eq " & sCriteria2
						Case "amount"
							sFilter = "amount eq " & sCriteria
						Case "name2"  '??? kontonavn ???
							sFilter = "name2 eq '" & sCriteria & "'"
							If sCriteria = "" Then
								bContinue = False
							End If
						Case "documentNo"
							sFilter = "documentNo eq '" & sCriteria & "'"
						Case "name"
							sFilter = "contains(name , '" & sCriteria & "')"
						Case "address"
							sFilter = "contains(address , '" & sCriteria & "')"
						Case "customerNo"
							sFilter = "customerNo eq '" & sCriteria & "'"
					End Select

					' Vi henter ut både åpne og lukkede poster. Dette må begrenses. Hent ut for 6 mnd tilbake i tid. For alle spørringer
					' ###############################################################################################
					' I TEST - ikke bruk filter på dato
					'sFilter = sFilter & " and dueDate gt " & DateTime.Now.AddMonths(-6).ToString("yyyy-MM-dd")
					' ###############################################################################################
					sFilter = "?$filter=" & sFilter


					If bContinue Then
						'Dim url = "https://olatest.westeurope.cloudapp.azure.com:7048/" & bcInstance & " /api/" & apiPublisher & "/" & apiGroup & "/" & apiVersion & "/companies(" & sCompanyString & ")/" & sWebService & sFilter
						'"https://olatest.westeurope.cloudapp.azure.com:7048/   {bcInstance}/    api/{   apiPublisher}/{      apiGroup}/{apiVersion}                  /companies({companyId})/            {QueryAutoInvoice(true)}"
						' test mot Ola
						'url = " https://olatest.westeurope.cloudapp.azure.com:7048/bc/api/" & apiPublisher & "/" & apiGroup & "/" & apiVersion & "/companies(" & sCompanyString & ")/" & sWebService & sFilter
						'Dim url="http://bc-api-test.planetabax.com:7048/BC_ABAX_no/api/pilaro/babel/v1.0/companies(f95dd429-73d5-ea11-80c0-0050569e9619)/invoices"
						'Dim url = "http://bc-api-test.planetabax.com:7048/BC_ABAX_no/api/" & apiPublisher & "/" & apiGroup & "/" & apiVersion & "/companies(" & sCompanyString & ")/" & sWebService & sFilter
						'url = "https://bc-api.planetabax.com:7048/BC_ABAX_DK/api/v1.0"  ' for test DK
						Dim url As String = "https://bc-api.planetabax.com:7048/" & urlCompany & "/api/" & apiPublisher & "/" & apiGroup & "/" & apiVersion & "/companies(" & sCompanyString & ")/" & sWebService & sFilter
						If bDebug Then
							MsgBox("url: " & url)
						End If
						'Using client As New WebClient
						Dim client As New WebClient

						' Logg på servicen;
						'------------------
						'dim myCache As System.Net.CredentialCache = New CredentialCache
						'myCache.Add(New Uri(url), "NTLM", New NetworkCredential(sERPUID, sERPPWD)) ', sDomain))
						'Dim webClient As NtlmWebClient = New NtlmWebClient()
						'webClient.Credentials = myCache
						' 19.10.2020 - Uses basic authentication, not NTLM
						Dim webClient As New WebClient
						Dim credidentials As String = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sERPUID + ":" + sERPPWD))
						webClient.Headers.Clear()
						webClient.Headers.Add(HttpRequestHeader.Authorization, String.Format("Basic {0}", credidentials))
						'webClient.Encoding = System.Text.Encoding.UTF8

						sResult = webClient.DownloadString(New Uri(url))
						sResult = System.Text.RegularExpressions.Regex.Unescape(sResult)

						If bDebug Then
							MsgBox("sResult: " & sResult)
						End If
						'sResult = System.Text.Encoding.UTF8.GetString(sResult).tostring

						'sResult = client.DownloadString(url)
						'Response.Write(result);

						'Dim stream As System.IO.Stream = client.OpenRead(url)
						'Dim streamreader As System.IO.StreamReader = New System.IO.StreamReader(stream)
						'sResult = streamreader.ReadToEnd
						'stream.Close()
						'streamreader.Close()


						If Not RunTime() Then
							' test by importing from file
							Dim all As String = ""
							Using reader As System.IO.StreamReader = New System.IO.StreamReader("C:\Slett\ABAX\BusinessCentral\WEBService\amountsResult3.txt")
								' Read one line from file
								all = reader.ReadToEnd
							End Using
							sResult = all
						End If

						aReturn = ABAX_ParseManually(sResult)


						' Tolking av resultatstreng;
						' --------------------------
						sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
						If Not aReturn Is Nothing Then
							For lCounter = 0 To aReturn.GetUpperBound(0)

								sCustomer_No = ABAX_ExtractFieldInfo(aReturn(lCounter), "customerNo")
								sInvoice_No = ABAX_ExtractFieldInfo(aReturn(lCounter), "documentNo")
								sAmount = ABAX_ExtractFieldInfo(aReturn(lCounter), "amount").Replace(".", ",")
								sOrigAmount = ABAX_ExtractFieldInfo(aReturn(lCounter), "debitAmount").Replace(".", ",")
								sMatch_Id = ABAX_ExtractFieldInfo(aReturn(lCounter), "documentNo")
								sCurrency = ABAX_ExtractFieldInfo(aReturn(lCounter), "currencyCode")
								If EmptyString(sCurrency) Then
									Select Case sCompanyName
										Case "ABAX_NO"
											sCurrency = "NOK"
										Case "ABAX_FI"
											sCurrency = "EUR"
										Case "ABAX_SE"
											sCurrency = "SEK"
										Case "ABAX Danmark AS", "ABAX Denmark AS", "ABAX_DK"
											sCurrency = "DKK"
										Case "ABAX_NL"
											sCurrency = "EUR"
										Case "ABAX_PL"
											sCurrency = "PLN"
										Case Else
											sCurrency = "NOK"
									End Select


								End If
								sName = RemoveIllegalXMLCharacters(ABAX_ExtractFieldInfo(aReturn(lCounter), "name"))
								sAddress = RemoveIllegalXMLCharacters(ABAX_ExtractFieldInfo(aReturn(lCounter), "address"))
								sCity = ABAX_ExtractFieldInfo(aReturn(lCounter), "city")
								sZip = ABAX_ExtractFieldInfo(aReturn(lCounter), "postCode")
								sDueDate = ABAX_ExtractFieldInfo(aReturn(lCounter), "dueDate")
								' 06.07.2021 - introduced postingDate, to be able to check against payments paid before postingDate, which causes problems in BC
								sPostingDate = ABAX_ExtractFieldInfo(aReturn(lCounter), "postingDate")  ' Format 2022-02-27

								bOpen = CBool(ABAX_ExtractFieldInfo(aReturn(lCounter), "open"))


								' Specialformatting of amounts, dates, etc. here;
								' -----------------------------------------------
								bFillUpXMLString = True

								If bFillUpXMLString Then
									sXMLString = sXMLString & "<Result>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
									'sXMLString = sXMLString & "    <Opprinnelig>" & Math.Round(((CDbl(sOrigAmount)) * 100), 0).ToString & "</Opprinnelig>" & vbCrLf
									' 16.03.2021 Fjernet * 100
									sXMLString = sXMLString & "    <Opprinnelig>" & Math.Round(((CDbl(sOrigAmount))), 2).ToString & "</Opprinnelig>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
									sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Address>" & sAddress & "</BBRET_Address>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Zip>" & sZip & "</BBRET_Zip>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_City>" & sCity & "</BBRET_City>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
									' 06.07.2021 - introduced postingDate, to be able to check against payments paid before postingDate, which causes problems in BC
									sXMLString = sXMLString & "    <InvoiceDate>" & sPostingDate & "</InvoiceDate>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_MyField>" & sPostingDate & "</BBRET_MyField>" & vbCrLf
									sXMLString = sXMLString & "</Result>" & vbCrLf
								End If
							Next
						End If
						sXMLString = sXMLString & "</Returns>"

						'' Load XML returnstring into readerWCF XMLdoc object
						readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

						If readerWCF.RecordCount > 0 Then
							bReader_HasRows = True
							bFirstRecordRead = False
							bLastRecordRead = False
						End If
						WebRequest.GetSystemWebProxy()
					End If ' If bContinue

			End Select

		Catch ex As Exception
			Err.Raise(11151, "DAL.RunWCF_ABAX", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)

		End Try

	End Function
	Friend Function RunWCF_Seaborn(ByVal sNameOfRule As String, ByVal sWCF As String, ByVal sCompanyName As String) As Boolean
		' For Seaborn,  Business Central ("ny" NAV)

		' Vi bruker RestAPI (IKKE Soap)

		Dim sWebService As String = ""  ' Name of webservice to run
		Dim sInitialWebService As String = ""  ' trenger denne når vi går vegen om "bankkundenavn" søk
		Dim sField As String = ""       ' Which field to search in
		Dim sCriteria As String = ""    ' Value to search for
		Dim sCriteria2 As String = ""    ' Value to search for
		Dim sXMLString As String = ""
		Dim lCounter As Long = 0
		Dim lCustomerCounter As Long = 0
		Dim sCustomer_No As String = ""
		Dim sInvoice_No As String = ""
		Dim sAmount As String = ""
		Dim sOrigAmount As String = ""
		Dim sMatch_Id As String = ""
		Dim sCurrency As String = ""
		Dim sName As String = ""
		Dim sAddress As String = ""
		Dim sCity As String = ""
		Dim sZip As String = ""
		Dim sDueDate As String = ""
		Dim sPostingDate As String = ""
		Dim bOpen As Boolean = True
		Dim sOriginalAmount As String = ""
		Dim dNetAmount As Double = 0
		Dim sBB_Amount As String = ""
		Dim bFillUpXMLString As Boolean = True
		Dim sFilter As String = ""
		Dim sAvdeling As String = ""
		Dim dExchRate As Double = 0

		Dim aReturn() As String
		Dim aCustomerNameReturn() As String
		Dim bContinue As Boolean = True
		'Using client As New myWebClient
		Dim myWebClient As New WebClient

		Dim sResult As String = ""
		Dim url As String = ""
		Dim credentials As String = ""
		Dim bDebug As Boolean = False

		Dim sToken As String

		Try
			' WCF-en må settes opp slik:
			' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
			' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
			' --------------------------------------------------------------
			sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
			sInitialWebService = sWebService

			sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
			' Problems with Amount, and decimal comma ...
			If sField = "BBRET_Amount" Then
				If Not EmptyString(xDelim(sWCF, ",", 4)) Then
					sCriteria = xDelim(sWCF, ",", 3, Chr(34)) & "." & xDelim(sWCF, ",", 4)
				Else
					sCriteria = xDelim(sWCF, ",", 3, Chr(34)) & ".00"  ' legg til øre
				End If
			Else
				sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
			End If
			sCriteria2 = xDelim(sWCF, ",", 4, Chr(34))       ' 1234567
			If xDelim(sWCF, ",", 5, Chr(34)) = "debug" Then
				bDebug = True
			End If

			' remove special chars which can not be part of criteria
			sCriteria = sCriteria.Replace("(", "").Replace(")", "")

			readerWCF = New ReaderWCF
			readerWCF.NameOfCompany = sCompanyName

			Select Case sWebService
				Case "CurrencyExchangeRatesWeb"
					' valuta
					' Set filter
					' ------------------
					Select Case sField
						Case "Currency_Code+Starting_Date"
							sFilter = "Currency_Code eq '" & sCriteria & "' " & "Starting_Date eq '" & sCriteria2
						Case "Currency_Code"
							sFilter = "Currency_Code eq '" & sCriteria & "'"
					End Select
					url = "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/Production/ODataV4/Company('Seaborn%20AS')/" & sWebService
					sFilter = "?$filter=" & sFilter
					url = url & sFilter


					' Logg på servicen;
					'------------------
					myWebClient = New WebClient
					'credentials = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sERPUID + ":" + sERPPWD))
					myWebClient.Headers.Clear()

					sToken = Seaborn_GetAccessToken(sERPPWD)
					myWebClient.Headers("Authorization") = "Bearer " + sToken

					'myWebClient.Headers.Add(HttpRequestHeader.Authorization, String.Format("Basic {0}", credentials))

					' spesial for NOK, der er kurs = 1
					If sCriteria <> "NOK" Then
						sResult = myWebClient.DownloadString(New Uri(url))
						sResult = System.Text.RegularExpressions.Regex.Unescape(sResult)
						aReturn = Seaborn_ParseManuallyCurrency(sResult)

						readerWCF = New ReaderWCF
						readerWCF.NameOfCompany = sCompanyName
						readerWCF.LoadXMLDefintions(sWebService)

						sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
						sCurrency = Seaborn_ExtractFieldInfo(aReturn(lCounter), "Currency_Code")
						Dim sExchRate As String = Seaborn_ExtractFieldInfo(aReturn(lCounter), "Relational_Exch_Rate_Amount")
						dExchRate = CDbl(Replace(sExchRate, ".", ","))
						bFillUpXMLString = True

						If bFillUpXMLString Then
							sXMLString = sXMLString & "<Result>" & vbCrLf
							sXMLString = sXMLString & "    <BBRET_CurrencyCode>" & sCurrency & "</BBRET_CurrencyCode>" & vbCrLf
							sXMLString = sXMLString & "    <BBRET_CurrencyRate>" & dExchRate & "</BBRET_CurrencyRate>" & vbCrLf
							sXMLString = sXMLString & "</Result>" & vbCrLf
						End If
						sXMLString = sXMLString & "</Returns>"
					Else
						' NOK
						'--- noe feil her ---
						readerWCF = New ReaderWCF
						readerWCF.NameOfCompany = sCompanyName
						readerWCF.LoadXMLDefintions(sWebService)

						sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
						sXMLString = sXMLString & "<Result>" & vbCrLf
						sXMLString = sXMLString & "    <BBRET_CurrencyCode>" & "NOK" & "</BBRET_CurrencyCode>" & vbCrLf
						sXMLString = sXMLString & "    <BBRET_CurrencyRate>" & " 1.00 " & "</BBRET_CurrencyRate>" & vbCrLf
						sXMLString = sXMLString & "</Result>" & vbCrLf
						sXMLString = sXMLString & "</Returns>"
					End If

					'' Load XML returnstring into readerWCF XMLdoc object
					readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

					If readerWCF.RecordCount > 0 Then
						bReader_HasRows = True
						bFirstRecordRead = False
						bLastRecordRead = False
					End If
					WebRequest.GetSystemWebProxy()

				Case "R025ApnePostRestbelopWeb", "R025ApnePostRestbelopVendorWeb", "R025InnBetKundeNavnWeb", "POST_R025InnBetKundeNavnWeb"
					'-------------------------------------------------------------------
					'readerWCF.LoadXMLDefintions(sWebService)
					url = "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/Production/ODataV4/Company('Seaborn%20AS')/" & Replace(sWebService, "POST_", "")
					sResult = ""
					' then load definitions from xml-file for this webservice;
					' -------------------------------------------------------
					readerWCF.LoadXMLDefintions(sWebService)

					' Set filter
					' ------------------
					Select Case sField
						Case "BBRET_InvoiceNo+BBRET_Amount"
							sFilter = "BBRET_InvoiceNo eq '" & sCriteria & "' and BBRET_Amount eq " & Replace(sCriteria2 / 100, ",", ".")
						Case "BBRET_Amount"
							sFilter = "BBRET_Amount eq " & sCriteria
						Case "name2"  '??? kontonavn ???
							sFilter = "name2 eq '" & sCriteria & "'"
							If sCriteria = "" Then
								bContinue = False
							End If
						Case "BBRET_InvoiceNo"
							sFilter = "BBRET_InvoiceNo eq '" & sCriteria & "'"
						Case "Name"
							'sFilter = "contains(toupper(Name) , '" & sCriteria.ToUpper & "')"
							' linjen over vil ikke fungere, da man ikke kan ha "nestede" funksjoner. Bruker må skrive store/små slik de er lagret
							sFilter = "contains(Name , '" & sCriteria & "')"
						Case "Navn"
							' Denne er for R025InnBetKundeNavnWeb.
							' Sjekk om vi der skal ha "eq" for at vi skal treffe hele navnet
							sFilter = "Navn eq '" & sCriteria & "'"
							If sCriteria = "" Then
								bContinue = False
							End If
						Case "BBRET_CustomerNo"
							sFilter = "BBRET_CustomerNo eq '" & sCriteria & "'"
						Case "BBRET_VendorNo"
							sFilter = "BBRET_VendorNo eq '" & sCriteria & "'"
						Case "Currency_Code"
							sFilter = "Currency_Code eq '" & sCriteria & "'"
					End Select

					sFilter = "?$filter=" & sFilter
					url = url & sFilter


					If bContinue Then
						If bDebug Then
							MsgBox("url: " & url)
						End If
						' Logg på servicen;
						'------------------

						myWebClient = New WebClient
						'credentials = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sERPUID + ":" + sERPPWD))
						myWebClient.Headers.Clear()

						sToken = Seaborn_GetAccessToken(sERPPWD)

						myWebClient.Headers("Authorization") = "Bearer " + sToken
						'myWebClient.Headers.Add(HttpRequestHeader.Authorization, String.Format("Basic {0}", credentials))
						'url = "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/Production/ODataV4/Company('Seaborn%20AS')/R025ApnePostRestbelopWeb?$filter=contains(Name , 'Hansen')"

						sResult = myWebClient.DownloadString(New Uri(url))
						sResult = System.Text.RegularExpressions.Regex.Unescape(sResult)

						If bDebug Then
							MsgBox("sResult: " & sResult)
						End If

						aCustomerNameReturn = Nothing
						If sWebService = "R025InnBetKundeNavnWeb" Or sWebService = "POST_R025InnBetKundeNavnWeb" Then
							' =======================================================
							' Her må vi først kjøre søket i R025InnBetKundeNavnWeb,
							' deretter Kundenr søk i R025ApnePostRestbelopWeb
							' =======================================================
							'sInitialWebService = "R025InnBetKundeNavnWeb"
							aCustomerNameReturn = Seaborn_ParseManuallyKundeNavn(sResult)
							' Tolking av resultatstreng;
							' --------------------------
							sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
							If Not aCustomerNameReturn Is Nothing Then
								For lCounter = 0 To aCustomerNameReturn.GetUpperBound(0)
									Dim seTag As String = Seaborn_ExtractFieldInfo(aCustomerNameReturn(lCounter), "@odata.etag")
									Dim sID As String = Seaborn_ExtractFieldInfo(aCustomerNameReturn(lCounter), "id")
									sCustomer_No = Seaborn_ExtractFieldInfo(aCustomerNameReturn(lCounter), "KundeNr")
									sName = RemoveIllegalXMLCharacters(Seaborn_ExtractFieldInfo(aCustomerNameReturn(lCounter), "Navn"))
									sMatch_Id = Seaborn_ExtractFieldInfo(aCustomerNameReturn(lCounter), "Antall")

									bFillUpXMLString = True

									If bFillUpXMLString Then
										sXMLString = sXMLString & "<Result>" & vbCrLf
										sXMLString = sXMLString & "    <eTag>" & seTag & "</eTag>" & vbCrLf
										sXMLString = sXMLString & "    <Id>" & sID & "</Id>" & vbCrLf
										sXMLString = sXMLString & "    <KundeNr>" & sCustomer_No & "</KundeNr>" & vbCrLf
										sXMLString = sXMLString & "    <Navn>" & sName & "</Navn>" & vbCrLf
										sXMLString = sXMLString & "    <Antall>" & sMatch_Id & "</Antall>" & vbCrLf
										sXMLString = sXMLString & "</Result>" & vbCrLf
									End If
								Next
							End If
							sXMLString = sXMLString & "</Returns>"
							sXMLString = ""  ' blank xmlstring, da vi skal fylle opp den på ny under
						End If

						Dim iNoOfIterasions As Integer
						iNoOfIterasions = 0
						If Not aCustomerNameReturn Is Nothing Then
							iNoOfIterasions = aCustomerNameReturn.GetUpperBound(0)
						End If

						If sWebService = "CurrencyExchangeRatesWeb" Then
							' find currency

						Else
							If sWebService = "POST_R025InnBetKundeNavnWeb" Then
								' Vi kommer hit for oppdatering av kundenavn ("POST_R025InnBetKundeNavnWeb"), etter å ha søkt på kundenavn
								' Da skal vi enten kjøre en POST eller en PUT, avhengig om vi har funnet noe i søk etter kunde (i R025InnBetKundeNavnWeb)
								' Ved oppdatering kjører vi en WS for å legge inn nye, matchede, kunder, i R025InnBetKundeNavnWeb.
								' Der lagrer vi BBRET_CustomerNo og Navn.

								' Utelukk to kundenr;
								' - 41733
								' - 23999
								If sCriteria2 <> "41733" And sCriteria2 <> "23999" Then
									myWebClient = New WebClient
									'credentials = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sERPUID + ":" + sERPPWD))
									myWebClient.Headers.Clear()
									sToken = Seaborn_GetAccessToken(sERPPWD)
									myWebClient.Headers("Authorization") = "Bearer " + sToken

									'myWebClient.Headers.Add(HttpRequestHeader.Authorization, String.Format("Basic {0}", credentials))

									myWebClient.Headers.Add("Content-Type", "application/json")
									sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
									If aCustomerNameReturn Is Nothing Then
										' kjør en POST på en og en kunde, dersom den ikke ble funnet i spørringen over:
										' "POST_R025InnBetKundeNavnWeb","Navn","BB_Name","BB_CustomerNo"
										Dim body As String = "{" & Chr(34) & "KundeNr" & Chr(34) & ":" & Chr(34) & sCriteria2 & Chr(34) & "," & Chr(34) & "Navn" & Chr(34) & ":" & Chr(34) & sCriteria & Chr(34) & "}"
										url = "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/Production/ODataV4/Company('Seaborn%20AS')/R025InnBetKundeNavnWeb"
										sResult = myWebClient.UploadString(New Uri(url), "POST", body)
									Else
										' VI TRENGER IKKE Å GJØRE NOE DERSOM NAVN ER FUNNET!
										' PUT, med Id-tag
										'Dim body As String = "{" & Chr(34) & "@odata.etag" & Chr(34) & ":" & Chr(34) & xDelim(aCustomerNameReturn(0), ",", 1) & Chr(34) & "," & Chr(34) & "KundeNr" & Chr(34) & ":" & Chr(34) & sCriteria2 & Chr(34) & "," & Chr(34) & "Navn" & Chr(34) & ":" & Chr(34) & sCriteria & Chr(34) & "}"
										'url = "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/Production/ODataV4/Company('Seaborn%20AS')/R025InnBetKundeNavnWeb"
										'sResult = myWebClient.UploadString(New Uri(url), "PUT", body)

									End If 'If Not aCustomerNameReturn Is Nothing Then Then
								End If   'If sCriteria2 <> "41733" And sCriteria2 <> "23999" Then
								' --------------------------------------
								' Ferdig med POST_R025InnBetKundeNavnWeb
								' --------------------------------------
							Else

								' Vi går vanligvis gjennom kun ett søk, bortsett fra når vi først har hentet ut flere kunder
								' i søk på "banknavn", gjennom R025InnBetKundeNavnWeb (se linjene rett over, i forrige If/End If
								For lCustomerCounter = 0 To iNoOfIterasions
									If sInitialWebService = "R025InnBetKundeNavnWeb" Then
										If Not aCustomerNameReturn Is Nothing Then
											' hent ut ett og ett kundenummer fra aCustomerNameReturn
											sCustomer_No = Seaborn_ExtractFieldInfo(aCustomerNameReturn(lCustomerCounter), "KundeNr")
											sWebService = "R025ApnePostRestbelopWeb"
											sFilter = "BBRET_CustomerNo eq '" & sCustomer_No & "'"
											sFilter = "?$filter=" & sFilter
											url = "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/Production/ODataV4/Company('Seaborn%20AS')/" & sWebService
											url = url & sFilter
											' Kjør søk for ett og ett kundenr
											sResult = myWebClient.DownloadString(New Uri(url))
											sResult = System.Text.RegularExpressions.Regex.Unescape(sResult)
											' must load readerWCF with R025ApnePostRestbelopWeb
											If lCustomerCounter = 0 Then
												readerWCF = New ReaderWCF
												readerWCF.NameOfCompany = sCompanyName
												readerWCF.LoadXMLDefintions(sWebService)
											End If
										End If
									End If
									aReturn = Seaborn_ParseManually(sResult)
									' Tolking av resultatstreng;
									' --------------------------
									If EmptyString(sXMLString) Then
										sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
									End If
									If Not aReturn Is Nothing Then
										For lCounter = 0 To aReturn.GetUpperBound(0)
											sInvoice_No = Seaborn_ExtractFieldInfo(aReturn(lCounter), "BBRET_InvoiceNo")
											sCurrency = Seaborn_ExtractFieldInfo(aReturn(lCounter), "BBRET_Currency")
											If EmptyString(sCurrency) Then
												sCurrency = "NOK"
											End If
											sDueDate = Seaborn_ExtractFieldInfo(aReturn(lCounter), "Forfallsdato")
											sPostingDate = Seaborn_ExtractFieldInfo(aReturn(lCounter), "Bilagsdato")  ' Format 2022-02-27
											sAvdeling = Seaborn_ExtractFieldInfo(aReturn(lCounter), "Avdeling")
											sAmount = Seaborn_ExtractFieldInfo(aReturn(lCounter), "BBRET_Amount").Replace(".", ",")
											If sWebService = "R025ApnePostRestbelopVendorWeb" Then
												sCustomer_No = Seaborn_ExtractFieldInfo(aReturn(lCounter), "BBRET_VendorNo")
											Else
												sCustomer_No = Seaborn_ExtractFieldInfo(aReturn(lCounter), "BBRET_CustomerNo")
											End If
											sName = RemoveIllegalXMLCharacters(Seaborn_ExtractFieldInfo(aReturn(lCounter), "Name"))
											sMatch_Id = Seaborn_ExtractFieldInfo(aReturn(lCounter), "BBRET_InvoiceNo")

											' Specialformatting of amounts, dates, etc. here;
											' -----------------------------------------------
											bFillUpXMLString = True

											If bFillUpXMLString Then
												sXMLString = sXMLString & "<Result>" & vbCrLf
												sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
												sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
												sXMLString = sXMLString & "    <Forfallsdato>" & sDueDate & "</Forfallsdato>" & vbCrLf
												sXMLString = sXMLString & "    <Bilagsdato>" & sPostingDate & "</Bilagsdato>" & vbCrLf
												sXMLString = sXMLString & "    <Avdeling>" & sAvdeling & "</Avdeling>" & vbCrLf
												sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
												If sWebService = "R025ApnePostRestbelopVendorWeb" Then
													sXMLString = sXMLString & "    <BBRET_VendorNo>" & sCustomer_No & "</BBRET_VendorNo>" & vbCrLf
												Else
													sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
												End If
												sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
												sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
												sXMLString = sXMLString & "</Result>" & vbCrLf
											End If
										Next lCounter
									End If
								Next lCustomerCounter
							End If
							sXMLString = sXMLString & "</Returns>"
							'' Load XML returnstring into readerWCF XMLdoc object
							readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

							If readerWCF.RecordCount > 0 Then
								bReader_HasRows = True
								bFirstRecordRead = False
								bLastRecordRead = False
							End If
							WebRequest.GetSystemWebProxy()
						End If ' If bContinue
					End If


			End Select

			'30.08.2022 – Added WebException        
		Catch exw As WebException
			Throw New Exception("DAL.RunWCF_Seaborn - Message: " & exw.Message & vbNewLine & "sXMLString: " & sXMLString, exw)

		Catch ex As Exception
			Err.Raise(11151, "DAL.RunWCF_Seaborn", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)

		End Try

	End Function
	Friend Function Seaborn_GetAccessToken(clientSecret As String) As String

        'ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        'Dim tenantId As String = "0c911776-4883-46c8-8d55-84a4e945ea56"
        'Dim clientId As String = "64c60c1b-7f1e-4138-afcc-1bed21dd84e3"
        '' ####HENT secret fra pålogging !!!!
        ''Dim clientSecret As String = "jTP8Q~cwpl9GKUKnkpTSUTCoOTePWY8p5t5t7bVf"
        'Dim authorityUri As String = $"https://login.microsoftonline.com/{tenantId}/oauth2/token"
        'Dim redirectUri As String = "https://businesscentral.dynamics.com/"
        'Dim scopes As String() = New String() {"https://api.businesscentral.dynamics.com/.default"}
        'Dim confidentialClient = ConfidentialClientApplicationBuilder.Create(clientId).WithClientSecret(clientSecret).WithAuthority(New Uri(authorityUri)).WithRedirectUri(redirectUri).Build()
        'Dim tokenReq = confidentialClient.AcquireTokenForClient(scopes)
        'Dim tokenRes = tokenReq.ExecuteAsync().Result
        'Dim token = tokenRes.AccessToken

        'Return token

	End Function
	Friend Function RunWCF_Patentstyret(ByVal sNameOfRule As String, ByVal sWCF As String, ByVal sCompanyName As String) As Boolean
		' For Patentstyret Business Central ("ny" NAV)

		' Vi bruker RestAPI (IKKE Soap)

		Dim sWebService As String = ""  ' Name of webservice to run
		Dim sField As String = ""       ' Which field to search in
		Dim sCriteria As String = ""    ' Value to search for
		Dim sCriteria2 As String = ""    ' Value to search for
		Dim sXMLString As String = ""
		Dim lCounter As Long = 0

		Dim sCustomer_No As String = ""
		Dim sInvoice_No As String = ""
		Dim sAmount As String = ""
		Dim sOrigAmount As String = ""
		Dim sMatch_Id As String = ""
		Dim sorderId As String = ""
		Dim sCurrency As String = ""
		Dim sName As String = ""
		Dim sAddress As String = ""
		Dim sCity As String = ""
		Dim sZip As String = ""
		Dim sDueDate As String = ""
		Dim sAccountNo As String = ""
		Dim sSoknadsNr As String = ""
		Dim sMyField As String = ""
		Dim sOriginalAmount As String = ""
		Dim dNetAmount As Double = 0
		Dim sBB_Amount As String = ""
		Dim bFillUpXMLString As Boolean = True
		Dim sType As String = ""  ' B=Åpen C=Lukket

		Dim aReturn() As String
		Dim bContinue As Boolean = True

		Dim bDebug As Boolean = False

		Try

			Dim sFilter As String = ""
			Dim sResult As String = ""
			Dim urlCompany As String = "P1"

			' WCF-en må settes opp slik:
			' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
			' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
			' --------------------------------------------------------------
			sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
			sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
			sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
			sCriteria2 = xDelim(sWCF, ",", 4, Chr(34))       ' 1234567
			If xDelim(sWCF, ",", 5, Chr(34)) = "debug" Then
				bDebug = True
			End If

			'System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls 'Or System.Net.SecurityProtocolType.Tls11 Or System.Net.SecurityProtocolType.Tls12

			' for amounts, we will need to divide. do this here
			If sField = "restAmount" Or sField = "amount" Then

				'' hvis bruker har brukt desimalpunktum, må vi gjøre om pga CDbl. Satser på bare Norge/Norden, slik at komma brukes i CDbl
				'sCriteria = Replace(sCriteria, ".", ",")
				'If sCriteria.IndexOf("/") > -1 And sCriteria.IndexOf(",") < 1 Then
				'	sCriteria = (CDbl(xDelim(sCriteria, "/", 1)) / CDbl(xDelim(sCriteria, "/", 2))).ToString
				'Else
				'	' remove / 100
				'	sCriteria = Left(sCriteria, InStr(sCriteria, "/") - 1)
				'End If
				sCriteria = sCriteria.Replace(",", ".")  ' must have decimal point
			End If

			' remove special chars which can not be part of criteria
			sCriteria = sCriteria.Replace("(", "").Replace(")", "")

			readerWCF = New ReaderWCF
			readerWCF.NameOfCompany = sCompanyName

			Select Case sWebService
				' One mappingfile pr WEBservice (NOT pr rule)
				Case "evvibabelbankhistrs", "evvibabelbanks"
					'-------------------------------------------------------------------

					' then load definitions from xml-file for this webservice;
					' -------------------------------------------------------
					readerWCF.LoadXMLDefintions(sWebService)

					' Run the WEBService
					' ------------------
					Select Case sField
						Case "voucherNo"
							sFilter = "voucherNo eq " & sCriteria   ' NB Numerisk !!!!, ingen '
						Case "orderId"
							sFilter = "orderId eq '" & sCriteria & "'"
						Case "soknadsnr"
							sFilter = "soknadsnr eq '" & sCriteria & "'"
						Case "documentNo+amount"
							sFilter = "documentno eq " & sCriteria & " amount eq " & sCriteria2
						Case "restAmount"
							sFilter = "restAmount eq " & sCriteria
						Case "amount"
							sFilter = "amount eq " & sCriteria
						Case "bankAccount"
							sFilter = "bankAccount eq '" & sCriteria & "'"
							If sCriteria = "" Then
								bContinue = False
							End If
						Case "extInvRef"
							sFilter = "extInvRef eq '" & sCriteria & "'"
						Case "aparName"
							sFilter = "contains(aparName , '" & sCriteria & "')"
						Case "address"
							sFilter = "contains(address , '" & sCriteria & "')"
						Case "aparId"
							sFilter = "aparId eq '" & sCriteria & "'"
						Case "kid" '09.11.2021 - Added kid
							sFilter = "kid eq '" & sCriteria & "'"
					End Select

					' Vi henter ut både åpne og lukkede poster. Dette må begrenses. Hent ut for 6 mnd tilbake i tid. For alle spørringer
					' ###############################################################################################
					' I TEST - ikke bruk filter på dato
					'sFilter = sFilter & " and dueDate gt " & DateTime.Now.AddMonths(-6).ToString("yyyy-MM-dd")
					' ###############################################################################################
					' replace all spaces with %20
					sFilter = sFilter.Replace(" ", "%20")
					' fjern dritt som vbLf of vbCR i tilfelle det med, f.eks. ved innliming
					sFilter = sFilter.Replace(vbLf, "").Replace(vbCr, "")
					sFilter = "?filter=" & sFilter


					If bContinue Then
						'Dim sURL = "https://ubw.unit4cloud.com/no_ptn_prod_webapi/v1/objects/evvibabelbankhistrs?companyId=" & urlCompany & "&select=address%2CpaymentDate%2CorderId%2Ckid%2CextInvRef%2CdueDate%2Ccurrency%2Cclient%2CbetalersNavn%2CbetalersKontonr%2CbankAccount%2Camount%2CaparGrId%2CaparId%2CaparName%2Ctype%2Csoknadsnr%2CrestAmount%2CvoucherNo%2CvoucherType%2Cplace" & sFilter
						Dim sURL = "https://ubw.unit4cloud.com/no_ptn_prod_webapi/v1/objects/evvibabelbankhistrs?companyId=" & urlCompany & sFilter
						'sURL = "https://ubw.unit4cloud.com/no_ptn_prod_webapi/v1/objects/evvibabelbankhistrs" & sFilter
						sURL = "https://ubw.unit4cloud.com/no_ptn_prod_webapi/v1/objects/" & sWebService & sFilter

						If bDebug Then
							MsgBox("url: " & sURL)
						End If
						'sResult=[{"address":"Hoffsveien 1A","amount":-5950.0,"aparGrId":"1","aparId":"30082","aparName":"OSLO PATENTKONTOR AS","bankAccount":"78740663762","betalersKontonr":null,"betalersNavn":null,"client":"P1","currency":"NOK","dueDate":"1900-01-01T00:00:00.000","extInvRef":"32015100","kid":"1300820320151002","orderId":"300310894","paymentDate":"2020-10-06T00:00:00.000","place":"OSLO","restAmount":0.0,"soknadsnr":"20081190","type":"C","voucherNo":32015100,"voucherType":"SA"},{"address":"Postboks 488","amount":5500.0,"aparGrId":"1","aparId":"20002","aparName":"ZACCO NORWAY AS","bankAccount":"97500648876","betalersKontonr":null,"betalersNavn":null,"client":"P1","currency":"NOK","dueDate":"2020-09-10T00:00:00.000","extInvRef":"32011639","kid":"1200020320116394","orderId":"300308438","paymentDate":"2020-08-27T00:00:00.000","place":"OSLO","restAmount":0.0,"soknadsnr":"EP3277862","type":"C","voucherNo":32011639,"voucherType":"SA"},{"address":"c/o Trademack-Formir, Standard House, Level 3 Birkirkara Hill","amount":2939.0,"aparGrId":"2","aparId":"525147","aparName":"ATTORNEY ANGELICA COPPINI","bankAccount":"00000000019","betalersKontonr":null,"betalersNavn":null,"client":"P1","currency":"NOK","dueDate":"2020-12-09T12:18:44.000","extInvRef":"32017267","kid":"1251470320172677","orderId":"770011439","paymentDate":"2020-11-10T00:00:00.000","place":"ST. JULIANS","restAmount":0.0,"soknadsnr":"202014181","type":"C","voucherNo":32017267,"voucherType":"SA"},{"address":"Hoffsveien 1A","amount":5500.0,"aparGrId":"1","aparId":"30082","aparName":"OSLO PATENTKONTOR AS","bankAccount":"78740663762","betalersKontonr":null,"betalersNavn":null,"client":"P1","currency":"NOK","dueDate":"2020-10-09T00:00:00.000","extInvRef":"32013085","kid":"1300820320130854","orderId":"300309514","paymentDate":"2020-09-29T00:00:00.000","place":"OSLO","restAmount":0.0,"soknadsnr":"EP3004250","type":"C","voucherNo":32013085,"voucherType":"SA"}]

						' Logg på servicen;
						Dim webClient As New System.Net.WebClient
						Dim credentials As String = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sERPUID + ":" + sERPPWD))


						'21.06.2021 - Added UTF8 Encoding
						webClient.Encoding = System.Text.Encoding.UTF8
						webClient.Headers.Clear()
						webClient.Headers.Add(System.Net.HttpRequestHeader.Authorization, String.Format("Basic {0}", credentials))
						sResult = webClient.DownloadString(New Uri(sURL))

						If bDebug Then
							MsgBox("sResult: " & sResult)
						End If
						If sResult.Length > 5 Then
							aReturn = PatentStyret_ParseManually(sResult)

							' Tolking av resultatstreng;
							' --------------------------
							sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
							If Not aReturn Is Nothing Then
								For lCounter = 0 To aReturn.GetUpperBound(0)

									sCustomer_No = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "aparId")
									sInvoice_No = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "voucherNo")
									sAmount = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "restAmount").Replace(".", ",")
									sAmount = CDbl(sAmount) * 100.ToString
									sOrigAmount = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "amount").Replace(".", ",")
									sOrigAmount = CDbl(sOrigAmount) * 100.ToString
									sMatch_Id = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "extInvRef") & "-" & PatentStyret_ExtractFieldInfo(aReturn(lCounter), "sequenceNo")
									sCurrency = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "currency")
									sName = RemoveIllegalXMLCharacters(PatentStyret_ExtractFieldInfo(aReturn(lCounter), "aparName"))
									sAddress = RemoveIllegalXMLCharacters(PatentStyret_ExtractFieldInfo(aReturn(lCounter), "address"))
									sCity = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "place")
									sAccountNo = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "bankAccount")
									sDueDate = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "dueDate").Substring(0, 10).Replace("-", "")
									sSoknadsNr = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "soknadsnr")
									sType = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "type")
									sMyField = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "arAccount") 'Added 24.08.2021
									sorderId = PatentStyret_ExtractFieldInfo(aReturn(lCounter), "orderId") ' Added 09.11.2021
									'bOpen = CBool(IIf(sType = "B", 0, 1))


									' Specialformatting of amounts, dates, etc. here;
									' -----------------------------------------------
									bFillUpXMLString = True

									If bFillUpXMLString Then
										sXMLString = sXMLString & "<Result>" & vbCrLf
										sXMLString = sXMLString & "    <voucherNo>" & sInvoice_No & "</voucherNo>" & vbCrLf
										sXMLString = sXMLString & "    <restAmount>" & Math.Round(((CDbl(sAmount))), 2).ToString & "</restAmount>" & vbCrLf
										sXMLString = sXMLString & "    <amount>" & Math.Round(((CDbl(sOrigAmount))), 2).ToString & "</amount>" & vbCrLf
										sXMLString = sXMLString & "    <currency>" & sCurrency & "</currency>" & vbCrLf
										sXMLString = sXMLString & "    <dueDate>" & sDueDate & "</dueDate>" & vbCrLf
										sXMLString = sXMLString & "    <aparId>" & sCustomer_No & "</aparId>" & vbCrLf
										sXMLString = sXMLString & "    <aparName>" & sName & "</aparName>" & vbCrLf
										sXMLString = sXMLString & "    <address>" & sAddress & "</address>" & vbCrLf
										sXMLString = sXMLString & "    <place>" & sCity & "</place>" & vbCrLf
										sXMLString = sXMLString & "    <extInvRef>" & sMatch_Id & "</extInvRef>" & vbCrLf
										sXMLString = sXMLString & "    <soknadsnr>" & sSoknadsNr & "</soknadsnr>" & vbCrLf
										sXMLString = sXMLString & "    <bankAccount>" & sAccountNo & "</bankAccount>" & vbCrLf
										'sXMLString = sXMLString & "    <type>" & CBool(IIf(sType = "B", 1, 0)) & "</type>" & vbCrLf
										' type er sannsynligvis reservert ord, og kan ikke være navn på en tag
										sXMLString = sXMLString & "    <BBRET_Filter>" & CBool(IIf(sType = "B", 1, 0)) & "</BBRET_Filter>" & vbCrLf
										sXMLString = sXMLString & "    <BBRET_MyField>" & sMyField & "</BBRET_MyField>" & vbCrLf 'Added 24.08.2021
										sXMLString = sXMLString & "    <orderId>" & sorderId & "</orderId>" & vbCrLf ' Added 09.11.2021
										sXMLString = sXMLString & "</Result>" & vbCrLf
									End If
								Next
							End If
							sXMLString = sXMLString & "</Returns>"

							'' Load XML returnstring into readerWCF XMLdoc object
							readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")
						End If

						If readerWCF.RecordCount > 0 Then
							bReader_HasRows = True
							bFirstRecordRead = False
							bLastRecordRead = False
						End If
						WebRequest.GetSystemWebProxy()
					End If ' If bContinue

			End Select

		Catch ex As Exception
			If Not ex.InnerException Is Nothing Then
				Err.Raise(11151, "DAL.RunWCF_Patentstyret", "Message: " & ex.Message & vbNewLine & "Inner exception: " & ex.InnerException.ToString & vbNewLine & "sXMLString: " & sXMLString)
			Else
				Err.Raise(11151, "DAL.RunWCF_Patentstyret", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)
			End If

		End Try

	End Function
	Friend Function RunWCF_Naviga(ByVal sNameOfRule As String, ByVal sWCF As String, ByVal sCompanyName As String) As Boolean
		' For Naviga used by NHST


		Dim sWebService As String = ""  ' Name of webservice to run
		Dim sField As String = ""       ' Which field to search in
		Dim sCriteria As String = ""    ' Value to search for
		Dim sCriteria2 As String = ""    ' Value to search for
		Dim sXMLString As String = ""
		Dim lCounter As Long = 0

		Dim sCustomer_No As String = ""
		Dim sInvoice_No As String = ""
		Dim sAmount As String = ""
		Dim sOrigAmount As String = ""
		Dim sMatch_Id As String = ""
		Dim sCurrency As String = ""
		Dim sName As String = ""
		Dim sAddress As String = ""
		Dim sCity As String = ""
		Dim sZip As String = ""
		Dim sDueDate As String = ""
		Dim sPostingDate As String = ""
		Dim bOpen As Boolean = True
		Dim sOriginalAmount As String = ""
		Dim dNetAmount As Double = 0
		Dim sBB_Amount As String = ""
		Dim bFillUpXMLString As Boolean = True

		Dim aReturn() As String
		Dim bContinue As Boolean = True

		Dim bDebug As Boolean = False

		Try
			' WCF-en må settes opp slik:
			' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
			' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"

			' invoices,InvoiceID,BB_InvoiceIdentifier,BB_Amount
			' --------------------------------------------------------------
			sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
			sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
			sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
			sCriteria2 = xDelim(sWCF, ",", 4, Chr(34))       ' 1234567
			If xDelim(sWCF, ",", 5, Chr(34)) = "debug" Then
				bDebug = True
			End If

			' for amounts, we will need to divide. do this here
			If sField = "Amount" Then

				' hvis bruker har brukt desimalpunktum, må vi gjøre om pga CDbl. Satser på bare Norge/Norden, slik at komma brukes i CDbl
				'sCriteria = Replace(sCriteria, ".", ",")
				' Men hvis vi bruker desimalkomma, havner desimalen i sCriteria2
				If Not EmptyString(sCriteria2) Then
					sCriteria = sCriteria & "." & sCriteria2
				End If

				'If sCriteria.IndexOf("/") > -1 And sCriteria.IndexOf(",") < 1 Then
				'	sCriteria = (CDbl(xDelim(sCriteria, "/", 1)) / CDbl(xDelim(sCriteria, "/", 2))).ToString
				'Else
				'	' remove / 100
				'	sCriteria = Left(sCriteria, InStr(sCriteria, "/") - 1)
				'End If
				'sCriteria = sCriteria.Replace(",", ".")  ' must have decimal point
			End If

				' remove special chars which can not be part of criteria
				sCriteria = sCriteria.Replace("(", "").Replace(")", "")

			readerWCF = New ReaderWCF
			readerWCF.NameOfCompany = sCompanyName

			Select Case sWebService
				' One mappingfile pr WEBservice (NOT pr rule)
				Case "invoices"   ' finnes invoices 
					'-------------------------------------------------------------------
					readerWCF.LoadXMLDefintions(sWebService)

					'Dim bcInstance As String = "bc160"
					'Dim apiPublisher As String = "pilaro"
					'Dim apiGroup As String = "babel"
					'Dim apiVersion As String = "v1.0"
					Dim sFilter As String = ""
					Dim sResult As String = ""
					Dim sCompanyString As String = ""
					Dim urlCompany As String = ""
					' test
					'sCompanyString = "f95dd429-73d5-ea11-80c0-0050569e9619"

					''Authorization: Basic QkFCRUxCQU5LOkFQSSBsaW5r' 
					'https://nmgtest.navigahub.com/ElanWebPlatform/NMG/api/accounting/invoices/IN57T'


					' then load definitions from xml-file for this webservice;
					' -------------------------------------------------------
					readerWCF.LoadXMLDefintions(sWebService)

					' Run the WEBService
					' ------------------
					Select Case sField
						Case "Amount"
							sFilter = "InvoiceBalanceValue=" & sCriteria
						Case "InvoiceID"
							sFilter = "InvoiceID=" & sCriteria
						Case "Name"
							sFilter = "NameSearchString=" & sCriteria
						Case "Address"
							sFilter = "AddressSearchString=" & sCriteria
						Case "CustomerID"
							sFilter = "CustomerID=" & sCriteria
					End Select



					If bContinue Then
						Dim url As String = "https://nmgtest.navigahub.com/ElanWebPlatform/NMG/api/accounting/" & sWebService & "/search"
						If bDebug Then
							MsgBox("url: " & url)
						End If

						Dim webClient As New WebClient
						Dim credidentials As String = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sERPUID + ":" + sERPPWD))
						webClient.Headers.Clear()
						webClient.Headers("content-type") = "application/x-www-form-urlencoded"
						webClient.Headers("accept") = "*/*"

						webClient.Headers.Add(HttpRequestHeader.Authorization, String.Format("Basic {0}", credidentials))

						sResult = webClient.UploadString(url, sFilter)


						If bDebug Then
							MsgBox("sResult: " & sResult)
						End If

						aReturn = Naviga_ParseManually(sResult)


						' Tolking av resultatstreng;
						' --------------------------
						sXMLString = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & vbCrLf & "<Returns>" & vbCrLf
                        If Not aReturn Is Nothing Then
							For lCounter = 0 To aReturn.GetUpperBound(0)

								sCustomer_No = Naviga_ExtractFieldInfo(aReturn(lCounter), "CustomerNo")
								sName = Trim(RemoveIllegalXMLCharacters(Naviga_ExtractFieldInfo(aReturn(lCounter), "CustomerName")))
								sInvoice_No = Naviga_ExtractFieldInfo(aReturn(lCounter), "ID")
								sMatch_Id = Naviga_ExtractFieldInfo(aReturn(lCounter), "ExternalRef")
								sAmount = Trim(Naviga_ExtractFieldInfo(aReturn(lCounter), "Balance").Replace(".", ","))
								sOrigAmount = Trim(Naviga_ExtractFieldInfo(aReturn(lCounter), "TotalAmount").Replace(".", ","))
								sDueDate = Trim(Left(Naviga_ExtractFieldInfo(aReturn(lCounter), "DueDate"), 10))
								sCurrency = Naviga_ExtractFieldInfo(aReturn(lCounter), "CurrencyCode")
								sAddress = RemoveIllegalXMLCharacters(Naviga_ExtractFieldInfo(aReturn(lCounter), "Address"))

								'bOpen = CBool(Naviga_ExtractFieldInfo(aReturn(lCounter), "open"))
								If sAmount = 0 Then
									bOpen = False
								End If

								' Specialformatting of amounts, dates, etc. here;
								' -----------------------------------------------
								bFillUpXMLString = True

								If bFillUpXMLString Then
									sXMLString = sXMLString & "<Result>" & vbCrLf
									sXMLString = sXMLString & "    <SYS>" & "NAV" & "</SYS>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_InvoiceNo>" & sInvoice_No & "</BBRET_InvoiceNo>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Amount>" & Math.Round(((CDbl(sAmount)) * 100), 0).ToString & "</BBRET_Amount>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Currency>" & sCurrency & "</BBRET_Currency>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_CustomerNo>" & sCustomer_No & "</BBRET_CustomerNo>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Name>" & sName & "</BBRET_Name>" & vbCrLf
									sXMLString = sXMLString & "    <TITTEL>" & "" & "</TITTEL>" & vbCrLf
									sXMLString = sXMLString & "    <KPNR>" & "" & "</KPNR>" & vbCrLf
									sXMLString = sXMLString & "    <FAKTURABETALER>" & "" & "</FAKTURABETALER>" & vbCrLf
									sXMLString = sXMLString & "    <BETPNR>" & "" & "</BETPNR>" & vbCrLf
									sXMLString = sXMLString & "    <FORFALLSDATO>" & sDueDate & "</FORFALLSDATO>" & vbCrLf
									sXMLString = sXMLString & "    <P>" & "" & "</P>" & vbCrLf
									sXMLString = sXMLString & "    <STATUS>" & "" & "</STATUS>" & vbCrLf
									sXMLString = sXMLString & "    <KUNDEADRESSE>" & sAddress & "</KUNDEADRESSE>" & vbCrLf
									sXMLString = sXMLString & "    <BETALERADRESSE>" & "" & "</BETALERADRESSE>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_MatchID>" & sMatch_Id & "</BBRET_MatchID>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_MyField>" & "NAVIGA" & "</BBRET_MyField>" & vbCrLf
									sXMLString = sXMLString & "    <BBRET_Filter>" & IIf(bOpen, "1", "0") & "</BBRET_Filter>" & vbCrLf

									sXMLString = sXMLString & "</Result>" & vbCrLf
								End If
							Next
						End If
                        sXMLString = sXMLString & "</Returns>"

                        '' Load XML returnstring into readerWCF XMLdoc object
                        readerWCF.LoadFromXMLString(sXMLString, "/ISO:Returns/ISO:Result")

                        If readerWCF.RecordCount > 0 Then
                            bReader_HasRows = True
                            bFirstRecordRead = False
                            bLastRecordRead = False
                        End If
                        WebRequest.GetSystemWebProxy()
                    End If ' If bContinue

            End Select

        Catch ex As Exception
            Err.Raise(11151, "DAL.RunWCF_Naviga", "Message: " & ex.Message & vbNewLine & "sXMLString: " & sXMLString)

        End Try

    End Function
	Public Class NtlmWebClient
		Inherits WebClient
		'
		Protected Overrides Function GetWebRequest(ByVal uri As Uri) As System.Net.WebRequest

			Dim request As WebRequest = MyBase.GetWebRequest(uri)

			If (TypeOf (request) Is HttpWebRequest) Then
				Dim myWebRequest As HttpWebRequest
				myWebRequest = request
				myWebRequest.UnsafeAuthenticatedConnectionSharing = True
				myWebRequest.KeepAlive = True
			End If

			Return request
		End Function
	End Class
	Friend Function PatentStyret_ParseManually(ByVal sResult As String) As String()
		Dim aResult() As String
		Dim sRestResult As String = sResult
		Dim sRecord As String = ""
		Dim sSurround = Chr(34)   '" rundt hver tag og hvert resultat
		Dim iCounter As Integer = -1
		' En "record" ser slik ut:
		' [{"address":"CALLE CAMPOAMOR 13, ATTICO A","amount":2940.0,"aparGrId":"2","aparId":"565509","aparName":"Tron Enger","bankAccount":"00000000019","betalersKontonr":null,"betalersNavn":null,"client":"P1","currency":"NOK","dueDate":"2020-10-26T00:00:00.000","extInvRef":"32014958","kid":"1655090320149582","orderId":"770010763","paymentDate":"2020-09-28T00:00:00.000","place":"TORREVIEJA","restAmount":0.0,"soknadsnr":"202012058","type":"C","voucherNo":32014958,"voucherType":"SA"},

		' gå gjennom sTmp, finn en "record", og del opp i elementer, og legg i aResult

		' Må først sjekke om vi har noen verdier.
		If sRestResult.IndexOf("{") > 0 And sRestResult.IndexOf("address") > 1 And sRestResult.IndexOf(":") > 1 Then
			' Kutt alt før første record
			sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "address" & sSurround & ":") + 1)

			' Test på at vi har streng, etc.
			Do
				' siste forekomst?
				If sRestResult.IndexOf("{" & sSurround & "address" & sSurround & ":", 10) < 1 Then
					' ja, siste forekomst, behandle de vi har i buffer
					sRecord = sRestResult
				Else
					sRecord = Left(sRestResult, sRestResult.IndexOf("{" & sSurround & "address" & sSurround & ":", 10))
					' fjern record fra sRestResult
					sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "address" & sSurround & ":", 10) + 1)
				End If

				iCounter = iCounter + 1
				If aResult Is Nothing Then
					ReDim aResult(0)
				Else
					ReDim Preserve aResult(iCounter)
				End If
				' ta bort alle " fra resultatet
				aResult(iCounter) = sRecord.Replace(Chr(34), "")

				' siste forekomst?
				If sRestResult.IndexOf("{" & sSurround & "address" & sSurround & ":", 10) < 1 Then
					Exit Do
				End If

			Loop
		End If
		Return aResult  ' array med oppdelte records

	End Function
	Friend Function PatentStyret_ExtractFieldInfo(ByVal sRecord As String, ByVal sFieldName As String) As String
		Dim sTmp As String = ""
		Dim i As Integer = 0
		' extract info from one field at a time, from a return-"record"
		'sTmp = sRecord.Substring(sRecord.IndexOf(sFieldName & Chr(34)) + sFieldName.Length + 2).Replace(Chr(34), "") & ","
		'sTmp = sRecord.Substring(sRecord.IndexOf(sFieldName & Chr(34)) + sFieldName.Length + 3).Replace(Chr(34), "") & ","
		sTmp = sRecord.Substring(sRecord.IndexOf(sFieldName) + sFieldName.Length + 1) & ","
		' then extract the info from rest of string
		sTmp = Left(sTmp, sTmp.IndexOf(","))
		' for last record, remove end chars
		sTmp = sTmp.Replace("}", "").Replace(",", "").Replace("]", "")
		PatentStyret_ExtractFieldInfo = sTmp
	End Function
	Friend Function ABAX_ParseManually(ByVal sResult As String) As String()
        Dim aResult() As String
        Dim sRestResult As String = sResult
        Dim sRecord As String = ""
        Dim sSurround = Chr(34)   '" rundt hver tag og hvert resultat
        Dim iCounter As Integer = -1
        ' En "record" ser slik ut:
        ' {"name":"adco","address":"","postCode":"","city":"","customerNo":"K00100","documentNo":"103033","dueDate":"2022-02-27","currencyCode":"","amount":292500,"debitAmount":292500},

        ' gå gjennom sTmp, finn en "record", og del opp i elementer, og legg i aResult

        ' Må først sjekke om vi har noen verdier.
        If sRestResult.IndexOf("{" & sSurround & "name" & sSurround & ":", 10) > 1 Then
            ' Kutt alt før første record
            sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "name" & sSurround & ":") + 1)

            ' Test på at vi har streng, etc.
            'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
            Do
                ' siste forekomst?
                If sRestResult.IndexOf("{" & sSurround & "name" & sSurround & ":", 10) < 1 Then
                    ' ja, siste forekomst, behandle de vi har i buffer
                    sRecord = sRestResult
                Else
                    sRecord = Left(sRestResult, sRestResult.IndexOf("{" & sSurround & "name" & sSurround & ":", 10))
                    ' fjern record fra sRestResult
                    sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "name" & sSurround & ":", 10) + 1)
                End If

                iCounter = iCounter + 1
                If aResult Is Nothing Then
                    ReDim aResult(0)
                Else
                    ReDim Preserve aResult(iCounter)
                End If
                aResult(iCounter) = sRecord

                ' siste forekomst?
                If sRestResult.IndexOf("{" & sSurround & "name" & sSurround & ":", 10) < 1 Then
                    Exit Do
                End If

            Loop
        End If
        Return aResult  ' array med oppdelte records

    End Function
    Friend Function ABAX_ExtractFieldInfo(ByVal sRecord As String, ByVal sFieldName As String) As String
        Dim sTmp As String = ""
        ' extract info from one field at a time, from a return-"record"
        sTmp = sRecord.Substring(sRecord.IndexOf(sFieldName & Chr(34)) + sFieldName.Length + 2).Replace(Chr(34), "") & ","
        ' then extract the info from rest of string
        ABAX_ExtractFieldInfo = Left(sTmp, sTmp.IndexOf(",")).Replace("}", "").Replace(",", "").Replace("]", "")  ' Needs the replaces for last field in record
    End Function
    Friend Function Seaborn_ParseManually(ByVal sResult As String) As String()
        Dim aResult() As String
        Dim sRestResult As String = sResult
        Dim sRecord As String = ""
        Dim sSurround = Chr(34)   '" rundt hver tag og hvert resultat
        Dim iCounter As Integer = -1
		' En "record" ser slik ut:
		' {"name":"adco","address":"","postCode":"","city":"","customerNo":"K00100","documentNo":"103033","dueDate":"2022-02-27","currencyCode":"","amount":292500,"debitAmount":292500},
		'{"BBRET_InvoiceNo":"142107434","BBRET_Currency":"EUR","Forfallsdato":"2021-09-07","Bilagsdato":"2021-09-07","Avdeling":"","BBRET_Amount":-6.8,"BRRET_CustomerNo":"21158","Name":"Hanseatic Delifood GmbH"}
		' gå gjennom sTmp, finn en "record", og del opp i elementer, og legg i aResult

		' Må først sjekke om vi har noen verdier.
		If sRestResult.IndexOf("{" & sSurround & "BBRET_InvoiceNo" & sSurround & ":", 10) > 1 Then
			' Kutt alt før første record
			sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "BBRET_InvoiceNo" & sSurround & ":") + 1)

			' Test på at vi har streng, etc.
			'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
			Do
				' siste forekomst?
				If sRestResult.IndexOf("{" & sSurround & "BBRET_InvoiceNo" & sSurround & ":", 10) < 1 Then
					' ja, siste forekomst, behandle de vi har i buffer
					sRecord = sRestResult
				Else
					sRecord = Left(sRestResult, sRestResult.IndexOf("{" & sSurround & "BBRET_InvoiceNo" & sSurround & ":", 10))
					' fjern record fra sRestResult
					sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "BBRET_InvoiceNo" & sSurround & ":", 10) + 1)
				End If

				iCounter = iCounter + 1
				If aResult Is Nothing Then
					ReDim aResult(0)
				Else
					ReDim Preserve aResult(iCounter)
				End If
				aResult(iCounter) = sRecord

				' siste forekomst?
				If sRestResult.IndexOf("{" & sSurround & "BBRET_InvoiceNo" & sSurround & ":", 10) < 1 Then
					Exit Do
				End If

			Loop
		End If
		Return aResult  ' array med oppdelte records

    End Function
	Friend Function Seaborn_ParseManuallyKundeNavn(ByVal sResult As String) As String()
		' gjelder kun for søk på KundeNavnWEB (lagrede "bank" navn
		Dim aResult() As String
		Dim sRestResult As String = sResult
		Dim sRecord As String = ""
		Dim sSurround = Chr(34)   '" rundt hver tag og hvert resultat
		Dim iCounter As Integer = -1
		' En "record" ser slik ut:
		'"value": [
		'{
		'    "@odata.etag": "W/\"JzQ0O0lyZGVLeWRiVUxwanVnRHZzcUpxSlRzTXAxcFk1V0p2aldLTmYybVFjWjg9MTswMDsn\"",
		'    "id": "007840f4-2788-ec11-a507-a085fca219e3",
		'    "KundeNr": "10023",
		'    "Navn": "HANSENS RØKERI AS",
		'    "Antall": 1
		'}
		' gå gjennom sTmp, finn en "record", og del opp i elementer, og legg i aResult

		' Må først sjekke om vi har noen verdier.
		If sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":", 10) > 1 Then
			' Kutt alt før første record
			sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":") + 1)

			' Test på at vi har streng, etc.
			'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
			Do
				' siste forekomst?
				If sRestResult.IndexOf("@odata.etag", 20) < 1 Then
					' ja, siste forekomst, behandle de vi har i buffer
					sRecord = sRestResult
				Else
					sRecord = Left(sRestResult, sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":", 10))
					' fjern record fra sRestResult
					sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":", 10) + 1)
				End If

				iCounter = iCounter + 1
				If aResult Is Nothing Then
					ReDim aResult(0)
				Else
					ReDim Preserve aResult(iCounter)
				End If
				aResult(iCounter) = sRecord

				' siste forekomst?
				If sRestResult.IndexOf("@odata.etag", 20) < 1 Then
					Exit Do
				End If

			Loop
		End If
		Return aResult  ' array med oppdelte records

	End Function
	Friend Function Seaborn_ParseManuallyCurrency(ByVal sResult As String) As String()
		' gjelder kun for søk på valuta' 
		' NB!!!!!!!!!! PLUKKER KUN SISTE ELEMENT; dvs den nyeste kursen

		Dim aResult() As String
		Dim sRestResult As String = sResult
		Dim sRecord As String = ""
		Dim sSurround = Chr(34)   '" rundt hver tag og hvert resultat
		Dim iCounter As Integer = -1
		' En "record" ser slik ut:
		'{
		'  "@odata.context": "https://api.businesscentral.dynamics.com/v2.0/0c911776-4883-46c8-8d55-84a4e945ea56/SeabornDev/ODataV4/$metadata#Company('Seaborn%20AS')/CurrencyExchangeRatesWeb",
		'  "value": [
		'      {
		'          "@odata.etag": "W/\"JzQ0O1k3a2lhYXNsMG9wL3hCZ2lRRWNqRnNSbHF5c3JqT3ZEa0Z3T2I2M0phU1U9MTswMDsn\"",
		'          "Currency_Code": "AUD",
		'          "Starting_Date": "2021-01-25",
		'          "Relational_Currency_Code": "",
		'          "Exchange_Rate_Amount": 1,
		'          "Relational_Exch_Rate_Amount": 6.517,
		'          "Adjustment_Exch_Rate_Amount": 1,
		'          "Relational_Adjmt_Exch_Rate_Amt": 6.517,
		'          "Fix_Exchange_Rate_Amount": "Currency"
		'      },
		'' gå gjennom sTmp, finn en "record", og del opp i elementer, og legg i aResult

		' Må først sjekke om vi har noen verdier.
		If sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":", 10) > 1 Then
			' Kutt alt før første record
			sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":") + 1)

			' Test på at vi har streng, etc.
			'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
			Do
				' siste forekomst?
				If sRestResult.IndexOf("@odata.etag", 20) < 1 Then
					' ja, siste forekomst, behandle de vi har i buffer
					sRecord = sRestResult
					iCounter = iCounter + 1
					If aResult Is Nothing Then
						ReDim aResult(0)
					Else
						ReDim Preserve aResult(iCounter)
					End If
					aResult(iCounter) = sRecord
					Exit Do
				Else
					sRecord = Left(sRestResult, sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":", 10))
					' fjern record fra sRestResult
					sRestResult = Mid(sRestResult, sRestResult.IndexOf("{" & sSurround & "@odata.etag" & sSurround & ":", 10) + 1)
				End If

				'iCounter = iCounter + 1
				'If aResult Is Nothing Then
				'	ReDim aResult(0)
				'Else
				'	ReDim Preserve aResult(iCounter)
				'End If
				'aResult(iCounter) = sRecord



			Loop
		End If
		Return aResult  ' array med oppdelte records

	End Function
	Friend Function Seaborn_ExtractFieldInfo(ByVal sRecord As String, ByVal sFieldName As String) As String
		Dim sTmp As String = ""
		' extract info from one field at a time, from a return-"record"
		sTmp = sRecord.Substring(sRecord.IndexOf(sFieldName & Chr(34)) + sFieldName.Length + 2).Replace(Chr(34), "") & ","
		' then extract the info from rest of string
		Seaborn_ExtractFieldInfo = Left(sTmp, sTmp.IndexOf(",")).Replace("}", "").Replace(",", "").Replace("]", "")  ' Needs the replaces for last field in record
	End Function
	Friend Function Naviga_ParseManually(ByVal sResult As String) As String()
		Dim aResult() As String
		Dim sRestResult As String = sResult
		Dim sRecord As String = ""
		Dim sSurround = Chr(34)   '" rundt hver tag og hvert resultat
		Dim iCounter As Integer = -1
		Dim sCustomerNo As String = ""
		Dim sCustomerName As String = ""
		Dim sAddress As String = ""
		Dim bNoRecords As Boolean = False

		' En "record" ser slik ut:
		' {"OtherOutput"{},"OutputData":{"Customers":[{"ID":"100040","LegacyID":",","Name":"NHST MEDIA GROUP AS","Address":"Christian Krohgs Gate 16,OSLO,0186,NORWAY",
		'	"Invoices":[{"ID":"IN32T","Date":"2022-03-14T00:00:00+00:00","DueDate":"2022-04-13T00:00:00+01:00","CurrencyCode":"NOK","Amount":38457.00,"TaxAmount":9614.25,"TotalAmount":48071.25,"Balance":48071.25,"Comment":"Order: 190","ExternalRef":"000000322"}]}],"Status":"Success","Messages":[]},"Status":"Success","Messages":[],"MessageText":"","StatusText":"Success","ErrorOccurred":false,"WarningOccurred":false,"Success":true}
		' gå gjennom sTmp, finn en "record", og del opp i elementer, og legg i aResult
		' Må først "mellomlagre" div. kundeinfo:

		sRestResult = sResult
		' Må først sjekke om vi har noen verdier.
		' Plukk ut kundenr og kundenavn fra "header"
		If sRestResult.IndexOf("Customers" & sSurround & ":", 2) > 1 And InStr(sRestResult, "[{" & sSurround & "ID" & sSurround) > 0 Then
			' Kutt alt før første record
			'sRestResult = Mid(sRestResult, sRestResult.IndexOf("Customers" & sSurround & ":") + 1)
			sRestResult = Mid(sRestResult, InStr(sRestResult, "[{" & sSurround & "ID" & sSurround))
		Else
			bNoRecords = True
		End If

		If bNoRecords = False Then
			Do
				' Vi kan ha flere kunder i en result streng:
				' Test på ny kunde ved å finne LegacyID tidlig i strengen:
				If InStr(sRestResult, "LegacyID") > 10 Then

					' Plukk kundenr
					sCustomerNo = Mid(sRestResult, InStr(sRestResult, "ID") + 5)  ' kundenr + resten av stringen
					' kutt ved første komma
					sCustomerNo = Left(sCustomerNo, InStr(sCustomerNo, ",") - 2)
					' Plukk kundenavn
					sCustomerName = Mid(sRestResult, InStr(sRestResult, "Name") + 7)  ' kundenr + resten av stringen
					' kutt ved første komma
					sCustomerName = Left(sCustomerName, InStr(sCustomerName, ",") - 2)
					' Plukk adresse
					sAddress = Mid(sRestResult, InStr(sRestResult, "Address") + 10)  ' kundenr + resten av stringen
					' kutt ved første komma
					sAddress = Left(sAddress, InStr(sAddress, ",") - 1)

					' Ta bort første del, da vi kan ha Invoices: helt i starten av sRestResult
					sRestResult = Mid(sRestResult, 10)
					' så skal vi kutte første del, og sitte igjen med Invoices
					sRestResult = Mid(sRestResult, sRestResult.IndexOf("Invoices" & sSurround & ":"))

				End If

				' Her har vi en streng med null, en eller flere "Invoices"
				'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
				' spesialcase hvor en av kundene ikke har noen åpne fakturaer:
				'Invoices":[]},{"ID":"100040","LegacyID":", ","Name":"NHST MEDIA GROUP AS","Address":"Christian Krohgs 
				If InStr(Left(sRestResult, 25), "[]") > 0 Then
					' jump to next customer, no invoices for this customer
					' remove empty invoices from string
					' test if more customers:
					If InStr(sRestResult, "LegacyID") > 10 Then
						sRestResult = Mid(sRestResult, InStr(sRestResult, "ID"))
					Else
						' no more customers
						sRestResult = ""
					End If
				Else
						' one or more invoices
						Do
						' siste forekomst? har Date: som del av info for en invoice
						'If sRestResult.IndexOf("Date" & sSurround & ":", 20) < 1 Then
						If InStr(Mid(sRestResult, 20, 30), "Date") < 1 Then
							' ja, siste forekomst for denne kunde, behandle de vi har i buffer
							sRecord = sRestResult
						Else
							' plukk ut neste invoice, test på Date:, og trekk fra 14 tegn for å komme til "ID":
							'{"ID":"IN32T","Date":"2022-03-14T00:00:00+00:00","DueDate":"2022-04-13T00:00:00+01:00","CurrencyCode":"NOK","Amount":38457.00,"TaxAmount":9614.25,"TotalAmount":48071.25,"Balance":48071.25,"Comment":"Order: 190","ExternalRef":"000000322"},
							sRecord = Mid(sRestResult, InStr(sRestResult, sSurround & "Date" & sSurround & ":") - 14)
							' kutt etter neste }
							sRecord = Left(sRecord, InStr(sRecord, "}")) ''''''''''''''''''''' + 1)
							' fjern  sRecord fra sRestResult
							sRestResult = Replace(sRestResult, sRecord, "")   'Mid(sRestResult, InStr(sRestResult, sSurround & "Date" & sSurround & ":") + 10)
							'sRestResult = Replace(sRestResult, ",,", "")  ' vi ender opp med flere kommaer etterhverandre uten denne
						End If

						iCounter = iCounter + 1
						If aResult Is Nothing Then
							ReDim aResult(0)
						Else
							ReDim Preserve aResult(iCounter)
						End If
						' Må også ha med kundenr, kundenavn og adresse
						aResult(iCounter) = "CustomerNo:" & sCustomerNo & ", " & "CustomerName:" & sCustomerName & ", " & "Address:" & sAddress & ", " & sRecord

						' siste forekomst for denne kunde?
						If InStr(Left(sRestResult, 40), "Date" & sSurround & ":") < 1 Then
							Exit Do
						End If

					Loop
				End If
				' ingen flere kunder?
				If InStr(sRestResult, "Date" & sSurround & ":") < 1 Then
					Exit Do
				End If
			Loop
		End If

		'End If
		Return aResult  ' array med oppdelte records

	End Function
	Friend Function Naviga_ExtractFieldInfo(ByVal sRecord As String, ByVal sFieldName As String) As String
        Dim sTmp As String = ""
		' extract info from one field at a time, from a return-"record"
		sTmp = sRecord.Substring(sRecord.IndexOf(sFieldName) + sFieldName.Length + 1).Replace(Chr(34), "").Replace(":", "") & ","
		' then extract the info from rest of string
		Naviga_ExtractFieldInfo = Left(sTmp, sTmp.IndexOf(",")).Replace("}", "").Replace(",", "").Replace("]", "")  ' Needs the replaces for last field in record
    End Function
    Friend Function RunWCF_XLedgerDEMO(ByVal sNameOfRule As String, ByVal sWCF As String) As Boolean
        ' '' For Storm Geo - XLedger
        ' Og Grieg test
        ' '' ######################## MOT DEMODATABASE ################################

        'Dim sWebService As String = ""  ' Name of webservice to run
        'Dim sField As String = ""       ' Which field to search in
        'Dim sCriteria As String = ""    ' Value to search for
        'Dim sReturnField As String = ""   ' If we need only one value to return
        'Dim sWebService2 As String = ""  ' Name of webservice to run
        'Dim sField2 As String = ""       ' Which field to search in
        'Dim sCriteria2 As String = ""    ' Value to search for
        'Dim sReturnField2 As String = "" ' If we need only one value to return
        'Dim sXMLString As String = ""
        'Dim lCounter As Long = 0
        'Dim sTmp1 As String = ""
        'Dim sTmp2 As String = ""
        'Dim aCriterias() As String      ' returned values from "inner" WEBService

        'Dim sApp As String = "XLEDGERDEMO"
        'Dim iEntity As Integer = 3252  ' find a variable for this, like ClientNo or so ???????
        'Dim myExport As XLedgerDemo_Export.ExportSoapClient
        'Dim sXML As String = ""
        'Dim sTotalXML As String = ""
        'Dim sFilter As String = ""
        'Dim sBaseCurrency As String = ""

        '' 17.08.2020 - changed next two to static, so we don't need to logon for each query
        'Static sKey As String = ""
        'Static mySoapClient As XLedgerDemo_Authentication.AuthenticationSoapClient

        'Try

        '    iEntity = Val(sERPClientNo)        ' pass from outside, use clientno
        '    If iEntity = 0 Then
        '        ' 07.12.2020 - add an extra field for EntityCode - to use in testing
        '        iEntity = Val(xDelim(sWCF, ",", 5, Chr(34)))
        '        If iEntity = 0 Then
        '            iEntity = 3252   ' Storm Geo AS
        '            'iEntity = 3389  ' Storm Geo Ltd
        '            iEntity = 25629  ' Grieg Logistics
        '        End If
        '    End If
        '    'MsgBox("iEntity=" & iEntity)

        '    ' Regnskap føres i NOK for StormGeo AS, i GBP for StormGeo Ltd.
        '    If iEntity = 3389 Then
        '        sBaseCurrency = "GBP"
        '    Else
        '        sBaseCurrency = "NOK"
        '    End If

        '    ' WCF-en må settes opp slik:
        '    ' BBGet_InvoiceItems,   "Document_No",   "BB_InvoiceIdentifier"
        '    ' "Navn på webservice", "Felt å søke i", "Kriterie å søke etter"
        '    ' --------------------------------------------------------------
        '    sWebService = xDelim(sWCF, ",", 1, Chr(34))     ' BBGet_InvoiceItems
        '    sField = xDelim(sWCF, ",", 2, Chr(34))          ' Document_No
        '    sCriteria = xDelim(sWCF, ",", 3, Chr(34))       ' 1234567
        '    sReturnField = xDelim(sWCF, ",", 4, Chr(34))       ' 

        '    ' We can have "nested" WEBServices, meaning that we must first consume one WEBService first, to return a value, 
        '    ' and use this value in the "outer" WEBService, like
        '    '         "GetCustomerTransactionsData","SubLedgerID", ("GetCustomers2","Description","LIKE BB_MyText*","SubLedgerID")
        '    ' First, consume GetCustomers2, using Description as filter, and return SubLedgerID
        '    ' Then use value of SubLedgerID as filter in GetCustomerTransactionsData
        '    If InStr(sCriteria, "(") > 0 Then
        '        ' yes, we have an "inner" WEBService to consume first
        '        ' we need to find sCriteria properly
        '        sCriteria = Mid(sWCF, InStr(sWCF, "("))  '"(GetCustomers2","Description","LIKE BB_MyText*","SubLedgerID")
        '        sCriteria = Replace(Replace(sCriteria, ")", ""), "(", "")  ' remove ()
        '        sWebService2 = xDelim(sCriteria, ",", 1, Chr(34))     ' BBGet_InvoiceItems
        '        sField2 = xDelim(sCriteria, ",", 2, Chr(34))          ' Document_No
        '        sCriteria2 = xDelim(sCriteria, ",", 3, Chr(34))       ' 1234567
        '        sReturnField2 = xDelim(sCriteria, ",", 4, Chr(34))       ' SubLedgerID
        '    End If
        '    readerWCF = New ReaderWCF
        '    readerWCF.NameOfCompany = sCompanyName

        '    'Dim mySoapClient As XLedgerDemo_Authentication.AuthenticationSoapClient
        '    'Dim sKey As String = ""
        '    'Dim sUID As String = sUID   '"jan-petter@visualbanking.net"
        '    'Dim sPWD As String = sPWD   '"skeid2002"
        '    '' TESTING ONLY:
        '    'sUID = "jan-petter@visualbanking.net"
        '    'sPWD = "skeid2002"

        '    'MsgBox("Før authentication")
        '    '            MsgBox("sERPUID=" & sERPUID & vbNewLine & "sERPPWD=" & sERPPWD)
        '    ' 17.08.2020 - try to reuse key/mySoapClient
        '    If EmptyString(sKey) Then
        '        mySoapClient = New XLedgerDemo_Authentication.AuthenticationSoapClient
        '        sKey = mySoapClient.LogonKey(sERPUID, sERPPWD, sApp)
        '    End If
        '    'MsgBox("sKey=" & sKey)

        '    If Not EmptyString(sWebService2) Then

        '        Select Case sWebService2
        '            ' One mappingfile pr WEBservice (NOT pr rule)
        '            Case "GetCustomers2"
        '                '---------------

        '                ' load definitions from xml-file for this webservice;
        '                ' -------------------------------------------------------
        '                readerWCF.LoadXMLDefintions(sWebService2)
        '                myExport = New XLedgerDemo_Export.ExportSoapClient

        '                Dim xlxmlHent As ChkCert = New ChkCert
        '                myExport.ClientCredentials.ClientCertificate.Certificate = xlxmlHent.CtrW.Certificate

        '                ' set filter
        '                ' ----------

        '                ' for amounts with decimals, we need to manipulate amounts here
        '                If InStr(sCriteria, "-/-") > 0 Then
        '                    sTmp1 = xDelim(sCriteria, "/", 1)
        '                    sTmp2 = xDelim(sCriteria, "/", 2)
        '                    sCriteria = Replace((CDbl(sTmp1) / CDbl(sTmp2)).ToString, ",", ".")  ' needs . as decimalsep.
        '                End If
        '                If InStr(sCriteria, "LIKE") > 0 Then
        '                    sFilter = "[" & sField2 & "] LIKE " & Chr(34) & Replace(sCriteria2, "LIKE", "").Trim & Chr(34)
        '                Else
        '                    sFilter = "[" & sField2 & "] = " & Chr(34) & sCriteria2 & Chr(34)
        '                End If

        '                ' Run the "inner" WEBService
        '                ' --------------------------
        '                ' returns a xml-string
        '                sXML = myExport.GetCustomers2Data(sERPUID, sKey, sApp, iEntity, "", sFilter, XLedger_Export.DataOption.All)

        '                If Left(sXML, 6) = "Error:" Then
        '                    Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML)
        '                End If
        '                If sXML = "Invalid User Access" Then
        '                    Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & " - BabelBank is not allowed to logon to XLedger")
        '                End If
        '                If sXML = "Ikke tilgang til webmetode" Then
        '                    Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & " - BabelBank is not allowed to use the webservice GetCustomers2 in XLedger")
        '                End If

        '                If Len(sXML) > 50 Then
        '                    ' Load XML returnstring into readerWCF XMLdoc object
        '                    readerWCF.LoadFromXMLString(sXML, "/ISO:Customers/ISO:Customer2")
        '                    If readerWCF.RecordCount > 0 Then
        '                        bReader_HasRows = True
        '                        bFirstRecordRead = False
        '                        bLastRecordRead = False
        '                    End If

        '                    ReDim aCriterias(0)
        '                    ' Can have more than one return"record"
        '                    Do While Reader_ReadRecord()
        '                        ' Must read the record;
        '                        'Reader_ReadRecord()
        '                        ' if one field to return, then find value, and pass into sCriteria
        '                        'sReturnField2 = "Key"
        '                        sCriteria = readerWCF.GetString(sReturnField2)
        '                        If aCriterias(0) Is Nothing Then
        '                            aCriterias(0) = sCriteria
        '                        Else
        '                            ReDim Preserve aCriterias(aCriterias.GetUpperBound(0) + 1)
        '                            aCriterias(aCriterias.GetUpperBound(0)) = sCriteria
        '                        End If
        '                    Loop

        '                    ' need to reset, and activate new ReaderWCF for next WEBService;
        '                    readerWCF = New ReaderWCF
        '                    readerWCF.NameOfCompany = sCompanyName
        '                    ' 17.08.2020 - No need to reset mySoapClient/sKey ???
        '                    'mySoapClient = New XLedgerDemo_Authentication.AuthenticationSoapClient
        '                    'sKey = mySoapClient.LogonKey(sERPUID, sERPPWD, sApp)
        '                Else
        '                    sCriteria = ""
        '                End If  '  If Len(sXML) > 50 Then

        '        End Select

        '    Else
        '        ' only one criteria, and now WEBService2
        '        ReDim aCriterias(0)
        '        aCriterias(0) = sCriteria
        '    End If   'If Not EmptyString(sWebService2) Then

        '    Select Case sWebService
        '        ' One mappingfile pr WEBservice (NOT pr rule)
        '        Case "GetCustomerTransactionsData"
        '            '-------------------------------------------------------------------
        '            ' run as many times as we have criterias (from WEBService2 - if in use)
        '            ' if no WEBService2, then we have only one criteria
        '            'MsgBox("Case GetCustomerTransactionsData")
        '            If Not aCriterias Is Nothing Then
        '                For lCounter = 0 To aCriterias.GetUpperBound(0)

        '                    sCriteria = aCriterias(lCounter)

        '                    'MsgBox("sCriteria=" & sCriteria)
        '                    If lCounter = 0 Then
        '                        'MsgBox("LoadDefinitions")
        '                        ' only for first criteria:
        '                        ' load definitions from xml-file for this webservice;
        '                        ' -------------------------------------------------------
        '                        readerWCF.LoadXMLDefintions(sWebService)
        '                        myExport = New XLedgerDemo_Export.ExportSoapClient

        '                        Dim xlxmlHent As ChkCert = New ChkCert
        '                        myExport.ClientCredentials.ClientCertificate.Certificate = xlxmlHent.CtrW.Certificate
        '                    End If

        '                    ' set filter
        '                    ' ----------
        '                    'Select Case sField
        '                    'Case "InvoiceNo"
        '                    ' fields
        '                    ' - InvoiceNo
        '                    ' - Remaining (Amount)
        '                    ' for amounts with decimals, we need to manipulate amounts here
        '                    If InStr(sCriteria, "/") > 0 Then
        '                        sTmp1 = xDelim(sCriteria, "/", 1)
        '                        sTmp2 = xDelim(sCriteria, "/", 2)
        '                        sCriteria = Replace((CDbl(sTmp1) / CDbl(sTmp2)).ToString, ",", ".")  ' needs . as decimalsep.
        '                    End If
        '                    ' Spesial med trsourceCode = "AR" for de som er lagt inn i demo for Grieg
        '                    'sFilter = "[trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " AND [" & sField & "] = " & Chr(34) & sCriteria & Chr(34)
        '                    sFilter = "([trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " OR [trsourceCode] = " & Chr(34) & "AR" & Chr(34) & ") AND [" & sField & "] = " & Chr(34) & sCriteria & Chr(34)
        '                    'sFilter = "[trsourceCode] = " & Chr(34) & "AR" & Chr(34) & " AND [" & sField & "] = " & Chr(34) & sCriteria & Chr(34)
        '                    'sField = "SubledgerKey"
        '                    'sFilter = "[trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " AND [" & sField & "] = " & Chr(34) & sCriteria & Chr(34)
        '                    'sFilter = "[trsourceCode] = " & Chr(34) & "SO" & Chr(34) & " AND [" & sField & "] = " & Chr(34) & "976.50" & Chr(34)
        '                    'MsgBox("sFilter=" & sFilter)
        '                    'End Select

        '                    ' PROD -
        '                    ' Run the WEBService
        '                    ' ------------------
        '                    ' returns a xml-string
        '                    sXML = myExport.GetCustomerTransactionsData(sERPUID, sKey, sApp, iEntity, "", sFilter, XLedger_Export.DataOption.Open)
        '                    ' Trap Error here, by testing on start of sXML
        '                    If Left(sXML, 6) = "Error:" Then
        '                        Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML)
        '                    End If
        '                    If sXML = "Invalid User Access" Then
        '                        Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & " - BabelBank is not allowed to logon to XLedger")
        '                    End If
        '                    If sXML = "Ikke tilgang til webmetode" Then
        '                        Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & " - BabelBank is not allowed to use the webservice GetCustomerTransactionsData in XLedger")
        '                    End If

        '                    ' sxml looks like this:
        '                    ' <CustomerTransactions>   <CustomerTransaction>     <OwnerCode>3389</OwnerCode>     <Owner>0794 - (3+) StormGeo Ltd</Owner>     <TransactionID>15867</TransactionID>     <TrSourceID>5b1f7948-fe95-47b9-8621-4a96b9e319e1</TrSourceID>     <TrSource>SO</TrSource>     <TrNo>28</TrNo>     <TrItemNo>0</TrItemNo>     <PeriodID>dbc3ecbe-0e38-42a5-8320-0dbd30b32d3c</PeriodID>     <Period>Mar/2017</Period>     <FiscalYear>2017</FiscalYear>     <PeriodNo>3</PeriodNo>     <PostedDate>2017-03-27T00:00:00+02:00</PostedDate>     <InvoiceDate>2017-03-27T00:00:00+02:00</InvoiceDate>     <ClientID>cde5452b-dc8f-456b-9fe0-5bfa81530fe5</ClientID>     <Client>Abermed Ltd.</Client>     <SubledgerID>7c323822-81e0-4149-8fbb-3a802338587d</SubledgerID>     <SubledgerCode>30002</SubledgerCode>     <Subledger>Abermed Ltd.</Subledger>     <LedgerType>Kundereskontro</LedgerType>     <PaymentDate>1900-01-01T00:00:00+01:00</PaymentDate>     <DueDate>2017-04-26T00:00:00+02:00</DueDate>     <OriginalDueDate>2017-04-26T00:00:00+02:00</OriginalDueDate>     <InvoiceNo>6191</InvoiceNo>     <ExtIdentifier />     <ExtOrderRef />     <OurRef>Alan Binley</OurRef>     <YourReference />     <PayTermsID>dcf5ffb7-74a7-462f-9656-acdaf8f33e27</PayTermsID>     <PayTerms>Netto 30 dager</PayTerms>     <PayMethodID>2daf3b9c-0c6b-4092-b375-0b554c7d3a1c</PayMethodID>     <PayMethod>Per faktura</PayMethod>     <CollectionID>377da315-36da-4777-91b8-97062c6fe2b4</CollectionID>     <CollectionCode>SG01</CollectionCode>     <BillStreetAddress>International SOS, Forest Grove House, Foresterhill Health &amp; Research Complex</BillStreetAddress>     <BillZipCode>AB25 2ZP</BillZipCode>     <BillPlace>Aberdeen</BillPlace>     <BillState />     <BillCountry>Storbritannia  </BillCountry>     <ShipStreetAddress>International SOS Forest Grove House Foresterhill Health &amp; Research Complex Foresterhill Road</ShipStreetAddress>     <ShipZipCode>AB25 2ZP</ShipZipCode>     <ShipPlace>Aberdeen</ShipPlace>     <ShipState />     <ShipCountry>Storbritannia  </ShipCountry>     <LastPaymentDate>2017-06-27T00:00:00+02:00</LastPaymentDate>     <ReminderDate>2017-06-22T00:00:00+02:00</ReminderDate>     <Reminder>0.0000</Reminder>     <AccountID>3a731f12-83e0-43cf-8d33-bfa2e9436233</AccountID>     <AccountCode>1500</AccountCode>     <Account>1500 - Trade debtors / debtors control account</Account>     <TaxRuleID>00000000-0000-0000-0000-000000000000</TaxRuleID>     <TaxRule />     <Text />     <GLObject1ID>548495cc-bdba-479d-91fb-daa02d8d604a</GLObject1ID>     <GLObject1>Koststed</GLObject1>     <GLObjectValue1ID>7f285496-8824-4c05-8974-a48bb23a92d6</GLObjectValue1ID>     <GLObjectValue1>9006 Aviation Training</GLObjectValue1>     <GLObjectValue1Code>9006</GLObjectValue1Code>     <GLObject2ID>d0765b06-e3ae-4aa5-b8a2-e3d59af776fc</GLObject2ID>     <GLObject2 />     <GLObjectValue2ID>d0765b06-e3ae-4aa5-b8a2-e3d59af776fc</GLObjectValue2ID>     <GLObjectValue2 />     <GLObjectValue2Code />     <XGLID>d0765b06-e3ae-4aa5-b8a2-e3d59af776fc</XGLID>     <XGL />     <BankAccount>GBP DNB (DNB GBP)</BankAccount>     <BankAccountCode>DNB GBP</BankAccountCode>     <Currency>GBP</Currency>     <Remaining>0.0000</Remaining>     <Invoice>660.0000</Invoice>     <ExchangeRate>1</ExchangeRate>     <Amount>660.0000</Amount>     <MatchID>10019</MatchID>     <Created>2017-03-27T11:00:50+02:00</Created>     <Modified>2017-03-27T11:00:50+02:00</Modified>   </CustomerTransaction> </CustomerTransactions>


        '                    ' sometimes returns only headerinfo, if no "records" are found
        '                    If Len(sXML) < 50 Then
        '                        sXML = ""
        '                    End If
        '                    ' remove "startpart" of sXML
        '                    sXML = Replace(sXML, "<CustomerTransactions>", "")
        '                    ' remove endpart of sXML
        '                    sXML = Replace(sXML, "</CustomerTransactions>", "")
        '                    sTotalXML = sTotalXML & sXML

        '                    If lCounter = aCriterias.GetUpperBound(0) Then
        '                        ' add "startpart" to sXML
        '                        sTotalXML = "<CustomerTransactions>" & sTotalXML

        '                        ' add "endpart" to sXML;
        '                        sTotalXML = sTotalXML & "</CustomerTransactions>"

        '                        ' we are at the last run, all criterias are handled
        '                        ' Load XML returnstring into readerWCF XMLdoc object
        '                        readerWCF.LoadFromXMLString(sTotalXML, "/ISO:CustomerTransactions/ISO:CustomerTransaction")

        '                        If readerWCF.RecordCount > 0 Then
        '                            bReader_HasRows = True
        '                            bFirstRecordRead = False
        '                            bLastRecordRead = False
        '                        End If
        '                    End If
        '                Next lCounter
        '            End If 'If Not aCriterias(0) Is Nothing Then

        '            ' One mappingfile pr WEBservice (NOT pr rule)
        '        Case "GetExchangeRateData"

        '            If Not aCriterias Is Nothing Then
        '                For lCounter = 0 To aCriterias.GetUpperBound(0)

        '                    sCriteria = aCriterias(lCounter)
        '                    ' For Exchangerates, we need two criterias, separated by /
        '                    ' Currencycode and Date
        '                    sCriteria2 = xDelim(sCriteria, "/", 2)   ' 20190823
        '                    sCriteria = xDelim(sCriteria, "/", 1)

        '                    If lCounter = 0 Then
        '                        ' only for first criteria:
        '                        ' load definitions from xml-file for this webservice;
        '                        ' -------------------------------------------------------
        '                        readerWCF.LoadXMLDefintions(sWebService)
        '                        myExport = New XLedgerDemo_Export.ExportSoapClient

        '                        Dim xlxmlHent As ChkCert = New ChkCert
        '                        myExport.ClientCredentials.ClientCertificate.Certificate = xlxmlHent.CtrW.Certificate
        '                    End If

        '                    Dim dSearchDate As Date
        '                    Dim i As Integer

        '                    For i = 0 To -30 Step -1  ' 18.06.2020 - Try some days earlier if no currency rate is found for exact date
        '                        ' set filter
        '                        ' ----------
        '                        ' sCriteria is always a currency code, like USD or EUR
        '                        ' [CurrencyFromCode] = "USD" AND [CurrencyToCode] = "NOK"
        '                        ' 03.03.2020 - changed to >= for fromdate, in case we do not have a exch.rate for the exact date
        '                        dSearchDate = DateAdd(DateInterval.Day, -1, StringToDate(Replace(sCriteria2, "-", "")))
        '                        sCriteria2 = DateToString(dSearchDate)

        '                        'sCriteria2 = "2020-06-01"
        '                        ' redo to 2019-08-23
        '                        sCriteria2 = Left(sCriteria2, 4) & "-" & Mid(sCriteria2, 5, 2) & "-" & Right(sCriteria2, 2)

        '                        sFilter = "[CurrencyFromCode] = " & Chr(34) & sCriteria & Chr(34) & " AND [CurrencyToCode] = " & Chr(34) & sBaseCurrency & Chr(34) & " AND [DateFrom] >= " & Chr(34) & sCriteria2 & Chr(34) '& " AND [DateTo] = " & Chr(34) & "2018-11-30" & Chr(34)
        '                        ' DEMO -
        '                        ' Run the WEBService
        '                        ' ------------------
        '                        ' returns a xml-string with currencyrates
        '                        sXML = myExport.GetExchangeRateData(sERPUID, sKey, sApp, iEntity, "", "", sFilter, XLedger_Export.DataOption.Open)
        '                        If Left(sXML, 6) = "Error:" Then
        '                            Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML)
        '                        End If
        '                        If sXML = "Invalid User Access" Then
        '                            Err.Raise(11251, "DAL.RunWCF_StormGeo", "Message: " & vbNewLine & "sXMLString: " & sXML & vbNewLine & "BabelBank is not allowed to logon to XLedger")
        '                        End If
        '                        ' check if we have found a currency code for the sCriteria2 date:
        '                        If Len(sXML) > 20 Then
        '                            Exit For
        '                        End If
        '                    Next i
        '                    ' TESTING ONLY
        '                    ' ======================================================================================================
        '                    ' Import soap-"cheat" xml object into ReaderWCF object
        '                    'Dim sFilename As String = ""
        '                    'If Not RunTime() Then
        '                    '    sFilename = "C:\Slett\StormGeo_XLedger\xmlresult.xml"
        '                    'Else
        '                    '    sFilename = My.Application.Info.DirectoryPath & "\WCF\xmlresult.xml"
        '                    'End If
        '                    ''readerWCF.LoadFromFile(sFilename, "/ISO:CustomerTransactions/ISO:CustomerTransaction")
        '                    'Dim oFs As Scripting.FileSystemObject 'Object
        '                    'Dim oFile As Scripting.TextStream 'Object

        '                    'oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
        '                    'oFile = oFs.OpenTextFile(sFilename, 1)

        '                    'If oFile.AtEndOfStream = False Then
        '                    '    'Read first line
        '                    '    sXML = oFile.Read(50000)
        '                    'End If
        '                    'oFile.Close()
        '                    'oFile = Nothing
        '                    'oFs = Nothing
        '                    '======================== END TESTING ================================================
        '                    '--------------------------------------------------------------------------------------

        '                    ' sxml looks like this: ??????
        '                    ' <DailyExchangeRates> <DailyExchangeRate>  ......


        '                    ' sometimes returns only headerinfo, if no "records" are found
        '                    If Len(sXML) < 50 Then
        '                        sXML = ""
        '                    End If
        '                    ' remove "startpart" of sXML
        '                    sXML = Replace(sXML, "<GetExchangeRateData>", "")
        '                    ' remove endpart of sXML
        '                    sXML = Replace(sXML, "</GetExchangeRateData>", "")
        '                    sTotalXML = sTotalXML & sXML

        '                    If lCounter = aCriterias.GetUpperBound(0) Then
        '                        ' add "startpart" to sXML
        '                        sTotalXML = "<GetExchangeRateData>" & sTotalXML

        '                        ' add "endpart" to sXML;
        '                        sTotalXML = sTotalXML & "</GetExchangeRateData>"

        '                        ' we are at the last run, all criterias are handled
        '                        ' Load XML returnstring into readerWCF XMLdoc object
        '                        readerWCF.LoadFromXMLString(sTotalXML, "/ISO:GetExchangeRateData/ISO:ExchangeRates/ISO:ExchangeRate")

        '                        If readerWCF.RecordCount > 0 Then
        '                            bReader_HasRows = True
        '                            bFirstRecordRead = False
        '                            bLastRecordRead = False
        '                        End If
        '                    End If
        '                Next lCounter
        '            End If 'If Not aCriterias(0) Is Nothing Then
        '    End Select

        'Catch ex As Exception
        '    Err.Raise(11151, "DAL.RunWCF_StormGeoDEMO", ex.Message & vbNewLine & "sXMLString: " & sXMLString)

        'End Try

    End Function
    Friend Function RemoveIllegalXMLCharacters(ByVal sInString As String) As String
        ' remove &
        If Not sInString Is Nothing Then
            If sInString.Length > 0 Then
                RemoveIllegalXMLCharacters = sInString.Replace("&", "")
            Else
                RemoveIllegalXMLCharacters = ""
            End If
        End If

    End Function
End Class

'===============================================================================================================
Public Class ReaderWCF
    Dim aFields As String(,)
    Dim XMLDoc As System.Xml.XmlDocument
    Dim nodCurrent As System.Xml.XmlNode  ' which one is the current node
    Dim NodeList As System.Xml.XmlNodeList  ' all Return nodes
    Dim iFieldCount As Integer = -1 ' no of fields in returnobject
    Dim iElementNo As Long = -1 ' which one is the current element in the xmldoc object?
    Dim iNoOfElements As Long = -1  ' no of "records" in returnobject
    Dim sCompanyName As String = ""
    Dim sCompanyCode As String = ""  ' added 09.03.2021

    Public Function LoadFromFile(ByVal sFileName As String, ByVal xPath As String) As Boolean
        ' load a Soap XML object from a file - to be used for testing

        ' status: råskrevet
        ' testet: nix

        Dim nsMgr As System.Xml.XmlNamespaceManager

        XMLDoc = New System.Xml.XmlDocument
        XMLDoc.Load(sFileName)

        nsMgr = New System.Xml.XmlNamespaceManager(XMLDoc.NameTable)
        nsMgr.AddNamespace("ISO", XMLDoc.DocumentElement.NamespaceURI)


        NodeList = XMLDoc.SelectNodes(xPath, nsMgr) ' returns a list of all Return objects
        ' like
        'NodeList = XMLDoc.SelectNodes("/ISO:Returns/ISO:Result", nsMgr) ' returns a list of all Return objects

        ' find max no of Return elements in Returns
        iNoOfElements = NodeList.Count

    End Function
    Public Function LoadFromXMLString(ByVal sXML As String, ByVal xPath As String) As Boolean
        ' load a Soap XML object from a string (xml formatted string)


        Dim nsMgr As System.Xml.XmlNamespaceManager

        Try
            XMLDoc = New System.Xml.XmlDocument
            XMLDoc.LoadXml(sXML)

            nsMgr = New System.Xml.XmlNamespaceManager(XMLDoc.NameTable)
            nsMgr.AddNamespace("ISO", XMLDoc.DocumentElement.NamespaceURI)

            NodeList = XMLDoc.SelectNodes(xPath, nsMgr) ' returns a list of all Return objects
            ' as in
            'NodeList = XMLDoc.SelectNodes("/ISO:Returns/ISO:Result", nsMgr) ' returns a list of all Return objects

            ' find max no of Return elements in Returns
            iNoOfElements = NodeList.Count

        Catch
            XMLDoc = Nothing
            Err.Raise(11001, "DAL.ReaderWCF.LoadFromXMLString", "Error occured during load" & vbNewLine & "XML: " & sXML & vbNewLine & "xPath: " & xPath)

        End Try

    End Function
    Friend Function LoadXMLDefintions(ByVal sNameOfWCF As String) As Boolean
        ' load definitions (mapping) for a WEBService, from a XMLfile

        ' status: forbedret, under endring
        ' testet: litt


        Dim sMappingFilename As String = ""
        Dim XMLMapping = New System.Xml.XmlDocument
        Dim nsMgr As System.Xml.XmlNamespaceManager
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim nodeElement As System.Xml.XmlElement
		'Dim nodTemp As System.Xml.XmlNode
		Dim sTmp As String = ""

        Try

            ' Load "mapping" xml - which describes the imported soap document;
            ' This requires a mappingfile with the same name as the "rule", prefixed with "Mapping_" - placed in the \WCF folder
            If Not RunTime() Then
                If sCompanyName = "Active Brands AS" Or sCompanyName = "Active Brands US" Or sCompanyName = "ECom USA" Or sCompanyName = "Active Brands_BCNY" Then
                    sMappingFilename = "C:\Slett\ActiveBrands\WEBServices\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                ElseIf sCompanyName = "Storm Geo" Then
                    sMappingFilename = "C:\Slett\StormGeo_XLedger\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                ElseIf InStr(sCompanyName, "Grieg") > 0 Then
                    sMappingFilename = "C:\Slett\Grieg\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                ElseIf sCompanyName.ToUpper.IndexOf("ABAX") > -1 Then
                    sMappingFilename = "C:\Slett\ABAX\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                ElseIf sCompanyName.ToUpper.IndexOf("PATENTSTYRET") > -1 Then
                    sMappingFilename = "C:\Slett\PatentStyret\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                ElseIf sCompanyName = "Seaborn" Then
                    sMappingFilename = "C:\Slett\Seaborn\WS\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                ElseIf sCompanyName.ToUpper.IndexOf("NHST") > -1 Then
                    sMappingFilename = "C:\Slett\NHST\Naviga\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
                End If

            Else
                ' Runtime !
                sMappingFilename = My.Application.Info.DirectoryPath & "\WCF\" & "Mapping_" & sNameOfWCF & ".xml"
            End If

            If Dir(sMappingFilename) = "" Then
                Err.Raise(11002, "DAL.ReaderWCF.LoadXMLDefinitions", "Mappingfile not found!" & vbNewLine & "Filename: " & sMappingFilename & vbNewLine & "sNameOfWCF: " & sNameOfWCF)
            End If

            XMLMapping.Load(sMappingFilename)
            nsMgr = New System.Xml.XmlNamespaceManager(XMLMapping.NameTable)
            nsMgr.AddNamespace("ISO", XMLMapping.DocumentElement.NamespaceURI)

            nodeElement = XMLMapping.SelectSingleNode("/ISO:Definition", nsMgr)
            iFieldCount = Val(nodeElement.GetAttribute("NoOfElements"))

            nodeList = nodeElement.SelectNodes("ISO:Fields/ISO:Field", nsMgr)
            ReDim aFields(4, 0)
            If nodeList.Count > 0 Then
                For Each node In nodeList
                    If Not aFields(0, 0) Is Nothing Then
                        ReDim Preserve aFields(4, aFields.GetUpperBound(1) + 1)
                    End If

                    aFields(0, aFields.GetUpperBound(1)) = node.SelectSingleNode("ISO:FieldName", nsMgr).InnerText
                    aFields(1, aFields.GetUpperBound(1)) = node.SelectSingleNode("ISO:FieldType", nsMgr).InnerText

                    ' DisplaySequence can be used, but not always
                    If Not node.SelectSingleNode("ISO:DisplaySequence", nsMgr) Is Nothing Then
                        aFields(2, aFields.GetUpperBound(1)) = node.SelectSingleNode("ISO:DisplaySequence", nsMgr).InnerText
                    End If
                    ' BB_FieldName can be used, but not always. Is used if ERP fieldname differs from what we will use as
                    ' BabelBank fieldname (like BBRET_InvoiceNo)
                    If Not node.SelectSingleNode("ISO:BB_FieldName", nsMgr) Is Nothing Then
                        aFields(3, aFields.GetUpperBound(1)) = node.SelectSingleNode("ISO:BB_FieldName", nsMgr).InnerText
                    End If
                    If Not node.SelectSingleNode("ISO:BB_Format", nsMgr) Is Nothing Then
                        aFields(4, aFields.GetUpperBound(1)) = node.SelectSingleNode("ISO:BB_Format", nsMgr).InnerText
                    End If

                Next
            End If

        Catch
            Err.Raise(11003, "DAL.ReaderWCF.LoadXMLDefinitions", "Error loading mappingfile!" & vbNewLine & "Filename: " & sMappingFilename & vbNewLine & "sNameOfWCF: " & sNameOfWCF)
        End Try

    End Function
    Public Function FieldCount() As Integer
        'Status: OK

        ' pick up no of field from XML definitons file
        FieldCount = iFieldCount
    End Function
    Public Function RecordCount() As Long
        ' return no of elements in soap xml object
        ' Status: OK
        RecordCount = iNoOfElements
    End Function
    Public Function HasRows() As Boolean
        ' returns True if XMLObject has "records"

        ' status: OK

        If iNoOfElements > 0 Then
            HasRows = True
        Else
            HasRows = False
        End If
    End Function
    Public Function GetOrdinal(ByVal sFieldName As String) As Integer
        ' status: endret
        ' testing: litt

        Dim i As Integer
        ' return fieldnumber in class based on name of field
        GetOrdinal = -1
        For i = 0 To aFields.GetUpperBound(1)
            If sFieldName = aFields(0, i) Then
                GetOrdinal = i
                Exit For
            End If
            ' also test for "BabelBank-names", like BBRET_MatchId, etc, in aFields(3, i)
            If sFieldName = aFields(3, i) Then
                GetOrdinal = i
                Exit For
            End If

        Next i

    End Function
    Public Function GetName(ByVal iFieldNo As Integer) As String
        ' return name of "field" from number

        ' status: forbedret
        ' testet: litt

        ' also checks if it is "displayable. in the definitons file, DisplaySequence = -1 for fields which we will not display
        If aFields(2, iFieldNo) <> "-1" Then
            ' we have both "real" ERP fieldnames in aFields - aFields(0,, and the BabelBank fieldname (like BBRET_InvoiceNo), in aFields(3,
            If Not EmptyString(aFields(3, iFieldNo)) Then
                GetName = aFields(3, iFieldNo)  ' Prioritize the BB_Fieldname if in use
            Else
                GetName = aFields(0, iFieldNo)
            End If

        Else
            GetName = ""
        End If

    End Function
    Public Function GetFormat(ByVal iFieldNo As Integer) As String
        ' return format of "field" from number
        GetFormat = ""
        ' status: forbedret
        ' testet: litt

        GetFormat = aFields(4, iFieldNo)

    End Function
    Public Function GetERPName(ByVal sFieldName As String) As String
        ' return name of "field" from ERP system from "BB_Fieldname"
        Dim i As Integer

        ' status: råskrevet
        ' testet: litt

        GetERPName = ""
        For i = 0 To aFields.GetUpperBound(1)
            If aFields(3, i) = sFieldName Then
                GetERPName = aFields(0, i)
                Exit For
            End If
            ' in some cases (ActiveBrands) we can have "real fieldname" in element 0, and nothing in element 3
            ' (no effect of using this function, but to be consistent we are always calling GetERPName
            If aFields(0, i) = sFieldName Then
                GetERPName = aFields(0, i)
                Exit For
            End If

        Next

    End Function
    Public Function GetDataTypeName(ByVal iFieldNo As Integer) As String
        ' return type of "field" from number

        ' status: OK
        GetDataTypeName = aFields(1, iFieldNo)
    End Function
    Public Function Read() As Boolean
        ' advance to next "row" in xmlobject

        ' status: råskrevet
        ' testet: litt

        Try
            If iElementNo < iNoOfElements - 1 Then
                iElementNo = iElementNo + 1
                nodCurrent = NodeList.Item(iElementNo)
                Read = True
            Else
                Read = False
            End If

        Catch
            Err.Raise(11004, "DAL.ReaderWCF.Read", "Error finding node!" & vbNewLine & "iElementNo: " & iElementNo.ToString)

        End Try
    End Function
    Public Function GetString(ByVal sFieldName As String) As String
        ' return nth element of current node, as string
        'Dim sReturn As String = ""
        Dim sFormat As String = ""
        Dim sDecimalInFile As String = ","
        Dim sDecimalInPC As String = ","
        Dim sMultiply As String = "1"
        Dim iNoOfDecimals = 2

        ' status: råskrevet
        ' testet: nix

        Try
            GetString = ""
            If sFieldName <> "" Then
                If Not nodCurrent.SelectSingleNode(sFieldName) Is Nothing Then
                    GetString = nodCurrent.Item(sFieldName).InnerText
                Else
                    ' then try to find it as a property if it's not a node
                    GetString = nodCurrent.Attributes(sFieldName).Value
                End If
            End If

            ' check if there is a special formatting, placed in BB_Format, aFields(5,
            sFormat = GetFormat(GetOrdinal(sFieldName))
            If Not EmptyString(sFormat) Then
                ' reformat returnstring

                ' 14.05.2020 - added formatting of dates.
                ' Only a limited set of datestrings so far.
                If InStr(sFormat.ToUpper, "D") > 0 And InStr(sFormat.ToUpper, "M") > 0 Then
                    ' datestring
                    ' ----------
                    Select Case sFormat.ToUpper
                        Case "YYYY-MM-DD", "YYYY/MM/DD"
                            GetString = Left(GetString, 4) & Mid(GetString, 6, 2) & Mid(GetString, 9, 2)
                        Case "DD-MM-YYYY", "DD/MM/YYYY"
                            GetString = Right(GetString, 4) & Mid(GetString, 4, 2) & Mid(GetString, 9, 2)
                    End Select
                Else
                    ' Amounts
                    ' -------
                    ' dette kan nok endres underveis, men vi starter med:
                    ' - dersom det er med . eller , så er dette desimaltegn
                    '   sjekk om dette avviker fra pc-ens setting
                    ' - ## angir antall tegn etter desimal
                    If GetString.IndexOf(",") > -1 Then
                        sDecimalInFile = ","
                    ElseIf GetString.IndexOf(".") > -1 Then
                        sDecimalInFile = "."
                    Else
                        sDecimalInFile = ""
                    End If
                    'GetString = ConvertToAmount(GetString, "", sDecimalInFile, False)
                    If sDecimalInFile <> sDecimalInPC Then
                        ' gjør om fra . til , eller omvendt
                        GetString = GetString.Replace(sDecimalInFile, sDecimalInPC)
                    End If
                    iNoOfDecimals = Len(sFormat) - Len(Replace(sFormat, "#", ""))
                    ' we will need 2 decimal
                    ' 23.08.2019 why always 2 decimals if wehave set f.ex. 4 in formatstring?
                    ' Changed line below:
                    'GetString = Left(GetString, Len(GetString) - (iNoOfDecimals - 2))
                    'GetString = Left(GetString, Len(GetString) - (iNoOfDecimals))
                    If Not EmptyString(sMultiply) Then
                        sMultiply = KeepNumericsOnly(sFormat)
                        GetString = (CDbl(GetString) * Val(sMultiply)).ToString
                    End If
                End If   'If InStr(sFormat.ToUpper, "D") > 0 And InStr(sFormat.ToUpper, "M") > 0 Then
            End If
        Catch
            Err.Raise(11005, "DAL.ReaderWCF.GetString", "Error getting string!" & vbNewLine & "sFieldName: " & sFieldName)

        End Try

    End Function
    Public WriteOnly Property NameOfCompany() As String
        Set(ByVal Value As String)
            sCompanyName = Value
        End Set
    End Property

End Class

