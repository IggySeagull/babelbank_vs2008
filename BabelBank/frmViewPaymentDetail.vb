﻿Imports DGVPrinterHelper
Public Class frmViewPaymentDetail
    Dim oBabelFiles As BabelFiles
    Dim oBatch As Batch
    Dim oPayment As Payment
    Dim oInvoice As Invoice
    Private Sub frmViewPaymentDetail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", 500)
        FormLRSCaptions(Me)
    End Sub
    Public Sub SetBabelFilesInViewPayment(ByRef NewVal As vbBabel.BabelFiles)
        oBabelFiles = NewVal
    End Sub
    Public Sub PassBatch(ByVal obj As Batch)
        ' pass batchobject to frmViewPaymentDetail
        oBatch = obj
    End Sub
    Public Sub PassPayment(ByVal obj As Payment)
        ' pass paymentobject to frmViewPaymentDetail
        oPayment = obj
    End Sub
    Public Sub PassInvoice(ByVal obj As Invoice)
        ' pass invoiceobject to frmViewPaymentDetail
        oInvoice = obj
    End Sub
    Public Sub Fill_gridBatchDetail()
        ' spin through batchtobject, and fill grid with all items
        Dim property_value As Object
        Dim properties_info As System.Reflection.PropertyInfo() = GetType(Batch).GetProperties()
        Dim i As Integer

        ' construct grid
        gridViewDetailConstruct(Me.GridPaymentDetail)

        For i = 0 To properties_info.Length - 1
            With properties_info(i)
                If .GetIndexParameters().Length = 0 Then
                    If UCase(Microsoft.VisualBasic.Left(.Name, 5)) <> "MATCH" And UCase(Microsoft.VisualBasic.Left(.Name, 3)) <> "VB_" Then
                        'If .PropertyType Is GetType(System.String) Then
                        property_value = .GetValue(oBatch, Nothing)
                        If property_value Is Nothing Then
                            gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, "<Nothing>")
                        Else
                            gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, property_value.ToString)
                        End If
                    End If
                Else
                    gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, "<array>")
                End If
            End With
        Next i
        Me.GridPaymentDetail.Rows(1).Selected = True
    End Sub
    Public Sub Fill_gridPaymentDetail()
        ' spin through paymentobject, and fill grid with all items
        Dim property_value As Object
        Dim properties_info As System.Reflection.PropertyInfo() = GetType(Payment).GetProperties()
        Dim i As Integer

        ' construct grid
        gridViewDetailConstruct(Me.GridPaymentDetail)

        For i = 0 To properties_info.Length - 1
            With properties_info(i)
                If .GetIndexParameters().Length = 0 Then
                    If UCase(Microsoft.VisualBasic.Left(.Name, 5)) <> "MATCH" And UCase(Microsoft.VisualBasic.Left(.Name, 3)) <> "VB_" Then
                        'If .PropertyType Is GetType(System.String) Then
                        property_value = .GetValue(oPayment, Nothing)
                        If property_value Is Nothing Then
                            gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, "<Nothing>")
                        Else
                            gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, property_value.ToString)
                        End If
                    End If
                Else
                    gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, "<array>")
                End If
            End With
        Next i
        Me.GridPaymentDetail.Rows(1).Selected = True
    End Sub
    Public Sub Fill_gridInvoiceDetail()
        ' spin through invoiceobject, and fill grid with all items
        Dim property_value As Object
        Dim properties_info As System.Reflection.PropertyInfo() = GetType(Invoice).GetProperties()
        Dim i As Integer

        ' construct grid
        gridViewDetailConstruct(Me.GridPaymentDetail)

        For i = 0 To properties_info.Length - 1
            With properties_info(i)
                If .GetIndexParameters().Length = 0 Then
                    If UCase(Microsoft.VisualBasic.Left(.Name, 5)) <> "MATCH" And UCase(Microsoft.VisualBasic.Left(.Name, 3)) <> "VB_" Then
                        property_value = .GetValue(oInvoice, Nothing)
                        If property_value Is Nothing Then
                            gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, "<Nothing>")
                        Else
                            gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, property_value.ToString)
                        End If
                    End If
                Else
                    gridViewDetailMakeRow(GridPaymentDetail, .Name, .PropertyType.ToString, "<array>")
                End If
            End With
        Next i
        Me.GridPaymentDetail.Rows(1).Selected = True
    End Sub
    Private Sub gridViewDetailMakeRow(ByVal grid As DataGridView, ByVal sName As String, ByVal sType As String, ByVal sValue As String)
        ' add row, present data
        With grid
            .Rows.Add()
            .Rows(.RowCount - 1).Cells(0).Value = sName
            .Rows(.RowCount - 1).Cells(1).Value = sValue
        End With

    End Sub
    Private Sub gridViewDetailConstruct(ByVal grid As DataGridView)
        Dim txtColumn As DataGridViewTextBoxColumn

        With grid
            .Columns.Clear()
            .ScrollBars = ScrollBars.Both
            .AllowUserToAddRows = False
            .RowHeadersVisible = False
            .RowHeadersWidth = 60
            .RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .BorderStyle = BorderStyle.None
            .RowTemplate.Height = 18
            .BackgroundColor = Color.White
            .CellBorderStyle = DataGridViewCellBorderStyle.Single
            .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
            .AutoSize = False
            .ReadOnly = True
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect

            ' then add columns
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "Name"
            txtColumn.Width = 200
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "Value"
            txtColumn.Width = 400
            .Columns.Add(txtColumn)

        End With
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.Close()
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        Dim Printer = New DGVPrinter
        Printer.Title = LRS(60002)  ' Payments
        Printer.SubTitle = Me.Text
        Printer.SubTitleFormatFlags = _
           StringFormatFlags.LineLimit Or StringFormatFlags.NoClip
        Printer.PageNumbers = True
        Printer.PageNumberInHeader = False
        Printer.PorportionalColumns = True
        Printer.HeaderCellAlignment = StringAlignment.Near
        Printer.Footer = "BabelBank - Visual Banking AS"
        Printer.FooterSpacing = 15
        Printer.PrintDataGridView(Me.GridPaymentDetail)
    End Sub
End Class