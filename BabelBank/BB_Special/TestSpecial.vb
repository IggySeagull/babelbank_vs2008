﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Linq
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Text
Imports System.Threading.Tasks
Imports Newtonsoft.Json.Linq
Imports Orders.Contracts
Imports Thinktecture.IdentityModel.Client
Imports Thinktecture.IdentityModel.Extensions
Imports System.Threading

Public Class TestSpecial
    Private sText As String
    Private aMyOrders(,) As String
    '0 - OrderID
    '1 - Payee
    '2 - TotalPaid
    '3 - PaidDate
    '4 - TransId
    '5 - PayeeBankAccount
    ' - TransIdDate? Dagens dato?
    ' - TotalFee
    ' - ReImbursement
    ' - Rounding
    ' - AcceptedDeviance
    Private aMyPayables(,) As String
    '0 - OrdreId
    '1 - PayablesId?
    '2 - TotalPaid     
    Private sBaseAddress = "https://login.patentstyret.no/core"
    Private AuthorizeEndpoint = sBaseAddress + "/connect/authorize"
    Private LogoutEndpoint = sBaseAddress + "/connect/endsession"
    Private TokenEndpoint = sBaseAddress + "/connect/token"
    Private UserInfoEndpoint = sBaseAddress + "/connect/userinfo"
    Private IdentityTokenValidationEndpoint = sBaseAddress + "/connect/identitytokenvalidation"
    Private TokenRevocationEndpoint = sBaseAddress + "/connect/revocation"
    'Private oClient As HttpClient
    Private sFixedErrorText As String
    Private bFunctionFinished As Boolean
    Private sOrderRef As String
    ' Ny 25.08.2015 - flyttet fra RequestToken
    Private oClient As HttpClient
    Private Token As TokenResponse

    'Private sClientBaseAddress = "https://217.144.244.51/"   ' Fra VBs lokaler
    Private sClientBaseAddress = "https://ws-test.patentstyret.no/"   'fra Patentstyrets lokaler
    Public Sub New()
        sText = ""
        ' Flyttet hit 25.08.2015
        Token = RequestToken()
    End Sub
    Public Property FixedErrorText As String
        Get
            FixedErrorText = sFixedErrorText
        End Get
        Set(ByVal value As String)
            sFixedErrorText = value
        End Set

    End Property
    Public Property BaseAddress As String
        Get
            BaseAddress = sBaseAddress
        End Get
        Set(ByVal value As String)
            sBaseAddress = value

            ' reset variables based on sBaseAddress
            AuthorizeEndpoint = sBaseAddress + "/connect/authorize"
            LogoutEndpoint = sBaseAddress + "/connect/endsession"
            TokenEndpoint = sBaseAddress + "/connect/token"
            UserInfoEndpoint = sBaseAddress + "/connect/userinfo"
            IdentityTokenValidationEndpoint = sBaseAddress + "/connect/identitytokenvalidation"
            TokenRevocationEndpoint = sBaseAddress + "/connect/revocation"
        End Set

    End Property
    Public Property PassOrders As String(,)
        Get
            PassOrders = aMyOrders
        End Get
        Set(ByVal value As String(,))
            aMyOrders = value
            ' test med visning av første post
            'MsgBox(aMyOrders(0, 0) & ", " & aMyOrders(1, 0))
        End Set
    End Property
    Public Property PassPayables As String(,)
        Get
            PassPayables = aMyPayables
        End Get
        Set(value As String(,))
            aMyPayables = value
            ' test med visning av første post
            'MsgBox(aMyPayables(0, 0))
        End Set
    End Property
    Public Property ReturnedOrderRef As String
        Get
            ReturnedOrderRef = sOrderRef
        End Get
        Set(value As String)
            sOrderRef = value
        End Set
    End Property
    Sub Pause(ByRef iSeconds As Double)
        Dim nStart As Double

        ' wait for n seconds
        nStart = Microsoft.VisualBasic.Timer() ' Set start time.
        Do While Microsoft.VisualBasic.Timer() < nStart + iSeconds
            'System.Windows.Form.Application.DoEvents() ' Yield to other processes.
        Loop

    End Sub
    Public Function DoTheUpdate() As Boolean
        ' ----------------------------------------------------
        ' Update an order or Payable
        ' Depends upon values set in oMyOrders and oMyPayables
        ' ----------------------------------------------------
        Dim bRetValue As Boolean = True

        Try
            'Dim Token As TokenResponse
            Dim cts As System.Threading.CancellationTokenSource
            Dim iCounter As Integer
            cts = New CancellationTokenSource()
            'Token = RequestToken()
            'Dim contentLength As Integer
            Dim sErrorString As String
            Dim s As String = ""
            '//////////////
            ' / her må vi gjøre selve jobben
            ' / kall på oppdateringsrutiner under her, men må først analysere innhold i de arrayer vi har
            ' / for å se hva vi skal gjløre
            sErrorString = Token.HttpErrorStatusCode
            sOrderRef = ""

            If sErrorString <> "0" Then
                Throw New Exception("Patentstyret - DoTheUpdate - Feil i eksportrutine." & vbCrLf & "RequestToken returnerer " & sErrorString)
            End If


            If InitiateClient(Token.AccessToken, s) Then

                'First, check if we have any new orders to be updated
                For iCounter = 0 To aMyOrders.GetUpperBound(1)

                    If aMyOrders(0, iCounter) = "NY_ORDRE" Then
                        'CallService(Token.AccessToken).ConfigureAwait(False)

                        bFunctionFinished = False
                        'Dim bNewOrderCreated As Task(Of Boolean) = CreateNewOrder(Token.AccessToken, iCounter)
                        'Dim b As Boolean = Await bNewOrderCreated
                        'Dim bNewOrderCreated As Boolean = CreateNewOrder(Token.AccessToken, iCounter)

                        If Not CreateNewOrder(Token.AccessToken, iCounter) Then

                        End If


                        'CreateNewOrder(Token.AccessToken, iCounter).ConfigureAwait(False)
                    End If
                Next iCounter

                For iCounter = 0 To aMyOrders.GetUpperBound(1)
                    UpdateOrderAsPaid(Token.AccessToken, iCounter)
                    'UpdateOrderAsPaid(Token.AccessToken, iCounter).Wait()
                    'UpdateOrderAsPaid(Token.AccessToken, iCounter).ConfigureAwait(False)
                Next iCounter
                'CallService(Token.AccessToken).ConfigureAwait(False)
            End If

        Catch ex As Exception

        End Try

        Return True

    End Function
    Public Function Update_Comments(lOrderId As Long, sExternaltext As String, sInternaltext As String) As Boolean
        Dim oresponse As HttpResponseMessage
        'Dim Token As TokenResponse
        Dim cts As System.Threading.CancellationTokenSource
        cts = New CancellationTokenSource()
        'Token = RequestToken()
        'Dim contentLength As Integer
        Dim sErrorString As String
        Dim oClient As HttpClient
        Dim OrderCommentUpdate = New OrderHeader()
        Dim s As String = ""

        Try

            sErrorString = Token.HttpErrorStatusCode
            If sErrorString <> "0" Then
                Throw New Exception("Patentstyret - Update_Comments - Feil." & vbCrLf & "RequestToken returnerer " & sErrorString)
            End If

            If InitiateClient(Token.AccessToken, s) Then
                '///////////////////////////////////////////////////////////////////
                ' Legg til kommentarer på en ordre
                ' External Internal comment
                ''using (var client = new HttpClient())

                oClient = New HttpClient
                oClient.BaseAddress = New Uri(sClientBaseAddress)
                oClient.SetBearerToken(Token.AccessToken)
                oClient.DefaultRequestHeaders.Accept.Clear()
                oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))

                OrderCommentUpdate.Id = lOrderId
                OrderCommentUpdate.ExternalComment = sExternaltext
                OrderCommentUpdate.InternalComment = sInternaltext

                oresponse = oClient.PutAsJsonAsync("orders/Orders/header", OrderCommentUpdate).Result
                'oresponse = Await oClient.PutAsJsonAsync("orders/Orders/header", OrderCommentUpdate)
                If oresponse.IsSuccessStatusCode Then
                    'Console.WriteLine("Ordre kommentar oppdatert")
                Else
                    MsgBox("Feil i oppdatering av kommentarer: " & Str(oresponse.StatusCode))
                End If

                'Dim OrderCommentUpdate = New OrderHeader()
                'OrderCommentUpdate.Id = 2163
                'OrderCommentUpdate.ExternalComment = "Externaltext"
                'OrderCommentUpdate.InternalComment = "InternalText"

                'oresponse = Await oClient.PutAsJsonAsync("orders/Orders/header", OrderCommentUpdate)
                'If oresponse.IsSuccessStatusCode Then
                '    Console.WriteLine("Ordre kommentar oppdatert")
                'Else
                '    MsgBox("Feil: " & Str(oresponse.StatusCode))
                'End If

            End If

        Catch ex As Exception

        End Try

        Return True

    End Function
    Private Function RequestToken() As TokenResponse
        ' 25.08.2015 - Vi prøver å kjøre kun en RequestToken for å spare tid
        Dim oRequestClient As OAuth2Client
        '   var client = new OAuth2Client(
        '       new Uri(Constants.TokenEndpoint),
        '       "ordersApiClient",
        '       "secret");

        '    return client.RequestResourceOwnerPasswordAsync("jan-petter@visualbanking.net", "ValidPassword02##", "ordersApi").Result;
        oRequestClient = New OAuth2Client(New Uri(TokenEndpoint), "ordersApiClient", "secret")
        'RequestToken = oRequestClient.RequestClientCredentialsAsync(scope:="ordersApi").Result
        RequestToken = oRequestClient.RequestResourceOwnerPasswordAsync("jan-petter@visualbanking.net", "ValidPassword02##", "ordersApi").Result
        'RequestToken = oRequestClient.RequestResourceOwnerPasswordAsync("fst@patentstyret.no", "Freddy@?", "ordersApi").Result
        'RequestToken = oRequestClient.RequestResourceOwnerPasswordAsync("ha@itverket.no", "Pass02##", "ordersApi").Result
        '26.08.2015
        ' Then instanciate oClient once and for all here, instead of doing it several places;
        oClient = New HttpClient
        oClient.BaseAddress = New Uri(sClientBaseAddress)
        'oClient.SetBearerToken(Token..AccessToken)
        oClient.SetBearerToken(RequestToken.AccessToken)
        oClient.DefaultRequestHeaders.Accept.Clear()
        oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))


    End Function
    Function CreateNewOrder(Token As String, iArrayOrderCounter As Integer) As Boolean
        Dim bCreateOrder As Boolean
        Dim lArrayCounter As Long
        'Dim newOrder = New OrderToPlace()
        'Dim oresponse As HttpResponseMessage '= Await oClient.GetAsync("orders/orders/1875")
        Dim oresponse As HttpResponseMessage
        Dim sId As String
        Dim oReadOrder = New Orders.Contracts.ReadOrder
        'Dim oClient As HttpClient
        'Dim sBaseAddress As String
        Dim payables = New Collection(Of Long)
        Dim newOrder = New OrderToPlace()
        Dim bReturnvalue As Boolean = False

        Try

            ''//var baseAddress =  "http://ws.test.patentstyret.no/";
            'sBaseAddress = "http://217.144.244.51/"

            'sBaseAddress = "http://ws.test.patentstyret.no/"

            ''using (var client = new HttpClient())
            'oClient = New HttpClient
            'oClient.BaseAddress = New Uri(sClientBaseAddress)
            'oClient.SetBearerToken(Token)
            'oClient.DefaultRequestHeaders.Accept.Clear()
            'oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))

            bCreateOrder = True
            If bCreateOrder Then
                'Dim payables = New Collection(Of Long)


                'payables.Add(205528)

                'Dim newOrder = New OrderToPlace()

                'newOrder.PayableIds = payables
                'newOrder.PaymentMethodCode = 0 '//0 = Banktransfer, 4 = BabelBank
                ''newOrder.UserEmail = "HarIngen@eMail.no"                ' Hent eMail fra kommentar i BB

                '' /////////////////////////////////////////////////////////////////
                '' Legg inn ny ordre
                'oresponse = Await oClient.PostAsJsonAsync("orders/Orders", newOrder)
                'If oresponse.IsSuccessStatusCode Then
                '    Console.WriteLine("Ny Ordre lagt inn")
                'Else
                '    MsgBox("Feil: " & Str(oresponse.StatusCode))
                'End If







                For lArrayCounter = 0 To aMyPayables.GetUpperBound(1)
                    'Check if it is a payable to be included in the new order

                    If aMyPayables(0, lArrayCounter) = "NY_ORDRE" Then
                        payables.Add(CLng(aMyPayables(1, lArrayCounter)))
                    End If

                Next lArrayCounter

                newOrder.PayableIds = payables
                newOrder.PaymentMethodCode = 0 '//0 = Banktransfer, 4 = BabelBank

                ' /////////////////////////////////////////////////////////////////
                ' Legg inn ny ordre

                'Dim oresponse As HttpResponseMessage = Await oClient.PostAsJsonAsync("orders/Orders", newOrder)

                oresponse = oClient.PostAsJsonAsync("orders/Orders", newOrder).Result
                'oresponse = Await oClient.PostAsJsonAsync("orders/Orders", newOrder)

                If oresponse.IsSuccessStatusCode Then
                    Console.WriteLine("Ny Ordre lagt inn")
                Else
                    MsgBox("Feil: " & Str(oresponse.StatusCode))
                End If
                oReadOrder = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)().Result
                'oReadOrder = Await oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)()
                sId = CStr(oReadOrder.Id)        ' Bruk lID videre når vi skal oppdatere data på den ny ordren !!!!!!!!

                ' Added 25.08.2015
                ' KJELL - er det denne vi er ute etter, som skal fylles opp i oInvoice.CustomerNo
                sOrderRef = oReadOrder.RefNr

                For lArrayCounter = 0 To aMyOrders.GetUpperBound(1)
                    'Check if it is a payable to be included in the new order

                    If aMyOrders(0, lArrayCounter) = "NY_ORDRE" Then
                        aMyOrders(0, lArrayCounter) = sId
                    End If

                Next lArrayCounter
                For lArrayCounter = 0 To aMyPayables.GetUpperBound(1)
                    'Check if it is a payable to be included in the new order

                    If aMyPayables(0, lArrayCounter) = "NY_ORDRE" Then
                        aMyPayables(0, lArrayCounter) = sId
                    End If

                Next lArrayCounter

                bFunctionFinished = True

                newOrder = Nothing
                oresponse = Nothing
                oReadOrder = Nothing 'Should probably keep this, to reuse it in the function UpdateOrderAsPaid

                bReturnvalue = True

            End If

        Catch ex As Exception
            'newOrder = Nothing
            'oresponse = Nothing
            If Not oReadOrder Is Nothing Then
                oReadOrder = Nothing 'Should probably keep this, to reuse it in the function UpdateOrderAsPaid
            End If

            bReturnvalue = False

            MsgBox(ex.Message & vbCrLf & vbCrLf & sFixedErrorText)

        End Try

        Return bReturnvalue

    End Function
    Function UpdateOrderAsPaid(Token As String, iArrayOrderCounter As Integer) As Boolean
        'Async Function UpdateOrderAsPaid(Token As String, iArrayOrderCounter As Integer) As Task(Of Boolean)
        'Dim oClient As HttpClient
        Dim oReadOrder As Orders.Contracts.ReadOrder
        Dim OrderToReconcile = New Orders.Contracts.OrderToReconcile
        Dim oReadOrderPayable As Orders.Contracts.ReadOrderPayable
        Dim oOrderPayableToReconcile As Orders.Contracts.OrderPayableToReconcile
        Dim oReadOrderPayableItem As Orders.Contracts.ReadOrderPayableItem
        Dim oOrderPayableItemToReconcile As Orders.Contracts.OrderPayableItemToReconcile
        Dim oOrderPayableItemsToReconcile As Collection(Of Orders.Contracts.OrderPayableItemToReconcile)
        Dim oOrderPayablesToReconcile As Collection(Of Orders.Contracts.OrderPayableToReconcile)
        Dim oResponse As HttpResponseMessage
        Dim nTotalDeviation As Double = 0
        Dim bFirstPayable As Boolean = True
        Dim bFirstPayableItem As Boolean = True
        Dim bContinue As Boolean = False
        Dim iArrayPayableCounter As Integer

        Try

            ''//var baseAddress =  "http://ws.test.patentstyret.no/";

            ''Pålogging fra Patentstyret
            'sLocalBaseAddress = "http://ws.test.patentstyret.no/"
            ''Pålogging fra VB lokaler
            'sLocalBaseAddress = "http://217.144.244.51/"

            'using (var client = new HttpClient())
            'oClient = New HttpClient
            'oClient.BaseAddress = New Uri(sClientBaseAddress)
            'oClient.SetBearerToken(Token)
            'oClient.DefaultRequestHeaders.Accept.Clear()
            'oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))

            'HTTP GET -- Henter ut en ordre fra API'et og fyller objekt av typen ReadOrder
            oResponse = oClient.GetAsync("orders/orders/" & aMyOrders(0, iArrayOrderCounter)).Result
            'Dim oresponse As HttpResponseMessage = Await oClient.GetAsync("orders/orders/" & aMyOrders(0, iArrayOrderCounter))

            If oResponse.IsSuccessStatusCode Then

                oReadOrder = oResponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)().Result
                'oReadOrder = Await oResponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)()

            Else
                MsgBox("Feil: " & Str(oResponse.StatusCode))
            End If

            'Check if order is open
            If oReadOrder.StateCode = 1 Then
                'It's open, check the amount
                If oReadOrder.TotalAmount >= CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 Then
                    '////////////////////////
                    '/ OK, update the order /
                    '////////////////////////

                    OrderToReconcile.Id = oReadOrder.Id    ' View: OrderID ex 1996
                    'OrderToReconcile.AcceptedDevience = CDbl(aMyOrders(?, iArrayOrderCounter))/100      ' over/underbetaling på en ordre, avvik mellom TotalPaid og Amount
                    'OrderToReconcile.PaymentFee = CDbl(aMyOrders(?, iArrayOrderCounter))/100            ' bankgebyr
                    'OrderToReconcile.Reimbursement = CDbl(aMyOrders(?, iArrayOrderCounter))/100         ' tilbakebealing
                    'OrderToReconcile.Rounding = CDbl(aMyOrders(?, iArrayOrderCounter))/100      ' avrunding
                    ' Datoformat: 2015-06-24
                    'OrderToReconcile.PaidDate = StringToDate(aMyOrders(3, iArrayOrderCounter))              ' betalt dato
                    OrderToReconcile.PaidDate = Left(aMyOrders(3, iArrayOrderCounter), 4) & "-" & Mid(aMyOrders(3, iArrayOrderCounter), 5, 2) & "-" & Right(aMyOrders(3, iArrayOrderCounter), 2)              ' betalt dato
                    nTotalDeviation = 0
                    nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(6, iArrayOrderCounter))
                    nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(7, iArrayOrderCounter))
                    nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(8, iArrayOrderCounter))
                    'nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(9, iArrayOrderCounter))
                    nTotalDeviation = nTotalDeviation / 100
                    OrderToReconcile.TotalPaid = CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 + nTotalDeviation 'oReadOrder.TotalAmount             ' betalt beløp
                    OrderToReconcile.PayeeCremul = aMyOrders(1, iArrayOrderCounter)                ' betalers navn, endres til PayeeCremul
                    OrderToReconcile.PayeeBankAccount = aMyOrders(5, iArrayOrderCounter)      ' betalers bankkonto
                    If oReadOrder.TotalAmount > CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 And aMyOrders(10, iArrayOrderCounter) = "ORDERPAYABLE" Then
                        OrderToReconcile.StateCode = 3
                    Else
                        OrderToReconcile.StateCode = 2           ' 2 = Betalt med korrekt ordrebeløp, innenfor AcceptedDeviance 3 = betalt med beløpsavvik.
                    End If
                    ' Vi må sjekke at TotalPaid er lik Amount, innenfor Roundings og AcceptedDevience
                    OrderToReconcile.BabelBankId = aMyOrders(4, iArrayOrderCounter)         ' unik id fra BabelBank
                    OrderToReconcile.BabelBankIdDate = Format(Date.Today, "yyyy-MM-dd")       ' dato for postering FORMAT ???
                    'OrderToReconcile.TransIdDate = "2015-06-24"
                    ' Trenger vi disse? Hva skjer hvis vi ikke setter de
                    'OrderToReconcile.ExternalComment = oReadOrder.ExternalComment
                    'OrderToReconcile.InternalComment = oReadOrder.InternalComment  'DENNE FINNES IKKE!!!!!
                    OrderToReconcile.PaymentFee = CDbl(aMyOrders(6, iArrayOrderCounter)) / -100
                    OrderToReconcile.Rounding = CDbl(aMyOrders(7, iArrayOrderCounter)) / -100
                    OrderToReconcile.AcceptedDevience = CDbl(aMyOrders(8, iArrayOrderCounter)) / -100
                    OrderToReconcile.Reimbursement = CDbl(aMyOrders(9, iArrayOrderCounter)) / -100

                    oOrderPayablesToReconcile = New Collection(Of Orders.Contracts.OrderPayableToReconcile)
                    For Each oReadOrderPayable In oReadOrder.ReadOrderPayables

                        bContinue = False
                        If aMyOrders(10, iArrayOrderCounter) = "ORDERPAYABLE" Then
                            For iArrayPayableCounter = 0 To aMyPayables.GetUpperBound(1)
                                If oReadOrderPayable.Id = CLng(aMyPayables(1, iArrayPayableCounter)) Then
                                    bContinue = True
                                    Exit For
                                End If
                            Next iArrayPayableCounter
                        Else
                            bContinue = True
                        End If

                        If bContinue Then
                            oOrderPayableToReconcile = New OrderPayableToReconcile  'OrderPayableToUpdateSummary

                            oOrderPayableToReconcile.Id = oReadOrderPayable.Id
                            oOrderPayableToReconcile.SourcePayableId = oReadOrderPayable.SourcePayableId
                            If bFirstPayable Then
                                bFirstPayable = False
                                oOrderPayableToReconcile.TotalPaid = oReadOrderPayable.TotalAmount + nTotalDeviation
                            Else
                                oOrderPayableToReconcile.TotalPaid = oReadOrderPayable.TotalAmount
                            End If

                            oOrderPayableItemsToReconcile = New Collection(Of Orders.Contracts.OrderPayableItemToReconcile)
                            For Each oReadOrderPayableItem In oReadOrderPayable.ReadOrderPayableItems
                                oOrderPayableItemToReconcile = New Orders.Contracts.OrderPayableItemToReconcile

                                oOrderPayableItemToReconcile.Id = oReadOrderPayableItem.Id
                                If bFirstPayableItem Then
                                    bFirstPayableItem = False
                                    oOrderPayableItemToReconcile.TotalPaid = oReadOrderPayableItem.Amount + nTotalDeviation
                                Else
                                    oOrderPayableItemToReconcile.TotalPaid = oReadOrderPayableItem.Amount
                                End If

                                oOrderPayableItemsToReconcile.Add(oOrderPayableItemToReconcile)

                            Next
                            oOrderPayableToReconcile.OrderPayableItemsToReconcile = oOrderPayableItemsToReconcile
                            'oOrderPayableToReconcile.OrderPayableItemsToReconcile = oReadOrderPayable.ReadOrderPayableItems
                            oOrderPayablesToReconcile.Add(oOrderPayableToReconcile)
                        End If
                    Next oReadOrderPayable
                    OrderToReconcile.OrderPayablesToReconcile = oOrderPayablesToReconcile

                    ''------->>>> oppsett i test fra Kari -------------
                    ''---------------------------------------------------
                    'OrderToReconcile.Id = 1879    ' View: OrderID ex 1996
                    'OrderToReconcile.PaidDate = "2015-06-24"
                    'OrderToReconcile.TotalPaid = 3000             ' betalt beløp
                    'OrderToReconcile.PayeeCremul = "En ny Cremul"                ' betalers navn, endres til PayeeCremul
                    'OrderToReconcile.PayeeBankAccount = "344567890"      ' betalers bankkonto
                    'OrderToReconcile.StateCode = 2           ' 2 = Betalt med korrekt ordrebeløp, innenfor AcceptedDeviance 3 = betalt med beløpsavvik.
                    'OrderToReconcile.BabelBankIdDate = "2015-06-26"       ' dato for postering FORMAT ???
                    ''OrderToReconcile.TransIdDate = "2015-06-24"

                    '' Nr 1
                    'oOrderPayablesToReconcile = New Collection(Of Orders.Contracts.OrderPayableToReconcile)
                    ''For Each oReadOrderPayable In oReadOrder.ReadOrderPayables
                    'oOrderPayableToReconcile = New OrderPayableToReconcile  'OrderPayableToUpdateSummary
                    'oOrderPayableToReconcile.Id = 1769
                    'oOrderPayableToReconcile.SourcePayableId = 205528
                    'oOrderPayableToReconcile.TotalPaid = 1500

                    'oOrderPayableItemsToReconcile = New Collection(Of Orders.Contracts.OrderPayableItemToReconcile)
                    ''For Each oReadOrderPayableItem In oReadOrderPayable.ReadOrderPayableItems
                    'oOrderPayableItemToReconcile = New Orders.Contracts.OrderPayableItemToReconcile
                    'oOrderPayableItemToReconcile.Id = 2501
                    'oOrderPayableItemToReconcile.TotalPaid = 1501
                    'oOrderPayableItemsToReconcile.Add(oOrderPayableItemToReconcile)

                    '' Nr 2
                    'oOrderPayableToReconcile = New OrderPayableToReconcile  'OrderPayableToUpdateSummary
                    'oOrderPayableToReconcile.Id = 3082
                    'oOrderPayableToReconcile.SourcePayableId = 281387
                    'oOrderPayableToReconcile.TotalPaid = 1300

                    'oOrderPayableItemsToReconcile = New Collection(Of Orders.Contracts.OrderPayableItemToReconcile)
                    ''For Each oReadOrderPayableItem In oReadOrderPayable.ReadOrderPayableItems
                    'oOrderPayableItemToReconcile = New Orders.Contracts.OrderPayableItemToReconcile
                    'oOrderPayableItemToReconcile.Id = 304071
                    'oOrderPayableItemToReconcile.TotalPaid = 1301
                    'oOrderPayableItemsToReconcile.Add(oOrderPayableItemToReconcile)

                    ''Next
                    'oOrderPayableToReconcile.OrderPayableItemsToReconcile = oOrderPayableItemsToReconcile
                    ''oOrderPayableToReconcile.OrderPayableItemsToReconcile = oReadOrderPayable.ReadOrderPayableItems
                    'oOrderPayablesToReconcile.Add(oOrderPayableToReconcile)
                    ''Next oReadOrderPayable
                    'OrderToReconcile.OrderPayablesToReconcile = oOrderPayablesToReconcile

                    'Do While iArrayPayableCounter <= aMyPayables.GetUpperBound(1)
                    '    If aMyPayables(0, iArrayPayableCounter) = aMyOrders(0, iArrayOrderCounter) Then
                    '        oPayable = New OrderPayableToReconcile  'OrderPayableToUpdateSummary
                    '        oPayable.Id = CLng(aMyPayables(1, iArrayPayableCounter) ' View: OrderPayableId
                    '        oPayable.SourcePayableId = 171869       ' View: SourcePayableID
                    '        oPayable.TotalPaid = 1000               ' Kommer fra innbetalt bank
                    '        ' Hent ut grunnlaget for OrderPayableItemsSummaries, fra uthenting fra API-et
                    '        ' Dette kan vi hente ut fra oReadOrder, som vi har hentet ut lenger opp i denne kode
                    '        ' Vi må traversere oss gjennom oReadOrder, finne ordren, og finne aktuelle Payable
                    '        ' Hent ut ReadOrderpayables
                    '        ' som har ID, TotalPaid, etc, samt ReadOrderPayableItems
                    '        ' ReadOrderPayableItems har ID, Amount, TotalPaid
                    '        ' Lag en oOrderPayableItemsToReconcile object, som er er collection bestående av ID og TotalPaid
                    '        ' <<<oPayable.OrderPayableItemsToReconcile= 

                    '        OrderToReconcile.OrderPayablesToReconcile.Add(oPayable)
                    '    End If
                    'Loop

                    oResponse = oClient.PutAsJsonAsync("orders/Orders", OrderToReconcile).Result
                    'oResponse = Await oClient.PutAsJsonAsync("orders/Orders", OrderToReconcile)

                    If oResponse.IsSuccessStatusCode Then
                        Console.WriteLine("Ordre oppdatert")
                    Else
                        Console.WriteLine("Feil: " + Str(oResponse.StatusCode))
                    End If

                End If
            End If

            oReadOrder = Nothing
            oResponse = Nothing
            OrderToReconcile = Nothing
            oOrderPayableToReconcile = Nothing
            oOrderPayableItemToReconcile = Nothing
            'Console.WriteLine("{0}\t${1}\t{2}", oReadOrder.Id, oReadOrder.TotalAmount, oReadOrder.StateDescription)


        Catch ex As Exception
            MsgBox(ex.Message & vbCrLf & vbCrLf & sFixedErrorText)

        End Try
    End Function
    Private Function InitiateClient(Token As String, sLocalBaseAddress As String) As Boolean


        ' DENNE TROR JEG IKKE BRUKES TIL NOE NYTTIG LENGER ???
        'Pålogging fra Patentstyret
        'sLocalBaseAddress = "http://ws.test.patentstyret.no/"
        'Pålogging fra VB lokaler
        'sLocalBaseAddress = "http://217.144.244.51/"

        'using (var client = new HttpClient())
        'oClient = New HttpClient
        'oClient.BaseAddress = New Uri(sLocalBaseAddress)
        'oClient.SetBearerToken(Token)
        'oClient.DefaultRequestHeaders.Accept.Clear()
        'oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))

        InitiateClient = True

    End Function
    Async Function CallService(Token As String) As Task(Of Integer)
        'static async Task CallService(string token)
        'Dim sBaseAddress As String
        'Dim oClient As HttpClient
        'Dim oResponse As HttpResponseMessage

        Try

            '//var baseAddress =  "http://ws.test.patentstyret.no/";
            'sBaseAddress = "http://217.144.244.51/"

            'sBaseAddress = "http://ws.test.patentstyret.no/"

            'using (var client = new HttpClient())
            'oClient = New HttpClient
            'oClient.BaseAddress = New Uri(sClientBaseAddress)
            'oClient.SetBearerToken(Token)
            'oClient.DefaultRequestHeaders.Accept.Clear()
            'oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))

            'HTTP GET -- Henter ut en ordre fra API'et og fyller objekt av typen ReadOrder
            'HttpResponseMessage response = await client.GetAsync("orders/orders/1996");

            Dim oresponse As HttpResponseMessage = Await oClient.GetAsync("orders/orders/1875")

            If oresponse.IsSuccessStatusCode Then

                'ReadOrder readOrder = await response.Content.ReadAsAsync<ReadOrder>()
                Dim oReadOrder As Orders.Contracts.ReadOrder
                oReadOrder = Await oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)()


                Console.WriteLine("{0}\t${1}\t{2}", oReadOrder.Id, oReadOrder.TotalAmount, oReadOrder.StateDescription)
            Else

                MsgBox("Feil: " & Str(oresponse.StatusCode))
            End If

            '//HTTP POST -- Oppretter en ny ordre.
            'var payables = new Collection<long>();

            Dim bCreateOrder As Boolean
            bCreateOrder = True
            If bCreateOrder Then
                Dim payables = New Collection(Of Long)
                'payables.Add(171869)    ' View: PayableId
                'payables.Add(171852)
                payables.Add(205528)

                Dim newOrder = New OrderToPlace()

                newOrder.PayableIds = payables
                newOrder.PaymentMethodCode = 0 '//0 = Banktransfer, 4 = BabelBank
                'newOrder.UserEmail = "HarIngen@eMail.no"                ' Hent eMail fra kommentar i BB

                ' /////////////////////////////////////////////////////////////////
                ' Legg inn ny ordre
                oresponse = Await oClient.PostAsJsonAsync("orders/Orders", newOrder)
                If oresponse.IsSuccessStatusCode Then
                    Console.WriteLine("Ny Ordre lagt inn")
                Else
                    MsgBox("Feil: " & Str(oresponse.StatusCode))
                End If
                Dim OoReadOrder = New Orders.Contracts.ReadOrder
                OoReadOrder = Await oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)()
                Dim lID As Long
                lID = OoReadOrder.Id        ' Bruk lID videre når vi skal oppdatere data på den ny ordren !!!!!!!!
            End If

            Dim bPostOrder As Boolean
            bPostOrder = True
            If bPostOrder Then
                '///////////////////////////////////////////////////////////////////
                ' Oppdatere ordre, basert på data fra BabelBank
                Dim OrderToReconcile = New Orders.Contracts.OrderToReconcile
                Dim oPayable As Orders.Contracts.OrderPayableToReconcile   'OrderPayableToUpdateSummary

                oPayable = New OrderPayableToReconcile  'OrderPayableToUpdateSummary
                oPayable.Id = 2040                      ' View: OrderPayableId
                oPayable.SourcePayableId = 171869       ' View: SourcePayableID
                oPayable.TotalPaid = 1000               ' Kommer fra innbetalt bank
                ' Hent ut grunnlaget for OrderPayableItemsSummaries, fra uthenting fra API-et
                ' Dette kan vi hente ut fra oReadOrder, som vi har hentet ut lenger opp i denne kode
                ' Vi må traversere oss gjennom oReadOrder, finne ordren, og finne aktuelle Payable
                ' Hent ut ReadOrderpayables
                ' som har ID, TotalPaid, etc, samt ReadOrderPayableItems
                ' ReadOrderPayableItems har ID, Amount, TotalPaid
                ' Lag en oOrderPayableItemsToReconcile object, som er er collection bestående av ID og TotalPaid
                ' <<<oPayable.OrderPayableItemsToReconcile= 

                OrderToReconcile.OrderPayablesToReconcile.Add(oPayable)

                oPayable = New OrderPayableToReconcile
                oPayable.Id = 2039
                oPayable.SourcePayableId = 171852
                oPayable.TotalPaid = 2500               ' Kommer fra innbetalt bank
                OrderToReconcile.OrderPayablesToReconcile.Add(oPayable)

                'OrderToReconcile.OrderPayableToUpdateSummaries = payables
                OrderToReconcile.Id = 1996    ' View: OrderID ex 1996
                OrderToReconcile.AcceptedDevience = 0      ' over/underbetaling på en ordre, avvik mellom TotalPaid og Amount
                OrderToReconcile.PaymentFee = 0            ' bankgebyr
                OrderToReconcile.Reimbursement = 0         ' tilbakebealing
                OrderToReconcile.Rounding = 0      ' avrunding
                OrderToReconcile.PaidDate = "20150618"             ' betalt dato
                OrderToReconcile.TotalPaid = 3500             ' betalt beløp
                OrderToReconcile.PayeeCremul = "Jan-Petter"                 ' betalers navn, endres til PayeeCremul
                OrderToReconcile.PayeeBankAccount = "12345678901"      ' betalers bankkonto
                OrderToReconcile.StateCode = 2            ' 2 = Betalt med korrekt ordrebeløp, innenfor AcceptedDeviance 3 = betalt med beløpsavvik.
                ' Vi må sjekke at TotalPaid er lik Amount, innenfor Roundings og AcceptedDevience
                OrderToReconcile.BabelBankId = 1234         ' unik id fra BabelBank
                OrderToReconcile.BabelBankIdDate = "18.06.2015"       ' dato for postering
                ' Kan være med:
                OrderToReconcile.ExternalComment = "External comment"       ' max 255
                OrderToReconcile.InternalComment = "Internal comment"

                oresponse = Await oClient.PutAsJsonAsync("orders/Orders", OrderToReconcile)

                If oresponse.IsSuccessStatusCode Then
                    Console.WriteLine("Ny Ordre oppdatert")
                Else
                    Console.WriteLine("Feil: " + oresponse.StatusCode)
                End If
                Dim OoReadOrder = New Orders.Contracts.ReadOrder
                OoReadOrder = Await oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)()
                Dim lID As Long
                lID = OoReadOrder.Id


            End If



            '///////////////////////////////////////////////////////////////////
            ' Legg til kommentarer på en ordre
            ' External Internal comment
            Dim OrderCommentUpdate = New OrderHeader()
            OrderCommentUpdate.Id = 2163
            OrderCommentUpdate.ExternalComment = "Externaltext"
            OrderCommentUpdate.InternalComment = "InternalText"

            oresponse = Await oClient.PutAsJsonAsync("orders/Orders/header", OrderCommentUpdate)
            If oresponse.IsSuccessStatusCode Then
                Console.WriteLine("Ordre kommentar oppdatert")
            Else
                MsgBox("Feil: " & Str(oresponse.StatusCode))
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Function
    Private Function StringToDate(ByRef sString As String) As Date

        StringToDate = CDate(DateSerial(CInt(Left(sString, 4)), CInt(Mid(sString, 5, 2)), CInt(Right(sString, 2))))

    End Function
End Class
