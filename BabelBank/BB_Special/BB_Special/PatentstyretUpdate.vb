﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Linq
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Text
Imports System.Threading.Tasks
Imports Newtonsoft.Json.Linq
Imports Orders.Contracts
Imports Thinktecture.IdentityModel.Client
Imports Thinktecture.IdentityModel.Extensions
Imports System.Threading

Public Class PatentstyretUpdate
    Private aMyOrders(,) As String
    '0 - OrderID
    '1 - Payee
    '2 - TotalPaid
    '3 - PaidDate
    '4 - TransId
    '5 - PayeeBankAccount
    ' - TransIdDate? Dagens dato?
    ' - TotalFee
    ' - ReImbursement
    ' - Rounding
    ' - AcceptedDeviance
    Private aMyPayables(,) As String
    '0 - OrdreId
    '1 - PayablesId?
    '2 - TotalPaid     
    Private sLoginAddress = "https://login-test.patentstyret.no/core"
    Private sLoginUser As String = ""
    Dim sLoginPassWord As String = ""
    Private AuthorizeEndpoint = sLoginAddress + "/connect/authorize"
    Private LogoutEndpoint = sLoginAddress + "/connect/endsession"
    Private TokenEndpoint = sLoginAddress + "/connect/token"
    Private UserInfoEndpoint = sLoginAddress + "/connect/userinfo"
    Private IdentityTokenValidationEndpoint = sLoginAddress + "/connect/identitytokenvalidation"
    Private TokenRevocationEndpoint = sLoginAddress + "/connect/revocation"
    Private sFixedErrorText As String
    Private sOrderRef As String
    ' Ny 25.08.2015 - flyttet fra RequestToken
    Private oClient As HttpClient
    Private Token As TokenResponse
    Dim aArticles As Object(,)
    Private nCardFee As Double = 0

    'Private sClientBaseAddress = "https://217.144.244.51/"   ' Fra VBs lokaler - NEI, etter endring av Hosts file, kan vi bruke samme begge steder, se linje under
    Private sClientBaseAddress As String '= "https://ws-test.patentstyret.no/"   'fra Patentstyrets lokaler
    Public Sub New()
        ''''Token = RequestToken()
    End Sub
    Public Sub RunRequestToken()
        Token = RequestToken()
    End Sub
    Public Property CardFee As Double
        Get
            CardFee = nCardFee
        End Get
        Set(ByVal value As Double)
            nCardFee = value
        End Set

    End Property
    Public Property FixedErrorText As String
        Get
            FixedErrorText = sFixedErrorText
        End Get
        Set(ByVal value As String)
            sFixedErrorText = value
        End Set

    End Property
    Public Property LoginAddress As String
        Get
            LoginAddress = sLoginAddress
        End Get
        Set(ByVal value As String)
            sLoginAddress = value

            ' reset variables based on sLoginAddress
            AuthorizeEndpoint = sLoginAddress + "/connect/authorize"
            LogoutEndpoint = sLoginAddress + "/connect/endsession"
            TokenEndpoint = sLoginAddress + "/connect/token"
            UserInfoEndpoint = sLoginAddress + "/connect/userinfo"
            IdentityTokenValidationEndpoint = sLoginAddress + "/connect/identitytokenvalidation"
            TokenRevocationEndpoint = sLoginAddress + "/connect/revocation"
        End Set

    End Property
    Public Property ClientBaseAddress As String
        Get
            ClientBaseAddress = sClientBaseAddress
        End Get
        Set(ByVal value As String)
            sClientBaseAddress = value
        End Set

    End Property
    Public Property LoginUser As String
        Get
            LoginUser = sLoginUser
        End Get
        Set(ByVal value As String)
            sLoginUser = value
        End Set

    End Property
    Public Property LoginPassWord As String
        Get
            LoginPassWord = sLoginPassWord
        End Get
        Set(ByVal value As String)
            sLoginPassWord = value
        End Set

    End Property
    Public Property PassOrders As String(,)
        Get
            PassOrders = aMyOrders
        End Get
        Set(ByVal value As String(,))
            aMyOrders = value
        End Set
    End Property
    Public Property PassPayables As String(,)
        Get
            PassPayables = aMyPayables
        End Get
        Set(value As String(,))
            aMyPayables = value
        End Set
    End Property
    Public Property ReturnedOrderRef As String
        Get
            ReturnedOrderRef = sOrderRef
        End Get
        Set(value As String)
            sOrderRef = value
        End Set
    End Property
    Public Property PassArticles As Object(,)
        Get
            PassArticles = aArticles
        End Get
        Set(value As Object(,))
            aArticles = value
        End Set
    End Property
    Sub Pause(ByRef iSeconds As Double)
        Dim nStart As Double

        ' wait for n seconds
        nStart = Microsoft.VisualBasic.Timer() ' Set start time.
        Do While Microsoft.VisualBasic.Timer() < nStart + iSeconds
            'System.Windows.Form.Application.DoEvents() ' Yield to other processes.
        Loop

    End Sub
    Public Function DoTheUpdate() As Boolean
        ' ----------------------------------------------------
        ' Update an order or Payable
        ' Depends upon values set in oMyOrders and oMyPayables
        ' ----------------------------------------------------
        Dim bRetValue As Boolean = True
        Dim iCounter As Integer
        Dim sErrorString As String = ""
        Dim bPaymentAlreadyPaid As Boolean = False

        Try
            '//////////////
            ' / her må vi gjøre selve jobben
            ' / kall på oppdateringsrutiner under her, men må først analysere innhold i de arrayer vi har
            ' / for å se hva vi skal gjløre

            sErrorString = Token.HttpErrorStatusCode
            sOrderRef = ""

            If sErrorString <> "0" Then
                Throw New Exception("Patentstyret - DoTheUpdate - Feil i eksportrutine." & vbCrLf & "RequestToken returnerer " & sErrorString)
            End If

            'First, check if the payment already is updated
            'Added 21.10.2015
            If PaymentAlreadyPaid(Token.AccessToken) Then
                bPaymentAlreadyPaid = True
            End If

            'Secondly, check if we shall remove latepaymentinfo
            If Not bPaymentAlreadyPaid Then
                '21.10.2015 - We may have a small problem here, if there are at least order paid but also 1 new order paid in the payment
                ' and it failed in the previous update after the new order was run through the function below.
                'To be a problem the new order must also have payables where the latPayments (respit) shall be removed.
                'The below function will be run once more and the latePayments wil be removed once more. That may cause an error
                If Not RemoveLatePaymentOnPayables(Token.AccessToken) Then
                    Throw New Exception("Feil i RemoveLatePayment.")
                End If

                'Thirdly, check if we have any new orders to create
                For iCounter = 0 To aMyOrders.GetUpperBound(1)
                    If aMyOrders(0, iCounter) = "NY_ORDRE" Then
                        If Not CreateNewOrder(Token.AccessToken, iCounter) Then
                            Throw New Exception("Feil etter CreateNewOrder.")
                        End If
                    End If
                Next iCounter

            End If
            'Finally, update the order
            For iCounter = 0 To aMyOrders.GetUpperBound(1)
                sErrorString = "UpdateOrderAsPaid, iCounter = " & CStr(iCounter)
                If aMyOrders(13, iCounter) = "True" Then
                    UpdateOrderAsPaid(Token.AccessToken, iCounter, True) 'Update the order
                Else
                    UpdateOrderAsPaid(Token.AccessToken, iCounter, False)
                End If
            Next iCounter

        Catch ex As Exception
            Throw New Exception("Function: PatentstyretUpdate.DoTheUpdate. " & sErrorString & vbCrLf & ex.Message)
            Return False
        End Try

        Return True

    End Function
    Public Function Update_Comments(lOrderId As Long, sExternaltext As String, sInternaltext As String) As Boolean
        Dim oresponse As HttpResponseMessage
        Dim sErrorString As String = ""
        Dim OrderCommentUpdate = New OrderHeader()

        Try

            sErrorString = Token.HttpErrorStatusCode
            If sErrorString <> "0" Then
                Throw New Exception("Feil." & vbCrLf & "RequestToken returnerer " & sErrorString)
            End If

            '///////////////////////////////////////////////////////////////////
            ' Legg til kommentarer på en ordre
            sErrorString = "Legger inn verdier i properties."
            OrderCommentUpdate.Id = lOrderId
            OrderCommentUpdate.ExternalComment = sExternaltext
            OrderCommentUpdate.InternalComment = sInternaltext

            sErrorString = "PutAsJsonAsync orders/Orders/header"
            oresponse = oClient.PutAsJsonAsync("orders/Orders/header", OrderCommentUpdate).Result
            If Not oresponse.IsSuccessStatusCode Then
                'MsgBox("Feil i oppdatering av kommentarer: Feilkode " & Str(oresponse.StatusCode) & " Feilmelding " & oresponse.ReasonPhrase, MsgBoxStyle.Exclamation)
                Dim oValidationError = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                Throw New Exception("Feil i oppdatering av kommentarer." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & " Feilmelding " & oValidationError.Message)
            End If

        Catch ex As Exception
            Throw New Exception("Function: PatentstyretUpdate.UpdateComments. " & sErrorString)
            Return False

        End Try

        Return True

    End Function
    Private Function RequestToken() As TokenResponse
        ' 25.08.2015 - Vi prøver å kjøre kun en RequestToken for å spare tid
        Dim oRequestClient As OAuth2Client
        Try
            oRequestClient = New OAuth2Client(New Uri(TokenEndpoint), "babelbankOrdersApiClient", "secret")
            If sLoginUser = "" Or sLoginPassWord = "" Then
                RequestToken = oRequestClient.RequestResourceOwnerPasswordAsync("jan-petter@visualbanking.net", "ValidPassword02##", "ordersApi").Result
            Else
                RequestToken = oRequestClient.RequestResourceOwnerPasswordAsync(sLoginUser, sLoginPassWord, "ordersApi").Result
            End If
            'RequestToken = oRequestClient.RequestResourceOwnerPasswordAsync("jan-petter@visualbanking.net", "ValidPassword02##", "ordersApi").Result

            '26.08.2015
            ' Then instanciate oClient once and for all here, instead of doing it several places;
            oClient = New HttpClient
            oClient.BaseAddress = New Uri(sClientBaseAddress)
            oClient.SetBearerToken(RequestToken.AccessToken)
            oClient.DefaultRequestHeaders.Accept.Clear()
            oClient.DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))
            '08.09.2016 - Added next 2 lines
            'This code was added when we had an error ergarding problems with the frontend server at Patentstyret
            'This code didn't have any effect, but Patentstyret wanted us to keep the code.
            oClient.DefaultRequestHeaders.Add("ContentType", "application/json")
            oClient.DefaultRequestHeaders.Add("ContentLength", "") '   ccept.Add(New HttpHeaderValueCollection(Of )

        Catch ex As Exception
            Throw New Exception("Function: PatentstyretUpdate.RequestToken. " & vbCrLf & "oClient.LoginAddress = " & sLoginAddress & vbCrLf & "RequestToken.AccessToken = " & RequestToken.AccessToken)

        End Try
    End Function
    Function PaymentAlreadyPaid(Token As String) As Boolean ', ByRef bNewOrderIsCreated As Boolean, ByRef nNewOrderId As Double) As Boolean
        'Function added 21.10.2015
        Dim oresponse As HttpResponseMessage
        Dim oReadOrder = New Orders.Contracts.ReadOrder
        Dim payables = New Collection(Of Long)
        Dim newOrder = New OrderToPlace()
        Dim sErrorString As String = ""
        Dim bReturnValue As Boolean = True

        'Thirdly, check if we have any new orders to be updated
        For iCounter = 0 To aMyOrders.GetUpperBound(1)

            sErrorString = "GetAsync(orders/orders/babelbankid/" & aMyOrders(4, iCounter) & ")"
            'oresponse = New HttpResponseMessage
            'oresponse.Headers.Add("Content-Length", "0")
            'Dim instance As New HttpResponseMessage
            'Dim value As HttpResponseHeaders
            'Dim ContentValues As New HttpResponseMessage
            'Dim newValue
            'value = instance.Headers


            'oresponse.Headers.Add("ContentLength")
            'oClient.DefaultRequestHeaders.Add("Content-Length", "0")
            oresponse = oClient.GetAsync("orders/orders/babelbankid/" & aMyOrders(4, iCounter)).Result
            'value = oresponse.Headers
            'newValue = oresponse.Content

            'sErrorString = "GetAsync(orders/orders/" & aMyOrders(0, iCounter) & ")"
            'oresponse = oClient.GetAsync("orders/orders/" & aMyOrders(0, iCounter)).Result

            If oresponse.IsSuccessStatusCode Then
                oReadOrder = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)().Result
                aMyOrders(13, iCounter) = "False"
                If aMyOrders(0, iCounter) = "NY_ORDRE" Then
                    'bNewOrderIsCreated = True
                    'nNewOrderId = oReadOrder.Id
                    aMyOrders(0, iCounter) = oReadOrder.Id
                    'bReturnValue = False
                Else

                End If
            Else
                'MsgBox("Feil: " & Str(oResponse.StatusCode))
                ' vis melding for test av poster med bet.dato 20160901
                'If aMyOrders(3, iCounter) = "20160901" Then
                'MsgBox("Før Dim oValidationError")
                'End If
                Dim oValidationError = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                'If aMyOrders(3, iCounter) = "20160901" Then
                'MsgBox("Før If oValidationError.ViolationCode = 0 Then")
                'End If
                If oValidationError.ViolationCode = 0 Then
                    'OK, the babelbankid is not updated yet
                    bReturnValue = False
                    'If aMyOrders(3, iCounter) = "20160901" Then
                    'MsgBox("oValidationError.ViolationCode = 0")
                    'End If

                Else
                    Throw New Exception(sErrorString & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & " Feilmelding " & oValidationError.Message)
                End If
            End If

            ''Check if order is open
            'If oReadOrder.StateCode = 1 Then
            '    'It's open, check the amount
            '    If oReadOrder.TotalAmount >= CDbl(aMyOrders(2, iCounter)) / 100 Then

            '    End If
            'End If


        Next iCounter



        'For lArrayCounter = 0 To aMyPayables.GetUpperBound(1)
        '    'Check if it is a payable to be included in the new order
        '    If aMyPayables(3, lArrayCounter) = "NO_CHARGES" Then
        '        sErrorString = "oClient.PutAsJsonAsync(orders/Orders."
        '        oresponse = oClient.PutAsJsonAsync("orders/payables/deleteLatePayment/" & aMyPayables(1, lArrayCounter).Trim, 0).Result

        '        If oresponse.IsSuccessStatusCode Then

        '        Else
        '            'MsgBox("Feil: " & Str(oresponse.StatusCode))
        '            'Throw New Exception("Feil etter PostAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & "Feilmelding " & oresponse.ReasonPhrase)
        '            Dim oValidationError = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
        '            Throw New Exception("Feil etter PostAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & " Feilmelding " & oValidationError.Message)
        '        End If
        '    End If

        'Next lArrayCounter

        Return bReturnValue

    End Function
    Function RemoveLatePaymentOnPayables(Token As String) As Boolean
        Dim lArrayCounter As Long
        Dim oresponse As HttpResponseMessage
        Dim oReadOrder = New Orders.Contracts.ReadOrder
        Dim payables = New Collection(Of Long)
        Dim newOrder = New OrderToPlace()
        Dim sErrorString As String = ""

        For lArrayCounter = 0 To aMyPayables.GetUpperBound(1)
            'Check if it is a payable to be included in the new order
            If aMyPayables(3, lArrayCounter) = "NO_CHARGES" Then
                sErrorString = "oClient.PutAsJsonAsync(orders/Orders."
                oresponse = oClient.PutAsJsonAsync("orders/payables/deleteLatePayment/" & aMyPayables(1, lArrayCounter).Trim, 0).Result
                'oresponse = oClient.PutAsJsonAsync("orders/payables/eteLatePayment/" & aMyPayables(1, lArrayCounter).Trim, 0).Result
                'oresponse = oClient.PutAsync("orders/payables/deleteLatePayment/" & aMyPayables(1, lArrayCounter).Trim, ).Result

                If oresponse.IsSuccessStatusCode Then

                Else
                    'MsgBox("Feil: " & Str(oresponse.StatusCode))
                    'Throw New Exception("Feil etter PostAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & "Feilmelding " & oresponse.ReasonPhrase)
                    Dim oValidationError = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                    Throw New Exception("Feil etter PostAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & " Feilmelding " & oValidationError.Message)
                End If
            End If

        Next lArrayCounter

        Return True

    End Function
    Function CreateNewOrder(Token As String, iArrayOrderCounter As Integer) As Boolean
        Dim bCreateOrder As Boolean
        Dim lArrayCounter As Long
        Dim oresponse As HttpResponseMessage
        Dim sId As String
        Dim oReadOrder = New Orders.Contracts.ReadOrder
        Dim payables = New Collection(Of Long)
        Dim newOrder = New OrderToPlace()
        Dim bReturnvalue As Boolean = False
        Dim sErrorString As String = ""
        Dim lSantCustomerID As Long = 0

        Try

            bCreateOrder = True
            If bCreateOrder Then
                sErrorString = "Fyller opp object med Payables."
                For lArrayCounter = 0 To aMyPayables.GetUpperBound(1)
                    'Check if it is a payable to be included in the new order
                    If aMyPayables(0, lArrayCounter) = "NY_ORDRE" Then
                        payables.Add(CLng(aMyPayables(1, lArrayCounter)))
                    End If

                Next lArrayCounter

                newOrder.PayableIds = payables
                newOrder.PaymentMethodCode = 0 '//0 = Banktransfer, 4 = BabelBank
                'OrderToReconcile.SantCustomerId = CLng(aMyOrders(12, iArrayOrderCounter))

                '19.11.2015 - Added next IF. The Sant CustomerID will always(?) be stored on the first entry in the OrderArray (from
                '   the first line in GridMatched).

                If CLng(aMyOrders(12, iArrayOrderCounter)) = 0 Then
                    lSantCustomerID = CLng(aMyOrders(12, 0))
                Else
                    lSantCustomerID = CLng(aMyOrders(12, iArrayOrderCounter))
                End If

                newOrder.SantCustomerId = lSantCustomerID
                ' /////////////////////////////////////////////////////////////////
                ' Legg inn ny ordre
                sErrorString = "PostAsJsonAsync orders/Orders"
                oresponse = oClient.PostAsJsonAsync("orders/Orders", newOrder).Result

                If oresponse.IsSuccessStatusCode Then
                    'Console.WriteLine("Ny Ordre lagt inn")
                Else
                    'MsgBox("Feil: " & Str(oresponse.StatusCode))
                    'Throw New Exception("Feil etter PostAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & "Feilmelding " & oresponse.ReasonPhrase)
                    Dim oValidationError = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                    Throw New Exception("Feil etter PostAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oresponse.StatusCode) & " Feilmelding " & oValidationError.Message)
                End If

                sErrorString = "ReadAsAsync"
                oReadOrder = oresponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)().Result
                sId = CStr(oReadOrder.Id)        ' Bruk lID videre når vi skal oppdatere data på den ny ordren !!!!!!!!

                ' Added 25.08.2015
                ' KJELL - er det denne vi er ute etter, som skal fylles opp i oInvoice.CustomerNo
                sOrderRef = oReadOrder.RefNr

                For lArrayCounter = 0 To aMyOrders.GetUpperBound(1)
                    'Check if it is a payable to be included in the new order
                    If aMyOrders(0, lArrayCounter) = "NY_ORDRE" Then
                        aMyOrders(0, lArrayCounter) = sId
                    End If
                Next lArrayCounter

                For lArrayCounter = 0 To aMyPayables.GetUpperBound(1)
                    'Check if it is a payable to be included in the new order
                    If aMyPayables(0, lArrayCounter) = "NY_ORDRE" Then
                        aMyPayables(0, lArrayCounter) = sId
                    End If
                Next lArrayCounter

                newOrder = Nothing
                oresponse = Nothing
                oReadOrder = Nothing 'Should probably keep this, to reuse it in the function UpdateOrderAsPaid

                bReturnvalue = True

            End If

        Catch ex As Exception
            'newOrder = Nothing
            'oresponse = Nothing
            If Not oReadOrder Is Nothing Then
                oReadOrder = Nothing 'Should probably keep this, to reuse it in the function UpdateOrderAsPaid
            End If
            bReturnvalue = False
            MsgBox("Patentstyret.Update.CreateNewOrder" & vbCrLf & ex.Message & vbCrLf & vbCrLf & sFixedErrorText & vbCrLf & sErrorString)
        End Try

        Return bReturnvalue
    End Function
    Function UpdateOrderAsPaid(Token As String, iArrayOrderCounter As Integer, bUpdateTheOrder As Boolean) As Boolean
        'Async Function UpdateOrderAsPaid(Token As String, iArrayOrderCounter As Integer) As Task(Of Boolean)
        Dim oReadOrder As Orders.Contracts.ReadOrder
        Dim OrderToReconcile = New Orders.Contracts.OrderToReconcile
        Dim OrderToReconcilePaidByBaOrCard As New Orders.Contracts.OrderToReconcilePaidByBaOrCard
        Dim oReadOrderPayable As Orders.Contracts.ReadOrderPayable
        Dim oOrderPayableToReconcile As Orders.Contracts.OrderPayableToReconcile
        Dim oReadOrderPayableItem As Orders.Contracts.ReadOrderPayableItem
        Dim oOrderPayableItemToReconcile As Orders.Contracts.OrderPayableItemToReconcile
        Dim oOrderPayableItemsToReconcile As Collection(Of Orders.Contracts.OrderPayableItemToReconcile)
        Dim oOrderPayablesToReconcile As Collection(Of Orders.Contracts.OrderPayableToReconcile)
        Dim oResponse As HttpResponseMessage
        Dim nTotalDeviation As Double = 0
        Dim bFirstPayable As Boolean = True
        Dim bFirstPayableItem As Boolean = True
        Dim bContinue As Boolean = False
        Dim iArrayPayableCounter As Integer
        Dim sErrorstring As String = ""
        Dim bReturnvalue As Boolean = False
        Dim iArrElement As Integer
        Dim bFound As Boolean

        Try

            'HTTP GET -- Henter ut en ordre fra API'et og fyller objekt av typen ReadOrder
            sErrorstring = "GetAsync(orders/orders/" & aMyOrders(0, iArrayOrderCounter) & ")"
            oResponse = oClient.GetAsync("orders/orders/" & aMyOrders(0, iArrayOrderCounter)).Result
            'oResponse = oClient.GetAsync("orders/orders/24816").Result

            If oResponse.IsSuccessStatusCode Then
                oReadOrder = oResponse.Content.ReadAsAsync(Of Orders.Contracts.ReadOrder)().Result
            Else
                'MsgBox("Feil: " & Str(oResponse.StatusCode))
                Dim oValidationError = oResponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                Throw New Exception(sErrorstring & vbCrLf & "Feilkode " & Str(oResponse.StatusCode) & " Feilmelding " & oValidationError.Message)
            End If

            'Check if order is open
            If oReadOrder.StateCode = 1 Or Not bUpdateTheOrder Then 'Added 21.10.2015
                'It's open, check the amount
                If oReadOrder.TotalAmount >= CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 Or Not bUpdateTheOrder Then 'Added 21.10.2015
                    '////////////////////////
                    '/ OK, update the order /
                    '////////////////////////

                    sErrorstring = "Fyller opp properties i OrderToReconcile."
                    OrderToReconcile.Id = oReadOrder.Id    ' View: OrderID ex 1996
                    'OrderToReconcile.AcceptedDevience = CDbl(aMyOrders(?, iArrayOrderCounter))/100      ' over/underbetaling på en ordre, avvik mellom TotalPaid og Amount
                    'OrderToReconcile.PaymentFee = CDbl(aMyOrders(?, iArrayOrderCounter))/100            ' bankgebyr
                    'OrderToReconcile.Reimbursement = CDbl(aMyOrders(?, iArrayOrderCounter))/100         ' tilbakebealing
                    'OrderToReconcile.Rounding = CDbl(aMyOrders(?, iArrayOrderCounter))/100      ' avrunding
                    ' Datoformat: 2015-06-24
                    OrderToReconcile.PaymentReceived = Left(aMyOrders(3, iArrayOrderCounter), 4) & "-" & Mid(aMyOrders(3, iArrayOrderCounter), 5, 2) & "-" & Right(aMyOrders(3, iArrayOrderCounter), 2)              ' betalt dato
                    nTotalDeviation = 0
                    nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(6, iArrayOrderCounter))
                    nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(7, iArrayOrderCounter))
                    nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(8, iArrayOrderCounter))
                    'nTotalDeviation = nTotalDeviation + CDbl(aMyOrders(9, iArrayOrderCounter))
                    nTotalDeviation = nTotalDeviation / 100
                    OrderToReconcile.TotalPaid = CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 + nTotalDeviation 'oReadOrder.TotalAmount             ' betalt beløp
                    OrderToReconcile.PayeeCremulName = aMyOrders(1, iArrayOrderCounter)                ' betalers navn, endres til PayeeCremul 09.09.0215 - Changed to PayeeCremulName
                    ' 09.09.2015 added Address
                    OrderToReconcile.PayeeCremulAddress = aMyOrders(11, iArrayOrderCounter)
                    OrderToReconcile.PayeeBankAccount = aMyOrders(5, iArrayOrderCounter)      ' betalers bankkonto
                    ' Må sette SantCustomerID, fra samme verdi som det som lå der fra før 
                    'OrderToReconcile.SantCustomerId = oReadOrder.SantCustomerID  <----- Err ennå ikke klar, eller blir ikke klar?
                    ' FOR TEST, kjør med dummyID
                    'OrderToReconcile.SantCustomerId = 101000
                    If oReadOrder.TotalAmount > CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 And aMyOrders(10, iArrayOrderCounter) = "ORDERPAYABLE" Then
                        OrderToReconcile.StateCode = 3
                    Else
                        OrderToReconcile.StateCode = 2           ' 2 = Betalt med korrekt ordrebeløp, innenfor AcceptedDeviance 3 = betalt med beløpsavvik.
                    End If
                    ' Vi må sjekke at TotalPaid er lik Amount, innenfor Roundings og AcceptedDevience
                    OrderToReconcile.BabelBankId = aMyOrders(4, iArrayOrderCounter)         ' unik id fra BabelBank
                    OrderToReconcile.BabelBankIdDate = Format(Date.Today, "yyyy-MM-dd")       ' dato for postering FORMAT ???
                    'OrderToReconcile.TransIdDate = "2015-06-24"
                    '///////// 10.09.2015 - Etter avtale med PS: Bruker ikke PaymentFee lenger /////////////////
                    'OrderToReconcile.   .PaymentFee = CDbl(aMyOrders(6, iArrayOrderCounter)) / -100

                    OrderToReconcile.Rounding = CDbl(aMyOrders(7, iArrayOrderCounter)) / -100
                    OrderToReconcile.AcceptedDevience = CDbl(aMyOrders(8, iArrayOrderCounter)) / -100
                    OrderToReconcile.Reimbursement = CDbl(aMyOrders(9, iArrayOrderCounter)) / -100

                    sErrorstring = "Fyller opp properties i OrderPayablesToReconcile."
                    oOrderPayablesToReconcile = New Collection(Of Orders.Contracts.OrderPayableToReconcile)
                    For Each oReadOrderPayable In oReadOrder.ReadOrderPayables

                        bContinue = False
                        If aMyOrders(10, iArrayOrderCounter) = "ORDERPAYABLE" Then
                            For iArrayPayableCounter = 0 To aMyPayables.GetUpperBound(1)
                                If oReadOrderPayable.Id = CLng(aMyPayables(1, iArrayPayableCounter)) Then
                                    bContinue = True
                                    Exit For
                                End If
                            Next iArrayPayableCounter
                        Else
                            bContinue = True
                        End If

                        If bContinue Then
                            oOrderPayableToReconcile = New OrderPayableToReconcile  'OrderPayableToUpdateSummary

                            oOrderPayableToReconcile.Id = oReadOrderPayable.Id
                            oOrderPayableToReconcile.SourcePayableId = oReadOrderPayable.SourcePayableId
                            If bFirstPayable Then
                                bFirstPayable = False
                                oOrderPayableToReconcile.TotalPaid = oReadOrderPayable.TotalAmount '08.10.2015 removed this after discussion in plenum + nTotalDeviation
                            Else
                                oOrderPayableToReconcile.TotalPaid = oReadOrderPayable.TotalAmount
                            End If

                            sErrorstring = "Fyller opp properties i OrderPayableItemsToReconcile."
                            oOrderPayableItemsToReconcile = New Collection(Of Orders.Contracts.OrderPayableItemToReconcile)
                            For Each oReadOrderPayableItem In oReadOrderPayable.ReadOrderPayableItems
                                oOrderPayableItemToReconcile = New Orders.Contracts.OrderPayableItemToReconcile

                                oOrderPayableItemToReconcile.Id = oReadOrderPayableItem.Id

                                ' array med feedescription (0), amount (1)


                                bFound = False
                                If aArticles Is Nothing Then
                                    bFound = False
                                Else
                                    For iArrElement = 0 To aArticles.GetUpperBound(1)
                                        If aArticles(0, iArrElement) = oReadOrderPayableItem.FeeCode Then
                                            ' add amount to this element (this feecode)
                                            aArticles(1, iArrElement) = aArticles(1, iArrElement) + CDbl(oReadOrderPayableItem.Amount)
                                            bFound = True
                                            Exit For
                                        End If
                                    Next
                                End If

                                If Not bFound Then
                                    ' add new element
                                    If aArticles Is Nothing Then
                                        ' first element
                                        ReDim aArticles(1, 0)
                                    Else
                                        ReDim Preserve aArticles(1, aArticles.GetUpperBound(1) + 1)
                                    End If
                                    aArticles(0, iArrElement) = oReadOrderPayableItem.FeeCode
                                    aArticles(1, iArrElement) = CDbl(oReadOrderPayableItem.Amount)
                                End If

                                If bFirstPayableItem Then
                                    bFirstPayableItem = False
                                    oOrderPayableItemToReconcile.TotalPaid = oReadOrderPayableItem.Amount '08.10.2015 removed this after discussion in plenum + nTotalDeviation
                                Else
                                    oOrderPayableItemToReconcile.TotalPaid = oReadOrderPayableItem.Amount
                                End If

                                oOrderPayableItemsToReconcile.Add(oOrderPayableItemToReconcile)

                            Next
                            oOrderPayableToReconcile.OrderPayableItemsToReconcile = oOrderPayableItemsToReconcile
                            'oOrderPayableToReconcile.OrderPayableItemsToReconcile = oReadOrderPayable.ReadOrderPayableItems
                            oOrderPayablesToReconcile.Add(oOrderPayableToReconcile)
                        End If
                    Next oReadOrderPayable
                    OrderToReconcile.OrderPayablesToReconcile = oOrderPayablesToReconcile

                    If bUpdateTheOrder Then 'Added 21.10.2015
                        sErrorstring = "oClient.PutAsJsonAsync(orders/Orders."
                        oResponse = oClient.PutAsJsonAsync("orders/Orders/BT", OrderToReconcile).Result

                        If oResponse.IsSuccessStatusCode Then
                            'Console.WriteLine("Ordre oppdatert")
                        Else
                            'Console.WriteLine("Feil: " + Str(oResponse.StatusCode))
                            'Throw New Exception("Feil etter PutAsJsonAsync orders/Orders." & vbCrLf & "Feilmelding " & Str(oResponse.StatusCode) & " Feilmelding " & oResponse.ReasonPhrase)
                            Dim oValidationError = oResponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                            Throw New Exception("Feil etter PutAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oResponse.StatusCode) & " Feilmelding " & oValidationError.Message)
                        End If
                    End If

                Else
                    'TODO - NBNBNBNBNBNBNBNBNBNB
                    'Skal vi gi feilmelding her.
                End If
            ElseIf oReadOrder.StateCode = 5 Then
                '13.05.2016 - Added update of paymentscard
                'Statecode = 5
                'If this is the scenario then just update the 'orderheader' not every OrderPayable
                If oReadOrder.TotalPaid = CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 Then 'Added 21.10.2015
                    '////////////////////////
                    '/ OK, update the order /
                    '////////////////////////

                    sErrorstring = "Statecode = 5: Fyller opp properties i OrderToReconcile."
                    OrderToReconcilePaidByBaOrCard.Id = oReadOrder.Id    ' View: OrderID ex 1996
                    ' Datoformat: 2015-06-24
                    OrderToReconcilePaidByBaOrCard.PaymentReceived = Left(aMyOrders(3, iArrayOrderCounter), 4) & "-" & Mid(aMyOrders(3, iArrayOrderCounter), 5, 2) & "-" & Right(aMyOrders(3, iArrayOrderCounter), 2)              ' betalt dato
                    'To set the next properties doesn't make any sense
                    'OrderToReconcile.TotalPaid = CDbl(aMyOrders(2, iArrayOrderCounter)) / 100 + nTotalDeviation 'oReadOrder.TotalAmount             ' betalt beløp
                    'OrderToReconcile.PayeeCremulName = aMyOrders(1, iArrayOrderCounter)                ' betalers navn, endres til PayeeCremul 09.09.0215 - Changed to PayeeCremulName
                    'OrderToReconcile.PayeeCremulAddress = aMyOrders(11, iArrayOrderCounter)
                    'OrderToReconcile.PayeeBankAccount = aMyOrders(5, iArrayOrderCounter)      ' betalers bankkonto
                    'The statecode is changed to 2 within the API
                    'OrderToReconcilePaidByBaOrCard. = 2           ' 2 = Betalt med korrekt ordrebeløp, innenfor AcceptedDeviance 3 = betalt med beløpsavvik.
                    OrderToReconcilePaidByBaOrCard.BabelBankId = aMyOrders(4, iArrayOrderCounter)         ' unik id fra BabelBank
                    OrderToReconcilePaidByBaOrCard.BabelBankIdDate = Format(Date.Today, "yyyy-MM-dd")       ' dato for postering FORMAT ???

                    nCardFee = nCardFee + oReadOrder.PaymentFee * 100

                    For Each oReadOrderPayable In oReadOrder.ReadOrderPayables

                        For Each oReadOrderPayableItem In oReadOrderPayable.ReadOrderPayableItems

                            bFound = False
                            If aArticles Is Nothing Then
                                bFound = False
                            Else
                                For iArrElement = 0 To aArticles.GetUpperBound(1)
                                    If aArticles(0, iArrElement) = oReadOrderPayableItem.FeeCode Then
                                        ' add amount to this element (this feecode)
                                        aArticles(1, iArrElement) = aArticles(1, iArrElement) + CDbl(oReadOrderPayableItem.Amount)
                                        bFound = True
                                        Exit For
                                    End If
                                Next
                            End If

                            If Not bFound Then
                                ' add new element
                                If aArticles Is Nothing Then
                                    ' first element
                                    ReDim aArticles(1, 0)
                                Else
                                    ReDim Preserve aArticles(1, aArticles.GetUpperBound(1) + 1)
                                End If
                                aArticles(0, iArrElement) = oReadOrderPayableItem.FeeCode
                                aArticles(1, iArrElement) = CDbl(oReadOrderPayableItem.Amount)
                            End If

                        Next
                    Next oReadOrderPayable
                    'OrderToReconcile.OrderPayablesToReconcile = oOrderPayablesToReconcile

                    If bUpdateTheOrder Then 'Added 21.10.2015
                        sErrorstring = "Statecode = 5: oClient.PutAsJsonAsync(orders/Orders."
                        oResponse = oClient.PutAsJsonAsync("orders/Orders/BAcard", OrderToReconcilePaidByBaOrCard).Result

                        If oResponse.IsSuccessStatusCode Then
                            'Console.WriteLine("Ordre oppdatert")
                        Else
                            'Console.WriteLine("Feil: " + Str(oResponse.StatusCode))
                            'Throw New Exception("Feil etter PutAsJsonAsync orders/Orders." & vbCrLf & "Feilmelding " & Str(oResponse.StatusCode) & " Feilmelding " & oResponse.ReasonPhrase)
                            Dim oValidationError = oResponse.Content.ReadAsAsync(Of Orders.Contracts.ValidationError)().Result
                            Throw New Exception("Feil etter PutAsJsonAsync orders/Orders." & vbCrLf & "Feilkode " & Str(oResponse.StatusCode) & " Feilmelding " & oValidationError.Message)
                        End If
                    End If

                Else
                    Throw New Exception("UpdateOrderAsPaid - kortinnbetalinger" & vbCrLf & "Innbetalt beløp på ordre stemmer ikke overens med betalt beløp på orderen." & vbCrLf & "Ordrenr: " & oReadOrder.RefNr & vbCrLf & "Beløp ordre: " & CDbl(oReadOrder.TotalPaid).ToString & "Beløp betalt: " & aMyOrders(2, iArrayOrderCounter))
                End If
            Else
                'TODO - NBNBNBNBNBNBNBNBNBNB
                'Skal vi gi feilmelding her.
            End If

            oReadOrder = Nothing
            oResponse = Nothing
            OrderToReconcile = Nothing
            oOrderPayableToReconcile = Nothing
            oOrderPayableItemToReconcile = Nothing
            'Console.WriteLine("{0}\t${1}\t{2}", oReadOrder.Id, oReadOrder.TotalAmount, oReadOrder.StateDescription)
            bReturnvalue = True

        Catch ex As Exception
            If oReadOrder Is Nothing Then
                Throw New Exception("Patentstyret.Update.UpdateOrdersAsPaid." & vbCrLf & ex.Message & vbCrLf & vbCrLf & sFixedErrorText & vbCrLf & sErrorstring)
            Else
                Throw New Exception("Patentstyret.Update.UpdateOrdersAsPaid." & vbCrLf & ex.Message & vbCrLf & vbCrLf & sFixedErrorText & vbCrLf & sErrorstring & vbCrLf & "Ordre fra API: " & oReadOrder.Id.ToString & "-" & oReadOrder.TotalAmount.ToString & "-" & oReadOrder.StateDescription)
            End If
            bReturnvalue = False
        End Try
        Return bReturnvalue
    End Function
    Private Function StringToDate(ByRef sString As String) As Date
        StringToDate = CDate(DateSerial(CInt(Left(sString, 4)), CInt(Mid(sString, 5, 2)), CInt(Right(sString, 2))))
    End Function
End Class
