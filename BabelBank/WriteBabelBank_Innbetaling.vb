Option Strict Off
Option Explicit On
'----
Module WriteBabelBank_Innbetaling
    Dim sBatchNo As String = ""

    'XNET - 17.01.2013 - Whole function
    Function WriteBabelBank_InnbetalingFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sClientNo As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal bPostAgainstObservationAccount As Boolean) As Boolean
        '11.05.2015 - Added the use of bPostAgainstObservationAccount
        'Comments marked KIKO is changed 31.08.2007 to start using invoiceyear in the new version of Movex
        '16.09.2008 - Elkem has still not started to use the new version, so now KIKIO are
        ' the code to be used when the new version is ready

        '23.10.2008 - Added sSpecial = "MARITECH"
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bBackUp As Boolean
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment, oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim iFreetextCounter As Integer, sFreetextFixed As String, sFreeTextVariable As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sLineToWrite As String
        Dim bFirstBatch As Boolean, nSequenceNoTotal As Double
        Dim bGLRecordWritten As Boolean
        Dim sVoucherNo As String
        Dim sVoucherNoToPass As String
        Dim sAlphaPart As String
        Dim nDigitsLen As Integer
        Dim sGLAccount As String
        Dim sGLResultAccount As String = ""
        Dim sOldAccountNo As String, sTempClientNo As String
        Dim bAccountFound As Boolean
        '22.02.2010 - the use of bAtLeastOnePaymentExported added
        Dim bAtLeastOnePaymentExported As Boolean
        Dim bThisIsElkem As Boolean
        Dim bThisIsElkemM3 As Boolean
        Dim bExportoInvoice As Boolean
        Dim bAllInvoicesExported As Boolean
        Dim sOriginalFile As String
        Dim nBankAmount As Double
        Dim sBankAmount As String

        Dim sNoMatchAccount As String
        Dim iNoMatchAccountType As Integer

        Dim lCounter As Long
        Dim lArrayCounter As Long
        Dim aAccountArray(,) As String
        Dim aARAmountArray() As Double
        Dim aGLAmountArray() As Double
        Dim sDim1 As String = ""
        Dim sTmp As String

        Try

            ' lag en outputfil
            bAppendFile = False
            bGLRecordWritten = False
            bFirstBatch = True
            sOldAccountNo = vbNullString
            sTempClientNo = vbNullString
            bAtLeastOnePaymentExported = False

            'New code 11.05.2015
            If Not oBabelFiles.VB_Profile Is Nothing Then
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)
                'Create an array equal to the client-array
                ReDim Preserve aARAmountArray(UBound(aAccountArray, 2))
                ReDim Preserve aGLAmountArray(UBound(aAccountArray, 2))
            End If

            'Doesn't matter if a file exist. If so, append

            If sDivision = "M3" Then
                bThisIsElkem = True
                bThisIsElkemM3 = True
            ElseIf sDivision = "MOVEX" Then
                bThisIsElkem = True
                bThisIsElkemM3 = False
            Else
                bThisIsElkem = False
                bThisIsElkemM3 = False
            End If

            'XNET - 04.02.2013 - added next IF
            If bThisIsElkem Then
                oBackup = New vbBabel.BabelFileHandling
                sOriginalFile = sFilenameOut
                sFilenameOut = Mid$(sOriginalFile, InStrRev(sOriginalFile, "\") + 1)
                ' If $ in filename, replace with clientno
                sFilenameOut = oBabelFiles.VB_Profile.BackupPath & "\" & sFilenameOut
                If oFs.FileExists(sFilenameOut) Then
                    oFs.DeleteFile(sFilenameOut)
                End If
                'oFs.MoveFile sGLFile, sNewGLFile

            End If

            'New 28.12.2005 for Elkem
            'Add clientNo on all payments when the passed 'exportaccount' is empty
            If EmptyString(sI_Account) Then
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.I_Account <> sOldAccountNo Then
                                bAccountFound = False
                                For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If oPayment.I_Account = oaccount.Account Then
                                                sTempClientNo = oClient.ClientNo
                                                sOldAccountNo = oPayment.I_Account
                                                bAccountFound = True
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then Exit For
                                    Next oClient
                                    If bAccountFound Then Exit For
                                Next oFilesetup
                            End If 'If oPayment.I_Account <> sOldAccountNo Then
                            If bAccountFound Then
                                oPayment.VB_ClientNo = sTempClientNo
                                For lCounter = 0 To UBound(aAccountArray, 2)
                                    If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                        lArrayCounter = lCounter
                                        Exit For
                                    End If
                                Next lCounter

                            Else
                                Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                '12003: Could not find account %1.Please enter the account in setup."
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabel
            End If

            ' 04.12.2018
            ' For ActiveBrands - Adyen and PayPal -
            ' set date = last payment date for ALL payments;
            ' 09.07.2019 Added same functionality for COAST_TROMSO
            If sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Or sSpecial = "COAST_TROMSO" Then
                ActiveBrandsResetPaymentDateForPayPalAndAdyen(oBabelFiles)
            End If
            If sSpecial = "ACTIVEBRANDS" Then
                ActiveBrandsResetPaymentDateForPayPalAndAdyen(oBabelFiles)
                ' 26.08.2019
                ' Check for Adyen payments. If Adyen, control if we have received payments for all brands for all currencies
                ' If not, warn, by raising a message.
                ' 09.09.2019 - Test in another way - do it when opening Adyen XML files (check for BalanceTransfer)
                'ActiveBrandsAdyenControlBrandsAndCurrencies(oBabelFiles)
            End If

            sOldAccountNo = vbNullString

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'Check for omitted payments
                        If oPayment.StatusCode <> "-1" Then
                            '22.02.2010 - New code to check the exported mark
                            If Not oPayment.Exported Then
                                ' 03.10.2019 - added next If
                                ' just pass on for NHST
                                If sSpecial = "NHST_INNBETALINGER" Then
                                    bExportoBabel = True
                                    Exit For
                                End If
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    'New 28.12.2005 for Elkem
                                    If EmptyString(sI_Account) Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoBabel = True
                                                End If
                                            Else
                                                bExportoBabel = True
                                            End If
                                            ' added 02.10.2015
                                        ElseIf sSpecial = "PELAGIA" Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            If bThisIsElkem And bExportoBabel Then
                                bExportoBabel = False
                                If Not bThisIsElkemM3 Then
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_Matched = False Then
                                                bExportoBabel = True
                                            Else
                                                If oInvoice.MyField = "MO" Then
                                                    bExportoBabel = True
                                                End If
                                            End If
                                        End If
                                        If bExportoBabel Then Exit For
                                    Next oInvoice
                                Else
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_Matched Then
                                                If oInvoice.MyField = "M3" Then
                                                    bExportoBabel = True
                                                End If
                                            End If
                                        End If
                                        If bExportoBabel Then Exit For
                                    Next oInvoice
                                End If
                            End If

                            ' 20.04.2017 added test for NHST, GLOBAL
                            'If sSpecial = "NHST_INNBETALINGER" And bExportoBabel Then
                            '    bExportoBabel = False
                            '    For Each oInvoice In oPayment.Invoices
                            '        If oInvoice.MATCH_Final Then
                            '            ' 20.04.2017 - change this next one, and more further down
                            '            If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" Then
                            '                'If sDivision = oInvoice.MyField2 Then  ' added 03.05.2017
                            '                '    ' ikke n�dvendig � splitte mer enn p� klient?
                            '                bExportoBabel = True
                            '                'End If
                            '            End If
                            '        End If
                            '        If bExportoBabel Then Exit For
                            '    Next oInvoice
                            'End If

                            If bExportoBabel Then
                                Exit For
                            End If
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'Check for omitted payments
                            If oPayment.StatusCode <> "-1" Then
                                '22.02.2010 - New code to check the exported mark
                                If Not oPayment.Exported Then
                                    ' 03.10.2019 - added next If
                                    ' just pass on for NHST
                                    If sSpecial = "NHST_INNBETALINGER" Then
                                        bExportoBatch = True
                                        Exit For
                                    End If

                                    'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        'New 28.12.2005 for Elkem
                                        If EmptyString(sI_Account) Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoBatch = True
                                                    End If
                                                Else
                                                    bExportoBatch = True
                                                End If
                                                ' added 02.10.2015
                                                ' 03.10.2019 removed this test, no need
                                            ElseIf sSpecial = "PELAGIA" Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            If bThisIsElkem And bExportoBatch Then
                                bExportoBatch = False
                                If Not bThisIsElkemM3 Then
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_Matched = False Then
                                                bExportoBatch = True
                                            Else
                                                If oInvoice.MyField = "MO" Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                        If bExportoBatch Then Exit For
                                    Next oInvoice
                                Else
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_Matched Then
                                                If oInvoice.MyField = "M3" Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                        If bExportoBatch Then Exit For
                                    Next oInvoice
                                End If
                            End If
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            If sSpecial = "ACTIVEBRANDS_KLARNA" Then
                                ' do not change bGLRecordWritten
                            Else
                                bGLRecordWritten = False
                            End If
                            j = 0
                            sBankAmount = ""

                            If bThisIsElkem Then
                                nBankAmount = 0
                                For Each oPayment In oBatch.Payments
                                    If Not bThisIsElkemM3 Then
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MATCH_Matched = False Then
                                                    nBankAmount = nBankAmount + oInvoice.MON_InvoiceAmount
                                                Else
                                                    If oInvoice.MyField = "MO" Then
                                                        nBankAmount = nBankAmount + oInvoice.MON_InvoiceAmount
                                                    End If
                                                End If
                                            End If
                                        Next oInvoice
                                    Else
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MATCH_Matched Then
                                                    If oInvoice.MyField = "M3" Then
                                                        nBankAmount = nBankAmount + oInvoice.MON_InvoiceAmount
                                                    End If
                                                End If
                                            End If
                                        Next oInvoice
                                    End If
                                Next oPayment
                                sBankAmount = Trim$(Str(nBankAmount))
                            Else
                                sBankAmount = ""
                            End If

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False

                                If sSpecial = "BUTIKKDRIFT_TELLER" Then  ' Ny If 13.12.2017
                                    ' for "BUTIKKDRIFT_TELLER", we have a situation where we can have 2 different dates inside  a batch.
                                    ' this leads to unbalanced batches in NAV.
                                    ' we need to use a date from Batch, from Tellers <ActualSettlementDate>
                                    oPayment.DATE_Payment = oBatch.DATE_Production
                                    oPayment.DATE_Value = oBatch.DATE_Production
                                End If

                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'Check for omitted payments
                                If oPayment.StatusCode <> "-1" Then
                                    '22.02.2010 - New code to check the exported mark
                                    If Not oPayment.Exported Then
                                        ' 03.10.2019 - added next If
                                        ' just pass on for NHST
                                        If sSpecial = "NHST_INNBETALINGER" Then
                                            bExportoPayment = True
                                        End If

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            'New 28.12.2005 for Elkem
                                            If EmptyString(sI_Account) Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                                        If oPayment.VB_ClientNo = sClientNo Then
                                                            bExportoPayment = True
                                                        End If
                                                    Else
                                                        bExportoPayment = True
                                                    End If
                                                    ' added 02.10.2015
                                                    ' 03.10.2019 Removed test, no need
                                                ElseIf sSpecial = "PELAGIA" Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                If bThisIsElkem And bExportoPayment Then
                                    bExportoPayment = False
                                    If Not bThisIsElkemM3 Then
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MATCH_Matched = False Then
                                                    bExportoPayment = True
                                                Else
                                                    If oInvoice.MyField = "MO" Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                            If bExportoPayment Then Exit For
                                        Next oInvoice
                                    Else
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MATCH_Matched Then
                                                    If oInvoice.MyField = "M3" Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                            If bExportoPayment Then Exit For
                                        Next oInvoice
                                    End If
                                End If

                                'New 18.07.2007 - Don't export unmatched items that is zero-amount
                                If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
                                    If oPayment.MON_InvoiceAmount = 0 Then
                                        bExportoPayment = False
                                        oPayment.Exported = True
                                    End If
                                End If

                                If bExportoPayment Then

                                    ' 24.10.2018 - 
                                    ' Active Brands needs 5 digits in VoucherNo - Like B04563 - and 0 is missing when we have 4 digits
                                    If sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
                                        If oPayment.VoucherNo.Length = 5 And Left(oPayment.VoucherNo, 1) = "B" Then
                                            oPayment.VoucherNo = Left(oPayment.VoucherNo, 1) & "0" & Mid(oPayment.VoucherNo, 2)
                                        End If
                                        ' 20.01.2021 - moved this one before "W"
                                        ' 23.10.2019 - problems with bilagsno like WU2, WU123, needs WU00002, WU00123, etc
                                        If oPayment.VoucherNo.Length < 6 And Left(oPayment.VoucherNo, 2) = "WU" Then
                                            oPayment.VoucherNo = "WU" & Right("00000" & Mid(oPayment.VoucherNo, 3), 5)
                                            ' 01.12.2020 - W01234 for Adyen NO
                                            ' 20.01.2021 change to ElseIf
                                        ElseIf oPayment.VoucherNo.Length < 6 And Left(oPayment.VoucherNo, 1) = "W" Then
                                            oPayment.VoucherNo = "W" & Right("00000" & Mid(oPayment.VoucherNo, 2), 5)
                                        End If


                                        ' 04.10.2019
                                        ' Active Brands will use Bookdate, instead of ValueDate, for all records
                                        ' Easist solution is to do it here:
                                        'WAIT oPayment.DATE_Value = oPayment.DATE_Payment
                                    End If
                                    bAtLeastOnePaymentExported = True
                                    'Find the client
                                    If oPayment.I_Account <> sOldAccountNo Then
                                        If oPayment.VB_ProfileInUse Then
                                            'sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If oPayment.I_Account = oaccount.Account Then
                                                            sGLAccount = Trim$(oaccount.GLAccount)
                                                            sGLResultAccount = Trim(oaccount.GLResultsAccount)
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True

                                                            sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                            iNoMatchAccountType = GetNoMatchAccountType(oBabel.VB_Profile.Company_ID, oClient.Client_ID)

                                                            sDim1 = oaccount.Dim1.Trim

                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If
                                    End If

                                    If Not bAccountFound Then
                                        Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                        '12003: Could not find account %1.Please enter the account in setup."
                                    End If

                                    ' added 18.03.2015
                                    ' 16.09.2019 - If payers name = ADYEN, then we are on ordinary matching profile for Adyen.
                                    ' 02.10.2019 - if e_name = Nothing, then bumbs
                                    If EmptyString(oPayment.E_Name) Then
                                        oPayment.E_Name = ""
                                    End If
                                    If sSpecial = "NHST_INNBETALINGER" Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or (sSpecial = "ACTIVEBRANDS_ADYEN" And (InStr(oPayment.E_Name.ToUpper, "ADYEN") = 0 And InStr(oPayment.E_Name.ToUpper, "WELLS FARGO BANK") = 0)) Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
                                        ' no bankrecord for Infosoft at NHST;
                                        ' and for ActiveBrands_PayPal/Adyen we need to sum up pr valuta

                                        If Not bGLRecordWritten And (sSpecial = "ACTIVEBRANDS_KLARNA" Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN") Then
                                            sVoucherNoToPass = oPayment.VoucherNo
                                        End If
                                        bGLRecordWritten = True

                                    End If

                                    '11.05.2015 - Added next IF
                                    If bPostAgainstObservationAccount Then

                                        If iNoMatchAccountType = BabelFiles.MatchType.MatchedOnGL Then
                                            sLineToWrite = WriteGLRecord(oPayment, oPayment.Invoices.Item(1), oClient, oPayment.Unique_PaymentID, sSpecial, sDivision, sNoMatchAccount, oPayment.VoucherNo)
                                            oFile.WriteLine(sLineToWrite)
                                        Else
                                            sLineToWrite = WriteCustomerRecord(oPayment, oPayment.Invoices.Item(1), oClient, oPayment.Unique_PaymentID, sSpecial, sDivision, sNoMatchAccount, oPayment.VoucherNo, sGLResultAccount)
                                            oFile.WriteLine(sLineToWrite)
                                        End If

                                    Else

                                        ' 13.06.2016 Special for Salmar, and DNB_Giek financing
                                        If sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
                                            ' do not write normal bankrecord for DNB/GIEK
                                            bGLRecordWritten = True
                                        End If

                                        'Write GL-line if not previously written
                                        If Not bGLRecordWritten Then
                                            If bThisIsElkem Then
                                                If bThisIsElkemM3 Then
                                                    sVoucherNoToPass = Trim(xDelim(oPayment.VoucherNo, "-", 1))
                                                Else
                                                    sVoucherNoToPass = Trim(xDelim(oPayment.VoucherNo, "-", 2))
                                                End If
                                            Else
                                                sVoucherNoToPass = ""
                                            End If
                                            If sSpecial = "ACTIVEBRANDS_VIPPS" Or sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "BUTIKKDRIFT_TELLER" Then
                                                ' Vipps for ActiveBrands: M� f�re etter gebyr. Nettobel�p ligger i MON_TransferredAmount
                                                sBankAmount = oBatch.MON_TransferredAmount.ToString
                                            End If
                                            '03.12.2019 - her skal vi ikke ha ut en Bank-record for den spesielle Adyen-profilen hvor vi
                                            '             leser direkte inn XML-filer fra Adyen (ikke g�r vegen om Cremulfiler og bytter ut)
                                            If Not (sSpecial = "ACTIVEBRANDS_ADYEN" And oPayment.I_Account = "ADYEN") Then
                                                sLineToWrite = WriteGLBankRecord(oBatch, oPayment, oClient, sGLAccount, sSpecial, sDivision, sBankAmount, sVoucherNoToPass)
                                                oFile.WriteLine(sLineToWrite)
                                            Else
                                                ' 03.12.2019 - find batchno for directimport of Adyen XML-files
                                                ' Find batch no from BabelFile name;
                                                sTmp = oBabel.FilenameIn
                                                If InStr(sTmp.ToUpper, "BATCH") > 0 Then
                                                    sTmp = Mid(sTmp, InStr(sTmp.ToUpper, "BATCH") + 5, 4)  ' 2 - 4 digits
                                                    ' but take only the numeric part, before the (1) as batch number
                                                    sTmp = Trim(KeepNumericsOnly(sTmp))
                                                    sBatchNo = sTmp
                                                End If
                                            End If

                                            ' 16.10.2017 - For ActiveBrands Vipps, or Teller, add a charges record
                                            If sSpecial = "ACTIVEBRANDS_VIPPS" Or sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "BUTIKKDRIFT_TELLER" Then
                                                sLineToWrite = WriteGLSpecialRecord(oBatch, oPayment, oClient, sDim1, sSpecial, sDivision, (oBatch.MON_InvoiceAmount - oBatch.MON_TransferredAmount).ToString, sVoucherNoToPass)
                                                oFile.WriteLine(sLineToWrite)
                                            End If
                                            bGLRecordWritten = True
                                        End If

                                    End If

                                    'First we have to find the freetext to the payment-lines.
                                    iFreetextCounter = 0
                                    sFreetextFixed = vbNullString
                                    For Each oInvoice In oPayment.Invoices


                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreetext In oInvoice.Freetexts
                                                iFreetextCounter = iFreetextCounter + 1
                                                If iFreetextCounter = 1 Then
                                                    sFreetextFixed = RTrim$(oFreetext.Text)
                                                ElseIf iFreetextCounter = 2 Then
                                                    sFreetextFixed = sFreetextFixed & " " & RTrim$(oFreetext.Text)
                                                Else
                                                    Exit For
                                                End If
                                            Next oFreetext
                                        End If
                                    Next oInvoice
                                    sFreetextFixed = Trim$(oPayment.E_Name) & " " & sFreetextFixed

                                    For Each oInvoice In oPayment.Invoices

                                        If bThisIsElkem Then
                                            bExportoInvoice = False
                                            If Not bThisIsElkemM3 Then
                                                If oInvoice.MATCH_Final Then
                                                    If oInvoice.MATCH_Matched = False Then
                                                        bExportoInvoice = True
                                                    Else
                                                        If oInvoice.MyField = "MO" Then
                                                            bExportoInvoice = True
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If oInvoice.MATCH_Final Then
                                                    If oInvoice.MATCH_Matched Then
                                                        If oInvoice.MyField = "M3" Then
                                                            bExportoInvoice = True
                                                        End If
                                                    End If
                                                End If
                                            End If

                                        Else
                                            bExportoInvoice = True
                                        End If

                                        ' added 18.03.2015 to have the correct invoices to NHST
                                        If sSpecial = "NHST_INNBETALINGER" Then
                                            bExportoInvoice = False
                                            If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" Then
                                                'If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" And oPayment.VB_ClientNo = sClientNo Then
                                                ' 03.10.2019 . there can be different clients in one payment! Like both TW and UP in same!!!!!
                                                ' Must test on MyField
                                                Dim sCode As String = ""
                                                Dim sInfosoftClient As String = ""
                                                sCode = xDelim(oInvoice.MyField, "|", 2)
                                                ' set payments clientno based on companycode in MyField (companycode is fetched from Infosoft)
                                                Select Case sCode
                                                    ' 21.10.2019 - Added DN
                                                    Case "DN"
                                                        sInfosoftClient = "F14002"
                                                    Case "TW"
                                                        sInfosoftClient = "F14003"
                                                    Case "UP"
                                                        sInfosoftClient = "F14006"
                                                        ' 21.10.2019 - Added EN, EP
                                                    Case "EN", "EP"
                                                        sInfosoftClient = "F14009"
                                                    Case "IFCO", "IFNO", "FFI", "FNI", "SFP", "SID"
                                                        sInfosoftClient = "F14015"
                                                    Case "RE"
                                                        sInfosoftClient = "F14032"
                                                    Case Else
                                                        ' rest to F14015
                                                        sInfosoftClient = "F14015"
                                                End Select
                                                If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" And sInfosoftClient = sClientNo Then
                                                    bExportoInvoice = True
                                                Else
                                                    bExportoInvoice = False
                                                End If
                                            End If
                                        End If

                                        'sFreetextFixed = ""
                                        If oInvoice.MATCH_Final = True And bExportoInvoice Then
                                            '01.12.2011
                                            'Add the variable part of the freetext
                                            sFreeTextVariable = sFreetextFixed
                                            ' XOKNET 05.05.2011 Added next If for TOBULL
                                            If sSpecial = "TOBULL" Then
                                                sFreeTextVariable = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier = 4 Then
                                                        sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                    End If
                                                Next oFreetext
                                                If Len(sFreeTextVariable) = 0 Then
                                                    sFreeTextVariable = "Innbetalinger uten KID"
                                                End If
                                            ElseIf sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
                                                ' 13.11.2017 - ActiveBrands: KUN tekst som er punchet i manuell, gridMatched, skal i eksportfilen
                                                sFreeTextVariable = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier = 4 Then
                                                        sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                    Else
                                                        ' 12.10.2018 - OR for Vipps, take last part of freetext, which is a referenceno
                                                        If oPayment.E_Name = "VBB AS" Then
                                                            sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                        End If
                                                    End If
                                                Next oFreetext
                                            ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
                                                '16.11.2017 Pick Authorization ref only as freetext, always 4. element in freetexts + add Avdelingsno
                                                sFreeTextVariable = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Index = 4 Then
                                                        sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                    End If
                                                Next
                                                ' preceed with BatchNum and department
                                                ' 21.12.2017 - not room for auth.ref  removed last part of line below
                                                sFreeTextVariable = oPayment.REF_Bank1 & " " & oClient.ClientNo '& sFreeTextVariable
                                            ElseIf sSpecial = "PELAGIA" Then
                                                ' 04.10.2018 added special texting for Pelagia
                                                sFreeTextVariable = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier = 4 Then
                                                        sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                    End If
                                                Next oFreetext
                                                If Len(sFreeTextVariable) = 0 Then
                                                    sFreeTextVariable = "Payment"
                                                End If

                                            Else
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier > 2 Then
                                                        If sSpecial = "ELKEM" Then
                                                            If Left(oFreetext.Text, 9) = "Fakturaen" And Right(oFreetext.Text, 13) = "er delbetalt!" Then
                                                                sFreeTextVariable = sFreeTextVariable
                                                            Else
                                                                sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                            End If
                                                        ElseIf sSpecial = "COAST_TROMSO" Then
                                                            sFreeTextVariable = RTrim$(oFreetext.Text)
                                                        Else
                                                            sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                        End If
                                                    End If
                                                Next oFreetext
                                            End If

                                            If oInvoice.MATCH_Matched Then

                                                ' 12.09.2018 -
                                                ' added use of supplier, by setting BBRET_MyField3 = 'Supplier'
                                                ' First time used for Pelagia
                                                If oInvoice.MyField3.ToUpper = "SUPPLIER" Then
                                                    oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier
                                                End If

                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    sLineToWrite = WriteCustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass, sGLResultAccount)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                    '11.05.2015 - Add to totalamountexported
                                                    aGLAmountArray(lArrayCounter) = aGLAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount

                                                    '15.03.2012 - XOKNET - Added next 2 IF's
                                                    ' XNET 31.10.2013 added "COAST"
                                                    If sSpecial = "MARITECH" Or sSpecial = "COAST" Or sSpecial = "COAST_TROMSO" Then
                                                        If IsEqualAmount(oInvoice.MON_InvoiceAmount, 0) Then
                                                            'Do not export
                                                        Else
                                                            sLineToWrite = WriteGLRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass)
                                                        End If
                                                    Else
                                                        If sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
                                                            sLineToWrite = WriteCustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, "SALMAR_R", sDivision, sGLAccount, sVoucherNoToPass, sGLResultAccount)
                                                            oFile.WriteLine(sLineToWrite)
                                                        End If
                                                        sLineToWrite = WriteGLRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass)
                                                    End If
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier Then
                                                    sLineToWrite = WriteSupplierRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass)
                                                Else
                                                    '11.05.2015 - Add to totalamountexported
                                                    aARAmountArray(lArrayCounter) = aARAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                    If sSpecial = "SALMAR" And oInvoice.MyField2 = "DNB" Then
                                                        '- if Salmar og DNB, skriv en ny SalmarDNBCustomerRecord, med "R" og Debetbel�p -
                                                        ' Write "R" line, for DNB reskontro
                                                        sLineToWrite = WriteCustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, "SALMAR_R", sDivision, sGLAccount, sVoucherNoToPass, sGLResultAccount)
                                                        oFile.WriteLine(sLineToWrite)
                                                    End If
                                                    sLineToWrite = WriteCustomerRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass, sGLResultAccount)
                                                End If
                                            Else
                                                If sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
                                                    ' For Salmar, DNB/GIEK, Write an R-record, instead of BankGl Record
                                                    sLineToWrite = WriteNotMatchedRecord(oPayment, oInvoice, oClient, sFreeTextVariable, "SALMAR_R", sDivision, sGLAccount, sVoucherNoToPass)
                                                    oFile.WriteLine(sLineToWrite)
                                                End If

                                                ' 13.10.2017 added next If
                                                If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "ACTIVEBRANDS_VIPPS" Then
                                                    sLineToWrite = WriteNotMatchedRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLResultAccount, sVoucherNoToPass)
                                                Else
                                                    sLineToWrite = WriteNotMatchedRecord(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass)
                                                End If
                                            End If 'If oInvoice.MATCH_Matched Then
                                            oFile.WriteLine(sLineToWrite)
                                            oInvoice.Exported = True

                                        End If 'oInvoice.MATCH_Final = True
                                    Next oInvoice


                                    If bThisIsElkem Then
                                        bAllInvoicesExported = True
                                        'Check if all invoices are exported
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If Not oInvoice.Exported Then
                                                    bAllInvoicesExported = False
                                                    Exit For
                                                End If
                                            End If
                                        Next oInvoice
                                        If bAllInvoicesExported Then
                                            oPayment.Exported = True
                                        End If
                                    Else
                                        ' 03.10.2019 added NHST
                                        If Not sSpecial = "NHST_INNBETALINGER" Then
                                            oPayment.Exported = True
                                        End If
                                    End If
                                End If 'bExportoPayment
                            Next ' payment


                        End If

                        ' 05.02.2020 added next line
                        If bExportoBatch Then
                            ' 22.11.2018:
                            If sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
                                ' needs one record pr currency to GL 1565
                                Do
                                    sLineToWrite = WriteGLSpecialActiveBrandsPayPal_1565_Record(oBatch, oClient, sVoucherNoToPass)
                                    If sLineToWrite <> "" Then
                                        oFile.WriteLine(sLineToWrite)
                                    Else
                                        ' all currencies written, get out
                                        Exit Do
                                    End If
                                Loop

                                ' s� lager vi linjer pr valuta for gebyrer
                                ' 03.12.2019 - samme for Adyen-spesialprofil for innlesing av Adyen XML
                                If sSpecial = "ACTIVEBRANDS_PAYPAL" Or (sSpecial = "ACTIVEBRANDS_ADYEN" And oPayment.I_Account = "ADYEN") Then
                                    Do
                                        sLineToWrite = WriteGLSpecialActiveBrandsPayPal_7781_Record(oBatch, oClient, sVoucherNoToPass)
                                        If sLineToWrite <> "" Then
                                            oFile.WriteLine(sLineToWrite)
                                        Else
                                            ' all currencies written, get out
                                            Exit Do
                                        End If
                                    Loop
                                End If

                            End If
                            ' 02.10.2019 - if e_name = Nothing, then bumbs
                            If EmptyString(oPayment.E_Name) Then
                                oPayment.E_Name = ""
                            End If
                            ' 26.11.2018:
                            If (sSpecial = "ACTIVEBRANDS_ADYEN" And (InStr(oPayment.E_Name.ToUpper, "ADYEN") = 0 And InStr(oPayment.E_Name.ToUpper, "WELLS FARGO BANK") = 0)) Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
                                ' One record with WEBGebyr pr currency to
                                Do
                                    sLineToWrite = WriteGLSpecialActiveBrandsPayPal_7781_Record(oBatch, oClient, sVoucherNoToPass)
                                    If sLineToWrite <> "" Then
                                        oFile.WriteLine(sLineToWrite)
                                    Else
                                        ' all currencies written, get out
                                        Exit Do
                                    End If
                                Loop

                            End If
                        End If  'If bExportoBatch Then -  added 05.02.2020

                        'We have to save the last used sequenceno. in some cases
                        'First check if we use a profile
                        If oBatch.VB_ProfileInUse = True Then
                            Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                'Always use the seqno at filesetup level
                                Case 1
                                    'Only for the first batch if we have one sequenceseries!
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                    oBatch.VB_Profile.Status = 2
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                Case 2
                                    'oBatch.VB_Profile.Status = 2
                                    'oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                    'oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                    sVoucherNo = sAlphaPart & PadLeft(Trim$(Str(nSequenceNoTotal)), nDigitsLen, "0")
                                    ' XNET 13.06.2013 added next if
                                    If Not oClient Is Nothing Then
                                        oClient.VoucherNo = sVoucherNo
                                        oClient.Status = Profile.CollectionStatus.Changed
                                        oBatch.VB_Profile.Status = Profile.CollectionStatus.ChangeUnder
                                        oBatch.VB_Profile.FileSetups(iFormat_ID).Status = Profile.CollectionStatus.ChangeUnder
                                    End If

                                Case Else
                                    'Err.Raise 12002, "WriteNavision_for_SI_Data", LRS(12002, "Navision", sFilenameOut)
                                    ' XNET 17.01.2013 rettet feil i tekst
                                    Err.Raise(12002, "WriteBabelBank_InnbetalingFile", LRS(12002, "", sFilenameOut))

                            End Select

                        End If

                    Next 'batch


                End If

            Next

            If bAtLeastOnePaymentExported Then
                oFile.Close()
                oFile = Nothing
                'XNET - 04.02.2012 -
                If bThisIsElkem Then
                    'sOriginalFile = sFilenameOut
                    'sFilenameOut = Left$(sGLFile, InStr(sGLFile, ".") - 1) & "_" & sBuntNummer & Mid$(sGLFile, InStr(sGLFile, "."))
                    ' If $ in filename, replace with clientno
                    'oFs.MoveFile sGLFile, sNewGLFile

                    oBackup.BackupPath = oBabelFiles.VB_Profile.BackupPath
                    oBackup.SourceFile = sFilenameOut
                    oBackup.DeleteOriginalFile = False
                    oBackup.FilesetupID = oFilesetup.FileSetup_ID
                    oBackup.InOut = "O"
                    'oBackup.FilenameNo = oFilesetup.FileNameOut1
                    oBackup.Client = vbNullString
                    oBackup.TheFilenameHasACounter = False

                    bBackUp = oBackup.CreateBackup()

                    oBackup = Nothing
                    'Move outputfil to original filenname passed to the function

                    oFs.CopyFile(sFilenameOut, sOriginalFile, True)
                    oFs.DeleteFile(sFilenameOut)

                End If
                If oBabelFiles.Count > 0 Then
                    If oBabelFiles(1).VB_ProfileInUse Then
                        If oBabelFiles(1).VB_Profile.ManMatchFromERP Then
                            For lCounter = 0 To UBound(aAccountArray, 2)
                                dbUpDayTotals(False, DateToString(Now), aARAmountArray(lCounter), "MATCHED_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                                dbUpDayTotals(False, DateToString(Now), aGLAmountArray(lCounter), "MATCHED_GL", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                            Next lCounter
                        End If
                    End If
                End If
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile(sFilenameOut)
            End If

            ' 08.03.2018 - ActiveBrands for NAV2018 needs to remove last CRLF from file;
            ' 07.09.2018 - test if an exportfile has been produced!!!
            ' 10.01.2018 . do this for ActiveBrands only - AND test that we have access to the filer
            If Left(sSpecial, 12) = "ACTIVEBRANDS" Then
                If oFs.FileExists(sFilenameOut) Then
                    RemoveLastCRLFInFile(sFilenameOut)
                End If
            End If

            oFs = Nothing

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBabelBank_InnbetalingFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBabelBank_InnbetalingFile = True

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteGLBankRecord(ByVal oBatch As vbBabel.Batch, ByVal oPayment As vbBabel.Payment, ByVal oClient As vbBabel.Client, ByVal sGLAccount As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sBankAmount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim nExchRate As Double
        Dim nAmountToUse As Double 'Added 19.07.2017 

        sLineToWrite = ""
        ' 19.11.2018 - for special temporary PayPal solution for ActiveBrands
        If sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_KLARNA" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            sLineToWrite = "F;" 'Finanskonto 1565, ikke bank
        Else
            sLineToWrite = "B;" 'Posttype
        End If
        sLineToWrite = sLineToWrite & " ;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        ' XNET 31.10.2013 added "COAST"
        ' 08.03.2018 added sSpecial = "ACTIVEBRANDS" for NAV2018
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
            sLineToWrite = sLineToWrite & PadRight("", 8, " ") & ";" 'Client
            'XNET - 25.02.2013 - Added next IF
        ElseIf sSpecial = "ELKEM" Then
            If sDivision = "M3" Then
                sLineToWrite = sLineToWrite & PadRight("190", 8, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
            End If
        ElseIf Trim(oClient.ClientNo) = "<>" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf Trim(oClient.ClientNo) = "=" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
            'ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            '   sLineToWrite = sLineToWrite & PadRight("400", 8, " ") & ";"
        Else
            'Normal situation
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        'XNET - 04.02.2013 - Added next IF
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else
            If sSpecial = "BUTIKKDRIFT_TELLER" Then  ' Ny If 13.12.2017
                sDateToUse = oBatch.DATE_Production
            Else
                'Use payment_date
                If Not oPayment.DATE_Payment = "19900101" Then
                    sDateToUse = oPayment.DATE_Payment
                Else
                    sDateToUse = DateToString(Now)
                End If
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else
            'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'GL-account
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        If sBankAmount = "" Then
            '19.07.2017 - Added next loop to deduct omitted payments from the batchamount
            nAmountToUse = oBatch.MON_InvoiceAmount
            For Each oPayment In oBatch.Payments
                'Check for omitted payments
                If oPayment.StatusCode = "-1" Then
                    nAmountToUse = nAmountToUse - oPayment.MON_InvoiceAmount
                End If
            Next oPayment
            'sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oBatch.MON_InvoiceAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
            sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(nAmountToUse), "-", vbNullString)), 23, " ") & ";" 'Amount
            If nAmountToUse < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            'Only for Elkem so far
            sLineToWrite = sLineToWrite & PadLeft(Replace(sBankAmount, "-", vbNullString), 23, " ") & ";" 'Amount
            If Left(sBankAmount, 1) = "-" Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        End If

        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        ' 11.02.2015
        ' For Salmar, AX, we need to specify an weighted, average, terminkurs on the bankposting
        ' Removed 13.02.2015 - but Keep it for a while
        ' iStone will do the averaging instead of us
        'If sSpecial = "SALMAR" Then
        '    ' Calculate average terminkurs
        '    nExchRate = Salmar_CalculateAverageTerminRate(oBatch)
        '    'sTemp = Replace(Trim(Str(oPayment.MON_LocalExchRate)), ",", ".")
        '    'If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
        '    '    sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        '    'Else
        '    '    sTemp = sTemp & ".00000000"
        '    'End If
        '    sLineToWrite = sLineToWrite & PadRight(Format(nExchRate, "##00.00000000"), 15, " ") & ";" ' Exch.rate against basevaluta
        'Else
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        'End If
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        sLineToWrite = sLineToWrite & Space(35) & ";" 'Blank (Payers name)
        ' XNET 29.03.2011 - added If for TOBULL
        If sSpecial = "TOBULL" Then
            sLineToWrite = sLineToWrite & PadRight("Innbetalinger uten KID", 80, " ")
        ElseIf oPayment.PayCode = "670" Then
            sLineToWrite = sLineToWrite & PadRight("Valutatermin", 80, " ")
        ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            ' 12.12.2017 - use name + department as freetext
            sLineToWrite = sLineToWrite & Trim(PadRight(oPayment.E_Name, 22, " "))
        Else
            ' 16.06.2017 - added text "OCR" for OCR-payments
            If IsOCR(oPayment.PayCode) Then
                sLineToWrite = sLineToWrite & "OCR " & PadRight(Trim$(Replace(oBatch.REF_Bank, ";", vbNullString)), 80, " ")
            Else
                sLineToWrite = sLineToWrite & PadRight(Trim$(Replace(oBatch.REF_Bank, ";", vbNullString)), 80, " ")
            End If
        End If
        ' 27.10.2017 - For ActiveBrands - remove blanks from the end of each record
        If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "BUTIKKDRIFT_TELLER" Or sSpecial = "ACTIVEBRANDS_VIPPS" Then
            sLineToWrite = sLineToWrite.Trim
        End If

        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "FONDET FOR REGIONALE" Then
            sLineToWrite = sLineToWrite & ";" & Space(50)
            'XNET 17.01.2013 - Added next ElseIf
        ElseIf sDivision = "M3" Then
            sLineToWrite = sLineToWrite & ";" & PadRight(xDelim(sGLAccount, "-", 1), 50, " ")
        ElseIf sSpecial = "ACTIVEBRANDS" Then
            ' for Lindorff, add an extra column (used for "LINDORFF". )
            sLineToWrite = sLineToWrite & ";"
            ' 14.05.2018 - added a Dimension2 field, 404-413
            sLineToWrite = sLineToWrite & Space(10) & ";"
        End If

        WriteGLBankRecord = sLineToWrite

    End Function
    Private Function WriteGLSpecialRecord(ByVal oBatch As vbBabel.Batch, ByVal oPayment As vbBabel.Payment, ByVal oClient As vbBabel.Client, ByVal sGLAccount As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAmount As String, ByVal sVoucherNo As String) As String
        ' 16.10.2017 New special record added
        ' First time used for ActiveBrands and Vipps charges
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim nExchRate As Double
        Dim nAmountToUse As Double 'Added 19.07.2017 

        sLineToWrite = ""
        sLineToWrite = "F;" 'Posttype
        sLineToWrite = sLineToWrite & "A;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        'If sSpecial = "BUTIKKDRIFT_TELLER" Then
        'sLineToWrite = sLineToWrite & PadRight("400", 8, " ") & ";"
        'Else
        If sSpecial = "ACTIVEBRANDS" Then
            sLineToWrite = sLineToWrite & PadRight("", 8, " ") & ";" 'Client
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        'End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'GL-account
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        If sGLAmount = "" Then
            '19.07.2017 - Added next loop to deduct omitted payments from the batchamount
            nAmountToUse = oBatch.MON_InvoiceAmount
            For Each oPayment In oBatch.Payments
                'Check for omitted payments
                If oPayment.StatusCode = "-1" Then
                    nAmountToUse = nAmountToUse - oPayment.MON_InvoiceAmount
                End If
            Next oPayment
            'sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oBatch.MON_InvoiceAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
            sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(nAmountToUse), "-", vbNullString)), 23, " ") & ";" 'Amount
            If nAmountToUse < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            sLineToWrite = sLineToWrite & PadLeft(Replace(sGLAmount, "-", vbNullString), 23, " ") & ";" 'Amount
            If Left(sGLAmount, 1) = "-" Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        End If

        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        sLineToWrite = sLineToWrite & Space(35) & ";" 'Blank (Payers name)
        If sSpecial = "BUTIKKDRIFT_TELLER" Then
            ' 12.12.2017 - use name + department as freetext
            sLineToWrite = sLineToWrite & Trim(PadRight(oPayment.E_Name, 22, " "))
        Else
            sLineToWrite = sLineToWrite & PadRight(Trim$(Replace(oBatch.REF_Bank, ";", vbNullString)), 80, " ")
        End If
        ' 27.10.2017 - For ActiveBrands - remove blanks from the end of each record
        'If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "ACTIVEBRANDS_VIPPS" Or sSpecial = "BUTIKKDRIFT_TELLER" Then
        ' 14.05.2018 - removed ACTIVEBRANDS_VIPPS, as they are on the new NAV2017 export
        If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "BUTIKKDRIFT_TELLER" Then
            sLineToWrite = sLineToWrite.Trim
        End If

        ' added 14.05.2018 for Dim1 and Dim2 for NAV2017 export;
        If sSpecial = "ACTIVEBRANDS_VIPPS" Then
            ' for Lindorff, add an extra column (used for "LINDORFF". )
            sLineToWrite = sLineToWrite & ";"
            ' 14.05.2018 - added a Dimension2 field, 404-413
            sLineToWrite = sLineToWrite & Space(10) & ";"
        End If

        WriteGLSpecialRecord = sLineToWrite

    End Function
    Private Function WriteGLSpecialActiveBrandsPayPal_1565_Record(ByVal oBatch As vbBabel.Batch, ByVal oClient As vbBabel.Client, ByVal sVoucherNo As String) As String
        ' 16.10.2017 New special record added
        ' First time used for ActiveBrands and Vipps charges
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim nAmountToUse As Double 'Added 19.07.2017 
        Dim sCurrentCurrency As String = ""
        Dim oPayment As Payment = Nothing

        sLineToWrite = ""
        sLineToWrite = "F;" 'Posttype
        sLineToWrite = sLineToWrite & "I;" ' Matchstatus
        sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'CompanyNo

        sLineToWrite = sLineToWrite & Space(8) & ";" 'Client
        sLineToWrite = sLineToWrite & Space(10) & ";" 'VoucherType
        'sLineToWrite = sLineToWrite & PadRight(oBatch.Payments(1).VoucherNo, 20, " ") & ";" 'VoucherNo
        ' 07.01.2019 - changed code from above
        If EmptyString(sVoucherNo) Then
            ' 05.02.2020 - endret, da vi ikke har en oPayment!!!
            'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
            sLineToWrite = sLineToWrite & PadRight(oBatch.Payments(1).VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        sDateToUse = DateToString(Now)
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        sLineToWrite = sLineToWrite & PadRight("1565", 45, " ") & ";" 'GL-account
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        ' traverse through all payments in batch, and total up pr currency.
        ' marked amounts are set as "TOTALED" in opayment.NOTI_NotificationAttention
        nAmountToUse = 0
        sCurrentCurrency = ""
        ' Run through all payments pr currency
        For Each oPayment In oBatch.Payments
            If oPayment.NOTI_NotificationAttention <> "TOTALED" Then
                If sCurrentCurrency = "" Then
                    sCurrentCurrency = oPayment.MON_InvoiceCurrency
                End If
                If oPayment.MON_InvoiceCurrency = sCurrentCurrency Then 'And oPayment.StatusCode <> "-1" Then
                    If oPayment.StatusCode <> "-1" Then
                        nAmountToUse = nAmountToUse + oPayment.MON_TransferredAmount
                    End If
                    oPayment.NOTI_NotificationAttention = "TOTALED"
                End If
            End If

        Next
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(nAmountToUse), "-", vbNullString)), 23, " ") & ";" 'Amount
        If nAmountToUse < 0 Then
            sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
        Else
            sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
        End If
        sLineToWrite = sLineToWrite & sCurrentCurrency & ";" 'Valuta
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        sLineToWrite = sLineToWrite & Space(35) & ";" 'Blank (Payers name)
        sLineToWrite = sLineToWrite & PadRight("PayPal/Klarna" & Trim$(Replace(oBatch.REF_Bank, ";", vbNullString)), 80, " ")

        sLineToWrite = sLineToWrite.Trim
        ' if we have processed all currencies, retur blank line
        If sCurrentCurrency = "" Then
            sLineToWrite = ""
        End If

        WriteGLSpecialActiveBrandsPayPal_1565_Record = sLineToWrite

    End Function
    Private Function WriteGLSpecialActiveBrandsPayPal_7781_Record(ByVal oBatch As vbBabel.Batch, ByVal oClient As vbBabel.Client, ByVal sVoucherNo As String) As String
        ' 16.10.2017 New special record added
        ' PayPal and Adyen WEBGebyr record
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim nAmountToUse As Double 'Added 19.07.2017 
        Dim sCurrentCurrency As String = ""
        Dim oPayment As Payment = Nothing

        sLineToWrite = ""
        sLineToWrite = "F;" 'Posttype
        sLineToWrite = sLineToWrite & "I;" ' Matchstatus
        sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'CompanyNo

        sLineToWrite = sLineToWrite & Space(8) & ";" 'Client
        sLineToWrite = sLineToWrite & Space(10) & ";" 'VoucherType
        'sLineToWrite = sLineToWrite & PadRight(oBatch.Payments(1).VoucherNo, 20, " ") & ";" 'VoucherNo
        ' 07.01.2019 - changed code from above
        If EmptyString(sVoucherNo) Then
            ' 05.02.2020 - endret, da vi ikke har en oPayment!!!
            'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
            sLineToWrite = sLineToWrite & PadRight(oBatch.Payments(1).VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If

        sDateToUse = ""
        sDateToUse = DateToString(Now)
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        If oBatch.VB_Profile.CompanyName = "Active Brands US" Or oBatch.VB_Profile.CompanyName = "ECom USA" Then
            sLineToWrite = sLineToWrite & PadRight("80401", 45, " ") & ";" 'GL-account  'USA
        Else
            sLineToWrite = sLineToWrite & PadRight("7781", 45, " ") & ";" 'GL-account
        End If
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        ' traverse through all payments in batch, and total up pr currency.
        ' marked amounts are set as "NETTO" in opayment.NOTI_NotificationAttention
        nAmountToUse = 0
        sCurrentCurrency = ""
        ' Run through all payments pr currency
        For Each oPayment In oBatch.Payments
            If oPayment.NOTI_NotificationAttention <> "NETTO" Then
                If sCurrentCurrency = "" Then
                    sCurrentCurrency = oPayment.MON_InvoiceCurrency
                End If
                If oPayment.MON_InvoiceCurrency = sCurrentCurrency Then 'And oPayment.StatusCode <> "-1" Then
                    ' Beregn gebyr, som er forskjell mellom Brutto (Mon_InvoiceAmount) og Netto (MON_TransferredAmount)
                    If oPayment.StatusCode <> "-1" Then
                        nAmountToUse = nAmountToUse + (oPayment.MON_InvoiceAmount - oPayment.MON_TransferredAmount)
                    End If
                    oPayment.NOTI_NotificationAttention = "NETTO"

                End If
            End If

        Next
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(Math.Abs(nAmountToUse)), "-", vbNullString)), 23, " ") & ";" 'Amount

        If nAmountToUse < 0 Then
            sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
        Else
            sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
        End If

        sLineToWrite = sLineToWrite & sCurrentCurrency & ";" 'Valuta
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        sLineToWrite = sLineToWrite & Space(35) & ";" 'Blank (Payers name)
        sLineToWrite = sLineToWrite & PadRight("WEBGebyr" & Trim$(Replace(oBatch.REF_Bank, ";", vbNullString)), 80, " ")

        sLineToWrite = sLineToWrite.Trim
        ' if we have processed all currencies, retur blank line
        If sCurrentCurrency = "" Then
            sLineToWrite = ""
        End If

        WriteGLSpecialActiveBrandsPayPal_7781_Record = sLineToWrite

    End Function 'XNET - 07.01.2013 - added parameters
    Private Function WriteGLRecord(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        'XNET - New variable
        Dim sTemp As String

        sLineToWrite = ""
        '18.05.2009 - New IF for Maritech
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Then
            If Len(Trim(oInvoice.MATCH_ID)) = 3 Then
                sLineToWrite = "B;" 'Posttype - Bank (bankkort)
            ElseIf InStr("23802381238223832384238523861590159315951596", oInvoice.MATCH_ID) > 0 Then
                ' 16.01.2018 - added test for Seaborn - bankaccounts 2380 - 2386 must be a B; record
                sLineToWrite = "B;" 'Posttype - Bank (bankkort)

                ' added 03.11.2015
                ' 29.02.2016 Changed from Extra1 to MON_Currency
            ElseIf oInvoice.MyField = "SEABORN_TERMIN" And (oInvoice.MON_Currency <> "NOK" And oInvoice.MON_Currency <> "") Then
                ' Dette er "utbetalingsposten i GBP, USD, etc for terminhandelen, konsturert av BabelBank
                sLineToWrite = "B;" 'Posttype - Bank (bankkort)
            Else
                sLineToWrite = "F;" 'Posttype
            End If
        Else
            sLineToWrite = "F;" 'Posttype
        End If
        sLineToWrite = sLineToWrite & "A;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
            If InStr(1, oInvoice.MATCH_ID, "-", CompareMethod.Text) > 0 Then
                sLineToWrite = sLineToWrite & PadRight(xDelim(Trim(oInvoice.MATCH_ID), "-", 2), 8, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight("", 8, " ") & ";" 'Client
            End If
            'XNET - 25.02.2013 - Added next IF
        ElseIf sSpecial = "ELKEM" Then
            If sDivision = "M3" Then
                sLineToWrite = sLineToWrite & PadRight("190", 8, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
            End If
        ElseIf Trim(oClient.ClientNo) = "<>" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf Trim(oClient.ClientNo) = "=" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf sSpecial = "SALMAR_R" Or sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
            ' we must "cheat" and have client 412 for DNB-invoices, but need 411 in the file
            sLineToWrite = sLineToWrite & PadRight("411", 8, " ") & ";" 'Client
            'ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            '   sLineToWrite = sLineToWrite & PadRight("400", 8, " ") & ";"
        Else
            'Normal situation
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        'XNET - 04.02.2013 - Added next IF
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Then
            If InStr(1, oInvoice.MATCH_ID, "-", CompareMethod.Text) > 0 Then
                sLineToWrite = sLineToWrite & PadRight(xDelim(Trim(oInvoice.MATCH_ID), "-", 1), 45, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 45, " ") & ";" 'GL-account
            End If
        Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 45, " ") & ";" 'GL-account
        End If
        If oInvoice.MATCH_ClientNo = "DISCOUNT_ELKEM" Then 'XNET - 04.02.2013 changed the IF
            'Special for Elkem
            sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 25, " ") & ";" 'InvoiceNo
        Else
            sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        End If
        'KIKO remove the 6 lines above, use the next 7 lines instead
        'If InStr(1, oInvoice.MyField, "DISCOUNT_ELKEM", vbTextCompare) > 0 Then
        '    oInvoice.MyField = Replace(oInvoice.MyField, "DISCOUNT_ELKEM", "", , , vbTextCompare)
        '    'Special for Elkem
        '    sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 25, " ") & ";" 'InvoiceNo
        'Else
        '    sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        'End If
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        ' New funcionality 13.12.2014
        ' If value filled into oInvoice.MON_CurrencyAmount, then use this one as amount.
        ' Can be used f.ex. for Terminkontrakt, motpost (utbetalingspost), to be able to post complete Terminkontraktsoppgj�r
        ' used this way for Salmar (AX2012)
        ' 02.03.2016 added and not sSpecial = "MARITECH" Or sSpecial = "COAST" Then
        If oInvoice.MON_CurrencyAmount > 0 And Not (sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST") Then
            sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(oInvoice.MON_CurrencyAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
            sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            sLineToWrite = sLineToWrite & oInvoice.MON_Currency & ";" 'Valuta
            sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        ElseIf oInvoice.MON_CurrencyAmount < 0 And Not (sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST") Then
            sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(oInvoice.MON_CurrencyAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
            sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            sLineToWrite = sLineToWrite & oInvoice.MON_Currency & ";" 'Valuta
            sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        Else

            'XNET - 15.03.2012 - Changed the line below (added INT())
            sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(oInvoice.MON_InvoiceAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
            ' 12.12.2014 JANP; Men det ble vel to ganger samme linje, har kommentert den under !!!!!!!
            'sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            End If

            ' XNET 31.10.2013 added "COAST"
            If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Then
                ' 29.10.2015 added Seaborn_termin
                If oInvoice.MyField = "LEROY_TERMIN" Or oInvoice.MyField = "SEABORN_TERMIN" Then
                    If Len(oInvoice.Extra1) = 3 Then
                        sLineToWrite = sLineToWrite & oInvoice.Extra1 & ";" 'Valuta
                        sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(oInvoice.MON_AccountAmount, "-", vbNullString)), 23, " ") & ";" 'Amount
                        ' 29.02.2016 added next If to be able to handle Termin when Undone in MatchManual
                    ElseIf Len(oInvoice.MON_Currency) = 3 Then
                        sLineToWrite = sLineToWrite & oInvoice.MON_Currency & ";" 'Valuta
                        sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(oInvoice.MON_CurrencyAmount, "-", vbNullString)), 23, " ") & ";" 'Amount
                    Else
                        sLineToWrite = sLineToWrite & "NOK;" 'Valuta
                        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
                    End If
                Else
                    'As before
                    If Len(oPayment.MON_InvoiceCurrency) = 3 Then
                        sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
                    Else
                        sLineToWrite = sLineToWrite & "NOK;" 'Valuta
                    End If
                    sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
                End If
            Else
                If Len(oPayment.MON_InvoiceCurrency) = 3 Then
                    sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
                Else
                    sLineToWrite = sLineToWrite & "NOK;" 'Valuta
                End If
                sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
            End If
        End If 'If oInvoice.MON_CurrencyAmount > 0 Then


        'XNET - 19.10.2010 - Added next 7 lines - deleted 1
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        sLineToWrite = sLineToWrite & PadRight(sTemp, 15, " ") & ";" ' Exch.rate against basevaluta
        'Old line
        'sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta		
        ' 12.12.2014 from JANP: What the fuck happended to basevaluta??? was commented out. !!!!!!!!!
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta

        If sSpecial = "ACTIVEBRANDS" And IsNumeric(oPayment.E_Name) And Len(Trim(oPayment.E_Name)) > 10 Then
            ' 27.11.2017blir feil med noen MT940 "navn" hvor det er mange tall, gir owerflow
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace("x" & Left(Replace(oPayment.E_Name, " ", ""), 10), ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        Else
            '23.09.2010 - Added CHR(157) in the next 2 replaces
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        End If
        ' XNET 23.03.2011 - added If for TOBULL
        ' XNET 05.05.2011 REMOVED If for TOBULL
        'XNET - 16.03.2012 - Added next IF
        ' 29.10.2015 added Seaborn_termin
        If Trim(oInvoice.MyField) = "LEROY_TERMIN" Or Trim(oInvoice.MyField) = "SEABORN_TERMIN" Then
            sLineToWrite = sLineToWrite & PadRight("VALUTATERMIN", 80, " ")
            ' 03.12.2019 - added text for Adyen for 2950 "motpost"
            ' 30.12.2020 - tatt vekk denne spesielle for 2950, og heller lagt inn i ElseIf en under, slik at det kan vises manuelt innlagte tekster
            'ElseIf sSpecial = "ACTIVEBRANDS_ADYEN" And oInvoice.MATCH_ID = "2950" Then
            '    sBatchNo = oPayment.ExtraD1
            '    ' 13.12.2019
            '    If Not EmptyOrNullString(oInvoice.Extra1) Then
            '        sTemp = oInvoice.Extra1
            '    Else
            '        sTemp = "BT"
            '    End If

            '    ' 13.12.2019 - added next If
            '    ' form Adyen import for 2950, have added Brandname (KT, SP, etc) into Dim9
            '    If Not EmptyString(oInvoice.Dim9) Then
            '        sLineToWrite = sLineToWrite & sTemp & " " & PadRight(oInvoice.Dim9 & " " & sBatchNo & " " & Format(Math.Abs(oInvoice.MON_InvoiceAmount) / 100, "###00.00") & " " & oPayment.MON_InvoiceCurrency & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            '    Else
            '        sLineToWrite = sLineToWrite & sTemp & " " & PadRight(Left(oPayment.E_Name, 10) & " " & sBatchNo & " " & Format(Math.Abs(oInvoice.MON_InvoiceAmount) / 100, "###00.00") & " " & oPayment.MON_InvoiceCurrency & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            '    End If
            ' 04.01.2021 added 1591 (ReserveAdjustment) and 7781 for Adyen Fees
        ElseIf sSpecial = "ACTIVEBRANDS_ADYEN" And (oInvoice.MATCH_ID = "2950" Or oInvoice.MATCH_ID = "1591" Or oInvoice.MATCH_ID = "7781" Or oInvoice.MATCH_ID = "25350" Or oInvoice.MATCH_ID = "25351" Or oInvoice.MATCH_ID = "80401" Or oInvoice.MATCH_ID = "15251") Then
            ' 30.12.2020 - simplified
            'If Not EmptyString(oInvoice.Dim10) Then
            '    sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim10 & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            'ElseIf Not EmptyString(oInvoice.Extra1) Then
            '    sLineToWrite = sLineToWrite & PadRight(oInvoice.Extra1 & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            'ElseIf oInvoice.Freetexts.Count > 0 Then
            If oInvoice.Freetexts.Count > 0 Then   ' added 15.02.2021
                sLineToWrite = sLineToWrite & PadRight(oInvoice.Freetexts(1).Text & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            Else
                sLineToWrite = sLineToWrite & PadRight(" (" & oPayment.Unique_PaymentID & ")", 80, " ")
            End If
            'End If
            ElseIf sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
                ' 12.11.2019 - add BB's unique payment ID to end of freetext
                sLineToWrite = sLineToWrite & PadRight(Left(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 70) & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            Else
                sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ")
            End If

            ' 01.12.2017 - For ActiveBrands - remove blanks from the end of each record
            If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "BUTIKKDRIFT_Teller" Then
                sLineToWrite = sLineToWrite.Trim
            End If

            If sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "FONDET FOR REGIONALE" Or Trim(oInvoice.MyField) = "LEROY_TERMIN" Then
                If EmptyString(oInvoice.MyField) Then
                    sLineToWrite = sLineToWrite & ";" & Space(50)
                Else
                    sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 50, " ")
                End If
                ' 04.11.2015 added next If
            ElseIf sSpecial = "MARITECH" Then
                sLineToWrite = sLineToWrite & ";" & Space(50)
                'XNET 17.01.2013 - Added next ElseIf
            ElseIf sDivision = "M3" Then
                sLineToWrite = sLineToWrite & ";" & PadRight(xDelim(sGLAccount, "-", 1), 50, " ")
            ElseIf sSpecial = "ACTIVEBRANDS" Then
                ' for Lindorff, add an extra column (used for "LINDORFF". )
                sLineToWrite = sLineToWrite & ";"
                ' 14.05.2018 - added a Dimension2 field, 404-413
                sLineToWrite = sLineToWrite & Space(10) & ";"

            End If

            WriteGLRecord = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteSupplierRecord(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        'XNET - New variable
        Dim sTemp As String

        sLineToWrite = ""
        sLineToWrite = "L;" 'Posttype
        sLineToWrite = sLineToWrite & "A;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Then
            sLineToWrite = sLineToWrite & PadRight("", 8, " ") & ";" 'Client
            'XNET - 25.02.2013 - Added next IF
        ElseIf sSpecial = "ELKEM" Then
            If sDivision = "M3" Then
                sLineToWrite = sLineToWrite & PadRight("190", 8, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
            End If
        ElseIf Trim(oClient.ClientNo) = "<>" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf Trim(oClient.ClientNo) = "=" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        Else
            'Normal situation
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        'XNET - 04.02.2013 - Added next IF
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        sLineToWrite = sLineToWrite & PadRight(oInvoice.CustomerNo, 45, " ") & ";" 'Vendorno
        sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 25, " ") & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
        If oInvoice.MON_InvoiceAmount < 0 Then
            sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
        Else
            sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
        End If
        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        'XNET - 19.10.2010 - Added next 7 lines - deleted 1
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        sLineToWrite = sLineToWrite & PadRight(sTemp, 15, " ") & ";" ' Exch.rate against basevaluta
        'Old line
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        '23.09.2010 - Added CHR(157) in the next 2 replaces
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", ""), Chr(157), ""), 80, " ")
        'KIKO remove the line above, use the next 2 lines instead
        'sLineToWrite = sLineToWrite & PadRight(Replace(sFreetext, ";", ""), 80, " ") & ";"
        'sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField, 4, " ") ' MyField - Elkem 07.11.2007

        '23.09.2010 - Added next IF
        'XNET - 08.11.2010 Added Fondet
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "FONDET FOR REGIONALE" Then
            If EmptyString(oInvoice.MyField) Then
                sLineToWrite = sLineToWrite & ";" & Space(50)
            Else
                sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 50, " ")
            End If
            'XNET 17.01.2013 - Added next ElseIf
        ElseIf sDivision = "M3" Then
            sLineToWrite = sLineToWrite & ";" & PadRight(xDelim(sGLAccount, "-", 1), 50, " ")
        End If

        WriteSupplierRecord = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteCustomerRecord(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String, ByVal sGLResultAccount As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim sTemp As String
        Dim oFreeText As Freetext

        sLineToWrite = ""
        If oInvoice.MATCH_ClientNo = "DISCOUNT_ELKEM" Or sSpecial = "SALMAR_R" Then 'XNET - 04.02.2013 changed the IF
            sLineToWrite = "R;" 'Posttype
        ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            ' 14.11.2017
            sLineToWrite = "F;" 'Posttype  (1510) - er hovedbokskonto, finanskonto
        Else
            sLineToWrite = "K;" 'Posttype
        End If

        If oInvoice.MATCH_Matched Then
            If oInvoice.MATCH_PartlyPaid Then
                sLineToWrite = sLineToWrite & "D;" ' Matchstatus
            Else
                sLineToWrite = sLineToWrite & "A;" ' Matchstatus
            End If
        Else
            sLineToWrite = sLineToWrite & "I;" 'Matchstatus
        End If
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            sLineToWrite = sLineToWrite & PadRight("", 8, " ") & ";" 'Client
            'XNET - 25.02.2013 - Added next IF
        ElseIf sSpecial = "ELKEM" Then
            If sDivision = "M3" Then
                sLineToWrite = sLineToWrite & PadRight("190", 8, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
            End If
        ElseIf sSpecial = "NHST_INNBETALINGER" Then
            ' for NHST Infosoft, present Tittel in Client-field
            sLineToWrite = sLineToWrite & PadRight(xDelim(oInvoice.MyField, "|", 2), 8, " ") & ";"
        ElseIf Trim(oClient.ClientNo) = "<>" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf Trim(oClient.ClientNo) = "=" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf sSpecial = "SALMAR_R" Or sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
            ' we must "cheat" and have client 412 for DNB-invoices, but need 411 in the file
            sLineToWrite = sLineToWrite & PadRight("411", 8, " ") & ";" 'Client
            'ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            '   sLineToWrite = sLineToWrite & PadRight("400", 8, " ") & ";"
        Else
            'Normal situation
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        If sSpecial = "NHST_INNBETALINGER" Then
            ' hardcoded INBA for Infosoft
            sLineToWrite = sLineToWrite & PadRight("INBA", 10, " ") & ";" 'VoucherType
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        End If

        'XNET - 04.02.2013 - Added next IF
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If

        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        ' 17.06.2016 Added SALMAR_R, to write the DNB CustomerAccount pr currency
        If sSpecial = "SALMAR_R" Then
            If oClient.ClientNo = "411" Then
                ' "normal" Salmar client
                sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'Customerno
            Else
                sLineToWrite = sLineToWrite & PadRight(sGLResultAccount, 45, " ") & ";" 'Motkonto til DNB oppgj�r
            End If
        Else
            ' 26.08.2020 - For Active Brands, special AROS (factoring) invoices starting with 88 or 99 shall not be matched.
            '               They shall instead be set OnAccount to customer 80067
            'If (Left(oInvoice.MATCH_ID, 2) = "99" Or Left(oInvoice.MATCH_ID, 2) = "99") And (sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN") Then
            '    sLineToWrite = sLineToWrite & PadRight("80067", 45, " ") & ";" 'Customerno
            'Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.CustomerNo, 45, " ") & ";" 'Customerno
            'End If
        End If
        ' 01.09.2017 added next If
        ' 17.07.2018 - added Or InStr(oPayment.E_Name, "KLARNA") > 0  
        If (sSpecial = "ACTIVEBRANDS" And (oInvoice.MyField = "KLARNA" Or InStr(oPayment.E_Name, "KLARNA") > 0 Or oInvoice.MyField = "PAYEX")) Or _
        sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            ' 14.08.2018 - hvis samme som customerNo, ikke legg inn noe her !!!!
            If oInvoice.MATCH_ID <> oInvoice.CustomerNo Then
                'sLineToWrite = sLineToWrite & PadRight(Left(oInvoice.MATCH_ID, 6), 25, " ") & ";"  ' invoiceNo i 6 first pos in matchid
                ' 22.07.2020 - must export 6 or 8 digits for ActiveBrands, as the Aros-invoices, starting with 99, are 8 digits.
                ' sLineToWrite = sLineToWrite & PadRight(Left(oInvoice.MATCH_ID, 8), 25, " ") & ";"  ' invoiceNo i 8 first pos in matchid
                ' 01.12.2021 - InvoiceNo is expanded to 9 digits
                sLineToWrite = sLineToWrite & PadRight(Left(oInvoice.MATCH_ID, 9), 25, " ") & ";"  ' invoiceNo i 9 first pos in matchid
            Else
                sLineToWrite = sLineToWrite & Space(25) & ";"
            End If
        Else
            ' 26.08.2020 - For Active Brands, special AROS (factoring) invoices starting with 88 or 99 shall not be matched.
            '               They shall instead be set OnAccount to customer 80067
            'If (Left(oInvoice.MATCH_ID, 2) = "99" Or Left(oInvoice.MATCH_ID, 2) = "99") And (sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN") Then
            'sLineToWrite = sLineToWrite & Space(25) & ";"   ' Blank (InvoiceNo)
            'Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 25, " ") & ";" 'Blank (InvoiceNo)
            'End If
        End If
        ' Changed 14.02.07, use Unique_ID only when OCR and match_id not in use
        If (IsOCR(oPayment.PayCode) Or IsAutogiro(oPayment.PayCode)) And EmptyString(oInvoice.MATCH_ID) Then
            sLineToWrite = sLineToWrite & PadLeft(oInvoice.Unique_Id, 25, " ") & ";" 'Blank (KID)
        ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            ' no matchingref to NAV for Tellerpayments - this booking only
            sLineToWrite = sLineToWrite & Space(25) & ";"  ' blank
        ElseIf sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            ' 26.08.2020 - For Active Brands, special AROS (factoring) invoices starting with 88 or 99 shall not be matched.
            '               They shall instead be set OnAccount to customer 80067
            'If (Left(oInvoice.MATCH_ID, 2) = "99" Or Left(oInvoice.MATCH_ID, 2) = "88") Then
            'sLineToWrite = sLineToWrite & Space(25) & ";"
            'Else
            ' 14.11.2018 - det st�r angitt som Numerisk felt hos oss - gj�r dette for ActiveBrands
            sLineToWrite = sLineToWrite & PadLeft(KeepNumericsOnly(oInvoice.MATCH_ID), 25, " ") & ";"
            'End If
        Else
            ' 14.11.2018 - det st�r angitt som Numerisk felt hos oss - her b�r vi egentlig beholde kun numeriske, eller endre i dok
            sLineToWrite = sLineToWrite & PadLeft(oInvoice.MATCH_ID, 25, " ") & ";"
        End If

        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
            sLineToWrite = sLineToWrite & PadLeft(oPayment.Unique_ERPID, 30, " ") & ";" 'BBREF - New kind of man matching
        Else
            sLineToWrite = sLineToWrite & Space(30) & ";"
        End If
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
        If sSpecial = "SALMAR_R" Then
            ' for Salmar DNB/GIEK, post a R-record to DNB Customerledger, instead of to a Bank-account
            ' Debit/Credit is turned for this posting
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            End If
        End If
        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        'XNET - 19.10.2010 - Added next 7 lines - deleted 1
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        ' added 18.12.2014 Exchangerate for Salmar-invoices:
        If sSpecial = "SALMAR" Then
            sTemp = oInvoice.MyField  ' picked up in matchingrules
        End If
        sLineToWrite = sLineToWrite & PadRight(sTemp, 15, " ") & ";" ' Exch.rate against basevaluta
        'Old line
        'sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta

        '23.09.2010 - Added CHR(157) in the next 2 replaces
        If sSpecial = "ACTIVEBRANDS" And IsNumeric(oPayment.E_Name) And Len(Trim(oPayment.E_Name)) > 10 Then
            ' 27.11.2017blir feil med noen MT940 "navn" hvor det er mange tall, gir owerflow
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace("x" & Left(Replace(oPayment.E_Name, " ", ""), 10), ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        Else
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        End If
        ' 16.06.2017 added next If
        If sSpecial = "SCANDIC" And IsOCR(oPayment.PayCode) Then
            ' try to find freetext added by user (new 26.06.2017);
            sFreetext = ""
            For Each oFreeText In oInvoice.Freetexts
                If oFreeText.Qualifier = "4" Then
                    sFreetext = sFreetext & oFreeText.Text
                End If
            Next
            ' 27.10.2017 - have Unique_Id only on first Invoice, not on the rest of the invoices
            If Trim(oInvoice.Unique_Id) = "" Then
                ' find KID from first invoice;
                sFreetext = oPayment.Invoices(1).Unique_Id & " " & sFreetext
            End If

            sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oInvoice.Unique_Id & " " & sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ")
        ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            '16.11.2017 Pick Authorization ref only as freetext, always 4. element in freetexts + add BatchNum + Avdelingsno
            If oInvoice.Freetexts.Count > 3 Then
                ' 21.12.2017 - skip auth.ref - not enough room 
                sFreetext = oPayment.REF_Bank1 & " " & oClient.ClientNo '& " " & oInvoice.Freetexts(4).Text
            Else
                sFreetext = oPayment.REF_Bank1 & " " & oClient.ClientNo
            End If
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ")
            sLineToWrite = Trim(sLineToWrite)
        ElseIf sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
            ' 12.11.2019 - for Adyen, remove most of freetext, keep orderno.
            '       BD00152617  -      4�369,30  Gebyr 6,97 - mc original in freetext, keep only BD00152617 
            If sSpecial = "ACTIVEBRANDS_ADYEN" Then
                If InStr(sFreetext, "-") > 0 Then
                    sFreetext = xDelim(sFreetext, "-", 1)
                Else
                    ' 30.11.2020
                    If oInvoice.Freetexts.Count > 0 Then
                        sFreetext = oInvoice.Freetexts(1).Text
                    End If
                End If
            End If
            ' 26.08.2020 - For Active Brands, special AROS (factoring) invoices starting with 88 or 99 shall not be matched.
            '               They shall instead be set OnAccount to customer 80067
            'If (Left(oInvoice.MATCH_ID, 2) = "99" Or Left(oInvoice.MATCH_ID, 2) = "88") Then
            '' for 88/99 invoices, put invoiceno into freetext column
            'sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID & "/" & oInvoice.CustomerNo, 80, " ")
            'Else
            ' 12.11.2019 - add BB's unique payment ID to end of freetext
            sLineToWrite = sLineToWrite & PadRight(Left(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 70) & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            'End If
        Else
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ")
        End If

        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "FONDET FOR REGIONALE" Then
            If EmptyString(oInvoice.MyField) Then
                sLineToWrite = sLineToWrite & ";" & Space(50)
            Else
                sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 50, " ")
            End If
            'XNET 17.01.2013 - Added next ElseIf
        ElseIf sDivision = "M3" Then
            sLineToWrite = sLineToWrite & ";" & PadRight(xDelim(sGLAccount, "-", 1), 50, " ")
        ElseIf sSpecial = "NHST_INNBETALINGER" Then
            ' Infosoft; legg ut eksportdato mmdd
            sLineToWrite = sLineToWrite & ";" & PadRight(Format(Date.Today, "MMdd"), 50, " ")
            'ElseIf sSpecial = "ACTIVEBRANDS" Then
        ElseIf sSpecial = "ACTIVEBRANDS" Or _
            sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            ' 29.04.2019
            If oPayment.E_Name Is Nothing Then
                oPayment.E_Name = ""
            End If
            If (oInvoice.REF_Bank = "LINDORFF" Or InStr(oPayment.E_Name.ToUpper, "LINDORFF") > 0) Then
                ' for Lindorff, add an extra column saying "LINDORFF". (Dimension1 393-402 in "new" standard format
                sLineToWrite = sLineToWrite & ";LINDORFF  "   ' 393-402
            Else
                sLineToWrite = sLineToWrite & ";" & Space(10)   '14.05.2018 added space(10)
            End If
            ' 14.05.2018 - added a dimension2 field, 404-413 in "new" format
            sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.Dim2, 10, " ")
            ' 02.10.2019 - if e_name = Nothing, then bumbs
            If EmptyString(oPayment.E_Name) Then
                oPayment.E_Name = ""
            End If
            If sSpecial = "ACTIVEBRANDS" And (oInvoice.MyField = "KLARNA" Or InStr(oPayment.E_Name, "KLARNA") > 0 Or InStr(oPayment.E_Name.ToUpper, "ADYEN") > 0 Or InStr(oPayment.E_Name.ToUpper, "WELLS FARGO BANK") > 0) Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
                ' 08.01.2019 - added YourReference to Dimension3 - pos 413-422
                ' 09.01.2019 - In some cases we have overwritten YourReference in InvoiceNo.
                ' If so, try to find it as first part of freetext;
                ' 05.06.2019 - BBRET_MyField should be filled if DAL is programmed correctly - if so, use this one
                If Not EmptyString(oInvoice.MyField) And Not vbIsNumeric(Left(oInvoice.MyField, 2)) Then
                    sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 12, " ")
                ElseIf Not EmptyString(oInvoice.MyField) And vbIsNumeric(Left(oInvoice.MyField, 9)) And Len(oInvoice.MyField) < 10 Then
                    ' mangler bokstaver, men kun 8 eller 9 tall
                    sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 10, " ")
                ElseIf Not EmptyString(oInvoice.InvoiceNo) And Not vbIsNumeric(Left(oInvoice.InvoiceNo, 2)) Then
                    sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.InvoiceNo, 10, " ")
                ElseIf oInvoice.Freetexts.Count > 0 Then
                    If Not vbIsNumeric(Left(oInvoice.Freetexts(1).Text, 2)) And vbIsNumeric(Mid(oInvoice.Freetexts(1).Text, 3, 8)) Or vbIsNumeric(Left(oInvoice.Freetexts(1).Text, 8)) Then
                        ' find in freetext, like KT, or old numbers like 00012157
                        sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.Freetexts(1).Text, 10, " ")
                    Else
                        sLineToWrite = sLineToWrite & ";" & Space(12)
                    End If
                Else
                    sLineToWrite = sLineToWrite & ";" & Space(10)
                End If
            Else
                sLineToWrite = sLineToWrite & ";" & Space(10)
            End If
        End If

        ' 28.11.2019 - Added a new column at the end for Adyen - to export BatchNumber
        If sSpecial = "ACTIVEBRANDS_ADYEN" Then
            If oPayment.VB_Profile.CompanyName = "Active Brands US" Or oPayment.VB_Profile.CompanyName = "ECom USA" Then
                ' 08.12.2020 - do not add the batchnumber for USA
            Else
                sLineToWrite = sLineToWrite & ";" & PadRight(oPayment.ExtraD1, 8, " ")
            End If
        End If
        WriteCustomerRecord = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteNotMatchedRecord(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim lNoMatchAccountType As Short

        sLineToWrite = ""


        ' 07.10.2009 - added possiblity to have observationaccount as GLAccount
        lNoMatchAccountType = GetNoMatchAccountType(oPayment.VB_Profile.Company_ID, oClient.Client_ID)
        If lNoMatchAccountType = TypeGLAccount Then '2
            sLineToWrite = "F;" 'Posttype  GL
        Else
            ' as pre 07.10.2009
            If sSpecial = "SALMAR_R" Then
                sLineToWrite = "R;" 'Special DNB Reskontro to AX
            Else
                sLineToWrite = "K;" 'Posttype Kunderesk '1
            End If
        End If

        sLineToWrite = sLineToWrite & "I;" 'Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            sLineToWrite = sLineToWrite & PadRight("", 8, " ") & ";" 'Client
            'XNET - 25.02.2013 - Added next IF
        ElseIf sSpecial = "ELKEM" Then
            If sDivision = "M3" Then
                sLineToWrite = sLineToWrite & PadRight("190", 8, " ") & ";" 'Client
            Else
                sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
            End If
        ElseIf Trim(oClient.ClientNo) = "<>" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf Trim(oClient.ClientNo) = "=" Then
            'Special for ELKEM, needs 090
            sLineToWrite = sLineToWrite & PadRight("090", 8, " ") & ";" 'Client
        ElseIf sSpecial = "SALMAR_R" Or sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
            ' we must "cheat" and have client 412 for DNB-invoices, but need 411 in the file
            sLineToWrite = sLineToWrite & PadRight("411", 8, " ") & ";" 'Client
            'ElseIf sSpecial = "BUTIKKDRIFT_TELLER" Then
            '   sLineToWrite = sLineToWrite & PadRight("400", 8, " ") & ";"
        Else
            'Normal situation
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        'XNET - 04.02.2013 - Added next IF
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        ' commented out bookdate 01.06.2007 for Elkem. Must change to Special = Elkem !!!!!!!!!!
        ' XNET 07.01.2012 Mr Isingrud er usikker p� hvilken Elkem dette gjelder - men han tar sjansen p� at det er ELKEM (og ikke ELKEM_MOSJOEN)
        If sSpecial = "ELKEM" Then
            ' special for Elkem - bruk valuteringsdato p� begge datoer.
            ' for alle andre; bruk bokf�ringsdato
        Else
            sDateToUse = vbNullString
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else 'Use payment_date
                If Not oPayment.DATE_Value = "19900101" Then
                    sDateToUse = oPayment.DATE_Value
                Else
                    sDateToUse = DateToString(Now())
                End If
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date (as valuedate for Elkem 01.06.2007
        If sSpecial = "SALMAR_R" Then
            sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'Vendorno
            ' 13.10.2017 - added next
        ElseIf sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "ACTIVEBRANDS_VIPPS" Then
            sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'motkonto Vipps, settes opp som ResultatKonto i kontooppsett
        Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 45, " ") & ";" 'Vendorno
        End If
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)

        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
            sLineToWrite = sLineToWrite & PadLeft(oPayment.Unique_ERPID, 30, " ") & ";" 'BBREF - New kind of man matching
        Else
            sLineToWrite = sLineToWrite & Space(30) & ";"
        End If
        'If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "ACTIVEBRANDS_VIPPS" Then
        '    sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_TransferredAmount), "-", "")), 23, " ") & ";" 'Amount
        'Else
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
        'End If
        If sSpecial = "SALMAR_R" Then
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            End If
        End If
        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        '23.09.2010 - Added CHR(157) in the next 2 replaces
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        If sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
            ' 12.11.2019 - add BB's unique payment ID to end of freetext
            If Not EmptyString(oInvoice.InvoiceNo) Then
                sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            Else
                sLineToWrite = sLineToWrite & PadRight(Left(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 70) & " (" & oPayment.Unique_PaymentID & ")", 80, " ")
            End If
        Else
            sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ")
        End If
        ' 27.10.2017 - For ActiveBrands - remove blanks from the end of each record
        If sSpecial = "BUTIKKDRIFT_VIPPS" Or sSpecial = "BUTIKKDRIFT_Teller" Or sSpecial = "ACTIVEBRANDS_VIPPS" Then
            sLineToWrite = sLineToWrite.Trim
        End If

        '23.09.2010 - Added next IF
        'XNET - 08.11.2010 Added Fondet
        ' XNET 31.10.2013 added "COAST"
        If sSpecial = "MARITECH" Or sSpecial = "COAST_TROMSO" Or sSpecial = "COAST" Or sSpecial = "FONDET FOR REGIONALE" Then
            If EmptyString(oInvoice.MyField) Then
                sLineToWrite = sLineToWrite & ";" & Space(50)
            Else
                sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 50, " ")
            End If
            'XNET 17.01.2013 - Added next ElseIf
        ElseIf sDivision = "M3" Then
            sLineToWrite = sLineToWrite & ";" & PadRight(xDelim(sGLAccount, "-", 1), 50, " ")
        ElseIf sSpecial = "ACTIVEBRANDS" Or sSpecial = "ACTIVEBRANDS_KLARNA" Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Then
            If EmptyString(oPayment.E_Name) Then
                oPayment.E_Name = "X"
            End If
            If (oInvoice.REF_Bank = "LINDORFF" Or InStr(oPayment.E_Name.ToUpper, "LINDORFF") > 0) Then
                ' for Lindorff, add an extra column saying "LINDORFF". 
                '16.01.2019 - added blanks at the end of the next 47 lines
                sLineToWrite = sLineToWrite & ";LINDORFF  "
            Else
                sLineToWrite = sLineToWrite & ";          "
            End If
            ' 14.05.2018 - added a Dimension2 field, 404-413
            'sLineToWrite = sLineToWrite & Space(10) & ";"
            sLineToWrite = sLineToWrite & ";" & Space(10)
        End If

        ' added 16.01.2019
        If sSpecial = "ACTIVEBRANDS" And (oInvoice.MyField = "KLARNA" Or InStr(oPayment.E_Name, "KLARNA") > 0) Or sSpecial = "ACTIVEBRANDS_PAYPAL" Or sSpecial = "ACTIVEBRANDS_ADYEN" Or sSpecial = "ACTIVEBRANDS_KLARNA" Then
            ' 08.01.2019 - added YourReference to Dimension3 - pos 413-422
            ' 09.01.2019 - In some cases we have overwritten YourReference in InvoiceNo.
            ' If so, try to find it as first part of freetext;
            ' 05.06.2019 - BBRET_MyField should be filled if DAL is programmed correctly - if so, use this one
            If Not EmptyString(oInvoice.MyField) And Not vbIsNumeric(Left(oInvoice.MyField, 2)) Then
                sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 12, " ")   ' 12 for Adyen USA, var 10 tegn
            ElseIf Not EmptyString(oInvoice.MyField) And vbIsNumeric(Left(oInvoice.MyField, 9)) And Len(oInvoice.MyField) < 10 Then
                ' mangler bokstaver, men kun 8 eller 9 tall
                sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.MyField, 10, " ")
            ElseIf Not EmptyString(oInvoice.InvoiceNo) And Not vbIsNumeric(Left(oInvoice.InvoiceNo, 2)) Then
                sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.InvoiceNo, 10, " ")
            ElseIf oInvoice.Freetexts.Count > 0 Then
                If Not vbIsNumeric(Left(oInvoice.Freetexts(1).Text, 2)) And vbIsNumeric(Mid(oInvoice.Freetexts(1).Text, 3)) Or vbIsNumeric(Left(oInvoice.Freetexts(1).Text, 8)) Then
                    ' find in freetext, like KT, or old numbers like 00012157
                    sLineToWrite = sLineToWrite & ";" & PadRight(oInvoice.Freetexts(1).Text, 10, " ")
                Else
                    sLineToWrite = sLineToWrite & ";" & Space(10)
                End If
            Else
                sLineToWrite = sLineToWrite & ";" & Space(10)
            End If
        Else
            sLineToWrite = sLineToWrite & ";" & Space(10)
        End If
        ' 28.11.2019 - Added a new column at the end for Adyen - to export BatchNumber
        If sSpecial = "ACTIVEBRANDS_ADYEN" Then
            If oPayment.VB_Profile.CompanyName = "Active Brands US" Or oPayment.VB_Profile.CompanyName = "ECom USA" Then
                ' 08.12.2020 - do not add the batchnumber for USA
            Else
                sLineToWrite = sLineToWrite & ";" & PadRight(oPayment.ExtraD1, 8, " ")
            End If
        End If


        WriteNotMatchedRecord = sLineToWrite

    End Function
    'Private Function Salmar_CalculateAverageTerminRate(ByVal oBatch As Batch) As Double
    '    ' 11.02.2015
    '    ' For Salmar, AX, we need to specify an weighted, average, terminkurs on the bankposting
    '    Dim oPayment As Payment
    '    Dim oInvoice As Invoice
    '    Dim nTotalBatchAmount As Double = 0
    '    Dim nTotalCurrencyAmount As Double = 0

    '    For Each oPayment In oBatch.Payments
    '        For Each oInvoice In oPayment.Invoices
    '            ' Exch.rate in oInvoiceMyField
    '            nTotalBatchAmount = nTotalBatchAmount + oInvoice.MON_InvoiceAmount
    '            If oInvoice.MyField Is Nothing Or oInvoice.MyField = "" Then
    '                nTotalCurrencyAmount = nTotalCurrencyAmount + oInvoice.MON_InvoiceAmount
    '            Else
    '                nTotalCurrencyAmount = nTotalCurrencyAmount + (oInvoice.MON_InvoiceAmount * (CDbl(Replace(oInvoice.MyField, ".", ",")) / 100))
    '            End If

    '        Next oInvoice
    '    Next oPayment

    '    Salmar_CalculateAverageTerminRate = Math.Round(nTotalCurrencyAmount / nTotalBatchAmount * 100, 8)
    'End Function
    Function WriteBabelBank_InnbetalingFile_ver2(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sClientNo As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal bPostAgainstObservationAccount As Boolean) As Boolean
        '11.05.2015 - Added the use of bPostAgainstObservationAccount
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bBackUp As Boolean
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment, oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim iFreetextCounter As Integer, sFreetextFixed As String, sFreeTextVariable As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sLineToWrite As String
        Dim bFirstBatch As Boolean, nSequenceNoTotal As Double
        Dim bGLRecordWritten As Boolean
        Dim sVoucherNo As String
        Dim sVoucherNoToPass As String
        Dim sAlphaPart As String
        Dim nDigitsLen As Integer
        Dim sGLAccount As String
        Dim sGLResultAccount As String = ""  ' added 16.09.2016
        Dim sOldAccountNo As String, sTempClientNo As String
        Dim bAccountFound As Boolean
        '22.02.2010 - the use of bAtLeastOnePaymentExported added
        Dim bAtLeastOnePaymentExported As Boolean
        Dim bExportoInvoice As Boolean
        Dim bAllInvoicesExported As Boolean
        Dim sOriginalFile As String
        Dim nBankAmount As Double
        Dim sBankAmount As String

        Dim sNoMatchAccount As String
        Dim iNoMatchAccountType As Integer

        Dim lCounter As Long
        Dim lArrayCounter As Long
        Dim aAccountArray(,) As String
        Dim aARAmountArray() As Double
        Dim aGLAmountArray() As Double

        Try

            ' lag en outputfil
            bAppendFile = False
            bGLRecordWritten = False
            bFirstBatch = True
            sOldAccountNo = vbNullString
            sTempClientNo = vbNullString
            bAtLeastOnePaymentExported = False

            'New code 11.05.2015
            If Not oBabelFiles.VB_Profile Is Nothing Then
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)
                'Create an array equal to the client-array
                ReDim Preserve aARAmountArray(UBound(aAccountArray, 2))
                ReDim Preserve aGLAmountArray(UBound(aAccountArray, 2))
            End If

            'Doesn't matter if a file exist. If so, append

            sOldAccountNo = vbNullString

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'Check for omitted payments
                        If oPayment.StatusCode <> "-1" Then
                            '22.02.2010 - New code to check the exported mark
                            If Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    'New 28.12.2005 for Elkem
                                    If EmptyString(sI_Account) Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoBabel = True
                                                End If
                                            Else
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If

                            If bExportoBabel Then
                                Exit For
                            End If
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'Check for omitted payments
                            If oPayment.StatusCode <> "-1" Then
                                '22.02.2010 - New code to check the exported mark
                                If Not oPayment.Exported Then
                                    'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        'New 28.12.2005 for Elkem
                                        If EmptyString(sI_Account) Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoBatch = True
                                                    End If
                                                Else
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            bGLRecordWritten = False
                            j = 0
                            sBankAmount = ""

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'Check for omitted payments
                                If oPayment.StatusCode <> "-1" Then
                                    '22.02.2010 - New code to check the exported mark
                                    If Not oPayment.Exported Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            'New 28.12.2005 for Elkem
                                            If EmptyString(sI_Account) Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                                        If oPayment.VB_ClientNo = sClientNo Then
                                                            bExportoPayment = True
                                                        End If
                                                    Else
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                'New 18.07.2007 - Don't export unmatched items that is zero-amount
                                If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
                                    If oPayment.MON_InvoiceAmount = 0 Then
                                        bExportoPayment = False
                                        oPayment.Exported = True
                                    End If
                                End If

                                If bExportoPayment Then
                                    bAtLeastOnePaymentExported = True
                                    'Find the client

                                    If oPayment.I_Account <> sOldAccountNo Then
                                        If oPayment.VB_ProfileInUse Then
                                            'sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If oPayment.I_Account = oaccount.Account Then
                                                            sTempClientNo = oClient.ClientNo
                                                            bAccountFound = True
                                                            sGLAccount = Trim$(oaccount.GLAccount)
                                                            sGLResultAccount = Trim(oaccount.GLResultsAccount)
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True

                                                            sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                                            iNoMatchAccountType = GetNoMatchAccountType(oBabel.VB_Profile.Company_ID, oClient.Client_ID)

                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                            If bAccountFound Then
                                                oPayment.VB_ClientNo = sTempClientNo
                                                For lCounter = 0 To UBound(aAccountArray, 2)
                                                    If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                        lArrayCounter = lCounter
                                                        Exit For
                                                    End If
                                                Next lCounter

                                            Else
                                                Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                                '12003: Could not find account %1.Please enter the account in setup."
                                            End If
                                        End If
                                    End If

                                    If Not bAccountFound Then
                                        Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003, oPayment.I_Account))
                                        '12003: Could not find account %1.Please enter the account in setup."
                                    End If

                                    '11.05.2015 - Added next IF
                                    If bPostAgainstObservationAccount Then

                                        If iNoMatchAccountType = BabelFiles.MatchType.MatchedOnGL Then
                                            sLineToWrite = WriteGLRecord_ver2(oPayment, oPayment.Invoices.Item(1), oClient, oPayment.Unique_PaymentID, sSpecial, sDivision, sNoMatchAccount, oPayment.VoucherNo, True, oPayment.MON_InvoiceAmount, True)
                                            oFile.WriteLine(sLineToWrite)
                                        Else
                                            sLineToWrite = WriteCustomerRecord_ver2(oPayment, oPayment.Invoices.Item(1), oClient, oPayment.Unique_PaymentID, sSpecial, sDivision, sNoMatchAccount, oPayment.VoucherNo, True, sGLResultAccount)
                                            oFile.WriteLine(sLineToWrite)
                                        End If

                                    Else

                                        ' 13.06.2016 Special for Salmar, and DNB_Giek financing
                                        If sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
                                            ' do not write normal bankrecord for DNB/GIEK
                                            bGLRecordWritten = True
                                        End If

                                        'Write GL-line if not previously written
                                        If Not bGLRecordWritten Then
                                            sVoucherNoToPass = ""
                                            sLineToWrite = WriteGLBankRecord_ver2(oBatch, oPayment, oClient, sGLAccount, sSpecial, sDivision, sBankAmount, sVoucherNoToPass)
                                            oFile.WriteLine(sLineToWrite)
                                            bGLRecordWritten = True
                                        End If

                                    End If

                                    'First we have to find the freetext to the payment-lines.
                                    iFreetextCounter = 0
                                    sFreetextFixed = vbNullString
                                    For Each oInvoice In oPayment.Invoices

                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreetext In oInvoice.Freetexts
                                                iFreetextCounter = iFreetextCounter + 1
                                                If iFreetextCounter = 1 Then
                                                    sFreetextFixed = RTrim$(oFreetext.Text)
                                                ElseIf iFreetextCounter = 2 Then
                                                    sFreetextFixed = sFreetextFixed & " " & RTrim$(oFreetext.Text)
                                                Else
                                                    Exit For
                                                End If
                                            Next oFreetext
                                        End If
                                    Next oInvoice
                                    sFreetextFixed = Trim$(oPayment.E_Name) & " " & sFreetextFixed

                                    For Each oInvoice In oPayment.Invoices

                                        bExportoInvoice = True

                                        ' 13.09.2018 -
                                        ' added use of supplier, by setting BBRET_MyField3 = 'Supplier'
                                        ' First time used for Pelagia
                                        If oInvoice.MyField3.ToUpper = "SUPPLIER" Then
                                            oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier
                                        End If

                                        If oInvoice.MATCH_Final = True And bExportoInvoice Then

                                            If sSpecial = "PELAGIA" Then
                                                ' 04.10.2018 added special texting for Pelagia
                                                sFreeTextVariable = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier = 4 Then
                                                        sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                    End If
                                                Next oFreetext
                                                If Len(sFreeTextVariable) = 0 Then
                                                    sFreeTextVariable = "Payment"
                                                End If
                                            Else
                                                ' standard, for all others
                                                '01.12.2011
                                                'Add the variable part of the freetext
                                                sFreeTextVariable = sFreetextFixed
                                                For Each oFreetext In oInvoice.Freetexts
                                                    If oFreetext.Qualifier > 2 Then
                                                        sFreeTextVariable = RTrim$(oFreetext.Text) & " " & sFreeTextVariable
                                                    End If
                                                Next oFreetext
                                            End If

                                            If oInvoice.MATCH_Matched Then

                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    sLineToWrite = WriteCustomerRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass, False, sGLResultAccount)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                    '11.05.2015 - Add to totalamountexported
                                                    aGLAmountArray(lArrayCounter) = aGLAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount

                                                    If sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
                                                        sLineToWrite = WriteCustomerRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, "SALMAR_R", sDivision, sGLAccount, sVoucherNoToPass, True, sGLResultAccount)
                                                        oFile.WriteLine(sLineToWrite)
                                                    End If

                                                    sLineToWrite = WriteGLRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass, False)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier Then
                                                    sLineToWrite = WriteSupplierRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass)
                                                Else
                                                    '11.05.2015 - Add to totalamountexported
                                                    aARAmountArray(lArrayCounter) = aARAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                    If sSpecial = "SALMAR" And oInvoice.MyField2 = "DNB" Then
                                                        '- if Salmar og DNB, skriv en ny SalmarDNBCustomerRecord, med "R" og Debetbel�p -
                                                        ' Write "R" line, for DNB reskontro
                                                        sLineToWrite = WriteCustomerRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, "SALMAR_R", sDivision, sGLAccount, sVoucherNoToPass, True, sGLResultAccount)
                                                        oFile.WriteLine(sLineToWrite)
                                                    End If
                                                    If sSpecial = "SALMAR" And oClient.ClientNo = "412" And Left(oInvoice.MATCH_ID, 5) = "77700" Then
                                                        ' 28.09.2016 - vi m� legge inn en ekstra "motpost" til gebyrposten for DNB-oppgj�rene, hvis ikke vil det ikke
                                                        ' balansere. Det er slik at "R"-postene, motpost til reskontropostene", ikke har noe fratrekk for bankgebyr.
                                                        ' dette m� kompenseres ved at vi f�rer bankgebyret debet/kredit
                                                        sLineToWrite = WriteGLRecord_ver2(oPayment, oPayment.Invoices.Item(1), oClient, oPayment.Unique_PaymentID, "SALMAR_GEBYR", sDivision, sGLAccount, oPayment.VoucherNo, True, oInvoice.MON_InvoiceAmount, True)
                                                        oFile.WriteLine(sLineToWrite)
                                                    End If
                                                    sLineToWrite = WriteCustomerRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass, False, sGLResultAccount)
                                                End If
                                            Else
                                                If sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
                                                    ' For Salmar, DNB/GIEK, Write an R-record, instead of BankGl Record
                                                    sLineToWrite = WriteNotMatchedRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, "SALMAR_R", sDivision, sGLAccount, sVoucherNoToPass)
                                                    oFile.WriteLine(sLineToWrite)
                                                End If

                                                sLineToWrite = WriteNotMatchedRecord_ver2(oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial, sDivision, sGLAccount, sVoucherNoToPass)
                                            End If

                                            oFile.WriteLine(sLineToWrite)

                                            oInvoice.Exported = True
                                        End If 'oInvoice.MATCH_Final = True
                                    Next oInvoice

                                    oPayment.Exported = True
                                End If 'bExportoPayment
                            Next ' payment


                        End If

                        'We have to save the last used sequenceno. in some cases
                        'First check if we use a profile
                        If oBatch.VB_ProfileInUse = True Then
                            Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                'Always use the seqno at filesetup level
                                Case 1
                                    'Only for the first batch if we have one sequenceseries!
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                    oBatch.VB_Profile.Status = 2
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                Case 2
                                    'oBatch.VB_Profile.Status = 2
                                    'oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                    'oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                    sVoucherNo = sAlphaPart & PadLeft(Trim$(Str(nSequenceNoTotal)), nDigitsLen, "0")
                                    ' XNET 13.06.2013 added next if
                                    If Not oClient Is Nothing Then
                                        oClient.VoucherNo = sVoucherNo
                                        oClient.Status = Profile.CollectionStatus.Changed
                                        oBatch.VB_Profile.Status = Profile.CollectionStatus.ChangeUnder
                                        oBatch.VB_Profile.FileSetups(iFormat_ID).Status = Profile.CollectionStatus.ChangeUnder
                                    End If

                                Case Else
                                    'Err.Raise 12002, "WriteNavision_for_SI_Data", LRS(12002, "Navision", sFilenameOut)
                                    ' XNET 17.01.2013 rettet feil i tekst
                                    Err.Raise(12002, "WriteBabelBank_InnbetalingFile", LRS(12002, "Navision", sFilenameOut))

                            End Select

                        End If

                    Next 'batch

                End If

            Next

            If bAtLeastOnePaymentExported Then
                oFile.Close()
                oFile = Nothing

                If oBabelFiles.Item(1).VB_ProfileInUse Then
                    If oBabelFiles.Item(1).VB_Profile.ManMatchFromERP Then
                        For lCounter = 0 To UBound(aAccountArray, 2)
                            dbUpDayTotals(False, DateToString(Now), aARAmountArray(lCounter), "MATCHED_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                            dbUpDayTotals(False, DateToString(Now), aGLAmountArray(lCounter), "MATCHED_GL", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                        Next lCounter
                    End If
                End If

            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile(sFilenameOut)
            End If

            oFs = Nothing

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBabelBank_InnbetalingFile_ver2" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBabelBank_InnbetalingFile_ver2 = True

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteGLBankRecord_ver2(ByVal oBatch As vbBabel.Batch, ByVal oPayment As vbBabel.Payment, ByVal oClient As vbBabel.Client, ByVal sGLAccount As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sBankAmount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim nExchRate As Double
        Dim nAmountToUse As Double 'Added 21.07.2016

        sLineToWrite = ""
        sLineToWrite = "B;" 'Posttype
        sLineToWrite = sLineToWrite & " ;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        'XNET - 04.02.2013 - Added next IF
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'GL-account
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        'XNET - 04.02.2013 - Added an IF
        If sBankAmount = "" Then
            '20.07.2017 - Added next loop to deduct omitted payments from the batchamount
            nAmountToUse = oBatch.MON_InvoiceAmount
            For Each oPayment In oBatch.Payments
                'Check for omitted payments
                If oPayment.StatusCode = "-1" Then
                    nAmountToUse = nAmountToUse - oPayment.MON_InvoiceAmount
                End If
            Next oPayment

            sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(nAmountToUse), "-", vbNullString)), 23, " ") & ";" 'Amount
            If nAmountToUse < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            sLineToWrite = sLineToWrite & PadLeft(Replace(sBankAmount, "-", vbNullString), 23, " ") & ";" 'Amount
            If Left(sBankAmount, 1) = "-" Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        End If

        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        sLineToWrite = sLineToWrite & Space(35) & ";" 'Blank (Payers name)
        If oPayment.PayCode = "670" Then
            sLineToWrite = sLineToWrite & PadRight("Valutatermin", 80, " ") & ";"
        Else
            sLineToWrite = sLineToWrite & PadRight(Trim$(Replace(oBatch.REF_Bank, ";", vbNullString)), 80, " ") & ";"
        End If

        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim1
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim2
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim3
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim4
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim5
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim6
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim7
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim8
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim9
        sLineToWrite = sLineToWrite & Space(10) & ";" 'Dim10
        sLineToWrite = sLineToWrite & Space(50) & ";" 'MyField
        sLineToWrite = sLineToWrite & Space(50) & ";" 'MyField2
        sLineToWrite = sLineToWrite & Space(50) 'MyField3

        WriteGLBankRecord_ver2 = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteGLRecord_ver2(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String, ByVal bPostAgainstObservationAccount As Boolean, Optional ByVal nAmount As Double = 0, Optional ByVal bUsePassedAmount As Boolean = False) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        'XNET - New variable
        Dim sTemp As String

        sLineToWrite = ""
        '18.05.2009 - New IF for Maritech
        ' XNET 31.10.2013 added "COAST"
        sLineToWrite = "F;" 'Posttype
        sLineToWrite = sLineToWrite & "A;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        If sSpecial = "SALMAR_R" Or sSpecial = "SALMAR" Or sSpecial = "SALMAR_GEBYR" And oClient.ClientNo = "412" Then
            ' we must "cheat" and have client 412 for DNB-invoices, but need 411 in the file
            sLineToWrite = sLineToWrite & PadRight("411", 8, " ") & ";" 'Client
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        If EmptyString(oInvoice.MATCH_ID) Then
            sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'GL-account
        Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 45, " ") & ";" 'GL-account
        End If
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching

        ' New funcionality 13.12.2014
        ' If value filled into oInvoice.MON_CurrencyAmount, then use this one as amount.
        ' Can be used f.ex. for Terminkontrakt, motpost (utbetalingspost), to be able to post complete Terminkontraktsoppgj�r
        ' used this way for Salmar (AX2012)
        If bUsePassedAmount Then
            sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(nAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
            If bPostAgainstObservationAccount Then
                If nAmount < 0 Then
                    sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                Else
                    sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                End If
            Else
                If nAmount < 0 Then
                    sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                Else
                    sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                End If
            End If
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
            sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        Else
            If oInvoice.MON_CurrencyAmount > 0 Then
                sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(oInvoice.MON_CurrencyAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
                If bPostAgainstObservationAccount Then
                    sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                Else
                    sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                End If
                sLineToWrite = sLineToWrite & oInvoice.MON_Currency & ";" 'Valuta
                sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
            ElseIf oInvoice.MON_CurrencyAmount < 0 Then
                sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(oInvoice.MON_CurrencyAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
                If bPostAgainstObservationAccount Then
                    sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                Else
                    sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                End If
                sLineToWrite = sLineToWrite & oInvoice.MON_Currency & ";" 'Valuta
                sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
            Else

                'XNET - 15.03.2012 - Changed the line below (added INT())
                sLineToWrite = sLineToWrite & PadLeft(Trim$(Replace(Int(oInvoice.MON_InvoiceAmount), "-", vbNullString)), 23, " ") & ";" 'Amount
                ' 12.12.2014 JANP; Men det ble vel to ganger samme linje, har kommentert den under !!!!!!!
                'sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
                If oInvoice.MON_InvoiceAmount < 0 Then
                    If bPostAgainstObservationAccount Then
                        sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                    Else
                        sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                    End If
                Else
                    If bPostAgainstObservationAccount Then
                        sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                    Else
                        sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                    End If
                End If

                If Len(oPayment.MON_InvoiceCurrency) = 3 Then
                    sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
                Else
                    sLineToWrite = sLineToWrite & "NOK;" 'Valuta
                End If
                sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
            End If 'If oInvoice.MON_CurrencyAmount > 0 Then
        End If

        'XNET - 19.10.2010 - Added next 7 lines - deleted 1
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        sLineToWrite = sLineToWrite & PadRight(sTemp, 15, " ") & ";" ' Exch.rate against basevaluta
        'Old line
        'sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta		
        ' 12.12.2014 from JANP: What the fuck happended to basevaluta??? was commented out. !!!!!!!!!
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        '23.09.2010 - Added CHR(157) in the next 2 replaces
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        'XNET - 16.03.2012 - Added next IF
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ") & ";"

        ' 19.08.2016 - endret fra Padleft til PadRight by JanP - vi venstrestiller vel alle andre konti ???
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim1, 10, " ") & ";" 'Dim1
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim2, 10, " ") & ";" 'Dim2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim3, 10, " ") & ";" 'Dim3
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim4, 10, " ") & ";" 'Dim4
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim5, 10, " ") & ";" 'Dim5
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim6, 10, " ") & ";" 'Dim6
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim7, 10, " ") & ";" 'Dim7
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim8, 10, " ") & ";" 'Dim8
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim9, 10, " ") & ";" 'Dim9
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim10, 10, " ") & ";" 'Dim10
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField, 50, " ") & ";" 'MyField
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField2, 50, " ") & ";" 'MyField2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField3, 50, " ") 'MyField3

        WriteGLRecord_ver2 = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteSupplierRecord_ver2(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        'XNET - New variable
        Dim sTemp As String

        sLineToWrite = ""
        sLineToWrite = "L;" 'Posttype
        sLineToWrite = sLineToWrite & "A;" ' Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        sLineToWrite = sLineToWrite & PadRight(oInvoice.CustomerNo, 45, " ") & ";" 'Vendorno
        sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 25, " ") & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        sLineToWrite = sLineToWrite & Space(30) & ";" 'BBREF - New kind of man matching
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
        If oInvoice.MON_InvoiceAmount < 0 Then
            sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
        Else
            sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
        End If
        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        'XNET - 19.10.2010 - Added next 7 lines - deleted 1
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        sLineToWrite = sLineToWrite & PadRight(sTemp, 15, " ") & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        '23.09.2010 - Added CHR(157) in the next 2 replaces
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", ""), Chr(157), ""), 80, " ") & ";"

        ' 19.08.2016 - endret fra Padleft til PadRight by JanP - vi venstrestiller vel alle andre konti ???
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim1, 10, " ") & ";" 'Dim1
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim2, 10, " ") & ";" 'Dim2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim3, 10, " ") & ";" 'Dim3
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim4, 10, " ") & ";" 'Dim4
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim5, 10, " ") & ";" 'Dim5
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim6, 10, " ") & ";" 'Dim6
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim7, 10, " ") & ";" 'Dim7
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim8, 10, " ") & ";" 'Dim8
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim9, 10, " ") & ";" 'Dim9
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim10, 10, " ") & ";" 'Dim10
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField, 50, " ") & ";" 'MyField
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField2, 50, " ") & ";" 'MyField2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField3, 50, " ") 'MyField3

        WriteSupplierRecord_ver2 = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteCustomerRecord_ver2(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String, ByVal bPostAgainstObservationAccount As Boolean, ByVal sGLResultAccount As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        'XNET - New variable
        Dim sTemp As String

        sLineToWrite = ""
        If oInvoice.MATCH_ClientNo = "DISCOUNT_ELKEM" Or sSpecial = "SALMAR_R" Then 'XNET - 04.02.2013 changed the IF
            sLineToWrite = "R;" 'Posttype
        Else
            sLineToWrite = "K;" 'Posttype
        End If

        If oInvoice.MATCH_Matched Then
            If oInvoice.MATCH_PartlyPaid Then
                sLineToWrite = sLineToWrite & "D;" ' Matchstatus
            Else
                sLineToWrite = sLineToWrite & "A;" ' Matchstatus
            End If
        Else
            sLineToWrite = sLineToWrite & "I;" 'Matchstatus
        End If
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        If sSpecial = "SALMAR_R" Or sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
            ' we must "cheat" and have client 412 for DNB-invoices, but need 411 in the file
            sLineToWrite = sLineToWrite & PadRight("411", 8, " ") & ";" 'Client
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If

        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        'sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If

        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = ""
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date
        ' 17.06.2016 Added SALMAR_R, to write the DNB CustomerAccount pr currency
        If sSpecial = "SALMAR_R" Then
            If oClient.ClientNo = "411" Then
                ' "normal" Salmar client
                sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'Customerno
            Else
                sLineToWrite = sLineToWrite & PadRight(sGLResultAccount, 45, " ") & ";" 'Motkonto til DNB oppgj�r
            End If
        Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.CustomerNo, 45, " ") & ";" 'Customerno
        End If
        ' 23.03.2021 - For "kid" matching we have no InvoiceNo
        If sSpecial = "ABAX_INCOMING" Then
            If oInvoice.MATCH_ID <> oInvoice.CustomerNo Then
                sLineToWrite = sLineToWrite & PadRight(Left(oInvoice.MATCH_ID, 8), 25, " ") & ";"  ' invoiceNo i 8 first pos in matchid
            Else
                sLineToWrite = sLineToWrite & Space(25) & ";"
            End If
        Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 25, " ") & ";" 'Blank (InvoiceNo)
        End If
        ' Changed 14.02.07, use Unique_ID only when OCR and match_id not in use
        If (IsOCR(oPayment.PayCode) Or IsAutogiro(oPayment.PayCode)) And EmptyString(oInvoice.MATCH_ID) Then
            sLineToWrite = sLineToWrite & PadLeft(oInvoice.Unique_Id, 25, " ") & ";" 'Blank (KID)
        Else
            sLineToWrite = sLineToWrite & PadLeft(oInvoice.MATCH_ID, 25, " ") & ";"
        End If
        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
            sLineToWrite = sLineToWrite & PadLeft(oPayment.Unique_ERPID, 30, " ") & ";" 'BBREF - New kind of man matching
        Else
            sLineToWrite = sLineToWrite & Space(30) & ";"
        End If
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
        If sSpecial = "SALMAR_R" Then
            ' for Salmar DNB/GIEK, post a R-record to DNB Customerledger, instead of to a Bank-account
            ' Debit/Credit is turned for this posting
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            If oInvoice.MON_InvoiceAmount < 0 Then
                If bPostAgainstObservationAccount Then
                    sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                Else
                    sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                End If
            Else
                If bPostAgainstObservationAccount Then
                    sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
                Else
                    sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
                End If
            End If
        End If
        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        'XNET - 19.10.2010 - Added next 7 lines - deleted 1
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        ' added 18.12.2014 Exchangerate for Salmar-invoices:
        If sSpecial = "SALMAR" Then
            sTemp = oInvoice.MyField  ' picked up in matchingrules
        End If
        sLineToWrite = sLineToWrite & PadRight(sTemp, 15, " ") & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        '23.09.2010 - Added CHR(157) in the next 2 replaces
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        ' 05.11.2018 - start freetest with InvoiceNo for ABAX
        If Left(oClient.Name.ToUpper, 4) = "ABAX" Then
            sFreetext = oInvoice.InvoiceNo & " " & sFreetext
        End If
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ") & ";"

        ' 19.08.2016 - endret fra Padleft til PadRight by JanP - vi venstrestiller vel alle andre konti ???
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim1, 10, " ") & ";" 'Dim1
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim2, 10, " ") & ";" 'Dim2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim3, 10, " ") & ";" 'Dim3
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim4, 10, " ") & ";" 'Dim4
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim5, 10, " ") & ";" 'Dim5
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim6, 10, " ") & ";" 'Dim6
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim7, 10, " ") & ";" 'Dim7
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim8, 10, " ") & ";" 'Dim8
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim9, 10, " ") & ";" 'Dim9
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim10, 10, " ") & ";" 'Dim10
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField, 50, " ") & ";" 'MyField
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField2, 50, " ") & ";" 'MyField2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField3, 50, " ") 'MyField3

        WriteCustomerRecord_ver2 = sLineToWrite

    End Function

    'XNET - 07.01.2013 - added parameters
    Private Function WriteNotMatchedRecord_ver2(ByVal oPayment As vbBabel.Payment, ByVal oInvoice As vbBabel.Invoice, ByVal oClient As vbBabel.Client, ByVal sFreetext As String, ByVal sSpecial As String, ByVal sDivision As String, ByVal sGLAccount As String, ByVal sVoucherNo As String) As String
        Dim sLineToWrite As String
        Dim sDateToUse As String
        Dim lNoMatchAccountType As Short

        sLineToWrite = ""

        ' 07.10.2009 - added possiblity to have observationaccount as GLAccount
        lNoMatchAccountType = GetNoMatchAccountType(oPayment.VB_Profile.Company_ID, oClient.Client_ID)
        If lNoMatchAccountType = TypeGLAccount Then '2
            sLineToWrite = "F;" 'Posttype  GL
        Else
            ' as pre 07.10.2009
            If sSpecial = "SALMAR_R" Then
                sLineToWrite = "R;" 'Special DNB Reskontro to AX
            Else
                sLineToWrite = "K;" 'Posttype Kunderesk '1
            End If
        End If

        sLineToWrite = sLineToWrite & "I;" 'Matchstatus
        If Len(oClient.Division) = 0 Then
            sLineToWrite = sLineToWrite & Space(8) & ";" 'CompanyNo
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.Division, 8, " ") & ";" 'CompanyNo
        End If
        If sSpecial = "SALMAR_R" Or sSpecial = "SALMAR" And oClient.ClientNo = "412" Then
            ' we must "cheat" and have client 412 for DNB-invoices, but need 411 in the file
            sLineToWrite = sLineToWrite & PadRight("411", 8, " ") & ";" 'Client
        Else
            sLineToWrite = sLineToWrite & PadRight(oClient.ClientNo, 8, " ") & ";" 'Client
        End If
        sLineToWrite = sLineToWrite & PadRight(oClient.VoucherType, 10, " ") & ";" 'VoucherType
        If EmptyString(sVoucherNo) Then
            sLineToWrite = sLineToWrite & PadRight(oPayment.VoucherNo, 20, " ") & ";" 'VoucherNo
        Else
            sLineToWrite = sLineToWrite & PadRight(sVoucherNo, 20, " ") & ";" 'VoucherNo
        End If
        sDateToUse = ""
        If Not oPayment.DATE_Value = "19900101" Then
            sDateToUse = oPayment.DATE_Value
        Else 'Use payment_date
            If Not oPayment.DATE_Payment = "19900101" Then
                sDateToUse = oPayment.DATE_Payment
            Else
                sDateToUse = DateToString(Now)
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Value date
        sDateToUse = vbNullString
        If Not oPayment.DATE_Payment = "19900101" Then
            sDateToUse = oPayment.DATE_Payment
        Else 'Use payment_date
            If Not oPayment.DATE_Value = "19900101" Then
                sDateToUse = oPayment.DATE_Value
            Else
                sDateToUse = DateToString(Now())
            End If
        End If
        sLineToWrite = sLineToWrite & sDateToUse & ";" 'Book date (as valuedate for Elkem 01.06.2007
        If sSpecial = "SALMAR_R" Then
            sLineToWrite = sLineToWrite & PadRight(sGLAccount, 45, " ") & ";" 'Vendorno
        ElseIf sSpecial = "ABAX_SWEDEN_INCOMING" Then ' Must use a GL obs-account
            sLineToWrite = sLineToWrite & PadRight(GetNoMatchAccount(oPayment.VB_Profile.Company_ID, oClient.Client_ID), 45, " ") & ";"
        Else
            sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 45, " ") & ";" 'Vendorno
        End If
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (InvoiceNo)
        sLineToWrite = sLineToWrite & Space(25) & ";" 'Blank (KID)
        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
            sLineToWrite = sLineToWrite & PadLeft(oPayment.Unique_ERPID, 30, " ") & ";" 'BBREF - New kind of man matching
        Else
            sLineToWrite = sLineToWrite & Space(30) & ";"
        End If
        sLineToWrite = sLineToWrite & PadLeft(Trim(Replace(CStr(oInvoice.MON_InvoiceAmount), "-", "")), 23, " ") & ";" 'Amount
        If sSpecial = "SALMAR_R" Then
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            End If
        Else
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "D;" 'Debit/Credit
            Else
                sLineToWrite = sLineToWrite & "C;" 'Debit/Credit
            End If
        End If
        If Len(oPayment.MON_InvoiceCurrency) = 3 Then
            sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";" 'Valuta
        Else
            sLineToWrite = sLineToWrite & "NOK;" 'Valuta
        End If
        sLineToWrite = sLineToWrite & Space(23) & ";" 'Amount in basevaluta
        sLineToWrite = sLineToWrite & Space(15) & ";" ' Exch.rate against basevaluta
        sLineToWrite = sLineToWrite & Space(3) & ";" ' BaseValuta
        '23.09.2010 - Added CHR(157) in the next 2 replaces
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(oPayment.E_Name, ";", ""), Chr(157), ""), 35, " ") & ";" 'Blank (Payers name)
        sLineToWrite = sLineToWrite & PadRight(Replace(Replace(sFreetext, ";", vbNullString), Chr(157), vbNullString), 80, " ") & ";"

        ' 19.08.2016 - endret fra padright til PadRight by JanP - vi venstrestiller vel alle andre konti ???
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim1, 10, " ") & ";" 'Dim1
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim2, 10, " ") & ";" 'Dim2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim3, 10, " ") & ";" 'Dim3
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim4, 10, " ") & ";" 'Dim4
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim5, 10, " ") & ";" 'Dim5
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim6, 10, " ") & ";" 'Dim6
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim7, 10, " ") & ";" 'Dim7
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim8, 10, " ") & ";" 'Dim8
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim9, 10, " ") & ";" 'Dim9
        sLineToWrite = sLineToWrite & PadRight(oInvoice.Dim10, 10, " ") & ";" 'Dim10
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField, 50, " ") & ";" 'MyField
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField2, 50, " ") & ";" 'MyField2
        sLineToWrite = sLineToWrite & PadRight(oInvoice.MyField3, 50, " ") 'MyField3

        WriteNotMatchedRecord_ver2 = sLineToWrite

    End Function
    Private Sub ActiveBrandsResetPaymentDateForPayPalAndAdyen(ByVal oBabelFiles As BabelFiles)
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim sMaxDate As String = "19900101"

        For Each oBabel In oBabelFiles
            For Each oBatch In oBabel.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.DATE_Payment > sMaxDate Then
                        sMaxDate = oPayment.DATE_Payment
                    End If
                Next oPayment
            Next oBatch
        Next oBabel
        For Each oBabel In oBabelFiles
            For Each oBatch In oBabel.Batches
                For Each oPayment In oBatch.Payments
                    oPayment.DATE_Payment = sMaxDate
                Next oPayment
            Next oBatch
        Next oBabel

    End Sub
    Public Sub ActiveBrandsAdyenControlBrandsAndCurrencies(ByVal oBabelFiles As BabelFiles)
        ' Check:
        ' - Are there Adyen payments?
        ' - If yes, have we received payments in all currencies for all brands?
        ' - If not, raise a message with warning
        Dim aBrandsAndCurrencies As String(,)
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bGetOut As Boolean = False
        Dim i As Integer
        Dim sWarning As String = ""
        Dim bAdyenExists As Boolean = False
        ReDim aBrandsAndCurrencies(1, 19)

        aBrandsAndCurrencies(0, 0) = "KT/USD"
        aBrandsAndCurrencies(0, 1) = "KT/GBP"
        aBrandsAndCurrencies(0, 2) = "KT/SEK"
        aBrandsAndCurrencies(0, 3) = "KT/DKK"
        aBrandsAndCurrencies(0, 4) = "KT/NOK"
        aBrandsAndCurrencies(0, 5) = "SP/USD"
        aBrandsAndCurrencies(0, 6) = "SP/GBP"
        aBrandsAndCurrencies(0, 7) = "SP/SEK"
        aBrandsAndCurrencies(0, 8) = "SP/DKK"
        aBrandsAndCurrencies(0, 9) = "SP/NOK"
        aBrandsAndCurrencies(0, 10) = "JO/USD"
        aBrandsAndCurrencies(0, 11) = "JO/GBP"
        aBrandsAndCurrencies(0, 12) = "JO/SEK"
        aBrandsAndCurrencies(0, 13) = "JO/DKK"
        aBrandsAndCurrencies(0, 14) = "JO/NOK"
        aBrandsAndCurrencies(0, 15) = "BD/USD"
        aBrandsAndCurrencies(0, 16) = "BD/GBP"
        aBrandsAndCurrencies(0, 17) = "BD/SEK"
        aBrandsAndCurrencies(0, 18) = "BD/DKK"
        aBrandsAndCurrencies(0, 19) = "BD/NOK"

        For Each oBabel In oBabelFiles
            For Each oBatch In oBabel.Batches
                For Each oPayment In oBatch.Payments
                    ' 02.10.2019 - if e_name = Nothing, then bumbs
                    If EmptyString(oPayment.E_Name) Then
                        oPayment.E_Name = ""
                    End If
                    If InStr(oPayment.E_Name.ToUpper, "ADYEN") > 0 Or InStr(oPayment.E_Name.ToUpper, "WELLS FARGO BANK") > 0 Then
                        bAdyenExists = True
                        bGetOut = False
                        For Each oInvoice In oPayment.Invoices
                            If Not EmptyString(oInvoice.MyField) Then
                                If "KTSPJOBD".IndexOf(oInvoice.MyField.Substring(0, 2)) > -1 Then
                                    ' try to find in array, and set as "covered"
                                    For i = 0 To UBound(aBrandsAndCurrencies, 2)
                                        If aBrandsAndCurrencies(0, i) = oInvoice.MyField.Substring(0, 2) & "/" & oPayment.MON_InvoiceCurrency Then
                                            aBrandsAndCurrencies(1, i) = "True"
                                            bGetOut = True  ' jump out, we only need first invoice with MyField to establish which brand it is
                                        End If
                                        If bGetOut Then
                                            Exit For
                                        End If
                                    Next i
                                End If
                            End If
                            If bGetOut Then
                                Exit For
                            End If
                        Next
                    End If
                Next oPayment
            Next oBatch
        Next oBabel

        If bAdyenExists Then  ' do not report if we have no Adyen payments
            ' traverse array, and warn about currencies not reported
            For i = 0 To UBound(aBrandsAndCurrencies, 2)
                If EmptyString(aBrandsAndCurrencies(1, i)) Then
                    sWarning = sWarning & "Ikke mottatt Adyeninformasjon for " & xDelim(aBrandsAndCurrencies(0, i), "/", 1) & " for valuta " & xDelim(aBrandsAndCurrencies(0, i), "/", 2) & vbNewLine
                End If
            Next i
            MsgBox(sWarning, MsgBoxStyle.Exclamation)
        End If

    End Sub
End Module
