Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
' Johannes# 
<System.Runtime.InteropServices.ProgId("Batch_NET.Batch")> Public Class Batch
	
	Private oFile As Scripting.TextStream 'Physical file
	Private oProfile As vbbabel.Profile
	Private oPayments As vbbabel.Payments
    ' XNET 21.03.2013, added oErrorObjects
    Private oErrorObjects As vbBabel.ErrorObjects
    'local variable(s) to hold property value(s)
    'Private mvartest As Variant 'local copy
	
	Private nSequenceNoStart, nIndex, nSequenceNoEnd As Double
    Private sVersion, sStatusCode, sOperatorID As String ', sTransDate As String
	Private sFormatType As String
	Private sI_EnterpriseNo, sI_Branch As String
	Private dDATE_Production As Date
	Private sBatch_ID As String
	Private sREF_Own, sREF_Bank As String
	Private nMON_TransferredAmount, nMON_InvoiceAmount As Double
	Private bAmountSetTransferred, bAmountSetInvoice As Boolean
	Private nNo_Of_Transactions, nNo_Of_Records As Double
	Private bProfileInUse As Boolean
	Private iImportFormat As Short
    Private oCargo As vbBabel.Cargo
	Private oMapping As vbbabel.Mapping
	' 04.05.05 - New, StatementInfo
    Private nMON_BalanceIN As Double, nMON_BalanceOUT As Double
	' added 12.02.2010, also keep outgoing interest balance (valutariktig saldo)
	Private nMON_BalanceOUTInterest As Double
    Private nMON_BalanceOUTInterestNOK, nMON_BalanceOUTNOK, nCurrencyRate As Double
    Private dDATE_BalanceIN As Date
    Private dDATE_BalanceOUT As Date
    Private sUNHString As String
	Private sNADString As String
	Private sBGMString As String
    Private sUCIString As String
    ' 16.10.2019 added next
    Private sBUSString As String = ""
	
	'UPGRADE_ISSUE: BabelFiles.FileType.Excel.Worksheet object was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
    Private oXLWSheet As Microsoft.Office.Interop.Excel.Worksheet
	
	Private sImportLineBuffer As String 'Batch data given from import

    'XNET 27.03.2013, 2 lines
    Private sVisma_FrmNo As String          ' Firm number, like 2
    Private eVisma_Bank As BabelFiles.Bank ' 06.10.2016
    Private sVisma_DatabaseName As String   ' Databasename (F0002)
    ' XNET 08.05.2013 added sPayType
    Private sPayType As String 'I = International   D = Domestic
    ' 13.08.2014
    Private bIsSEPAPaymentSet As Boolean
    ' 25.03.2015 added first for SEB ISO2002
    Private bThisIsTBIO As Boolean

	'********* START PROPERTY SETTINGS ***********************
	Friend WriteOnly Property objFile() As Scripting.TextStream
		Set(ByVal Value As Scripting.TextStream)
			'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Not IsReference(Value) Then ' Or NewVal Is Nothing Then
				'Error # 10001
				Err.Raise(10001,  , LRS(10001))
			End If
			
			oFile = Value
		End Set
	End Property
    Friend WriteOnly Property objXLSheet() As Microsoft.Office.Interop.Excel.Worksheet
        Set(ByVal Value As Microsoft.Office.Interop.Excel.Worksheet)
            'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Not IsReference(Value) Then ' Or NewVal Is Nothing Then
                'Error # 10001
                Err.Raise(10001, , LRS(10001))
            End If

            oXLWSheet = Value
        End Set
    End Property
	Public Property Payments() As vbbabel.Payments
		Get
			Payments = oPayments
		End Get
		Set(ByVal Value As vbbabel.Payments)
			'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Not IsReference(Value) Then
				RaiseInvalidObject()
			End If
			'Store reference
			oPayments = Value
		End Set
	End Property
	
	Public Property VB_Profile() As vbbabel.Profile
		Get
			VB_Profile = oProfile
		End Get
		Set(ByVal Value As vbbabel.Profile)
			'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Not IsReference(Value) Then
				RaiseInvalidObject()
			End If
			
			'Store reference
			oProfile = Value
			bProfileInUse = True
		End Set
    End Property
    Public Property IsSEPAPaymentSet() As Boolean
        Get
            IsSEPAPaymentSet = bIsSEPAPaymentSet
        End Get
        Set(ByVal Value As Boolean)
            bIsSEPAPaymentSet = Value
        End Set
    End Property

    Public Property Cargo() As vbBabel.Cargo
        Get
            Cargo = oCargo
        End Get
        Set(ByVal Value As vbBabel.Cargo)
            'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oCargo = Value
        End Set
    End Property
	Friend WriteOnly Property Mapping() As vbbabel.Mapping
		Set(ByVal Value As vbbabel.Mapping)
			'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Not IsReference(Value) Then
				RaiseInvalidObject()
			End If
			
			'Store reference
			oMapping = Value
		End Set
	End Property
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	Public Property SequenceNoStart() As Double
		Get
			SequenceNoStart = nSequenceNoStart
		End Get
		Set(ByVal Value As Double)
			nSequenceNoStart = Value
		End Set
	End Property
	Public Property SequenceNoEnd() As Double
		Get
			SequenceNoEnd = nSequenceNoEnd
		End Get
		Set(ByVal Value As Double)
			nSequenceNoEnd = Value
		End Set
	End Property
	Public Property StatusCode() As String
		Get
			StatusCode = sStatusCode
		End Get
		Set(ByVal Value As String)
			sStatusCode = Value
		End Set
	End Property
    Public Property PayType() As String
        Get
            PayType = sPayType
        End Get
        Set(ByVal Value As String)
            sPayType = Value
        End Set
    End Property
    Public Property Version() As String
        Get
            Version = sVersion
        End Get
        Set(ByVal Value As String)
            sVersion = Value
        End Set
    End Property
	'UPGRADE_NOTE: Operator was upgraded to Operator_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Property OperatorID() As String
        Get
            OperatorID = sOperatorID
        End Get
        Set(ByVal Value As String)
            sOperatorID = Value
        End Set
    End Property
	Public Property I_EnterpriseNo() As String
		Get
			I_EnterpriseNo = sI_EnterpriseNo
		End Get
		Set(ByVal Value As String)
			sI_EnterpriseNo = Value
		End Set
	End Property
	Public Property I_Branch() As String
		Get
			I_Branch = sI_Branch
		End Get
		Set(ByVal Value As String)
			sI_Branch = Value
		End Set
	End Property
	Public Property DATE_Production() As String
		Get
			DATE_Production = DateToString(dDATE_Production)
		End Get
		Set(ByVal Value As String)
			If StringContainsValidDate(CStr(Value)) Then
				dDATE_Production = StringToDate(CStr(Value))
			Else
                Err.Raise(30010, , LRS(30010, "DATE_Production", "Batch", CStr(Value)))
				'msgbox "Feil i dato som legges inn i %1 DATE_Production i klassen %2 BabelFile"
				'& vbcrlf & Dato %3 cstr(NewVal) er ikke gyldig.
				'& vbcrlf & vbBabel benytter formatet YYYYMMDD
			End If
		End Set
	End Property
    Public Property DATE_BalanceIN() As String
        Get
            DATE_BalanceIN = DateToString(dDATE_BalanceIN)
        End Get
        Set(ByVal Value As String)
            If StringContainsValidDate(CStr(Value)) Then
                dDATE_BalanceIN = StringToDate(CStr(Value))
            Else
                Err.Raise(30010, , Replace(Replace(Replace(LRS(30010), "%1", "DATE_BalanceIN"), "%2", "Batch"), "%3", CStr(Value)))
                'msgbox "Feil i dato som legges inn i %1 DATE_Production i klassen %2 BabelFile"
                '& vbcrlf & Dato %3 cstr(NewVal) er ikke gyldig.
                '& vbcrlf & vbBabel benytter formatet YYYYMMDD
            End If
        End Set
    End Property
    Public Property DATE_BalanceOUT() As String
        Get
            DATE_BalanceOUT = DateToString(dDATE_BalanceOUT)
        End Get
        Set(ByVal Value As String)
            If StringContainsValidDate(CStr(Value)) Then
                dDATE_BalanceOUT = StringToDate(CStr(Value))
            Else
                Err.Raise(30010, , Replace(Replace(Replace(LRS(30010), "%1", "DATE_BalanceOUT"), "%2", "Batch"), "%3", CStr(Value)))
                'msgbox "Feil i dato som legges inn i %1 DATE_Production i klassen %2 BabelFile"
                '& vbcrlf & Dato %3 cstr(NewVal) er ikke gyldig.
                '& vbcrlf & vbBabel benytter formatet YYYYMMDD
            End If
        End Set
    End Property
    Public Property MON_TransferredAmount() As Double
        Get
            MON_TransferredAmount = nMON_TransferredAmount
        End Get
        Set(ByVal Value As Double)
            nMON_TransferredAmount = Value
            bAmountSetTransferred = True
        End Set
    End Property
	Public Property MON_InvoiceAmount() As Double
		Get
			MON_InvoiceAmount = nMON_InvoiceAmount
		End Get
		Set(ByVal Value As Double)
			nMON_InvoiceAmount = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Public Property MON_BalanceIN() As Double
		Get
			MON_BalanceIN = nMON_BalanceIN
		End Get
		Set(ByVal Value As Double)
			nMON_BalanceIN = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Public Property MON_BalanceOUT() As Double
		Get
			MON_BalanceOUT = nMON_BalanceOUT
		End Get
		Set(ByVal Value As Double)
			nMON_BalanceOUT = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Public Property MON_BalanceOUTNOK() As Double
		Get
			MON_BalanceOUTNOK = nMON_BalanceOUTNOK
		End Get
		Set(ByVal Value As Double)
			nMON_BalanceOUTNOK = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Public Property MON_BalanceOUTInterest() As Double
		Get
			MON_BalanceOUTInterest = nMON_BalanceOUTInterest
		End Get
		Set(ByVal Value As Double)
			nMON_BalanceOUTInterest = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Public Property MON_BalanceOUTInterestNOK() As Double
		Get
			MON_BalanceOUTInterestNOK = nMON_BalanceOUTInterestNOK
		End Get
		Set(ByVal Value As Double)
			nMON_BalanceOUTInterestNOK = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Public Property CurrencyRate() As Double
		Get
			CurrencyRate = nCurrencyRate
		End Get
		Set(ByVal Value As Double)
			CurrencyRate = Value
			bAmountSetInvoice = True
		End Set
	End Property
	Friend ReadOnly Property AmountSetTransferred() As Boolean
		Get
			AmountSetTransferred = bAmountSetTransferred
		End Get
	End Property
	Friend ReadOnly Property AmountSetInvoice() As Boolean
		Get
			AmountSetInvoice = bAmountSetInvoice
		End Get
	End Property
	Public Property No_Of_Transactions() As Double
		Get
			No_Of_Transactions = nNo_Of_Transactions
		End Get
		Set(ByVal Value As Double)
			nNo_Of_Transactions = Value
		End Set
	End Property
	Public Property No_Of_Records() As Double
		Get
			No_Of_Records = nNo_Of_Records
		End Get
		Set(ByVal Value As Double)
			nNo_Of_Records = Value
		End Set
	End Property
	Public Property Batch_ID() As String
		Get
			Batch_ID = sBatch_ID
		End Get
		Set(ByVal Value As String)
			sBatch_ID = Value
		End Set
	End Property
	Public Property UNHString() As String
		Get
			UNHString = sUNHString
		End Get
		Set(ByVal Value As String)
			sUNHString = Value
		End Set
	End Property
	Public Property NADString() As String
		Get
			NADString = sNADString
		End Get
		Set(ByVal Value As String)
			sNADString = Value
		End Set
	End Property
	Public Property BGMString() As String
		Get
			BGMString = sBGMString
		End Get
		Set(ByVal Value As String)
			sBGMString = Value
		End Set
	End Property
    Public Property BUSString() As String  ' added 16.10.2019
        Get
            BUSString = sBUSString
        End Get
        Set(ByVal Value As String)
            sBUSString = Value
        End Set
    End Property
    Public Property UCIString() As String
        Get
            UCIString = sUCIString
        End Get
        Set(ByVal Value As String)
            sUCIString = Value
        End Set
    End Property
	Public Property REF_Own() As String
		Get
			REF_Own = sREF_Own
		End Get
		Set(ByVal Value As String)
			sREF_Own = Value
		End Set
	End Property
	Public Property REF_Bank() As String
		Get
			REF_Bank = sREF_Bank
		End Get
		Set(ByVal Value As String)
			sREF_Bank = Value
		End Set
	End Property
    Public Property FormatType() As String
        Get
            FormatType = sFormatType
        End Get
        Set(ByVal Value As String)
            sFormatType = Value
        End Set
    End Property
	Public Property VB_ProfileInUse() As Boolean
		Get
			VB_ProfileInUse = bProfileInUse
		End Get
		Set(ByVal Value As Boolean)
			bProfileInUse = Value
		End Set
	End Property
	Public Property ImportFormat() As Short
		Get
			ImportFormat = iImportFormat
		End Get
		Set(ByVal Value As Short)
			iImportFormat = Value
		End Set
	End Property
    Public Property Visma_DatabaseName() As String
        Get
            Visma_DatabaseName = sVisma_DatabaseName
        End Get
        Set(ByVal Value As String)
            sVisma_DatabaseName = Value
        End Set
    End Property
    Public Property Visma_FrmNo() As String
        Get
            Visma_FrmNo = sVisma_FrmNo
        End Get
        Set(ByVal Value As String)
            sVisma_FrmNo = Value
        End Set
    End Property
    Public Property Visma_Bank() As BabelFiles.Bank
        Get
            Visma_Bank = eVisma_Bank
        End Get
        Set(ByVal Value As BabelFiles.Bank)
            eVisma_Bank = Value
        End Set
    End Property
    ' added 25.03.2015
    Public Property ThisIsTBIO() As Boolean
        Get
            ThisIsTBIO = bThisIsTBIO
        End Get
        Set(ByVal Value As Boolean)
            bThisIsTBIO = Value
        End Set
    End Property

    '********* END PROPERTY SETTINGS ***********************
	
    Public Function BatchContainsPaymentsAccordingToQualifiers(ByRef iFormat_IDQualifier As Short, ByRef bMultiFiles As Boolean, ByRef sClientNoQualifier As String, ByRef nReturnPaymentIndex As Double, ByRef bCheckIfAlreadyExported As Boolean, Optional ByRef sOwnRefQualifier As String = "") As Boolean
        Dim bReturnValue As Boolean
        Dim sNewOwnref As String
        Dim oPayment As vbBabel.Payment

        bReturnValue = False

        'Have to go through each payment-object to see if we have payments that qualifies according to the passed parameters
        For Each oPayment In Payments
            If (bCheckIfAlreadyExported And Not oPayment.Exported) Or Not bCheckIfAlreadyExported Then
                If oPayment.VB_FilenameOut_ID = iFormat_IDQualifier Then
                    If Not bMultiFiles Then
                        bReturnValue = True
                        '21.01.2010 - Return Payment-index and not Batch-index
                        nReturnPaymentIndex = oPayment.Index
                    Else
                        If oPayment.VB_ClientNo = sClientNoQualifier Then
                            If InStr(oPayment.REF_Own, "&?") Then
                                'Set in the part of the OwnRef that BabelBank is using
                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                            Else
                                sNewOwnref = ""
                            End If
                            If sNewOwnref = sOwnRefQualifier Then
                                bReturnValue = True
                                '21.01.2010 - Return Payment-index and not Batch-index
                                nReturnPaymentIndex = oPayment.Index
                            End If
                        End If
                    End If
                End If
            End If
            'If true exit the loop, and we have data to export
            If bReturnValue Then
                '21.01.2010 - Return Payment-index and not Batch-index. Commented next value
                'nReturnPaymentIndex = nIndex
                Exit For
            End If
        Next oPayment

        BatchContainsPaymentsAccordingToQualifiers = bReturnValue

    End Function
	Private Function ReadFile() As String
		
		oPayments.Cargo = oCargo
		
		ReadFile = ""
		
		Select Case iImportFormat
			Case BabelFiles.FileType.Telepay
				ReadFile = ReadTelepayFile()
			Case BabelFiles.FileType.Telepay2
				ReadFile = ReadTelepay2File()
			Case BabelFiles.FileType.TelepayPlus
				ReadFile = ReadTelepay2File("+")
			Case BabelFiles.FileType.AutoGiro
				ReadFile = ReadAutogiroFile()
			Case BabelFiles.FileType.AutogiroEngangs
				ReadFile = ReadAutogiroEngangs()
			Case BabelFiles.FileType.OCR
				ReadFile = ReadOCRFile()
			Case BabelFiles.FileType.Dirrem
				ReadFile = ReadDirremFile()
			Case BabelFiles.FileType.coda
				ReadFile = ReadCodaFile()
			Case BabelFiles.FileType.Leverantorsbetalningar
				ReadFile = Readleverantorsbetalningar()
			Case BabelFiles.FileType.TelepayTBIO
				ReadFile = ReadTelepayFile()
			Case BabelFiles.FileType.OneTimeFormat
				ReadFile = ReadOneTimeFormat()
			Case BabelFiles.FileType.DnB_TBIW_Standard
				ReadFile = ReadDnB_TBIW_Standard()
			Case BabelFiles.FileType.DTAUS
				ReadFile = ReadDTAUS()
			Case BabelFiles.FileType.NordeaUnitel
				ReadFile = ReadNordeaUnitel()
			Case BabelFiles.FileType.ShipNet
				ReadFile = ReadShipNet()
			Case BabelFiles.FileType.CitiDirect_US_Flat_File
				ReadFile = ReadCitiDirectUSFlatFile()
			Case BabelFiles.FileType.DanskeBankTeleService
				ReadFile = ReadDanskeBankTeleService()
			Case BabelFiles.FileType.BACS_For_TBI
				ReadFile = ReadBACSforTBI()
			Case BabelFiles.FileType.First_Union_ACH
				ReadFile = ReadFirst_Union_ACH()
			Case BabelFiles.FileType.DnBNattsafe
				ReadFile = ReadDnBNattsafe()
			Case BabelFiles.FileType.SecuritasNattsafe
				ReadFile = ReadSecuritasNattsafe()
            Case BabelFiles.FileType.MT940, BabelFiles.FileType.MT940_DebitsOnly
                ReadFile = ReadMT940()
            Case BabelFiles.FileType.Corporate_Banking_Interbancario
                ReadFile = ReadCorporate_Banking_Interbancario()
            Case BabelFiles.FileType.MT940E_Handelbanken
                ReadFile = ReadMT940E_Handelsbanken(False)
			Case BabelFiles.FileType.MT940E_Handelbanken_DEBANDCRED
				ReadFile = ReadMT940E_Handelsbanken(True)
			Case BabelFiles.FileType.DanskeBankLokaleDanske, BabelFiles.FileType.DnBNOR_TBIW_Denmark
				ReadFile = ReadDanskeBankLokaleDanske()
			Case BabelFiles.FileType.DnBTBUKIncoming
				ReadFile = ReadDnBTBUKIncoming()
			Case BabelFiles.FileType.Nordea_Innbet_Ktoinf_utl
				ReadFile = ReadNordea_Innbet_Ktoinf_utl()
			Case BabelFiles.FileType.Handelsbanken_DK_Domestic
				ReadFile = ReadHandelsbanken_DK_Domestic()
			Case BabelFiles.FileType.LM_Domestic_Finland
				ReadFile = ReadLM_Domestic_Finland
			Case BabelFiles.FileType.LUM_International_Finland
				ReadFile = ReadLUM_International_Finland
			Case BabelFiles.FileType.bec
				ReadFile = ReadBEC
			Case BabelFiles.FileType.Sydbank
				ReadFile = ReadSydbank()
			Case BabelFiles.FileType.SEK2
				ReadFile = ReadSEK2File()
			Case BabelFiles.FileType.ErhvervsGiro_Udbetaling
				ReadFile = ReadErhvervsGiro
			Case BabelFiles.FileType.DnBTBWK
				ReadFile = ReadDnBTBWK()
			Case BabelFiles.FileType.NordeaDK_EDI
				ReadFile = ReadNordeaDK_EDI()
			Case BabelFiles.FileType.Excel_XLS
				ReadFile = ReadExcelXLS()
			Case BabelFiles.FileType.Bankdata_DK_4_45 ' Bankdata from SEB DK (SEBdec)
				ReadFile = ReadSydbank() ' Nearly the same format as Sydbank
			Case BabelFiles.FileType.Avprickning
				ReadFile = ReadAvprickning()
			Case BabelFiles.FileType.BgMax
				ReadFile = ReadBgMax()
			Case BabelFiles.FileType.OCR_Bankgirot
				ReadFile = ReadOCR_Bankgirot()
			Case BabelFiles.FileType.OCR_Bankgirot_As_Message
				ReadFile = ReadOCR_Bankgirot()
			Case BabelFiles.FileType.Autogiro_Bankgirot
				ReadFile = ReadAutogiro_Bankgirot ' added 01.09.2010
			Case BabelFiles.FileType.BOLS
				ReadFile = ReadBOLSFile
			Case BabelFiles.FileType.PTG
				ReadFile = ReadErhvervsGiro
			Case BabelFiles.FileType.Periodiska_Betalningar
				ReadFile = ReadPeriodiskaBetalningarFinland
			Case BabelFiles.FileType.BankOfAmerica_UK
				ReadFile = ReadBankOfAmerica_UK
			Case BabelFiles.FileType.BabelBank_Mapped_Out
				ReadFile = ReadBabelBankMappingOut()
			Case BabelFiles.FileType.KLINK_ExcelUtbetaling, BabelFiles.FileType.Nordea_Liv_Registrert
				ReadFile = ReadKLINK_ExcelUtbetaling()
			Case BabelFiles.FileType.DnBNOR_UK_Standard
				ReadFile = ReadDnBNOR_UK_TBI_Standard()
			Case BabelFiles.FileType.Erhvervsformat_BEC_DK
				ReadFile = ReadHandelsbanken_DK_Domestic()
			Case BabelFiles.FileType.MT101
				ReadFile = ReadMT101()
			Case BabelFiles.FileType.ABNAmro_BTL91
				ReadFile = ReadABNAmroBTL91
			Case BabelFiles.FileType.Handelsbanken_Bankavstemming
				ReadFile = ReadHandelsbanken_Bankavstemming
			Case BabelFiles.FileType.ABA_Australia
				ReadFile = ReadABA_Australia
			Case BabelFiles.FileType.Australia_Excel_XLS
				ReadFile = ReadExcelXLS()
                ' XNET 02.11.2010
            Case BabelFiles.FileType.GiroDirekt_Plusgiro
                ReadFile = ReadGirodirekt_Plusgiro
                'XNET 01.12.2010
            Case BabelFiles.FileType.ISABEL_APS130_Foreign
                ReadFile = ReadISABEL_APS130_Foreign
                ' XNET 06.01.2011
            Case BabelFiles.FileType.SebScreen_Finland
                ReadFile = ReadSEBScreen_Finland
            Case BabelFiles.FileType.Bacstel
                ReadFile = ReadBACSTel
                ' XokNET 29.11.2011
            Case BabelFiles.FileType.MT100
                ReadFile = ReadMT100()
                ' XokNET 23.01.2012
            Case BabelFiles.FileType.Nacha
                ReadFile = Read_Nacha_US
            Case BabelFiles.FileType.Swedbank_CardReconciliation
                ReadFile = ReadSwedbank_CardReconciliation()
            Case BabelFiles.FileType.Commerzbank_CSV_Incoming
                ReadFile = ReadCommerzbank_CSV_Incoming()
            Case BabelFiles.FileType.SG_CSV_Incoming
                ReadFile = ReadSG_CSV_Incoming
            Case BabelFiles.FileType.OCBC_SG
                ReadFile = ReadOCBC_SG_Giro_Return()
            Case BabelFiles.FileType.KTL
                ReadFile = ReadKTL()
            Case BabelFiles.FileType.TITO
                ReadFile = ReadTITO()

                'Specialformats
                'Case FileType.CREMUL
                '    ReadFile = ReadEDIFile()
                'Case FileType.CapsyAG
                '    ReadFile = ReadCapsyAGFile()
			Case BabelFiles.FileType.OrklaMediaExcel
				ReadFile = ReadOrklaMediaExcelFile()
			Case BabelFiles.FileType.OriflameExcel
				ReadFile = ReadOriflameExcelFile()
			Case BabelFiles.FileType.ISS_SDV
				ReadFile = ReadISS_SDVFile()
			Case BabelFiles.FileType.ISS_LonnsTrekk
				ReadFile = ReadISS_LonnsTrekk()
			Case BabelFiles.FileType.SRI_Access
				ReadFile = ReadSRI_Access()
			Case BabelFiles.FileType.eCommerce_Logistics
				ReadFile = ReadeCommerce_Logistics()
			Case BabelFiles.FileType.Compuware
				ReadFile = ReadCompuware()
			Case BabelFiles.FileType.SeaTruck
				ReadFile = ReadSeaTruck()
			Case BabelFiles.FileType.VB_Internal
				ReadFile = ReadVB_InternalFile()
			Case BabelFiles.FileType.Elkjop
				ReadFile = ReadElkjopFile()
			Case BabelFiles.FileType.TazettAutogiro
				ReadFile = ReadTazettAutogiro()
			Case BabelFiles.FileType.ISS_Paleggstrekk
				ReadFile = ReadISS_Paleggstrekk
			Case BabelFiles.FileType.MMM_Reiseregning
				ReadFile = Read3M_Reiseregning
			Case BabelFiles.FileType.Navision_SIData_Reskontro
				ReadFile = ReadNavision_SIData_Reskontro()
			Case BabelFiles.FileType.BACS_for_Agresso
				ReadFile = ReadBACS_for_Agresso()
			Case BabelFiles.FileType.ControlConsultExcel
				ReadFile = ReadControlConsultExcelFile
			Case BabelFiles.FileType.DnBStockholmExcel
				ReadFile = ReadDnBStockholmExcelFile
			Case BabelFiles.FileType.FirstNordic
				ReadFile = ReadFirstNordic
			Case BabelFiles.FileType.Kvaerner_Singapore_Salary
				ReadFile = ReadKvaernerSingaporeSalary
			Case BabelFiles.FileType.Excel
				ReadFile = ReadExcelFile()
			Case BabelFiles.FileType.EXCEL_Innbetaling
				ReadFile = ReadExcelInnbetalingFile()
            Case BabelFiles.FileType.Modhi_Internal
                ReadFile = ReadExcelInnbetalingFile()
            Case BabelFiles.FileType.pgs
                ReadFile = ReadPGS()
			Case BabelFiles.FileType.Sandvik_Tamrock
				ReadFile = ReadExcelFile()
			Case BabelFiles.FileType.NCL
				ReadFile = ReadNCLFile()
			Case BabelFiles.FileType.RTV
				ReadFile = ReadRTVFile()
			Case BabelFiles.FileType.RTV2
				ReadFile = ReadRTVFile2()
            Case BabelFiles.FileType.RTVAltInn
                ReadFile = ReadRTVFileAltInn
            Case BabelFiles.FileType.Panfish_Scotland
                ReadFile = ReadPanfish_Scotland()
			Case BabelFiles.FileType.Sage_UK_Bacs
				ReadFile = ReadSage_UK_Bacs
			Case BabelFiles.FileType.Autocare_Norge
				ReadFile = ReadAutocare_Norge
            Case BabelFiles.FileType.StrommeSingapore, BabelFiles.FileType.StrommeSingaporeG3
                ReadFile = ReadStrommeSingapore()
			Case BabelFiles.FileType.TeekaySingapore
				ReadFile = ReadTeekaySingapore
			Case BabelFiles.FileType.PGSUK_Salary
				ReadFile = ReadHexagon
			Case BabelFiles.FileType.EltekSG_Salary
				ReadFile = ReadEltekSG_Salary
			Case BabelFiles.FileType.Sudjaca
				ReadFile = ReadSudjaca
			Case BabelFiles.FileType.Tandberg_ACH
				ReadFile = ReadExcelFile ' Set oCargoSpecial in BabelFile.ReadFile
			Case BabelFiles.FileType.CyberCity
				ReadFile = ReadCyberCity
			Case BabelFiles.FileType.NSA
				ReadFile = ReadNSA
			Case BabelFiles.FileType.AquariusOutgoing
				ReadFile = ReadAquariusOutgoing
			Case BabelFiles.FileType.SGFINANSBOLS
				ReadFile = ReadSGFINANS_BOLSFile
			Case BabelFiles.FileType.SG_ClientReporting
				ReadFile = ReadSGClientReportingFile
			Case BabelFiles.FileType.NewPhoneFactoring
				ReadFile = ReadNewphoneFactoringCustomerFile
				'Case FileType.NewphoneCustomerFile
				'    ReadFile = ReadNewphoneCustomerFile
			Case BabelFiles.FileType.OMustadSingapore
				ReadFile = ReadOMustadSingaporeSalary
			Case BabelFiles.FileType.SGFinans_Faktura
				ReadFile = ReadSGFinansFactoring
			Case BabelFiles.FileType.SGFinans_Kunde
				ReadFile = ReadSGFinansCustomerFile
			Case BabelFiles.FileType.SG_0000778_LadeDekkservice
				ReadFile = ReadSG_0000778_LadeDekkserviceFactoringCustomerFile
			Case BabelFiles.FileType.SG_0000032_LadeDekkserviceSYD
				' same format for 000032 and 000778
				ReadFile = ReadSG_0000778_LadeDekkserviceFactoringCustomerFile
			Case BabelFiles.FileType.SG_0000127_SkaarTransFactoring
				ReadFile = Read_0000127_SkaarTransFactoring
			Case BabelFiles.FileType.SG_0000127_SkaarTransCustomer
				ReadFile = Read_0000127_SkaarTransCustomer
			Case BabelFiles.FileType.DnBNORUS_ACH_Incoming
				ReadFile = Read_DnBNORUS_ACH_Incoming
			Case BabelFiles.FileType.DnBNORUS_ACH_Matching
				ReadFile = Read_DnBNORUS_ACH_Incoming
			Case BabelFiles.FileType.SG_0001327_StrandCoFactoring
				ReadFile = Read_0001327_StrandCoFactoring
			Case BabelFiles.FileType.PGS_Sweden
				ReadFile = ReadPGS
			Case BabelFiles.FileType.Gjensidige_S2000_PAYMUL
				ReadFile = ReadGjensidige_S2000_PAYMULFile
			Case BabelFiles.FileType.AquariusIncoming
				ReadFile = CStr(ReadAquariusIncoming)
			Case BabelFiles.FileType.SG_SaldoFil_Aq
				ReadFile = ReadSGFinans_SaldoAq
			Case BabelFiles.FileType.SG_FakturaHistorikFil_Aq
				ReadFile = ReadSGFinans_FakturaHistorikkAq
			Case BabelFiles.FileType.SG_0004889_MillbaFactoring
				ReadFile = Read_0004889_MillbaCustomer
			Case BabelFiles.FileType.SG_0004193_T�nsbergRammeFactoring
				ReadFile = Read_0004193_T�nsbergRammeCustomer
			Case BabelFiles.FileType.SG_0001471_WebergFactoring
				ReadFile = Read_0001471_WebergFactoring
			Case BabelFiles.FileType.SG_0001471_WebergCustomer
				ReadFile = Read_0001471_WebergCustomer
			Case BabelFiles.FileType.SG_NORFinans_Factoring
				ReadFile = Read_SG_NORFinans_Factoring
			Case BabelFiles.FileType.Biomar_UK
				ReadFile = Read_Biomar_UK
			Case BabelFiles.FileType.ActavisExcel_XLS
				ReadFile = ReadActavisExcelXLS()
			Case BabelFiles.FileType.PayEx_CSV
				ReadFile = ReadPayEx_CSV()
			Case BabelFiles.FileType.DnBNOR_US_Penta
				ReadFile = ReadDnBNOR_US_Penta
			Case BabelFiles.FileType.RECSingapore
				ReadFile = ReadRECSingapore
			Case BabelFiles.FileType.DnV_UK_Sage
				ReadFile = Read_DnV_UK_Sage
			Case BabelFiles.FileType.Seajack_Wages
				ReadFile = ReadSeajackWages
			Case BabelFiles.FileType.KongsbergAutomotive_BACS
				ReadFile = Read_KongsbergAutomotive_BACS
			Case BabelFiles.FileType.Gjensidige_Utbytte
				ReadFile = ReadGjensidigeUtbytte()
			Case BabelFiles.FileType.Ulefos_Salary_FI
				ReadFile = ReadUlefos_Salary_FI()
			Case BabelFiles.FileType.NemkoCanadaSalary
				ReadFile = ReadNemkoCanadaSalary
                ' XNET 23.06.2011
            Case BabelFiles.FileType.AkerSolutions_UK
                ReadFile = ReadAkerSolutions_UK
                ' XNET 29.06.2011
            Case BabelFiles.FileType.MT942_Odin
                ReadFile = ReadMT942_Odin
                ' XNET 14.07.2011
            Case BabelFiles.FileType.DnV_UK_BACS
                ReadFile = Read_DnV_UK_BACS
                ' XNET 15.07.2011
            Case BabelFiles.FileType.Morpol_UK_Bacs
                ReadFile = Read_Morpol_UK_BACS
                ' XNET 11.08.2011
            Case BabelFiles.FileType.Givertelefon
                ReadFile = ReadGivertelefon
                ' XNET 15.11.2011
            Case BabelFiles.FileType.Polyeurope_UK
                ReadFile = Read_Polyeurope_UK
                'XNET 10.10.2012
            Case BabelFiles.FileType.Vingcard_netting
                ReadFile = ReadVingcardNettingFile
                ' XNET 05.03.2013
            Case BabelFiles.FileType.Spike_UK
                ReadFile = ReadSpike_UK
                ' XNET 09.04.2013
            Case BabelFiles.FileType.Vroon_UK
                ReadFile = ReadVroon_UK
                ' XNET 10.05.2013
            Case BabelFiles.FileType.Faroe
                ReadFile = ReadFaroe_UK
                ' XNET 13.05.2013
            Case BabelFiles.FileType.AndresenBil
                ReadFile = ReadAndresenBil()
                ' XNET 29.06.2013
            Case BabelFiles.FileType.ColArt_UK
                ReadFile = ReadColArt_UK
            Case BabelFiles.FileType.Phuel_Oil, BabelFiles.FileType.Sage_Salaries
                ReadFile = ReadPhuel_Oil()

                ' XNET 14.10.2010 Added ReadSAP_PAYEXT_PEXR2002
            Case BabelFiles.FileType.SAP_PAYEXT_PEXR2002
                ReadFile = ReadSAP_PAYEXT_PEXR2002
                ' XNET 29.10.2010 added format
            Case BabelFiles.FileType.DnBNOR_Connect_Test
                ReadFile = ReadDnBNOR_Connect_Test()
                'XNET 11.11.2011 - Added format
            Case BabelFiles.FileType.Conecto_Dekkmann
                ReadFile = ReadConecto_Dekkmann()
            Case BabelFiles.FileType.GC_Rieber_UK
                ReadFile = ReadGC_Rieber_UK()
            Case BabelFiles.FileType.SeaCargo_UK
                ReadFile = ReadSeaCargo_UK
                ' XokNET 05.12.2011
            Case BabelFiles.FileType.Falck_UK
                ReadFile = ReadFalck_UK
                ' XokNET 20.12.2011
            Case BabelFiles.FileType.DnBNOR_US_ACH_820
                ReadFile = Read_DnBNORUS_ACH_820
                ' Gjensidige convert from different formats to JD Edwards
                ' -------------------------------------------------------
            Case BabelFiles.FileType.GjensidigeJDE_Basware
                ReadFile = ReadGjensidigeJDE_Basware()
            Case BabelFiles.FileType.GjensidigeJDE_FileNet
                ReadFile = ReadGjensidigeJDE_FileNet()
            Case BabelFiles.FileType.GjensidigeJDE_HB01
                ReadFile = ReadGjensidigeJDE_HB01()
            Case BabelFiles.FileType.GjensidigeF2100_SE_DK
                ReadFile = ReadGjensidigeJDE_F2100_SE_DK()
            Case BabelFiles.FileType.GjensidigeJDE_CSV_S12
                ReadFile = ReadGjensidigeJDE_CSV_S12()
            Case BabelFiles.FileType.GjensidigeJDE_HB01_H1
                ReadFile = ReadGjensidigeJDE_HB01_H1
            Case BabelFiles.FileType.GjensidigeJDE_FileNet_XLedger
                ReadFile = ReadGjensidigeJDE_FileNet_XLedger()
            Case BabelFiles.FileType.GjensidigeJDE_F2100_SE_DK_GL02b
                ReadFile = ReadGjensidigeJDE_F2100_SE_DK_GL02b()
            Case BabelFiles.FileType.GjensidigeJDE_Aditro
                ReadFile = ReadGjensidigeJDE_Aditro()
            Case BabelFiles.FileType.GjensidigeJDE_IDIT
                ReadFile = ReadGjensidigeJDE_IDIT()
            Case BabelFiles.FileType.GjensidigeJDE_P2000_XLedger
                ReadFile = ReadGjensidigeJDE_P2000_XLedger()
            Case BabelFiles.FileType.GjensidigeJDE_INPAS
                ReadFile = ReadGjensidigeJDE_INPAS()
            Case BabelFiles.FileType.GjensidigeJDE_NICE
                ' bruk samme input som for INPAS
                ReadFile = ReadGjensidigeJDE_INPAS()

                ' ---- End Gjensidige JDE_ -------------------------------

                'XokNET 12.04.2012
            Case BabelFiles.FileType.DTAZV
                ReadFile = ReadDTAZV()
                ' XNET 19.03.2012
            Case BabelFiles.FileType.Handicare_UK
                ReadFile = ReadHandicare_UK
                ' XNET 24.04.2012
            Case BabelFiles.FileType.Odenberg
                ReadFile = ReadOdenberg
                ' XOKNET 13.05.2013
            Case BabelFiles.FileType.AndresenBil
                ReadFile = ReadAndresenBil
                ' XNET 09.10.2012
            Case BabelFiles.FileType.SEB_CI_Incoming
                ReadFile = ReadSEB_CI_Incoming()
                ' XNET 30.10.2012
            Case BabelFiles.FileType.DanskeBankCollection
                ReadFile = ReadDanskeBankCollection
                ' XNET 30.10.2012
            Case BabelFiles.FileType.Avtalegiro
                ReadFile = ReadAvtalegiroFile
                ' XNET 14.11.2012
            Case BabelFiles.FileType.SEBScreen_UK
                ReadFile = ReadSEBScreen_UK
                ' XNET 05.03.2013
            Case BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService
                ReadFile = ReadNordea_SE_Plusgiro_FakturabetalningService
                ' XNET 10.02.2013
            Case BabelFiles.FileType.Total_IN
                ReadFile = ReadTotal_IN()
                ' 11.04.2017 - added SEB_Sisu . it was missing!!!!
            Case BabelFiles.FileType.SEB_Sisu
                ReadFile = ReadSEB_Sisu()

                ' XNET 06.11.2012
            Case BabelFiles.FileType.Elavon_Specification
                ReadFile = ReadElavonFile
                ' XNET 10.01.2013
            Case BabelFiles.FileType.Rapp_Ecosse_Bacs
                ReadFile = ReadRappEcosse_UK
                ' XNET 21.01.2013
            Case BabelFiles.FileType.Duni_Salary_UK
                ReadFile = ReadDuni_Salary_UK
                ' XNET 28.02.2013
            Case BabelFiles.FileType.TGS_UK
                ReadFile = ReadTGS_UK
                ' XNET 17.07.2013 added SageReport
            Case BabelFiles.FileType.SageReport
                ReadFile = Read_SageReport
                ' XokNET 13.08.2013 added DRiVR UK format
            Case BabelFiles.FileType.DRiVR_UK
                ReadFile = ReadDRiVR_UK
                ' XokNET 21.01.2014
            Case BabelFiles.FileType.Vroon_Scotland
                ReadFile = ReadVroon_Scotland
            Case BabelFiles.FileType.Gjensidige_kontovask
                ReadFile = ReadGjensidige_Kontovask
            Case BabelFiles.FileType.ColArt_Moorbrook
                ReadFile = ReadColArt_Moorbrook()
                'XokNET 07.10.2014
            Case BabelFiles.FileType.NordeaCorporateFilePayments
                ReadFile = ReadCorporateFilePayments()
            Case BabelFiles.FileType.HandicareBACS
                ReadFile = ReadHandicareBACS()
            Case BabelFiles.FileType.BankAxess_DebitCard_XLS
                ReadFile = ReadExcelXLS()
            Case BabelFiles.FileType.DNBSingapore_Standard
                ReadFile = ReadDNBSingapore_Standard()
            Case BabelFiles.FileType.DOF_Supplier
                ReadFile = ReadDOF_Supplier()
            Case BabelFiles.FileType.DOF_Salary
                ReadFile = ReadDOF_Salary()
            Case BabelFiles.FileType.Vipps_Excel
                ReadFile = ReadVippsXLS()
            Case BabelFiles.FileType.Excel_Incoming_DIV
                ReadFile = ReadExcel_IncomingXLS()
            Case BabelFiles.FileType.Lindorff_Innbetaling
                ReadFile = ReadLindorff_Incoming()
            Case BabelFiles.FileType.Lindorff_Gebyr
                ReadFile = ReadLindorff_Gebyr()
            Case BabelFiles.FileType.Elavon_2017
                ReadFile = ReadElavon_2017()
            Case BabelFiles.FileType.FlyWire
                ReadFile = ReadFlyWire_Incoming()
            Case BabelFiles.FileType.SkyBridge_BACS
                ReadFile = ReadSkyBridge_BACS()
            Case BabelFiles.FileType.PayPal_ActiveBrands
                ReadFile = CStr(ReadPayPal_activebrands())
            Case BabelFiles.FileType.Musto_BACS
                ReadFile = ReadMusto_BACS()
            Case BabelFiles.FileType.Confide_Excel
                ReadFile = ReadConfide_Excel()
            Case BabelFiles.FileType.Factolink
                ReadFile = ReadFactolink
            Case BabelFiles.FileType.Videotel_BACS
                ReadFile = ReadVideotel_BACS()
            Case BabelFiles.FileType.Vipps_Nets
                ReadFile = ReadVipps_Nets()
            Case BabelFiles.FileType.TridentSalary
                ReadFile = ReadTrident_Salary()

        End Select
		
		'ReadFile = True
	End Function
	
	Private Function ReadTelepayFile() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		Dim dPaymentDateForMass As Date
		Dim sPayCodeForMass, sPayTypeForMass As String
		Dim sPaymentREFOwnForMass As String
		Dim bx As Boolean
		Dim sOldAccountNo As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			bCreateObject = False
			
			Select Case Mid(sImportLineBuffer, 41, 8)
				'BETFOR00 - Start batch, have to store information
				Case "BETFOR00"
					If bFromLevelUnder Then
						Exit Do
					Else
						'Extract data from sBImportLineBuffer given from function Import
						nSequenceNoStart = Val(Mid(sImportLineBuffer, 71, 4))
						sStatusCode = Mid(sImportLineBuffer, 4, 2)
						'sTransDate = Mid$(sImportLineBuffer, 10, 4)
						sI_EnterpriseNo = Mid(sImportLineBuffer, 49, 11)
						sI_Branch = Mid(sImportLineBuffer, 60, 11)
                        sOperatorID = Mid(sImportLineBuffer, 115, 11)
						sVersion = Mid(sImportLineBuffer, 95, 10)
						sFormatType = "TELEPAY"
						bReadLine = True
						bCreateObject = True
					End If
					
					'BETFOR01 - Start international payment, have to create a payment-object
				Case "BETFOR01"
					If bFromLevelUnder Then
						bReadLine = False
						bCreateObject = True
					Else
						'Feil importformatet
					End If
					
					'BETFOR21 - Start domestic payment, have to create a payment-object
				Case "BETFOR21"
					'M� vi legge inn egen for massetrans type 21?
					If bFromLevelUnder Then
						bReadLine = False
						bCreateObject = True
					Else
						'Feil importformatet
					End If
					
					
					'BETFOR22 - New mass-payment
				Case "BETFOR22"
					dPaymentDateForMass = DateSerial(CInt(Left(oPayment.DATE_Payment, 4)), CInt(Mid(oPayment.DATE_Payment, 5, 2)), CInt(Right(oPayment.DATE_Payment, 2)))
					sPayCodeForMass = oPayment.PayCode
					sPayTypeForMass = oPayment.PayType
					sPaymentREFOwnForMass = oPayment.REF_Own
					bReadLine = False
					bCreateObject = True
					
					
					'BETFOR99 - End of batch
				Case "BETFOR99"
					nSequenceNoEnd = Val(Mid(sImportLineBuffer, 71, 4))
					If Len(Trim(Mid(sImportLineBuffer, 81, 2))) <> 0 Then
						dDATE_Production = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(Mid(sImportLineBuffer, 81, 2)), CInt(Mid(sImportLineBuffer, 83, 2)))
					Else
						' No production date given, this goes for DnB Avregningsretur
						' Use todays date
						dDATE_Production = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(VB6.Format(Now, "MM")), CInt(VB6.Format(Now, "DD")))
					End If
					Exit Do
					
					'All other content would indicate error
				Case Else
					'All other BETFOR-types gives error
					'Error # 10010-002
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-002")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
                ' XNET 06.07.2012 extra columns in file from XELLIA
                If oCargo.Special = "XELLIA" Then
                    sImportLineBuffer = ReadTelepayXelliaRecord(oFile)
                Else
                    sImportLineBuffer = ReadTelepayRecord(oFile) ', oProfile)
                End If

                ' New 26.11.02, due to problems with empty batches
				If Mid(sImportLineBuffer, 41, 8) = "BETFOR99" Then
					bCreateObject = False
				End If
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				'Have to set some properties from the BETFOR21-record if it is a 22-record
				If Mid(sImportLineBuffer, 41, 8) = "BETFOR22" Then
					'FIXED by Janp 27.09.01 from Set oPayment.DATE_Payment ....
					oPayment.DATE_Payment = VB6.Format(dPaymentDateForMass, "YYYYMMDD") 'CVar(dPaymentDateForMass)
					oPayment.PayCode = CObj(sPayCodeForMass)
					oPayment.PayType = CObj(sPayTypeForMass)
					oPayment.REF_Own = CObj(sPaymentREFOwnForMass)
				End If
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
			'Store end of batch data here (if needed)...
		Loop 
		
		If sStatusCode = "00" Or StatusCode = "01" Then
			bx = AddInvoiceAmountOnBatch()
		Else
			bx = AddInvoiceAmountOnBatch()
			bx = AddTransferredAmountOnBatch()
		End If
		
		sOldAccountNo = ""
        ' XNET 06.06.2013
        If oCargo.Special <> "KONGSBERGAUTOMOTIVE" Then
            If iImportFormat = BabelFiles.FileType.TelepayTBIO Then
                ' added 25.03.2015 for SEB ISO20022
                bThisIsTBIO = True
                For Each oPayment In Payments
                    If IsPaymentDomestic(oPayment) Then
                        oPayment.PayType = "D"
                    End If
                Next oPayment
            End If
        End If

        ReadTelepayFile = sImportLineBuffer
    End Function
	Private Function ReadTelepay2File(Optional ByRef TelepayType As String = "") As String
		'-----------------------------------------------------------------------
		' 30.12.2008: Added parameter TelepayType.
		'             Used so far for DnBNOR Telepay+, then the parameter = "+"
		'-----------------------------------------------------------------------
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		Dim dPaymentDateForMass As Date
		Dim sPayCodeForMass, sPayTypeForMass As String
		Dim sPaymentREFOwnForMass As String
		Dim bx As Boolean
        'XNET 16.12.2010, to fetch content for Telepay+ for BETFOR22
        Dim sBANK_I_SWIFTCode As String
        Dim sBank_SwiftCode As String
        Dim sE_Zip As String
        Dim sE_CountryCode As String
        Dim sREF_EndToEnd As String
        Dim sBANK_BranchNo As String
        Dim sInvoiceCurrencyForMass As String
        Dim sTransferCurrencyForMass As String

		bFromLevelUnder = False
		
        'Try ' 02.08.2018 - Testing Try ... Catch

        Do While True
            bReadLine = False
            bCreateObject = False

            Select Case Mid(sImportLineBuffer, 41, 8)
                'BETFOR00 - Start batch, have to store information
                Case "BETFOR00"
                    If bFromLevelUnder Then
                        Exit Do
                    Else
                        'Extract data from sBImportLineBuffer given from function Import
                        nSequenceNoStart = Val(Mid(sImportLineBuffer, 71, 4))
                        sStatusCode = Mid(sImportLineBuffer, 4, 2)
                        ' XokNET 19.11.2013
                        ' We can't test Mid$(sImportLineBuffer, 4, 2) only. For "avvisningsretur" we can have status 00 in BETFOR00
                        ' Must instead test on AH200TBRI, to find "R"
                        If sStatusCode = "00" Then
                            If Mid$(sImportLineBuffer, 8, 1) = "R" Then
                                ' classify it as "mottaksretur"
                                sStatusCode = "01"
                            End If
                        End If

                        'sTransDate = Mid$(sImportLineBuffer, 10, 4)
                        sI_EnterpriseNo = Mid(sImportLineBuffer, 49, 11)
                        sI_Branch = Mid(sImportLineBuffer, 60, 11)
                        sOperatorID = Mid(sImportLineBuffer, 115, 11)
                        sVersion = Mid(sImportLineBuffer, 95, 10)
                        ' XNET 17.11.2010 added ref_owm
                        sREF_Own = Trim$(Mid$(sImportLineBuffer, 297, 15)) 'OWN REFERENCE BATCH 297;311 15  ALPHA

                        If TelepayType = "+" Then
                            sFormatType = "TELEPAY+"
                        Else
                            sFormatType = "TELEPAY2"
                        End If
                        bReadLine = True
                        bCreateObject = True
                    End If

                    'BETFOR01 - Start international payment, have to create a payment-object
                Case "BETFOR01"
                    If bFromLevelUnder Then
                        bReadLine = False
                        bCreateObject = True
                    Else
                        'Feil i importformatet
                    End If

                    'BETFOR21 - Start domestic payment, have to create a payment-object
                Case "BETFOR21"
                    If bFromLevelUnder Then
                        bReadLine = False
                        bCreateObject = True
                    Else
                        'Feil i importformatet
                    End If


                    'BETFOR22 - New mass-payment
                Case "BETFOR22"
                    dPaymentDateForMass = DateSerial(CInt(Left(oPayment.DATE_Payment, 4)), CInt(Mid(oPayment.DATE_Payment, 5, 2)), CInt(Right(oPayment.DATE_Payment, 2)))
                    sPayCodeForMass = oPayment.PayCode
                    sPayTypeForMass = oPayment.PayType
                    sPaymentREFOwnForMass = oPayment.REF_Own
                    sInvoiceCurrencyForMass = oPayment.MON_InvoiceCurrency
                    sTransferCurrencyForMass = oPayment.MON_TransferCurrency

                    If TelepayType = "+" Then
                        'XNET 16.12.2010, to fetch content for Telepay+ for BETFOR22
                        sBANK_I_SWIFTCode = oPayment.BANK_I_SWIFTCode
                        sBank_SwiftCode = oPayment.BANK_SWIFTCode
                        sE_Zip = oPayment.E_Zip
                        sE_CountryCode = oPayment.E_CountryCode
                        sREF_EndToEnd = oPayment.REF_EndToEnd
                        sBANK_BranchNo = oPayment.BANK_BranchNo
                    End If
                    bReadLine = False
                    bCreateObject = True


                    'BETFOR99 - End of batch
                Case "BETFOR99"
                    nSequenceNoEnd = Val(Mid(sImportLineBuffer, 71, 4))
                    If Len(Trim(Mid(sImportLineBuffer, 81, 2))) <> 0 Then
                        dDATE_Production = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(Mid(sImportLineBuffer, 81, 2)), CInt(Mid(sImportLineBuffer, 83, 2)))
                    Else
                        ' No production date given, this goes for DnB Avregningsretur
                        ' Use todays date
                        dDATE_Production = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(VB6.Format(Now, "MM")), CInt(VB6.Format(Now, "DD")))
                    End If
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'All other BETFOR-types gives error
                    'Error # 10010-002
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-002")

                    Exit Do

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                ' New 27.03.06
                If oCargo.Special = "LINE80" Then
                    sImportLineBuffer = ReadTelepayRecord(oFile, True) ', oProfile)
                Else
                    ' added TelepayPlus 30.12.2008
                    If TelepayType = "+" Then
                        sImportLineBuffer = ReadTelepayPlusRecord(oFile)
                    Else
                        sImportLineBuffer = ReadTelepayRecord(oFile) ', oProfile)
                    End If
                End If

                ' New 26.11.02, due to problems with empty batches
                If Mid(sImportLineBuffer, 41, 8) = "BETFOR99" Then
                    bCreateObject = False
                End If
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                'Have to set some properties from the BETFOR21-record if it is a 22-record
                If Mid(sImportLineBuffer, 41, 8) = "BETFOR22" Then
                    'FIXED by Janp 27.09.01 from Set oPayment.DATE_Payment ....
                    oPayment.DATE_Payment = VB6.Format(dPaymentDateForMass, "YYYYMMDD") 'CVar(dPaymentDateForMass)
                    oPayment.PayCode = CObj(sPayCodeForMass)
                    oPayment.PayType = CObj(sPayTypeForMass)
                    oPayment.REF_Own = CObj(sPaymentREFOwnForMass)
                    oPayment.MON_InvoiceCurrency = sInvoiceCurrencyForMass
                    oPayment.MON_TransferCurrency = sTransferCurrencyForMass
                End If
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                ' XNET 18.11.2010
                If TelepayType = "+" Then
                    ' find B-level own reference down in payment, but must be set at batch-level (if not already set in BETFOR00)
                    If EmptyString(sREF_Own) Then
                        ' (If already in use from BETFOR00 we have no room for B-level ref own)
                        sREF_Own = oCargo.REF
                    End If
                    ' XNET 16.12.2010 several lines to payment for Telepay+
                    If oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                        oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                        oPayment.MON_TransferCurrency = oCargo.MONCurrency
                        oPayment.BANK_I_SWIFTCode = sBANK_I_SWIFTCode
                        If EmptyString(oPayment.BANK_SWIFTCode) Then
                            oPayment.BANK_SWIFTCode = sBank_SwiftCode
                        End If
                        If EmptyString(oPayment.E_Zip) Then
                            oPayment.E_Zip = sE_Zip
                        End If
                        If EmptyString(oPayment.E_CountryCode) Then
                            oPayment.E_CountryCode = sE_CountryCode
                        End If
                        If EmptyString(oPayment.BANK_BranchNo) Then
                            oPayment.BANK_BranchNo = sBANK_BranchNo
                            If EmptyString(oPayment.BANK_BranchNo) And oPayment.E_CountryCode = "GB" And Len(oPayment.BANK_SWIFTCode) = 6 Then
                                ' in betfor22, room only for one bankcode, which will be Sortcode in GB
                                oPayment.BANK_BranchNo = oPayment.BANK_SWIFTCode
                                oPayment.BANK_SWIFTCode = ""
                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                            End If
                        End If
                    End If

                End If

                bFromLevelUnder = True
            End If

            'Store end of batch data here (if needed)...
        Loop

        If sStatusCode = "00" Or StatusCode = "01" Then
            bx = AddInvoiceAmountOnBatch()
        Else
            bx = AddInvoiceAmountOnBatch()
            bx = AddTransferredAmountOnBatch()
        End If

        'Catch ex As vbBabel.Payment.PaymentException ' - Testing Try ... Catch

        '    Throw New vbBabel.Payment.PaymentException(ex.Message, ex, ex.ErrorLineNumber.Trim, ex.PaymentInfo, ex.LastLineRead, ex.FilenameImported)

        'Catch ex As Exception

        '    Throw New Exception(ex.Message, Nothing)

        'End Try

        ReadTelepay2File = sImportLineBuffer
    End Function
	Private Function ReadDirremFile() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			' 13.01.06 - Opened for mix of DirRem and Autogiro - ref Hafslund
			If Left(sImportLineBuffer, 4) = "NY01" Or Left(sImportLineBuffer, 4) = "NY02" Then
				Exit Do ' Jump to BabelFile, and create a Autogiro-batch
			End If
			
			' Nwe by JanP 06.08.02
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			Select Case Mid(sImportLineBuffer, 7, 2)
				'20 - Start of batch, have to store information
				Case "20"
					If bFromLevelUnder Then
						' have read a 20-record in batch (new batch)
						' jump up, to create object
						Exit Do
					Else
						'Extract data from sImportLineBuffer given from function Import
						sBatch_ID = Mid(sImportLineBuffer, 18, 7) 'Oppdragsnummer
						sREF_Own = Mid(sImportLineBuffer, 9, 9)
						sI_Account = Mid(sImportLineBuffer, 25, 11)
						sFormatType = "DIRREM"
						bReadLine = True
						bCreateObject = False
						
					End If
					
					'30 - Bel�pspost 1
				Case "30"
					bReadLine = False
					bCreateObject = True
					
					'88 - End of Batch
				Case "88"
					' NY000088 - Sluttrecord oppdrag
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 25, 17)) ' Sum amount in batch, NB! in Kroner
					bAmountSetInvoice = True
					If sStatusCode = "02" Then
						nMON_TransferredAmount = nMON_InvoiceAmount
						bAmountSetTransferred = True
					End If
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 9, 8)) ' Antall transer i filen
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 17, 8)) ' Antall records i filen
					
					
					bReadLine = True
					bCreateObject = False
					bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up
					
					
				Case "89" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadDirremRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadDirremRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			'Store end of batch data here (if needed)...
		Loop 
		
		ReadDirremFile = sImportLineBuffer
	End Function
	Private Function ReadBOLSFile() As String
		' Used so far for SG Finans.
		' NB! Not all fields are used
		'----------------------------
		
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		Dim nTmp As Double
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			Select Case Mid(sImportLineBuffer, 1, 2)
				
				'13 - Bunthode
				Case "13"
					If bFromLevelUnder Then
						' have read a 13-record in batch (new batch)
						' jump up, to create object
						Exit Do
					Else
						'Extract data from sImportLineBuffer given from function Import
						sBatch_ID = Mid(sImportLineBuffer, 9, 4) 'Banknumber
						sFormatType = "BOLS"
						bReadLine = True
						bCreateObject = False
						
					End If
					
					'15 - Bel�pstransaksjon
				Case "15"
					bReadLine = False
					bCreateObject = True
					
					'17 - Buntsumtransaksjon
				Case "17"
					' One amount for totaldebit and one for totalcredit
					nTmp = TranslateSignedAmount(Mid(sImportLineBuffer, 9, 13)) ' Debit, minusamounts
					nMON_InvoiceAmount = -nTmp
					
					nTmp = TranslateSignedAmount(Mid(sImportLineBuffer, 22, 13)) ' Credit, plusamounts
					nMON_InvoiceAmount = nMON_InvoiceAmount + nTmp
					
					bAmountSetInvoice = True
					nMON_TransferredAmount = nMON_InvoiceAmount
					bAmountSetTransferred = True
					
					bReadLine = True
					bCreateObject = False
					bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up
					
					
				Case "19" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadBOLSRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadBOLSRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			'Store end of batch data here (if needed)...
		Loop 
		
		ReadBOLSFile = sImportLineBuffer
		
	End Function
	Private Function ReadSGFINANS_BOLSFile() As String
		' Special for BOLS-records from/to Aquarius for SG Finans.
		'---------------------------------------------------------
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		
		bFromLevelUnder = False
		
		Do 
			bReadLine = False
			
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			Select Case xDelim(sImportLineBuffer, ";", 1)
				
				Case "BOLS" 'BOLS paymentrecord
					bReadLine = False
					bCreateObject = True
					
				Case "BOLSTOTAL" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadSGFINANS_BOLSRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadSGFINANS_BOLSRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
		Loop 
		
		ReadSGFINANS_BOLSFile = sImportLineBuffer
		
	End Function
	Private Function ReadSGClientReportingFile() As String
		' Clientreporting from Aquarius - records will be exported to OCR and SDV-file
		'-----------------------------------------------------------------------------
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		
		bFromLevelUnder = False
		
		Do 
			bReadLine = False
			
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			Select Case xDelim(sImportLineBuffer, ";", 1)
				
				Case "OCR" 'BOLS paymentrecord
					bReadLine = False
					bCreateObject = True
				Case "OTHER" 'Not OCR, for SDV-file
					bReadLine = False
					bCreateObject = True
					
				Case "OCRTOTAL" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadSGClientReportingRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadSGClientReportingRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
		Loop 
		
		ReadSGClientReportingFile = sImportLineBuffer
		
	End Function
	Private Function ReadLM_Domestic_Finland() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		Dim sTmpDate As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			' Same layout for both LM02 and LM03
			Select Case Mid(sImportLineBuffer, 5, 1)
				Case "0"
					' Header record
					sTmpDate = "20" & Mid(sImportLineBuffer, 30, 6) 'File creation date
					dDATE_Production = DateSerial(CInt(Left(sTmpDate, 4)), CInt(Mid(sTmpDate, 5, 2)), CInt(Right(sTmpDate, 2)))
					
					' Read another line
					bReadLine = True
				Case "1"
					' Transactionrecord
					bCreateObject = True
					
					'9 - End of Batch
				Case "9"
					' LM029
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 42, 13)) ' Sum amount in batch, NB! in Kroner
					bAmountSetInvoice = True
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 36, 6)) ' Antall transer i filen
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 36, 6)) ' Antall records i filen
					
					
					bReadLine = False
					bCreateObject = False
					bFromLevelUnder = True
					Exit Do ' Jump to BabelFile
					
				Case ""
					bReadLine = True
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadLM_Domestic_FinlandRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadLM_Domestic_FinlandRecord(oFile, oCargo.Special) '04.02.05; Added Special for MI Chemicals, Finland
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
			If bFromLevelUnder And oFile.AtEndOfStream Then
				Exit Do
			End If
		Loop 
		AddInvoiceAmountOnBatch()
		
		ReadLM_Domestic_Finland = sImportLineBuffer
	End Function
	Private Function ReadLUM_International_Finland() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		Dim sTmpDate As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			' Same layout for both LM02 and LM03
			Select Case Mid(sImportLineBuffer, 5, 1)
				Case "0"
					' Header record
					sTmpDate = "20" & Mid(sImportLineBuffer, 27, 6) 'File creation date
					dDATE_Production = DateSerial(CInt(Left(sTmpDate, 4)), CInt(Mid(sTmpDate, 5, 2)), CInt(Right(sTmpDate, 2)))
					
					' Read another line
					bReadLine = True
				Case "1"
					' Transactionrecord
					bCreateObject = True
					
					'9 - End of Batch
				Case "9"
					' LM029
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 37, 15)) ' Sum amount in batch, NB! in Kroner
					bAmountSetInvoice = True
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 27, 6)) ' Antall transer i filen
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 27, 5)) ' Antall records i filen
					
					
					bReadLine = False
					bCreateObject = False
					bFromLevelUnder = True
					Exit Do ' Jump to BabelFile
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadLUM_International_FinlandRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadLUM_International_FinlandRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
		Loop 
		'AddInvoiceAmountOnBatch
		
		ReadLUM_International_Finland = sImportLineBuffer
	End Function
	Private Function ReadPeriodiskaBetalningarFinland() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		Dim sTmpDate As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			Select Case Mid(sImportLineBuffer, 1, 1)
				Case "1"
					' Transaktionspost
					bReadLine = False
					bCreateObject = True
					
					'4 - End of Batch
				Case "4"
					bReadLine = False
					bCreateObject = False
					bFromLevelUnder = True
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 35, 11)) ' Sum amount in batch, in EUR
					bAmountSetInvoice = True
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 65, 6)) ' Antall transer i filen
					
					Exit Do ' Jump to BabelFile
					
					
				Case Else
					'All other record-types gives error
					'Error # 10010-006
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")
					
					Exit Do
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
			If bFromLevelUnder And oFile.AtEndOfStream Then
				Exit Do
			End If
		Loop 
		AddInvoiceAmountOnBatch()
		
		ReadPeriodiskaBetalningarFinland = sImportLineBuffer
	End Function
	Private Function ReadAutogiroFile() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			' 29.12.05 - Opened for mix of autogiro and OCR - ref Kennelklubben
			' 13.01.06 - Opened also for DirRem-mix
			' 22.12.06  - Added Avtalegiro (NY21)
			If Left(sImportLineBuffer, 4) = "NY09" Or Left(sImportLineBuffer, 4) = "NY04" Or Left(sImportLineBuffer, 4) = "NY21" Then
				Exit Do ' Jump to BabelFile, to create an OCR-batch or DirRem-batch
				
			End If
			
			Select Case Mid(sImportLineBuffer, 7, 2)
				'20 - Start of batch, have to store information
				Case "20"
					If bFromLevelUnder Then
						' have read a 20-record in batch (new batch)
						' jump up, to create object
						Exit Do
					Else
						'Extract data from sImportLineBuffer given from function Import
						sBatch_ID = Mid(sImportLineBuffer, 18, 7) '
						sREF_Own = Mid(sImportLineBuffer, 9, 9)
						sI_Account = Mid(sImportLineBuffer, 25, 11)
						
						sFormatType = "Autogiro"
						'sVersion = "OCR"   ' fjernes
						
						bReadLine = True
						bCreateObject = False
						
					End If
					
					'30 - Bel�pspost 1
				Case "30"
					bReadLine = False
					bCreateObject = True
					
					'88 - End of Batch
				Case "88"
					' NY000088 - Sluttrecord oppdrag
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 25, 17)) ' Sum amount in batch, NB! in Kroner
					nMON_TransferredAmount = nMON_InvoiceAmount
					bAmountSetInvoice = True
					bAmountSetTransferred = True
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 9, 8)) ' Antall transer i filen
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 17, 8)) ' Antall records i filen
					dDATE_Production = DateSerial(CInt(Mid(sImportLineBuffer, 46, 2)), CInt(Mid(sImportLineBuffer, 44, 2)), CInt(Mid(sImportLineBuffer, 42, 2)))
					
					bReadLine = True ' read 89 record
					bCreateObject = False
					bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up
					
				Case "89" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-010
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadAutogiroRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadAutogiroRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 
		
		ReadAutogiroFile = sImportLineBuffer
	End Function
	Private Function ReadAutogiroEngangs() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			' 29.12.05 - Opened for mix of autogiro and OCR - ref Kennelklubben
			' 13.01.06 - Opened also for DirRem-mix
			' 22.12.06  - Added Avtalegiro (NY21)
			If Left(sImportLineBuffer, 4) = "NY09" Or Left(sImportLineBuffer, 4) = "NY04" Or Left(sImportLineBuffer, 4) = "NY21" Or Left(sImportLineBuffer, 4) = "NY01" Then
				Exit Do ' Jump to BabelFile, to create an OCR-batch or DirRem-batch
				
			End If
			
			Select Case Mid(sImportLineBuffer, 7, 2)
				'20 - Start of batch, have to store information
				Case "20"
					If bFromLevelUnder Then
						' have read a 20-record in batch (new batch)
						' jump up, to create object
						Exit Do
					Else
						'Extract data from sImportLineBuffer given from function Import
						sBatch_ID = Mid(sImportLineBuffer, 18, 7) '
						sREF_Own = Mid(sImportLineBuffer, 9, 9)
						sI_Account = Mid(sImportLineBuffer, 25, 11)
						
						sFormatType = "Autogiro Engangsfullmakt"
						'sVersion = "OCR"   ' fjernes
						
						bReadLine = True
						bCreateObject = False
						
					End If
					
					'30 - Bel�pspost 1
				Case "30"
					bReadLine = False
					bCreateObject = True
					
					'88 - End of Batch
				Case "88"
					' NY000088 - Sluttrecord oppdrag
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 25, 17)) ' Sum amount in batch, NB! in Kroner
					nMON_TransferredAmount = nMON_InvoiceAmount
					bAmountSetInvoice = True
					bAmountSetTransferred = True
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 9, 8)) ' Antall transer i filen
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 17, 8)) ' Antall records i filen
					dDATE_Production = DateSerial(CInt(Mid(sImportLineBuffer, 46, 2)), CInt(Mid(sImportLineBuffer, 44, 2)), CInt(Mid(sImportLineBuffer, 42, 2)))
					
					bReadLine = True ' read 89 record
					bCreateObject = False
					bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up
					
				Case "89" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-010
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadAutogiroRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadAutogiroRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 
		
		ReadAutogiroEngangs = sImportLineBuffer
	End Function
	Private Function ReadOCRFile() As String
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			' 29.12.05 - Opened for mix of autogiro and OCR - ref Kennelklubben
			If Left(sImportLineBuffer, 4) = "NY01" Or Left(sImportLineBuffer, 4) = "NY02" Then
				'iImportFormat = FileType.AutoGiro
				Exit Do ' Jump to BabelFile, and create a Autogiro-batch
				'''''ReadAutogiroFile
			End If
			
			Select Case Mid(sImportLineBuffer, 7, 2)
				'20 - Start of batch, have to store information
				Case "20"
					If bFromLevelUnder Then
						' have read a 20-record in batch (new batch)
						' jump up, to create object
						Exit Do
					Else
						'Extract data from sImportLineBuffer given from function Import
						sBatch_ID = Mid(sImportLineBuffer, 18, 7) '
						sREF_Own = Mid(sImportLineBuffer, 9, 9)
						sI_Account = Mid(sImportLineBuffer, 25, 11)

                        '23.05.2018 - Added next if for Fjordkraft - egeninkasso
                        If oCargo.Special = "FJORDKRAFT" Then
                            If sI_Account = "15038091058" Then
                                sI_Account = oCargo.I_Account
                            End If
                        End If

                        ' Added 29.12.05, due to possible mix of Autogiro and OCR
                        nMON_InvoiceAmount = 0
                        nMON_TransferredAmount = nMON_InvoiceAmount

                        If Mid(sImportLineBuffer, 3, 2) = "09" Then
                            sFormatType = "OCR"
                        Else
                            sFormatType = "OCR_FBO"
                        End If

                        bReadLine = True
                        bCreateObject = False
						
					End If
					
					'30 - Bel�pspost 1
				Case "30"
					bReadLine = False
					bCreateObject = True
					
					'70 - FBO
				Case "70"
					oPayment = oPayments.Add
					oPayment.I_Account = CObj(sI_Account)
					oPayment.PayType = "D"
					oPayment.PayCode = bbSetPayCode(Mid(sImportLineBuffer, 16, 1), "D", "OCR_FBO")
					If Mid(sImportLineBuffer, 42, 1) = "J" Then
						'Payer shall be notified about the direct debit order
						oPayment.NOTI_NotificationType = "PAPER"
					End If
					oInvoice = oPayment.Invoices.Add
					oInvoice.Unique_Id = CObj(Mid(sImportLineBuffer, 17, 25))
					If bProfileInUse Then
						oPayment.VB_FilenameOut_ID = CheckAccountNo(sI_Account, "", oProfile)
                                            ' added 02.12.2015
                                            oPayment.VB_Profile = oProfile
                                            oInvoice.VB_Profile = oProfile
					End If
					bReadLine = True
					bCreateObject = False
					
					'88 - End of Batch
				Case "88"
					' NY000088 - Sluttrecord oppdrag
					If sFormatType = "OCR" Then
                        'nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 25, 17)) ' Sum amount in batch, NB! in Kroner
                        nMON_InvoiceAmount = Val(RemoveLeadingCharacters(Mid$(sImportLineBuffer, 25, 17), "0")) ' Sum amount in batch, NB! in Kroner
						nMON_TransferredAmount = nMON_InvoiceAmount
						bAmountSetInvoice = True
						bAmountSetTransferred = True
						dDATE_Production = DateSerial(CInt(Mid(sImportLineBuffer, 46, 2)), CInt(Mid(sImportLineBuffer, 44, 2)), CInt(Mid(sImportLineBuffer, 42, 2)))
					End If
					nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 9, 8)) ' Antall transer i filen
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 17, 8)) ' Antall records i filen
					
					bReadLine = True ' read 89 record
					bCreateObject = False
					bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up
					
				Case "89" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-010
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadOCRRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadOCRRecord(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 

        ' 02.03.2017- added to have amount on batch
        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

		ReadOCRFile = sImportLineBuffer
	End Function
	Private Function ReadAvprickning() As String
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		sFormatType = "Avprickning"
		
		Do While True
			bReadLine = False
			bCreateObject = False
			
			Select Case Mid(sImportLineBuffer, 1, 2)
				'20 - Start of batch, have to store information
				Case "11" '�pningspost
					If bFromLevelUnder Then
						' New �ppningspost, create a new batch
						' jump up, to create object
						Exit Do
					End If
					
					'30 - Bel�pspost 1
				Case "26", "27" 'Namnrecord, adressrecord
					bReadLine = False
					bCreateObject = True
					
					
				Case "29"
					' End of file
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 21, 12)) ' Sum bel�p i filen
					nMON_TransferredAmount = nMON_InvoiceAmount
					bAmountSetInvoice = True
					bAmountSetTransferred = True
					bCreateObject = False
					bFromLevelUnder = True
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-010
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadLeverantorsbetalningarRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadLeverantorsbetalningarRecord(oFile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 
		
		ReadAvprickning = sImportLineBuffer
	End Function
	Private Function ReadBgMax() As String
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		Dim nTotalAmount As Double
		Dim sDatePayment As String
		Dim nNoOfTransactions As Double
		
		bFromLevelUnder = False
		sFormatType = "BgMax"
		
		Do While True
			bReadLine = False
			bCreateObject = False
			
			Select Case Mid(sImportLineBuffer, 1, 2)
				
				
				Case "15" 'Deposit record - End of record
					If bFromLevelUnder Then
						sDatePayment = Mid(sImportLineBuffer, 38, 8)
						sREF_Bank = Mid(sImportLineBuffer, 46, 5)
						'Create a REF_Own. Vital when converting to OCR, to create a identical and unqiue lump reference
						sREF_Own = Mid(sImportLineBuffer, 40, 6) & Mid(sImportLineBuffer, 47, 4)
						nTotalAmount = Val(Mid(sImportLineBuffer, 51, 18))
						
						AddInvoiceAmountOnBatch()
						AddTransferredAmountOnBatch()

                        ' 12.02.2018 - Noen ganger kommer det med 000000000 som nTotalAmount i record 15 - ser ut som om det er ved kreditnota
                        If nTotalAmount <> 0 Then
                            If nTotalAmount <> nMON_InvoiceAmount Then
                                'Error 10031-020
                                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10031-212", Trim(Str(nMON_InvoiceAmount)), Trim(Str(nTotalAmount)), "Batch Amount")
                            End If
                        End If

                        For Each oPayment In Payments
                            oPayment.DATE_Payment = sDatePayment
                            'XNET - 27.09.2013 - Changed next line
                            oPayment.DATE_Value = sDatePayment
                            oPayment.REF_Own = sREF_Own
                            'XNET 20.08.2013 - Changed next line
                            nNoOfTransactions = nNoOfTransactions + 1 'oPayment.Invoices.Count
                            If oPayment.MON_InvoiceAmount = 0 Then
                                If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    oPayment.MON_TransferCurrency = oCargo.MONCurrency
                                    oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                                    oPayment.MON_AccountCurrency = oCargo.MONCurrency
                                    oPayment.MON_OriginallyPaidCurrency = oCargo.MONCurrency
                                    oPayment.I_Account = oCargo.I_Account
                                End If
                            End If
                        Next oPayment

                        'Error 10030-020
                        If nNoOfTransactions <> Val(Mid(sImportLineBuffer, 72, 8)) Then
                            If Val(Mid(sImportLineBuffer, 72, 8)) <> 0 Then 'Added 05.06.2018 - for Visma case 533150, no of transactions is sometimes 0
                                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10030-212", Trim(Str(nNoOfTransactions)), Mid(sImportLineBuffer, 72, 8))
                            Else
                                oCargo.MON_InvoiceAmount = oCargo.MON_InvoiceAmount + 1 'Added 05.06.2018 - for Visma case 533150 - This depositrecord is not being counted in the FileEnd record
                            End If
                        End If

                        Exit Do
                    Else
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-212")
                    End If

                    '20 - Start of new payment
				Case "20", "21" 'Payment record"
                        bReadLine = False
                        bCreateObject = True

                        '30 - Bel�pspost 1
                        '        Case "26", "27"  'Namnrecord, adressrecord
                        '            bReadLine = False
                        '            bCreateObject = True


                        '        Case "29"
                        '            ' End of file
                        '            nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 21, 12)) ' Sum bel�p i filen
                        '            nMON_TransferredAmount = nMON_InvoiceAmount
                        '            bAmountSetInvoice = True
                        '            bAmountSetTransferred = True
                        '            bCreateObject = False
                        '            bFromLevelUnder = True
                        '            Exit Do

                Case "25" 'Added 05.06.2018 - for Visma case 533150
                        bReadLine = False
                        bCreateObject = True

				Case Else
                        'All other record-types gives error
                        'Error # 10010-010
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-212")

			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadLeverantorsbetalningarRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadLeverantorsbetalningarRecord(oFile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 
		
		ReadBgMax = sImportLineBuffer
		
		AddInvoiceAmountOnBatch()
		AddTransferredAmountOnBatch()
		
	End Function
	Private Function ReadSEK2File() As String
		' Danish Incoming payments
		' Pengeinstitutternes standardformat
		'------------------------------------
		
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		
		Do While True
			bReadLine = False
			
			Select Case Mid(sImportLineBuffer, 3, 3)
				'020 - Start of batch, have to store information
				Case "020" ' Sektionsstart
					If bFromLevelUnder Then
						' have read a 020-record in batch (new batch)
						' jump up, to create object
						Exit Do
					Else
						'Extract data from sImportLineBuffer given from function Import
						sREF_Own = Mid(sImportLineBuffer, 42, 10)
						sI_Account = Mid(sImportLineBuffer, 14, 14)
						dDATE_Production = DateSerial(CInt(Mid(sImportLineBuffer, 52, 4)), CInt(Mid(sImportLineBuffer, 56, 2)), CInt(Mid(sImportLineBuffer, 58, 2)))
						sI_EnterpriseNo = Mid(sImportLineBuffer, 6, 8) ' Kreditornummer
						sFormatType = "SEK2"
						
						bReadLine = True
						bCreateObject = True
						
					End If
					
				Case "030"
					bReadLine = False
					bCreateObject = True
					
					
					'88 - End of Batch
				Case "080"
					
					nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 25, 15)) ' Sum amount in batch, NB! in Kroner
					nMON_TransferredAmount = nMON_InvoiceAmount
					bAmountSetInvoice = True
					bAmountSetTransferred = True
					nNo_Of_Records = Val(Mid(sImportLineBuffer, 14, 11)) ' Antall records i filen
					
					bReadLine = True ' read 090 record
					bCreateObject = False
					bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up
					
				Case "090" ' end of file
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-010
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				'UPGRADE_WARNING: Couldn't resolve default property of object ReadSEK2Record(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sImportLineBuffer = ReadSEK2Record(oFile) ', oProfile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = CObj(sI_Account)
				oPayment.I_Name = sI_EnterpriseNo ' Kreditornummer
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 
		
		ReadSEK2File = sImportLineBuffer
	End Function
	Private Function ReadOCR_Bankgirot() As String
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		'Variables used to set properties in the payment-object
		Dim sI_Account As String
		
		bFromLevelUnder = False
		sFormatType = "OCR_Bankgirot"
		sStatusCode = "02"
		
		Do While True
			bReadLine = False
			bCreateObject = False
			
			Select Case Mid(sImportLineBuffer, 1, 2)
				Case "20" 'Mottagarpost
					If bFromLevelUnder Then
						Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
						Exit Do
					Else
						sI_Account = Mid(sImportLineBuffer, 9, 10)
						bReadLine = True
						bCreateObject = False
					End If
					
				Case "30" 'Behandlingsdatumpost
					oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 19, 6) 'Skrivdag hos bankgirot
					bReadLine = True
					bCreateObject = False
					
				Case "40" 'Transaktionspost
					If bFromLevelUnder Then
						'We don't read any lines in payment or invoice
						bReadLine = True
						bCreateObject = False
						bFromLevelUnder = False
					Else
						bReadLine = False
						bCreateObject = True
					End If
					
				Case "50" 'Deltotalpost
					Exit Do
					
				Case Else
					'All other record-types gives error
					'Error # 10010-010
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
					
			End Select
			
			'WARNING! Is this correct
			If oFile.AtEndOfStream = True Then
				Exit Do
			End If
			
			'Read next line
			If bReadLine Then
				sImportLineBuffer = ReadGenericRecord(oFile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = Trim(CObj(sI_Account))
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
		Loop 
		
		AddInvoiceAmountOnBatch()
		bAmountSetInvoice = True
		nMON_TransferredAmount = nMON_InvoiceAmount
		bAmountSetTransferred = True
		ReadOCR_Bankgirot = sImportLineBuffer
	End Function
	Private Function ReadBEC() As String
		Dim oPayment As Payment
		Dim oFilesetup As FileSetup
		Dim oClient As Client
		Dim oaccount As Account
		
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		
		bFromLevelUnder = False
		
		Do While True
			
			If Left(sImportLineBuffer, 2) = "20" And Len(sImportLineBuffer) < 28 Or Left(sImportLineBuffer, 5) = ":DATE" Then '08.03.06 Added new format for BEC
				
				' If New header in file, jump to BabelFile to create new Batch
				If bFromLevelUnder Then
					Exit Do
				Else
					' Headerrecord - find Date and account
					If Left(sImportLineBuffer, 2) = "20" And Len(sImportLineBuffer) < 28 Then
						' old format (pre 08.03.06
						oCargo.DATE_Payment = Left(sImportLineBuffer, 8)
						'End If
						'oCargo.I_Account = Mid$(sImportLineBuffer, 15)
						' 13.06.05: Added 5290 in front of accountno
						oCargo.I_Account = "5290" & Mid(sImportLineBuffer, 15) ' ?? re new format
					Else
						' New format
						If Left(sImportLineBuffer, 5) = ":DATE" Then
							oCargo.DATE_Payment = Mid(sImportLineBuffer, 6, 8)
							
							' read next line with :ACCOUNTNO;
							'UPGRADE_WARNING: Couldn't resolve default property of object ReadBECRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							sImportLineBuffer = ReadBECRecord(oFile)
							If Left(sImportLineBuffer, 8) = ":ACCOUNT" Then
								' Find accountno here;
								oCargo.I_Account = Trim(Mid(sImportLineBuffer, 9, 14)) ' Including 5290
							End If
						End If
					End If
					
					If Not VB_Profile Is Nothing Then
						' Set date for last import into clientstable
						' If only one client, set for this one
						For	Each oFilesetup In VB_Profile.FileSetups
							For	Each oClient In oFilesetup.Clients
								For	Each oaccount In oClient.Accounts
									If oCargo.I_Account = oaccount.Account Then
										' Set date for last update
										oClient.SeqNoTotal = Val(VB6.Format(Now, "YYYYMMDD"))
										VB_Profile.Status = 2
										oFilesetup.Status = 2
										oClient.Status = 1
										Exit For
									End If
								Next oaccount
							Next oClient
							'If oFilesetup.Clients.Count = 1 Then
							'    ' Set date for last update
							'    Set oClient = oFilesetup.Clients(1)
                            '    oClient.SeqNoTotal =VB6.Format(Now, "YYYYMMDD")
                            '    VB_Profile.Status = 2
                            '    oFilesetup.Status = 2
                            '    oClient.Status = 1

                            'End If
                        Next oFilesetup
                    End If
                    bReadLine = True
                    bCreateObject = False
                End If
            Else

                bReadLine = False
                bCreateObject = True

                'If bFromLevelUnder Then
                '    ' jump up, to create object
                '    Exit Do
                'End If
                sFormatType = "BEC"
            End If

            If bReadLine Then
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadBECRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadBECRecord(oFile)
            End If
            ' added 05.05.2008
            ' sometimes the files ends with a line with ~26 as the only token
            ' solve this:
            If sImportLineBuffer = "~26" Then
                sImportLineBuffer = ""
                Exit Do
            End If

            If Not oPayment Is Nothing Then
                ' New 16.03.05
                ' For each UDUS, create another Batch, due to AccountInfo
                ' and for shift between FI and UDUS, create batch
                If oPayment.PayCode = "180" Then
                    Exit Do
                End If
                ' 02.08.2007 added 357 og 358 (FIK73 and 75)
                If oPayment.PayCode = "180" And (Left(sImportLineBuffer, 3) = "351" Or Left(sImportLineBuffer, 3) = "357" Or Left(sImportLineBuffer, 3) = "358") Then
                    ' Shift from UDUS to FI
                    Exit Do
                End If
                If oPayment.PayCode = "180" And (Left(sImportLineBuffer, 6) = ":FI351" Or Left(sImportLineBuffer, 6) = ":FI357" Or Left(sImportLineBuffer, 6) = ":FI358") Then '08.03.06
                    ' Shift from UDUS to FI
                    Exit Do
                End If
                If oPayment.PayCode = "510" And Left(sImportLineBuffer, 3) = "356" Then
                    ' Shift from FI to UDUS
                    Exit Do
                End If
                If oPayment.PayCode = "510" And Left(sImportLineBuffer, 3) = ":UD356" Then '08.03.06
                    ' Shift from FI to UDUS
                    Exit Do
                End If
            End If


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
            bFromLevelUnder = True

            ' 02.08.2007 moved from further up!
            If oFile.AtEndOfStream = True Then
                ' added next IF, otherwise we would end up without last line for old bectype!!!
                If sImportLineBuffer = "" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        ReadBEC = sImportLineBuffer
    End Function
    Private Function ReadCodaFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean ', bReadLine As Boolean, bCreateObject As Boolean
        Dim bx As Boolean
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bKID As Boolean
        Dim bFreetext As Boolean
        Dim iOffset As Short

        bFromLevelUnder = False

        sFormatType = "CODA"

        Do While True

            ' 25.09.03 Find offset due to accountno shorter than 11 digits (re Navion)
            'For iOffset = 0 To 10
            ' Changed 07.10.03, might be Letters in ToAccount!!!!
            For iOffset = 10 To 0 Step -1
                If InStr("FKU", Mid(sImportLineBuffer, 20 - iOffset, 1)) > 0 Then
                    Exit For
                End If
            Next iOffset

            ' New 09.03.05
            ' If mix between Domestic and International,
            ' create a new batch.
            ' Store last paytype in oCargo.ref
            If oCargo.REF = "" Then
                oCargo.REF = Mid(sImportLineBuffer, 20 - iOffset, 1)
            End If
            If Mid(sImportLineBuffer, 20 - iOffset, 1) <> oCargo.REF Then
                oCargo.REF = Mid(sImportLineBuffer, 20 - iOffset, 1)
                Exit Do ' Jump out to create new batch
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)

            ' Added 02.08.04, ref Narvesen/7-Eleven, but relevant for all Coda-customers
            ' Check if mix of KID and Freetext. This may be caused by mix for Kreditnota
            ' without KID, and debetnota with KID
            ' If so, then undo from KID to Freetext, to avoid mix
            bKID = False
            bFreetext = False

            'For Each oInvoice In oPayment.Invoices
            '    If Len(Trim(oInvoice.Unique_Id)) > 0 Then
            '        bKID = True
            '    Else
            '        bFreetext = True
            '    End If
            'Next oInvoice
            ' Check if mix
            ' Commented out 09.10.06. Some banks can solve this
            ' Otherwise, set it up under Advanced in setup!
            'If bKID And bFreetext Then
            '    oPayment.PayCode = "150"
            '    ' Avoid mixed payment, by moving KID to freetext
            '    For Each oInvoice In oPayment.Invoices
            '        If Len(Trim(oInvoice.Unique_Id)) > 0 Then
            '            Set oFreeText = oInvoice.Freetexts.Add
            '            oFreeText.Text = "KID: " & oInvoice.Unique_Id
            '            oInvoice.Unique_Id = ""
            '        End If
            '    Next oInvoice

            'End If

            ReadCodaFile = sImportLineBuffer


            'WARNING! Is this correct
            ' FIX:ED moved by janP 28.06
            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadPGS() As String
        ' Special format for PGS - originally special payX-format

        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean ', bReadLine As Boolean, bCreateObject As Boolean
        Dim bx As Boolean
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bKID As Boolean
        Dim bFreetext As Boolean

        bFromLevelUnder = False

        sFormatType = "PGS"

        Do While True

            ' Must create new batch if shift from Domestic to International, vice versa
            If Len(oCargo.PayCode) > 0 And Len(sImportLineBuffer) > 0 Then
                If oCargo.PayCode <> Mid(sImportLineBuffer, 20, 1) Then
                    ' Create new batch!
                    oCargo.PayCode = Mid(sImportLineBuffer, 20, 1)
                    Exit Do
                End If
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)

            ' Added 02.08.04, ref Narvesen/7-Eleven, but relevant for all "payx"-customers
            ' Check if mix of KID and Freetext. This may be caused by mix for Kreditnota
            ' without KID, and debetnota with KID
            ' If so, then undo from KID to Freetext, to avoid mix
            bKID = False
            bFreetext = False

            ' removed this test 17.11.06, because of structured invoiceno
            'For Each oInvoice In oPayment.Invoices
            '    If Len(Trim(oInvoice.Unique_Id)) > 0 Then
            '        bKID = True
            '    Else
            '        bFreetext = True
            ''    End If
            'Next oInvoice
            ' Check if mix
            'If bKID And bFreetext Then
            '    oPayment.PayCode = "150"
            ' Avoid mixed payment, by moving KID to freetext

            'For Each oInvoice In oPayment.Invoices
            '    If Len(Trim(oInvoice.Unique_Id)) > 0 Then
            '        Set oFreeText = oInvoice.Freetexts.Add
            '        oFreeText.Text = "KID: " & oInvoice.Unique_Id
            '        oInvoice.Unique_Id = ""
            '    End If
            'Next oInvoice

            'ElseIf bKID Then
            ' KID-only
            '    oPayment.PayCode = "301" 'KID

            'End If

            ReadPGS = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadHexagon() As String
        ' Special format for PGS UK Salary
        '$FN:1
        '$FT:114662.64
        '$DS
        'F01:11850873
        'F02:190805
        'F03: PAYROLL (this line not in use by Vector)
        'F04:42
        'F05:114662.64
        '$LS
        'S01:34417510
        'S02:090126
        'S03: Webber Carol
        'S04:296.45
        'S05: Webber Carol
        '$LE

        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean ', bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "Hexagon"

        sImportLineBuffer = ReadGenericRecord(oFile) 'F01:11850873
        If Left(sImportLineBuffer, 4) <> "F01:" Then
            ' Wrong format
            sImportLineBuffer = ""
            'Error # 10011 '  Wrong format !
            Call BabelImportError("ReadPSUK_Salary", FilenameImported(), sImportLineBuffer, "10011")
        End If
        ' Find DebitAccount;
        oCargo.I_Account = Mid(sImportLineBuffer, 5)

        sImportLineBuffer = ReadGenericRecord(oFile) 'F02:190805
        ' Find paymentdate;
        oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 9, 2) & Mid(sImportLineBuffer, 7, 2) & Mid(sImportLineBuffer, 5, 2)
        sImportLineBuffer = ReadGenericRecord(oFile) 'F03: PAYROLL or F04:42
        If Left(sImportLineBuffer, 4) = "F03:" Then
            ' F03: line is not in use by Vector
            sImportLineBuffer = ReadGenericRecord(oFile) 'F04:42
        End If
        sImportLineBuffer = ReadGenericRecord(oFile) 'F05:114662.64
        nMON_TransferredAmount = Val(sImportLineBuffer) * 100
        nMON_InvoiceAmount = nMON_TransferredAmount

        Do While True
            sImportLineBuffer = ReadGenericRecord(oFile) '$LS  Start of record
            If Left(sImportLineBuffer, 3) = "$DE" Then
                Exit Do
            End If
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)

            ReadHexagon = sImportLineBuffer



        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadSudjaca() As String
        ' Special format for Sudjaca Singapore (General)

        'Home Allotment Listing For The Month Of Jul 2004,,,
        'Home Allotment Type :,HA1-USD,Home Allotment (1),
        'Vessel code :,RGD,RED GOLD,
        'S/No,Employee No,Beneficiary Name,Amt USD
        ' ,Employee Name,Beneficiary Bank A/c,
        ' , ,Beneficiary Bank,
        ' , ,Beneficiary Address,
        '1,C30019,Siti Nurhayati,$434.00
        '  ,Budiono Dian,A/c No 102-000-319-190-901,
        '  , ,BNI,
        '  , ,Cabang Garut,
        '  , ,Indonesia,
        '  , ,,
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean ', bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "Sudjaca"

        Do While True
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)

            ReadSudjaca = sImportLineBuffer

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadISS_LonnsTrekk() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim sResponse As String
        Dim bx As Boolean
        'Variables for storing date for test
        Dim dPaymentDate As Date
        Dim sMonth, sDay, sYear As String
        Dim sAccount As String
        Dim bOutOfLoop As Boolean


        bFromLevelUnder = False

        sFormatType = "ISS_LonnsTrekk"

        Do While True

            If Not bFromLevelUnder Then

                bOutOfLoop = False
                ' Ask for paymentdate:
                Do While bOutOfLoop = False

                    bOutOfLoop = True
                    sResponse = InputBox("Legg inn betalingsdato (dd.mm.yyyy)", "Utbetalingsdato", Trim(Str(VB.Day(Now))) & "." & Trim(Str(Month(Now))) & "." & Trim(Str(Year(Now))))
                    If sResponse = "" Then
                        ' user pressed Cancel
                        bOutOfLoop = False
                        Exit Do
                    End If

                    If Len(sResponse) - Len(Replace(sResponse, ".", "")) = 2 Then
                        ' OK, two . in datestring:
                        ' Test date given;
                        sDay = Left(sResponse, InStr(sResponse, ".") - 1)
                        sMonth = Mid(sResponse, InStr(sResponse, ".") + 1, 2)
                        If Right(sMonth, 1) = "." Then
                            ' only one digit for month
                            sMonth = Left(sMonth, 1)
                        End If
                        sYear = Right(sResponse, 4)
                        If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                            MsgBox("Feil i �rstall - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                            MsgBox("Feil i m�ned - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sDay) < 1 Or Val(sDay) > 31 Then
                            MsgBox("Feil i dag - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                            If Val(sDay) > 30 Then
                                MsgBox("Feil i dag - Pr�v igjen!")
                                bOutOfLoop = False
                            End If
                        End If
                        If Val(sMonth) = 2 Then
                            If Val(sDay) > 29 Then
                                MsgBox("Feil i dag - Pr�v igjen!")
                                bOutOfLoop = False
                            End If
                        End If
                    Else
                        MsgBox("Feil i dato!" & vbCrLf & "Skal ha formen dd.mm.yyyy" & vbCrLf & "Husk punktum som skilletegn!")
                        bOutOfLoop = False
                    End If
                Loop
                If sResponse = "" Then
                    bOutOfLoop = False
                Else
                    dPaymentDate = DateSerial(Int(CDbl(sYear)), Int(CDbl(sMonth)), Int(CDbl(sDay)))
                End If

            End If

            If bOutOfLoop Then

                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = CObj(sAccount)
                oPayment.DATE_Payment = VB6.Format(dPaymentDate, "YYYYMMDD") 'CVar(dPaymentDate)

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True


                ReadISS_LonnsTrekk = sImportLineBuffer


                If oFile.AtEndOfStream = True Then
                    If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                        Exit Do
                    End If
                End If
            Else
                ' user has canceled InputBox
                ' Skip the rest of the lines
                Do Until oFile.AtEndOfStream = True
                    oFile.SkipLine()
                Loop
                sImportLineBuffer = "ERROR"

                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadSRI_Access() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        'Dim sResponse As String
        'Variables for storing date for test
        'Dim dPayment_Date As Date
        'Dim sDay As String, sMonth As String, sYear As String
        'Dim sAccount As String
        'Dim bOutOfLoop As Boolean

        bFromLevelUnder = False
        sFormatType = "SRI_Access"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            'oPayment.I_Account = CVar(sAccount)
            'oPayment.Payment_Date = CVar(dPayment_Date)

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True


            ReadSRI_Access = sImportLineBuffer


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    'Removed 11.09.2019
    'Friend Function ReadCOACSU(ByRef oUNH As MSXML2.IXMLDOMNode, ByRef oEDIMapping As MSXML2.IXMLDOMElement, ByVal iFilesetup_ID As Integer) As Boolean
    '    Dim bReturnValue As Boolean
    '    Dim oPayment As vbBabel.Payment
    '    Dim bx As Boolean

    '    'Assume failure
    '    bReturnValue = False

    '    If FileSetup_ID() = -1 Then
    '        bProfileInUse = False
    '    End If

    '    oFile = Nothing

    '    oPayment = oPayments.Add()
    '    oPayment.objFile = oFile
    '    oPayment.VB_Profile = oProfile
    '    oPayment.Cargo = oCargo
    '    oPayment.ImportFormat = iImportFormat
    '    oPayment.StatusCode = sStatusCode
    '    ' oPayment.DATE_Payment =VB6.Format(dDATE_Production, "YYYYMMDD") 'dDATE_Production
    '    'oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
    '    'oPayment.DATE_Payment = VB6.Format(dPayment_Date, "YYYYMMDD") 'dPayment_Date
    '    'oPayment.PayType = sPayType
    '    'oPayment.PayCode = sPayCode

    '    bReturnValue = oPayment.ReadCOACSUFile(oUNH, oEDIMapping)

    '    bx = AddInvoiceAmountOnBatch()
    '    nMON_TransferredAmount = nMON_InvoiceAmount

    'End Function
    Private Function ReadEDIFile(ByRef oLIN As MSXML2.IXMLDOMNode, ByRef oEDIMapping As MSXML2.IXMLDOMElement, ByVal iFilesetup_ID As Integer) As Boolean
        Dim oPayment As Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bAccountFound As Boolean
        'Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        'Dim oSEQList As MSXML2.IXMLDOMNodeList
        'Dim l As Long,
        Dim bReturnValue As Boolean
        Dim dValueDate, dPayment_Date As Date
        Dim sPayType, sPayCode As String
        Dim sI_Account, sBANK_I_SWIFTCode As String
        Dim nCharges As Double
        Dim sCharges_Currency, sCharges As String
        Dim bCharges_Domestic, bCharges_Abroad As Boolean
        Dim sInvCurrency, sTempDate, sTraCurrency As String
        Dim nodActive As MSXML2.IXMLDOMNode
        Dim nodTemp As MSXML2.IXMLDOMNode
        Dim nodTemp2 As MSXML2.IXMLDOMNode
        Dim nodTempList As MSXML2.IXMLDOMNodeList
        Dim lCounter As Integer
        Dim bx As Boolean
        Dim nERAExchRateAgreed As Double
        Dim sERADealMadeWith As String
        Dim nFRWForwardContractRate As Double
        Dim sFRWForwardContractNo As String
        Dim nSEQAmount As Double
        Dim sSEQName, sSEQAccount As String
        Dim sSEQAdr1, sSeqDate As String
        Dim bJustAddMoreFreetext As Boolean
        Dim sTmp As String
        Dim bToOwnAccount As Boolean = False
        Dim bPriority As Boolean = False

        bReturnValue = False
        bJustAddMoreFreetext = False
        'Set oSEQList = oLIN.selectNodes("GROUP/SEQ")

        'New 22.03.2006 - sometimes are we unable to interpret the correct bank
        ' Problems to distinguish between DnBNOR_NO and DnBNOR_SE
        'New test
        'If oCargo.Bank = BabelFiles.Bank.DnB Then

        ' VBDISCUSS 14.07.2017
        ' Added Or oCargo.Bank = BabelFiles.Bank.DnBNOR_SE
        ' Otherwise oCargo.bank will stay as DNBNOR_SE for rest of this batch, and this can screw up the currency settings.
        ' Using sFormatCurrencyCode in oPayment where we can't find TransferCurrency - then with bank DNBNOR_SE Currency is set to SEK
        ' See case Salmar, 20170714 - 
        If oCargo.Bank = BabelFiles.Bank.DnB Or oCargo.Bank = BabelFiles.Bank.DnBNOR_SE Then
            If GetPropValue("batch/CONDITION_Is_DnBNOR_SE", oLIN, oEDIMapping) = "SEK" Then
                If GetPropValue("batch/paytype", oLIN, oEDIMapping) = "DO" Then
                    oCargo.Bank = BabelFiles.Bank.DnBNOR_SE
                End If
            Else
                ' Not perfect, but set to DNB No so far - need to discuss this next line!
                oCargo.Bank = BabelFiles.Bank.DnB
            End If
        End If

        sI_EnterpriseNo = GetPropValue("batch/i_enterpriseno", oLIN, oEDIMapping)

        If iImportFormat <> BabelFiles.FileType.BANSTA Then
            sTempDate = GetPropValue("batch/date_production", oLIN, oEDIMapping)
            'XNET 01.07.2013 - Added next IF
            If Not EmptyString(sTempDate) Then
                dDATE_Production = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))
            Else
                sTempDate = Format(Now(), "yyyyMMdd")
                dDATE_Production = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))
            End If

        End If
        sTempDate = ""
        'sBatch_ID = GetPropValue("batch/batch_id", oLIN, oEDIMapping)
        sREF_Own = GetPropValue("batch/ref_own", oLIN, oEDIMapping)

        sTempDate = GetPropValue("batch/date_value", oLIN, oEDIMapping)
        If Len(sTempDate) > 0 Then
            dValueDate = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))
        Else
            ' added next test 12.03.07
            ' Found no date in file. Must differentiate between CREMUL and PAYMUL
            If iImportFormat = BabelFiles.FileType.PAYMUL Then
                dValueDate = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(VB6.Format(Now, "MM")), CInt(VB6.Format(Now, "DD")))
            Else
                ' (as pre 12.03.07)
                dValueDate = DateSerial(CInt(VB6.Format(System.DateTime.FromOADate(Now.ToOADate - 1), "YYYY")), CInt(VB6.Format(System.DateTime.FromOADate(Now.ToOADate - 1), "MM")), CInt(VB6.Format(System.DateTime.FromOADate(Now.ToOADate - 1), "DD")))
            End If
        End If

        sTempDate = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sTempDate = GetPropValue("batch/date_payment", oLIN, oEDIMapping)
        If Len(sTempDate) > 0 Then
            dPayment_Date = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))
        Else
            ' added next test 12.03.07
            ' Found no date in file. Must differentiate between CREMUL and PAYMUL
            If iImportFormat = BabelFiles.FileType.PAYMUL Then
                dPayment_Date = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(VB6.Format(Now, "MM")), CInt(VB6.Format(Now, "DD")))
            ElseIf iImportFormat = BabelFiles.FileType.CREMUL And oCargo.Bank = BabelFiles.Bank.DnBNOR_INPS Then  ' Added 25.05.2009
                dPayment_Date = DateSerial(CInt("1990"), CInt("01"), CInt("01")) '- See payment for explaiantion
            Else
                ' (as pre 12.03.07)
                dPayment_Date = DateSerial(CInt(VB6.Format(System.DateTime.FromOADate(Now.ToOADate - 1), "YYYY")), CInt(VB6.Format(System.DateTime.FromOADate(Now.ToOADate - 1), "MM")), CInt(VB6.Format(System.DateTime.FromOADate(Now.ToOADate - 1), "DD")))
            End If
        End If
        sTempDate = ""

        sI_Account = GetPropValue("batch/i_account", oLIN, oEDIMapping)
        sBANK_I_SWIFTCode = GetPropValue("batch/i_swiftcode", oLIN, oEDIMapping)

        sTmp = GetPropValue("batch/paytype", oLIN, oEDIMapping)
        If sTmp = "IN" Or sTmp = "SPA" Then
            sPayType = "I"
        Else
            If iImportFormat = BabelFiles.FileType.DEBMUL Then
                If sTmp = "SCT" Or sTmp = "SPA" Then 'SEPA
                    sPayType = "I"
                Else
                    sPayType = "D"
                End If
            ElseIf iImportFormat = BabelFiles.FileType.BANSTA Then
                sPayType = ""
            Else
                sPayType = "D"
            End If
        End If
        If oCargo.Bank = BabelFiles.Bank.Nordea_eGateway And iImportFormat <> BabelFiles.FileType.PAYMUL Then
            sPayCode = bbSetPayCode(GetPropValue("batch/paycode2", oLIN, oEDIMapping), sPayType, GetEnumFileType(iImportFormat))
            If Not EmptyString(sPayCode) Then
                Select Case sPayCode
                    Case "10"
                        sPayCode = 150
                    Case "42"
                        sPayCode = 150
                    Case "Z1"
                        bToOwnAccount = True
                    Case "Z2", "52"
                        bPriority = True
                    Case Else
                        sPayCode = 150
                End Select
            End If
        Else
            '15.08.2012 - Added next IF
            If Len(sBANK_I_SWIFTCode) = 8 Or Len(sBANK_I_SWIFTCode) = 11 Then
                Select Case Mid(sBANK_I_SWIFTCode, 5, 2)

                    Case "SE" 'Sweden
                        sTmp = GetPropValue("batch/paytypespecifier", oLIN, oEDIMapping) 'IF PGI, then the account to be debited is a Plusgiro-account

                        If Left(oCargo.Special, 18) = "GJENSIDIGE_PAIN001" Then
                            '15.08.2012
                            'This code is specific for the F2100 system which is used for Gjensidige Sweden
                            'Not complete (not implemented for debit Bankgironumber
                            'PayCode may be changed further down (after population of Payment and Invoice)

                            'THIS MAY ALSO BE USEFUL FOR NORDEA_EGATEWAY AND MAYBE ALSO FOR OTHER SWEDISH BANKS!

                            'The paytype is not set on the LIN-level but on the SEQ-level

                            sPayCode = sTmp
                            oCargo.PayCode = sTmp
                            'sPayCode = GetPropValue("batch/paycode2", oLIN, oEDIMapping)

                            'If sTmp = "PGI" Then 'Plusgiroaccount to be debites
                            '    Select Case sPayCode
                            '        Case "9" 'Payment from Plusgiro. to Bankgiro with text or OCR
                            '            sPayCode = "345"
                            '        Case "10" 'Payment from Plusgiro to money order
                            '            sPayCode = "347"
                            '        Case "42" 'Payment from Plusgiro to Bank account with advice or without advice
                            '            'TODO: Not retrieving if it is salary or pension yet
                            '            If sPayType = "S" Then
                            '                sPayCode = "348"
                            '            ElseIf sPayType = "M" Then 'Assume Pension
                            '                sPayCode = "349"
                            '            Else
                            '                sPayCode = "346" ' with advice
                            '            End If
                            '        Case "Z1" ' Not documented
                            '            bToOwnAccount = True
                            '        Case "Z2", "52" 'Not documented
                            '            bPriority = True
                            '        Case Else
                            '            sPayCode = 343 'may also be with OCR (and creditnote OCR) 331 and 332 
                            '            '334 = message
                            '            'NB IF THE DEFAULT PAYCODE IS CHANGED CHANGE ALSO BELOW
                            '    End Select
                            'Else
                            '    Select Case sPayCode
                            '        Case "8" 'Payment from Bankgiro no. Structured
                            '            sPayCode = "333"
                            '            'Case "10", "23" '23 is not document, but appeared in a testfile
                            '            '    sPayCode = 347
                            '        Case "10" 'Payment from Bankgiro no. to money order
                            '            sPayCode = "337"
                            '        Case "42" 'Payment from Bankgiro no. to Bank account with advice and without (use AGN) or salary
                            '            sPayCode = "335"
                            '        Case "Z1" ' Not documented
                            '            bToOwnAccount = True
                            '        Case "Z2", "52" 'Not documented
                            '            bPriority = True
                            '        Case Else
                            '            sPayCode = "334" 'may also be with OCR (and creditnote OCR) 331 and 332 
                            '            '334 = message
                            '            'NB IF THE DEFAULT PAYCODE IS CHANGED CHANGE ALSO BELOW
                            '    End Select
                            '    'End If
                            'End If
                        Else
                            sPayCode = bbSetPayCode(GetPropValue("batch/paycode", oLIN, oEDIMapping), sPayType, GetEnumFileType(iImportFormat))
                        End If

                    Case Else
                        sPayCode = bbSetPayCode(GetPropValue("batch/paycode", oLIN, oEDIMapping), sPayType, GetEnumFileType(iImportFormat))

                End Select
            Else
                sPayCode = bbSetPayCode(GetPropValue("batch/paycode", oLIN, oEDIMapping), sPayType, GetEnumFileType(iImportFormat))
            End If

        End If

        '16.09.2009 - Next IF, may influence others
        If sPayCode = "643" And sPayType = "D" Then 'Salary
            sPayType = "S"
        End If

        '*****************************************************************************************
        'New code
        '12.03.2008 - All old code is beneath
        'SEB doesn't mark the inpayment specifically as structured or KID. We have to check the DOC-segment to
        '  find which transactiontype it is.
        If oCargo.Bank = BabelFiles.Bank.SEB_NO Or oCargo.Bank = BabelFiles.Bank.Svenska_Handelsbanken Then
            If iImportFormat = BabelFiles.FileType.CREMUL Then
                nodTemp = oLIN.selectSingleNode("child::*/child::*[name()='SEQ']")
                If Not nodTemp Is Nothing Then
                    nodTemp2 = nodTemp.parentNode.selectSingleNode("child::*/child::*/child::*[name()='DOC']")
                    If Not nodTemp2 Is Nothing Then
                        nodTemp2 = nodTemp2.selectSingleNode("TG010/T1001")
                        If Not nodTemp2 Is Nothing Then
                            If oCargo.Bank = BabelFiles.Bank.SEB_NO Then
                                If nodTemp2.nodeTypedValue = "YW3" Then
                                    'This signifies KID, but may also be a structured payment
                                    'The assumption will be that if it is only 1 DOC it is a KID -payment, if not it is structured
                                    nodTempList = nodTemp.parentNode.selectNodes("child::*/child::*/child::*[name()='DOC']")
                                    If nodTempList.length > 1 Then
                                        sPayCode = "629"
                                    Else
                                        sPayCode = "510"
                                    End If
                                ElseIf nodTemp2.nodeTypedValue = "998" Then
                                    sPayCode = "629"
                                ElseIf nodTemp2.nodeTypedValue = "380" Then
                                    sPayCode = "629"
                                ElseIf nodTemp2.nodeTypedValue = "381" Then
                                    sPayCode = "629"
                                End If
                            Else
                                If Not IsOCR(sPayCode) Then
                                    If nodTemp2.nodeTypedValue = "YW3" Then
                                        'This signifies KID, but may also be a structured payment
                                        'The assumption will be that if it is only 1 DOC it is a KID -payment, if not it is structured
                                        nodTempList = nodTemp.parentNode.selectNodes("child::*/child::*/child::*[name()='DOC']")
                                        If nodTempList.length > 1 Then
                                            sPayCode = "629"
                                        Else
                                            sPayCode = "510"
                                        End If
                                    ElseIf nodTemp2.nodeTypedValue = "998" Then
                                        sPayCode = "629"
                                    ElseIf nodTemp2.nodeTypedValue = "999" Then
                                        sPayCode = "629"
                                    ElseIf nodTemp2.nodeTypedValue = "380" Then
                                        sPayCode = "629"
                                    ElseIf nodTemp2.nodeTypedValue = "381" Then
                                        sPayCode = "629"
                                    End If
                                Else
                                    'It is an OCR
                                End If
                            End If
                        End If
                    End If
                End If

                If Not nodTemp Is Nothing Then
                    nodTemp = Nothing
                End If
                If Not nodTemp2 Is Nothing Then
                    nodTemp2 = Nothing
                End If
                If Not nodTempList Is Nothing Then
                    nodTempList = Nothing
                End If
            End If

        End If
        'END XNET - 27.12.2011



        ''''''Old code
        ''''''06.03.2008 by Kjell next IF about SEB_NO
        '''''If oCargo.Bank = SEB_NO And iImportFormat = FileType.CREMUL Then
        '''''    Set nodTemp = oLIN.selectSingleNode("child::*/child::*[name()='SEQ']")
        '''''    If Not nodTemp Is Nothing Then
        '''''        Set nodTemp = nodTemp.parentNode.selectSingleNode("child::*/child::*/child::*[name()='DOC']")
        '''''        If Not nodTemp Is Nothing Then
        '''''            Set nodTemp = nodTemp.selectSingleNode("TG010/T1001")
        '''''            If Not nodTemp Is Nothing Then
        '''''                If nodTemp.nodeTypedValue = "YW3" Then
        '''''                    sPayCode = "510"
        '''''                End If
        '''''                Set nodTemp = Nothing
        '''''            End If
        '''''        End If
        '''''    End If
        '''''End If
        '*****************************************************************************************


        oCargo.PayCode = sPayCode


        If iImportFormat = BabelFiles.FileType.CREMUL Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            nMON_TransferredAmount = Val(GetPropValue("batch/mon_transferredamount", oLIN, oEDIMapping))
            bAmountSetTransferred = True
            'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sREF_Bank = GetPropValue("batch/ref_bank", oLIN, oEDIMapping)
            sBatch_ID = sREF_Bank
            'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            nCharges = Val(GetPropValue("batch/mon_chargesamount", oLIN, oEDIMapping)) / 100
            'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sCharges_Currency = GetPropValue("batch/mon_chargescurrency", oLIN, oEDIMapping)
            'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sCharges = GetPropValue("batch/mon_chargewho", oLIN, oEDIMapping)
            If Not Len(sCharges) = 0 Then
                If sCharges = "13" Then
                    bCharges_Domestic = True
                    bCharges_Abroad = True
                ElseIf sCharges = "14" Then
                    bCharges_Domestic = True
                    bCharges_Abroad = False
                ElseIf sCharges = "15" Then
                    bCharges_Domestic = False
                    bCharges_Abroad = False
                Else
                    'MsgBox "Babelerror, unknown code for charges"
                End If
                'XokNET - 27.12.2011 - Added the Else-clause with the defaultvalues
            Else
                bCharges_Domestic = True
                bCharges_Abroad = False
            End If

            If oCargo.Bank = BabelFiles.Bank.DnBNOR_SE Then
                'Store BGCno. in ocargo.ref
                oCargo.REF = sREF_Own
            ElseIf oCargo.Bank = BabelFiles.Bank.DnB Then
                'Sometimes the currency is stated in th LIN, but not in the SEQ.
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oCargo.MONCurrency = GetPropValue("batch/mon_transferredcurrency", oLIN, oEDIMapping)
            ElseIf oCargo.Bank = BabelFiles.Bank.Fellesdata Then
                '21.09.2010 - Added this ElseIf
                'Sometimes the currency is stated in th LIN, but not in the SEQ.
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oCargo.MONCurrency = GetPropValue("batch/mon_transferredcurrency", oLIN, oEDIMapping)
            ElseIf oCargo.Bank = BabelFiles.Bank.Nordea_eGateway Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oCargo.MONCurrency = GetPropValue("batch/mon_transferredcurrency", oLIN, oEDIMapping)
            End If
        End If

        'XokNET 19.08.2013 - Added next IF
        If iImportFormat = BabelFiles.FileType.CREMUL Then
            If oCargo.Bank = BabelFiles.Bank.Danske_Bank_dk Then
                If bProfileInUse Then
                    'Retrieve information about default currency and country from the client, because this format is transnational
                    bAccountFound = False

                    oClient = FindExistingClient(oProfile, iFilesetup_ID, sI_Account)
                    If Not oClient Is Nothing Then
                        For Each oaccount In oClient.Accounts
                            If oaccount.Account = sI_Account Then
                                If EmptyString(oaccount.CurrencyCode) Then
                                    Err.Raise(35049, "ReadEDIFile", LRS(35049, sI_Account))
                                    '35049: No currencycode stated on the account %1." & vbLf & "You must enter this in the client setup."
                                Else
                                    oCargo.Temp = Trim(oaccount.CurrencyCode)
                                End If
                                If EmptyString(oaccount.DebitAccountCountryCode) Then
                                    Err.Raise(35050, "ReadEDIFile", LRS(35050, sI_Account))
                                    '35050: No countrycode stated on the account %1." & vbLf & "You must enter this in the client setup."
                                Else
                                    oCargo.Temp = oCargo.Temp & ";" & Trim(oaccount.DebitAccountCountryCode)
                                End If
                                bAccountFound = True
                                Exit For
                            End If
                        Next oaccount
                    End If

                    If Not bAccountFound And RunTime() Then
                        Err.Raise(35028, "ReadEDIFile", LRS(35028, sI_Account))
                        '12003: Konto %1 er ukjent. Vennligst legg den inn under 'Oppsett'.
                    End If
                End If
            End If
        End If
        'End XokNET 19.08.2013 - Added next IF

        If iImportFormat = BabelFiles.FileType.PAYMUL Or iImportFormat = BabelFiles.FileType.DEBMUL Then
            If iImportFormat = BabelFiles.FileType.DEBMUL And oCargo.Bank <> BabelFiles.Bank.Danske_Bank_NO Then
                'Don't set invoiceamount here
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nMON_InvoiceAmount = Val(GetPropValue("batch/mon_invoiceamount", oLIN, oEDIMapping))
            End If
            bAmountSetInvoice = True

            If iImportFormat = BabelFiles.FileType.DEBMUL Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sREF_Bank = GetPropValue("batch/ref_bank", oLIN, oEDIMapping)
                sStatusCode = "02"
            Else
                sStatusCode = "00"
            End If

            If sPayType = "I" Then
                sInvCurrency = ""
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sInvCurrency = GetPropValue("batch/mon_invoicecurrency", oLIN, oEDIMapping)
                'The following code is not tested
                ' It is probably wrong if the currency on LIN differs from the currency on SEQ
                sTraCurrency = ""
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTraCurrency = GetPropValue("batch/mon_transfercurrency", oLIN, oEDIMapping)
                sERADealMadeWith = ""
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sERADealMadeWith = GetPropValue("payment/era_dealmadewith", oLIN, oEDIMapping)
                If Len(sERADealMadeWith) > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    nERAExchRateAgreed = Val(GetPropValue("payment/era_exchrateagreed", oLIN, oEDIMapping))
                Else
                    nERAExchRateAgreed = 0
                End If
                'dERADate =
                sFRWForwardContractNo = ""
                'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sFRWForwardContractNo = GetPropValue("payment/frw_forwardcontractno", oLIN, oEDIMapping)
                If Len(sFRWForwardContractNo) > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    nFRWForwardContractRate = Val(GetPropValue("payment/frw_forwardcontractrate", oLIN, oEDIMapping))
                Else
                    nFRWForwardContractRate = 0
                End If
                ' End of untested code
            Else
                If oCargo.Bank = BabelFiles.Bank.Nordea_eGateway Then
                    sInvCurrency = GetPropValue("batch/mon_invoicecurrency", oLIN, oEDIMapping)
                    If EmptyString(sInvCurrency) Then
                        sTraCurrency = sInvCurrency
                    End If
                    sTraCurrency = sInvCurrency
                Else
                    sInvCurrency = "NOK"
                End If
            End If
        ElseIf iImportFormat = BabelFiles.FileType.BANSTA Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oLIN. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sREF_Bank = GetPropValue("batch/ref_bank", oLIN, oEDIMapping)
            sStatusCode = "01"
        End If

        nodActive = oLIN
        nodActive = oLIN.selectSingleNode("GROUP/SEQ")

        lCounter = 0

        'Set nodActive = nodActive.childNodes(lCounter)


        Do While True
            '    'The following code can be used to control the usage of memory
            '    If lCounter / 1000 = Int(lCounter / 1000) Then
            '        bx = MemoryMessage(lCounter, "vbBabel")
            '    End If
            '    'End control
            If nodActive.nodeName = "SEQ" Then

                bJustAddMoreFreetext = False
                If iImportFormat = BabelFiles.FileType.CREMUL Then
                    If oCargo.Bank = BabelFiles.Bank.BBS Or oCargo.Bank = BabelFiles.Bank.Nordea_NO Then '01.09.2010 - Previous it said 'Or Nordea_NO' without oCargo.Bank =
                        If sPayType = "D" Then
                            If sPayCode = "601" Or "603" Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object nodActive.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                nSEQAmount = Val(GetPropValue("payment/mon_invoiceamount", nodActive.parentNode, oEDIMapping))
                                If nSEQAmount = 0 Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object nodActive.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    nSEQAmount = Val(GetPropValue("payment/mon_transferredamount", nodActive.parentNode, oEDIMapping))
                                    If nSEQAmount = 0 Then
                                        'We have a 0-payment
                                        'Check other parameters to see if it is a splitted payment
                                        If Payments.Count > 0 Then 'Don't continue if it is the first payment
                                            'UPGRADE_WARNING: Couldn't resolve default property of object nodActive.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            sSEQName = GetPropValue("payment/e_name", nodActive.parentNode, oEDIMapping, 1)
                                            If sSEQName = oPayment.E_Name Then
                                                'UPGRADE_WARNING: Couldn't resolve default property of object nodActive.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                sSEQAccount = Trim(GetPropValue("payment/e_account", nodActive.parentNode, oEDIMapping))
                                                If sSEQAccount = oPayment.E_Account Then
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object nodActive.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    sSEQAdr1 = GetPropValue("payment/e_adr1", nodActive.parentNode, oEDIMapping, 1)
                                                    If sSEQAdr1 = oPayment.E_Adr1 Then
                                                        If oPayment.Invoices.Count = 1 Then
                                                            'Date are always identical inside a batch
                                                            bJustAddMoreFreetext = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                '            nMON_TransferredAmount = Val(GetPropValue("payment/mon_transferredamount", oSEQ, oEDIMapping))
                '            bAmountSetTransferred = True
                '            sMON_TransferCurrency = GetPropValue("payment/mon_transfercurrency", oSEQ, oEDIMapping)
                '            nMON_InvoiceAmount = Val(GetPropValue("payment/mon_invoiceamount", oSEQ, oEDIMapping))
                '            If nMON_InvoiceAmount = 0 Then
                '                nMON_InvoiceAmount = nMON_TransferredAmount
                '            End If

                If Not bJustAddMoreFreetext Then 'Normal situation
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.Cargo = oCargo
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    ' oPayment.DATE_Payment =VB6.Format(dDATE_Production, "YYYYMMDD") 'dDATE_Production
                    oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                    oPayment.DATE_Payment = VB6.Format(dPayment_Date, "YYYYMMDD") 'dPayment_Date
                    oPayment.PayType = sPayType
                    oPayment.PayCode = sPayCode
                    oPayment.I_Account = sI_Account
                    If Not EmptyString(sBANK_I_SWIFTCode) Then
                        oPayment.BANK_I_SWIFTCode = sBANK_I_SWIFTCode
                    End If
                    If iImportFormat = BabelFiles.FileType.CREMUL Then
                        oPayment.MON_ChargesAmount = nCharges
                        oPayment.MON_ChargesCurrency = sCharges_Currency
                        oPayment.MON_ChargeMeDomestic = bCharges_Domestic
                        oPayment.MON_ChargeMeAbroad = bCharges_Abroad
                    End If
                    If iImportFormat = BabelFiles.FileType.PAYMUL Then
                        oPayment.MON_InvoiceCurrency = sInvCurrency
                        oPayment.MON_TransferCurrency = sTraCurrency
                        oPayment.ERA_DealMadeWith = sERADealMadeWith
                        oPayment.ERA_ExchRateAgreed = nERAExchRateAgreed
                        oPayment.FRW_ForwardContractNo = sFRWForwardContractNo
                        oPayment.FRW_ForwardContractRate = nFRWForwardContractRate
                        If oCargo.Bank = BabelFiles.Bank.Nordea_SE Then
                            bToOwnAccount = True
                            bPriority = True
                        End If
                    End If
                    If iImportFormat = BabelFiles.FileType.DEBMUL Then
                        oPayment.ERA_DealMadeWith = sERADealMadeWith
                        oPayment.ERA_ExchRateAgreed = nERAExchRateAgreed
                        oPayment.FRW_ForwardContractNo = sFRWForwardContractNo
                        oPayment.FRW_ForwardContractRate = nFRWForwardContractRate
                    End If
                    If ImportFormat = BabelFiles.FileType.BANSTA Then
                        oPayment.REF_Own = sREF_Own
                    End If

                    bReturnValue = oPayment.ImportEDI(nodActive.parentNode, oEDIMapping, False)
                    'New 24.10.2005 - version 1.03.44
                    'Special import for internal DnBNOR transactions, retrieve the batch bankreference
                    '  from the payment level
                    'Qualificator ACK is set in REFBank1 - 6 positions
                    'Qualificator AEK is set in REFBank2
                    'Qualificator ACD is not imported unless ACK is missing and then it is stored in REFBank1
                    If iImportFormat = BabelFiles.FileType.CREMUL Then
                        If oCargo.Bank = BabelFiles.Bank.DnB Then
                            If EmptyString(sREF_Bank) Then
                                If EmptyString((oPayment.REF_Bank2)) Then
                                    sREF_Bank = TransformDnBNORReference((oPayment.REF_Bank1)) 'ACK - Telebank
                                Else
                                    ' XNET 20.03.0213 - Vegfinans vil ha hele referansen - NB! De har ogs� en spesialmapping, hvor vi henter REF_Bank2 fra ACD (DNB-mapping)
                                    If oCargo.Special = "VEGFINANS" Then
                                        sREF_Bank = Trim$(oPayment.REF_Bank2) ' ACD
                                    Else
                                        sREF_Bank = Right$(Trim(oPayment.REF_Bank2), 6) ' AEK (ACD)
                                    End If
                                End If
                            End If
                        End If
                    End If


                Else
                    bReturnValue = oPayment.ImportEDI(nodActive.parentNode, oEDIMapping, True)
                End If
                nodActive = nodActive.parentNode.nextSibling
                If nodActive Is Nothing Then
                    'FIX: BabelError
                    Exit Do
                End If
                nodActive = nodActive.childNodes(0)
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object nodActive. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nodActive = nodActive.nextSibling
                If nodActive Is Nothing Then
                    'FIX: BabelError
                    Exit Do
                End If
            End If
            lCounter = lCounter + 1
        Loop

        'Changed 29/6-2004 Create problems because from Nordea you can get SWIFT-payments
        ' with differences between Transefer and Invoice amount (charges). There setting the
        ' Batch.Mon_InvoiceAmount = MON_TransferredAmount creates an error
        'If oPayment.PayType = "I" Then

        If iImportFormat = BabelFiles.FileType.DEBMUL Then
            bx = AddTransferredAmountOnBatch()
            bx = AddInvoiceAmountOnBatch()
        Else
            bx = AddInvoiceAmountOnBatch()
        End If

        'Else
        '    nMON_InvoiceAmount = nMON_TransferredAmount
        '    bAmountSetInvoice = True
        'End If

        'Alternativve code


    End Function
    Friend Function ReadPain001(ByRef nodBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager, ByVal sFileNameIn As String) As Boolean
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim nodeList2 As System.Xml.XmlNodeList
        Dim node2 As System.Xml.XmlNode
        Dim sCounterFromFile As String
        Dim sTmp As String
        Dim sTmp2 As String
        Dim sTmp3 As String
        Dim sPayType As String
        Dim sPayCode As String
        Dim nTmp As Double
        Dim dBookDate As Date = DateTime.MinValue
        Dim dValueDate As Date = DateTime.MinValue
        Dim bReturnValue As Boolean = True
        Dim oFreeText As vbBabel.Freetext

        Try
            sREF_Own = GetInnerText(nodBatch, "ISO:PmtInfId", nsMgr)
            oCargo.REF = sREF_Own
            oCargo.DATE_Payment = GetInnerText(nodBatch, "ISO:ReqdExctnDt", nsMgr).Replace("-", "")
            oCargo.I_Account = GetInnerText(nodBatch, "ISO:DbtrAcct/ISO:Id/ISO:IBAN", nsMgr)
            ' not IBAN?
            If EmptyString(oCargo.I_Account) Then
                oCargo.I_Account = GetInnerText(nodBatch, "ISO:DbtrAcct/ISO:Id/ISO:Othr/ISO:Id", nsMgr)
            End If
            oCargo.BANK_I_SWIFTCode = GetInnerText(nodBatch, "ISO:DbtrAgt/ISO:FinInstnId/ISO:BIC", nsMgr)
            sTmp = GetInnerText(nodBatch, "ISO:PmtTpInf/ISO:LclInstrm/ISO:Cd", nsMgr)
            Select Case sTmp
                Case "DO"
                    oCargo.PayType = "D"
                Case "IN", "SEPA", "SDCL", "CII"
                    oCargo.PayType = "I"
                Case "NURG" '27.05.2020 - Probablu not correct, could also be "I" . need more examples
                    oCargo.PayType = "D"
                Case Else
                    oCargo.PayType = "D"
            End Select

            ' VBDISCUSS - skal vi ha med denne?
            sTmp = GetInnerText(nodBatch, "ISO:PmtTpInf/ISO:SvcLvl/ISO:Cd", nsMgr)
            If sTmp = "SEPA" Then
                'oCargo.PayType = "I"
            End If

            'Added 27.05.2020 to find paycode
            sTmp2 = GetInnerText(nodBatch, "ISO:PmtMtd", nsMgr)
            Select Case sTmp2
                'THis must obviously be changed when we get more examples/information
                'Mark also that the 150 code may be changed in payment if it is a OCR, FIK or structured payment etc...
                Case "TRF"
                    If sTmp = "URGP" Then
                        oCargo.PayCode = "175"  ' haste
                    Else
                        oCargo.PayCode = "150"
                    End If
                Case Else
                    oCargo.PayCode = "150"

            End Select

            ' Loop p� CdtTrfTxInf with PmtInf, and create payments pr CdtTrfTxInf
            nodeList = nodBatch.SelectNodes("ISO:CdtTrfTxInf", nsMgr)

            If nodeList.Count > 0 Then
                For Each node In nodeList

                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    If bProfileInUse Then
                        oPayment.VB_Profile = oProfile
                    End If
                    oPayment.VB_ProfileInUse = bProfileInUse
                    oPayment.Cargo = oCargo
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    ' oPayment.DATE_Payment =VB6.Format(dDATE_Production, "YYYYMMDD") 'dDATE_Production
                    'oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                    'oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                    'oPayment.PayType = sPayType
                    'oPayment.PayCode = sPayCode
                    'If Left(oCargo.I_Account, 2) = "PG" Then
                    '    oPayment.I_AccountType = BabelFiles.AccountType.SE_PlusGiro
                    '    oPayment.I_Account = Mid(oCargo.I_Account, 3)
                    'ElseIf Left(oCargo.I_Account, 2) = "BG" Then
                    '    oPayment.I_AccountType = BabelFiles.AccountType.SE_Bankgiro
                    '    oPayment.I_Account = Mid(oCargo.I_Account, 3)
                    'Else
                    '    oPayment.I_Account = oCargo.I_Account
                    'End If
                    ''05.02.2014 . Removed next IF. The Cargo.REF may be used in the population of Payment.
                    ''If the value changed with a new batch, the Cargo.Ref didn't change because the IF-test below
                    ''This created a wrong output for Gjensidige, but I don't think it is critical if this change creates problems for aother customers
                    ''If EmptyString(oCargo.REF) Then
                    'oCargo.REF = sREF_Bank
                    ''End If

                    bReturnValue = oPayment.ReadPain001(node, nsMgr, sFileNameIn, sCounterFromFile)

                Next node

                AddInvoiceAmountOnBatch()
                AddTransferredAmountOnBatch()

            End If

            Return bReturnValue

        Catch ex As Exception

            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadPain001_Batch" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sCounterFromFile, ConvertFromAmountToString(MON_InvoiceAmount, " ", ",")))
            End If

        Finally

            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If
            If Not nodeList2 Is Nothing Then
                nodeList2 = Nothing
            End If
            If Not node2 Is Nothing Then
                node2 = Nothing
            End If

        End Try

    End Function
    Friend Function ReadCamt054(ByRef nodBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager, ByVal sFileNameIn As String) As Boolean
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim nodeList2 As System.Xml.XmlNodeList
        Dim node2 As System.Xml.XmlNode
        Dim nodeTemp As System.Xml.XmlNode
        Dim sCounterFromFile As String
        Dim sTmp As String
        Dim sTmp2 As String
        Dim sTmp3 As String
        Dim sPayType As String
        Dim sPayCode As String
        Dim nTmp As Double
        Dim dBookDate As Date = DateTime.MinValue
        Dim dValueDate As Date = DateTime.MinValue
        Dim bReturnValue As Boolean = True
        Dim oFreeText As vbBabel.Freetext
        Dim sTmpPayCode As String

        '*Ntry - Occurence 1 .. unbounded - Set of elements used to specify an entry in the debit credit notification.
        '   *NtryRef - Occurence 1 .. 1 - Type Max35Text 
        '       NOT IMPLEMENTED YET
        '       ISO Definition Unique reference for the entry. 
        '       Use: Increased by one for each entry.
        '   *Amt - Occurence 1 .. 1
        '       Set in BabelFile
        '       Amount of money in the cash entry. 
        '       Use: Posted amount to the account can contain one or many entry details.
        '       Ccy - ATTRIBUTE
        '   *CdtDbtInd - Occurence 1 .. 1
        '       Used in BabelFile
        '       If the amount is zero, the indicator is credit.
        '       Allowed Codes: 
        '           CRDT Credit 
        '           DBIT Debit 
        '   *Sts - Occurence 1 .. 1
        '       NOT IMPLEMENTED YET
        '       Allowed Codes: 
        '           BOOK Booked 
        '   *BookgDt - Occurence 1 .. 1 - Date and time when an entry is posted to an account on the account servicer's books
        '       *Dt - Occurence 1 .. 1     
        '   *ValDt - Occurence 1 .. 1 - 
        '       Date and time at which assets become available to the account owner in case of a credit entry, or cease to be available to the account owner in case of a debit entry.
        '       *Dt - Occurence 1 .. 1 
        '   *AcctSvcrRef - Occurence 0 .. 1 - Unique reference as assigned by the account servicing institution to unambiguously identify the entry.
        '       Sweden: Bank reference will only be sent for TotalIN and card transactions. For incoming international payments, except through TotalIN, no reference will be available. 
        '           When no bank reference is present, see Usage. 
        '       Finland: For reference payments (KTL), no reference is present on entry level. For all other transactions (from Tito) bank reference may be present. 
        '       Denmark: For Direct Debit payments the bank reference will contain the debtor group number. 
        '           For direct debit transactions from Leverand�rService no bank reference will be sent. 
        '       Poland: For direct debit transactions from Poland no bank reference is given.
        '   *BkTxCd - Occurence 1 .. 1 - Set of elements used to fully identify the type of underlying transaction resulting in an entry.
        '       *Domn - Occurence 1 .. 1
        '           *Cd - 1 .. 1 - Length 1 .. 4 - ISO Definition: Specifies the business area of the underlying transaction.
        '           *Fmly - Occurence 1 .. 1
        '               *Cd - 1 .. 1 - Length 1 .. 4 - Specifies the family within a domain.
        '               SubFmlyCd - Occurence 1 .. 1 - Length 1 .. 4 - Specifies the sub-product family within a specific family.
        '       *Prtry - Occurence 0 .. 1 - Used by Nordea for payment service description.
        '           *Cd - Occurence 1 .. 1 - Proprietary bank transaction code to identify the underlying transaction
        '           *Issr - Occurence 0 .. 1 - Identification of the issuer of the proprietary bank transaction code.
        '               Allowed Codes: NORDEA 
        '   *Chrgs - Occurence 0 .. 1 
        '       NORDEA: Nordea books all charges in separate entries according to rules in �Payment Services Directive� (PSD). This means, that this element is NOT used by Nordea, as ISO definition only allows reporting of charges included in the entry amount.
        '       *Amt - Occurence 1 .. 1 - Transaction charges to be paid by the charge bearer
        '           *Ccy - ATTRIBUTE - 
        '   *NtryDtls - SEE SEPARATE SPECIFICATIONS

        Try
            sCounterFromFile = GetInnerText(nodBatch, "ISO:NtryRef", nsMgr)
            'GetInnerText(nodBatch, "//ISO:Amt", nsMgr) - Set from BabelFile
            ' 07.09.2016 added next line to see if we have Amt-tag in file;
            sTmp = GetInnerText(nodBatch, "ISO:Amt/ISO:Ccy", nsMgr)
            If Not EmptyString(sTmp) Then
                oCargo.MONCurrency = nodBatch.SelectSingleNode("ISO:Amt", nsMgr).Attributes.GetNamedItem("Ccy").Value
            Else
                '27.11.2019 - Added for Societe Genral in France (mo amount is stated in the AmountDetails under TxDtls)
                'Might have implications for other customers, but I can't see why
                'The first If probably never returned anything because Ccy is an attribute
                nodeTemp = nodBatch.SelectSingleNode("ISO:Amt", nsMgr)
                If Not nodeTemp Is Nothing Then
                    sTmp = ""
                    sTmp = nodeTemp.Attributes.GetNamedItem("Ccy").Value
                    oCargo.MONCurrency = sTmp
                    nodeTemp = Nothing
                End If
            End If
            sTmp = ""
            sTmp = GetInnerText(nodBatch, "ISO:BookgDt/ISO:Dt", nsMgr)
            If Not EmptyString(sTmp) Then
                dBookDate = DateSerial(CInt(Left(sTmp, 4)), CInt(Mid(sTmp, 6, 2)), CInt(Mid(sTmp, 9, 2)))
            End If
            sTmp = ""
            sTmp = GetInnerText(nodBatch, "ISO:ValDt/ISO:Dt", nsMgr)
            If Not EmptyString(sTmp) Then
                dValueDate = DateSerial(CInt(Left(sTmp, 4)), CInt(Mid(sTmp, 6, 2)), CInt(Mid(sTmp, 9, 2)))
            End If
            '20.04.2020 - Added for Societe General Fr
            If oCargo.Bank = BabelFiles.Bank.SocieteGenerale_France Then
                sTmp = ""
                sTmp = GetInnerText(nodBatch, "ISO:AddtlNtryInf", nsMgr)
                If Not EmptyString(sTmp) Then
                    sTmp = sTmp.Trim
                    If sTmp.Length >= 16 Then
                        If sTmp.Substring(0, 6) = "/DTCP/" Then
                            If ValidateDate(sTmp.Substring(6, 10), "YYYY-MM-DD", False, , System.DateTime.FromOADate(0), System.DateTime.FromOADate(0)) Then
                                dValueDate = StringToDate(sTmp.Substring(6, 4) & sTmp.Substring(11, 2) & sTmp.Substring(14, 2))
                                dBookDate = dValueDate
                            End If
                        End If
                    End If
                End If
            End If

            If dBookDate = DateTime.MinValue Then
                If dValueDate = DateTime.MinValue Then
                    '16.04.2020 - Added next IF for Societe General Fr -Using CreDtTm for book and valuedate
                    If EmptyString(oCargo.DATE_Payment) Then
                        dValueDate = Now
                        dBookDate = Now
                    Else
                        dValueDate = StringToDate(oCargo.DATE_Payment)
                        dBookDate = dValueDate
                    End If
                Else
                    dBookDate = dValueDate
                End If
            Else
                If dValueDate = DateTime.MinValue Then
                    dValueDate = dBookDate
                End If
            End If

            '23.07.2020 - Why the next line
            'For camt.054D (Corporate Access Nordea was the example) retrieved this correct earlier in the function,
            'but when we try to retrieve it her we get nothing and the AcctSvcrRef is not stored.
            'Kjell doesn't dare to just delete the line, but have rewritten it
            'Old code
            'sREF_Bank = GetInnerText(nodBatch, "ISO:AcctSvcrRef", nsMgr)
            'New code
            sTmp = GetInnerText(nodBatch, "ISO:AcctSvcrRef", nsMgr)
            If Not EmptyString(sTmp) Then
                sREF_Bank = sTmp
            End If

            ' 03.09.2019
            ' added next If to find if Camt.054 is from Nets or DNB
            ' 30.10.2019 - Added Danske Bank
            If IsDnB(oCargo.Bank) Or IsDanskeBank(oCargo.Bank) Then
                sBUSString = ""
                oCargo.Temp = ""
                node = nodBatch.SelectSingleNode("ISO:BkTxCd/ISO:Prtry", nsMgr)
                If Not node Is Nothing Then
                    oCargo.Temp = GetInnerText(node, "ISO:Issr", nsMgr)
                    If IsDanskeBank(oCargo.Bank) Then
                        sTmp2 = GetInnerText(node, "ISO:Cd", nsMgr)
                        If Not EmptyString(sTmp2) Then
                            If sTmp2.Length > 4 Then
                                If sTmp2.Substring(0, 2) = "NO" Then
                                    If vbIsNumeric(sTmp2.Substring(2, 3), "0123456789") Then
                                        '20.11.2019 - Added next IF
                                        If sTmp2.Length > 10 Then
                                            sREF_Bank = sTmp2.Substring(6).Trim
                                        End If
                                        sTmp2 = sTmp2.Substring(2, 3)
                                    Else
                                        oCargo.Temp = ""
                                        sTmp2 = ""
                                    End If
                                Else
                                    If sTmp2.ToUpper = "KONTOT�MMING" Then
                                        sTmp2 = sTmp2.ToUpper
                                    Else
                                        oCargo.Temp = ""
                                        sTmp2 = ""
                                    End If
                                End If
                            Else
                                oCargo.Temp = ""
                                sTmp2 = ""
                            End If
                        Else
                            oCargo.Temp = ""
                            sTmp2 = ""
                        End If
                    End If
                End If
            End If

            node = nodBatch.SelectSingleNode("ISO:BkTxCd/ISO:Domn", nsMgr)
            sTmpPayCode = ""

            ' 10.09.2019
            ' Added next If, to catch paymentcodes from Nets-files to DNB (Tip from H�kon Belt)
            ' Use the "old" (Cremul) codes 230.231.232.233.234.240
            If IsDnB(oCargo.Bank) And oCargo.Temp = "NETS" Then
                node = nodBatch.SelectSingleNode("ISO:BkTxCd/ISO:Prtry", nsMgr)
                If Not node Is Nothing Then
                    sTmp = GetInnerText(node, "ISO:Cd", nsMgr)
                End If
            ElseIf IsDanskeBank(oCargo.Bank) And oCargo.Temp = "DBA" Then
                ' 30.10.2019 - Added Danske Bank
                '02.11.2019 - Added for SG Finans and Visma
                sTmpPayCode = GetInnerText(node, "ISO:Cd", nsMgr) & "-" & GetInnerText(node, "ISO:Fmly/ISO:Cd", nsMgr) & "-" & GetInnerText(node, "ISO:Fmly/ISO:SubFmlyCd", nsMgr)
                sTmp = sTmp2
            ElseIf Not node Is Nothing Then
                sTmp = GetInnerText(node, "ISO:Cd", nsMgr)
                node = node.SelectSingleNode("ISO:Fmly", nsMgr)
                sTmp2 = GetInnerText(node, "ISO:Cd", nsMgr)
                sTmp3 = GetInnerText(node, "ISO:SubFmlyCd", nsMgr)
                sTmpPayCode = sTmp & "-" & sTmp2 & "-" & sTmp3
            Else
                node = nodBatch.SelectSingleNode("ISO:BkTxCd/ISO:Prtry", nsMgr)
                If Not node Is Nothing Then
                    sTmp = GetInnerText(node, "ISO:Cd", nsMgr)
                End If
            End If
            ' 10.09.2019
            ' Added next If, to catch paymentcodes from Nets-files to DNB (Tip from H�kon Belt)
            ' Use the "old" (Cremul) codes 230.231.232.233.234.240
            If IsDnB(oCargo.Bank) And oCargo.Temp = "NETS" Then
                ' 16.10.2019 - save NETS codes like 230, 233, 240 etc
                sBUSString = sTmp
                sPayCode = bbSetPayCode(sTmp, sPayType, "CREMUL", oCargo.Credit, sTmp2, sTmp3, oCargo.CountryName, , oCargo.Bank)
            ElseIf IsDanskeBank(oCargo.Bank) And oCargo.Temp = "DBA" Then
                ' 30.10.2019 - Added Danske Bank
                'save NETS codes like 230, 233, 240 etc
                sBUSString = sTmp
                If sTmp2 = "KONTOT�MMING" Then
                    sPayCode = "720"
                    sPayType = "D"
                Else
                    sPayCode = bbSetPayCode(sTmp, sPayType, "CREMUL", oCargo.Credit, sTmp2, sTmp3, oCargo.CountryName, , oCargo.Bank)
                End If
            Else
                sPayCode = bbSetPayCode(sTmp, sPayType, "ISO20022", oCargo.Credit, sTmp2, sTmp3, oCargo.CountryName, , oCargo.Bank)
            End If

            'Structured incoming payments doesn't have a seperate code
            'Check if we have structured paymentinfo
            If oCargo.Credit Then
                If sTmp = "PMNT" And sPayType = "D" Then
                    If sTmp2 = "RCDT" Then
                        If sTmp3 = "AUTT" Then
                            node = nodBatch.SelectSingleNode("ISO:NtryDtls/ISO:TxDtls/ISO:RmtInf/ISO:Strd", nsMgr)
                            If Not node Is Nothing Then
                                sPayCode = "629"
                            End If
                        End If
                    End If
                End If
            End If

            '24.01.2018 - Added this code to secure that we add something
            If EmptyString(sPayCode) Then
                sPayCode = "601"
            End If
            If EmptyString(sPayType) Then
                sPayType = "D"
            End If

            'sPayType = ""
            node = nodBatch.SelectSingleNode("ISO:Chrgs", nsMgr)
            If Not node Is Nothing Then
                sTmp = GetInnerText(node, "ISO:Amt", nsMgr)
                nTmp = ConvertToAmount(sTmp, "", ".", , True)
            End If

            nodeList = nodBatch.SelectNodes("ISO:NtryDtls", nsMgr)

            If nodeList.Count = 0 Then
                '27.06.2017 - Moved code after this IF, because there are other situation we also do not get any detailinfo
                'oPayment = oPayments.Add()
                'oPayment.objFile = oFile
                'If bProfileInUse Then
                '    oPayment.VB_Profile = oProfile
                'End If
                'oPayment.VB_ProfileInUse = bProfileInUse
                'oPayment.Cargo = oCargo
                'oPayment.ImportFormat = iImportFormat
                'oPayment.StatusCode = sStatusCode
                'oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                'oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                'oPayment.PayType = sPayType
                'oPayment.PayCode = sPayCode
                'If Left(oCargo.I_Account, 2) = "PG" Then
                '    oPayment.I_AccountType = BabelFiles.AccountType.SE_PlusGiro
                '    oPayment.I_Account = Mid(oCargo.I_Account, 3)
                'ElseIf Left(oCargo.I_Account, 2) = "BG" Then
                '    oPayment.I_AccountType = BabelFiles.AccountType.SE_Bankgiro
                '    oPayment.I_Account = Mid(oCargo.I_Account, 3)
                'Else
                '    oPayment.I_Account = oCargo.I_Account
                'End If
                ''Retrieve info about the amount here not at the payment/invoice-level
                'sTmp = GetInnerText(nodBatch, "ISO:Amt", nsMgr)
                'If Not EmptyString(sTmp) Then
                '    nTmp = ConvertToAmount(sTmp, "", ".")
                'End If

                'oPayment.MON_InvoiceAmount = nTmp
                'oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                'oPayment.MON_TransferredAmount = nTmp
                'oPayment.MON_TransferCurrency = oCargo.MONCurrency
                'oPayment.MON_AccountAmount = nTmp
                'oPayment.MON_AccountCurrency = oCargo.MONCurrency
                'oPayment.MON_OriginallyPaidAmount = nTmp
                'oPayment.MON_OriginallyPaidCurrency = oCargo.MONCurrency
                'oPayment.REF_Bank1 = sREF_Bank

                'If bProfileInUse Then
                '    oPayment.VB_FilenameOut_ID = CheckAccountNo(oCargo.I_Account, "", oProfile)
                'End If

                'oInvoice = oPayment.Invoices.Add()
                'If bProfileInUse Then
                '    oInvoice.VB_Profile = oProfile
                'End If
                'oInvoice.Cargo = oCargo
                'oInvoice.ImportFormat = iImportFormat
                'oInvoice.StatusCode = sStatusCode
                'oInvoice.MON_InvoiceAmount = nTmp
                'oInvoice.MON_TransferredAmount = nTmp

                'AddInvoiceAmountOnBatch()
                'AddTransferredAmountOnBatch()
            Else

                For Each node In nodeList

                    '15.10.2014 - Added for DnB INPS (Visma Collectors)
                    sTmp = GetInnerText(node, "ISO:Btch/ISO:PmtInfId", nsMgr)
                    If Not EmptyString(sTmp) Then
                        oCargo.REF = sTmp
                        ' 22.08.2019 - if we have no REF_Bank at batchlevel, use this ref from PmtInfId
                        If EmptyString(sREF_Bank) Then
                            sREF_Bank = sTmp
                        End If
                    End If

                    nodeList2 = node.SelectNodes("ISO:TxDtls", nsMgr) 'Not sure what this level is but according to documentation we may have many TxDtls within a NtryDtls

                    For Each node2 In nodeList2
                        'nodeTemp = node.SelectSingleNode("//ISO:Acct/ISO:Id", nsMgr)
                        oPayment = oPayments.Add()
                        oPayment.objFile = oFile
                        If bProfileInUse Then
                            oPayment.VB_Profile = oProfile
                        End If
                        oPayment.VB_ProfileInUse = bProfileInUse
                        oPayment.Cargo = oCargo
                        oPayment.ImportFormat = iImportFormat
                        oPayment.StatusCode = sStatusCode
                        ' oPayment.DATE_Payment =VB6.Format(dDATE_Production, "YYYYMMDD") 'dDATE_Production
                        oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                        oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                        oPayment.PayType = sPayType
                        oPayment.PayCode = sPayCode
                        If Left(oCargo.I_Account, 2) = "PG" Then
                            oPayment.I_AccountType = BabelFiles.AccountType.SE_PlusGiro
                            oPayment.I_Account = Mid(oCargo.I_Account, 3)
                        ElseIf Left(oCargo.I_Account, 2) = "BG" Then
                            oPayment.I_AccountType = BabelFiles.AccountType.SE_Bankgiro
                            oPayment.I_Account = Mid(oCargo.I_Account, 3)
                        Else
                            oPayment.I_Account = oCargo.I_Account
                        End If
                        ' 03.09.2019
                        ' added next If to find if Camt.054 is from Nets or DNB
                        If IsDnB(oCargo.Bank) Then
                            If oCargo.Temp = "NETS" Then
                                oPayment.I_AccountType = BabelFiles.AccountType.NO_Nets
                            End If
                        End If

                        '05.02.2014 . Removed next IF. The Cargo.REF may be used in the population of Payment.
                        'If the value changed with a new batch, the Cargo.Ref didn't change because the IF-test below
                        'This created a wrong output for Gjensidige, but I don't think it is critical if this change creates problems for aother customers
                        'If EmptyString(oCargo.REF) Then
                        oCargo.REF = sREF_Bank
                        'End If

                        '06.11.2019 - Added SGF_EQ
                        '01.08.2021 - Added NORDEAFINANCE_APTIC
                        If oCargo.Special = "SG FINANS" Or oCargo.Special = "SGF_EQ" Or oCargo.Special = "NORDEAFINANCE_APTIC" Then
                            oPayment.Visma_StatusCode = sTmpPayCode
                        End If

                        '27.11.2019 - Added for Societe Genral in France (mo amount is stated in the AmountDetails
                        If nodeList.Count = 1 And nodeList2.Count = 1 Then
                            nodeTemp = node2.SelectSingleNode("ISO:AmtDtls", nsMgr)
                            If nodeTemp Is Nothing Then
                                sTmp = "0"
                                sTmp = GetInnerText(nodBatch, "ISO:Amt", nsMgr)
                                oPayment.MON_InvoiceAmount = ConvertToAmount(sTmp, "", ".", , True)
                                oPayment.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                                oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                                oPayment.MON_TransferCurrency = oCargo.MONCurrency
                            End If
                        End If

                        bReturnValue = oPayment.ReadCamt054(node2, nsMgr, sFileNameIn, sCounterFromFile)

                        '30.10.2019 - Added next IF - overrides paycodes set in Payment and/or Invoice
                        'This is only done (so far) when we get the old paycode from CREMUL - see testabove
                        If Not EmptyString(sBUSString) Then
                            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220125" Then
                                MsgBox("Hva pokker?")
                            End If

                            oPayment.PayCode = sPayCode
                        End If

                    Next node2

                Next node

                If Payments.Count = 1 Then
                    If EmptyString(oPayment.REF_Bank1) Then
                        oPayment.REF_Bank1 = sREF_Bank
                    End If
                End If

                If oCargo.Credit Then
                    AddInvoiceAmountOnBatch()
                    AddTransferredAmountOnBatch()
                    'Check accountamount
                Else
                    AddInvoiceAmountOnBatch()
                    AddTransferredAmountOnBatch()
                    'nMON_InvoiceAmount = oCargo.MON_InvoiceAmount
                    'nMON_TransferredAmount = oCargo.MON_InvoiceAmount
                End If

                '10.03.2015 - DnBNOR INPS for Visma Collectors, sometimes textinfo is stated on this level
                If Payments.Count = 1 Then
                    If oPayments.Item(1).Invoices.Count = 1 Then
                        sTmp = GetInnerText(nodBatch, "ISO:AddtlNtryInf", nsMgr)
                        If Not EmptyString(sTmp) Then
                            With oPayments.Item(1).Invoices.Item(1)
                                Do While Len(sTmp) > 0
                                    oFreeText = .Freetexts.Add()
                                    oFreeText.Qualifier = CShort("1")
                                    If Len(sTmp) > 40 Then
                                        oFreeText.Text = Left(sTmp, 40)
                                        sTmp = Right(sTmp, Len(sTmp) - 40)
                                    Else
                                        oFreeText.Text = Mid(sTmp, 1, Len(sTmp))
                                        sTmp = ""
                                    End If
                                Loop
                            End With
                        End If
                    End If
                End If

            End If

            '27.06.2017 - Moved code from above
            If Payments.Count = 0 Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                If bProfileInUse Then
                    oPayment.VB_Profile = oProfile
                End If
                oPayment.VB_ProfileInUse = bProfileInUse
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                oPayment.PayType = sPayType
                oPayment.PayCode = sPayCode
                If Left(oCargo.I_Account, 2) = "PG" Then
                    oPayment.I_AccountType = BabelFiles.AccountType.SE_PlusGiro
                    oPayment.I_Account = Mid(oCargo.I_Account, 3)
                ElseIf Left(oCargo.I_Account, 2) = "BG" Then
                    oPayment.I_AccountType = BabelFiles.AccountType.SE_Bankgiro
                    oPayment.I_Account = Mid(oCargo.I_Account, 3)
                Else
                    oPayment.I_Account = oCargo.I_Account
                End If
                'Retrieve info about the amount here not at the payment/invoice-level
                sTmp = GetInnerText(nodBatch, "ISO:Amt", nsMgr)
                If Not EmptyString(sTmp) Then
                    nTmp = ConvertToAmount(sTmp, "", ".", , True)
                End If

                oPayment.MON_InvoiceAmount = nTmp
                oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                oPayment.MON_TransferredAmount = nTmp
                oPayment.MON_TransferCurrency = oCargo.MONCurrency
                oPayment.MON_AccountAmount = nTmp
                oPayment.MON_AccountCurrency = oCargo.MONCurrency
                oPayment.MON_OriginallyPaidAmount = nTmp
                oPayment.MON_OriginallyPaidCurrency = oCargo.MONCurrency
                oPayment.REF_Bank1 = sREF_Bank

                '06.11.2019 - Added SGF_EQ
                '01.03.2021 - Added NORDEAFINANCE_APTIC
                If oCargo.Special = "SG FINANS" Or oCargo.Special = "SGF_EQ" Or oCargo.Special = "NORDEAFINANCE_APTIC" Then
                    oPayment.Visma_StatusCode = sTmpPayCode
                End If

                If bProfileInUse Then
                    oPayment.VB_FilenameOut_ID = CheckAccountNo(oCargo.I_Account, "", oProfile)
                End If

                oInvoice = oPayment.Invoices.Add()
                If bProfileInUse Then
                    oInvoice.VB_Profile = oProfile
                End If
                oInvoice.Cargo = oCargo
                oInvoice.ImportFormat = iImportFormat
                oInvoice.StatusCode = sStatusCode
                oInvoice.MON_InvoiceAmount = nTmp
                oInvoice.MON_TransferredAmount = nTmp

                AddInvoiceAmountOnBatch()
                AddTransferredAmountOnBatch()

                If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220125" Then
                    MsgBox("Her skal vi ikke v�re")
                End If


            End If

            If oCargo.Special = "SGF_EQ" And oCargo.Credit And oCargo.Bank = BabelFiles.Bank.DanskeBank_SE Then

                'KAN KANSKJE BRUKES AV ANDRE N�R DET GJELDER INNBETALINGER SVERIGE
                'De bruker Danske Bank (Sverige er satt som land).
                'Umulig � skj�nne hva som er KID-innbetalinger og hva som er andre
                'G� derfor igjennom alle invoices, og sjekk om Unique_ID inneholder bokstaver.
                'Hvis JA, flytt fra Unique_ID til fritekst og gj�r om til paycode = "601"
                Dim bLettersInUniqueID As Boolean = False
                Dim iCounter As Integer
                For Each oPayment In oPayments
                    If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "629" Then
                        For Each oInvoice In oPayment.Invoices
                            If Not vbIsNumeric(oInvoice.Unique_Id, "0123456789- ;") Then
                                bLettersInUniqueID = True
                                Exit For
                            End If
                        Next oInvoice
                        '14.09.2020 - Moved from outside the If IsOCR
                        If oCargo.Special = "SGF_EQ" Or Not RunTime() Then
                            'Added 07.09.2020
                            'Always do the conversion for SGF_EQ to make matching work
                            bLettersInUniqueID = True
                        End If
                    Else
                        bLettersInUniqueID = False 'Usikker p� om dette blir riktig
                    End If
                Next oPayment
                If bLettersInUniqueID Then
                    'MsgBox("Funnet en bunt med fakturainnbetalinger.")
                    For Each oPayment In oPayments
                        For Each oInvoice In oPayment.Invoices
                            If Not EmptyString(oInvoice.Unique_Id) Then
                                ' Delete all freetexts!
                                If oInvoice.Freetexts.Count = 1 Then
                                    For iCounter = oInvoice.Freetexts.Count To 1 Step -1
                                        oInvoice.Freetexts.Remove(iCounter)
                                    Next iCounter
                                End If

                                sTmp = oInvoice.Unique_Id.Trim
                                Do Until EmptyString(sTmp) = True
                                    If sTmp.Length > 40 Then
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = sTmp.Substring(0, 40)
                                        oFreeText.Qualifier = 1
                                        sTmp = sTmp.Substring(40)
                                    Else
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = sTmp & " " '20201007 - Added a blank to retrieve the correct invoiceno for matching
                                        oFreeText.Qualifier = 1
                                        sTmp = ""
                                    End If
                                Loop
                                oInvoice.Unique_Id = ""
                            End If
                        Next oInvoice

                        'If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "629" Then
                        oPayment.PayCode = "601"
                        'End If
                    Next oPayment
                End If
            End If

            If oCargo.Bank = BabelFiles.Bank.Sparebank1 Then
                '06.02.2019 - Added for Confide
                'Check amount against invoices.
                'For internationsl payments
                If Payments.Count = 1 Then
                    If Payments.Item(1).Invoices.Count = 1 Then
                        If Not IsEqualAmount(nMON_TransferredAmount, Payments.Item(1).MON_TransferredAmount) Then
                            'If nMON_InvoiceAmount <> Payments.Item(1).MON_InvoiceAmount Then '<> Payments.Item(1).Invoices.Item(1).MON_InvoiceAmount Then
                            'OK, we have to do something
                            If oCargo.MONCurrency = "NOK" And Payments.Item(1).MON_InvoiceCurrency = "NOK" Then
                                If Payments.Item(1).MON_TransferredAmount - nMON_TransferredAmount < 12000 Then
                                    'we adjust the payment and invoiceamount, set mon_originalAmount equal to the amount on the payment
                                    Payments.Item(1).MON_OriginallyPaidAmount = Payments.Item(1).MON_InvoiceAmount
                                    Payments.Item(1).MON_AccountAmount = nMON_InvoiceAmount
                                    Payments.Item(1).MON_InvoiceAmount = nMON_InvoiceAmount
                                    Payments.Item(1).MON_TransferredAmount = nMON_InvoiceAmount
                                Else
                                    'Do not adjust, an error will be raised later
                                End If
                            Else
                                'Not possible to check anything, just adjust
                                'we adjust the payment and invoiceamount and currency, set mon_originalAmount equal to the amount on the payment
                                Payments.Item(1).MON_OriginallyPaidAmount = Payments.Item(1).MON_InvoiceAmount
                                Payments.Item(1).MON_OriginallyPaidCurrency = Payments.Item(1).MON_InvoiceCurrency
                                Payments.Item(1).MON_AccountAmount = nMON_InvoiceAmount
                                Payments.Item(1).MON_AccountCurrency = oCargo.MONCurrency
                                Payments.Item(1).MON_InvoiceAmount = nMON_InvoiceAmount
                                Payments.Item(1).MON_InvoiceCurrency = oCargo.MONCurrency
                                Payments.Item(1).MON_TransferredAmount = nMON_InvoiceAmount
                                Payments.Item(1).MON_TransferCurrency = oCargo.MONCurrency
                            End If

                            Payments.Item(1).Invoices.Item(1).MON_InvoiceAmount = Payments.Item(1).MON_InvoiceAmount
                            Payments.Item(1).Invoices.Item(1).MON_TransferredAmount = Payments.Item(1).MON_TransferredAmount

                        End If
                    End If
                End If
            End If

            Return bReturnValue

        Catch ex As Exception

            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadCamt054_Batch" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sCounterFromFile, ConvertFromAmountToString(MON_InvoiceAmount, " ", ",")))
            End If

        Finally

            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If
            If Not nodeList2 Is Nothing Then
                nodeList2 = Nothing
            End If
            If Not node2 Is Nothing Then
                node2 = Nothing
            End If

        End Try

    End Function
    Friend Function ReadCamt053(ByRef nodBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager, ByVal sFileNameIn As String) As Boolean
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim nodeList2 As System.Xml.XmlNodeList
        Dim node2 As System.Xml.XmlNode
        Dim node3 As System.Xml.XmlNode
        Dim sCounterFromFile As String
        Dim sTmp As String
        Dim sTmp2 As String
        Dim sTmp3 As String
        Dim sTmpPayCode As String = ""
        Dim sPayType As String
        Dim sPayCode As String
        Dim nTmp As Double
        Dim dBookDate As Date
        Dim dValueDate As Date
        Dim bReturnValue As Boolean = True
        Dim iTempFilenameOut_ID As Integer

        '*Ntry - Occurence 1 .. unbounded - Set of elements used to specify an entry in the debit credit notification.
        '   *NtryRef - Occurence 1 .. 1 - Type Max35Text 
        '       NOT IMPLEMENTED YET
        '       ISO Definition Unique reference for the entry. 
        '       Use: Increased by one for each entry.
        '   *Amt - Occurence 1 .. 1
        '       Set in BabelFile
        '       Amount of money in the cash entry. 
        '       Use: Posted amount to the account can contain one or many entry details.
        '       Ccy - ATTRIBUTE
        '   *CdtDbtInd - Occurence 1 .. 1
        '       Used in BabelFile
        '       If the amount is zero, the indicator is credit.
        '       Allowed Codes: 
        '           CRDT Credit 
        '           DBIT Debit 
        '   *Sts - Occurence 1 .. 1
        '       NOT IMPLEMENTED YET
        '       Allowed Codes: 
        '           BOOK Booked 
        '   *BookgDt - Occurence 1 .. 1 - Date and time when an entry is posted to an account on the account servicer's books
        '       *Dt - Occurence 1 .. 1     
        '   *ValDt - Occurence 1 .. 1 - 
        '       Date and time at which assets become available to the account owner in case of a credit entry, or cease to be available to the account owner in case of a debit entry.
        '       *Dt - Occurence 1 .. 1 
        '   *AcctSvcrRef - Occurence 0 .. 1 - Unique reference as assigned by the account servicing institution to unambiguously identify the entry.
        '       Sweden: Bank reference will only be sent for TotalIN and card transactions. For incoming international payments, except through TotalIN, no reference will be available. 
        '           When no bank reference is present, see Usage. 
        '       Finland: For reference payments (KTL), no reference is present on entry level. For all other transactions (from Tito) bank reference may be present. 
        '       Denmark: For Direct Debit payments the bank reference will contain the debtor group number. 
        '           For direct debit transactions from Leverand�rService no bank reference will be sent. 
        '       Poland: For direct debit transactions from Poland no bank reference is given.
        '   *BkTxCd - Occurence 1 .. 1 - Set of elements used to fully identify the type of underlying transaction resulting in an entry.
        '       *Domn - Occurence 1 .. 1
        '           *Cd - 1 .. 1 - Length 1 .. 4 - ISO Definition: Specifies the business area of the underlying transaction.
        '           *Fmly - Occurence 1 .. 1
        '               *Cd - 1 .. 1 - Length 1 .. 4 - Specifies the family within a domain.
        '               SubFmlyCd - Occurence 1 .. 1 - Length 1 .. 4 - Specifies the sub-product family within a specific family.
        '       *Prtry - Occurence 0 .. 1 - Used by Nordea for payment service description.
        '           *Cd - Occurence 1 .. 1 - Proprietary bank transaction code to identify the underlying transaction
        '           *Issr - Occurence 0 .. 1 - Identification of the issuer of the proprietary bank transaction code.
        '               Allowed Codes: NORDEA 
        '   *Chrgs - Occurence 0 .. 1 
        '       NORDEA: Nordea books all charges in separate entries according to rules in �Payment Services Directive� (PSD). This means, that this element is NOT used by Nordea, as ISO definition only allows reporting of charges included in the entry amount.
        '       *Amt - Occurence 1 .. 1 - Transaction charges to be paid by the charge bearer
        '           *Ccy - ATTRIBUTE - 
        '   *NtryDtls - SEE SEPARATE SPECIFICATIONS

        Try
            sCounterFromFile = GetInnerText(nodBatch, "ISO:NtryRef", nsMgr)
            'GetInnerText(nodBatch, "//ISO:Amt", nsMgr) - Set from BabelFile

            oCargo.MONCurrency = nodBatch.SelectSingleNode("ISO:Amt", nsMgr).Attributes.GetNamedItem("Ccy").Value
            sTmp = GetInnerText(nodBatch, "ISO:Amt", nsMgr)
            nMON_InvoiceAmount = ConvertToAmount(sTmp, "", ".")
            sTmp = GetInnerText(nodBatch, "ISO:CdtDbtInd", nsMgr)
            If sTmp = "CRDT" Then
                'Creditamount no change
                oCargo.Credit = True

            ElseIf sTmp = "DBIT" Then
                ' added If 15.05.2017
                oCargo.Credit = False
                If iImportFormat <> BabelFiles.FileType.Camt053_Outgoing Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
            Else
                Throw New Exception(LRS(11049))
            End If
            nMON_TransferredAmount = nMON_InvoiceAmount
            ' added 28.09.2017 - to be used at paymentlevel if we have no InstdAmt
            oCargo.MON_InvoiceAmount = nMON_InvoiceAmount


            sTmp2 = ""
            sTmp3 = ""
            sTmp = GetInnerText(nodBatch, "ISO:BookgDt/ISO:Dt", nsMgr)
            sTmp2 = sTmp
            sTmp = GetInnerText(nodBatch, "ISO:ValDt/ISO:Dt", nsMgr)
            sTmp3 = sTmp

            If Not EmptyString(sTmp2) Then
                dBookDate = DateSerial(CInt(Left(sTmp2, 4)), CInt(Mid(sTmp2, 6, 2)), CInt(Mid(sTmp2, 9, 2)))
                If Not EmptyString(sTmp3) Then
                    dValueDate = DateSerial(CInt(Left(sTmp3, 4)), CInt(Mid(sTmp3, 6, 2)), CInt(Mid(sTmp3, 9, 2)))
                Else
                    dValueDate = dBookDate
                End If
            Else
                If Not EmptyString(sTmp3) Then
                    dValueDate = DateSerial(CInt(Left(sTmp3, 4)), CInt(Mid(sTmp3, 6, 2)), CInt(Mid(sTmp3, 9, 2)))
                    dBookDate = dValueDate
                Else
                    dValueDate = Date.Today
                    dBookDate = dValueDate
                End If
            End If

            sREF_Bank = GetInnerText(nodBatch, "ISO:AcctSvcrRef", nsMgr)
            node = nodBatch.SelectSingleNode("ISO:BkTxCd/ISO:Domn", nsMgr)
            If Not node Is Nothing Then
                sTmp = GetInnerText(node, "ISO:Cd", nsMgr)
                node = node.SelectSingleNode("ISO:Fmly", nsMgr)
                sTmp2 = GetInnerText(node, "ISO:Cd", nsMgr)
                sTmp3 = GetInnerText(node, "ISO:SubFmlyCd", nsMgr)
            Else
                node = nodBatch.SelectSingleNode("ISO:BkTxCd/ISO:Prtry", nsMgr)
                If Not node Is Nothing Then
                    sTmp = GetInnerText(node, "ISO:Cd", nsMgr)
                Else
                    Throw New Exception(LRS(11043))
                End If
            End If
            '07.02.2013 - Just implemented what's necessary for Gjensidige Norway
            sPayCode = bbSetPayCode(sTmp, sPayType, "ISO20022", oCargo.Credit, sTmp2, sTmp3, , "Camt.053", oCargo.Bank)
            sTmpPayCode = sTmp & "-" & sTmp2 & "-" & sTmp3
            If EmptyString(sPayCode) Then
                If oCargo.Credit Then
                    sPayCode = "601"
                Else
                    sPayCode = "150"
                End If
            End If
            'PayType = ""
            node = nodBatch.SelectSingleNode("ISO:Chrgs", nsMgr)
            If Not node Is Nothing Then
                sTmp = GetInnerText(node, "ISO:Amt", nsMgr)
                nTmp = ConvertToAmount(sTmp, "", ".")
            End If

            nodeList = nodBatch.SelectNodes("ISO:NtryDtls", nsMgr)

            'Not implemented this level yet, because it is not interesting for Gjensidige

            'So far just fill in the payment/invocie with info

            'oPayment = oPayments.Add()
            'oPayment.objFile = oFile
            'oPayment.VB_Profile = oProfile
            'oPayment.Cargo = oCargo
            'oPayment.ImportFormat = iImportFormat
            'oPayment.StatusCode = sStatusCode
            'oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
            'oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
            'oPayment.PayType = sPayType
            'oPayment.PayCode = sPayCode
            'oPayment.MON_InvoiceAmount = nMON_InvoiceAmount
            'oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
            'oPayment.MON_TransferredAmount = nMON_TransferredAmount
            'oPayment.MON_TransferCurrency = oCargo.MONCurrency
            'oPayment.MON_AccountAmount = nMON_InvoiceAmount
            'oPayment.MON_AccountCurrency = oCargo.MONCurrency
            'oPayment.I_Account = oCargo.I_Account

            ''Check if it is a new and known accountno.
            'If bProfileInUse Then
            '    oPayment.VB_FilenameOut_ID = CheckAccountNo(oPayment.I_Account, sREF_Own, oProfile)
            'End If

            'oInvoice = oPayment.Invoices.Add()
            'oInvoice.objFile = oFile
            'oInvoice.VB_Profile = oProfile
            'oInvoice.Cargo = oCargo
            'oInvoice.ImportFormat = iImportFormat
            'oInvoice.StatusCode = sStatusCode
            'oInvoice.MON_InvoiceAmount = nMON_InvoiceAmount
            'oInvoice.MON_TransferredAmount = nMON_TransferredAmount


            For Each node In nodeList

                nodeList2 = node.SelectNodes("ISO:TxDtls", nsMgr) 'Not sure what this level is but according to documentation we may have many TxDtls within a NtryDtls

                If nodeList2.Count = 0 Then
                    '24.05.2017 - Added this IF, because for danske Bank/SG Finans we have examples of entries without specifications
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.VB_ProfileInUse = bProfileInUse
                    oPayment.Cargo = oCargo
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    ' oPayment.DATE_Payment =VB6.Format(dDATE_Production, "YYYYMMDD") 'dDATE_Production
                    If oCargo.Special = "VINGCARD_UK" And oCargo.Bank = BabelFiles.Bank.CommerzBank_PT Then
                        'If Added 07.03.2022 after training
                        oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                        oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                        If oPayment.DATE_Payment <> oPayment.DATE_Value Then
                            sPayCode = sPayCode
                        End If
                    Else
                        oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                        oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                    End If
                    oPayment.PayType = sPayType
                    oPayment.PayCode = sPayCode
                    oPayment.I_Account = oCargo.I_Account

                    oPayment.MON_InvoiceAmount = nMON_InvoiceAmount
                    oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                    oPayment.MON_AccountAmount = nMON_InvoiceAmount
                    oPayment.MON_AccountCurrency = oCargo.MONCurrency
                    oPayment.MON_TransferredAmount = nMON_TransferredAmount
                    oPayment.MON_TransferCurrency = oCargo.MONCurrency
                    ' 30.04.2020
                    oPayment.MON_OriginallyPaidCurrency = oCargo.MONCurrency

                    If oCargo.Special = "SGFINANS_AML" Then
                        oPayment.Visma_StatusCode = sTmpPayCode
                    End If

                    oInvoice = oPayment.Invoices.Add()
                    oInvoice.MON_InvoiceAmount = nMON_InvoiceAmount
                    oInvoice.MON_TransferredAmount = nMON_TransferredAmount
                    oInvoice.MON_AccountAmount = nMON_InvoiceAmount

                    ' 30.04.2020 - for ABN Amro, we have a AddtlNtryInf node with freetext.
                    ' a bit strange to fetch it here, but this node is not transferred down to oPayment.
                    If oCargo.Bank = BabelFiles.Bank.ABN_Amro_NL Then
                        nodeList = nodBatch.SelectNodes("ISO:AddtlNtryInf", nsMgr)
                        For Each node3 In nodeList
                            sTmp = node3.InnerText
                            ' add text to first invoice - is this correct? can we have more invoices?
                            oInvoice = oPayment.Invoices(1)
                            Do While sTmp.Length > 0
                                oFreeText = oInvoice.Freetexts.Add
                                oFreeText.Text = Left(sTmp, 40)
                                sTmp = Mid(sTmp, 40)
                            Loop

                        Next
                    ElseIf oCargo.Bank = BabelFiles.Bank.CommerzBank_PT Then
                        '25.07.2021 - Added this to retrieve payers name
                        If EmptyString(oPayment.E_Name) Then
                            nodeList = nodBatch.SelectNodes("ISO:AddtlNtryInf", nsMgr)
                            sTmp = ""
                            For Each node3 In nodeList
                                sTmp = sTmp & node3.InnerText
                            Next
                            If Not EmptyString(sTmp) Then
                                oPayment.E_Name = sTmp
                            End If
                        End If
                    End If

                    If bProfileInUse Then
                        iTempFilenameOut_ID = CheckAccountNo(oCargo.I_Account, sREF_Own, oProfile)
                        oPayment.VB_FilenameOut_ID = iTempFilenameOut_ID
                    End If

                Else
                    For Each node2 In nodeList2
                        'nodeTemp = node.SelectSingleNode("//ISO:Acct/ISO:Id", nsMgr)
                        oPayment = oPayments.Add()
                        oPayment.objFile = oFile
                        oPayment.VB_Profile = oProfile
                        oPayment.VB_ProfileInUse = bProfileInUse
                        oPayment.Cargo = oCargo
                        oPayment.ImportFormat = iImportFormat
                        oPayment.StatusCode = sStatusCode
                        ' oPayment.DATE_Payment =VB6.Format(dDATE_Production, "YYYYMMDD") 'dDATE_Production
                        If oCargo.Special = "VINGCARD_UK" And oCargo.Bank = BabelFiles.Bank.CommerzBank_PT Then
                            'If Added 07.03.2022 after training
                            oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                            oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                            If oPayment.DATE_Payment <> oPayment.DATE_Value Then
                                sPayCode = sPayCode
                            End If
                        Else
                            oPayment.DATE_Value = VB6.Format(dValueDate, "YYYYMMDD") 'dValueDate
                            oPayment.DATE_Payment = VB6.Format(dBookDate, "YYYYMMDD") 'dPayment_Date
                        End If
                        oPayment.PayType = sPayType
                        oPayment.PayCode = sPayCode
                        oPayment.I_Account = oCargo.I_Account

                        bReturnValue = oPayment.ReadCamt053(node2, nsMgr, sFileNameIn, sCounterFromFile)

                        If Not oPayment.AmountSetInvoice Then
                            oPayment.MON_InvoiceAmount = nMON_InvoiceAmount
                            oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                            oPayment.MON_AccountAmount = nMON_InvoiceAmount
                            oPayment.MON_AccountCurrency = oCargo.MONCurrency
                            If oPayment.Invoices.Count = 1 Then
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_InvoiceAmount = nMON_InvoiceAmount
                                    oInvoice.MON_AccountAmount = nMON_InvoiceAmount
                                Next oInvoice
                            End If
                        End If
                        If Not oPayment.AmountSetTransferred Then
                            oPayment.MON_TransferredAmount = nMON_TransferredAmount
                            oPayment.MON_TransferCurrency = oCargo.MONCurrency
                            If oPayment.Invoices.Count = 1 Then
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_TransferredAmount = nMON_TransferredAmount
                                Next oInvoice
                            End If
                        End If

                        ' 16.11.2017 - for HSBC Singapore, we have a AddtlNtryInf node with freetext.
                        ' a bit strange to fetch it here, but this node is not transferred down to oPayment.
                        If oCargo.Bank = BabelFiles.Bank.HSBC_SG Then
                            nodeList = nodBatch.SelectNodes("ISO:AddtlNtryInf", nsMgr)
                            For Each node3 In nodeList
                                sTmp = node3.InnerText
                                ' add text to first invoice - is this correct? can we have more invoices?
                                oInvoice = oPayment.Invoices(1)
                                Do While sTmp.Length > 0
                                    oFreeText = oInvoice.Freetexts.Add
                                    oFreeText.Text = Left(sTmp, 40)
                                    sTmp = Mid(sTmp, 40)
                                Loop

                            Next
                        ElseIf oCargo.Bank = BabelFiles.Bank.CommerzBank_PT Then
                            '25.07.2021 - Added this to retrieve payers name
                            If EmptyString(oPayment.E_Name) Then
                                nodeList = nodBatch.SelectNodes("ISO:AddtlNtryInf", nsMgr)
                                sTmp = ""
                                For Each node3 In nodeList
                                    sTmp = sTmp & node3.InnerText
                                Next
                                If Not EmptyString(sTmp) Then
                                    oPayment.E_Name = sTmp
                                End If
                            End If
                        End If
                    Next node2
                End If

            Next node

            Return bReturnValue

        Catch ex As Exception

            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadCamt054_Batch" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sCounterFromFile, ConvertFromAmountToString(MON_InvoiceAmount, " ", ",")))
            End If

        Finally

            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If
            If Not nodeList2 Is Nothing Then
                nodeList2 = Nothing
            End If
            If Not node2 Is Nothing Then
                node2 = Nothing
            End If

        End Try

    End Function
    Friend Function ReadKAR_kontovask_Nets_Batch(ByRef nodeList As System.Xml.XmlNodeList, ByRef sFileNameIn As String, ByRef eTypeOfFile As vbBabel.BabelFiles.FileType) As Boolean
        Dim node As System.Xml.XmlNode
        Dim oPayment As vbBabel.Payment
        Dim bReturnValue As Boolean = True

        Try

            For Each node In nodeList

                oPayment = Payments.Add()
                oPayment.ImportFormat = CShort(eTypeOfFile) ' CShort(BabelFiles.FileType.KAR_kontovask_Nets_AccountOwner)
                oPayment.Cargo = oCargo
                If bProfileInUse Then
                    oPayment.VB_Profile = oProfile
                    oPayment.VB_FilenameOut_ID = CheckAccountNo("99999999999", "", oProfile)
                End If
                oPayment.VB_ProfileInUse = bProfileInUse
                bReturnValue = oPayment.ReadKAR_kontovask_Nets_Payment(node, sFileNameIn)

            Next node

            Return bReturnValue

        Catch ex As Exception

            If Not node Is Nothing Then
                node = Nothing
            End If

            Throw New Exception("Function: WriteISO_20022NordeaeGateway" & vbCrLf & ex.Message)

        Finally

            If Not node Is Nothing Then
                node = Nothing
            End If

        End Try

    End Function
    'Removed 11.09.2019
    'Private Function ReadDNV_GermanyFile(ByRef oUNH As MSXML2.IXMLDOMNode, ByRef oEDIMapping As MSXML2.IXMLDOMElement) As Boolean
    '    Dim oPayment As Payment
    '    'Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
    '    'Variables used to set properties in the payment-object
    '    'Dim sI_Account As String
    '    'Dim oSEQList As MSXML2.IXMLDOMNodeList
    '    'Dim l As Long,
    '    Dim bReturnValue As Boolean
    '    Dim dValueDate, dPayment_Date As Date
    '    Dim sPayType, sPayCode As String
    '    Dim sI_Account As String
    '    Dim nCharges As Double
    '    Dim sCharges_Currency, sCharges As String
    '    Dim bCharges_Domestic, bCharges_Abroad As Boolean
    '    Dim sInvCurrency, sTempDate, sTraCurrency As String
    '    Dim nodActive As MSXML2.IXMLDOMNode
    '    Dim lCounter As Integer
    '    Dim bx As Boolean
    '    Dim nERAExchRateAgreed As Double
    '    Dim sERADealMadeWith As String
    '    Dim nFRWForwardContractRate As Double
    '    Dim sFRWForwardContractNo As String
    '    Dim nSEQAmount As Double
    '    Dim sSEQName, sSEQAccount As String
    '    Dim sSEQAdr1, sSeqDate As String
    '    Dim bJustAddMoreFreetext As Boolean

    '    bReturnValue = False

    '    'UPGRADE_WARNING: Couldn't resolve default property of object oUNH. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    sI_EnterpriseNo = GetPropValue("batch/i_enterpriseno", oUNH, oEDIMapping)

    '    'nMON_InvoiceAmount = Val(GetPropValue("batch/mon_invoiceamount", oUNH, oEDIMapping))
    '    bAmountSetTransferred = True
    '    'UPGRADE_WARNING: Couldn't resolve default property of object oUNH. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    sREF_Own = GetPropValue("batch/ref_own", oUNH, oEDIMapping)

    '    lCounter = 0


    '    oPayment = oPayments.Add()
    '    oPayment.objFile = oFile
    '    oPayment.VB_Profile = oProfile
    '    oPayment.Cargo = oCargo
    '    oPayment.ImportFormat = iImportFormat
    '    oPayment.StatusCode = sStatusCode
    '    bReturnValue = oPayment.ImportEDI(oUNH, oEDIMapping, False)

    '    bx = AddInvoiceAmountOnBatch()


    'End Function
    Private Function ReadTellerXML(ByRef nodMerchant As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodPos_DetailList As MSXML2.IXMLDOMNodeList
        Dim nodMerchant_DetailList As MSXML2.IXMLDOMNodeList
        Dim nodPos_Detail As MSXML2.IXMLDOMNode
        Dim nodMerchant_Detail As MSXML2.IXMLDOMNode
        Dim sTempDate As String
        Dim sIName, sIAddress As String
        Dim lNodeCounter As Integer

        bReturnValue = False

        '************************************************************************
        '    comment_detail      Innpakking av kommentar informasjon  NOT IN USE YET!!!!!!!!
        ' Same level as Pos_Detail
        'X signifies not implemented
        'X�   pos_no  �   Brukerstedsnummer       x(7)
        'X�   delivery_date   �   Mottaksdato (nota_dato)     date(8)
        'X�   your_ref    �   Referanse       x(15)
        'X�   purchase_date   �   Kj�psdato       date(8)
        'X�   card_no �   Kortnummer (strippet)       x(12)
        'X�   brand   �
        'X�   amount  �   Bel�p       m(14)
        'X�   currency_amount �   Bel�p i valuta      m(14)
        'X�   currency    �   Valuta kode (f.eks 'NOK')       x(3)
        'X�   text    �   Kommentar       x(255)

        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sIName = nodMerchant.selectSingleNode("child::*[name()='name']").nodeTypedValue
        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sIAddress = nodMerchant.selectSingleNode("child::*[name()='merchant_no']").nodeTypedValue 'Brukerstednummer

        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sTempDate = nodMerchant.selectSingleNode("child::*[name()='letter_date']").nodeTypedValue '�   Utsendelsesdato for e-post
        dDATE_Production = DateSerial(CInt(Left(sTempDate, 4)), CInt(Mid(sTempDate, 6, 2)), CInt(Mid(sTempDate, 9, 2))) '
        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sI_EnterpriseNo = nodMerchant.selectSingleNode("child::*[name()='customer_no']").nodeTypedValue ' �   Kundenummer

        nodPos_DetailList = nodMerchant.selectNodes("child::*[name()='pos_detail'][brand='Total Bunt'] ")

        'Merchant_detail = Brukerstedreskontro
        nodMerchant_DetailList = nodMerchant.selectNodes("child::*[name()='merchant_detail']")

        nNo_Of_Transactions = 0

        For lNodeCounter = 0 To nodPos_DetailList.length - 1
            'Pick an element from the lists
            nodPos_Detail = nodPos_DetailList.Item(lNodeCounter)
            'Set nodMerchant_Detail = nodMerchant_DetailList.Item(lNodeCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            oPayment.I_Name = sIName
            oPayment.I_Adr1 = sIAddress & " - " & nodPos_Detail.selectSingleNode("child::*[name()='pos_no']").nodeTypedValue ' Brukerstedsnummer

            nNo_Of_Transactions = nNo_Of_Transactions + Val(nodPos_Detail.selectSingleNode("child::*[name()='no_transactions']").nodeTypedValue) 'Antall transaksjoner i notasammendraget

            'Continue to the payment-object
            bReturnValue = oPayment.ImportTellerXML(nodMerchant, nodPos_Detail)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodeCounter

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadTellerXML = bReturnValue

    End Function
    Private Function ReadTellerXMLNew(ByRef nodMerchant As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodBatchList As MSXML2.IXMLDOMNodeList
        Dim nodMerchant_DetailList As MSXML2.IXMLDOMNodeList
        Dim nodBatch As MSXML2.IXMLDOMNode
        Dim nodMerchant_Detail As MSXML2.IXMLDOMNode
        Dim sTemp As String
        Dim sTempDate As String
        Dim sIName, sIAddress As String
        Dim lNodeCounter As Integer

        bReturnValue = False

        '************************************************************************
        'Batches                 Innpakking av brandinndeling av bunt
        '************************************************************************
        'ReceivalNo                 Buntforsendelsesnummer                          int
        'BatchNo                    Buntnummer                                      int
        'SettlementMerchantNo       Brukerstedsnummer for overliggende oppgj�rssted int
        'MerchantNo                 Brukerstedsnummer                               int
        'SubmissionNo               Forsendelsesnummer oppgj�r                      int
        'SettledCode                Oppgjortkode                                    string(256)
        'PointOfSale                Salgssted                                       string(25)
        'Brands                     Hvilke brands som bunten kan inneholde          string(20)
        'BatchDate                  Buntdato                                        Date(yyyy-mm-dd)
        'BatchRef                   Buntreferanse                                   string(256)
        'PurchaseDateFrom           Kj�psdato fra (i bunt)                          Date(yyyy-mm-dd)
        'PurchaseDateTo             Kj�psdato til (i bunt)                          Date(yyyy-mm-dd)
        'NOKGrossAmount             Brutto bel�p oppgj�r i NOK                      decimal(11,2)
        'NOKNetAmount               Nettobel�p oppgj�r i NOK                        decimal(11,2)
        'SettlementCurrency         Oppgj�rsvaluta                                  string(3)
        'GrossAmount                Bruttobel�p                                     decimal(11,2)
        'NetAmount                  Nettobel�p                                      decimal(11,2)
        'ServiceCharge              Serviceavgift                                   decimal(11,2)
        'TransactionPrice           Transaksjonspris                                decimal(11,2)
        'FixedPriceDebCard          Kostnad p� fastpris debetkort                   decimal(11,2)
        'RollingReserve             Rolling reservce                                decimal(11,2)
        'ServiceProviderCommission  Agentprovisjon                                  decimal(11,2)
        'LoyaltyDiscount            Lojalitetsrabatt                                decimal(11,2)
        'NoTransactions             Antall transaksjoner i bunt                     int
        'NoDebTransactions          Antall detbet transaksjoner i bunt              int
        'NoCreTransactions          Antall kredit transaksjoner i bunt              int
        'TellerRef                  Teller referanse for bunt                       string(256)
        'ConceptName                Evt konseptnavn                                 string(256)
        '************************************************************************

        sIName = nodMerchant.selectSingleNode("child::*[name()='Name']").nodeTypedValue
        'nodMerchant.getAttribute ("MerchantNo")
        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.Attributes.getNamedItem().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sIAddress = nodMerchant.Attributes.getNamedItem("MerchantNo").nodeTypedValue 'Brukerstednummer

        'sTempDate = nodMerchant.selectSingleNode("child::*[name()='letter_date']").nodeTypedValue '�   Utsendelsesdato for e-post
        'dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2)) '
        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sI_EnterpriseNo = nodMerchant.selectSingleNode("child::*[name()='OrganizationNo']").nodeTypedValue ' �   Kundenummer
        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sREF_Bank = nodMerchant.selectSingleNode("child::*[name()='Settlements']/@SubmissionNo").nodeTypedValue ' SubmissionNo - Forsendelsesnummer

        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sTemp = nodMerchant.selectSingleNode("child::*/child::*[name()='GrossAmount']").nodeTypedValue ' �   Gross amount
        nMON_InvoiceAmount = CDbl(ConvertToAmount(sTemp, "", "."))
        sTemp = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sTemp = nodMerchant.selectSingleNode("child::*/child::*[name()='NetAmount']").nodeTypedValue ' �   Net amount
        nMON_TransferredAmount = CDbl(ConvertToAmount(sTemp, "", "."))
        nNo_Of_Transactions = Val(nodMerchant.selectSingleNode("child::*/child::*[name()='NoTransactions']").nodeTypedValue) '

        nodBatchList = nodMerchant.selectNodes("child::*/child::*[name()='Batches']")
        'Set nodPos_DetailList = nodMerchant.selectNodes("child::*[name()='pos_detail'][brand='Total Bunt'] ")

        'Merchant_detail = Brukerstedreskontro
        'Set nodMerchant_DetailList = nodMerchant.selectNodes("child::*/child::*[name()='merchant_detail']")
        '
        'Set nodMerchant_DetailList = nodMerchant.selectNodes("child::*[name()='merchant_detail']")

        For lNodeCounter = 0 To nodBatchList.length - 1
            'Pick an element from the lists
            nodBatch = nodBatchList.Item(lNodeCounter)
            'Set nodMerchant_Detail = nodMerchant_DetailList.Item(lNodeCounter)

            'added 04.06.2014
            oCargo.I_Name = nodBatch.selectSingleNode("child::*[name()='PointOfSale']").nodeTypedValue

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            ' changed 04.06.2014 oPayment.I_Name = sIName
            'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oPayment.I_Adr1 = nodMerchant.selectSingleNode("child::*[name()='Address']").nodeTypedValue 'Address
            'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oPayment.I_Zip = nodMerchant.selectSingleNode("child::*[name()='ZipCode']").nodeTypedValue 'Zip
            'UPGRADE_WARNING: Couldn't resolve default property of object nodMerchant.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oPayment.I_City = nodMerchant.selectSingleNode("child::*[name()='PostalAddress']").nodeTypedValue 'City

            'Continue to the payment-object
            bReturnValue = oPayment.ImportTellerXMLNew(nodBatch)
            If bReturnValue = False Then
                Exit For
            End If


            'Gammel kode
            'M� n� hente dato fra payment i ettertid
            If Payments.Count > 0 Then
                dDATE_Production = StringToDate((oPayment.DATE_Payment))
            End If


        Next lNodeCounter

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadTellerXMLNew = bReturnValue

    End Function
    Private Function ReadTellerCardXML(ByVal nodCardAcceptor As System.Xml.XmlNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodBatchList As System.Xml.XmlNodeList
        Dim nodCardAcceptor_DetailList As System.Xml.XmlNodeList
        Dim nodPaymentsInfo As System.Xml.XmlNode
        Dim nodBatch As System.Xml.XmlNode
        Dim nodCardAcceptor_Detail As System.Xml.XmlNode
        Dim sTemp As String
        Dim sTempDate As String
        Dim sIName As String, sIAddress As String
        Dim lNodeCounter As Long
        Dim nodTemp As MSXML2.IXMLDOMNode
        Dim nTempCharges As Double = 0

        bReturnValue = False

        sIName = GetInnerText(nodCardAcceptor, "child::*[name()='Name1']", Nothing)
        sIAddress = nodCardAcceptor.Attributes.GetNamedItem("SettlementCardAcceptorNum").Value ' �   
        '25.10.2017 sIAddress er jo kun lokal, dvs at Brukerstedsnummer ikke ble lagret noe sted, f�r i linjen under:
        sI_Branch = sIAddress

        sI_EnterpriseNo = nodCardAcceptor.Attributes.GetNamedItem("AcquirerId").Value ' �   Kundenummer

        ' Select node PaymentsInfo
        'Set nodPaymentsInfo = nodCardAcceptor.selectNodes("child::*/child::*[name()='PaymentsInfo']")
        nodPaymentsInfo = nodCardAcceptor.SelectNodes("child::*[name()='PaymentsInfo']").Item(0)  ' Is only one

        'sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='PaymentReferenceNum']").nodeTypedValue 
        ' "PaymentReferenceNum" finnes ikke i alle filer (Ikke for Teller DK for JapanPhoto)
        '28.11.2017 - Changed from "PaymentReferenceNum" to "child::*[name()='PaymentReferenceNum']"

        ' 13.12,2017 - find probable bankdate from node below:
        sTempDate = GetInnerText(nodPaymentsInfo, "child::*[name()='ActualSettlementDate']", Nothing)
        If Not EmptyString(sTempDate) Then
            dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2))
        End If

        sREF_Bank = GetInnerText(nodPaymentsInfo, "child::*[name()='PaymentReferenceNum']", Nothing)
        ' 16.11.2017 - import BatchInfo, if not "PaymentReferenceNum" is in use
        If EmptyString(sREF_Bank) Then
            '28.11.2017 - Changed from:
            'sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='BatchRefNum']").nodeTypedValue
            'Created an error if BatchRefNum doesn't exists, f.ex. for SISMO
            sREF_Bank = GetInnerText(nodPaymentsInfo, "child::*[name()='BatchRefNum']", Nothing)
        End If

        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='GrossTransactionAmount']", Nothing) ' �   Gross amount
        nMON_InvoiceAmount = ConvertToAmount(sTemp, vbNullString, ".")
        sTemp = vbNullString
        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='ExpectedNetSettlementAmount']", Nothing) ' �   Net amount
        nMON_TransferredAmount = ConvertToAmount(sTemp, vbNullString, ".")

        ' 01.12.2017 - in some files we have ServiceChargeAmount and/or OtherFeesAmount
        ' Save this info, and put it into the first payment (???)
        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='ServiceChargeAmount']", Nothing)
        If Not EmptyString(sTemp) Then
            nTempCharges = System.Xml.XmlConvert.ToDouble(sTemp) * 100
        End If
        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='OtherFeesAmount']", Nothing)
        If Not EmptyString(sTemp) Then
            nTempCharges = nTempCharges + (System.Xml.XmlConvert.ToDouble(sTemp) * 100)
        End If

        'nNo_Of_Transactions = Val(nodPaymentsInfo.selectSingleNode("child::*[name()='NumOfTransactions']").nodeTypedValue) '
        nodBatchList = nodPaymentsInfo.SelectNodes("child::*[name()='BatchInfo']")
        'Set nodPos_DetailList = nodCardAcceptor.selectNodes("child::*[name()='pos_detail'][brand='Total Bunt'] ")

        For Each nodBatch In nodBatchList
            'Set nodCardAcceptor_Detail = nodCardAcceptor_DetailList.Item(lNodeCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            oPayment.I_Name = sIName
            oPayment.I_Adr1 = GetInnerText(nodCardAcceptor, "child::*[name()='MainAddress']", Nothing) 'Address
            oPayment.I_Zip = GetInnerText(nodCardAcceptor, "child::*[name()='MainAddressZipCode']", Nothing) 'Zip
            oPayment.I_City = GetInnerText(nodCardAcceptor, "child::*[name()='MainAddressCity']", Nothing) 'City

            ' New in 2015
            oPayment.I_Account = GetInnerText(nodPaymentsInfo, "child::*[name()='SettlementAccountNum']", Nothing)   ' Mottakerkonto
            ' For Butikkdrift, we have 4 clients, using same accountnumber;
            If oCargo.Special = "BUTIKKDRIFT_TELLER" Then
                oPayment.I_Account = sI_Branch
            End If

            If lNodeCounter = 0 Then
                '01.12.2017 - put charges into first payment
                If nTempCharges <> 0 Then
                    oPayment.MON_ChargesAmount = nTempCharges * -1
                End If
                nTempCharges = 0
            End If

            'Continue to the payment-object
            bReturnValue = oPayment.ImportTellerCardXML(nodBatch)
            If bReturnValue = False Then
                Exit For
            End If


            'Gammel kode
            'M� n� hente dato fra payment i ettertid
            '' 13.12.2017 - fjernet koden nedenfor - se dato lenger opp!
            'If Payments.Count > 0 Then
            '    dDATE_Production = StringToDate(oPayment.DATE_Payment)
            'End If


        Next

        ' 13.11.2017 - IKKE beregn bel�p basert p� paymentbel�p - vi trenger forskjellen p� Mon_InvoiceAmount og MON_TransferredAmount
        'AddInvoiceAmountOnBatch()
        'AddTransferredAmountOnBatch()

        ReadTellerCardXML = bReturnValue

    End Function

    Private Function ReadHelseFonna_XML_Nets(ByVal nodCardAcceptor As System.Xml.XmlNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodBatchList As System.Xml.XmlNodeList
        Dim nodCardAcceptor_DetailList As System.Xml.XmlNodeList
        Dim nodPaymentsInfo As System.Xml.XmlNode
        Dim nodBatch As System.Xml.XmlNode
        Dim nodCardAcceptor_Detail As System.Xml.XmlNode
        Dim sTemp As String
        Dim sTempDate As String
        Dim sIName As String, sIAddress As String
        Dim lNodeCounter As Long
        Dim nodTemp As MSXML2.IXMLDOMNode
        Dim nTempCharges As Double = 0

        bReturnValue = False

        sIName = GetInnerText(nodCardAcceptor, "child::*[name()='Name1']", Nothing)
        sIAddress = nodCardAcceptor.Attributes.GetNamedItem("SettlementCardAcceptorNum").Value ' �   
        '25.10.2017 sIAddress er jo kun lokal, dvs at Brukerstedsnummer ikke ble lagret noe sted, f�r i linjen under:
        sI_Branch = sIAddress

        sI_EnterpriseNo = nodCardAcceptor.Attributes.GetNamedItem("AcquirerId").Value ' �   Kundenummer

        ' Select node PaymentsInfo
        'Set nodPaymentsInfo = nodCardAcceptor.selectNodes("child::*/child::*[name()='PaymentsInfo']")
        nodPaymentsInfo = nodCardAcceptor.SelectNodes("child::*[name()='PaymentsInfo']").Item(0)  ' Is only one

        'sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='PaymentReferenceNum']").nodeTypedValue 
        ' "PaymentReferenceNum" finnes ikke i alle filer (Ikke for Teller DK for JapanPhoto)
        '28.11.2017 - Changed from "PaymentReferenceNum" to "child::*[name()='PaymentReferenceNum']"

        ' 13.12,2017 - find probable bankdate from node below:
        sTempDate = GetInnerText(nodPaymentsInfo, "child::*[name()='ActualSettlementDate']", Nothing)
        If Not EmptyString(sTempDate) Then
            dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2))
        End If

        sREF_Bank = GetInnerText(nodPaymentsInfo, "child::*[name()='PaymentReferenceNum']", Nothing)
        ' 16.11.2017 - import BatchInfo, if not "PaymentReferenceNum" is in use
        If EmptyString(sREF_Bank) Then
            '28.11.2017 - Changed from:
            'sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='BatchRefNum']").nodeTypedValue
            'Created an error if BatchRefNum doesn't exists, f.ex. for SISMO
            sREF_Bank = GetInnerText(nodPaymentsInfo, "child::*[name()='BatchRefNum']", Nothing)
        End If

        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='GrossTransactionAmount']", Nothing) ' �   Gross amount
        nMON_InvoiceAmount = ConvertToAmount(sTemp, vbNullString, ".")
        sTemp = vbNullString
        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='ExpectedNetSettlementAmount']", Nothing) ' �   Net amount
        nMON_TransferredAmount = ConvertToAmount(sTemp, vbNullString, ".")

        ' 01.12.2017 - in some files we have ServiceChargeAmount and/or OtherFeesAmount
        ' Save this info, and put it into the first payment (???)
        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='ServiceChargeAmount']", Nothing)
        If Not EmptyString(sTemp) Then
            nTempCharges = System.Xml.XmlConvert.ToDouble(sTemp) * 100
        End If
        sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='OtherFeesAmount']", Nothing)
        If Not EmptyString(sTemp) Then
            nTempCharges = nTempCharges + (System.Xml.XmlConvert.ToDouble(sTemp) * 100)
        End If

        'nNo_Of_Transactions = Val(nodPaymentsInfo.selectSingleNode("child::*[name()='NumOfTransactions']").nodeTypedValue) '
        nodBatchList = nodPaymentsInfo.SelectNodes("child::*[name()='BatchInfo']")
        'Set nodPos_DetailList = nodCardAcceptor.selectNodes("child::*[name()='pos_detail'][brand='Total Bunt'] ")

        For Each nodBatch In nodBatchList
            'Set nodCardAcceptor_Detail = nodCardAcceptor_DetailList.Item(lNodeCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            oPayment.I_Name = sIName
            oPayment.I_Adr1 = GetInnerText(nodCardAcceptor, "child::*[name()='MainAddress']", Nothing) 'Address
            oPayment.I_Zip = GetInnerText(nodCardAcceptor, "child::*[name()='MainAddressZipCode']", Nothing) 'Zip
            oPayment.I_City = GetInnerText(nodCardAcceptor, "child::*[name()='MainAddressCity']", Nothing) 'City

            ' New in 2015
            oPayment.I_Account = GetInnerText(nodPaymentsInfo, "child::*[name()='SettlementAccountNum']", Nothing)   ' Mottakerkonto
            ' For Butikkdrift, we have 4 clients, using same accountnumber;
            If oCargo.Special = "BUTIKKDRIFT_TELLER" Then
                oPayment.I_Account = sI_Branch
            End If

            If lNodeCounter = 0 Then
                '01.12.2017 - put charges into first payment
                If nTempCharges <> 0 Then
                    oPayment.MON_ChargesAmount = nTempCharges * -1
                End If
                nTempCharges = 0
            End If

            'Continue to the payment-object
            bReturnValue = oPayment.ImportHelseFonna_XML_Nets(nodBatch)
            If bReturnValue = False Then
                Exit For
            End If


            'Gammel kode
            'M� n� hente dato fra payment i ettertid
            '' 13.12.2017 - fjernet koden nedenfor - se dato lenger opp!
            'If Payments.Count > 0 Then
            '    dDATE_Production = StringToDate(oPayment.DATE_Payment)
            'End If


        Next

        ' 13.11.2017 - IKKE beregn bel�p basert p� paymentbel�p - vi trenger forskjellen p� Mon_InvoiceAmount og MON_TransferredAmount
        'AddInvoiceAmountOnBatch()
        'AddTransferredAmountOnBatch()

        ReadHelseFonna_XML_Nets = bReturnValue

    End Function
    ' XokNET 21.08.2015 Added function
    Private Function ReadTellerXMLNew_2015(ByVal nodCardAcceptor As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodBatchList As MSXML2.IXMLDOMNodeList
        Dim nodCardAcceptor_DetailList As MSXML2.IXMLDOMNodeList
        Dim nodPaymentsInfo As MSXML2.IXMLDOMNode
        Dim nodBatch As MSXML2.IXMLDOMNode
        Dim nodCardAcceptor_Detail As MSXML2.IXMLDOMNode
        Dim nodTransactionList As MSXML2.IXMLDOMNodeList
        Dim nodTransaction As MSXML2.IXMLDOMNode
        Dim sTemp As String
        Dim sTempDate As String
        Dim sIName As String, sIAddress As String
        Dim lNodeCounter As Long
        Dim lNodeCounter2 As Long
        Dim nodTemp As MSXML2.IXMLDOMNode
        Dim nTempCharges As Double = 0

        On Error GoTo ERR_ReadTellerXMLNew

        bReturnValue = False

        sIName = nodCardAcceptor.selectSingleNode("child::*[name()='Name1']").nodeTypedValue
        sIAddress = nodCardAcceptor.attributes.getNamedItem("SettlementCardAcceptorNum").nodeTypedValue 'Brukerstednummer
        '25.10.2017 sIAddress er jo kun lokal, dvs at Brukerstedsnummer ikke ble lagret noe sted, f�r i linjen under:
        sI_Branch = sIAddress

        sI_EnterpriseNo = nodCardAcceptor.attributes.getNamedItem("AcquirerId").nodeTypedValue ' �   Kundenummer

        ' Select node PaymentsInfo
        'Set nodPaymentsInfo = nodCardAcceptor.selectNodes("child::*/child::*[name()='PaymentsInfo']")
        nodPaymentsInfo = nodCardAcceptor.selectNodes("child::*[name()='PaymentsInfo']").item(0)  ' Is only one

        'sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='PaymentReferenceNum']").nodeTypedValue 
        ' "PaymentReferenceNum" finnes ikke i alle filer (Ikke for Teller DK for JapanPhoto)
        '28.11.2017 - Changed from "PaymentReferenceNum" to "child::*[name()='PaymentReferenceNum']"

        ' 13.12,2017 - find probable bankdate from node below:
        nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='ActualSettlementDate']")
        If Not nodTemp Is Nothing Then
            sTempDate = nodTemp.nodeTypedValue  'nodPaymentsInfo.selectSingleNode("PaymentReferenceNum").nodeTypedValue '
            '25.11.2020 - Added next IF, Else-part is old code
            If sTempDate = "T00:00:00" Then
                nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='ExpectedSettlementDate']")
                If Not nodTemp Is Nothing Then
                    sTempDate = nodTemp.nodeTypedValue  'nodPaymentsInfo.selectSingleNode("PaymentReferenceNum").nodeTypedValue '
                    If sTempDate = "T00:00:00" Then
                        dDATE_Production = Now()
                    Else
                        dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2))
                    End If
                    '''sREF_Bank = nodMerchant.selectSingleNodAttributes.getNamedIteme("child::*[name()='Settlements' ]/@SubmissionNo       ").nodeTypedValue     ' SubmissionNo - Forsendelsesnummer
                End If
            Else
                dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2))
            End If
            '''sREF_Bank = nodMerchant.selectSingleNodAttributes.getNamedIteme("child::*[name()='Settlements' ]/@SubmissionNo       ").nodeTypedValue     ' SubmissionNo - Forsendelsesnummer
        End If

        nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='PaymentReferenceNum']")
        If Not nodTemp Is Nothing Then
            sREF_Bank = nodTemp.nodeTypedValue  'nodPaymentsInfo.selectSingleNode("PaymentReferenceNum").nodeTypedValue '
            '''sREF_Bank = nodMerchant.selectSingleNodAttributes.getNamedIteme("child::*[name()='Settlements' ]/@SubmissionNo       ").nodeTypedValue     ' SubmissionNo - Forsendelsesnummer
        End If
        ' 16.11.2017 - import BatchInfo, if not "PaymentReferenceNum" is in use
        If EmptyString(sREF_Bank) Then
            '28.11.2017 - Changed from:
            'sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='BatchRefNum']").nodeTypedValue
            'Created an error if BatchRefNum doesn't exists, f.ex. for SISMO
            nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='BatchRefNum']")
            If Not nodTemp Is Nothing Then
                sREF_Bank = nodTemp.nodeTypedValue
            End If
        End If

        sTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='GrossTransactionAmount']").nodeTypedValue ' �   Gross amount
        nMON_InvoiceAmount = ConvertToAmount(sTemp, vbNullString, ".")
        sTemp = vbNullString
        sTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='ExpectedNetSettlementAmount']").nodeTypedValue ' �   Net amount
        nMON_TransferredAmount = ConvertToAmount(sTemp, vbNullString, ".")

        ' 01.12.2017 - in some files we have ServiceChargeAmount and/or OtherFeesAmount
        ' Save this info, and put it into the first payment (???)
        nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='ServiceChargeAmount']")
        If Not nodTemp Is Nothing Then
            nTempCharges = System.Xml.XmlConvert.ToDouble(nodTemp.nodeTypedValue) * 100
        End If
        nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='OtherFeesAmount']")
        If Not nodTemp Is Nothing Then
            nTempCharges = nTempCharges + (System.Xml.XmlConvert.ToDouble(nodTemp.nodeTypedValue) * 100)
        End If

        'nNo_Of_Transactions = Val(nodPaymentsInfo.selectSingleNode("child::*[name()='NumOfTransactions']").nodeTypedValue) '
        nodBatchList = nodPaymentsInfo.selectNodes("child::*[name()='BatchInfo']")
        'Set nodPos_DetailList = nodCardAcceptor.selectNodes("child::*[name()='pos_detail'][brand='Total Bunt'] ")

        If oCargo.Special = "SERGELNORGE" Then
            '25.06.2020 - Must import every specification (invoice in regular cases) as a payment to get the matching effective
            For lNodeCounter = 0 To nodBatchList.length - 1
                'Pick an element from the lists
                nodBatch = nodBatchList.item(lNodeCounter)

                nodTransactionList = nodBatch.selectNodes("child::*[name()='TransactionLevel']")

                For lNodeCounter2 = 0 To nodTransactionList.length - 1

                    nodTransaction = nodTransactionList.item(lNodeCounter2)

                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.Cargo = oCargo
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    oPayment.I_Name = sIName
                    oPayment.I_Adr1 = nodCardAcceptor.selectSingleNode("child::*[name()='MainAddress']").nodeTypedValue 'Address
                    oPayment.I_Zip = nodCardAcceptor.selectSingleNode("child::*[name()='MainAddressZipCode']").nodeTypedValue 'Zip
                    oPayment.I_City = nodCardAcceptor.selectSingleNode("child::*[name()='MainAddressCity']").nodeTypedValue 'City

                    ' New in 2015
                    oPayment.I_Account = nodPaymentsInfo.selectSingleNode("child::*[name()='SettlementAccountNum']").nodeTypedValue   ' Mottakerkonto

                    'New 02.09.2020 - Next 3 lines
                    sTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='ActualSettlementDate']").nodeTypedValue
                    oPayment.DATE_Payment = Left$(sTemp, 4) & Mid$(sTemp, 6, 2) & Mid$(sTemp, 9, 2)
                    oPayment.DATE_Value = oPayment.DATE_Payment
                    oPayment.DATE_Payment = DateToString(GetBankday(StringToDate(oPayment.DATE_Payment), "NO", 1))

                    If lNodeCounter = 0 Then
                        '01.12.2017 - put charges into first payment
                        If nTempCharges <> 0 Then
                            oPayment.MON_ChargesAmount = nTempCharges * -1
                        End If
                        nTempCharges = 0
                    End If

                    'Continue to the payment-object
                    bReturnValue = oPayment.ImportTellerXMLNew_2015_DetailedImport(nodBatch, nodTransaction)

                    'oPayment.DATE_Payment = DateToString(GetBankday(StringToDate(oPayment.DATE_Payment), "NO", 1))

                    If bReturnValue = False Then
                        Exit For
                    End If


                    'Gammel kode
                    'M� n� hente dato fra payment i ettertid
                    '' 13.12.2017 - fjernet koden nedenfor - se dato lenger opp!
                    'If Payments.Count > 0 Then
                    '    dDATE_Production = StringToDate(oPayment.DATE_Payment)
                    'End If

                Next lNodeCounter2
            Next lNodeCounter

        Else
            For lNodeCounter = 0 To nodBatchList.length - 1
                'Pick an element from the lists
                nodBatch = nodBatchList.item(lNodeCounter)
                'Set nodCardAcceptor_Detail = nodCardAcceptor_DetailList.Item(lNodeCounter)

                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.I_Name = sIName
                oPayment.I_Adr1 = nodCardAcceptor.selectSingleNode("child::*[name()='MainAddress']").nodeTypedValue 'Address
                oPayment.I_Zip = nodCardAcceptor.selectSingleNode("child::*[name()='MainAddressZipCode']").nodeTypedValue 'Zip
                oPayment.I_City = nodCardAcceptor.selectSingleNode("child::*[name()='MainAddressCity']").nodeTypedValue 'City

                ' New in 2015
                oPayment.I_Account = nodPaymentsInfo.selectSingleNode("child::*[name()='SettlementAccountNum']").nodeTypedValue   ' Mottakerkonto
                ' For Butikkdrift, we have 4 clients, using same accountnumber;
                If oCargo.Special = "BUTIKKDRIFT_TELLER" Then
                    oPayment.I_Account = sI_Branch
                End If

                If lNodeCounter = 0 Then
                    '01.12.2017 - put charges into first payment
                    If nTempCharges <> 0 Then
                        oPayment.MON_ChargesAmount = nTempCharges * -1
                    End If
                    nTempCharges = 0
                End If

                'Continue to the payment-object
                bReturnValue = oPayment.ImportTellerXMLNew_2015(nodBatch)
                If bReturnValue = False Then
                    Exit For
                End If


                'Gammel kode
                'M� n� hente dato fra payment i ettertid
                '' 13.12.2017 - fjernet koden nedenfor - se dato lenger opp!
                'If Payments.Count > 0 Then
                '    dDATE_Production = StringToDate(oPayment.DATE_Payment)
                'End If


            Next lNodeCounter
        End If


        ' 13.11.2017 - IKKE beregn bel�p basert p� paymentbel�p - vi trenger forskjellen p� Mon_InvoiceAmount og MON_TransferredAmount
        'AddInvoiceAmountOnBatch()
        'AddTransferredAmountOnBatch()

        ReadTellerXMLNew_2015 = bReturnValue

        On Error GoTo 0
        Exit Function

ERR_ReadTellerXMLNew:

        If Not nodCardAcceptor Is Nothing Then
            nodCardAcceptor = Nothing
        End If

        Err.Raise(Err.Number, "Batch.ReadTellerXMLNEW_2015", Err.Description)

    End Function
    Private Function ReadVippsXML(ByVal SettlementDetailsInfo As System.Xml.XmlNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodSettlementInfoList As System.Xml.XmlNodeList
        Dim nodPaymentsInfo As System.Xml.XmlNode
        Dim nodTransactionList As System.Xml.XmlNodeList
        Dim nodTransaction As System.Xml.XmlNode
        Dim nodBatch As System.Xml.XmlNode
        Dim sTemp As String = "", sTemp2 As String = ""
        Dim sTempDate As String
        Dim sIName As String, sIAddress As String
        Dim lNodeCounter As Long
        Dim nodTemp As System.Xml.XmlNode

        bReturnValue = False

        sIName = GetInnerText(SettlementDetailsInfo, "child::*[name()='OrganizationName']", Nothing)
        sIAddress = GetInnerText(SettlementDetailsInfo, "child::*[name()='SaleUnitName']", Nothing) 'Brukerstednavn
        sI_EnterpriseNo = GetInnerText(SettlementDetailsInfo, "child::*[name()='OrganizationNumber']", Nothing) 'Kundenummer


        ' Select node PaymentsInfo
        nodPaymentsInfo = SettlementDetailsInfo.SelectSingleNode("child::*[name()='PaymentsInfo']")
        'nodPaymentsInfo = SettlementDetailsInfo.SelectNodes("child::*[name()='PaymentsInfo']").Item(0)  ' Is only one

        sREF_Bank = GetInnerText(nodPaymentsInfo, "child::*[name()='ReportReferenceNum']", Nothing)

        ' 14.05.2019 - Different types of Vipps XML files. Some of them do not have TotalSettledGrossAmount
        nodTemp = nodPaymentsInfo.SelectSingleNode("child::*[name()='TotalSettledNetAmount']")
        ' 14.05.2019 - Byttet om Gross og Net amount
        If Not nodTemp Is Nothing Then
            sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='TotalSettledNetAmount']", Nothing) ' �   Gross amount
            nMON_InvoiceAmount = Val(ConvertToAmount(sTemp, vbNullString, "."))
        End If
        sTemp = vbNullString
        nodTemp = nodPaymentsInfo.SelectSingleNode("child::*[name()='TotalSettledGrossAmount']")
        If Not nodTemp Is Nothing Then
            sTemp = GetInnerText(nodPaymentsInfo, "child::*[name()='TotalSettledGrossAmount']", Nothing) ' �   Net amount
            nMON_TransferredAmount = Val(ConvertToAmount(sTemp, vbNullString, "."))
        End If

        nodTemp = nodPaymentsInfo.SelectSingleNode("child::*[name()='TotalSettledRefundAmount']")
        If Not nodTemp Is Nothing Then
            sTemp2 = GetInnerText(nodPaymentsInfo, "child::*[name()='TotalSettledRefundAmount']", Nothing) ' Refund amount
            nMON_BalanceOUT = Val(ConvertToAmount(sTemp2, vbNullString, "."))
        End If
        nodTemp = nodPaymentsInfo.SelectSingleNode("child::*[name()='NumOfSettlements']")
        If Not nodTemp Is Nothing Then
            nNo_Of_Transactions = Val(GetInnerText(nodPaymentsInfo, "child::*[name()='NumOfSettlements']", Nothing)) '
        End If
        nodSettlementInfoList = nodPaymentsInfo.SelectNodes("child::*[name()='SettlementInfo']")

        For Each nodBatch In nodSettlementInfoList
            'Set nodCardAcceptor_Detail = nodCardAcceptor_DetailList.Item(lNodeCounter)

            ' added 14.05.2019
            oCargo.DATE_Payment = "19900101"
            nodTemp = nodBatch.SelectSingleNode("child::*[name()='SettlementDate']")
            If Not nodTemp Is Nothing Then
                sTempDate = GetInnerText(nodBatch, "child::*[name()='SettlementDate']", Nothing)
                dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2))
                oCargo.DATE_Payment = DateToString(dDATE_Production)
            End If

            If oCargo.Special = "GJENSIDIGE_VIPPS" Then
                '16.06.2021 - Added this If for Gjensidige
                'Every transaction becomes one payment!

                nodTransactionList = nodBatch.SelectNodes("child::*[name()='TransactionInfo']")

                For Each nodTransaction In nodTransactionList


                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.Cargo = oCargo
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    oPayment.I_Name = sIName
                    oPayment.I_Adr1 = GetInnerText(SettlementDetailsInfo, "child::*[name()='MainAddress']", Nothing) 'Address
                    oPayment.I_Zip = GetInnerText(SettlementDetailsInfo, "child::*[name()='MainAddressZipCode']", Nothing) 'Zip
                    oPayment.I_City = GetInnerText(SettlementDetailsInfo, "child::*[name()='MainAddressCity']", Nothing) 'City
                    oPayment.I_Account = GetInnerText(nodPaymentsInfo, "child::*[name()='SettlementAccountNum']", Nothing)   ' Mottakerkonto

                    'Continue to the payment-object
                    bReturnValue = oPayment.ImportVippsXML(nodBatch, nodTransaction)
                    If bReturnValue = False Then
                        Exit For
                    End If
                Next
            Else
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.I_Name = sIName
                oPayment.I_Adr1 = GetInnerText(SettlementDetailsInfo, "child::*[name()='MainAddress']", Nothing) 'Address
                oPayment.I_Zip = GetInnerText(SettlementDetailsInfo, "child::*[name()='MainAddressZipCode']", Nothing) 'Zip
                oPayment.I_City = GetInnerText(SettlementDetailsInfo, "child::*[name()='MainAddressCity']", Nothing) 'City
                oPayment.I_Account = GetInnerText(nodPaymentsInfo, "child::*[name()='SettlementAccountNum']", Nothing)   ' Mottakerkonto

                'Continue to the payment-object
                bReturnValue = oPayment.ImportVippsXML(nodBatch, Nothing)
                If bReturnValue = False Then
                    Exit For
                End If

            End If

            ' 14.05.2019 - added next If
            'M� n� hente dato fra payment i ettertid
            If dDATE_Production <= DateSerial(1990, 1, 1) Then
                If Payments.Count > 0 Then
                    dDATE_Production = StringToDate(oPayment.DATE_Payment)
                End If
            End If

        Next

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        If Not nodSettlementInfoList Is Nothing Then
            nodSettlementInfoList = Nothing
        End If

        ReadVippsXML = bReturnValue

    End Function
    Private Function SLETT_ReadVippsXML(ByVal SettlementDetailsInfo As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodSettlementInfoList As MSXML2.IXMLDOMNodeList
        'Dim nodCardAcceptor_DetailList As MSXML2.IXMLDOMNodeList
        Dim nodPaymentsInfo As MSXML2.IXMLDOMNode
        Dim nodBatch As MSXML2.IXMLDOMNode
        'Dim nodCardAcceptor_Detail As MSXML2.IXMLDOMNode
        Dim sTemp As String = "", sTemp2 As String = ""
        Dim sTempDate As String
        Dim sIName As String, sIAddress As String
        Dim lNodeCounter As Long
        Dim nodTemp As MSXML2.IXMLDOMNode

        Try

            bReturnValue = False

            sIName = SettlementDetailsInfo.selectSingleNode("child::*[name()='OrganizationName']").nodeTypedValue
            sIAddress = SettlementDetailsInfo.selectSingleNode("child::*[name()='SaleUnitName']").nodeTypedValue 'Brukerstednavn
            sI_EnterpriseNo = SettlementDetailsInfo.selectSingleNode("child::*[name()='OrganizationNumber']").nodeTypedValue ' �   Kundenummer


            ' Select node PaymentsInfo
            'Set nodPaymentsInfo = nodCardAcceptor.selectNodes("child::*/child::*[name()='PaymentsInfo']")
            nodPaymentsInfo = SettlementDetailsInfo.selectNodes("child::*[name()='PaymentsInfo']").item(0)  ' Is only one

            sREF_Bank = nodPaymentsInfo.selectSingleNode("child::*[name()='ReportReferenceNum']").nodeTypedValue

            ' 14.05.2019 - Different types of Vipps XML files. Some of them do not have TotalSettledGrossAmount
            nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='TotalSettledNetAmount']")
            ' 14.05.2019 - Byttet om Gross og Net amount
            If Not nodTemp Is Nothing Then
                sTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='TotalSettledNetAmount']").nodeTypedValue ' �   Gross amount
                nMON_TransferredAmount = Val(ConvertToAmount(sTemp, vbNullString, "."))
            End If
            sTemp = vbNullString
            nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='TotalSettledGrossAmount']")
            If Not nodTemp Is Nothing Then
                sTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='TotalSettledGrossAmount']").nodeTypedValue ' �   Net amount
                nMON_InvoiceAmount = Val(ConvertToAmount(sTemp, vbNullString, "."))
            End If

            nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='TotalSettledRefundAmount']")
            If Not nodTemp Is Nothing Then
                sTemp2 = nodPaymentsInfo.selectSingleNode("child::*[name()='TotalSettledRefundAmount']").nodeTypedValue ' Refund amount
                nMON_BalanceOUT = Val(ConvertToAmount(sTemp2, vbNullString, "."))
            End If
            nodTemp = nodPaymentsInfo.selectSingleNode("child::*[name()='NumOfSettlements']")
            If Not nodTemp Is Nothing Then
                nNo_Of_Transactions = Val(nodPaymentsInfo.selectSingleNode("child::*[name()='NumOfSettlements']").nodeTypedValue) '
            End If
            nodSettlementInfoList = nodPaymentsInfo.selectNodes("child::*[name()='SettlementInfo']")

            For lNodeCounter = 0 To nodSettlementInfoList.length - 1
                'Pick an element from the lists
                nodBatch = nodSettlementInfoList.item(lNodeCounter)
                'Set nodCardAcceptor_Detail = nodCardAcceptor_DetailList.Item(lNodeCounter)

                ' added 14.05.2019
                oCargo.DATE_Payment = "19900101"
                nodTemp = nodBatch.selectSingleNode("child::*[name()='SettlementDate']")
                If Not nodTemp Is Nothing Then
                    sTempDate = nodBatch.selectSingleNode("child::*[name()='SettlementDate']").nodeTypedValue
                    dDATE_Production = DateSerial(Left$(sTempDate, 4), Mid$(sTempDate, 6, 2), Mid$(sTempDate, 9, 2))
                    oCargo.DATE_Payment = DateToString(dDATE_Production)
                End If

                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.I_Name = sIName
                oPayment.I_Adr1 = SettlementDetailsInfo.selectSingleNode("child::*[name()='MainAddress']").nodeTypedValue 'Address
                oPayment.I_Zip = SettlementDetailsInfo.selectSingleNode("child::*[name()='MainAddressZipCode']").nodeTypedValue 'Zip
                oPayment.I_City = SettlementDetailsInfo.selectSingleNode("child::*[name()='MainAddressCity']").nodeTypedValue 'City
                oPayment.I_Account = nodPaymentsInfo.selectSingleNode("child::*[name()='SettlementAccountNum']").nodeTypedValue   ' Mottakerkonto

                'Continue to the payment-object
                bReturnValue = oPayment.ImportVippsXML(nodBatch, Nothing)
                If bReturnValue = False Then
                    Exit For
                End If

                ' 14.05.2019 - added next If
                'M� n� hente dato fra payment i ettertid
                If dDATE_Production <= DateSerial(1990, 1, 1) Then
                    If Payments.Count > 0 Then
                        dDATE_Production = StringToDate(oPayment.DATE_Payment)
                    End If
                End If


            Next lNodeCounter

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            If Not nodSettlementInfoList Is Nothing Then
                nodSettlementInfoList = Nothing
            End If

            SLETT_ReadVippsXML = bReturnValue


        Catch ex As Exception

            If Not nodSettlementInfoList Is Nothing Then
                nodSettlementInfoList = Nothing
            End If

            Err.Raise(Err.Number, "Batch.ReadVippsXML", Err.Description)
        End Try

    End Function
    Private Function ReadGjensidigeJDE_Basware_XML(ByVal nodInvoice As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodRow As MSXML2.IXMLDOMNode
        Dim nodRows As MSXML2.IXMLDOMNode
        Dim oROWList As MSXML2.IXMLDOMNodeList
        Dim l As Long = 0

        Try

            bReturnValue = False

            ' create 2 payments, one SU (LE01/04) and one GL (Le02/03)
            oCargo.PayType = "SU"
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            'Continue to the payment-object
            bReturnValue = oPayment.ImportGjensidigeJDE_Basware_XML(nodInvoice, Nothing)

            ' 17.04.2018 - there can be more than one <ROW> under <ROWS> - we need one payment pr <ROW>
            nodRows = nodInvoice.selectSingleNode("child::*[name()='ROWS']")
            oROWList = nodRows.selectNodes("child::*[name()='ROW']")
            'oDOCList = oSEQ.selectNodes("//GROUP[@group = ' 7']")
            '"//GROUP/DOC"
            'oSEQ.selectNodes("GROUP/GROUP[@group = ' 21']")
            'oUNH = oXMLDoc.selectSingleNode("//UNB/UNG/GROUP/UNH").parentNode

            For l = 0 To oROWList.length - 1
                'nodRow = nodRows.selectSingleNode("child::*[name()='ROW']")
                nodRow = oROWList(l)
                oCargo.PayType = "GL"
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                'Continue to the payment-object
                bReturnValue = oPayment.ImportGjensidigeJDE_Basware_XML(nodInvoice, nodRow)
            Next l

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            ReadGjensidigeJDE_Basware_XML = bReturnValue


        Catch ex As Exception

            Err.Raise(Err.Number, "Batch.ReadGjensidigeJDE_Basware_XML", Err.Description)
        End Try

    End Function
    Private Function ReadGjensidigeJDE_Basware_GL02_XML(ByVal nodInvoice As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodRow As MSXML2.IXMLDOMNode
        Dim nodRows As MSXML2.IXMLDOMNode
        Dim oROWList As MSXML2.IXMLDOMNodeList
        Dim l As Long = 0

        Try

            bReturnValue = False

            ' create 2 payments, one SU (LE01/04) and one GL (Le02/03)
            oCargo.PayType = "SU"
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            'Continue to the payment-object
            bReturnValue = oPayment.ImportGjensidigeJDE_Basware_GL02_XML(nodInvoice, Nothing)

            ' 17.04.2018 - there can be more than one <ROW> under <ROWS> - we need one payment pr <ROW>
            nodRows = nodInvoice.selectSingleNode("child::*[name()='ROWS']")
            oROWList = nodRows.selectNodes("child::*[name()='ROW']")
            'oDOCList = oSEQ.selectNodes("//GROUP[@group = ' 7']")
            '"//GROUP/DOC"
            'oSEQ.selectNodes("GROUP/GROUP[@group = ' 21']")
            'oUNH = oXMLDoc.selectSingleNode("//UNB/UNG/GROUP/UNH").parentNode

            For l = 0 To oROWList.length - 1
                'nodRow = nodRows.selectSingleNode("child::*[name()='ROW']")
                nodRow = oROWList(l)
                oCargo.PayType = "GL"
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                'Continue to the payment-object
                bReturnValue = oPayment.ImportGjensidigeJDE_Basware_GL02_XML(nodInvoice, nodRow)
            Next l

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            ReadGjensidigeJDE_Basware_GL02_XML = bReturnValue


        Catch ex As Exception

            Err.Raise(Err.Number, "Batch.ReadGjensidigeJDE_Basware_GL02_XML", Err.Description)
        End Try

    End Function
    Private Function ReadGjensidigeJDE_XLedger_Accounts_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodInvoice As MSXML2.IXMLDOMNode
        Dim l As Long = 0

        Try

            bReturnValue = False

            ' there can be more than one <ObjectValue> under <ObjectValues> - we need one payment pr <ObjectValue>
            For l = 0 To nodInvoices.length - 1
                nodInvoice = nodInvoices(l)
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                'Continue to the payment-object
                bReturnValue = oPayment.ImportGjensidigeJDE_Xledger_Accounts_XML(nodInvoice)
            Next l

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            ReadGjensidigeJDE_XLedger_Accounts_XML = bReturnValue


        Catch ex As Exception

            Err.Raise(Err.Number, "Batch.ReadGjensidigeJDE_Xledger_Accounts_XML", Err.Description)
        End Try

    End Function
    Private Function ReadGjensidigeJDE_XLedger_Costcenters_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodInvoice As MSXML2.IXMLDOMNode
        Dim nodTemp As MSXML2.IXMLDOMNode
        Dim l As Long = 0

        Try

            bReturnValue = False

            ' there can be more than one <ObjectValue> under <ObjectValues> - we need one payment pr <ObjectValue>
            For l = 0 To nodInvoices.length - 1
                nodInvoice = nodInvoices(l)

                ' do not add record if <Code> is empty:
                '1	Kontonummer	Code	Text_1	<Code>10121</Code>
                nodTemp = nodInvoice.selectSingleNode("child::*[name()='Code']")
                If Not nodTemp Is Nothing Then
                    If nodInvoice.selectSingleNode("child::*[name()='Code']").nodeTypedValue <> "" Then
                        oPayment = oPayments.Add()
                        oPayment.objFile = oFile
                        oPayment.VB_Profile = oProfile
                        oPayment.Cargo = oCargo
                        oPayment.ImportFormat = iImportFormat
                        'Continue to the payment-object
                        bReturnValue = oPayment.ImportGjensidigeJDE_Xledger_Costcenters_XML(nodInvoice)
                    End If
                End If
            Next l

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            ReadGjensidigeJDE_XLedger_Costcenters_XML = bReturnValue


        Catch ex As Exception

            Err.Raise(Err.Number, "Batch.ReadGjensidigeJDE_Xledger_Costcenters_XML", Err.Description)
        End Try

    End Function
    Private Function ReadGjensidigeJDE_XLedger_Underregnskap_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodInvoice As MSXML2.IXMLDOMNode
        Dim l As Long = 0

        Try

            bReturnValue = False

            ' there can be more than one <ObjectValue> under <ObjectValues> - we need one payment pr <ObjectValue>
            For l = 0 To nodInvoices.length - 1
                nodInvoice = nodInvoices(l)
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                'Continue to the payment-object
                bReturnValue = oPayment.ImportGjensidigeJDE_Xledger_Underregnskap_XML(nodInvoice)
            Next l

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            ReadGjensidigeJDE_XLedger_Underregnskap_XML = bReturnValue


        Catch ex As Exception

            Err.Raise(Err.Number, "Batch.ReadGjensidigeJDE_Xledger_Underregnskap_XML", Err.Description)
        End Try

    End Function
    Private Function ReadGjensidigeJDE_XLedger_Suppliers_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim nodInvoice As MSXML2.IXMLDOMNode
        Dim l As Long = 0

        Try

            bReturnValue = False

            ' there can be more than one <Suppliers> under <Suppliers> - we need one payment pr <Suppliers>
            For l = 0 To nodInvoices.length - 1
                nodInvoice = nodInvoices(l)

                ' 04.09.2020
                ' If <Code> is blank, then do not import
                '1	Leverand�rnummer	<Code>	<Code>	<Code>2001510</Code>
                If Not nodInvoice.selectSingleNode("child::*[name()='Code']") Is Nothing Then
                    If Not EmptyString(nodInvoice.selectSingleNode("child::*[name()='Code']").nodeTypedValue) Then

                        oPayment = oPayments.Add()
                        oPayment.objFile = oFile
                        oPayment.VB_Profile = oProfile
                        oPayment.Cargo = oCargo
                        oPayment.ImportFormat = iImportFormat
                        'Continue to the payment-object
                        bReturnValue = oPayment.ImportGjensidigeJDE_Xledger_Suppliers_XML(nodInvoice)
                    End If
                End If

            Next l

            AddInvoiceAmountOnBatch()
            AddTransferredAmountOnBatch()

            ReadGjensidigeJDE_XLedger_Suppliers_XML = bReturnValue


        Catch ex As Exception

            Err.Raise(Err.Number, "Batch.ReadGjensidigeJDE_Xledger_Suppliers_XML", Err.Description)
        End Try

    End Function
    'Removed 11.09.2019
    'Private Function ReadTBIFile(ByRef nodBatch As MSXML2.IXMLDOMNode) As Boolean
    '    Dim oPayment As Payment
    '    Dim bReturnValue As Boolean
    '    Dim lNodePaymentCounter As Integer
    '    Dim lNodePaymentListCounter As Integer
    '    Dim nodPaymentList As MSXML2.IXMLDOMNodeList
    '    Dim nodPayment As MSXML2.IXMLDOMNode
    '    'Dim nodPaymentDetailsList As MSXML2.IXMLDOMNodeList
    '    'Dim nodPaymentDetails As MSXML2.IXMLDOMNode

    '    bReturnValue = False

    '    'The nod PaymentList will contain all the PaymentDetail Tags under this Payment
    '    nodPaymentList = nodBatch.selectNodes("child::*[name()='PaymentDetails']")

    '    'UPGRADE_WARNING: Couldn't resolve default property of object nodBatch.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    sREF_Own = nodBatch.selectSingleNode("child::*[name()='PayersReference']").nodeTypedValue
    '    nMON_InvoiceAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='Amount']").nodeTypedValue, ".", ""))
    '    bAmountSetInvoice = True
    '    sFormatType = "TBI_" & nodBatch.selectSingleNode("child::*[name()='PaymentType']").nodeTypedValue
    '    If sStatusCode = "02" Then
    '        'UPGRADE_WARNING: Couldn't resolve default property of object nodBatch.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        sREF_Bank = nodBatch.selectSingleNode("child::*[name()='BankReference']").nodeTypedValue
    '        nMON_TransferredAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='AmountTransferred']").nodeTypedValue, ".", ""))
    '        bAmountSetTransferred = True
    '    End If


    '    For lNodePaymentCounter = 0 To nodPaymentList.length - 1

    '        'Pick an element from the list
    '        nodPayment = nodPaymentList.item(lNodePaymentCounter)

    '        '    Set nodPaymentDetailsList = nodPayment.selectNodes("child::*[name()='PaymentDetails']")
    '        '    Set nodPaymentDetails = nodPaymentDetailsList.Item(lNodePaymentListCounter)

    '        oPayment = oPayments.Add()
    '        oPayment.objFile = oFile
    '        oPayment.VB_Profile = oProfile
    '        oPayment.Cargo = oCargo
    '        oPayment.ImportFormat = iImportFormat
    '        oPayment.StatusCode = sStatusCode

    '        'Continue to the payment-object
    '        bReturnValue = oPayment.ImportTBI(nodBatch, nodPayment)
    '        If bReturnValue = False Then
    '            Exit For
    '        End If

    '    Next lNodePaymentCounter


    '    'If oPayment.PayType = "I" Then
    '    '    bx = AddInvoiceAmountOnBatch()
    '    'Else
    '    '    nMON_InvoiceAmount = nMON_TransferredAmount
    '    '    bAmountSetInvoice = True
    '    'End If

    '    ' New 31.03.05
    '    AddInvoiceAmountOnBatch()
    '    For Each oPayment In Payments
    '        If IsPaymentDomestic(oPayment, (oPayment.BANK_I_SWIFTCode)) Then
    '            oPayment.PayType = "D"
    '        Else
    '            oPayment.PayType = "I"
    '        End If
    '    Next oPayment

    '    ReadTBIFile = bReturnValue

    'End Function
    Private Function ReadOrklaMediaExcelFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim sResponse As String

        bFromLevelUnder = False

        sFormatType = "OrklaMediaExcel"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto")
                    If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop
            End If
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.I_Account = CObj(sResponse)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            'FIX: What's this??????????
            ReadOrklaMediaExcelFile = sImportLineBuffer

            'WARNING! Is this correct
            ' FIX:ED moved by janP 28.06
            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadExcelFile() As String
        ' Common importroutine for several Excel-files
        ' Special in setup decides format (oBabelfile.Special)
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean
        Dim sResponse As String, iResponse As Integer
        Dim bAbandon As Boolean, dPaymentDate As Date
        Dim bOutOfLoop As Boolean
        Dim sDay As String, sMonth As String, sYear As String
        Dim sI_Account As String
        ' XokNET 21.09.2011, removed some variables
        ' XokNET 16.11.2011 and added some agian
        Dim sOldCountryCode As String, sSwiftCode As String
        Dim sPaymentCurrency As String
        Dim oFs As New Scripting.FileSystemObject
        'Dim xmlSWIFTCodesDoc As New MSXML2.DOMDocument40
        'Dim eleCountryNode As MSXML2.IXMLDOMElement
        Dim xmlSWIFTCodes As System.Xml.XmlDocument
        Dim nodeCountryNode As System.Xml.XmlNode

        bFromLevelUnder = False

        sFormatType = "Excel"
        bAbandon = False

        Do While True


            Select Case UCase(oCargo.Special)

                Case "LITRA"

                    If Not bFromLevelUnder Then
                        Do While True
                            sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto")
                            If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                                Exit Do
                            Else
                                MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", vbOKOnly, "Feil kontonummer")
                            End If
                        Loop
                    End If

                    ' Remove headerlines;
                    If Left$(sImportLineBuffer, 19) = "Navn;Kontonr;Antall" Then
                        ' Read another line
                        If oFile.AtEndOfStream = False Then
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Else
                            Exit Do
                        End If
                    End If
                    ' --- End LITRA ---

                Case "PLANBO"

                    ' Remove headerlines;
                    If Left$(sImportLineBuffer, 17) = "Andel;Leil.nr;Til" Then
                        ' Read another line
                        If oFile.AtEndOfStream = False Then
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Else
                            Exit Do
                        End If
                    End If

                    Do
                        ' Also, skip payments with no accountno or zero-amount
                        If Len(xDelim(sImportLineBuffer, ";", 3)) = 0 Or ConvertToAmount(xDelim(sImportLineBuffer, ";", 7), vbNullString, ".,") = 0 Then
                            ' Carry on with next record
                            If Not oFile.AtEndOfStream Then
                                sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                            Else
                                sImportLineBuffer = ""
                                Exit Do
                            End If
                        Else
                            Exit Do
                        End If
                    Loop
                    ' --- End PLANBO ---
                Case "SANDVIK TAMROCK"
                    If EmptyString(oCargo.I_Account) Then
                        If Not bFromLevelUnder Then
                            Do While True
                                sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto")
                                If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                                    oCargo.I_Account = Trim$(sResponse)
                                    Exit Do
                                Else
                                    MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", vbOKOnly, "Feil kontonummer")
                                End If
                            Loop
                        End If
                    End If

                    '--- End SANDVIK TAMROCK ---

                    ' XNET 21.09.2011 Removed DNBNOR VERDIPAPIR
                    ' XNET 16.11.2011 Added it again !!!
                Case "DNBNOR VERDIPAPIR"

                    If Not bFromLevelUnder Then
                        sResponse = vbNullString
                        Do While True
                            sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                            If Len(sResponse) = 0 Then
                                bAbandon = True
                                Exit Do
                            ElseIf CheckAccountNoNOR(Trim$(sResponse)) Then
                                sI_Account = Trim$(sResponse)
                                Exit Do
                            Else
                                iResponse = MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "�nsker du � avbryte?", vbYesNo, "Feil kontonummer")
                                If iResponse = vbYes Then
                                    bAbandon = True
                                    Exit Do
                                Else
                                    'Continue
                                End If
                            End If
                        Loop

                        If bAbandon Then
                            Exit Do
                        End If

                        bOutOfLoop = False
                        ' Ask for paymentdate:
                        Do While bOutOfLoop = False

                            bOutOfLoop = True
                            sResponse = InputBox("Legg inn betalingsdato (dd.mm.yyyy)", "Utbetalingsdato", Trim(Str(DatePart(DateInterval.Day, Now))) + "." + Trim(Str(DatePart(DateInterval.Month, Now))) + "." + Trim(Str(DatePart(DateInterval.Year, Now))))
                            If sResponse = vbNullString Then
                                ' user pressed Cancel
                                bOutOfLoop = False
                                Exit Do
                            End If

                            If Len(sResponse) - Len(Replace(sResponse, ".", vbNullString)) = 2 Then
                                ' OK, two . in datestring:
                                ' Test date given;
                                sDay = Left$(sResponse, InStr(sResponse, ".") - 1)
                                sMonth = Mid$(sResponse, InStr(sResponse, ".") + 1, 2)
                                If Right$(sMonth, 1) = "." Then
                                    ' only one digit for month
                                    sMonth = Left$(sMonth, 1)
                                End If
                                sYear = Right$(sResponse, 4)
                                If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                                    MsgBox("Feil i �rstall - Pr�v igjen!")
                                    bOutOfLoop = False
                                End If
                                If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                                    MsgBox("Feil i m�ned - Pr�v igjen!")
                                    bOutOfLoop = False
                                End If
                                If Val(sDay) < 1 Or Val(sDay) > 31 Then
                                    MsgBox("Feil i dag - Pr�v igjen!")
                                    bOutOfLoop = False
                                End If
                                If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                                    If Val(sDay) > 30 Then
                                        MsgBox("Feil i dag - Pr�v igjen!")
                                        bOutOfLoop = False
                                    End If
                                End If
                                If Val(sMonth) = 2 Then
                                    If Val(sDay) > 29 Then
                                        MsgBox("Feil i dag - Pr�v igjen!")
                                        bOutOfLoop = False
                                    End If
                                End If
                            Else
                                MsgBox("Feil i dato!" + vbCrLf + "Skal ha formen dd.mm.yyyy" + _
                                vbCrLf + "Husk punktum som skilletegn!")
                                bOutOfLoop = False
                            End If
                        Loop
                        If sResponse = vbNullString Then
                            bOutOfLoop = True
                        Else
                            dPaymentDate = DateSerial(Int(sYear), Int(sMonth), Int(sDay))
                        End If

                        Do While True
                            sResponse = vbNullString
                            sResponse = InputBox("Legg inn melding til mottaker.", "Melding", sResponse)
                            If Len(sResponse) > 0 Then
                                iResponse = MsgBox("F�lgende melding blir lagt inn: " & vbCrLf & vbCrLf & sResponse & vbCrLf & "�nsker du � fortsette?", vbYesNo, "Melding OK?")
                                If iResponse = vbYes Then
                                    Exit Do
                                Else
                                    'Continue
                                End If
                            Else
                                iResponse = MsgBox("Det m� angis en melding til mottaker." & vbCrLf & "�nsker du � avbryte?", vbYesNo, "Feil kontonummer")
                                If iResponse = vbYes Then
                                    bAbandon = True
                                    Exit Do
                                Else
                                    'Continue
                                End If
                            End If
                        Loop

                        If bAbandon Then
                            Exit Do
                        Else
                            oCargo.FreeText = sResponse
                        End If

                        ' Remove headerlines;
                        If Left$(sImportLineBuffer, 14) = "Navn;Adr1;Adr2" Then
                            ' Read another line
                            If oFile.AtEndOfStream = False Then
                                sImportLineBuffer = ReadGenericRecord(oFile)
                            Else
                                Exit Do
                            End If
                        End If

                        'Load SWIFTCodes from XML-document
                        If oFs.FileExists(My.Application.Info.DirectoryPath & "\DnBNOR SWIFTCodes.xml") Then
                            'New 04.05.2022 - Removed use of MSXML
                            xmlSWIFTCodes = New System.Xml.XmlDocument
                            xmlSWIFTCodes.Load(My.Application.Info.DirectoryPath & "\DnBNOR SWIFTCodes.xml")
                            'xmlSWIFTCodesDoc.load(My.Application.Info.DirectoryPath & "\DnBNOR SWIFTCodes.xml")
                        Else
                            Err.Raise(10000, "ReadExcelFile", "Finner ikke filen DnBNOR SWIFTCodes.xml" & vbCrLf & vbCrLf & "Program avbrytes!")
                        End If
                        sOldCountryCode = vbNullString
                        sSwiftCode = vbNullString
                    Else
                        bx = False
                        Do Until Trim$(xDelim(sImportLineBuffer, ";", 6)) <> vbNullString
                            If oFile.AtEndOfStream = True Then
                                bx = True
                                Exit Do
                            End If
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Loop
                        If bx Then
                            Exit Do
                        End If
                        'Nothing to do


                    End If
                    ' --- End DNBNOR VERDIPAPIR ---

                Case "�STLENDINGEN"

                    ' Remove headerlines;
                    If Left$(sImportLineBuffer, 16) = "Dato;Bel�p;Konto" Then
                        ' Read another line
                        If oFile.AtEndOfStream = False Then
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Else
                            Exit Do
                        End If
                    End If
                    ' --- End �stlendingen ---

                Case "NESPRESSO"
                    ' mix between Domestic and International payments
                    ' When change between Int and Dom, create new batch
                    If xDelim(sImportLineBuffer, ";", 5) = "NOK" And Len(Replace(Replace(Replace(Trim$(xDelim(sImportLineBuffer, ";", 7)), "IBAN:", ""), ".", ""), " ", "")) = 11 Then
                        ' domestic
                        If oCargo.PayType <> "D" And Not EmptyString(oCargo.PayType) Then
                            ' from Int to Dom, jump to create batch
                            oCargo.PayType = "D"
                            Exit Do
                        End If
                    Else
                        If oCargo.PayType <> "I" And Not EmptyString(oCargo.PayType) Then
                            ' from Dom to Int, jump to create batch
                            oCargo.PayType = "I"
                            Exit Do
                        End If
                    End If
                    ' --- End Nespresso ---

                Case "FYSIO"

                    ' Remove headerlines;
                    If Left$(sImportLineBuffer, 18) = "Topp;Enhet;Forbund" Then
                        ' Read another line
                        If oFile.AtEndOfStream = False Then
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Else
                            Exit Do
                        End If
                    End If

                    Do
                        ' Also, skip payments zero-amount
                        If ConvertToAmount(xDelim(sImportLineBuffer, ";", 12), vbNullString, ".,") = 0 Then
                            ' Carry on with next record
                            If Not oFile.AtEndOfStream Then
                                sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                            Else
                                sImportLineBuffer = ""
                                Exit Do
                            End If
                        Else
                            Exit Do
                        End If
                    Loop
                    ' --- End FYSIO ---

                    'XOKNET 20.05.2011 - Added next Case
                Case "ODIN_UTBETALING"
                    ''XNET - 31.05.2011 - Whole Case "ODIN.....
                    ' Remove headerlines;

                    If bFromLevelUnder Then
                        If EmptyString(sImportLineBuffer) Then
                            Exit Do 'End of file just return
                        End If
                        If xDelim(sImportLineBuffer, ";", 7, Chr(34)) <> oCargo.Temp Then
                            oCargo.REF = ""
                            Exit Do 'Create a new batch
                        End If
                    End If
                    If Left$(sImportLineBuffer, 19) = "KONTONUMMERBETALER;" Then
                        ' Read another line
                        If oFile.AtEndOfStream = False Then
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Else
                            Exit Do
                        End If
                    End If
                    oCargo.REF = "ODIN_LastLineIsUpdated"
                    sI_EnterpriseNo = xDelim(sImportLineBuffer, ";", 7, Chr(34))
                    oCargo.Temp = sI_EnterpriseNo
                    ' --- End ODIN_UTBETALING ---

                    'XNET 24.08.2011 - Added next Case
                Case "ODIN_UTBETALING_UTLAND"
                    ''XNET - 31.05.2011 - Whole Case "ODIN.....
                    ' Remove headerlines;

                    If bFromLevelUnder Then
                        If EmptyString(sImportLineBuffer) Then
                            Exit Do 'End of file just return
                        End If
                        If Trim(xDelim(sImportLineBuffer, ";", 15, Chr(34))) <> oCargo.Temp Then
                            oCargo.REF = ""
                            Exit Do 'Create a new batch
                        End If
                    End If
                    If Left$(sImportLineBuffer, 19) = "KONTONUMMERBETALER;" Then
                        ' Read another line
                        If oFile.AtEndOfStream = False Then
                            sImportLineBuffer = ReadGenericRecord(oFile)
                        Else
                            Exit Do
                        End If
                    End If
                    oCargo.REF = "ODIN_LastLineIsUpdated"
                    sI_EnterpriseNo = Trim(xDelim(sImportLineBuffer, ";", 15, Chr(34)))
                    oCargo.Temp = sI_EnterpriseNo
                    ' --- End ODIN_UTBETALING_UTLAND ---
                Case "LOOMIS_CASHIN", "LOOMIS_CASHIN_ISO", "LOOMIS_SCANCOIN", "LOOMIS_SCANCOIN_FINLAND", "LOOMIS_SCANCOIN_ISO"
                    If Trim$(xDelim(sImportLineBuffer, ";", 1)) = "T" Then
                        Exit Do
                    End If
                Case "LOOMIS_VALUTAAVDELING", "LOOMIS_VALUTAAVDELING_ISO"
                    If Trim$(xDelim(sImportLineBuffer, ";", 1)) = "T" Then
                        Exit Do
                    End If

            End Select

            If Len(sImportLineBuffer) > 0 Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                If UCase(oCargo.Special) = "LITRA" Then
                    oPayment.I_Account = CObj(sResponse)
                ElseIf UCase(oCargo.Special) = "SANDVIK TAMROCK" Then
                    oPayment.I_Account = CObj(oCargo.I_Account)
                ElseIf UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
                    oPayment.I_Account = CObj(sI_Account)
                    oPayment.DATE_Payment = DateToString(dPaymentDate)
                End If

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)

                ' XNET 23.09.2011 Removed DNBNOR VERDIPAPIR
                ' XNET 16.11.2011 Added next if
                If UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
                    'Add SWIFT-adress to the payment
                    If oPayment.PayCode = "160" And oPayment.PayType = "I" Then
                        If sOldCountryCode = oPayment.E_CountryCode Then
                            ' 26.06.2007
                            ' next line commented out, because DnBNOR will not have Swift for check-paymnents
                            'oPayment.BANK_SWIFTCode = sSwiftCode
                            oPayment.MON_TransferCurrency = sPaymentCurrency
                        Else
                            'New 04.05.2022 - Removed use of MSXML
                            'eleCountryNode = xmlSWIFTCodesDoc.selectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/SWIFT")
                            nodeCountryNode = xmlSWIFTCodes.SelectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/SWIFT")
                            If nodeCountryNode Is Nothing Then
                                Err.Raise(10000, "ReadExcelFile", "Finner ikke SWIFTkoden for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            Else
                                sOldCountryCode = oPayment.E_CountryCode
                                sSwiftCode = nodeCountryNode.InnerText
                                ' 26.06.2007
                                ' next line commented out, because DnBNOR will not have Swift for check-paymnents
                                'oPayment.BANK_SWIFTCode = sSwiftCode
                            End If

                            'New 04.05.2022 - Removed use of MSXML
                            nodeCountryNode = xmlSWIFTCodes.SelectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/BETVALUTA")
                            'eleCountryNode = xmlSWIFTCodesDoc.selectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/BETVALUTA")
                            If nodeCountryNode Is Nothing Then
                                Err.Raise(10000, "ReadExcelFile", "Finner ikke betalingsvaluta for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            Else
                                sPaymentCurrency = nodeCountryNode.InnerText
                                oPayment.MON_TransferCurrency = sPaymentCurrency
                            End If
                            'eleCountryNode = xmlSWIFTCodesDoc.selectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/SWIFT")
                            'If eleCountryNode Is Nothing Then
                            '    Err.Raise(10000, "ReadExcelFile", "Finner ikke SWIFTkoden for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            'Else
                            '    sOldCountryCode = oPayment.E_CountryCode
                            '    sSwiftCode = eleCountryNode.nodeTypedValue
                            '    ' 26.06.2007
                            '    ' next line commented out, because DnBNOR will not have Swift for check-paymnents
                            '    'oPayment.BANK_SWIFTCode = sSwiftCode
                            'End If
                            'eleCountryNode = xmlSWIFTCodesDoc.selectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/BETVALUTA")
                            'If eleCountryNode Is Nothing Then
                            '    Err.Raise(10000, "ReadExcelFile", "Finner ikke betalingsvaluta for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            'Else
                            '    sPaymentCurrency = eleCountryNode.nodeTypedValue
                            '    oPayment.MON_TransferCurrency = sPaymentCurrency
                            'End If
                        End If
                    End If
                End If

                bFromLevelUnder = True
            End If

            ReadExcelFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
                    If Trim$(xDelim(sImportLineBuffer, ";", 6)) = vbNullString Then
                        Exit Do
                    End If
                    'XNET - 31.05.2011 - Added next ElseIf
                ElseIf Left(UCase(oCargo.Special), 15) = "ODIN_UTBETALING" Then
                    'Nothing to do
                    'XNET - 24.08.2011 - Added next ElseIf
                ElseIf sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        If Not bAbandon Then
            bx = AddInvoiceAmountOnBatch()
        Else
            Err.Raise(10000, "ReadExcel", "Brukeren har avbrutt prosessen")
        End If

        ' XNET 23.09.2011 Removed DNBNOR VERDIPAPIR
        ' XNET 16.11.2011 Added next if
        If UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
            'xmlSWIFTCodesDoc = Nothing
            'eleCountryNode = Nothing
            'New 04.05.2022 - Removed use of MSXML
            xmlSWIFTCodes = Nothing
            nodeCountryNode = Nothing
        End If

    End Function
    Private Function ReadExcelInnbetalingFile() As String
        ' Common importroutine for several Excel-files
        ' Special in setup decides format (oBabelfile.Special)
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim sResponse As String
        Dim iResponse As Short
        Dim bAbandon As Boolean
        Dim dPaymentDate As Date
        Dim bOutOfLoop As Boolean
        Dim sMonth, sDay, sYear As String
        Dim sI_Account As String
        Dim sOldCountryCode, sSwiftCode As String
        Dim sPaymentCurrency As String

        bFromLevelUnder = False

        sFormatType = "Excel"
        bAbandon = False

        Do While True


            If iImportFormat = vbBabel.BabelFiles.FileType.Modhi_Internal Then
                '01.05.2022 - Added this If
                If Len(sImportLineBuffer) > 0 Then
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile

                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                    bFromLevelUnder = True
                End If

                ReadExcelInnbetalingFile = sImportLineBuffer

                If oFile.AtEndOfStream = True Then
                    Exit Do
                ElseIf sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then  'FIX:ED New by janp 28.06
                    Exit Do
                Else
                    sImportLineBuffer = ReadGenericRecord(oFile)
                End If

            Else
                'Select Case UCase(oCargo.Special)
                Select Case oCargo.Bank

                    Case BabelFiles.Bank.DnB
                        If Len(sImportLineBuffer) > 0 Then
                            oPayment = oPayments.Add()
                            oPayment.objFile = oFile
                            oPayment.VB_Profile = oProfile

                            oPayment.ImportFormat = iImportFormat
                            oPayment.StatusCode = sStatusCode
                            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                            bFromLevelUnder = True
                        End If

                        ReadExcelInnbetalingFile = sImportLineBuffer

                        If oFile.AtEndOfStream = True Then
                            Exit Do
                        ElseIf sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then  'FIX:ED New by janp 28.06
                            Exit Do
                        End If

                        '06.11.2014 - Do not create a new batch per payment
                        'I do not know what is the best solution, but for LHL it was one batchamount
                        'Exit Do
                        ' 18.08.2020 - Difficult to find the best solution,
                        '              BUT - if not NOK, then create one batch pr payment, as it usually is for Int.payments.
                        '              This way we will also be able to test with correct Agio-calculations (like for Storm Geo)
                        If oPayment.MON_InvoiceCurrency <> "NOK" Then
                            Exit Do
                        End If

                        'XNET 27.09.2012 - Added case NoBankSpecified
                    Case BabelFiles.Bank.No_Bank_Specified
                        Select Case oCargo.Special

                            Case "FJORDKRAFT_TEMP"
                                If Len(sImportLineBuffer) > 0 And xDelim(sImportLineBuffer, ";", 1) = "1" Then
                                    oPayment = oPayments.Add()
                                    oPayment.objFile = oFile
                                    oPayment.VB_Profile = oProfile

                                    oPayment.ImportFormat = iImportFormat
                                    oPayment.StatusCode = sStatusCode
                                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                                    bFromLevelUnder = True
                                Else
                                    sImportLineBuffer = ReadGenericRecord(oFile)
                                End If

                                ReadExcelInnbetalingFile = sImportLineBuffer

                                If oFile.AtEndOfStream = True Then
                                    Exit Do
                                ElseIf sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                                    Exit Do
                                End If

                            Case "UAVST_RAPP"
                                If Len(sImportLineBuffer) > 0 Then
                                    oPayment = oPayments.Add()
                                    oPayment.objFile = oFile
                                    oPayment.VB_Profile = oProfile

                                    oPayment.ImportFormat = iImportFormat
                                    oPayment.StatusCode = sStatusCode
                                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                                    bFromLevelUnder = True
                                Else
                                    sImportLineBuffer = ReadGenericRecord(oFile)
                                End If

                                ReadExcelInnbetalingFile = sImportLineBuffer

                                If oFile.AtEndOfStream = True Then
                                    Exit Do
                                ElseIf sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                                    Exit Do
                                End If

                        End Select
                End Select
            End If
        Loop

        If Not bAbandon Then
            bx = AddInvoiceAmountOnBatch()
            nMON_TransferredAmount = nMON_InvoiceAmount
        Else
            Err.Raise(10000, "ReadExcelInnbetaling", "Brukeren har avbrutt prosessen")
        End If

    End Function
    'XNET - 10.10.2012 - New function
    Private Function ReadVingcardNettingFile() As String
        ' Common importroutine for several Excel-files
        ' Special in setup decides format (oBabelfile.Special)
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bCreateObject As Boolean
        Dim sI_Name As String
        Dim sE_Name As String
        Dim sMON_InvoiceCurrency As String

        bFromLevelUnder = False

        sFormatType = "Vingcard netting"
        'oCargo.PayCode = "901" 'Payment

        ' 1. Payers name
        sE_Name = xDelim(sImportLineBuffer, ",", 3, Chr(34))
        ' 2. Receivers name
        sI_Name = xDelim(sImportLineBuffer, ",", 4, Chr(34))
        sMON_InvoiceCurrency = xDelim(sImportLineBuffer, ",", 7, Chr(34))

        Do While True

            bCreateObject = False

            If Left(sImportLineBuffer, 11) = ",,,,,,,,,,," Or EmptyString(sImportLineBuffer) Then
                'End of payables, try to find receivebles
                Do While True
                    sImportLineBuffer = ReadGenericRecord(oFile)
                    If Not oFile.AtEndOfStream Then
                        If Left(sImportLineBuffer, 19) = "INVOICES RECEIVABLE" Then
                            '2340966,,NOO3136,USO4302,12.07.2012,17.07.2012,USD,257.4,,,0.16472496,1562.6
                            ',,,,,,,,,,,
                            'INVOICES RECEIVABLE BY NOO3136 IN NOK FOR SETTLEMENT 30072012,,,,,,,,,,,
                            'IN781149,,AUC5052,NOO3136,19.07.2012,30.07.2012,AUD,11075.27,,,0.15951815,69429.53
                            oCargo.PayCode = "902" 'Receiving
                            sImportLineBuffer = ReadVingcardRecord(oFile)
                            bCreateObject = True
                            Exit Do
                        ElseIf Left(sImportLineBuffer, 16) = "INVOICES PAYABLE" Then
                            oCargo.PayCode = "901" 'Payable
                            sImportLineBuffer = ReadVingcardRecord(oFile)
                            bCreateObject = True
                            Exit Do
                        Else
                            'Read another line
                        End If
                    Else
                        bCreateObject = False
                        Exit Do
                    End If
                Loop
            Else
                bCreateObject = True
            End If

            'If I- or E_Name differs from the last payment exit and create a new batch
            If sE_Name <> xDelim(sImportLineBuffer, ",", 3, Chr(34)) Then
                bCreateObject = False
                Exit Do
            ElseIf sI_Name <> xDelim(sImportLineBuffer, ",", 4, Chr(34)) Then
                bCreateObject = False
                Exit Do
            End If
            If sMON_InvoiceCurrency <> xDelim(sImportLineBuffer, ",", 7, Chr(34)) Then
                bCreateObject = False
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            ReadVingcardNettingFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                Exit Do
            ElseIf sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                Exit Do
            Else
                sImportLineBuffer = ReadVingcardRecord(oFile)
            End If

        Loop

        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadVingcardNettingFile = sImportLineBuffer

    End Function
    'XNET - 10.10.2012 - New function
    Private Function ReadElavonFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "Elavon specifications"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadElavonFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                Exit Do
            ElseIf sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                Exit Do
            Else
                sImportLineBuffer = ReadGenericRecord(oFile)
                If xDelim(sImportLineBuffer, ";", 5, Chr(34)) = "" And xDelim(sImportLineBuffer, ";", 9, Chr(34)) = "" Then
                    'Not a regular transaction, read another line and create a new batch

                    'Sometimes there is a special transaction for chargeback (tilbakef�ring), use the same batch for this
                    If xDelim(sImportLineBuffer, ";", 4, Chr(34)) = "CHARGEBACK" Then
                        sImportLineBuffer = ReadGenericRecord(oFile) 'Read the actual chargeback
                    Else
                        sImportLineBuffer = ReadGenericRecord(oFile)
                        'Exit Do
                    End If
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadElavonFile = sImportLineBuffer

    End Function
    Private Function ReadElavon_2017() As String
        ' As documentented in:
        ' Nordic Statement CSV File Delivery Format Guide         July, 2017
        '
        'OH;1;STATENS INNKREVINGSSENTRAL;TERMINALVEIEN 2,MO I RANA,8601,NOR;CSVV02;regn@sismo.no;2018-06-11;8041133;971648198;
        'BD;2;8041133;111816205151;2018-06-11;0512057;137262.32;36;NOK;
        'SD;3;8041133;111816205151;87162950416;M/C;N;2018-06-01;2018-06-30;2018-06-11;00000428387 177;GMNETEDC;75113.30;-480.00;0.00;-480.00;74633.30;12;
        'TD;4;8041133;111816205151;87162950416;MC CONS CR - M FUC 2263;2018-06-10;03177885203280180506488;X1981;068956;801814;3440.00;NOK;3440.00;3400.00;-40.0000;0.0000;-40.00;0.00;-40.00;1U05G4RGROI;87162950417;;
        'TD;5;8041133;111816205151;87162950416;MC CONS CR - M FUC 2263;2018-06-10;01061743903934265600000;X5135;138024;983800;5388.00;NOK;5388.00;5348.00;-40.0000;0.0000;-40.00;0.00;-40.00;1U05G4RGROJ;87162950418;;
        'TD;6;8041133;111816205151;87162950416;MC CONS DB - M FUC 8338;2018-06-10;05436928204280180513797;X4333;009528;480026;3440.00;NOK;3440.00;3400.00;-40.0000;0.0000;-40.00;0.00;-40.00;1U05G4RGROK;87162950419;;


        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "Elavon_2017"

        Do While True

            If Left(sImportLineBuffer, 3) = "SD;" Then
                ' If new batch-record, then jump up to BabelFile and create a new oBatch
                Exit Do
            End If
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadElavon_2017 = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                Exit Do
            ElseIf sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                Exit Do
            Else
                sImportLineBuffer = ReadGenericRecord(oFile)
                If xDelim(sImportLineBuffer, ";", 5, Chr(34)) = "" And xDelim(sImportLineBuffer, ";", 9, Chr(34)) = "" Then
                    'Not a regular transaction, read another line and create a new batch
                    ' ??????? Finnes disse "unregulars" i det nye formatet????
                    'Sometimes there is a special transaction for chargeback (tilbakef�ring), use the same batch for this
                    ' ??????? Finnes disse "chargeback" i det nye formatet????
                    If xDelim(sImportLineBuffer, ";", 4, Chr(34)) = "CHARGEBACK" Then
                        sImportLineBuffer = ReadGenericRecord(oFile) 'Read the actual chargeback
                    Else
                        sImportLineBuffer = ReadGenericRecord(oFile)
                        'Exit Do
                    End If
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadElavon_2017 = sImportLineBuffer

    End Function
    Friend Function ReadDatabase(ByRef oMyDal As vbBabel.DAL, ByRef sMySQL As String) As Boolean
        Dim oPayment As Payment
        Dim bAbandon As Boolean
        Dim bOutOfLoop As Boolean
        Dim bReadLine As Boolean
        Dim bCreateObject As Boolean
        Dim bx As Boolean
        Dim bReturnValue As Boolean
        Dim bInsideThisFunction As Boolean = True
        'XNET 21.11.2014 - Added several new variables
        Dim oLocalClient As vbBabel.Client
        Dim oLocalAccount As vbBabel.Account
        Dim sOldAccount As String
        Dim sBIC As String
        Dim sBANK_AccountCountryCode As String
        Dim bMerge As Boolean
        Dim sPaymentDate As String
        Dim sDateFormat As String
        Dim dDate_Payment As Date

        'Try '- 02.02.2021 - Removed ErrorHandling because it destoys the info created in payment

        sFormatType = "Database"
        bAbandon = False

        ' do not advance to next record in oMyDal first time we reach here -
        ' because of this we must call payment here for first record
        If InStr(1, sMySQL, "BBRET_CompanyNo", CompareMethod.Text) > 0 Then
            sI_EnterpriseNo = oMyDal.Reader_GetString("BBRET_CompanyNo")
        End If
        oPayment = oPayments.Add()
        oPayment.objFile = oFile
        oPayment.VB_Profile = oProfile

        oPayment.ImportFormat = iImportFormat
        oPayment.StatusCode = sStatusCode
        oPayment.Cargo = oCargo
        bInsideThisFunction = False
        bReturnValue = oPayment.ReadDatabase(oMyDal, sMySQL)
        bInsideThisFunction = True

        ' then rest of records
        'If oMyDal.Reader_Execute() Then
        'If oMyDal.Reader_HasRows Then
        Do While oMyDal.Reader_ReadRecord

            'If InStr(1, sMySQL, "BBRET_CompanyNo", CompareMethod.Text) > 0 Then
            '    sI_EnterpriseNo = oMyDal.Reader_GetString("BBRET_CompanyNo")
            'End If
            bReadLine = False
            bCreateObject = True

            If bCreateObject Then

                ' 13.03.2015
                ' added functionality to Merge payments
                ' In SQL, have to set , 'True' AS BBRET_Merge
                If InStr(sMySQL, "BBRET_Merge", CompareMethod.Binary) Then
                    If oMyDal.Reader_GetString("BBRET_Merge") = "True" Then
                        bMerge = True
                    End If
                End If
                ' 20.03.2015 Ta h�yde for b�de BBRET_Merge og BBRET_MERGE
                If InStr(sMySQL, "BBRET_MERGE", CompareMethod.Binary) Then
                    If oMyDal.Reader_GetString("BBRET_MERGE") = "True" Then
                        bMerge = True
                    End If
                End If

                ' Criterias for merging;
                ' I_Account + E_Account + E_Name + Date_Payment + MON_InvoiceCurrency 
                ' MERK; I case hvor det kreves mer, m� vi legge inn dette i koden lenger ned.
                ' Kan f.ex. gjelde Charges, BIC

                If bMerge Then
                    ' to be able to check if date on last payment is same as date in new record, we have to do some testing, same as in Payment
                    If InStr(1, sMySQL, "BBRET_PaymentDate", CompareMethod.Text) > 0 Then
                        sPaymentDate = oMyDal.Reader_GetString("BBRET_PaymentDate")
                        If Not sPaymentDate = "" Then
                            If InStr(1, sMySQL, "BBRET_DateFormat", CompareMethod.Text) > 0 Then
                                sDateFormat = oMyDal.Reader_GetString("BBRET_DateFormat")
                            End If
                            If Not EmptyString(sDateFormat) Then
                                dDate_Payment = StringToDateConversion(sPaymentDate, sDateFormat)
                            Else
                                If Mid$(sPaymentDate, 3, 1) = "." And Mid$(sPaymentDate, 6, 1) = "." Then
                                    ' type dato 21.06.2021
                                    sPaymentDate = Replace(Replace(sPaymentDate, "/", vbNullString), ".", vbNullString)
                                    dDate_Payment = DateSerial(Mid(sPaymentDate, 5, 4), Mid(sPaymentDate, 3, 2), Left(sPaymentDate, 2))
                                    ' 30.06.2021 - added next 3 types for dates
                                ElseIf Mid$(sPaymentDate, 3, 1) = "-" And Mid$(sPaymentDate, 6, 1) = "-" Then
                                    ' type dato 21-06-2021
                                    sPaymentDate = Replace(Replace(sPaymentDate, "/", vbNullString), "-", vbNullString)
                                    dDate_Payment = DateSerial(Mid(sPaymentDate, 5, 4), Mid(sPaymentDate, 3, 2), Left(sPaymentDate, 2))
                                ElseIf Mid$(sPaymentDate, 5, 1) = "." And Mid$(sPaymentDate, 8, 1) = "." Then
                                    ' type dato 2021.06.21
                                    sPaymentDate = Replace(Replace(sPaymentDate, "/", vbNullString), ".", vbNullString)
                                    dDate_Payment = DateSerial(Left(sPaymentDate, 4), Mid(sPaymentDate, 5, 2), Mid(sPaymentDate, 7, 2))
                                ElseIf Mid$(sPaymentDate, 5, 1) = "-" And Mid$(sPaymentDate, 8, 1) = "-" Then
                                    ' type dato 2021-06-21
                                    sPaymentDate = Replace(Replace(sPaymentDate, "/", vbNullString), "-", vbNullString)
                                    dDate_Payment = DateSerial(Left(sPaymentDate, 4), Mid(sPaymentDate, 5, 2), Mid(sPaymentDate, 7, 2))

                                Else
                                    sPaymentDate = Replace(Replace(sPaymentDate, "/", vbNullString), ".", vbNullString)
                                    dDate_Payment = DateSerial(CInt(Left(sPaymentDate, 4)), CInt(Mid(sPaymentDate, 5, 2)), CInt(Right(sPaymentDate, 2)))

                                End If

                            End If
                        End If
                        If dDate_Payment < Now Then
                            dDate_Payment = DateSerial(Year(Now), Month(Now), VB.Day(Now))
                        End If


                    End If
                End If

                ' added 23.03.2015, changed 30.03.2015
                '--------------------------------------------------
                ' In this section we can add special mergesettings
                '--------------------------------------------------
                If oCargo.Special = "AEROGULF" Then
                    '--- m� teste med data fra Aerogulf.
                    '--- husk at Order by m� avsluttes med ,comments
                    If bMerge Then
                        ' consider also KID when merging;
                        ' Compare to last Invoice in payment !
                        If Len(oPayment.Invoices(oPayment.Invoices.Count).Unique_Id) = 0 And Len(Trim(oMyDal.Reader_GetString("BBRET_UniqueID"))) = 0 Then
                            ' Freetext on this one, and on last one
                            bMerge = True
                        ElseIf Len(oPayment.Invoices(oPayment.Invoices.Count).Unique_Id) > 0 And Len(Trim(oMyDal.Reader_GetString("BBRET_UniqueID"))) > 0 Then
                            ' KID on this one, and on last one
                            bMerge = True
                        Else
                            ' mix of KID and freetext, no merge
                            bMerge = False
                        End If
                    End If
                End If
                '--------------------------------------------------



                If bMerge And oPayment.I_Account = Trim(oMyDal.Reader_GetString("BBRET_I_Account")) And oPayment.E_Account = Trim(oMyDal.Reader_GetString("BBRET_E_Account")) And StringToDate(oPayment.DATE_Payment) = dDate_Payment And oPayment.MON_InvoiceCurrency = oMyDal.Reader_GetString("BBRET_Currency") Then
                    ' add to existing payment

                Else
                    ' no merging, add new payment
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                End If

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.Cargo = oCargo
                bInsideThisFunction = False
                bReturnValue = oPayment.ReadDatabase(oMyDal, sMySQL)
                bInsideThisFunction = True
            End If

        Loop

        'XNET 21.11.2014
        'The old test to find PayType created an error for Ulefos Finland

        If bProfileInUse Then
            'Find debitaccounts bic, to find "debit-country"
            sOldAccount = ""
            sBIC = ""
            For Each oPayment In Payments
                If oPayment.I_Account <> sOldAccount Then
                    For Each oLocalClient In oProfile.FileSetups(oPayment.VB_FilenameOut_ID).Clients
                        For Each oLocalAccount In oLocalClient.Accounts
                            If Trim(oLocalAccount.Account) = oPayment.I_Account Or Trim(oLocalAccount.ConvertedAccount) = oPayment.I_Account Then
                                sBIC = Trim(oLocalAccount.SWIFTAddress)
                                If EmptyString(oLocalAccount.DebitAccountCountryCode) Then
                                    sBANK_AccountCountryCode = Mid$(sBIC, 5, 2)
                                Else
                                    sBANK_AccountCountryCode = Trim(oLocalAccount.DebitAccountCountryCode)
                                End If
                                oCargo.BANK_I_SWIFTCode = sBIC
                                Exit For
                            End If
                        Next oLocalAccount
                    Next oLocalClient
                    sOldAccount = oPayment.I_Account
                End If

                If Not EmptyString(sBIC) Then
                    If IsPaymentDomestic(oPayment, sBIC, False, True, True) Then
                        oPayment.PayType = "D"
                    Else
                        oPayment.PayType = "I"
                    End If
                Else
                    ' No BIC setup for debitaccount
                    If oPayment.MON_InvoiceCurrency <> "NOK" Or (oPayment.BANK_CountryCode <> "NO" And oPayment.BANK_CountryCode <> "") Or oPayment.Priority = True Then
                        oPayment.PayType = "I"
                    Else
                        oPayment.PayType = "D"
                    End If
                End If
            Next oPayment

        End If

        If UCase(oCargo.Special) = "ULEFOS_NV" Then
            For Each oPayment In Payments
                If oPayment.MON_InvoiceCurrency = "EUR" Then
                    oPayment.PayType = "D"
                End If
            Next oPayment
        End If

        bx = AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        'Catch ex As Exception

        '    MsgBox(ex.InnerException)

        '    bReturnValue = False
        '    If bInsideThisFunction Then
        '        Throw New Exception("Function: ReadDatabase_Batch" & vbCrLf & ex.Message)
        '    Else
        '        Throw New Exception(ex.Message)
        '    End If

        'End Try

        ReadDatabase = bReturnValue
    End Function
    Private Function ReadVippsXLS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim bOutOfLoop As Boolean
        Dim bCreatePayment As Boolean
        Dim sTmp As String

        Try

            bFromLevelUnder = False

            sFormatType = "VippsXLS"
            sREF_Bank = "DNB Vipps"

            Do While True


                bCreatePayment = True  ' added 23.04.2015

                ' for all other, create payment
                If bCreatePayment Then
                    oPayment = oPayments.Add()
                End If
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If

                ' 13.11.2017
                ' Create new batch pr salesdate
                sTmp = Trim(xDelim(sImportLineBuffer, ";", 1))
                If oPayment.DATE_Payment <> Right(sTmp, 4) & Mid(sTmp, 4, 2) & Left(sTmp, 2) Then   ' A  20171001
                    Exit Do
                End If
            Loop

            bx = AddInvoiceAmountOnBatch()
            bx = AddTransferredAmountOnBatch()

            ReadVippsXLS = sImportLineBuffer

        Catch ex As Exception
            Throw New Exception("(BB)Source: ReadExcelXLS" & vbCrLf & ex.Message)

        End Try

    End Function
    Private Function ReadExcel_IncomingXLS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim bOutOfLoop As Boolean
        Dim bCreatePayment As Boolean
        Dim sTmp As String
        Dim lNoOfColumns As Double
        Dim i As Integer
        Dim sGroupingInfo As String = ""

        Try

            sGroupingInfo = ""
            bFromLevelUnder = False

            sFormatType = "Excel_IncomingXLS"

            Do While True

                Select Case oCargo.Special
                    Case "BOOTS_OCR"
                        lNoOfColumns = 2
                        bCreatePayment = True
                        ' ref_own used as avtaleid in OCR 20-record
                        sREF_Own = "123456789"

                    Case "SISMO"
                        lNoOfColumns = 26
                        If Not xDelim(sImportLineBuffer, ";", 2).Trim = "*" Then   ' B 

                        Else
                            For i = 1 To 100
                                sImportLineBuffer = ReadLineFromExcel(oXLWSheet, lNoOfColumns)
                                If Not EmptyString(xDelim(sImportLineBuffer, ";", 8)) Then
                                    If Not xDelim(sImportLineBuffer, ";", 2).Trim = "*" Then   ' B 
                                        Exit For
                                    End If
                                End If
                            Next i

                            If i > 99 Then
                                Exit Do
                            End If

                        End If

                        If sImportLineBuffer.Length = 0 Or sImportLineBuffer = "ERROR" Then
                            Exit Do
                        End If

                        sTmp = xDelim(sImportLineBuffer, ";", 16).Trim   'P  Arkivref
                        If Not EmptyString(sTmp) Then
                            sREF_Bank = xDelim(sTmp, "/", 1)
                        End If
                        bCreatePayment = True  ' added 23.04.2015

                    Case "HELSE_FONNA_NETS"
                        lNoOfColumns = 27

                        If sImportLineBuffer.Length = 0 Or sImportLineBuffer = "ERROR" Then
                            Exit Do
                        End If

                        If sGroupingInfo <> xDelim(sImportLineBuffer, ";", 5).Trim Then
                            If EmptyString(sGroupingInfo) Then
                                sGroupingInfo = xDelim(sImportLineBuffer, ";", 5).Trim
                            Else
                                If Not RunTime() Then
                                    'For testing 
                                Else
                                    Exit Do 'Create a new batch
                                End If
                            End If
                        End If

                        sTmp = xDelim(sImportLineBuffer, ";", 5).Trim.Replace(".", "")   'E  Valuteringsdato
                        If Not EmptyString(sTmp) Then
                            sREF_Bank = xDelim(sTmp, "/", 1)
                        End If
                        bCreatePayment = True  ' added 23.04.2015

                    Case "HELSE_STAVANGER_BAX"
                        lNoOfColumns = 27
                        bCreatePayment = True

                    Case "VEGFINANS_EXCELTOCREMUL"
                        lNoOfColumns = 4
                        bCreatePayment = True

                    Case "SGF_EQ"
                        lNoOfColumns = 9
                        bCreatePayment = True

                End Select
                ' for all other, create payment
                If bCreatePayment Then
                    oPayment = oPayments.Add()
                End If
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oCargo.Special = "SISMO" Then
                    Exit Do
                End If

                If sImportLineBuffer.Length = 0 Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If

            Loop

            bx = AddInvoiceAmountOnBatch()
            bx = AddTransferredAmountOnBatch()

            ReadExcel_IncomingXLS = sImportLineBuffer

        Catch ex As Exception
            Throw New Exception("(BB)Source: ReadExcelIncoming_XLS" & vbCrLf & ex.Message)

        End Try

    End Function
    Private Function ReadExcelXLS() As String
        ' Common importroutine for several Excel-files in .XLS-format
        ' Special in setup decides format (oBabelfile.Special)
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim sResponse As String
        Dim iResponse As Short
        Dim bAbandon As Boolean
        Dim bOutOfLoop As Boolean
        Dim sOldCountryCode As String, sSwiftCode As String
        Dim sPaymentCurrency As String
        Dim sI_Account As String
        Dim sDay As String, sMonth As String, sYear As String
        Dim dPaymentDate As Date
        Dim oFs As New Scripting.FileSystemObject
        'Dim xmlSWIFTCodesDoc As New MSXML2.DOMDocument40
        'Dim eleCountryNode As MSXML2.IXMLDOMElement
        Dim xmlSWIFTCodes As System.Xml.XmlDocument
        Dim nodeCountryNode As System.Xml.XmlNode

        ' XNET 15.05.2012
        Static sOld_Date As String
        Static sOld_Account As String
        Dim bCreatePayment As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oAccount As vbBabel.Account
        Dim bAccountFound As Boolean
        Dim sTmp As String = ""

        Try

            bFromLevelUnder = False

            sFormatType = "ExcelXLS"
            bAbandon = False

            Do While True

                ' Special handling for some Excel-sheets:
                '----------------------------------------
                Select Case UCase(oCargo.Special)
                    Case "VEGFINANS"
                        sI_EnterpriseNo = xDelim(sImportLineBuffer, ";", 6)   'F Org.no
                        sI_Branch = xDelim(sImportLineBuffer, ";", 5)   ' E Divisjon

                    Case "DNBNOR VERDIPAPIR"

                        If Not bFromLevelUnder Then
                            sResponse = vbNullString
                            Do While True
                                sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                                If Len(sResponse) = 0 Then
                                    bAbandon = True
                                    Exit Do
                                ElseIf CheckAccountNoNOR(Trim$(sResponse)) Then
                                    sI_Account = Trim$(sResponse)
                                    Exit Do
                                Else
                                    iResponse = MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "�nsker du � avbryte?", vbYesNo, "Feil kontonummer")
                                    If iResponse = vbYes Then
                                        bAbandon = True
                                        Exit Do
                                    Else
                                        'Continue
                                    End If
                                End If
                            Loop

                            If bAbandon Then
                                Exit Do
                            End If

                            bOutOfLoop = False
                            ' Ask for paymentdate:
                            Do While bOutOfLoop = False

                                bOutOfLoop = True
                                sResponse = InputBox("Legg inn betalingsdato (dd.mm.yyyy)", "Utbetalingsdato", Trim(Str(DatePart(DateInterval.Day, Now))) + "." + Trim(Str(DatePart(DateInterval.Month, Now))) + "." + Trim(Str(DatePart(DateInterval.Year, Now))))
                                If sResponse = vbNullString Then
                                    ' user pressed Cancel
                                    bOutOfLoop = False
                                    Exit Do
                                End If

                                If Len(sResponse) - Len(Replace(sResponse, ".", vbNullString)) = 2 Then
                                    ' OK, two . in datestring:
                                    ' Test date given;
                                    sDay = Left$(sResponse, InStr(sResponse, ".") - 1)
                                    sMonth = Mid$(sResponse, InStr(sResponse, ".") + 1, 2)
                                    If Right$(sMonth, 1) = "." Then
                                        ' only one digit for month
                                        sMonth = Left$(sMonth, 1)
                                    End If
                                    sYear = Right$(sResponse, 4)
                                    If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                                        MsgBox("Feil i �rstall - Pr�v igjen!")
                                        bOutOfLoop = False
                                    End If
                                    If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                                        MsgBox("Feil i m�ned - Pr�v igjen!")
                                        bOutOfLoop = False
                                    End If
                                    If Val(sDay) < 1 Or Val(sDay) > 31 Then
                                        MsgBox("Feil i dag - Pr�v igjen!")
                                        bOutOfLoop = False
                                    End If
                                    If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                                        If Val(sDay) > 30 Then
                                            MsgBox("Feil i dag - Pr�v igjen!")
                                            bOutOfLoop = False
                                        End If
                                    End If
                                    If Val(sMonth) = 2 Then
                                        If Val(sDay) > 29 Then
                                            MsgBox("Feil i dag - Pr�v igjen!")
                                            bOutOfLoop = False
                                        End If
                                    End If
                                Else
                                    MsgBox("Feil i dato!" + vbCrLf + "Skal ha formen dd.mm.yyyy" + _
                                    vbCrLf + "Husk punktum som skilletegn!")
                                    bOutOfLoop = False
                                End If
                            Loop
                            If sResponse = vbNullString Then
                                bOutOfLoop = True
                            Else
                                dPaymentDate = DateSerial(Int(sYear), Int(sMonth), Int(sDay))
                            End If

                            Do While True
                                sResponse = vbNullString
                                sResponse = InputBox("Legg inn melding til mottaker.", "Melding", sResponse)
                                If Len(sResponse) > 0 Then
                                    iResponse = MsgBox("F�lgende melding blir lagt inn: " & vbCrLf & vbCrLf & sResponse & vbCrLf & "�nsker du � fortsette?", vbYesNo, "Melding OK?")
                                    If iResponse = vbYes Then
                                        Exit Do
                                    Else
                                        'Continue
                                    End If
                                Else
                                    iResponse = MsgBox("Det m� angis en melding til mottaker." & vbCrLf & "�nsker du � avbryte?", vbYesNo, "Feil kontonummer")
                                    If iResponse = vbYes Then
                                        bAbandon = True
                                        Exit Do
                                    Else
                                        'Continue
                                    End If
                                End If
                            Loop

                            If bAbandon Then
                                Exit Do
                            Else
                                oCargo.FreeText = sResponse
                            End If

                            ' Remove headerlines;
                            If Left$(sImportLineBuffer, 14) = "Navn;Adr1;Adr2" Then
                                ' Read another line
                                If oFile.AtEndOfStream = False Then
                                    sImportLineBuffer = ReadGenericRecord(oFile)
                                Else
                                    Exit Do
                                End If
                            End If

                            'Load SWIFTCodes from XML-document
                            If oFs.FileExists(My.Application.Info.DirectoryPath & "\DnBNOR SWIFTCodes.xml") Then
                                'xmlSWIFTCodesDoc.load(My.Application.Info.DirectoryPath & "\DnBNOR SWIFTCodes.xml")
                                'New 04.05.2022 - Removed use of MSXML
                                xmlSWIFTCodes = New System.Xml.XmlDocument
                                xmlSWIFTCodes.Load(My.Application.Info.DirectoryPath & "\DnBNOR SWIFTCodes.xml")
                            Else
                                Err.Raise(10000, "ReadExcelFile", "Finner ikke filen DnBNOR SWIFTCodes.xml" & vbCrLf & vbCrLf & "Program avbrytes!")
                            End If
                            sOldCountryCode = vbNullString
                            sSwiftCode = vbNullString
                        Else
                            bx = False
                            Do Until Trim$(xDelim(sImportLineBuffer, ";", 6)) <> vbNullString
                                If oFile.AtEndOfStream = True Then
                                    bx = True
                                    Exit Do
                                End If
                                sImportLineBuffer = ReadGenericRecord(oFile)
                            Loop
                            If bx Then
                                Exit Do
                            End If
                            'Nothing to do


                        End If
                        ' --- End DNBNOR VERDIPAPIR ---

                    Case "CUBERA"
                        ' XNET 01.11.2011
                        Dim sThisPayType As String

                        If Not EmptyString(Trim$(xDelim(sImportLineBuffer, ";", 2))) Then  ' Swift
                            ' create new batch between Domestic and Int if mix
                            sThisPayType = "I"
                        Else
                            sThisPayType = "D"
                        End If
                        If oCargo.PayType <> "" And sThisPayType <> oCargo.PayType Then
                            ' jump to BabelFile to create new batch
                            ReadExcelXLS = sImportLineBuffer
                            oCargo.PayType = sThisPayType
                            Exit Do
                        End If
                        oCargo.PayType = sThisPayType

                        ' XNET 16.05.2012 Added HB_OCR
                        '----------------------------------------------
                    Case "HB_OCR"
                        ' Spesialoppdrag for Handelsbanken, lag OCR-fil fra Excel
                        '----------------------------------------------
                        ' Must create new batch if changes in accountno or date
                        sOld_Account = Replace(Trim$(xDelim(sImportLineBuffer, ";", 1)), " ", "") ' A Til konto (husk: dette er OCR !)
                        sOld_Date = xDelim(sImportLineBuffer, ";", 3) ' C Dato

                    Case "TOBB ENGANG"  ' Trondheim og Omegn Bolugbyggelag
                        If bFromLevelUnder Then
                            'sI_EnterpriseNo = Trim(xDelim(sImportLineBuffer, ";", 8))
                            Exit Do
                        End If

                    Case "HANDELSBANKEN_ODIN"  ' Handelsbanken for Odin, engangs
                        'Added 23.06.2015
                        If bFromLevelUnder Then
                            If sI_EnterpriseNo = Trim(xDelim(sImportLineBuffer, ";", 2)) Then
                                'OK, no new batch
                            Else
                                ReadExcelXLS = sImportLineBuffer
                                Exit Do
                            End If
                        Else
                            sI_EnterpriseNo = Trim(xDelim(sImportLineBuffer, ";", 2))
                        End If

                    Case "LOOMIS UTBETALING"
                        If xDelim(sImportLineBuffer, ";", 8).Trim = "0" Or xDelim(sImportLineBuffer, ";", 8).Trim = "" Then
                            Do While True
                                sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 19)

                                If xDelim(sImportLineBuffer, ";", 8).Trim = "0" Or xDelim(sImportLineBuffer, ";", 8).Trim = "" Then

                                Else
                                    Exit Do

                                End If
                                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                                    Exit Do
                                End If
                            Loop
                            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                                Exit Do
                            End If
                        End If
                        'Check if it is a new client, if so create a new batch
                        If sOld_Account <> Replace(xDelim(sImportLineBuffer, ";", 6).Trim, ".", "") Then 'F
                            sOld_Account = Replace(xDelim(sImportLineBuffer, ";", 6).Trim, ".", "")
                            bAccountFound = False
                            For Each oFilesetup In oProfile.FileSetups
                                For Each oClient In oFilesetup.Clients
                                    For Each oAccount In oClient.Accounts
                                        If oAccount.Account = sOld_Account Then
                                            bAccountFound = True
                                            Exit For
                                        End If
                                    Next oAccount
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oClient
                                If bAccountFound Then
                                    Exit For
                                End If
                            Next oFilesetup

                            If bAccountFound Then
                                If bFromLevelUnder Then

                                    If sI_EnterpriseNo = oClient.CompNo Then
                                        'OK, no new batch
                                    Else
                                        ReadExcelXLS = sImportLineBuffer
                                        Exit Do
                                    End If
                                Else
                                    sI_EnterpriseNo = oClient.CompNo
                                End If
                            Else
                                Throw New Exception("Kontonummer " & sOld_Account & " ikke funnet i BabelBank sitt oppsett.")
                            End If

                        End If

                    Case "BOOTS"
                        'Added 21.11.2016
                        'If bFromLevelUnder Then
                        'Exit Do
                        'End If
                    Case "SCANDZA"
                        ' hvis mangler bel�p eller konto, les ny linje
                        'Do While xDelim(sImportLineBuffer, ";", 4) = vbNullString Or xDelim(sImportLineBuffer, ";", 5) = vbNullString
                        '    ' Must have accountno AND amount (col D and E)
                        '    ' Count number of lines where info is missing
                        '    sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 5)
                        'Loop
                    Case "PBU"
                        'Added 28.10.2020
                        'Nothing to do here

                    Case "HANDELSBANKEN_SKAGEN"
                        sI_EnterpriseNo = Replace(Trim(xDelim(sImportLineBuffer, ";", 2)), Chr(160), "")  'B
                        oCargo.Temp = Replace(Trim(xDelim(sImportLineBuffer, ";", 2)), Chr(160), "")  ' Sammenligne neste med oCargo.Temp

                    Case "GF DANSK FILIAL"
                        'Added 20.01.2021
                        'Nothing to do

                End Select

                ' Special handling completed, rest is general
                '--------------------------------------------
                bCreatePayment = True  ' added 23.04.2015

                ' 23.04.2015 - special handling for ACTIC - do not create new payment if same receiver 
                If oCargo.Special = "ACTIC" Then
                    If oPayments.Count > 0 Then
                        If InStr(Trim(xDelim(sImportLineBuffer, ";", 1)), oPayments(oPayments.Count).E_Account) > 0 And Trim(xDelim(sImportLineBuffer, ";", 3)) = oPayments(oPayments.Count).E_Name Then
                            ' stick to same payment, create new invoices
                            bCreatePayment = False
                            oPayment = oPayments(oPayments.Count)
                        End If
                    End If
                End If

                If oCargo.Special = "TROMS_OFFSHORE" Then
                    If oPayments.Count > 0 Then
                        If InStr(Trim(Mid(xDelim(sImportLineBuffer, ";", 59), 5)), oPayments(oPayments.Count).E_Account) > 0 And Trim(xDelim(sImportLineBuffer, ";", 9)) = oPayments(oPayments.Count).E_Name And Trim(Mid(xDelim(sImportLineBuffer, ";", 63), 5)) = oPayments(oPayments.Count).I_Account Then
                            ' stick to same payment, create new invoices
                            bCreatePayment = False
                            oPayment = oPayments(oPayments.Count)
                        End If
                    End If
                End If

                If oCargo.Special = "TROMS_OFFSHORE_UTLAND" Then
                    If oPayments.Count > 0 Then
                        If Trim(xDelim(sImportLineBuffer, ";", 59)) = oPayments(oPayments.Count).E_Account And Trim(xDelim(sImportLineBuffer, ";", 9)) = oPayments(oPayments.Count).E_Name And Trim(Mid(xDelim(sImportLineBuffer, ";", 63), 5)) = oPayments(oPayments.Count).I_Account And Trim(xDelim(sImportLineBuffer, ";", 41)) = oPayments(oPayments.Count).MON_InvoiceCurrency Then
                            ' stick to same payment, create new invoices
                            bCreatePayment = False
                            oPayment = oPayments(oPayments.Count)
                        End If
                    End If
                End If

                ' 21.11.2016 - special handling for Boots - do not create new payment if same account, date and currency 
                If oCargo.Special = "BOOTS" Then
                    If oPayments.Count > 0 Then
                        If InStr(xDelim(sImportLineBuffer, ";", 15).Trim, oPayments(oPayments.Count).E_Account) > 0 Then
                            'Account is identical

                            '06.12.2016 - DATE_Payment is always identical because it is set through AskForDate
                            'sTmp = xDelim(sImportLineBuffer, ";", 5).Trim
                            'If sTmp.Length <> 10 Then
                            '    Err.Raise(23017, "ReadExcel_Batch", "Feil format p� dato - Endre i Excel og pr�v igjen!" & vbCrLf & sImportLineBuffer)
                            '    'sTmp = Format(Now(), "dd.MM.yyyy")
                            'End If
                            'sDay = sTmp.Substring(0, 2)
                            'sMonth = sTmp.Substring(3, 2)
                            'sYear = sTmp.Substring(6)
                            'If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                            '    Err.Raise(23017, "ReadExcel_Batch", "Feil i �rstall - Endre i Excel og pr�v igjen!" & vbCrLf & sImportLineBuffer)
                            'End If
                            'If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                            '    Err.Raise(23017, "ReadExcel_Batch", "Feil i m�ned - Endre i Excel og pr�v igjen!" & vbCrLf & sImportLineBuffer)
                            'End If
                            'If Val(sDay) < 1 Or Val(sDay) > 31 Then
                            '    Err.Raise(23017, "ReadExcel_Batch", "Feil i dag - Endre i Excel og pr�v igjen!" & vbCrLf & sImportLineBuffer)
                            'End If
                            'sTmp = sYear & sMonth & sDay

                            'If InStr(sTmp, oPayments(oPayments.Count).DATE_Payment) > 0 Then
                            ''Due date is identical
                            If InStr(xDelim(sImportLineBuffer, ";", 9).Trim, oPayments(oPayments.Count).MON_InvoiceCurrency) > 0 Then
                                'Currency is identical, stick to same payment, create new invoices
                                bCreatePayment = False
                                oPayment = oPayments(oPayments.Count)
                            End If
                            'End If
                        End If
                    End If
                End If

                ' for all other, create payment
                If bCreatePayment Then
                    oPayment = oPayments.Add()
                End If
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                If UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
                    oPayment.I_Account = CObj(sI_Account)
                    oPayment.DATE_Payment = DateToString(dPaymentDate)
                End If
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oCargo.Special = "TOBB ENGANG" Then  ' Trondheim og Omegn Bolugbyggelag
                    sI_EnterpriseNo = Trim(xDelim(sImportLineBuffer, ";", 3))
                    If xDelim(sImportLineBuffer, ";", 10).Trim.Length = 0 Then
                        sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 8)
                    Else
                        If oCargo.Temp = "INVOICE2" Then
                            sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 11)
                            oCargo.Temp = "INVOICE1"
                        Else
                            If CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 8), "", ".,")) = 0 Then
                                sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 11)
                            Else
                                oCargo.Temp = "INVOICE2"
                            End If
                        End If
                    End If
                End If

                ' XNET 16.05.2012 Added HB_OCR
                '----------------------------------------------
                If UCase(oCargo.Special) = "HB_OCR" Then
                    ' Spesialoppdrag for Handelsbanken, lag OCR-fil fra Excel
                    '----------------------------------------------
                    ' Set a running number into REF_Own to be used in OCR-export
                    oPayment.REF_Own = Trim$(Str(oPayments.Count))
                    ' Must create new batch if changes in accountno or date
                    If Replace(Trim$(xDelim(sImportLineBuffer, ";", 1)), " ", "") <> sOld_Account Then
                        sOld_Account = Replace(Trim$(xDelim(sImportLineBuffer, ";", 1)), " ", "") ' A Til konto (husk: dette er OCR !)
                        ReadExcelXLS = sImportLineBuffer
                        Exit Function ' to create new batch
                    End If
                    If xDelim(sImportLineBuffer, ";", 3) <> sOld_Date Then
                        sOld_Date = xDelim(sImportLineBuffer, ";", 3) ' C Dato
                        ReadExcelXLS = sImportLineBuffer
                        Exit Function ' to create new batch
                    End If
                    sOld_Account = Replace(Trim$(xDelim(sImportLineBuffer, ";", 1)), " ", "") ' A Til konto (husk: dette er OCR !)
                    sOld_Date = xDelim(sImportLineBuffer, ";", 3) ' C Dato
                End If

                If UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
                    'Add SWIFT-adress to the payment
                    If oPayment.PayCode = "160" And oPayment.PayType = "I" Then
                        If sOldCountryCode = oPayment.E_CountryCode Then
                            ' 26.06.2007
                            ' next line commented out, because DnBNOR will not have Swift for check-paymnents
                            'oPayment.BANK_SWIFTCode = sSwiftCode
                            oPayment.MON_TransferCurrency = sPaymentCurrency
                        Else
                            'eleCountryNode = xmlSWIFTCodesDoc.selectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/SWIFT")
                            nodeCountryNode = xmlSWIFTCodes.SelectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/SWIFT")

                            'If eleCountryNode Is Nothing Then
                            '    Err.Raise(10000, "ReadExcelFile", "Finner ikke SWIFTkoden for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            'Else
                            '    sOldCountryCode = oPayment.E_CountryCode
                            '    sSwiftCode = eleCountryNode.nodeTypedValue
                            '    ' 26.06.2007
                            '    ' next line commented out, because DnBNOR will not have Swift for check-paymnents
                            '    'oPayment.BANK_SWIFTCode = sSwiftCode
                            'End If

                            'New 04.05.2022 - Removed use of MSXML
                            If nodeCountryNode Is Nothing Then
                                Err.Raise(10000, "ReadExcelFile", "Finner ikke SWIFTkoden for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            Else
                                sOldCountryCode = oPayment.E_CountryCode
                                sSwiftCode = nodeCountryNode.InnerText
                                ' 26.06.2007
                                ' next line commented out, because DnBNOR will not have Swift for check-paymnents
                                'oPayment.BANK_SWIFTCode = sSwiftCode
                            End If


                            'eleCountryNode = xmlSWIFTCodesDoc.selectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/BETVALUTA")
                            'If eleCountryNode Is Nothing Then
                            '    Err.Raise(10000, "ReadExcelFile", "Finner ikke betalingsvaluta for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            'Else
                            '    sPaymentCurrency = eleCountryNode.nodeTypedValue
                            '    oPayment.MON_TransferCurrency = sPaymentCurrency
                            'End If

                            'New 04.05.2022 - Removed use of MSXML
                            nodeCountryNode = xmlSWIFTCodes.SelectSingleNode("//COUNTRIES/" & oPayment.E_CountryCode & "/BETVALUTA")
                            If nodeCountryNode Is Nothing Then
                                Err.Raise(10000, "ReadExcelFile", "Finner ikke betalingsvaluta for landkode " & oPayment.E_CountryCode & vbCrLf & vbCrLf & "Program avbrytes!")
                            Else
                                sPaymentCurrency = nodeCountryNode.InnerText
                                oPayment.MON_TransferCurrency = sPaymentCurrency
                            End If




                        End If
                    End If
                End If

                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If

                If oCargo.Special = "HANDELSBANKEN_MT940" Then
                    ' last two rows are balance rows
                    If Not EmptyString(xDelim(sImportLineBuffer, ";", 15)) Then   ' 30.11.2018 - endret fra 19
                        ' 20 = saldo dato
                        ' find IB and UB
                        ' First, UB in col 22 (U)
                        nMON_BalanceOUT = ConvertToAmount(xDelim(sImportLineBuffer, ";", 17), "", ",")  ' 30.11.2018 - endret fra 21
                        ' find balance date
                        sTmp = xDelim(sImportLineBuffer, ";", 15)  '' 30.11.2018 - endret fra 19
                        dDATE_BalanceOUT = DateSerial(Right(sTmp, 4), Mid(sTmp, 4, 2), Left(sTmp, 2))

                        sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 23)
                        nMON_BalanceIN = ConvertToAmount(xDelim(sImportLineBuffer, ";", 16), "", ",")   '' 30.11.2018 - endret fra 20                        ' find balance date
                        sTmp = xDelim(sImportLineBuffer, ";", 15)  '' 30.11.2018 - endret fra 19
                        dDATE_BalanceIN = DateSerial(Right(sTmp, 4), Mid(sTmp, 4, 2), Left(sTmp, 2))
                        sImportLineBuffer = ""
                        Exit Do
                    End If
                End If

                If UCase(oCargo.Special) = "HANDELSBANKEN_SKAGEN" Then
                    If oCargo.Temp <> Replace(Trim(xDelim(sImportLineBuffer, ";", 2)), Chr(160), "") Then
                        ' jump out to create new batch for new org.no.
                        Exit Do
                    End If
                End If
            Loop

            If UCase(oCargo.Special) = "DNBNOR VERDIPAPIR" Then
                'xmlSWIFTCodesDoc = Nothing
                'eleCountryNode = Nothing
                'New 04.05.2022 - Removed use of MSXML
                xmlSWIFTCodes = Nothing
                nodeCountryNode = Nothing
            End If

            If Not bAbandon Then
                bx = AddInvoiceAmountOnBatch()
            Else
                Err.Raise(10000, "ReadExcelXLS", "Brukeren har avbrutt prosessen")
            End If

            ReadExcelXLS = sImportLineBuffer

        Catch ex As Exception
            Throw New Exception("(BB)Source: ReadExcelXLS" & vbCrLf & ex.Message)

        End Try

    End Function
    Private Function ReadActavisExcelXLS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean
        Dim sResponse As String, iResponse As Integer
        Dim bAbandon As Boolean
        Dim bOutOfLoop As Boolean
        Dim i As Long
        Dim sTmp As String

        bFromLevelUnder = False

        sFormatType = "ActavisExcelXLS"
        bAbandon = False

        Do While True

            ' XNET 12.12.2011 - Actavis FI has two different formats;
            ' Amount is in column G for both !

            ' 19.02.2009 - do not import 0-amount records
            ' filter them out here;
            ' XNET 25.08.2011
            ' Actavis has changed the format to have room for IBAN/BIC-
            sTmp = xDelim(sImportLineBuffer, ";", 7)
            ' if more than 2 digits, cut
            sTmp = Left$(sTmp, InStr(sTmp, ",") + 2)

            ' XNET 25.08.2011
            ' Actavis has changed the format to have room for IBAN/BIC-
            If ConvertToAmount(sTmp, ".", ",") > 0.01 Then 'G Bel�p

                oPayment = oPayments.Add()
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            Else
                ' read next line
                ' XNET 25.08.2011
                ' Actavis has changed the format to have room for IBAN/BIC-
                sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 17)
                Do While EmptyString(xDelim(sImportLineBuffer, ";", 1)) And EmptyString(xDelim(sImportLineBuffer, ";", 3))
                    ' Count number of empty lines, empty = Empty col A and col C
                    i = i + 1
                    ' XNET 25.08.2011
                    ' Actavis has changed the format to have room for IBAN/BIC-
                    sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 17)
                    If i > 100 Then
                        ' assume end when we have more than 100 empty lines
                        Exit Do
                    End If
                Loop
                If i > 100 Then
                    Exit Do
                End If

            End If

        Loop

        If Not bAbandon Then
            bx = AddInvoiceAmountOnBatch()
        Else
            Err.Raise(10000, "ReadActavisExcelXLS", "User has cancelled process")
        End If


    End Function
    Private Function ReadFirstNordic() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        'Variables used to set properties in the payment-object

        bFromLevelUnder = False

        sFormatType = "FirstNordic"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadFirstNordic = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadNCLFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim lNumberOfPayments As Integer
        Dim nTotalAmountOnBatch, nTotalAmountOnReport As Double
        Dim sTempI_Account, sTempI_Name As String
        Dim sTempDATE_Payment As String
        Dim dTempDATE_Payment As Date
        Dim sMonth, sDay, sYear As String
        Dim iAccountPosition, iDatePosition As Short
        Dim sResponse As String
        Dim iResponse As Short
        Dim bAbandon As Boolean
        'Variables used to set properties in the payment-object

        bFromLevelUnder = False

        sFormatType = "NCL"
        nTotalAmountOnBatch = 0

        If oCargo.Special = "NCPHR114" Then
            'Must be line like - Company  : .........
            sTempI_Name = Trim(Mid(sImportLineBuffer, 11, 30))
            iAccountPosition = InStr(1, sImportLineBuffer, "acct#:", CompareMethod.Text)
            If iAccountPosition = 0 Then
                Err.Raise(10999, "ReadNCLFile", "Unable to find a debit account on the report 'NCPHR114'." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
            Else
                sTempI_Account = Trim(Mid(sImportLineBuffer, iAccountPosition + 6, 13))
                If Not CheckAccountNoNOR(Trim(sTempI_Account)) Then
                    Do While True
                        sResponse = InputBox("Invalid debit account for company: " & sTempI_Name & vbCrLf & "Please enter a valid accountnumber.", "Invalid account", sTempI_Account)
                        If Len(sResponse) = 0 Then
                            bAbandon = True
                            Exit Do
                        ElseIf CheckAccountNoNOR(Trim(sResponse)) Then
                            sTempI_Account = Trim(sResponse)
                            Exit Do
                        Else
                            iResponse = MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "�nsker du � avbryte?", MsgBoxStyle.YesNo, "Feil kontonummer")
                            If iResponse = MsgBoxResult.Yes Then
                                bAbandon = True
                                Exit Do
                            Else
                                sTempI_Account = Trim(sResponse)
                                'Continue
                            End If
                        End If
                    Loop
                End If
            End If
        End If

        Do While True And Not bAbandon

            Select Case oCargo.Special
                Case "AP-PA026"
                    'Nothing to do

                Case "NCPHR114"
                    If Not bFromLevelUnder Then
                        sImportLineBuffer = ReadGenericRecord(oFile, True)
                        If UCase(Left(Trim(sImportLineBuffer), 8)) = "LOCATION" Then
                            'Correct line to find date
                            iDatePosition = InStr(1, sImportLineBuffer, "as of:", CompareMethod.Text)
                            If iDatePosition = 0 Then
                                Err.Raise(10999, "ReadNCLFile", "Unable to find payment date on the report 'NCPHR114'." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                            Else
                                sTempDATE_Payment = Trim(Mid(sImportLineBuffer, iDatePosition + 7, 8))
                                sDay = Mid(sTempDATE_Payment, 4, 2)
                                sMonth = Left(sTempDATE_Payment, 2)
                                sYear = "20" & Right(sTempDATE_Payment, 2)
                                If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                                    Err.Raise(10999, "ReadNCLFile", "Unable to retrieve a correct year from the paymentdate on the report 'NCPHR114'." & vbCrLf & "The paymentdate must be on the format MM/DD/YY." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                                End If
                                If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                                    Err.Raise(10999, "ReadNCLFile", "Unable to retrieve a correct month from the paymentdate on the report 'NCPHR114'." & vbCrLf & "The paymentdate must be on the format MM/DD/YY." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                                End If
                                If Val(sDay) < 1 Or Val(sDay) > 31 Then
                                    Err.Raise(10999, "ReadNCLFile", "Unable to retrieve a correct day from the paymentdate on the report 'NCPHR114'." & vbCrLf & "The paymentdate must be on the format MM/DD/YY." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                                End If
                                If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                                    If Val(sDay) > 30 Then
                                        Err.Raise(10999, "ReadNCLFile", "Unable to retrieve a correct day from the paymentdate on the report 'NCPHR114'." & vbCrLf & "The paymentdate must be on the format MM/DD/YY." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                                    End If
                                End If
                                If Val(sMonth) = 2 Then
                                    If Val(sDay) > 29 Then
                                        Err.Raise(10999, "ReadNCLFile", "Unable to retrieve a correct day from the paymentdate on the report 'NCPHR114'." & vbCrLf & "The paymentdate must be on the format MM/DD/YY." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                                    End If
                                End If
                                dTempDATE_Payment = DateSerial(CInt(sYear), CInt(sMonth), CInt(sDay))
                                'Read the line 'Exchange rate ....'
                                sImportLineBuffer = ReadGenericRecord(oFile, True)
                                'Read the line '..... Name. The header record
                                sImportLineBuffer = ReadGenericRecord(oFile, True)
                            End If
                        Else
                            Err.Raise(10999, "ReadNCLFile", "Unable to find payment date on the report 'NCPHR114'." & vbCrLf & "Last line read:" & vbCrLf & sImportLineBuffer & vbCrLf & "Filename: " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates!")
                        End If
                    Else
                        'Nothing to do, just create a new payment
                    End If

            End Select


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            If oCargo.Special = "NCPHR114" Then
                oPayment.DATE_Payment = DateToString(dTempDATE_Payment)
                oPayment.I_Account = sTempI_Account
            End If
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            nTotalAmountOnBatch = nTotalAmountOnBatch + oPayment.MON_InvoiceAmount
            bFromLevelUnder = True

            ReadNCLFile = sImportLineBuffer

            Select Case oCargo.Special

                Case "AP-PA026"
                    If Left(LTrim(sImportLineBuffer), 21) = "TOTAL NUMBER OF PRINT" Then
                        'End of report. Do some controls
                        lNumberOfPayments = Val(Mid(sImportLineBuffer, 50))
                        If lNumberOfPayments <> Payments.Count Then
                            Err.Raise(10999, "ReadNCLFile", "The number of payments stated differs from the number of payments imported." & vbCrLf & "The number of payments stated:     " & Str(lNumberOfPayments) & vbCrLf & "The number of payments imported: " & Str(Payments.Count) & vbCrLf & "Filename:   " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates")
                        End If
                        Do Until Left(LTrim(sImportLineBuffer), 32) = "TOTAL AMOUNT OF CHECKS IN NATIVE" Or oFile.AtEndOfStream
                            sImportLineBuffer = ReadGenericRecord(oFile, True)
                        Loop
                        nTotalAmountOnReport = CDbl(ConvertToAmount(Trim(Mid(sImportLineBuffer, 50)), ".", ","))
                        If nTotalAmountOnBatch <> nTotalAmountOnReport Then
                            Err.Raise(10999, "ReadNCLFile", "The total amount of payments stated differs from the total amount of payments imported." & vbCrLf & "The total amount stated:     " & ConvertFromAmountToString(nTotalAmountOnReport, ".", ",") & vbCrLf & "The total amount imported: " & ConvertFromAmountToString(nTotalAmountOnBatch, ".", ",") & vbCrLf & "Filename:   " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates")
                        End If

                        Do While True
                            sImportLineBuffer = ReadGenericRecord(oFile, True)
                            If oFile.AtEndOfStream Then Exit Do
                            If InStr(1, sImportLineBuffer, "REPORT ID", CompareMethod.Text) Then
                                'OK We have found a new report
                                If InStr(1, sImportLineBuffer, "AP-PA026", CompareMethod.Text) Then
                                    'Ok, It is a valid report, used for domestic payments
                                    Exit Do
                                End If
                            End If
                        Loop

                        Exit Do

                    End If
                Case "NCPHR114"
                    'Read next line
                    sImportLineBuffer = ReadGenericRecord(oFile, True)
                    Select Case Left(UCase(LTrim(sImportLineBuffer)), 10)

                        Case "TOTALS FOR"
                            'End of report. Do some controls
                            lNumberOfPayments = Val(Mid(sImportLineBuffer, 109))
                            If lNumberOfPayments <> Payments.Count Then
                                Err.Raise(10999, "ReadNCLFile", "The number of payments stated differs from the number of payments imported." & vbCrLf & "The number of payments stated:     " & Str(lNumberOfPayments) & vbCrLf & "The number of payments imported: " & Str(Payments.Count) & vbCrLf & "Filename:   " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates")
                            End If
                            nTotalAmountOnReport = CDbl(ConvertToAmount(Trim(Mid(sImportLineBuffer, 45, 20)), ",", "."))
                            If nTotalAmountOnBatch <> nTotalAmountOnReport Then
                                Err.Raise(10999, "ReadNCLFile", "The total amount of payments stated differs from the total amount of payments imported." & vbCrLf & "The total amount stated:     " & ConvertFromAmountToString(nTotalAmountOnReport, ".", ",") & vbCrLf & "The total amount imported: " & ConvertFromAmountToString(nTotalAmountOnBatch, ".", ",") & vbCrLf & "Filename:   " & oCargo.FilenameIn & vbCrLf & vbCrLf & "BabelBank terminates")
                            End If

                            'Read until next report
                            Do Until InStr(1, sImportLineBuffer, "REPORT ID", CompareMethod.Text) > 0 Or oFile.AtEndOfStream
                                sImportLineBuffer = ReadGenericRecord(oFile, True)
                            Loop

                            Exit Do 'To create a new batch

                        Case Else
                            'Another poor soul getting salary

                    End Select
            End Select

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        If Not bAbandon Then
            nMON_InvoiceAmount = nTotalAmountOnBatch
            ReadNCLFile = sImportLineBuffer
        Else
            Err.Raise(10999, "ReadNCLFile", "The user has cancelled the process.")
        End If

    End Function
    Private Function ReadControlConsultExcelFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim sResponse As String

        bFromLevelUnder = False

        sFormatType = "ControlConsultExcel"
        sResponse = "71770552081"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                    If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop
            End If
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.I_Account = CObj(sResponse)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadControlConsultExcelFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadDnBStockholmExcelFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim sResponse As String

        bFromLevelUnder = False

        sFormatType = "DnBStockholmExcel"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    sResponse = InputBox("Hvilken konto skal det betales FRA ?", "Belastningskonto")
                    If Len(sResponse) > 7 And IsNumeric(CObj(sResponse)) Then
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop
            End If
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.I_Account = CObj(sResponse)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDnBStockholmExcelFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadStrommeSingapore() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean
        Dim sDelimChar As String  'XNET 06.03.2015
        Dim i As Integer

        bFromLevelUnder = False

        sFormatType = "StrommeSingapore"

        Do While True

            ' added next If 18.08.2009
            ' do not import 0-amount records:
            ' xoknet 23.02.2011, added trim, and changed from cDbl to Val
            If Val(Trim$(xDelim(sImportLineBuffer, ";", 3, Chr(34)))) <> 0 Then
                oPayment = oPayments.Add()
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.objFile = oFile 'XokNET - 06.07.2011
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            Else

                'XokNET - 06.07.2011 - Added next IF
                If oCargo.Special = "TAB_SEPARATED" Then
                    sImportLineBuffer = ReadGenericRecord(oFile)
                    sImportLineBuffer = Replace(sImportLineBuffer, Chr(9), ";")
                Else
                    ' XNET 06.05.2014 changed to 14
                    sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 14)
                    If EmptyString(sImportLineBuffer) Then
                        Exit Do
                    End If
                    ' XNET 04.03.2015 - next lines
                    ' remove trailing ;
                    For i = Len(sImportLineBuffer) To 0 Step -1
                        If Mid(sImportLineBuffer, i, 1) = ";" Then
                            sImportLineBuffer = Left(sImportLineBuffer, i - 1)
                        Else
                            Exit For
                        End If
                    Next i

                    ' We are accepting both TAB, ; , as separators in new solution
                    ' TAB is already accepted OK in ReadLineFromExcel
                    ' Semicolon not - will be seen as comma in ReadLineFromExcel
                    ' Same with Comma
                    ' Space sep?
                    ' Mens ren Excel og TAB gir oss ; som skilletegn, og da lar vi det vare v�re som det er
                    If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) > 10 Then
                        sDelimChar = ";"
                    ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ",", "")) > 10 Then
                        sDelimChar = ","
                    ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, " ", "")) > 10 Then
                        'sImportLineBuffer = Replace(sImportLineBuffer, " ", ";")
                        sDelimChar = " "
                    ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, Chr(9), "")) > 10 Then
                        'sImportLineBuffer = Replace(sImportLineBuffer, " ", ";")
                        sDelimChar = Chr(9)
                    End If
                    ' Because we may have spaces or ;  inside an element, we cant just replace space with ;
                    sImportLineBuffer = xDelim(sImportLineBuffer, sDelimChar, 1, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 2, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 3, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 4, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 5, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 6, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 7, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 8, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 9, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 10, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 11, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 12, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 13, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 14, Chr(34))

                End If
            End If

            bFromLevelUnder = True

            ReadStrommeSingapore = sImportLineBuffer

            If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadRECSingapore() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = " RECSingapore"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadRECSingapore = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadTeekaySingapore() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = " TeekaySingapore"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadTeekaySingapore = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadAutocare_Norge() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "Autocare"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadAutocare_Norge = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadTazettAutogiro() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim bAccountFound As String
        Dim sAccountNo As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account



        bFromLevelUnder = False

        sFormatType = "Tazett Autogiro"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            'sI_Account = CVar(xDelim(sImportLineBuffer, ";", 7))  ' Debit Account, own account, kol 7
            'oPayment.I_Account = sI_Account
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True
            If bProfileInUse Then
                sAccountNo = oPayment.I_Account
                bAccountFound = CStr(False)
                ' Fill Avtale_ID into Batch.Ref_Own;
                For Each oFilesetup In oProfile.FileSetups
                    For Each oClient In oFilesetup.Clients
                        For Each oaccount In oClient.Accounts
                            If sAccountNo = oaccount.Account Then
                                sREF_Own = oaccount.ContractNo
                                bAccountFound = CStr(True)
                                Exit For
                            End If
                        Next oaccount
                    Next oClient
                    If CBool(bAccountFound) Then
                        Exit For
                    End If
                Next oFilesetup
            End If

            ReadTazettAutogiro = sImportLineBuffer
            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadISS_SDVFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String
        'Dim sResponse As String

        bFromLevelUnder = False

        sFormatType = "ISS_SDV"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            If sImportLineBuffer <> "ERROR" Then
                sI_Account = CObj(xDelim(sImportLineBuffer, ";", 8, Chr(34)))
                oPayment.I_Account = CObj(sI_Account)
            End If
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            'FIX: What's this??????????
            ReadISS_SDVFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadOriflameExcelFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim sResponse As String
        'Variables for storing date for test
        Dim dPayment_Date As Date
        Dim sMonth, sDay, sYear As String
        Dim sAccount As String
        Dim bOutOfLoop As Boolean


        bFromLevelUnder = False

        sFormatType = "OriflameExcel"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    sResponse = "83980512099"
                    sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                    If sResponse = "" Then
                        ' user pressed Cancel
                        bOutOfLoop = False
                        Exit Do
                    End If
                    sResponse = Replace(sResponse, ".", "") 'Remove any .


                    If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                        sAccount = sResponse
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop

                ' Ask for paymentdate:
                Do While bOutOfLoop = False

                    bOutOfLoop = True
                    sResponse = InputBox("Legg inn betalingsdato (dd.mm.yyyy)", "Utbetalingsdato", Trim(Str(VB.Day(Now))) & "." & Trim(Str(Month(Now))) & "." & Trim(Str(Year(Now))))
                    If sResponse = "" Then
                        ' user pressed Cancel
                        bOutOfLoop = False
                        Exit Do
                    End If

                    If Len(sResponse) - Len(Replace(sResponse, ".", "")) = 2 Then
                        ' OK, two . in datestring:
                        ' Test date given;
                        sDay = Left(sResponse, InStr(sResponse, ".") - 1)
                        sMonth = Mid(sResponse, InStr(sResponse, ".") + 1, 2)
                        If Right(sMonth, 1) = "." Then
                            ' only one digit for month
                            sMonth = Left(sMonth, 1)
                        End If
                        sYear = Right(sResponse, 4)
                        If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                            MsgBox("Feil i �rstall - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                            MsgBox("Feil i m�ned - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sDay) < 1 Or Val(sDay) > 31 Then
                            MsgBox("Feil i dag - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                            If Val(sDay) > 30 Then
                                MsgBox("Feil i dag - Pr�v igjen!")
                                bOutOfLoop = False
                            End If
                        End If
                        If Val(sMonth) = 2 Then
                            If Val(sDay) > 29 Then
                                MsgBox("Feil i dag - Pr�v igjen!")
                                bOutOfLoop = False
                            End If
                        End If
                    Else
                        MsgBox("Feil i dato!" & vbCrLf & "Skal ha formen dd.mm.yyyy" & vbCrLf & "Husk punktum som skilletegn!")
                        bOutOfLoop = False
                    End If
                Loop
                If sResponse = "" Then
                    bOutOfLoop = False
                Else
                    dPayment_Date = DateSerial(Int(CDbl(sYear)), Int(CDbl(sMonth)), Int(CDbl(sDay)))
                End If

            End If

            If bOutOfLoop Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = CObj(sAccount)
                oPayment.DATE_Payment = VB6.Format(dPayment_Date, "YYYYMMDD") 'CVar(dPayment_Date)
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                ReadOriflameExcelFile = sImportLineBuffer

                If oFile.AtEndOfStream = True Then
                    If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                        Exit Do
                    End If
                End If
            Else
                ' user has canceled InputBox
                Exit Do
            End If
            'Exit Do  ' always exit
        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadeCommerce_Logistics() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim sResponse As String
        'Variables for storing date for test
        Dim dPayment_Date As Date
        Dim sMonth, sDay, sYear As String
        Dim sAccount As String
        Dim bOutOfLoop As Boolean
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "eCommerce_Logistics"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    'sResponse = "83980512099"
                    sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                    If sResponse = "" Then
                        ' user pressed Cancel
                        bOutOfLoop = True 'Don't ask about payment_date. Just exit
                        Exit Do
                    End If
                    sResponse = Replace(sResponse, ".", "") 'Remove any .


                    If IsNumeric(CObj(sResponse)) Then
                        sAccount = sResponse
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop

                ' Ask for paymentdate:
                Do While bOutOfLoop = False

                    bOutOfLoop = True
                    sResponse = InputBox("Legg inn betalingsdato (DD.MM.YYYY)", "Utbetalingsdato", VB6.Format(Now, "DD.MM.YYYY"))
                    If sResponse = "" Then
                        ' user pressed Cancel
                        bOutOfLoop = False ' Don't ask about payment-date, just exit!
                        Exit Do
                    End If

                    If Len(sResponse) - Len(Replace(sResponse, ".", "")) = 2 Then
                        ' OK, two . in datestring:
                        ' Test date given;
                        sDay = Left(sResponse, InStr(sResponse, ".") - 1)
                        sMonth = Mid(sResponse, InStr(sResponse, ".") + 1, 2)
                        If Right(sMonth, 1) = "." Then
                            ' only one digit for month
                            sMonth = Left(sMonth, 1)
                        End If
                        sYear = Right(sResponse, 4)
                        If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                            MsgBox("Feil i �rstall - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                            MsgBox("Feil i m�ned - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sDay) < 1 Or Val(sDay) > 31 Then
                            MsgBox("Feil i dag - Pr�v igjen!")
                            bOutOfLoop = False
                        End If
                        If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                            If Val(sDay) > 30 Then
                                MsgBox("Feil i dag - Pr�v igjen!")
                                bOutOfLoop = False
                            End If
                        End If
                        If Val(sMonth) = 2 Then
                            If Val(sDay) > 29 Then
                                MsgBox("Feil i dag - Pr�v igjen!")
                                bOutOfLoop = False
                            End If
                        End If
                    Else
                        MsgBox("Feil i dato!" & vbCrLf & "Skal ha formen dd.mm.yyyy" & vbCrLf & "Husk punktum som skilletegn!")
                        bOutOfLoop = False
                    End If
                Loop
                If sResponse = "" Then
                    bOutOfLoop = False
                Else
                    dPayment_Date = DateSerial(Int(CDbl(sYear)), Int(CDbl(sMonth)), Int(CDbl(sDay)))
                End If

            End If

            If bOutOfLoop Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = CObj(sAccount)
                oPayment.DATE_Payment = VB6.Format(dPayment_Date, "YYYYMMDD")
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                ReadeCommerce_Logistics = sImportLineBuffer
                If sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If

                If oFile.AtEndOfStream = True Then
                    If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                        Exit Do
                    End If
                End If
            Else
                ' user has canceled InputBox
                ReadeCommerce_Logistics = "ERROR"
                Exit Do
            End If
            'Exit Do  ' always exit
        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadDanskeBankTeleService() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "DanskeBankTeleService"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDanskeBankTeleService = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

    End Function
    Private Function ReadDanskeBankLokaleDanske() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "DanskeBankLokaleDanske"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDanskeBankLokaleDanske = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadSydbank() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "Sydbank"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadSydbank = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadCyberCity() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "CyberCity"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadCyberCity = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadNSA() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "NSA"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadNSA = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadNordeaDK_EDI() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "NordeaDK_EDI"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadNordeaDK_EDI = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()


    End Function
    Private Function ReadHandelsbanken_DK_Domestic() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "Handelsbanken_DK_Domestic"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadHandelsbanken_DK_Domestic = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadErhvervsGiro() As String
        ' 11.09.06: Also for PTG (SEB, Torben)
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "ErhvervsGiro"

        Do While True

            If Mid(sImportLineBuffer, 8, 2) = "99" Then
                ' Slutrecord
                nNo_Of_Records = Val(Mid(sImportLineBuffer, 28, 10))
                nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 48, 10))
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 72, 14))

                Exit Do
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadErhvervsGiro = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadBACSforTBI() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sResponse As String
        Dim oClient As Client
        Dim oaccount As Account

        '----------------------------------------------------------------------------------------------
        ' Standard UK Bacs format for TBI

        ' We can accept files both with or without " surrounding each field
        ' OLD
        '"J Major","202020","80273891","ABC1234","104.95","8/10/2003","11112222","Myref1"
        '"P ASHDOWN","304050","40253891","ABC1235","1023.05","8/10/2003","11112222","Myref2"
        '"T BLAIR","405400","10233891","ABC1236","145.95","10/13/2003","11112222","Myref3"

        'A minimum format consists of 5 fields, but we want to be flexible here
        'Field 6 in this format is used to state the 'Processing day
        'Field 7 is payers acc.no


        'Field 8 is payers ref.

        '----------------------------------------------------------------------------------------------

        bFromLevelUnder = False

        sFormatType = "BACS for TBI"

        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadBACSforTBI = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadFirst_Union_ACH() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        Dim bx As Boolean
        Dim sServiceClassCode As String
        Dim lNumberOfEntryDetailsRecords, lCountedNoOfEntryDetilsRecords As Integer
        Dim sEntryHash As String
        Dim nCalculatedBankIdentifications As Double
        Dim nMON_InvoiceDebitAmount As Double
        Dim nMON_InvoiceCreditAmount As Double
        Dim sTemp_Text_E_Statement As String
        Dim sTemp_PayCode As String
        Dim dTempDATE_Payment As Date
        Dim lBatchcounter As Integer

        bFromLevelUnder = False
        lBatchcounter = 0

        nCalculatedBankIdentifications = 0

        Do While True
            bReadLine = False
            bCreateObject = False

            'Check linetype
            Select Case Trim(Mid(sImportLineBuffer, 1, 1))

                '1 - File header
                Case "1"
                    If bFromLevelUnder Then
                        Exit Do
                    Else
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-025")
                    End If

                    '5 - Batch header
                Case "5"
                    sServiceClassCode = Mid(sImportLineBuffer, 2, 3)
                    If sServiceClassCode <> "200" Then
                        If sServiceClassCode <> "200" Then
                            If sServiceClassCode <> "200" Then
                                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10042-025")
                            End If
                        End If
                    End If
                    sI_Branch = Mid(sImportLineBuffer, 5, 16)
                    sTemp_Text_E_Statement = Mid(sImportLineBuffer, 21, 20)
                    sI_EnterpriseNo = Trim(Mid(sImportLineBuffer, 41, 10))
                    If Left(sI_EnterpriseNo, 1) <> "1" Then
                        If Left(sI_EnterpriseNo, 1) <> "3" Then
                            If Left(sI_EnterpriseNo, 1) <> "9" Then
                                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10041-025", Mid(sImportLineBuffer, 14, 10), "Company Identification")
                            End If
                        End If
                    End If
                    sTemp_PayCode = Mid(sImportLineBuffer, 51, 3)
                    'Not set in the object
                    If sTemp_PayCode <> "PPD" Then
                        If sTemp_PayCode <> "CCD" Then
                            Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10043-025", "'PPD'", "'CCD'")
                        End If
                    End If
                    sTemp_Text_E_Statement = Trim(Mid(sImportLineBuffer, 54, 10)) & " " & Trim(Mid(sImportLineBuffer, 64, 6)) & " " & sTemp_Text_E_Statement
                    dTempDATE_Payment = DateSerial(CInt("20" & Mid(sImportLineBuffer, 70, 2)), CInt(Mid(sImportLineBuffer, 72, 2)), CInt(Mid(sImportLineBuffer, 74, 2)))
                    lBatchcounter = lBatchcounter + 1
                    If Val(Mid(sImportLineBuffer, 88, 7)) <> lBatchcounter Then
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10044-025", Mid(sImportLineBuffer, 88, 7), Trim(Str(lBatchcounter)))
                    End If
                    bReadLine = True


                    '6 - Entry detail
                Case "6"
                    'Count entry details + entry details addenda records
                    If Mid(sImportLineBuffer, 79, 1) = "1" Then
                        lCountedNoOfEntryDetilsRecords = lCountedNoOfEntryDetilsRecords + 2
                    Else
                        lCountedNoOfEntryDetilsRecords = lCountedNoOfEntryDetilsRecords + 1
                    End If
                    'Calculate BankIDs
                    nCalculatedBankIdentifications = nCalculatedBankIdentifications + Val(Mid(sImportLineBuffer, 4, 8))
                    bCreateObject = True
                    bReadLine = False


                    '7 - Entry detail addenda
                Case "7"


                    '8 - Batch control record
                Case "8"
                    If sStatusCode = "00" Or StatusCode = "01" Then
                        bx = AddInvoiceAmountOnBatch()
                    Else
                        bx = AddInvoiceAmountOnBatch()
                        bx = AddTransferredAmountOnBatch()
                    End If
                    lNumberOfEntryDetailsRecords = Val(Mid(sImportLineBuffer, 5, 6))
                    If lNumberOfEntryDetailsRecords <> lCountedNoOfEntryDetilsRecords Then
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "11006-025", Trim(Str(lCountedNoOfEntryDetilsRecords)), Trim(Str(lNumberOfEntryDetailsRecords)))
                    End If
                    nNo_Of_Records = lNumberOfEntryDetailsRecords
                    sEntryHash = Mid(sImportLineBuffer, 11, 10)
                    If sEntryHash <> Right(PadLeft(Trim(Str(nCalculatedBankIdentifications)), 10, "0"), 10) Then
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "11007-025", Right(PadLeft(Trim(Str(nCalculatedBankIdentifications)), 10, "0"), 10), sEntryHash)
                    End If
                    'Store the value temporary in sVersion. It will be deleted in BabelFile
                    sVersion = sEntryHash
                    nMON_InvoiceDebitAmount = Val(Mid(sImportLineBuffer, 21, 12))
                    nMON_InvoiceCreditAmount = Val(Mid(sImportLineBuffer, 33, 12))
                    If nMON_InvoiceCreditAmount - nMON_InvoiceDebitAmount <> nMON_InvoiceAmount Then
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10048-025", Trim(Str(nMON_InvoiceCreditAmount)), Trim(Str(nMON_InvoiceDebitAmount - nMON_InvoiceDebitAmount)))
                    End If
                    If sI_EnterpriseNo <> Trim(Mid(sImportLineBuffer, 45, 10)) Then
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "11008-025", Trim(Str(nMON_InvoiceDebitAmount)), Trim(Str(nMON_InvoiceDebitAmount - nMON_InvoiceCreditAmount)))
                    End If
                    If Val(Mid(sImportLineBuffer, 88, 7)) <> lBatchcounter Then
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10044-025", Trim(Str(lBatchcounter)), Mid(sImportLineBuffer, 88, 7))
                    End If
                    bCreateObject = False
                    bReadLine = True

                    '9 - File trailer
                Case "9"
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'All other BETFOR-types gives error
                    'Error # 10010-002
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-025")

                    Exit Do

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadFirst_Union_ACHRecord(oFile) ', oProfile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.Text_E_Statement = sTemp_Text_E_Statement
                oPayment.DATE_Payment = DateToString(dTempDATE_Payment)
                'opayment.PayCode = '

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'Store end of batch data here (if needed)...
        Loop



        ReadFirst_Union_ACH = sImportLineBuffer
    End Function
    Private Function ReadCompuware() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim sResponse As String

        bFromLevelUnder = False

        sFormatType = "Compuware"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    sResponse = "70320597585" ' Default account
                    sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                    If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop
            End If
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.I_Account = CObj(sResponse)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadCompuware = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadISS_Paleggstrekk() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "ISS_Paleggstrekk"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            '    oPayment.I_Account = CVar(sResponse)
            oPayment.ImportFormat = iImportFormat
            'oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadISS_Paleggstrekk = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function Read3M_Reiseregning() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "3M_Reiseregning"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            Read3M_Reiseregning = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadRTVFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim bx, bReadLine As Boolean
        Dim sTempDate As String
        Dim bDateSetInRecordType1 As Boolean
        Dim sMonth, sDay, sYear As String

        bFromLevelUnder = False
        bReadLine = False
        bDateSetInRecordType1 = False

        sFormatType = "RTV"

        Do While True

            Select Case Left(sImportLineBuffer, 1)

                Case "0"
                    If Not bFromLevelUnder Then
                        sTempDate = Mid(sImportLineBuffer, 3, 8)
                        '            sDay = Mid$(sImportLineBuffer, 9, 2)
                        '            sMonth = Mid$(sImportLineBuffer, 7, 2)
                        '            sYear = Mid$(sImportLineBuffer, 3, 4)
                        '            dDate_Payment = DateSerial(sYear, sMonth, sDay)
                        bDateSetInRecordType1 = True
                        bFromLevelUnder = False
                        bReadLine = True
                    Else
                        Err.Raise(27004, "ReadRTVFile", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")
                    End If

                Case "1"
                    If Not bFromLevelUnder Then
                        oPayment = oPayments.Add()
                        oPayment.objFile = oFile
                        oPayment.VB_Profile = oProfile
                        oPayment.ImportFormat = iImportFormat
                        oPayment.StatusCode = sStatusCode
                        If bDateSetInRecordType1 Then
                            oPayment.DATE_Payment = sTempDate
                        End If
                        sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                        bFromLevelUnder = True
                        bReadLine = True
                    Else
                        Err.Raise(27004, "ReadRTVFile", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")
                    End If

                Case "2"
                    Err.Raise(27004, "ReadRTVFile", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")

                Case "8"
                    If bFromLevelUnder Then
                        bReadLine = True
                    Else
                        Err.Raise(27004, "ReadRTVFile", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")
                    End If

                Case "9"
                    If bFromLevelUnder Then
                        Err.Raise(27004, "ReadRTVFile", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")
                    Else
                        Exit Do
                    End If

            End Select

            ReadRTVFile = sImportLineBuffer


            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
                bFromLevelUnder = False
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        ReadRTVFile = sImportLineBuffer

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadRTVFile2() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim bReadLine, bx, bCreateObject As Boolean
        Dim sDatePayment As String
        Dim sYear As String
        Dim lCounter As Integer

        bFromLevelUnder = False
        bReadLine = False

        sFormatType = "RTV2"

        Do While True

            Select Case Mid(sImportLineBuffer, 18, 6)

                Case "0000  ", "000008", "000009", "000010", "000011" 'Start new kommune
                    '        If bFromLevelUnder Then
                    '            Err.Raise 27004, "ReadRTVFile2", "Uventet record i fil." & vbCrLf & _
                    ''            "Linje lest:     " & sImportLineBuffer & vbCrLf & _
                    ''            vbCrLf & "Babelbank avbrytes!", vbOKOnly, "UVENTET RECORD"
                    '        Else
                    If Mid(sImportLineBuffer, 18, 6) = "0000  " Then
                        If Trim(Str(CDbl(VB6.Format(Now, "MM")))) = "1" Then
                            sYear = Trim(Str(Val(VB6.Format(Now, "YYYY")) - 1))
                            sDatePayment = sYear & Mid(sImportLineBuffer, 8, 2) & Mid(sImportLineBuffer, 10, 2)
                        Else
                            sYear = VB6.Format(Now, "YYYY")
                            sDatePayment = sYear & Mid(sImportLineBuffer, 8, 2) & Mid(sImportLineBuffer, 10, 2)
                        End If
                    Else
                        '03.03.2008 - another fileformat differing slightly from the other format SISMO receives
                        sDatePayment = "20" & Mid(sImportLineBuffer, 22, 2) & Mid(sImportLineBuffer, 24, 2) & Mid(sImportLineBuffer, 26, 2)
                    End If
                    bCreateObject = True
                    bReadLine = False
                    '        End If

                Case "0002  ", "0008  " 'End kommune
                    If bFromLevelUnder Then
                        bReadLine = True
                        bCreateObject = False
                    Else
                        Err.Raise(27004, "ReadRTVFile2", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")
                    End If

                Case "0003  " 'End file
                    Exit Do

                Case Else 'New payment
                    Err.Raise(27004, "ReadRTVFile2", "Uventet record i fil." & vbCrLf & "Linje lest:     " & sImportLineBuffer & vbCrLf & vbCrLf & "Babelbank avbrytes!", MsgBoxStyle.OKOnly, "UVENTET RECORD")

            End Select

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
                bFromLevelUnder = False
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        ReadRTVFile2 = sImportLineBuffer

        bx = AddInvoiceAmountOnBatch()

    End Function
    'XokNET - 04.11.2011 - New function
    Private Function ReadRTVFileAltInn() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim bx As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sDatePayment As String
        Dim sYear As String, lCounter As Long
        Dim sTempAmount As String

        bFromLevelUnder = False
        bReadLine = False

        sFormatType = "RTVAltInn"

        Do While True

            Select Case Mid$(sImportLineBuffer, 1, 1)

                Case "E"  'Start new kommune/enhet
                    bCreateObject = True
                    bReadLine = False
                    '        End If

                Case "D" 'End kommune
                    If bFromLevelUnder Then
                        bReadLine = True
                        bCreateObject = False
                    Else
                        Err.Raise(27004, "ReadRTVFileAltInn", "Uventet record i fil." & vbCrLf & _
                        "Linje lest:     " & sImportLineBuffer & vbCrLf & _
                        vbCrLf & "Babelbank avbrytes!", vbOKOnly, "UVENTET RECORD")
                    End If

                Case "A"
                    bReadLine = True
                    bCreateObject = False

                Case "T" 'End file
                    Exit Do

                Case Else 'New payment
                    Err.Raise(27004, "ReadRTVFile2", "Uventet record i fil." & vbCrLf & _
                    "Linje lest:     " & sImportLineBuffer & vbCrLf & _
                    vbCrLf & "Babelbank avbrytes!", vbOKOnly, "UVENTET RECORD")

            End Select

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
                bFromLevelUnder = False
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        ReadRTVFileAltInn = sImportLineBuffer

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadSeaTruck() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "SeaTruck"

        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            'Set oPayment.I_Account = CVar(sResponse)
            ' Remove possible . and -
            oPayment.I_Account = Trim(Replace(Replace(xDelim(sImportLineBuffer, ",", 23, Chr(34)), "-", ""), ".", ""))
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadSeaTruck = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadNordeaUnitel() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "NordeaUnitel"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            ' Combine field 36 (Regnr betaler) og 37 (Kontonr betaler)
            oPayment.I_Account = Trim(xDelim(sImportLineBuffer, ",", 36, Chr(34))) & Trim(xDelim(sImportLineBuffer, ",", 37, Chr(34)))
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadNordeaUnitel = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadShipNet() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "ShipNet"

        Do While True

            sI_EnterpriseNo = Trim(xDelim(sImportLineBuffer, ";", 2))
            sI_Branch = Trim(xDelim(sImportLineBuffer, ";", 3))
            sREF_Own = Trim(xDelim(sImportLineBuffer, ";", 6)) ' Egenref Oppdrag

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat

            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadShipNet = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadCitiDirectUSFlatFile() As String
        Dim oPayment As vbbabel.Payment
        Dim bFromLevelUnder As Boolean
        Dim sPaymentType As String

        bFromLevelUnder = False

        sFormatType = "CitiDirect US Flat File"

        Do While True


            '    sI_EnterpriseNo = Trim$(xDelim(sImportLineBuffer, ";", 2))
            '    sI_Branch = Trim$(xDelim(sImportLineBuffer, ";", 3))
            '    sREF_Own = Trim$(xDelim(sImportLineBuffer, ";", 6)) ' Egenref Oppdrag

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat

            'This format may consist of different payment-types. See Batch
            '�   ACH Credit
            '�   ACH Debit
            '�   ACH - PreFormat
            '�   Book Transfer
            '�   Drawdown Chargewire
            '�   Drawdown Chargewire - PreFormat
            '�   Funds Transfer (FT)
            '�   Book and Funds Transfer - PreFormat

            'Test to find the right formattype
            sPaymentType = ""
            Select Case xDelim(sImportLineBuffer, "#", 3)

                Case "ACH"
                    sPaymentType = "ACH Credit"
                    oPayment.PaymentMethod = BabelFiles.PaymentMethod.ACH_Credit
                Case "ACHI"
                    sPaymentType = "ACH Debit"
                    oPayment.PaymentMethod = BabelFiles.PaymentMethod.ACH_Debit
                Case "BKT"
                    sPaymentType = "Book Transfer"
                    oPayment.PaymentMethod = BabelFiles.PaymentMethod.Book_Transfer
                Case "DDCWI"
                    sPaymentType = "Drawdown Chargewire"
                    oPayment.PaymentMethod = BabelFiles.PaymentMethod.Drawdown_Chargewire
                Case "FT"
                    sPaymentType = "Funds Transfer"
                    oPayment.PaymentMethod = BabelFiles.PaymentMethod.Funds_Transfer

            End Select

            If sPaymentType = "" Then
                If Len(xDelim(sImportLineBuffer, "#", 23)) = 0 Then
                    sPaymentType = "PreFormat Book and Funds"
                    oPayment.PaymentMethod = BabelFiles.PaymentMethod.Book_and_Funds_Transfer_PreFormat
                Else
                    'No way to distinguish between ACH - PreFormat and Drawdown Chargewire - Preformat

                    'Some fields can be distinguished by optional fields
                    sPaymentType = "Unknown"
                End If
            End If

            If oPayment.PaymentMethod <> BabelFiles.PaymentMethod.Book_and_Funds_Transfer_PreFormat Then
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10037-022", sPaymentType)
            End If


            sImportLineBuffer = oPayment.Import(sImportLineBuffer)

            If oFile.AtEndOfStream = True Then
                Exit Do
            Else
                sImportLineBuffer = ReadCitiDirectUSFlatFileRecord(oFile)
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadCitiDirectUSFlatFile = sImportLineBuffer

    End Function

    Private Function Readleverantorsbetalningar() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim dPayment_Date As Date
        Dim sPaymentType As String
        Dim sTmpDate As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            '  04.07.2008
            ' Taken away Select Case, and instead used several IF/ELSEIFs.
            ' This because we had problems to tell if we had a Domestic or International LB-file
            ' F.ex. we could have the start "26" in an Int. file, which was interpreted as a Domestic file,
            ' with recordtype "26"

            'Select Case Left$(sImportLineBuffer, 2)


            '---------------------------------
            ' Leverant�rsbetalningar Innland;
            '---------------------------------
            '''Case "11"  ' �ppningspost (TK11)
            If Left(sImportLineBuffer, 2) = "11" Then
                sPaymentType = "D" 'Domestic
                oCargo.PayType = "D" ' added 04.07.2008

                If bFromLevelUnder Then
                    ' have read a 20-record in batch (new batch)
                    ' jump up, to create object
                    Exit Do
                Else
                    sFormatType = "Leverantorsbetalningar"
                    bReadLine = True
                    bCreateObject = False 'True changed 03.01.06

                    ' added 09.11.2007
                    If Val(Mid(sImportLineBuffer, 47, 1)) > 0 Then
                        sStatusCode = "02"
                        oCargo.StatusCode = "02" 'return
                        oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 41, 6)
                    Else
                        sStatusCode = "00"
                        oCargo.StatusCode = "00"
                        ' added next line 31.05.2018 - needs oCargo.Date_Payment for creditnotes
                        oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 13, 6)
                    End If

                End If
                If UCase(oCargo.Special) = "TRIBEHOTEL" Or UCase(oCargo.Special) = "TRIBE_DK" Then
                    ' 11.07.2008 - for TribeHotels, changed dateimport. Now imports from headerline instead of 14-record
                    sTmpDate = "20" & Mid(sImportLineBuffer, 13, 6)
                    'there are some times problems with Tribes datesetting. Thus test, and use now if far away date.
                    If StringToDate(sTmpDate) > System.DateTime.FromOADate(Date.Today.ToOADate + 60) Or StringToDate(sTmpDate) < Date.Today Then
                        oCargo.DATE_Payment = DateToString(Date.Today)
                    Else
                        oCargo.DATE_Payment = sTmpDate
                    End If
                End If

                'XOKNET 19.08.2013 added next ElseIf
            ElseIf Left$(sImportLineBuffer, 2) = "12" And oCargo.PayType = "D" Then
                ' fast tekst, forel�pig gj�r vi ikke noe her
                bReadLine = True
                bCreateObject = False

                ' New record 03.01.06
                '''Case "13"  ' Rubrikpost (TK13)
            ElseIf Left(sImportLineBuffer, 2) = "13" And oCargo.PayType = "D" Then
                ' General advicetext for all payments
                ' XNET 29.10.2012 - to be able to recreate a correct Lev.bet-fil, we must have this info saved other than as freetext
                ' 27.10.2015 Changed length of importfield
                'sREF_Bank = Trim$(Mid$(sImportLineBuffer, 3, 25))
                'oCargo.FreeText = Trim(Mid(sImportLineBuffer, 3, 25))
                sREF_Bank = Trim$(Mid$(sImportLineBuffer, 3, 77))
                If oCargo.Special <> "JERNIA_LB" Then
                    oCargo.FreeText = Trim(Mid(sImportLineBuffer, 3, 77))
                End If
                bReadLine = True
                bCreateObject = False

                '''Case "14"        'TK14, Betalningsuppdrag
                ElseIf (Left(sImportLineBuffer, 2) = "14" Or Left$(sImportLineBuffer, 2) = "54") And oCargo.PayType = "D" Then
                    ' Sometimes TK14 comes before TK26, or there are not TK26
                    ' Have returned from payment with TK14, create new payment!
                    sPaymentType = "D" 'Domestic
                    bCreateObject = True
                    bReadLine = False

                    '''Case "15", "16"       'TK16, Kreditnota or TK15 Kreditnota, avdrag bestemt dag
                ElseIf (Left(sImportLineBuffer, 2) = "15" Or Left(sImportLineBuffer, 2) = "16") And oCargo.PayType = "D" Then
                    ' Sometimes TK15/16 comes before TK26.
                    ' Must therefore create paymentrecord from TK15/16!!!
                    ' Have returned from payment with TK15/16, create new payment!
                    sPaymentType = "D" 'Domestic
                    bCreateObject = True
                    bReadLine = False

                    '''Case "26"        'TK26, Navnerecord
                ElseIf Left(sImportLineBuffer, 2) = "26" And oCargo.PayType = "D" Then
                    ' Have returned from payment with TK26, create new payment!
                    sPaymentType = "D" 'Domestic
                    bCreateObject = True
                    bReadLine = False


                    '''Case "29"        'Slutsumma (TK29)
                ElseIf Left(sImportLineBuffer, 2) = "29" And oCargo.PayType = "D" Then
                    sPaymentType = "D" 'Domestic
                    nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 21, 12)) ' Sum amount in batch, NB! in Kroner

                    bAmountSetInvoice = True
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True ' true to trigger that if we get a 29-rec, jump up

                    '''Case "40"        'TK40, Konto/l�neins�tting
                ElseIf Left(sImportLineBuffer, 2) = "40" And oCargo.PayType = "D" Then
                    sPaymentType = "D" 'Domestic
                    bCreateObject = True
                    bReadLine = False

                    'XOKNET 19.08.2013 added next ElseIf
                ElseIf Left$(sImportLineBuffer, 2) = "49" And oCargo.PayType = "D" Then
                    ' avvisnings�rsak
                    sPaymentType = "D" 'Domestic
                    bCreateObject = True
                    bReadLine = False

                    '''Case E�lse

                    '--------------------------------
                    ' Utlandsformat
                    '--------------------------------
                    '''Select Case Left$(sImportLineBuffer, 1)
                    '''Case "0"        ' �ppningspost (TK0)
                ElseIf Left(sImportLineBuffer, 1) = "0" Then
                    sPaymentType = "I" 'International
                    oCargo.PayType = "I" ' added 04.07.2008

                    If bFromLevelUnder Then
                        ' have read a 20-record in batch (new batch)
                        ' jump up, to create object
                        Exit Do
                    Else
                        ' Extract paymentdate if present;
                        If Len(Trim(Mid(sImportLineBuffer, 73, 6))) = 6 Then

                            If UCase(oCargo.Special) = "SKTPETRI" Then
                                dPayment_Date = DateSerial(CInt("20" & Mid(sImportLineBuffer, 77, 2)), CInt(Mid(sImportLineBuffer, 73, 2)), CInt(Mid(sImportLineBuffer, 75, 2))) 'MMDDYY in file
                                oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 77, 2) & Mid(sImportLineBuffer, 73, 2) & Mid(sImportLineBuffer, 75, 2) 'MMDDYY in file
                            Else
                                dPayment_Date = DateSerial(CInt("20" & Mid(sImportLineBuffer, 73, 2)), CInt(Mid(sImportLineBuffer, 75, 2)), CInt(Mid(sImportLineBuffer, 77, 2)))
                                ' Changed 27.06.06, put date into oCargo
                                oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 73, 2) & Mid(sImportLineBuffer, 75, 2) & Mid(sImportLineBuffer, 77, 2)
                            End If
                        End If
                        sFormatType = "Leverantorsbetalningar"
                        bReadLine = True
                        bCreateObject = True

                    End If

                    '''Case "2"        ' TK2, Namn
                ElseIf Left(sImportLineBuffer, 1) = "2" And oCargo.PayType = "I" Then
                    bReadLine = False
                    bCreateObject = True

                '''Case "9"        'Slutsumma (TK9)
                ElseIf Left(sImportLineBuffer, 1) = "9" And oCargo.PayType = "I" Then
                    sPaymentType = "I" 'International
                    nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 64, 15)) / 100 ' Sum amount in batch, NB! in Kroner
                    bAmountSetInvoice = True
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True ' true to trigger that if we get a 20-rec, jump up

                    '''Case Else
                Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

                    '''End Select
                    '''End Select
                End If

                'WARNING! Is this correct
                If oFile.AtEndOfStream = True Then
                    Exit Do
                End If


                'Read next line
                If bReadLine Then
                sImportLineBuffer = ReadLeverantorsbetalningarRecord(oFile, oCargo.Special) ', oProfile)
                End If

                If bCreateObject Then
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.StatusCode = oCargo.StatusCode
                    '''oPayment.I_Account = CVar(sI_Account)
                    ' Have cheated about i_account. Passed as sVersion from BabelFile.
                    'If bProfileInUse Then
                    oPayment.I_Account = CObj(sREF_Own)
                    oPayment.PayType = sPaymentType ' "D" or "I"

                    'End If

                    'If Len(dPayment_Date) > 0 Then
                    If dPayment_Date >= Now Then
                        oPayment.DATE_Payment = VB6.Format(dPayment_Date, "YYYYMMDD") 'CVar(dPayment_Date)
                    End If

                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    If Left(sImportLineBuffer, 2) = "16" Or Left(sImportLineBuffer, 2) = "15" Then
                        ' Set e_account for new payment if 15/16-record before 25!
                        If EmptyString((oPayment.E_Account)) = 0 Then ' 10.05.06 added iftest not to destroy kontoinsetting (40)
                            oPayment.E_Account = Mid(sImportLineBuffer, 3, 10)
                        End If
                    End If
                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                    bFromLevelUnder = True
                End If
        Loop

        sREF_Own = ""
        nMON_InvoiceAmount = 0
        AddInvoiceAmountOnBatch()

        Readleverantorsbetalningar = sImportLineBuffer
    End Function
    Private Function ReadVB_InternalFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim sResponse As String
        'Variables for storing date for test
        Dim sAccount As String
        Dim bAbandon, bPaymentFound As Boolean

        bFromLevelUnder = False
        bAbandon = False

        sFormatType = "VB_Internal"

        Do While True

            If Not bFromLevelUnder Then
                Do While True
                    sResponse = "50100006222"
                    sResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", sResponse)
                    If sResponse = "" Then
                        ' user pressed Cancel
                        bAbandon = True
                        Exit Do
                    End If
                    sResponse = Replace(sResponse, ".", "") 'Remove any .

                    If Len(sResponse) = 11 And IsNumeric(CObj(sResponse)) Then
                        sAccount = sResponse
                        bFromLevelUnder = True
                        Exit Do
                    Else
                        MsgBox("Feil belastningskonto oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
                    End If
                Loop
            End If

            If bAbandon Then
                Exit Do
            End If
            bPaymentFound = False

            Do While oFile.AtEndOfStream = False And Not bPaymentFound
                If Len(Trim(xDelim(sImportLineBuffer, ";", 9))) = 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object ReadExcelRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sImportLineBuffer = ReadExcelRecord(oFile)
                Else
                    bPaymentFound = True
                End If
            Loop


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            'oPayment.I_Account = CVar(sAccount)
            'oPayment.DATE_Payment =VB6.Format(dPayment_Date, "YYYYMMDD") 'CVar(dPayment_Date)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            oPayment.I_Account = CObj(sAccount)
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            'ReadVB_InternalFile = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        ReadVB_InternalFile = sImportLineBuffer

        bx = AddInvoiceAmountOnBatch()

    End Function

    Private Function ReadElkjopFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        'Variables for storing date for test
        Dim bAbandon As Boolean

        bFromLevelUnder = False
        bAbandon = False
        bProfileInUse = True

        sFormatType = "Elkjop"
        dDATE_Production = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(VB6.Format(Now, "MM")), CInt(VB6.Format(Now, "DD")))

        Do While oFile.AtEndOfStream = False

            'If bFromLevelUnder Then
            sImportLineBuffer = ReadGenericRecord(oFile)
            'End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            'oPayment.DATE_Payment =VB6.Format(dPayment_Date, "YYYYMMDD") 'CVar(dPayment_Date)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        ReadElkjopFile = sImportLineBuffer

        bx = AddInvoiceAmountOnBatch()

    End Function
	Private Function enitelReadOneTimeFormat() As String
		' FOr Enitel konkursbo
		
		Dim oPayment As Payment
		Dim bFromLevelUnder As Boolean
		'Variables used to set properties in the payment-object
		Dim bx As Boolean
		'Variables for storing date for test
		Dim dPaymentDate As Date
		Dim sAccountResponse As String
		
		bFromLevelUnder = False
		sFormatType = "OneTimeFormat"
		' Oppdragsnr, 7 digits, at batchlevel
		' Use Dhhmmss
		sBatch_ID = Right(CStr(VB.Day(Now)), 1) & PadLeft(CStr(Hour(Now)), 2, "0") & PadLeft(CStr(Minute(Now)), 2, "0") & PadLeft(CStr(Second(Now)), 2, "0")
		
		Do While True
			
			If Not bFromLevelUnder Then
				
				Do While True
					sAccountResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", "99999999999")
					If Len(sAccountResponse) = 11 And IsNumeric(CObj(sAccountResponse)) Then
						Exit Do
					Else
						MsgBox("Feil belastningskonto oppgitt: " & sAccountResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
					End If
				Loop 
				
				dPaymentDate = DateSerial(Int(Year(Now)), Int(Month(Now)), Int(VB.Day(Now)))
				
			Else
				If Not oFile.AtEndOfStream Then
					'sImportLineBuffer = ReadGenericRecord(oFile)  ' 1. line
					sImportLineBuffer = ReadGenericRecord(oFile) ' 2. line
					'sImportLineBuffer = sImportLineBuffer + ReadGenericRecord(oFile) ' 3. line
				End If
			End If
			
			If Left(sImportLineBuffer, 3) = ";;;" Then
				sImportLineBuffer = "ERROR"
				Exit Do
			End If
			
			
			oPayment = oPayments.Add()
			oPayment.objFile = oFile
			oPayment.VB_Profile = oProfile
			oPayment.I_Account = sAccountResponse
			oPayment.DATE_Payment = VB6.Format(dPaymentDate, "YYYYMMDD") 'CVar(dPaymentDate)
			
			oPayment.ImportFormat = iImportFormat
			oPayment.StatusCode = sStatusCode
			sImportLineBuffer = oPayment.Import(sImportLineBuffer)
			
			
			bFromLevelUnder = True
			enitelReadOneTimeFormat = sImportLineBuffer
			
			
			If oFile.AtEndOfStream = True Then
				'If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
				Exit Do
				'End If
			End If
			
		Loop 
		
		bx = AddInvoiceAmountOnBatch()
		
		enitelReadOneTimeFormat = sImportLineBuffer
		
	End Function
	
	Private Function ReadOneTimeFormat() As String
		Dim oPayment As Payment
		Dim bFromLevelUnder As Boolean
		'Variables used to set properties in the payment-object
		Dim sResponse As String
		Dim bx As Boolean
		'Variables for storing date for test
		Dim dPaymentDate As Date
		Dim sMonth, sDay, sYear As String
		Dim bOutOfLoop As Boolean
		Dim sAccountResponse As String
		Dim sCategory As String
		Dim sStartNumber As String
		Dim nStartNumber As Double
		Dim sNumber As String
		Dim nNumber As Double
		Dim nLinesRead As Double
		Dim bAbandonExport As Boolean
		Dim bExport As Boolean
		Dim oRejectsFile As Scripting.TextStream
        Dim oRejectsObject As Scripting.FileSystemObject
		Dim aFreetexts() As String
		Dim sFilename As String
		Dim otxtFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject


        ' Handelsbanken til OCR
        '----------------------------
        bFromLevelUnder = False
        sFormatType = "OneTimeFormat"
        ' Oppdragsnr, 7 digits, at batchlevel
        ' Use Dhhmmss
        sBatch_ID = Right(CStr(VB.Day(Now)), 1) & PadLeft(CStr(Hour(Now)), 2, "0") & PadLeft(CStr(Minute(Now)), 2, "0") & PadLeft(CStr(Second(Now)), 2, "0")

        Do While True

            Do
                ' les til vi finner 03.04.2017 i starten av linje
                If Left(sImportLineBuffer, 10) = "03.04.2017" Then
                    Exit Do
                End If
                sImportLineBuffer = ReadGenericRecord(oFile)
            Loop


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)


            bFromLevelUnder = True


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        '      oRejectsObject = New Scripting.FileSystemObject

        'oRejectsFile = oRejectsObject.OpenTextFile("C:\Slett\Rejects.dat", Scripting.IOMode.ForAppending, True, 0)

        'bFromLevelUnder = False
        'sFormatType = "OneTimeFormat"
        '' Oppdragsnr, 7 digits, at batchlevel
        '' Use Dhhmmss
        'sBatch_ID = Right(CStr(VB.Day(Now)), 1) & PadLeft(CStr(Hour(Now)), 2, "0") & PadLeft(CStr(Minute(Now)), 2, "0") & PadLeft(CStr(Second(Now)), 2, "0")

        'Do While True

        '	If Not bFromLevelUnder Then

        '		Do While True
        '			sAccountResponse = InputBox("Legg inn konto for belastning", "Belastningskonto", "82000110023")
        '			If Len(sAccountResponse) = 11 And IsNumeric(CObj(sAccountResponse)) Then
        '				Exit Do
        '			Else
        '				MsgBox("Feil belastningskonto oppgitt: " & sAccountResponse & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
        '			End If
        '		Loop 

        '		bOutOfLoop = False
        '		' Ask for paymentdate:
        '		Do While bOutOfLoop = False

        '			bOutOfLoop = True
        '			sResponse = InputBox("Legg inn betalingsdato (dd.mm.yyyy)", "Utbetalingsdato", Trim(Str(VB.Day(Now))) & "." & Trim(Str(Month(Now))) & "." & Trim(Str(Year(Now))))
        '			If sResponse = "" Then
        '				' user pressed Cancel
        '				bOutOfLoop = False
        '				Exit Do
        '			End If

        '			If Len(sResponse) - Len(Replace(sResponse, ".", "")) = 2 Then
        '				' OK, two . in datestring:
        '				' Test date given;
        '				sDay = Left(sResponse, InStr(sResponse, ".") - 1)
        '				sMonth = Mid(sResponse, InStr(sResponse, ".") + 1, 2)
        '				If Right(sMonth, 1) = "." Then
        '					' only one digit for month
        '					sMonth = Left(sMonth, 1)
        '				End If
        '				sYear = Right(sResponse, 4)
        '				If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
        '					MsgBox("Feil i �rstall - Pr�v igjen!")
        '					bOutOfLoop = False
        '				End If
        '				If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
        '					MsgBox("Feil i m�ned - Pr�v igjen!")
        '					bOutOfLoop = False
        '				End If
        '				If Val(sDay) < 1 Or Val(sDay) > 31 Then
        '					MsgBox("Feil i dag - Pr�v igjen!")
        '					bOutOfLoop = False
        '				End If
        '				If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
        '					If Val(sDay) > 30 Then
        '						MsgBox("Feil i dag - Pr�v igjen!")
        '						bOutOfLoop = False
        '					End If
        '				End If
        '				If Val(sMonth) = 2 Then
        '					If Val(sDay) > 29 Then
        '						MsgBox("Feil i dag - Pr�v igjen!")
        '						bOutOfLoop = False
        '					End If
        '				End If
        '			Else
        '				MsgBox("Feil i dato!" & vbCrLf & "Skal ha formen dd.mm.yyyy" & vbCrLf & "Husk punktum som skilletegn!")
        '				bOutOfLoop = False
        '			End If
        '		Loop 
        '		If sResponse = "" Then
        '			bOutOfLoop = False
        '		Else
        '			dPaymentDate = DateSerial(Int(CDbl(sYear)), Int(CDbl(sMonth)), Int(CDbl(sDay)))
        '		End If

        '		'Ask for catergory
        '		Do While True
        '			sCategory = InputBox("Legg inn kategori:", "Kategori", "1")
        '			If Len(sCategory) = 1 And IsNumeric(CObj(sCategory)) Then
        '				Exit Do
        '			Else
        '				MsgBox("Feil kategori angitt: " & sCategory & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
        '			End If
        '		Loop 

        '		Do While True
        '			' Prepare for freetext-import
        '			' Import freetext from file FREETEXT.1, FREETEXT.2, etc, depentdant on Kategori
        '			Select Case sCategory
        '				Case CStr(1)
        '					sFilename = My.Application.Info.DirectoryPath & "\freetext1.txt"
        '				Case CStr(2)
        '					sFilename = My.Application.Info.DirectoryPath & "\freetext2.txt"
        '				Case CStr(3)
        '					sFilename = My.Application.Info.DirectoryPath & "\freetext3.txt"
        '				Case CStr(4)
        '					sFilename = My.Application.Info.DirectoryPath & "\freetext4.txt"
        '				Case CStr(5)
        '					sFilename = My.Application.Info.DirectoryPath & "\freetext5.txt"
        '				Case CStr(6)
        '					sFilename = My.Application.Info.DirectoryPath & "\freetext6.txt"
        '			End Select

        '                  ofs = New Scripting.FileSystemObject

        '			oFs = CreateObject ("Scripting.Filesystemobject")
        '			If oFs.FileExists(sFilename) Then
        '				otxtFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)
        '				Do While otxtFile.AtEndOfStream = False
        '					If Array_IsEmpty(aFreetexts) Then
        '						ReDim aFreetexts(0)
        '					Else
        '						ReDim Preserve aFreetexts(UBound(aFreetexts) + 1)
        '					End If
        '					'Read one line, fill up array
        '					aFreetexts(UBound(aFreetexts)) = otxtFile.ReadLine()
        '				Loop 
        '				'UPGRADE_NOTE: Object otxtFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '				otxtFile = Nothing
        '				Exit Do
        '			Else
        '				MsgBox("Finner ikke " & vbCrLf & sFilename & vbCrLf & "Opprett filen, og pr�v igjen")
        '			End If
        '			'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '			oFs = Nothing
        '		Loop 


        '		'Ask for the startnumber of payment to produce
        '		Do While True
        '			sStartNumber = InputBox("Legg inn startnummer p� betaling", "Startnummer")
        '			If IsNumeric(CObj(sStartNumber)) Then
        '				nStartNumber = Val(sStartNumber)
        '				Exit Do
        '			Else
        '				MsgBox("Feil antall oppgitt: " & sStartNumber & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
        '			End If
        '		Loop 

        '		'Ask for number of payments to produce
        '		Do While True
        '			sNumber = InputBox("Legg inn antall betalinger", "Startnummer")
        '			If IsNumeric(CObj(sNumber)) Then
        '				nNumber = Val(sNumber)
        '				Exit Do
        '			Else
        '				MsgBox("Feil antall oppgitt: " & sNumber & vbCrLf & "Pr�v p� nytt!", MsgBoxStyle.OKOnly, "Feil kontonummer")
        '			End If
        '		Loop 

        '		nLinesRead = 0
        '		bAbandonExport = False
        '		bExport = True

        '	End If


        '	If bOutOfLoop Then

        '		'Don export payments abroad, without accountno.
        '		' Save them in a file
        '		'        Do While True
        '		'            sTempCountry = xDelim(sImportLineBuffer, ";", 7)
        '		'
        '		'            If Len(sTempCountry) > 1 And Left$(sTempCountry, 1) <> Chr(9) Then
        '		'            'If Len(sTempCountry) > 1 Or (Len(sTempCountry) > 1 And Left$(sTempCountry, 1) <> Chr(9)) Then
        '		'                If Len(xDelim(sImportLineBuffer, ";", 8)) <> 11 Then
        '		'                    oRejectsFile.WriteLine sImportLineBuffer
        '		'                    If oFile.AtEndOfStream Then
        '		'                        sImportLineBuffer = "ERROR"
        '		'                        bAbandonExport = True
        '		'                        Exit Do
        '		'                    Else
        '		'                        sImportLineBuffer = ReadGenericRecord(oFile)
        '		'                    End If
        '		'                Else
        '		'                    Exit Do
        '		'                End If
        '		'            Else
        '		'                Exit Do
        '		'            End If
        '		'
        '		'        Loop

        '		If bAbandonExport Then
        '			sImportLineBuffer = "ERROR"
        '			Exit Do
        '		End If

        '		Do While True
        '			'Check if it is correct category
        '			' New 20.03.03 - No category given If Trim(xDelim(sImportLineBuffer, ";", 10)) = sCategory Then
        '			If Trim(xDelim(sImportLineBuffer, ";", 10)) = sCategory Then
        '				nLinesRead = nLinesRead + 1
        '				'Check if we have to read some lines before we start
        '				If nLinesRead >= nStartNumber Then
        '					'check if we shal stop exporting. The max number has been reached.
        '					If nLinesRead <= nStartNumber + nNumber - 1 Then
        '						Exit Do
        '					Else
        '						'The max no. has been reached
        '						bAbandonExport = True
        '						Exit Do
        '					End If
        '				End If
        '			End If
        '			If oFile.AtEndOfStream Then
        '				sImportLineBuffer = "ERROR"
        '				bAbandonExport = True
        '				Exit Do
        '			Else
        '				sImportLineBuffer = ReadGenericRecord(oFile)
        '			End If
        '		Loop 

        '		If bAbandonExport Then
        '			sImportLineBuffer = "ERROR"
        '			Exit Do
        '		End If

        '		oPayment = oPayments.Add()
        '		oPayment.objFile = oFile
        '		oPayment.VB_Profile = oProfile
        '		oPayment.I_Account = sAccountResponse
        '		oPayment.DATE_Payment = VB6.Format(dPaymentDate, "YYYYMMDD") 'CVar(dPaymentDate)

        '		oPayment.ImportFormat = iImportFormat
        '		oPayment.StatusCode = sStatusCode
        '		sImportLineBuffer = oPayment.Import(sImportLineBuffer)

        '		' Put in freetext, previously stored in aFreetexts

        '		'        If oPayment.Invoices(1).MATCH_Matched = True Then
        '		'            oPayment.Invoices.Item(1).Freetexts.Add
        '		'            oPayment.Invoices.Item(1).Freetexts(1).Text = "Utbetaling av eierverdier ifm omdanning" '"Utbetaling av eierverdier ifm omdanning "
        '		'            oPayment.Invoices.Item(1).Freetexts.Add
        '		'            oPayment.Invoices.Item(1).Freetexts(2).Text = "av Gjensidige NOR Spareforsikring til" '"av Gjensidige NOR Spareforsikring til "
        '		'            oPayment.Invoices.Item(1).Freetexts.Add
        '		'            oPayment.Invoices.Item(1).Freetexts(3).Text = "aksjeselskap. Skatten er fratrukket." '"aksjeselskap. Skatten er fratrukket."
        '		'        Else
        '		'            For iLine = 1 To UBound(aFreetexts) + 1
        '		'                oPayment.Invoices.Item(1).Freetexts.Add
        '		'                oPayment.Invoices.Item(1).Freetexts(iLine).Text = aFreetexts(iLine - 1)
        '		'                If Len(aFreetexts(iLine - 1)) > 40 Then
        '		'                    'error
        '		'                    sImportLineBuffer = "ERROR"
        '		'                    MsgBox "For lang tekstlinje ( > 40 )"
        '		'                End If
        '		'            Next iLine
        '		'        End If

        '		bFromLevelUnder = True


        '		ReadOneTimeFormat = sImportLineBuffer


        '		If oFile.AtEndOfStream = True Then
        '			If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
        '				Exit Do
        '			End If
        '		End If
        '	Else
        '		' user has canceled InputBox
        '		' Skip the rest of the lines
        '		Do Until oFile.AtEndOfStream = True
        '			oFile.SkipLine()
        '		Loop 
        '		sImportLineBuffer = "ERROR"

        '		Exit Do
        '	End If

        'Loop 

        'bx = AddInvoiceAmountOnBatch()

        '      If Not oRejectsFile Is Nothing Then
        '          oRejectsFile.Close()
        '          oRejectsFile = Nothing
        '      End If

        '      If Not oRejectsObject Is Nothing Then
        '          oRejectsObject = Nothing
        '      End If

        '      If Not oFs Is Nothing Then
        '          oFs = Nothing
        '      End If

        ReadOneTimeFormat = sImportLineBuffer
		
	End Function
	Private Function ReadDnB_TBIW_Standard() As String
		Dim oPayment As Payment
		Dim bFromLevelUnder, bx As Boolean
		
		bFromLevelUnder = False
		sFormatType = "DnB_TBIW_Standard"
		
		Do While True
			
			oPayment = oPayments.Add()
			oPayment.objFile = oFile
			oPayment.VB_Profile = oProfile
			oPayment.ImportFormat = iImportFormat
			oPayment.StatusCode = sStatusCode
			sImportLineBuffer = oPayment.Import(sImportLineBuffer)
			bFromLevelUnder = True
			
			ReadDnB_TBIW_Standard = sImportLineBuffer
			
			If oFile.AtEndOfStream = True Then
				If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
					Exit Do
				End If
			End If
			
		Loop 
		
		bx = AddInvoiceAmountOnBatch()
		
	End Function
	Private Function ReadDnBNOR_UK_TBI_Standard() As String
		Dim oPayment As Payment
		Dim bFromLevelUnder, bx As Boolean
		
		bFromLevelUnder = False
		sFormatType = "DnBNOR_UK_TBI_Standard"
		
		Do While True
			
			oPayment = oPayments.Add()
			oPayment.objFile = oFile
			oPayment.VB_Profile = oProfile
			oPayment.ImportFormat = iImportFormat
			oPayment.StatusCode = sStatusCode
			sImportLineBuffer = oPayment.Import(sImportLineBuffer)
			bFromLevelUnder = True
			
			ReadDnBNOR_UK_TBI_Standard = sImportLineBuffer
			
			If oFile.AtEndOfStream = True Then
				If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
					Exit Do
				End If
			End If
			
		Loop 
		
		bx = AddInvoiceAmountOnBatch()
		
	End Function
	
	Private Function ReadDTAUS() As String
		Dim oPayment As Payment
		Dim bx As Boolean
		Dim sTmp As String
		Dim dDate_Payment As Date
		Dim sValuta As String
		Dim bFoundEndBatchRecord As Boolean
		Dim nTotalOfAccounts, nTotalAmount, nTotalOfBranchIDs As Double
		
		
		'sI_AccountNo = Mid$(sImportLineBuffer, 61, 10)
		sTmp = Mid(sImportLineBuffer, 96, 8)
		If Len(Trim(sTmp)) = 8 Then
			dDate_Payment = DateSerial(CInt(Mid(sImportLineBuffer, 100, 4)), CInt(Mid(sImportLineBuffer, 98, 2)), CInt(Mid(sImportLineBuffer, 96, 2)))
			If dDate_Payment < dDATE_Production Then
				Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10021-020", DateToString(dDate_Payment), DateToString(dDATE_Production))
			ElseIf dDate_Payment > System.Date.FromOADate(dDATE_Production.ToOADate + 15) Then 
				Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10021-020", DateToString(dDate_Payment), DateToString(dDATE_Production))
			End If
		Else
			dDate_Payment = dDATE_Production
		End If
		If Mid(sImportLineBuffer, 128, 1) = "1" Then
			sValuta = "EUR"
		Else
			sValuta = "DEM"
		End If
		
		bFoundEndBatchRecord = False
		
		'The first record is read, delete the record A type - record, length 128 from sImportLinebuffer.
		sImportLineBuffer = Mid(sImportLineBuffer, 129)
		
		Do While True
			
			sImportLineBuffer = ReadDTAUSRecord(oFile, sImportLineBuffer)
			'Every payment consists of at least 2 lines. Each 128 long.
			'sImportLineBuffer = sImportLineBuffer & ReadDTAUSRecord(oFile)
			
			'Check if we have read the start of a new payment
			If Mid(sImportLineBuffer, 5, 1) = "C" Then
				'OK, regular transaction
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				'    oPayment.I_Account = sI_AccountNo
				oPayment.DATE_Payment = DateToString(dDate_Payment)
				'    oPayment.MON_TransferCurrency = sValuta
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
			ElseIf Mid(sImportLineBuffer, 1, 5) = "0128E" Then 
				'End of file
				bFoundEndBatchRecord = True
				Exit Do
			Else
				'Illegal record
				'Error # 10024-020
				Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10024-020")
			End If
			
			If oFile.AtEndOfStream = True Then
				If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
					Exit Do
				End If
			End If
			
		Loop 
		
		If bFoundEndBatchRecord Then
			nTotalAmount = 0
			nTotalOfAccounts = 0
			nTotalOfBranchIDs = 0
			'Check the total number of transactions
			If Payments.Count <> Val(Mid(sImportLineBuffer, 11, 7)) Then
				'Error 10030-020
				Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10030-020", CStr(Payments.Count), CStr(Val(Mid(sImportLineBuffer, 11, 7))))
			End If
			For	Each oPayment In Payments
				nTotalAmount = nTotalAmount + oPayment.MON_InvoiceAmount
				nTotalOfAccounts = nTotalOfAccounts + Val(oPayment.E_Account)
				nTotalOfBranchIDs = nTotalOfBranchIDs + Val(oPayment.BANK_BranchNo)
			Next oPayment
			'Check the total amount
			If sValuta = "EUR" Then
				If nTotalAmount <> Val(Mid(sImportLineBuffer, 65, 13)) Then
					'Error 10031-020
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10031-020", Trim(Str(nTotalAmount)), Mid(sImportLineBuffer, 65, 13), "Total Amount")
				End If
			Else
				'DEM
				If nTotalAmount <> Val(Mid(sImportLineBuffer, 18, 13)) Then
					'Error 10031-020
					Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10031-020", Trim(Str(nTotalAmount)), Mid(sImportLineBuffer, 65, 13), "Total Amount")
				End If
			End If
			'Check the accounts stated
			If nTotalOfAccounts <> Val(Mid(sImportLineBuffer, 31, 17)) Then
				'Error 10031-020
				Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10031-020", Trim(Str(nTotalOfAccounts)), Mid(sImportLineBuffer, 31, 17), "Totals of account")
			End If
			'Check the branchIDs stated
			If nTotalOfBranchIDs <> Val(Mid(sImportLineBuffer, 48, 17)) Then
				'Error 10031-020
				Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10031-020", Trim(Str(nTotalOfBranchIDs)), Mid(sImportLineBuffer, 48, 17), "Totals of bank codes")
			End If
			If sStatusCode = "00" Or StatusCode = "01" Then
				bx = AddInvoiceAmountOnBatch()
			Else
				bx = AddInvoiceAmountOnBatch()
				bx = AddTransferredAmountOnBatch()
			End If
			sImportLineBuffer = Mid(sImportLineBuffer, 129)
			ReadDTAUS = sImportLineBuffer
		Else
			'Error 10029-020
			Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10029-020")
		End If
		
		
		
	End Function
	
	Private Function ReadMT940() As String
		Dim oPayment As Payment
		Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
		
		bFromLevelUnder = False
		
		sFormatType = "MT940"
		
		bFromLevelUnder = False
		
		Do While True
			bCreateObject = False
			
			Select Case Left(sImportLineBuffer, 4)
				
				Case ":61:"
					
					'Added 04.01.2006 Elkem had an example where a 61 was followed by another 61
					'If so have to exit payment, and add a new batch
					If bFromLevelUnder Then
						bReadLine = False
						bCreateObject = False
						Exit Do
					Else
						'        If IsNumeric(Mid$(sImportLineBuffer, 11, 1)) Then
						'            If Mid$(sImportLineBuffer, 15, 1) = "D" Then
						'                sImportLineBuffer = "REMOVE" & sImportLineBuffer
						'                Exit Do
						'            End If
						'        Else
						'            If Mid$(sImportLineBuffer, 11, 1) = "D" Then
						'                sImportLineBuffer = "REMOVE" & sImportLineBuffer
						'                Exit Do
						'            End If
						'        End If
						bReadLine = False
						bCreateObject = True
					End If
					
				Case ":62F"
					bReadLine = False
					bCreateObject = False
					Exit Do
					
				Case ":62M"
					bReadLine = False
					bCreateObject = False
					Exit Do
					
				Case ":86:"
					If bFromLevelUnder Then
						bReadLine = False
						bCreateObject = False
						Exit Do
					Else
						'Error # 10010-029
						Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-029")
					End If
					
			End Select
			
			If bReadLine Then
				sImportLineBuffer = ReadMT940Record(oFile)
			End If
			
			If bCreateObject Then
				oPayment = oPayments.Add()
				oPayment.objFile = oFile
				oPayment.VB_Profile = oProfile
				oPayment.I_Account = oCargo.I_Account
				oPayment.REF_Bank2 = oCargo.REF
				oPayment.MON_AccountCurrency = oCargo.MONCurrency
				oPayment.ImportFormat = iImportFormat
				oPayment.StatusCode = sStatusCode
				sImportLineBuffer = oPayment.Import(sImportLineBuffer)
				bFromLevelUnder = True
			End If
			
			'ReadMT940 = sImportLineBuffer
			If sImportLineBuffer = "ERROR" Then
				Exit Do
			End If
			
			
			If oFile.AtEndOfStream = True Then
				If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
					Exit Do
				End If
			End If
			
		Loop 
		
		AddInvoiceAmountOnBatch()
		AddTransferredAmountOnBatch()
		
		ReadMT940 = sImportLineBuffer
		
	End Function
    Private Function ReadCorporate_Banking_Interbancario() As String
        Dim oPayment As Payment
        Dim bReadLine As Boolean
        Dim bFromLevelUnder As Boolean
        Dim bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "Corporate Banking Interbancario"

        bFromLevelUnder = False

        Do While True
            bCreateObject = False

            Select Case Left(sImportLineBuffer, 3)

                Case " 62"
                    'New payment
                    If bFromLevelUnder Then
                        bReadLine = False
                        bCreateObject = False
                        Exit Do
                    Else
                        bReadLine = False
                        bCreateObject = True
                    End If

                Case " 63"
                    'Error # 10010-029
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-029")

                Case " 64"
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case Else
                    'Error # 10010-029
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-128")

            End Select

            If bReadLine Then
                sImportLineBuffer = ReadMT940Record(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = oCargo.I_Account
                oPayment.MON_AccountCurrency = oCargo.MONCurrency
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadCorporate_Banking_Interbancario = sImportLineBuffer

    End Function
    Private Function ReadMT940E_Handelsbanken(ByRef bBothCreditAndDebit As Boolean) As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "MT940_Handelsbanken"

        bFromLevelUnder = False

        Do While True
            bCreateObject = False

            Select Case Left(sImportLineBuffer, 4)

                Case ":61:"

                    'Added 04.01.2006 Elkem had an example where a 61 was followed by another 61
                    'If so have to exit payment, and add a new batch
                    If bFromLevelUnder Then
                        bReadLine = False
                        bCreateObject = False
                        Exit Do
                    Else
                        '        If IsNumeric(Mid$(sImportLineBuffer, 11, 1)) Then
                        '            If Mid$(sImportLineBuffer, 15, 1) = "D" Then
                        '                sImportLineBuffer = "REMOVE" & sImportLineBuffer
                        '                Exit Do
                        '            End If
                        '        Else
                        '            If Mid$(sImportLineBuffer, 11, 1) = "D" Then
                        '                sImportLineBuffer = "REMOVE" & sImportLineBuffer
                        '                Exit Do
                        '            End If
                        '        End If
                        bReadLine = False
                        bCreateObject = True
                    End If

                Case ":62F"
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case ":62M"
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case ":86:"
                    If bFromLevelUnder Then
                        bReadLine = False
                        bCreateObject = False
                        Exit Do
                    Else
                        'Error # 10010-029
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-029")
                    End If

                    'XNET - 20.10.2010 - Added next recordtype
                Case ":90C" 'MT942 - Summary credit transaction
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                    'XNET - 20.10.2010 - Added next recordtype
                Case ":90D" 'MT942 - Summary debit transaction
                    bReadLine = False
                    bCreateObject = False
                    Exit Do
                    ' End of message

                Case "-}{5"
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

            End Select

            If bReadLine Then
                sImportLineBuffer = ReadMT940Record(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = oCargo.I_Account
                oPayment.REF_Bank2 = oCargo.REF
                oPayment.MON_AccountCurrency = oCargo.MONCurrency
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'ReadMT940 = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadMT940E_Handelsbanken = sImportLineBuffer

    End Function
    Private Function ReadNavision_SIData_Reskontro() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "OrklaMediaExcel"

        Do While True

            If bFromLevelUnder Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadNavision_SIData_Reskontro = sImportLineBuffer


    End Function
    Private Function ReadBACS_for_Agresso() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "BACS for Agresso"


        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadBACS_for_Agresso = sImportLineBuffer

    End Function
    Private Function ReadPanfish_Scotland() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "Panfish_Scotland"


        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadPanfish_Scotland = sImportLineBuffer

    End Function
    Private Function Read_Biomar_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "Biomar_UK"

        Do While True
            ' Two recordtypes
            ' 01 = Payment
            ' 02 = Invoice

            If Left(sImportLineBuffer, 3) = "01," Then
                sImportLineBuffer = Trim(sImportLineBuffer)
                ' From recordtype 01;
                oCargo.E_Name = xDelim(sImportLineBuffer, ",", 9, Chr(34))
                oCargo.I_Account = xDelim(sImportLineBuffer, ",", 6, Chr(34))

                ' added 07.11.2008, now takes date from record01
                oCargo.DATE_Payment = Right(xDelim(sImportLineBuffer, ",", 4), 4) & Mid(xDelim(sImportLineBuffer, ",", 4), 4, 2) & Left(xDelim(sImportLineBuffer, ",", 4), 2)

                oCargo.E_Account = xDelim(sImportLineBuffer, ",", 10, Chr(34))
                oCargo.Temp = Replace(xDelim(sImportLineBuffer, ",", 11), "-", "") ' Sortcode receiver
                oCargo.FreeText = xDelim(sImportLineBuffer, ",", 3, Chr(34))

                ' Then read next record
                sImportLineBuffer = Trim(ReadGenericRecord(oFile))
                ' should start with "02,"
                If Left(sImportLineBuffer, 3) <> "02," Then
                    Call BabelImportError("Read_Biomar_UK", oCargo.FilenameIn, sImportLineBuffer, "10010-001")
                End If

            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_Biomar_UK = sImportLineBuffer

    End Function
    Private Function Read_KongsbergAutomotive_BACS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "KongsbergAutomotive_BACS"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_KongsbergAutomotive_BACS = sImportLineBuffer

    End Function
    Private Function ReadSage_UK_Bacs() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "Sage_UK_Bacs"


        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSage_UK_Bacs = sImportLineBuffer

    End Function
    Private Function ReadKvaernerSingaporeSalary() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "KvaernerSingaporeSalary "


        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadKvaernerSingaporeSalary = sImportLineBuffer

    End Function
    Private Function ReadOMustadSingaporeSalary() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "OMustadSingaporeSalary "


        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadOMustadSingaporeSalary = sImportLineBuffer

    End Function
    Private Function ReadEltekSG_Salary() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "EltekSG_Salary"


        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadEltekSG_Salary = sImportLineBuffer

    End Function


    Friend Function Import(ByRef sData As Object) As String

        'Assume failure
        Import = ""

        'BabelBank_Error

        'UPGRADE_WARNING: Couldn't resolve default property of object sData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = sData


        'Check if we have a Profile, if not set bProfileInUse = False
        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetup_ID(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        Import = ReadFile()

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        Exit Function
    End Function

    Friend Function ImportEDI(ByRef oLIN As MSXML2.IXMLDOMNode, ByRef oEDIMapping As MSXML2.IXMLDOMElement, ByVal iFilesetup_ID As Integer) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetup_ID(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        Select Case iImportFormat

            'Removed 11.09.2019
            'Case BabelFiles.FileType.DNV_Germany
            '    ImportEDI = ReadDNV_GermanyFile(oLIN, oEDIMapping)

            Case Else
                ImportEDI = ReadEDIFile(oLIN, oEDIMapping, iFilesetup_ID)

        End Select

        'Exit Function
    End Function
    'Removed 11.09.2019
    'Friend Function ImportTBI(ByRef oBatch As MSXML2.IXMLDOMNode) As Boolean
    '    Dim bReturnValue As Boolean

    '    'Assume failure
    '    bReturnValue = False

    '    'Check if we have a Profile, if not set bProfileInUse = False
    '    'UPGRADE_WARNING: Couldn't resolve default property of object FileSetup_ID(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    If FileSetup_ID() = -1 Then
    '        bProfileInUse = False
    '    End If

    '    'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    oFile = Nothing

    '    ImportTBI = ReadTBIFile(oBatch)

    '    'Exit Function
    'End Function
    Friend Function ImportINS2000(ByRef nodBatch As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetup_ID(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportINS2000 = ReadINS2000(nodBatch)

        'Exit Function
    End Function
    'Removed 11.09.2019
    'Friend Function ImportNHC_Philippines(ByRef nodBatchList As System.Xml.XmlNodeList) As Boolean
    '    Dim bReturnValue As Boolean

    '    'Assume failure
    '    bReturnValue = False

    '    'Check if we have a Profile, if not set bProfileInUse = False
    '    If FileSetup_ID() = -1 Then
    '        bProfileInUse = False
    '    End If

    '    oFile = Nothing

    '    ImportNHC_Philippines = ReadNHC_Philippines(nodBatchList)

    '    'Exit Function
    'End Function
    Friend Function ImportBambora_XML(ByRef nodBatch As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportBambora_XML = ReadBambora_XML(nodBatch)

        'Exit Function
    End Function
    Friend Function ImportKlarna_XML(ByRef nodBatch As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportKlarna_XML = ReadKlarna_XML(nodBatch)

        'Exit Function
    End Function
    Friend Function ImportAdyen_XML(ByRef nodBatch As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportAdyen_XML = ReadAdyen_XML(nodBatch)

        'Exit Function
    End Function
    Friend Function ImportPayex_XML(ByRef nodBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportPayex_XML = ReadPayex_XML(nodBatch, nsMgr)

    End Function
    Friend Function ImporteFaktura_e2b(ByRef nodBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImporteFaktura_e2b = ReadeFaktura_e2b(nodBatch, nsMgr)

    End Function
    'XokNET - 05.12.2011 - Whole function
    Friend Function ImporteGlobal(ByVal nodBatch As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImporteGlobal = ReadeGlobal(nodBatch)

        'Exit Function
    End Function

    Friend Function ImportTellerXML(ByRef nodMerchant As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetup_ID(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportTellerXML = ReadTellerXML(nodMerchant)

        'Exit Function
    End Function
    Friend Function ImportTellerXMLNew(ByRef nodMerchant As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportTellerXMLNew = ReadTellerXMLNew(nodMerchant)

        'Exit Function
    End Function
    ' XokNET 21.08.2015 added function
    Friend Function ImportTellerXMLNew_2015(ByVal nodMerchant As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportTellerXMLNew_2015 = ReadTellerXMLNew_2015(nodMerchant)

        'Exit Function
    End Function
    Friend Function ImportTellerCardXML(ByVal nodMerchant As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportTellerCardXML = ReadTellerCardXML(nodMerchant)

        'Exit Function
    End Function

    Friend Function ImportHelseFonna_XML_Nets(ByVal nodMerchant As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportHelseFonna_XML_Nets = ReadHelseFonna_XML_Nets(nodMerchant)

        'Exit Function
    End Function
    Friend Function ImportVippsXML(ByVal nodPaymentInfo As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportVippsXML = ReadVippsXML(nodPaymentInfo)

        'Exit Function
    End Function
    Friend Function ImportGjensidigeJDE_Basware_XML(ByVal nodInvoice As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportGjensidigeJDE_Basware_XML = ReadGjensidigeJDE_Basware_XML(nodInvoice)

        'Exit Function
    End Function
    Friend Function ImportGjensidigeJDE_Basware_GL02_XML(ByVal nodInvoice As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportGjensidigeJDE_Basware_GL02_XML = ReadGjensidigeJDE_Basware_GL02_XML(nodInvoice)

        'Exit Function
    End Function
    Friend Function ImportGjensidigeJDE_Xledger_Accounts_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportGjensidigeJDE_Xledger_Accounts_XML = ReadGjensidigeJDE_XLedger_Accounts_XML(nodInvoices)

    End Function
    Friend Function ImportGjensidigeJDE_Xledger_Costcenters_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportGjensidigeJDE_Xledger_Costcenters_XML = ReadGjensidigeJDE_XLedger_Costcenters_XML(nodInvoices)

    End Function
    Friend Function ImportGjensidigeJDE_Xledger_Underregnskap_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportGjensidigeJDE_Xledger_Underregnskap_XML = ReadGjensidigeJDE_XLedger_Underregnskap_XML(nodInvoices)

        'Exit Function
    End Function
    Friend Function ImportGjensidigeJDE_Xledger_Suppliers_XML(ByVal nodInvoices As MSXML2.IXMLDOMNodeList) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'Check if we have a Profile, if not set bProfileInUse = False
        If FileSetup_ID() = -1 Then
            bProfileInUse = False
        End If

        oFile = Nothing

        ImportGjensidigeJDE_Xledger_Suppliers_XML = ReadGjensidigeJDE_Xledger_Suppliers_XML(nodInvoices)

        'Exit Function
    End Function

    Private Sub Class_Initialize_Renamed()


        oPayments = New Payments
        nSequenceNoStart = -1
        nSequenceNoEnd = -1
        sStatusCode = "00"
        sVersion = ""
        sOperatorID = ""
        sFormatType = ""
        sI_EnterpriseNo = ""
        sI_Branch = ""
        'sTransDate = "    "
        dDATE_Production = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
        dDATE_BalanceIN = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
        dDATE_BalanceOUT = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
        sBatch_ID = ""
        sREF_Own = ""
        sREF_Bank = ""
        nMON_TransferredAmount = 0
        bAmountSetTransferred = False
        nMON_InvoiceAmount = 0
        bAmountSetInvoice = False
        nNo_Of_Transactions = 0
        nNo_Of_Records = 0
        sUNHString = ""
        sBGMString = ""
        sNADString = ""
        sUCIString = ""
        bProfileInUse = True
        iImportFormat = BabelFiles.FileType.Unknown
        ' XNET 27.03.2013  added properties
        sVisma_FrmNo = vbNullString         ' Firm number
        eVisma_Bank = BabelFiles.Bank.No_Bank_Specified 'Added 06.10.2016
        sVisma_DatabaseName = vbNullString  ' Databasename (like F0002)
        ' XNET 08.05.2013 added sPayType
        sPayType = vbNullString
        bThisIsTBIO = False ' added 25.03.2015 to mark if TBIo or not

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub


    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object oPayments may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oPayments = Nothing
    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub

    Public Function AddInvoiceAmountOnBatch() As Boolean
        Dim oPayment As Payment

        If nMON_InvoiceAmount = 0 Then
            For Each oPayment In Payments
                nMON_InvoiceAmount = nMON_InvoiceAmount + oPayment.MON_InvoiceAmount
            Next oPayment
        End If

        bAmountSetInvoice = True
        'UPGRADE_NOTE: Object oPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oPayment = Nothing
        AddInvoiceAmountOnBatch = True

    End Function
    Public Function AddTransferredAmountOnBatch() As Boolean
        Dim oPayment As Payment

        If nMON_TransferredAmount = 0 Then
            For Each oPayment In Payments
                nMON_TransferredAmount = nMON_TransferredAmount + oPayment.MON_TransferredAmount
            Next oPayment
        End If

        bAmountSetTransferred = True
        'UPGRADE_NOTE: Object oPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oPayment = Nothing
        AddTransferredAmountOnBatch = True

    End Function

    Public Function SaveData(ByVal bSQLServer As Boolean, ByRef myBBDBConnectionAccess As System.Data.OleDb.OleDbConnection, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByRef bMatchedItemsOnly As Boolean, ByRef lBabelFile_ID As Integer, ByRef lMyID As Integer, ByRef bSaveOnlyThisPayment As Boolean, ByRef bSaveOnlyNameAndAddress As Boolean, ByRef bUseCommitAgainstBBDB As Boolean) As Boolean
        Dim sMySQL, sErrorString As String
        Dim bReturnValue As Boolean
        Dim lCounter As Integer
        Dim oPayment As Payment

        On Error GoTo ERRSaveData

        If Not bSaveOnlyThisPayment Then
            sMySQL = "INSERT INTO Batch(Company_ID, Babelfile_ID, Batch_ID, "
            'GUNNRITA
            'sMySQL = sMySQL & "XIndex, SequenceNoStart, SequenceNoEnd, StatusCode, "
            sMySQL = sMySQL & "SequenceNoStart, SequenceNoEnd, StatusCode, "
            sMySQL = sMySQL & "Version, Operator, FormatType, I_EnterpriseNo, "
            sMySQL = sMySQL & "I_Branch, DATE_Production, REF_Own, REF_Bank, "
            sMySQL = sMySQL & "MON_TransferredAmount, MON_InvoiceAmount, AmountSetTransferred, AmountSetInvoice, "
            sMySQL = sMySQL & "No_Of_Transactions, No_Of_Records, ImportFormat, IsSEPAPaymentSet, ThisIsTBIO, ProfileInUse) "
            sMySQL = sMySQL & "VALUES(" & Trim(Str(oProfile.Company_ID)) & ", " & Trim(Str(lBabelFile_ID))
            sMySQL = sMySQL & ", " & Trim(Str(lMyID))
            'GUNNRITA
            'sMySQL = sMySQL & ", " & Trim$(Str(nIndex)) & ", " & Trim$(Str(nSequenceNoStart))
            sMySQL = sMySQL & ", " & Trim(Str(nSequenceNoStart))
            sMySQL = sMySQL & ", " & Trim(Str(nSequenceNoEnd)) & ", '" & sStatusCode
            sMySQL = sMySQL & "', '" & Right(sVersion, 7) & "', '" & sOperatorID
            sMySQL = sMySQL & "', '" & sFormatType & "', '" & sI_EnterpriseNo
            sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(sI_Branch), 20) & "', '" & DateToString(dDATE_Production)
            sMySQL = sMySQL & "', '" & sREF_Own & "', '" & sREF_Bank
            sMySQL = sMySQL & "', " & Trim(Str(nMON_TransferredAmount)) & ", " & Trim(Str(nMON_InvoiceAmount))
            If bSQLServer Then
                sMySQL = sMySQL & ", " & CInt(bAmountSetTransferred) & ", " & CInt(bAmountSetInvoice)
                sMySQL = sMySQL & ", " & Trim(Str(nNo_Of_Transactions)) & ", " & Trim(Str(nNo_Of_Records))
                sMySQL = sMySQL & ", " & Trim(Str(iImportFormat))
                sMySQL = sMySQL & ", " & CInt(bIsSEPAPaymentSet)
                sMySQL = sMySQL & ", " & CInt(bThisIsTBIO) & ", " & CInt(bProfileInUse)
            Else
                sMySQL = sMySQL & ", " & Str(bAmountSetTransferred) & ", " & Str(bAmountSetInvoice)
                sMySQL = sMySQL & ", " & Trim(Str(nNo_Of_Transactions)) & ", " & Trim(Str(nNo_Of_Records))
                sMySQL = sMySQL & ", " & Trim(Str(iImportFormat))
                sMySQL = sMySQL & ", " & Str(bIsSEPAPaymentSet)
                sMySQL = sMySQL & ", " & Str(bThisIsTBIO) & ", " & Str(bProfileInUse) ' 18.07.2016 - Added bProfileInUse
            End If
            sMySQL = sMySQL & ")"

            'Not saved:
            '    sMySQL = "ALTER TABLE Batch ADD ProfileInUse Logical NULL"
            ' sUNHString
            ' sBGMString
            ' sNADString

            myBBDB_AccessCommand.CommandText = sMySQL

            If Not myBBDB_AccessCommand.ExecuteNonQuery() = 1 Then
                Err.Raise(14000, "Batch", sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
            Else
                'MsgBox "Babelfile saving OK"
            End If

            'If Not ExecuteTheSQL(cnProfile, sMySQL, sErrorString, bUseCommitAgainstBBDB) Then
            '    MsgBox(sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
            'Else
            '    'MsgBox "Batch saved OK"
            'End If
        Else
            'Don't save info at the batch-level.
        End If

        lCounter = 0
        For Each oPayment In Payments
            'XNET - New code 22.02.2011 - To be able to save unique Payment_ID's in the archive DB - Odin
            'This may be a critical change but it is done the same way in the batch (I think the reason ther is that we
            '  have removed OCR/Autgiro - batches. Should this also be done Invoice.Savedata
            lCounter = oPayment.Index
            'Old code5
            'lCounter = lCounter + 1
            'ReIndexPayments()
            bReturnValue = oPayment.SaveData(bSQLServer, myBBDBConnectionAccess, myBBDB_AccessCommand, bMatchedItemsOnly, lBabelFile_ID, lMyID, lCounter, bSaveOnlyThisPayment, bSaveOnlyNameAndAddress, bUseCommitAgainstBBDB)
        Next oPayment

       
        SaveData = bReturnValue

        Exit Function

ERRSaveData:
        If Err.Number = 999 Then
            'Err is raised in Payment, Invoice or Freetext
            Err.Raise(Err.Number, Err.Source, Err.Description)
        Else
            Err.Raise(999, "Batch", Err.Description & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        End If

    End Function

    Public Function RetrieveData(ByVal bSQLServer As Boolean, ByRef myBBDB_AccessBatchReader As System.Data.OleDb.OleDbDataReader, ByRef myBBDB_AccessConnection As System.Data.OleDb.OleDbConnection, ByRef bOpenItemsOnly As Boolean, ByRef bMatchedItemsOnly As Boolean, ByRef iCompanyNo As Short, ByRef lBabelFile_ID As Integer, ByRef lBatch_ID As Integer, ByRef lPayment_ID As Integer, ByRef bUseProfile As Boolean, ByRef bOmittedData As Boolean) As Boolean
        Dim lCounter As Integer
        Dim oPayment As vbBabel.Payment
        Dim sMySQL As String
        Dim lDBBatchIndex As Integer
        Dim bReturnValue As Boolean
        Dim sSelectPart As String = ""
        Dim myBBDB_AccessPaymentReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessPaymentCommand As System.Data.OleDb.OleDbCommand = Nothing

        'On Error GoTo errorRetrieveData
        Try

            bReturnValue = False
            'Retrieve and save the data to the local variables

            lDBBatchIndex = myBBDB_AccessBatchReader.GetInt32(0) 'Batch_ID
            nIndex = myBBDB_AccessBatchReader.GetInt32(0) 'Batch_ID
            nSequenceNoStart = myBBDB_AccessBatchReader.GetInt32(1) 'SequenceNoStart
            nSequenceNoEnd = myBBDB_AccessBatchReader.GetInt32(2) 'SequenceNoEnd
            sStatusCode = myBBDB_AccessBatchReader.GetString(3) 'StatusCode
            sVersion = myBBDB_AccessBatchReader.GetString(4) 'Version
            sOperatorID = myBBDB_AccessBatchReader.GetString(5) 'OperatorID
            sFormatType = myBBDB_AccessBatchReader.GetString(6) 'FormatType
            sI_EnterpriseNo = myBBDB_AccessBatchReader.GetString(7) 'I_EnterpriseNo
            If Not myBBDB_AccessBatchReader.IsDBNull(8) Then
                sI_Branch = myBBDB_AccessBatchReader.GetString(8) 'I_Branch
            End If
            dDATE_Production = StringToDate(myBBDB_AccessBatchReader.GetString(9)) 'DATE_Production
            sREF_Own = myBBDB_AccessBatchReader.GetString(10) 'REF_Own
            sREF_Bank = myBBDB_AccessBatchReader.GetString(11) 'REF_Bank
            nMON_TransferredAmount = myBBDB_AccessBatchReader.GetDouble(12) 'MON_TransferredAmount
            nMON_InvoiceAmount = myBBDB_AccessBatchReader.GetDouble(13) 'MON_InvoiceAmount
            bAmountSetTransferred = myBBDB_AccessBatchReader.GetBoolean(14) 'AmountSetTransferred
            bAmountSetInvoice = myBBDB_AccessBatchReader.GetBoolean(15) 'AmountSetInvoice
            nNo_Of_Transactions = myBBDB_AccessBatchReader.GetInt32(16) 'No_Of_Transactions
            nNo_Of_Records = myBBDB_AccessBatchReader.GetInt32(17) 'No_Of_Records
            iImportFormat = myBBDB_AccessBatchReader.GetInt32(18) 'ImportFormat
            bIsSEPAPaymentSet = myBBDB_AccessBatchReader.GetInt32(18)  'IsSEPAPaymentSet
            ' added 25.03.2015
            bThisIsTBIO = myBBDB_AccessBatchReader.GetBoolean(19)    ' ThisIsTBIO

            'sSelectPart = "SELECT Payment_ID, StatusCode, StatusText, Cancel, DATE_Payment, MON_TransferredAmount, AmountSetInvoice, AmountSetTransferred, "
            'sSelectPart = sSelectPart & "MON_TransferCurrency, MON_OriginallyPaidAmount, MON_OriginallyPaidCurrency, REF_Own, REF_Bank1, REF_Bank2, "
            'sSelectPart = sSelectPart & "FormatType, E_Account, E_AccountPrefix, E_AccountSuffix, E_Name, E_Adr1, E_Adr2, E_Adr2, e_OrgNo, "
            'sSelectPart = sSelectPart & "E_Zip, E_City, E_CountryCode, DATE_Value, Priority, I_Client, Text_E_Statement, Text_I_Statement, "
            'sSelectPart = sSelectPart & "Cheque, ToOwnAccount, PayCode, PayType, I_Account, I_Name, I_Adr1, I_Adr2, I_Adr3, I_Zip, I_City, "
            'sSelectPart = sSelectPart & "I_CountryCode, Structured, VB_ClientNo, ExtraD1, ExtraD2, ExtraD3, ExtraD4, ExtraD5, "
            'sSelectPart = sSelectPart & "MON_LocalAmount, MON_LocalCurrency, MON_AccountAmount, MON_AccountCurrency, MON_InvoiceAmount, MON_InvoiceCurrency, "
            'sSelectPart = sSelectPart & "MON_EuroAmount, MON_AccountExchRate, MON_LocalExchRate, MON_LocalExchRateSet, MON_EuroExchRate, MON_ChargesAmount, "
            'sSelectPart = sSelectPart & "MON_ChargesCurrency, MON_ChargeMeDomestic, MON_ChargeMeAbroad, ERA_ExchRateAgreed, ERA_DealMadeWith, "
            'sSelectPart = sSelectPart & "FRW_ForwardContractRate, FRW_ForwardContractNo, NOTI_NotificationMessageToBank, NOTI_NotificationType, "
            'sSelectPart = sSelectPart & "NOTI_NotificationParty, NOTI_NotificationAttention, NOTI_NotificationIdent, BANK_SWIFTCode, BANK_BranchNo, "
            'sSelectPart = sSelectPart & "BANK_Name, BANK_Adr1, BANK_Adr2, BANK_Adr3, BANK_CountryCode, BANK_SWIFTCodeCorrBank, "
            'sSelectPart = sSelectPart & "ExtraI1, ExtraI2, ExtraI3, ImportFormat, FilenameOut_ID, StatusSplittedFreetext, "
            'sSelectPart = sSelectPart & "Exported, VoucherNo, VoucherType, MATCH_Matched, Unique_PaymentID, Unique_ERPID, MATCH_MatchingIDAgainstObservationAccount, "
            'sSelectPart = sSelectPart & "UserID, HowMatched, MATCH_UseoriginalAmountInMatching, SpecialMark, DoNotShow, BANK_BranchType, "
            'sSelectPart = sSelectPart & "DnBNORTBIPayType, REF_EndToEnd, PurposeCode, UltimateCreditor, UltimateDebitor, CategoryPurposeCode, Match_Note, Ref_Own_BabelBankGenerated, IsSEPA "

            '27.08.2014 - Moved the above code to a seperate funtion because we also use this in the SEB ISO20022 project
            sSelectPart = SQLToRetrievePaymentFromDatabase()

            If Not bOmittedData Then 'Normal situation
                sMySQL = sSelectPart & "FROM Payment WHERE Company_ID = " & Trim(Str(iCompanyNo)) & " AND BabelFile_ID = " & Trim(Str(lBabelFile_ID)) & " AND Batch_ID = " & Trim(Str(lDBBatchIndex))
                'If one specific Payment_ID is stated, retrieve just that one.
                If lPayment_ID > 0 Then
                    sMySQL = sMySQL & " AND Payment_ID = " & Trim(Str(lPayment_ID))
                End If
                If bMatchedItemsOnly Then
                    sMySQL = sMySQL & " AND MATCH_Matched = " & Trim(Str(BabelFiles.MatchStatus.Matched))
                End If
            Else
                sMySQL = sSelectPart & "FROM Payment WHERE Company_ID = " & Trim(Str(iCompanyNo)) & " AND BabelFile_ID = " & Trim(Str(lBabelFile_ID)) & " AND Batch_ID = " & Trim(Str(lDBBatchIndex)) & " AND StatusCode = '-1'"
                'If one specific Payment_ID is stated, retrieve just that one.
            End If

            If myBBDB_AccessPaymentCommand Is Nothing Then
                myBBDB_AccessPaymentCommand = myBBDB_AccessConnection.CreateCommand()
                myBBDB_AccessPaymentCommand.CommandType = CommandType.Text
                'Default er CommandTimeout = 30
                myBBDB_AccessPaymentCommand.CommandTimeout = 120
                myBBDB_AccessPaymentCommand.Connection = myBBDB_AccessConnection
            End If

            If Not myBBDB_AccessPaymentReader Is Nothing Then
                If Not myBBDB_AccessPaymentReader.IsClosed Then
                    myBBDB_AccessPaymentReader.Close()
                End If
            End If

            myBBDB_AccessPaymentCommand.CommandText = sMySQL

            myBBDB_AccessPaymentReader = myBBDB_AccessPaymentCommand.ExecuteReader

            'Create the collection
            If myBBDB_AccessPaymentReader.HasRows Then
                Do While myBBDB_AccessPaymentReader.Read()
                    oPayment = oPayments.Add()
                    If bUseProfile Then
                        oPayment.VB_Profile = oProfile
                        bProfileInUse = True
                    Else
                        bProfileInUse = False
                    End If
                    If oPayment.RetrieveData(bSQLServer, myBBDB_AccessPaymentReader, myBBDB_AccessConnection, bOpenItemsOnly, iCompanyNo, lBabelFile_ID, lDBBatchIndex, lPayment_ID, True) Then
                        bReturnValue = True
                    Else
                        bReturnValue = False
                    End If
                Loop
            Else
                If bMatchedItemsOnly Then
                    'Not neseccary any open payments
                    bReturnValue = True
                End If
            End If

            'If Not myBBDB_AccessPaymentReader Is Nothing Then
            '    If Not myBBDB_AccessPaymentReader.IsClosed Then
            '        myBBDB_AccessPaymentReader.Close()
            '    End If
            '    myBBDB_AccessPaymentReader = Nothing
            'End If
            'If Not myBBDB_AccessPaymentCommand Is Nothing Then
            '    myBBDB_AccessPaymentCommand.Dispose()
            '    myBBDB_AccessPaymentCommand = Nothing
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)

        Finally
            If Not myBBDB_AccessPaymentReader Is Nothing Then
                If Not myBBDB_AccessPaymentReader.IsClosed Then
                    myBBDB_AccessPaymentReader.Close()
                End If
                myBBDB_AccessPaymentReader = Nothing
            End If
            If Not myBBDB_AccessPaymentCommand Is Nothing Then
                myBBDB_AccessPaymentCommand.Dispose()
                myBBDB_AccessPaymentCommand = Nothing
            End If

        End Try

        '            RetrieveData = bReturnValue

        '            Exit Function

        'errorRetrieveData:
        '            If Not myBBDB_AccessPaymentReader Is Nothing Then
        '                If Not myBBDB_AccessPaymentReader.IsClosed Then
        '                    myBBDB_AccessPaymentReader.Close()
        '                End If
        '                myBBDB_AccessPaymentReader = Nothing
        '            End If
        '            If Not myBBDB_AccessPaymentCommand Is Nothing Then
        '                myBBDB_AccessPaymentCommand.Dispose()
        '                myBBDB_AccessPaymentCommand = Nothing
        '            End If

        '            If Err.Number < 999 Then
        '                'Err is raised in Payment, Invoice or Freetext
        '                Err.Raise(Err.Number, Err.Source, Err.Description)
        '            Else
        '                Err.Raise(999, "Batch", Err.Description)
        '            End If

        Return bReturnValue

    End Function
    ' XNET 19.06.2013
    Public Sub DeleteExported()
        ' Delete payments with Exported = True
        Dim lCounter As Long
        For lCounter = oPayments.Count To 1 Step -1
            If oPayments.Item(lCounter).Exported Then
                oPayments.Remove(lCounter)
            End If
        Next lCounter
        ' then, make sure the Index property is updated correctly
        ReIndexPayments()
    End Sub
    Public Function DELETE_FI_RetrieveData(ByRef cnProfile As String, ByRef rsBatch As String, ByRef iCompanyNo As Short, ByRef lBabelFile_ID As Integer) As Boolean
        '    Public Function FI_RetrieveData(ByRef cnProfile As ADODB.Connection, ByRef rsBatch As ADODB.Recordset, ByRef iCompanyNo As Short, ByRef lBabelFile_ID As Integer) As Boolean
        'TODO: THIS FUNCTION IS NOT CONVERTED IN VB.NET


        '        ' Retrieve data necessary for the DnBTBI FI_Project
        '        Dim lCounter As Integer
        '        Dim lFreeCounter As Integer
        '        Dim oPayment As Payment
        '        Dim oInvoice As Invoice
        '        Dim oFreeText As Freetext
        '        Dim sMySQL As String
        '        Dim rsPayment As New ADODB.Recordset
        '        Dim rsFreetexts As New ADODB.Recordset
        '        Dim lDBBatchIndex As Integer
        '        Dim bReturnValue As Boolean

        '        On Error GoTo errorFI_RetrieveData

        '        bReturnValue = False
        '        'Retrieve and save the data to the local variables

        '        lDBBatchIndex = rsBatch.Fields("Batch_ID").Value
        '        nIndex = rsBatch.Fields("Batch_ID").Value
        '        'nSequenceNoStart = rsBatch!SequenceNoStart
        '        'nSequenceNoEnd = rsBatch!SequenceNoEnd
        '        sStatusCode = rsBatch.Fields("StatusCode").Value
        '        'sVersion = rsBatch!Version
        '        'sOperator = rsBatch!Operator
        '        sFormatType = rsBatch.Fields("FormatType").Value
        '        'sI_EnterpriseNo = rsBatch!I_EnterpriseNo
        '        'sI_Branch = rsBatch!I_Branch
        '        dDATE_Production = StringToDate(rsBatch.Fields("DATE_Production").Value)
        '        'sREF_Own = rsBatch!REF_Own
        '        'sREF_Bank = rsBatch!REF_Bank
        '        nMON_TransferredAmount = rsBatch.Fields("MON_TransferredAmount").Value
        '        nMON_InvoiceAmount = rsBatch.Fields("MON_InvoiceAmount").Value
        '        bAmountSetTransferred = rsBatch.Fields("AmountSetTransferred").Value
        '        bAmountSetInvoice = rsBatch.Fields("AmountSetInvoice").Value
        '        nNo_Of_Transactions = rsBatch.Fields("No_Of_Transactions").Value
        '        nNo_Of_Records = rsBatch.Fields("No_Of_Records").Value
        '        iImportFormat = rsBatch.Fields("ImportFormat").Value

        '        sMySQL = "SELECT P.I_Name as I_Name, P.I_Account AS I_Account, P.Payment_ID AS Payment_ID, "
        '        sMySQL = sMySQL & " P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, "
        '        sMySQL = sMySQL & " P.MON_InvoiceAmount AS MON_InvoiceAmount, P.Exported as Exported, "
        '        sMySQL = sMySQL & " P.Filenameout_id AS Filenameout_id, "
        '        sMySQL = sMySQL & " P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, p.E_Adr2 AS E_Adr2, "
        '        sMySQL = sMySQL & " P.E_City AS E_City, P.E_Zip AS E_Zip, P.Ref_Bank1 AS Ref_Bank1, P.Ref_Bank2 as Ref_Bank2, P.PayCode AS PayCode, "
        '        sMySQL = sMySQL & " I.Unique_ID AS Unique_ID, I.MON_InvoiceAmount AS InvMON_InvoiceAmount FROM Payment P, Invoice I "
        '        sMySQL = sMySQL & " WHERE P.Company_ID = " & Trim(Str(iCompanyNo))
        '        sMySQL = sMySQL & " AND P.BabelFile_ID = " & Trim(Str(lBabelFile_ID))
        '        sMySQL = sMySQL & " AND P.Batch_ID = " & Trim(Str(lDBBatchIndex))

        '        'If one specific Payment_ID is stated, retrieve just that one.
        '        'If lPayment_ID > 0 Then
        '        '    sMySQL = sMySQL & " AND P.Payment_ID = " & Trim$(Str(lPayment_ID))
        '        'End If
        '        sMySQL = sMySQL & " AND P.MATCH_Matched = " & Trim(Str(BabelFiles.MatchStatus.Matched))
        '        sMySQL = sMySQL & " AND P.BabelFile_ID = I.BabelFile_ID"
        '        sMySQL = sMySQL & " AND P.Batch_ID = I.Batch_ID"
        '        sMySQL = sMySQL & " AND P.Payment_ID = I.Payment_ID"

        '        rsPayment.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        '        'Create the collection
        '        If rsPayment.RecordCount > 0 Then
        '            oPayments = New Payments
        '            rsPayment.MoveFirst()
        '            For lCounter = 1 To rsPayment.RecordCount
        '                oPayment = oPayments.Add(Trim(Str(rsPayment.Fields("Payment_ID").Value)))
        '                oInvoice = oPayment.Invoices.Add("1")
        '                bProfileInUse = True
        '                oPayment.VB_Profile = oProfile
        '                oPayment.I_Name = rsPayment.Fields("I_Name").Value
        '                oPayment.I_Account = rsPayment.Fields("I_Account").Value
        '                oPayment.DATE_Payment = rsPayment.Fields("DATE_Payment").Value
        '                oPayment.DATE_Value = rsPayment.Fields("DATE_Value").Value
        '                oPayment.MON_InvoiceAmount = rsPayment.Fields("MON_InvoiceAmount").Value
        '                oPayment.Exported = rsPayment.Fields("Exported").Value
        '                oPayment.VB_FilenameOut_ID = rsPayment.Fields("FilenameOut_ID").Value
        '                oInvoice.Unique_Id = rsPayment.Fields("Unique_Id").Value
        '                oInvoice.MON_InvoiceAmount = rsPayment.Fields("InvMON_InvoiceAmount").Value
        '                ' New 01.02.05
        '                oPayment.E_Name = rsPayment.Fields("E_Name").Value
        '                oPayment.E_Adr1 = rsPayment.Fields("E_Adr1").Value
        '                oPayment.E_Adr2 = rsPayment.Fields("E_Adr2").Value
        '                oPayment.E_Zip = rsPayment.Fields("E_Zip").Value
        '                oPayment.E_City = rsPayment.Fields("E_City").Value
        '                oPayment.PayCode = rsPayment.Fields("PayCode").Value
        '                oPayment.REF_Bank1 = rsPayment.Fields("REF_Bank1").Value
        '                oPayment.REF_Bank2 = rsPayment.Fields("REF_Bank2").Value

        '                ' Added 09.02.05
        '                ' For UDUS, we also need Freetext.Text
        '                If oPayment.PayCode = "180" Then

        '                    sMySQL = "SELECT XText FROM Freetext "
        '                    sMySQL = sMySQL & " WHERE Company_ID = " & Trim(Str(iCompanyNo))
        '                    sMySQL = sMySQL & " AND BabelFile_ID = " & Trim(Str(lBabelFile_ID))
        '                    sMySQL = sMySQL & " AND Batch_ID = " & Trim(Str(lDBBatchIndex))
        '                    sMySQL = sMySQL & " AND Payment_ID = " & Trim(Str(oPayment.Index))
        '                    sMySQL = sMySQL & " AND Invoice_ID = 1"
        '                    rsFreetexts.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        '                    If rsFreetexts.RecordCount > 0 Then
        '                        rsFreetexts.MoveFirst()
        '                        For lFreeCounter = 1 To rsFreetexts.RecordCount
        '                            oFreeText = oInvoice.Freetexts.Add(Str(lFreeCounter))
        '                            oFreeText.Text = rsFreetexts.Fields("XText").Value
        '                            rsFreetexts.MoveNext()
        '                        Next lFreeCounter
        '                    End If
        '                    rsFreetexts.Close()
        '                End If

        '                rsPayment.MoveNext()
        '            Next lCounter
        '        Else
        '            bReturnValue = True
        '        End If

        '        'UPGRADE_NOTE: Object rsPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        rsPayment = Nothing
        '        'UPGRADE_NOTE: Object rsFreetexts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '        rsFreetexts = Nothing


        '        FI_RetrieveData = bReturnValue

        '        Exit Function

        'errorFI_RetrieveData:
        '        If Not rsPayment Is Nothing Then
        '            'UPGRADE_NOTE: Object rsPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '            rsPayment = Nothing
        '        End If
        '        If Not rsFreetexts Is Nothing Then
        '            'UPGRADE_NOTE: Object rsFreetexts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        '            rsFreetexts = Nothing
        '        End If
        '        If Err.Number < 999 Then
        '            'Err is raised in Payment, Invoice or Freetext
        '            Err.Raise(Err.Number, Err.Source, Err.Description)
        '        Else
        '            Err.Raise(999, "Batch FI_RetrieveData", Err.Description)
        '        End If

    End Function
    Private Function ReadDnBNattsafe() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String
        Dim sTempDate As String
        Dim sKKDate As String
        Dim dKKDate As Date
        Dim bAddNewPayment, bDateExistInKK As Boolean
        Dim sLocalPayType As String
        Dim dDATE_Value As Date


        bFromLevelUnder = False

        Do While True
            bReadLine = False
            bAddNewPayment = True


            Select Case xDelim(sImportLineBuffer, ",", 1)
                'TOT - Accountlevel
                Case "TOT"

                    ' Import only TP-records at batchlevel (totals)
                    ' read values into batchobject
                    If xDelim(sImportLineBuffer, ",", 4) = "TP" Then
                        sI_Account = xDelim(sImportLineBuffer, ",", 2) ' Til konto
                        sI_Branch = xDelim(sImportLineBuffer, ",", 4) ' Avdelingsnummer

                        'nMON_TransferredAmount = nMON_TransferredAmount + CDbl(Replace(xDelim(sImportLineBuffer, ",", 6), ".", ""))           'Opptalt bel�p
                        'nMON_InvoiceAmount = nMON_InvoiceAmount + CDbl(Replace(xDelim(sImportLineBuffer, ",", 6), ".", ""))          'Opptalt

                        sTempDate = xDelim(sImportLineBuffer, ",", 7) ' Valuteringsdato YYYYMMDD
                        dDATE_Production = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))

                        sImportLineBuffer = ReadDnBNattsafeRecord(oFile) ', oProfile)
                        Exit Do

                    Else
                        bCreateObject = False
                        bReadLine = True
                    End If

                    'INN - Payment
                Case "INN"

                    ' Try to extract date from KK-field
                    ' Date should be pos 6,4 as DDMM
                    ' Check if avdelingsnr is equal in the avdeling-field and in the KK-felt

                    sTempDate = xDelim(sImportLineBuffer, ",", 9) ' Valuteringsdato YYYYMMDD
                    dDATE_Value = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))

                    sLocalPayType = "650"
                    If xDelim(sImportLineBuffer, ",", 4) = LStrip(Mid(xDelim(sImportLineBuffer, ",", 10), 4, 2), "0") Then
                        ' Check if Veksel:

                        If Right(xDelim(sImportLineBuffer, ",", 10), 2) = "99" Then
                            '-----------------------------------
                            ' Match VEKSEL agaist other accounts
                            '-----------------------------------
                            bDateExistInKK = True
                            sLocalPayType = "651"
                            sKKDate = Mid(xDelim(sImportLineBuffer, ",", 10), 6, 2)

                            If Len(sKKDate) <> 2 Then
                                bDateExistInKK = False
                            Else
                                If Val(sKKDate) > 31 Then
                                    bDateExistInKK = False
                                End If
                            End If

                            If bDateExistInKK Then
                                ' Date as day in pos 6,2
                                dKKDate = DateSerial(Year(Now), Month(Now), CInt(sKKDate))
                                ' Check if we have jumped into a new year;
                                If dKKDate > System.DateTime.FromOADate(Now.ToOADate + 10) Then
                                    ' Deduct 1 form year-part;
                                    dKKDate = DateSerial(Year(Now) - 1, Month(Now), CInt(sKKDate))
                                End If
                                ' Check if we have jumped into a new month
                                If dKKDate > System.DateTime.FromOADate(Now.ToOADate + 10) Then
                                    ' Deduct 1 form month-part;
                                    If Month(Now) > 1 Then
                                        dKKDate = DateSerial(Year(Now), Month(Now) - 1, CInt(Left(sKKDate, 2)))
                                    End If
                                End If
                                ' Validate against ValueDate;
                                If Not (dKKDate > dDATE_Value Or dKKDate < System.DateTime.FromOADate(dDATE_Value.ToOADate - 5)) Then
                                    ' Date OK, use this one;
                                    sKKDate = DateToString(dKKDate)
                                Else
                                    sKKDate = DateToString(DateSerial(1990, 1, 1))
                                End If
                            Else
                                sKKDate = DateToString(DateSerial(1990, 1, 1))
                            End If

                        Else
                            '------------------
                            ' Ordinary Nattsafe
                            '------------------
                            bDateExistInKK = True
                            sKKDate = Mid(xDelim(sImportLineBuffer, ",", 10), 6, 4)

                            'Check if it is a valid date. If not set date equal to DATE_Production
                            If Len(sKKDate) <> 4 Then
                                bDateExistInKK = False
                            Else
                                If Val(Right(sKKDate, 2)) > 12 Then
                                    bDateExistInKK = False
                                End If
                                If Val(Left(sKKDate, 2)) > 31 Then
                                    bDateExistInKK = False
                                End If
                            End If

                            If bDateExistInKK Then
                                dKKDate = DateSerial(Year(Now), CInt(Right(sKKDate, 2)), CInt(Left(sKKDate, 2)))
                                ' Check if we have jumped to next year (re first part of january
                                If dKKDate > System.DateTime.FromOADate(Now.ToOADate + 10) Then
                                    ' Deduct 1 form year-part;
                                    dKKDate = DateSerial(Year(Now) - 1, CInt(Right(sKKDate, 2)), CInt(Left(sKKDate, 2)))
                                End If
                                ' Validate against ValueDate;
                                If Not (dKKDate > dDATE_Value Or dKKDate < System.DateTime.FromOADate(dDATE_Value.ToOADate - 15)) Then
                                    ' Date OK, use this one;
                                    sKKDate = DateToString(dKKDate)
                                Else
                                    'Set date to 1/1-1990
                                    sKKDate = DateToString(DateSerial(1990, 1, 1))
                                End If
                            Else
                                'sTempDate = xDelim(sImportLineBuffer, ",", 9) ' Valuteringsdato YYYYMMDD
                                'Use Innleveringsdato as date
                                'sKKDate = DateToString(DateSerial(Mid$(sTempDate, 1, 4), Mid$(sTempDate, 5, 2), Mid$(sTempDate, 7, 2)))
                                'Set date to 1/1-1990
                                sKKDate = DateToString(DateSerial(1990, 1, 1))
                            End If

                        End If
                    Else
                        If Right(xDelim(sImportLineBuffer, ",", 10), 2) = "99" Then
                            sLocalPayType = "651"
                        Else
                            sLocalPayType = "650"
                        End If
                        sKKDate = DateToString(DateSerial(1990, 1, 1))
                    End If

                    bReadLine = False
                    ' Create a payment for each date
                    ' Check if payment (date) is created previously
                    bCreateObject = True
                    bAddNewPayment = True

                    ' Next lines commented out 04.08.04 by JanP
                    ' - Treat each bag as one payment from now on
                    '-----------------
                    'For Each oPayment In Payments
                    '    If oPayment.DATE_Value = sKKDate Then
                    '        If oPayment.PayCode = sLocalPayType Then
                    '            bAddNewPayment = False
                    '            Exit For
                    '        End If
                    '    End If
                    'Next oPayment

                Case Else
                    Exit Do
            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadDnBNattsafeRecord(oFile) ', oProfile)
            End If

            If bCreateObject Then
                If bAddNewPayment Then
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                Else
                    'Unneseccary?
                    For Each oPayment In Payments
                        If oPayment.DATE_Value = sKKDate Then
                            If oPayment.PayCode = sLocalPayType Then
                                oPayment.objFile = oFile
                                Exit For
                            End If
                        End If
                    Next oPayment
                End If
            End If

            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

        Loop

        AddTransferredAmountOnBatch()
        AddInvoiceAmountOnBatch()

        ReadDnBNattsafe = sImportLineBuffer
    End Function
    Private Function ReadSecuritasNattsafe() As String
        Dim oPayment As Payment
        'Variables used to set properties in the payment-object
        Dim sI_Account As String
        Dim sTempDate As String

        ''''''''   ' jump to babelFile for each new Department, to create another batch;
        '15.04.2005 - changed to jump to Babelfile for each 'bag'.
        ''''''''    sDepartment = xDelim(sImportLineBuffer, ";", 4)
        ''''''''    If sREF_Own <> sDepartment Then
        ''''''''        bReadLine = False
        ''''''''        If bFromLevelUnder Then
        ''''''''            bCreateObject = False
        ''''''''            ' jump to babelfile
        ''''''''            Exit Do
        ''''''''        End If
        ''''''''    End If
        sI_Account = xDelim(sImportLineBuffer, ";", 4) ' Til konto
        sI_Branch = xDelim(sImportLineBuffer, ";", 5) ' Avdeling

        '        nMON_TransferredAmount = nMON_TransferredAmount + CDbl(xDelim(sImportLineBuffer, ";", 8)) + CDbl(xDelim(sImportLineBuffer, ";", 9))            '9 shows diffamount
        '        nMON_InvoiceAmount = nMON_InvoiceAmount + CDbl(xDelim(sImportLineBuffer, ";", 8))            'Opptalt bel�p
        sTempDate = xDelim(sImportLineBuffer, ";", 8) ' Valdato
        dDATE_Production = DateSerial(CInt(Mid(sTempDate, 1, 4)), CInt(Mid(sTempDate, 5, 2)), CInt(Mid(sTempDate, 7, 2)))


        Do While True
            'bReadLine = True
            '    bCreateObject = True
            '
            '
            '    If bCreateObject Then
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            '    Else
            '        Set oPayment.objFile = oFile
            '        sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            '    End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Then
                    Exit Do
                End If
            Else
                'Changed 15.04.2005 by Kjell
                ' Make a new batch for every entry in the file.
                'Do this by setting a sI_branch to trigger Exit Do
                sI_Branch = "EXIT DO"
                If sI_Branch <> xDelim(sImportLineBuffer, ";", 5) Then
                    Exit Do
                End If
                If sTempDate <> xDelim(sImportLineBuffer, ";", 8) Then
                    'sTempDate still keeps the date on Securitas-format
                    Exit Do
                End If
            End If
        Loop


        AddTransferredAmountOnBatch()
        AddInvoiceAmountOnBatch()

        ReadSecuritasNattsafe = sImportLineBuffer

    End Function
    Private Function ReadDnBTBUKIncoming() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sCurrency As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left(sImportLineBuffer, 8)
                Case "940SWI01"
                    ' Find currency here!
                    sCurrency = CStr(oCargo.MONCurrency = Mid(sImportLineBuffer, 117, 3))

                Case "940SWI02"
                    ' Transactionrecord
                    If bFromLevelUnder Then
                        ' New batch for each SWI02
                        Exit Do
                    Else
                        'Extract data from sImportLineBuffer
                        sREF_Bank = Mid(sImportLineBuffer, 123, 16) ' Avsenderbanks ref
                        sREF_Own = Mid(sImportLineBuffer, 141, 16) ' Bank-Ref
                        sBatch_ID = Mid(sImportLineBuffer, 80, 8) ' Kontoutskr.nr

                        bReadLine = False
                        bCreateObject = True

                    End If

                Case "940SWI04"
                    ' Payers referencerecord
                    ' should not show up here
                    bReadLine = False
                    bCreateObject = True
                    MsgBox("Batch, SWI04!!")

                Case "940SWI05"
                    ' Outgoing balance
                    ' Up to BabelFile
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                oPayment.MON_TransferCurrency = oCargo.MONCurrency
                oPayment.MON_AccountCurrency = oCargo.MONCurrency
                oPayment.MON_OriginallyPaidCurrency = oCargo.MONCurrency
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount
        ReadDnBTBUKIncoming = sImportLineBuffer
    End Function
    Private Function ReadNordea_Innbet_Ktoinf_utl() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sTmp As String
        Dim nAmount As Double
        Dim bPositiveAmount As Boolean
        Dim bEndOfPayment As Boolean

        bFromLevelUnder = False
        bPositiveAmount = False

        Do While True
            bReadLine = False

            Select Case Left(sImportLineBuffer, 2)
                Case "21"
                    If bFromLevelUnder Then
                        ' New batch for each SWI02
                        Exit Do
                    Else
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-001")

                        Exit Do
                    End If

                Case "25" 'Transactionrecord - Create a new Payment
                    'Extract data from sImportLineBuffer
                    sTmp = Mid(sImportLineBuffer, 19, 17)
                    If IsNumeric(Right(sTmp, 1)) Then
                        nAmount = Val(sTmp)
                    Else
                        nAmount = TranslateSignedAmount(sTmp)
                    End If
                    If nAmount < 0 Then
                        'Calculate the sum of transactions
                        oCargo.MON_InvoiceAmount = oCargo.MON_InvoiceAmount + nAmount
                        'Don't import negative payments
                        bEndOfPayment = False
                        Do Until bEndOfPayment
                            sImportLineBuffer = oFile.ReadLine()
                            Select Case Left(sImportLineBuffer, 2)

                                Case "25", "27"
                                    'New payment, or UB
                                    bEndOfPayment = True
                                    bReadLine = False
                                    bCreateObject = False

                                Case "26"
                                    'Still same payment, keep on reading

                                Case Else '21, 23
                                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-001")
                                    Exit Do

                            End Select

                        Loop
                    Else
                        'Calculate the sum of transactions
                        oCargo.MON_InvoiceAmount = oCargo.MON_InvoiceAmount + nAmount

                        bReadLine = False
                        bCreateObject = True
                    End If

                Case "27" 'Last record on this I_Account
                    Exit Do

                Case Else
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-001")

                    Exit Do

            End Select

            If bReadLine Then
                sImportLineBuffer = oFile.ReadLine()
            End If

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()
        ReadNordea_Innbet_Ktoinf_utl = sImportLineBuffer
    End Function
    Private Function ReadDnBTBWK() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sCurrency As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left(sImportLineBuffer, 8)
                Case "940SWI01"
                    ' Save Incoming Balance
                    nMON_BalanceIN = Val(Mid(sImportLineBuffer, 120, 15))
                    If Mid(sImportLineBuffer, 110, 1) = "D" Then
                        nMON_BalanceIN = nMON_BalanceIN * -1
                    End If

                    ' Set in SWI05 !! dDATE_Production = DateSerial(VB6.Format(Now, "YYYY"), Mid$(sImportLineBuffer, 111, 2), Mid$(sImportLineBuffer, 113, 2))
                    oCargo.I_Account = Mid(sImportLineBuffer, 42, 35)
                    oCargo.MONCurrency = Mid(sImportLineBuffer, 117, 3)
                    sI_EnterpriseNo = Mid(sImportLineBuffer, 9, 11)

                    ' Kontoutskriftsnr
                    sBatch_ID = Mid(sImportLineBuffer, 80, 8)
                    bReadLine = True
                    bCreateObject = False

                Case "940SWI02"
                    ' New payment for each SWI02
                    bCreateObject = True
                    bReadLine = False

                Case "940SWI03"
                    ' Payers referencerecord
                    ' should not show up here
                    bReadLine = False
                    bCreateObject = True
                    MsgBox("Batch, SWI03!!")

                Case "940SWI05"
                    ' Outgoing balance
                    dDATE_Production = DateSerial(CInt("20" & Mid(sImportLineBuffer, 120, 2)), CInt(Mid(sImportLineBuffer, 122, 2)), CInt(Mid(sImportLineBuffer, 124, 2)))
                    ' Save Outgoing Balance
                    nMON_BalanceOUT = Val(Mid(sImportLineBuffer, 101, 15)) ' 12.07.05 Rettet fra 129, 15))
                    If Mid(sImportLineBuffer, 91, 1) = "D" Then ' 12.07.05 Rettet fra 119, 1) = "D"
                        nMON_BalanceOUT = nMON_BalanceOUT * -1
                    End If
                    nMON_TransferredAmount = nMON_BalanceOUT ' To show outgoing balance in browser

                    bReadLine = False
                    bCreateObject = False
                    ' jump to babelFile
                    Exit Do

                Case "940SWI07"
                    ' Payers referencerecord
                    ' should not show up here
                    bReadLine = False
                    bCreateObject = True
                    MsgBox("Batch, SWI07!!")

                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBTBWKRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadDnBTBWKRecord(oFile)
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadDnBTBWK = sImportLineBuffer
    End Function
    Private Function ReadAquariusOutgoing() As String
        ' File from Aquarius for SG FinansDim oPayment As Payment
        Dim oPayment As vbBabel.Payment
        Dim oFreeText As Freetext
        Dim bFromLevelUnder, bx As Boolean
        Dim sResponse As String
        Dim iResponse As Short
        Dim bAbandon As Boolean
        Dim bOutOfLoop As Boolean

        bFromLevelUnder = False

        sFormatType = "AquariusOutgoing"
        bAbandon = False

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ' 22.02.2017
            ' KID for NOK payments inside Norway only!
            If oPayment.Invoices.Count > 0 Then
                If Not EmptyString(oPayment.Invoices(1).Unique_Id) Then
                    If Not IsPaymentDomestic(oPayment, , , , True) Then
                        ' redo from KID to freetext
                        oFreeText = oPayment.Invoices(1).Freetexts.Add
                        oFreeText.Text = oPayment.Invoices(1).Unique_Id
                        oPayment.Invoices(1).Unique_Id = ""
                    End If
                End If
            End If

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        If Not bAbandon Then
            bx = AddInvoiceAmountOnBatch()
        Else
            Err.Raise(10000, "ReadAquariusOutgoing", "Brukeren har avbrutt prosessen")
        End If


    End Function
    Private Function ReadNewphoneFactoring() As String
        '-----------------------------------------------
        ' Factoringfile from Newphone used by SG Finans
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean


        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            Select Case Mid(sImportLineBuffer, 1, 2)


                Case "F;" ' Fakturarecord
                    bReadLine = False
                    bCreateObject = True

                Case "S;" ' sluttrecord
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()
        ReadNewphoneFactoring = sImportLineBuffer
    End Function
    Private Function ReadSG_0000778_LadeDekkserviceFactoringCustomerFile() As String
        '-------------------------------------------------------
        ' Customerfile factoring from 0000778 LadeDekkservice used by SG Finans
        ' All records from the "customerfile" will be the basis
        ' for both the Invoicefile and the Customerfile to SG Finans.
        ' Must then create TWO records, one Invoice and one Customer, for each imported record !!!
        '-------------------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False
        ' Important, update oCargo.Paycode to 710, tell that first "run" is to create a Customerrecord
        oCargo.PayCode = "710"

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()
        ReadSG_0000778_LadeDekkserviceFactoringCustomerFile = sImportLineBuffer
    End Function
    Private Function ReadNewphoneFactoringCustomerFile() As String
        '-------------------------------------------------------
        ' Customerfile factoring from Newphone used by SG Finans
        ' 23.02.07
        ' Things have changed dramatically! Now all records from the "customerfile" will be the basis
        ' for both the Invoicefile and the Customerfile to SG Finans.
        ' Must then create TWO records, one Invoice and one Customer, for each imported record !!!
        '-------------------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False
        ' Important, update oCargo.Paycode to 710, tell that first "run" is to create a Customerrecord
        oCargo.PayCode = "710"

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "" Then
                Exit Do
            End If

            Select Case Mid(sImportLineBuffer, 1, 2)

                Case "98" ' Kunderecord
                    sFormatType = "Newphone"
                    bReadLine = False
                    bCreateObject = True

                Case "S;" ' Slutt p� fil
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If


            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadNewphoneCustomerRecord(oFile) ', oProfile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        AddInvoiceAmountOnBatch()
        ReadNewphoneFactoringCustomerFile = sImportLineBuffer
    End Function
    Private Function ReadSGFinansFactoring() As String
        '-----------------------------------------------
        ' Standard Factoringfile from SGFinans
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                ' there may be files with a mix of Fakturarecords and Kunderecords,
                ' thus, we must first test what kind of line we have just read;
                If Left(sImportLineBuffer, 5) = "K9409" Or (Left(sImportLineBuffer, 1) = "K" And Mid(sImportLineBuffer, 3, 4) = "9409") Or Mid(sImportLineBuffer, 5, 4) = "9409" Then
                    oPayment.ImportFormat = BabelFiles.FileType.SGFinans_Kunde
                Else
                    oPayment.ImportFormat = BabelFiles.FileType.SGFinans_Faktura
                End If

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSGFinansFactoring = sImportLineBuffer
    End Function
    Private Function ReadSGFinansCustomerFile() As String
        '-------------------------------------------------------
        ' Customerfile factoring from SGFinans used by SG Finans
        '-------------------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "" Then
                Exit Do
            End If

            sFormatType = "SG Finans Kundefil"
            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                ' there may be files with a mix of Fakturarecords and Kunderecords,
                ' thus, we must first test what kind of line we have just read;
                If Left(sImportLineBuffer, 5) = "K9409" Or (Left(sImportLineBuffer, 1) = "K" And Mid(sImportLineBuffer, 3, 4) = "9409") Or (Mid(sImportLineBuffer, 2, 1) = "K" And Mid(sImportLineBuffer, 5, 4) = "9409") Or Mid(sImportLineBuffer, 4, 4) = "9409" Then
                    oPayment.ImportFormat = BabelFiles.FileType.SGFinans_Kunde
                Else
                    oPayment.ImportFormat = BabelFiles.FileType.SGFinans_Faktura
                End If

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        ReadSGFinansCustomerFile = sImportLineBuffer
    End Function
    Private Function Read_0000127_SkaarTransFactoring() As String
        '-----------------------------------------------
        ' Special Factoringfile from 0000127 SkaarTrans
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_0000127_SkaarTransFactoring = sImportLineBuffer
    End Function
    Private Function Read_0000127_SkaarTransCustomer() As String
        '-----------------------------------------------
        ' Special customerfile from 0000127 SkaarTrans
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "" Then
                Exit Do
            End If

            sFormatType = "SG Finans Kundefil"
            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        Read_0000127_SkaarTransCustomer = sImportLineBuffer
    End Function
    Private Function Read_0001471_WebergFactoring() As String
        '-----------------------------------------------
        ' Special Factoringfile from 0001471_WebergFactoring
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_0001471_WebergFactoring = sImportLineBuffer
    End Function
    Private Function Read_0001471_WebergCustomer() As String
        '-----------------------------------------------
        ' Special customerfile from 0001471_WebergCustomer
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "" Then
                Exit Do
            End If

            sFormatType = "SG Finans Kundefil"
            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        Read_0001471_WebergCustomer = sImportLineBuffer
    End Function
    Private Function Read_SG_NORFinans_Factoring() As String
        '-----------------------------------------------
        ' Old factoringformat for NORFinans
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_SG_NORFinans_Factoring = sImportLineBuffer
    End Function
    Private Function Read_0004889_MillbaCustomer() As String
        '-----------------------------------------------
        ' Special customerfile from 0004889 Millba
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sSeparator As String

        bFromLevelUnder = False
        sSeparator = Space(1)

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "" Then
                Exit Do
            End If

            sFormatType = "SG Finans Kundefil"
            bReadLine = False

            ' Create Payment only when
            ' filled in CustomerNo, Name and City, else fail
            If Not EmptyString(xDelim(sImportLineBuffer, sSeparator, 1, Chr(34))) And Not EmptyString(xDelim(sImportLineBuffer, sSeparator, 2, Chr(34))) And Not EmptyString(xDelim(sImportLineBuffer, sSeparator, 6, Chr(34))) Then
                bCreateObject = True
                bReadLine = False
            Else
                bCreateObject = False
                bReadLine = True
                If oFile.AtEndOfStream Then
                    sImportLineBuffer = ""
                    Exit Do
                End If
            End If


            If bReadLine Then
                'Read new record
                sImportLineBuffer = Read_0004889_MillbaCustomerRecord(oFile, "?", False) ', oProfile)
            End If


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        Read_0004889_MillbaCustomer = sImportLineBuffer
    End Function
    Private Function Read_0004193_T�nsbergRammeCustomer() As String
        '-----------------------------------------------
        ' Special customerfile from 0004193_T�nsbergRamme
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sSeparator As String

        bFromLevelUnder = False
        sSeparator = ";"

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "" Then
                Exit Do
            End If

            sFormatType = "SG Finans Kundefil"
            bReadLine = False

            ' Create Payment only when
            ' filled in CustomerNo, Name and City, else fail
            If Not EmptyString(xDelim(sImportLineBuffer, sSeparator, 1, Chr(34))) And Not EmptyString(xDelim(sImportLineBuffer, sSeparator, 3, Chr(34))) And Not EmptyString(xDelim(sImportLineBuffer, sSeparator, 6, Chr(34))) Then
                bCreateObject = True
                bReadLine = False
            Else
                bCreateObject = False
                bReadLine = True
                If oFile.AtEndOfStream Then
                    sImportLineBuffer = ""
                    Exit Do
                End If
            End If


            If bReadLine Then
                'Read new record
                sImportLineBuffer = Read_0004193_T�nsbergRammeCustomerRecord(oFile, "?", False) ', oProfile)
            End If


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        Read_0004193_T�nsbergRammeCustomer = sImportLineBuffer
    End Function
    Private Function Read_0001327_StrandCoFactoring() As String
        '-----------------------------------------------
        ' Special Factoringfile from 0001327 StrandCo
        '-----------------------------------------------

        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'WARNING! Is this correct
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_0001327_StrandCoFactoring = sImportLineBuffer
    End Function
    Private Function Read_DnBNORUS_ACH_Incoming() As String
        '-----------------------------------------------
        ' ACH Lockboxfile from DnBNOR US
        '-----------------------------------------------
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sI_Account As String
        Dim sISO As String
        Dim lNumberOfEntryDetailsRecords As Long, lCountedNoOfEntryDetilsRecords As Long
        Dim sEntryHash As String, nCalculatedBankIdentifications As Double
        Dim nMON_InvoiceDebitAmount As Double
        Dim nMON_InvoiceCreditAmount As Double
        Dim sTemp_Text_E_Statement As String
        Dim sTemp_PayCode As String
        Dim dTempDATE_Payment As Date
        Dim lBatchcounter As Long
        Dim sBankName As String
        Dim sEname As String
        Dim sExtraD1 As String
        Dim sExtraD2 As String
        Dim sExtraD5 As String   'XokNET 26.03.2015
        Dim sBankBranchNO As String
        Dim sVoucherNo As String
        Dim bDebit As Boolean
        Dim iOptionalPositionsAdded As Integer
        Dim bNegative As Boolean
        Dim sTemp As String
        Dim iPos As Integer
        Dim bExported As Boolean


        bFromLevelUnder = False
        sStatusCode = "02"

        Do

            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = vbNullString Or oFile.AtEndOfStream Then
                Exit Do
            End If

            Select Case Left$(sImportLineBuffer, 1)


                ' 19.05.2008 added also MT940, which starts with : in first post
                Case ":"
                    bCreateObject = False  ' Only True for :61:
                    Select Case Trim$(Mid$(sImportLineBuffer, 1, 4))
                        Case ":20:"
                            If bFromLevelUnder Then
                                ' Create new batch for each :20:
                                Exit Do
                            End If

                            ' XokNET 03.11.2011
                            If bProfileInUse Then
                                ' Fill in name from BabelBank setup
                                oCargo.I_Name = oProfile.CompanyName
                            End If
                            bReadLine = True

                        Case ":13D"
                            ' These are MT942's, daterecord
                            ' For DnBNOR US we do not need MT942's

                            oCargo.DATE_Payment = "20" & Mid$(sImportLineBuffer, 6, 6)
                            dDATE_Production = StringToDate(oCargo.DATE_Payment)
                            oCargo.REF = "MT942"
                            bReadLine = True

                        Case ":25:"
                            oCargo.I_Account = Trim$(RemoveCharacters(UCase(Mid$(sImportLineBuffer, 5, 17)), "ABCDEFGHIJKLMNOPQRSTUVWXYZ")) ' Numerics only, delete chars
                            ' XokNET 19.09.2011 - AccountNo's can be more than 8 digits. Keep last 8
                            If Len(oCargo.I_Account) Then
                                oCargo.I_Account = Right$(oCargo.I_Account, 8)
                            End If
                            bReadLine = True

                        Case ":61:"
                            ' :61:0712211221D4242456,02FCMS200712214895
                            If IsNumeric(Mid$(sImportLineBuffer, 11, 1)) Then  ' Valuedate is optional
                                If Mid$(sImportLineBuffer, 15, 1) = "D" Then 'Debit
                                    bDebit = True
                                    bReadLine = True
                                    'bCreateObject = False
                                ElseIf Mid$(sImportLineBuffer, 15, 1) = "R" Then 'Reverse transaction
                                    If Mid$(sImportLineBuffer, 16, 1) = "D" Then 'RD = Reversal of debit (credit entry)
                                        bDebit = False
                                        bReadLine = False
                                        'bCreateObject = True
                                        'bAtLeastOneTransaction = True
                                    Else 'RC = Reversal of credit (debit entry)
                                        bDebit = True
                                        bReadLine = True
                                        'bCreateObject = False
                                    End If
                                Else 'Credit
                                    bDebit = False
                                    bReadLine = False
                                    'bCreateObject = True
                                    'bAtLeastOneTransaction = True
                                End If
                            Else
                                If Mid$(sImportLineBuffer, 11, 1) = "D" Then
                                    bDebit = True
                                    bReadLine = True
                                    'bCreateObject = False
                                ElseIf Mid$(sImportLineBuffer, 15, 1) = "R" Then 'Reverse transaction
                                    If Mid$(sImportLineBuffer, 16, 1) = "D" Then 'RD = Reversal of debit (credit entry)
                                        bDebit = False
                                        bReadLine = False
                                        bCreateObject = True
                                        'bAtLeastOneTransaction = True
                                    Else 'RC = Reversal of credit (debit entry)
                                        bDebit = True
                                        bReadLine = True
                                        'bCreateObject = False
                                    End If
                                Else
                                    bDebit = False
                                    bReadLine = False
                                    'bCreateObject = True
                                    'bAtLeastOneTransaction = True
                                End If
                            End If
                            If IsNumeric(Mid$(sImportLineBuffer, 11, 1)) Then
                                iOptionalPositionsAdded = 4
                            End If
                            If Mid$(sImportLineBuffer, 15, 1) = "R" Then 'Reverse transaction
                                iOptionalPositionsAdded = iOptionalPositionsAdded + 1
                            End If
                            If Mid$(sImportLineBuffer, 11 + iOptionalPositionsAdded, 1) = "C" Then
                                bNegative = False
                            Else
                                bNegative = True
                            End If
                            If Not IsNumeric(Mid$(sImportLineBuffer, 12 + iOptionalPositionsAdded, 1)) Then
                                'No idea what this field is
                                'Described as: The third position of the currency designation, if this is important for differentiation purposes
                                iOptionalPositionsAdded = iOptionalPositionsAdded + 1
                            Else
                                'Nothing to do
                            End If
                            iPos = InStr(12 + iOptionalPositionsAdded, sImportLineBuffer, ",", vbTextCompare)
                            If iPos = 0 Then
                                Err.Raise(1, "ReadMT940,", "No amount found")
                            Else
                                sTemp = Mid$(sImportLineBuffer, 12 + iOptionalPositionsAdded, iPos + 3 - 12 - iOptionalPositionsAdded)
                                'iOptionalPositionsAdded = iOptionalPositionsAdded + Len(sAmount)
                                'sometimes ,00 is not stated in the amount, check for this
                                If Not vbIsNumeric(Mid$(sTemp, Len(sTemp) - 1, 1), "0123456789") Then
                                    sTemp = Left$(sTemp, Len(sTemp) - 2) & "00"
                                ElseIf Not vbIsNumeric(Right$(sTemp, 1), "0123456789") Then
                                    sTemp = Left$(sTemp, Len(sTemp) - 1) & "0"
                                End If
                            End If
                            ' added + 1 27.05.2008, missed one pos before that, which indicates soemthing other than code, next 3 is code
                            iPos = 12 + iOptionalPositionsAdded + Len(sTemp) + 1
                            sTemp_PayCode = Mid$(sImportLineBuffer, iPos, 3)
                            '''''sVersion = sTemp_PayCode
                            iPos = iPos + 3
                            ' Pick up subfield 7, Reference for the Account Owner (16 pos)
                            sREF_Own = Trim$(Mid$(sImportLineBuffer, iPos, 16))

                            ' 27.06.2017
                            ' DNB has extended the reference, which creates problems for Seacor.
                            If oCargo.Special = "SEACOR" Then
                                sREF_Own = Mid(sREF_Own, 5, 4) & Right(sREF_Own, 5)
                            End If

                            'If sTemp_PayCode = "LBX" Or sTemp_PayCode = "CMI" Or sTemp_PayCode = "CMS" Or bDebit Then
                            'If sTemp_PayCode = "LBX" Or sTemp_PayCode = "CMS" Or bDebit Then
                            ' Changed 04.06.2008 - We do only need TRF
                            ' XokNET 07.11.2011 - also need FEX
                            If (sTemp_PayCode <> "TRF" And sTemp_PayCode <> "FEX") Or bDebit = True Then
                                ' WE DO NOT NEED THIS PAYMENT!
                                ''''bCreateObject = False ' Delete LBX, CMS and CMI and debits
                                ' 16.06.2008, added next 2 lines
                                ' We need them for picking up referenceinfo to put into rec 6 for ACH-payments
                                bCreateObject = True
                                bExported = True ' Do NOT export them, just used to pick up reference info for ACH

                                '''''''''                ' Read next line to see if we have addenda info;
                                '''''''''                sImportLineBuffer = ReadGenericRecord(oFile)
                                '''''''''                If Left$(sImportLineBuffer, 1) = ":" Then
                                '''''''''                    ' other MT940 label, do not read new line
                                '''''''''                    bReadLine = False
                                '''''''''                Else
                                '''''''''                    ' addenda record, do not use, read next line
                                '''''''''                    bReadLine = True
                                '''''''''                End If

                            Else
                                bCreateObject = True ' Create payments for credits, except LBX, CMS and CMI
                                bExported = False
                            End If

                            oCargo.MON_InvoiceAmount = ConvertToAmount(sTemp, vbNullString, ",", False)
                            ' 6 first after :61: is date
                            oCargo.DATE_Payment = "20" & Mid$(sImportLineBuffer, 5, 6)
                            dDATE_Production = StringToDate(oCargo.DATE_Payment)
                            If oCargo.REF <> "MT942" Then    ' If imported :13D:-record, then set to MT942
                                oCargo.REF = "MT940"
                            End If
                            bReadLine = False  ' read no more, do it in payment
                            '''''''''            End If


                            'End If


                        Case ":86:"
                            'If sTemp_PayCode = "LBX" Or sTemp_PayCode = "CMI" Or sTemp_PayCode = "CMS" Or bDebit Then
                            'If sTemp_PayCode = "LBX" Or sTemp_PayCode = "CMS" Or bDebit Then
                            ' Changed 04.06.2008 - We do only need TRF
                            If sTemp_PayCode <> "TRF" Or bDebit = True Then

                                ' 86-record for payments not needed
                                Do While Not oFile.AtEndOfStream
                                    ' read another line, until we are finished with the 86 line(s)
                                    sImportLineBuffer = ReadGenericRecord(oFile)
                                    If Left$(sImportLineBuffer, 1) <> ":" Then
                                        bReadLine = True
                                    Else
                                        bReadLine = False ' already in importbuffer with :nn: line
                                        Exit Do
                                    End If
                                Loop
                            Else
                                ' jump out
                                bCreateObject = False
                                bReadLine = False
                                Exit Do
                            End If

                        Case Else
                            ' do nothing !!!!, read on
                            bReadLine = True

                    End Select


                Case "A"  'AHDR
                    ' AHDRACH12816001USDLAERDAL             20070124122303

                    If bFromLevelUnder Then
                        Exit Do  ' up to BabelFile to create another batch
                    End If

                    ' XokNET 21.12.2011 changed from DnBNOR to DNB
                    sFormatType = "DnBUS ACH Incoming"  ' removed 10.12.2007
                    oCargo.REF = Mid$(sImportLineBuffer, 5, 3) ' ACH or LBX
                    sI_Account = Mid$(sImportLineBuffer, 8, 8)
                    sISO = Mid$(sImportLineBuffer, 16, 3)
                    sI_Branch = Trim$(Mid$(sImportLineBuffer, 19, 20))
                    dDATE_Production = StringToDate(Mid$(sImportLineBuffer, 39, 8))
                    ' XokNET 26.03.2015 added next line
                    sExtraD5 = Trim$(Mid$(sImportLineBuffer, 47, 6))

                    bReadLine = True
                    bCreateObject = False

                    ' XOKNET 11.09.2013
                    ' Lockboxfiles can come from Wells Fargo in new solution. DNV US will use them
                Case "1"  '100DETNORVERIWELLSFARGO1309091629
                    ' AHDRACH12816001USDLAERDAL             20070124122303
                    If bFromLevelUnder Then
                        Exit Do  ' up to BabelFile to create another batch
                    End If

                    sFormatType = "DnBUS ACH Incoming"  ' removed 10.12.2007
                    oCargo.REF = "LBX" ' ACH or LBX
                    sISO = "USD"    ' Assume USD
                    sI_Branch = Trim$(Mid$(sImportLineBuffer, 4, 10))
                    dDATE_Production = StringToDate("20" & Mid$(sImportLineBuffer, 24, 6))
                    oCargo.DATE_Payment = "20" & Mid$(sImportLineBuffer, 24, 6)
                    oCargo.BANK_I_SWIFTCode = sI_Branch
                    oCargo.MONCurrency = sISO


                    bReadLine = True
                    bCreateObject = False

                    ' XOKNET 11.09.2013
                    ' Lockboxfiles can come from Wells Fargo in new solution. DNV US will use them
                Case "2"
                    '200003100001140008008020000020088001
                    sI_Account = Mid$(sImportLineBuffer, 29, 8)
                    dDATE_Production = StringToDate(oCargo.DATE_Payment)
                    sI_Branch = oCargo.BANK_I_SWIFTCode
                    sISO = oCargo.MONCurrency
                    bReadLine = True
                    bCreateObject = False

                    '5 - Batch header
                Case "5"
                    ' ACH
                    ' 5200MN STATE FINANCEUS BANK ACH         7416007162CTXACH PYMT        0701240241042000010000115
                    ' or
                    ' LBX
                    ' 515500029252001070124SECORMARIN0031000011

                    sBatch_ID = Mid$(sImportLineBuffer, 2, 4)  ' Batch Number

                    If Left$(oCargo.REF, 3) = "ACH" Then

                        sBatch_ID = Trim$(Mid$(sImportLineBuffer, 41, 10))  '
                        'XokNET 08.11.2011; This is Company_ID for payer. We have no payers accountno in ACH, CompanyID only
                        oCargo.E_Account = sBatch_ID
                        ''''''''''sREF_Bank = Mid$(sImportLineBuffer, 84, 9) ' Receiving Bank ID, Field 12,

                        lBatchcounter = lBatchcounter + 1
                        ' Pick CTX or CCD from the 5-record:
                        oCargo.REF = oCargo.REF & Mid$(sImportLineBuffer, 51, 3) ' From ACH to ACHCTX or ACHCCD
                        sVersion = Mid$(sImportLineBuffer, 51, 3) ' CTX or CCD or PPD
                        oCargo.PayCode = Mid$(sImportLineBuffer, 51, 3)           ' CTX or CCD
                        'oCargo.DATE_Payment = "20" + Mid$(sImportLineBuffer, 70, 2) & Mid$(sImportLineBuffer, 72, 2) & Mid$(sImportLineBuffer, 74, 2)
                        ' Date changed 19.02.2008, use Julian date from 76-78
                        ' Julian dates are number of days since 01.01.yy, and should be 5 digits.
                        ' Here we have three digits only, and have to calculate for a given year. Find that year in 70,2
                        ' But have to test!

                        ' 15.09.2008
                        ' Sometimes we get 000 as julian date. If so, put todays date as date;
                        If Mid(sImportLineBuffer, 76, 3) = "000" Then
                            oCargo.DATE_Payment = DateToString(Date.Today)
                        Else
                            oCargo.DATE_Payment = DateToString(System.DateTime.FromOADate(DateAdd(Microsoft.VisualBasic.DateInterval.Day, Val(Mid(sImportLineBuffer, 76, 3)), StringToDate("20" & Mid(sImportLineBuffer, 70, 2) & "0101")).ToOADate - 1))
                        End If

                        ' Pass some values to payment from 5-record
                        sEname = RTrim$(Mid$(sImportLineBuffer, 5, 16))
                        sExtraD2 = Trim$(Mid$(sImportLineBuffer, 54, 10)) ' Ordering Company Desc Entry
                        sBankBranchNO = Trim$(Mid$(sImportLineBuffer, 80, 8)) ' Field 12, Originating DFI (Orig Bank ID)
                        sExtraD1 = Mid$(sImportLineBuffer, 41, 10) ' Ordering Company ID, Field 5

                        sVoucherNo = vbNullString

                        bReadLine = False
                        bCreateObject = True
                    Else
                        'Lockbox
                        lBatchcounter = lBatchcounter + 1
                        bReadLine = True
                        bCreateObject = False

                        sVoucherNo = Mid$(sImportLineBuffer, 9, 7)   ' Lockboxnumber

                        oCargo.DATE_Payment = "20" + Mid$(sImportLineBuffer, 16, 2) & Mid$(sImportLineBuffer, 18, 2) & Mid$(sImportLineBuffer, 20, 2)
                        sI_Branch = Mid$(sImportLineBuffer, 22, 10)
                        ' Location
                        ' --------
                        'Atlanta AL
                        'Baltimore CO
                        'Charlotte CL
                        'Chicago IL
                        'Dallas DA
                        'Los Angeles LA
                        'New York    NY
                        'Orlando OR
                        'Philadelphia PH
                        oCargo.FreeText = Mid$(sImportLineBuffer, 42, 3)

                        oCargo.I_Account = Mid$(sImportLineBuffer, 45, 13)
                        ' XokNET 19.09.2011 - AccountNo's can be more than 8 digits. Keep last 8
                        ' XOKNET 11.09.2013 - something funny with the test below !!!
                        'If Len(oCargo.I_Account) Then
                        oCargo.I_Account = Right$(oCargo.I_Account, 8)
                        'End If

                        oCargo.I_Name = Mid$(sImportLineBuffer, 58, 10)
                        ' XokNET 31.10.2011 Added next If, save bankname
                        If Left$(oCargo.REF, 3) = "LBX" Then
                            oCargo.Temp = Mid$(sImportLineBuffer, 32, 10)
                        End If

                    End If

                    '6 - Entry detail
                Case "6"
                    'Calculate BankIDs
                    'nCalculatedBankIdentifications = nCalculatedBankIdentifications + Val(Mid$(sImportLineBuffer, 30, 10))

                    ' XOKNET 25.04.2012 - added If "ACH", and else-part
                    If Left$(oCargo.REF, 3) = "ACH" Then

                        ' XokNET 25.11.2011
                        ' DO NOT import DirectDebits.
                        ' They have 27 or 37 in pos 2 and 3
                        ' FOR ACH ONLY !!!!
                        If Mid$(sImportLineBuffer, 2, 2) = "27" Or Mid$(sImportLineBuffer, 2, 2) = "37" Then
                            bCreateObject = False
                            bReadLine = True
                        Else
                            bCreateObject = True
                            bReadLine = False
                        End If

                    Else
                        ' LBX
                        bCreateObject = True
                        bReadLine = False

                    End If

                    '7 - Entry detail addenda (ACH) or Batch Trailer (Lockbox)
                Case "7"
                    If Left$(oCargo.REF, 3) = "LBX" Then
                        If sStatusCode = "00" Or StatusCode = "01" Then
                            AddInvoiceAmountOnBatch()
                        Else
                            AddInvoiceAmountOnBatch()
                            AddTransferredAmountOnBatch()
                        End If

                        ' XokNET 26.03.2015 Added next line
                        sBatch_ID = Mid$(sImportLineBuffer, 2, 4) ' Batch number

                        lNumberOfEntryDetailsRecords = Val(Mid$(sImportLineBuffer, 22, 3))
                        nNo_Of_Records = lNumberOfEntryDetailsRecords
                        If Val(Mid$(sImportLineBuffer, 25, 12)) <> nMON_InvoiceAmount Then
                            ' XOKNET 11.09.2013 - something strange in the errormsg below, changed
                            'Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10048-025", Trim$(Str(nMON_InvoiceCreditAmount)), Trim$(Str(nMON_InvoiceDebitAmount - nMON_InvoiceDebitAmount)), , oFile.Line)
                            Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10048-025", Trim$(Str(nMON_InvoiceAmount)), Trim$(Str(Val(Mid$(sImportLineBuffer, 25, 12)))), , oFile.Line)
                        End If
                        bCreateObject = False
                        bReadLine = True

                        ' XOKNET 11.09.2013
                        ' The new lbx file from Wells Fargo has no trailerbatch (like ATRL for the DNB-file)
                        ' Must exit do BabelFile to create new batch
                        If oCargo.Temp = "WELLSFARGO" Then
                            Exit Do
                        End If

                    Else
                        ' lockbox
                        bCreateObject = False
                        bReadLine = True

                    End If

                    '8 - Batch control record, for ACH only
                Case "8"
                    If sStatusCode = "00" Or StatusCode = "01" Then
                        AddInvoiceAmountOnBatch()
                    Else
                        AddInvoiceAmountOnBatch()
                        AddTransferredAmountOnBatch()
                    End If

                    lNumberOfEntryDetailsRecords = Val(Mid$(sImportLineBuffer, 5, 6))
                    nNo_Of_Records = lNumberOfEntryDetailsRecords
                    sEntryHash = Mid$(sImportLineBuffer, 11, 10)
                    'If Val(sEntryHash) <> nCalculatedBankIdentifications Then
                    '    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "11007-025", Right(PadLeft(Trim$(Str(nCalculatedBankIdentifications)), 10, "0"), 10), sEntryHash, , oFile.Line)
                    'End If
                    'Store the value temporary in sVersion. It will be deleted in BabelFile
                    '''''''sVersion = sEntryHash

                    ' ????????????Difference for CCD and CTX  ???????????????????
                    If Mid$(oCargo.REF, 4) = "CTX" Then
                        nMON_InvoiceCreditAmount = Val(Mid$(sImportLineBuffer, 21, 12))
                        nMON_InvoiceDebitAmount = Val(Mid$(sImportLineBuffer, 33, 12))
                    Else
                        nMON_InvoiceDebitAmount = Val(Mid$(sImportLineBuffer, 21, 12))
                        nMON_InvoiceCreditAmount = Val(Mid$(sImportLineBuffer, 33, 12))
                    End If
                    ' We seem to have a problem with credit and debit here, thus accept one of two in controls, because I cant
                    ' figure out what is what here
                    ' XokNET 10.11.2011 - added another IF
                    ' XokNET 25.11.2011 - DO NOT CONTROL - We may exlude DirectDebits, then the amounts will not match
                    '        If (nMON_InvoiceDebitAmount - nMON_InvoiceCreditAmount) <> nMON_InvoiceAmount And _
                    '             (nMON_InvoiceCreditAmount - nMON_InvoiceDebitAmount) <> nMON_InvoiceAmount And _
                    '             (nMON_InvoiceCreditAmount + nMON_InvoiceDebitAmount) <> nMON_InvoiceAmount _
                    '        Then
                    '            Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10048-025", Trim$(Str(nMON_InvoiceDebitAmount - nMON_InvoiceCreditAmount)), Trim$(Str(nMON_InvoiceAmount)), , oFile.Line)
                    '        End If
                    bCreateObject = False
                    bReadLine = True

                    '9 - File trailer
                Case "9"
                    Exit Do

                    'All other content would indicate error
                Case Else

                    'All other BETFOR-types gives error
                    'Error # 10010-002
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-025")

                    Exit Do

            End Select

            'Read next line
            If bReadLine Then
                sImportLineBuffer = Read_DnBNORUS_ACH_IncomingRecord(oFile, oCargo)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.PayCode = oCargo.PayCode
                oPayment.DATE_Payment = oCargo.DATE_Payment
                oPayment.DATE_Value = oCargo.DATE_Payment
                oPayment.MON_AccountCurrency = sISO
                oPayment.I_Name = sI_Branch
                oPayment.E_Name = sEname
                oPayment.BANK_BranchNo = sBankBranchNO
                oPayment.VoucherNo = sVoucherNo ' LockboxNumber for LBX, "fake" lockboxnumber from ACH
                oPayment.ExtraD1 = sExtraD1
                oPayment.ExtraD2 = sExtraD2
                ' XokNET 26.03.2015
                oPayment.ExtraD5 = sExtraD5
                oPayment.REF_Own = sREF_Own
                oPayment.Exported = bExported


                ' BabelBank Internal paycodes
                ' Lockbox              660
                ' ACH CTX              661
                ' ACH CCD              662
                ' ACH PPD              663
                If Left$(oCargo.REF, 6) = "ACHCTX" Then
                    oPayment.PayCode = "661"
                ElseIf Left$(oCargo.REF, 6) = "ACHCCD" Then
                    oPayment.PayCode = "662"
                ElseIf Left$(oCargo.REF, 6) = "ACHPPD" Then
                    oPayment.PayCode = "663"
                ElseIf Left$(oCargo.REF, 5) = "MT940" Then
                    oPayment.PayCode = "665"  ' DnBNOR MT940
                    ' Special handling of CMI (ACH-trans in MT940-files)
                    ' They need to be imported, beacuse we need to collect info from them to use in ACH-payments
                    ' Because of that, we are importing "MT940 ACH-payments", but set them as Exported = True
                    If sTemp_PayCode = "CMI" Then
                        oPayment.Exported = True
                    End If
                Else
                    oPayment.PayCode = "660"  ' Lockbox
                End If


                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)

                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop
        'XokNET 30.11.2011
        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        Read_DnBNORUS_ACH_Incoming = sImportLineBuffer
    End Function
    Private Function ReadBankOfAmerica_UK() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sTmpDate As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            Select Case Mid(sImportLineBuffer, 1, 3)

                Case "CRF"
                    bCreateObject = True
                    bReadLine = False

                Case "P20"
                    ' Transactionrecord
                    bCreateObject = True

                    'P80 - End of file
                Case "P80"
                    bReadLine = False
                    bCreateObject = False
                    bFromLevelUnder = True
                    Exit Do ' Jump to BabelFile

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If


            'Read next line
            If bReadLine Then
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadBankOfAmerica_UKRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadBankOfAmerica_UKRecord(oFile) ', oProfile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop
        'AddInvoiceAmountOnBatch

        ReadBankOfAmerica_UK = sImportLineBuffer
    End Function

    Private Function ReadGjensidige_S2000_PAYMULFile() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sProgress As String 'Indicates how far in the process we have run. Used in errormsg

        bFromLevelUnder = False

        sBatch_ID = oCargo.Temp
        oCargo.Temp = ""

        Do While True
            bReadLine = True ' Default
            bCreateObject = False ' Default

            'Check linetype
            Select Case Mid(sImportLineBuffer, 1, 3)

                Case "000" ' Start of file. Can be more than one "File" inside a physical file
                    sProgress = "Leser 000-record, BabelFile / ReadGjensidige_S2000_PAYMULFile()"
                    'If the sender is BBS identified by "00008080" then the file is send from
                    ' BBS and not from the internal system.
                    Exit Do

                Case "020" ' Ser ut som en "batch" header record, for poster gruppert etter valuta, dato og konto
                    '020200708240708240101                         ZZZ000000019580313.00NOKNOK347NOK

                    If bFromLevelUnder Then
                        ' have read a 020-record in batch (new batch)
                        ' jump up, to create object
                        sProgress = "Leser 020-record, bFromLevelUnder = True, Batch / ReadGjensidige_S2000_PAYMULFile()"
                        Exit Do
                    Else
                        sProgress = "Tolker 020-record, Batch / ReadGjensidige_S2000_PAYMULFile()"
                        dDATE_Production = StringToDate(Mid(sImportLineBuffer, 4, 8))
                        nMON_InvoiceAmount = Val(Replace(Mid(sImportLineBuffer, 50, 18), ".", ""))
                        oCargo.MONCurrency = Mid(sImportLineBuffer, 68, 3)
                        sREF_Own = Mid(sImportLineBuffer, 12, 11)

                        oCargo.REF = Mid(sImportLineBuffer, 74, 3) ' Mon-type to be used in Paymul
                        bReadLine = True
                        bCreateObject = False
                    End If

                Case "021" ' Batch f�lgerecord
                    ' 021                IN
                    sProgress = "Tolker 021-record, Batch / ReadGjensidige_S2000_PAYMULFile()"
                    oCargo.PayType = Mid(sImportLineBuffer, 20, 2) ' 18.12.2007 Used to test for International or Domestic
                    bReadLine = True
                    bCreateObject = False


                Case "022" ' ?????????? Vet ikke hva denne recorden er for noe ????????????!!!
                    ' kaller p� feil, for � se om denne er ibruk!!!
                    sProgress = "Leser 022-record, DENNE ER ENN� IKKE PROGRAMMERT. BabelFile / ReadGjensidige_S2000_PAYMULFile()"
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-005", , , , (oFile.Line))

                Case "030" ' Batch belastningskontorecord
                    ' 03097500707864
                    sProgress = "Tolker 030-record, Batch / ReadGjensidige_S2000_PAYMULFile()"
                    oCargo.I_Account = Mid(sImportLineBuffer, 4, 11)
                    bReadLine = True
                    bCreateObject = False

                Case "040" ' F�rste record i en betaling
                    ' 040000000000000403.0007082401010001
                    sProgress = "Leser 040-record, Batch / ReadGjensidige_S2000_PAYMULFile()"
                    ' Goto Payment
                    bReadLine = False
                    bCreateObject = True

                Case "080" ' End of payment, if not KID
                    ' Empty
                    ' 080
                    sProgress = "Tolker 080-record, Batch / ReadGjensidige_S2000_PAYMULFile()"
                    ' Never mind this one, hopefully we get a 020 next time, of endoffile

                Case "084" ' KID, and end of payment
                    ' 08422088521577983953254
                    sProgress = "Tolker 084-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'Error # 10010-005
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-005", , , , (oFile.Line))

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If


            'Read next line
            If bReadLine Then
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadGjensidige_S2000_PAYMULRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadGjensidige_S2000_PAYMULRecord(oFile) ', oProfile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oCargo.FreeText = ""
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop

        ReadGjensidige_S2000_PAYMULFile = sImportLineBuffer
    End Function
    Private Function ReadAquariusIncoming() As Boolean
        '*/
        ' Import of incoming payments for SG Finans
        ' This is planned to be used in the browser
        '*/
        Dim oPayment As Payment
        Dim sTmpDate As String
        Dim bFromLevelUnder As Boolean
        Dim bReadLine As Boolean
        Dim bCreateObject As Boolean

        bFromLevelUnder = False

        Do While oFile.AtEndOfStream = False

            If Left(sImportLineBuffer, 8) = "PAYMENT;" Then
                If bFromLevelUnder Then
                    Exit Do
                Else
                    nMON_InvoiceAmount = CDbl(xDelim(sImportLineBuffer, ";", 2))
                    sREF_Bank = xDelim(sImportLineBuffer, ";", 9) 'Bankref
                    sREF_Own = xDelim(sImportLineBuffer, ";", 10) 'VoucherNo
                    sTmpDate = xDelim(sImportLineBuffer, ";", 4) 'Creation date, YYYYMMDD
                    dDATE_Production = DateSerial(CInt(Left(sTmpDate, 4)), CInt(Mid(sTmpDate, 5, 2)), CInt(Right(sTmpDate, 2)))

                    bReadLine = False
                    bCreateObject = True
                End If
            ElseIf Left(sImportLineBuffer, 6) = "ALLOC;" Then
                If bFromLevelUnder Then
                    bReadLine = False
                    bCreateObject = True
                Else
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-005", , , , (oFile.Line))
                End If
            Else
                'Error # 10010-005
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-005", , , , (oFile.Line))

            End If

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            'Create Payment object
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

        Loop

        'AddInvoiceAmountOnBatch
        ReadAquariusIncoming = CBool(sImportLineBuffer)

    End Function
    Private Function ReadSGFinans_SaldoAq() As String
        '-----------------------------------------------
        ' Saldofile from SG Finans Aquarius
        '-----------------------------------------------
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = BabelFiles.FileType.SG_SaldoFil_Aq

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSGFinans_SaldoAq = sImportLineBuffer
    End Function
    Private Function ReadSGFinans_FakturaHistorikkAq() As String
        '-----------------------------------------------
        ' FakturaHistorikkfile from SG Finans Aquarius
        '-----------------------------------------------
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            bReadLine = False
            bCreateObject = True

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = BabelFiles.FileType.SG_FakturaHistorikFil_Aq

                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSGFinans_FakturaHistorikkAq = sImportLineBuffer
    End Function
    Private Function ReadBabelBankMappingOut() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        '----------------------------------------------------------------------------------------------
        ' Outpayments
        ' Use mappingfile to map format into this general importroutine
        ' Used for "simple" delimited and fixed formats
        '----------------------------------------------------------------------------------------------

        bFromLevelUnder = False

        sFormatType = "BabeBank_Mapping_Out"

        Do While True
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.objXLSheet = oXLWSheet
            oPayment.VB_Profile = oProfile
            oPayment.Mapping = oMapping
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadBabelBankMappingOut = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = "EXIT" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadKLINK_ExcelUtbetaling() As String
        Dim oPayment As Payment
        Dim bx As Boolean
        ', bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String
        'Dim sResponse As String

        sFormatType = "KLINK ExcelUtbetaling"

        oPayment = oPayments.Add()
        oPayment.objFile = oFile
        oPayment.VB_Profile = oProfile
        oPayment.ImportFormat = iImportFormat
        oPayment.StatusCode = sStatusCode
        If sImportLineBuffer <> "ERROR" Then
            sI_Account = Replace(Replace(CObj(xDelim(sImportLineBuffer, Chr(9), 9)), ".", ""), " ", "")
            oPayment.I_Account = CObj(sI_Account)
        End If
        sImportLineBuffer = oPayment.Import(sImportLineBuffer)

        ReadKLINK_ExcelUtbetaling = sImportLineBuffer

        bx = AddInvoiceAmountOnBatch()
        bx = AddTransferredAmountOnBatch()

        nMON_TransferredAmount = nMON_InvoiceAmount

    End Function
    Private Function ReadPayEx_CSV() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "PayEx_CSV"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadPayEx_CSV = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadPayPal_ActiveBrands() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim sSeparator As String = sSeparator

        ' vi m� skifte mellom separatorer, avhengig av hvordan filene er laget
        sSeparator = ","   'Chr(9)  '(TAB)      or ","

        bFromLevelUnder = False

        sFormatType = "PayPal_ActiveBrands"

        Do While True

            ' 22.11.2018 - Do not import lines with empty AQ-column (AQ is orderno)
            If EmptyString(xDelim(sImportLineBuffer, sSeparator, 43, Chr(34))) Then
                ' read next line
                ' do not create oPayment
                If Not oFile.AtEndOfStream Then
                    sImportLineBuffer = ReadGenericRecord(oFile)
                Else
                    sImportLineBuffer = ""
                End If
            Else

                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                ReadPayPal_ActiveBrands = sImportLineBuffer
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadDnBNOR_US_Penta() As String
        ' Format created by Gordon Skjeie, DnBNOR NewYork
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sTmpDate As String

        bFromLevelUnder = False

        ' No batchlevel in this fileformat

        Do While True
            bReadLine = False
            bCreateObject = True

            If sImportLineBuffer = "ERROR" Or Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop
        AddInvoiceAmountOnBatch()

        ReadDnBNOR_US_Penta = sImportLineBuffer
    End Function
    Private Function Read_DnV_UK_Sage() As String
        ' Det norske Veritas (DnV in UK - from Sage)
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "DnV_UK_Sage"

        Do While True

            ' may have some sum info???
            'If Mid$(sImportLineBuffer, 8, 2) = "99" Then
            '    ' Slutrecord
            '    nNo_Of_Records = Val(Mid$(sImportLineBuffer, 28, 10))
            '    nNo_Of_Transactions = Val(Mid$(sImportLineBuffer, 48, 10))
            '    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 72, 14))'
            '
            '        Exit Do
            '    End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            Read_DnV_UK_Sage = sImportLineBuffer
            If sImportLineBuffer = "EOF" Then
                Exit Do
            End If

            'If oFile.AtEndOfStream = True Then
            '    Exit Do
            'End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadMT101() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "MT101"

        bFromLevelUnder = False

        Do While True
            bCreateObject = False

            Select Case Left(sImportLineBuffer, 4)

                Case ":20:"
                    ' skip this one, not correct to have more than one, but we need to kope with that
                    bReadLine = True
                    bCreateObject = False

                Case ":21:"  ' 25.08.2015 added :21:
                    ' 19.04.2016 removed next line
                    'oCargo.REF = Trim(Mid(sImportLineBuffer, 5))

                    bReadLine = False
                    bCreateObject = True

                Case ":21R"
                    oCargo.REF = Trim(Mid(sImportLineBuffer, 6))

                    bReadLine = False
                    bCreateObject = True

                    ' added 50H 16.12.2015
                Case ":50H"
                    ' find payers account in :50H: line, like :50H:/NO4550050636671
                    oCargo.I_Account = Trim(Mid(sImportLineBuffer, 6))
                    oCargo.I_Account = CheckForValidCharacters(oCargo.I_Account, False, True, False, True, "")

                    ' read line 2 in :50H:, Customer Identity
                    sImportLineBuffer = ReadGenericRecord(oFile)

                    sImportLineBuffer = ReadGenericRecord(oFile) ' :52A:DNBANOKK, senders BIC
                    oCargo.BANK_I_SWIFTCode = Replace(Trim(Mid(sImportLineBuffer, 6)), "/", "")   ' 21.04.2021 remove /

                    sImportLineBuffer = ReadGenericRecord(oFile) ' :30:090526
                    oCargo.DATE_Payment = "20" & Mid(sImportLineBuffer, 5, 6)

                    bReadLine = True
                    bCreateObject = True


                Case Else
                    'Error # 10010-029
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-029")

            End Select

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If
            If sImportLineBuffer = "" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadMT101 = sImportLineBuffer

    End Function
    Private Function ReadSeajackWages() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = " SeajackWages"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objXLSheet = oXLWSheet
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadSeajackWages = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadGjensidigeUtbytte() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim sResponse As String

        bFromLevelUnder = False

        sFormatType = "GjensidigeUtbytte"
        GC.Collect()

        Do While True
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.I_Account = CObj(sResponse)
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadGjensidigeUtbytte = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadABNAmroBTL91() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            Select Case Mid(sImportLineBuffer, 1, 2)

                '21 - Sub record 1
                Case "21"
                    bReadLine = False
                    bCreateObject = True

                    '31 - End of Batch
                Case "31"
                    ' 31EUR0000000129500000002
                    'XNET - 06.12.2010 - Changed next lines because the 31 record is per valuta
                    ' 31-3 Total amount per valuta, 6,15 - last 3 decimals
                    nMON_InvoiceAmount = nMON_InvoiceAmount + Val(Mid$(sImportLineBuffer, 6, 14)) ' Sum amount in batch
                    bAmountSetInvoice = True
                    nMON_TransferredAmount = nMON_InvoiceAmount
                    bAmountSetTransferred = True
                    'XNET - 06.12.2010 - Changed next lines because the 31 record is per valuta
                    ' 31-4 No of paymentrecords, 21,4
                    nNo_Of_Transactions = nNo_Of_Transactions + Val(Mid$(sImportLineBuffer, 21, 4)) ' Antall transer i filen

                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True  ' true to trigger that if we get a 31-rec, jump up


                Case "41" ' end of file
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadABNAmroBTL91Record(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadABNAmroBTL91 = sImportLineBuffer
    End Function
    Private Function ReadHandelsbanken_Bankavstemming() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        Dim sCurrency As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left(sImportLineBuffer, 2)
                Case "HH"
                    ' jump "up" to create new batch

                    ' --> is it ok with no transactions in a batch ?
                    If oPayments.Count = 0 Then
                        ' must create a "fake" payment
                        oPayment = oPayments.Add()
                        oPayment.objFile = oFile
                        oPayment.VB_Profile = oProfile
                        oPayment.ImportFormat = iImportFormat
                        oPayment.StatusCode = sStatusCode
                        If bProfileInUse Then
                            oPayment.VB_FilenameOut_ID = CheckAccountNo(oCargo.I_Account, "", oProfile)
                        End If
                        ' XNET 28.02.2013 - must add more info to the "empty" payment
                        oPayment.I_Account = oCargo.I_Account
                        oPayment.DATE_Payment = oCargo.DATE_Payment
                        oPayment.DATE_Value = oCargo.DATE_Payment
                        oPayment.MON_AccountCurrency = oCargo.MONCurrency
                        oPayment.MON_InvoiceCurrency = oCargo.MONCurrency
                    End If


                    Exit Do

                Case "S-"
                    ' Save outgoing Balance, domestic
                    oCargo.I_Account = Mid(sImportLineBuffer, 3, 11)
                    ' also put account into sI_Branch, to have it present in batch
                    sI_Branch = oCargo.I_Account

                    nMON_BalanceOUT = Val(Mid(sImportLineBuffer, 15, 13))
                    If Mid(sImportLineBuffer, 14, 1) = "-" Then
                        nMON_BalanceOUT = nMON_BalanceOUT * -1
                    End If

                    nMON_BalanceOUTInterest = Val(Mid(sImportLineBuffer, 29, 13))
                    If Mid(sImportLineBuffer, 28, 1) = "-" Then
                        nMON_BalanceOUTInterest = nMON_BalanceOUTInterest * -1
                    End If
                    dDATE_Production = StringToDate(oCargo.DATE_Payment)

                    oCargo.MONCurrency = "NOK"
                    sREF_Bank = "NOK"
                    sVersion = "Domestic"

                    bReadLine = True
                    bCreateObject = False

                Case "VS"
                    ' Save outgoing Balance, international
                    oCargo.I_Account = Mid(sImportLineBuffer, 3, 11)
                    ' also put account into sI_Branch, to have it present in batch
                    sI_Branch = oCargo.I_Account

                    ' import NOK-balances
                    nMON_BalanceOUTNOK = Val(Mid(sImportLineBuffer, 15, 13))
                    If Mid(sImportLineBuffer, 14, 1) = "-" Then
                        nMON_BalanceOUTNOK = nMON_BalanceOUTNOK * -1
                    End If

                    nMON_BalanceOUTInterestNOK = Val(Mid(sImportLineBuffer, 29, 13))
                    If Mid(sImportLineBuffer, 28, 1) = "-" Then
                        nMON_BalanceOUTInterestNOK = nMON_BalanceOUTInterestNOK * -1
                    End If

                    dDATE_Production = StringToDate(oCargo.DATE_Payment)

                    ' Currencyrate - NB! last 4 digits are decimals !!!!
                    nCurrencyRate = Val(Mid(sImportLineBuffer, 46, 9))

                    ' reads balance in accountcurrency !!
                    nMON_BalanceOUT = Val(Mid(sImportLineBuffer, 56, 13))
                    If Mid(sImportLineBuffer, 55, 1) = "-" Then
                        nMON_BalanceOUT = nMON_BalanceOUT * -1
                    End If

                    nMON_BalanceOUTInterest = Val(Mid(sImportLineBuffer, 70, 13))
                    If Mid(sImportLineBuffer, 69, 1) = "-" Then
                        nMON_BalanceOUTInterest = nMON_BalanceOUTInterest * -1
                    End If

                    oCargo.MONCurrency = Mid(sImportLineBuffer, 42, 3)
                    sREF_Bank = oCargo.MONCurrency
                    sVersion = "International"

                    bReadLine = True
                    bCreateObject = False


                Case "VT"
                    ' New payment for each VT
                    bCreateObject = True
                    bReadLine = False

                Case "T-"
                    ' New payment for each T-
                    bCreateObject = True
                    bReadLine = False

                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select


            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'If oFile.AtEndOfStream = True Then
            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

            If bReadLine Then
                sImportLineBuffer = ReadHandelsbanken_BankavstemmingRecord(oFile)
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadHandelsbanken_Bankavstemming = sImportLineBuffer
    End Function
    Private Function ReadUlefos_Salary_FI() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False
        bProfileInUse = True

        sFormatType = GetEnumFileType(iImportFormat)
        dDATE_Production = DateSerial(CInt(VB6.Format(Now, "YYYY")), CInt(VB6.Format(Now, "MM")), CInt(VB6.Format(Now, "DD")))
        sStatusCode = "00"

        Do While oFile.AtEndOfStream = False

            If Left(sImportLineBuffer, 1) = "4" Then
                'End record
                Exit Do
            ElseIf Left(sImportLineBuffer, 1) = "1" Then
                'Just continue

                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

            ElseIf oFile.AtEndOfStream = True Or Len(sImportLineBuffer) = 0 Or sImportLineBuffer = "EOF" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadUlefos_Salary_FI = sImportLineBuffer

    End Function
    Private Function ReadINS2000(ByVal nodBatch As System.Xml.XmlNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodPaymentList As System.Xml.XmlNodeList
        Dim nodPayment As System.Xml.XmlNode

        'XNET - 18.11.2010 - Added errorhandling

        bReturnValue = False

        'The nodPaymentList will contain all the Account-Tags under this INS2000EXPORT
        nodPaymentList = nodBatch.selectNodes("//ACCOUNT")

        'sREF_Own = nodBatch.selectSingleNode("child::*[name()='PayersReference']").nodeTypedValue
        'nMON_InvoiceAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='Amount']").nodeTypedValue, ".", ""))
        'bAmountSetInvoice = True
        sFormatType = "INS2000"
        'If sStatusCode = "02" Then
        '    sREF_Bank = nodBatch.selectSingleNode("child::*[name()='BankReference']").nodeTypedValue
        '    nMON_TransferredAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='AmountTransferred']").nodeTypedValue, ".", ""))
        '    bAmountSetTransferred = True
        'End If


        For lNodePaymentCounter = 0 To nodPaymentList.Count - 1

            'Pick an element from the list
            nodPayment = nodPaymentList.Item(lNodePaymentCounter)

            '    Set nodPaymentDetailsList = nodPayment.selectNodes("child::*[name()='PaymentDetails']")
            '    Set nodPaymentDetails = nodPaymentDetailsList.Item(lNodePaymentListCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImportINS2000(nodPayment)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodePaymentCounter

        ' New 31.03.05
        AddInvoiceAmountOnBatch()


        ReadINS2000 = bReturnValue
        'XNET - 18.11.2010 - Added errorhandling

    End Function
    'Removed 11.09.2019
    '    Private Function ReadNHC_Philippines(ByVal nodePaymentList As System.Xml.XmlNodeList) As Boolean
    '        Dim oPayment As Payment
    '        Dim bReturnValue As Boolean
    '        Dim lNodePaymentCounter As Long
    '        Dim lNodePaymentListCounter As Long
    '        Dim nodePayment As System.Xml.XmlNode
    '        Dim sOldAccount As String = ""
    '        Dim sOldSwift As String = ""
    '        Dim sOldCur As String = ""
    '        Dim sTmpAccount As String = ""
    '        Dim sTmpSWIFT As String = ""
    '        Dim sTmpDate As String = ""
    '        Dim sTmpCur As String = ""
    '        Dim bCreateNewObject As Boolean = True

    '        'XNET - 18.11.2010 - Added errorhandling
    '        On Error GoTo ERR_ReadNHC_Philippines

    '        bReturnValue = False

    '        'The nodPaymentList will contain all the Account-Tags under this INS2000EXPORT
    '        'nodPaymentList = nodBatch.selectNodes("//ACCOUNT")

    '        'sREF_Own = nodBatch.selectSingleNode("child::*[name()='PayersReference']").nodeTypedValue
    '        'nMON_InvoiceAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='Amount']").nodeTypedValue, ".", ""))
    '        'bAmountSetInvoice = True
    '        sFormatType = "NHC_Philippines"
    '        'If sStatusCode = "02" Then
    '        '    sREF_Bank = nodBatch.selectSingleNode("child::*[name()='BankReference']").nodeTypedValue
    '        '    nMON_TransferredAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='AmountTransferred']").nodeTypedValue, ".", ""))
    '        '    bAmountSetTransferred = True
    '        'End If


    '        For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

    '            'Pick an element from the list
    '            nodePayment = nodePaymentList.Item(lNodePaymentCounter)

    '            '    Set nodPaymentDetailsList = nodPayment.selectNodes("child::*[name()='PaymentDetails']")
    '            '    Set nodPaymentDetails = nodPaymentDetailsList.Item(lNodePaymentListCounter)

    '            sTmpAccount = GetInnerText(nodePayment, "BANACCRECEIVER", Nothing).Trim
    '            sTmpAccount = sTmpAccount.Replace(" ", "").Replace("-", "")
    '            sTmpSWIFT = GetInnerText(nodePayment, "RECEIVERSWIFT", Nothing).Trim
    '            'sTmpDate = GetInnerText(nodePayment, "RECEIVERNAME", Nothing)
    '            sTmpCur = GetInnerText(nodePayment, "CCYCODE", Nothing).Trim

    '            bCreateNewObject = True
    '            If sTmpAccount = sOldAccount Then
    '                If sTmpSWIFT = sOldSwift Then
    '                    If sTmpCur = sOldCur Then
    '                        bCreateNewObject = False
    '                    End If
    '                End If
    '            End If
    '            sOldAccount = sTmpAccount
    '            sOldSwift = sTmpSWIFT
    '            sOldCur = sTmpCur

    '            If bCreateNewObject Then
    '                oPayment = oPayments.Add()
    '                oPayment.objFile = oFile
    '                oPayment.VB_Profile = oProfile
    '                oPayment.Cargo = oCargo
    '                oPayment.ImportFormat = iImportFormat
    '                oPayment.StatusCode = sStatusCode
    '                oPayment.E_Account = sTmpAccount
    '                oPayment.BANK_SWIFTCode = sTmpSWIFT
    '                oPayment.MON_InvoiceCurrency = sTmpCur
    '                oPayment.MON_TransferCurrency = sTmpCur
    '            End If

    '            'Continue to the payment-object
    '            bReturnValue = oPayment.ImportNHC_Philippines(nodePayment, bCreateNewObject)
    '            If bReturnValue = False Then
    '                Exit For
    '            End If

    '        Next lNodePaymentCounter

    '        ' New 31.03.05
    '        AddInvoiceAmountOnBatch()


    '        ReadNHC_Philippines = bReturnValue
    '        'XNET - 18.11.2010 - Added errorhandling
    '        On Error GoTo 0
    '        Exit Function

    'ERR_ReadNHC_Philippines:

    '        Err.Raise(Err.Number, "ReadINS2000 - Batch", Err.Description)

    '    End Function
    Private Function ReadBambora_XML(ByVal nodeBatch As System.Xml.XmlNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodePaymentList As System.Xml.XmlNodeList
        Dim nodePayment As System.Xml.XmlNode
        Dim sTmpName As String = ""

        On Error GoTo ERR_ReadBambora_XML

        bReturnValue = False

        'The nodPaymentList will contain all the Account-Tags under this INS2000EXPORT
        'nodPaymentList = nodBatch.selectNodes("//ACCOUNT")

        'sREF_Own = nodBatch.selectSingleNode("child::*[name()='PayersReference']").nodeTypedValue
        'nMON_InvoiceAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='Amount']").nodeTypedValue, ".", ""))
        'bAmountSetInvoice = True
        sFormatType = "Bambora_XML"

        If Not GetInnerText(nodeBatch, "@merchantid", Nothing) Is Nothing Then
            sI_Branch = GetInnerText(nodeBatch, "@merchantid", Nothing).Trim
        End If
        If Not GetInnerText(nodeBatch, "@description", Nothing) Is Nothing Then
            sTmpName = GetInnerText(nodeBatch, "@description", Nothing).Trim
        End If

        nodePaymentList = nodeBatch.SelectNodes("ReconciliationItem")

        For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

            'Pick an element from the list
            nodePayment = nodePaymentList.Item(lNodePaymentCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImportBambora_XML(nodePayment)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodePaymentCounter

        ' New 31.03.05
        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadBambora_XML = bReturnValue
        'XNET - 18.11.2010 - Added errorhandling
        On Error GoTo 0
        Exit Function

ERR_ReadBambora_XML:

        If Not nodePaymentList Is Nothing Then
            nodePaymentList = Nothing
        End If
        If Not nodePayment Is Nothing Then
            nodePayment = Nothing
        End If
        If Not nodeBatch Is Nothing Then
            nodeBatch = Nothing
        End If

        If Err.Description.Length > 7 Then
            If Err.Description.Substring(0, 11) = "ReadBambora" Then
                Err.Raise(Err.Number, Err.Source, Err.Description)
            Else
                Err.Raise(Err.Number, "ReadBambora_XML - Batch", Err.Description)
            End If
        Else
            Err.Raise(Err.Number, "ReadBambora_XML - Batch", Err.Description)
        End If

    End Function
    Private Function ReadKlarna_XML(ByVal nodeBatch As System.Xml.XmlNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodePaymentList As System.Xml.XmlNodeList
        Dim nodePayment As System.Xml.XmlNode
        Dim sTmpName As String = ""

        On Error GoTo ERR_ReadKlarna_XML

        bReturnValue = False
        sFormatType = "Klarna_XML"

        nodePaymentList = nodeBatch.SelectNodes("transaction")


        For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

            'Pick an element from the list
            nodePayment = nodePaymentList.Item(lNodePaymentCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImportKlarna_XML(nodePayment)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodePaymentCounter

        ' New 31.03.05
        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadKlarna_XML = bReturnValue
        On Error GoTo 0
        Exit Function

ERR_ReadKlarna_XML:

        If Not nodePaymentList Is Nothing Then
            nodePaymentList = Nothing
        End If
        If Not nodePayment Is Nothing Then
            nodePayment = Nothing
        End If
        If Not nodeBatch Is Nothing Then
            nodeBatch = Nothing
        End If

        If Err.Description.Length > 7 Then
            If Err.Description.Substring(0, 11) = "ReadKlarna" Then
                Err.Raise(Err.Number, Err.Source, Err.Description)
            Else
                Err.Raise(Err.Number, "ReadKlarna_XML - Batch", Err.Description)
            End If
        Else
            Err.Raise(Err.Number, "ReadKlarna_XML - Batch", Err.Description)
        End If

    End Function
    Private Function ReadAdyen_XML(ByVal nodeBatch As System.Xml.XmlNode) As Boolean

        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodePaymentList As System.Xml.XmlNodeList
        Dim nodePayment As System.Xml.XmlNode
        Dim sTmpName As String = ""

        On Error GoTo ERR_ReadAdyen_XML

        bReturnValue = False
        sFormatType = "Adyen_XML"

        nodePaymentList = nodeBatch.SelectNodes("row")


        For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

            'Pick an element from the list
            nodePayment = nodePaymentList.Item(lNodePaymentCounter)

            ' do not import "Balancetransfer"
            ' 26.08.2019 - Gine will import also Balancetransfer ! Next If is removed
            'If nodePayment.OuterXml.IndexOf("Balancetransfer") > -1 Then
            ' do not use!
            ' 24.09.2018 - also introduced other payment types which will not be imported
            'ElseIf nodePayment.OuterXml.IndexOf("Transaction Fees") > -1 Then
            ' do not import!
            ' YES IMport - but put this one to GL 7781
            'ElseIf nodePayment.OuterXml.IndexOf("InvoiceDeduction") > -1 Then

            ' 02.12.2020 - Skal importere InvoiceDeduction - dette brukes i USA
            'If nodePayment.OuterXml.IndexOf("InvoiceDeduction") > -1 Then
            ' do not import!
            'ElseIf nodePayment.OuterXml.IndexOf("Deposit(Correction") > -1 Then
            ' do not import!

            'Else

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImportAdyen_XML(nodePayment)
            If bReturnValue = False Then
                Exit For
            End If
            'End If

        Next lNodePaymentCounter

        If oCargo.Special = "ACTIVEBRANDS_ADYEN" Then
            ' 26.11.2020
            ' We have different kinds of fees, hidden within each Adyen "payment".
            ' Track these fees, and total them up;
            ' Then add one payment pr fee type
            ' DETTE GJ�RES KUN FOR "Profile2"  dvs der hvor vi direkte importerer en Adyen XML fil
            ' For "Profile 1", der vi bytter ut en bankinnbetaling fra Adyen med innhold i Adyen XML-fil, gj�res tilsvarende i SwapFileChangeInvoiceInfo i TreatSpecial
            Dim oInvoice As Invoice
            Dim oFreetext As Freetext
            Dim nMarkupNC As Double = 0
            Dim nInterchangeNC As Double = 0
            Dim nScheemeFees As Double = 0
            Dim nCommissionNC As Double = 0
            Dim bActiveBrandsUS As Boolean = False
            Dim i As Integer
            Dim sCurrency As String = ""
            Dim sDate As String = ""
            Dim sBatch As String = ""
            Dim sBrand As String = ""

            ' We need to do this pr currency, in case we have an Adyen XML file with multi currencies
            If VB_Profile.CompanyName = "Active Brands US" Or VB_Profile.CompanyName = "ECom USA" Then
                bActiveBrandsUS = True
            End If

            For i = 1 To 6

                nMarkupNC = 0
                nInterchangeNC = 0
                nScheemeFees = 0
                nCommissionNC = 0

                Select Case i
                    Case 1
                        sCurrency = "NOK"
                    Case 2
                        sCurrency = "SEK"
                    Case 3
                        sCurrency = "DKK"
                    Case 4
                        sCurrency = "EUR"
                    Case 5
                        sCurrency = "USD"
                    Case 6   ' 06.01.2020 added GBP
                        sCurrency = "GBP"
                End Select

                ' total up fees for complete batch
                For Each oPayment In oPayments
                    If oPayment.MON_InvoiceCurrency = sCurrency Then
                        sBatch = oPayment.ExtraD3
                        sBrand = oPayment.I_Name
                        nMarkupNC = nMarkupNC + oPayment.MON_LocalAmount
                        nInterchangeNC = nInterchangeNC + oPayment.MON_EuroAmount
                        nScheemeFees = nScheemeFees + oPayment.MON_ChargesAmount
                        nCommissionNC = nCommissionNC + oPayment.MON_AccountAmount
                        sDate = oPayment.DATE_Payment
                        oPayment.MON_LocalAmount = 0
                        oPayment.MON_EuroAmount = 0
                        oPayment.MON_ChargesAmount = 0
                        oPayment.MON_AccountAmount = 0
                    End If
                Next oPayment

                ' then add upto four new payments for these charges;
                If nMarkupNC > 0 Then
                    oPayment = oPayments.Add()
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = "02"
                    oPayment.E_Name = "Adyen Markup"
                    oPayment.I_Account = "ADYEN"
                    oPayment.MON_InvoiceAmount = nMarkupNC * -1
                    oPayment.MON_TransferredAmount = nMarkupNC * -1
                    oPayment.MON_InvoiceCurrency = sCurrency
                    oPayment.MON_TransferCurrency = sCurrency
                    oPayment.PayType = "D"
                    oPayment.PayCode = "150"
                    oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched
                    oPayment.DATE_Payment = sDate
                    oPayment.DATE_Value = sDate
                    oPayment.VB_FilenameOut_ID = 3

                    oInvoice = oPayment.Invoices.Add
                    oInvoice.MON_InvoiceAmount = nMarkupNC * -1
                    oInvoice.MON_TransferredAmount = nMarkupNC * -1
                    If bActiveBrandsUS Then
                        oInvoice.MATCH_ID = "80401"
                    Else
                        oInvoice.MATCH_ID = "7781"
                    End If
                    oInvoice.MATCH_Final = True
                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
                    oInvoice.MATCH_Matched = True
                    oInvoice.MATCH_Original = True
                    oInvoice.Dim10 = "Markup"

                    oFreetext = oInvoice.Freetexts.Add
                    oFreetext.Text = "Markup" & " " & sBrand & " B" & sBatch & "   " & Format(Math.Abs(oPayment.MON_InvoiceAmount / 100), "##,##0.00") & " " & oPayment.MON_InvoiceCurrency

                End If

                If nInterchangeNC > 0 Then
                    oPayment = oPayments.Add()
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = "02"
                    oPayment.E_Name = "Adyen Interchange"
                    oPayment.I_Account = "ADYEN"
                    oPayment.MON_InvoiceAmount = nInterchangeNC * -1
                    oPayment.MON_TransferredAmount = nInterchangeNC * -1
                    oPayment.MON_InvoiceCurrency = sCurrency
                    oPayment.MON_TransferCurrency = sCurrency
                    oPayment.PayType = "D"
                    oPayment.PayCode = "150"
                    oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched
                    oPayment.DATE_Payment = sDate
                    oPayment.DATE_Value = sDate
                    oPayment.VB_FilenameOut_ID = 3

                    oInvoice = oPayment.Invoices.Add
                    oInvoice.MON_InvoiceAmount = nInterchangeNC * -1
                    oInvoice.MON_TransferredAmount = nInterchangeNC * -1
                    'oInvoice.MATCH_ID = "7781"
                    ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
                    If bActiveBrandsUS Then
                        oInvoice.MATCH_ID = "80401"
                    Else
                        oInvoice.MATCH_ID = "7781"
                    End If
                    oInvoice.MATCH_Final = True
                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
                    oInvoice.MATCH_Matched = True
                    oInvoice.MATCH_Original = True

                    oFreetext = oInvoice.Freetexts.Add
                    oFreetext.Text = "Interchange" & " " & sBrand & " B" & sBatch & "   " & Format(Math.Abs(oPayment.MON_InvoiceAmount / 100), "##,##0.00") & " " & oPayment.MON_InvoiceCurrency
                End If

                If nScheemeFees > 0 Then
                    oPayment = oPayments.Add()
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = "02"
                    oPayment.E_Name = "Adyen ScheemeFees"
                    oPayment.I_Account = "ADYEN"
                    oPayment.MON_InvoiceAmount = nScheemeFees * -1
                    oPayment.MON_TransferredAmount = nScheemeFees * -1
                    oPayment.MON_InvoiceCurrency = sCurrency
                    oPayment.MON_TransferCurrency = sCurrency
                    oPayment.PayType = "D"
                    oPayment.PayCode = "150"
                    oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched
                    oPayment.DATE_Payment = sDate
                    oPayment.DATE_Value = sDate
                    oPayment.VB_FilenameOut_ID = 3

                    oInvoice = oPayment.Invoices.Add
                    oInvoice.MON_InvoiceAmount = nScheemeFees * -1
                    oInvoice.MON_TransferredAmount = nScheemeFees * -1
                    'oInvoice.MATCH_ID = "7781"
                    ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
                    If bActiveBrandsUS Then
                        oInvoice.MATCH_ID = "80401"
                    Else
                        oInvoice.MATCH_ID = "7781"
                    End If
                    oInvoice.MATCH_Final = True
                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
                    oInvoice.MATCH_Matched = True
                    oInvoice.MATCH_Original = True
                    oInvoice.Dim10 = "ScheemeFees"

                    oFreetext = oInvoice.Freetexts.Add
                    oFreetext.Text = "ScheemeFees" & " " & sBrand & " B" & sBatch & "   " & Format(Math.Abs(oPayment.MON_InvoiceAmount / 100), "##,##0.00") & " " & oPayment.MON_InvoiceCurrency
                End If

                If nCommissionNC > 0 Then
                    oPayment = oPayments.Add()
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = "02"
                    oPayment.E_Name = "Adyen Commission"
                    oPayment.I_Account = "ADYEN"
                    oPayment.MON_InvoiceAmount = nCommissionNC * -1
                    oPayment.MON_TransferredAmount = nCommissionNC * -1
                    oPayment.MON_InvoiceCurrency = sCurrency
                    oPayment.MON_TransferCurrency = sCurrency
                    oPayment.PayType = "D"
                    oPayment.PayCode = "150"
                    oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched
                    oPayment.DATE_Payment = sDate
                    oPayment.DATE_Value = sDate
                    oPayment.VB_FilenameOut_ID = 3

                    oInvoice = oPayment.Invoices.Add
                    oInvoice.MON_InvoiceAmount = nCommissionNC * -1
                    oInvoice.MON_TransferredAmount = nCommissionNC * -1
                    'oInvoice.MATCH_ID = "7781"
                    ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
                    If bActiveBrandsUS Then
                        oInvoice.MATCH_ID = "80401"
                    Else
                        oInvoice.MATCH_ID = "7781"
                    End If
                    oInvoice.MATCH_Final = True
                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
                    oInvoice.MATCH_Matched = True
                    oInvoice.MATCH_Original = True
                    oInvoice.Dim10 = "Commission"

                    oFreetext = oInvoice.Freetexts.Add
                    oFreetext.Text = "Comission" & " " & sBrand & " B" & sBatch & "   " & Format(Math.Abs(oPayment.MON_InvoiceAmount / 100), "##,##0.00") & " " & oPayment.MON_InvoiceCurrency
                End If
            Next i

        End If
        ' New 31.03.05
        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadAdyen_XML = bReturnValue
        On Error GoTo 0
        Exit Function

ERR_ReadAdyen_XML:

        If Not nodePaymentList Is Nothing Then
            nodePaymentList = Nothing
        End If
        If Not nodePayment Is Nothing Then
            nodePayment = Nothing
        End If
        If Not nodeBatch Is Nothing Then
            nodeBatch = Nothing
        End If

        If Err.Description.Length > 7 Then
            If Err.Description.Substring(0, 11) = "ReadAdyen" Then
                Err.Raise(Err.Number, Err.Source, Err.Description)
            Else
                Err.Raise(Err.Number, "ReadAdyen_XML - Batch", Err.Description)
            End If
        Else
            Err.Raise(Err.Number, "ReadAdyen_XML - Batch", Err.Description)
        End If


        '        Dim oPayment As Payment
        '        Dim bReturnValue As Boolean
        '        Dim lNodePaymentCounter As Long
        '        Dim lNodePaymentListCounter As Long
        '        Dim nodePaymentList As System.Xml.XmlNodeList
        '        Dim nodePayment As System.Xml.XmlNode
        '        Dim sTmpName As String = ""

        '        On Error GoTo ERR_ReadAdyen_XML

        '        bReturnValue = False
        '        sFormatType = "Adyen_XML"

        '        nodePaymentList = nodeBatch.SelectNodes("row")

        '        'For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

        '        'Pick an element from the list
        '        nodePayment = nodePaymentList.Item(lNodePaymentCounter)

        '        ' do not import "Balancetransfer"
        '        'If nodePayment.OuterXml.IndexOf("Balancetransfer") > -1 Then
        '        ' do not use!
        '        'bReturnValue = bReturnValue
        '        'Else
        '        oPayment = oPayments.Add()
        '        oPayment.objFile = oFile
        '        oPayment.VB_Profile = oProfile
        '        oPayment.Cargo = oCargo
        '        oPayment.ImportFormat = iImportFormat
        '        oPayment.StatusCode = sStatusCode

        '        'Continue to the payment-object
        '        bReturnValue = oPayment.ImportAdyen_XML(nodePaymentList) ' pass list of "settlements" to payment, to create many invoices
        '        'If bReturnValue = False Then
        '        ' Exit For
        '        ' End If
        '        'End If
        '        'Next lNodePaymentCounter

        '        ' New 31.03.05
        '        AddInvoiceAmountOnBatch()
        '        nMON_TransferredAmount = nMON_InvoiceAmount

        '        ReadAdyen_XML = bReturnValue
        '        On Error GoTo 0
        '        Exit Function

        'ERR_ReadAdyen_XML:

        '        If Not nodePaymentList Is Nothing Then
        '            nodePaymentList = Nothing
        '        End If
        '        If Not nodePayment Is Nothing Then
        '            nodePayment = Nothing
        '        End If
        '        If Not nodeBatch Is Nothing Then
        '            nodeBatch = Nothing
        '        End If

        '        If Err.Description.Length > 7 Then
        '            If Err.Description.Substring(0, 11) = "ReadAdyen" Then
        '                Err.Raise(Err.Number, Err.Source, Err.Description)
        '            Else
        '                Err.Raise(Err.Number, "ReadAdyen_XML - Batch", Err.Description)
        '            End If
        '        Else
        '            Err.Raise(Err.Number, "ReadAdyen_XML - Batch", Err.Description)
        '        End If

    End Function
    Private Function ReadPayex_XML(ByVal nodeBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodePaymentList As System.Xml.XmlNodeList
        Dim nodePayment As System.Xml.XmlNode
        Dim sTmpName As String = ""

        On Error GoTo ERR_ReadPayex_XML

        bReturnValue = False
        sFormatType = "Payex_XML"

        nodePaymentList = nodeBatch.SelectNodes("ISO:TRAN", nsMgr)


        For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

            'Pick an element from the list
            nodePayment = nodePaymentList.Item(lNodePaymentCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImportPayex_XML(nodePayment, nsMgr)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodePaymentCounter

        ' New 31.03.05
        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadPayex_XML = bReturnValue
        On Error GoTo 0
        Exit Function

ERR_ReadPayex_XML:

        If Not nodePaymentList Is Nothing Then
            nodePaymentList = Nothing
        End If
        If Not nodePayment Is Nothing Then
            nodePayment = Nothing
        End If
        If Not nodeBatch Is Nothing Then
            nodeBatch = Nothing
        End If

        If Err.Description.Length > 7 Then
            If Err.Description.Substring(0, 11) = "ReadPayex" Then
                Err.Raise(Err.Number, Err.Source, Err.Description)
            Else
                Err.Raise(Err.Number, "ReadPayex_XML - Batch", Err.Description)
            End If
        Else
            Err.Raise(Err.Number, "ReadPayex_XML - Batch", Err.Description)
        End If

    End Function 'XNET - 05.12.2011 - Whole function
    Private Function ReadeFaktura_e2b(ByVal nodeBatch As System.Xml.XmlNode, ByVal nsMgr As System.Xml.XmlNamespaceManager) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodePaymentList As System.Xml.XmlNodeList
        Dim nodePayment As System.Xml.XmlNode
        'Dim sTmp As String = ""

        On Error GoTo ERR_ReadeFaktura_e2b

        bReturnValue = False
        sFormatType = "eFaktura_e2b"
        ' Find the batch-date from InvoiceDate in XML file (like 2020-09-23)
        Dim stmp As String
        'stmp = GetInnerText(nodeBatch, "/ISO:MessageTimestamp", nsMgr)
        'dDATE_Production = StringToDate(Left(Replace(GetInnerText(nodeBatch, "/ISO:MessageTimestamp", nsMgr), "-", ""), 8))

        nodePaymentList = nodeBatch.SelectNodes("ISO:Invoice", nsMgr)


        For lNodePaymentCounter = 0 To nodePaymentList.Count - 1

            'Pick an element from the list
            nodePayment = nodePaymentList.Item(lNodePaymentCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImporteFaktura_e2b(nodePayment, nsMgr)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodePaymentCounter

        ' New 31.03.05
        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        ReadeFaktura_e2b = bReturnValue
        On Error GoTo 0
        Exit Function

ERR_ReadeFaktura_e2b:

        If Not nodePaymentList Is Nothing Then
            nodePaymentList = Nothing
        End If
        If Not nodePayment Is Nothing Then
            nodePayment = Nothing
        End If
        If Not nodeBatch Is Nothing Then
            nodeBatch = Nothing
        End If

        If Err.Description.Length > 7 Then
            If Err.Description.Substring(0, 11) = "ReadPayex" Then
                Err.Raise(Err.Number, Err.Source, Err.Description)
            Else
                Err.Raise(Err.Number, "ReadeFaktura_e2b - Batch", Err.Description)
            End If
        Else
            Err.Raise(Err.Number, "ReadeFaktura_e2b - Batch", Err.Description)
        End If

    End Function 'XNET - 05.12.2011 - Whole function
    Private Function ReadeGlobal(ByVal nodBatch As MSXML2.IXMLDOMNode) As Boolean
        Dim oPayment As Payment
        Dim bReturnValue As Boolean
        Dim lNodePaymentCounter As Long
        Dim lNodePaymentListCounter As Long
        Dim nodPaymentList As MSXML2.IXMLDOMNodeList
        Dim nodPayment As MSXML2.IXMLDOMNode

        'XOKNET - 18.11.2010 - Added errorhandling
        On Error GoTo ERR_ReadeGlobal

        bReturnValue = False

        'The nodPaymentList will contain all the Account-Tags under this INS2000EXPORT
        nodPaymentList = nodBatch.selectNodes("ACCOUNT")

        'sREF_Own = nodBatch.selectSingleNode("child::*[name()='PayersReference']").nodeTypedValue
        'nMON_InvoiceAmount = Val(Replace(nodBatch.selectSingleNode("child::*[name()='Amount']").nodeTypedValue, ".", vbNullString))
        'bAmountSetInvoice = True
        sFormatType = "eGlobal"

        For lNodePaymentCounter = 0 To nodPaymentList.length - 1

            'Pick an element from the list
            nodPayment = nodPaymentList.item(lNodePaymentCounter)

            '    Set nodPaymentDetailsList = nodPayment.selectNodes("child::*[name()='PaymentDetails']")
            '    Set nodPaymentDetails = nodPaymentDetailsList.Item(lNodePaymentListCounter)

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.Cargo = oCargo
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode

            'Continue to the payment-object
            bReturnValue = oPayment.ImporteGlobal(nodPayment)
            If bReturnValue = False Then
                Exit For
            End If

        Next lNodePaymentCounter

        ' New 31.03.05
        AddInvoiceAmountOnBatch()


        ReadeGlobal = bReturnValue
        'XOKNET - 18.11.2010 - Added errorhandling
        On Error GoTo 0
        Exit Function

ERR_ReadeGlobal:

        Err.Raise(Err.Number, "ReadeGlobal - Batch", Err.Description)

    End Function
    Friend Function ReadConnect_WCFRemit(ByVal wcfPaymentList() As Conecto.BabelTransactionDto) As Boolean
        Dim wcfPayment As Conecto.BabelTransactionDto
        Dim oPayment As vbBabel.Payment
        Dim bReturnValue As Boolean = False
        Dim iOldGroupPaymentId As Integer = -1

        On Error GoTo ERR_ReadConnect_WCFRemit

        sFormatType = "Conecto_WCF_Remit"

        If oCargo.Special = "CONECTO_WCR_REMIT_INKASSO" Then
            For Each wcfPayment In wcfPaymentList

                If iOldGroupPaymentId <> wcfPayment.GroupPaymentId Then
                    iOldGroupPaymentId = wcfPayment.GroupPaymentId
                    oPayment = oPayments.Add()
                    oPayment.VB_Profile = oProfile
                    oPayment.Cargo = oCargo
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode

                    'Continue to the payment-object
                    bReturnValue = oPayment.ReadConnect_WCFRemit(wcfPayment, False)
                    If bReturnValue = False Then
                        Exit For
                    End If
                Else
                    'Just add a new invoice
                    'Continue to the payment-object
                    bReturnValue = oPayment.ReadConnect_WCFRemit(wcfPayment, True)
                    If bReturnValue = False Then
                        Exit For
                    End If
                End If

            Next wcfPayment
        Else
            For Each wcfPayment In wcfPaymentList

                oPayment = oPayments.Add()
                oPayment.VB_Profile = oProfile
                oPayment.Cargo = oCargo
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode

                'Continue to the payment-object
                bReturnValue = oPayment.ReadConnect_WCFRemit(wcfPayment, False)
                If bReturnValue = False Then
                    Exit For
                End If

            Next wcfPayment
        End If

        ' New 31.03.05
        AddInvoiceAmountOnBatch()

        Return True

ERR_ReadConnect_WCFRemit:

        Err.Raise(Err.Number, "ReadConnect_WCFRemit", Err.Description)


    End Function

    Private Function ReadABA_Australia() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            Select Case Left(sImportLineBuffer, 1)

                '1 - transaction record
                Case "1"
                    bReadLine = False

                    ' At the end (next last line) there is a debit-trans, which is the total debit, and not a real payment
                    ' 1343-001130608003 130035052953KVAERNER OIL & GAS AUSTRALIA PL 0000000028/02/2010343-001130608003KOP             00000000
                    ' Exclude that one
                    If Mid(sImportLineBuffer, 19, 2) = "13" Then
                        bCreateObject = False
                        bReadLine = True
                    Else
                        bCreateObject = True
                    End If

                Case "7" ' end of file
                    bReadLine = False
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadABA_AustraliaRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadABA_Australia = sImportLineBuffer
    End Function
    Private Function ReadNemkoCanadaSalary() As String
        Dim oPayment As Payment
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If Mid(sImportLineBuffer, 7, 4) = "ZTRL" Then
                ' end of file
                bReadLine = False
                Exit Do

            Else
                bReadLine = False
                bCreateObject = True
            End If

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadNemkoCanadaSalaryRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadNemkoCanadaSalary = sImportLineBuffer
    End Function
    Private Function ReadAutogiro_Bankgirot() As String
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String

        bFromLevelUnder = False
        sFormatType = "Autogiro_Bankgirot"
        sStatusCode = "02"

        Do While True
            bReadLine = False
            bCreateObject = False

            Select Case Mid(sImportLineBuffer, 1, 2)

                Case "01" '�ppningspost TK01
                    bReadLine = True
                    bCreateObject = False

                Case "15" 'New 09.12.2021
                    ' do not have documentation for this record, skip
                    bReadLine = True
                    bCreateObject = False

                Case "32", "82" 'Betalningspost TK32 or TK82
                    bReadLine = False
                    bCreateObject = True

                Case "09" 'Slutsummapost, TK09
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = Trim(CObj(sI_Account))
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        '09.12.2021 - oppdateres med 0.00 dersom vi ikke setter neste linje - strange !!!!
        nMON_InvoiceAmount = 0
        nMON_TransferredAmount = 0
        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()
        bAmountSetInvoice = True
        ' 09.12.2021 kommentert ut de neste linjer
        'nMON_TransferredAmount = nMON_InvoiceAmount
        bAmountSetTransferred = True
        ReadAutogiro_Bankgirot = sImportLineBuffer
    End Function
    ' XNET 14.10.2010 Added function ReadSAP_PAYEXT_PEXR2002
    Private Function ReadSAP_PAYEXT_PEXR2002() As String
        Dim oPayment As Payment, oInvoice As Invoice
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String

        bFromLevelUnder = False
        sFormatType = "SAP_PAYEXT_PEXR2002"
        'sStatusCode = "02"
        ' 26.09.2016 May be dangerous, but for PacificDrilling this file is a outgoing paymentsfile
        sStatusCode = "00"

        Do While True
            bReadLine = False
            bCreateObject = False


            'Check linetype
            ' Read recordtype from first 10
            Select Case Trim$(Mid$(sImportLineBuffer, 1, 10))
                Case "EDI_DC40"
                    ' New payment
                    bReadLine = True
                    bCreateObject = True

                Case "E2IDKU1"
                    ' second headerline - continue to payment.
                    ' no specific batchlevel in PAYEXT files
                    bReadLine = True
                    bCreateObject = True

                Case "E2IDB02002"
                Case "E2IDPU1"
                Case "E2IDKU3002"
                Case "E2IDKU5"
                Case "E2IDPU5"
                Case "E2EDKA1003"
                Case "E2IDB02002"
                Case Else
                    ' many other legal recordtypes, which we do not use
            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        AddInvoiceAmountOnBatch()
        bAmountSetInvoice = True
        nMON_TransferredAmount = nMON_InvoiceAmount
        bAmountSetTransferred = True
        ReadSAP_PAYEXT_PEXR2002 = sImportLineBuffer

    End Function
    'XNET 29.10.2010 added next function
    ' Used by DnBNOR (Sigurd) to test DnBNOR Connect
    Private Function ReadDnBNOR_Connect_Test() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean

        bFromLevelUnder = False
        ' XNET 21.12.2011 changed from DnBNOR to DNB
        sFormatType = "DnB_Connect_Test"

        ' import <PmtInfId> from Excel
        sREF_Own = xDelim(sImportLineBuffer, ";", 40, Chr(34)) 'AN

        Do While True

            oPayment = oPayments.Add()
            oPayment.objXLSheet = oXLWSheet
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDnBNOR_Connect_Test = sImportLineBuffer

            If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    ' XNET 02.11.2010 New format Girodirekt
    Private Function ReadGirodirekt_Plusgiro() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left$(sImportLineBuffer, 4)
                Case "MH00"  ' Innledningspost MH00
                    bReadLine = True
                    bCreateObject = False

                Case "PI00"  ' Betalning/Girering till PlusGirokonto

                    sFormatType = "Girodirekt"
                    bReadLine = False
                    bCreateObject = True  ' jump to payment

                    sStatusCode = "00"

                Case "MT00"  ' Avslutningspost
                    Exit Do

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                sImportLineBuffer = ReadGiroDirektRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)

                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadGirodirekt_Plusgiro = sImportLineBuffer
    End Function
    'XNET 11.11.2010 added next function
    Private Function ReadConecto_Dekkmann() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sTmpDate As String

        bFromLevelUnder = False

        ' No batchlevel in this fileformat

        Do While True
            bReadLine = False
            bCreateObject = True

            If sImportLineBuffer = "ERROR" Or Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

            If InStr(1, sImportLineBuffer, "TOTAL", vbTextCompare) > 0 Then
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop
        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadConecto_Dekkmann = sImportLineBuffer

    End Function
    'XNET 01.12.2010 Added function ReadISABEL_APS130_Foreign
    Private Function ReadISABEL_APS130_Foreign() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left$(sImportLineBuffer, 1)
                Case "0"  ' Headerrecord
                    bReadLine = True
                    bCreateObject = False

                Case "1"  ' Datarecord

                    sFormatType = "Isabel_APS130"
                    bReadLine = False
                    bCreateObject = True  ' jump to payment

                    sStatusCode = "00"

                Case "9"  ' Trailerrecord
                    Exit Do

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'XNET - 06.12.2010 - Uncommented and changed next 3 lines
            If bReadLine Then
                sImportLineBuffer = ReadISABEL_APS130_ForeignRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)

                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadISABEL_APS130_Foreign = sImportLineBuffer
    End Function
    ' XNET 06.01.2011 added next function
    Private Function ReadSEBScreen_Finland() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False
        bReadLine = False
        sFormatType = "SEBScreen_Finland"

        bFromLevelUnder = False

        Do While True
            bCreateObject = True

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If Left$(sImportLineBuffer, 4) = ":Z1:" Then
                'Trailer part, No of transactions
                ' Exit
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadSEBScreen_Finland = sImportLineBuffer

    End Function
    ' XokNET 10.03.2011 - added function
    Private Function ReadGC_Rieber_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "GC_Rieber_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadGC_Rieber_UK = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    ' XNET 20.06.2011 -
    ' Added function to set index from 1 and up, in sequence
    Public Function ReIndexPayments() As Boolean
        Dim i As Double
        Dim oPayment As vbBabel.Payment
        '
        On Error GoTo errReindex

        i = 0
        For Each oPayment In Payments
            i = i + 1
            oPayment.Index = i
        Next

        ReIndexPayments = True
        Exit Function

errReindex:
        ReIndexPayments = False

    End Function
    Private Function ReadBACSTel() As String
        'XokNET 05.01.2015 Take whole function

        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sResponse As String
        Dim oClient As Client
        Dim oaccount As Account
        Dim bCreateObject As Boolean
        Dim bReadLine As Boolean

        bFromLevelUnder = False
        bReadLine = False
        bCreateObject = True

        sFormatType = "BACSTel"

        Do While True

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            ReadBACSTel = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If bReadLine Then
                If oFile.AtEndOfStream = True Then
                    sImportLineBuffer = ""
                Else
                    'Read new record
                    sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                End If
            End If

            ' XNET 05.01.2015 New recordtypes introduced by DNV UK
            If EmptyString(sImportLineBuffer) Or Left$(sImportLineBuffer, 4) = "EOF1" Then
                ' carry on
                bCreateObject = False
                bReadLine = True
            End If
            If EmptyString(sImportLineBuffer) Or Left$(sImportLineBuffer, 4) = "EOF2" Then
                ' carry on
                bCreateObject = False
                bReadLine = True
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Or Left$(sImportLineBuffer, 4) = "UTL1" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

    End Function
    ' XNET 23.06.2011 - added function
    Private Function ReadAkerSolutions_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean

        On Error GoTo ERR_ReadAkerSolutions_UK

        bFromLevelUnder = False
        sFormatType = "AkerSolutions_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadAkerSolutions_UK = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()
        Exit Function

ERR_ReadAkerSolutions_UK:

        Err.Raise(Err.Number, "ReadAkerSolutions_UK - Batch", Err.Description)

    End Function
    Private Function ReadMT942_Odin() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "MT942_Odin"

        bFromLevelUnder = False

        Do While True
            bCreateObject = False

            Select Case Mid$(sImportLineBuffer, 9, 3)

                Case "20:" 'Transaction Reference Number
                    If bFromLevelUnder Then
                        ' up to BabelFile, and create new batch
                        Exit Do
                    End If
                    ' the real info is in next line
                    sImportLineBuffer = ReadGenericRecord(oFile)

                    ':20:0712310645560001
                    sREF_Bank = Trim$(Mid$(sImportLineBuffer, 13)) 'Transaction reference number
                    bReadLine = True
                    bCreateObject = False

                Case "25:" 'Account Identification
                    bReadLine = False
                    bCreateObject = True

                Case "61:"
                    bReadLine = False
                    bCreateObject = True

                Case "90C" 'MT942 - Summary credit transaction
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case "90D" 'MT942 - Summary debit transaction
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case Else
                    ' other linetypes;
                    ' another 61-record ?
                    If IsNumeric(Mid$(sImportLineBuffer, 13, 6)) And InStr(sImportLineBuffer, "#") > 0 Then
                        bReadLine = False
                        bCreateObject = True
                    End If

            End Select

            If bReadLine Then
                sImportLineBuffer = ReadMT940Record(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'ReadMT940 = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadMT942_Odin = sImportLineBuffer

    End Function
    Private Function Read_DnV_UK_BACS() As String
        'XNET 14.07.2011 Added function
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sResponse As String
        Dim oClient As Client
        Dim oaccount As Account

        On Error GoTo ERR_ReadDNV_UK

        bFromLevelUnder = False

        sFormatType = "DnV_UK_Bacs"

        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            Read_DnV_UK_BACS = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Or Mid$(sImportLineBuffer, 65, 6) = "CONTRA" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Exit Function

ERR_ReadDNV_UK:

        Err.Raise(Err.Number, "Read_DnV_UK_Bacs - Batch", Err.Description)

    End Function
    Private Function Read_Morpol_UK_BACS() As String
        'XNET 15.07.2011 Added function
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sResponse As String
        Dim oClient As Client
        Dim oaccount As Account

        On Error GoTo ERR_ReadMorpol_UK

        bFromLevelUnder = False

        sFormatType = "Morpol_UK_Bacs"

        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            Read_Morpol_UK_BACS = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Exit Function

ERR_ReadMorpol_UK:

        Err.Raise(Err.Number, "Read_Morpol_UK_Bacs - Batch", Err.Description)

    End Function
    'XNET - 11.08.2011 - New function
    Private Function ReadGivertelefon() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean
        Dim sTempDate As String

        bFromLevelUnder = False

        sFormatType = "Givertelefon"
        sTempDate = Mid$(sImportLineBuffer, 169, 10)

        Do While True

            If sTempDate <> Mid$(sImportLineBuffer, 169, 10) Then
                Exit Do ' New date, create a new batch
            End If

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadGivertelefon = sImportLineBuffer

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

    End Function
    ' XNET 01.09.2011 added function
    Private Function ReadSeaCargo_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "SeaCargo_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSeaCargo_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadSeaCargo_UK - BabelFile", Err.Description)

    End Function
    Private Function Read_Polyeurope_UK() As String
        'XNET 15.11.2011 Added function
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sResponse As String
        Dim oClient As Client
        Dim oaccount As Account

        On Error GoTo ERR_Polyeurope_UK

        bFromLevelUnder = False

        sFormatType = "Polyeurope_UK"

        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            Read_Polyeurope_UK = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Exit Function

ERR_Polyeurope_UK:

        Err.Raise(Err.Number, "Read_Polyeurope_UK - Batch", Err.Description)

    End Function
    ' XNET 29.11.2011 added function
    Private Function ReadMT100() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        sFormatType = "MT100"

        bFromLevelUnder = False

        Do While True
            bCreateObject = False

            Select Case Left$(sImportLineBuffer, 4)

                Case ":20:"
                    ' New payment, carry on
                    bReadLine = True

                Case ":32A"

                    bReadLine = False
                    bCreateObject = True

                Case Else
                    'Error # 10010-029
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-029")

            End Select

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadMT100 = sImportLineBuffer

    End Function
    ' XNET 05.12.2011 added function
    Private Function ReadFalck_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Falck_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadFalck_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadFalck_UK - Batch", Err.Description)

    End Function
    ' XNET 20.12.2011 New function
    Private Function Read_DnBNORUS_ACH_820() As String
        '------------------------------------------------------------------------------
        ' ACH from DnBNOR US, Used for import before export of Wachovia 820 receivables
        '------------------------------------------------------------------------------
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sI_Account As String
        Dim sISO As String
        Dim lNumberOfEntryDetailsRecords As Long, lCountedNoOfEntryDetilsRecords As Long
        Dim sEntryHash As String, nCalculatedBankIdentifications As Double
        Dim nMON_InvoiceDebitAmount As Double
        Dim nMON_InvoiceCreditAmount As Double
        Dim sTemp_Text_E_Statement As String
        Dim sTemp_PayCode As String
        Dim dTempDATE_Payment As Date
        Dim lBatchcounter As Long
        Dim sBankName As String
        Dim sEname As String
        Dim sExtraD1 As String
        Dim sExtraD2 As String
        Dim sBankBranchNO As String
        Dim sVoucherNo As String
        Dim bDebit As Boolean
        Dim iOptionalPositionsAdded As Integer
        Dim bNegative As Boolean
        Dim sTemp As String
        Dim iPos As Integer
        Dim bExported As Boolean


        bFromLevelUnder = False
        sStatusCode = "02"

        On Error GoTo ERR_Read

        Do

            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = vbNullString Or oFile.AtEndOfStream Then
                Exit Do
            End If

            Select Case Left$(sImportLineBuffer, 1)

                Case "A"  'AHDR
                    ' AHDRACH12816001USDLAERDAL             20070124122303

                    If bFromLevelUnder Then
                        Exit Do  ' up to BabelFile to create another batch
                    End If

                    ' XNET 21.12.2011 changed from DnBNOR to DNB
                    sFormatType = "DnBUS ACH 820"
                    oCargo.REF = "ACH"
                    sI_Account = Mid$(sImportLineBuffer, 8, 8)
                    sISO = Mid$(sImportLineBuffer, 16, 3)
                    sI_Branch = Trim$(Mid$(sImportLineBuffer, 19, 20))
                    dDATE_Production = StringToDate(Mid$(sImportLineBuffer, 39, 8))

                    bReadLine = True
                    bCreateObject = False

                    '5 - Batch header
                Case "5"
                    ' ACH
                    ' 5200MN STATE FINANCEUS BANK ACH         7416007162CTXACH PYMT        0701240241042000010000115
                    ' or
                    ' LBX
                    ' 515500029252001070124SECORMARIN0031000011

                    sBatch_ID = Mid$(sImportLineBuffer, 2, 4)  ' Batch Number

                    sBatch_ID = Trim$(Mid$(sImportLineBuffer, 41, 10))  '
                    oCargo.E_Account = sBatch_ID

                    lBatchcounter = lBatchcounter + 1
                    ' Pick CTX or CCD from the 5-record:
                    oCargo.REF = oCargo.REF & Mid$(sImportLineBuffer, 51, 3) ' From ACH to ACHCTX or ACHCCD
                    sVersion = Mid$(sImportLineBuffer, 51, 3) ' CTX or CCD or PPD
                    oCargo.PayCode = Mid$(sImportLineBuffer, 51, 3)           ' CTX or CCD

                    ' Sometimes we get 000 as julian date. If so, put todays date as date;
                    If Mid$(sImportLineBuffer, 76, 3) = "000" Then
                        oCargo.DATE_Payment = DateToString(Date.Today)
                    Else
                        oCargo.DATE_Payment = DateToString(System.DateTime.FromOADate(DateAdd(Microsoft.VisualBasic.DateInterval.Day, Val(Mid(sImportLineBuffer, 76, 3)), StringToDate("20" & Mid(sImportLineBuffer, 70, 2) & "0101")).ToOADate - 1))
                    End If

                    ' Pass some values to payment from 5-record
                    sEname = RTrim$(Mid$(sImportLineBuffer, 5, 16))
                    sExtraD2 = Trim$(Mid$(sImportLineBuffer, 54, 10)) ' Ordering Company Desc Entry
                    sBankBranchNO = Trim$(Mid$(sImportLineBuffer, 80, 8)) ' Field 12, Originating DFI (Orig Bank ID)
                    sExtraD1 = Mid$(sImportLineBuffer, 41, 10) ' Ordering Company ID, Field 5

                    sVoucherNo = vbNullString

                    bReadLine = False
                    bCreateObject = True

                    '6 - Entry detail
                Case "6"
                    ' DO NOT import DirectDebits.
                    ' They have 27 or 37 in pos 2 and 3
                    If Mid$(sImportLineBuffer, 2, 2) = "27" Or Mid$(sImportLineBuffer, 2, 2) = "37" Then
                        bCreateObject = False
                        bReadLine = True
                    Else
                        bCreateObject = True
                        bReadLine = False
                    End If

                    '7 - Entry detail addenda (ACH) or Batch Trailer (Lockbox)
                Case "7"
                    bCreateObject = False
                    bReadLine = True

                    '8 - Batch control record, for ACH only
                Case "8"
                    If sStatusCode = "00" Or StatusCode = "01" Then
                        AddInvoiceAmountOnBatch()
                    Else
                        AddInvoiceAmountOnBatch()
                        AddTransferredAmountOnBatch()
                    End If

                    lNumberOfEntryDetailsRecords = Val(Mid$(sImportLineBuffer, 5, 6))
                    nNo_Of_Records = lNumberOfEntryDetailsRecords
                    sEntryHash = Mid$(sImportLineBuffer, 11, 10)

                    If Mid$(oCargo.REF, 4) = "CTX" Then
                        nMON_InvoiceCreditAmount = Val(Mid$(sImportLineBuffer, 21, 12))
                        nMON_InvoiceDebitAmount = Val(Mid$(sImportLineBuffer, 33, 12))
                    Else
                        nMON_InvoiceDebitAmount = Val(Mid$(sImportLineBuffer, 21, 12))
                        nMON_InvoiceCreditAmount = Val(Mid$(sImportLineBuffer, 33, 12))
                    End If
                    bCreateObject = False
                    bReadLine = True

                    '9 - File trailer
                Case "9"
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'All other BETFOR-types gives error
                    'Error # 10010-002
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-025")

                    Exit Do

            End Select

            'Read next line
            If bReadLine Then
                sImportLineBuffer = Read_DnBNORUS_ACH_IncomingRecord(oFile, oCargo)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.PayCode = oCargo.PayCode
                oPayment.DATE_Payment = oCargo.DATE_Payment
                oPayment.DATE_Value = oCargo.DATE_Payment
                oPayment.MON_AccountCurrency = sISO
                oPayment.I_Name = sI_Branch
                oPayment.E_Name = sEname
                oPayment.BANK_BranchNo = sBankBranchNO
                oPayment.ExtraD1 = sExtraD1
                oPayment.ExtraD2 = sExtraD2
                oPayment.REF_Own = sREF_Own
                oPayment.Exported = bExported


                ' BabelBank Internal paycodes
                ' Lockbox              660
                ' ACH CTX              661
                ' ACH CCD              662
                ' ACH PPD              663
                If Left$(oCargo.REF, 6) = "ACHCTX" Then
                    oPayment.PayCode = "661"
                ElseIf Left$(oCargo.REF, 6) = "ACHCCD" Then
                    oPayment.PayCode = "662"
                ElseIf Left$(oCargo.REF, 6) = "ACHPPD" Then
                    oPayment.PayCode = "663"
                End If

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)

                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop
        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        Read_DnBNORUS_ACH_820 = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "Read_DnBNORUS_ACH_820 - Batch", Err.Description)

    End Function
    ' XNET 24.01.2012 New function
    Private Function Read_Nacha_US() As String
        '------------------------------------------------------------------------------
        ' Nacha outgoing payments
        '------------------------------------------------------------------------------
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sI_Account As String
        'Dim lNumberOfEntryDetailsRecords As Long, lCountedNoOfEntryDetilsRecords As Long
        'Dim sEntryHash As String, nCalculatedBankIdentifications As Double
        'Dim nMON_InvoiceDebitAmount As Double
        'Dim nMON_InvoiceCreditAmount As Double
        'Dim sTemp_Text_E_Statement As String
        'Dim sTemp_PayCode As String
        'Dim dTempDATE_Payment As Date
        'Dim sBankName As String
        'Dim sEname As String
        'Dim sExtraD1 As String
        'Dim sExtraD2 As String
        'Dim sBankBranchNO As String
        'Dim sVoucherNo As String
        'Dim bDebit As Boolean
        'Dim iOptionalPositionsAdded  As Integer
        'Dim bNegative As Boolean
        'Dim sTemp As String
        'Dim iPos As Integer
        'Dim bExported As Boolean


        bFromLevelUnder = False
        sStatusCode = "00"

        On Error GoTo ERR_Read

        Do

            bReadLine = False

            If sImportLineBuffer = "ERROR" Or sImportLineBuffer = vbNullString Or oFile.AtEndOfStream Then
                Exit Do
            End If

            Select Case Left$(sImportLineBuffer, 1)

                Case "1"  ' Filestart, probably not going to happen, jump up

                    If bFromLevelUnder Then
                        Exit Do  ' up to BabelFile to create another batch
                    End If

                    '5 - Batch header
                Case "5"
                    '5220DOCUMENT CO - VC                    3480220000CCDInvoices  110331110331   1003095910012193

                    sBatch_ID = Mid$(sImportLineBuffer, 88, 7)  ' Batch Number

                    ' Pick CTX or CCD from the 5-record:
                    sVersion = Mid$(sImportLineBuffer, 51, 3) ' CTX or CCD or PPD
                    oCargo.PayCode = Mid$(sImportLineBuffer, 51, 3)           ' CTX or CCD

                    ' Sometimes we get 000 or "   " as julian date. If so, put todays date as date;
                    If Mid$(sImportLineBuffer, 76, 3) = "000" Or Mid$(sImportLineBuffer, 76, 3) = "   " Then
                        oCargo.DATE_Payment = DateToString(Date.Today)
                    Else
                        oCargo.DATE_Payment = DateToString(System.DateTime.FromOADate(DateAdd(Microsoft.VisualBasic.DateInterval.Day, Val(Mid(sImportLineBuffer, 76, 3)), StringToDate("20" & Mid(sImportLineBuffer, 70, 2) & "0101")).ToOADate - 1))
                    End If

                    ' Pass some values to payment from 5-record
                    'sEname = RTrim$(Mid$(sImportLineBuffer, 5, 16))
                    'sExtraD2 = Trim$(Mid$(sImportLineBuffer, 54, 10)) ' Ordering Company Desc Entry
                    'sBankBranchNO = Trim$(Mid$(sImportLineBuffer, 80, 8)) ' Field 12, Originating DFI (Orig Bank ID)
                    ' We have no debitaccount, I_Account, in a Nacha file, use CompanyID instead
                    oCargo.I_Account = Mid$(sImportLineBuffer, 41, 10) ' Ordering Company ID, Field 5
                    sREF_Bank = Trim$(Mid$(sImportLineBuffer, 54, 10))          ' Company Entry Descritpion 54-63

                    bReadLine = True
                    bCreateObject = True

                    '6 - Entry Detail Record
                Case "6"
                    ' New payment, go to Payment
                    bReadLine = False
                    bCreateObject = True

                    '8 - Batch control record, for ACH only
                Case "8"
                    If sStatusCode = "00" Or StatusCode = "01" Then
                        AddInvoiceAmountOnBatch()
                    Else
                        AddInvoiceAmountOnBatch()
                        AddTransferredAmountOnBatch()
                    End If

                    '        If Mid$(oCargo.REF, 4) = "CTX" Then
                    '            nMON_InvoiceCreditAmount = Val(Mid$(sImportLineBuffer, 21, 12))
                    '            nMON_InvoiceDebitAmount = Val(Mid$(sImportLineBuffer, 33, 12))
                    '        Else
                    '            nMON_InvoiceDebitAmount = Val(Mid$(sImportLineBuffer, 21, 12))
                    '            nMON_InvoiceCreditAmount = Val(Mid$(sImportLineBuffer, 33, 12))
                    '        End If
                    bCreateObject = False
                    ' 8 record is end of batch, jump to BabelFile
                    bReadLine = False
                    Exit Do

                    '9 - File trailer
                Case "9"
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'All other BETFOR-types gives error
                    'Error # 10010-002
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-025")

                    Exit Do

            End Select

            'Read next line
            If bReadLine Then
                sImportLineBuffer = Read_NachaUS_Record(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)

                bFromLevelUnder = True
            End If

            If EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop
        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        Read_Nacha_US = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "Read_Nacha_US - Batch", Err.Description)

    End Function
    ' XNET 19.03.2012 added function
    Private Function ReadHandicare_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Handicare_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadHandicare_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "Readhandicare_UK - Batch", Err.Description)

    End Function

    Private Function ReadGjensidigeJDE_Basware() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Try
            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_Basware"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_Basware = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_Basware_BabelFile" & vbCrLf & ex.Message)

        End Try

    End Function
    Private Function ReadGjensidigeJDE_FileNet() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_FileNet"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If

                If oCargo.Special = "P2000" Or oCargo.Special = "P2000_NY" Then
                    ' For P2000, we can have two different inputformats, so must test for shift from ML01/LE1/LE2-record to HB01
                    If Mid$(sImportLineBuffer, 3, 4) = "HB01" Then
                        Exit Do
                    End If
                End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_FileNet = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_FileNet_Batch" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_P2000_XLedger() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_P2000_XLedger"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If

                'If oCargo.Special = "P2000" Or oCargo.Special = "P2000_NY" Then
                '    ' For P2000, we can have two different inputformats, so must test for shift from ML01/LE1/LE2-record to HB01
                '    If Mid$(sImportLineBuffer, 3, 4) = "HB01" Then
                '        Exit Do
                '    End If
                'End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_P2000_XLedger = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_P2000_XLedger_Batch" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_FileNet_XLedger() As String
        '------------------------------------------------------------------------------
        ' 21.09.2020: Filenet format for export to XLedger
        '             Exportformats
        '             - AP02_Filenet.csv
        '             - GL02-SYS_Filenet.csv
        '------------------------------------------------------------------------------
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_FileNet"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If

            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_FileNet_XLedger = sImportLineBuffer

        Catch ex As Exception

            '    Throw New Exception("Function: ReadGjensidigeJDE_FileNet_XLedger_Batch" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_HB01() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_HB01"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If

                If oCargo.Special = "P2000" Or oCargo.Special = "P2000_NY" Then
                    ' For P2000, we can have two different inputformats, so must test for shift fro HB01 record to ML01/LE1/LE2-record
                    If Mid$(sImportLineBuffer, 3, 4) <> "HB01" Then
                        Exit Do
                    End If
                End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_HB01 = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_HB01_Batch" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_CSV_S12() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_CSV_S12"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If

            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_CSV_S12 = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_CSV_S12" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_INPAS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim nMonTotalAMount As Double = 0
        Dim lPaymentCounter As Long = 0
        Dim sE_Account As String = ""
        Dim sI_Account As String = ""
        Dim sDatePayment As String = ""
        Dim sExtraD1 As String = ""
        Dim sE_AccountPrefix As String = ""
        Dim oInvoice As Invoice = Nothing

        bFromLevelUnder = False

        sFormatType = "ReadGjensidigeJDE_INPAS"

        'only one batch pr importfile

        Do
            ' Ikke ta med recordtype = 9.
            If xDelim(sImportLineBuffer, ";", 3) = "0" Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

            Else
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

        Loop

        ' Special for NICE
        'Check for total sum = 0

        'After conversion of each input file a check should be made to verify that the total sum = 0
        'If the total sum is <> 0 a separate record should be created with amount = -Total amount
        'This record should have the same fixed input, entity code, voucher num, voucher type, GL-dates, text, currency like the other records. The following Output fields for this record should be filled with this fixed information:
        If iImportFormat = BabelFiles.FileType.GjensidigeJDE_NICE Then
            ' g� gjennom bel�pen i Payments
            For Each oPayment In Payments
                nMonTotalAMount = nMonTotalAMount + oPayment.MON_InvoiceAmount
                lPaymentCounter = lPaymentCounter + 1
				' catch values for last payment, to use in diff-record
				'sE_Account = oPayment.E_Account
				' 06.10.2022 - skal alltid v�re konto 5575100
				sE_Account = "5575100"
				sDatePayment = oPayment.DATE_Payment
                sI_Account = oPayment.I_Account
                sE_AccountPrefix = oPayment.E_AccountPrefix
				'sExtraD1 = oPayment.ExtraD1
				' 06.10.2022 - bruk alltid 40-14271  for denne ekstra recorden
				sExtraD1 = "40-14271 "
			Next
            If nMonTotalAMount <> 0 Then
                ' create a diff-record:
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode

                bFromLevelUnder = True

                ' Pick data from last payment

                oPayment.PayType = "D"
                '4	KONTO	Col D
                oPayment.E_Account = sE_Account

                '5. Tekst COl 4, not Index usel
                '6 Bel�p, col f
                oPayment.MON_InvoiceAmount = nMonTotalAMount * -1
                oPayment.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                ' 9 �r, col I
                oPayment.DATE_Payment = sDatePayment

                '11	CostCenter	Col K
                oPayment.I_Account = sI_Account

                nMON_TransferredAmount = nMON_InvoiceAmount * -1
                oInvoice = oPayment.Invoices.Add()
                oInvoice.objFile = oFile
                oInvoice.VB_Profile = oProfile

                oInvoice.MON_InvoiceAmount = oPayment.MON_InvoiceAmount
                oInvoice.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                oPayment.MON_InvoiceCurrency = "DKK"

                'Check if it is a new and known accountno.
                oPayment.VB_FilenameOut_ID = CheckAccountNo(oPayment.I_Account, "", oProfile)


                'Data Transformation 1. Company code - ID
                oPayment.I_Client = "24721"  ' Fixed

                ' Data Transformation 5. Account ledger
                ' ------------------------------------
                'The information used for the output account ledger is found from an account table lookup in GL02b mapping table �10-Account Ledger� column A and B.
                oPayment.E_AccountPrefix = sE_AccountPrefix

                ' Data Transformation 6. Costcenter
                ' ---------------------------------
                'The information used for the output cost center is found from input ID 11. 
                'The conversion of the Cost Centre is according to this rule
                'Input file ID 11	Output file ID 11
                'NEXT	40-68030
                'Any other	40-68010

                oPayment.ExtraD1 = sExtraD1
                oPayment.MergeCriteria = oPayment.E_Account & oPayment.ExtraD1
                oPayment.MergeThis = True
            End If
        End If

        AddInvoiceAmountOnBatch()

        ReadGjensidigeJDE_INPAS = sImportLineBuffer


    End Function
    Private Function ReadGjensidigeJDE_F2100_SE_DK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_F2100"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_F2100_SE_DK = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_F2100_SE_DK_Batch" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_F2100_SE_DK_GL02b() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_F2100"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_F2100_SE_DK_GL02b = sImportLineBuffer

        Catch ex As Exception
            Throw New Exception("Function: ReadGjensidigeJDE_F2100_SE_DK_GL02b_Batch" & vbCrLf & ex.Message)
        End Try
    End Function
    Private Function ReadGjensidigeJDE_HB01_H1() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        Try

            bFromLevelUnder = False

            sFormatType = "ReadGjensidigeJDE_HB01_H1"

            'only one batch pr importfile

            Do
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                    Exit Do
                End If
            Loop
            AddInvoiceAmountOnBatch()

            ReadGjensidigeJDE_HB01_H1 = sImportLineBuffer

        Catch ex As Exception

            Throw New Exception("Function: ReadGjensidigeJDE_HB01_H1_Batch" & vbCrLf & ex.Message)

        End Try


    End Function
    Private Function ReadGjensidigeJDE_Aditro() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "ReadGjensidigeJDE_Aditro"

        'only one batch pr importfile

        Do
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                Exit Do
            End If
        Loop
        AddInvoiceAmountOnBatch()

        ReadGjensidigeJDE_Aditro = sImportLineBuffer
    End Function
    Private Function ReadGjensidigeJDE_IDIT() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        sFormatType = "ReadGjensidigeJDE_IDIT"

        'only one batch pr importfile

        Do
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If oFile.AtEndOfStream And EmptyString(sImportLineBuffer) Then
                Exit Do
            End If
        Loop
        AddInvoiceAmountOnBatch()

        ReadGjensidigeJDE_IDIT = sImportLineBuffer
    End Function
    ' XokNET 12.04.2012 - added DTAZV
    Private Function ReadDTAZV() As String
        Dim oPayment As Payment
        Dim sTmp As String
        Dim dDate_Payment As Date, sValuta As String
        Dim bFoundEndBatchRecord As Boolean
        Dim nTotalAmount As Double
        Dim bx As Boolean

        bFoundEndBatchRecord = False

        Do While True

            ' We are at the T-record here
            ' ---------------------------
            ' Imports a "stream" - files normally have no CR/LF
            ''''sImportLineBuffer = ReadDTAZVRecord(oFile, sImportLineBuffer)


            'Check if we have a T-record;
            If Mid$(sImportLineBuffer, 1, 5) = "0768T" Then
                'OK, regular transaction
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            ElseIf Mid$(sImportLineBuffer, 1, 5) = "0256Z" Then
                'End of file
                bFoundEndBatchRecord = True
                Exit Do
            Else
                'Illegal record
                'Error # 10024-020
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10024-020")
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        If bFoundEndBatchRecord Then
            nTotalAmount = 0
            'Check the total number of transactions
            If Payments.Count <> Val(Mid$(sImportLineBuffer, 21, 15)) Then
                'Error 10030-020
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10030-020", Payments.Count, Val(Mid$(sImportLineBuffer, 11, 7)))
            End If
            For Each oPayment In Payments
                ' Total only the "integer" part !
                nTotalAmount = nTotalAmount + Int(oPayment.MON_InvoiceAmount / 100)
            Next oPayment
            'Check the total amount
            If nTotalAmount <> Val(Mid$(sImportLineBuffer, 6, 15)) Then
                'Error 10031-020
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10031-020", Trim$(Str(nTotalAmount)), Mid$(sImportLineBuffer, 65, 13), "Total Amount")
            End If

            If sStatusCode = "00" Or StatusCode = "01" Then
                bx = AddInvoiceAmountOnBatch()
            Else
                bx = AddInvoiceAmountOnBatch()
                bx = AddTransferredAmountOnBatch()
            End If
            sImportLineBuffer = Mid$(sImportLineBuffer, 257) ' Remove Z (should be the last record!)
            ReadDTAZV = sImportLineBuffer
        Else
            'Error 10029-020
            Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10029-020")
        End If

    End Function
    ' XNET 24.04.2012 added function
    Private Function ReadOdenberg() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Odenberg"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadOdenberg = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadOdenberg - Batch", Err.Description)

    End Function
    ' XNET 09.10.2012 added next function
    ' and XNET 20.11.2012 several changes not xnetted
    Private Function ReadSEB_CI_Incoming() As String
        ' SEB C&I Incoming
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean
        Dim sDelimiter As String
        Dim sSurround As String
        Dim sI_Account As String
        Dim oClient As Client
        Dim oFilesetup As FileSetup
        Dim oaccount As Account
        Dim sTemp1 As String
        Dim bRequiredRowsArePresent As Boolean
        Dim nControlsumPayments As Double
        Dim bCreateObject As Boolean
        '
        '"Kontonummer";"Valuta";"Bokf�ringsdato";"Valutadato";"Bel�p";"Tekst";"Referanse";"Strukturert informasjon";"Tillegsinformasjon";"Account Holder";"Account Name";"Country of Account";"Bank Name";"Acceptance date";"Remitter / Receiver Account Number";"Remitter / Receiver Address";"Remitter / Receiver Branch Code/Address";"Remitter / Receiver Correspondent Bank";"Beneficiary reference party identification";"Beneficiary reference party identification type";"Beneficiary reference party name";"Beneficiary Identification";"Beneficiary Identification Type";"Remitter / Receiver Bank";"Remitter / Receiver Name";"Transaction Code";"Business Description";"Charges details";"Remitter Reference";"Customer reference number";"Reason Code";"GVC";"GVC Text";"Instructed Amount";"Instructed Amount Currency";"Bank Reference";"Additional Booking Reference";"Transaction Currency";"Original Amount";"Original Currency";"Exchange Rate";"Originators Identification";"Originators Identification Type";
        '"Originators Reference (End to End)";"Originator reference party identification";"Originator reference party identification type";"Originator reference party name";"Posting Text";"Purpose";"Remitted Amount";"Remitted Amount Currency";"Remitter Country";"Charges currency";"Charges";"Text Key";"T-Account"
        '"97500720003";"NOK";"08-03-2012";"07-03-2012";"1 173 877,45";"DOMESTIC INCOMING CUSTOMER TRANSFER";"OSL 12030700292";"";"BOLS:230REF:08028122270 RCRF:*90000000:08028122270:08028122270";"SEB LAUNCH TECH AB";"SEB LAUNCH TECH AB - NOK KONTO";"NO";"SEB";"";"00000000000";"POSTBOKS 1843, VIKA;POSTBOKS 1843, VIKA;N-0123 OSLO";"";"";"";"";"";"";"";"";"SKANDINAVISKA ENSKILDA BANKEN";"";"DOMESTIC INCOMING CUSTOMER TRANSFER";"";"";"";"CLR";"";"";"";"";"OINNEI0I201203061817594360";"";"NOK";"1 173 877,45";"NOK";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";""
        '"97500720003";"NOK";"08-03-2012";"07-03-2012";"331,00";"DOMESTIC INCOMING CUSTOMER TRANSFER";"OSL 12030700292";"KID;61046687150;NOK;331,00";"08028122270:08028122270";"SEB LAUNCH TECH AB";"SEB LAUNCH TECH AB - NOK KONTO";"NO";"SEB";"";"64010678607";"NORDSET INDUSTRIOMR�DE AS;7540;KL�BU";"";"";"";"";"";"";"";"";"TASS BIL & KAROSSERI AS";"230";"DOMESTIC INCOMING CUSTOMER TRANSFER";"";"";"";"CLR";"";"";"";"";"OINNEI0I201203061817594360";"";"NOK";"331,00";"NOK";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";""
        '"97500720003";"NOK";"08-03-2012";"07-03-2012";"4 745,00";"DOMESTIC INCOMING CUSTOMER TRANSFER";"OSL 12030700292";"KID;61046879690;NOK;4 745,00";"08028122270:08028122270";"SEB LAUNCH TECH AB";"SEB LAUNCH TECH AB - NOK KONTO";"NO";"SEB";"";"30300545153";"";"";"";"";"";"";"";"";"";"Gunnar Sirekrok";"230";"DOMESTIC INCOMING CUSTOMER TRANSFER";"";"";"";"CLR";"";"";"";"";"OINNEI0I201203061817594360";"";"NOK";"4 745,00";"NOK";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";""

        sDelimiter = ";"
        sSurround = Chr(34) '"
        bFromLevelUnder = False

        sFormatType = "SEB_CI_Incoming"
        bRequiredRowsArePresent = False ' for NO and DE, we have to have specific rows present before we import more lines
        nControlsumPayments = 0

        Do While True

            bCreateObject = True
            ' find receivers account
            sI_Account = xDelim(sImportLineBuffer, sDelimiter, 1, sSurround)

            ' find swift for receivers account
            If sI_Account <> oCargo.I_Account And bProfileInUse Then '06.03.2018
                oCargo.BANK_I_SWIFTCode = ""
                For Each oFilesetup In VB_Profile.FileSetups
                    For Each oClient In oFilesetup.Clients
                        For Each oaccount In oClient.Accounts
                            If sI_Account = oaccount.Account Then
                                oCargo.BANK_I_SWIFTCode = UCase(oaccount.SWIFTAddress)
                                oCargo.I_Account = sI_Account
                                Exit For
                            End If
                        Next oaccount
                        If oCargo.BANK_I_SWIFTCode <> "" Then
                            Exit For
                        End If
                    Next oClient
                    If oCargo.BANK_I_SWIFTCode <> "" Then
                        Exit For
                    End If
                Next oFilesetup

                ' we must have an account and i_swift in clientsetup
                If oCargo.BANK_I_SWIFTCode = "" Then
                    MsgBox("Finner ikke Swift (BIC) for konto " & sI_Account & " i BabelBanks klientoppsett.", vbCritical, "Trenger Swift (BIC)")
                    sImportLineBuffer = "ERROR"
                    ReadSEB_CI_Incoming = "ERROR"
                    Exit Function
                End If

                If oCargo.BANK_I_SWIFTCode <> "" Then
                    ' create new batch for new account
                    ' but not for first account
                    ' and not if we have 0 payments in this batch.
                    If Not oCargo.StatusCode = "first" And oPayments.Count > 0 Then
                        Exit Do
                    Else
                        oCargo.StatusCode = "notfirst"
                    End If
                End If

            End If

            ' flyttet ned, til etter test mot sREF_Bank
            'sREF_Bank = xDelim(sImportLineBuffer, sDelimiter, 7, sSurround)

            If Len(sImportLineBuffer) > 0 Then
                If Mid$(oCargo.BANK_I_SWIFTCode, 5, 2) = "NO" Then
                    ' for Norwegian accounts there are batchlines
                    ' - do not import "batchlines", totallines
                    ' If Col M Transaction Code is blank
                    ' And Col J Remitter / Receiver Account Number = 02
                    ' And Col K Remitter / Receiver Name is blank

                    sTemp1 = xDelim(sImportLineBuffer, sDelimiter, 6, sSurround)
                    If sTemp1.Length < 6 Then
                        sTemp1 = PadRight(sTemp1, 13, "0")
                    End If

                    If sTemp1.Substring(0, 13).ToUpper = "BETALINGSKORT" Then
                        sTemp1 = sTemp1
                    End If
                    If xDelim(sImportLineBuffer, sDelimiter, 13, sSurround) = "" And _
                        (xDelim(sImportLineBuffer, sDelimiter, 10, sSurround) = "00000000000" Or xDelim(sImportLineBuffer, sDelimiter, 10, sSurround) = "0") And _
                        xDelim(sImportLineBuffer, sDelimiter, 11, sSurround) = "" And sTemp1.Substring(0, 13).ToUpper <> "BETALINGSKORT" Then '05.06.2018 - Added last AND

                        ' find batch total
                        '5. Bel�p
                        sTemp1 = Replace(xDelim(sImportLineBuffer, sDelimiter, 5, sSurround), " ", "")
                        nMON_InvoiceAmount = ConvertToAmount(sTemp1, ",", ".")
                        nMON_TransferredAmount = nMON_InvoiceAmount
                        bRequiredRowsArePresent = True

                        ' control of batchtotal against total for each payment;
                        If nControlsumPayments <> nMON_InvoiceAmount Then
                            MsgBox("Avvik mellom buntsum og sum av enkeltbetalinger" & vbCrLf & "Buntsum: " & Format(nMON_InvoiceAmount / 100, "##,#00.00") & vbCrLf & "Sum av betalinger: " & Format(nControlsumPayments / 100, "##,#00.00") & vbCrLf & "Vi avbryter import." & vbCrLf & "Ta kontakt med Visual Banking ", vbCritical, "Feil i importfil")
                            sImportLineBuffer = "ERROR"
                            ReadSEB_CI_Incoming = "ERROR"
                            Exit Function
                        End If

                        ' read new line
                        sImportLineBuffer = ReadGenericRecord(oFile)
                        ' there may also be uncomplete lines, must have info in col 3,4 and 5
                        If EmptyString(xDelim(sImportLineBuffer, sDelimiter, 3, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 4, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 5, sSurround)) Then
                            ' read another line, forget this one
                            sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                        End If


                        ' can also be other breaks, like blank in col M (13)
                    ElseIf bFromLevelUnder Then
                        '14.06.2016 - The test beneath didn't work. The payment was not imported so added 
                        ' And xDelim(sImportLineBuffer, sDelimiter, 7, sSurround) = sREF_Bank 
                        If xDelim(sImportLineBuffer, sDelimiter, 13, sSurround) = "" And xDelim(sImportLineBuffer, sDelimiter, 7, sSurround) = sREF_Bank Then
                            ' create new batch, but must also have this one as a payment. That's why we test on bFromLevelUnder.
                            bRequiredRowsArePresent = True
                            bCreateObject = False

                            ' read new line
                            sImportLineBuffer = ReadGenericRecord(oFile)
                            ' there may also be uncomplete lines, must have info in col 3,4 and 5
                            If EmptyString(xDelim(sImportLineBuffer, sDelimiter, 3, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 4, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 5, sSurround)) Then
                                ' read another line, forget this one
                                sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                            End If


                            Exit Do
                        End If
                    End If

                ElseIf Mid$(oCargo.BANK_I_SWIFTCode, 5, 2) = "DE" Then
                    ' for German accounts, remove lines with Cash Pool Ausgleich in col F
                    If xDelim(sImportLineBuffer, sDelimiter, 6, sSurround) = "Cash Pool Ausgleich" Then
                        ' read new line
                        sImportLineBuffer = ReadGenericRecord(oFile)
                        ' there may also be uncomplete lines, must have info in col 3,4 and 5
                        If EmptyString(xDelim(sImportLineBuffer, sDelimiter, 3, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 4, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 5, sSurround)) Then
                            ' read another line, forget this one
                            sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                        End If

                        bRequiredRowsArePresent = True
                        bCreateObject = False
                    End If
                Else
                    ' other countries - no demands so far
                    ' only US tested, no batchlines or balancelines
                    bRequiredRowsArePresent = True
                End If

                If Mid$(oCargo.BANK_I_SWIFTCode, 5, 2) = "NO" Then
                    ' For NO-accounts only !
                    ' If new "buntnr", then create new batch
                    If xDelim(sImportLineBuffer, sDelimiter, 7, sSurround) <> sREF_Bank And oPayments.Count > 0 Then
                        Exit Do
                    End If
                End If

                sREF_Bank = xDelim(sImportLineBuffer, sDelimiter, 7, sSurround)

                If bCreateObject Then
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile

                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                    bFromLevelUnder = True
                    nControlsumPayments = nControlsumPayments + oPayment.MON_InvoiceAmount
                End If
            End If

            'ReadSEB_CI_Incoming = sImportLineBuffer

            If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oCargo.Special = "VINGCARD_UK" Or Not RunTime() Then
                Exit Do
            End If

        Loop

        ' the required lines may be after the payments ! thus, must be tested here -
        ' M� bare drite i de kontrollene, da det er s� mye rart i dette formatet.
        'If Not bRequiredRowsArePresent Then
        '    ' we do not dare to import if we have not found the lines we think should be present
        '    If Mid$(oCargo.BANK_I_SWIFTCode, 5, 2) = "NO" Then
        '        ' for Norwegian accounts there should be batchlines
        '        MsgBox "Finner ikke Batch-sumlinjer i filen, og vi avbryter import." & vbCrLf & "Ta kontakt med Visual Banking ", vbCritical, "Feil i importfil"
        '        sImportLineBuffer = "ERROR"
        '        'ReadSEB_CI_Incoming = "ERROR"
        '        'Exit Function
        '    ElseIf Mid$(oCargo.BANK_I_SWIFTCode, 5, 2) = "DE" Then
        '        ' for German accounts, we demand rows with Cash Pool Ausgleich in col F
        '        MsgBox "Finner ikke rader med 'Cash Pool Ausgleich' i kolonne F i filen, og vi avbryter import." & vbCrLf & "Ta kontakt med Visual Banking ", vbCritical, "Feil i importfil"
        '        sImportLineBuffer = "ERROR"
        '        ' ReadSEB_CI_Incoming = "ERROR"
        '        'Exit Function
        '    End If
        'End If

        ReadSEB_CI_Incoming = sImportLineBuffer

        '19.05.2018 - Uncommented next 2 lines
        'bx = AddInvoiceAmountOnBatch()
        'nMON_TransferredAmount = nMON_InvoiceAmount

    End Function
    ' XNET 30.10.2012 new function
    Private Function ReadDanskeBankCollection() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "DanskeBankCollection"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDanskeBankCollection = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    ' XNET 30.10.2012 new function
    Private Function ReadAvtalegiroFile() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sI_Account As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Mid$(sImportLineBuffer, 7, 2)
                '20 - Start of batch, have to store information
                Case "20"
                    If bFromLevelUnder Then
                        ' have read a 20-record in batch (new batch)
                        ' jump up, to create object
                        Exit Do
                    Else
                        'Extract data from sImportLineBuffer given from function Import
                        sBatch_ID = Mid$(sImportLineBuffer, 18, 7) '
                        sREF_Own = Mid$(sImportLineBuffer, 9, 9)
                        sI_Account = Mid$(sImportLineBuffer, 25, 11)

                        sFormatType = "Avtalegiro"

                        bReadLine = True
                        bCreateObject = False

                    End If

                    '30 - Bel�pspost 1
                Case "30"
                    bReadLine = False
                    bCreateObject = True

                    '70 - FBO
                Case "70"
                    bReadLine = False
                    bCreateObject = True

                    '88 - End of Batch
                Case "88"
                    ' NY000088 - Sluttrecord oppdrag
                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 25, 17)) ' Sum amount in batch, NB! in Kroner
                    nMON_TransferredAmount = nMON_InvoiceAmount
                    bAmountSetInvoice = True
                    bAmountSetTransferred = True
                    nNo_Of_Transactions = Val(Mid$(sImportLineBuffer, 9, 8))  ' Antall transer i filen
                    nNo_Of_Records = Val(Mid$(sImportLineBuffer, 17, 8)) ' Antall records i filen
                    dDATE_Production = DateSerial(Mid$(sImportLineBuffer, 46, 2), Mid$(sImportLineBuffer, 44, 2), Mid$(sImportLineBuffer, 42, 2))

                    bReadLine = True  ' read 89 record
                    bCreateObject = False
                    bFromLevelUnder = True  ' true to trigger that if we get a 20-rec, jump up

                Case "89" ' end of file
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadAvtalegiroRecord(oFile) ', oProfile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = sI_Account
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            If sImportLineBuffer = "" Then
                Exit Do
            End If

        Loop

        ReadAvtalegiroFile = sImportLineBuffer
    End Function
    ' XNET 14.11.2012 added next function
    Private Function ReadSEBScreen_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False
        bReadLine = False
        sFormatType = "SEBScreen_UK"

        bFromLevelUnder = False

        Do While True
            bCreateObject = True

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If Left$(sImportLineBuffer, 4) = ":Z1:" Then
                'Trailer part, No of transactions
                ' Exit
                Exit Do
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
                bReadLine = True ' always read next line after beeing into oPayment
            End If

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadSEBScreen_UK = sImportLineBuffer

    End Function
    ' XNET 10.01.2013 added function
    Private Function ReadRappEcosse_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "RappEcosse_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadRappEcosse_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadRappEcosse_UK - Batch", Err.Description)

    End Function
    ' XNET 21.01.2013 added function
    Private Function ReadDuni_Salary_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Duni_Salary_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadDuni_Salary_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadDuni_Salary_UK - Batch", Err.Description)

    End Function
    ' XNET 28.02.2013 added function
    Private Function ReadTGS_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "TGS_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadTGS_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadTGS_UK - Batch", Err.Description)

    End Function
    ' XNET 28.02.2013 added function
    Private Function ReadSpike_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Spike_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSpike_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadSpike_UK - Batch", Err.Description)

    End Function
    Private Function ReadNordea_SE_Plusgiro_FakturabetalningService() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            sStatusCode = "02" ' Used for Redovisning only (not import of sendfile)

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            Select Case Mid$(sImportLineBuffer, 1, 1)
                '20 - Start of batch, have to store information
                Case "0"
                    If bFromLevelUnder Then
                        ' have read a 0-record in batch (new batch)
                        ' jump up, to create object
                        Exit Do
                    Else
                        'Extract data from sImportLineBuffer given from function Import
                        sFormatType = "Nordea_Fakturabetalingsservice"
                        bReadLine = True
                        bCreateObject = False

                    End If

                    '1 - Mottagare identpost
                Case "1"
                    ' different between Domestic and International
                    ' But try to find some commons here
                    sI_EnterpriseNo = Trim$(Mid$(sImportLineBuffer, 5, 15))
                    bReadLine = True
                    bCreateObject = False

                    ' Her kan vi finne ut om vi har med innland eller utland � gj�re.
                    ' Betalingsset for utland er 0 eller 9, for innland 3,4,5,7
                    If Mid$(sImportLineBuffer, 4, 1) = "0" Or Mid$(sImportLineBuffer, 4, 1) = "9" Then
                        oCargo.PayType = "I"
                    Else
                        oCargo.PayType = "D"
                    End If



                    '2 - Mottagarnamnpost utland
                Case "2"
                    ' used for International

                    ' Will we need anything from here ?
                    ' Not well documented - seems to be some bankinfo and some receiversinfo from example
                    ' Seems like receiver name is 35-67
                    oCargo.E_Name = Mid$(sImportLineBuffer, 35, 33)
                    bReadLine = True
                    bCreateObject = False

                    ' 3- Mottagaradresspost utland or Mottagarpost with different layouts for innland
                Case 3
                    'pass this on to Payment
                    bReadLine = False
                    bCreateObject = True

                Case 4
                    ' pass this on to Payment
                    bReadLine = False
                    bCreateObject = True

                    '5 - Beloppspost debet
                Case 5
                    ' pass this on to Payment
                    bReadLine = False
                    bCreateObject = True

                    '6 - Beloppspost kredit
                Case 6
                    ' pass this on to Payment
                    bReadLine = False
                    bCreateObject = True



                    '8 - Summapost
                Case "8"
                    If Not EmptyString(Mid$(sImportLineBuffer, 32, 17)) Then
                        ' utland, nonsence total
                        nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 32, 17))     ' Non sence total
                    Else
                        ' innland, eller valutafickabel�p
                        nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 19, 13))     ' Non sence total
                    End If
                    bAmountSetInvoice = True
                    If sStatusCode = "02" Then
                        nMON_TransferredAmount = nMON_InvoiceAmount
                        bAmountSetTransferred = True
                    End If
                    nNo_Of_Transactions = Val(Mid$(sImportLineBuffer, 49, 6))  ' Antall transer i filen

                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True  ' true to trigger that if we get a 20-rec, jump up


                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If


            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadReadNordea_SE_FakturabetalningService(oFile) ', oProfile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = sI_Account
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
            'Store end of batch data here (if needed)...
        Loop

        ReadNordea_SE_Plusgiro_FakturabetalningService = sImportLineBuffer
    End Function
    Private Function ReadTotal_IN() As String
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim sI_Account As String
        Dim sAccountCurrency As String
        Dim sPayDate As String
        Dim nTotalNumberOfTransactions As Double

        bFromLevelUnder = False

        sFormatType = "TotalIn"

        Do While True
            bCreateObject = False

            Select Case Left$(sImportLineBuffer, 2) 'XOKNET 27.08.2013

                Case "10"
                    If bFromLevelUnder Then
                        bReadLine = False
                        bCreateObject = False
                        Exit Do
                    Else
                        sI_Account = Trim(Mid(sImportLineBuffer, 3, 36))   ' 08.12.2021 added Trim(
                        sAccountCurrency = Mid(sImportLineBuffer, 39, 3)
                        sPayDate = Mid(sImportLineBuffer, 42, 8)
                        bReadLine = True
                        bCreateObject = False
                    End If

                Case "20"
                    bReadLine = False
                    bCreateObject = True

                    'XOKNET 27.08.2013
                Case "25"
                    bReadLine = False
                    bCreateObject = True

                Case "90"
                    'Check total number of transactions
                    nNo_Of_Transactions = Val(Mid$(sImportLineBuffer, 3, 8))  ' Antal transaktioner. Avser betalningsposter och avdragsposter.
                    nTotalNumberOfTransactions = 0
                    For Each oPayment In Payments
                        'XOKNET 27.08.2013 - Commented next For Each ....
                        'For Each oInvoice In oPayment.Invoices
                        nTotalNumberOfTransactions = nTotalNumberOfTransactions + 1
                        'Next oInvoice
                    Next oPayment
                    If nTotalNumberOfTransactions <> nNo_Of_Transactions Then
                        Err.Raise(11016, "ReadTotalIn", Replace(LRS(11016, Str(nNo_Of_Transactions), Str(nTotalNumberOfTransactions)), "%3", oCargo.FilenameIn))
                    End If
                    'nTotalAmount = 'Totalt belopp.

                    bReadLine = True
                    bCreateObject = False
                    Exit Do


                    '    Case ":62F"
                    '        If bAtLeastOneTransaction Then
                    '            iPos = InStr(16, sImportLineBuffer, ",", vbTextCompare)
                    '            If iPos = 0 Then
                    '                Err.Raise 1, "ReadMT940,", "No closing balance found"
                    '            Else
                    '                sTemp = Mid$(sImportLineBuffer, 16, iPos + 3 - 16)
                    '                nUB = ConvertToAmount(sTemp, vbNullString, ",", False)
                    '                If Mid$(sImportLineBuffer, 6, 1) = "D" Then
                    '                    nUB = nUB * -1
                    '                End If
                    '            End If
                    '            If nUB - nIB <> nTotalAmount Then
                    '                Err.Raise 1, "ReadMT940,", "The total sum of imported amounts, differs from the difference between the opening balance and the closing balance."
                    '            End If
                    '        End If
                    '        bReadLine = True
                    '        bCreateObject = False
                    '
                    '    Case ":62M"
                    '        If bAtLeastOneTransaction Then
                    '            iPos = InStr(16, sImportLineBuffer, ",", vbTextCompare)
                    '            If iPos = 0 Then
                    '                Err.Raise 1, "ReadMT940,", "No closing balance found"
                    '            Else
                    '                sTemp = Mid$(sImportLineBuffer, 16, iPos + 3 - 16)
                    '                nUB = ConvertToAmount(sTemp, vbNullString, ",", False)
                    '                If Mid$(sImportLineBuffer, 6, 1) = "D" Then
                    '                    nUB = nUB * -1
                    '                End If
                    '            End If
                    '            If nUB - nIB <> nTotalAmount Then
                    '                Err.Raise 1, "ReadMT940,", "The total sum of imported amounts, differs from the difference between the opening balance and the closing balance."
                    '            End If
                    '        End If
                    '        bReadLine = True
                    '        bCreateObject = False

                Case Else
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-002")

            End Select

            If bReadLine Then
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = sI_Account
                oPayment.MON_AccountCurrency = sAccountCurrency
                oPayment.DATE_Payment = sPayDate
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

            'ReadMT940 = sImportLineBuffer
            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If


            If oFile.AtEndOfStream = True Then
                If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                    Exit Do
                End If
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadTotal_IN = sImportLineBuffer

    End Function
    ' XNET 09.04.2013 added function
    Private Function ReadVroon_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Vroon_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadVroon_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadVroon_UK - Batch", Err.Description)

    End Function
    ' XNET 10.05.2013 added function
    Private Function ReadFaroe_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Faroe_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadFaroe_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadFaroe_UK - Batch", Err.Description)

    End Function
    ' XNET 13.05.2013 added function
    Private Function ReadAndresenBil() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "AndresenBil"

        Do While True

            If Not EmptyString(sImportLineBuffer) Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True


            Else
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

            If oFile.AtEndOfStream And sImportLineBuffer = "" Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadAndresenBil = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "AndresenBil - Batch", Err.Description)

    End Function
    ' XNET 10.05.2013 added function
    Private Function ReadColArt_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "ColArt_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadColArt_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadColArt_UK - Batch", Err.Description)

    End Function
    ' XNET 10.05.2013 added function
    Private Function Read_SageReport() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "SageReport"

        Do While True

            If Len(sImportLineBuffer) > 70 Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            Else
                If Not oFile.AtEndOfStream Then
                    sImportLineBuffer = ReadGenericRecord(oFile)
                Else
                    sImportLineBuffer = ""
                    Exit Do
                End If
            End If

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        Read_SageReport = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "Read_SageReport - Batch", Err.Description)

    End Function
    ' XOKNET 13.06.2013 added function
    Private Function ReadDRiVR_UK() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "DRiVR_UK"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadDRiVR_UK = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadDRiVR_UK - Batch", Err.Description)

    End Function
    ' XokNET 15.11.2013 Added SEB_Sisu
    Private Function ReadSEB_Sisu() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left$(sImportLineBuffer, 1)

                Case "1"    ' load record
                    Exit Do

                Case "2"        ' 2. Namerecord
                    bReadLine = False
                    bCreateObject = True

                Case "6"        ' 6. Reconciliation record
                    bReadLine = False
                    ' jump to batch
                    bCreateObject = False
                    bFromLevelUnder = True
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            'If oFile.AtEndOfStream = True Then
            ' 11.04.2017 added emptystring...
            If oFile.AtEndOfStream Or EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadSEBSisuRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.StatusCode = oCargo.StatusCode
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadSEB_Sisu = sImportLineBuffer
    End Function
    ' XokNET 21.01.2014 added function
    Private Function ReadVroon_Scotland() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Vroon_Scotland"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadVroon_Scotland = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadVroon_Scotland - Batch", Err.Description)

    End Function
    '07.04.2015 added function
    Private Function ReadGjensidige_Kontovask() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "ReadGjensidige_Kontovask"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer.Trim) = 0 Or oFile.AtEndOfStream Then
                Exit Do
            Else
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadGjensidige_Kontovask = sImportLineBuffer

        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadGjensidige_Kontovask - Batch", Err.Description)

    End Function
    Private Function ReadColArt_Moorbrook() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "ColArt_Moorbrook"

        Do While True
            If xDelim(sImportLineBuffer, ",", 23, Chr(34)) <> "CONTRA" Then  ' last line has Contra, do not import
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile

                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If Len(sImportLineBuffer) = 0 Then
                    Exit Do
                End If
            Else
                sImportLineBuffer = ""
                Exit Do
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadColArt_Moorbrook = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadColArt_Moorbrook - Batch", Err.Description)

    End Function
    ' XOKNET 12.03.2013 Added function
    '******************************************VISMA************************************************
    Friend Function ReadVisma_Database(ByVal oMyExternalDal As vbBabel.DAL, ByVal sMySQL As String, ByRef bContinue As Boolean) As vbBabel.DAL
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bAbandon As Boolean
        Dim bOutOfLoop As Boolean
        Dim bReadLine As Boolean
        Dim bCreateObject As Boolean
        Dim bx As Boolean
        Dim sErrorString As String
        Static sOldCustNoBank As String
        Static sOldDivision As String
        Static sOldLineType As String

        On Error GoTo ERR_ReadVisma_Database

        bFromLevelUnder = False

        sFormatType = "Visma_Database"
        bAbandon = False
        bContinue = True

        Do While True

            If StripSpecialChars(oMyExternalDal.Reader_GetString("CustNoBank")) <> sOldCustNoBank And oPayments.Count > 0 Then
                sOldCustNoBank = StripSpecialChars(oMyExternalDal.Reader_GetString("CustNoBank"))
                Exit Do
            End If
            sOldCustNoBank = oMyExternalDal.Reader_GetString("CustNoBank")
            ' Must create new batch if change in CustNoBank or Division
            If StripSpecialChars(oMyExternalDal.Reader_GetString("Division")) <> sOldDivision And oPayments.Count > 0 Then
                sOldDivision = StripSpecialChars(oMyExternalDal.Reader_GetString("Division"))
                Exit Do
            End If
            sOldDivision = StripSpecialChars(oMyExternalDal.Reader_GetString("Division"))
            ' 01.07.2013 Must create new batch also if change in LineType (type of payment)
            If oMyExternalDal.Reader_GetString("LineType") <> sOldLineType And oPayments.Count > 0 Then
                sOldLineType = oMyExternalDal.Reader_GetString("LineType")
                Exit Do
            End If
            sOldLineType = oMyExternalDal.Reader_GetString("LineType")

            ' Enterpriseno to bank
            sErrorString = "CustNoBank"
            sI_EnterpriseNo = StripSpecialChars(oMyExternalDal.Reader_GetString("CustNoBank").Replace(" ", ""))
            ' Division to bank
            sErrorString = "Division"
            sI_Branch = StripSpecialChars(oMyExternalDal.Reader_GetString("Division"))

            bReadLine = False
            bCreateObject = True

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                If bProfileInUse Then
                    oPayment.VB_Profile = oProfile
                Else
                    bProfileInUse = False
                End If
                oPayment.VB_ProfileInUse = bProfileInUse
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.Visma_FrmNo = sVisma_FrmNo
                oPayment.Visma_DatabaseName = sVisma_DatabaseName
                oPayment.Cargo = oCargo
                oMyExternalDal = oPayment.ReadVisma_Database(oMyExternalDal, sMySQL)
                bFromLevelUnder = True
            End If


            If Not oMyExternalDal.Reader_ReadRecord() Then
                bContinue = False  ' to "warn" oBabelFile that we will stop reading from database
                Exit Do
            End If


        Loop

        ReadVisma_Database = oMyExternalDal

        bx = AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

        Exit Function

ERR_ReadVisma_Database:

        Err.Raise(Err.Number, "ReadVisma_Database - Batch, Errorstring: " & sErrorString, Err.Description)

    End Function
    Private Function ReadCorporateFilePayments() As String

        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            'sStatusCode = "02" ' Used for Redovisning only (not import of sendfile) FEIL

            If sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

            Select Case Mid$(sImportLineBuffer, 1, 4)
                Case "MH00", "MH01", "MH10"
                    If bFromLevelUnder Then
                        ' have read a start of file record
                        ' jump up, to create object
                        Exit Do
                    Else
                        'Extract data from sImportLineBuffer given from function Import
                        sFormatType = "CorporateFilePayments"
                        bReadLine = True
                        bCreateObject = False

                    End If

                Case "MT00", "MT01"
                    ' End of file, jump up
                    Exit Do

                Case "DR00"
                    'Betalningspost, debiteringsbeskjed innland
                    bReadLine = False
                    bCreateObject = True

                Case "PI00" 'Betalningsinstruktion

                    bReadLine = False
                    bCreateObject = True


                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If


            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadCorporateFilePayments_Record(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = sI_Account
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
            sStatusCode = oCargo.StatusCode ' set in babelfile

        Loop

        ReadCorporateFilePayments = sImportLineBuffer

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadCorporateFilePayments - Batch", Err.Description)

    End Function
    ' XNET 21.01.2014 added function
    Private Function ReadPhuel_Oil() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "Phuel_Oil"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadPhuel_Oil = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadPhuel_Oil - Batch", Err.Description)

    End Function
    Private Function ReadHandicareBACS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean

        On Error GoTo ERR_Read

        bFromLevelUnder = False

        sFormatType = "HandicareBACS"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If
            If Mid(sImportLineBuffer, 65, 6) = "CONTRA" Then
                ' last line is a "totalline", exit 
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadHandicareBACS = sImportLineBuffer
        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadHandicareBACS - Batch", Err.Description)

    End Function

    Private Function ReadSwedbank_CardReconciliation() As String
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False
            bCreateObject = False

            Select Case Left(sImportLineBuffer, 4).Trim
                Case "FH4"  'FH4 - Transaction Level Header
                    ' Find some common info from FH4
                    oCargo.I_Name = Mid(sImportLineBuffer, 54, 7)   ' MerchantID, Brukerstedsnr (settes til oPayment.I_adr1 later on)
                    sREF_Bank = Mid(sImportLineBuffer, 84, 4)       ' Bundle referencenumber

                    bReadLine = True
                    bCreateObject = False

                Case "F400" ' F400 - Transaction details
                    ' Go to payment;
                    bReadLine = False
                    bCreateObject = True


                Case "FT4"  'FT4 - Transaction Level Trailer
                    ' End of batch
                    nMON_InvoiceAmount = Val(Replace(Mid(sImportLineBuffer, 153, 14), ",", "")) ' Sum bel�p i batchen
                    nMON_TransferredAmount = nMON_InvoiceAmount
                    bAmountSetInvoice = True
                    bAmountSetTransferred = True
                    nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 140, 11)) ' Antall transer i batchen
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadSwedbank_CardReconciliationRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        AddInvoiceAmountOnBatch()

        ReadSwedbank_CardReconciliation = sImportLineBuffer
    End Function
    Private Function ReadCommerzbank_CSV_Incoming() As String
        '20040000;0402800700;119;20.06.16;97261;EREF+1-12/85-70223-D115554-;SEPA-CT HABEN EINZELBUCHUNG;;;NONREF;29,75;;;20.06.16;;;5;SVWZ+RNR SR28914 VOM ;08.06.16;;;;;;;;;;;RH Operations GmbH + Co.KG;;DEUTDEDKXXX;DE12370700600194073300;166;;;
        '20040000;0402800700;119;20.06.16;97261;EREF+ZV01002042917645000000;SEPA-CT HABEN EINZELBUCHUNG;;;NONREF;32,61;;;20.06.16;;;09;SVWZ+Rech.SR28878;;;;;;;;;;;;MAINTAL HOTEL VERWALTUNGSGE;;PBNKDEFFXXX;DE81500100600974965608;166;;;
        '20040000;0402800700;119;20.06.16;97261;EREF+NOTPROVIDED;SEPA-CT HABEN EINZELBUCHUNG;;;NONREF;59,50;;;20.06.16;;;SVWZ+D112390/ SR28970 / ;59,50;;;;;;;;;;;;Platzl Hotel Inselkammer KG;;SSKMDEMM;DE55701500000000405415;166;;;

        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String
        Dim sTempAmount As String = ""
        Dim nTempAmount As Double

        bFromLevelUnder = False

        Do While True

            sTempAmount = xDelim(sImportLineBuffer, ";", 11)
            nTempAmount = Math.Round(CDbl(sTempAmount))

            If nTempAmount > 0 Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

            Else
                'Do not import outgoing payments
            End If

            sImportLineBuffer = ReadGenericRecord(oFile)

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If sImportLineBuffer = Nothing Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadCommerzbank_CSV_Incoming = sImportLineBuffer
    End Function
    Private Function ReadSG_CSV_Incoming() As String
        'BIC CODE;ACCOUNT NB;ACCOUNT OWNER;ACOUNT CCY;OPENING BALANCE DATE;OPENING BALANCE;CLOSING BALANCE DATE;CLOSING BALANCE;AVAILABLE BALANCE DATE;AVAILABLE BALANCE;OPERATION DATE;OPERATION CODE;ADDITIONAL DETAILS; AMOUNT ;VALUE DATE; CLIENT REF; BANK REF ;INFORMATION TO ACCOUNT OWNER;INFORMATION TO ACCOUNT OWNER;INFORMATION TO ACCOUNT OWNER;INFORMATION TO ACCOUNT OWNER;INFORMATION TO ACCOUNT OWNER;INFORMATION TO ACCOUNT OWNER
        'SOGEFRPP;FR7630003036200002030348194;ASSA ABLOY HOSPITALITY;EUR;21/09/2016;0;22/09/2016;0;22/09/2016;0;22/09/2016;TRF;SR23609 SR237METZ VANNES FERTE/ORDP//NAME/METZ VANNES FERTE HENDA;1668,62;22/09/2016;EREF;8385961816S;/FR018/AUTRES VIREMENTS RECUS;SR23609 SR237METZ VANNES FERTE/ORDP//NAME/METZ VANNES FERTE HENDA;YE INVEST HOTELS/REMI/SR23609 SR23764/EREF/BUCA.RGL.VRT.1572;;;
        'SOGEFRPP;FR7630003036200002030348194;ASSA ABLOY HOSPITALITY;EUR;21/09/2016;0;22/09/2016;0;22/09/2016;0;22/09/2016;TRF;SR23876      HOTELS VAL DE BUSS/ORDP//NAME/HOTELS VAL DE BUSSY/RE;1553,78;22/09/2016;EREF;8480745130S;/FR018/AUTRES VIREMENTS RECUS;SR23876 HOTELS VAL DE BUSS/ORDP//NAME/HOTELS VAL DE BUSSY/RE;MI/SR23876/EREF/BUCA.RGL.VRT.521;;;
        'SOGEFRPP;FR7630003036200002030348194;ASSA ABLOY HOSPITALITY;EUR;21/09/2016;0;22/09/2016;0;22/09/2016;0;22/09/2016;TRF;SR23663      HOTEL GRIL MARSEIL/ORDP//NAME/HOTEL GRIL MARSEILLE L;1021,63;22/09/2016;EREF;8480729933S;/FR018/AUTRES VIREMENTS RECUS;SR23663 HOTEL GRIL MARSEIL/ORDP//NAME/HOTEL GRIL MARSEILLE L;A CALIXTINE ET CIE/REMI/SR23663/EREF/BUCA.RGL.VRT.3715;;;

        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bFromLevelUnder As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String
        Dim sTempAmount As String = ""
        Dim nTempAmount As Double

        bFromLevelUnder = False

        Do While True

            sTempAmount = xDelim(sImportLineBuffer, ";", 14)
            nTempAmount = Math.Round(CDbl(sTempAmount))

            If nTempAmount > 0 Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

            Else
                'Do not import outgoing payments
            End If

            sImportLineBuffer = ReadGenericRecord(oFile)

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If sImportLineBuffer = Nothing Then
                Exit Do
            End If

        Loop

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadSG_CSV_Incoming = sImportLineBuffer
    End Function
    Private Function ReadDNBSingapore_Standard() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = " RECSingapore"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDNBSingapore_Standard = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Public Sub SplitPaymentsWithOCR(ByVal bISOSplitOCR As Boolean, ByVal bISOSplitOCRWithCredits As Boolean)
        ' 20.02.2017
        ' This sub will try to find payments with KID, and split this payment into several payments, one pr KID

        Dim oPayment As Payment
        Dim oTmpPayment As Payment
        Dim oInvoice As Invoice
        Dim i As Integer
        Dim ipaymentCounter As Integer
        Dim iInvoiceCounter As Integer
        Dim bIsOCR As Boolean = True
        Dim iNoOfInvoices As Integer = 0
        Dim dPaymentAmount As Double = 0
        Dim dControlSum As Double = 0
        Dim oTempInvoices As vbBabel.Invoices
        '    Dim bSomePaymentsWhereSplitted As Boolean = False

        For ipaymentCounter = Payments.Count To 1 Step -1
            oPayment = Payments(ipaymentCounter)
            iNoOfInvoices = oPayment.Invoices.Count
            dPaymentAmount = oPayment.MON_InvoiceAmount

            bIsOCR = True
            ' first, check if all invoices have KID
            For Each oInvoice In oPayment.Invoices
                If EmptyString(oInvoice.Unique_Id) Then
                    bIsOCR = False
                    Exit For
                End If
            Next

            If bISOSplitOCR And Not bISOSplitOCRWithCredits Then
                'Don't do any split if there are a creditnote
                For Each oInvoice In oPayment.Invoices
                    If oInvoice.MON_InvoiceAmount < 0 Then
                        bIsOCR = False
                        Exit For
                    End If
                Next
            End If

            If bIsOCR And oPayment.Invoices.Count > 1 Then

                oTempInvoices = oPayment.Invoices

                oPayment.Invoices = Nothing

                oPayment.MATCH_MyField3 = "KIDorig"  ' set a tmp-mark on involved payments
                oTmpPayment = Nothing
                ' first add new payments as many as there are invoices
                'OLD: For iInvoiceCounter = oPayment.Invoices.Count To 2 Step -1
                For iInvoiceCounter = oTempInvoices.Count To 1 Step -1
                    oTmpPayment = New Payment
                    CopyPaymentObject(oPayment, oTmpPayment)
                    'oTmpPayment = Payments.Add
                    oTmpPayment = Payments.VB_AddWithObject(oTmpPayment)
                    oTmpPayment.MATCH_MyField3 = "KIDsplit"  ' set a tmp-mark on involved payments
                    oTmpPayment = Nothing
                Next

                iInvoiceCounter = 0
                For Each oTmpPayment In Payments
                    If oTmpPayment.MATCH_MyField3 = "KIDsplit" Then  ' have a tmp-mark on involved payments
                        oTmpPayment.MergeCriteria = oPayment.I_Account & oPayment.E_Account & oPayment.E_Name & "S" & Chr(34) & "/d" & oPayment.DATE_Payment

                        iInvoiceCounter = iInvoiceCounter + 1
                        ' mark invoices to remove for all payments
                        oTmpPayment.Invoices = New vbBabel.Invoices
                        oInvoice = oTmpPayment.Invoices.VB_AddWithObject(oTempInvoices(iInvoiceCounter))
                        'OLDoTmpPayment.Invoices(iInvoiceCounter).MATCH_Final = False
                        ' reset paymentamounts
                        oInvoice.MATCH_Final = False
                        oTmpPayment.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount
                        oTmpPayment.MON_TransferredAmount = oInvoice.MON_TransferredAmount
                    End If
                Next oTmpPayment
                oTmpPayment = Nothing
                oPayment = Nothing

                '' then remove extra invoices
                'For Each oTmpPayment In Payments
                '    If oTmpPayment.MATCH_MyField3 = "KIDsplit" Then  ' have a tmp-mark on involved payments
                '        For i = oTmpPayment.Invoices.Count To 1 Step -1
                '            If oTmpPayment.Invoices(i).MATCH_Final = True Then
                '                oTmpPayment.Invoices.Remove(i)
                '            End If
                '        Next i
                '        ' reindex invoices
                '        oTmpPayment.ReIndexInvoices()
                '    End If
                'Next

                ' Control total for this splitted payment
                dControlSum = 0
                For Each oTmpPayment In Payments
                    If oTmpPayment.MATCH_MyField3 = "KIDsplit" Then  ' have a tmp-mark on involved payments
                        dControlSum = dControlSum + oTmpPayment.MON_InvoiceAmount
                        oTmpPayment.MATCH_MyField3 = ""
                    End If
                Next
                If dControlSum <> dPaymentAmount Then
                    Err.Raise(62098, , LRS(62098, dPaymentAmount.ToString))
                End If

            End If
            oTmpPayment = Nothing
            oPayment = Nothing
        Next ipaymentCounter

        oPayment = Nothing



    End Sub
    Private Function ReadDOF_Supplier() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "DOF Supplier"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDOF_Supplier = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadDOF_Salary() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "DOF Salary"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadDOF_Salary = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadLindorff_Incoming() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "Lindorff"

        Do While True

            If oCargo.Special = "FJORDKRAFT" Then
                ' read I or F records only
                If Left(sImportLineBuffer, 1) = "F" Then
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                    bFromLevelUnder = True

                    ReadLindorff_Incoming = sImportLineBuffer
                Else
                    ' advance one record
                    If Not oFile.AtEndOfStream Then
                        sImportLineBuffer = ReadGenericRecord(oFile)
                    Else
                        sImportLineBuffer = vbNullString
                    End If
                End If
            Else
                ' read I or F records only
                If Left(sImportLineBuffer, 1) = "I" Or Left(sImportLineBuffer, 1) = "F" Then
                    oPayment = oPayments.Add()
                    oPayment.objFile = oFile
                    oPayment.VB_Profile = oProfile
                    oPayment.ImportFormat = iImportFormat
                    oPayment.StatusCode = sStatusCode
                    sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                    bFromLevelUnder = True

                    ReadLindorff_Incoming = sImportLineBuffer
                Else
                    ' advance one record
                    If Not oFile.AtEndOfStream Then
                        sImportLineBuffer = ReadGenericRecord(oFile)
                    Else
                        sImportLineBuffer = vbNullString
                    End If
                End If
            End If

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadLindorff_Gebyr() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "Lindorff_Gebyr"

        Do While True

            ' read I or F records only
            If Left(sImportLineBuffer, 2) = "C;" Or Left(sImportLineBuffer, 2) = "I;" Or Left(sImportLineBuffer, 2) = "R;" Then

                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                ReadLindorff_Gebyr = sImportLineBuffer

            ElseIf Left(sImportLineBuffer, 1) = "E" Then

                Exit Do

            Else
                ' advance one record
                If Not oFile.AtEndOfStream Then
                    sImportLineBuffer = ReadGenericRecord(oFile)
                Else
                    sImportLineBuffer = vbNullString
                End If
            End If
            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount

    End Function
    Private Function ReadFlyWire_Incoming() As String
    ' FlyWire incoming payments, for Keystone Academic   
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "FlyWire"

        Do While True
            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True
            ReadFlyWire_Incoming = sImportLineBuffer

            If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then 'FIX:ED New by janp 28.06
                Exit Do
            End If
        Loop

        bx = AddInvoiceAmountOnBatch()
        nMON_TransferredAmount = nMON_InvoiceAmount
        
    End Function
    Private Function ReadSkyBridge_BACS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "SkyBridge_BACS"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadSkyBridge_BACS = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadOCBC_SG_Giro_Return() As String
        ' OCBC Singapore
        ' Giro and Fast RETURN format

        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left$(sImportLineBuffer, 1)

                Case "0"    ' Header
                    dDATE_Production = StringToDate(Mid(sImportLineBuffer, 8, 8))  'YYYYMMDD
                    oCargo.PayType = Mid(sImportLineBuffer, 2, 2) '2-3 Type code Payment(10), Collection(20), Payroll(30), Management Payroll (40)
                    bReadLine = False
                    bCreateObject = False
                    Exit Do

                Case "1"        ' Payment details
                    bReadLine = False
                    bCreateObject = True

                Case "8"        ' Credit trailer record (NOT IMPLEMENTED)
                    bReadLine = False
                    bCreateObject = False
                    bFromLevelUnder = False
                    Err.Raise(2836, , "OCBC Creditfiles are not implemented & sImportLineBuffer)")

                Case "9"        ' Debit trailer record
                    nMON_InvoiceAmount = Val(RemoveLeadingCharacters(Mid$(sImportLineBuffer, 39, 11), "0")) ' ' 39-49 Total Amount of Returned DRTransaction
                    nMON_TransferredAmount = nMON_InvoiceAmount
                    bAmountSetInvoice = True
                    bAmountSetTransferred = True
                    nNo_Of_Transactions = Val(Mid(sImportLineBuffer, 34, 5)) ' 34-38  Total Number of Returned DR Transactions
                   
                    bReadLine = False
                    bCreateObject = False
                    bFromLevelUnder = True
                    Exit Do

                Case Else
                    'All other record-types gives error
                    'Error # 10010-006
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-006")

                    Exit Do

            End Select

            If oFile.AtEndOfStream Or EmptyString(sImportLineBuffer) Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = Trim(ReadGenericRecord(oFile))
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.StatusCode = oCargo.StatusCode
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If
        Loop

        AddInvoiceAmountOnBatch()

        ReadOCBC_SG_Giro_Return = sImportLineBuffer
    End Function
    Private Function ReadMusto_BACS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "Musto_BACS"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadMusto_BACS = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadConfide_Excel() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim bOutOfLoop As Boolean
        Dim bCreatePayment As Boolean
        Dim sTmp As String
        Dim lNoOfColumns As Double
        Dim i As Integer

        Try

            bFromLevelUnder = False

            sFormatType = "Confide_Excel"

            Do While True

                oPayment = oPayments.Add()
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.objFile = oFile
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If sImportLineBuffer.Length = 0 Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If

            Loop

            bx = AddInvoiceAmountOnBatch()
            bx = AddTransferredAmountOnBatch()

            ReadConfide_Excel = sImportLineBuffer

        Catch ex As Exception
            Throw New Exception("(BB)Source: ReadConfide_Excel" & vbCrLf & ex.Message)

        End Try

    End Function
    Private Function ReadVipps_Nets() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean
        Dim bOutOfLoop As Boolean
        Dim bCreatePayment As Boolean
        Dim sTmp As String
        Dim lNoOfColumns As Double
        Dim i As Integer

        Try

            bFromLevelUnder = False

            sFormatType = "Vipps_Nets"

            Do While True

                oPayment = oPayments.Add()
                oPayment.objXLSheet = oXLWSheet
                oPayment.VB_Profile = oProfile
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.objFile = oFile
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True

                If sImportLineBuffer.Length = 0 Or sImportLineBuffer = "ERROR" Then
                    Exit Do
                End If

            Loop

            bx = AddInvoiceAmountOnBatch()
            bx = AddTransferredAmountOnBatch()

            ReadVipps_Nets = sImportLineBuffer

        Catch ex As Exception
            Throw New Exception("(BB)Source: ReadVipps_Nets" & vbCrLf & ex.Message)

        End Try

    End Function
    Private Function ReadFactolink() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean, bx As Boolean

        bFromLevelUnder = False
        sFormatType = "Factolink"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objXLSheet = oXLWSheet
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadFactolink = sImportLineBuffer

            If sImportLineBuffer = vbNullString Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadVideotel_BACS() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder, bx As Boolean

        bFromLevelUnder = False

        sFormatType = "Videotel_BACS"

        Do While True

            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile
            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            ReadVideotel_BACS = sImportLineBuffer

            If sImportLineBuffer = "" Or sImportLineBuffer = "ERROR" Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

    End Function
    Private Function ReadKTL() As String
        ' Finnish Incoming payments
        '------------------------------------

        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left(sImportLineBuffer, 1)
                '0 - Start of batch, have to store information
                Case "0" ' Batch record
                    If bFromLevelUnder Then
                        ' have read a 0-record in batch (new batch)
                        ' jump up, to create object
                        Exit Do
                    Else
                        'Extract data from sImportLineBuffer given from function Import
                        dDATE_Production = DateSerial("20" & Mid$(sImportLineBuffer, 2, 2), Mid$(sImportLineBuffer, 4, 2), Mid$(sImportLineBuffer, 6, 2))
                        sI_EnterpriseNo = Mid$(sImportLineBuffer, 14, 9) ' Datamottaker
                        sFormatType = "KTL"

                        bReadLine = True
                        bCreateObject = True

                    End If

                Case "3", "5"  'Transaction record
                    bReadLine = False
                    bCreateObject = True


                    '88 - End of Batch
                Case "9"

                    ' sum-up record
                    nNo_Of_Records = Val(Mid$(sImportLineBuffer, 2, 6)) ' Antall records i filen
                    nMON_InvoiceAmount = Val(RemoveLeadingCharacters(Mid$(sImportLineBuffer, 8, 11), "0")) ' Sum amount in batch, NB! in EUR
                    ' 06.07.2021
                    ' Consider the cancellation records when creating totals:
                    nNo_Of_Records = nNo_Of_Records + Val(Mid$(sImportLineBuffer, 19, 6)) ' Antall cancellation records i filen
                    nMON_InvoiceAmount = nMON_InvoiceAmount + Val(RemoveLeadingCharacters(Mid$(sImportLineBuffer, 25, 11), "0")) ' Sum amount of cancellation records in batch, NB! in EUR

                    nMON_TransferredAmount = nMON_InvoiceAmount

                    bAmountSetInvoice = True
                    bAmountSetTransferred = True

                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True
                    Exit Do


                Case Else
                    ' 11.03.2021
                    ' We have seen KTL files with blank lines at the end
                    If Len(Replace(sImportLineBuffer, " ", "")) > 5 Then
                        'All other record-types gives error
                        'Error # 10010-010
                        Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")
                    Else
                        sImportLineBuffer = sImportLineBuffer
                        ' OK, at end, read all empty lines
                        Do While oFile.AtEndOfStream = False
                            sImportLineBuffer = ReadKTLRecord(oFile)
                        Loop
                        Exit Do
                    End If

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadKTLRecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = CObj(sI_Account)
                oPayment.I_Name = sI_EnterpriseNo ' Kreditornummer
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        If oPayments.Count > 0 Then
            ' set en fake dato for balance
            dDATE_BalanceIN = StringToDate(oPayments(1).DATE_Payment)
            'dDATE_BalanceOUT = StringToDate(oPayments(oPayments.Count).DATE_Payment)
            ' Her kan det v�re tvil om hvilken dato vi skal sette p� utg.balanse.
            ' For LOwell OY ser vi at det er en dags innhold i en fil, og da setter vi samme dato for inn/ut 
            dDATE_BalanceOUT = StringToDate(oPayments(1).DATE_Payment)
        End If
        sREF_Bank = "NOREF"

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadKTL = sImportLineBuffer
    End Function
    Private Function ReadTITO() As String
        ' Finnish Statement file
        '------------------------------------

        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bReadLine, bFromLevelUnder, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        Dim sI_Account As String = ""
        Dim bWeHavePaymentsInBatch As Boolean = False

        bFromLevelUnder = False

        Do While True
            bReadLine = False

            Select Case Left(sImportLineBuffer, 3)
                '0 - Start of batch, have to store information
                Case "T00" ' Batch record
                    If bFromLevelUnder Then
                        ' have read a 0-record in batch (new batch)
                        ' jump up, to create object
                        Exit Do
                    Else
                        'Extract data from sImportLineBuffer given from function Import
                        sI_Account = Mid(sImportLineBuffer, 10, 14) '5 Account no. X(14) Accountnumber for the statement.
                        oCargo.I_Account = sI_Account
                        ' 21.04.2021 - to "connect" incoming and outgoing balances, we need to know the first batch-index with IB
                        oCargo.Temp = Index.ToString

                        dDATE_BalanceIN = StringToDate("20" & Mid(sImportLineBuffer, 27, 6))  '7 Statementperiod from 9(6) YYMMDD
                        dDATE_BalanceOUT = StringToDate("20" & Mid(sImportLineBuffer, 33, 6))  '8 Statementperiod to 9(6) YYMMDD
                        dDATE_Production = DateSerial("20" & Mid$(sImportLineBuffer, 39, 2), Mid$(sImportLineBuffer, 41, 2), Mid$(sImportLineBuffer, 43, 2))
                        sI_EnterpriseNo = Mid$(sImportLineBuffer, 49, 17).Trim ' Datamottaker
                        nMON_BalanceIN = CDbl(Mid(sImportLineBuffer, 73, 18))  '12 Opening balance
                        If Mid(sImportLineBuffer, 72, 1) = "-" Then
                            nMON_BalanceIN = nMON_BalanceIN * -1
                        End If
                        nNo_Of_Records = Val(Mid$(sImportLineBuffer, 91, 6)) '13 Number of records 9(6) Total number of records.
                        oCargo.MONCurrency = Mid(sImportLineBuffer, 97, 3) '14 Currency code X(3) The accounts ISO-currency code.
                        oCargo.I_Name = ConvertCharactersForNordeaeGateway_FI(Mid(sImportLineBuffer, 148, 35))  ' 05.05.2021 added ConvertCharactersForNordeaeGateway_FI(
                        sFormatType = "TITO"

                        bWeHavePaymentsInBatch = False ' 22.02.2021
                        bReadLine = True
                        'bCreateObject = True
                        bCreateObject = False  ' 22.02.2021

                    End If

                Case "T10", "T11"  'Transaction record
                    bWeHavePaymentsInBatch = True  ' 22.20.2021
                    bReadLine = False
                    bCreateObject = True
                    ' 19.01.2021
                    ' Level-indicator in pos 188
                    ' 18 188-188    Level(ID)       AN() 1:  0 or blanc = basic transaction  1-9 = specification level transaction
                    If Mid(sImportLineBuffer, 188, 1) = "0" Then
                        If bFromLevelUnder Then
                            ' create a new batch
                            bCreateObject = False
                            Exit Do
                        Else
                        End If
                    End If

                Case "T40"

                    'Extract data from sImportLineBuffer given from function Import
                    dDATE_BalanceOUT = StringToDate("20" & Mid(sImportLineBuffer, 7, 6))  '8 Statementperiod to 9(6) YYMMDD

                    ' 21.04.2021 - to "connect" incoming and outgoing balances, we need to know the first batch-index with IB
                    oCargo.LastPayment_ID = Index

                    nMON_BalanceOUT = CDbl(Mid(sImportLineBuffer, 14, 18))  '12 Closing balance
                    If Mid(sImportLineBuffer, 13, 1) = "-" Then
                        'nMON_BalanceIN = nMON_BalanceIN * -1
                        ' 21.04.2021  ERROR - changed from In to Out
                        nMON_BalanceOUT = nMON_BalanceOUT * -1
                    End If
                    sFormatType = "TITO"

                    nMON_BalanceOUTInterest = CDbl(Mid(sImportLineBuffer, 33, 18))  '6 Closing available balance
                    If Mid(sImportLineBuffer, 32, 1) = "-" Then
                        nMON_BalanceOUTInterest = nMON_BalanceOUTInterest * -1
                    End If

                    bAmountSetInvoice = True
                    bAmountSetTransferred = True

                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True

                    ' 22.02.2021
                    ' We can have batches with no payments, we skip those, and do not create a new batch
                    If bWeHavePaymentsInBatch Then
                        bFromLevelUnder = False
                    End If

                Case "T50"
                    ' Cumulative base record (1 per statement/date)
                    ' No info to use from this one ???
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True
                    Exit Do

                Case "T51"
                    ' T51 Cumulative correction record
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True

                Case "T60"
                    ' Special record
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True

                Case "T70"
                    ' undocumented end record
                    bReadLine = True   'False  22.02.2021
                    bCreateObject = False
                    bFromLevelUnder = True
                    'Exit Do   ' 22.02.2021 - do not jump out, there may be more
                    If bWeHavePaymentsInBatch Then
                        Exit Do
                    End If

                Case "T80"
                    '  Notifying transaction record
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True

                Case "T81"
                    bReadLine = True
                    bCreateObject = False
                    bFromLevelUnder = True


                Case Else
                    'All other record-types gives error
                    'Error # 10010-010
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-010")

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadTITORecord(oFile)
            End If

            If bCreateObject Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = oCargo.I_Account
                oPayment.I_Name = oCargo.I_Name
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                sImportLineBuffer = oPayment.Import(sImportLineBuffer)
                bFromLevelUnder = True
            End If

        Loop

        ' 26.04.2021
        ' Lowell have files with headerrecord only, no transactions.
        ' Must cheat by adding an "empty" oPayment.
        If oCargo.Special = "LOWELL_OY" Then
            If oPayments.Count = 0 Then
                oPayment = oPayments.Add()
                oPayment.objFile = oFile
                oPayment.VB_Profile = oProfile
                oPayment.I_Account = oCargo.I_Account
                oPayment.I_Name = oCargo.I_Name
                oPayment.ImportFormat = iImportFormat
                oPayment.StatusCode = sStatusCode
                oPayment.MON_InvoiceCurrency = "EUR"
                oPayment.MON_TransferCurrency = "EUR"
                oPayment.REF_Bank1 = "NOT PROVIDED"
                oPayment.REF_EndToEnd = "NOT PROVIDED"
                oPayment.PayType = "D"
                oPayment.PayCode = "150"
                oPayment.DATE_Payment = DateToString(dDATE_BalanceIN)
                oPayment.DATE_Value = DateToString(dDATE_BalanceIN)
                oPayment.VB_FilenameOut_ID = CheckAccountNo(oPayment.I_Account, "", oProfile)

                oInvoice = oPayment.Invoices.Add()
                oInvoice.REF_Bank = "NOT PROVIDED"

                ' fake outgoing balance same as incoming
                nMON_BalanceOUT = nMON_BalanceIN
                nMON_BalanceOUTInterest = nMON_BalanceIN

                bFromLevelUnder = True

            End If
        End If

        AddInvoiceAmountOnBatch()
        AddTransferredAmountOnBatch()

        ReadTITO = sImportLineBuffer
    End Function
    Private Function ReadTrident_Salary() As String
        Dim oPayment As Payment
        Dim bFromLevelUnder As Boolean
        Dim bx As Boolean

        bFromLevelUnder = False

        sFormatType = "Trident_Salary"


        Do While True


            oPayment = oPayments.Add()
            oPayment.objFile = oFile
            oPayment.VB_Profile = oProfile

            oPayment.ImportFormat = iImportFormat
            oPayment.StatusCode = sStatusCode
            sImportLineBuffer = oPayment.Import(sImportLineBuffer)
            bFromLevelUnder = True

            If Len(sImportLineBuffer) = 0 Then
                Exit Do
            End If

        Loop

        bx = AddInvoiceAmountOnBatch()

        ReadTrident_Salary = sImportLineBuffer

    End Function

End Class
