Option Strict Off
Option Explicit On
Module WriteFactoring
    Function WriteSGFinans_Fakturafil(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sClientNo As String) As Boolean
        ' For SG Finans
        ' Creates a fixed length file with Factoringrecords (Fakturafil)
        '---------------------------------------------------------------
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim lCounter As Long, i As Long
        Dim sTmp As String
        Dim sTmpDate As String
        ' XNET 25.01.2012
        Dim sTmpFileName As String
        Dim iFileNumber As Integer
        Dim bEmptyfile As Boolean
        bEmptyfile = True
        iFileNumber = 0

        Try

            lCounter = 0

            ' create an outputfile
            ' XNET 25.01.2012 - moved
            'Set oFile = oFs.OpenTextFile(sFilenameOut, ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                ' XNET 25.01.2012
                ' If more than one input file, SG will need as many exportfiles
                ' as there are importfiles.
                ' Then we must create new exportfiles if this is not the first oBabelFile
                'If oBabel.Index > 1 Then

                ' If files already present, do NOT overwrite, but add to higher number
                Do
                    ' find filename, use a runningnumber
                    sTmpFileName = sFilenameOut
                    sTmpFileName = Left$(sFilenameOut, InStr(sFilenameOut, ".") - 1)  'upto .

                    ' add runningnumber
                    iFileNumber = iFileNumber + 1
                    sTmpFileName = sTmpFileName & LTrim(Str(iFileNumber)) & Mid$(sFilenameOut, InStr(sFilenameOut, "."))
                    If Not oFs.FileExists(sTmpFileName) Then
                        Exit Do
                    End If
                    If iFileNumber > 500 Then
                        ' never mind, something rotten, carry on to avoid looping forever
                        Exit Do
                    End If
                Loop
                'Else
                '    sTmpFileName = sFilenameOut
                'End If

                oFile = oFs.OpenTextFile(sTmpFileName, Scripting.IOMode.ForAppending, True, 0)
                bEmptyfile = True

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.VB_ClientNo = sClientNo Then
                                    bExportoBabel = True
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBatch = True
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoPayment = True
                                        End If
                                    End If
                                End If

                                If bExportoPayment And Not oPayment.OmitExport Then
                                    ' Export Fakturafil-records only
                                    If oPayment.PayCode > 700 And oPayment.PayCode < 710 And Not oPayment.Exported Then

                                        For Each oInvoice In oPayment.Invoices
                                            lCounter = lCounter + 1
                                            sLine = ""
                                            'XNET 25.01.2012
                                            bEmptyfile = False
                                            'F1 Transaksjonskode
                                            sLine = sLine & bbGetPayCode(oPayment.PayCode, "SGFINANS_FAKTURA")

                                            'F2  Klientnr
                                            ' Changed 07.02.2008
                                            ' If imported in a normal run, Clients are named F\0009999_001, due to folderdemands by HPD
                                            ' Also make sure that it can be run in other setups, when clientno is like 0009999 or 9999
                                            If Len(oPayment.VB_ClientNo) = 4 Then
                                                sLine = sLine & oPayment.VB_ClientNo
                                            ElseIf Len(oPayment.VB_ClientNo) = 7 Then
                                                sLine = sLine & Right$(oPayment.VB_ClientNo, 4)
                                            Else
                                                sLine = sLine & Mid$(oPayment.VB_ClientNo, 6, 4)  ' F\0003432_001 blir 3432
                                            End If

                                            'F3  Kundenr (debitors kundenr)
                                            sLine = sLine & PadLeft(oInvoice.CustomerNo, 9, "0")

                                            'F4  Fakturanr / Kreditnotanr
                                            sLine = sLine & PadLeft(oInvoice.InvoiceNo, 8, "0")

                                            'F5  Fakturadato
                                            sLine = sLine & Mid$(oPayment.DATE_Payment, 3) 'YYMMDD

                                            'F6  Forfallsdato
                                            sLine = sLine & Mid$(oPayment.DATE_Value, 3) 'YYMMDD

                                            'F7  Bel�p
                                            'sLine = sLine & PadLeft(Abs(oPayment.MON_InvoiceAmount), 11, "0") ' i �re
                                            ' 02.02.2008 - changed to reflect new principle in Aquarius
                                            ' Prioritize amount from field F14 Valutabel�p if other than from field 7
                                            ''If oPayment.MON_TransferredAmount <> 0 Then
                                            '    sLine = sLine & PadLeft(Abs(oPayment.MON_TransferredAmount), 11, "0") ' i �re
                                            'Else

                                            ' 05.02.2008 IKKE FLYTT BEL�P !!!
                                            sLine = sLine & PadLeft(Math.Abs(oPayment.MON_InvoiceAmount), 11, "0") ' i �re
                                            'End If


                                            'F8  Rabattbetingelse / Ref til faktura
                                            If oInvoice.MON_InvoiceAmount > 0 Then
                                                sLine = sLine & PadLeft(oInvoice.REF_Own, 5, "0")
                                            Else
                                                ' Kreditnota
                                                sLine = sLine & PadRight(oInvoice.REF_Own, 8, "0")  'Ref til faktura
                                            End If

                                            'F9  Ikke i bruk
                                            If oInvoice.MON_InvoiceAmount > 0 Then
                                                sLine = sLine & Space(8)
                                            Else
                                                sLine = sLine & Space(5)
                                            End If

                                            'F10  Fritekst
                                            If oInvoice.Freetexts.Count > 0 Then
                                                sLine = sLine & PadRight(oInvoice.Freetexts(1).Text, 40, " ")
                                            Else
                                                sLine = sLine & Space(40)
                                            End If

                                            'F11 Selger, SellersNumber
                                            sLine = sLine & PadRight(oPayment.ExtraD1, 4, " ")

                                            'F12 Prosjekt, ProjectNumber
                                            sLine = sLine & PadRight(oPayment.ExtraD2, 7, " ")

                                            'F13 Valutakode
                                            sLine = sLine & PadRight(oPayment.MON_TransferCurrency, 3, " ")

                                            'F14 Valutabel�p
                                            sLine = sLine & PadLeft(Math.Abs(oPayment.MON_TransferredAmount), 11, "0") ' i �re

                                            'F15 ikke i bruk
                                            sLine = sLine & Space(5)

                                            oFile.WriteLine(sLine) '& vbCrLf
                                        Next oInvoice
                                        oPayment.Exported = True
                                    End If
                                End If

                            Next ' oPayment
                        End If
                    Next 'batch
                End If
                ' XNET 25.01.2012 - close here !!
                oFile.Close()
                ' XNET 25.01.2012
                ' If more than one input file, SG will need as many exportfiles
                ' Make a backupcopy of it
                If bEmptyfile Then
                    ' no records in file, remove!
                    If oFs.FileExists(sTmpFileName) Then
                        oFs.DeleteFile(sTmpFileName, True)
                    End If
                    iFileNumber = iFileNumber - 1 ' not to have "holes" in filenumber sequence
                Else
                    'If oBabel.Index > 1 Then
                    If oBabel.VB_Profile.FileSetups(iFormat_ID).BackupOut Then
                        sTmp = Trim$(oBabel.VB_Profile.FileSetups(iFormat_ID).BackupPath)
                        If EmptyString(sTmp) Then
                            ' find backup from company
                            sTmp = oBabel.VB_Profile.BackupPath
                        End If
                        ' copy file
                        If oFs.FileExists(sTmpFileName) Then
                            oFs.CopyFile(sTmpFileName, sTmp & "\" & VB6.Format(Date.Today, "YYYYMMDD") & "_" & VB6.Format(Now, "hhmmss") & "_" & oFs.GetFileName(sTmpFileName))
                        End If
                    End If
                    'End If
                End If
            Next

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteSGFinans_Fakturafil" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSGFinans_Fakturafil = True

    End Function
    Function WriteSGFinans_Kundefil(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sClientNo As String, ByVal sSpecial As String) As Boolean
        ' For SG Finans
        ' Creates a fixed length file with kundedata, (this file accompanies the Fakturafil
        '----------------------------------------------------------------------------------
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim lCounter As Long, i As Long
        Dim sTmp As String
        Dim sTmpDate As String
        Dim aExportedCustomers() As String
        ' XNET 25.01.2012
        Dim sTmpFileName As String
        Dim iFileNumber As Integer
        Dim bEmptyfile As Boolean
        bEmptyfile = True
        iFileNumber = 0

        Try

            lCounter = 0
            ' create an outputfile
            ' XNET 25.01.2012, moved down
            'Set oFile = oFs.OpenTextFile(sFilenameOut, ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                ' XNET 25.01.2012
                ' If more than one input file, SG will need as many exportfiles
                ' as there are importfiles.
                ' Then we must create new exportfiles if this is not the first oBabelFile
                'If oBabel.Index > 1 Then
                Do
                    ' find filename, use a runningnumber
                    sTmpFileName = sFilenameOut
                    sTmpFileName = Left$(sFilenameOut, InStr(sFilenameOut, ".") - 1)  'upto .

                    ' add runningnumber
                    ' If files already present, do NOT overwrite, but add to higher number
                    ' add runningnumber
                    iFileNumber = iFileNumber + 1
                    sTmpFileName = sTmpFileName & LTrim(Str(iFileNumber)) & Mid$(sFilenameOut, InStr(sFilenameOut, "."))
                    If Not oFs.FileExists(sTmpFileName) Then
                        Exit Do
                    End If
                    If iFileNumber > 500 Then
                        ' never mind, something rotten, carry on to avoid looping forever
                        Exit Do
                    End If
                Loop
                'Else
                '    sTmpFileName = sFilenameOut
                'End If

                oFile = oFs.OpenTextFile(sTmpFileName, Scripting.IOMode.ForAppending, True, 0)
                bEmptyfile = True

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.VB_ClientNo = sClientNo Then
                                    bExportoBabel = True
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBatch = True
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoPayment = True
                                        End If
                                    End If
                                End If

                                If bExportoPayment And Not oPayment.OmitExport Then

                                    ' Only one record pr customer
                                    ' Check to see if this customer has been exported before;
                                    If Not Array_IsEmpty(aExportedCustomers) Then
                                        For i = 0 To UBound(aExportedCustomers) - 1
                                            If oPayment.Invoices(1).CustomerNo = aExportedCustomers(i) Then
                                                ' is this necessary????              oPayment.Exported = True
                                                Exit For
                                            End If
                                        Next i
                                    End If

                                    ' Export Fakturafil-records only
                                    If oPayment.PayCode = 710 And Not oPayment.Exported Then
                                        'XNET 25.01.2012
                                        bEmptyfile = False

                                        'For Each oInvoice In oPayment.Invoices
                                        lCounter = lCounter + 1
                                        sLine = ""

                                        'K1 Transaksjonskode
                                        sLine = sLine & "K"

                                        'K2 Versjonsnr
                                        sLine = sLine & "9409"

                                        'K3  Klientnr
                                        'sLine = sLine & Mid$(oPayment.VB_ClientNo, 6, 4)  ' F\0003432_001 blir 3432
                                        ' Changed 07.02.2008
                                        ' If imported in a normal run, Clients are named F\0009999_001, due to folderdemands by HPD
                                        ' Also make sure that it can be run in other setups, when clientno is like 0009999 or 9999
                                        If Len(oPayment.VB_ClientNo) = 4 Then
                                            sLine = sLine & oPayment.VB_ClientNo
                                        ElseIf Len(oPayment.VB_ClientNo) = 7 Then
                                            sLine = sLine & Right$(oPayment.VB_ClientNo, 4)
                                        Else
                                            sLine = sLine & Mid$(oPayment.VB_ClientNo, 6, 4)  ' F\0003432_001 blir 3432
                                        End If


                                        'K4  Kundenr (debitors kundenr)
                                        sLine = sLine & PadLeft(oPayment.Invoices(1).CustomerNo, 9, "0")

                                        'K5 Org.nr
                                        sLine = sLine & PadLeft(oPayment.e_OrgNo, 11, "0")

                                        'K6 Etternavn/Firmanavn
                                        sLine = sLine & PadRight(oPayment.E_Name, 35, " ") 'YYMMDD

                                        'K7 Fornavn, brukes ikke her
                                        sLine = sLine & Space(20)

                                        'K8  Adr1
                                        sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ")

                                        'K9  Postnr
                                        sLine = sLine & PadLeft(oPayment.E_Zip, 4, "0")

                                        'K10  Poststed
                                        sLine = sLine & PadRight(oPayment.E_City, 23, " ")

                                        'K11 Attention
                                        sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")

                                        'K12 Telefon
                                        sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 12, " ")

                                        'K13 Land
                                        If Not EmptyString(oPayment.E_Adr2) Then
                                            sLine = sLine & PadRight(oPayment.E_Adr2, 20, " ")
                                        Else
                                            sLine = sLine & PadRight(oPayment.E_CountryCode, 20, " ")
                                            ' endre: lag oppslag mot v�r landtabell  FindCountryname(oPayment.E_Countrycode)
                                        End If

                                        'K14 Landkode
                                        sLine = sLine & PadRight(oPayment.E_CountryCode, 2, " ")

                                        'K15 Landkode numerisk, benyttes fra 06.10.2008
                                        ' 27.11.2009
                                        ' M� bruke padleft!! F.ex. Austria, 40, became 400 (Jordan!)
                                        'sLine = sLine & PadRight(oPayment.I_CountryCode, 3, "0")
                                        ' Changed 12.07.2010
                                        ' If countrycode = 000, then export as blank
                                        ' Ref mail from Terje Embla/Morten Nissen-Lie 12.07.2010
                                        If EmptyString(oPayment.I_CountryCode) Or oPayment.I_CountryCode = "000" Then
                                            sLine = sLine & Space(3)
                                        Else
                                            sLine = sLine & PadLeft(oPayment.I_CountryCode, 3, "0")
                                        End If
                                        'K16 Bankgironr
                                        sLine = sLine & PadLeft(oPayment.E_Account, 11, " ")

                                        'K17, Distrikt/selger,
                                        sLine = sLine & PadRight(oPayment.ExtraD3, 4, " ")

                                        'K18 Navn2, brukes ikke her
                                        sLine = sLine & Space(35)

                                        If sSpecial <> "NEWPHONE" Then
                                            ' Added K19 Valutakode, to be used by Aquarius
                                            sLine = sLine & oPayment.MON_InvoiceCurrency
                                        End If

                                        oFile.WriteLine(sLine) '& vbCrLf
                                        'Next oInvoice
                                        oPayment.Exported = True
                                        ' Add to array
                                        If Array_IsEmpty(aExportedCustomers) Then
                                            ReDim aExportedCustomers(0)
                                            aExportedCustomers(0) = oPayment.Invoices(1).CustomerNo
                                        Else
                                            ReDim Preserve aExportedCustomers(UBound(aExportedCustomers) + 1)
                                        End If

                                    End If
                                End If

                            Next ' oPayment
                        End If
                    Next 'batch
                End If

                ' XNET 25.01.2012 - close here !!
                oFile.Close()
                ' XNET 25.01.2012
                ' If more than one input file, SG will need as many exportfiles
                ' Make a backupcopy of it
                If bEmptyfile Then
                    ' no records in file, remove!
                    If oFs.FileExists(sTmpFileName) Then
                        oFs.DeleteFile(sTmpFileName, True)
                    End If
                    iFileNumber = iFileNumber - 1 ' not to have "holes" in filenumber sequence
                Else
                    'If oBabel.Index > 1 Then
                    If oBabel.VB_Profile.FileSetups(iFormat_ID).BackupOut Then
                        sTmp = Trim$(oBabel.VB_Profile.FileSetups(iFormat_ID).BackupPath)
                        If EmptyString(sTmp) Then
                            ' find backup from company
                            sTmp = oBabel.VB_Profile.BackupPath
                        End If
                        ' copy file
                        If oFs.FileExists(sTmpFileName) Then
                            oFs.CopyFile(sTmpFileName, sTmp & "\" & VB6.Format(Date.Today, "YYYYMMDD") & "_" & VB6.Format(Now, "hhmmss") & "_" & oFs.GetFileName(sTmpFileName))
                        End If
                    End If
                    'End If
                End If

            Next


        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteSGFinans_Kundefil" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSGFinans_Kundefil = True

    End Function

End Module
