Option Strict Off
Option Explicit On
Module WriteOCR
	
	'Dim oFs As Object
	'Dim oFile As TextStream
	Dim sLine As String ' en output-linje
	Dim i As Double
	Dim oBatch As Batch
	' Vi m� dimme noen variable for
	' - batchniv�: sum bel�p, antall records, antall transer, f�rste dato, siste dato
	Dim nBatchNoRecords, nBatchSumAmount, nBatchNoTransactions As Double
	Dim dBatchLastDate, dBatchFirstDate As Date
	' - fileniv�: sum bel�p, antall records, antall transer, siste dato
	Dim nFileNoRecords, nFileSumAmount, nFileNoTransactions As Double
	'Dim dFileFirstDate As Date
	
    Function WriteOCRFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Integer, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String, Optional ByRef bExportGLTransactions As Boolean = True) As Boolean
        'Function WriteOCRFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String, Optional ByRef bExportGLTransactions As Boolean = True) As Object
        'bFalseOCR, False = not an original OCR-file, i.e. CREMUL
        'bOCRTransactions, False = not original OCR-transaction. i.e. transaction from CREMUL which is not of type 233.

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        'Dim op As Payment
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim bKeepOppdragsNr, bFoundContractNo As Boolean
        Dim sContractNo As String
        Dim sGroupNumber As String 'Used to create a false, Sentral-ID + Dagkode +
        ' Delavregningsnr + l�penummer (for grouping on the bankstatement).
        Dim bFBOsExist, bBatchStartWritten As Boolean
        Dim nFBOTransactionNo As Double
        Dim sFBOFileName As String
        Dim iStartPosFilename As Short
        Dim bLastBatchType As String
        Dim sInAccount As String
        Dim bContinue As Boolean
        Dim oPayment2 As vbBabel.Payment
        Dim eBank As vbBabel.BabelFiles.Bank
        Dim bCheckIfInvoiceIsExported As Boolean
        Dim bUseMatch_ID As Boolean

        'Miroslav Klose
        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim bUseDayTotals As Boolean
        Dim lCounter, lArrayCounter As Integer
        '29.09.2010 - Added the use of bExportPaymentAmountForUnmatchedStructured and bContinue2 for BKK
        Dim bExportPaymentAmountForUnmatchedStructured As Boolean
        Dim bContinue2 As Boolean
        'XNET - 04.01.2011 - Added next 2 variables
        Dim bCheckClientNoAgainstInvoice As Boolean
        Dim bAllInvoicesExported As Boolean
        ' XNET 16.05.2012
        Dim oBatch2 As Batch
        Dim bExit_HBOCR As Boolean

        Try

            ' new 19.12.02, due to problems running to OCR-export connescutive (without leaving BB.EXE)
            nBatchSumAmount = 0
            nBatchNoRecords = 0
            nBatchNoTransactions = 0
            dBatchLastDate = CDate(Nothing)
            dBatchFirstDate = CDate(Nothing)
            ' ----------------------------------nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0
            'XNET - 04.01.2011 - Added next default value
            bCheckClientNoAgainstInvoice = False

            oFs = New Scripting.FileSystemObject

            'Added 02.09.2010
            If sSpecial = "CONECTO_AF" Then
                'XNET - 04.01.2011 - Added next for eaches
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_ClientNo = "21" Then
                                bCheckClientNoAgainstInvoice = True
                            End If
                            Exit For
                        Next oPayment
                        Exit For
                    Next oBatch
                    Exit For
                Next oBabel
                'XNET - 17.02.2011 - Added next for eaches
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    If EmptyString(oInvoice.MATCH_ClientNo) Then
                                        oInvoice.MATCH_ClientNo = "21"
                                    End If
                                End If
                            Next oInvoice
                        Next oPayment
                    Next oBatch
                Next oBabel

                bUseMatch_ID = True
            Else
                bUseMatch_ID = False
            End If

            'XNET - 11.11.2010 - removed And Not RunTime from the IF beneath
            If sSpecial = "BKK" Then 'And Not RunTime Then
                bExportPaymentAmountForUnmatchedStructured = True
                'ElseIf Not RunTime Then 'NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNB
                '    bExportPaymentAmountForUnmatchedStructured = True
            Else
                bExportPaymentAmountForUnmatchedStructured = False
            End If

            '----------
            bKeepOppdragsNr = True
            ' lag en outputfil

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            sContractNo = ""
            sGroupNumber = ""
            bFBOsExist = False
            '19.06.2009 - New IF
            'XNET - 06.12.2010 - Added Conecto_AF
            'XNET - 09.02.2012 - Added AKTIV_KAPITAL
            If sSpecial = "SANTANDER" Or sSpecial = "CONECTO_AF" Or sSpecial = "SGFINANS_LEASING" Or sSpecial = "AKTIV_KAPITAL" Or sSpecial = "LOWELL_NO" Then  'XNET - 24.06.2011
                bCheckIfInvoiceIsExported = True
            Else
                bCheckIfInvoiceIsExported = False
            End If

            '***********************************************************
            'New code 15.06.2010 - To use DayTotals
            'All code regarding this change in this function is marked with Der Torsch�tze Miroslav Klose
            bUseDayTotals = False

            '07.10.2015 - Implemented the new way to populate DayTotals
            If oBabelFiles.Count > 0 Then
                If oBabelFiles(1).VB_ProfileInUse Then
                    If oBabelFiles(1).VB_Profile.ManMatchFromERP Then
                        bUseDayTotals = True
                    End If
                End If
            End If

            'Old code
            'If sSpecial = "AKTIV_KAPITAL" Or sSpecial = "UPDAYTOTALS" Or sSpecial = "CONECTO_HAMAR" Then
            '    bUseDayTotals = True
            'End If

            If bUseDayTotals Then
                'New code 21.12.2006
                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.Company_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim Preserve aAmountArray(UBound(aAccountArray, 2))
            End If
            '***********************************************************
            'End new code

            'New code 21.12.2006
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.Company_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'XNET - 01.03.2012 - removed next lines, created error when no accounts existed
            'aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

            'Create an array equal to the client-array
            'ReDim Preserve aAmountArray(UBound(aAccountArray, 2))

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord89 = False
                nExistingLines = 0
                'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 8) = "NY000089" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord89 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                If Not bFoundRecord89 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    MsgBox("The existing file," & sFilenameOut & ", is not an OCR-file")
                    'UPGRADE_WARNING: Couldn't resolve default property of object WriteOCRFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    WriteOCRFile = False
                    Exit Function
                    'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nFileNoTransactions = Val(Mid(sLine, 9, 8))
                    'Subtract 1 because we have deleted the 89-line
                    nFileNoRecords = Val(Mid(sLine, 17, 8)) - 1
                    nFileSumAmount = Val(Mid(sLine, 25, 17))
                    'Kill'em all
                    oFileRead.Close()
                    'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileRead = Nothing
                    oFileWrite.Close()
                    'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            ' XNET 16.05.2012
            If sSpecial = "HB_OCR" Then
                bExit_HBOCR = False
                ' for this case, we will export one file pr account
                ' the call to the export loops until all payments exported
                ' Find next account not exported
                sI_Account = ""
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.Exported = False Then
                                sI_Account = oPayment.I_Account
                                Exit For
                            End If
                        Next oPayment
                        If Len(sI_Account) > 0 Then
                            Exit For
                        End If
                    Next oBatch
                    If Len(sI_Account) > 0 Then
                        Exit For
                    End If
                Next oBabel

                If Len(sI_Account) = 0 Then
                    ' all payments exported !
                    WriteOCRFile = False
                    Exit Function
                End If



                oFile = oFs.OpenTextFile(Replace(UCase(sFilenameOut), "OCR.DAT", sI_Account & ".OCR"), Scripting.IOMode.ForAppending, True, 0)
            Else
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            End If

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments

                        'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                        If oBatch.FormatType <> "OCR_FBO" Then

                            '27.03.2009 - Changed for Norstat. If the original format is a format which may consist
                            ' of KID and non-KID payments in a mixture, like BgMax, we get a problem here because we only
                            ' test if the first payment IsOCR()
                            'If the export format is set to FalseOCR change the test to traverse through all payments
                            ' in the batch, and if one of them IsOCR() then continue.
                            If (bFalseOCR And bOCRTransactions) Then
                                bContinue = False
                                For Each oPayment2 In oBatch.Payments
                                    If IsOCR(oPayment2.PayCode) Then
                                        bContinue = True
                                        Exit For
                                    End If
                                Next oPayment2
                            Else
                                bContinue = False
                            End If

                            '05.09.2008 - Changed due to error from "510" to IsOCR
                            If Not bFalseOCR Or bContinue Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Or (IsAutogiro((oPayment.PayCode)) And oBabel.Cargo.Special <> "AUTOGIRO") Then

                                'OLD CODE
                                '                '05.09.2008 - Changed due to error from "510" to IsOCR
                                '                If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR(oPayment.PayCode)) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR(oPayment.PayCode)) Or _
                                ''                (IsAutogiro(oPayment.PayCode) And oBabel.Cargo.Special <> "AUTOGIRO") Then
                                'Old code
                                'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Or _
                                '(IsAutogiro(oPayment.PayCode) And oBabel.Cargo.Special <> "AUTOGIRO") Then
                                ' Added Autogirotest 17.11.06. If special AUTOGIRO, then Autogiro is exported in separate file

                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        'XNET - 04.01.2011 - Added next IF
                                        If bCheckClientNoAgainstInvoice Then
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                    If oInvoice.MATCH_ClientNo = sClientNo Then
                                                        bExportoBabel = True
                                                        Exit For
                                                    End If
                                                ElseIf Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                    'An open invoice will be exported to the default client stated as clientno in the BB setup
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoBabel = True
                                                        Exit For
                                                    End If
                                                End If
                                            Next oInvoice
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoBabel = True
                                                    End If
                                                Else
                                                    bExportoBabel = True
                                                End If
                                            End If
                                        End If
                                    End If
                                    '21.12.2009 - New for Haugaland, do not export GL-postings
                                    If bExportoBabel And Not bExportGLTransactions Then
                                        bExportoBabel = False
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final Then
                                                If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                    bExportoBabel = True
                                                End If
                                            End If
                                        Next oInvoice
                                    End If
                                End If
                            Else
                                ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                'If sSpecial = "NHST_INNBETALINGER" Then
                                'Else
                                Exit For
                                'End If

                            End If 'bFalseOCR
                            If bExportoBabel Then
                                Exit For
                            End If
                        Else
                            bFBOsExist = True
                            Exit For
                        End If ' FormatType
                        'Next oPayment


                        ' added 27.02.2015
                        ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                        'If sSpecial = "NHST_INNBETALINGER" Then

                        '    For Each oInvoice In oPayment.Invoices
                        '        If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" And oPayment.VB_ClientNo = sClientNo Then
                        '            bExportoBabel = True
                        '            '  must exit here, since we have at least one invoice to export
                        '            Exit For
                        '            'Else
                        '            '    bExportoBabel = False
                        '        End If
                        '    Next
                        'End If
                        ' 12.03.2015 Moved next line here, from 16 lines above, for NHST to check ALL payments in batch, not only last one
                    Next oPayment

                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        If sSpecial = "ERGOGROUP" Then
                            ' manipulate Forsendelsesnr, must be unique in Maconomy
                            ' use date(3) + min(2) + clientno (2)
                            oBabel.File_id = Right$(Right$(Format(Now(), "mmdd"), 3) & Format(Now(), "n") & oPayment.VB_ClientNo, 7)  ' forsendelsesnr
                        End If

                        ' XNET 03.04.2013 added sI_Account
                        '07.08.2019 - Changed from sI_Account to sClientNo.
                        ' This parameter is JUSt used by Vegfinans
                        ' sI_Account where "" for Vegfinans and therefore the exportfile wasn't valid.
                        ' Probably this is caused by some changes we did some months ago in the EXE-file, regarding creating clientfiles (using client instead om account as base.
                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, bFalseOCR, bOCRTransactions, sSpecial, sClientNo)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments

                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If oBatch.FormatType <> "OCR_FBO" Then


                                '27.03.2009 - Changed for Norstat. If the original format is a format which may consist
                                ' of KID and non-KID payments in a mixture, like BgMax, we get a problem here because we only
                                ' test if the first payment IsOCR()
                                'If the export format is set to FalseOCR change the test to traverse through all payments
                                ' in the batch, and if one of them IsOCR() then continue.
                                If (bFalseOCR And bOCRTransactions) Then
                                    bContinue = False
                                    For Each oPayment2 In oBatch.Payments
                                        If IsOCR(oPayment2.PayCode) Then
                                            bContinue = True
                                            Exit For
                                        End If
                                    Next oPayment2
                                Else
                                    bContinue = False
                                End If

                                ' XNET 16.05.2012
                                If sSpecial = "HB_OCR" Then
                                    ' for this case, we will export one file pr account
                                    ' the call to the export loops until all payments exported
                                    ' Must check if any payments for this client in next batch
                                    ' otherwise there will be empty batches
                                    bContinue = False

                                    For Each oBatch2 In oBabel.Batches
                                        For Each oPayment2 In oBatch2.Payments
                                            If Not oPayment2.Exported Then
                                                If oPayment2.I_Account = sI_Account Then
                                                    bContinue = True
                                                    Exit For
                                                End If
                                            End If
                                        Next oPayment2
                                        If bContinue Then
                                            Exit For
                                        End If
                                    Next oBatch2
                                    If Not bContinue Then
                                        ' Write fileEnd, and exit
                                        If nFileNoRecords > 0 Then
                                            sLine = Wr_OCRFileEnd(dProductionDate)
                                            oFile.WriteLine(sLine)
                                            oFile.Close()
                                            oFile = Nothing
                                            WriteOCRFile = True
                                            Exit Function
                                        End If

                                    End If
                                End If

                                ' added 27.02.2015
                                ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                'If bExportoBabel And sSpecial = "NHST_INNBETALINGER" Then
                                '    bExportoBatch = False
                                '    bContinue = False
                                '    For Each oInvoice In oPayment.Invoices
                                '        If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" And oPayment.VB_ClientNo = sClientNo Then
                                '            bExportoBatch = True
                                '            bContinue = True
                                '            ' 14.01.2015 - must exit here, since we have at least one invoice to export
                                '            Exit For
                                '        Else
                                '            bExportoBatch = False
                                '        End If
                                '    Next
                                'End If

                                '05.09.2008 - Changed due to error from "510" to IsOCR
                                If Not bFalseOCR Or bContinue Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then

                                    'OLD CODE
                                    '                    '05.09.2008 - Changed due to error from "510" to IsOCR
                                    '                    If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR(oPayment.PayCode)) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR(oPayment.PayCode)) Then
                                    'Old code
                                    'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                    'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' Changed by JanP 07.01.04
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            'XNET - 04.01.2011 - Added next IF
                                            If bCheckClientNoAgainstInvoice Then
                                                For Each oInvoice In oPayment.Invoices
                                                    If oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                        If oInvoice.MATCH_ClientNo = sClientNo Then
                                                            bExportoBatch = True
                                                            Exit For
                                                        End If
                                                    ElseIf Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                        'An open invoice will be exported to the default client stated as clientno in the BB setup
                                                        If oPayment.VB_ClientNo = sClientNo Then
                                                            bExportoBatch = True
                                                            Exit For
                                                        End If
                                                    End If
                                                Next oInvoice
                                            Else
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                                        bKeepOppdragsNr = False ' Construct oppdragsnr
                                                        If oPayment.VB_ClientNo = sClientNo Then
                                                            ' added 07.03.2007
                                                            ' in "split-cases" it is better to find accountno for batch-level
                                                            ' here, and pass it to Wr_OCRBatchStart
                                                            sInAccount = oPayment.I_Account
                                                            bExportoBatch = True
                                                        End If
                                                    Else
                                                        bExportoBatch = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                    'If sSpecial = "NHST_INNBETALINGER" Then
                                    'Else
                                    Exit For
                                    'End If
                                End If 'falseOCR


                                '21.12.2009 - New for Haugaland, do not export GL-postings
                                If bExportoBatch And Not bExportGLTransactions Then
                                    bExportoBatch = False
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                bExportoBatch = True
                                                ' New 28.03.2011
                                                Exit For
                                            End If
                                        End If
                                    Next oInvoice
                                End If
                                ' 29.10.2014
                                ' changed 27.02.2015
                                ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                'If sSpecial = "NHST_INNBETALINGER" Then
                                '    bExportoBatch = False
                                '    For Each oInvoice In oPayment.Invoices
                                '        If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" And oPayment.VB_ClientNo = sClientNo Then
                                '            bExportoBatch = True
                                '            ' 14.01.2015 - must exit here, since we have at least one invoice to export
                                '            Exit For
                                '        Else
                                '            bExportoBatch = False
                                '        End If
                                '    Next
                                'End If


                            Else
                                bFBOsExist = True
                                Exit For
                            End If


                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                If bFalseOCR Then 'If FalseOCR then calculate the Oppdragsnr.
                                    bKeepOppdragsNr = False
                                    'Have to find the Avtale_ID when the import-format isn't OCR
                                    sContractNo = ""
                                    bFoundContractNo = False
                                    For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                        For Each oClient In oFilesetup.Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oBatch.Payments.Item(1).I_Account Then
                                                    sContractNo = oaccount.ContractNo
                                                    If Len(sContractNo) > 0 Then
                                                        bFoundContractNo = True

                                                        '15.06.2010 - New code - Miroslav Klose
                                                        If bUseDayTotals Then
                                                            'Find the correct client in the array
                                                            For lCounter = 0 To UBound(aAccountArray, 2)
                                                                If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                                    lArrayCounter = lCounter
                                                                    Exit For
                                                                End If
                                                            Next lCounter
                                                            Exit For
                                                        End If

                                                        Exit For
                                                    End If

                                                    ' 25.09.06 Added a test also to ConvertedAccount (ref SG Finans
                                                    ' Removed 09.07.2008 beacuse if one common ConvertedAccountno was set for several
                                                    ' clients, then the Contractno may be collected from the wrong client !!!!!
                                                ElseIf Not EmptyString((oaccount.ConvertedAccount)) And (Trim(oaccount.ConvertedAccount) = oBatch.Payments.Item(1).I_Account) Then
                                                    If sSpecial = "SGCLIENTREPORTING" Then ' added 09.07.2008
                                                        ' 17.07.200 Added a test for SG to also check for correct client in case
                                                        ' of converted accounts
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        If oBatch.Payments(1).VB_ClientNo = oClient.ClientNo Then
                                                            sContractNo = oaccount.ContractNo
                                                            If Len(sContractNo) > 0 Then
                                                                bFoundContractNo = True
                                                                Exit For
                                                            End If
                                                        End If
                                                    Else
                                                        sContractNo = oaccount.ContractNo
                                                        If Len(sContractNo) > 0 Then
                                                            bFoundContractNo = True
                                                            Exit For
                                                        End If
                                                    End If
                                                    ' 28.02.2008
                                                    ' added a test to use clientno as lookup
                                                    ' must have clientno as accountno (used for SG ClientReporting)
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                ElseIf Not EmptyString(oBatch.Payments(1).VB_ClientNo) And (oBatch.Payments(1).VB_ClientNo = oClient.ClientNo And oClient.ClientNo = oaccount.Account) Then
                                                    sContractNo = oaccount.ContractNo
                                                    If Len(sContractNo) > 0 Then
                                                        bFoundContractNo = True
                                                        Exit For
                                                    End If
                                                End If

                                                If bFoundContractNo Then
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundContractNo Then
                                                Exit For
                                            End If
                                        Next oClient

                                        If bFoundContractNo Then
                                            Exit For
                                        End If
                                    Next oFilesetup
                                    If Not bFoundContractNo Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Err.Raise(35023, , LRS(35023, oBatch.Payments.Item(1).I_Account, "Avtale-ID") & vbCrLf & LRS(35004, "OCR") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                        '35023: No Contract-number (Avtale-ID) stated for account: %1.
                                        'Unable to create a correct OCR-file.
                                    End If
                                    'Find the GroupingNo, see declaration for explanation
                                    'Changed 11.09.2006 to get correct reference for NRX
                                    'Old code
                                    'sGroupNumber = "99" &VB6.Format(StringToDate(oBatch.DATE_Production), "DD") & "9" & PadLeft(Trim$(Str(oBatch.Index)), 5, "0")
                                    'New code
                                    If bOCRTransactions Then
                                        If Len(oBatch.REF_Bank) = 11 Then
                                            'New IF - ENDIF 18.06.2009, somtimes Cargo is not present (f.x. we retrieve from the DB
                                            If Not oBatch.Cargo Is Nothing Then
                                                eBank = oBatch.Cargo.Bank
                                            ElseIf oBabel.BankByCode <> BabelFiles.Bank.No_Bank_Specified Then
                                                eBank = oBabel.BankByCode
                                            Else
                                                eBank = BabelFiles.Bank.No_Bank_Specified
                                            End If

                                            Select Case eBank 'Old code - oBatch.Cargo.Bank

                                                Case BabelFiles.Bank.DnB
                                                    If Left(oBatch.REF_Bank, 3) = "050" Then
                                                        sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                    Else
                                                        sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                    End If
                                                Case BabelFiles.Bank.Nordea_NO
                                                    If Left(oBatch.REF_Bank, 3) = "013" Then
                                                        sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                    Else
                                                        sGroupNumber = "13" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                    End If
                                                Case BabelFiles.Bank.Fellesdata
                                                    If Left(oBatch.REF_Bank, 3) = "012" Then
                                                        sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                    Else
                                                        sGroupNumber = "12" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                    End If
                                                Case BabelFiles.Bank.Fokus_Bank, BabelFiles.Bank.Danske_Bank_NO
                                                    If Left(oBatch.REF_Bank, 3) = "019" Then
                                                        sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                    Else
                                                        sGroupNumber = "19" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                    End If
                                                Case Else
                                                    sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                            End Select
                                        Else
                                            sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                        End If
                                    Else
                                        If sSpecial = "CONECTO_HAMAR" Then
                                            If Len(oBatch.REF_Bank) > 10 Then
                                                oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                                            ElseIf Len(oBatch.REF_Bank) < 10 Then
                                                oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                                            End If
                                            sGroupNumber = oBatch.REF_Bank
                                        ElseIf sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Or sSpecial = "SERGELNORGE" Then
                                            '31.08.2022 - Added "MODHI_NORGE"
                                            '04.12.2018 - Added this IF
                                            '02.07.2020 - Added SERGELNORGE
                                            If Len(oBatch.REF_Bank) > 10 Then
                                                oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                                            ElseIf Len(oBatch.REF_Bank) < 10 Then
                                                oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                                            End If
                                            sGroupNumber = oBatch.REF_Bank
                                        Else
                                            sGroupNumber = "50" & Format(StringToDate(oBatch.DATE_Production), "dd") & "9" & PadLeft(Trim$(Str(oBatch.Index)), 5, "0")
                                        End If
                                    End If

                                    ' New by JanP 08.01.04
                                    ' Do not set ContractNo here if we already have filled it
                                    ' in f.ex. in TreatSpecial (ref BAMA)
                                    If Len(oBatch.REF_Own) = 9 Then
                                        sContractNo = ""
                                    End If
                                End If
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1
                            If IsAutogiro((oPayment.PayCode)) Then
                                bLastBatchType = "AUTOGIRO"
                                sLine = Wr_AutogiroReturnBatchStart(oBatch, sContractNo)
                                nBatchSumAmount = 0
                                nBatchNoRecords = 1
                                nBatchNoTransactions = 0
                            Else
                                bLastBatchType = "OCR"
                                'XNET 10.02.2011 - added special AvtaleID for Ergo
                                If sSpecial = "ERGOGROUP" Then
                                    ' use clientno, preceeded by 0's
                                    sContractNo = oPayment.VB_ClientNo
                                End If

                                ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                'If sSpecial = "NHST_INNBETALINGER" Then
                                '    ' In Infosoftexport, special accounts for NHST-companies
                                '    Select Case oClient.ClientNo
                                '        Case "F14002", "F13002"
                                '            '7014.05.34399	Dagens N�ringsliv	NOK	NO	DNBANOKK
                                '            sInAccount = "70140534399"
                                '        Case "F14003", "F13003"
                                '            '7014.05.34410	TradeWinds	NOK	NO	DNBANOKK
                                '            sInAccount = "70140534410"
                                '        Case "F14006", "F13006"
                                '            '7014.05.39420	Upstream	NOK	NO	DNBANOKK
                                '            sInAccount = "70140539420"
                                '        Case "F14009", "F13009"
                                '            '5082.05.35259	Europower	NOK	NO	DNBANOKK
                                '            sInAccount = "50820535259"
                                '        Case "F14015", "F13015"
                                '            '7050.05.76977	Intrafish Media	NOK	NO	DNBANOKK
                                '            sInAccount = "70500576977"
                                '        Case "F14032", "F13032"
                                '            '7014.05.39420	Recharge	NOK	NO	DNBANOKK
                                '            sInAccount = "70140539420"
                                '        Case "F14007", "F13007"
                                '            '6511.09.60101	Fiskereibladet Fiskaren	NOK	NO	NDEANOKK
                                '            sInAccount = "65110960101"
                                '    End Select

                                'End If

                                sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False, sInAccount, sSpecial)
                            End If
                            oFile.WriteLine((sLine))

                            j = 0
                            For Each oPayment In oBatch.Payments

                                ' XNET 16.05.2012
                                If sSpecial = "HB_OCR" Then
                                    ' for this case, we will export one file pr account
                                    ' the call to the export loops until all payments exported
                                    If oPayment.I_Account <> sI_Account Then
                                        bExit_HBOCR = True
                                        Exit For
                                    End If
                                End If

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile



                                '05.09.2008 - Changed due to error from "510" to IsOCR
                                If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                    'Old code
                                    'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                    'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' Changed by JanP 07.01.04
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                        '29.12.05 - Added test, in case of mix of Autogiro/OCR
                                        ' Will then export Autogiro in separate file(s)
                                        If oPayment.ImportFormat <> BabelFiles.FileType.AutoGiro Or oBabel.Cargo.Special = "AUTOGIRO" Or IsAutogiro((oPayment.PayCode)) Then

                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                'XNET - 04.01.2011 - Added next IF
                                                If bCheckClientNoAgainstInvoice Then
                                                    For Each oInvoice In oPayment.Invoices
                                                        If oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                            If oInvoice.MATCH_ClientNo = sClientNo Then
                                                                bExportoPayment = True
                                                                Exit For
                                                            End If
                                                        ElseIf Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                            If oPayment.VB_ClientNo = sClientNo Then
                                                                bExportoPayment = True
                                                                Exit For
                                                            End If
                                                        End If
                                                    Next oInvoice
                                                Else
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                                            If oPayment.VB_ClientNo = sClientNo Then
                                                                bExportoPayment = True
                                                            End If
                                                        Else
                                                            bExportoPayment = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If 'falseOCR

                                ' Conecto Procasso needs to export two OCR-files;
                                ' - one for Procasso
                                ' - one for Predator
                                If sSpecial = "TILPREDATOR" Then
                                    If oPayment.ExtraD1 = "TILPREDATOR" Then
                                        bExportoPayment = True
                                    Else
                                        bExportoPayment = False
                                    End If
                                End If

                                '21.12.2009 - New for Haugaland, do not export GL-postings
                                If bExportoPayment And Not bExportGLTransactions Then
                                    bExportoPayment = False
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                bExportoPayment = True
                                                ' 28.03.2011, new line
                                                Exit For
                                            End If
                                        End If
                                    Next oInvoice
                                End If

                                ' changed 27.02.2015
                                ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                'If sSpecial = "NHST_INNBETALINGER" Then
                                '    bExportoPayment = False
                                '    For Each oInvoice In oPayment.Invoices
                                '        If xDelim(oInvoice.MyField.ToUpper, "|", 1) = "INFOSOFT" And oPayment.VB_ClientNo = sClientNo Then
                                '            bExportoPayment = True
                                '            ' 14.01.2015 - must exit here, since we have at least one invoice to export
                                '            Exit For
                                '        Else
                                '            bExportoPayment = False
                                '        End If
                                '    Next
                                'End If


                                If bExportoPayment Then
                                    j = j + 1

                                    '29.09.2010 - New code for BKK - Unmatched structured payments should as one payment with the invoiceamount, not each unmatched invoice
                                    bContinue2 = True

                                    If bExportPaymentAmountForUnmatchedStructured Then
                                        If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
                                            If oPayment.PayCode = "629" Then
                                                If oPayment.Invoices.Count > 0 Then
                                                    For Each oInvoice In oPayment.Invoices
                                                        If oInvoice.MATCH_Final Then
                                                            If oInvoice.MATCH_Matched = False Then
                                                                Exit For
                                                            End If
                                                        End If
                                                    Next oInvoice
                                                    sLine = Wr_OCRPayOne(oPayment, oInvoice, nTransactionNo, sGroupNumber, bFalseOCR, bOCRTransactions, sSpecial, bUseMatch_ID, True)
                                                    oFile.WriteLine((sLine))
                                                    sLine = Wr_OCRPayTwo(oPayment, oInvoice, nTransactionNo, bFalseOCR, bOCRTransactions, (oBabel.Special))
                                                    oFile.WriteLine((sLine))

                                                    bContinue2 = False
                                                    oPayment.Exported = True
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bContinue2 Then
                                        For Each oInvoice In oPayment.Invoices
                                            If (bFalseOCR And Not bOCRTransactions And Not oInvoice.MATCH_Final) Then
                                                'Nothing to do. This is not a final transaction, and should therefore not be exported.
                                                'Added next ElseIf 02.09.2010 for Conecto to prevent exporting manually matched OCR-transactions double
                                            ElseIf (bFalseOCR And bOCRTransactions And Not oInvoice.MATCH_Final) Then
                                                'Nothing to do, just export the final invoices.
                                            ElseIf bCheckIfInvoiceIsExported And oInvoice.Exported Then
                                                bCheckIfInvoiceIsExported = True
                                                'Nothing to do. The invoice is previouslly exported
                                            Else
                                                ' Add to transactionno
                                                nTransactionNo = nTransactionNo + 1
                                                If IsAutogiro((oPayment.PayCode)) And oBabel.Cargo.Special <> "AUTOGIRO" Then
                                                    ' oBabel.Cargo.Special = "AUTOGIRO" means that Autogiro goes to a separate file
                                                    sLine = Wr_AutogiroReturnPayOne(oPayment, oInvoice, nTransactionNo)
                                                    ' Added 22.12.06.
                                                    ' In a mix of OCR and Autogiro, we must update totals here, to be able to
                                                    ' keep correct info in endOfFile record
                                                    ' Add to Batch-totals:
                                                    nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
                                                    nBatchNoRecords = nBatchNoRecords + 2 'both 30 and 31, payment1 and 2
                                                    nBatchNoTransactions = nBatchNoTransactions + 1

                                                    oFile.WriteLine((sLine))
                                                    sLine = Wr_AutogiroReturnPayTwo(oPayment, nTransactionNo)
                                                    oFile.WriteLine((sLine))

                                                    '15.06.2010 - New code - Miroslav Klose
                                                    If bUseDayTotals Then
                                                        'Add to totalamountexported
                                                        aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                    End If

                                                Else

                                                    '21.12.2009 - New for Haugaland, do not export GL-postings
                                                    If Not bExportGLTransactions Then
                                                        bContinue = False
                                                        If oInvoice.MATCH_Final Then
                                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                                bContinue = True
                                                            End If
                                                        End If
                                                    Else
                                                        bContinue = True
                                                    End If

                                                    'XNET - 04.01.2011 - Added next IF
                                                    If bCheckClientNoAgainstInvoice And bContinue Then
                                                        If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.OpenInvoice And oInvoice.MATCH_Final Then
                                                            If oInvoice.MATCH_ClientNo = sClientNo Then
                                                                bContinue = True
                                                                oInvoice.Exported = True
                                                                ' Else new 28.03.2011
                                                            Else
                                                                bContinue = False
                                                                nTransactionNo = nTransactionNo - 1 'Added before the test
                                                            End If
                                                        ElseIf Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                                            'An open invoice will be exported to the default client stated as clientno in the BB setup
                                                            If oPayment.VB_ClientNo = sClientNo Then
                                                                bContinue = True
                                                                oInvoice.Exported = True
                                                            End If
                                                        End If
                                                    End If

                                                    ' 15.10.2014 For NHST, export as OCR only payments marked with "INFOSOFT" in oPayment.Match_Myfield
                                                    ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
                                                    'If sSpecial = "NHST_INNBETALINGER" Then
                                                    '    If xDelim(oInvoice.MyField.ToUpper, "|", 1) <> "INFOSOFT" Then
                                                    '        'bContinue = True
                                                    '        'Else
                                                    '        bContinue = False
                                                    '    End If
                                                    'End If

                                                    If bContinue Then
                                                        ' 2008.03.14 - something wrong with special to OCR, check later.
                                                        ' Kjell har problems, must go back to office
                                                        If UCase(oBabel.VB_Profile.CompanyName) = "DELTA" Then
                                                            sSpecial = "DELTA"
                                                        End If
                                                        sLine = Wr_OCRPayOne(oPayment, oInvoice, nTransactionNo, sGroupNumber, bFalseOCR, bOCRTransactions, sSpecial, bUseMatch_ID)
                                                        oFile.WriteLine((sLine))
                                                        If sSpecial = "DELTA" Then
                                                            sLine = Wr_OCRPayTwo(oPayment, oInvoice, nTransactionNo, bFalseOCR, bOCRTransactions, sSpecial)
                                                        Else
                                                            sLine = Wr_OCRPayTwo(oPayment, oInvoice, nTransactionNo, bFalseOCR, bOCRTransactions, (oBabel.Special))
                                                        End If
                                                        oFile.WriteLine((sLine))
                                                        '15.06.2010 - New code - Miroslav Klose
                                                        If bUseDayTotals Then
                                                            'Add to totalamountexported
                                                            aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                        End If
                                                    End If
                                                End If
                                                'XNET - 04.01.2011 - Added next IF
                                                If Not bCheckClientNoAgainstInvoice Then 'Done later based on oInvoice.Exported (may be more than 1 client under a payment
                                                    oPayment.Exported = True
                                                End If
                                            End If
                                        Next oInvoice 'invoice
                                    End If
                                End If

                            Next oPayment ' payment

                            If bLastBatchType = "AUTOGIRO" Then
                                sLine = Wr_AutogiroReturnBatchEnd()
                                ' Added 22.12.06.
                                ' In a mix of OCR and Autogiro, we must update totals here, to be able to
                                ' keep correct info in endOfFile record
                                ' Add to Batch-totals:

                                'Added 18.01.2010 - We (read: Mr. Skogvang I presume) had forgotten to count the 88-record itself.
                                nBatchNoRecords = nBatchNoRecords + 1 'Also count 88-recs

                                ' Add to File-totals:
                                nFileSumAmount = nFileSumAmount + nBatchSumAmount
                                nFileNoRecords = nFileNoRecords + nBatchNoRecords
                                nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions
                            Else
                                sLine = Wr_OCRBatchEnd(oBatch, False)
                            End If
                            oFile.WriteLine((sLine))

                            If StringToDate((oBatch.DATE_Production)) > dProductionDate Then
                                dProductionDate = StringToDate((oBatch.DATE_Production))
                            End If

                        End If

                        ' XNET 16.05.2012
                        If sSpecial = "HB_OCR" Then
                            ' for this case, we will export one file pr account
                            ' the call to the export loops until all payments exported
                            If bExit_HBOCR Then
                                Exit For
                            End If
                        End If

                    Next oBatch 'batch

                End If

                ' XNET 16.05.2012
                If sSpecial = "HB_OCR" Then
                    ' for this case, we will export one file pr account
                    ' the call to the export loops until all payments exported
                    If bExit_HBOCR Then
                        Exit For
                    End If
                End If

            Next oBabel

            'New code 07.11.2002 by Iggy
            If bFBOsExist Then
                'New 24.05.2006 - possible to split FBOs in a seperate file
                If oBabelFiles.Item(1).VB_ProfileInUse Then
                    'Find correct filesetup
                    'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For Each oFilesetup In oBabelFiles.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabelFiles.Item(1).VB_FileSetupID Then
                            Exit For
                        End If
                    Next oFilesetup
                    'Check if we shall split OCR and FBO
                    If Not oFilesetup Is Nothing Then
                        'Switch to the FilesetupOut
                        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        oFilesetup = oBabelFiles.VB_Profile.FileSetups(oFilesetup.FileSetupOut)
                        If oFilesetup.SplitOCRandFBO Then
                            'OK FBOsExist and they sahll be split in a seperate file
                            'First, write file end for the OCR-postings
                            If nFileNoRecords > 0 Then
                                sLine = Wr_OCRFileEnd(dProductionDate)
                                oFile.WriteLine((sLine))
                                oFile.Close()
                                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                oFile = Nothing
                            Else
                                oFile.Close()
                                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                oFile = Nothing
                                oFs.DeleteFile((sFilenameOut))
                            End If
                            'Then reset counters
                            nBatchSumAmount = 0
                            nBatchNoRecords = 0
                            nBatchNoTransactions = 0
                            nFileSumAmount = 0
                            nFileNoRecords = 0
                            nFileNoTransactions = 0
                            'Create the new FBO filename
                            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iStartPosFilename = InStrRev(sFilenameOut, "\", , CompareMethod.Text) + 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sFBOFileName = Left(sFilenameOut, iStartPosFilename - 1) & "FBO_" & Mid(sFilenameOut, iStartPosFilename)
                            oFile = oFs.OpenTextFile(sFBOFileName, Scripting.IOMode.ForWriting, True, 0)
                            'Write a FileStartRecord
                            bStartRecordWritten = False
                        Else
                            'bStartRecordWritten = True ' Is already set to true if a OCR-trans is written
                        End If
                    End If
                End If
                'FBOs exist, and we have to write a new Oppdrag (Batch).
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        bBatchStartWritten = False
                        nFBOTransactionNo = 0
                        If oBatch.FormatType = "OCR_FBO" Then
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'XNET - Removed next 8 lines
                                'If Not bMultiFiles Then
                                '    bExportoPayment = True
                                'Else
                                '    If sI_Account = oPayment.I_Account Then
                                '        bExportoPayment = True
                                '    Else
                                '        bExportoPayment = False
                                '    End If

                                'XNET 07.12.2010 - added next IF
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    If Not bStartRecordWritten Then
                                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, bFalseOCR, bOCRTransactions, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bStartRecordWritten = True
                                    End If
                                    'Write the start Batch
                                    ' Only 1 batch per account, and new
                                    '   batch for a new account.
                                    If Not bBatchStartWritten Then
                                        'XNET 10.02.2011 - added special AvtaleID for Ergo
                                        If sSpecial = "ERGOGROUP" Then
                                            ' use clientno, preceeded by 0's
                                            sContractNo = oPayment.VB_ClientNo
                                        End If
                                        sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, True, sInAccount, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bBatchStartWritten = True
                                    End If
                                    nFBOTransactionNo = nFBOTransactionNo + 1
                                    sLine = Wr_FBO(oPayment, nFBOTransactionNo)
                                    oFile.WriteLine((sLine))
                                    oPayment.Exported = True
                                End If
                            Next oPayment
                            'If the start is written, write the end of the batch
                            If bBatchStartWritten Then
                                sLine = Wr_OCRBatchEnd(oBatch, True)
                                oFile.WriteLine((sLine))
                            End If
                        End If
                    Next oBatch
                Next oBabel
            End If

            'End new code 07.11.2002 by Iggy
            'Changed 17/10 by Kjell
            'This code is taken out of the For each oBabel.Babelfiles loop (at the end)
            'Changed 28/11 by Kjell
            'That was not a good idea. If there are no records to export, don't write the OCRFileEnd

            'XNET - 04.01.2011 - Added next IF
            If bCheckClientNoAgainstInvoice Then
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            bAllInvoicesExported = True
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    If Not oInvoice.Exported Then
                                        bAllInvoicesExported = False
                                        Exit For
                                    End If
                                End If
                            Next oInvoice
                            If bAllInvoicesExported Then
                                oPayment.Exported = True
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabel
            End If
            'End XNET

            '04.12.2008 - New added next IF
            If bUseDayTotals Then
                For lCounter = 0 To UBound(aAccountArray, 2)
                    If bOCRTransactions Then
                        dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "OCR", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                    Else
                        'Old code
                        'dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "Matched_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                        'New code 27.04.2022
                        dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "Matched_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                    End If
                Next lCounter
            End If

            ' 22.01.2015 changed from > 0 to > 1, since startrecord creates nFileNoRecords = 1
            If nFileNoRecords > 1 Then
                sLine = Wr_OCRFileEnd(dProductionDate)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteOCRFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteOCRFile = True

    End Function
    Function Wr_OCRFileStart(ByRef oBabelFile As BabelFile, ByRef sAdditionalID As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, Optional ByRef sSpecial As String = "", Optional ByVal sClientNo As String = "") As String

        Dim sLine As String
        ' XNET 03.04.2013
        Dim sKundeenhetsID As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0

        sLine = "NY000010"
        ' husk evt ledende 0-er!

        'Changed 16/06-2003, Kjell
        'From
        'Changed Sender and receiver, as in the import of OCR

        If bFalseOCR Then
            '24/1-04 Added the next IF. No special reason, but it  seemed logical to
            '  write the BBS-ID on real OCR-transactions
            If bOCRTransactions Then
                sLine = sLine & "00008080"
            Else
                sLine = sLine & "82735532"
            End If
        Else
            If Len(oBabelFile.IDENT_Sender) = 0 Or sSpecial = "FJELLINJEN_VIPPS" Then
                sLine = sLine & "00008080"
            Else
                sLine = sLine & PadLeft(Mid(RemoveCharacters(oBabelFile.IDENT_Sender, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 1, 8), 8, "0") ' dataavsender XNET 10.05.2011 - Added RemoveCharacters
            End If
        End If

        '05.11.2008 - New for BKK - added sSpecial and the next IF
        ' XNET 11.06.2012 Added SFE (Sogn og Fjordane Energi)
        ' XNET 13.09.2012 Added APRESSEN
        ' XNET 02.05.2013 Added VEGFINANS
        '13.06.2017 - Added FIRST_SEC
        If sSpecial = "FIRST_SEC" Then
            sLine = sLine & Right(Right(Format(Now, "yyyyMMdd"), 6) & Right(Format(Now, "ss"), 1), 7) ' forsendelsesnr
            ' 30.11.2017 - We are not using OCR as Infosoft format anylonger
        ElseIf sSpecial = "TRYM" Or sSpecial = "BKK" Or sSpecial = "SKAGERAK ENERGI" Or sSpecial = "HAUGALAND KRAFT" Or sSpecial = "SFE" Or sSpecial = "APRESSEN" Or sSpecial = "VEGFINANS" Or sSpecial = "GJENSIDIGE_GAUDA" Then
            sLine = sLine & Right(Right(VB6.Format(Now, "mmdd"), 3) & VB6.Format(Now, "NnSs"), 7) ' forsendelsesnr
        ElseIf Len(oBabelFile.File_id) < 8 Then
            sLine = sLine & PadLeft(Mid(RemoveCharacters(oBabelFile.File_id, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 1, 7), 7, "0") ' forsendelsesnr XNET 10.05.2011 - Added RemoveCharacters
        Else
            sLine = sLine & Right$(RemoveCharacters(oBabelFile.File_id, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 7) 'XNET 10.05.2011 - Added RemoveCharacters
        End If

        ' XNET 03.04.2013 - special handling of receiverID (kundeenhetsID) for Vegfinans, Drammen
        If sSpecial = "VEGFINANS" Or sSpecial = "VEGFINANS_REGNR" Then
            ' have set kundeenhetsID on account, as AvtaleID
            ' Find client, and account
            ' Assumes that kundeenhetsID is set on all accounts for actual client. What's left for us is to find setup for first account, as files as splitted at this stage
            '07.08.2018 - Changed from sI_Account to sClientNo, see calling routine for explaination
            sKundeenhetsID = FindKundeenhetsIDVegfinans(oBabelFile, sClientNo)
            'sKundeenhetsID = FindKundeenhetsIDVegfinans(oBabelFile, sI_Account)
            sLine = sLine & PadLeft(sKundeenhetsID, 8, "0")
        ElseIf sSpecial = "ESPEN" Then
            sLine = sLine & PadLeft("00118125", 8, "0")
        Else
            If sAdditionalID.Trim <> vbNullString Then
                sLine = sLine & PadLeft(sAdditionalID, 8, "0")
            Else
                sLine = sLine & PadLeft(Mid(RemoveCharacters(oBabelFile.IDENT_Receiver, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 1, 8), 8, "0") ' datamottaker XOKNET 10.05.2011 - Added RemoveCharacters
            End If
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_OCRFileStart = sLine

    End Function
    Private Function Wr_OCRPayOne(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef nTransactionNo As Double, ByRef sGroupNumber As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String, Optional ByRef bUseMatch_ID As Boolean = False, Optional ByRef bUsePaymentAmount As Boolean = False, Optional ByRef sNoMatchAccount As String = "") As String

        Dim sLine As String
        Dim sTempString As String

        ' Add to Batch-totals:
        If bUsePaymentAmount Then
            nBatchSumAmount = nBatchSumAmount + oPayment.MON_TransferredAmount
        Else
            nBatchSumAmount = nBatchSumAmount + oInvoice.MON_TransferredAmount
        End If
        nBatchNoRecords = nBatchNoRecords + 2 'both 30 and 31, payment1 and 2
        nBatchNoTransactions = nBatchNoTransactions + 1

        If StringToDate((oPayment.DATE_Payment)) > dBatchLastDate Then
            dBatchLastDate = StringToDate((oPayment.DATE_Payment))
        End If
        If StringToDate((oPayment.DATE_Payment)) < dBatchFirstDate Then
            dBatchFirstDate = StringToDate((oPayment.DATE_Payment))
        End If

        sLine = "NY09"
        ' added 2008.03.12 for Delta, using Focus.
        ' Set "88" as Transtype, to make Focus able to classify as "Innbet. via BabelBank"
        If sSpecial = "DELTA" And Not bOCRTransactions Then
            sLine = sLine & "88"
        ElseIf sSpecial = "BKK" Then
            sLine = sLine & "10"
        ElseIf sSpecial = "SERGELNORGE" Then
            If oPayment.E_Name.Trim = "Kortbetaling" Then
                sLine = sLine & "12"
            Else
                sLine = sLine & "10"
            End If
        Else
            ' trekker transtype fra Babelbanks interne kodesett
            sLine = sLine & bbGetPayCode(Trim(oPayment.PayCode), "OCR")
            End If
            sLine = sLine & "30"
            sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        If sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Then
            'Added 31.08.2022 - "MODHI_NORGE"
            'Added this IF for Confide 01.04.2019
            '02.07.2020 - Added SERGELNORGE
            If oPayment.SpecialMark = True Then
                If Not EmptyString(oInvoice.MyField) Then
                    If oInvoice.MyField.Length = 10 Then
                        sTempString = oInvoice.MyField
                        sLine = sLine & oInvoice.MyField.Substring(0, 2) & oInvoice.MyField.Substring(3, 2) & oInvoice.MyField.Substring(8, 2)  ' Bankdate
                    Else
                        sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
                    End If
                Else
                    sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
                End If
            Else
                sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "DDMMYY") ' Bankdate
            End If
        ElseIf StringToDate((oPayment.DATE_Payment)) > (System.DateTime.FromOADate(Now.ToOADate - 1000)) Then
            ' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "ddmmyy")
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "ddmmyy")
        End If
            'Possible error since REF_Own consists of 4 different fields in the format. If some are
            'omitted it may affect the others

            ' XNET 16.05.2012 - serious error when REF_Own is blank, og groupno < 10, two changes below, padright
            If Len(sGroupNumber) > 0 Then
                sLine = sLine & PadLeft(sGroupNumber, 10, "0")
            Else
                sLine = sLine & PadLeft(Right$(Trim$(oPayment.REF_Own), 10), 10, "0") ' First 2pos. Sentral ID, 3-4 Dagkode
                ' 5 Delavregningsnr., 6-10 L�penummer
            End If
            'New IF-sentence 15.11.2006
            If sSpecial = "BAMA" Then
                sLine = sLine & "0" ' Filler
                sLine = sLine & PadLeft(Str(oInvoice.MON_TransferredAmount), 17, "0") ' Amount
                'New IF-sentence 30.06.2008
                '07.09.2009 - Removed next 2 IFs. IS Customer are now able to accept creditnotes according to the OCR-format.
                '''ElseIf sSpecial = "SKAGERAK ENERGI" Then
                '''    If oInvoice.MON_TransferredAmount < 0 Then
                '''        sLine = sLine & "0-"
                '''    Else
                '''        sLine = sLine & "00"              ' Filler
                '''    End If
                '''    sLine = sLine & PadLeft(Str(Abs(oInvoice.MON_TransferredAmount)), 16, "0") ' Amount
                '''ElseIf sSpecial = "HAUGALAND KRAFT" Then
                '''    If oInvoice.MON_TransferredAmount < 0 Then
                '''        sLine = sLine & "0-"
                '''    Else
                '''        sLine = sLine & "00"              ' Filler
                '''    End If
                '''    sLine = sLine & PadLeft(Str(Abs(oInvoice.MON_TransferredAmount)), 16, "0") ' Amount
            Else
                'Normal situation
                ' 21.04.06 Changed to be able to handle Creditnotes
                '29.09.2010
                If bUsePaymentAmount Then
                    sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 18, "0") ' Amount
                Else
                    'Normal situation
                    If oInvoice.MON_TransferredAmount < 0 Then
                        sLine = sLine & "-"
                    Else
                        sLine = sLine & "0" ' Filler
                    End If
                    sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_TransferredAmount)), 17, "0") ' Amount
                End If
            End If
            '20.11-03, KID ligger i Match_MatchID for avstemte transaksjoner
            If bFalseOCR And Not bOCRTransactions Then
                If InStr(oInvoice.MATCH_ID, "&") > 0 Then
                    'If the KID contains spaces, Elkj�p
                    sLine = sLine & Replace(PadLeft(Trim(oInvoice.MATCH_ID), 25, " "), "&", " ") ' KID
                Else
                    ' added 30.03.2010 - fetches wrong OrgNo from Focus for NAV-poster
                    ' test if it is "abnormal long"
                    If sSpecial = "DELTA" And Len(Trim(oInvoice.MATCH_ID)) > 19 Then
                        sLine = sLine & PadLeft("500999999999001", 25, " ") ' KID
                    Else
                        If sSpecial = "CONECTO_FAS" Then
                            If oPayment.MATCH_Matched <> BabelFiles.MatchStatus.Matched Then
                                sLine = sLine & PadLeft(Trim(sNoMatchAccount), 25, " ") ' KID
                            Else
                                sLine = sLine & PadLeft(Trim(oInvoice.MATCH_ID), 25, " ") ' KID
                            End If
                        ElseIf sSpecial = "TRYM" Or sSpecial = "HELSEVEST_VIPPS" Then
                            sLine = sLine & PadLeft(oInvoice.Unique_Id, 25, " ") ' KID
                        Else
                            ' normal
                            sLine = sLine & PadLeft(Trim(oInvoice.MATCH_ID), 25, " ") ' KID
                        End If
                    End If
                End If
            Else
                ' 02.09.2010 added next test The IF-part is new
                If bUseMatch_ID Then ' Usually this means that OCR-payments is been matched by BB
                    ' no KID, use match_id
                    sLine = sLine & PadLeft(Trim(oInvoice.MATCH_ID), 25, " ") ' KID
                Else
                    'Normal situation
                    sLine = sLine & PadLeft(oInvoice.Unique_Id, 25, " ") ' KID
                End If
            End If

            sLine = PadLine(sLine, 80, "0")
            Wr_OCRPayOne = sLine

    End Function

    Private Function Wr_OCRPayTwo(ByRef oPayment As Payment, ByRef oInvoice As vbBabel.Invoice, ByRef nTransactionNo As Double, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String) As String
        '04.12.2018 - Added oInvoice as a parameter, also added in calling function.
        Dim sLine, sTempString As String
        Dim lCounter As Integer

        sLine = "NY09"
        ' added 2008.03.12 for Delta, using Focus.
        ' Set "88" as Transtype, to make Focus able to classify as "Innbet. via BabelBank"
        If sSpecial = "DELTA" And Not bOCRTransactions Then
            sLine = sLine & "88"
        ElseIf sSpecial = "BKK" Then
            sLine = sLine & "10"
        Else
            ' trekker transtype fra Babelbanks interne kodesett
            sLine = sLine & bbGetPayCode(Trim(oPayment.PayCode), "OCR")
        End If
        sLine = sLine & "31"
        sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        If sSpecial = "CONECTO_FAS" Then
            sLine = sLine & PadLeft(oPayment.Unique_PaymentID, 10, "0") ' Form no (Blankettnr.)
        Else
            sLine = sLine & PadLeft(oPayment.REF_Bank2, 10, "0") ' Form no (Blankettnr.)
        End If
        If bFalseOCR Then
            'This field is numeric, so when we create a false-OCR-file we replace alfasigns with 0.
            If IsNumeric(oPayment.REF_Bank1) Then
                sLine = sLine & PadLeft(Mid(oPayment.REF_Bank1, 1, 9), 9, "0") ' ContractID/Archiveref
            Else
                sTempString = Trim(oPayment.REF_Bank1)
                For lCounter = 1 To Len(sTempString)
                    If Not IsNumeric(Mid(sTempString, lCounter, 1)) Then
                        If Not lCounter = Len(sTempString) Then
                            sTempString = Left(sTempString, lCounter - 1) & "0" & Mid(sTempString, lCounter + 1)
                        Else
                            sTempString = Left(sTempString, lCounter - 1) & "0"
                        End If
                    End If
                Next lCounter
                sLine = sLine & PadLeft(sTempString, 9, "0") ' ContractID/Archiveref
            End If
        Else
            sLine = sLine & PadLeft(Mid(oPayment.REF_Bank1, 1, 9), 9, "0") ' ContractID/Archiveref
        End If
        sLine = sLine & PadLeft(Mid(oPayment.ExtraD5, 1, 7), 7, "0") ' Postgiro account
        If sSpecial = "CONECTO_AF" Then
            If oPayment.DATE_Payment = "19900101" Then
                If oPayment.DATE_Value = "19900101" Then
                    sLine = sLine & New String("0", 6)
                Else
                    sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
                End If
            Else
                sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "DDMMYY") ' Bankdate
            End If
        ElseIf sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Then
            '31.08.2022 - Added "MODHI_NORGE"
            If oPayment.SpecialMark = True Then
                If Not EmptyString(oInvoice.MyField) Then
                    If oInvoice.MyField.Length = 10 Then
                        sTempString = oInvoice.MyField
                        sLine = sLine & oInvoice.MyField.Substring(0, 2) & oInvoice.MyField.Substring(3, 2) & oInvoice.MyField.Substring(8, 2)  ' Bankdate
                    Else
                        sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
                    End If
                Else
                    sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
                End If
            Else
                sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "DDMMYY") ' Bankdate
            End If
        ElseIf sSpecial = "SERGELNORGE" Then
            If oPayment.DATE_Initiated = "19900101" Then
                If oPayment.DATE_Value = "19900101" Then
                    If oPayment.DATE_Payment = "19900101" Then
                        sLine = sLine & New String("0", 6)
                    Else
                        sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "DDMMYY") ' Bankdate
                    End If
                    sLine = sLine & New String("0", 6)
                Else
                    sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
                End If
            Else
                sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Initiated)), "DDMMYY") ' Bankdate
            End If
        Else
            If oPayment.DATE_Value = "19900101" Then
                sLine = sLine & New String("0", 6)
            Else
                sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
            End If
        End If

        '01.06.2021 - Added specialaccountno for kortbetalinger
        '01.06.2021 - Stein called and they want to delay it.
        'Uncomment this IF later
        'If sSpecial = "SERGELNORGE" And oPayment.E_Name.Trim = "Kortbetaling" Then
        'sLine = sLine & "99999999999"
        'Else
        '03.06.2010 - Next IF is added to not export invalid accountnos to the ocr-file
        If bFalseOCR Then
            '01.09.2022 - Removed removing accounts with 9 in fifth position.

            '19.10.2022 - Added next IF
            If sSpecial = "SPBK1_PORTEFOLJE" Then
                If EmptyString(oPayment.E_Account) Then
                    sLine = sLine & "18132100426"
                ElseIf oPayment.E_Account = "00000000000" Then
                    sLine = sLine & "18132100426"
                Else
                    sLine = sLine & PadLeft(Mid(oPayment.E_Account, 1, 11), 11, "0")
                End If
            Else
                sLine = sLine & PadLeft(Mid(oPayment.E_Account, 1, 11), 11, "0")
            End If

            'Old code
            'If oPayment.PayType = "D" And Mid(oPayment.E_Account, 5, 1) = "9" Then
            '    sLine = sLine & "00000000000"
            'Else
            '    sLine = sLine & PadLeft(Mid(oPayment.E_Account, 1, 11), 11, "0")
            'End If
        Else
            sLine = sLine & PadLeft(Mid(oPayment.E_Account, 1, 11), 11, "0")
        End If
        'End If

        ' For Politiets Fellesforbund, we are putting first 22 chars from payers name into Filler, from pos 59-80
        If sSpecial = "POLITIETSFELLESFORBUND" Then
            sLine = sLine & PadRight(oPayment.E_Name, 22, " ")
        End If
        sLine = PadLine(sLine, 80, "0")

        Wr_OCRPayTwo = sLine

    End Function
    Private Function Wr_FBO(ByRef oPayment As Payment, ByRef nFBOTransactionNo As Double) As String

        Dim sLine As String

        ' Add to Batch-totals:
        nBatchNoRecords = nBatchNoRecords + 1 'just a 70-record
        nBatchNoTransactions = nBatchNoTransactions + 1

        'If StringToDate(oPayment.DATE_Payment) > dBatchLastDate Then
        '    dBatchLastDate = StringToDate(oPayment.DATE_Payment)
        'End If
        'If StringToDate(oPayment.DATE_Payment) < dBatchFirstDate Then
        '    dBatchFirstDate = StringToDate(oPayment.DATE_Payment)
        'End If

        sLine = "NY219470"
        sLine = sLine & PadLeft(Str(nFBOTransactionNo), 7, "0") ' transnr
        ' trekker transtype fra Babelbanks interne kodesett
        sLine = sLine & bbGetPayCode(Trim(oPayment.PayCode), "OCR_FBO")
        sLine = sLine & PadLeft(oPayment.Invoices.Item(1).Unique_Id, 25, " ") ' KID
        If oPayment.NOTI_NotificationType = "PAPER" Then
            sLine = sLine & "J"
        Else
            sLine = sLine & "N"
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_FBO = sLine


    End Function

    Private Function Wr_OCRFileEnd(ByRef dProductionDate As Date) As String

        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1 ' also count 89-recs

        sLine = "NY000089"

        sLine = sLine & PadLeft(Str(nFileNoTransactions), 8, "0") ' No of transactions in file (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0") ' No of records in file (including 20 and 88)
        sLine = sLine & PadLeft(Str(nFileSumAmount), 17, "0") ' Total amount in file

        If dProductionDate > DateSerial(CInt("1999"), CInt("12"), CInt("01")) Then
            sLine = sLine & VB6.Format(dProductionDate, "DDMMYY")
        Else
            sLine = sLine & VB6.Format(Now, "ddmmyy") ' BBS date (Productiondate of file)
        End If

        ' pad with 0 to 80
        sLine = PadLine(sLine, 80, "0")

        Wr_OCRFileEnd = sLine

    End Function
    Function Wr_OCRBatchStart(ByRef oBatch As Batch, ByRef bKeepOppdragsNr As Boolean, ByRef sContractNo As String, ByRef bFBO As Boolean, ByRef sInAccount As String, ByRef sSpecial As String) As String
        Dim sLine As String
        Dim bPostAgainstObservationAccount As Boolean = False
        Static lTaskNumber As Integer

        If sSpecial = "PostAgainstObservationAccount" Then
            bPostAgainstObservationAccount = True
        End If

        ' Reset Batch-totals:
        nBatchSumAmount = 0
        nBatchNoRecords = 1
        nBatchNoTransactions = 0
        dBatchLastDate = System.Date.FromOADate(Now.ToOADate - 1000)
        dBatchFirstDate = System.Date.FromOADate(Now.ToOADate + 1000)

        If bFBO Then
            sLine = "NY212420"
        Else
            sLine = "NY090020"
        End If
        If Len(sContractNo) > 0 Then 'Only when FalseOCR
            sLine = sLine & PadLeft(sContractNo, 9, "0") ' avtaleid
        Else
            sLine = sLine & PadLeft(Mid(oBatch.REF_Own, 1, 9), 9, "0") ' avtaleid
        End If
        If bKeepOppdragsNr Then
            sLine = sLine & PadLeft(Mid(oBatch.Batch_ID, 1, 7), 7, "0") ' oppdragsnr
        Else
            ' To be used when we split on other than accountno, and for FalseOCR
            lTaskNumber = lTaskNumber + 1
            sLine = sLine & PadLeft(Str(lTaskNumber), 7, "0") ' oppdragsnr
        End If
        ' added 07.03.2007 for "split-cases" with more than one account in same batch;
        If Not EmptyString(sInAccount) Then
            sLine = sLine & PadLeft(Mid(sInAccount, 1, 11), 11, "0") ' oppdragskonto
        Else
            '20.03.2009 - Added next IF
            If sSpecial = "NORSTAT" Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sLine = sLine & PadLeft(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, " ") ' oppdragskonto
            Else
                ' as before 07.03.2007
                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sLine = sLine & PadLeft(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, "0") ' oppdragskonto
            End If
        End If

        sLine = PadLine(sLine, 80, "0")

        Wr_OCRBatchStart = sLine

    End Function

    Function Wr_OCRBatchEnd(ByRef oBatch As Batch, ByRef bFBO As Boolean) As String
        Dim sLine As String

        nBatchNoRecords = nBatchNoRecords + 1 'Also count 88-recs

        ' Add to File-totals:
        nFileSumAmount = nFileSumAmount + nBatchSumAmount
        nFileNoRecords = nFileNoRecords + nBatchNoRecords
        nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions

        If bFBO Then
            sLine = "NY212488"
        Else
            sLine = "NY090088"
        End If

        sLine = sLine & PadLeft(Str(nBatchNoTransactions), 8, "0") ' No of transactions in batch (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nBatchNoRecords), 8, "0") ' No of records in batch (including 20 and 88)

        If bFBO Then
            sLine = sLine & "00000000000000000" 'Total amount
            sLine = sLine & "000000" ' BBS date (todays date)
            sLine = sLine & "000000" ' First date in batch,DDMMYY
            sLine = sLine & "000000" ' First date in batch, DDMMYY
        Else
            sLine = sLine & PadLeft(Str(nBatchSumAmount), 17, "0") ' Total amount in batch
            If oBatch.DATE_Production <> "19900101" Then
                sLine = sLine & VB6.Format(StringToDate((oBatch.DATE_Production)), "DDMMYY")
            Else
                sLine = sLine & VB6.Format(Now, "ddmmyy") ' BBS date (todays date)
            End If
            sLine = sLine & VB6.Format(dBatchFirstDate, "ddmmyy") ' First date in batch,DDMMYY
            sLine = sLine & VB6.Format(dBatchLastDate, "ddmmyy") ' First date in batch, DDMMYY
        End If

        sLine = PadLine(sLine, 80, "0")

        Wr_OCRBatchEnd = sLine

    End Function

    Function WriteSantanderOCRFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef sSpecial As String) As Object
        'Write all kinds of transactions to the OCR-file.
        ' May be used by other companies as weel
        'NB!
        'The KID is always stated in the oInvoice.Match_ID
        'GL-transactions are supposed to be previouslly exported in a seperate file (this is a parameter and may be altered)
        'CDV10 is added to the match_ID
        'Autogiro is also exported as OCR

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        'Dim op As Payment
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim bKeepOppdragsNr, bFoundContractNo As Boolean
        Dim sContractNo As String
        Dim sGroupNumber As String 'Used to create a false, Sentral-ID + Dagkode +
        ' Delavregningsnr + l�penummer (for grouping on the bankstatement).
        Dim bFBOsExist, bBatchStartWritten As Boolean
        Dim nFBOTransactionNo As Double
        Dim sFBOFileName As String
        Dim iStartPosFilename As Short
        Dim bLastBatchType As String
        Dim sInAccount As String
        Dim bContinue As Boolean
        Dim oPayment2 As vbBabel.Payment
        Dim eBank As vbBabel.BabelFiles.Bank
        Dim bCheckIfInvoiceIsExported As Boolean
        Dim sTempKID As String

        Try

            oFs = New Scripting.FileSystemObject

            ' new 19.12.02, due to problems running to OCR-export connescutive (without leaving BB.EXE)
            nBatchSumAmount = 0
            nBatchNoRecords = 0
            nBatchNoTransactions = 0
            dBatchLastDate = CDate(Nothing)
            dBatchFirstDate = CDate(Nothing)
            ' ----------------------------------nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0

            '----------
            bKeepOppdragsNr = True

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            sContractNo = ""
            sGroupNumber = ""
            bFBOsExist = False
            '19.06.2009 - New IF
            If sSpecial = "SANTANDER" Then
                bCheckIfInvoiceIsExported = True
            Else
                bCheckIfInvoiceIsExported = False
            End If

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord89 = False
                nExistingLines = 0
                'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 8) = "NY000089" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord89 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                If Not bFoundRecord89 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    MsgBox("The existing file," & sFilenameOut & ", is not an OCR-file")
                    'UPGRADE_WARNING: Couldn't resolve default property of object WriteSantanderOCRFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    WriteSantanderOCRFile = False
                    Exit Function
                    'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nFileNoTransactions = Val(Mid(sLine, 9, 8))
                    'Subtract 1 because we have deleted the 89-line
                    nFileNoRecords = Val(Mid(sLine, 17, 8)) - 1
                    nFileSumAmount = Val(Mid(sLine, 25, 17))
                    'Kill'em all
                    oFileRead.Close()
                    'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileRead = Nothing
                    oFileWrite.Close()
                    'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                        If oBatch.FormatType <> "OCR_FBO" Then

                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBabel = True
                                            End If
                                        Else
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            bFBOsExist = True
                            Exit For
                        End If ' FormatType
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, True, True, sSpecial)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If oBatch.FormatType <> "OCR_FBO" Then

                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                bKeepOppdragsNr = False ' Construct oppdragsnr
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    ' added 07.03.2007
                                                    ' in "split-cases" it is better to find accountno for batch-level
                                                    ' here, and pass it to Wr_OCRBatchStart
                                                    sInAccount = oPayment.I_Account
                                                    bExportoBatch = True
                                                End If
                                            Else
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                bFBOsExist = True
                                Exit For
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                bKeepOppdragsNr = False
                                'Have to find the Avtale_ID when the import-format isn't OCR
                                sContractNo = ""
                                bFoundContractNo = False
                                For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If oaccount.Account = oBatch.Payments.Item(1).I_Account Then
                                                sContractNo = oaccount.ContractNo
                                                If Len(sContractNo) > 0 Then
                                                    bFoundContractNo = True
                                                    Exit For
                                                End If

                                                ' 25.09.06 Added a test also to ConvertedAccount (ref SG Finans
                                                ' Removed 09.07.2008 beacuse if one common ConvertedAccountno was set for several
                                                ' clients, then the Contractno may be collected from the wrong client !!!!!
                                            ElseIf Not EmptyString((oaccount.ConvertedAccount)) And (Trim(oaccount.ConvertedAccount) = oBatch.Payments.Item(1).I_Account) Then
                                                If sSpecial = "SGCLIENTREPORTING" Then ' added 09.07.2008
                                                    ' 17.07.200 Added a test for SG to also check for correct client in case
                                                    ' of converted accounts
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    If oBatch.Payments(1).VB_ClientNo = oClient.ClientNo Then
                                                        sContractNo = oaccount.ContractNo
                                                        If Len(sContractNo) > 0 Then
                                                            bFoundContractNo = True
                                                            Exit For
                                                        End If
                                                    End If
                                                Else
                                                    sContractNo = oaccount.ContractNo
                                                    If Len(sContractNo) > 0 Then
                                                        bFoundContractNo = True
                                                        Exit For
                                                    End If
                                                End If
                                                ' 28.02.2008
                                                ' added a test to use clientno as lookup
                                                ' must have clientno as accountno (used for SG ClientReporting)
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            ElseIf Not EmptyString(oBatch.Payments(1).VB_ClientNo) And (oBatch.Payments(1).VB_ClientNo = oClient.ClientNo And oClient.ClientNo = oaccount.Account) Then
                                                sContractNo = oaccount.ContractNo
                                                If Len(sContractNo) > 0 Then
                                                    bFoundContractNo = True
                                                    Exit For
                                                End If
                                            End If

                                            If bFoundContractNo Then
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bFoundContractNo Then
                                            Exit For
                                        End If
                                    Next oClient

                                    If bFoundContractNo Then
                                        Exit For
                                    End If
                                Next oFilesetup
                                If Not bFoundContractNo Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    Err.Raise(35023, , LRS(35023, oBatch.Payments.Item(1).I_Account, "Avtale-ID") & vbCrLf & LRS(35004, "OCR") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                    '35023: No Contract-number (Avtale-ID) stated for account: %1.
                                    'Unable to create a correct OCR-file.
                                End If
                                'Find the GroupingNo, see declaration for explanation
                                'Changed 11.09.2006 to get correct reference for NRX
                                'Old code
                                'sGroupNumber = "99" &VB6.Format(StringToDate(oBatch.DATE_Production), "DD") & "9" & PadLeft(Trim$(Str(oBatch.Index)), 5, "0")
                                'New code
                                If IsOCR(oBatch.Payments.Item(1).PayCode) Then
                                    If Len(oBatch.REF_Bank) = 11 Then
                                        'New IF - ENDIF 18.06.2009, somtimes Cargo is not present (f.x. we retrieve from the DB
                                        If Not oBatch.Cargo Is Nothing Then
                                            eBank = oBatch.Cargo.Bank
                                        ElseIf oBabel.BankByCode <> BabelFiles.Bank.No_Bank_Specified Then
                                            eBank = oBabel.BankByCode
                                        Else
                                            eBank = BabelFiles.Bank.No_Bank_Specified
                                        End If

                                        Select Case eBank 'Old code - oBatch.Cargo.Bank

                                            Case BabelFiles.Bank.DnB
                                                If Left(oBatch.REF_Bank, 3) = "050" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case BabelFiles.Bank.Nordea_NO
                                                If Left(oBatch.REF_Bank, 3) = "013" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "13" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case BabelFiles.Bank.Fellesdata
                                                If Left(oBatch.REF_Bank, 3) = "012" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "12" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case BabelFiles.Bank.Fokus_Bank, BabelFiles.Bank.Danske_Bank_NO
                                                If Left(oBatch.REF_Bank, 3) = "019" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "19" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case Else
                                                sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                        End Select
                                    Else
                                        sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                    End If
                                Else
                                    sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                End If

                                ' New by JanP 08.01.04
                                ' Do not set ContractNo here if we already have filled it
                                ' in f.ex. in TreatSpecial (ref BAMA)
                                If Len(oBatch.REF_Own) = 9 Then
                                    sContractNo = ""
                                End If
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1
                            'AUTOGIRO****************
                            If IsAutogiro((oPayment.PayCode)) Then
                                bLastBatchType = "AUTOGIRO"
                                sLine = Wr_AutogiroReturnBatchStart(oBatch, sContractNo)
                                nBatchSumAmount = 0
                                nBatchNoRecords = 1
                                nBatchNoTransactions = 0

                            Else
                                bLastBatchType = "OCR"
                                sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False, sInAccount, sSpecial)
                            End If
                            oFile.WriteLine((sLine))

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile

                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    '29.12.05 - Added test, in case of mix of Autogiro/OCR
                                    ' Will then export Autogiro in separate file(s)
                                    'AUTOGIRO****************
                                    If oPayment.ImportFormat <> BabelFiles.FileType.AutoGiro Or oBabel.Cargo.Special = "AUTOGIRO" Or IsAutogiro((oPayment.PayCode)) Then
                                        ''''Instead
                                        ''''                        If oPayment.ImportFormat <> FileType.AutoGiro Or oBabel.Cargo.Special = "AUTOGIRO" Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoPayment = True
                                                    End If
                                                Else
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    j = j + 1

                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If bCheckIfInvoiceIsExported And oInvoice.Exported Then
                                                bCheckIfInvoiceIsExported = True
                                                'Nothing to do. The invoice is previouslly exported
                                            Else
                                                ' Add to transactionno
                                                nTransactionNo = nTransactionNo + 1
                                                'AUTOGIRO****************
                                                If IsAutogiro((oPayment.PayCode)) And oBabel.Cargo.Special <> "AUTOGIRO" Then
                                                    ' oBabel.Cargo.Special = "AUTOGIRO" means that Autogiro goes to a separate file
                                                    sLine = Wr_AutogiroReturnPayOne(oPayment, oInvoice, nTransactionNo)
                                                    ' Added 22.12.06.
                                                    ' In a mix of OCR and Autogiro, we must update totals here, to be able to
                                                    ' keep correct info in endOfFile record
                                                    ' Add to Batch-totals:
                                                    nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
                                                    nBatchNoRecords = nBatchNoRecords + 2 'both 30 and 31, payment1 and 2
                                                    nBatchNoTransactions = nBatchNoTransactions + 1

                                                    oFile.WriteLine((sLine))
                                                    sLine = Wr_AutogiroReturnPayTwo(oPayment, nTransactionNo)
                                                    oFile.WriteLine((sLine))
                                                Else

                                                    'Store the original KID in a variable, and store the Match_ID as KID
                                                    sTempKID = oInvoice.Unique_Id
                                                    oInvoice.Unique_Id = oInvoice.MATCH_ID & AddCDV10digit((oInvoice.MATCH_ID))
                                                    sLine = Wr_OCRPayOne(oPayment, oInvoice, nTransactionNo, sGroupNumber, False, True, sSpecial)
                                                    oFile.WriteLine((sLine))
                                                    oInvoice.Unique_Id = sTempKID
                                                    sLine = Wr_OCRPayTwo(oPayment, oInvoice, nTransactionNo, True, True, (oBabel.Special))
                                                    oFile.WriteLine((sLine))
                                                End If
                                                oPayment.Exported = True
                                            End If
                                        End If
                                    Next oInvoice 'invoice
                                End If

                            Next oPayment ' payment

                            'AUTOGIRO****************
                            If bLastBatchType = "AUTOGIRO" Then
                                sLine = Wr_AutogiroReturnBatchEnd()
                                ' Added 22.12.06.
                                ' In a mix of OCR and Autogiro, we must update totals here, to be able to
                                ' keep correct info in endOfFile record
                                ' Add to Batch-totals:

                                ' Add to File-totals:
                                nFileSumAmount = nFileSumAmount + nBatchSumAmount
                                nFileNoRecords = nFileNoRecords + nBatchNoRecords
                                nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions
                            Else
                                sLine = Wr_OCRBatchEnd(oBatch, False)
                            End If
                            oFile.WriteLine((sLine))

                            If StringToDate((oBatch.DATE_Production)) > dProductionDate Then
                                dProductionDate = StringToDate((oBatch.DATE_Production))
                            End If

                        End If
                    Next oBatch 'batch

                End If

            Next oBabel

            'New code 07.11.2002 by Iggy
            If bFBOsExist Then
                'New 24.05.2006 - possible to split FBOs in a seperate file
                If oBabelFiles.Item(1).VB_ProfileInUse Then
                    'Find correct filesetup
                    'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For Each oFilesetup In oBabelFiles.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabelFiles.Item(1).VB_FileSetupID Then
                            Exit For
                        End If
                    Next oFilesetup
                    'Check if we shall split OCR and FBO
                    If Not oFilesetup Is Nothing Then
                        'Switch to the FilesetupOut
                        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        oFilesetup = oBabelFiles.VB_Profile.FileSetups(oFilesetup.FileSetupOut)
                        If oFilesetup.SplitOCRandFBO Then
                            'OK FBOsExist and they sahll be split in a seperate file
                            'First, write file end for the OCR-postings
                            If nFileNoRecords > 0 Then
                                sLine = Wr_OCRFileEnd(dProductionDate)
                                oFile.WriteLine((sLine))
                                oFile.Close()
                                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                oFile = Nothing
                            Else
                                oFile.Close()
                                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                oFile = Nothing
                                oFs.DeleteFile((sFilenameOut))
                            End If
                            'Then reset counters
                            nBatchSumAmount = 0
                            nBatchNoRecords = 0
                            nBatchNoTransactions = 0
                            nFileSumAmount = 0
                            nFileNoRecords = 0
                            nFileNoTransactions = 0
                            'Create the new FBO filename
                            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iStartPosFilename = InStrRev(sFilenameOut, "\", , CompareMethod.Text) + 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sFBOFileName = Left(sFilenameOut, iStartPosFilename - 1) & "FBO_" & Mid(sFilenameOut, iStartPosFilename)
                            oFile = oFs.OpenTextFile(sFBOFileName, Scripting.IOMode.ForWriting, True, 0)
                            'Write a FileStartRecord
                            bStartRecordWritten = False
                        Else
                            'bStartRecordWritten = True ' Is already set to true if a OCR-trans is written
                        End If
                    End If
                End If
                'FBOs exist, and we have to write a new Oppdrag (Batch).
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        bBatchStartWritten = False
                        nFBOTransactionNo = 0
                        If oBatch.FormatType = "OCR_FBO" Then
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                If Not bMultiFiles Then
                                    bExportoPayment = True
                                Else
                                    If sI_Account = oPayment.I_Account Then
                                        bExportoPayment = True
                                    Else
                                        bExportoPayment = False
                                    End If
                                End If
                                If bExportoPayment Then
                                    If Not bStartRecordWritten Then
                                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, True, True, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bStartRecordWritten = True
                                    End If
                                    'Write the start Batch
                                    ' Only 1 batch per account, and new
                                    '   batch for a new account.
                                    If Not bBatchStartWritten Then
                                        sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, True, sInAccount, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bBatchStartWritten = True
                                    End If
                                    nFBOTransactionNo = nFBOTransactionNo + 1
                                    sLine = Wr_FBO(oPayment, nFBOTransactionNo)
                                    oFile.WriteLine((sLine))
                                    oPayment.Exported = True
                                End If
                            Next oPayment
                            'If the start is written, write the end of the batch
                            If bBatchStartWritten Then
                                sLine = Wr_OCRBatchEnd(oBatch, True)
                                oFile.WriteLine((sLine))
                            End If
                        End If
                    Next oBatch
                Next oBabel
            End If

            If nFileNoRecords > 0 Then
                sLine = Wr_OCRFileEnd(dProductionDate)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteSantanderOCRFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSantanderOCRFile = True

    End Function
    Function WriteOCRFileAllTransactions(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByVal sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef sSpecial As String, Optional ByRef bExportGLTransactions As Boolean = True, Optional ByRef bPostAgainstObservationAccount As Boolean = False) As Object
        'All transactions to be written in 1 OCR-file, except FBO, Autogiro and GL transactions!!!
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        'Dim op As Payment
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim bKeepOppdragsNr, bFoundContractNo As Boolean
        Dim sContractNo As String
        Dim sGroupNumber As String 'Used to create a false, Sentral-ID + Dagkode +
        ' Delavregningsnr + l�penummer (for grouping on the bankstatement).
        Dim bFBOsExist, bBatchStartWritten As Boolean
        Dim nFBOTransactionNo As Double
        Dim sFBOFileName As String
        Dim iStartPosFilename As Short
        Dim bLastBatchType As String
        Dim sInAccount As String
        Dim bContinue As Boolean
        Dim oPayment2 As vbBabel.Payment
        Dim eBank As vbBabel.BabelFiles.Bank
        Dim bCheckIfInvoiceIsExported As Boolean
        Dim sObservationAccount As String = String.Empty
        Dim sNoMatchAccount As String

        'Miroslav Klose
        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim bUseDayTotals As Boolean
        Dim lCounter, lArrayCounter As Integer

        Try

            oFs = New Scripting.FileSystemObject

            ' new 19.12.02, due to problems running to OCR-export connescutive (without leaving BB.EXE)
            nBatchSumAmount = 0
            nBatchNoRecords = 0
            nBatchNoTransactions = 0
            dBatchLastDate = CDate(Nothing)
            dBatchFirstDate = CDate(Nothing)
            ' ----------------------------------nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0

            '----------
            bKeepOppdragsNr = True

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            sContractNo = ""
            sGroupNumber = ""
            bFBOsExist = False
            '19.06.2009 - New IF
            'XNET - 26.04.2012 - Added Fjordkraft
            If sSpecial = "SANTANDER" Or sSpecial = "FJORDKRAFT" Or sSpecial = "PROCASSO" Or sSpecial = "TILPREDATOR" Then
                bCheckIfInvoiceIsExported = True
            Else
                bCheckIfInvoiceIsExported = False
            End If

            '***********************************************************
            'New code 15.06.2010 - To use DayTotals
            'All code regarding this change in this function is marked with Der Torsch�tze Miroslav Klose
            bUseDayTotals = False

            '07.10.2015 - Implemented the new way to populate DayTotals
            If oBabelFiles.Count > 0 Then
                If oBabelFiles(1).VB_ProfileInUse Then
                    If oBabelFiles(1).VB_Profile.ManMatchFromERP Then
                        bUseDayTotals = True
                    End If
                End If
            End If

            'Old code
            'XNET - 26.04.2012 - Added Fjordkraft
            'If sSpecial = "AKTIV_KAPITAL" Or sSpecial = "FJORDKRAFT" Or sSpecial = "CONECTO_FAS" Then
            '    bUseDayTotals = True
            'End If

            If bUseDayTotals Then
                'New code 21.12.2006
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim Preserve aAmountArray(UBound(aAccountArray, 2))
            End If
            '***********************************************************
            'End new code


            '19.08.2015 - Removed next two lines, set in the IF above
            ''New code 21.12.2006
            'aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)
            ''Create an array equal to the client-array
            'ReDim Preserve aAmountArray(UBound(aAccountArray, 2))

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord89 = False
                nExistingLines = 0
                'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                oFile = Nothing
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 8) = "NY000089" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord89 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                oFile = Nothing
                If Not bFoundRecord89 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    MsgBox("The existing file," & sFilenameOut & ", is not an OCR-file")
                    WriteOCRFileAllTransactions = False
                    Exit Function
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nFileNoTransactions = Val(Mid(sLine, 9, 8))
                    'Subtract 1 because we have deleted the 89-line
                    nFileNoRecords = Val(Mid(sLine, 17, 8)) - 1
                    nFileSumAmount = Val(Mid(sLine, 25, 17))
                    'Kill'em all
                    oFileRead.Close()
                    oFileRead = Nothing
                    oFileWrite.Close()
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                        If oBatch.FormatType <> "OCR_FBO" Then

                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBabel = True
                                            End If
                                        Else
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                                '21.12.2009 - New for Haugaland, do not export GL-postings
                                If bExportoBabel And Not bExportGLTransactions Then
                                    bExportoBabel = False
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    Next oInvoice
                                End If
                            End If

                            If bExportoBabel Then
                                Exit For
                            End If
                        Else
                            bFBOsExist = True
                            Exit For
                        End If ' FormatType
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, True, True, sSpecial)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If oBatch.FormatType <> "OCR_FBO" Then

                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    ' XokNET 12.06.2014 Added next If, and If oPayment.ExtraD1 = "TILPREDATOR" Then, and EndIfs
                                    If sSpecial = "TILPREDATOR" Then
                                        'If oPayment.ExtraD1 = "TILPREDATOR" Then
                                        ' changed 22.10.2014
                                        If oPayment.ExtraD1 = "TILPREDATOR" Or oPayment.ExtraD1 = "TILPREDATORTELEPAY" Then
                                            bExportoBatch = True
                                        End If
                                    Else

                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    bKeepOppdragsNr = False ' Construct oppdragsnr
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        ' added 07.03.2007
                                                        ' in "split-cases" it is better to find accountno for batch-level
                                                        ' here, and pass it to Wr_OCRBatchStart
                                                        sInAccount = oPayment.I_Account
                                                        bExportoBatch = True
                                                    End If
                                                Else
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                '21.12.2009 - New for Haugaland, do not export GL-postings
                                If bExportoBatch And Not bExportGLTransactions Then
                                    bExportoBatch = False
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    Next oInvoice
                                End If
                            Else
                                bFBOsExist = True
                                Exit For
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                bKeepOppdragsNr = False
                                'Have to find the Avtale_ID when the import-format isn't OCR
                                sContractNo = ""
                                bFoundContractNo = False
                                For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If oaccount.Account = oBatch.Payments.Item(1).I_Account Then
                                                sContractNo = oaccount.ContractNo
                                                If Len(sContractNo) > 0 Then
                                                    bFoundContractNo = True
                                                    sObservationAccount = oaccount.GLResultsAccount.Trim

                                                    '15.06.2010 - New code - Miroslav Klose
                                                    If bUseDayTotals Then
                                                        'Find the correct client in the array
                                                        For lCounter = 0 To UBound(aAccountArray, 2)
                                                            If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                                lArrayCounter = lCounter
                                                                Exit For
                                                            End If
                                                        Next lCounter
                                                        Exit For
                                                    End If

                                                    Exit For
                                                End If

                                                ' 25.09.06 Added a test also to ConvertedAccount (ref SG Finans
                                                ' Removed 09.07.2008 beacuse if one common ConvertedAccountno was set for several
                                                ' clients, then the Contractno may be collected from the wrong client !!!!!
                                            ElseIf Not EmptyString((oaccount.ConvertedAccount)) And (Trim(oaccount.ConvertedAccount) = oBatch.Payments.Item(1).I_Account) Then
                                                If sSpecial = "SGCLIENTREPORTING" Then ' added 09.07.2008
                                                    ' 17.07.200 Added a test for SG to also check for correct client in case
                                                    ' of converted accounts
                                                    If oBatch.Payments(1).VB_ClientNo = oClient.ClientNo Then
                                                        sContractNo = oaccount.ContractNo
                                                        If Len(sContractNo) > 0 Then
                                                            bFoundContractNo = True
                                                            Exit For
                                                        End If
                                                    End If
                                                Else
                                                    sContractNo = oaccount.ContractNo
                                                    If Len(sContractNo) > 0 Then
                                                        bFoundContractNo = True
                                                        Exit For
                                                    End If
                                                End If
                                                ' 28.02.2008
                                                ' added a test to use clientno as lookup
                                                ' must have clientno as accountno (used for SG ClientReporting)
                                            ElseIf Not EmptyString(oBatch.Payments(1).VB_ClientNo) And (oBatch.Payments(1).VB_ClientNo = oClient.ClientNo And oClient.ClientNo = oaccount.Account) Then
                                                sContractNo = oaccount.ContractNo
                                                If Len(sContractNo) > 0 Then
                                                    bFoundContractNo = True
                                                    Exit For
                                                End If
                                            End If

                                            If bFoundContractNo Then
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bFoundContractNo Then
                                            Exit For
                                        End If
                                    Next oClient

                                    If bFoundContractNo Then
                                        Exit For
                                    End If
                                Next oFilesetup
                                If Not bFoundContractNo Then
                                    Err.Raise(35023, , LRS(35023, oBatch.Payments.Item(1).I_Account, "Avtale-ID") & vbCrLf & LRS(35004, "OCR") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                    '35023: No Contract-number (Avtale-ID) stated for account: %1.
                                    'Unable to create a correct OCR-file.
                                Else
                                    If sSpecial = "CONECTO_FAS" Then
                                        sNoMatchAccount = GetNoMatchAccount(oBabel.VB_Profile.Company_ID, oClient.Client_ID)
                                    End If
                                End If

                                'Find the GroupingNo, see declaration for explanation
                                'Changed 11.09.2006 to get correct reference for NRX
                                'Old code
                                'sGroupNumber = "99" &VB6.Format(StringToDate(oBatch.DATE_Production), "DD") & "9" & PadLeft(Trim$(Str(oBatch.Index)), 5, "0")
                                'New code
                                If IsOCR((oPayment.PayCode)) Then
                                    If Len(oBatch.REF_Bank) = 11 Then
                                        'New IF - ENDIF 18.06.2009, somtimes Cargo is not present (f.x. we retrieve from the DB
                                        If Not oBatch.Cargo Is Nothing Then
                                            eBank = oBatch.Cargo.Bank
                                        ElseIf oBabel.BankByCode <> BabelFiles.Bank.No_Bank_Specified Then
                                            eBank = oBabel.BankByCode
                                        Else
                                            eBank = BabelFiles.Bank.No_Bank_Specified
                                        End If

                                        Select Case eBank 'Old code - oBatch.Cargo.Bank

                                            Case BabelFiles.Bank.DnB
                                                If Left(oBatch.REF_Bank, 3) = "050" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case BabelFiles.Bank.Nordea_NO
                                                If Left(oBatch.REF_Bank, 3) = "013" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "13" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case BabelFiles.Bank.Fellesdata
                                                If Left(oBatch.REF_Bank, 3) = "012" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "12" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case BabelFiles.Bank.Fokus_Bank, BabelFiles.Bank.Danske_Bank_NO
                                                If Left(oBatch.REF_Bank, 3) = "019" Then
                                                    sGroupNumber = Right(oBatch.REF_Bank, 10)
                                                Else
                                                    sGroupNumber = "19" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                                End If
                                            Case Else
                                                sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                        End Select
                                    Else
                                        If sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Or sSpecial = "SERGELNORGE" Then
                                            '31.08.2022 - Added "MODHI_NORGE"
                                            '27.03.2019 - Added this IF
                                            '02.07.2020 - Added SERGELNORGE
                                            If Len(oBatch.REF_Bank) > 10 Then
                                                oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                                            ElseIf Len(oBatch.REF_Bank) < 10 Then
                                                oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                                            End If
                                            sGroupNumber = oBatch.REF_Bank
                                        Else
                                            sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                        End If
                                    End If
                                Else
                                    If sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Or sSpecial = "SERGELNORGE" Then
                                        '31.08.2022 - Added "MODHI_NORGE"
                                        '27.03.2019 - Added this IF
                                        '02.07.2020 - Added SERGELNORGE
                                        If Len(oBatch.REF_Bank) > 10 Then
                                            oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                                        ElseIf Len(oBatch.REF_Bank) < 10 Then
                                            oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                                        End If
                                        sGroupNumber = oBatch.REF_Bank
                                    Else
                                        '30.09.2015 - Changed the code below to make it unique. We Have added the Babelfile_ID as well
                                        sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(oBatch.Index.ToString & oBabel.Index.ToString, 5, "0")
                                        'Old code
                                        'sGroupNumber = "50" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")
                                    End If

                                End If

                                ' New by JanP 08.01.04
                                ' Do not set ContractNo here if we already have filled it
                                ' in f.ex. in TreatSpecial (ref BAMA)
                                If Len(oBatch.REF_Own) = 9 Then
                                    sContractNo = ""
                                End If
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1
                            If IsAutogiro((oPayment.PayCode)) Then
                                bLastBatchType = "AUTOGIRO"
                                sLine = Wr_AutogiroReturnBatchStart(oBatch, sContractNo)
                                nBatchSumAmount = 0
                                nBatchNoRecords = 1
                                nBatchNoTransactions = 0
                                oFile.WriteLine((sLine))
                            Else
                                If Not bPostAgainstObservationAccount Then
                                    bLastBatchType = "OCR"
                                    sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False, sInAccount, sSpecial)
                                    oFile.WriteLine((sLine))
                                Else
                                    'Write the OCRBatchStart for each payment later
                                End If
                            End If

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile

                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    '29.12.05 - Added test, in case of mix of Autogiro/OCR
                                    ' Will then export Autogiro in separate file(s)
                                    If oPayment.ImportFormat <> BabelFiles.FileType.AutoGiro Or oBabel.Cargo.Special = "AUTOGIRO" Or IsAutogiro((oPayment.PayCode)) Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoPayment = True
                                                    End If
                                                Else
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                '21.12.2009 - New for Haugaland, do not export GL-postings
                                If bExportoPayment And Not bExportGLTransactions Then
                                    bExportoPayment = False
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final Then
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    Next oInvoice
                                End If
                                ' XokNET 10.06.2014
                                ' Conecto Procasso needs to export two OCR-files;
                                ' - one for Procasso
                                ' - one for Predator
                                If sSpecial = "TILPREDATOR" Then
                                    If oPayment.ExtraD1 = "TILPREDATOR" Or oPayment.ExtraD1 = "TILPREDATORTELEPAY" Then
                                        bExportoPayment = True
                                    Else
                                        bExportoPayment = False
                                    End If
                                End If

                                If bExportoPayment Then
                                    j = j + 1

                                    If bPostAgainstObservationAccount Then
                                        bLastBatchType = "OCR"
                                        sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False, sObservationAccount, "PostAgainstObservationAccount")
                                        oFile.WriteLine((sLine))
                                    End If

                                    For Each oInvoice In oPayment.Invoices
                                        If Not oInvoice.MATCH_Final Then
                                            'Nothing to do. This is not a final transaction, and should therefore not be exported.
                                        ElseIf bCheckIfInvoiceIsExported And oInvoice.Exported Then
                                            bCheckIfInvoiceIsExported = True
                                            'Nothing to do. The invoice is previouslly exported
                                        Else
                                            ' Add to transactionno
                                            nTransactionNo = nTransactionNo + 1
                                            If IsAutogiro((oPayment.PayCode)) And oBabel.Cargo.Special <> "AUTOGIRO" And oBabel.Cargo.Special <> "CONECTO_FAS" Then
                                                ' oBabel.Cargo.Special = "AUTOGIRO" means that Autogiro goes to a separate file
                                                sLine = Wr_AutogiroReturnPayOne(oPayment, oInvoice, nTransactionNo)
                                                ' Added 22.12.06.
                                                ' In a mix of OCR and Autogiro, we must update totals here, to be able to
                                                ' keep correct info in endOfFile record
                                                ' Add to Batch-totals:
                                                nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
                                                nBatchNoRecords = nBatchNoRecords + 2 'both 30 and 31, payment1 and 2
                                                nBatchNoTransactions = nBatchNoTransactions + 1

                                                oFile.WriteLine((sLine))
                                                sLine = Wr_AutogiroReturnPayTwo(oPayment, nTransactionNo)
                                                oFile.WriteLine((sLine))

                                                '15.06.2010 - New code - Miroslav Klose
                                                If bUseDayTotals Then
                                                    'Add to totalamountexported
                                                    aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                End If

                                            Else

                                                '21.12.2009 - New for Haugaland, do not export GL-postings
                                                If Not bExportGLTransactions Then
                                                    bContinue = False
                                                    If oInvoice.MATCH_Final Then
                                                        If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                            bContinue = True
                                                        End If
                                                    End If
                                                Else
                                                    bContinue = True
                                                End If

                                                ' XokNET 27.08.2014
                                                If sSpecial = "TILPREDATOR" Then
                                                    If oInvoice.Extra1 <> "TILPREDATOR" Then
                                                        bContinue = False
                                                    End If
                                                End If
                                                ' XokNET 23.10.2014
                                                If sSpecial = "CONECTO" Then
                                                    If oInvoice.Exported Then
                                                        ' do not export invoices already exported to Predator
                                                        bContinue = False
                                                    End If
                                                End If

                                                If bContinue Then
                                                    ' 2008.03.14 - something wrong with special to OCR, check later.
                                                    ' Kjell har problems, must go back to office
                                                    If UCase(oBabel.VB_Profile.CompanyName) = "DELTA" Then
                                                        sSpecial = "DELTA"
                                                    End If
                                                    '09.08.2022 - Added next If
                                                    If sSpecial = "SPBK1_PORTEFOLJE" Or sSpecial = "MODHI_NORGE" Then
                                                        '31.08.2022 - Added "MODHI_NORGE"
                                                        oPayment.ToSpecialReport = True
                                                        oInvoice.ToSpecialReport = True
                                                    End If
                                                    sLine = Wr_OCRPayOne(oPayment, oInvoice, nTransactionNo, sGroupNumber, True, False, sSpecial, , , sNoMatchAccount)
                                                    oFile.WriteLine((sLine))
                                                    If sSpecial = "DELTA" Then
                                                        sLine = Wr_OCRPayTwo(oPayment, oInvoice, nTransactionNo, True, True, sSpecial)
                                                    Else
                                                        sLine = Wr_OCRPayTwo(oPayment, oInvoice, nTransactionNo, True, True, (oBabel.Special))
                                                    End If
                                                    oFile.WriteLine((sLine))
                                                    '15.06.2010 - New code - Miroslav Klose
                                                    If bUseDayTotals Then
                                                        'Add to totalamountexported
                                                        If sSpecial = "CONECTO_FAS" Then
                                                            If oPayment.MATCH_Matched = BabelFiles.MatchStatus.Matched Then
                                                                aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                            End If
                                                        Else
                                                            aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oInvoice.MON_InvoiceAmount
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            oPayment.Exported = True
                                        End If
                                    Next oInvoice 'invoice

                                    If bPostAgainstObservationAccount Then
                                        bLastBatchType = "OCR"
                                        sLine = Wr_OCRBatchEnd(oBatch, False)
                                        oFile.WriteLine((sLine))
                                    End If

                                End If

                            Next oPayment ' payment

                            If bLastBatchType = "AUTOGIRO" Then
                                sLine = Wr_AutogiroReturnBatchEnd()
                                ' Added 22.12.06.
                                ' In a mix of OCR and Autogiro, we must update totals here, to be able to
                                ' keep correct info in endOfFile record
                                ' Add to Batch-totals:

                                'Added 18.01.2010 - We (read: Mr. Skogvang I presume) had forgotten to count the 88-record itself.
                                nBatchNoRecords = nBatchNoRecords + 1 'Also count 88-recs

                                ' Add to File-totals:
                                nFileSumAmount = nFileSumAmount + nBatchSumAmount
                                nFileNoRecords = nFileNoRecords + nBatchNoRecords
                                nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions
                            Else

                                sLine = Wr_OCRBatchEnd(oBatch, False)
                            End If
                            oFile.WriteLine((sLine))

                            If StringToDate((oBatch.DATE_Production)) > dProductionDate Then
                                dProductionDate = StringToDate((oBatch.DATE_Production))
                            End If

                        End If
                    Next oBatch 'batch

                End If

            Next oBabel

            'New code 07.11.2002 by Iggy
            If bFBOsExist Then
                'New 24.05.2006 - possible to split FBOs in a seperate file
                If oBabelFiles.Item(1).VB_ProfileInUse Then
                    'Find correct filesetup
                    For Each oFilesetup In oBabelFiles.VB_Profile.FileSetups
                        If oFilesetup.FileSetup_ID = oBabelFiles.Item(1).VB_FileSetupID Then
                            Exit For
                        End If
                    Next oFilesetup
                    'Check if we shall split OCR and FBO
                    If Not oFilesetup Is Nothing Then
                        'Switch to the FilesetupOut
                        oFilesetup = oBabelFiles.VB_Profile.FileSetups(oFilesetup.FileSetupOut)
                        If oFilesetup.SplitOCRandFBO Then
                            'OK FBOsExist and they sahll be split in a seperate file
                            'First, write file end for the OCR-postings
                            If nFileNoRecords > 0 Then
                                sLine = Wr_OCRFileEnd(dProductionDate)
                                oFile.WriteLine((sLine))
                                oFile.Close()
                                oFile = Nothing
                            Else
                                oFile.Close()
                                oFile = Nothing
                                oFs.DeleteFile((sFilenameOut))
                            End If
                            'Then reset counters
                            nBatchSumAmount = 0
                            nBatchNoRecords = 0
                            nBatchNoTransactions = 0
                            nFileSumAmount = 0
                            nFileNoRecords = 0
                            nFileNoTransactions = 0
                            'Create the new FBO filename
                            iStartPosFilename = InStrRev(sFilenameOut, "\", , CompareMethod.Text) + 1
                            sFBOFileName = Left(sFilenameOut, iStartPosFilename - 1) & "FBO_" & Mid(sFilenameOut, iStartPosFilename)
                            oFile = oFs.OpenTextFile(sFBOFileName, Scripting.IOMode.ForWriting, True, 0)
                            'Write a FileStartRecord
                            bStartRecordWritten = False
                        Else
                            'bStartRecordWritten = True ' Is already set to true if a OCR-trans is written
                        End If
                    End If
                End If
                'FBOs exist, and we have to write a new Oppdrag (Batch).
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        bBatchStartWritten = False
                        nFBOTransactionNo = 0
                        If oBatch.FormatType = "OCR_FBO" Then
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                If Not bMultiFiles Then
                                    bExportoPayment = True
                                Else
                                    If sI_Account = oPayment.I_Account Then
                                        bExportoPayment = True
                                    Else
                                        bExportoPayment = False
                                    End If
                                End If
                                If bExportoPayment Then
                                    If Not bStartRecordWritten Then
                                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, True, True, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bStartRecordWritten = True
                                    End If
                                    'Write the start Batch
                                    ' Only 1 batch per account, and new
                                    '   batch for a new account.
                                    If Not bBatchStartWritten Then
                                        sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, True, sInAccount, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bBatchStartWritten = True
                                    End If
                                    nFBOTransactionNo = nFBOTransactionNo + 1
                                    sLine = Wr_FBO(oPayment, nFBOTransactionNo)
                                    oFile.WriteLine((sLine))
                                    oPayment.Exported = True
                                End If
                            Next oPayment
                            'If the start is written, write the end of the batch
                            If bBatchStartWritten Then
                                sLine = Wr_OCRBatchEnd(oBatch, True)
                                oFile.WriteLine((sLine))
                            End If
                        End If
                    Next oBatch
                Next oBabel
            End If

            'End new code 07.11.2002 by Iggy
            'Changed 17/10 by Kjell
            'This code is taken out of the For each oBabel.Babelfiles loop (at the end)
            'Changed 28/11 by Kjell
            'That was not a good idea. If there are no records to export, don't write the OCRFileEnd

            '04.12.2008 - New added next IF
            If bUseDayTotals Then
                For lCounter = 0 To UBound(aAccountArray, 2)
                    dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "Matched_BB", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                Next lCounter
            End If

            If nFileNoRecords > 0 Then
                sLine = Wr_OCRFileEnd(dProductionDate)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteOCRFileAllTransactions" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteOCRFileAllTransactions = True

    End Function
    ' XNET 03.04.2013 - find KundeenhetsID for Vegfians
    Private Function FindKundeenhetsIDVegfinans(ByVal oBabelFile As BabelFile, ByVal sClientNo As String)
        'Private Function FindKundeenhetsIDVegfinans(ByVal oBabelFile As BabelFile, ByVal sI_Account As String)
        '07.08.2018 - Changed from sI_Account to sClientNo, see calling routine for explaination
        Dim sId As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account

        sId = vbNullString
        For Each oFilesetup In oBabelFile.Batches(1).VB_Profile.FileSetups
            For Each oClient In oFilesetup.Clients
                If oClient.ClientNo.Trim = sClientNo.Trim Then
                    For Each oaccount In oClient.Accounts
                        sId = oaccount.ContractNo
                        If Len(sId) > 0 Then
                            Exit For
                        End If
                    Next oaccount
                    If Len(sId) > 0 Then
                        Exit For
                    End If
                End If
            Next oClient
            If Len(sId) > 0 Then
                Exit For
            End If
        Next oFilesetup

        FindKundeenhetsIDVegfinans = Trim$(sId)

    End Function

    Function WriteBetalingsservice_0602File(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Integer, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String, Optional ByRef bExportGLTransactions As Boolean = True) As Boolean
        'Function WriteOCRFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String, Optional ByRef bExportGLTransactions As Boolean = True) As Object
        'bFalseOCR, False = not an original OCR-file, i.e. CREMUL
        'bOCRTransactions, False = not original OCR-transaction. i.e. transaction from CREMUL which is not of type 233.

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        'Dim op As Payment
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile
        Dim bFileStartRecordWritten As Boolean
        Dim bBatchStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim bKeepOppdragsNr As Boolean
        Dim sPBSNumber As String
        Dim bFoundPBSNumber As Boolean
        Dim sContractNo As String
        Dim sGroupNumber As String 'Used to create a false, Sentral-ID + Dagkode +
        ' Delavregningsnr + l�penummer (for grouping on the bankstatement).
        Dim bFBOsExist As Boolean
        Dim nFBOTransactionNo As Double
        Dim sFBOFileName As String
        Dim iStartPosFilename As Short
        Dim bLastBatchType As String
        Dim sInAccount As String
        Dim bContinue As Boolean
        Dim oPayment2 As vbBabel.Payment
        Dim eBank As vbBabel.BabelFiles.Bank
        Dim bCheckIfInvoiceIsExported As Boolean
        Dim bUseMatch_ID As Boolean

        'Miroslav Klose
        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim bUseDayTotals As Boolean
        Dim lCounter, lArrayCounter As Integer
        '29.09.2010 - Added the use of bExportPaymentAmountForUnmatchedStructured and bContinue2 for BKK
        Dim bExportPaymentAmountForUnmatchedStructured As Boolean
        Dim bContinue2 As Boolean
        'XNET - 04.01.2011 - Added next 2 variables
        Dim bCheckClientNoAgainstInvoice As Boolean
        Dim bAllInvoicesExported As Boolean
        ' XNET 16.05.2012
        Dim oBatch2 As Batch
        Dim bExit_HBOCR As Boolean
        Dim sTransactionCode As String
        Dim nBatch042Amount As Double
        Dim lBatch042Counter As Long
        Dim lTotalNumberOfSections As Long
        Dim lTotalNumberOfRecordType42 As Long
        Dim lTotalNumberOfrecordType52 As Long
        Dim lTotalNumberOfRecordType22 As Long
        Dim nTotalAmountOfRecordType42 As Double

        Try

            ' new 19.12.02, due to problems running to OCR-export connescutive (without leaving BB.EXE)
            nBatchSumAmount = 0
            nBatchNoRecords = 0
            nBatchNoTransactions = 0
            nBatch042Amount = 0
            lBatch042Counter = 0
            lTotalNumberOfSections = 0
            lTotalNumberOfRecordType42 = 0
            lTotalNumberOfrecordType52 = 0
            lTotalNumberOfRecordType22 = 0
            nTotalAmountOfRecordType42 = 0
            dBatchLastDate = CDate(Nothing)
            dBatchFirstDate = CDate(Nothing)
            ' ----------------------------------nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0
            'XNET - 04.01.2011 - Added next default value
            bCheckClientNoAgainstInvoice = False

            oFs = New Scripting.FileSystemObject

            bAppendFile = False
            bFileStartRecordWritten = False
            bBatchStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            bUseDayTotals = False

            '07.10.2015 - Implemented the new way to populate DayTotals
            If oBabelFiles.Count > 0 Then
                If oBabelFiles(1).VB_ProfileInUse Then
                    If oBabelFiles(1).VB_Profile.ManMatchFromERP Then
                        bUseDayTotals = True
                    End If
                End If
            End If

            If bUseDayTotals Then
                'New code 21.12.2006
                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.Company_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim Preserve aAmountArray(UBound(aAccountArray, 2))
            End If

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                nExistingLines = 0
                'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                oFile = Nothing
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFs.CopyFile(sFilenameOut, sTempFilename, True)
                oFs.DeleteFile(sFilenameOut)
                oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                Do Until oFileRead.Line > nExistingLines
                    sLine = oFileRead.ReadLine
                    oFileWrite.WriteLine((sLine))
                Loop

                ' '' ''sLine = oFileRead.ReadLine
                ' '' ''nFileNoTransactions = Val(Mid(sLine, 9, 8))
                '' '' ''Subtract 1 because we have deleted the 89-line
                ' '' ''nFileNoRecords = Val(Mid(sLine, 17, 8)) - 1
                ' '' ''nFileSumAmount = Val(Mid(sLine, 25, 17))
                'Kill'em all
                oFileRead.Close()
                oFileRead = Nothing
                oFileWrite.Close()
                oFileWrite = Nothing
                oFs.DeleteFile(sTempFilename)
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments

                        If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.VB_ClientNo = sClientNo Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBabel = True
                                        End If
                                    Else
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If

                        If bExportoBabel Then
                            Exit For
                        End If

                    Next oPayment

                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bFileStartRecordWritten Then
                        bFileStartRecordWritten = True

                        'sLine = Wr_OCRFileStart(oBabel, sAdditionalID, bFalseOCR, bOCRTransactions, sSpecial, sClientNo)

                        'Data delivery start - payments information
                        '1: System identification()         2 X 001-002 Yes BS
                        '2: Data record type                3 N 003-005 Yes 002
                        '3: Data Supplier no.               8 N 006-013 Yes CVR no. of the Data Supplier.
                        '4: Subsystem                       3 X 014-016 Yes Subsystem
                        '5: Data delivery type              4 N 017-020 Yes BS 0602 (Payments information) 
                        '6: Data(delivery) specification() 10 N 021-030 Yes Identification of data delivery serial number
                        '7: Filler                         19 X 031-049 Spaces
                        '8: Date                            6 N 050-055 Yes Creation date (ddmmyy)
                        '9: Filler                         73 X 056-128 Spaces

                        'BS-002-39334216-BS5-0602-9597237001--220321

                        sLine = ""
                        sLine = "BS"
                        sLine = sLine & "002"
                        sLine = sLine & PadLeft(sAdditionalID, 8, "0")
                        sLine = sLine & "BS5" 'Should be a variable
                        sLine = sLine & "0602"
                        sLine = sLine & PadLeft(oBabel.File_id, 10, "0")
                        sLine = sLine & Space(19)
                        sLine = sLine & oBabel.DATE_Production.Substring(6, 2) & oBabel.DATE_Production.Substring(4, 2) & oBabel.DATE_Production.Substring(2, 2)
                        sLine = sLine & Space(73)
                        oFile.WriteLine((sLine))

                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments

                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            bKeepOppdragsNr = False ' Construct oppdragsnr
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                sInAccount = oPayment.I_Account
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If

                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If

                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                bFoundPBSNumber = False
                                For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If oaccount.Account = oBatch.Payments.Item(1).I_Account Or oaccount.ConvertedAccount.Trim = oBatch.Payments.Item(1).I_Account Then
                                                sPBSNumber = oClient.Division
                                                If Len(sPBSNumber) > 0 Then
                                                    bFoundPBSNumber = True

                                                    '15.06.2010 - New code - Miroslav Klose
                                                    If bUseDayTotals Then
                                                        'Find the correct client in the array
                                                        For lCounter = 0 To UBound(aAccountArray, 2)
                                                            If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                                lArrayCounter = lCounter
                                                                Exit For
                                                            End If
                                                        Next lCounter
                                                        Exit For
                                                    End If

                                                    Exit For
                                                End If

                                            End If

                                            If bFoundPBSNumber Then
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bFoundPBSNumber Then
                                            Exit For
                                        End If
                                    Next oClient

                                    If bFoundPBSNumber Then
                                        Exit For
                                    End If
                                Next oFilesetup
                                If Not bFoundPBSNumber Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    Err.Raise(35023, , LRS(35023, oBatch.Payments.Item(1).I_Account, "PBS Number") & vbCrLf & LRS(35004, "Betalingsservice 0602") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                    '35023: No Contract-number (Avtale-ID) stated for account: %1.
                                    'Unable to create a correct OCR-file.
                                    '''End If
                                End If
                            End If

                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1

                            If Not bAppendFile And Not bBatchStartRecordWritten Then
                                bBatchStartRecordWritten = True

                                'sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False, sInAccount, sSpecial)
                                'oFile.WriteLine((sLine))
                                '1: System identification            2 X 001-002 Yes BS
                                '2: Data record type                 3 N 003-005 Yes 012
                                '3: PBS no.                          8 N 006-013 Yes Creditor�s PBS number
                                '4: Section no.                      4 N 014-017 Yes 0211 (payments information � automated payments)
                                '5: Filler                           3 X 018-020 Yes 000
                                '6: Debtor group no.                 5 N 021-025 Yes Debtor group number
                                '7: Data(Supplier) identification() 15 X 026-040 Yes Creditor�s identification with the Data(Supplier)
                                '8: Filler                           9 X 041-049 Spaces
                                '9: Date                             6 N 050-055 Yes Creation date (ddmmyy)
                                '10:Filler                          73 X 056-128 Spaces

                                sLine = ""
                                sLine = "BS"
                                sLine = sLine & "012"
                                sLine = sLine & PadLeft(sPBSNumber, 8, "0")
                                sLine = sLine & "0211" 'Should be a variable
                                sLine = sLine & "000"
                                sLine = sLine & PadLeft("9999", 4, "0") '???
                                sLine = sLine & PadLeft("99999999999", 15, "0")
                                sLine = sLine & Space(9)
                                sLine = sLine & oBabel.DATE_Production.Substring(6, 2) & oBabel.DATE_Production.Substring(4, 2) & oBabel.DATE_Production.Substring(2, 2)
                                sLine = sLine & Space(73)
                                oFile.WriteLine((sLine))

                                nBatch042Amount = 0
                                lBatch042Counter = 0
                            End If

                            For Each oPayment In oBatch.Payments


                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile

                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    '1: System identification       2 X 001-002 Yes BS
                                    '2: Data record type            3 N 003-005 Yes 042
                                    '3: PBS no.                     8 N 006-013 Yes Creditor�s PBS number
                                    '4:*Transaction code            4 N 014-017 Yes 0236 (Automated payment completed)
                                    '4:*Transaction code            4 N 014-017 Yes 0237 (automated payment rejected)
                                    '4:*Transaction code            4 N 014-017 Yes 0238 (automated payment cancelled)
                                    '4:*Transaction code            4 N 014-017 Yes 0239 (Automated payment charged back/rejected disbursement)
                                    '5: Data record no.             3 N 018-020 Yes 000
                                    '6: Debtor group no.            5 N 021-025 Yes Debtor group number
                                    '7: Customer no.               15 X 026-040 Yes Debtor�s customer number with creditor()
                                    '8: Mandate no.                 9 N 041-049 Yes Mandate number
                                    '9: Date                        6 N 050-055 Yes Payment date (ddmmyy)
                                    '10:Sign code                   1 N 056-056 Yes 0 = No
                                        '1 = Collection
                                        '2 = Disbursement
                                    '11:Amount                     13 N 057-069 Yes Amount in ore (�re) without sign
                                    '12:Creditor() 's reference    30 X 070-099 Creditor�s reference for the payment
                                    '13:Filler                      4 X 100-103 Spaces
                                    'Type 0236
                                    'Type 0237
                                    'Type 0239
                                    '14:Payment date                6 N 104-109 Yes Actual payment date (ddmmyy)
                                    '15:Bookkeeping(entry) date     6 N 110-115 Yes Bookkeeping entry date (ddmmyy)
                                    '16:Payment amount             13 N 116-128 Yes Amount actually paid
                                    'Type 0238
                                    '14 Filler                     25 N 104-128 Yes '0.....0'

                                    If oPayment.PayType = "" Then
                                        sTransactionCode = "0237"
                                    ElseIf oPayment.PayType = "" Then
                                        sTransactionCode = "0238"
                                    ElseIf oPayment.PayType = "" Then
                                        sTransactionCode = "0239"
                                    Else
                                        sTransactionCode = "0236"
                                    End If

                                    sLine = ""
                                    sLine = "BS"
                                    sLine = sLine & "042"
                                    sLine = sLine & PadLeft(sPBSNumber, 8, "0")
                                    sLine = sLine & "0211" 'Should be a variable
                                    sLine = sLine & sTransactionCode
                                    sLine = sLine & "000"
                                    sLine = sLine & PadLeft("99999", 5, "0") '6: Debtor group no.
                                    sLine = sLine & PadRight("999999999999999", 15, "0")
                                    sLine = sLine & PadLeft("999999999", 9, "0")
                                    sLine = sLine & oPayment.DATE_Payment.Substring(6, 2) & oPayment.DATE_Payment.Substring(4, 2) & oPayment.DATE_Payment.Substring(2, 2)
                                    sLine = sLine & "1" '1 = Collection 2 = Disbursement
                                    sLine = sLine & PadLeft(oPayment.MON_InvoiceAmount.ToString, 13, "0")
                                    sLine = sLine & PadRight("999999999", 30, "0")
                                    sLine = sLine & Space(4)
                                    If sTransactionCode = "" Then
                                        sLine = sLine & oPayment.DATE_Value.Substring(6, 2) & oPayment.DATE_Value.Substring(4, 2) & oPayment.DATE_Value.Substring(2, 2)
                                        sLine = sLine & oPayment.DATE_Value.Substring(6, 2) & oPayment.DATE_Value.Substring(4, 2) & oPayment.DATE_Value.Substring(2, 2)
                                        sLine = sLine & PadLeft(oPayment.MON_InvoiceAmount.ToString, 13, "0")
                                    Else
                                        sLine = sLine & PadLine("0", 25, "0")
                                    End If
                                    oFile.WriteLine()

                                    nBatch042Amount = nBatch042Amount + oPayment.MON_InvoiceAmount
                                    lBatch042Counter = lBatch042Counter + 1
                                    nTotalAmountOfRecordType42 = nTotalAmountOfRecordType42 + oPayment.MON_InvoiceAmount
                                    lTotalNumberOfRecordType42 = lTotalNumberOfRecordType42 + 1

                                End If

                            Next oPayment ' payment

                            sLine = Wr_OCRBatchEnd(oBatch, False)
                            oFile.WriteLine((sLine))

                        End If

                        '1: System identification                    2 X 001-002 Yes BS
                        '2: Data record type                         3 N 003-005 Yes 092
                        '3: PBS no.                                  8 N 006-013 Yes Creditor�s PBS number
                        '4: Section no.                              4 N 014-017 Yes 0211 (Payments information � automated payments)
                        '5: Filler                                   3 N 018-020 Yes 000
                        '6: Debtor group no.                         5 N 021-025 Yes Debtor group number
                        '7: Filler                                   6 X 026-031 Spaces
                        '8: Number of data record type 042 entries  11 N 032-042 Yes Number of preceding data record type 042 entries in the section
                        '9: Amount                                  15 N 043-057 Yes Total amount from type 042 data records
                        '10:Filler                                  11 N 058-068 Yes 00000000000
                        '11:Filler                                  15 X 069-083 Spaces
                        '12:Filler                                  11 N 084-094 Yes 00000000000
                        '13:Filler                                  34 X 095-128 Spaces

                        sLine = ""
                        sLine = "BS"
                        sLine = sLine & "092"
                        sLine = sLine & PadLeft(sPBSNumber, 8, "0")
                        sLine = sLine & "0211" 'Should be a variable
                        sLine = sLine & "000"
                        sLine = sLine & PadLeft("99999", 4, "0") '???
                        sLine = sLine & Space(6)
                        sLine = sLine & PadLeft(lBatch042Counter.ToString, 11, "0")
                        sLine = sLine & PadLeft(nBatch042Amount.ToString, 15, "0")
                        sLine = sLine & PadLeft("0", 11, "0")
                        sLine = sLine & Space(15)
                        sLine = sLine & PadLeft("0", 11, "0")
                        sLine = sLine & Space(34)
                        oFile.WriteLine((sLine))

                    Next oBatch 'batch

                End If

                '1: System identification          2 X 001-002 Yes BS
                '2: Data record type               3 N 003-005 Yes 992
                '3: Data Supplier no.              8 N 006-013 Yes CVR no. of the Data Supplier.
                '4: Subsystem                      3 X 014-016 Yes Subsystem
                '5: Delivery type                  4 N 017-020 Yes BS 0602 (Payments information)
                '6: Number of sections            11 N 021-031 Yes Number of sections in the delivery
                '7: Number of record Type(42)     11 N 032-042 Yes Number of prefixed record type 042
                '8: Amount                        15 N 043-057 Yes Net amount for record type 042
                '9: Number of record Type(52)     11 N 058-068 Yes Number of prefixed record type 052
                '10:Filler                        15 N 069-083 Yes 000000000000000
                '11:Number of record Type(22)     11 N 084-094 Yes Number of prefixed record type 022
                '12:Filler                        34 N 095-128 Yes '0....0'

                sLine = ""
                sLine = "BS"
                sLine = sLine & "992"
                sLine = sLine & PadLeft(sAdditionalID, 8, "0")
                sLine = sLine & "BS5" 'Should be a variable
                sLine = sLine & "0602"
                sLine = sLine & PadLeft(lTotalNumberOfSections.ToString, 11, "0")
                sLine = sLine & PadLeft(lTotalNumberOfRecordType42.ToString, 11, "0")
                sLine = sLine & PadLeft(nTotalAmountOfRecordType42.ToString, 15, "0")
                sLine = sLine & PadLeft(lTotalNumberOfrecordType52.ToString, 11, "0")
                sLine = sLine & "000000000000000"
                sLine = sLine & PadLeft(lTotalNumberOfRecordType22.ToString, 11, "0")
                sLine = sLine & "0000000000000000000000000000000000"
                oFile.WriteLine((sLine))

            Next oBabel

            '04.12.2008 - New added next IF
            If bUseDayTotals Then
                For lCounter = 0 To UBound(aAccountArray, 2)
                    dbUpDayTotals(False, DateToString(Now), aAmountArray(lCounter), "OCR", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
                Next lCounter
            End If

            ' 22.01.2015 changed from > 0 to > 1, since startrecord creates nFileNoRecords = 1
            If lTotalNumberOfRecordType42 > 0 Then
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteOCRFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBetalingsservice_0602File = True

    End Function

End Module
