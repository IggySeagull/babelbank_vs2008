﻿Public Class bbViewer
    ' View Batch, Payment and Invoicelevel based on oBabelFiles
    ' Uses the DataGridView control
    Private oBabelFiles As vbBabel.BabelFiles
    Private iReturn As Integer
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public ReadOnly Property ReturnValue() As Short
        Get
            ReturnValue = iReturn
        End Get
    End Property
    Public Sub Show()
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment

        Dim nTotalAmount As Double
        ' 14.10.2019
        Dim nTotalBatchAmount As Double = 0
        Dim nTotalNoOfPayments As Double
        Dim frmViewBatch As New frmViewBatch
        Dim iImportFmt As vbBabel.BabelFiles.FileType
        Dim nNoOfPayments As Double
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim CheckboxColumn As DataGridViewCheckBoxColumn
        Dim lCounter As Long
        Dim iCellCounter As Integer
        Dim aColumns(,) As Object

        Try

            ' First, construct grid
            With frmViewBatch.gridBatch
                .Columns.Clear()
                .ScrollBars = ScrollBars.Both
                .AllowUserToAddRows = False
                .RowHeadersVisible = True
                .RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
                .RowHeadersWidth = 60
                .RowHeadersDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowHeadersDefaultCellStyle.SelectionForeColor = Color.Black
                .RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .BorderStyle = BorderStyle.None
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                '.AutoSize = False
                '.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                '.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None

                .ReadOnly = True
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                .TopLeftHeaderCell.Style.BackColor = Color.LightBlue

                ' then add columns
                ' always a hidden col with babelfileindex as col 0
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' and always a hidden col with batchindex as col 1
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                If oBabelFiles.Count > 0 Then
                    ' assume same format for all imported files, otherwise setup will be for the first format
                    iImportFmt = oBabelFiles.Item(1).ImportFormat
                End If
                aColumns = FindViewColumnsBatch(iImportFmt, oBabelFiles.Item(1).StatusCode)

                ' Headers, widths, etc
                For lCounter = 0 To aColumns.GetUpperBound(1)
                    txtColumn = New DataGridViewTextBoxColumn
                    txtColumn.HeaderText = aColumns(1, lCounter)
                    txtColumn.Width = aColumns(2, lCounter)
                    If aColumns(0, lCounter) = "MON_InvoiceAmount" Or _
                       aColumns(0, lCounter) = "MON_TransferredAmount" Or _
                       aColumns(0, lCounter) = "SequenceNoStart" Or _
                       aColumns(0, lCounter) = "SequenceNoEnd" Or _
                       aColumns(0, lCounter) = "NoOfPayments" Then
                        txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        txtColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                        If aColumns(0, lCounter) = "MON_InvoiceAmount" Or _
                           aColumns(0, lCounter) = "MON_TransferredAmount" Then
                            txtColumn.DefaultCellStyle.Format = "##,##0.00"
                            txtColumn.ValueType = GetType(System.Double)  ' to sort correctly
                        End If
                    End If
                    .Columns.Add(txtColumn)
                Next


            End With

            For Each oBabelFile In oBabelFiles
                If oBabelFile.StatusCode <> "02" Then
                    nTotalAmount = nTotalAmount + oBabelFile.MON_InvoiceAmount / 100
                Else
                    nTotalAmount = nTotalAmount + oBabelFile.MON_TransferredAmount / 100
                End If
                ' added 14.10.2019 
                ' if we have no amount at BabelFile level, try Batch;
                For Each oBatch In oBabelFile.Batches
                    If oBabelFile.StatusCode <> "02" Then
                        nTotalBatchAmount = nTotalBatchAmount + oBatch.MON_InvoiceAmount / 100
                    Else
                        nTotalBatchAmount = nTotalBatchAmount + oBatch.MON_TransferredAmount / 100
                    End If
                Next

                iImportFmt = oBabelFile.ImportFormat
                nTotalNoOfPayments = 0
                For Each oBatch In oBabelFile.Batches
                    ' Calculate no of payments for each batch:
                    nNoOfPayments = 0
                    For Each oPayment In oBatch.Payments
                        nNoOfPayments = nNoOfPayments + 1
                    Next 'oPayment
                    nTotalNoOfPayments = nTotalNoOfPayments + nNoOfPayments
                    ' fill grid
                    ' the setup for the grid can either have been save by the user, for each format, or
                    ' not saved, but is dependent upon formatdefaults

                    With frmViewBatch.gridBatch

                        .Rows.Add()
                        .Rows(.RowCount - 1).HeaderCell.Value = .RowCount.ToString

                        .Rows(.RowCount - 1).Cells(0).Value = oBabelFile.Index
                        .Rows(.RowCount - 1).Cells(1).Value = oBatch.Index

                        '' default to "Checked" for all lines
                        '.Rows(.RowCount - 1).Cells(2).Value = 1
                        'iCellCounter = 2  ' Start in col 3, because we have a hidden one in col 0 and 1, and checkbox in 2 !

                        iCellCounter = 1  ' Start in col 2, because we have a hidden one in col 0 and 1
                        For lCounter = 0 To aColumns.GetUpperBound(1)
                            iCellCounter = iCellCounter + 1
                            If aColumns(0, lCounter) = "NoOfPayments" Then
                                .Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(nNoOfPayments, "##,##0")
                            End If
                            If aColumns(0, lCounter) = "MON_InvoiceAmount" Then
                                '.Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oBatch.MON_InvoiceAmount / 100, "##,##0.00")
                                .Rows(.RowCount - 1).Cells(iCellCounter).Value = oBatch.MON_InvoiceAmount / 100
                            End If
                            If aColumns(0, lCounter) = "MON_TransferredAmount" Then
                                '.Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oBatch.MON_TransferredAmount / 100, "##,##0.00")
                                .Rows(.RowCount - 1).Cells(iCellCounter).Value = oBatch.MON_TransferredAmount / 100
                            End If
                            If aColumns(0, lCounter) = "FileNameIn" Then
                                If Len(oBabelFile.FilenameIn) > 45 Then  'FIX: Test hvor langt filnavn vi har plass til
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = "..." & Strings.Right(oBabelFile.FilenameIn, 45)
                                Else
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oBabelFile.FilenameIn
                                End If
                            End If
                            If aColumns(0, lCounter) = "SequenceNoStart" Then
                                .Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oBatch.SequenceNoStart, "####0")
                            End If
                            If aColumns(0, lCounter) = "SequenceNoEnd" Then
                                .Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oBatch.SequenceNoEnd, "####0")
                            End If
                            If aColumns(0, lCounter) = "Branch" Then
                                .Rows(.RowCount - 1).Cells(iCellCounter).Value = oBatch.I_Branch
                            End If

                        Next lCounter
                    End With

                    '.Col = 4
                    'If iImportFmt = Telepay Or iImportFmt = FileType.Telepay2 Then
                    '    ' show Branch
                    '    .Text = oBatch.I_Branch
                    '    'ElseIf iImportFmt = OCR Or Dirrem Or CREMUL Or AutoGiro Then
                    'Else  ' Changed 21.02.2003 JanP
                    '    ' show own account
                    '    If oBatch.Payments.Count > 0 Then
                    '        sTmp = Trim$(oBatch.Payments(1).I_Account)
                    '        If Len(sTmp) = 11 Then
                    '            .Text = Left$(sTmp, 4) + "." + Mid$(sTmp, 5, 2) + "." + Right$(sTmp, 5)
                    '        Else
                    '            .Text = sTmp
                    '        End If
                    '    Else
                    '        sTmp = vbNullString
                    '    End If
                    'End If

                Next 'oBatch
                'End If

            Next 'oBabel
            frmViewBatch.Text = LRS(40017) & " " & Trim$(Format(nTotalAmount, "##,##0.00")) & " / " & LRS(40014) & " " & Format(nTotalNoOfPayments, "##,##0")
            ' 14.10.2019
            If nTotalAmount = 0 Then
                frmViewBatch.Text = LRS(40017) & " " & Trim$(Format(nTotalBatchAmount, "##,##0.00")) & " / " & LRS(40014) & " " & Format(nTotalNoOfPayments, "##,##0")
            End If
            frmViewBatch.SetBabelFilesInViewBatch(oBabelFiles)
            frmViewBatch.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try


    End Sub
    Private Function FindViewColumnsBatch(ByVal iImportFmt As vbBabel.BabelFiles.FileType, ByVal sStatusCode As String) As Object(,)
        Dim aReturn(,) As Object
        Dim bSavedSetup As Boolean = False

        ' aReturn holds
        ' (0) CollectionName (field)
        ' (1) Headertext
        ' (2) ColumnWidth
        ' (3) Sorting, A, D or nothing

        ' if not any saved setup, then use default, based in imported format
        If Not bSavedSetup Then
            ' Always NoOfPayments, Amount, Filename
            ReDim aReturn(3, 2)
            aReturn(0, 0) = "NoOfPayments"
            aReturn(1, 0) = LRS(60059)   ' Number
            aReturn(2, 0) = "50"
            aReturn(3, 0) = "A"

            If sStatusCode = "02" Then
                ' use transferredAmount for returnfiles
                aReturn(0, 1) = "MON_TransferredAmount"
            Else
                aReturn(0, 1) = "MON_InvoiceAmount"
            End If
            aReturn(1, 1) = LRS(40021)  ' Amount
            aReturn(2, 1) = "100"
            aReturn(3, 1) = ""

            aReturn(0, 2) = "FileNameIn"
            aReturn(1, 2) = LRS(60061)  ' Filename
            aReturn(2, 2) = "500"
            aReturn(3, 2) = ""

            Select Case iImportFmt
                Case vbBabel.BabelFiles.FileType.Telepay, vbBabel.BabelFiles.FileType.Telepay2, vbBabel.BabelFiles.FileType.TelepayPlus, vbBabel.BabelFiles.FileType.TelepayTBIO
                    ' For Telepay, add Sequence from, to, companyNo, Division
                    ReDim Preserve aReturn(3, aReturn.GetUpperBound(1) + 1)
                    aReturn(0, aReturn.GetUpperBound(1)) = "SequenceNoStart"
                    aReturn(1, aReturn.GetUpperBound(1)) = LRS(60063)  ' Sekv.
                    aReturn(2, aReturn.GetUpperBound(1)) = "50"
                    aReturn(3, aReturn.GetUpperBound(1)) = ""

                    ReDim Preserve aReturn(3, aReturn.GetUpperBound(1) + 1)
                    aReturn(0, aReturn.GetUpperBound(1)) = "SequenceNoEnd"
                    aReturn(1, aReturn.GetUpperBound(1)) = LRS(60063)  ' Sekv.
                    aReturn(2, aReturn.GetUpperBound(1)) = "50"
                    aReturn(3, aReturn.GetUpperBound(1)) = ""

                    ReDim Preserve aReturn(3, aReturn.GetUpperBound(1) + 1)
                    aReturn(0, aReturn.GetUpperBound(1)) = "Branch"
                    aReturn(1, aReturn.GetUpperBound(1)) = LRS(60075)  ' Sekv.
                    aReturn(2, aReturn.GetUpperBound(1)) = "80"
                    aReturn(3, aReturn.GetUpperBound(1)) = ""


                    ' For OCR, DirRem, Autogiro add 
                    ' For Cremul, Debmul, Paymul add
                    ' For LB add

            End Select


        End If

        FindViewColumnsBatch = aReturn
    End Function
End Class
