Option Strict Off
Option Explicit On

Friend Class frmViewerReportDialog
    Inherits System.Windows.Forms.Form
    ' 0, NOBREAK          : No pagebreaks (only on full page)
    ' 1, BREAK_ON_ACCOUNT : Break on productiondate + CreditAccount
    ' 2, BREAK_ON_BATCH   : Break on productiondate + CreditAccount + iBatches
    ' 3, BREAK_ON_PAYMENT : Break on productiondate + CreditAccount + iBatches + iPayments
    ' 1, BREAK_ON_ACCOUNTONLY : CreditAccount
    Dim nLastItem As Integer
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 172, 271, 700, 271)
    End Sub
    Private Sub frmViewerReportDialog_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", 400)
        FormLRSCaptions(Me)
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = 0
        Me.Hide()
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.DialogResult = 1
        Me.Hide()
    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFilename.Text, "\") > 0 And InStr(Me.txtFilename.Text, ".") > 0 Then
            sFile = Mid(Me.txtFilename.Text, InStrRev(Me.txtFilename.Text, "\"))
        ElseIf InStr(Me.txtFilename.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFilename.Text
        Else
            sFile = ""
        End If

        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilename.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFilename.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFilename.Text = spath
            End If
        End If
    End Sub
    Private Sub chkExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExport.Click
        ShowExportControls(Me.chkExport.Checked)
    End Sub
    Public Sub ShowExportControls(ByVal bOn As Boolean)
        If bOn Then
            Me.optHTML.Visible = True
            Me.optPDF.Visible = True
            Me.optRTF.Visible = True
            Me.optTIFF.Visible = True
            Me.optTXT.Visible = True
            Me.optXLS.Visible = True
            Me.lblFilename.Visible = True
            Me.txtFilename.Visible = True
            Me.cmdFileOpen.Visible = True
            If Me.optHTML.Checked = False And Me.optPDF.Checked = False And Me.optRTF.Checked = False And _
                Me.optTIFF.Checked = False And Me.optTXT.Checked = False And Me.optXLS.Checked = False Then
                ' must mark on, set default to PDF
                Me.optPDF.Checked = True
            End If
        Else
            Me.optHTML.Visible = False
            Me.optPDF.Visible = False
            Me.optRTF.Visible = False
            Me.optTIFF.Visible = False
            Me.optTXT.Visible = False
            Me.optXLS.Visible = False
            Me.lblFilename.Visible = False
            Me.txtFilename.Visible = False
            Me.cmdFileOpen.Visible = False
        End If


    End Sub
    Private Sub chkeMail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkeMail.CheckedChanged
        If Me.chkeMail.Checked Then
            ' show lbleMail (@) and txteMail
            Me.lbleMail.Visible = True
            Me.txteMail.Visible = True
        Else
            Me.lbleMail.Visible = False
            Me.txteMail.Visible = False
        End If
    End Sub

    Private Sub chkExport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExport.CheckedChanged
        If Me.chkExport.Checked Then
            Me.optHTML.Visible = True
            Me.optPDF.Visible = True
            Me.optRTF.Visible = True
            Me.optTIFF.Visible = True
            Me.optTXT.Visible = True
            Me.optXLS.Visible = True
            Me.txtFilename.Visible = True
        Else
            Me.optHTML.Visible = True
            Me.optPDF.Visible = True
            Me.optRTF.Visible = True
            Me.optTIFF.Visible = True
            Me.optTXT.Visible = True
            Me.optXLS.Visible = True
            Me.txtFilename.Visible = True
        End If
    End Sub
End Class
