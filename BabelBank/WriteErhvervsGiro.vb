Option Strict Off
Option Explicit On
Module WriteErhvervsGiro
	
	
	Dim sLine As String ' en output-linje
	
	' Vi m� dimme noen variable for
	Private sI_EnterpriseNo As String
	'Private sTransDate As String
	'Private bDomestic As Boolean
	'Private bTBIO As Boolean
	'This variable show if the importfile we are working with is created from the
	' accounting system, or is a returnfile. The variable is set during reading the
	' BabelFile-object.
	Private bFileFromBank As Boolean
	
	'Dim nTransactionNo
	Function WriteErhvervsgiroUtbet(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sAdditionalID As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef bEuroAccount As Boolean) As Boolean
		Dim i As Double
		'Dim bFirstLine As Boolean
		Dim oBabel As BabelFile
		Dim oBatch As Batch
		Dim oPayment As Payment
		'Dim oClient As Client
		'Dim oAccount As Account
		Dim oInvoice As Invoice
		Dim oFreeText As Freetext
		'Dim oProfile As Profile
		'Dim oFileSetup As FileSetup
		'Dim op As Payment
		Dim sNewOwnref As String
		
		Dim bWriteRecord10 As Boolean
        Dim oFs As Scripting.FileSystemObject
		Dim oFile As Scripting.TextStream
		Dim sLastAgreedRate As String
		Dim sLastAccount As String
		Dim sLastCurrencyCode As String
		Dim lNumberofTransactions As Integer
		Dim lNumberofAmounts, lNumberofRecords, lTotalAmount As Integer
		Dim lCountFreetext, lRecordType As Integer
		Dim sFreetext As String
		Dim bNewRecord20 As Boolean
		
		Dim bWriteBETFOR99Later As Boolean 'Used with the sequencenumbering
		'Dim bLastBatchWasDomestic As Boolean 'Used to check if we change to/from domestic/intenational
		Dim bFirstBatch As Boolean 'True until we write the first BETFOR00
		'Dim bFirstMassTransaction As Boolean, bWriteOK As Boolean
		'Dim bCheckFilenameOut As Boolean
		'Dim iCount As Integer
		'Dim nNoOfBETFOR As Double
		Dim nSequenceNoTotal As Double
		Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
		'Dim nClientNo As Double, bFoundClient As Boolean
		'Dim xAccount As String
		'Dim bExportMassRecords As Boolean ' New 25.10.01 by janp - default: No masstrans for returnfiles
		'Dim bLastPaymentWasMass As Boolean
		'Dim sDummy As String
		
		' lag en outputfil
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            bExportoBabel = False

            bWriteRecord10 = True
            lNumberofTransactions = 0
            lNumberofRecords = 0
            lNumberofAmounts = 0
            lTotalAmount = 0
            bNewRecord20 = False

            bWriteBETFOR99Later = False
            bFirstBatch = True
            nSequenceNoTotal = 1 'new by JanP 25.10.01

            For Each oBabel In oBabelFiles
                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then
                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        'i = 0 'FIXED Moved by Janp under case 2


                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                i = i + 1

                                'If i = 1 Then
                                '    bFirstLine = True
                                'End If

                                'Get the companyno either from a property passed from BabelExport, or
                                ' from the imported file
                                If sCompanyNo <> "" Then
                                    sI_EnterpriseNo = sCompanyNo
                                Else
                                    sI_EnterpriseNo = oBatch.I_EnterpriseNo
                                End If
                                'BETFOR00
                                If bWriteRecord10 Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).MON_TransferCurrency. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).ERA_DealMadeWith. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().ERA_ExchRateAgreed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    sLine = WriteRecord10(sI_Account, bEuroAccount, oBatch.Payments(1).ERA_ExchRateAgreed, oBatch.Payments(1).ERA_DealMadeWith, oBatch.Payments(1).MON_TransferCurrency, sFilenameOut)
                                    oFile.WriteLine((sLine))
                                    bWriteRecord10 = False
                                    lNumberofRecords = lNumberofRecords + 1
                                    'New record10 for this vriables
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().ERA_ExchRateAgreed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    sLastAgreedRate = oBatch.Payments(1).ERA_ExchRateAgreed
                                    sLastAccount = sI_Account
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().MON_TransferCurrency. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    sLastCurrencyCode = oBatch.Payments(1).MON_TransferCurrency
                                End If

                                For Each oPayment In oBatch.Payments


                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then

                                        'If some of the "Grouping"-variables change,
                                        ' add a stop-record, and a new start record.
                                        If oPayment.I_Account <> sLastAccount Or oPayment.ERA_ExchRateAgreed <> CDbl(sLastAgreedRate) Or oPayment.MON_TransferCurrency <> sLastCurrencyCode Then
                                            'First Create a 99-records
                                            lNumberofRecords = lNumberofRecords + 1
                                            sLine = WriteRecord99(bEuroAccount, sI_Account, lNumberofRecords, lNumberofAmounts, lTotalAmount)
                                            oFile.WriteLine((sLine))
                                            lNumberofAmounts = 0
                                            lNumberofRecords = 0
                                            lTotalAmount = 0
                                            sLine = WriteRecord10(sI_Account, bEuroAccount, (oPayment.ERA_ExchRateAgreed), (oPayment.ERA_DealMadeWith), (oPayment.MON_TransferCurrency), sFilenameOut)
                                            oFile.WriteLine((sLine))
                                            lNumberofRecords = lNumberofRecords + 1
                                        End If

                                        'Write Adressrecord 1 only if it is a payment
                                        ' without receivers accountno.
                                        If oPayment.PayCode = "160" Then
                                            lNumberofTransactions = lNumberofTransactions + 1
                                            bNewRecord20 = True
                                            sLine = WriteRecord20(lNumberofTransactions, oPayment, (oBabel.FilenameIn))
                                            oFile.WriteLine((sLine))
                                            lNumberofRecords = lNumberofRecords + 1
                                            If Len(oPayment.E_Adr3) > 0 Then
                                                sLine = WriteRecord21(lNumberofTransactions, (oPayment.E_Adr3))
                                                oFile.WriteLine((sLine))
                                                lNumberofRecords = lNumberofRecords + 1
                                            End If
                                        End If

                                        'FIX: We have not implemented the use of recordtype 22 and 23
                                        ' The payor can use this records to state his alternative
                                        ' adress. If not used the payors adress will be set to
                                        ' payors adress on the debitaccount
                                        ' If used the code will be somewhat like this
                                        'If Len(oPayment.I_Name) > 0 Then
                                        '    sLine = WriteRecord20(lNumberofTransactions, oPayment)
                                        '    oFile.WriteLine (sLine)
                                        '    If Len(oPayment.I_Adr3) > 0 Then
                                        '        sLine = WriteRecord20(lNumberofTransactions, oPayment.I_Adr3)
                                        '        oFile.WriteLine (sLine)
                                        '    End If
                                        'End If

                                        For Each oInvoice In oPayment.Invoices
                                            'If this is the first 30 record, lNumberofTransactions
                                            ' are already counted when creating record 20.
                                            If Not bNewRecord20 Then
                                                lNumberofTransactions = lNumberofTransactions + 1
                                            Else
                                                'Next time we'll count a new transaction
                                                bNewRecord20 = False
                                            End If
                                            sLine = WriteRecord30(lNumberofTransactions, oPayment, oInvoice, sAdditionalID, (oBabel.FilenameIn))
                                            oFile.WriteLine((sLine))
                                            lNumberofRecords = lNumberofRecords + 1
                                            lNumberofAmounts = lNumberofAmounts + 1
                                            lTotalAmount = lTotalAmount + oInvoice.MON_InvoiceAmount
                                            If oPayment.PayType = "I" Then
                                                sLine = WriteRecord31(lNumberofTransactions, oPayment, oInvoice, (oBabel.FilenameIn))
                                                oFile.WriteLine((sLine))
                                                If Len(oInvoice.STATEBANK_Text) > 70 Then
                                                    sLine = WriteRecord32(lNumberofTransactions, oInvoice)
                                                    oFile.WriteLine((sLine))
                                                    lNumberofRecords = lNumberofRecords + 1
                                                End If
                                            End If


                                            lCountFreetext = 0
                                            sFreetext = ""
                                            lRecordType = 40
                                            For Each oFreeText In oInvoice.Freetexts
                                                sFreetext = sFreetext & PadRight(oFreeText.Text, 40, " ")
                                                If Len(sFreetext) > 105 Then
                                                    If lRecordType > 53 Then
                                                        'Should be a warning, not an errormessage
                                                        Err.Raise(35006, , LRS(35006, "Erhvervsgiro") & vbCrLf & LRS(35001, oBabel.FilenameIn) & vbCrLf & LRS(35002, CStr(oInvoice.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name))
                                                        'Can't write all the freetext in the %1-format.
                                                    End If
                                                    sLine = WriteTextRecord(lNumberofTransactions, lRecordType, sFreetext)
                                                    oFile.WriteLine((sLine))
                                                    lNumberofRecords = lNumberofRecords + 1
                                                    lRecordType = lRecordType + 1
                                                    If Len(sFreetext) > 105 Then
                                                        sFreetext = Mid(sFreetext, 106)
                                                    Else
                                                        sFreetext = ""
                                                    End If
                                                End If
                                            Next oFreeText
                                            If Len(sFreetext) > 0 Then
                                                If lRecordType > 53 Then
                                                    'Should be a warning, not an errormessage
                                                    Err.Raise(35006, , LRS(35006, "Erhvervsgiro") & vbCrLf & LRS(35001, oBabel.FilenameIn) & vbCrLf & LRS(35002, CStr(oInvoice.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name))
                                                    'Can't write all the freetext in the %1-format.
                                                End If
                                                sLine = WriteTextRecord(lNumberofTransactions, lRecordType, sFreetext)
                                                oFile.WriteLine((sLine))
                                                lNumberofRecords = lNumberofRecords + 1
                                            End If



                                            '                                lCountFreetext = 0
                                            '                                sFreetext = ""
                                            '                                lRecordType = 39
                                            '                                For Each oFreeText In oInvoice.Freetexts
                                            '                                    lCountFreetext = lCountFreetext + 1
                                            '                                    If lCountFreetext = 3 Then
                                            '                                        sFreetext = sFreetext & oFreeText.Text
                                            '                                        lRecordType = lRecordType + 1
                                            '                                        If lRecordType > 53 Then
                                            '                                            MsgBox "Too much freetext"
                                            '                                        End If
                                            '                                        sLine = WriteTextRecord(lNumberofTransactions, lCountFreetext, sFreetext)
                                            '                                        oFile.WriteLine (sLine)
                                            '                                        lCountFreetext = 0
                                            '                                        sFreetext = ""
                                            '                                    Else
                                            '                                        sFreetext = sFreetext & oFreeText.Text
                                            '                                        oFile.WriteLine (sLine)
                                            '                                    End If
                                            '                                Next
                                            '                                If lCountFreetext <> 0 Then
                                            '                                    lRecordType = lRecordType + 1
                                            '                                    If lRecordType > 53 Then
                                            '                                        MsgBox "Too much freetext"
                                            '                                    End If
                                            '                                    sLine = WriteTextRecord(lNumberofTransactions, lCountFreetext, sFreetext)
                                            '                                End If
                                        Next oInvoice

                                        oPayment.Exported = True

                                    End If
                                Next oPayment ' payment

                            End If

                        Next oBatch 'batch

                    End If
                End If
                lNumberofRecords = lNumberofRecords + 1
                sLine = WriteRecord99(bEuroAccount, sI_Account, lNumberofRecords, lNumberofAmounts, lTotalAmount)
                oFile.WriteLine((sLine))
                lNumberofAmounts = 0
                lNumberofRecords = 0
                lTotalAmount = 0
            Next oBabel 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteErhvervsgiroUtbet" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteErhvervsgiroUtbet = True

        Exit Function

    End Function

    Private Function WriteRecord10(ByRef sI_Account As String, ByRef bEuroAccount As Boolean, ByRef nAgreedRate As Double, ByRef sContractNo As String, ByRef sCurrencyCode As String, ByRef sFilenameOut As String) As String
        Dim sLine As String
        Dim bError As Boolean

        bError = False

        sLine = "0000000" ' Transactionnumber
        sLine = sLine & "10 " ' Recordtype + filler(1)
        If bEuroAccount Then
            sLine = sLine & "0213"
        Else
            sLine = sLine & "1199"
        End If
        sLine = sLine & "0" 'Reserved
        sLine = sLine & PadLeft(sI_Account, 10, "0") 'Debit account
        sLine = sLine & Space(5)
        sLine = sLine & VB6.Format(Now, "YYMMDD")
        sLine = sLine & Space(4) & "00" & Space(3)
        If nAgreedRate = 0 Then
            sLine = sLine & Space(13)
            If sContractNo = "" Then
                sLine = sLine & Space(16)
            Else
                Err.Raise(35007, , LRS(35007) & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                'A contractnumber for an agreed rate is stated, but no agreed rate is stated. The Program terminates.
                bError = True
            End If
        Else
            If sContractNo = "" Then
                Err.Raise(35008, , LRS(35008) & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                'An agreed rate is stated, but no corresponding contractnumber is stated. The program terminates.
                bError = True
            Else
                sLine = sLine & PadLeft(CStr(Int(nAgreedRate)), 5, "0")
                sLine = sLine & PadRight(CStr(nAgreedRate - Int(nAgreedRate)), 8, "0")
                'sLine = sLine & PadLeft(sAgreedRate, 13, "0")
                sLine = sLine & PadLeft(sContractNo, 16, "0")
            End If
        End If
        Select Case UCase(sCurrencyCode)
            Case "", "NOK" ' added 19.01.2009, because this format can be Danmark Domestic only
                sLine = sLine & "DKK"
            Case "DKK"
                sLine = sLine & "DKK"
            Case "EUR"
                sLine = sLine & "EUR"
            Case Else
                Err.Raise(35009, , LRS(35009, UCase(sCurrencyCode), "Erhvervsgiro") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                ' The currencycode " & UCase(sCurrencyCode) & " is not valid in %2."
                bError = True
        End Select
        sLine = sLine & Space(48)

        If bError Then
            WriteRecord10 = "ERROR"
        Else
            WriteRecord10 = sLine
        End If

    End Function
	
	Private Function WriteRecord20(ByRef lNumberofTransactions As Integer, ByRef oPayment As Payment, ByRef sImportFilename As String) As String
		Dim bError As Boolean
		Dim sLine As String
		
		bError = False
		sLine = PadLeft(Trim(Str(lNumberofTransactions)), 7, "0")
		sLine = sLine & "20" 'Recordtype
		sLine = sLine & " " 'Filler
		
		If Len(oPayment.E_Name) = 0 Then
            Err.Raise(35010, , LRS(35010, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oPayment.MON_InvoiceAmount / 100)) & vbCrLf & vbCrLf & LRS(10017))
			'"Receivers name is mandatory" '+ Betalers navn, adresse etc.. Program avbrytes
			bError = True
		End If
		sLine = sLine & PadRight(oPayment.E_Name, 30, " ")
		
		sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ")
		
		sLine = sLine & PadRight(oPayment.E_Adr2, 30, " ")
		
		If Not IsNumeric(oPayment.E_Zip) Then
            Err.Raise(35011, , LRS(35011, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oPayment.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
            'The Zipcode must be numeric in the format %1."
            bError = True
        End If
        If Len(oPayment.E_Zip) = 0 Then
            Err.Raise(35012, , LRS(35012, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oPayment.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
            'The zip-code is mandatory in the format %1. Program avbrytes
            bError = True
        End If
        If Len(oPayment.E_Zip) > 4 Then
            Err.Raise(35013, , LRS(35013, "4") & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oPayment.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
            'The Zipcode is too long, maximum number of digits is %1."
            bError = True
        Else
            sLine = sLine & PadLeft(oPayment.E_Zip, 4, "0")
        End If
		
		sLine = sLine & PadRight(oPayment.E_City, 15, " ")
		
		sLine = sLine & Space(6)
		
		If bError Then
			WriteRecord20 = "ERROR"
		Else
			WriteRecord20 = sLine
		End If
		
	End Function
	
	Private Function WriteRecord21(ByRef lNumberofTransactions As Integer, ByRef sAdr3 As String) As String
		Dim sLine As String
		
		sLine = PadLeft(Trim(Str(lNumberofTransactions)), 7, "0")
		sLine = sLine & "21" 'Recordtype
		sLine = sLine & " " 'Filler
		
		sLine = sLine & PadRight(sAdr3, 30, " ")
		
		sLine = sLine & Space(85)
		
		WriteRecord21 = sLine
		
	End Function
	
	Private Function WriteRecord30(ByRef lNumberofTransactions As Integer, ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sAdditionalID As String, ByRef sImportFilename As String) As String
		Dim sLine As String
		Dim bError As Boolean
		Dim sTransactionType As String
		Dim sKortartsKode As String
		
		bError = False
		
		sLine = PadLeft(Trim(Str(lNumberofTransactions)), 7, "0")
		sLine = sLine & "30" 'Recordtype
		sLine = sLine & " " 'Filler
		sTransactionType = bbGetPayCode((oPayment.PayCode), "ErhvervsGiro", oPayment.PayType)
		sLine = sLine & sTransactionType
		If sTransactionType = "910" Then
			'FIX: This is wrong!!!!!!
			' Has to do some tests. It may be only 15 pos, or 16 from position 3,
			' see format-description.
			'FIX: This is wrong. Does not support all "Kortarts"-codes.
			Select Case Len(oInvoice.Unique_Id)
				Case 26
					sLine = sLine & Mid(oInvoice.Unique_Id, 3, 16) ' Betaleridentification
					sKortartsKode = Left(oInvoice.Unique_Id, 2) ' Kortarts-code
					sLine = sLine & sKortartsKode
					sLine = sLine & "00" 'Tekstkode
				Case 25
					sLine = sLine & Mid(oInvoice.Unique_Id, 3, 15) ' Betaleridentification
					sKortartsKode = Left(oInvoice.Unique_Id, 2) ' Kortarts-code
					sLine = sLine & sKortartsKode
					sLine = sLine & "00" 'Tekstkode
				Case 27
					sLine = sLine & Mid(oInvoice.Unique_Id, 4, 16) ' Betaleridentification
					sKortartsKode = Left(oInvoice.Unique_Id, 1) ' Kortarts-code
					sLine = sLine & sKortartsKode
					sLine = sLine & Mid(oInvoice.Unique_Id, 2, 2) 'Tekstkode
				Case Else
                    Err.Raise(35014, , LRS(35014, "4") & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oPayment.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
                    bError = True
                    'Feil under generering av kortarkkode for betalerinformasjon.
                    'Error in creation of betaleridentifikasjon, kortartkode"
            End Select
        Else
            sLine = sLine & PadLeft(sAdditionalID, 14, "0") ' Betaleridentifikation
            sLine = sLine & "18"
            sLine = sLine & "0000" ' Kortarts-code + Tekstkode
        End If
        If sKortartsKode = "71" Or sKortartsKode = "73" Or sKortartsKode = "75" Then
            sLine = sLine & "0000" ' Registreringsnummer
        ElseIf sTransactionType = "630" Then
            sLine = sLine & "0000" ' Registreringsnummer
        Else
            sLine = sLine & "1199" ' Registreringsnummer
        End If
        sLine = sLine & "00000" 'Reserved
        If sTransactionType = "630" Then
            sLine = sLine & "0000000000"
        Else
            sLine = sLine & PadLeft(oPayment.I_Account, 10, "0")
        End If
        sLine = sLine & Space(4) 'Filler
        sLine = sLine & "00" 'Reserved
        sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 12, "0") ' Amount
        If System.DateTime.FromOADate(StringToDate((oPayment.DATE_Payment)).ToOADate - Now.ToOADate) > System.DateTime.FromOADate(90) Then
            'Should be an warning, not an error.
            Err.Raise(35015, , Replace(LRS(35015, oPayment.DATE_Payment), "%2", "90") & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oPayment.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
            bError = True
            'The duedate %1 is incorrect. Maximum 90 days in the future.
            'Forfallsdatoen %1 er feil. Kan ikke v�re mer enn %2 dager frem i tid.
        Else
            sLine = sLine & Right(oPayment.DATE_Payment, 6) ' "YYMMDD")
        End If
		sLine = sLine & "0000000000" 'Reserved
		sLine = sLine & "000000000000" 'Uddatasorteringskode
		sLine = sLine & Space(5) 'Filler
		sLine = sLine & Space(20) 'Tekstlinie
		sLine = sLine & "  " 'Filler
		
		WriteRecord30 = sLine
		
	End Function
	Private Function WriteRecord31(ByRef lNumberofTransactions As Integer, ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sImportFilename As String) As String
		Dim sLine As String
		Dim bError As Boolean
		
		bError = False
		sLine = PadLeft(Trim(Str(lNumberofTransactions)), 7, "0")
		sLine = sLine & "31" 'Recordtype
		sLine = sLine & " " 'Filler
		If Len(oInvoice.STATEBANK_Code) = 1 Then
			' Short code, 1 position
			sLine = sLine & Space(4) & oInvoice.STATEBANK_Code
		ElseIf Len(oInvoice.STATEBANK_Code) = 4 Then 
			' Long code, 1 position
			sLine = sLine & oInvoice.STATEBANK_Code & " "
		Else
            Err.Raise(35016, , LRS(35016, oInvoice.STATEBANK_Code) & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oInvoice.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
            'Feil i koden til Nationalbanken. Koden %1 er ikke gyldig.
            'Wrong Statebankcode, Code " & oInvoice.StateBankCode & " is not valid."
            bError = True
        End If
        If Not Len(oInvoice.STATEBANK_DATE) = 0 Then
            If Val(Left(oInvoice.STATEBANK_DATE, 2)) > 13 Then
                Err.Raise(35017, , LRS(35017, oInvoice.STATEBANK_DATE) & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oInvoice.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
                'Wrong import/export date, " & oInvoice.STATEBANK_DATE & "is not valid."
                'Feil i Import/eksport dato. %1 er ikke en gyldig verdi.
                bError = True
            Else
                If CDbl(Right(oInvoice.STATEBANK_DATE, 4)) < 2000 Or CDbl(Right(oInvoice.STATEBANK_DATE, 4)) > 2100 Then
                    Err.Raise(35017, , LRS(35017, oInvoice.STATEBANK_DATE) & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oInvoice.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
                    'Wrong import/export date, " & oInvoice.STATEBANK_DATE & "is not valid."
                    bError = True
                End If
            End If
        End If
        sLine = sLine & PadLeft(oInvoice.STATEBANK_DATE, 6, " ")
        If Len(oPayment.E_CountryCode) <> 2 Then
            Err.Raise(35018, , LRS(35018, oPayment.E_CountryCode) & vbCrLf & LRS(35004, "Erhvervsgiro") & vbCrLf & LRS(35001, sImportFilename) & vbCrLf & LRS(35002, CStr(oInvoice.MON_InvoiceAmount / 100)) & vbCrLf & LRS(35003, oPayment.E_Name) & vbCrLf & vbCrLf & LRS(10017))
            'Wrong countrycode, " & oPayment.E_CountryCode & "is not valid."
            bError = True
        Else
            sLine = sLine & oPayment.E_CountryCode
        End If
		sLine = sLine & PadRight(oInvoice.STATEBANK_Text, 70, " ")
		sLine = sLine & Space(32)
		
		If bError Then
			WriteRecord31 = "ERROR"
		Else
			WriteRecord31 = sLine
		End If
		
	End Function
	
	Private Function WriteRecord32(ByRef lNumberofTransactions As Integer, ByRef oInvoice As Invoice) As String
		Dim sLine As String
		
		sLine = PadLeft(Trim(Str(lNumberofTransactions)), 7, "0")
		sLine = sLine & "32" 'Recordtype
		sLine = sLine & " " 'Filler
		sLine = sLine & PadRight(Mid(oInvoice.STATEBANK_Text, 71), 70, " ")
		sLine = sLine & Space(45)
		
		WriteRecord32 = sLine
		
	End Function
	
	Private Function WriteTextRecord(ByRef lNumberofTransactions As Integer, ByRef lRecordNumber As Integer, ByRef sFreetext As String) As String
		Dim sLine As String
		
		If Len(sFreetext) < 105 Then
			sFreetext = PadRight(sFreetext, 105, " ")
		End If
		
		sLine = PadLeft(Trim(Str(lNumberofTransactions)), 7, "0")
		sLine = sLine & Trim(Str(lRecordNumber)) 'Recordtype
		sLine = sLine & " " 'Filler
		
		sLine = sLine & Mid(sFreetext, 1, 35) ' Freetext
		sLine = sLine & Space(3) 'Filler
		sLine = sLine & Mid(sFreetext, 36, 35) ' Freetext
		sLine = sLine & Space(3) 'Filler
		sLine = sLine & Mid(sFreetext, 71, 35) ' Freetext
		sLine = sLine & Space(4) 'Filler
		
		WriteTextRecord = sLine
		
	End Function
	'Private Function WriteRecord99(lNumberofTransactions As Long, bEuroAccount As Boolean, sI_Account As String, lNumberofRecords As Long, lNumberofAmounts As Long, lTotalAmount As Long) As String
	Private Function WriteRecord99(ByRef bEuroAccount As Boolean, ByRef sI_Account As String, ByRef lNumberofRecords As Integer, ByRef lNumberofAmounts As Integer, ByRef lTotalAmount As Integer) As String
		Dim sLine As String
		'Dim bError As Boolean
		'Dim sTransactionType As String
		'Dim sKortartsKode As String
		
		
		sLine = "9999999" 'Constant
		sLine = sLine & "99" 'Recordtype
		sLine = sLine & " " 'Filler
		If bEuroAccount Then
			sLine = sLine & "0213"
		Else
			sLine = sLine & "1199"
		End If
		sLine = sLine & "0"
		sLine = sLine & PadLeft(sI_Account, 10, "0") 'Debit account
		sLine = sLine & PadLeft(Trim(Str(lNumberofRecords)), 10, "0") ' Number of Records
		sLine = sLine & "0000000000" 'Reserved
		sLine = sLine & PadLeft(Trim(Str(lNumberofAmounts)), 10, "0") ' Number of Amounts
		sLine = sLine & "00000000000000" 'Reserved
		sLine = sLine & PadLeft(Trim(Str(lTotalAmount)), 14, "0") ' Total amount
		sLine = sLine & Space(32)
		sLine = sLine & "00"
		sLine = sLine & Space(8)
		
		
		WriteRecord99 = sLine
		
	End Function
End Module
