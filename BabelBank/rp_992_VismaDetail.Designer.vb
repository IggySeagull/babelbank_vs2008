<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_992_VismaDetail 
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_992_VismaDetail))
        Dim OleDBDataSource1 As DataDynamics.ActiveReports.DataSources.OleDBDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtUniqueID = New DataDynamics.ActiveReports.TextBox
        Me.SubRptFreetext = New DataDynamics.ActiveReports.SubReport
        Me.txtInvoiceAmount = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.linePageHeader = New DataDynamics.ActiveReports.Line
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblValueDate = New DataDynamics.ActiveReports.Label
        Me.txtProdDate = New DataDynamics.ActiveReports.TextBox
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.lblCreditAccount = New DataDynamics.ActiveReports.Label
        Me.txtCreditAccount = New DataDynamics.ActiveReports.TextBox
        Me.txtClient = New DataDynamics.ActiveReports.TextBox
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblFilename = New DataDynamics.ActiveReports.Label
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.txtTotalNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.Line2 = New DataDynamics.ActiveReports.Line
        Me.grPaymentHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtE_Name = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferredAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr1 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr2 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr3 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Zip = New DataDynamics.ActiveReports.TextBox
        Me.txtE_City = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank2 = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank1 = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Value = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferCurrency = New DataDynamics.ActiveReports.TextBox
        Me.lblE_Account = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank2 = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank1 = New DataDynamics.ActiveReports.Label
        Me.lblDATE_Value = New DataDynamics.ActiveReports.Label
        Me.lblREF_Own = New DataDynamics.ActiveReports.Label
        Me.txtREF_Own = New DataDynamics.ActiveReports.TextBox
        Me.lblBankInfo = New DataDynamics.ActiveReports.Label
        Me.txtBankInfo = New DataDynamics.ActiveReports.TextBox
        Me.txtStatus = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grPaymentInternationalHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapegrPaymentInternationalHeader = New DataDynamics.ActiveReports.Shape
        Me.lblMON_OriginallyPaidAmount = New DataDynamics.ActiveReports.Label
        Me.lblMON_InvoiceAmount = New DataDynamics.ActiveReports.Label
        Me.lblMON_AccountAmount = New DataDynamics.ActiveReports.Label
        Me.lblExchangeRate = New DataDynamics.ActiveReports.Label
        Me.lblChargesAbroad = New DataDynamics.ActiveReports.Label
        Me.lblChargesDomestic = New DataDynamics.ActiveReports.Label
        Me.txtMON_OriginallyPaidAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_InvoiceAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_AccountAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtChargesAbroad = New DataDynamics.ActiveReports.TextBox
        Me.txtChargesDomestic = New DataDynamics.ActiveReports.TextBox
        Me.txtExchangeRate = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_OriginallyPaidCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_InvoiceCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_AccountCurrency = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentInternationalFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grFreetextHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtErrorText = New DataDynamics.ActiveReports.TextBox
        Me.grFreetextFooter = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.txtUniqueID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblValueDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtProdDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCreditAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCreditAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Zip, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_City, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Own, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Own, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBankInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBankInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMON_AccountAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblExchangeRate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblChargesAbroad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblChargesDomestic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_AccountAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChargesAbroad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChargesDomestic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExchangeRate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_OriginallyPaidCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_InvoiceCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_AccountCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtErrorText, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtUniqueID, Me.SubRptFreetext, Me.txtInvoiceAmount})
        Me.Detail.Height = 0.2604167!
        Me.Detail.Name = "Detail"
        '
        'txtUniqueID
        '
        Me.txtUniqueID.CanShrink = True
        Me.txtUniqueID.DataField = "UniqueID"
        Me.txtUniqueID.Height = 0.15!
        Me.txtUniqueID.Left = 4.3!
        Me.txtUniqueID.Name = "txtUniqueID"
        Me.txtUniqueID.Style = "font-size: 8.25pt; text-align: right"
        Me.txtUniqueID.Text = "txtUniqueID"
        Me.txtUniqueID.Top = 0.0!
        Me.txtUniqueID.Width = 2.0!
        '
        'SubRptFreetext
        '
        Me.SubRptFreetext.CloseBorder = False
        Me.SubRptFreetext.Height = 0.15!
        Me.SubRptFreetext.Left = 0.0!
        Me.SubRptFreetext.Name = "SubRptFreetext"
        Me.SubRptFreetext.Report = Nothing
        Me.SubRptFreetext.ReportName = "SubRptFreetext"
        Me.SubRptFreetext.Top = 0.0!
        Me.SubRptFreetext.Width = 6.2!
        '
        'txtInvoiceAmount
        '
        Me.txtInvoiceAmount.CanShrink = True
        Me.txtInvoiceAmount.DataField = "InvoiceAmount"
        Me.txtInvoiceAmount.Height = 0.15!
        Me.txtInvoiceAmount.Left = 2.0!
        Me.txtInvoiceAmount.Name = "txtInvoiceAmount"
        Me.txtInvoiceAmount.OutputFormat = resources.GetString("txtInvoiceAmount.OutputFormat")
        Me.txtInvoiceAmount.Style = "font-size: 8.25pt; text-align: right"
        Me.txtInvoiceAmount.Text = "txtInvoiceAmount"
        Me.txtInvoiceAmount.Top = 0.0!
        Me.txtInvoiceAmount.Width = 1.0!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.rptInfoPageHeaderDate, Me.linePageHeader})
        Me.PageHeader1.Height = 0.4479167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 0.858!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Detailreport"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 4.392!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'linePageHeader
        '
        Me.linePageHeader.Height = 0.0!
        Me.linePageHeader.Left = 0.0!
        Me.linePageHeader.LineWeight = 1.0!
        Me.linePageHeader.Name = "linePageHeader"
        Me.linePageHeader.Top = 0.375!
        Me.linePageHeader.Width = 6.5!
        Me.linePageHeader.X1 = 0.0!
        Me.linePageHeader.X2 = 6.5!
        Me.linePageHeader.Y1 = 0.375!
        Me.linePageHeader.Y2 = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grFileHeader
        '
        Me.grFileHeader.CanShrink = True
        Me.grFileHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblValueDate, Me.txtProdDate, Me.txtFilename, Me.lblCreditAccount, Me.txtCreditAccount, Me.txtClient, Me.lblClient, Me.lblFilename, Me.Line1})
        Me.grFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grFileHeader.Height = 0.9270834!
        Me.grFileHeader.Name = "grFileHeader"
        '
        'lblValueDate
        '
        Me.lblValueDate.Height = 0.17!
        Me.lblValueDate.HyperLink = Nothing
        Me.lblValueDate.Left = 0.0!
        Me.lblValueDate.Name = "lblValueDate"
        Me.lblValueDate.Style = "font-size: 10pt"
        Me.lblValueDate.Text = "lblValueDate"
        Me.lblValueDate.Top = 0.41!
        Me.lblValueDate.Width = 1.313!
        '
        'txtProdDate
        '
        Me.txtProdDate.CanShrink = True
        Me.txtProdDate.Height = 0.17!
        Me.txtProdDate.Left = 1.375!
        Me.txtProdDate.Name = "txtProdDate"
        Me.txtProdDate.OutputFormat = resources.GetString("txtProdDate.OutputFormat")
        Me.txtProdDate.Style = "font-size: 10pt"
        Me.txtProdDate.Text = "txtProdDate"
        Me.txtProdDate.Top = 0.41!
        Me.txtProdDate.Width = 1.563!
        '
        'txtFilename
        '
        Me.txtFilename.DataField = "Filename"
        Me.txtFilename.Height = 0.27!
        Me.txtFilename.Left = 1.35!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Style = "font-size: 14.25pt; font-weight: normal; ddo-char-set: 0"
        Me.txtFilename.Text = "txtFilename"
        Me.txtFilename.Top = 0.0!
        Me.txtFilename.Width = 4.925!
        '
        'lblCreditAccount
        '
        Me.lblCreditAccount.Height = 0.17!
        Me.lblCreditAccount.HyperLink = Nothing
        Me.lblCreditAccount.Left = 3.375!
        Me.lblCreditAccount.Name = "lblCreditAccount"
        Me.lblCreditAccount.Style = "font-size: 10pt"
        Me.lblCreditAccount.Text = "lblCreditAccount"
        Me.lblCreditAccount.Top = 0.5699998!
        Me.lblCreditAccount.Width = 1.0!
        '
        'txtCreditAccount
        '
        Me.txtCreditAccount.DataField = "I_Account"
        Me.txtCreditAccount.Height = 0.17!
        Me.txtCreditAccount.Left = 4.375!
        Me.txtCreditAccount.Name = "txtCreditAccount"
        Me.txtCreditAccount.Style = "font-size: 10pt; font-weight: normal"
        Me.txtCreditAccount.Text = "txtCreditAccount"
        Me.txtCreditAccount.Top = 0.5699998!
        Me.txtCreditAccount.Width = 1.9!
        '
        'txtClient
        '
        Me.txtClient.CanGrow = False
        Me.txtClient.DataField = "fldClient"
        Me.txtClient.Height = 0.17!
        Me.txtClient.Left = 4.375!
        Me.txtClient.Name = "txtClient"
        Me.txtClient.Style = "font-size: 10pt"
        Me.txtClient.Text = "txtClient"
        Me.txtClient.Top = 0.41!
        Me.txtClient.Width = 1.9!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.17!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.375!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.41!
        Me.lblClient.Width = 1.0!
        '
        'lblFilename
        '
        Me.lblFilename.Height = 0.27!
        Me.lblFilename.HyperLink = Nothing
        Me.lblFilename.Left = 0.0!
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Style = "font-size: 14.25pt; ddo-char-set: 0"
        Me.lblFilename.Text = "lblFilename"
        Me.lblFilename.Top = 0.0!
        Me.lblFilename.Width = 1.313!
        '
        'Line1
        '
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.85!
        Me.Line1.Width = 6.5!
        Me.Line1.X1 = 0.0!
        Me.Line1.X2 = 6.5!
        Me.Line1.Y1 = 0.85!
        Me.Line1.Y2 = 0.85!
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBabelFileFooter2, Me.LineBabelFileFooter1, Me.txtTotalNoOfPayments, Me.lblTotalNoOfPayments, Me.txtTotalAmount, Me.lblTotalAmount, Me.Line2})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 3.7!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 2.8!
        Me.LineBabelFileFooter2.X1 = 3.7!
        Me.LineBabelFileFooter2.X2 = 6.5!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 3.7!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 2.8!
        Me.LineBabelFileFooter1.X1 = 3.7!
        Me.LineBabelFileFooter1.X2 = 6.5!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'txtTotalNoOfPayments
        '
        Me.txtTotalNoOfPayments.CanGrow = False
        Me.txtTotalNoOfPayments.DataField = "Grouping"
        Me.txtTotalNoOfPayments.DistinctField = "Grouping"
        Me.txtTotalNoOfPayments.Height = 0.15!
        Me.txtTotalNoOfPayments.Left = 4.6!
        Me.txtTotalNoOfPayments.Name = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalNoOfPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtTotalNoOfPayments.SummaryGroup = "grFileHeader"
        Me.txtTotalNoOfPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalNoOfPayments.Text = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Top = 0.1!
        Me.txtTotalNoOfPayments.Width = 0.3!
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Height = 0.15!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 3.7!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 0.9!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.DataField = "MON_TransferredAmount"
        Me.txtTotalAmount.DistinctField = "Grouping"
        Me.txtTotalAmount.Height = 0.15!
        Me.txtTotalAmount.Left = 5.45!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtTotalAmount.SummaryGroup = "grFileHeader"
        Me.txtTotalAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.0!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.15!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.95!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.5!
        '
        'Line2
        '
        Me.Line2.Height = 0.0!
        Me.Line2.Left = 3.7!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.05!
        Me.Line2.Width = 2.8!
        Me.Line2.X1 = 3.7!
        Me.Line2.X2 = 6.5!
        Me.Line2.Y1 = 0.05!
        Me.Line2.Y2 = 0.05!
        '
        'grPaymentHeader
        '
        Me.grPaymentHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentHeader.CanShrink = True
        Me.grPaymentHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtE_Name, Me.txtMON_P_TransferredAmount, Me.txtE_Adr1, Me.txtE_Adr2, Me.txtE_Adr3, Me.txtE_Zip, Me.txtE_City, Me.txtE_Account, Me.txtREF_Bank2, Me.txtREF_Bank1, Me.txtDATE_Value, Me.txtMON_P_TransferCurrency, Me.lblE_Account, Me.lblREF_Bank2, Me.lblREF_Bank1, Me.lblDATE_Value, Me.lblREF_Own, Me.txtREF_Own, Me.lblBankInfo, Me.txtBankInfo, Me.txtStatus})
        Me.grPaymentHeader.DataField = "Grouping"
        Me.grPaymentHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentHeader.Height = 1.11875!
        Me.grPaymentHeader.Name = "grPaymentHeader"
        '
        'txtE_Name
        '
        Me.txtE_Name.CanShrink = True
        Me.txtE_Name.DataField = "E_Name"
        Me.txtE_Name.Height = 0.15!
        Me.txtE_Name.Left = 0.0!
        Me.txtE_Name.Name = "txtE_Name"
        Me.txtE_Name.Style = "font-size: 8pt"
        Me.txtE_Name.Text = "txtE_Name"
        Me.txtE_Name.Top = 0.0!
        Me.txtE_Name.Width = 2.0!
        '
        'txtMON_P_TransferredAmount
        '
        Me.txtMON_P_TransferredAmount.DataField = "MON_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Height = 0.15!
        Me.txtMON_P_TransferredAmount.Left = 5.45!
        Me.txtMON_P_TransferredAmount.Name = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.OutputFormat = resources.GetString("txtMON_P_TransferredAmount.OutputFormat")
        Me.txtMON_P_TransferredAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_P_TransferredAmount.Text = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Top = 0.28!
        Me.txtMON_P_TransferredAmount.Width = 1.0!
        '
        'txtE_Adr1
        '
        Me.txtE_Adr1.DataField = "E_Adr1"
        Me.txtE_Adr1.Height = 0.15!
        Me.txtE_Adr1.Left = 0.0!
        Me.txtE_Adr1.Name = "txtE_Adr1"
        Me.txtE_Adr1.Style = "font-size: 8pt"
        Me.txtE_Adr1.Text = "txtE_Adr1"
        Me.txtE_Adr1.Top = 0.15!
        Me.txtE_Adr1.Width = 2.0!
        '
        'txtE_Adr2
        '
        Me.txtE_Adr2.DataField = "E_Adr2"
        Me.txtE_Adr2.Height = 0.15!
        Me.txtE_Adr2.Left = 0.0!
        Me.txtE_Adr2.Name = "txtE_Adr2"
        Me.txtE_Adr2.Style = "font-size: 8pt"
        Me.txtE_Adr2.Text = "txtE_Adr2"
        Me.txtE_Adr2.Top = 0.3!
        Me.txtE_Adr2.Width = 2.0!
        '
        'txtE_Adr3
        '
        Me.txtE_Adr3.DataField = "E_Adr3"
        Me.txtE_Adr3.Height = 0.15!
        Me.txtE_Adr3.Left = 0.0!
        Me.txtE_Adr3.Name = "txtE_Adr3"
        Me.txtE_Adr3.Style = "font-size: 8pt"
        Me.txtE_Adr3.Text = "txtE_Adr3"
        Me.txtE_Adr3.Top = 0.6000001!
        Me.txtE_Adr3.Width = 2.0!
        '
        'txtE_Zip
        '
        Me.txtE_Zip.DataField = "E_Zip"
        Me.txtE_Zip.Height = 0.1875!
        Me.txtE_Zip.Left = 0.0!
        Me.txtE_Zip.Name = "txtE_Zip"
        Me.txtE_Zip.Style = "font-size: 8pt"
        Me.txtE_Zip.Text = "txtE_Zip"
        Me.txtE_Zip.Top = 0.45!
        Me.txtE_Zip.Width = 0.375!
        '
        'txtE_City
        '
        Me.txtE_City.DataField = "E_City"
        Me.txtE_City.Height = 0.15!
        Me.txtE_City.Left = 0.5!
        Me.txtE_City.Name = "txtE_City"
        Me.txtE_City.Style = "font-size: 8pt"
        Me.txtE_City.Text = "txtE_City"
        Me.txtE_City.Top = 0.45!
        Me.txtE_City.Width = 1.5!
        '
        'txtE_Account
        '
        Me.txtE_Account.DataField = "E_Account"
        Me.txtE_Account.Height = 0.15!
        Me.txtE_Account.Left = 3.25!
        Me.txtE_Account.Name = "txtE_Account"
        Me.txtE_Account.Style = "font-size: 8pt"
        Me.txtE_Account.Text = "txtE_Account"
        Me.txtE_Account.Top = 0.0!
        Me.txtE_Account.Width = 1.5!
        '
        'txtREF_Bank2
        '
        Me.txtREF_Bank2.DataField = "REF_Bank2"
        Me.txtREF_Bank2.Height = 0.15!
        Me.txtREF_Bank2.Left = 3.25!
        Me.txtREF_Bank2.Name = "txtREF_Bank2"
        Me.txtREF_Bank2.Style = "font-size: 8pt"
        Me.txtREF_Bank2.Text = "txtREF_Bank2"
        Me.txtREF_Bank2.Top = 0.15!
        Me.txtREF_Bank2.Width = 1.5!
        '
        'txtREF_Bank1
        '
        Me.txtREF_Bank1.DataField = "REF_Bank1"
        Me.txtREF_Bank1.Height = 0.15!
        Me.txtREF_Bank1.Left = 3.25!
        Me.txtREF_Bank1.Name = "txtREF_Bank1"
        Me.txtREF_Bank1.Style = "font-size: 8pt"
        Me.txtREF_Bank1.Text = "txtREF_Bank1"
        Me.txtREF_Bank1.Top = 0.3000001!
        Me.txtREF_Bank1.Width = 1.5!
        '
        'txtDATE_Value
        '
        Me.txtDATE_Value.DataField = "DATE_Value"
        Me.txtDATE_Value.Height = 0.15!
        Me.txtDATE_Value.Left = 3.25!
        Me.txtDATE_Value.Name = "txtDATE_Value"
        Me.txtDATE_Value.OutputFormat = resources.GetString("txtDATE_Value.OutputFormat")
        Me.txtDATE_Value.Style = "font-size: 8pt"
        Me.txtDATE_Value.Text = "txtDATE_Value"
        Me.txtDATE_Value.Top = 0.4500002!
        Me.txtDATE_Value.Width = 2.0!
        '
        'txtMON_P_TransferCurrency
        '
        Me.txtMON_P_TransferCurrency.DataField = "MON_TransferCurrency"
        Me.txtMON_P_TransferCurrency.Height = 0.15!
        Me.txtMON_P_TransferCurrency.Left = 5.0!
        Me.txtMON_P_TransferCurrency.Name = "txtMON_P_TransferCurrency"
        Me.txtMON_P_TransferCurrency.Style = "font-size: 8pt"
        Me.txtMON_P_TransferCurrency.Text = "PCur"
        Me.txtMON_P_TransferCurrency.Top = 0.28!
        Me.txtMON_P_TransferCurrency.Width = 0.4!
        '
        'lblE_Account
        '
        Me.lblE_Account.Height = 0.15!
        Me.lblE_Account.HyperLink = Nothing
        Me.lblE_Account.Left = 2.2!
        Me.lblE_Account.MultiLine = False
        Me.lblE_Account.Name = "lblE_Account"
        Me.lblE_Account.Style = "font-size: 8pt"
        Me.lblE_Account.Text = "lblE_Account"
        Me.lblE_Account.Top = 0.0!
        Me.lblE_Account.Width = 1.0!
        '
        'lblREF_Bank2
        '
        Me.lblREF_Bank2.Height = 0.15!
        Me.lblREF_Bank2.HyperLink = Nothing
        Me.lblREF_Bank2.Left = 2.2!
        Me.lblREF_Bank2.Name = "lblREF_Bank2"
        Me.lblREF_Bank2.Style = "font-size: 8pt"
        Me.lblREF_Bank2.Text = "lblREF_Bank2-ExecutionRef"
        Me.lblREF_Bank2.Top = 0.15!
        Me.lblREF_Bank2.Width = 1.0!
        '
        'lblREF_Bank1
        '
        Me.lblREF_Bank1.Height = 0.15!
        Me.lblREF_Bank1.HyperLink = Nothing
        Me.lblREF_Bank1.Left = 2.2!
        Me.lblREF_Bank1.Name = "lblREF_Bank1"
        Me.lblREF_Bank1.Style = "font-size: 8pt"
        Me.lblREF_Bank1.Text = "lblREF_Bank1-Giroref"
        Me.lblREF_Bank1.Top = 0.3000001!
        Me.lblREF_Bank1.Width = 1.0!
        '
        'lblDATE_Value
        '
        Me.lblDATE_Value.Height = 0.15!
        Me.lblDATE_Value.HyperLink = Nothing
        Me.lblDATE_Value.Left = 2.2!
        Me.lblDATE_Value.Name = "lblDATE_Value"
        Me.lblDATE_Value.Style = "font-size: 8pt"
        Me.lblDATE_Value.Text = "lblDATE_Value"
        Me.lblDATE_Value.Top = 0.4500002!
        Me.lblDATE_Value.Width = 1.0!
        '
        'lblREF_Own
        '
        Me.lblREF_Own.Height = 0.15!
        Me.lblREF_Own.HyperLink = Nothing
        Me.lblREF_Own.Left = 2.2!
        Me.lblREF_Own.MultiLine = False
        Me.lblREF_Own.Name = "lblREF_Own"
        Me.lblREF_Own.Style = "font-size: 8pt"
        Me.lblREF_Own.Text = "lblREF_Own"
        Me.lblREF_Own.Top = 0.6000001!
        Me.lblREF_Own.Width = 1.0!
        '
        'txtREF_Own
        '
        Me.txtREF_Own.DataField = "REF_Own"
        Me.txtREF_Own.Height = 0.15!
        Me.txtREF_Own.Left = 3.25!
        Me.txtREF_Own.Name = "txtREF_Own"
        Me.txtREF_Own.Style = "font-size: 8pt"
        Me.txtREF_Own.Text = "txtREF_Own"
        Me.txtREF_Own.Top = 0.6000001!
        Me.txtREF_Own.Width = 3.0!
        '
        'lblBankInfo
        '
        Me.lblBankInfo.Height = 0.15!
        Me.lblBankInfo.HyperLink = Nothing
        Me.lblBankInfo.Left = 2.2!
        Me.lblBankInfo.Name = "lblBankInfo"
        Me.lblBankInfo.Style = "font-size: 8pt"
        Me.lblBankInfo.Text = "lblBankInfo"
        Me.lblBankInfo.Top = 0.7500001!
        Me.lblBankInfo.Width = 1.0!
        '
        'txtBankInfo
        '
        Me.txtBankInfo.CanShrink = True
        Me.txtBankInfo.DataField = "fldBankInfo"
        Me.txtBankInfo.Height = 0.15!
        Me.txtBankInfo.Left = 3.25!
        Me.txtBankInfo.Name = "txtBankInfo"
        Me.txtBankInfo.Style = "font-size: 8pt"
        Me.txtBankInfo.Text = "txtBIC"
        Me.txtBankInfo.Top = 0.7500001!
        Me.txtBankInfo.Width = 1.5!
        '
        'txtStatus
        '
        Me.txtStatus.DataField = "fldStatus"
        Me.txtStatus.Height = 0.15!
        Me.txtStatus.Left = 4.95!
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.Style = "font-size: 8pt"
        Me.txtStatus.Text = "txtStatus"
        Me.txtStatus.Top = 0.0!
        Me.txtStatus.Width = 1.5!
        '
        'grPaymentFooter
        '
        Me.grPaymentFooter.CanShrink = True
        Me.grPaymentFooter.Height = 0.0!
        Me.grPaymentFooter.Name = "grPaymentFooter"
        '
        'grPaymentInternationalHeader
        '
        Me.grPaymentInternationalHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentInternationalHeader.CanShrink = True
        Me.grPaymentInternationalHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapegrPaymentInternationalHeader, Me.lblMON_OriginallyPaidAmount, Me.lblMON_InvoiceAmount, Me.lblMON_AccountAmount, Me.lblExchangeRate, Me.lblChargesAbroad, Me.lblChargesDomestic, Me.txtMON_OriginallyPaidAmount, Me.txtMON_InvoiceAmount, Me.txtMON_AccountAmount, Me.txtChargesAbroad, Me.txtChargesDomestic, Me.txtExchangeRate, Me.txtMON_OriginallyPaidCurrency, Me.txtMON_InvoiceCurrency, Me.txtMON_AccountCurrency})
        Me.grPaymentInternationalHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentInternationalHeader.Height = 1.176389!
        Me.grPaymentInternationalHeader.KeepTogether = True
        Me.grPaymentInternationalHeader.Name = "grPaymentInternationalHeader"
        Me.grPaymentInternationalHeader.Visible = False
        '
        'shapegrPaymentInternationalHeader
        '
        Me.shapegrPaymentInternationalHeader.Height = 1.0!
        Me.shapegrPaymentInternationalHeader.Left = 0.0!
        Me.shapegrPaymentInternationalHeader.Name = "shapegrPaymentInternationalHeader"
        Me.shapegrPaymentInternationalHeader.RoundingRadius = 9.999999!
        Me.shapegrPaymentInternationalHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapegrPaymentInternationalHeader.Top = 0.1!
        Me.shapegrPaymentInternationalHeader.Width = 5.5!
        '
        'lblMON_OriginallyPaidAmount
        '
        Me.lblMON_OriginallyPaidAmount.Height = 0.15!
        Me.lblMON_OriginallyPaidAmount.HyperLink = Nothing
        Me.lblMON_OriginallyPaidAmount.Left = 0.1!
        Me.lblMON_OriginallyPaidAmount.Name = "lblMON_OriginallyPaidAmount"
        Me.lblMON_OriginallyPaidAmount.Style = "font-size: 8pt"
        Me.lblMON_OriginallyPaidAmount.Text = "lblMON_OriginallyPaidAmount"
        Me.lblMON_OriginallyPaidAmount.Top = 0.15!
        Me.lblMON_OriginallyPaidAmount.Width = 2.0!
        '
        'lblMON_InvoiceAmount
        '
        Me.lblMON_InvoiceAmount.Height = 0.15!
        Me.lblMON_InvoiceAmount.HyperLink = Nothing
        Me.lblMON_InvoiceAmount.Left = 0.1!
        Me.lblMON_InvoiceAmount.Name = "lblMON_InvoiceAmount"
        Me.lblMON_InvoiceAmount.Style = "font-size: 8pt"
        Me.lblMON_InvoiceAmount.Text = "lblMON_InvoiceAmount"
        Me.lblMON_InvoiceAmount.Top = 0.3!
        Me.lblMON_InvoiceAmount.Width = 2.0!
        '
        'lblMON_AccountAmount
        '
        Me.lblMON_AccountAmount.Height = 0.15!
        Me.lblMON_AccountAmount.HyperLink = Nothing
        Me.lblMON_AccountAmount.Left = 0.1!
        Me.lblMON_AccountAmount.Name = "lblMON_AccountAmount"
        Me.lblMON_AccountAmount.Style = "font-size: 8pt"
        Me.lblMON_AccountAmount.Text = "lblMON_AccountAmount"
        Me.lblMON_AccountAmount.Top = 0.45!
        Me.lblMON_AccountAmount.Width = 2.0!
        '
        'lblExchangeRate
        '
        Me.lblExchangeRate.Height = 0.15!
        Me.lblExchangeRate.HyperLink = Nothing
        Me.lblExchangeRate.Left = 0.1!
        Me.lblExchangeRate.Name = "lblExchangeRate"
        Me.lblExchangeRate.Style = "font-size: 8pt"
        Me.lblExchangeRate.Text = "lblExchangeRate"
        Me.lblExchangeRate.Top = 0.6000001!
        Me.lblExchangeRate.Width = 2.0!
        '
        'lblChargesAbroad
        '
        Me.lblChargesAbroad.Height = 0.15!
        Me.lblChargesAbroad.HyperLink = Nothing
        Me.lblChargesAbroad.Left = 0.1!
        Me.lblChargesAbroad.Name = "lblChargesAbroad"
        Me.lblChargesAbroad.Style = "font-size: 8pt"
        Me.lblChargesAbroad.Text = "lblChargesAbroad"
        Me.lblChargesAbroad.Top = 0.75!
        Me.lblChargesAbroad.Width = 2.0!
        '
        'lblChargesDomestic
        '
        Me.lblChargesDomestic.Height = 0.15!
        Me.lblChargesDomestic.HyperLink = Nothing
        Me.lblChargesDomestic.Left = 0.1!
        Me.lblChargesDomestic.Name = "lblChargesDomestic"
        Me.lblChargesDomestic.Style = "font-size: 8pt"
        Me.lblChargesDomestic.Text = "lblChargesDomestic"
        Me.lblChargesDomestic.Top = 0.9!
        Me.lblChargesDomestic.Width = 2.0!
        '
        'txtMON_OriginallyPaidAmount
        '
        Me.txtMON_OriginallyPaidAmount.DataField = "MON_OriginallyPaidAmount"
        Me.txtMON_OriginallyPaidAmount.Height = 0.15!
        Me.txtMON_OriginallyPaidAmount.Left = 3.5!
        Me.txtMON_OriginallyPaidAmount.Name = "txtMON_OriginallyPaidAmount"
        Me.txtMON_OriginallyPaidAmount.OutputFormat = resources.GetString("txtMON_OriginallyPaidAmount.OutputFormat")
        Me.txtMON_OriginallyPaidAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_OriginallyPaidAmount.Text = "txtMON_OriginallyPaidAmount"
        Me.txtMON_OriginallyPaidAmount.Top = 0.15!
        Me.txtMON_OriginallyPaidAmount.Width = 1.25!
        '
        'txtMON_InvoiceAmount
        '
        Me.txtMON_InvoiceAmount.DataField = "MON_InvoiceAmount"
        Me.txtMON_InvoiceAmount.Height = 0.15!
        Me.txtMON_InvoiceAmount.Left = 3.5!
        Me.txtMON_InvoiceAmount.Name = "txtMON_InvoiceAmount"
        Me.txtMON_InvoiceAmount.OutputFormat = resources.GetString("txtMON_InvoiceAmount.OutputFormat")
        Me.txtMON_InvoiceAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_InvoiceAmount.Text = "txtMON_InvoiceAmount"
        Me.txtMON_InvoiceAmount.Top = 0.3!
        Me.txtMON_InvoiceAmount.Width = 1.25!
        '
        'txtMON_AccountAmount
        '
        Me.txtMON_AccountAmount.DataField = "MON_AccountAmount"
        Me.txtMON_AccountAmount.Height = 0.15!
        Me.txtMON_AccountAmount.Left = 3.5!
        Me.txtMON_AccountAmount.Name = "txtMON_AccountAmount"
        Me.txtMON_AccountAmount.OutputFormat = resources.GetString("txtMON_AccountAmount.OutputFormat")
        Me.txtMON_AccountAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_AccountAmount.Text = "txtMON_AccountAmount"
        Me.txtMON_AccountAmount.Top = 0.45!
        Me.txtMON_AccountAmount.Width = 1.25!
        '
        'txtChargesAbroad
        '
        Me.txtChargesAbroad.DataField = "ChargesAbroad"
        Me.txtChargesAbroad.Height = 0.15!
        Me.txtChargesAbroad.Left = 3.75!
        Me.txtChargesAbroad.Name = "txtChargesAbroad"
        Me.txtChargesAbroad.OutputFormat = resources.GetString("txtChargesAbroad.OutputFormat")
        Me.txtChargesAbroad.Style = "font-size: 8pt; text-align: right"
        Me.txtChargesAbroad.Text = "txtChargesAbroad"
        Me.txtChargesAbroad.Top = 0.75!
        Me.txtChargesAbroad.Width = 1.0!
        '
        'txtChargesDomestic
        '
        Me.txtChargesDomestic.DataField = "ChargesDomestic"
        Me.txtChargesDomestic.Height = 0.15!
        Me.txtChargesDomestic.Left = 3.75!
        Me.txtChargesDomestic.Name = "txtChargesDomestic"
        Me.txtChargesDomestic.OutputFormat = resources.GetString("txtChargesDomestic.OutputFormat")
        Me.txtChargesDomestic.Style = "font-size: 8pt; text-align: right"
        Me.txtChargesDomestic.Text = "txtChargesDomestic"
        Me.txtChargesDomestic.Top = 0.9!
        Me.txtChargesDomestic.Width = 1.0!
        '
        'txtExchangeRate
        '
        Me.txtExchangeRate.DataField = "MON_LocalExchRate"
        Me.txtExchangeRate.Height = 0.15!
        Me.txtExchangeRate.Left = 3.75!
        Me.txtExchangeRate.Name = "txtExchangeRate"
        Me.txtExchangeRate.OutputFormat = resources.GetString("txtExchangeRate.OutputFormat")
        Me.txtExchangeRate.Style = "font-size: 8pt; text-align: right"
        Me.txtExchangeRate.Text = "txtExchangeRate"
        Me.txtExchangeRate.Top = 0.6000001!
        Me.txtExchangeRate.Width = 1.0!
        '
        'txtMON_OriginallyPaidCurrency
        '
        Me.txtMON_OriginallyPaidCurrency.DataField = "MON_OriginallyPaidCurrency"
        Me.txtMON_OriginallyPaidCurrency.Height = 0.15!
        Me.txtMON_OriginallyPaidCurrency.Left = 3.1!
        Me.txtMON_OriginallyPaidCurrency.Name = "txtMON_OriginallyPaidCurrency"
        Me.txtMON_OriginallyPaidCurrency.Style = "font-size: 8pt"
        Me.txtMON_OriginallyPaidCurrency.Text = "OCur"
        Me.txtMON_OriginallyPaidCurrency.Top = 0.15!
        Me.txtMON_OriginallyPaidCurrency.Width = 0.438!
        '
        'txtMON_InvoiceCurrency
        '
        Me.txtMON_InvoiceCurrency.DataField = "MON_InvoiceCurrency"
        Me.txtMON_InvoiceCurrency.Height = 0.15!
        Me.txtMON_InvoiceCurrency.Left = 3.1!
        Me.txtMON_InvoiceCurrency.Name = "txtMON_InvoiceCurrency"
        Me.txtMON_InvoiceCurrency.Style = "font-size: 8pt"
        Me.txtMON_InvoiceCurrency.Text = "ICur"
        Me.txtMON_InvoiceCurrency.Top = 0.3!
        Me.txtMON_InvoiceCurrency.Width = 0.438!
        '
        'txtMON_AccountCurrency
        '
        Me.txtMON_AccountCurrency.DataField = "MON_AccountCurrency"
        Me.txtMON_AccountCurrency.Height = 0.15!
        Me.txtMON_AccountCurrency.Left = 3.1!
        Me.txtMON_AccountCurrency.Name = "txtMON_AccountCurrency"
        Me.txtMON_AccountCurrency.Style = "font-size: 8pt"
        Me.txtMON_AccountCurrency.Text = "ACur"
        Me.txtMON_AccountCurrency.Top = 0.45!
        Me.txtMON_AccountCurrency.Width = 0.438!
        '
        'grPaymentInternationalFooter
        '
        Me.grPaymentInternationalFooter.CanShrink = True
        Me.grPaymentInternationalFooter.Height = 0.03125!
        Me.grPaymentInternationalFooter.Name = "grPaymentInternationalFooter"
        '
        'grFreetextHeader
        '
        Me.grFreetextHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grFreetextHeader.CanShrink = True
        Me.grFreetextHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtErrorText})
        Me.grFreetextHeader.Height = 0.2604167!
        Me.grFreetextHeader.Name = "grFreetextHeader"
        '
        'txtErrorText
        '
        Me.txtErrorText.CanShrink = True
        Me.txtErrorText.Height = 0.15!
        Me.txtErrorText.Left = 0.0!
        Me.txtErrorText.Name = "txtErrorText"
        Me.txtErrorText.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.txtErrorText.Text = "txtErrorText"
        Me.txtErrorText.Top = 0.0!
        Me.txtErrorText.Visible = False
        Me.txtErrorText.Width = 6.438001!
        '
        'grFreetextFooter
        '
        Me.grFreetextFooter.CanShrink = True
        Me.grFreetextFooter.Height = 0.03125!
        Me.grFreetextFooter.Name = "grFreetextFooter"
        Me.grFreetextFooter.Visible = False
        '
        'rp_992_VismaDetail
        '
        Me.MasterReport = False
        OleDBDataSource1.ConnectionString = ""
        OleDBDataSource1.SQL = "Select * from"
        Me.DataSource = OleDBDataSource1
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 6.500501!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grFileHeader)
        Me.Sections.Add(Me.grPaymentHeader)
        Me.Sections.Add(Me.grPaymentInternationalHeader)
        Me.Sections.Add(Me.grFreetextHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grFreetextFooter)
        Me.Sections.Add(Me.grPaymentInternationalFooter)
        Me.Sections.Add(Me.grPaymentFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.txtUniqueID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblValueDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtProdDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCreditAccount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCreditAccount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Zip, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_City, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Own, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Own, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBankInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBankInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMON_AccountAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblExchangeRate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblChargesAbroad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblChargesDomestic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_OriginallyPaidAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_InvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_AccountAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChargesAbroad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChargesDomestic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExchangeRate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_OriginallyPaidCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_InvoiceCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_AccountCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtErrorText, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail
    Private WithEvents txtUniqueID As DataDynamics.ActiveReports.TextBox
    Private WithEvents SubRptFreetext As DataDynamics.ActiveReports.SubReport
    Private WithEvents txtInvoiceAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Private WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents linePageHeader As DataDynamics.ActiveReports.Line
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Private WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents grFileHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Private WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Private WithEvents txtTotalNoOfPayments As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Private WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Private WithEvents grPaymentHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents txtE_Name As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_P_TransferredAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Adr1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Adr2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Adr3 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Zip As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_City As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Account As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtREF_Bank2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtREF_Bank1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDATE_Value As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_P_TransferCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblE_Account As DataDynamics.ActiveReports.Label
    Private WithEvents lblREF_Bank2 As DataDynamics.ActiveReports.Label
    Private WithEvents lblREF_Bank1 As DataDynamics.ActiveReports.Label
    Private WithEvents lblDATE_Value As DataDynamics.ActiveReports.Label
    Private WithEvents lblREF_Own As DataDynamics.ActiveReports.Label
    Private WithEvents txtREF_Own As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblBankInfo As DataDynamics.ActiveReports.Label
    Private WithEvents txtBankInfo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtStatus As DataDynamics.ActiveReports.TextBox
    Private WithEvents grPaymentFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents grPaymentInternationalHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents shapegrPaymentInternationalHeader As DataDynamics.ActiveReports.Shape
    Private WithEvents lblMON_OriginallyPaidAmount As DataDynamics.ActiveReports.Label
    Private WithEvents lblMON_InvoiceAmount As DataDynamics.ActiveReports.Label
    Private WithEvents lblMON_AccountAmount As DataDynamics.ActiveReports.Label
    Private WithEvents lblExchangeRate As DataDynamics.ActiveReports.Label
    Private WithEvents lblChargesAbroad As DataDynamics.ActiveReports.Label
    Private WithEvents lblChargesDomestic As DataDynamics.ActiveReports.Label
    Private WithEvents txtMON_OriginallyPaidAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_InvoiceAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_AccountAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtChargesAbroad As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtChargesDomestic As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtExchangeRate As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_OriginallyPaidCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_InvoiceCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_AccountCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents grPaymentInternationalFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents lblValueDate As DataDynamics.ActiveReports.Label
    Private WithEvents txtProdDate As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblCreditAccount As DataDynamics.ActiveReports.Label
    Private WithEvents txtCreditAccount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtClient As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblClient As DataDynamics.ActiveReports.Label
    Private WithEvents lblFilename As DataDynamics.ActiveReports.Label
    Private WithEvents grFreetextHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents txtErrorText As DataDynamics.ActiveReports.TextBox
    Private WithEvents grFreetextFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents Line1 As DataDynamics.ActiveReports.Line
    Private WithEvents Line2 As DataDynamics.ActiveReports.Line
End Class 
