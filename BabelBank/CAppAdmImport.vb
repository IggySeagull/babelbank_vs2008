Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("CAppAdmImport_NET.CAppAdmImport")> Public Class CAppAdmImport
	
	'Input file
	Private msEDIFilePath As String
    Private oFs As Scripting.FileSystemObject
	Private mrStreamEDI As Scripting.TextStream
	Private miCharacterSet As Short
	Private bAtEndOfStream As Boolean
	Private lSizeImportfile As Integer
	Private lCharactersReadSoFar As Integer
	Private lCharctersReadUpTo10000 As Integer
	Private sFormatIn As String
	
	'Input Objects
	Private nodValidCurrencyCodes As MSXML2.IXMLDOMElement
	
	'Output object
	Private mrXmlDoc As New MSXML2.DOMDocument40
	Private msXmlOutputPath As String
	
	'Mapping object
    Private mrEDISettingsImport As New vbBabel.CEdiSettingsImport 'CEdiSettingsImport
	
	' Special-characters in edi-file
	Private sSegmentSep As String
	Private sEscapeChar As String
	Private sDecimalSep As String
	Private sGroupSep As String
	Private sElementSep As String
	Private lSegmentLength As Integer
	
	'Private variables
	Private bTransmittedFromFDByRJE As Boolean
    Private sSpecial As String
    Private sLastSegment As String
	'Private bGetSegmentForTheFirstTime As Boolean
	
    'Events
	Public Event HeadingsRead(ByVal domHeaderDoc As MSXML2.DOMDocument40)
	Public Event ProgressUpdated(ByVal dblProgressPercentage As Double)
	
	Private miCurGroup As Short
    Private eBank As vbBabel.BabelFiles.Bank

    '********* START PROPERTY SETTINGS ***********************
    Public ReadOnly Property XMLDoc() As MSXML2.DOMDocument40
        Get
            XMLDoc = mrXmlDoc
        End Get
    End Property
    Public ReadOnly Property LastSegment() As String
        Get
            LastSegment = sLastSegment
        End Get
    End Property
    Public ReadOnly Property EDIFilePath() As String
        Get
            EDIFilePath = msEDIFilePath
        End Get
    End Property
    Public WriteOnly Property TransmittedFromFDByRJE() As Boolean
        Set(ByVal Value As Boolean)
            bTransmittedFromFDByRJE = Value
        End Set
    End Property
    Public WriteOnly Property FormatIn() As String
        Set(ByVal Value As String)
            sFormatIn = Value
        End Set
    End Property
    Public WriteOnly Property BankByCode() As vbBabel.BabelFiles.Bank
        Set(ByVal Value As vbBabel.BabelFiles.Bank)
            'UPGRADE_WARNING: Couldn't resolve default property of object NewVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            eBank = Val(Value)
        End Set
    End Property
    '********* END PROPERTY SETTINGS ***********************

    'UPGRADE_WARNING: ParamArray aSegmentInfo was changed from ByRef to ByVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="93C6A0DC-8C99-429A-8696-35FC4DCEFCCC"'
    Friend Function AddXMLElement(ByRef rParent As MSXML2.IXMLDOMElement, ByRef sName As String, ByRef lGroupNo As Integer, ByRef iGroupIndex As Double, ByVal ParamArray aSegmentInfo() As Object) As MSXML2.IXMLDOMElement
        ' 13.05.2020 changed from ByRef iGroupIndex As Short (to double)
        Dim newEle As MSXML2.IXMLDOMElement
        Dim nodTagGroup As MSXML2.IXMLDOMElement
        Dim nodTag As MSXML2.IXMLDOMElement
        Dim childNode As MSXML2.IXMLDOMText
        Dim iCount As Short
        Dim sOldTagGroupNo As String

        sOldTagGroupNo = "-1"
        newEle = mrXmlDoc.createElement(sName)
        'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        rParent.appendChild(newEle)
        If UBound(aSegmentInfo) > -1 Then
            For iCount = 0 To UBound(aSegmentInfo(0), 2)
                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(0)(0, iCount). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If aSegmentInfo(0)(0, iCount) <> sOldTagGroupNo Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo()(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    nodTagGroup = mrXmlDoc.createElement(aSegmentInfo(0)(0, iCount))
                    'UPGRADE_WARNING: Couldn't resolve default property of object nodTagGroup. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    newEle.appendChild(nodTagGroup)
                    'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo()(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sOldTagGroupNo = aSegmentInfo(0)(0, iCount)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo()(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nodTag = mrXmlDoc.createElement(aSegmentInfo(0)(1, iCount))
                'UPGRADE_WARNING: Couldn't resolve default property of object nodTag. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nodTagGroup.appendChild(nodTag)
                childNode = mrXmlDoc.createNode(MSXML2.tagDOMNodeType.NODE_TEXT, "", "")
                'UPGRADE_WARNING: Couldn't resolve default property of object childNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nodTag.appendChild(childNode)
                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo()(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                childNode.nodeTypedValue = aSegmentInfo(0)(2, iCount)
            Next iCount
        ElseIf lGroupNo > -1 Then
            newEle.setAttribute("group", Str(lGroupNo))
            newEle.setAttribute("index", Str(iGroupIndex))
            'newEle.nodeTypedValue = lGroupNo
        End If

        AddXMLElement = newEle

    End Function

    Private Function IsEndOfMessage() As Boolean

    End Function
    Public Function ParseFile() As Boolean
        Dim sTmp As String
        Dim sTmp2 As String
        Dim bx As Boolean
        Dim sCNTSegment As String
        Dim oEdiMsg As New CEDIMessage
        Dim elmNode As MSXML2.IXMLDOMElement
        Dim sTempElmNode As MSXML2.IXMLDOMElement
        Dim newEle As MSXML2.IXMLDOMElement
        Dim nodWrite As MSXML2.IXMLDOMElement
        Dim lGroupNo As Double ' 13.05.2020 changed from Integer
        Dim i, iGroupIndex As Double ' 13.05.2020 changed from Short
        Dim bSegmentOK As Boolean
        Dim aSegmentInfo(,) As String
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aSegmentInfo() As String
        Dim iLevelChange As Short
        Dim bCorrectNodeFound As Boolean
        Dim bCheckInterchangeHeader, bCheckFunctionalGroupHeader As Boolean
        Dim bCheckMessageHeader, bResetGroupCounters As Boolean
        Dim bFirstFunctionalGroupHeader, bFirstMessageHeader As Boolean
        Dim lSEQCounter As Integer
        Dim bInsideDOCpart As Boolean
        Dim bAutackMessage As Boolean
        Dim lCounter As Integer

        nodValidCurrencyCodes = mrEDISettingsImport.XMLDoc.selectSingleNode("//VALUES/CURCODES")

        'bx = MemoryMessage(0, "F�r import")

        lSEQCounter = 0
        bFirstFunctionalGroupHeader = True

        '*******   TEMP TEMP   *********
        Dim iGroupCount As Short
        '*******   TEMP TEMP   *********

        'UPGRADE_WARNING: Couldn't resolve default property of object mrXmlDoc. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If Not oEdiMsg.Init(Me, mrXmlDoc, mrEDISettingsImport) Then
            err.Raise(vbObjectError + 15006, , LRS(15006))
            'Error during the initialization of the EDI-class
        End If

        'sTmp = GetSegment()
        ' Set EDI-specialchars
        sTmp = GetUNASegment()
        sLastSegment = sTmp '16.08.2018 - Added this line in the entire function after running GetSegment (GetUNASegment)

        If sSpecial = "KREDINOR_K90" Then
            If InStr(1, sTmp, "UNA:+", CompareMethod.Text) > 0 Then
                sTmp = Mid(sTmp, InStr(1, sTmp, "UNA:+", CompareMethod.Text))
            Else
                Err.Raise(vbObjectError + 15007, , LRS(15007, msEDIFilePath))
                'Can't find the first segment (UNA or UNB) in the file msEDIFilePath
                'The segment is missing, or it is not an EDI-file
            End If
        End If

        If Not (Left(sTmp, 3) = "UNA" Or Left(sTmp, 3) = "UNB") Then
            Err.Raise(vbObjectError + 15007, , LRS(15007, msEDIFilePath))
            'Can't find the first segment (UNA or UNB) in the file msEDIFilePath
            'The segment is missing, or it is not an EDI-file
        End If

        If sTmp = "ERROR" Then
            Err.Raise(vbObjectError + 15008, , LRS(15008, msEDIFilePath))
            'Can't read the first segment in the file msEDIFilePath
            'It is not an valid EDI-file
        End If

        If oEdiMsg Is Nothing Then
            err.Raise(vbObjectError + 15009, , LRS(15009))
            'Can't write the header-information to the XML-object.
        End If

        If Left(sTmp, 3) = "UNB" Then
            '    oEdiMsg.AddXMLHeader "UNA:+,? '"
            oEdiMsg.AddXMLHeader("")
        Else
            oEdiMsg.AddXMLHeader(sTmp)
        End If

        bx = mrEDISettingsImport.InitStdElements(sFormatIn)

        mrEDISettingsImport.BankByCode = eBank

        bx = InitiateSeperators((mrEDISettingsImport.ElementSep), (mrEDISettingsImport.SegmentSep), (mrEDISettingsImport.GroupSep), (mrEDISettingsImport.DecimalSep), (mrEDISettingsImport.EscapeChar), CStr(mrEDISettingsImport.SegmentLength))
        If bx = False Then
            MsgBox("Error in setting of seperators. Check the mappingfile", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
        End If

        '07.07.2008 Set the local seperators
        sSegmentSep = mrEDISettingsImport.SegmentSep
        sEscapeChar = mrEDISettingsImport.EscapeChar
        sDecimalSep = mrEDISettingsImport.DecimalSep
        sGroupSep = mrEDISettingsImport.GroupSep
        sElementSep = mrEDISettingsImport.ElementSep


        bx = InitTagSettingsArray((mrEDISettingsImport.XMLTagSettings))
        If bx = False Then
            MsgBox("Error in the initiation of the element-description. Check Tagsettings.XML", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical, "PARSE ERROR")
        End If

        If Not mrEDISettingsImport.setGroupMaxRep() Then
            err.Raise(vbObjectError + 15010, , LRS(15010))
            'Error during initiation of the Group-repetition
        End If

        'Read the UNB segment in the CREMUL-file
        If Not Left(sTmp, 3) = "UNB" Then
            sTmp = GetSegment()
            sLastSegment = sTmp
            If sTmp = "ERROR" Then
                Err.Raise(vbObjectError + 15011, , LRS(15011, "UNB", msEDIFilePath))
                'Fatal error: Can't read the "UNB" segment in the file msEDIFilePath
            End If
        End If

        bCheckInterchangeHeader = True

        Do While bCheckInterchangeHeader
            'First check the "UNB"-segment Interchange Header
            mrEDISettingsImport.SetCurrentElement("//GROUPS/UNB")
            bx = mrEDISettingsImport.ResetPointers
            bFirstMessageHeader = True
            'Check if UNB is in use
            If mrEDISettingsImport.CurrentElement.getAttribute("inuse") = 1 Then
                'Check if the next segment in the CREMUL-file is UNB
                If Left(sTmp, 3) <> "UNB" Then
                    ' If not UNB, check if the segment is mandatory. If so Error
                    'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If mrEDISettingsImport.CurrentElement.getAttribute("mandatory") = 1 Then
                        Err.Raise(vbObjectError + 15012, , LRS(15012, "UNB", msEDIFilePath))
                        ' "Mandatory segment "UNB" missing in file msEDIFilePath"
                    Else
                        nodWrite = AddXMLElement(mrXmlDoc.childNodes(1), "UNB", -1, -1)
                    End If
                Else
                    ' If it is a UNB, Check and save it
                    bSegmentOK = CheckSegment(sTmp, mrEDISettingsImport.CurrentElement, mrEDISettingsImport.LINCounter, mrEDISettingsImport.SEQCounter, msEDIFilePath, nodValidCurrencyCodes, True)
                    aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                    '29.05.2009 - Add the complete segment as well.
                    ReDim Preserve aSegmentInfo(2, UBound(aSegmentInfo, 2) + 1)
                    aSegmentInfo(0, UBound(aSegmentInfo, 2)) = "TGUNBTOTAL"
                    aSegmentInfo(1, UBound(aSegmentInfo, 2)) = "TUNBTOTAL"
                    aSegmentInfo(2, UBound(aSegmentInfo, 2)) = sTmp
                    nodWrite = AddXMLElement(mrXmlDoc.childNodes(1), "UNB", -1, -1, CObj(aSegmentInfo))
                    ' Store the interchange reference number
                    elmNode = nodWrite.selectSingleNode("TG050/T0020")
                    mrEDISettingsImport.InterchangeRefNo = elmNode.nodeTypedValue
                    sTmp = GetSegment() ' read the next segment
                    sLastSegment = sTmp
                    If sTmp = "ERROR" Then
                        Err.Raise(vbObjectError + 15013, , LRS(15013, "UNB", msEDIFilePath))
                        'Fatal error: Can't read the segment succeeding the "UNB"-segment in the file msEDIFilePath
                    End If
                End If
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object mrXmlDoc.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nodWrite = AddXMLElement(mrXmlDoc.childNodes(1), "UNB", -1, -1)
            End If
            bCheckFunctionalGroupHeader = True

            Do While bCheckFunctionalGroupHeader
                'Check the UNG-segment - functionalgroupheader
                mrEDISettingsImport.SetCurrentElement("//GROUPS/UNB/UNG")
                'Check if UNG is in use
                'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If mrEDISettingsImport.CurrentElement.getAttribute("inuse") = 1 Then
                    'Check if the next segment in the CREMUL-file is UNG
                    If Left(sTmp, 3) <> "UNG" Then
                        ' If not UNG, check if the segment is mandatory. If so Error
                        'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If mrEDISettingsImport.CurrentElement.getAttribute("mandatory") = 1 Then
                            Err.Raise(vbObjectError + 15012, , LRS(15012, "UNG", msEDIFilePath))
                            ' "Mandatory segment "UNG" is missing in file msEDIFilePath"
                        Else
                            nodWrite = AddXMLElement(nodWrite, "UNG", -1, -1)
                        End If
                    Else
                        ' If it is a UNG, Check and save it
                        bSegmentOK = CheckSegment(sTmp, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object mrXmlDoc.childNodes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nodWrite = AddXMLElement(mrXmlDoc.childNodes(1), "UNG", -1, -1, CObj(aSegmentInfo))
                        ' Store the functionalgroup reference number
                        elmNode = nodWrite.selectSingleNode("TG030/T0048") 'The xpath may be wrong!!!!!!!
                        'UPGRADE_WARNING: Couldn't resolve default property of object elmNode.nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        mrEDISettingsImport.FunctionalGroupRefNo = elmNode.nodeTypedValue
                        sTmp = GetSegment() ' read the next segment
                        sLastSegment = sTmp
                        If sTmp = "ERROR" Then
                            Err.Raise(vbObjectError + 15013, , LRS(15013, "UNG", msEDIFilePath))
                            'Fatal error: Can't read the segment succeeding the "UNG"-segment in the file msEDIFilePath
                        End If
                        bFirstFunctionalGroupHeader = False
                    End If
                Else
                    nodWrite = AddXMLElement(nodWrite, "UNG", -1, -1)
                End If
                bCheckMessageHeader = True

                Do While bCheckMessageHeader
                    'Check the UNH-segment
                    mrEDISettingsImport.SetCurrentElement("//GROUPS/UNB/UNG/GROUP/UNH")
                    'Check if UNB is in use
                    'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If mrEDISettingsImport.CurrentElement.getAttribute("inuse") = 1 Then
                        'Check if the next segment in the CREMUL-file is UNB

                        If Left(sTmp, 3) <> "UNH" Then
                            ' If not UNH, check if the segment is mandatory. If so Error
                            'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If mrEDISettingsImport.CurrentElement.getAttribute("mandatory") = 1 Then
                                Err.Raise(vbObjectError + 15012, , LRS(15012, "UNH", msEDIFilePath))
                                ' "Mandatory segment "UNH" is missing in file msEDIFilePath"
                            Else
                                nodWrite = AddXMLElement(nodWrite, "UNH", -1, -1)
                            End If
                        Else

                            ''11.05.2021 - Test moving to UNH no.3 in a file (test for Fjordkraft)
                            'If sTmp.Substring(4, 1) = "1" Then
                            '    Do While True
                            '        sTmp = GetSegment()
                            '        If Left(sTmp, 3) = "UNH" Then
                            '            Exit Do
                            '        ElseIf Left(sTmp, 3) = "UNZ" Then
                            '            Exit Do
                            '        End If
                            '    Loop
                            '    sTmp = sTmp
                            'End If


                            ' If it is a UNH, Check and save it
                            '19.09.2008
                            If InStr(1, sTmp, "AUTACK", CompareMethod.Text) > 0 Then
                                bAutackMessage = True
                            Else
                                bAutackMessage = False
                            End If

                            If Not bAutackMessage Then
                                bSegmentOK = CheckSegment(sTmp, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                                If bFirstMessageHeader Then
                                    nodWrite = AddXMLElement(nodWrite, "GROUP", 0, 1)
                                    'HEGEB
                                Else
                                    nodWrite = nodWrite.parentNode
                                End If
                                '29.05.2009 - Add the complete segment as well.
                                ReDim Preserve aSegmentInfo(2, UBound(aSegmentInfo, 2) + 1)
                                aSegmentInfo(0, UBound(aSegmentInfo, 2)) = "TGUNHTOTAL"
                                aSegmentInfo(1, UBound(aSegmentInfo, 2)) = "TUNHTOTAL"
                                aSegmentInfo(2, UBound(aSegmentInfo, 2)) = sTmp

                                nodWrite = AddXMLElement(nodWrite, "UNH", -1, -1, CObj(aSegmentInfo))
                                'Reset groupcounters. This is unnecessary the first time, but does do
                                ' any harm. It is necessary if we have more than 1 UNA, UNB or UNG.
                                sTempElmNode = mrEDISettingsImport.CurrentElement.parentNode
                                bResetGroupCounters = mrEDISettingsImport.SetGroupRep(CShort(Val(sTempElmNode.getAttribute("number")) + 1), True)
                                sTempElmNode = Nothing
                                ' Store the interchange reference number
                                elmNode = nodWrite.selectSingleNode("TG010/T0062")
                                mrEDISettingsImport.MessageRefNo = elmNode.nodeTypedValue
                                mrEDISettingsImport.SegmentCount = 1
                                'sTmp = GetSegment() ' read the next segment
                                bFirstMessageHeader = False
                            End If
                        End If
                    Else
                        nodWrite = AddXMLElement(nodWrite, "UNH", -1, -1)
                    End If

                    RaiseEvent HeadingsRead(mrXmlDoc)

                    mrEDISettingsImport.LINCounter = 0

                    'Do While Not IsEndOfMessage And Not mrStreamEDI.AtEndOfStream

                    'TEMPCODE due to error in file from BBS
                    bInsideDOCpart = False
                    Do While Not IsEndOfMessage And Not bAtEndOfStream
                        lGroupNo = 0
                        sTmp = GetSegment()
                        sLastSegment = sTmp
                        'New code 30.06.2009 - Character found by DnBNOR Finans.
                        'Not valid in as a valid charcter in a XML-document
                        sTmp = Replace(sTmp, Chr(27), "")

                        '19.09.2008 If Autack, just read until You reach
                        If bAutackMessage Then
                            'Read segments until CNT or UNT
                            Do While Not bAtEndOfStream Or lCounter > 100 'To be sure
                                If Left(sTmp, 3) = "CNT" Or Left(sTmp, 3) = "UNT" Then
                                    Exit Do
                                End If
                                sTmp = GetSegment()
                                sLastSegment = sTmp
                            Loop
                            Exit Do
                        End If

                        If sTmp = "ERROR" Then
                            Err.Raise(vbObjectError + 15014, , Replace(LRS(15014, msEDIFilePath, Str(mrEDISettingsImport.LINCounter)), "%3", Str(mrEDISettingsImport.SEQCounter)))
                            'Fatal error reading the file: msEDIFilePath
                            'LIN no.:
                            'SEQ no.:
                        ElseIf sTmp = "ENDOFFILEERROR" Then
                            Err.Raise(vbObjectError + 15045, , LRS(15045) & vbCrLf & LRS(15030, msEDIFilePath) & vbCrLf & LRS(15031, Str(mrEDISettingsImport.LINCounter)) & vbCrLf & LRS(15032, Str(mrEDISettingsImport.SEQCounter)))
                        End If

                        If Left(sTmp, 3) = "SEQ" Then
                            lSEQCounter = lSEQCounter + 1
                            'If lSEQCounter / 1000 = Int(lSEQCounter / 1000) Or lSEQCounter = 1 Or lSEQCounter = 100 Then
                            '    bx = MemoryMessage(lSEQCounter, "EDI2XML")
                            'End If
                        End If

                        'TEMPCODE due to error in file from BBS
                        'If bInsideDOCpart Then
                        '    If Left(sTmp, 3) = "MOA" Then
                        '        If eBank <> vbBabel.BabelFiles.Bank.SEB_NO Then
                        '            sTmp = Replace(sTmp, ".", ",")
                        '        End If
                        '    End If
                        'End If

                        mrEDISettingsImport.SegmentCount = mrEDISettingsImport.SegmentCount + 1
                        'Changed 20.09.2005 by Kjell, due to change in Danske Bank
                        ' They now omit the CNT-segment.
                        If Left(sTmp, 3) = "CNT" Or Left(sTmp, 3) = "UNT" Then
                            Exit Do
                        End If

                        elmNode = mrEDISettingsImport.FindNextSegment(Left(sTmp, 3))
                        'UPGRADE_WARNING: Couldn't resolve default property of object elmNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        lGroupNo = FindGroupNo(elmNode.parentNode)
                        If lGroupNo > 0 Then
                            iGroupIndex = mrEDISettingsImport.GetGroupRep(lGroupNo)
                        End If

                        bSegmentOK = CheckSegment(sTmp, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        aSegmentInfo = VB6.CopyArray(GetSegmentInfo)

                        iLevelChange = mrEDISettingsImport.LevelChange
                        'iLevelChange = 3
                        If Left(sTmp, 3) = "LIN" Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If Not Val(aSegmentInfo(2, 0)) = mrEDISettingsImport.LINCounter Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Err.Raise(vbObjectError + 15015, , LRS(15015, "line item number (LIN)", aSegmentInfo(2, 0)))
                                ' "Error in the "Line item number (LIN)" enumeration." & vbCrLf & "Incorrect number is " & aSegmentInfo(2, 0)
                            End If
                            'TEMPCODE due to error in file from BBS
                            bInsideDOCpart = False
                        ElseIf Left(sTmp, 3) = "SEQ" Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If Not Val(aSegmentInfo(2, 0)) = mrEDISettingsImport.SEQCounter Then
                                'New 17.02.2008
                                'Not a good solution, but in BANSTA from SEB there is stated a qualificator in the SEQ-segment
                                'Should either test for BANSTA and SEB or better use the correct element in the SEQ-record
                                'But it's Sunday and biathlon on the TV, and Tora just did a fantastic shooting
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If aSegmentInfo(2, 0) = "YF3" Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If Not Val(aSegmentInfo(2, 1)) = mrEDISettingsImport.SEQCounter Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Err.Raise(vbObjectError + 15015, , LRS(15015, "sequence number (SEQ)", aSegmentInfo(2, 0)))
                                    End If
                                Else
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    Err.Raise(vbObjectError + 15015, , LRS(15015, "sequence number (SEQ)", aSegmentInfo(2, 0)))
                                    ' "Error in the "Sequence number (SEQ)" enumeration." & vbCrLf & "Incorrect number is " & aSegmentInfo(2, 0)
                                End If
                            End If
                            'TEMPCODE due to error in file from BBS
                            bInsideDOCpart = False
                        ElseIf Left(sTmp, 3) = "DOC" Then
                            'Have to do this to create a new group21-element when it's not
                            ' the first DOC (then it has already been written).
                            If iLevelChange = 0 Then
                                mrEDISettingsImport.GroupChange = True
                            End If
                            'TEMPCODE due to error in file from BBS
                            bInsideDOCpart = True
                        ElseIf Left(sTmp, 3) = "UCI" Then
                            '02.09.2009 - Store the entire segment in a string as done with UNB and UNH
                            ReDim Preserve aSegmentInfo(2, UBound(aSegmentInfo, 2) + 1)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aSegmentInfo(0, UBound(aSegmentInfo, 2)) = "TGUCITOTAL"
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aSegmentInfo(1, UBound(aSegmentInfo, 2)) = "TUCITOTAL"
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aSegmentInfo(2, UBound(aSegmentInfo, 2)) = sTmp

                        End If



                        Select Case iLevelChange
                            'New child
                            Case -1
                                nodWrite = nodWrite.parentNode
                                nodWrite = AddXMLElement(nodWrite, "GROUP", lGroupNo, iGroupIndex)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                nodWrite = AddXMLElement(nodWrite, Left(sTmp, 3), -1, -1, CObj(aSegmentInfo))
                                'New sibling
                            Case 0
                                If mrEDISettingsImport.GroupChange = True Then
                                    nodWrite = nodWrite.parentNode
                                    nodWrite = nodWrite.parentNode
                                    nodWrite = AddXMLElement(nodWrite, "GROUP", lGroupNo, iGroupIndex)
                                Else
                                    nodWrite = nodWrite.parentNode
                                End If
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                nodWrite = AddXMLElement(nodWrite, Left(sTmp, 3), -1, -1, CObj(aSegmentInfo))
                                'bAddAttribs = AddXMLAttribs(nodWrite, sTmp)
                            Case Else
                                nodWrite = nodWrite.parentNode
                                For i = 1 To iLevelChange + 1
                                    nodWrite = nodWrite.parentNode
                                Next i
                                nodWrite = AddXMLElement(nodWrite, "GROUP", lGroupNo, iGroupIndex)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                nodWrite = AddXMLElement(nodWrite, Left(sTmp, 3), -1, -1, CObj(aSegmentInfo))
                        End Select
                    Loop

                    'We have gone through the User data segments, and continue to check the trailers

                    'Changed 20.09.2005 by Kjell, due to change in Danske Bank
                    ' They now omit the CNT-segment.

                    If Left(sTmp, 3) = "CNT" Then 'Validate number of LINS
                        'The CNT-segment is missing, fake it!
                        sCNTSegment = sTmp
                    Else
                        sCNTSegment = "CNT+LIN:" & Trim(Str(mrEDISettingsImport.LINCounter)) & "'"
                        'Deduct 1 from the segment count
                        mrEDISettingsImport.SegmentCount = mrEDISettingsImport.SegmentCount - 1
                    End If

                    '18.11.2015 - Added COACSU
                    If (sFormatIn <> "CONTRL" And sFormatIn <> "COACSU") And Not bAutackMessage Then '19.09.2008 added And Not bAutackMessage Then
                        bSegmentOK = mrEDISettingsImport.CheckNoOfLIN(sCNTSegment)
                    End If

                    bCorrectNodeFound = False

                    '19.09.2008 Added next IF
                    If Not bAutackMessage Then
                        Do Until bCorrectNodeFound
                            If nodWrite.nodeName = "GROUP" Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If Trim(nodWrite.getAttribute("group")) = "0" Then
                                    bCorrectNodeFound = True
                                Else
                                    nodWrite = nodWrite.parentNode
                                End If
                            Else
                                nodWrite = nodWrite.parentNode
                            End If
                        Loop

                        If sFormatIn <> "CONTRL" And sFormatIn <> "COACSU" Then '18.11.2015 - Added COACSU
                            'Store the CNT-segment
                            mrEDISettingsImport.SetCurrentElement("//GROUPS/UNB/UNG/GROUP/CNT")
                            bSegmentOK = CheckSegment(sCNTSegment, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            nodWrite = AddXMLElement(nodWrite, "CNT", -1, -1, CObj(aSegmentInfo))

                            If Left(sTmp, 3) = "CNT" Then
                                'If the present segment is a CNT, then read the next
                                sTmp = GetSegment()
                                sLastSegment = sTmp

                                ' 22.01.07 Added by JanP to cover CNT+LIN:3'CNT+SEQ:3'UNT+66
                                If Left(sTmp, 3) = "CNT" Then
                                    'If the present segment is a CNT, (to counter no of SEQs, then read the next
                                    sTmp = GetSegment()
                                    sLastSegment = sTmp
                                    mrEDISettingsImport.SegmentCount = mrEDISettingsImport.SegmentCount + 1
                                End If

                            End If

                            If sTmp = "ERROR" Then
                                Err.Raise(vbObjectError + 15013, , LRS(15013, "CNT", msEDIFilePath))
                                'Fatal error: Can't read the segment succeeding the "UNG"-segment in the file msEDIFilePath
                            End If
                        End If

                        'Check the UNT-segment
                        '18.11.2015 - Added new code for COACSU
                        sTmp2 = ""
                        If sFormatIn <> "COACSU" Then
                            sTmp2 = "//GROUPS/UNB/UNG/GROUP/UNT"
                        Else
                            sTmp2 = "//GROUPS/UNB/UNG/GROUP/GROUP/UNT"
                        End If
                        mrEDISettingsImport.SetCurrentElement(sTmp2)
                        'Old code
                        'mrEDISettingsImport.SetCurrentElement("//GROUPS/UNB/UNG/GROUP/UNT")

                        'Check if UNT is in use
                        'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If mrEDISettingsImport.CurrentElement.getAttribute("inuse") = 1 Then
                            'Check if the next segment in the CREMUL-file is UNT
                            If Left(sTmp, 3) <> "UNT" Then
                                ' If not UNT, check if the segment is mandatory. If so Error
                                bCheckMessageHeader = False 'to exit loop
                                'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If mrEDISettingsImport.CurrentElement.getAttribute("mandatory") = 1 Then
                                    Err.Raise(vbObjectError + 15012, , LRS(15012, "UNT", msEDIFilePath))
                                    ' "Mandatory segment "UNT" is missing in file msEDIFilePath"
                                Else
                                    'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    nodWrite = AddXMLElement(nodWrite.parentNode, "UNT", -1, -1)
                                End If
                            Else
                                ' If it is a UNT, Check and save it
                                bSegmentOK = CheckSegment(sTmp, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                nodWrite = AddXMLElement(nodWrite.parentNode, "UNT", -1, -1, CObj(aSegmentInfo))
                                mrEDISettingsImport.SegmentCount = mrEDISettingsImport.SegmentCount + 1
                                bSegmentOK = mrEDISettingsImport.CheckNoofSegments(sTmp)
                                elmNode = nodWrite.selectSingleNode("TG020/T0062")
                                If elmNode.nodeTypedValue <> mrEDISettingsImport.MessageRefNo Then
                                    Err.Raise(vbObjectError + 15017, , Replace(LRS(15017, elmNode.nodeTypedValue, mrEDISettingsImport.MessageRefNo), "%3", msEDIFilePath))
                                    'The messagereference in UNT: 9999 is not identical to the messagereference in UNH: 9999. Filename: msEDIFilePath"
                                End If
                                sTmp = GetSegment() ' read the next segment
                                sLastSegment = sTmp
                                If sTmp = "ERROR" Then
                                    Err.Raise(vbObjectError + 15013, , LRS(15013, "UNT", msEDIFilePath))
                                    'Fatal error: Can't read the segment succeeding the "UNT"-segment in the file msEDIFilePath
                                End If
                            End If
                        Else
                            nodWrite = AddXMLElement(nodWrite, "UNT", -1, -1)
                        End If
                    Else
                        sTmp = GetSegment() ' read the next segment
                        sLastSegment = sTmp
                    End If '                If Not bAutackMessage Then

                        If Left(sTmp, 3) <> "UNH" Then
                            bCheckMessageHeader = False ' To exit loop
                        End If
                Loop

                'Check the UNE-segment
                mrEDISettingsImport.SetCurrentElement("//GROUPS/UNB/UNE")
                'Check if UNE is in use
                'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If mrEDISettingsImport.CurrentElement.getAttribute("inuse") = 1 Then
                    'Check if the next segment in the CREMUL-file is UNE
                    If Left(sTmp, 3) <> "UNE" Then
                        ' If not UNT, check if the segment is mandatory. If so Error
                        bCheckFunctionalGroupHeader = False 'To exit loop
                        'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If mrEDISettingsImport.CurrentElement.getAttribute("mandatory") = 1 Then
                            Err.Raise(vbObjectError + 15012, , LRS(15012, "UNE", msEDIFilePath))
                            ' "Mandatory segment "UNE" is missing in file msEDIFilePath"
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode.parentNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            nodWrite = AddXMLElement(nodWrite.parentNode.parentNode.parentNode, "UNE", -1, -1)
                        End If
                    Else
                        ' If it is a UNE, Check and save it
                        bSegmentOK = CheckSegment(sTmp, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode.parentNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nodWrite = AddXMLElement(nodWrite.parentNode.parentNode.parentNode, "UNE", -1, -1, CObj(aSegmentInfo))
                        elmNode = nodWrite.selectSingleNode("TG010/T0048") 'The xpath may be wrong!!!!!!!
                        If elmNode.nodeTypedValue <> mrEDISettingsImport.FunctionalGroupRefNo Then
                            Err.Raise(vbObjectError + 15018, , Replace(LRS(15018, elmNode.nodeTypedValue, mrEDISettingsImport.FunctionalGroupRefNo), "%3", msEDIFilePath))
                            'The functional group reference in UNE: 9999 is not identical to the reference in UNG: 9999. Filename: msEDIFilePath"
                        End If
                        sTmp = GetSegment() ' read the next segment
                        sLastSegment = sTmp
                        If sTmp = "ERROR" Then
                            Err.Raise(vbObjectError + 15013, , LRS(15013, "UNE", msEDIFilePath))
                            'Fatal error: Can't read the segment succeeding the "UNE"-segment in the file msEDIFilePath
                        End If
                    End If
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode.parentNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    nodWrite = AddXMLElement(nodWrite.parentNode.parentNode.parentNode, "UNE", -1, -1)
                End If
                If Left(sTmp, 3) <> "UNG" Then
                    bCheckFunctionalGroupHeader = False ' To exit loop
                End If
            Loop

            'Check the UNZ-segment
            mrEDISettingsImport.SetCurrentElement("//GROUPS/UNZ")
            'Check if UNZ is in use
            'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If mrEDISettingsImport.CurrentElement.getAttribute("inuse") = 1 Then
                'Check if the next segment in the CREMUL-file is UNZ
                If Left(sTmp, 3) <> "UNZ" Then
                    ' If not UNZ, check if the segment is mandatory. If so Error
                    bCheckInterchangeHeader = False
                    'UPGRADE_WARNING: Couldn't resolve default property of object mrEDISettingsImport.CurrentElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If mrEDISettingsImport.CurrentElement.getAttribute("mandatory") = 1 Then
                        Err.Raise(vbObjectError + 15012, , LRS(15012, "UNZ", msEDIFilePath))
                        ' "Mandatory segment "UNZ" is missing in file msEDIFilePath"
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nodWrite = AddXMLElement(nodWrite.parentNode.parentNode, "UNZ", -1, -1)
                    End If
                Else
                    ' If it is a UNZ, Check and save it
                    bSegmentOK = CheckSegment(sTmp, (mrEDISettingsImport.CurrentElement), (mrEDISettingsImport.LINCounter), (mrEDISettingsImport.SEQCounter), msEDIFilePath, nodValidCurrencyCodes, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    aSegmentInfo = VB6.CopyArray(GetSegmentInfo)
                    If sFormatIn = "CONTRL" Or sFormatIn = "COACSU" Then '18.11.2015 - Added COACSU
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nodWrite = AddXMLElement(nodWrite.parentNode, "UNZ", -1, -1, CObj(aSegmentInfo))
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object aSegmentInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nodWrite = AddXMLElement(nodWrite.parentNode.parentNode, "UNZ", -1, -1, CObj(aSegmentInfo))
                    End If
                    elmNode = nodWrite.selectSingleNode("TG020/T0020")
                    If elmNode.nodeTypedValue <> mrEDISettingsImport.InterchangeRefNo Then
                        Err.Raise(vbObjectError + 15019, , Replace(LRS(15019, elmNode.nodeTypedValue, mrEDISettingsImport.InterchangeRefNo), "%3", msEDIFilePath))
                        'The interchange reference in UNZ: 9999 is not identical to the reference in UNB: 9999. Filename: msEDIFilePath"
                    End If
                    ' FIX: ERROR: Hva hvis det ikke ligger noe mer.
                    sTmp = GetSegment(False) ' read the next segment
                    sLastSegment = sTmp
                End If
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object nodWrite.parentNode.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nodWrite = AddXMLElement(nodWrite.parentNode.parentNode, "UNZ", -1, -1)
            End If
            'If Left(sTmp, 3) = "UNA" Then  REMOVED BY JANP 08.01.02
            '    sTmp = GetUNASegment()
            'End If
            'Changed 01.02.2006 by Kjell, next line and the possibility to continue with another UNA
            sTmp = Trim(sTmp) 'Sometimes the banks pad the UNZ line with blanks, before starting with a new UNA/UNB
            If Left(sTmp, 3) = "UNA" Then
                'Sometimes when we got a new UNA, it is from another bank/central/sender
                'Find the new mappingfile, the bank etc..
                'Must move the function FindMappingFile to vbEDIUtils or create a new class

                'Inititate EDISettings with the new XML-mappingfile
                'mrEDISettingsImport.Init me,

                'Must add the mappingfilename (retrieved through FindMappingFiles) to
                ' the LIn-level (or maybe a level above).
                'This must be used in vbBabel-DLL to find the appropriate *MAP.XML file
                ' to populate the collections.




                'Presuppose that we use the same separators in all file even if it is a new UNA
                '  Alternatively we must do something like:
                ' oEdiMsg.AddXMLHeader sTmp
                'Alas, dont use the new UNA, read the next segment
                sTmp = GetSegment()
                sLastSegment = sTmp
                If Left(sTmp, 3) <> "UNB" Then
                    Err.Raise(vbObjectError + 15007, , LRS(15007, msEDIFilePath))
                    'Can't find the first segment (UNA or UNB) in the file msEDIFilePath
                    'The segment is missing, or it is not an EDI-file

                    bCheckInterchangeHeader = False ' To exit loop
                End If
            ElseIf Left(sTmp, 3) <> "UNB" Then
                bCheckInterchangeHeader = False ' To exit loop
            End If
        Loop


        '    '*******   TEMP TEMP   *********
        '    mrXmlDoc.Save App.path & "\xxxxx.xml"
        '    '*******   TEMP TEMP   *********

        elmNode = mrXmlDoc.selectSingleNode("//EDIFACT")
        newEle = mrXmlDoc.createElement("SEQNO")
        'UPGRADE_WARNING: Couldn't resolve default property of object newEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        elmNode.appendChild(newEle)
        newEle.setAttribute("Number", Str(lSEQCounter))
        'UPGRADE_NOTE: Object elmNode may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        elmNode = Nothing
        'UPGRADE_NOTE: Object newEle may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        newEle = Nothing

        bx = mrEDISettingsImport.KillStdElements
        'UPGRADE_NOTE: Object oEdiMsg may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oEdiMsg = Nothing
        'UPGRADE_NOTE: Object mrStreamEDI may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        ' VIKTIG ? ENDRING 10.12.2012 - vi closet aldri mrStreamEDI, gj�r det n� !
        mrStreamEDI.Close()
        mrStreamEDI = Nothing
        oFs = Nothing

        RaiseEvent ProgressUpdated(100 / 2)

        ParseFile = True
        Exit Function

        'errorHandler:
        '    #If CDEBUG Then
        '         Err.Description
        '    #End If
        '    ParseFile = False

    End Function
    Private Function FindGroupNo(ByRef nodTemp As MSXML2.IXMLDOMElement) As Integer

        'UPGRADE_WARNING: Couldn't resolve default property of object nodTemp.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        FindGroupNo = nodTemp.getAttribute("number")

    End Function

    Public Function CreateProcessingInstruction(ByRef sTarget As String, ByRef sData As String) As Boolean
        Dim pi As MSXML2.IXMLDOMProcessingInstruction
        Dim bReturnValue As Boolean

        On Error GoTo errorHandler

        pi = mrXmlDoc.CreateProcessingInstruction(sTarget, sData)
        'UPGRADE_WARNING: Couldn't resolve default property of object pi. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        mrXmlDoc.insertBefore(pi, mrXmlDoc.childNodes.Item(0))

        bReturnValue = True
        CreateProcessingInstruction = bReturnValue

        Exit Function

errorHandler:
        bReturnValue = False
        CreateProcessingInstruction = bReturnValue

    End Function
    Public Function GetSegment(Optional ByVal bRaiseEndOfFileError As Boolean = True) As String
        Dim sData As String
        Dim sUNA As String
        Dim bSegmentEndOK As Boolean
        Dim lSegmentEnd As Integer
        Dim sError As String
        Dim sTempBuffer, sTempTrimBuffer As String
        Dim lLinesRead As Integer
        Dim bTryToFindSegmendEnd As Boolean
        Static sInputBuffer As String

        sError = ""
        sUNA = "UNA"
        'New 28.06.2006 - To resolve problems with many escapecharacters in a row
        bTryToFindSegmendEnd = True
        On Error GoTo errorHandler

        lSegmentEnd = 1
        bSegmentEndOK = False
        sData = ""

        'Check if we have a file from FD and it's sent via RJE
        ' The RJE-solution adds spaces on up to 80 positins on each line, and we have to remove them

        Do
            ' Read into sInputBuffer if
            ' - not AtEndOfStream and  Instr$(sInputBuffer,sSegmentSep) = 0

            'New IF 28.06.2006 - To resolve problems with many escapecharacters in a row
            If bTryToFindSegmendEnd Then ' False only when we have removed 2 escapechars
                lSegmentEnd = InStr(lSegmentEnd + 1, sInputBuffer, sSegmentSep, CompareMethod.Binary)
            End If
            If lSegmentEnd = 0 Then
                If Not mrStreamEDI.AtEndOfStream Then
                    If bTransmittedFromFDByRJE Then
                        'Check if we have a file from FD and it's sent via RJE
                        ' The RJE-solution adds spaces on up to 80 positins on each line, and we have to remove them
                        For lLinesRead = 1 To 20
                            If Not mrStreamEDI.AtEndOfStream Then
                                sTempBuffer = mrStreamEDI.ReadLine
                                sTempTrimBuffer = RTrim(sTempBuffer)
                                If Right(sTempTrimBuffer, 1) = "'" Then
                                    'Probably end of segment
                                    If Right(sTempTrimBuffer, 2) = "?'" Then
                                        'Probably not end of segment after all
                                        If Right(sTempTrimBuffer, 3) = "??'" Then
                                            'Probably end of segment. The ? was valid
                                            If Right(sTempTrimBuffer, 4) = "???'" Then
                                                'No end of segment after all. Stop the craziness here
                                            Else
                                                sTempBuffer = sTempTrimBuffer
                                            End If
                                        Else
                                            'Don't trim
                                        End If
                                    Else
                                        'End of segmentmark found. Right trim the line
                                        sTempBuffer = sTempTrimBuffer
                                    End If
                                Else
                                    'No end of segment, don't trim the line
                                End If
                                sInputBuffer = sInputBuffer & sTempBuffer
                            Else
                                Exit For
                            End If
                        Next lLinesRead
                        sInputBuffer = Replace(Replace(sInputBuffer, Chr(10), ""), Chr(13), "")
                        sInputBuffer = ReplaceUTF8Characters(sInputBuffer)
                        lCharactersReadSoFar = lCharactersReadSoFar + Len(sTempBuffer)
                        lCharctersReadUpTo10000 = lCharctersReadUpTo10000 + Len(sTempBuffer)
                    Else
                        'Regular formats
                        sInputBuffer = sInputBuffer & mrStreamEDI.Read(1000)
                        sInputBuffer = Replace(Replace(sInputBuffer, Chr(10), ""), Chr(13), "")
                        sInputBuffer = ReplaceUTF8Characters(sInputBuffer)
                        lCharactersReadSoFar = lCharactersReadSoFar + 1000
                        lCharctersReadUpTo10000 = lCharctersReadUpTo10000 + 1000
                    End If
                    If lCharctersReadUpTo10000 >= 10000 Then
                        If lCharactersReadSoFar > lSizeImportfile Then
                            lCharactersReadSoFar = lSizeImportfile
                        End If
                        RaiseEvent ProgressUpdated(System.Math.Round((lCharactersReadSoFar / lSizeImportfile * 100) / 2, 0))
                        lCharctersReadUpTo10000 = 0
                    End If
                Else
                    ' at end of stream, but no segmentsep found ! error !
                    sInputBuffer = ""
                    sError = "ENDOFFILE"
                    'Raise an error
                    If bRaiseEndOfFileError Then
                        GetSegment = sError
                    Else
                        GetSegment = ""
                    End If
                    Exit Function
                End If
            End If

            Do
                ' sInputBuffer now has at least on segment (or a "fake" segmentseparator in it)
                ' find the correct segmentend
                If lSegmentEnd > 0 Then
                    'Test for "fake" separator (same character as segmentseparator used in text)
                    If Mid(sInputBuffer, lSegmentEnd - 1, 1) = sEscapeChar Then
                        ' OK, fake char here, carry on testing ...
                        If Mid(sInputBuffer, lSegmentEnd - 2, 1) = sEscapeChar Then
                            ' aha, f.ex. ??, that is segment ends with the char ?
                            ' but keep on testing ...
                            If Mid(sInputBuffer, lSegmentEnd - 3, 1) = sEscapeChar Then
                                ' wrong segmentend, f.ex. ???', , interpret as ?' inside a segment
                                If Mid(sInputBuffer, lSegmentEnd - 4, 1) = sEscapeChar Then
                                    'OK again f.x. ????', that is the segment ends with ??
                                    If Mid(sInputBuffer, lSegmentEnd - 5, 1) = sEscapeChar Then
                                        'Not an segment end, f.ex. ?????', interpret as ??' inside a segment
                                        If Mid(sInputBuffer, lSegmentEnd - 6, 1) = sEscapeChar Then
                                            'OK again f.x. ??????', that is the segment ends with ???
                                            If Mid(sInputBuffer, lSegmentEnd - 7, 1) = sEscapeChar Then
                                                'Not an segment end, f.ex. ???????', interpret as ???' inside a segment
                                                If Mid(sInputBuffer, lSegmentEnd - 8, 1) = sEscapeChar Then
                                                    'OK again f.x. ????????', that is the segment ends with ????
                                                    If Mid(sInputBuffer, lSegmentEnd - 8, 1) = sEscapeChar Then
                                                        'Not an segment end, f.ex. ?????????', interpret as ????' inside a segment
                                                        'Enough is enough, stop here!

                                                        'New 28.06.2006 - To resolve problems with many escapecharacters in a row
                                                        'Remove two of the questionmarks, and rerun the loop
                                                        sInputBuffer = Left(sInputBuffer, lSegmentEnd - 9) & Mid(sInputBuffer, lSegmentEnd - 6)
                                                        lSegmentEnd = lSegmentEnd - 2
                                                        bTryToFindSegmendEnd = False
                                                        bSegmentEndOK = False
                                                    Else
                                                        ' OK, f.ex ????????' at end, that is the segment ends with ????
                                                        bSegmentEndOK = True
                                                        Exit Do
                                                    End If
                                                Else
                                                    ' Fake segmentend, f.ex.  x?'
                                                    bSegmentEndOK = False
                                                    Exit Do
                                                End If
                                            Else
                                                ' OK, correct segmentend, no sEscapeChar before sSegmentSep
                                                bSegmentEndOK = True
                                                Exit Do
                                            End If
                                        Else
                                            ' Fake segmentend, f.ex.  x?'
                                            bSegmentEndOK = False
                                            Exit Do
                                        End If
                                    Else
                                        ' OK, correct segmentend, no sEscapeChar before sSegmentSep
                                        bSegmentEndOK = True
                                        Exit Do
                                    End If
                                Else
                                    ' Fake segmentend, f.ex.  x?'
                                    bSegmentEndOK = False
                                    Exit Do
                                End If
                            Else
                                ' OK, correct segmentend, no sEscapeChar before sSegmentSep
                                bSegmentEndOK = True
                                Exit Do
                            End If
                        Else
                            ' Fake segmentend, f.ex.  x?'
                            bSegmentEndOK = False
                            Exit Do
                        End If
                    Else
                        ' OK, correct segmentend, no sEscapeChar before sSegmentSep
                        bSegmentEndOK = True
                        Exit Do
                    End If
                    ' Fake segmentend discovered, find next sSegmentSep
                    ' See first line after Do
                    bSegmentEndOK = False
                    Exit Do
                Else
                    ' no segmentend, read another chunk ..
                    bSegmentEndOK = False
                    Exit Do
                End If
            Loop

            ' Must check if we have reached a correct segmentend, or we have just found fakes ...
            If bSegmentEndOK Then
                Exit Do
            End If
        Loop

        If bSegmentEndOK Then
            sData = Left(sInputBuffer, lSegmentEnd)
            sInputBuffer = Mid(sInputBuffer, lSegmentEnd + 1)

            ' Remove Carriage Return / Line Feed (CRLF)
            '    If InStr(sData, vbCrLf) > 0 Then
            '        sData = Replace(sData, vbCrLf, "")
            '    ElseIf InStr(sData, vbCr) > 0 Then
            '        sData = Replace(sData, vbCr, "")
            '    ElseIf InStr(sData, vbLf) > 0 Then
            '        sData = Replace(sData, vbLf, "")
            '    End If
            '    If InStr(sData, vbCr) > 0 Then
            '        sData = Replace(sData, vbCr, "")
            '    End If
            '    If InStr(sData, vbLf) > 0 Then
            '        sData = Replace(sData, vbLf, "")
            '    End If
        End If

        If Left(sData, 3) = "UNA" Then
            ' If two or more files are copied togethere, there will be several UNAs
            sSegmentSep = Mid(sData, 9, 1)
            sEscapeChar = Mid(sData, 7, 1)
            sDecimalSep = Mid(sData, 6, 1)
            sGroupSep = Mid(sData, 5, 1)
            sElementSep = Mid(sData, 4, 1)
            ' Don't need this one, read another segment
            sData = GetSegment()

        End If


        If Len(Trim(sData)) = 0 Then
            If mrStreamEDI.AtEndOfStream Then
                bAtEndOfStream = True
                GetSegment = ""
            Else
                GetSegment = "ERROR"
            End If
        Else
            If sData.IndexOf(Chr(26)) > 0 Then
                sData = sData.Replace(Chr(26), "")
            End If
            GetSegment = sData '& "'"
        End If

        Exit Function

errorHandler:
        sError = sError & "ERROR"
        GetSegment = sError

    End Function
    Public Function GetUNASegment() As String
        ' Read first segment, and set EDI special-characters in class
        Dim sData As String

        On Error GoTo errorHandler

        sData = mrStreamEDI.Read(3)


        If Left(sData, 3) = "UNA" Then

            sData = sData & mrStreamEDI.Read(6)

            sSegmentSep = Mid(sData, 9, 1)
            sEscapeChar = Mid(sData, 7, 1)
            sDecimalSep = Mid(sData, 6, 1)
            sGroupSep = Mid(sData, 5, 1)
            sElementSep = Mid(sData, 4, 1)
            GetUNASegment = sData

        Else
            sData = sData & GetSegment()
            If Len(Trim(sData)) = 0 Then
                GetUNASegment = "ERROR"
            Else
                If Right(sData, 1) <> "'" Then
                    GetUNASegment = sData & "'"
                Else
                    GetUNASegment = sData
                End If
            End If
        End If

        Exit Function

errorHandler:
        GetUNASegment = "ERROR"

    End Function
    Friend Function Init(ByRef sEDIFile As String, ByRef iCharacterSet As CEdi2Xml.FILEFORMAT, ByRef sMappingFile As String, ByRef sTagSettingsFile As String, ByRef sFormat As String, Optional ByRef sOutputPath As String = "", Optional ByRef sValues As String = "", Optional ByRef sTreatSpecial As String = "") As Boolean

        'oCEdiError = CreateObject ("EDI2XML.CEDIError")

        oFs = New Scripting.FileSystemObject

        msEDIFilePath = sEDIFile
        miCharacterSet = iCharacterSet
        If Not mrEDISettingsImport.Init(Me, sMappingFile, sTagSettingsFile, sFormat, sValues) Then
            'GoTo errorHandler
        End If

        msXmlOutputPath = sOutputPath
        sSpecial = sTreatSpecial


        OpenEDIFile(sEDIFile)

        mrXmlDoc.loadXML("<EDIFACT></EDIFACT>")
        mrXmlDoc.preserveWhiteSpace = True

        'EndOK:
        Init = True
        Exit Function

        'errorHandler:
        '
        '    Init = False
    End Function
    Private Function OpenEDIFile(ByRef sFile As String) As Boolean
        On Error GoTo errorHandler

        'UPGRADE_WARNING: Couldn't resolve default property of object oFs.GetFile().Size. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        lSizeImportfile = oFs.GetFile(msEDIFilePath).Size
        mrStreamEDI = oFs.OpenTextFile(msEDIFilePath, Scripting.IOMode.ForReading, False, miCharacterSet)

EndOK:
        OpenEDIFile = True
        Exit Function

errorHandler:
        OpenEDIFile = False

    End Function
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        miCurGroup = 0

        'PORTVALE 07.07.2008 - removed initiation of specialcharacters
        sSegmentSep = "'"
        'sEscapeChar = "?"
        'sDecimalSep = ","
        'sGroupSep = "+"
        'sElementSep = ":"
        lSegmentLength = 3
        bAtEndOfStream = False
        eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

    End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
