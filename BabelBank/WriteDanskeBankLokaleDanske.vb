Option Strict Off
Option Explicit On
Module WriteDanskebankLokaleDanske
    Function WriteDanskeBankCMBOCMUO(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal eBank As vbBabel.BabelFiles.Bank, ByVal sSpecial As String) As Boolean
        ' XOKNET 26.09.2013 - take whole function
        ' XokNET 05.12.2013 Take whole function
        ' XokNET 13.05.2014 Several changes - take whole function

        ' 26.09.2013 Added International payments (CMUO)
        '"CMBO","3258186214","3258186230","140,00","","DKK","","U","","","","","J","Felt 14, Brev til afsender","Felt 15, Linie 2","","","","","F.20, tekst til afs.","","F.22,tekst til modt.","","","","","Felt 27; Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Felt 68, P� vegne af","Felt 69, Adresse1","Felt 70, Adresse2","Felt 71, Adresse 3","2000  Frederiksberg"
        '"CMBO","3258186214","3258186230","150,00","","DKK","","S","","","","","","","","","","","","F.20, tekst til afs.","","F.22,tekst til modt.","","","Felt 25, DebitorID","Felt 26, Dokument-reference","Felt 27, Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, osv. op til linie 41"
        '"CMBO","3258186214","3258186230","155,00","","DKK","","N","","","","","","","","","","","","F.20, tekst til afs.","","F.22,tekst til modt.","","","","","Felt 27, Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, osv. op til linie 41"
        '"CMBO","3258186214","0001000012","100,00","","DKK","","N","","","","","","","","","","","","F.20, tekst til afs.","","F.22,tekst til modt."
        '"CMBO","3258186214","3001535269","145,25","","USD","","N","","","","","","","","","","","","F.20, tekst til afs.","","F.22,tekst til modt."
        '"CMBO","3258186230","3001774832","525,","","EUR","","N","","","","","","","","","","","","F.20, tekst til afs.","","F.22,tekst til modt."
        '"CMBO","3258186214","3258186230","160,00","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","Felt 21, bem�rkning","F.22,tekst til modt."
        '"CMBO","3258186214","CHECK","170,00","","DKK","","","","","","","","","","Felt 16, Modtagers navn","Felt 17, Adresse1","Felt 18, Adresse2","2000  Frederiksberg","F.20, tekst til afs.","Felt 21, bem�rkning","","","","","","Felt 27, Besked til modtager"
        '"CMBO","3258186214","CHECK","180,00","","DKK","","U","","","","","","","","Felt 16, Modtagers navn","Felt 17, Adresse1","Felt 18, Adresse2","2000  Frederiksberg","F.20, tekst til afs.","","","","","","","Felt 27, Besked til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, linie 4"
        '"CMBO","3258186214","IK1000012","100,00","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","Felt 21, bem�rkning","","01","","","","Felt 27, Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, Linie 4","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Felt 68, P� vegne af","Felt 69, Adresse1","Felt 70, Adresse2","Felt 71, Adresse 3","2000  Frederiksberg"
        '"CMBO","3258186214","IK1000012","120,00","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","","","01","","","","Felt 27, Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, Linie 4"
        '"CMBO","3258186214","IK1000012","101,00","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","","","41","0123456789","","","Felt 27, Tekst til modtager","Felt 28, Linie 2","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Felt 68, P� vegne af","Felt 69, Adresse1","Felt 70, Adresse2","Felt 71, Adresse3","2000  Frederiksberg"
        '"CMBO","3258186214","IK85443771","26,00","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","","","71","000000134045012"
        '"CMBO","3258186214","IK85443771","103,00","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","","","73","","","","Felt 27, Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, Linie 4"
        '"CMBO","3258186214","IK85443771","104,50","","DKK","","","","","","","","","","","","","","F.20, tekst til afs.","","","75","0000000160066247","","","Felt 27, Tekst til modtager","Felt 28, Linie 2","Felt 29, Linie 3","Felt 30, Linie 4"

        '"CMUO","3258186214","54110000032334","GBP","117,00","DKK","","1","Felt 9, Modtagers navn","Felt 10, Adresse1","Felt 11, Adresse2","Felt 12, Adresse3","","","","","2","Felt 18, Tekst til modtager","Felt 19, Linie 2","Felt 20, Linie 3","Felt 21, Linie 4","","","","","","","","","","","","","","","","","","F.39, tekst til afs.","Felt 40, Bem�rkning","","","","","","1","","","Felt 49, afsenderad-vis","Felt 50, Linie 2","LOYDGB2L","SC","309634","GB"
        '"CMUO","3258186230","12345901234567","EUR","450,00","","","3","Felt 9, Modtagers navn","Felt 10, Adresse1","Felt 11, Adresse2","Felt 12, Adresse3","","","","","1","Felt 18, Tekst til modtager","Felt 19, Linie 2","Felt 20, Linie 3","Felt 21, Linie 4","","","","","","","","","","","Felt 32, Betalingsform�l","Felt 33, Linie 2","Felt 34, Linie 3","Felt 35, Linie 4","","","","F.39, tekst til afs.","","","","","","","1","","","Felt 49, afsenderadvis","Felt 50, Linie 2","DRESDEFF600","BL","60080000","DE",
        '"CMUO","3258186214","123456789000","EUR","562,50","","","2","Felt 9, Modtagers navn","Felt 10, Adresse1","Felt 11, Adresse2","Felt 12, Adresse3","","","","","3","Felt 18, Tekst til modtager","Felt 19, Linie 2","Felt 20, Linie 3","Felt 21, Linie 4","","","","","","","","","","","Felt 32, Betalingsform�l","Felt 33, Linie 2","Felt 34, Linie 3","Felt 35, Linie 4","","","","F.39, tekst til afs.","","","","","","","","","","","","DRESDEFF600","BL","60080000","DE"
        '"CMUO","3258186214","987654321-989898","SGD","800,00","","","1","Felt 9, Modtagers navn","Felt 10, Adresse1","Felt 11, Adresse2","Felt 12, Adresse3","Felt 13, Modtagers bank","Felt 14, Adresse 1","Felt 15, Adresse 2","Felt 16, Adresse 3","2","Felt 18, Tekst til modtager","Felt 19, Linie 2","Felt 20, Linie 3","Felt 21, Linie 4","","","","1800","022001","","","","","","Felt 32, Betalingsform�l","Felt 33, Linie 2","Felt 34, Linie 3","Felt 35, Linie 4","","","","F.39, tekst til afs.","","","","","","","","","","","","HSBCSGSG","","","SG"
        '"CMUO","3258186214","CHECK","GBP","633,00","DKK","","","Felt 9, Modtagers navn","Felt 10, Adresse1","Felt 11, Adresse2","Felt 12, Adresse3","","","","","","Felt 18, Tekst til modtager","Felt 19, Linie 2","Felt 20, Linie 3","Felt 21, Linie 4","","","","1800","032001","","","","GB","","Felt 32, Betalingsform�l","Felt 33, Linie 2","Felt 34, Linie 3","Felt 35, Linie 4","","","","F.39, tekst til afs.","","","X","","","","1","","","Felt 49, afsenderadvis","Felt 50, Linie 2"
        '"CMUO","3258186214","CHECK","EUR","200,00","","","","Felt 9, Modtagers navn","Felt 10, Adresse","","","","","","","","","","","","","","","","","","","","DE","","","","","","","","","F.39, tekst til afs.","Felt 40, Bem�rkning","","X","2","","","1","4610","X"
        '"CMUO","3258186214","CHECK","DKK","655,00","","","","Felt 9, Modtagers navn","Felt 10, Adresse","","","","","","","","","","","","","","","","","","","","DK","","","","","","","","","F.39, tekst til afs.","Felt 40, Bem�rkning","","X","2","","","","3115","X"

        '
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim sDate2 As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim nAmount As Double
        Dim sFromAccount As String, sToAccount As String, sShortNotification As String
        Dim i As Integer
        Dim sAmount As String
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim sERH As String
        Dim sTmp As String
        'XNET 09.05.2014
        Dim bThisIsVismaIntegrator As Boolean
        '22.10.2018 - Added 3 variables
        Dim sMessageID As String = ""
        Dim bPaymentExported As Boolean = False
        Dim bFileInitiated As Boolean = False
        '***************VISMA*****************************
        ' Testing if this is Visma Integrator
        bThisIsVismaIntegrator = IsThisVismaIntegrator()

        '
        ' create an outputfile
        Try
            bAppendFile = False


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bFileInitiated = True
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        ' XOKNET 03.12.2015 added  And Not oPayment.Exported
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            ' XOKNET 03.12.2015 added  And Not oPayment.Exported
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                ' XOKNET 03.12.2015 added  And Not oPayment.Exported
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    '22.10.2018 - Added returnfiles
                                    ' According to format Status message of payments sent to the bank in comma separated format (BANSTA).
                                    ' Filenmae: CMBO_Retur.pdf - received from Danske Bank via Gjensidige
                                    If oPayment.StatusCode <> "00" And oPayment.StatusCode <> "01" Then

                                        If EmptyString(sMessageID) Then
                                            sMessageID = CreateMessageReference(oBabel)
                                            sDate = PadRight(Format(Date.Now, "yyyyMMddHHmm"), 12, "0")
                                            sDate2 = PadRight(Format(Date.Now, "yyyy-MM-dd-HH.mm.ss.ffffff"), 26, "0")
                                        End If

                                        If sSpecial = "DK_GJENSIDIGE_CMBO" Then
                                            sLine = ""
                                            sLine = Chr(34) & "KOMMAMFB" & Chr(34) & "," '1. Fixed
                                            If sMessageID.Length > 35 Then
                                                sLine = sLine & Chr(34) & sMessageID.Substring(0, 35) & Chr(34) & "," '2. Unique identification of this message.
                                            Else
                                                sLine = sLine & Chr(34) & sMessageID & Chr(34) & "," '2. Unique identification of this message.
                                            End If
                                            sLine = sLine & Chr(34) & sDate & Chr(34) & ","  '3. Length = 12 - The time the message was created  Format CCYYMMDDHHMM 
                                            sLine = sLine & Chr(34) & Chr(34) & "," '4. The ID of the payment instruction file owning the payments in the message, may be blank if the BANSTA is a manual order, not created on basis of an incoming payment file.
                                            sLine = sLine & Chr(34) & oPayment.REF_Own.Trim & Chr(34) & "," '5. The technical debitreference of the debit, that was given in the incoming file. 
                                            sLine = sLine & Chr(34) & oPayment.REF_Own.Trim & Chr(34) & "," '6. The technical creditreference/End-to-end reference of the credit, that was given in the incoming file. 
                                            If oPayment.StatusCode = "02" Then
                                                sLine = sLine & Chr(34) & "ST" & Chr(34) & "," '7. Status of the transaction 2 = debit rejected AK = credit rejected 3 or ST = status update, occurs when a payment changes status to another not rejected state. For example if a payment is authorized.
                                            Else
                                                sLine = sLine & Chr(34) & "2" & Chr(34) & "," '7. Status of the transaction 2 = debit rejected AK = credit rejected 3 or ST = status update, occurs when a payment changes status to another not rejected state. For example if a payment is authorized.
                                            End If
                                            sLine = sLine & Chr(34) & sDate2 & Chr(34) & "," '8. Length = 26 - Timestamp of status change (format fx CCYY-MM-DDHH.MM.SS.Microseconds) Microseconds are 6 digits. 
                                            If oPayment.StatusCode = "02" Then
                                                sLine = sLine & Chr(34) & Chr(34) & "," '9. K + 4 digits i.e.: �K1234�
                                                sLine = sLine & Chr(34) & Chr(34) & "," '10. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 11 
                                                sLine = sLine & Chr(34) & Chr(34) & "," '11. ISO 639-1 code of the text in field 10 controlled by the users language code 
                                                sLine = sLine & Chr(34) & Chr(34) & "," '12. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 13
                                                sLine = sLine & Chr(34) & Chr(34) & "," '13. ISO 639-1 code of the text in field 12 (always EN) 
                                            Else
                                                sLine = sLine & Chr(34) & "K1234" & Chr(34) & "," '9. K + 4 digits i.e.: �K1234�
                                                sFreetext = ""
                                                If Not EmptyString(oPayment.StatusText) Then
                                                    sFreetext = oPayment.StatusText.Trim
                                                Else
                                                    For Each oInvoice In oPayment.Invoices
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sFreetext = sFreetext & " " & oFreetext.Text.Trim
                                                        Next oFreetext
                                                    Next oInvoice
                                                End If
                                                If Not EmptyString(sFreetext) Then
                                                    sFreetext = sFreetext.Trim
                                                    If sFreetext.Length > 70 Then
                                                        sLine = sLine & Chr(34) & sFreetext.Substring(0.7) & Chr(34) & "," '10. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 11 
                                                        sLine = sLine & Chr(34) & "DA" & Chr(34) & "," '11. ISO 639-1 code of the text in field 10 controlled by the users language code 
                                                        sLine = sLine & Chr(34) & sFreetext.Substring(0.7) & Chr(34) & "," '12. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 13 
                                                        sLine = sLine & Chr(34) & "EN" & Chr(34) & "," '13. ISO 639-1 code of the text in field 12 (always EN) 
                                                    Else
                                                        sLine = sLine & Chr(34) & sFreetext & Chr(34) & "," '10. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 11 
                                                        sLine = sLine & Chr(34) & "DA" & Chr(34) & "," '11. ISO 639-1 code of the text in field 10 controlled by the users language code 
                                                        sLine = sLine & Chr(34) & sFreetext & Chr(34) & "," '12. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 13 
                                                        sLine = sLine & Chr(34) & "EN" & Chr(34) & "," '13. ISO 639-1 code of the text in field 12 (always EN) 
                                                    End If
                                                Else
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '10. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 11 
                                                    sLine = sLine & Chr(34) & "DA" & Chr(34) & "," '11. ISO 639-1 code of the text in field 10 controlled by the users language code 
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '12. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 13 
                                                    sLine = sLine & Chr(34) & "EN" & Chr(34) & "," '13. ISO 639-1 code of the text in field 12 (always EN) 
                                                End If
                                            End If
                                            sLine = sLine & Chr(34) & Chr(34) & "," '14. Field ID (Used by Business PC) 
                                        Else
                                            sLine = ""
                                            sLine = "KOMMAMFB" '1. Fixed
                                            sLine = sLine & PadLeft(sMessageID, 35, " ") '2. Unique identification of this message.
                                            sLine = sLine & sDate '3. Length = 12 - The time the message was created  Format CCYYMMDDHHMM 
                                            sLine = sLine & Space(35) '4. The ID of the payment instruction file owning the payments in the message, may be blank if the BANSTA is a manual order, not created on basis of an incoming payment file.
                                            sLine = sLine & PadLeft(sDate, 35, " ") '5. The technical debitreference of the debit, that was given in the incoming file. 
                                            'oPayment.Ref_Own_BabelBankGenerated = oPayment.Ref_Own_BabelBankGenerated
                                            'oPayment.REF_Own = oPayment.REF_Own
                                            sLine = sLine & PadLeft(oPayment.REF_Own.Trim, 35, " ") '6. The technical creditreference/End-to-end reference of the credit, that was given in the incoming file. 
                                            If oPayment.StatusCode = "02" Then
                                                sLine = sLine & PadLeft("ST", 6, " ") '7. Status of the transaction 2 = debit rejected AK = credit rejected 3 or ST = status update, occurs when a payment changes status to another not rejected state. For example if a payment is authorized.
                                            Else
                                                sLine = sLine & PadLeft("2", 6, " ") '7. Status of the transaction 2 = debit rejected AK = credit rejected 3 or ST = status update, occurs when a payment changes status to another not rejected state. For example if a payment is authorized.
                                            End If
                                            sLine = sLine & sDate2 '8. Length = 26 - Timestamp of status change (format fx CCYY-MM-DDHH.MM.SS.Microseconds) Microseconds are 6 digits. 
                                            sLine = sLine & PadLeft("K1234", 6, " ") '9. K + 4 digits i.e.: �K1234�
                                            If oPayment.StatusCode = "02" Then
                                                sLine = sLine & Space(70) '10. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 11 
                                                sLine = sLine & PadLeft("DA", 2, " ") '11. ISO 639-1 code of the text in field 10 controlled by the users language code 
                                                sLine = sLine & Space(70) '12. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 13
                                                sLine = sLine & PadLeft("EN", 2, " ") '13. ISO 639-1 code of the text in field 12 (always EN) 
                                            Else
                                                sFreetext = ""
                                                If Not EmptyString(oPayment.StatusText) Then
                                                    sFreetext = oPayment.StatusText.Trim
                                                Else
                                                    For Each oInvoice In oPayment.Invoices
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sFreetext = sFreetext & oFreetext.Text
                                                        Next oFreetext
                                                    Next oInvoice
                                                End If
                                                sLine = sLine & PadLeft(sFreetext, 70, " ") '10. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 11 
                                                sLine = sLine & PadLeft("DA", 2, " ") '11. ISO 639-1 code of the text in field 10 controlled by the users language code 
                                                sLine = sLine & PadLeft(sFreetext, 70, " ") '12. Reason of rejection, Textual description of the status code in the language of the ISO-code given in field 13
                                                sLine = sLine & PadLeft("EN", 2, " ") '13. ISO 639-1 code of the text in field 12 (always EN) 
                                            End If
                                            sLine = sLine & Space(35) '14. Field ID (Used by Business PC) 

                                        End If

                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine(sLine)
                                        End If
                                        oPayment.Exported = True
                                        For Each oInvoice In oPayment.Invoices
                                            oInvoice.Exported = True
                                        Next
                                        bPaymentExported = True
                                    ElseIf oPayment.StatusCode = "01" Then
                                        oPayment.Exported = True
                                        For Each oInvoice In oPayment.Invoices
                                            oInvoice.Exported = True
                                        Next
                                    Else
                                        If oPayment.PayType <> "I" Then
                                            ' Domestic payments

                                            '-------- fetch content of each payment ---------
                                            sFromAccount = Trim$(oPayment.I_Account)
                                            sToAccount = Trim$(oPayment.E_Account)
                                            sName = Trim$(Left$(oPayment.E_Name, 35))
                                            sOwnRef = Trim$(Left$(oPayment.REF_Own, 20))


                                            For iInvoiceCounter = 1 To oPayment.Invoices.Count
                                                ' XNET 13.05.2014
                                                ' Critical change to avoid exporing cancelled payments
                                                oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
                                                If Not oInvoice.Exported Then
                                                    '                                    If oPayment.PayCode = "301" Or oPayment.Invoices.Count > 40 Then 'FIK
                                                    '                                        'FIK, or if more than 40 Invoices, has not enough space for notification
                                                    '                                        nAmount = oPayment.Invoices(iInvoiceCounter).MON_InvoiceAmount
                                                    '                                    Else
                                                    '                                        nAmount = 0
                                                    '                                        For Each oInvoice In oPayment.Invoices
                                                    '                                            nAmount = nAmount + Val(oInvoice.MON_InvoiceAmount)
                                                    '                                        Next
                                                    '                                    End If
                                                    nAmount = oInvoice.MON_InvoiceAmount

                                                    ' 05.12.2013 changed to DDMMYYY
                                                    sDate = Right$(oPayment.DATE_Payment, 2) & Mid$(oPayment.DATE_Payment, 5, 2) & Left$(oPayment.DATE_Payment, 4) 'skal v�re DDMMYYYY

                                                    ' Start writing to file, one payment pr line
                                                    sLine = Chr(34) & "CMBO" & Chr(34) & ","
                                                    sLine = sLine & Chr(34) & sFromAccount & Chr(34) & ","          '2 Fra konto
                                                    '08.06.2021 - Changed test from If oPayment.PayCode = "301" Then
                                                    '   to IsFIK(oPayment.PayCode)
                                                    If IsFIK(oPayment.PayCode) Then   'FIK
                                                        sLine = sLine & Chr(34) & "IK" & sToAccount & Chr(34) & ","  '3 Debitorid
                                                    ElseIf Len(sToAccount) = 0 Then
                                                        sLine = sLine & Chr(34) & "CHECK" & Chr(34) & ","   '3 /CHECK/
                                                    Else
                                                        ' 05.12.2013
                                                        ' AccountNo DK is either 14, with first 4 as regnr, last as accountno
                                                        ' In between, there amy be zeroes, to pad the accountno.
                                                        ' Or, there may be an IBANno, 18 chars
                                                        If Not IsNumeric(Left$(sToAccount, 2)) Then
                                                            If Len(sToAccount) < 14 And Len(sToAccount) > 5 Then
                                                                sToAccount = Left$(sToAccount, 4) & New String("0", 14 - Len(sToAccount)) & Right$(sToAccount, Len(sToAccount) - 4)
                                                            End If
                                                        End If
                                                        sLine = sLine & Chr(34) & sToAccount & Chr(34) & ","            '3 Til konto/CHECK/Kortart+Kreditornr
                                                    End If
                                                    sLine = sLine & Chr(34) & Format(nAmount / 100, "###0.00") & Chr(34) & ","             '4 Bel�p
                                                    sLine = sLine & Chr(34) & sDate & Chr(34) & ","                 '5 Betalingsdato DDMMYY
                                                    sLine = sLine & Chr(34) & oPayment.MON_TransferCurrency & Chr(34) & ","  '6 Valuta

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '7 Not used
                                                    ' 09.05.2007 - Added functionality for Utvidet tekst p� kontoutskrift "U" for
                                                    ' Reitan Danmark
                                                    If UCase(oPayment.Cargo.Special) = "REITAN_DANMARK" Then ' 09.06.2007 - endret fra sm� til store bokstaver
                                                        ' Meddelelse p� modtagers kontoutskrift - 404
                                                        sLine = sLine & Chr(34) & "U" & Chr(34) & ","     'Utvidet tekst p� kontoutskrift "U"
                                                        '08.06.2021 - Changed test from oPayment.PayCode = "301" Then
                                                        '   to IsFIK(oPayment.PayCode)
                                                    ElseIf oPayment.PayCode = "250" Or IsFIK(oPayment.PayCode) Then  ' Uten melding
                                                        sLine = sLine & Chr(34) & "N" & Chr(34) & ","     '8 Meddelelsesform,  N = Ingen melding
                                                    Else
                                                        'sLine = sLine & Chr(34) & "J" & Chr(34) & ","     '8 Meddelelsesform, J=melding til mottaker
                                                        ' 05.12.2013 set defalt to U if not otherwise noted (Extended message on receivers accountstatement
                                                        sLine = sLine & Chr(34) & "U" & Chr(34) & ","
                                                    End If

                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '9 ikke i bruk
                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '10 ikke i bruk
                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '11 ikke i bruk
                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '12 ikke i bruk
                                                    sLine = sLine & Chr(34) & "N" & Chr(34) & ","   '13 brev til avsender, alltid N
                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '14 tekst p� brev til avstender, ikke i bruk
                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '15 tekst p� brev til avstender, ikke i bruk
                                                    ' name/address for cheques only
                                                    ' XOKNET 27.01.2016 added Reitan_Danmark in next If
                                                    If EmptyString(oPayment.E_Account) Or UCase(oPayment.Cargo.Special) = "REITAN_DANMARK" Then

                                                        sLine = sLine & Chr(34) & Trim$(oPayment.E_Name) & Chr(34) & ","  '16 Navn/adr mottaker
                                                        sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr1) & Chr(34) & ","  '17 Navn/adr mottaker
                                                        sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr2) & Chr(34) & ","  '18 Navn/adr mottaker
                                                        If Len(oPayment.E_Zip) > 0 Then
                                                            sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr3) & " " & oPayment.E_City & Chr(34) & ","   '19 Navn/adr mottaker
                                                        Else
                                                            sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr3) & Chr(34) & ","  '19 Navn/adr mottaker
                                                        End If
                                                    Else
                                                        ' 02.09.2016
                                                        ' SEB needs Name if field 16 for all transactions (not just check)
                                                        If IsSEB(eBank) Then
                                                            sLine = sLine & Chr(34) & Trim$(oPayment.E_Name) & Chr(34) & ","  '16 Navn til mottaker
                                                        Else
                                                            sLine = sLine & Chr(34) & Chr(34) & ","   '16 Navn/adr mottaker
                                                        End If
                                                        sLine = sLine & Chr(34) & Chr(34) & ","   '17 Navn/adr mottaker
                                                        sLine = sLine & Chr(34) & Chr(34) & ","  '18 Navn/adr mottaker
                                                        sLine = sLine & Chr(34) & Chr(34) & ","   '19 Navn/adr mottaker

                                                    End If
                                                    'sLine = sLine & Chr(34) & Trim$(oPayment.REF_Own) & Chr(34) & ","  '20 tekst p� betalers kontoutskrift
                                                    ' 05.12.2013

                                                    ' XNET 09.05.2014
                                                    If bThisIsVismaIntegrator Then
                                                        sLine = sLine & Chr(34) & Trim$(oInvoice.CustomerNo) & "/" & Trim$(oInvoice.InvoiceNo) & " " & Trim$(oPayment.E_Name) & Chr(34) & ","      '20 tekst p� betalers kontoutskrift
                                                    Else
                                                        ' XOKNET 27.01.2016 added Reitan_Danmark in next If
                                                        If UCase(oPayment.Cargo.Special) = "REITAN_DANMARK" Then
                                                            sLine = sLine & Chr(34) & Trim$(oPayment.REF_Own) & Chr(34) & ","  '20 tekst p� betalers kontoutskrift
                                                        Else
                                                            sLine = sLine & Chr(34) & Trim$(oInvoice.REF_Own) & Chr(34) & ","  '20 tekst p� betalers kontoutskrift
                                                        End If

                                                    End If
                                                    sLine = sLine & Chr(34) & Chr(34) & ","    '21 05.12.2013 Not in use
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.Text_E_Statement) & Chr(34) & ","  '22 Tekst p� mottakers kontoutdrag
                                                    '08.06.2021 - Changed test from If oPayment.PayCode = "301" Then
                                                    '   to IsFIK(oPayment.PayCode)
                                                    If IsFIK(oPayment.PayCode) Then   'FIK
                                                        sLine = sLine & Chr(34) & Left$(oInvoice.Unique_Id, 2) & Chr(34) & "," '23 Kortartkode
                                                        sLine = sLine & Chr(34) & Mid$(oInvoice.Unique_Id, 3) & Chr(34) & "," '24 Betalingsid
                                                    Else
                                                        'No FIK/Girokort
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '23 Kortartkode
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '24 Betalingsid
                                                    End If
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '25
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '26

                                                    ' melding til mottaker, Felt 27-67

                                                    'If oPayment.Invoices.Count > 40 Then
                                                    ' XOKNET 12.06.2015 - No freetext if FIK or Giropayment
                                                    '08.06.2021 - Changed test from If oPayment.PayCode <> "301" Then
                                                    '   to IF Not IsFIK(oPayment.PayCode)
                                                    If Not IsFIK(oPayment.PayCode) Then   'FIK
                                                        i = 27
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            i = i + 1
                                                            If i > 67 Then
                                                                Exit For
                                                            End If
                                                            sLine = sLine & Chr(34) & Trim$(Left$(oFreetext.Text, 35)) & Chr(34) & ","
                                                        Next
                                                        If i > 67 Then
                                                            Exit For
                                                        End If
                                                    End If
                                                    ' 13.05.2014 run one payment at a time
                                                    '                                    Else
                                                    '                                        ' Not more than 40 invoices, total all into one payment!
                                                    '                                        i = 27
                                                    '                                        For Each oInvoice In oPayment.Invoices
                                                    '                                            For Each oFreeText In oInvoice.Freetexts
                                                    '                                                i = i + 1
                                                    '                                                If i > 67 Then
                                                    '                                                    Exit For
                                                    '                                                End If
                                                    '                                                sLine = sLine & Chr(34) & Trim$(Left$(oFreeText.Text, 35)) & Chr(34) & ","
                                                    '                                            Next
                                                    '                                            If i > 67 Then
                                                    '                                                Exit For
                                                    '                                            End If
                                                    '                                        Next
                                                    '                                        ' remove last comma
                                                    '                                        If Right$(sLine, 1) = "," Then
                                                    '                                            sLine = Left$(sLine, Len(sLine) - 1)
                                                    '                                        End If
                                                    '                                    End If

                                                    If Len(sLine) > 0 Then
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    oPayment.Exported = True
                                                    oInvoice.Exported = True
                                                    bPaymentExported = True
                                                    '                                    If oPayment.PayCode <> "301" And oPayment.PayCode <> "250" Then
                                                    '                                        If oPayment.Invoices.Count < 41 Then
                                                    '                                            ' if not FIK, or more than 40 invoices, finished
                                                    '                                            Exit For
                                                    '                                        End If
                                                    '                                    Else
                                                    '                                        ' If FIK, or Uten melding, may have more invoices to handle
                                                    '                                    End If

                                                End If 'If Not oPayment.Invoices(iInvoiceCounter).Exported Then

                                            Next iInvoiceCounter

                                        Else
                                            ' International

                                            '-------- fetch content of each payment ---------
                                            sFromAccount = Trim$(oPayment.I_Account)
                                            sToAccount = Trim$(oPayment.E_Account)
                                            sName = Trim$(Left$(oPayment.E_Name, 35))
                                            sOwnRef = Trim$(Left$(oPayment.REF_Own, 20))

                                            ' TODO Visma - se p� sammensl�ing av poster slik som for innland
                                            ' sammenlign f�rst med hvordan ZData gj�r det.

                                            For iInvoiceCounter = 1 To oPayment.Invoices.Count
                                                oInvoice = oPayment.Invoices.Item(iInvoiceCounter)

                                                ' XNET 13.05.2014
                                                ' Critical change to avoid exporing cancelled payments
                                                If Not oInvoice.Exported Then

                                                    nAmount = oPayment.Invoices(iInvoiceCounter).MON_InvoiceAmount
                                                    sDate = Right$(oPayment.DATE_Payment, 2) & Mid$(oPayment.DATE_Payment, 5, 2) & Mid$(oPayment.DATE_Payment, 1, 4) 'skal v�re DDMMYYYY

                                                    ' Start writing to file, one payment pr line
                                                    sLine = Chr(34) & "CMUO" & Chr(34) & ","
                                                    sLine = sLine & Chr(34) & sFromAccount & Chr(34) & ","          '2 Fra konto
                                                    sLine = sLine & Chr(34) & sToAccount & Chr(34) & ","            '3 Til konto/CHECK/Kortart+Kreditornr
                                                    sLine = sLine & Chr(34) & oPayment.MON_InvoiceCurrency & Chr(34) & ","            '4 Valuta
                                                    sLine = sLine & Chr(34) & Format(nAmount / 100, "###0.00") & Chr(34) & ","             '5 Bel�p
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","                    '6 Valutakode for motverdi
                                                    sLine = sLine & Chr(34) & sDate & Chr(34) & ","                 '7 Betalingsdato DDMMYYYY

                                                    If oPayment.Priority Then
                                                        ' Express
                                                        sLine = sLine & Chr(34) & "3" & Chr(34) & ","     '8 Betalingstype
                                                    ElseIf EmptyString(oPayment.E_Account) Then
                                                        ' Check
                                                        sLine = sLine & Chr(34) & "" & Chr(34) & ","     '8 Betalingstype
                                                    Else
                                                        ' Normal
                                                        sLine = sLine & Chr(34) & "1" & Chr(34) & ","     '8 Betalingstype
                                                    End If
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.E_Name) & Chr(34) & ","  ' 9-12 Name and address
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr1) & Chr(34) & ","  ' 9-12 Name and address
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr2) & Chr(34) & ","  ' 9-12 Name and address
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.E_Adr3) & Chr(34) & ","  ' 9-12 Name and address

                                                    sLine = sLine & Chr(34) & Trim$(oPayment.BANK_Name) & Chr(34) & ","  ' 13-16 Bank name/address
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.BANK_Adr1) & Chr(34) & ","  ' 13-16 Bank name/address
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.BANK_Adr2) & Chr(34) & ","  ' 13-16 Bank name/address
                                                    sLine = sLine & Chr(34) & Trim$(oPayment.BANK_Adr3) & Chr(34) & ","  ' 13-16 Bank name/address

                                                    If oPayment.MON_ChargeMeDomestic = True And oPayment.MON_ChargeMeAbroad = False Then
                                                        ' Shared
                                                        sLine = sLine & Chr(34) & "1" & Chr(34) & ","     '17 Costs
                                                    ElseIf oPayment.MON_ChargeMeDomestic = True And oPayment.MON_ChargeMeAbroad = True Then
                                                        ' Sender pays
                                                        sLine = sLine & Chr(34) & "2" & Chr(34) & ","     '17 Costs
                                                    Else
                                                        ' Beneficiary pays
                                                        sLine = sLine & Chr(34) & "3" & Chr(34) & ","     '17 Costs
                                                    End If
                                                    ' Melding til mottaker, 4 felt a 35
                                                    ' Test om vi har inntil 4 freetexter totalt
                                                    If oInvoice.Freetexts.Count < 5 Then
                                                        sLine = sLine & Chr(34) & Trim$(oInvoice.Freetexts(1).Text) & Chr(34) & ","     '18-21 Melding til mottaker
                                                        If oInvoice.Freetexts.Count > 1 Then
                                                            sLine = sLine & Chr(34) & Trim$(oInvoice.Freetexts(2).Text) & Chr(34) & ","     '18-21 Melding til mottaker
                                                        Else
                                                            sLine = sLine & Chr(34) & "" & Chr(34) & ","     '18-21 Melding til mottaker
                                                        End If
                                                        If oInvoice.Freetexts.Count > 2 Then
                                                            sLine = sLine & Chr(34) & Trim$(oInvoice.Freetexts(3).Text) & Chr(34) & ","     '18-21 Melding til mottaker
                                                        Else
                                                            sLine = sLine & Chr(34) & "" & Chr(34) & ","     '18-21 Melding til mottaker
                                                        End If
                                                        If oInvoice.Freetexts.Count > 3 Then
                                                            sLine = sLine & Chr(34) & Trim$(oInvoice.Freetexts(4).Text) & Chr(34) & ","     '18-21 Melding til mottaker
                                                        Else
                                                            sLine = sLine & Chr(34) & "" & Chr(34) & ","     '18-21 Melding til mottaker
                                                        End If
                                                    Else
                                                        ' more than 4 freetextlines
                                                        sTmp = ""
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sTmp = sTmp & Trim$(oFreetext.Text)
                                                        Next oFreetext
                                                        ' split freetext in 4 fields; each 35
                                                        sLine = sLine & Chr(34) & Left$(sTmp, 35) & Chr(34) & ","    '18 18-21 Melding til mottaker
                                                        sTmp = Mid$(sTmp, 36)
                                                        sLine = sLine & Chr(34) & Left$(sTmp, 35) & Chr(34) & ","    '19  18-21 Melding til mottaker
                                                        sTmp = Mid$(sTmp, 36)
                                                        sLine = sLine & Chr(34) & Left$(sTmp, 35) & Chr(34) & ","    '20  18-21 Melding til mottaker
                                                        sTmp = Mid$(sTmp, 36)
                                                        sLine = sLine & Chr(34) & Left$(sTmp, 35) & Chr(34) & ","    '21  18-21 Melding til mottaker
                                                    End If

                                                    sLine = sLine & Chr(34) & Left$(oPayment.NOTI_NotificationMessageToBank, 35) & Chr(34) & ","  ' 22 Melding til Danske Bank
                                                    sLine = sLine & Chr(34) & Mid$(oPayment.NOTI_NotificationMessageToBank, 36, 35) & Chr(34) & ","  ' 23 Melding til Danske Bank
                                                    sLine = sLine & Chr(34) & Mid$(oPayment.NOTI_NotificationMessageToBank, 71, 35) & Chr(34) & ","  ' 24 Melding til Danske Bank

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '25 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '26 Not in use

                                                    If oPayment.FRW_ForwardContractRate > 0 Then
                                                        sLine = sLine & Chr(34) & Format(oPayment.FRW_ForwardContractRate / 100, "####.0000") & Chr(34) & ","  '27 Forward rate
                                                    Else
                                                        sLine = sLine & Chr(34) & "" & Chr(34) & ","     '27 Forward rate
                                                    End If
                                                    If oPayment.ERA_ExchRateAgreed > 0 Then
                                                        sLine = sLine & Chr(34) & Format(oPayment.ERA_ExchRateAgreed / 100, "####.0000") & Chr(34) & ","  '28 Agreed rate
                                                    Else
                                                        sLine = sLine & Chr(34) & "" & Chr(34) & ","     '28 Agreed rate
                                                    End If
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '29 Not in use
                                                    sLine = sLine & Chr(34) & oPayment.E_CountryCode & Chr(34) & ","     '30 Landkode mottaker

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '31 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '32 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '33 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '34 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '35 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '36 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '37 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '38 Not in use

                                                    ' XNET 09.05.2014
                                                    If bThisIsVismaIntegrator Then
                                                        sLine = sLine & Chr(34) & Trim$(Left$(oPayment.E_Name, 20)) & Chr(34) & "," '20 tekst p� betalers kontoutskrift
                                                    Else
                                                        sLine = sLine & Chr(34) & Trim$(Left$(oPayment.REF_Own, 20)) & Chr(34) & ","    '39 Senders ref
                                                    End If

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '40 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '41 Not in use

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '42 krysset sjekk
                                                    sLine = sLine & Chr(34) & "1" & Chr(34) & ","     '43 send sjekk til (mottaker)

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '44 Not in use
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '45 Not in use

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '46 Vekslingstype, forel�pig ikke i bruk
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '47 Branch for � hente sjekk, ikke i bruk
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '48 Kostnad for sjekk forel�pig ikke i bruk

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '49 Ikke i bruk
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '50 Ikke i bruk

                                                    sLine = sLine & Chr(34) & Trim$(oPayment.BANK_SWIFTCode) & Chr(34) & ","     '51 Swift/BIC

                                                    If oPayment.BANK_BranchType > BabelFiles.BankBranchType.SWIFT Then
                                                        Select Case oPayment.BANK_BranchType
                                                            Case BabelFiles.BankBranchType.Bankleitzahl_Austria
                                                                sLine = sLine & Chr(34) & "AT" & Chr(34) & ","     '52 bankcode
                                                            Case BabelFiles.BankBranchType.Bankleitzahl
                                                                sLine = sLine & Chr(34) & "BL" & Chr(34) & ","     '52 bankcode
                                                            Case BabelFiles.BankBranchType.CC
                                                                sLine = sLine & Chr(34) & "CC" & Chr(34) & ","     '52 bankcode
                                                            Case BabelFiles.BankBranchType.Chips
                                                                sLine = sLine & Chr(34) & "CH" & Chr(34) & ","     '52 bankcode
                                                            Case BabelFiles.BankBranchType.Fedwire
                                                                sLine = sLine & Chr(34) & "FW" & Chr(34) & ","     '52 bankcode
                                                            Case BabelFiles.BankBranchType.SortCode
                                                                sLine = sLine & Chr(34) & "SC" & Chr(34) & ","     '52 bankcode
                                                            Case Else
                                                                sLine = sLine & Chr(34) & "" & Chr(34) & ","     '52 bankcode
                                                        End Select
                                                    Else
                                                        ' Bankcode
                                                        Select Case oPayment.BANK_CountryCode
                                                            Case "AT"
                                                                sLine = sLine & Chr(34) & "AT" & Chr(34) & ","     '52 bankcode
                                                            Case "DE"
                                                                sLine = sLine & Chr(34) & "BL" & Chr(34) & ","     '52 bankcode
                                                            Case "CA"
                                                                sLine = sLine & Chr(34) & "CC" & Chr(34) & ","     '52 bankcode
                                                            Case "US"
                                                                sLine = sLine & Chr(34) & "FW" & Chr(34) & ","     '52 bankcode
                                                            Case "GB", "IE"
                                                                sLine = sLine & Chr(34) & "SC" & Chr(34) & ","     '52 bankcode
                                                            Case "SW"
                                                                sLine = sLine & Chr(34) & "SW" & Chr(34) & ","     '52 bankcode
                                                            Case Else
                                                                sLine = sLine & Chr(34) & "" & Chr(34) & ","     '52 bankcode
                                                        End Select
                                                    End If

                                                    sLine = sLine & Chr(34) & Trim$(oPayment.BANK_BranchNo) & Chr(34) & ","     '53 Bank number
                                                    sLine = sLine & Chr(34) & oPayment.BANK_CountryCode & Chr(34) & ","  '54 bank countrycode

                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '55 Ikke i bruk
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '56 Ikke i bruk

                                                    sLine = sLine & Chr(34) & Trim$(oPayment.REF_Own) & Chr(34) & ","     '57 Technical reference
                                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","     '58 Electronic advice (set to None)

                                                    If Len(sLine) > 0 Then
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    oPayment.Exported = True
                                                    oInvoice.Exported = True
                                                    bPaymentExported = True
                                                End If 'If Not oInvoice.Exported Then
                                            Next iInvoiceCounter
                                        End If ' <> "I"
                                    End If 'If oPayment.StatusCode <> "00" then
                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next


            '22.10.2018 - Changed the closing of objects
            'It is vital to delete the exportfile when empty, f.ex. when there are only payments with statuscode = "01"
            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not bPaymentExported Then
                If bFileInitiated Then
                    If Not oFs Is Nothing Then
                        oFs.DeleteFile((sFilenameOut))
                    End If
                End If
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDanskeBankCMBOCMUO" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDanskeBankCMBOCMUO = True

    End Function
    ' XOKNET 31.10.2012 - new function
    ' XNET 26.11.2013 added as string for sFilenameOut
    Function WriteDanskeBankColDebitor(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean

        '"COLDEBITOR","01","6755166240"," ","3136835","201","20080915","20080915","","","","","";
        '"COLDEBITOR","01","6755166240"," ","3137281","201","20080915","20080915","","","","","";
        '"COLDEBITOR","01","6755166240"," ","3137983","201","20080915","20080915","","","","","";

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim sKreditorNo As String
        '
        ' create an outputfile
        Try
            bAppendFile = False


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    bExportoPayment = True
                                End If

                                If bExportoPayment Then

                                    ' Start writing to file, one payment pr line
                                    sLine = Chr(34) & "COLDEBITOR" & Chr(34) & ","                                  '1.Function, fixed
                                    sLine = sLine & Chr(34) & "01" & Chr(34) & ","                                  '2 Version, fixed
                                    'If EmptyString(oPayment.VB_Profile.FileSetups(oPayment.VB_Profile.FileSetups(iFormat_ID).FileSetupOut).AdditionalNo) Then
                                    If EmptyString(oPayment.VB_Profile.FileSetups(iFormat_ID).AdditionalNo) Then
                                        Err.Raise(17432, "WriteDanskeBankColDebitor", "Kreditornummer er ikke angitt i oppsettet i BabelBank")
                                    Else
                                        sKreditorNo = Trim$(oPayment.VB_Profile.FileSetups(iFormat_ID).AdditionalNo)
                                    End If
                                    sLine = sLine & Chr(34) & sKreditorNo & Chr(34) & "," '3 Kreditornr
                                    sLine = sLine & Chr(34) & " " & Chr(34) & ","                                   '4 Testmarker, blank
                                    If oPayment.Invoices.Count > 0 Then
                                        sLine = sLine & Chr(34) & oPayment.Invoices(1).CustomerNo & Chr(34) & "," '5 Debitor kundenr
                                    Else
                                        sLine = sLine & Chr(34) & "UKJENT" & Chr(34) & ","       '5 Debitor kundenrnr
                                    End If
                                    sLine = sLine & Chr(34) & "201" & Chr(34) & ","                                 '6 Collectiontype, fixed 201 for Avtalegiro NO
                                    sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & ","                 '7 Establishment date YYYYMMDD
                                    sLine = sLine & Chr(34) & oPayment.DATE_Value & Chr(34) & ","                   '8 Effective date YYYYMMDD
                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","                                    '9 Expiry date YYYYMMDD, not in use so far
                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","                                    '10 Debtors ID, not in use so far
                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","                                    '11 Debtors accountno, not in use so far
                                    sLine = sLine & Chr(34) & "" & Chr(34) & ","                                    '12 Amount limit, not in use for avtalegiro
                                    sLine = sLine & Chr(34) & "" & Chr(34)                                          '13 Periodecode, not in use so far
                                    ' add an extra ; at end
                                    sLine = sLine & ";"

                                    If Len(sLine) > 0 Then
                                        oFile.WriteLine(sLine)
                                    End If
                                    oPayment.Exported = True

                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDanskeBankColDebitor" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDanskeBankColDebitor = True

    End Function

    Function WriteDanskeBankCMFI(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean
        ' Function added 01.06.2016
        ' Local payments Finland
        '"CMFI","34499400001396","34499400001701","22,00","","EUR","F","U","","","","","J","Field 14, Letter to sender line 1","Field 15, Line 2","","","","","Field 20,Text sender","","","","","","","Field 27, Message to beneficiary","Field 28, Line 2",
        '"CMFI","34499400001396","34499400001701","45,00","","EUR","F","R","","","","","","","","","","","","","","","","123456780",
        '"CMFI","34499400001396","34499400001701","60,00","","EUR","F","S","","","","","","","","","","","","","","","","","","","1234567890","2345","08091999",
        '"CMFI","34499400001396","34499400001701","70,00","","EUR","L","","10","","","","","","","","","","","Field 20,Text sender",
        '
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim nAmount As Double
        Dim sFromAccount As String, sToAccount As String, sShortNotification As String
        Dim i As Integer
        Dim sAmount As String
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim sERH As String
        Dim sTmp As String

        '
        ' create an outputfile
        Try
            bAppendFile = False


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If oPayment.PayType <> "I" Then
                                        ' Domestic payments only !

                                        '-------- fetch content of each payment ---------
                                        sFromAccount = Trim$(oPayment.I_Account)
                                        sToAccount = Trim$(oPayment.E_Account)
                                        sName = Trim$(Left$(oPayment.E_Name, 35))
                                        sOwnRef = Trim$(Left$(oPayment.REF_Own, 20))

                                        For iInvoiceCounter = 1 To oPayment.Invoices.Count
                                            ' Important to avoid exporing cancelled payments
                                            oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
                                            If Not oInvoice.Exported Then
                                                nAmount = oInvoice.MON_InvoiceAmount
                                                sDate = Right$(oPayment.DATE_Payment, 2) & Mid$(oPayment.DATE_Payment, 5, 2) & Left$(oPayment.DATE_Payment, 4) 'skal v�re DDMMYYYY

                                                ' Start writing to file, one payment pr line
                                                sLine = Chr(34) & "CMFI" & Chr(34) & ","
                                                sLine = sLine & Chr(34) & sFromAccount & Chr(34) & ","          '2 Fra konto
                                                ' AccountNo FI is either 14
                                                ' Or, there may be an IBANno, 18 chars
                                                If Not IsNumeric(Left$(sToAccount, 2)) Then
                                                    If Len(sToAccount) < 14 Then
                                                        sToAccount = Left$(sToAccount, 4) & New String("0", 14 - sToAccount.Length) & Right$(sToAccount, Len(sToAccount) - 4)
                                                    End If
                                                End If
                                                sLine = sLine & Chr(34) & sToAccount & Chr(34) & ","            '3 Til konto/CHECK/Kortart+Kreditornr
                                                sLine = sLine & Chr(34) & Format(nAmount / 100, "###0.00") & Chr(34) & ","             '4 Bel�p
                                                sLine = sLine & Chr(34) & sDate & Chr(34) & ","                 '5 Betalingsdato DDMMYY
                                                sLine = sLine & Chr(34) & oPayment.MON_TransferCurrency & Chr(34) & ","  '6 Valuta
                                                ' 7 Paymenttype 
                                                ' �F� or blank = Account transfer �E� = Express transfer �L� = Salaries, pensions, etc.
                                                If oPayment.PayType = "S" Then
                                                    sLine = sLine & Chr(34) & "L" & Chr(34) & ","     '7 PaymentType
                                                ElseIf oPayment.Priority Then
                                                    sLine = sLine & Chr(34) & "E" & Chr(34) & ","     '7 PaymentType
                                                Else
                                                    sLine = sLine & Chr(34) & "F" & Chr(34) & ","     '7 PaymentType
                                                End If

                                                If IsOCR(oPayment.PayCode) Then
                                                    sLine = sLine & Chr(34) & "R" & Chr(34) & ","   ' Reference
                                                ElseIf oPayment.Structured Then
                                                    ' Meddelelse p� modtagers kontoutskrift - 404
                                                    sLine = sLine & Chr(34) & "S" & Chr(34) & ","     ' Structured
                                                ElseIf oPayment.PayType = "S" Then   ' l�nn
                                                    sLine = sLine & Chr(34) & "N" & Chr(34) & ","     '8 Meddelelsesform,  N = Ingen melding
                                                Else
                                                    ' Default med melding
                                                    sLine = sLine & Chr(34) & "U" & Chr(34) & ","
                                                End If
                                                If oPayment.PayType = "S" Then
                                                    ' 10 = Pay  ' Utvid med flere koder dersom vi f�r massebetalingscase !!
                                                    sLine = sLine & Chr(34) & "10" & Chr(34) & ","         '9 ikke i bruk, annet enn for l�nn
                                                Else
                                                    sLine = sLine & Chr(34) & Chr(34) & ","         '9 ikke i bruk, annet enn for l�nn
                                                End If

                                                sLine = sLine & Chr(34) & Chr(34) & ","         '10 ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '11 ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '12 ikke i bruk
                                                sLine = sLine & Chr(34) & "N" & Chr(34) & ","   '13 brev til avsender, alltid N
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '14 tekst p� brev til avstender, ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '15 tekst p� brev til avstender, ikke i bruk

                                                sLine = sLine & Chr(34) & Chr(34) & ","   '16 Navn/adr mottaker
                                                sLine = sLine & Chr(34) & Chr(34) & ","   '17 Navn/adr mottaker
                                                sLine = sLine & Chr(34) & Chr(34) & ","  '18 Navn/adr mottaker
                                                sLine = sLine & Chr(34) & Chr(34) & ","   '19 Navn/adr mottaker
                                                sLine = sLine & Chr(34) & Trim$(oInvoice.REF_Own) & Chr(34) & ","  '20 tekst p� betalers kontoutskrift


                                                sLine = sLine & Chr(34) & Chr(34) & ","     '21 Not in use
                                                sLine = sLine & Chr(34) & Chr(34) & ","     '22 Not in use
                                                sLine = sLine & Chr(34) & Chr(34) & ","     '23 Not in use

                                                sLine = sLine & Chr(34) & Mid$(oInvoice.Unique_Id, 3) & Chr(34) & "," '24 Betalingsid
                                                sLine = sLine & Chr(34) & Chr(34) & "," '25
                                                sLine = sLine & Chr(34) & Chr(34) & "," '26

                                                ' melding til mottaker, Felt 27-67
                                                If oPayment.Structured Then
                                                    sLine = sLine & Chr(34) & Left(oInvoice.CustomerNo, 10) & Chr(34) & "," '27 Customer ID
                                                    sLine = sLine & Chr(34) & Left(oInvoice.InvoiceNo, 15) & Chr(34) & "," '28 InvoiceNo
                                                    sTmp = oInvoice.InvoiceDate  'YYYYMMDD
                                                    sLine = sLine & Chr(34) & Mid(sTmp, 7, 2) & Mid(sTmp, 5, 2) & Left(sTmp, 4) & Chr(34) & "," '29 InvoiceDate DDMMYYYY
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '30
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '31
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '32
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '33
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '34
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '35
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '36
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '37
                                                    sLine = sLine & Chr(34) & Chr(34) & "," '38
                                                Else

                                                    If oPayment.PayCode <> "301" And oPayment.PayType <> "S" Then
                                                        ' Felt 27-38
                                                        i = 27
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            i = i + 1
                                                            If i > 38 Then
                                                                Exit For
                                                            End If
                                                            sLine = sLine & Chr(34) & Trim$(Left$(oFreetext.Text, 35)) & Chr(34) & ","
                                                        Next
                                                        If i > 38 Then
                                                            Exit For
                                                        End If
                                                    Else
                                                        ' Not structured, not U = Message
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '27
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '28
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '29
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '30
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '31
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '32
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '33
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '34
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '35
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '36
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '37
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '38

                                                    End If
                                                End If
                                                sLine = sLine & Chr(34) & Left(oInvoice.REF_Own, 35) & Chr(34) & "," '39 Technical ref
                                                sLine = sLine & Chr(34) & Chr(34) & "," '40 No electronic advise

                                                If Len(sLine) > 0 Then
                                                    oFile.WriteLine(sLine)
                                                End If
                                                oPayment.Exported = True
                                                oInvoice.Exported = True

                                            End If 'If Not oPayment.Invoices(iInvoiceCounter).Exported Then

                                        Next iInvoiceCounter

                                    Else
                                        ' International payments from FI not covered in this export

                                    End If ' <> "I"
                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDanskeBankCMFI" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDanskeBankCMFI = True

    End Function
    Function WriteDanskeBankCMSI(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String) As Boolean
        ' Function added 27.04.2018
        ' Local payments Sweden
        '"CMSI","12201234567","8754321","800,00","","SEK","G","M","","","","","","","","","","","","Felt 20, Afs. kto.","","Felt 22, Modt.ref",
        '"CMSI","12202233444","8754321","130,00","","EUR","G","U","","","","","","","","","","","","Felt 20, Afs. kto.","","","","","","","Felt 27, Meddelelse til modtager","Felt 28, Linie 2","Felt 29, Linie 3",
        '"CMSI","12201234567","8754321","900,00","","SEK","G","R","","","","","","","","","","","","Felt 20, Afs. kto.","","","","32219038042",
        '"CMSI","12201234567","1712345","700,00","","SEK","P","U","","","","","","","","","","","","Felt 20, Afs. kto.","","","","","","","Felt 27, Meddelelse til modtager","Felt 28, Linie 2","Felt 29, Linie 3",

        '
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim sName As String ', sFirstName As String, sLastName As String
        Dim nAmount As Double
        Dim sFromAccount As String, sToAccount As String, sShortNotification As String
        Dim i As Integer
        Dim sAmount As String
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim sTmp As String

        '
        ' create an outputfile
        Try

            bAppendFile = False


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    If oPayment.PayType <> "I" Then
                                        ' Domestic payments only !

                                        '-------- fetch content of each payment ---------
                                        sFromAccount = Trim$(oPayment.I_Account)
                                        sToAccount = Trim$(oPayment.E_Account)
                                        sName = Trim$(Left$(oPayment.E_Name, 35))
                                        sOwnRef = Trim$(Left$(oPayment.REF_Own, 20))

                                        For iInvoiceCounter = 1 To oPayment.Invoices.Count
                                            ' Important to avoid exporting cancelled payments
                                            oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
                                            If Not oInvoice.Exported Then

                                                If oInvoice.InvoiceNo <> "" Then
                                                    oPayment.Structured = True
                                                End If
                                                nAmount = oInvoice.MON_InvoiceAmount
                                                sDate = Right$(oPayment.DATE_Payment, 2) & Mid$(oPayment.DATE_Payment, 5, 2) & Left$(oPayment.DATE_Payment, 4) 'skal v�re DDMMYYYY

                                                ' Start writing to file, one payment pr line
                                                sLine = Chr(34) & "CMSI" & Chr(34) & ","
                                                sLine = sLine & Chr(34) & sFromAccount & Chr(34) & ","          '2 Fra konto
                                                sLine = sLine & Chr(34) & sToAccount & Chr(34) & ","            '3 Til konto/CHECK/Kortart+Kreditornr
                                                sLine = sLine & Chr(34) & Format(nAmount / 100, "###0.00") & Chr(34) & "," '4 Bel�p
                                                sLine = sLine & Chr(34) & sDate & Chr(34) & ","                 '5 Betalingsdato DDMMYY
                                                sLine = sLine & Chr(34) & oPayment.MON_TransferCurrency & Chr(34) & ","  '6 Valuta

                                                ' 7 Paymenttype 
                                                'G' = Girering via Bankgiro
                                                'P' = Girering via Postgiro
                                                'K' = Kontooverf�rsel
                                                'L' = L�n (kontooverf�rsel)
                                                'U' = Kontantudbetaling
                                                'T' = PlusGirot-t�mning
                                                If oPayment.PayType = "S" Then
                                                    sLine = sLine & Chr(34) & "L" & Chr(34) & ","     '7 L�n
                                                ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
                                                    sLine = sLine & Chr(34) & "P" & Chr(34) & ","     '7 Plusgiro
                                                ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Then
                                                    sLine = sLine & Chr(34) & "K" & Chr(34) & ","     '7 Kontooverf�rsel
                                                ElseIf sToAccount = "" Then
                                                    sLine = sLine & Chr(34) & "U" & Chr(34) & ","     '7 Kontant (check)
                                                Else
                                                    sLine = sLine & Chr(34) & "G" & Chr(34) & ","     'Default: bankgiro
                                                End If

                                                '8 Meddelelse
                                                'R' = OCR-reference
                                                'M' = Tekstreference (kort meddelelse/tekst p� kontoudskrift)
                                                'U' = Meddelelse til modtager (lang meddelelse)
                                                'N' eller blank (��) = Ingen meddelelse
                                                If IsOCR(oPayment.PayCode) Then
                                                    sLine = sLine & Chr(34) & "R" & Chr(34) & ","   ' Reference
                                                ElseIf oPayment.Structured Then
                                                    ' Meddelelse p� modtagers kontoutskrift - 404
                                                    sLine = sLine & Chr(34) & "M" & Chr(34) & ","     ' Structured
                                                ElseIf sToAccount = "" Then
                                                    ' Check= Melding
                                                    sLine = sLine & Chr(34) & "U" & Chr(34) & ","
                                                ElseIf oPayment.PayType = "S" Then   ' l�nn
                                                    sLine = sLine & Chr(34) & "N" & Chr(34) & ","     '8 Meddelelsesform,  N = Ingen melding
                                                Else
                                                    ' Default med melding
                                                    sLine = sLine & Chr(34) & "U" & Chr(34) & ","
                                                End If


                                                sLine = sLine & Chr(34) & Chr(34) & ","         '9 ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '10 ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '11 ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '12 ikke i bruk

                                                sLine = sLine & Chr(34) & "N" & Chr(34) & ","   '13 brev til avsender, alltid N
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '14 tekst p� brev til avstender, ikke i bruk
                                                sLine = sLine & Chr(34) & Chr(34) & ","         '15 tekst p� brev til avstender, ikke i bruk

                                                If oPayment.ToOwnAccount Then
                                                    sLine = sLine & Chr(34) & Chr(34) & ","   '16 Navn mottaker
                                                    sLine = sLine & Chr(34) & Chr(34) & ","   '17 Adr mottaker
                                                    sLine = sLine & Chr(34) & Chr(34) & ","   '18 Zip mottaker
                                                    sLine = sLine & Chr(34) & Chr(34) & ","   '19 By mottaker
                                                Else
                                                    sLine = sLine & Chr(34) & oPayment.E_Name & Chr(34) & ","   '16 Navn mottaker
                                                    sLine = sLine & Chr(34) & oPayment.E_Adr1 & Chr(34) & ","   '17 Adr mottaker
                                                    sLine = sLine & Chr(34) & oPayment.E_Zip & Chr(34) & ","    '18 Zip mottaker
                                                    sLine = sLine & Chr(34) & oPayment.E_City & Chr(34) & ","   '19 By mottaker
                                                End If


                                                sLine = sLine & Chr(34) & Trim$(oInvoice.REF_Own) & Chr(34) & ","  '20 tekst p� betalers kontoutskrift


                                                sLine = sLine & Chr(34) & Chr(34) & ","     '21 Not in use
                                                If oPayment.Structured Then
                                                    sLine = sLine & Chr(34) & Left(oInvoice.InvoiceNo, 15) & Chr(34) & "," '22 Tekstreference
                                                Else
                                                    sLine = sLine & Chr(34) & Chr(34) & ","     '22 Not in use
                                                End If
                                                sLine = sLine & Chr(34) & Chr(34) & ","     '23 Not in use

                                                sLine = sLine & Chr(34) & Mid$(oInvoice.Unique_Id, 3) & Chr(34) & "," '24 OCR reference
                                                sLine = sLine & Chr(34) & Chr(34) & "," '25
                                                sLine = sLine & Chr(34) & Chr(34) & "," '26

                                                ' melding til mottaker, Felt 27-56
                                                If Not oPayment.Structured Then

                                                    If oPayment.PayCode <> "301" And oPayment.PayType <> "S" Then
                                                        ' Felt 27-56
                                                        i = 27
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            i = i + 1
                                                            If i > 56 Then
                                                                Exit For
                                                            End If
                                                            sLine = sLine & Chr(34) & Trim$(Left$(oFreetext.Text, 35)) & Chr(34) & ","
                                                        Next
                                                        If i > 56 Then
                                                            Exit For
                                                        End If
                                                    Else
                                                        For i = 27 To 56
                                                            ' Not structured, not U = Message
                                                            sLine = sLine & Chr(34) & Chr(34) & "," '27-56
                                                        Next i
                                                    End If
                                                Else
                                                    For i = 27 To 56
                                                        ' Not structured, not U = Message
                                                        sLine = sLine & Chr(34) & Chr(34) & "," '27-56
                                                    Next i

                                                End If
                                                sLine = sLine & Chr(34) & Trim(Left(oInvoice.REF_Own, 35)) & Chr(34) & "," '57 Technical ref
                                                sLine = sLine & Chr(34) & Chr(34)  '58 No electronic advise

                                                If Len(sLine) > 0 Then
                                                    oFile.WriteLine(sLine)
                                                End If
                                                oPayment.Exported = True
                                                oInvoice.Exported = True

                                            End If 'If Not oPayment.Invoices(iInvoiceCounter).Exported Then

                                        Next iInvoiceCounter

                                    Else
                                        ' International payments from SE not covered in this export

                                    End If ' <> "I"
                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch
                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDanskeBankCMSI" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDanskeBankCMSI = True

    End Function

End Module
