﻿Public Class arBBView
    Private rp As Object
    Private bLandscape As Boolean = False '26.08.2019

    Friend Sub SetDocument(ByVal o As Object)
        rp = o
    End Sub
    Friend Sub SetPageOrientation_ToLandscape()
        bLandscape = True
    End Sub

    Private Sub arView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim btn As New DataDynamics.ActiveReports.Toolbar.Button
        Dim sep As New DataDynamics.ActiveReports.Toolbar.Separator
        Dim images As ImageList = ARView.Toolbar.Images
        Dim newImage As System.Drawing.Image
        Dim newSize As System.Drawing.Size


        ARView.Document = rp.Document

        Me.AutoScroll = True
        Me.AutoSize = True
        'Me.UseWaitCursor = True

        If bLandscape Then
            newSize.Width = 1380
            newSize.Height = 785
            Me.Size = newSize

            newSize.Width = 1300
            newSize.Height = 785
            Me.ARView.Size = newSize
        End If

        'http://www.datadynamics.com/forums/117903/ShowPost.aspx

        ARView.Toolbar.Tools.Item(ARView.Toolbar.Tools.Count - 3).Caption = ""
        ARView.Toolbar.Tools.Item(ARView.Toolbar.Tools.Count - 4).Caption = ""

        'Remove the TOC-button
        ARView.Toolbar.Tools.RemoveAt(1)
        ARView.Toolbar.Tools.RemoveAt(0)

        sep.Enabled = True
        sep.Id = 201

        'insert the separator at the end
        ARView.Toolbar.Tools.Insert(ARView.Toolbar.Tools.Count, sep)

        sep = Nothing

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\PDFFile_8.ico")
        newImage = Global.vbBabel.Resources.PDFFile_8
        images.Images.Add(newImage)
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 101
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = "&PDF"
        btn.ToolTip = "Export To PDF"
        ARView.Toolbar.Tools.Add(btn)

        newImage = Nothing
        btn = Nothing

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\HTML.ico")
        newImage = Global.vbBabel.Resources.HTML
        images.Images.Add(newImage)
        btn = New DataDynamics.ActiveReports.Toolbar.Button
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 102
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = "&HTML"
        btn.ToolTip = "Export To HTML"
        ARView.Toolbar.Tools.Add(btn)

        newImage = Nothing
        btn = Nothing

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\MSWord.ico")
        newImage = Global.vbBabel.Resources.MSWord
        images.Images.Add(newImage)
        btn = New DataDynamics.ActiveReports.Toolbar.Button
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 103
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = "&RTF"
        btn.ToolTip = "Export To RTF"
        ARView.Toolbar.Tools.Add(btn)

        newImage = Nothing
        btn = Nothing

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\MSExcel.ico")
        newImage = Global.vbBabel.Resources.MSExcel
        images.Images.Add(newImage)
        btn = New DataDynamics.ActiveReports.Toolbar.Button
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 104
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = "&Excel"
        btn.ToolTip = "Export To Excel"
        ARView.Toolbar.Tools.Add(btn)

        newImage = Nothing
        btn = Nothing

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\Text.ico")
        newImage = Global.vbBabel.Resources.Text
        images.Images.Add(newImage)
        btn = New DataDynamics.ActiveReports.Toolbar.Button
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 105
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = "&Text"
        btn.ToolTip = "Export To Text"
        ARView.Toolbar.Tools.Add(btn)

        newImage = Nothing
        btn = Nothing

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\Tiff.jpg")
        newImage = Global.vbBabel.Resources.Tiff
        images.Images.Add(newImage)
        btn = New DataDynamics.ActiveReports.Toolbar.Button
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 106
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = "T&IFF"
        btn.ToolTip = "Export To Tiff"
        ARView.Toolbar.Tools.Add(btn)

        'newImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\graphics\DOOREXI1.ICO")
        newImage = Global.vbBabel.Resources.DOOREXI1
        images.Images.Add(newImage)
        btn = New DataDynamics.ActiveReports.Toolbar.Button
        btn.ImageIndex = images.Images.Count - 1
        btn.Id = 107
        btn.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.TextAndIcon
        btn.Caption = ""
        btn.ToolTip = Replace(LRS(55007), "&", "")
        ARView.Toolbar.Tools.Add(btn)

        newImage = Nothing
        btn = Nothing

    End Sub
    Private Sub arView_ToolClick(ByVal sender As Object, ByVal e As DataDynamics.ActiveReports.Toolbar.ToolClickEventArgs) Handles ARView.ToolClick

        Select e.Tool.Id

            Case 107
                tbEnd()

            Case 101
                tbPDFExport()

            Case 102
                tbHTMLExport()

            Case 103
                tbRTFExport()

            Case 104
                tbExcelExport()

            Case 105
                tbTextExport()

            Case 106
                tbTIFFExport()

        End Select
    End Sub
    Private Sub tbEnd()
        Me.Hide()
    End Sub
    Private Sub tbPDFExport()
        Dim pdfExport As New DataDynamics.ActiveReports.Export.Pdf.PdfExport

        pdfExport = New DataDynamics.ActiveReports.Export.Pdf.PdfExport

        Me.dlgFileNameSave.FileName = ""
        Me.dlgFileNameSave.Filter = "Portable Document Format" & " (*.pdf)|*.pdf"
        Me.dlgFileNameSave.DefaultExt = ".pdf"
        Me.dlgFileNameSave.ShowDialog()
        If Me.dlgFileNameSave.FileName <> "" Then
            pdfExport.Export(Me.ARView.Document, Me.dlgFileNameSave.FileName)
        End If

        pdfExport = Nothing

    End Sub
    Private Sub tbHTMLExport()
        Dim htmExport As New DataDynamics.ActiveReports.Export.Html.HtmlExport

        htmExport = New DataDynamics.ActiveReports.Export.Html.HtmlExport

        Me.dlgFileNameSave.FileName = ""
        Me.dlgFileNameSave.Filter = "HTML Format" & " (*.htm)|*.htm"
        Me.dlgFileNameSave.DefaultExt = ".htm"
        Me.dlgFileNameSave.ShowDialog()
        If Me.dlgFileNameSave.FileName <> "" Then
            htmExport.Export(Me.ARView.Document, Me.dlgFileNameSave.FileName)
        End If

        htmExport = Nothing

    End Sub
    Private Sub tbRTFExport()
        Dim rtfExport As New DataDynamics.ActiveReports.Export.Rtf.RtfExport

        rtfExport = New DataDynamics.ActiveReports.Export.Rtf.RtfExport

        Me.dlgFileNameSave.FileName = ""
        Me.dlgFileNameSave.Filter = "Richt Text Format" & " (*.rtf)|*.rtf"
        Me.dlgFileNameSave.DefaultExt = ".rtf"
        Me.dlgFileNameSave.ShowDialog()
        If Me.dlgFileNameSave.FileName <> "" Then
            rtfExport.Export(Me.ARView.Document, Me.dlgFileNameSave.FileName)
        End If

        rtfExport = Nothing

    End Sub
    Private Sub tbExcelExport()
        Dim xlsExport As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        xlsExport = New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Me.dlgFileNameSave.FileName = ""
        Me.dlgFileNameSave.Filter = "Microsoft Excel Format" & " (*.xls)|*.xls"
        Me.dlgFileNameSave.DefaultExt = ".xls"
        Me.dlgFileNameSave.ShowDialog()
        If Me.dlgFileNameSave.FileName <> "" Then
            xlsExport.Export(Me.ARView.Document, Me.dlgFileNameSave.FileName)
        End If

        xlsExport = Nothing

    End Sub
    Private Sub tbTextExport()
        Dim txtExport As New DataDynamics.ActiveReports.Export.Text.TextExport

        txtExport = New DataDynamics.ActiveReports.Export.Text.TextExport

        Me.dlgFileNameSave.FileName = ""
        Me.dlgFileNameSave.Filter = "Text Format" & " (*.txt)|*.txt"
        Me.dlgFileNameSave.DefaultExt = ".txt"
        Me.dlgFileNameSave.ShowDialog()
        If Me.dlgFileNameSave.FileName <> "" Then
            txtExport.Export(Me.ARView.Document, Me.dlgFileNameSave.FileName)
        End If

        txtExport = Nothing

    End Sub
    Private Sub tbTIFFExport()
        Dim tifExport As New DataDynamics.ActiveReports.Export.Tiff.TiffExport

        tifExport = New DataDynamics.ActiveReports.Export.Tiff.TiffExport

        Me.dlgFileNameSave.FileName = ""
        Me.dlgFileNameSave.Filter = "Tagged Image File Format " & " (*.tif)|*.tif"
        Me.dlgFileNameSave.DefaultExt = ".tif"
        Me.dlgFileNameSave.ShowDialog()
        If Me.dlgFileNameSave.FileName <> "" Then
            tifExport.Export(Me.ARView.Document, Me.dlgFileNameSave.FileName)
        End If

        tifExport = Nothing

    End Sub

End Class