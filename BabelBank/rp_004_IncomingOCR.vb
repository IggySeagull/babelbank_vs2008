Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document

'Things to do when implementing this report in BB
'Search for 'LRS(, and replace the text with the LRS
'Search for vbBabel.MatchType. and replace the number stated with the correct Enum

Public Class rp_004_IncomingOCR

    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String

    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim bShowFilename As Boolean

    Dim oBabelFiles As vbBabel.BabelFiles
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim bFirstTime As Boolean
    Dim bClientSpecificReport As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim sClientNumber As String
    Dim bSQLServer As Boolean = False
    Dim bIncludeOCR As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean



    Private Sub rp_004_IncomingOCR_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Fields.Add("BreakField")
        If Not bReportFromDatabase Then
            Fields.Add("Company_ID")
            Fields.Add("Babelfile_ID")
            Fields.Add("Batch_ID")
            Fields.Add("Payment_ID")
            Fields.Add("Invoice_ID")
            Fields.Add("Filename")
            Fields.Add("DATE_Production")
            Fields.Add("Paycode")
            Fields.Add("I_Account")
            Fields.Add("E_Account")
            Fields.Add("E_Name")
            Fields.Add("REF_Bank1")
            Fields.Add("Unique_ID")
            Fields.Add("MON_TransferCurrency")
            Fields.Add("MON_TransferredAmount")
            Fields.Add("Client")
        End If

    End Sub

    Private Sub rp_004_IncomingOCR_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If
    End Sub

    Private Sub rp_004_IncomingOCR_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        bFirstTime = True
        bShowBatchfooterTotals = True

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False
                bShowFilename = False

            Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = True

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

        End Select

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = "OCR " & LRS(40040)   ' OCR Payments - "OCR Betalinger" 
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        'Set the caption of the labels

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 
        Me.lblE_Account.Text = LRS(40013) '"Kontonr.:" 
        Me.lblE_Name.Text = LRS(40071) '"Navn:" 
        Me.lblREF_Bank1.Text = LRS(40045) '"Blankettref.:" 
        Me.lblKID.Text = LRS(40046) '"KID:" 
        Me.lblCurrency.Text = LRS(40007) '"Valuta:" 
        Me.lblAmount.Text = LRS(40021) '"Bel�p:" 

        'grBatchFooter
        Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

        ' Special cases;
        '*******VB6 Code

        ''Me.lblFileSum = LRS(40028)

        sOldI_Account = ""
        sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 

        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If
        If Not bShowFilename Then
            Me.txtFilename.Visible = False
        End If

        Me.txtBatchfooterAmount.Value = 0
        Me.txtBatchfooterNoofPayments.Value = 0
        Me.txtTotalAmount.Value = 0
        Me.txtTotalNoOfPayments.Value = 0

    End Sub
    Private Sub rp_004_IncomingOCR_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String
        Dim bContinue As Boolean
        Dim bFoundClient As Boolean

        ' Counters for items in the different Babel collections
        Static iBabelFiles As Integer
        Static iLastUsedBabelFilesItem As Integer
        Static iBatches As Integer
        Static iPayments As Integer
        Static iInvoices As Integer
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String

        If Not bReportFromDatabase Then
            iPayments = iPayments + 1

            If bFirstTime Then
                bFirstTime = False
                iBabelFiles = 1
                iBatches = 1
                iPayments = 1
                iInvoices = 1
            End If

            ' Spin through collections to find next suitbable record before any other
            ' proecessing is done
            ' Position to correct items in collections
            Do Until iBabelFiles > oBabelFiles.Count
                ' Try to set as few times as possible
                If oBabelFile Is Nothing Then
                    oBabelFile = oBabelFiles(iBabelFiles)
                    iLastUsedBabelFilesItem = iBabelFiles
                Else
                    'If iBabelFiles <> oBabelFile.Index Then
                    If iLastUsedBabelFilesItem <> iBabelFiles Then
                        iBatches = 1
                        iPayments = 1
                        iInvoices = 1
                        ' Added 19.05.2004
                        xCreditAccount = "xxxx"
                        xDate = "x"

                        oBabelFile = oBabelFiles(iBabelFiles)
                        iLastUsedBabelFilesItem = iBabelFiles  'oBabelFile.Index
                    End If
                End If

                Do Until iBatches > oBabelFile.Batches.Count
                    oBatch = oBabelFile.Batches(iBatches)

                    ' Find the next payment in this batch which is original
                    Do Until iPayments > oBatch.Payments.Count
                        oPayment = oBatch.Payments.Item(iPayments)
                        ' 25.11.2015 - dette er feil, navn kommer i kolonnen foran !!
                        'If oBabelFile.ImportFormat = 501 Or sSpecial = "CANALDIGITAL_SE" Then
                        If sSpecial = "CANALDIGITAL_SE" Then
                            ' Show name when CREMULfile
                            Me.lblREF_Bank1.Text = LRS(60086) ' Name
                        End If
                        ' Show ONLY OCR-payments (510)
                        'Changed by Kjell 26.04.2005 because there are more sorts of OCR's
                        'If oPayment.PayCode <> "510" Then

                        'Sometimes there are restrictions on which payments to export -  Not implemented in this report
                        'If bIncludeOCR Then
                        '    bExportPayment = True
                        'Else
                        '    If IsOCR(oPayment.PayCode) Then
                        '        bExportPayment = True
                        '    Else
                        '        bExportPayment = False
                        '    End If
                        'End If

                        If Val(oPayment.PayCode) < 510 Or Val(oPayment.PayCode) > 517 Then

                            'New code 10.01.2006 for CanalDigital_SE
                            If sSpecial = "CANALDIGITAL_SE" Then
                                bContinue = True
                                iInvoices = 1
                            Else
                                'Normal situation
                                iPayments = iPayments + 1
                                iInvoices = oPayment.Invoices.Count + 1 ' force another tour in loop
                                bContinue = False
                            End If
                        Else
                            bContinue = True
                            iInvoices = 1
                        End If

                        bFoundClient = False
                        If Not EmptyString(sClientNumber) Then
                            If oPayment.VB_ClientNo = sClientNumber Then
                                bFoundClient = True
                            End If
                        Else
                            bFoundClient = True
                        End If

                        'If Not EmptyString(oPayment.VB_ClientNo) Then
                        '    bFoundClient = bFoundClient
                        'End If

                        ' Added 12.11.07
                        ' Check if it is a clientseparated report, and if so, correct client
                        If bFoundClient Then

                            ' Postiton to a, final invoice
                            ' Find the next invoice in this payment which is original;
                            Do Until iInvoices > oPayment.Invoices.Count
                                oInvoice = oPayment.Invoices.Item(iInvoices)

                                If oInvoice.MATCH_Original Then
                                    bContinue = True
                                    Exit Do
                                End If
                                iInvoices = iInvoices + 1
                            Loop 'iInvoices
                        Else
                            bContinue = False
                            iPayments = iPayments + 1
                            iInvoices = 1
                        End If 'If frmViewer.CorrectReportClient(oPayment) Then

                        If bContinue Then
                            Exit Do
                        End If

                    Loop 'iPayments
                    If bContinue Then
                        Exit Do
                    End If
                    iBatches = iBatches + 1
                    iPayments = 1
                Loop ' iBatches
                If bContinue Then
                    Exit Do
                End If
                iBabelFiles = iBabelFiles + 1
                iBatches = 1
            Loop ' iBabelFiles

            If iBabelFiles > oBabelFiles.Count Then
                eArgs.EOF = True
                iBabelFiles = 0 ' to reset statics next time !

                Exit Sub
            End If

            If oBabelFile.VB_ProfileInUse = True Then
                Me.Fields("Company_ID").Value = oPayment.VB_Profile.Company_ID
            Else
                Me.Fields("Company_ID").Value = "1"
            End If
            Me.Fields("Babelfile_ID").Value = oBabelFile.Index
            Me.Fields("Batch_ID").Value = oBatch.Index
            Me.Fields("Payment_ID").Value = oPayment.Index
            Me.Fields("Invoice_ID").Value = oInvoice.Index
            Me.Fields("Filename").Value = Right(oBabelFile.FilenameIn, 20)
            Me.Fields("DATE_Production").Value = oBabelFile.DATE_Production
            Me.Fields("Paycode").Value = oPayment.PayCode
            Me.Fields("I_Account").Value = oPayment.I_Account
            Me.Fields("E_Account").Value = oPayment.E_Account
            Me.Fields("E_Name").Value = oPayment.E_Name
            Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
            Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
            Me.Fields("MON_TransferCurrency").Value = oPayment.MON_TransferCurrency
            Me.Fields("MON_TransferredAmount").Value = oPayment.MON_TransferredAmount / 100
            Me.Fields("Client").Value = oPayment.VB_Client

            eArgs.EOF = False

        End If

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    ' 27.09.2017 - changed back again - we have no DATE_Payment
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                    Me.Fields("BreakField").Value = Me.Fields("BabelFile_ID").Value.ToString

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            'Store the keys for usage in the subreport(s)
            iCompany_ID = Me.Fields("Company_ID").Value
            iBabelfile_ID = Me.Fields("Babelfile_ID").Value
            iBatch_ID = Me.Fields("Batch_ID").Value
            iPayment_ID = Me.Fields("Payment_ID").Value
            iInvoice_ID = Me.Fields("Invoice_ID").Value
            'START WORKING HERE

            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            sDATE_Production = Me.Fields("DATE_Production").Value
            Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))

            If Me.Fields("Paycode").Value = "602" Then
                Me.txtE_Name.Value = "GIROMAIL"
            Else
                Me.txtE_Name.Value = Me.Fields("E_Name").Value
            End If

        End If
    End Sub

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
        Me.txtBatchfooterAmount.Value = Me.txtBatchfooterAmount.Value + Me.txtMON_P_TransferredAmount.Value
        Me.txtBatchfooterNoofPayments.Value = Me.txtBatchfooterNoofPayments.Value + 1
        Me.txtTotalAmount.Value = Me.txtTotalAmount.Value + Me.txtMON_P_TransferredAmount.Value
        Me.txtTotalNoOfPayments.Value = Me.txtTotalNoOfPayments.Value + 1

    End Sub
    Private Sub grBatchFooter_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles grBatchFooter.AfterPrint
        Me.txtBatchfooterAmount.Value = 0
        Me.txtBatchfooterNoofPayments.Value = 0
    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub grBatchHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grBatchHeader.Format
        Dim pLocation As System.Drawing.PointF
        Dim pSize As System.Drawing.SizeF

        If Me.txtFilename.Visible Then
            pLocation.X = 1.5
            pLocation.Y = 0.3
            Me.txtFilename.Location = pLocation
            pSize.Height = 0.19
            pSize.Width = 4.5
            Me.txtFilename.Size = pSize
            pLocation.X = 0.125
            pLocation.Y = 0.3
            Me.lblI_Account.Location = pLocation
            Me.lblI_Account.Value = LRS(20007)
            Me.txtI_Account.Visible = False
        End If

    End Sub
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
