<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCorrectBank
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkChargeMeAbroad As System.Windows.Forms.CheckBox
	Public WithEvents chkChargeMeDomestic As System.Windows.Forms.CheckBox
	Public WithEvents cmbCorrBank_IDType As System.Windows.Forms.ComboBox
	Public WithEvents cmbBank_IDType As System.Windows.Forms.ComboBox
	Public WithEvents txtCorrBank_ID As System.Windows.Forms.TextBox
	Public WithEvents txtCorrBank_Adr3 As System.Windows.Forms.TextBox
	Public WithEvents txtCorrBank_Adr1 As System.Windows.Forms.TextBox
	Public WithEvents txtCorrBank_Adr2 As System.Windows.Forms.TextBox
	Public WithEvents txtCorrBank_Adr4 As System.Windows.Forms.TextBox
	Public WithEvents txtBank_Adr4 As System.Windows.Forms.TextBox
	Public WithEvents txtBank_Adr2 As System.Windows.Forms.TextBox
	Public WithEvents txtBank_Adr1 As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtBank_Info As System.Windows.Forms.TextBox
	Public WithEvents txtBank_Adr3 As System.Windows.Forms.TextBox
	Public WithEvents txtBank_ID As System.Windows.Forms.TextBox
	Public WithEvents _Image2_16 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_10 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_16 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_10 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_15 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_15 As System.Windows.Forms.PictureBox
	Public WithEvents lblCorrBank_ID As System.Windows.Forms.Label
	Public WithEvents lblCorrBank_Adr3 As System.Windows.Forms.Label
	Public WithEvents lblCorrBank_IDType As System.Windows.Forms.Label
	Public WithEvents lblCorrBank_Adr1 As System.Windows.Forms.Label
	Public WithEvents lblCorrBank_Adr2 As System.Windows.Forms.Label
	Public WithEvents lblCorrBank_Adr4 As System.Windows.Forms.Label
	Public WithEvents lblIntBankInfo As System.Windows.Forms.Label
    Public WithEvents lblBenBankInfo As System.Windows.Forms.Label
	Public WithEvents lblBank_Adr4 As System.Windows.Forms.Label
	Public WithEvents lblBank_Adr2 As System.Windows.Forms.Label
	Public WithEvents lblBank_Adr1 As System.Windows.Forms.Label
	Public WithEvents lblBank_IDType As System.Windows.Forms.Label
	Public WithEvents lblErrMessage As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblBank_Info As System.Windows.Forms.Label
	Public WithEvents lblBank_Adr3 As System.Windows.Forms.Label
	Public WithEvents lblBank_ID As System.Windows.Forms.Label
	Public WithEvents _imgExclamation_4 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_7 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_12 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_0 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_13 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_13 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_11 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_11 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_9 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_9 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_8 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_8 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_7 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_6 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_6 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_4 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_5 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_3 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_5 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_3 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_1 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_1 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_0 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_12 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_14 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_14 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents Image2 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents imgExclamation As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCorrectBank))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkChargeMeAbroad = New System.Windows.Forms.CheckBox
        Me.chkChargeMeDomestic = New System.Windows.Forms.CheckBox
        Me.cmbCorrBank_IDType = New System.Windows.Forms.ComboBox
        Me.cmbBank_IDType = New System.Windows.Forms.ComboBox
        Me.txtCorrBank_ID = New System.Windows.Forms.TextBox
        Me.txtCorrBank_Adr3 = New System.Windows.Forms.TextBox
        Me.txtCorrBank_Adr1 = New System.Windows.Forms.TextBox
        Me.txtCorrBank_Adr2 = New System.Windows.Forms.TextBox
        Me.txtCorrBank_Adr4 = New System.Windows.Forms.TextBox
        Me.txtBank_Adr4 = New System.Windows.Forms.TextBox
        Me.txtBank_Adr2 = New System.Windows.Forms.TextBox
        Me.txtBank_Adr1 = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtBank_Info = New System.Windows.Forms.TextBox
        Me.txtBank_Adr3 = New System.Windows.Forms.TextBox
        Me.txtBank_ID = New System.Windows.Forms.TextBox
        Me._Image2_16 = New System.Windows.Forms.PictureBox
        Me._Image2_10 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_16 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_10 = New System.Windows.Forms.PictureBox
        Me._Image2_15 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_15 = New System.Windows.Forms.PictureBox
        Me.lblCorrBank_ID = New System.Windows.Forms.Label
        Me.lblCorrBank_Adr3 = New System.Windows.Forms.Label
        Me.lblCorrBank_IDType = New System.Windows.Forms.Label
        Me.lblCorrBank_Adr1 = New System.Windows.Forms.Label
        Me.lblCorrBank_Adr2 = New System.Windows.Forms.Label
        Me.lblCorrBank_Adr4 = New System.Windows.Forms.Label
        Me.lblIntBankInfo = New System.Windows.Forms.Label
        Me.lblBenBankInfo = New System.Windows.Forms.Label
        Me.lblBank_Adr4 = New System.Windows.Forms.Label
        Me.lblBank_Adr2 = New System.Windows.Forms.Label
        Me.lblBank_Adr1 = New System.Windows.Forms.Label
        Me.lblBank_IDType = New System.Windows.Forms.Label
        Me.lblErrMessage = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblBank_Info = New System.Windows.Forms.Label
        Me.lblBank_Adr3 = New System.Windows.Forms.Label
        Me.lblBank_ID = New System.Windows.Forms.Label
        Me._imgExclamation_4 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_7 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_12 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_0 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_13 = New System.Windows.Forms.PictureBox
        Me._Image2_13 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_11 = New System.Windows.Forms.PictureBox
        Me._Image2_11 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_9 = New System.Windows.Forms.PictureBox
        Me._Image2_9 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_8 = New System.Windows.Forms.PictureBox
        Me._Image2_8 = New System.Windows.Forms.PictureBox
        Me._Image2_7 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_6 = New System.Windows.Forms.PictureBox
        Me._Image2_6 = New System.Windows.Forms.PictureBox
        Me._Image2_4 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_5 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_3 = New System.Windows.Forms.PictureBox
        Me._Image2_5 = New System.Windows.Forms.PictureBox
        Me._Image2_3 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_1 = New System.Windows.Forms.PictureBox
        Me._Image2_1 = New System.Windows.Forms.PictureBox
        Me._Image2_0 = New System.Windows.Forms.PictureBox
        Me._Image2_12 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_14 = New System.Windows.Forms.PictureBox
        Me._Image2_14 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.Image2 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.imgExclamation = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        CType(Me._Image2_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgExclamation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkChargeMeAbroad
        '
        Me.chkChargeMeAbroad.BackColor = System.Drawing.SystemColors.Control
        Me.chkChargeMeAbroad.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkChargeMeAbroad.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkChargeMeAbroad.Location = New System.Drawing.Point(381, 344)
        Me.chkChargeMeAbroad.Name = "chkChargeMeAbroad"
        Me.chkChargeMeAbroad.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkChargeMeAbroad.Size = New System.Drawing.Size(207, 21)
        Me.chkChargeMeAbroad.TabIndex = 12
        Me.chkChargeMeAbroad.TabStop = False
        Me.chkChargeMeAbroad.Text = "60160 - Charges correspondent bank"
        Me.chkChargeMeAbroad.UseVisualStyleBackColor = False
        '
        'chkChargeMeDomestic
        '
        Me.chkChargeMeDomestic.BackColor = System.Drawing.SystemColors.Control
        Me.chkChargeMeDomestic.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkChargeMeDomestic.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkChargeMeDomestic.Location = New System.Drawing.Point(29, 344)
        Me.chkChargeMeDomestic.Name = "chkChargeMeDomestic"
        Me.chkChargeMeDomestic.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkChargeMeDomestic.Size = New System.Drawing.Size(207, 21)
        Me.chkChargeMeDomestic.TabIndex = 11
        Me.chkChargeMeDomestic.TabStop = False
        Me.chkChargeMeDomestic.Text = "60159 - Charges own bank"
        Me.chkChargeMeDomestic.UseVisualStyleBackColor = False
        '
        'cmbCorrBank_IDType
        '
        Me.cmbCorrBank_IDType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbCorrBank_IDType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbCorrBank_IDType.Enabled = False
        Me.cmbCorrBank_IDType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbCorrBank_IDType.Location = New System.Drawing.Point(498, 240)
        Me.cmbCorrBank_IDType.Name = "cmbCorrBank_IDType"
        Me.cmbCorrBank_IDType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbCorrBank_IDType.Size = New System.Drawing.Size(89, 21)
        Me.cmbCorrBank_IDType.TabIndex = 31
        Me.cmbCorrBank_IDType.Text = "<ID Type>"
        '
        'cmbBank_IDType
        '
        Me.cmbBank_IDType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbBank_IDType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbBank_IDType.Enabled = False
        Me.cmbBank_IDType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbBank_IDType.Location = New System.Drawing.Point(498, 76)
        Me.cmbBank_IDType.Name = "cmbBank_IDType"
        Me.cmbBank_IDType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBank_IDType.Size = New System.Drawing.Size(129, 21)
        Me.cmbBank_IDType.TabIndex = 30
        Me.cmbBank_IDType.Text = "<ID Type>"
        '
        'txtCorrBank_ID
        '
        Me.txtCorrBank_ID.AcceptsReturn = True
        Me.txtCorrBank_ID.BackColor = System.Drawing.SystemColors.Window
        Me.txtCorrBank_ID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCorrBank_ID.Enabled = False
        Me.txtCorrBank_ID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCorrBank_ID.Location = New System.Drawing.Point(144, 240)
        Me.txtCorrBank_ID.MaxLength = 0
        Me.txtCorrBank_ID.Name = "txtCorrBank_ID"
        Me.txtCorrBank_ID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCorrBank_ID.Size = New System.Drawing.Size(203, 20)
        Me.txtCorrBank_ID.TabIndex = 6
        Me.txtCorrBank_ID.Text = "<Bank ID>"
        '
        'txtCorrBank_Adr3
        '
        Me.txtCorrBank_Adr3.AcceptsReturn = True
        Me.txtCorrBank_Adr3.BackColor = System.Drawing.SystemColors.Window
        Me.txtCorrBank_Adr3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCorrBank_Adr3.Enabled = False
        Me.txtCorrBank_Adr3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCorrBank_Adr3.Location = New System.Drawing.Point(145, 314)
        Me.txtCorrBank_Adr3.MaxLength = 0
        Me.txtCorrBank_Adr3.Name = "txtCorrBank_Adr3"
        Me.txtCorrBank_Adr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCorrBank_Adr3.Size = New System.Drawing.Size(201, 20)
        Me.txtCorrBank_Adr3.TabIndex = 9
        Me.txtCorrBank_Adr3.Text = "<Bank Adr3>"
        '
        'txtCorrBank_Adr1
        '
        Me.txtCorrBank_Adr1.AcceptsReturn = True
        Me.txtCorrBank_Adr1.BackColor = System.Drawing.SystemColors.Window
        Me.txtCorrBank_Adr1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCorrBank_Adr1.Enabled = False
        Me.txtCorrBank_Adr1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCorrBank_Adr1.Location = New System.Drawing.Point(145, 292)
        Me.txtCorrBank_Adr1.MaxLength = 0
        Me.txtCorrBank_Adr1.Name = "txtCorrBank_Adr1"
        Me.txtCorrBank_Adr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCorrBank_Adr1.Size = New System.Drawing.Size(201, 20)
        Me.txtCorrBank_Adr1.TabIndex = 7
        Me.txtCorrBank_Adr1.Text = "<Bank Adr1>"
        '
        'txtCorrBank_Adr2
        '
        Me.txtCorrBank_Adr2.AcceptsReturn = True
        Me.txtCorrBank_Adr2.BackColor = System.Drawing.SystemColors.Window
        Me.txtCorrBank_Adr2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCorrBank_Adr2.Enabled = False
        Me.txtCorrBank_Adr2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCorrBank_Adr2.Location = New System.Drawing.Point(498, 292)
        Me.txtCorrBank_Adr2.MaxLength = 0
        Me.txtCorrBank_Adr2.Name = "txtCorrBank_Adr2"
        Me.txtCorrBank_Adr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCorrBank_Adr2.Size = New System.Drawing.Size(200, 20)
        Me.txtCorrBank_Adr2.TabIndex = 8
        Me.txtCorrBank_Adr2.Text = "<Bank Adr2>"
        '
        'txtCorrBank_Adr4
        '
        Me.txtCorrBank_Adr4.AcceptsReturn = True
        Me.txtCorrBank_Adr4.BackColor = System.Drawing.SystemColors.Window
        Me.txtCorrBank_Adr4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCorrBank_Adr4.Enabled = False
        Me.txtCorrBank_Adr4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCorrBank_Adr4.Location = New System.Drawing.Point(498, 314)
        Me.txtCorrBank_Adr4.MaxLength = 0
        Me.txtCorrBank_Adr4.Name = "txtCorrBank_Adr4"
        Me.txtCorrBank_Adr4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCorrBank_Adr4.Size = New System.Drawing.Size(202, 20)
        Me.txtCorrBank_Adr4.TabIndex = 10
        Me.txtCorrBank_Adr4.Text = "<Bank Adr4>"
        '
        'txtBank_Adr4
        '
        Me.txtBank_Adr4.AcceptsReturn = True
        Me.txtBank_Adr4.BackColor = System.Drawing.SystemColors.Window
        Me.txtBank_Adr4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBank_Adr4.Enabled = False
        Me.txtBank_Adr4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBank_Adr4.Location = New System.Drawing.Point(498, 150)
        Me.txtBank_Adr4.MaxLength = 0
        Me.txtBank_Adr4.Name = "txtBank_Adr4"
        Me.txtBank_Adr4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBank_Adr4.Size = New System.Drawing.Size(202, 20)
        Me.txtBank_Adr4.TabIndex = 4
        Me.txtBank_Adr4.Text = "<Bank Adr4>"
        '
        'txtBank_Adr2
        '
        Me.txtBank_Adr2.AcceptsReturn = True
        Me.txtBank_Adr2.BackColor = System.Drawing.SystemColors.Window
        Me.txtBank_Adr2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBank_Adr2.Enabled = False
        Me.txtBank_Adr2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBank_Adr2.Location = New System.Drawing.Point(498, 127)
        Me.txtBank_Adr2.MaxLength = 0
        Me.txtBank_Adr2.Name = "txtBank_Adr2"
        Me.txtBank_Adr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBank_Adr2.Size = New System.Drawing.Size(200, 20)
        Me.txtBank_Adr2.TabIndex = 2
        Me.txtBank_Adr2.Text = "<Bank Adr2>"
        '
        'txtBank_Adr1
        '
        Me.txtBank_Adr1.AcceptsReturn = True
        Me.txtBank_Adr1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBank_Adr1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBank_Adr1.Enabled = False
        Me.txtBank_Adr1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBank_Adr1.Location = New System.Drawing.Point(145, 127)
        Me.txtBank_Adr1.MaxLength = 0
        Me.txtBank_Adr1.Name = "txtBank_Adr1"
        Me.txtBank_Adr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBank_Adr1.Size = New System.Drawing.Size(201, 20)
        Me.txtBank_Adr1.TabIndex = 1
        Me.txtBank_Adr1.Text = "<Bank Adr1>"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(628, 435)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 13
        Me.cmdOK.TabStop = False
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'txtBank_Info
        '
        Me.txtBank_Info.AcceptsReturn = True
        Me.txtBank_Info.BackColor = System.Drawing.SystemColors.Window
        Me.txtBank_Info.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBank_Info.Enabled = False
        Me.txtBank_Info.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBank_Info.Location = New System.Drawing.Point(145, 175)
        Me.txtBank_Info.MaxLength = 0
        Me.txtBank_Info.Name = "txtBank_Info"
        Me.txtBank_Info.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBank_Info.Size = New System.Drawing.Size(550, 20)
        Me.txtBank_Info.TabIndex = 5
        Me.txtBank_Info.Text = "<Info to Bank>"
        '
        'txtBank_Adr3
        '
        Me.txtBank_Adr3.AcceptsReturn = True
        Me.txtBank_Adr3.BackColor = System.Drawing.SystemColors.Window
        Me.txtBank_Adr3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBank_Adr3.Enabled = False
        Me.txtBank_Adr3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBank_Adr3.Location = New System.Drawing.Point(145, 150)
        Me.txtBank_Adr3.MaxLength = 0
        Me.txtBank_Adr3.Name = "txtBank_Adr3"
        Me.txtBank_Adr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBank_Adr3.Size = New System.Drawing.Size(201, 20)
        Me.txtBank_Adr3.TabIndex = 3
        Me.txtBank_Adr3.Text = "<Bank Adr3>"
        '
        'txtBank_ID
        '
        Me.txtBank_ID.AcceptsReturn = True
        Me.txtBank_ID.BackColor = System.Drawing.SystemColors.Window
        Me.txtBank_ID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBank_ID.Enabled = False
        Me.txtBank_ID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBank_ID.Location = New System.Drawing.Point(144, 76)
        Me.txtBank_ID.MaxLength = 0
        Me.txtBank_ID.Name = "txtBank_ID"
        Me.txtBank_ID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBank_ID.Size = New System.Drawing.Size(203, 20)
        Me.txtBank_ID.TabIndex = 0
        Me.txtBank_ID.Text = "<Bank ID>"
        '
        '_Image2_16
        '
        Me._Image2_16.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_16.Image = CType(resources.GetObject("_Image2_16.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_16, CType(16, Short))
        Me._Image2_16.Location = New System.Drawing.Point(360, 344)
        Me._Image2_16.Name = "_Image2_16"
        Me._Image2_16.Size = New System.Drawing.Size(18, 18)
        Me._Image2_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_16.TabIndex = 32
        Me._Image2_16.TabStop = False
        Me._Image2_16.Visible = False
        '
        '_Image2_10
        '
        Me._Image2_10.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_10.Image = CType(resources.GetObject("_Image2_10.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_10, CType(10, Short))
        Me._Image2_10.Location = New System.Drawing.Point(8, 344)
        Me._Image2_10.Name = "_Image2_10"
        Me._Image2_10.Size = New System.Drawing.Size(18, 18)
        Me._Image2_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_10.TabIndex = 33
        Me._Image2_10.TabStop = False
        Me._Image2_10.Visible = False
        '
        '_imgExclamation_16
        '
        Me._imgExclamation_16.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_16.Image = CType(resources.GetObject("_imgExclamation_16.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_16, CType(16, Short))
        Me._imgExclamation_16.Location = New System.Drawing.Point(360, 344)
        Me._imgExclamation_16.Name = "_imgExclamation_16"
        Me._imgExclamation_16.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_16.TabIndex = 34
        Me._imgExclamation_16.TabStop = False
        Me._imgExclamation_16.Visible = False
        '
        '_imgExclamation_10
        '
        Me._imgExclamation_10.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_10.Image = CType(resources.GetObject("_imgExclamation_10.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_10, CType(10, Short))
        Me._imgExclamation_10.Location = New System.Drawing.Point(8, 344)
        Me._imgExclamation_10.Name = "_imgExclamation_10"
        Me._imgExclamation_10.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_10.TabIndex = 35
        Me._imgExclamation_10.TabStop = False
        Me._imgExclamation_10.Visible = False
        '
        '_Image2_15
        '
        Me._Image2_15.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_15.Image = CType(resources.GetObject("_Image2_15.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_15, CType(15, Short))
        Me._Image2_15.Location = New System.Drawing.Point(9, 378)
        Me._Image2_15.Name = "_Image2_15"
        Me._Image2_15.Size = New System.Drawing.Size(18, 18)
        Me._Image2_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_15.TabIndex = 36
        Me._Image2_15.TabStop = False
        Me._Image2_15.Visible = False
        '
        '_imgExclamation_15
        '
        Me._imgExclamation_15.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_15.Image = CType(resources.GetObject("_imgExclamation_15.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_15, CType(15, Short))
        Me._imgExclamation_15.Location = New System.Drawing.Point(9, 377)
        Me._imgExclamation_15.Name = "_imgExclamation_15"
        Me._imgExclamation_15.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_15.TabIndex = 37
        Me._imgExclamation_15.TabStop = False
        Me._imgExclamation_15.Visible = False
        '
        'lblCorrBank_ID
        '
        Me.lblCorrBank_ID.BackColor = System.Drawing.SystemColors.Control
        Me.lblCorrBank_ID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCorrBank_ID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCorrBank_ID.Location = New System.Drawing.Point(8, 242)
        Me.lblCorrBank_ID.Name = "lblCorrBank_ID"
        Me.lblCorrBank_ID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCorrBank_ID.Size = New System.Drawing.Size(117, 36)
        Me.lblCorrBank_ID.TabIndex = 29
        Me.lblCorrBank_ID.Text = "60121 - Bank ID"
        '
        'lblCorrBank_Adr3
        '
        Me.lblCorrBank_Adr3.BackColor = System.Drawing.SystemColors.Control
        Me.lblCorrBank_Adr3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCorrBank_Adr3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCorrBank_Adr3.Location = New System.Drawing.Point(8, 316)
        Me.lblCorrBank_Adr3.Name = "lblCorrBank_Adr3"
        Me.lblCorrBank_Adr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCorrBank_Adr3.Size = New System.Drawing.Size(120, 16)
        Me.lblCorrBank_Adr3.TabIndex = 28
        Me.lblCorrBank_Adr3.Text = "60125 -  Bank Adr3"
        '
        'lblCorrBank_IDType
        '
        Me.lblCorrBank_IDType.BackColor = System.Drawing.SystemColors.Control
        Me.lblCorrBank_IDType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCorrBank_IDType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCorrBank_IDType.Location = New System.Drawing.Point(360, 242)
        Me.lblCorrBank_IDType.Name = "lblCorrBank_IDType"
        Me.lblCorrBank_IDType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCorrBank_IDType.Size = New System.Drawing.Size(118, 41)
        Me.lblCorrBank_IDType.TabIndex = 27
        Me.lblCorrBank_IDType.Text = "60122 - ID Type"
        '
        'lblCorrBank_Adr1
        '
        Me.lblCorrBank_Adr1.BackColor = System.Drawing.SystemColors.Control
        Me.lblCorrBank_Adr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCorrBank_Adr1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCorrBank_Adr1.Location = New System.Drawing.Point(8, 294)
        Me.lblCorrBank_Adr1.Name = "lblCorrBank_Adr1"
        Me.lblCorrBank_Adr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCorrBank_Adr1.Size = New System.Drawing.Size(117, 16)
        Me.lblCorrBank_Adr1.TabIndex = 26
        Me.lblCorrBank_Adr1.Text = "60123 - Bank Adr1"
        '
        'lblCorrBank_Adr2
        '
        Me.lblCorrBank_Adr2.BackColor = System.Drawing.SystemColors.Control
        Me.lblCorrBank_Adr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCorrBank_Adr2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCorrBank_Adr2.Location = New System.Drawing.Point(360, 294)
        Me.lblCorrBank_Adr2.Name = "lblCorrBank_Adr2"
        Me.lblCorrBank_Adr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCorrBank_Adr2.Size = New System.Drawing.Size(123, 16)
        Me.lblCorrBank_Adr2.TabIndex = 25
        Me.lblCorrBank_Adr2.Text = "60124 - Bank Adr2"
        '
        'lblCorrBank_Adr4
        '
        Me.lblCorrBank_Adr4.BackColor = System.Drawing.SystemColors.Control
        Me.lblCorrBank_Adr4.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCorrBank_Adr4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCorrBank_Adr4.Location = New System.Drawing.Point(360, 316)
        Me.lblCorrBank_Adr4.Name = "lblCorrBank_Adr4"
        Me.lblCorrBank_Adr4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCorrBank_Adr4.Size = New System.Drawing.Size(120, 16)
        Me.lblCorrBank_Adr4.TabIndex = 24
        Me.lblCorrBank_Adr4.Text = "60126 - Bank Adr4"
        '
        'lblIntBankInfo
        '
        Me.lblIntBankInfo.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntBankInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntBankInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntBankInfo.Location = New System.Drawing.Point(8, 216)
        Me.lblIntBankInfo.Name = "lblIntBankInfo"
        Me.lblIntBankInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntBankInfo.Size = New System.Drawing.Size(337, 18)
        Me.lblIntBankInfo.TabIndex = 23
        Me.lblIntBankInfo.Text = "60128 - Intermediary Bank"
        '
        'lblBenBankInfo
        '
        Me.lblBenBankInfo.BackColor = System.Drawing.SystemColors.Control
        Me.lblBenBankInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBenBankInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBenBankInfo.Location = New System.Drawing.Point(8, 53)
        Me.lblBenBankInfo.Name = "lblBenBankInfo"
        Me.lblBenBankInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBenBankInfo.Size = New System.Drawing.Size(285, 18)
        Me.lblBenBankInfo.TabIndex = 22
        Me.lblBenBankInfo.Text = "60120 - Beneficiary Bank:"
        '
        'lblBank_Adr4
        '
        Me.lblBank_Adr4.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_Adr4.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_Adr4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_Adr4.Location = New System.Drawing.Point(360, 152)
        Me.lblBank_Adr4.Name = "lblBank_Adr4"
        Me.lblBank_Adr4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_Adr4.Size = New System.Drawing.Size(120, 16)
        Me.lblBank_Adr4.TabIndex = 21
        Me.lblBank_Adr4.Text = "60126 - Bank Adr4"
        '
        'lblBank_Adr2
        '
        Me.lblBank_Adr2.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_Adr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_Adr2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_Adr2.Location = New System.Drawing.Point(360, 129)
        Me.lblBank_Adr2.Name = "lblBank_Adr2"
        Me.lblBank_Adr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_Adr2.Size = New System.Drawing.Size(123, 16)
        Me.lblBank_Adr2.TabIndex = 20
        Me.lblBank_Adr2.Text = "60124 - Bank Adr2"
        '
        'lblBank_Adr1
        '
        Me.lblBank_Adr1.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_Adr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_Adr1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_Adr1.Location = New System.Drawing.Point(8, 129)
        Me.lblBank_Adr1.Name = "lblBank_Adr1"
        Me.lblBank_Adr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_Adr1.Size = New System.Drawing.Size(117, 16)
        Me.lblBank_Adr1.TabIndex = 19
        Me.lblBank_Adr1.Text = "60123 - Bank Adr1"
        '
        'lblBank_IDType
        '
        Me.lblBank_IDType.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_IDType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_IDType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_IDType.Location = New System.Drawing.Point(360, 78)
        Me.lblBank_IDType.Name = "lblBank_IDType"
        Me.lblBank_IDType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_IDType.Size = New System.Drawing.Size(118, 40)
        Me.lblBank_IDType.TabIndex = 18
        Me.lblBank_IDType.Text = "60122 - ID Type"
        '
        'lblErrMessage
        '
        Me.lblErrMessage.BackColor = System.Drawing.SystemColors.Window
        Me.lblErrMessage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblErrMessage.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblErrMessage.Location = New System.Drawing.Point(30, 376)
        Me.lblErrMessage.Name = "lblErrMessage"
        Me.lblErrMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblErrMessage.Size = New System.Drawing.Size(677, 47)
        Me.lblErrMessage.TabIndex = 17
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(671, 346)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 27)
        Me._Image1_0.TabIndex = 38
        Me._Image1_0.TabStop = False
        '
        'lblBank_Info
        '
        Me.lblBank_Info.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_Info.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_Info.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_Info.Location = New System.Drawing.Point(8, 177)
        Me.lblBank_Info.Name = "lblBank_Info"
        Me.lblBank_Info.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_Info.Size = New System.Drawing.Size(123, 16)
        Me.lblBank_Info.TabIndex = 16
        Me.lblBank_Info.Text = "60127 -  Info to Bank"
        '
        'lblBank_Adr3
        '
        Me.lblBank_Adr3.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_Adr3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_Adr3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_Adr3.Location = New System.Drawing.Point(8, 152)
        Me.lblBank_Adr3.Name = "lblBank_Adr3"
        Me.lblBank_Adr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_Adr3.Size = New System.Drawing.Size(123, 16)
        Me.lblBank_Adr3.TabIndex = 15
        Me.lblBank_Adr3.Text = "60125 -  Bank Adr3"
        '
        'lblBank_ID
        '
        Me.lblBank_ID.BackColor = System.Drawing.SystemColors.Control
        Me.lblBank_ID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBank_ID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBank_ID.Location = New System.Drawing.Point(8, 78)
        Me.lblBank_ID.Name = "lblBank_ID"
        Me.lblBank_ID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBank_ID.Size = New System.Drawing.Size(117, 38)
        Me.lblBank_ID.TabIndex = 14
        Me.lblBank_ID.Text = "60121 - Bank ID"
        '
        '_imgExclamation_4
        '
        Me._imgExclamation_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_4.Image = CType(resources.GetObject("_imgExclamation_4.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_4, CType(4, Short))
        Me._imgExclamation_4.Location = New System.Drawing.Point(128, 151)
        Me._imgExclamation_4.Name = "_imgExclamation_4"
        Me._imgExclamation_4.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_4.TabIndex = 39
        Me._imgExclamation_4.TabStop = False
        Me._imgExclamation_4.Visible = False
        '
        '_imgExclamation_7
        '
        Me._imgExclamation_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_7.Image = CType(resources.GetObject("_imgExclamation_7.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_7, CType(7, Short))
        Me._imgExclamation_7.Location = New System.Drawing.Point(128, 176)
        Me._imgExclamation_7.Name = "_imgExclamation_7"
        Me._imgExclamation_7.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_7.TabIndex = 40
        Me._imgExclamation_7.TabStop = False
        Me._imgExclamation_7.Visible = False
        '
        '_imgExclamation_12
        '
        Me._imgExclamation_12.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_12.Image = CType(resources.GetObject("_imgExclamation_12.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_12, CType(12, Short))
        Me._imgExclamation_12.Location = New System.Drawing.Point(480, 296)
        Me._imgExclamation_12.Name = "_imgExclamation_12"
        Me._imgExclamation_12.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_12.TabIndex = 41
        Me._imgExclamation_12.TabStop = False
        Me._imgExclamation_12.Visible = False
        '
        '_imgExclamation_0
        '
        Me._imgExclamation_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_0.Image = CType(resources.GetObject("_imgExclamation_0.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_0, CType(0, Short))
        Me._imgExclamation_0.Location = New System.Drawing.Point(127, 77)
        Me._imgExclamation_0.Name = "_imgExclamation_0"
        Me._imgExclamation_0.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_0.TabIndex = 42
        Me._imgExclamation_0.TabStop = False
        Me._imgExclamation_0.Visible = False
        '
        '_imgExclamation_13
        '
        Me._imgExclamation_13.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_13.Image = CType(resources.GetObject("_imgExclamation_13.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_13, CType(13, Short))
        Me._imgExclamation_13.Location = New System.Drawing.Point(127, 315)
        Me._imgExclamation_13.Name = "_imgExclamation_13"
        Me._imgExclamation_13.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_13.TabIndex = 43
        Me._imgExclamation_13.TabStop = False
        Me._imgExclamation_13.Visible = False
        '
        '_Image2_13
        '
        Me._Image2_13.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_13.Image = CType(resources.GetObject("_Image2_13.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_13, CType(13, Short))
        Me._Image2_13.Location = New System.Drawing.Point(127, 315)
        Me._Image2_13.Name = "_Image2_13"
        Me._Image2_13.Size = New System.Drawing.Size(18, 18)
        Me._Image2_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_13.TabIndex = 44
        Me._Image2_13.TabStop = False
        Me._Image2_13.Visible = False
        '
        '_imgExclamation_11
        '
        Me._imgExclamation_11.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_11.Image = CType(resources.GetObject("_imgExclamation_11.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_11, CType(11, Short))
        Me._imgExclamation_11.Location = New System.Drawing.Point(127, 293)
        Me._imgExclamation_11.Name = "_imgExclamation_11"
        Me._imgExclamation_11.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_11.TabIndex = 45
        Me._imgExclamation_11.TabStop = False
        Me._imgExclamation_11.Visible = False
        '
        '_Image2_11
        '
        Me._Image2_11.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_11.Image = CType(resources.GetObject("_Image2_11.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_11, CType(11, Short))
        Me._Image2_11.Location = New System.Drawing.Point(127, 293)
        Me._Image2_11.Name = "_Image2_11"
        Me._Image2_11.Size = New System.Drawing.Size(18, 18)
        Me._Image2_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_11.TabIndex = 46
        Me._Image2_11.TabStop = False
        Me._Image2_11.Visible = False
        '
        '_imgExclamation_9
        '
        Me._imgExclamation_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_9.Image = CType(resources.GetObject("_imgExclamation_9.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_9, CType(9, Short))
        Me._imgExclamation_9.Location = New System.Drawing.Point(480, 242)
        Me._imgExclamation_9.Name = "_imgExclamation_9"
        Me._imgExclamation_9.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_9.TabIndex = 47
        Me._imgExclamation_9.TabStop = False
        Me._imgExclamation_9.Visible = False
        '
        '_Image2_9
        '
        Me._Image2_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_9.Image = CType(resources.GetObject("_Image2_9.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_9, CType(9, Short))
        Me._Image2_9.Location = New System.Drawing.Point(478, 242)
        Me._Image2_9.Name = "_Image2_9"
        Me._Image2_9.Size = New System.Drawing.Size(18, 18)
        Me._Image2_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_9.TabIndex = 48
        Me._Image2_9.TabStop = False
        Me._Image2_9.Visible = False
        '
        '_imgExclamation_8
        '
        Me._imgExclamation_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_8.Image = CType(resources.GetObject("_imgExclamation_8.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_8, CType(8, Short))
        Me._imgExclamation_8.Location = New System.Drawing.Point(127, 242)
        Me._imgExclamation_8.Name = "_imgExclamation_8"
        Me._imgExclamation_8.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_8.TabIndex = 49
        Me._imgExclamation_8.TabStop = False
        Me._imgExclamation_8.Visible = False
        '
        '_Image2_8
        '
        Me._Image2_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_8.Image = CType(resources.GetObject("_Image2_8.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_8, CType(8, Short))
        Me._Image2_8.Location = New System.Drawing.Point(127, 242)
        Me._Image2_8.Name = "_Image2_8"
        Me._Image2_8.Size = New System.Drawing.Size(18, 18)
        Me._Image2_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_8.TabIndex = 50
        Me._Image2_8.TabStop = False
        Me._Image2_8.Visible = False
        '
        '_Image2_7
        '
        Me._Image2_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_7.Image = CType(resources.GetObject("_Image2_7.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_7, CType(7, Short))
        Me._Image2_7.Location = New System.Drawing.Point(126, 176)
        Me._Image2_7.Name = "_Image2_7"
        Me._Image2_7.Size = New System.Drawing.Size(18, 18)
        Me._Image2_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_7.TabIndex = 51
        Me._Image2_7.TabStop = False
        Me._Image2_7.Visible = False
        '
        '_imgExclamation_6
        '
        Me._imgExclamation_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_6.Image = CType(resources.GetObject("_imgExclamation_6.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_6, CType(6, Short))
        Me._imgExclamation_6.Location = New System.Drawing.Point(480, 151)
        Me._imgExclamation_6.Name = "_imgExclamation_6"
        Me._imgExclamation_6.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_6.TabIndex = 52
        Me._imgExclamation_6.TabStop = False
        Me._imgExclamation_6.Visible = False
        '
        '_Image2_6
        '
        Me._Image2_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_6.Image = CType(resources.GetObject("_Image2_6.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_6, CType(6, Short))
        Me._Image2_6.Location = New System.Drawing.Point(480, 151)
        Me._Image2_6.Name = "_Image2_6"
        Me._Image2_6.Size = New System.Drawing.Size(18, 18)
        Me._Image2_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_6.TabIndex = 53
        Me._Image2_6.TabStop = False
        Me._Image2_6.Visible = False
        '
        '_Image2_4
        '
        Me._Image2_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_4.Image = CType(resources.GetObject("_Image2_4.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_4, CType(4, Short))
        Me._Image2_4.Location = New System.Drawing.Point(127, 151)
        Me._Image2_4.Name = "_Image2_4"
        Me._Image2_4.Size = New System.Drawing.Size(18, 18)
        Me._Image2_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_4.TabIndex = 54
        Me._Image2_4.TabStop = False
        Me._Image2_4.Visible = False
        '
        '_imgExclamation_5
        '
        Me._imgExclamation_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_5.Image = CType(resources.GetObject("_imgExclamation_5.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_5, CType(5, Short))
        Me._imgExclamation_5.Location = New System.Drawing.Point(480, 128)
        Me._imgExclamation_5.Name = "_imgExclamation_5"
        Me._imgExclamation_5.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_5.TabIndex = 55
        Me._imgExclamation_5.TabStop = False
        Me._imgExclamation_5.Visible = False
        '
        '_imgExclamation_3
        '
        Me._imgExclamation_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_3.Image = CType(resources.GetObject("_imgExclamation_3.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_3, CType(3, Short))
        Me._imgExclamation_3.Location = New System.Drawing.Point(127, 128)
        Me._imgExclamation_3.Name = "_imgExclamation_3"
        Me._imgExclamation_3.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_3.TabIndex = 56
        Me._imgExclamation_3.TabStop = False
        Me._imgExclamation_3.Visible = False
        '
        '_Image2_5
        '
        Me._Image2_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_5.Image = CType(resources.GetObject("_Image2_5.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_5, CType(5, Short))
        Me._Image2_5.Location = New System.Drawing.Point(480, 128)
        Me._Image2_5.Name = "_Image2_5"
        Me._Image2_5.Size = New System.Drawing.Size(18, 18)
        Me._Image2_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_5.TabIndex = 57
        Me._Image2_5.TabStop = False
        Me._Image2_5.Visible = False
        '
        '_Image2_3
        '
        Me._Image2_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_3.Image = CType(resources.GetObject("_Image2_3.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_3, CType(3, Short))
        Me._Image2_3.Location = New System.Drawing.Point(127, 128)
        Me._Image2_3.Name = "_Image2_3"
        Me._Image2_3.Size = New System.Drawing.Size(18, 18)
        Me._Image2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_3.TabIndex = 58
        Me._Image2_3.TabStop = False
        Me._Image2_3.Visible = False
        '
        '_imgExclamation_1
        '
        Me._imgExclamation_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_1.Image = CType(resources.GetObject("_imgExclamation_1.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_1, CType(1, Short))
        Me._imgExclamation_1.Location = New System.Drawing.Point(480, 77)
        Me._imgExclamation_1.Name = "_imgExclamation_1"
        Me._imgExclamation_1.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_1.TabIndex = 59
        Me._imgExclamation_1.TabStop = False
        Me._imgExclamation_1.Visible = False
        '
        '_Image2_1
        '
        Me._Image2_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_1.Image = CType(resources.GetObject("_Image2_1.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_1, CType(1, Short))
        Me._Image2_1.Location = New System.Drawing.Point(480, 77)
        Me._Image2_1.Name = "_Image2_1"
        Me._Image2_1.Size = New System.Drawing.Size(18, 18)
        Me._Image2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_1.TabIndex = 60
        Me._Image2_1.TabStop = False
        Me._Image2_1.Visible = False
        '
        '_Image2_0
        '
        Me._Image2_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_0.Image = CType(resources.GetObject("_Image2_0.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_0, CType(0, Short))
        Me._Image2_0.Location = New System.Drawing.Point(127, 77)
        Me._Image2_0.Name = "_Image2_0"
        Me._Image2_0.Size = New System.Drawing.Size(18, 18)
        Me._Image2_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_0.TabIndex = 61
        Me._Image2_0.TabStop = False
        Me._Image2_0.Visible = False
        '
        '_Image2_12
        '
        Me._Image2_12.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_12.Image = CType(resources.GetObject("_Image2_12.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_12, CType(12, Short))
        Me._Image2_12.Location = New System.Drawing.Point(480, 293)
        Me._Image2_12.Name = "_Image2_12"
        Me._Image2_12.Size = New System.Drawing.Size(18, 18)
        Me._Image2_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_12.TabIndex = 62
        Me._Image2_12.TabStop = False
        Me._Image2_12.Visible = False
        '
        '_imgExclamation_14
        '
        Me._imgExclamation_14.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_14.Image = CType(resources.GetObject("_imgExclamation_14.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_14, CType(14, Short))
        Me._imgExclamation_14.Location = New System.Drawing.Point(478, 315)
        Me._imgExclamation_14.Name = "_imgExclamation_14"
        Me._imgExclamation_14.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_14.TabIndex = 63
        Me._imgExclamation_14.TabStop = False
        Me._imgExclamation_14.Visible = False
        '
        '_Image2_14
        '
        Me._Image2_14.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_14.Image = CType(resources.GetObject("_Image2_14.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_14, CType(14, Short))
        Me._Image2_14.Location = New System.Drawing.Point(480, 315)
        Me._Image2_14.Name = "_Image2_14"
        Me._Image2_14.Size = New System.Drawing.Size(18, 18)
        Me._Image2_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_14.TabIndex = 64
        Me._Image2_14.TabStop = False
        Me._Image2_14.Visible = False
        '
        'frmCorrectBank
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(715, 465)
        Me.ControlBox = False
        Me.Controls.Add(Me.chkChargeMeAbroad)
        Me.Controls.Add(Me.chkChargeMeDomestic)
        Me.Controls.Add(Me.cmbCorrBank_IDType)
        Me.Controls.Add(Me.cmbBank_IDType)
        Me.Controls.Add(Me.txtCorrBank_ID)
        Me.Controls.Add(Me.txtCorrBank_Adr3)
        Me.Controls.Add(Me.txtCorrBank_Adr1)
        Me.Controls.Add(Me.txtCorrBank_Adr2)
        Me.Controls.Add(Me.txtCorrBank_Adr4)
        Me.Controls.Add(Me.txtBank_Adr4)
        Me.Controls.Add(Me.txtBank_Adr2)
        Me.Controls.Add(Me.txtBank_Adr1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtBank_Info)
        Me.Controls.Add(Me.txtBank_Adr3)
        Me.Controls.Add(Me.txtBank_ID)
        Me.Controls.Add(Me._Image2_16)
        Me.Controls.Add(Me._Image2_10)
        Me.Controls.Add(Me._imgExclamation_16)
        Me.Controls.Add(Me._imgExclamation_10)
        Me.Controls.Add(Me._Image2_15)
        Me.Controls.Add(Me._imgExclamation_15)
        Me.Controls.Add(Me.lblCorrBank_ID)
        Me.Controls.Add(Me.lblCorrBank_Adr3)
        Me.Controls.Add(Me.lblCorrBank_IDType)
        Me.Controls.Add(Me.lblCorrBank_Adr1)
        Me.Controls.Add(Me.lblCorrBank_Adr2)
        Me.Controls.Add(Me.lblCorrBank_Adr4)
        Me.Controls.Add(Me.lblIntBankInfo)
        Me.Controls.Add(Me.lblBenBankInfo)
        Me.Controls.Add(Me.lblBank_Adr4)
        Me.Controls.Add(Me.lblBank_Adr2)
        Me.Controls.Add(Me.lblBank_Adr1)
        Me.Controls.Add(Me.lblBank_IDType)
        Me.Controls.Add(Me.lblErrMessage)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblBank_Info)
        Me.Controls.Add(Me.lblBank_Adr3)
        Me.Controls.Add(Me.lblBank_ID)
        Me.Controls.Add(Me._imgExclamation_4)
        Me.Controls.Add(Me._imgExclamation_7)
        Me.Controls.Add(Me._imgExclamation_12)
        Me.Controls.Add(Me._imgExclamation_0)
        Me.Controls.Add(Me._imgExclamation_13)
        Me.Controls.Add(Me._Image2_13)
        Me.Controls.Add(Me._imgExclamation_11)
        Me.Controls.Add(Me._Image2_11)
        Me.Controls.Add(Me._imgExclamation_9)
        Me.Controls.Add(Me._Image2_9)
        Me.Controls.Add(Me._imgExclamation_8)
        Me.Controls.Add(Me._Image2_8)
        Me.Controls.Add(Me._Image2_7)
        Me.Controls.Add(Me._imgExclamation_6)
        Me.Controls.Add(Me._Image2_6)
        Me.Controls.Add(Me._Image2_4)
        Me.Controls.Add(Me._imgExclamation_5)
        Me.Controls.Add(Me._imgExclamation_3)
        Me.Controls.Add(Me._Image2_5)
        Me.Controls.Add(Me._Image2_3)
        Me.Controls.Add(Me._imgExclamation_1)
        Me.Controls.Add(Me._Image2_1)
        Me.Controls.Add(Me._Image2_0)
        Me.Controls.Add(Me._Image2_12)
        Me.Controls.Add(Me._imgExclamation_14)
        Me.Controls.Add(Me._Image2_14)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmCorrectBank"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60093 - Bank Information"
        CType(Me._Image2_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgExclamation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region 
End Class
