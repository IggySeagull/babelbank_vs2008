Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_992_VismaDetail 
    ' Payments which have been filtered out, and deleted
    '------------------------------------------------------

    Private oBabelFiles As BabelFiles
    ' Declare all objects to be used in actual report:
    Dim oBabelFile As BabelFile
    Dim oBatch As Batch
    Dim oPayment As Payment
    Dim oInvoice As Invoice
    Dim oFreetext As vbBabel.Freetext
    Dim cTotalAccountAmount As Decimal
    Dim cTotalTransferredAmount As Decimal
    Dim cRunningTotalAmount As Decimal
    Dim cInvoiceTotal As Decimal
    Dim nNoOfPayments As Double
    Dim cInvoiceAmountPrPayment As Decimal

    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean = False
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean


    Private Sub rp_992_VismaDetail_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If

    End Sub
    Private Sub rp_992_VismaDetail_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Declare all fields to be used in actual report:
        ' Header
        Fields.Add("Grouping")
        Fields.Add("Babelfile_ID")
        Fields.Add("Batch_ID")
        Fields.Add("Payment_ID")
        Fields.Add("Invoice_ID")
        Fields.Add("fldDateTime")
        ' grFile
        Fields.Add("fldFileIndex")
        Fields.Add("Filename")
        Fields.Add("Date_Value")
        Fields.Add("I_Account")
        Fields.Add("fldAmountStatement")
        Fields.Add("fldRef")
        Fields.Add("MON_TransferCurrency")
        Fields.Add("fldTransType")
        Fields.Add("fldNoOfPayments")
        Fields.Add("fldProdDate")
        Fields.Add("fldBreak")
        Fields.Add("fldClient")

        'grPayment
        Fields.Add("fldPaymentIndex")
        Fields.Add("E_Name")
        Fields.Add("E_Adr1")
        Fields.Add("E_Adr2")
        Fields.Add("E_Adr3")
        Fields.Add("E_Zip")
        Fields.Add("E_City")
        Fields.Add("MON_TransferredAmount")
        Fields.Add("E_Account")
        Fields.Add("REF_Bank2")
        Fields.Add("REF_Bank1")
        Fields.Add("REF_Own")
        Fields.Add("fldBankInfo")
        Fields.Add("fldStatus")

        'grPaymentFooter
        Fields.Add("fldErrorText")

        'Detail
        Fields.Add("fldFreeText")
        Fields.Add("UniqueID")
        Fields.Add("InvoiceAmount")
        ' Sum
        Fields.Add("fldTotalAmount")
        Fields.Add("fldInvoiceTotal")
        ' Footer
        Fields.Add("fldRunDate")
        ' Sorting ?
        ' Call a method to sort collections here

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtProdDate.OutputFormat = "D"
            Me.txtDATE_Value.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtProdDate.OutputFormat = "d"
            Me.txtDATE_Value.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

    End Sub

    Private Sub rp_992_VismaDetail_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart
        ' Dataobject:;
        '----------------
        ' Set breaklevel;
        '----------------
        ' 4, New 26.06.05
        ' 4, BREAK_ON_ACCOUNTONLY : Break on CreditAccount
        ' 1, BREAK_ON_ACCOUNT : Break on productiondate + CreditAccount
        ' 2, BREAK_ON_BATCH   : Break on productiondate + CreditAccount + iBatches
        ' 3, BREAK_ON_PAYMENT : Break on productiondate + CreditAccount + iBatches + iPayments
        ' 4, New 26.06.05
        ' 4, BREAK_ON_ACCOUNTONLY : Break on CreditAccount
        ' Default;
        'iBreakOn = BREAK_ON_PAYMENT 'BAMA
        'iBreakOn = BREAK_ON_BATCH   ' LHL
        ' If set, fetch breakelevel from frmViewer
        'If frmViewer.iBreakLevel > 0 Then
        '    iBreakOn = frmViewer.iBreakLevel
        'End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If sSpecial = "ErrorReport" Then
            Detail.Height = 400
        Else
            Detail.Height = 210
        End If

        'If iBreakLevel = BREAK_ON_PAYMENT Then
        '    ' Test new page pr payment
        '    Me.grPaymentHeader.NewPage = ddNPBefore
        'End If

        '' added 14.05.2007
        'If iBreakLevel = NOBREAK Then
        '    ' No page break
        '    Me.grFileHeader.NewPage = ddNPNone
        'End If

        ' Localize capitions for headings, etc
        lblFilename.Text = LRS(40001)
        ' Use Userdefined heading for report
        If EmptyString(sReportName) Then
            lblrptHeader.Text = LRS(40034)   ' Filtered and deleted payments
        Else
            lblrptHeader.Text = sReportName
        End If
        ' If not set any userdefined heading, use default;
        lblValueDate.Text = LRS(40027)
        'lblRef.Text = LRS(40005)
        lblClient.Text = LRS(40030)  'Client
        lblCreditAccount.Text = LRS(40006)
        'lblCurrency.Caption = LRS(40007)
        'lblTransType.Caption = LRS(40008)

        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        lblREF_Bank2.Text = LRS(40011)
        lblREF_Bank1.Text = LRS(40012)
        lblREF_Own.Text = LRS(60072) & ":"
        lblBankInfo.Text = LRS(40079) & ":"
        lblE_Account.Text = LRS(40013)
        lblDATE_Value.Text = LRS(40003)
        'Me.lblFileSum = LRS(40017)
        Me.lblTotalAmount.Text = LRS(40138)  ' 17.01.2014 Sum / Total / Summa

        'If iTotalLevel = 1 Then
        '    ' Hide totals
        '    Me.txtFileSumAmount.Visible = False
        '    Me.lblFileSum.Visible = False
        'End If

        Me.lblREF_Own.Visible = False
        Me.txtREF_Own.Visible = False

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:"

    End Sub
    Private Sub rp_992_VismaDetail_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        ' Counters for items in the different Babel collections
        Static iBabelFiles As Long
        Static iLastUsedBabelFilesItem As Integer
        Static iBatches As Long
        Static iPayments As Long
        Static iInvoices As Long
        Static iFreeTexts As Long
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String
        Static xBabelFiles As Long
        Static bOriginalFreetext As Boolean
        Static bBabelComment As Boolean   ' Freetext with qualifier > 2 have been displayed for this invoice

        Dim sClientNo As String
        Dim sBranch As String

        Dim sDate As String
        Dim oFreetext As vbBabel.Freetext
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim oErrorObject As ErrorObject
        Dim oLocalInvoice As Invoice

        eArgs.EOF = False

        'Kept grPaymentInternationalHeader for possible future use
        grPaymentInternationalHeader.Visible = False

        If iBabelFiles = 0 Then
            iBabelFiles = 1
            iBatches = 1
            iPayments = 1
            iInvoices = 1
            iFreeTexts = 0
            xCreditAccount = "xxxx"
            xDate = "x"
            xBabelFiles = 0
        End If

        ' Spin through collections to find next suitbable record before any other
        ' proecessing is done
        ' Position to correct items in collections
        bOriginalFreetext = False
        iFreeTexts = iFreeTexts + 1

        Do Until iBabelFiles > oBabelFiles.Count
            ' Try to set as few times as possible
            If oBabelFile Is Nothing Then
                oBabelFile = oBabelFiles(iBabelFiles)
                iLastUsedBabelFilesItem = 1 'oBabelFile.Index
            Else
                If iLastUsedBabelFilesItem <> iBabelFiles Then 'oBabelFile.Index Then
                    ' Added 19.05.2004
                    xCreditAccount = "xxxx"
                    xDate = "x"
                    oBabelFile = oBabelFiles(iBabelFiles)
                    iLastUsedBabelFilesItem = iBabelFiles  'oBabelFile.Index
                End If
            End If

            Do Until iBatches > oBabelFile.Batches.Count
                'If oBatch Is Nothing Then
                '    Set oBatch = oBabelFile.Batches(iBatches)
                'ElseIf iBatches <> oBatch.Index Or iBatches = 1 Then
                oBatch = oBabelFile.Batches(iBatches)
                'End If

                ' Find the next payment in this batch which is original
                Do Until iPayments > oBatch.Payments.Count
                    oPayment = oBatch.Payments.Item(iPayments)

                    ' Print only oPayment.ToSpecialReport = True
                    If oPayment.ToSpecialReport Then

                        ' Postiton to a, final invoice
                        ' Find the next invoice in this payment which has .ToSpecialReport = True
                        Do Until iInvoices > oPayment.Invoices.Count
                            oInvoice = oPayment.Invoices.Item(iInvoices)
                            bBabelComment = False  ' have not, yet, displayed comments(freetexts) added by BabelBank

                            bOriginalFreetext = True

                            'If oInvoice.ToSpecialReport Then
                            '    If iFreeTexts <= oInvoice.Freetexts.Count Then
                            '        bOriginalFreetext = True
                            '        oFreetext = oInvoice.Freetexts.Item(iFreeTexts)
                            '        'iFreeTexts = iFreeTexts + 1
                            '        Exit Do
                            '    End If
                            '    If oInvoice.Freetexts.Count = 0 And iFreeTexts = 1 Then
                            '        bOriginalFreetext = True
                            '        Exit Do
                            '    End If
                            'End If
                            iInvoices = iInvoices + 1
                            ' 30.01.2018 - added next to display all invoices
                            Exit Do

                            'iFreeTexts = 1
                        Loop 'iInvoices
                    End If 'oPayment.ToSpecialReport Then

                    If bOriginalFreetext Then
                        Exit Do
                    End If
                    iPayments = iPayments + 1
                    iInvoices = 1
                    cInvoiceTotal = 0

                Loop 'iPayments
                If bOriginalFreetext Then
                    Exit Do
                End If
                iBatches = iBatches + 1
                iPayments = 1
            Loop ' iBatches
            If bOriginalFreetext Then
                Exit Do
            End If
            iBabelFiles = iBabelFiles + 1
            iBatches = 1
        Loop ' iBabelFiles

        If iBabelFiles > oBabelFiles.Count Then
            eArgs.EOF = True
            iBabelFiles = 0 ' to reset statics next time !

            Exit Sub
        End If

        ' grBatch data
        '-------------
        Fields("fldFileIndex").Value = iBabelFiles '
        ' XokNET 12.09.2013
        If Not EmptyString(oBabelFile.FilenameIn) And oBabelFiles.Count = 1 Then
            If oBabelFile.FilenameIn.Length > 36 Then
                Fields("Filename").Value = "..." & Right(oBabelFile.FilenameIn, 36)
            Else
                Fields("Filename").Value = oBabelFile.FilenameIn
            End If
        Else
            ' No filename set, hide
            Me.txtFilename.Visible = False
            Me.lblFilename.Visible = False
        End If

        ' grFile data
        '-------------
        'PreGroupTotals ' Calculate totals
        If oBabelFile.Batches(iBatches).DATE_Production <> "19900101" Then
            sDate = oBabelFile.Batches(iBatches).DATE_Production 'Me.Fields("DATE_Payment").Value
            Me.txtProdDate.Value = DateSerial(CInt(Left(sDate, 4)), CInt(Mid(sDate, 5, 2)), CInt(Right(sDate, 2)))
        Else
            ' Show todays date
            Me.txtProdDate.Value = Now()
        End If
        Me.Fields("Babelfile_ID").Value = oBabelFile.Index
        Me.Fields("Batch_ID").Value = oBatch.Index
        Me.Fields("Payment_ID").Value = oPayment.Index
        Me.Fields("Invoice_ID").Value = oInvoice.Index

        Me.Fields("Grouping").Value = "1-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))

        ' Footer data:
        ' ------------
        'Fields("fldRunDate").Value = Format(Date, "Long Date")

        sClientNo = ""
        sBranch = ""

        sClientNo = oPayment.I_Client

        If sClientNo = vbNullString And sBranch = vbNullString Then
            lblClient.Visible = False
        Else
            lblClient.Visible = True
            Fields("fldClient").Value = sClientNo & " " & sBranch
        End If

        ' grFileHeader breaks on  Proddate + creditaccount, thats why its filled up here!

        Fields("I_Account").Value = oBabelFile.Batches(iBatches).Payments(iPayments).I_Account

        'If iBabelFiles <> xBabelFiles Or oPayment.I_Account <> xCreditAccount Or oPayment.DATE_Payment <> xDate Then
        ' New 26.06.05 - Allow break on creditaccount only, no matter how date changes
        If iBabelFiles <> xBabelFiles Or oPayment.I_Account <> xCreditAccount Or _
        (iBreakLevel <> BREAK_ON_ACCOUNTONLY And oPayment.DATE_Payment <> xDate) Then
            xBabelFiles = iBabelFiles
            xCreditAccount = oPayment.I_Account
            xDate = oPayment.DATE_Payment
            If iBreakLevel = BREAK_ON_ACCOUNTONLY Then
                xDate = ""
            End If
            PreGroupTotals(oPayment.I_Account, xDate, iBabelFiles, iBatches, iPayments)
            Fields("fldTotalAmount").Value = cTotalTransferredAmount / 100
            Fields("fldAmountStatement").Value = cTotalAccountAmount / 100

            Fields("fldTransType").Value = bbGetPayCodeString(oBabelFile.Batches(iBatches).Payments(iPayments).PayCode)
            Fields("fldNoOfPayments").Value = nNoOfPayments

        End If
        If iBreakLevel = BREAK_ON_ACCOUNTONLY Or iBreakLevel = NOBREAK Then
            Fields("fldBreak").Value = Trim$(Str(iBabelFiles)) & oPayment.I_Account
        ElseIf iBreakLevel = BREAK_ON_PAYMENT Then
            Fields("fldBreak").Value = Trim$(Str(iBabelFiles)) & Trim$(Str(iBatches)) & Trim$(Str(iPayments))
        Else
            Fields("fldBreak").Value = Str(iBabelFiles) & oPayment.I_Account & oPayment.DATE_Payment
            Fields("fldBreak").Value = vbNullString
        End If

        bBabelComment = False  ' have not, yet, displayed comments(freetexts) added by BabelBank

        Fields("fldPaymentIndex").Value = Val(Trim(Str(iBatches)) & Trim(Str(iPayments))) ' force a brake here -iPayments ' Group break

        'grPayments data
        '----------------
        ' Give text GiroMail for manual payments :
        If oPayment.PayCode = "602" Then
            Fields("E_Name").Value = "GIROMAIL"
        Else
            Fields("E_Name").Value = Left$(oPayment.E_Name, 30)
        End If
        Fields("E_Adr1").Value = oPayment.E_Adr1
        Fields("E_Adr2").Value = oPayment.E_Adr2
        If oPayment.E_City <> vbNullString Then
            Fields("E_Zip").Value = oPayment.E_Zip
            Fields("E_City").Value = oPayment.E_City
            Me.txtE_City.Visible = True
            Me.txtE_Zip.Visible = True
            Me.txtE_Adr3.Visible = False
        Else
            Fields("E_Adr3").Value = oPayment.E_Adr3
            Me.txtE_City.Visible = False
            Me.txtE_Zip.Visible = False
            Me.txtE_Adr3.Visible = False
        End If

        'If oPayment.MON_TransferredAmount <> 0 Then
        '    Fields("MON_TransferredAmount").Value = oPayment.MON_TransferredAmount / 100
        'Else
        'Fields("MON_TransferredAmount").Value = oPayment.MON_InvoiceAmount / 100
        ' changed 30.05.2014 to reflect unmarked invoices
        cInvoiceAmountPrPayment = 0
        For Each oLocalInvoice In oPayment.Invoices
            If oLocalInvoice.ToSpecialReport = True Then
                cInvoiceAmountPrPayment = cInvoiceAmountPrPayment + oLocalInvoice.MON_InvoiceAmount / 100
            End If
        Next oLocalInvoice
        Fields("MON_TransferredAmount").Value = cInvoiceAmountPrPayment
        'End If

        'If oPayment.MON_AccountAmount <> 0 Then
        '    Fields("fldAccountAmount").Value = oPayment.MON_AccountAmount / 100
        'End If



        Fields("E_Account").Value = oPayment.E_Account.Trim
        Fields("REF_Bank2").Value = oPayment.REF_Bank2
        Fields("REF_Bank1").Value = oPayment.REF_Bank1
        Fields("REF_Own").Value = oPayment.REF_Own
        If EmptyString(oPayment.BANK_BranchNo) And EmptyString(oPayment.BANK_Name) And EmptyString(oPayment.BANK_SWIFTCode) Then
            Me.txtBankInfo.Visible = False
            Me.lblBankInfo.Visible = False
        Else
            Me.txtBankInfo.Visible = True
            Me.lblBankInfo.Visible = True
            Fields("fldBankInfo").Value = oPayment.BANK_Name & IIf(Not EmptyString(oPayment.BANK_SWIFTCode), "   BIC:" & oPayment.BANK_SWIFTCode, "") & " " & oPayment.BANK_BranchNo
        End If

        '  Added fldStatus, to show if Mottaksretur, avregningsretur, avvisning, innbetaling
        If oBabelFile.TypeOfTransaction = vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Outgoing Then
            ' Give status instead;
            If Val(oPayment.StatusCode) = 0 Then
                Fields("fldStatus").Value = ""
            ElseIf Val(oPayment.StatusCode) = 1 And Val(oInvoice.StatusCode) = 1 Then
                Fields("fldStatus").Value = LRS(36059)  ' Mottaksretur
            ElseIf Val(oPayment.StatusCode) = 2 And Val(oInvoice.StatusCode) = 2 Then
                Fields("fldStatus").Value = LRS(36060)  ' Avregning
            Else
                Fields("fldStatus").Value = LRS(36061)     ' Avvisning
            End If
        Else
            ' Incoming payments
            Fields("fldStatus").Value = LRS(36062)   ' Innbetalinger
        End If

        Fields("MON_TransferCurrency").Value = oBabelFile.Batches(iBatches).Payments(iPayments).MON_InvoiceCurrency
        If oPayment.DATE_Value > "19900101" Then
            ' use valuedate if specified
            Fields("Date_Value").Value = StringToDate(oPayment.DATE_Value)
        Else
            'Use bookdate
            Fields("Date_Value").Value = StringToDate(oPayment.DATE_Payment)
        End If
        Fields("fldFreetext").Value = vbNullString  ' test, in case we do not set fldFreetext
        Fields("UniqueID").Value = vbNullString
        'If oInvoice.MON_TransferredAmount <> 0 Then
        '    Fields("InvoiceAmount").Value = oInvoice.MON_TransferredAmount / 100
        'Else
        Fields("InvoiceAmount").Value = oInvoice.MON_InvoiceAmount / 100
        'End If

        'Store the keys for usage in the subreport(s)
        iBabelfile_ID = Me.Fields("Babelfile_ID").Value
        iBatch_ID = Me.Fields("Batch_ID").Value
        iPayment_ID = Me.Fields("Payment_ID").Value
        iInvoice_ID = Me.Fields("Invoice_ID").Value

        ' Freetext not always in use, test:
        If iFreeTexts < 2 Then

            ' count only once pr. Invoice!!!!
            If oInvoice.MON_TransferredAmount <> 0 Then
                cRunningTotalAmount = cRunningTotalAmount + oInvoice.MON_TransferredAmount
            Else
                cRunningTotalAmount = cRunningTotalAmount + oInvoice.MON_InvoiceAmount
            End If

            ' In grPaymentFooter
            If oInvoice.MON_TransferredAmount <> 0 Then
                cInvoiceTotal = cInvoiceTotal + (oInvoice.MON_TransferredAmount / 100)
            Else
                cInvoiceTotal = cInvoiceTotal + (oInvoice.MON_InvoiceAmount / 100)
            End If
            Fields("fldInvoiceTotal").Value = cInvoiceTotal

            Fields("fldErrorText").Value = ""
            ' Show text for oInvoice.ErrorObject
            For Each oErrorObject In oInvoice.ErrorObjects
                If oErrorObject.ErrorType = "Error" Then
                    Fields("fldErrorText").Value = Fields("fldErrorText").Value & " " & oErrorObject.ErrorMessage
                End If
            Next oErrorObject

            ' If KID as well as freetext, show KID;
            If Trim$(oInvoice.Unique_Id) <> vbNullString Then
                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.OCR Then
                    Fields("UniqueID").Value = "KID: " & oInvoice.Unique_Id
                ElseIf oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.DanskeBankTeleService Then
                    Fields("UniqueID").Value = "FIK: " & oInvoice.Unique_Id
                Else
                    Fields("UniqueID").Value = oInvoice.Unique_Id
                End If
            End If
            ' New 11.02.05: If Structured Invoiceno, show;
            ' XokNET 31.03.2014 Changed to reflect OCR
            If Trim$(oInvoice.InvoiceNo) <> vbNullString And EmptyString(oInvoice.Unique_Id) Then
                Fields("UniqueID").Value = LRS(60158) & " " & oInvoice.InvoiceNo
            End If

            txtInvoiceAmount.Visible = True
        Else
            ' 30.01.2018 - removed next line - otherwise invoiceamount was not displayes
            'txtInvoiceAmount.Visible = False
        End If

        'If oInvoice.Freetexts.Count > 0 Then
        '    Me.Detail.Visible = True
        '    Fields("fldFreetext").Value = oFreetext.Text
        'Else
        '    ' If KID, and no freetext, no detail to show
        '    'Me.Detail.Visible = False
        '    'lblConcerns.Visible = False
        '    Fields("fldFreetext").Value = vbNullString
        'End If ' oInvoice.Freetexts.Count > 0 Then

        '  Added next IF because freetext wasn't shown for incoming payments
        If oBabelFile.TypeOfTransaction = vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Outgoing Then
            ' show ownref at invoicelevel + freetext if no KID/Invoice
            Fields("fldFreetext").Value = LRS(60072) & ": " & oInvoice.REF_Own
        End If

    End Sub
    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim rptFreetext As New rp_Sub_Freetext()
        'Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String
        Dim lTextCounter As Long
        Dim sText As String
        Dim pSize As System.Drawing.SizeF

        'If iInvoices = 1 And iFreeTexts < 2 Then
        '    ' Data for details-section, Freetext only
        '    '    ' ---------------------------------------
        '    ' Show "Amount Concerns" only once;
        '    lblConcerns.Visible = True
        '    lblConcerns.Font.Bold = False
        '    lblConcerns.Caption = LRS(40010)
        'Else
        '    lblConcerns.Visible = False
        'End If

        'If bReportFromDatabase Then
        '    childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        '    sMySQL = "SELECT XText AS Freetext, '' AS Label FROM Freetext WHERE Company_ID = " & Trim$(Str(iCompany_ID))
        '    sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
        '    sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
        '    sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
        '    sMySQL = sMySQL & " AND Invoice_ID = " & Trim$(Str(iInvoice_ID))
        '    sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Freetext_ID ASC"

        '    rptFreetext.ReportFromDatabase = True
        '    childFreetextDataSource.SQL = sMySQL
        '    rptFreetext.Label = LRS(40010) '"Bel�pet gjelder:" 
        '    rptFreetext.DataSource = childFreetextDataSource
        '    Me.SubRptFreetext.Report = rptFreetext

        'Else
        If oInvoice.Freetexts.Count > 0 Then
            sText = ""
            For Each oFreetext In oInvoice.Freetexts
                sText = sText & oFreetext.Text
                sText = sText & vbCrLf 'Add carriagereturn and line feed to the end
                lTextCounter = lTextCounter + 1
            Next oFreetext
            'Remove last vbCrLf
            If sText.Length > 1 Then
                sText = Left(sText, Len(sText) - 2)
            End If

            rptFreetext.Label = LRS(40010) '"Bel�pet gjelder:" 
            rptFreetext.Text = sText
            rptFreetext.ReportFromDatabase = False
            Me.SubRptFreetext.Report = rptFreetext
        Else
            rptFreetext.ReportFromDatabase = False
            rptFreetext.Text = ""
            Me.SubRptFreetext.Report = rptFreetext
        End If

        'End If


    End Sub

    Private Sub rp_992_VismaDetail_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportEnd
        If Not oBatch Is Nothing Then
            oBatch = Nothing
        End If
        If Not oPayment Is Nothing Then
            oPayment = Nothing
        End If
        If Not oInvoice Is Nothing Then
            oInvoice = Nothing
        End If
        'frmViewer.SetBabelFilesToNothing
        cTotalTransferredAmount = 0
        cTotalAccountAmount = 0
        nNoOfPayments = 0

    End Sub

    Private Sub PreGroupTotals(ByVal sAccount As String, ByVal sDate As String, ByVal iBabelFiles As Long, ByVal iBatches As Long, ByVal iPayments As Long)
        Dim lPaymentCounter As Long
        Dim lBatchcounter As Long
        Dim lInvoiceCounter As Long
        'Dim lBabelCounter As Long

        ' totals to show in group header
        cTotalTransferredAmount = 0
        cTotalAccountAmount = 0
        nNoOfPayments = 0

        For lBatchcounter = iBatches To oBabelFile.Batches.Count

            ' changed totals 22.09.2008 - gave wrong results before
            '        If Len(sDate) > 0 Then
            '            For lPaymentCounter = iPayments To oBabelFile.Batches(lBatchcounter).Payments.Count
            '                If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).DATE_Payment <> sDate Or _
            '                    oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).I_Account <> sAccount Then
            '                    ' continue as long as we have same account and date
            '                    Exit For
            '                End If
            '                If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).MON_TransferredAmount <> 0 Then
            '                    cTotalTransferredAmount = cTotalTransferredAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).MON_TransferredAmount
            '                Else
            '                    cTotalTransferredAmount = cTotalTransferredAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).MON_InvoiceAmount
            '                End If
            '                If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).MON_AccountAmount <> 0 Then
            '                    cTotalAccountAmount = cTotalAccountAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).MON_AccountAmount
            '                Else
            '                    cTotalAccountAmount = cTotalAccountAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).MON_InvoiceAmount
            '                End If
            '                nNoOfPayments = nNoOfPayments + 1
            '            Next lPaymentCounter
            '        Else
            ' New 26.05.05 - Do not consider changes in date!
            '(frmViewer.iBreakLevel = BREAK_ON_ACCOUNTONLY)
            'For lPaymentCounter = iPayments To oBabelFile.Batches(lBatchcounter).Payments.Count
            For lPaymentCounter = 1 To oBabelFile.Batches(lBatchcounter).Payments.Count
                If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).I_Account <> sAccount Then
                    ' continue as long as we have same account
                    Exit For
                End If
                If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).ToSpecialReport = True Then
                    For lInvoiceCounter = 1 To oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices.Count
                        If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).ToSpecialReport = True Then
                            If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).MON_TransferredAmount <> 0 Then
                                cTotalTransferredAmount = cTotalTransferredAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).MON_TransferredAmount
                            Else
                                cTotalTransferredAmount = cTotalTransferredAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).MON_InvoiceAmount
                            End If
                            If oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).MON_AccountAmount <> 0 Then
                                cTotalAccountAmount = cTotalAccountAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).MON_AccountAmount
                            Else
                                cTotalAccountAmount = cTotalAccountAmount + oBabelFile.Batches(lBatchcounter).Payments(lPaymentCounter).Invoices(lInvoiceCounter).MON_InvoiceAmount
                            End If

                        End If
                    Next lInvoiceCounter
                    nNoOfPayments = nNoOfPayments + 1
                End If
            Next lPaymentCounter
            'End If
        Next lBatchcounter


    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property

End Class
