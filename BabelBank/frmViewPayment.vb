﻿Imports DGVPrinterHelper
Public Class frmViewPayment
    Dim oBabelFiles As vbBabel.BabelFiles
    Dim frmViewSearch As frmViewFilter
    Private aColumns(,) As Object
    Private bInGridInvoice As Boolean = False
    Private Sub frmViewPayment_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", 150)
        FormLRSCaptions(Me)
    End Sub
    Private Sub frmViewPayment_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        ' check if searchwindow is open
        If Not frmViewSearch Is Nothing Then
            frmViewSearch.Close()
            frmViewSearch = Nothing
        End If
    End Sub
    Public Sub SetBabelFilesInViewPayment(ByRef NewVal As vbBabel.BabelFiles)
        oBabelFiles = NewVal
    End Sub
    Private Sub gridPayment_CellDoubleClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles gridPayment.CellDoubleClick
        ' Bring up detailform when doubleclicking rowheader
        Dim nBabelFileIndex As Double
        Dim nBatchIndex As Double
        Dim nPaymentIndex As Double

        If e.RowIndex > -1 Then
            nBabelFileIndex = Me.gridPayment.Rows(e.RowIndex).Cells(0).Value
            nBatchIndex = Me.gridPayment.Rows(e.RowIndex).Cells(1).Value
            nPaymentIndex = Me.gridPayment.Rows(e.RowIndex).Cells(2).Value

            If e.ColumnIndex = -1 Then
                ' clicked in row header - present detailinfo
                Dim frmViewPaymentDetail As New frmViewPaymentDetail
                frmViewPaymentDetail.PassPayment(oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex))
                frmViewPaymentDetail.Fill_gridPaymentDetail()
                frmViewPaymentDetail.ShowDialog()
                frmViewPaymentDetail = Nothing
            End If
        End If
    End Sub
    '    Private Sub gridPayment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridPayment.CellContentClick
    ' call form with details pr payment
    Private Sub gridPayment_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles gridPayment.CellMouseClick
        Dim nBabelFileIndex As Double
        Dim nBatchIndex As Double
        Dim nPaymentIndex As Double

        If e.Clicks = 1 Then
            If e.RowIndex > -1 Then
                If (Control.ModifierKeys And Keys.Control) <> Keys.Control Then  ' If Ctrl+Click, do nothing, leave it to default handling (select)

                    nBabelFileIndex = Me.gridPayment.Rows(e.RowIndex).Cells(0).Value
                    nBatchIndex = Me.gridPayment.Rows(e.RowIndex).Cells(1).Value
                    nPaymentIndex = Me.gridPayment.Rows(e.RowIndex).Cells(2).Value

                    If e.ColumnIndex = -1 Then
                        ' clicked in row header - present detailinfo
                        Dim frmViewPaymentDetail As New frmViewPaymentDetail
                        frmViewPaymentDetail.PassPayment(oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex))
                        frmViewPaymentDetail.Fill_gridPaymentDetail()
                        frmViewPaymentDetail.ShowDialog()
                        frmViewPaymentDetail = Nothing
                    Else

                        ' gridconstruct
                        FillGridInvoiceHeaders(nBabelFileIndex, nBatchIndex, nPaymentIndex)
                        ' fill grid
                        FillGridInvoices(nBabelFileIndex, nBatchIndex, nPaymentIndex)
                        ' light gray color for selected column
                        'gridPayment.Columns(e.ColumnIndex).DefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromOle(RGB(192, 192, 192)) ' light gray
                        ' must also set all other columns to white ! - takes to much recources
                    End If
                ElseIf e.ColumnIndex = -1 Then
                    ' rowindex = -1, colindex = -1, upper left cornercell
                    ' TODO reposition to original sorting, turn off filters
                End If
            End If
        End If
    End Sub
    Private Sub gridPayment_RowEnter(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles gridPayment.RowEnter
        ' when we enter a new row, populate gridInvoice
        ' Browse all invoices for current payment
        Dim nBabelFileIndex As Double
        Dim nBatchIndex As Double
        Dim nPaymentIndex As Double

        If e.RowIndex > -1 Then
            nBabelFileIndex = Me.gridPayment.Rows(e.RowIndex).Cells(0).Value
            nBatchIndex = Me.gridPayment.Rows(e.RowIndex).Cells(1).Value
            nPaymentIndex = Me.gridPayment.Rows(e.RowIndex).Cells(2).Value

            If nBabelFileIndex > 0 And nBatchIndex > 0 And nPaymentIndex > 0 Then
                ' gridconstruct
                FillGridInvoiceHeaders(nBabelFileIndex, nBatchIndex, nPaymentIndex)
                ' fill grid
                FillGridInvoices(nBabelFileIndex, nBatchIndex, nPaymentIndex)
            End If
        End If

    End Sub
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.Close()
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        Dim Printer = New DGVPrinter
        Printer.Title = LRS(60002)  ' Payments
        Printer.SubTitle = Me.Text
        Printer.SubTitleFormatFlags = _
           StringFormatFlags.LineLimit Or StringFormatFlags.NoClip
        Printer.PageNumbers = True
        Printer.PageNumberInHeader = False
        Printer.PorportionalColumns = True
        Printer.HeaderCellAlignment = StringAlignment.Near
        Printer.Footer = "BabelBank - Visual Banking AS"
        Printer.FooterSpacing = 15
        Printer.PrintDataGridView(Me.gridPayment)
    End Sub

    Private Sub cmdSaveSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveSetup.Click
        ' lagre i .xml-files eller configuration files eller ini-filer?

        ' XML, se i følgende linker
        ' http://visualbasic.about.com/od/usingvbnet/a/xmlconfig.htm
        ' http://visualbasic.about.com/od/usingvbnet/a/appsettings.htm  (app.config)
        ' http://visualbasic.about.com/od/usingvbnet/a/configlist.htm

        ' config-files

    End Sub
    Private Sub FillGridInvoiceHeaders(ByVal nBabelFileIndex As Double, ByVal nBatchIndex As Double, ByVal nPaymentIndex As Double)
        ' Browse all invoices for actual payment
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim lCounter As Long
        Dim iImportFmt As vbBabel.BabelFiles.FileType

        Try

            ' First, construct grid
            With Me.gridInvoice
                .Columns.Clear()
                .ScrollBars = ScrollBars.Both
                .AllowUserToAddRows = False
                .RowHeadersVisible = True
                .RowHeadersWidth = 60
                .RowHeadersDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowHeadersDefaultCellStyle.SelectionForeColor = Color.Black
                .RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .BorderStyle = BorderStyle.None
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .AutoSize = False
                .ReadOnly = True
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect

                ' then add columns
                ' always a hidden col with babelfileindex as col 0
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' and always a hidden col with batchindex as col 1
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' and always a hidden col with paymentindex as col 2
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' and always a hidden col with invoiceindex as col 3
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                If nPaymentIndex > 0 Then
                    iImportFmt = oBabelFiles.Item(nBabelFileIndex).Batches.Item(nBatchIndex).Payments.Item(nPaymentIndex).ImportFormat
                ElseIf nBatchIndex > 0 Then
                    iImportFmt = oBabelFiles.Item(nBabelFileIndex).Batches.Item(nBatchIndex).ImportFormat
                Else
                    If oBabelFiles.Count > 0 Then
                        ' assume same format for all imported files, otherwise setup will be for the first format
                        iImportFmt = oBabelFiles.Item(nBabelFileIndex).ImportFormat
                    End If
                End If

                aColumns = FindViewColumnsInvoice(iImportFmt, oBabelFiles.Item(1).StatusCode)

                ' Headers, widths, etc
                For lCounter = 0 To aColumns.GetUpperBound(1)
                    txtColumn = New DataGridViewTextBoxColumn
                    txtColumn.HeaderText = aColumns(1, lCounter)
                    txtColumn.Width = aColumns(2, lCounter)
                    If aColumns(0, lCounter) = "MON_InvoiceAmount" Or _
                       aColumns(0, lCounter) = "MON_TransferredAmount" Then
                        txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        txtColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                        txtColumn.DefaultCellStyle.Format = "##,##0.00"
                        txtColumn.ValueType = GetType(System.Double)  ' to sort correctly
                    End If ' to sort correctly
                    .Columns.Add(txtColumn)
                Next

            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub
    Private Sub FillGridInvoices(ByVal nBabelFileIndex As Double, ByVal nBatchIndex As Double, ByVal nPaymentIndex As Double)
        ' fill the grid with content from invoices
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext

        Dim nTotalAmount As Double
        Dim nTotalNoOfPayments As Double
        Dim frmViewBatch As New frmViewBatch
        Dim iImportFmt As vbBabel.BabelFiles.FileType
        Dim nNoOfPayments As Double
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim lCounter As Long
        Dim iCellCounter As Integer
        Dim sTmp As String

        Try
            oBatch = oBabelFiles.Item(nBabelFileIndex).Batches.Item(nBatchIndex)
            oPayment = oBatch.Payments.Item(nPaymentIndex)

            iImportFmt = oPayment.ImportFormat
            nTotalNoOfPayments = 0

            For Each oInvoice In oPayment.Invoices
                ' fill grid
                ' the setup for the grid can either have been save by the user, for each format, or
                ' not saved, but is dependent upon formatdefaults

                With Me.gridInvoice

                    .Rows.Add()
                    .Rows(.RowCount - 1).HeaderCell.Value = .RowCount.ToString

                    .Rows(.RowCount - 1).Cells(0).Value = nBabelFileIndex
                    .Rows(.RowCount - 1).Cells(1).Value = nBatchIndex
                    .Rows(.RowCount - 1).Cells(2).Value = oPayment.Index
                    .Rows(.RowCount - 1).Cells(3).Value = oInvoice.Index
                    iCellCounter = 3  ' Start in col 4, because we have a hidden one in col 0 and 1, 2, 3

                    For lCounter = 0 To aColumns.GetUpperBound(1)
                        iCellCounter = iCellCounter + 1

                        If aColumns(0, lCounter) = "MON_InvoiceAmount" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = oInvoice.MON_InvoiceAmount / 100
                        End If
                        If aColumns(0, lCounter) = "MON_TransferredAmount" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = oInvoice.MON_TransferredAmount / 100
                        End If
                        If aColumns(0, lCounter) = "MON_LocalAmount" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = oInvoice.MON_LocalAmount / 100
                        End If
                        If aColumns(0, lCounter) = "MON_AccountAmount" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = oInvoice.MON_AccountAmount / 100
                        End If
                        If aColumns(0, lCounter) = "Ref_Bank" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.REF_Bank)
                        End If
                        If aColumns(0, lCounter) = "Ref_Own" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.REF_Own)
                        End If
                        If aColumns(0, lCounter) = "Invoice_ID" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.Invoice_ID)
                        End If
                        If aColumns(0, lCounter) = "InvoiceNo" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.InvoiceNo)
                        End If
                        If aColumns(0, lCounter) = "InvoiceDate" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = oInvoice.InvoiceDate
                        End If
                        If aColumns(0, lCounter) = "CustomerNo" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.CustomerNo)
                        End If
                        If aColumns(0, lCounter) = "StateBank_Text" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.STATEBANK_Text)
                        End If
                        If aColumns(0, lCounter) = "StateBank_Date" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.STATEBANK_DATE)
                        End If
                        If aColumns(0, lCounter) = "StateBank_Code" Then
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = Trim(oInvoice.STATEBANK_Code)
                        End If
                        If aColumns(0, lCounter) = "InfoBen" Then
                            sTmp = ""
                            If Not EmptyString(oInvoice.Unique_Id) Then
                                If oPayment.BANK_CountryCode = "NO" Or oPayment.BANK_CountryCode = "" Then
                                    sTmp = "KID: " & Trim(oInvoice.Unique_Id)
                                ElseIf oPayment.BANK_CountryCode = "DK" Then
                                    sTmp = "FIK: " & Trim(oInvoice.Unique_Id)
                                ElseIf oPayment.BANK_CountryCode = "FI" Then
                                    sTmp = "Ref: " & Trim(oInvoice.Unique_Id)
                                Else
                                    sTmp = Trim(oInvoice.Unique_Id)
                                End If
                            ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
                                sTmp = LRS(60158) & " " & Trim(oInvoice.InvoiceNo)
                            Else
                                sTmp = ""
                                For Each oFreeText In oInvoice.Freetexts
                                    sTmp = sTmp & Trim(oFreeText.Text)
                                Next
                            End If
                            .Rows(.RowCount - 1).Cells(iCellCounter).Value = sTmp
                        End If
                    Next lCounter
                End With
            Next 'oBatch

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        ' hvilke kolonner

        'nMON_InvoiceAmount
        'nMON_AccountAmount
        'nMON_TransferredAmount
        'nMON_LocalAmount
        'nMON_DiscountAmount
        'sREF_Bank
        'sREF_Own
        'sInvoice_ID
        'sInvoiceNo
        'sCustomerNo
        'sUnique_Id
        'sInvoiceDate
        'sSTATEBANK_Text
        'sSTATEBANK_Code
        'sSTATEBANK_DATE
        'eTypeOfStructuredInfo
        'oFreetexts


    End Sub
    Private Function FindViewColumnsInvoice(ByVal iImportFmt As vbBabel.BabelFiles.FileType, ByVal sStatusCode As String) As Object(,)
        Dim aReturn(,) As Object
        Dim bSavedSetup As Boolean = False

        ' aReturn holds
        ' (0) CollectionName (field)
        ' (1) Headertext
        ' (2) ColumnWidth
        ' (3) Sorting, A, D or nothing

        ' if not any saved setup, then use default, based in imported format
        If Not bSavedSetup Then

            'nMON_InvoiceAmount
            'nMON_AccountAmount
            'nMON_TransferredAmount
            'nMON_LocalAmount
            'nMON_DiscountAmount
            'sREF_Bank
            'sREF_Own
            'sInvoice_ID
            'sInvoiceNo
            'sCustomerNo
            'sUnique_Id
            'sInvoiceDate
            'sSTATEBANK_Text
            'sSTATEBANK_Code
            'sSTATEBANK_DATE
            'eTypeOfStructuredInfo
            'oFreetexts


            ' Always own  amount, Ref_Own, Ref_Bank, KID/InvoiceNo/Freetext
            ReDim aReturn(3, 3)

            If sStatusCode = "02" Then
                ' use transferredAmount for returnfiles
                aReturn(0, 0) = "MON_TransferredAmount"
            Else
                aReturn(0, 0) = "MON_InvoiceAmount"
            End If
            aReturn(1, 0) = LRS(40021)  ' Amount
            aReturn(2, 0) = "90"
            aReturn(3, 0) = ""

            aReturn(0, 1) = "Ref_Own"
            aReturn(1, 1) = LRS(60072)  ' Ref own
            aReturn(2, 1) = "120"
            aReturn(3, 1) = ""

            aReturn(0, 2) = "Ref_Bank"
            aReturn(1, 2) = LRS(60074)  ' Ref
            aReturn(2, 2) = "120"
            aReturn(3, 2) = ""

            aReturn(0, 3) = "InfoBen"
            aReturn(1, 3) = LRS(60085)  ' Meldingstekst
            aReturn(2, 3) = "400"
            aReturn(3, 3) = ""

            Select Case iImportFmt
                Case vbBabel.BabelFiles.FileType.Telepay, vbBabel.BabelFiles.FileType.Telepay2, vbBabel.BabelFiles.FileType.TelepayPlus, vbBabel.BabelFiles.FileType.TelepayTBIO


                    ' For OCR, DirRem, Autogiro add 
                    ' For Cremul, Debmul, Paymul add
                    ' For LB add

            End Select


        End If

        FindViewColumnsInvoice = aReturn
    End Function


    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        ' call frmViewerReportDialog for setup of report
        Dim frmViewerReportDialog As New frmViewerReportDialog
        Dim sTmp As String
        Dim x As Integer
        With frmViewerReportDialog
            .chkPreview.Checked = True
            .lstSorting.Items.Clear()
            ' TODO for real
            ' er disse lrs-ene OK i setup?
            ' skal vi lage dem i lrscommon ?
            ' TODO Fix LRS
            '.lstSorting.Items.Add(LRS(60563))   '"LRS-Ingen sortering/Ingen brudd") 'NOBREAK
            '.lstSorting.Items.Add(LRS(60564))   '"LRS-Kontonummer, dato") 'BREAK_ON_ACCOUNT (and per day)
            '.lstSorting.Items.Add(LRS(60565))   '"LRS-Kontosummering")BREAK_ON_BATCH
            '.lstSorting.Items.Add(LRS(60566))   '"LRS-Pr. betaling")BREAK_ON_PAYMENT
            '.lstSorting.Items.Add(LRS(60567))   '"LRS-Kontonummer")BREAK_ON_ACCOUNTONLY
            '.lstSorting.Items.Add(LRS(60568))  '"LRS-Klientnummer")BREAK_ON_CLIENT
            .lstSorting.Items.Add("LRS-Ingen sortering/Ingen brudd") 'NOBREAK
            .lstSorting.Items.Add("LRS-Kontonummer, dato") 'BREAK_ON_ACCOUNT (and per day)
            .lstSorting.Items.Add("LRS-Kontosummering")  'BREAK_ON_BATCH
            .lstSorting.Items.Add("LRS-Pr. betaling") 'BREAK_ON_PAYMENT
            .lstSorting.Items.Add("LRS-Kontonummer") 'BREAK_ON_ACCOUNTONLY
            .lstSorting.Items.Add("LRS-Klientnummer") 'BREAK_ON_CLIENT
            .lstSorting.SelectedIndex = 0

            .ShowDialog()
        End With
        If frmViewerReportDialog.DialogResult = 1 Then
            Dim oBabelReport As New vbBabel.BabelReport
            With frmViewerReportDialog
                oBabelReport.ReportNumber = "003"
                oBabelReport.Heading = .txtHeading.Text
                oBabelReport.BreakLevel = .lstSorting.SelectedIndex + 1 ' TODO check !!!
                oBabelReport.Preview = .chkPreview.Checked
                oBabelReport.ToPrint = .chkPrint.Checked
                oBabelReport.ExportFilename = .txtFilename.Text
                oBabelReport.IncludeOCR = True
                If .optHTML.Checked Then
                    oBabelReport.ExportType = "HTML"
                ElseIf .optPDF.Checked Then
                    oBabelReport.ExportType = "PDF"
                ElseIf .optRTF.Checked Then
                    oBabelReport.ExportType = "RTF"
                ElseIf .optTIFF.Checked Then
                    oBabelReport.ExportType = "TIFF"
                ElseIf .optTXT.Checked Then
                    oBabelReport.ExportType = "TXT"
                ElseIf .optXLS.Checked Then
                    oBabelReport.ExportType = "XLS"
                Else
                    oBabelReport.ExportFilename = ""
                    oBabelReport.ExportType = ""
                End If
                If .chkeMail.Checked Then
                    oBabelReport.ToEmail = True
                    ' add one or more e-mail addresses into reportobject
                    sTmp = .txteMail.Text
                    If InStr(sTmp, ";") > 0 Then
                        oBabelReport.AddEmailAddress(Strings.Left(sTmp, InStr(sTmp, ";") - 1))
                        sTmp = Strings.Mid(sTmp, InStr(sTmp, ";") + 1)
                    Else
                        oBabelReport.AddEmailAddress(sTmp)
                    End If
                End If
                ' Must mark which payments to report.
                ' If payments are filtered, do not report them
                oBabelReport.ReportOnSelectedItems = True  ' So far, always sets this one to true. False only when the complete BabelFiles will be reported

                ' If any rows are selected, then report on those only
                ' Otherwise, report on all visible ones.
                If Me.gridPayment.SelectedRows.Count > 1 Then  ' >1, because there will always be at least one row marked. How to print one single one ?
                    ' Mark all selected records
                    While x < Me.gridPayment.Rows.Count
                        If Me.gridPayment.Rows(x).Selected Then
                            oBabelFiles(Me.gridPayment.Rows(x).Cells(0).Value).Batches(Me.gridPayment.Rows(x).Cells(1).Value).Payments(Me.gridPayment.Rows(x).Cells(2).Value).ToSpecialReport = True
                        Else
                            oBabelFiles(Me.gridPayment.Rows(x).Cells(0).Value).Batches(Me.gridPayment.Rows(x).Cells(1).Value).Payments(Me.gridPayment.Rows(x).Cells(2).Value).ToSpecialReport = False
                        End If
                        System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
                    End While
                Else
                    ' Mark all visible records
                    While x < Me.gridPayment.Rows.Count
                        If Me.gridPayment.Rows(x).Visible Then
                            oBabelFiles(Me.gridPayment.Rows(x).Cells(0).Value).Batches(Me.gridPayment.Rows(x).Cells(1).Value).Payments(Me.gridPayment.Rows(x).Cells(2).Value).ToSpecialReport = True
                        Else
                            oBabelFiles(Me.gridPayment.Rows(x).Cells(0).Value).Batches(Me.gridPayment.Rows(x).Cells(1).Value).Payments(Me.gridPayment.Rows(x).Cells(2).Value).ToSpecialReport = False
                        End If
                        System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
                    End While
                End If
                oBabelReport.BabelFiles = oBabelFiles
                oBabelReport.RunReport()
                oBabelReport = Nothing

            End With
        End If

    End Sub
    Private Sub gridInvoice_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles gridInvoice.CellMouseClick
        Dim nBabelFileIndex As Double
        Dim nBatchIndex As Double
        Dim nPaymentIndex As Double
        Dim nInvoiceIndex As Double

        If e.RowIndex > -1 Then
            nBabelFileIndex = Me.gridInvoice.Rows(e.RowIndex).Cells(0).Value
            nBatchIndex = Me.gridInvoice.Rows(e.RowIndex).Cells(1).Value
            nPaymentIndex = Me.gridInvoice.Rows(e.RowIndex).Cells(2).Value
            nInvoiceIndex = Me.gridInvoice.Rows(e.RowIndex).Cells(3).Value

            If e.ColumnIndex = -1 Then
                ' clicked in row header - present detailinfo
                Dim frmViewPaymentDetail As New frmViewPaymentDetail
                frmViewPaymentDetail.PassInvoice(oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(nInvoiceIndex))
                frmViewPaymentDetail.Fill_gridInvoiceDetail()
                frmViewPaymentDetail.ShowDialog()
                frmViewPaymentDetail = Nothing
            End If
        End If
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        ' Bring up searchwindow
        frmViewSearch = New frmViewFilter
        ' gridPayment or gridInvoice ?
        If bInGridInvoice Then
            frmViewSearch.passGrid(Me.gridInvoice)
        Else
            frmViewSearch.passGrid(Me.gridPayment)
        End If
        frmViewSearch.SetBabelFilesInViewSearch(oBabelFiles)
        frmViewSearch.chkFromStartOfCell.Checked = True
        frmViewSearch.chkFilter.Checked = False
        frmViewSearch.txtSearchFor.Focus()
        frmViewSearch.Show() ' modeless
    End Sub
    Private Sub gridPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridPayment.Click
        If bInGridInvoice Then
            ' Mark "active" grid in upper left corner
            gridInvoice.TopLeftHeaderCell.Style.BackColor = gridPayment.RowHeadersDefaultCellStyle.BackColor
            gridPayment.TopLeftHeaderCell.Style.BackColor = Color.LightBlue
        End If
        bInGridInvoice = False
    End Sub
    Private Sub gridInvoice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridInvoice.Click
        If Not bInGridInvoice Then
            gridPayment.TopLeftHeaderCell.Style.BackColor = gridPayment.RowHeadersDefaultCellStyle.BackColor
            gridInvoice.TopLeftHeaderCell.Style.BackColor = Color.LightBlue
        End If
        bInGridInvoice = True
    End Sub
    Private Sub frmViwPayment_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles Me.KeyUp
        ' hotkey Ctrl+F = Search
        Try
            If e.Control = True And e.KeyCode = Keys.F Then
                cmdSearch_Click(sender, e)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class