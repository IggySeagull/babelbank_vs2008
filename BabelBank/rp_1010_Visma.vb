Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_1010_Visma 
    Private oBabelFiles As BabelFiles
    ' Declare all objects to be used in actual report:
    Dim oBabelFile As BabelFile
    Dim oBatch As Batch
    Dim oPayment As Payment
    ' added 30.05.2014
    Dim oInvoice As Invoice
    Dim cTotalAmount As Decimal

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bSQLServer As Boolean = False
    Dim bIncludeOCR As Boolean
    Private Sub rp_1010_Visma_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_1010_Visma_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Declare all fields to be used in actual report:
        ' Header
        Fields.Add("fldDateTime")
        ' grFile

        Fields.Add("fldAccountNo")
        Fields.Add("fldClientNo")
        Fields.Add("fldType")
        Fields.Add("fldFilename")
        Fields.Add("fldValDate")
        Fields.Add("fldNumber")
        Fields.Add("fldAmount")
        Fields.Add("fldBankRef")
        Fields.Add("fldCurrency")
        Fields.Add("fldBreak")

        ' Sum
        Fields.Add("fldTotalAmount")

        ' Footer
        Fields.Add("fldRunDate")
    End Sub
    Private Sub rp_1010_Visma_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart
        ' Dataobject:;
        cTotalAmount = 0

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            'Me.txtDATE_Production.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            'Me.txtDATE_Production.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        ' If not set any userdefined heading, use default;
        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(60118)   ' Report batches
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4

        lblFilename.Text = LRS(40001)
        Me.lblBatchfooterNoofPayments.Text = LRS(40106)  '"Total - Antall:" '
        Me.lblBatchfooterAmount.Text = LRS(40017) '"Bel�p:" 
        'Me.lblFileSum.text = LRS(40138) '  Sum / Total / Summa

        ' headings
        Me.lblHeadClientNo.Text = LRS(60107)
        Me.lblType.Text = LRS(60091)
        Me.lblAccount.Text = LRS(40048)
        Me.lblNumber.Text = LRS(40019)
        Me.lblCurrency.Text = LRS(60066)
        Me.lblAmount.Text = LRS(40021)

        'If iTotalLevel = 1 Then
        '    ' Hide totals
        '    Me.txtFileSumAmount.Visible = False
        '    Me.lblFileSum.Visible = False
        'End If

        ' Show details or not
        'If iDetailLevel = 0 Then
        '    ' no details
        '    Me.txtNumber.Top = 1
        '    Me.txtAmount.Top = 1
        '    Me.txtCurrency.Top = 1
        '    ' .NET 17.06.2010 - added next line (txtAccountNo /fldAccountNo in Detailssection)
        '    Me.txtAccountNo.Top = 1
        '    ' Resize detailssection;
        '    Me.Detail.Height = 255
        'End If

    End Sub
    Private Sub rp_1010_Visma_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        ' Counters for items in the different Babel collections
        Static iBabelFiles As Integer
        Static iLastUsedBabelFilesItem As Integer
        Static iBatches As Integer
        Static xCreditAccount As String
        Static xBabelFiles As Long
        Static xAccount As String
        Dim sCurrency As String
        Dim bOK As Boolean

        Dim nNoOfPayments As Long
        Dim nBatchAmount As Double

        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed

        Dim bContinue As Boolean
        iBatches = iBatches + 1
        eArgs.EOF = False
        If iBabelFiles = 0 Then
            iBabelFiles = 1
            iBatches = 1
            xCreditAccount = "xxxx"
            xBabelFiles = 0
        End If

        ' oBatches are organized into one International and one Domestic pr oBabelFile
        ' Batchtotals are recalculated to hold totals for exportmarked payments only

        ' ----------------------------------------------------------------------------
        ' Spin through collections to find next suitbable record before any other
        ' processing is done
        ' Position to correct items in collections
        Do Until iBabelFiles > oBabelFiles.Count
            ' Try to set as few times as possible
            If oBabelFile Is Nothing Then
                oBabelFile = oBabelFiles(iBabelFiles)
                iLastUsedBabelFilesItem = 1 'oBabelFile.Index
            Else
                If iLastUsedBabelFilesItem <> iBabelFiles Then 'oBabelFile.Index Then
                    oBabelFile = oBabelFiles(iBabelFiles)
                    iLastUsedBabelFilesItem = iBabelFiles 'oBabelFile.Index
                End If
            End If

            Do Until iBatches > oBabelFile.Batches.Count
                oBatch = oBabelFile.Batches(iBatches)

                ' sjekk om alle i bunten er spesialmarkert
                bOK = False
                For Each oPayment In oBatch.Payments
                    If oPayment.ToSpecialReport = True Then
                        bOK = True
                        Exit For
                    End If
                Next oPayment
                If Not bOK Then
                    ' advance to next batch
                    bContinue = False
                    iBatches = iBatches + 1
                Else
                    ' normal case, exit to show in report
                    bContinue = True
                    Exit Do
                End If

            Loop ' iBatches
            If bContinue Then
                Exit Do
            End If
            iBabelFiles = iBabelFiles + 1
            iBatches = 1
        Loop ' iBabelFiles

        If iBabelFiles > oBabelFiles.Count Then
            eArgs.EOF = True
            iBabelFiles = 0 ' to reset statics next time !

            Exit Sub
        End If

        ' Report filename only if one file imported;
        If Not EmptyString(oBabelFile.FilenameIn) And oBabelFiles.Count = 1 Then
            If oBabelFile.FilenameIn.Length > 36 Then
                Fields("fldFilename").Value = "..." & Right(oBabelFile.FilenameIn, 36)
            Else
                Fields("fldFilename").Value = oBabelFile.FilenameIn
            End If
        Else
            ' more than one file
            Me.lblFilename.Visible = False
            Me.txtFilename.Visible = False
        End If


        ' 04.09.2009 - oBatch.Payments(1).I_Account gave errors for empty batches !!
        ' added new If
        If oBatch.Payments.Count = 0 Then
            Me.Detail.Visible = False ' nothing to show!
            Fields("fldAmount").Value = 0  ' To make the reporttotal correct
            Exit Sub
        End If

        Me.Detail.Visible = True
        ' added break on file as well 20.02.2008
        If iBabelFiles <> xBabelFiles Or oBatch.Payments(1).I_Account <> xCreditAccount Then
            xBabelFiles = iBabelFiles
            xCreditAccount = oBatch.Payments(1).I_Account
            cTotalAmount = 0
        End If

        'NET - 30.04.2010 - Added next IF. Before it was always break on iBabelFiles and I_Account
        If iBreakLevel = NOBREAK Then
            'Fields("fldBreak").Value = Str(iBabelFiles)
        Else
            Fields("fldBreak").Value = Str(iBabelFiles) & oBatch.Payments(1).I_Account
        End If

        If oBatch.Payments.Count > 0 Then
            If oBatch.Payments(1).I_Account <> xAccount Then
                xAccount = oBatch.Payments(1).I_Account
                cTotalAmount = 0
            End If
        End If


        'Details data
        '----------------
        'Fields("fldFilename").Value = oBabelFile.FilenameIn
        'Fields("fldBankRef").Value = oBatch.REF_Bank

        'If oBatch.MON_TransferredAmount <> 0 Then

        Fields("fldClientNo").Value = oBatch.Payments(1).I_Client

        '    If oBatch.PayType = "I" Then
        '        Fields("fldType").Value = LRS(60140)   '"International"
        '    Else
        '        Fields("fldType").Value = LRS(60139)  '"Domestic"
        '    End If
        ' 08.11.2013 show Mottaksretur, Avregnings, Innbetaling, etc, based on info in first payment in Batch;
        If oBabelFile.TypeOfTransaction = vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Outgoing Then
            ' Give status instead;
            If oBatch.Payments.Count > 0 And oBatch.Payments(1).Invoices.Count > 0 Then
                If Val(oBatch.Payments(1).StatusCode) = 0 Then
                    Fields("fldType").Value = LRS(36063)    ' Utbetaling, innsending
                ElseIf Val(oBatch.Payments(1).StatusCode) = 1 And Val(oBatch.Payments(1).Invoices(1).StatusCode) = 1 Then
                    Fields("fldType").Value = LRS(36059)  ' Mottaksretur
                ElseIf Val(oBatch.Payments(1).StatusCode) = 2 And Val(oBatch.Payments(1).Invoices(1).StatusCode) = 2 Then
                    Fields("fldType").Value = LRS(36060)  ' Avregning
                Else
                    Fields("fldType").Value = LRS(36061)     ' Avvisning
                End If
            Else
                Fields("fldType").Value = ""
            End If
        Else
            ' Incoming payments
            Fields("fldType").Value = LRS(36062)   ' Innbetalinger
        End If

        Fields("fldAccountNo").Value = xAccount

        ' calculate batchamount
        nBatchAmount = 0
        For Each oPayment In oBatch.Payments
            If oPayment.ToSpecialReport Then
                'nBatchAmount = nBatchAmount + oPayment.MON_InvoiceAmount
                ' 30.05.2014 Changed to reflect InvoiceLevel
                For Each oInvoice In oPayment.Invoices
                    If oInvoice.ToSpecialReport = True Then
                        nBatchAmount = nBatchAmount + oInvoice.MON_InvoiceAmount
                    End If
                Next oInvoice

            End If
        Next oPayment
        Fields("fldAmount").Value = nBatchAmount / 100
        If oBatch.Payments.Count > 0 Then
            sCurrency = oBatch.Payments(1).MON_TransferCurrency
            For Each oPayment In oBatch.Payments
                If sCurrency <> oPayment.MON_TransferCurrency Then
                    sCurrency = ""
                    Exit For
                End If
            Next
            Fields("fldCurrency").Value = sCurrency
        Else
            Fields("fldCurrency").Value = ""
        End If
        cTotalAmount = cTotalAmount + oBatch.MON_InvoiceAmount
        'End If
        If oBatch.DATE_Production <> "19900101" Then
            If bUseLongDate Then
                Fields("fldValDate").Value = Format(StringToDate(oBatch.DATE_Production), "Long Date")
            Else
                Fields("fldValDate").Value = CStr(StringToDate(oBatch.DATE_Production))
            End If
        Else
            ' Show todays date
            Fields("fldValDate").Value = Now()
        End If

        nNoOfPayments = 0
        For Each oPayment In oBatch.Payments
            If oPayment.ToSpecialReport Then
                nNoOfPayments = nNoOfPayments + 1
            End If
        Next
        Fields("fldNumber").Value = CInt(nNoOfPayments)

    End Sub

    Private Sub rp_1010_Visma_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportEnd
        'If Not oBabelFiles Is Nothing Then
        '    Set oBabelFiles = Nothing
        'End If
        'If Not oBabelFile Is Nothing Then
        '    Set oBabelFile = Nothing
        'End If
        If Not oBatch Is Nothing Then
            oBatch = Nothing
        End If
        If Not oPayment Is Nothing Then
            oPayment = Nothing
        End If
        'frmViewer.SetBabelFilesToNothing
        cTotalAmount = 0
        'nNoOfPayments = 0

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
            If sSpecial = "VismaBusiness" Then
                bReportOnSelectedItems = True
            End If
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property

End Class
