Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("FileSetup_NET.FileSetup")> Public Class FileSetup
	
	Public nIndex As Double
	
	Private oClients As vbbabel.Clients
	Private oReports As vbbabel.Reports ' New 05.03.03, Hook Reports to each Filesetup
	Private nFileSetup_ID As Double
	Private iFormat_ID, iEnum_ID As Short
	Private lBank_ID, lEnumBank_ID As Integer
	Private sShortName, sDescription As String
	Private bSilent As Boolean
	Private iFilesetupOut As Short
	Private sFileNameIn2, sFileNameIn1, sFileNameIn3 As String
	Private bOverWriteIn, bBackupIn, bWarningIn As Boolean
	Private sFileNameOut2, sFileNameOut1, sFileNameOut3 As String
	Private bWarningOut, bBackupOut, bOverWriteOut, bRemoveOldOut As Boolean
	Private bNotSplitOnAccount As Boolean
    Private eSeqnoType As vbBabel.BabelFiles.SeqnoType
	Private nSeqnoTotal, nSeqNoDay As Double
	Private sDivision As String
	Private sCompanyNo, sAdditionalNo As String
	Private bInfoNewClient, bFromAccountingSystem, bKeepBatch As Boolean
	Private bCtrlNegative, bCtrlZip As Boolean
	Private bCtrlKID, bCtrlAccount, bReturnMass As Boolean
	Private bRejectWrong, bChangeDateDue, bChangeDateAll, bRedoWrong As Boolean
	Private bClientToScreen, bAccumulation, bClientToPrint As Boolean
	Private bClientToFile, bClientToMail As Boolean
	Private bReportToPrint2, bReportToPrint1, bReportToPrint3 As Boolean
	Private bReportToScreen2, bReportToScreen1, bReportToScreen3 As Boolean
	Private sMessage As String
    Private eStatus As vbBabel.Profile.CollectionStatus
	Private sVersion As String
	Private bEDIFormat As Boolean
	'Added 23.10.02 by Kjell
	Private bAutoMatch As Boolean
	' Added 30.10.02 by JanP
	Private bMatchOCR As Boolean
	' Added 13.05.03 by JanP
	Private iShowCorrections As Short ' 0=Show nothing, 1=Errors only, 2=Errors&Warnings
	Private sFilenameOut4 As String
	Private iFormatOutID3, iFormatOutID1, iFormatOutID2, iFormatOutID4 As Short
	Private iMATCHOutAutogiro, iMATCHOutGL, iMATCHOutOCR, iMATCHOutRest As Short
	Private iDelDays As Short
	Private bReportAllPayments As Boolean
	Private iMergePayments As Short
	Private sPaymentType As String
	Private bCrypt As Boolean 'Re: DnB TBIW
	'Added 29.04.2005 by Kjell
	Private bSplitAfter450 As Boolean
	' added for setup-reasons ver 1.0.20 - 21
	Private bSetup_NewSend, bSetup_Send, bSetup_Return, bSetup_NewReturn As Boolean
	Private bSetup_InitSend, bSetup_InitReturn As Boolean
	Private sPostJCL, sPreJCL, sTreatSpecial As String
	Private bLogOverwrite, bLog, bLogShowStartStop As Boolean
	Private nLogType As Byte
	Private sLogFilename As String
	Private iLogDetails As Short
	' 31.10.2007 - changed to double from integer
	Private nLogMaxLines As Double
	Private bNotificationMail, bNotificationPrint As Boolean
	' 13.11.2008 - added bChargesFromSetup
	Private bChargeMeAbroad, bChargeMeDomestic, bChargesFromSetup As Boolean
	Private bManual As Boolean ' New 18.10.05
	Private lCodeNB As Integer
	Private sExplanationNB As String
	Private sStructText As String
	Private iStructInvoicePos As Short
	Private sStructTextNegative As String
	Private iStructInvoicePosNegative As Short
	Private bSplitOCRandFBO, bExtractInfofromKID As Boolean
	Private sBackUpPath As String
	Private iAddDays As Short
	Private sMappingFilename As String
	Private iSorting As Short
	'01.07.2008
	Private bUseSignalFile As Boolean
	Private sFilenameSignalFile As String
	Private bValidateFormatAtImport As Boolean
    ' XNET 13.12.2010 added sSpecialMarker
    Private sSpecialMarker As String
    'XNET 03.07.2012 added CurrencyToUse
    Private sCurrencyToUse As String
    ' XNET 03.09.2012 added 2 fields
    Private sDivision2 As String
    Private bTelepayDebitNO As Boolean
    ' XNET 06.11.2012
    Private bCodeNBCurrencyOnly As Boolean
    'XokNET 23.06.2014
    Private sStandardPurposeCode As String
    ' added 03.02.2016
    Private bSplitFiles As Boolean
    Private bISO20022 As Boolean
    ' 05.01.2017 Security
    Private sHash As String
    Private sCypher As String
    Private bEncrypt As Boolean
    Private bSign As Boolean
    Private bSecureEnvelope As Boolean
    Private sFileNamePublicKey As String
    Private sFileNameBankPublicKey As String
    Private sCompression As String
    Private sOurPublicKey As String
    Private sOurPrivateKey As String
    Private sKeyCreationDate As String
    Private sSecUserID As String
    Private sSecPassword As String
    Private sBANKPublicKey As String
    Private eFileEncoding As BabelFiles.Encoding
    Private bConfidential As Boolean
    Private sFilenameSignalFileError As String
    '********* START PROPERTY SETTINGS ***********************
	Public Property Setup_InitSend() As Boolean
		Get
			Setup_InitSend = bSetup_InitSend
		End Get
		Set(ByVal Value As Boolean)
			bSetup_InitSend = Value
		End Set
	End Property
	Public Property Setup_InitReturn() As Boolean
		Get
			Setup_InitReturn = bSetup_InitReturn
		End Get
		Set(ByVal Value As Boolean)
			bSetup_InitReturn = Value
		End Set
	End Property
	Public Property Clients() As vbbabel.Clients
		Get
			If oClients Is Nothing Then
				oClients = New vbbabel.Clients
			End If
			Clients = oClients
		End Get
		Set(ByVal Value As vbbabel.Clients)
			oClients = Value
		End Set
	End Property
	Public Property Reports() As vbbabel.Reports
		Get
			If oReports Is Nothing Then
				oReports = New vbbabel.Reports
			End If
			Reports = oReports
		End Get
		Set(ByVal Value As vbbabel.Reports)
			oReports = Value
		End Set
	End Property
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	Public Property FileSetup_ID() As Double
		Get
			FileSetup_ID = nFileSetup_ID
		End Get
		Set(ByVal Value As Double)
			nFileSetup_ID = Value
		End Set
	End Property
	Public Property Format_ID() As Short
		Get
			Format_ID = iFormat_ID
		End Get
		Set(ByVal Value As Short)
			iFormat_ID = Value
		End Set
	End Property
	Public Property Enum_ID() As Short
		Get
			Enum_ID = iEnum_ID
		End Get
		Set(ByVal Value As Short)
			iEnum_ID = Value
		End Set
	End Property
	Public Property Bank_ID() As Integer
		Get
			Bank_ID = lBank_ID
		End Get
		Set(ByVal Value As Integer)
			lBank_ID = Value
		End Set
	End Property
	Public Property EnumBank_ID() As Integer
		Get
			EnumBank_ID = lEnumBank_ID
		End Get
		Set(ByVal Value As Integer)
			lEnumBank_ID = Value
		End Set
	End Property
	Public Property ShortName() As String
		Get
			ShortName = sShortName
		End Get
		Set(ByVal Value As String)
			sShortName = Value
		End Set
	End Property
	Public Property PreJCL() As String
		Get
			PreJCL = sPreJCL
		End Get
		Set(ByVal Value As String)
			sPreJCL = Value
		End Set
	End Property
	Public Property PostJCL() As String
		Get
			PostJCL = sPostJCL
		End Get
		Set(ByVal Value As String)
			sPostJCL = Value
		End Set
	End Property
	Public Property TreatSpecial() As String
		Get
			TreatSpecial = sTreatSpecial
		End Get
		Set(ByVal Value As String)
			sTreatSpecial = Value
		End Set
	End Property
	Public Property Description() As String
		Get
			Description = sDescription
		End Get
		Set(ByVal Value As String)
			sDescription = Value
		End Set
	End Property
	Public Property Silent() As Boolean
		Get
			Silent = bSilent
		End Get
		Set(ByVal Value As Boolean)
			bSilent = Value
		End Set
	End Property
	Public Property Log() As Boolean
		Get
			Log = bLog
		End Get
		Set(ByVal Value As Boolean)
			bLog = Value
		End Set
	End Property
	Public Property LogOverwrite() As Boolean
		Get
			LogOverwrite = bLogOverwrite
		End Get
		Set(ByVal Value As Boolean)
			bLogOverwrite = Value
		End Set
	End Property
	Public Property LogShowStartStop() As Boolean
		Get
			LogShowStartStop = bLogShowStartStop
		End Get
		Set(ByVal Value As Boolean)
			bLogShowStartStop = Value
		End Set
	End Property
	Public Property LogType() As Byte
		Get
			LogType = nLogType '*********************************************
		End Get
		Set(ByVal Value As Byte)
			nLogType = Value '***********************************************
		End Set
	End Property
	Public Property LogMaxLines() As Double
		Get
			LogMaxLines = nLogMaxLines
		End Get
		Set(ByVal Value As Double)
			nLogMaxLines = Value
		End Set
	End Property
	Public Property LogDetails() As Short
		Get
			LogDetails = iLogDetails
		End Get
		Set(ByVal Value As Short)
			iLogDetails = Value
		End Set
	End Property
	Public Property LogFilename() As String
		Get
			LogFilename = sLogFilename
		End Get
		Set(ByVal Value As String)
			sLogFilename = Value
		End Set
	End Property
	Public Property FileSetupOut() As Short
		Get
			FileSetupOut = iFilesetupOut
		End Get
		Set(ByVal Value As Short)
			iFilesetupOut = Value
		End Set
	End Property
	Public Property FileNameIn1() As String
		Get
			FileNameIn1 = sFileNameIn1
		End Get
		Set(ByVal Value As String)
			sFileNameIn1 = Value
		End Set
	End Property
	Public Property FileNameIn2() As String
		Get
			FileNameIn2 = sFileNameIn2
		End Get
		Set(ByVal Value As String)
			sFileNameIn2 = Value
		End Set
	End Property
	Public Property FileNameIn3() As String
		Get
			FileNameIn3 = sFileNameIn3
		End Get
		Set(ByVal Value As String)
			sFileNameIn3 = Value
		End Set
	End Property
	Public Property Manual() As Boolean
		Get
			Manual = bManual
		End Get
		Set(ByVal Value As Boolean)
			bManual = Value
		End Set
	End Property
	Public Property BackupIn() As Boolean
		Get
			BackupIn = bBackupIn
		End Get
		Set(ByVal Value As Boolean)
			bBackupIn = Value
		End Set
	End Property
	Public Property OverWriteIn() As Boolean
		Get
			OverWriteIn = bOverWriteIn
		End Get
		Set(ByVal Value As Boolean)
			bOverWriteIn = Value
		End Set
	End Property
	Public Property WarningIn() As Boolean
		Get
			WarningIn = bWarningIn
		End Get
		Set(ByVal Value As Boolean)
			bWarningIn = Value
		End Set
	End Property
	Public Property FileNameOut1() As String
		Get
			FileNameOut1 = sFileNameOut1
		End Get
		Set(ByVal Value As String)
			sFileNameOut1 = Value
		End Set
	End Property
	Public Property FileNameOut2() As String
		Get
			FileNameOut2 = sFileNameOut2
		End Get
		Set(ByVal Value As String)
			sFileNameOut2 = Value
		End Set
	End Property
	Public Property FileNameOut3() As String
		Get
			FileNameOut3 = sFileNameOut3
		End Get
		Set(ByVal Value As String)
			sFileNameOut3 = Value
		End Set
	End Property
	Public Property BackupOut() As Boolean
		Get
			BackupOut = bBackupOut
		End Get
		Set(ByVal Value As Boolean)
			bBackupOut = Value
		End Set
	End Property
	Public Property OverWriteOut() As Boolean
		Get
			OverWriteOut = bOverWriteOut
		End Get
		Set(ByVal Value As Boolean)
			bOverWriteOut = Value
		End Set
	End Property
	Public Property WarningOut() As Boolean
		Get
			WarningOut = bWarningOut
		End Get
		Set(ByVal Value As Boolean)
			bWarningOut = Value
		End Set
	End Property
	Public Property RemoveOldOut() As Boolean
		Get
			RemoveOldOut = bRemoveOldOut
		End Get
		Set(ByVal Value As Boolean)
			bRemoveOldOut = Value
		End Set
	End Property
	Public Property NotSplitOnAccount() As Boolean
		Get
			NotSplitOnAccount = bNotSplitOnAccount
		End Get
		Set(ByVal Value As Boolean)
			bNotSplitOnAccount = Value
		End Set
	End Property
    Public Property SeqnoType() As vbBabel.BabelFiles.SeqnoType
        Get
            SeqnoType = eSeqnoType
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.SeqnoType)
            eSeqnoType = Value
        End Set
    End Property
	Public Property SeqNoTotal() As Double
		Get
			SeqNoTotal = nSeqnoTotal
		End Get
		Set(ByVal Value As Double)
			nSeqnoTotal = Value
		End Set
	End Property
	Public Property SeqNoDay() As Double
		Get
			SeqNoDay = nSeqNoDay
		End Get
		Set(ByVal Value As Double)
			nSeqNoDay = Value
		End Set
	End Property
	Public Property Division() As String
		Get
			Division = sDivision
		End Get
		Set(ByVal Value As String)
			sDivision = Value
		End Set
	End Property
	Public Property CompanyNo() As String
		Get
			CompanyNo = sCompanyNo
		End Get
		Set(ByVal Value As String)
			sCompanyNo = Value
		End Set
	End Property
	Public Property AdditionalNo() As String
		Get
			AdditionalNo = sAdditionalNo
		End Get
		Set(ByVal Value As String)
			sAdditionalNo = Value
		End Set
	End Property
	Public Property FromAccountingSystem() As Boolean
		Get
			FromAccountingSystem = bFromAccountingSystem
		End Get
		Set(ByVal Value As Boolean)
			bFromAccountingSystem = Value
		End Set
	End Property
	Public Property InfoNewClient() As Boolean
		Get
			InfoNewClient = bInfoNewClient
		End Get
		Set(ByVal Value As Boolean)
			bInfoNewClient = Value
		End Set
	End Property
	Public Property KeepBatch() As Boolean
		Get
			KeepBatch = bKeepBatch
		End Get
		Set(ByVal Value As Boolean)
			bKeepBatch = Value
		End Set
	End Property
	Public Property CtrlNegative() As Boolean
		Get
			CtrlNegative = bCtrlNegative
		End Get
		Set(ByVal Value As Boolean)
			bCtrlNegative = Value
		End Set
	End Property
	Public Property ReturnMass() As Boolean
		Get
			ReturnMass = bReturnMass
		End Get
		Set(ByVal Value As Boolean)
			bReturnMass = Value
		End Set
	End Property
	Public Property CtrlZip() As Boolean
		Get
			CtrlZip = bCtrlZip
		End Get
		Set(ByVal Value As Boolean)
			bCtrlZip = Value
		End Set
	End Property
	Public Property CtrlAccount() As Boolean
		Get
			CtrlAccount = bCtrlAccount
		End Get
		Set(ByVal Value As Boolean)
			bCtrlAccount = Value
		End Set
	End Property
	Public Property CtrlKID() As Boolean
		Get
			CtrlKID = bCtrlKID
		End Get
		Set(ByVal Value As Boolean)
			bCtrlKID = Value
		End Set
	End Property
	Public Property ChangeDateDue() As Boolean
		Get
			ChangeDateDue = bChangeDateDue
		End Get
		Set(ByVal Value As Boolean)
			bChangeDateDue = Value
		End Set
	End Property
	Public Property ChangeDateAll() As Boolean
		Get
			ChangeDateAll = bChangeDateAll
		End Get
		Set(ByVal Value As Boolean)
			bChangeDateAll = Value
		End Set
	End Property
	Public Property RejectWrong() As Boolean
		Get
			RejectWrong = bRejectWrong
		End Get
		Set(ByVal Value As Boolean)
			bRejectWrong = Value
		End Set
	End Property
	Public Property RedoWrong() As Boolean
		Get
			RedoWrong = bRedoWrong
		End Get
		Set(ByVal Value As Boolean)
			bRedoWrong = Value
		End Set
	End Property
	Public Property Accumulation() As Boolean
		Get
			Accumulation = bAccumulation
		End Get
		Set(ByVal Value As Boolean)
			bAccumulation = Value
		End Set
	End Property
	Public Property ClientToScreen() As Boolean
		Get
			ClientToScreen = bClientToScreen
		End Get
		Set(ByVal Value As Boolean)
			bClientToScreen = Value
		End Set
	End Property
	Public Property ClientToPrint() As Boolean
		Get
			ClientToPrint = bClientToPrint
		End Get
		Set(ByVal Value As Boolean)
			bClientToPrint = Value
		End Set
	End Property
	Public Property ClientToFile() As Boolean
		Get
			ClientToFile = bClientToFile
		End Get
		Set(ByVal Value As Boolean)
			bClientToFile = Value
		End Set
	End Property
	Public Property ClientToMail() As Boolean
		Get
			ClientToMail = bClientToMail
		End Get
		Set(ByVal Value As Boolean)
			bClientToMail = Value
		End Set
	End Property
	Public Property ReportToPrint1() As Boolean
		Get
			ReportToPrint1 = bReportToPrint1
		End Get
		Set(ByVal Value As Boolean)
			bReportToPrint1 = Value
		End Set
	End Property
	Public Property ReportToPrint2() As Boolean
		Get
			ReportToPrint2 = bReportToPrint2
		End Get
		Set(ByVal Value As Boolean)
			bReportToPrint2 = Value
		End Set
	End Property
	Public Property ReportToPrint3() As Boolean
		Get
			ReportToPrint3 = bReportToPrint3
		End Get
		Set(ByVal Value As Boolean)
			bReportToPrint3 = Value
		End Set
	End Property
	Public Property ReportToScreen1() As Boolean
		Get
			ReportToScreen1 = bReportToScreen1
		End Get
		Set(ByVal Value As Boolean)
			bReportToScreen1 = Value
		End Set
	End Property
	Public Property ReportToScreen2() As Boolean
		Get
			ReportToScreen2 = bReportToScreen2
		End Get
		Set(ByVal Value As Boolean)
			bReportToScreen2 = Value
		End Set
	End Property
	Public Property ReportToScreen3() As Boolean
		Get
			ReportToScreen3 = bReportToScreen3
		End Get
		Set(ByVal Value As Boolean)
			bReportToScreen3 = Value
		End Set
	End Property
	
    Public Property Status() As vbBabel.Profile.CollectionStatus
        Get
            Status = eStatus
        End Get
        Set(ByVal Value As vbBabel.Profile.CollectionStatus)
            If CDbl(Value) = 0 Then
                eStatus = Profile.CollectionStatus.NoChange
            End If
            If CDbl(Value) = 1 Then '1=SeqNoChange
                If eStatus = Profile.CollectionStatus.NoChange Then
                    eStatus = Profile.CollectionStatus.SeqNoChange
                ElseIf eStatus = Profile.CollectionStatus.ChangeUnder Then
                    eStatus = Profile.CollectionStatus.Changed
                End If
            End If
            If CDbl(Value) = 2 Then '2=ChangeUnder
                If eStatus = Profile.CollectionStatus.NoChange Then
                    eStatus = Profile.CollectionStatus.ChangeUnder
                ElseIf eStatus = Profile.CollectionStatus.SeqNoChange Then
                    eStatus = Profile.CollectionStatus.Changed
                End If
            End If
            If Value > eStatus Then
                eStatus = Value
            End If
        End Set
    End Property
	Public Property AutoMatch() As Boolean
		Get
			AutoMatch = bAutoMatch
		End Get
		Set(ByVal Value As Boolean)
			bAutoMatch = Value
		End Set
	End Property
	Public Property MatchOCR() As Boolean
		Get
			MatchOCR = bMatchOCR
		End Get
		Set(ByVal Value As Boolean)
			bMatchOCR = Value
		End Set
	End Property
	Public Property ShowCorrections() As Short
		Get
			ShowCorrections = iShowCorrections
		End Get
		Set(ByVal Value As Short)
			iShowCorrections = Value
		End Set
	End Property
	Public Property PaymentType() As String
		Get
			PaymentType = sPaymentType
		End Get
		Set(ByVal Value As String)
			sPaymentType = Value
		End Set
	End Property
	Public Property Crypt() As Boolean
		Get
			Crypt = bCrypt
		End Get
		Set(ByVal Value As Boolean)
			bCrypt = Value
		End Set
	End Property
	Public Property SplitAfter450() As Boolean
		Get
			SplitAfter450 = bSplitAfter450
		End Get
		Set(ByVal Value As Boolean)
			bSplitAfter450 = Value
		End Set
	End Property
	Public Property FilenameOut4() As String
		Get
			FilenameOut4 = sFilenameOut4
		End Get
		Set(ByVal Value As String)
			sFilenameOut4 = Value
		End Set
	End Property
	Public Property FormatOutID1() As Short
		Get
			FormatOutID1 = iFormatOutID1
		End Get
		Set(ByVal Value As Short)
			iFormatOutID1 = Value
		End Set
	End Property
	Public Property FormatOutID2() As Short
		Get
			FormatOutID2 = iFormatOutID2
		End Get
		Set(ByVal Value As Short)
			iFormatOutID2 = Value
		End Set
	End Property
	Public Property FormatOutID3() As Short
		Get
			FormatOutID3 = iFormatOutID3
		End Get
		Set(ByVal Value As Short)
			iFormatOutID3 = Value
		End Set
	End Property
	Public Property FormatOutID4() As Short
		Get
			FormatOutID4 = iFormatOutID4
		End Get
		Set(ByVal Value As Short)
			iFormatOutID4 = Value
		End Set
	End Property
	Public Property MATCHOutGL() As Short
		Get
			MATCHOutGL = iMATCHOutGL
		End Get
		Set(ByVal Value As Short)
			iMATCHOutGL = Value
		End Set
	End Property
	Public Property MATCHOutOCR() As Short
		Get
			MATCHOutOCR = iMATCHOutOCR
		End Get
		Set(ByVal Value As Short)
			iMATCHOutOCR = Value
		End Set
	End Property
	Public Property MATCHOutAutogiro() As Short
		Get
			MATCHOutAutogiro = iMATCHOutAutogiro
		End Get
		Set(ByVal Value As Short)
			iMATCHOutAutogiro = Value
		End Set
	End Property
	Public Property MATCHOutRest() As Short
		Get
			MATCHOutRest = iMATCHOutRest
		End Get
		Set(ByVal Value As Short)
			iMATCHOutRest = Value
		End Set
	End Property
	Public Property DelDays() As Short
		Get
			DelDays = iDelDays
		End Get
		Set(ByVal Value As Short)
			iDelDays = Value
		End Set
	End Property
	Public Property ReportAllPayments() As Boolean
		Get
			ReportAllPayments = bReportAllPayments
		End Get
		Set(ByVal Value As Boolean)
			bReportAllPayments = Value
		End Set
	End Property
	Public Property MergePayments() As Short
		Get
			MergePayments = iMergePayments
		End Get
		Set(ByVal Value As Short)
			iMergePayments = Value
		End Set
	End Property
	Public Property NotificationMail() As Boolean
		Get
			NotificationMail = bNotificationMail
		End Get
		Set(ByVal Value As Boolean)
			bNotificationMail = Value
		End Set
	End Property
	Public Property NotificationPrint() As Boolean
		Get
			NotificationPrint = bNotificationPrint
		End Get
		Set(ByVal Value As Boolean)
			bNotificationPrint = Value
		End Set
	End Property
	Public Property ChargeMeDomestic() As Boolean
		Get
			ChargeMeDomestic = bChargeMeDomestic
		End Get
		Set(ByVal Value As Boolean)
			bChargeMeDomestic = Value
		End Set
	End Property
	Public Property ChargeMeAbroad() As Boolean
		Get
			ChargeMeAbroad = bChargeMeAbroad
		End Get
		Set(ByVal Value As Boolean)
			bChargeMeAbroad = Value
		End Set
	End Property
	Public Property ChargesFromSetup() As Boolean
		Get
			ChargesFromSetup = bChargesFromSetup
		End Get
		Set(ByVal Value As Boolean)
			bChargesFromSetup = Value
		End Set
	End Property
	Public Property CodeNB() As Integer
		Get
			CodeNB = lCodeNB
		End Get
		Set(ByVal Value As Integer)
			lCodeNB = Value
		End Set
	End Property
    Public Property CodeNBCurrencyOnly() As Boolean
        Get
            CodeNBCurrencyOnly = bCodeNBCurrencyOnly
        End Get
        Set(ByVal Value As Boolean)
            bCodeNBCurrencyOnly = Value
        End Set
    End Property
    Public Property ExplanationNB() As String
        Get
            ExplanationNB = sExplanationNB
        End Get
        Set(ByVal Value As String)
            sExplanationNB = Value
        End Set
    End Property
	Public Property StructText() As String
		Get
			StructText = sStructText
		End Get
		Set(ByVal Value As String)
			sStructText = Value
		End Set
	End Property
	Public Property StructTextNegative() As String
		Get
			StructTextNegative = sStructTextNegative
		End Get
		Set(ByVal Value As String)
			sStructTextNegative = Value
		End Set
	End Property
	Public Property MappingFileName() As String
		Get
			MappingFileName = sMappingFilename
		End Get
		Set(ByVal Value As String)
			sMappingFilename = Value
		End Set
	End Property
	Public Property StructInvoicePos() As Short
		Get
			StructInvoicePos = iStructInvoicePos
		End Get
		Set(ByVal Value As Short)
			iStructInvoicePos = Value
		End Set
	End Property
	Public Property AddDays() As Short
		Get
			AddDays = iAddDays
		End Get
		Set(ByVal Value As Short)
			iAddDays = Value
		End Set
	End Property
	Public Property StructInvoicePosNegative() As Short
		Get
			StructInvoicePosNegative = iStructInvoicePosNegative
		End Get
		Set(ByVal Value As Short)
			iStructInvoicePosNegative = Value
		End Set
	End Property
	Public Property SplitOCRandFBO() As Boolean
		Get
			SplitOCRandFBO = bSplitOCRandFBO
		End Get
		Set(ByVal Value As Boolean)
			bSplitOCRandFBO = Value
		End Set
	End Property
	Public Property ExtractInfofromKID() As Boolean
		Get
			ExtractInfofromKID = bExtractInfofromKID
		End Get
		Set(ByVal Value As Boolean)
			bExtractInfofromKID = Value
		End Set
	End Property
	Public Property BackupPath() As String
		Get
			BackupPath = sBackUpPath
		End Get
		Set(ByVal Value As String)
			sBackUpPath = Value
		End Set
	End Property
	Public Property Sorting() As Short
		Get
			Sorting = iSorting
		End Get
		Set(ByVal Value As Short)
			iSorting = Value
		End Set
	End Property
	Public Property Message() As String
		Get
			Message = sMessage
		End Get
		Set(ByVal Value As String)
			sMessage = Value
		End Set
	End Property
	Public Property Version() As String
		Get
			Version = sVersion
		End Get
		Set(ByVal Value As String)
			sVersion = Value
		End Set
	End Property
	Public Property EDIFormat() As Boolean
		Get
			EDIFormat = bEDIFormat
		End Get
		Set(ByVal Value As Boolean)
			bEDIFormat = Value
		End Set
	End Property
	Public Property Setup_Send() As Boolean
		Get
			Setup_Send = bSetup_Send
		End Get
		Set(ByVal Value As Boolean)
			bSetup_Send = Value
		End Set
	End Property
	Public Property Setup_Return() As Boolean
		Get
			Setup_Return = bSetup_Return
		End Get
		Set(ByVal Value As Boolean)
			bSetup_Return = Value
		End Set
	End Property
	Public Property Setup_NewSend() As Boolean
		Get
			Setup_NewSend = bSetup_NewSend
		End Get
		Set(ByVal Value As Boolean)
			bSetup_NewSend = Value
		End Set
	End Property
	Public Property Setup_NewReturn() As Boolean
		Get
			Setup_NewReturn = bSetup_NewReturn
		End Get
		Set(ByVal Value As Boolean)
			bSetup_NewReturn = Value
		End Set
	End Property
	Public Property UseSignalFile() As Boolean
		Get
			UseSignalFile = bUseSignalFile
		End Get
		Set(ByVal Value As Boolean)
			bUseSignalFile = Value
		End Set
	End Property
	Public Property ValidateFormatAtImport() As Boolean
		Get
			ValidateFormatAtImport = bValidateFormatAtImport
		End Get
		Set(ByVal Value As Boolean)
			bValidateFormatAtImport = Value
		End Set
	End Property
	Public Property FilenameSignalFile() As String
		Get
			FilenameSignalFile = sFilenameSignalFile
		End Get
		Set(ByVal Value As String)
			sFilenameSignalFile = Value
		End Set
	End Property
    Public Property FilenameSignalFileError() As String
        Get
            FilenameSignalFileError = sFilenameSignalFileError
        End Get
        Set(ByVal Value As String)
            sFilenameSignalFileError = Value
        End Set
    End Property
    ' XNET 13.12.2010 added sSpecialMarker
    Public Property SpecialMarker() As String
        Get
            SpecialMarker = sSpecialMarker
        End Get
        Set(ByVal Value As String)
            sSpecialMarker = Value
        End Set
    End Property
    'XNET 03.07.2012 added CurrencyToUse
    Public Property CurrencyToUse() As String
        Get
            SpecialMarker = sSpecialMarker
        End Get
        Set(ByVal Value As String)
            sSpecialMarker = Value
        End Set
    End Property
    'XNET 03.09.2012 added Division2
    Public Property Division2() As String
        Get
            Division2 = sDivision2
        End Get
        Set(ByVal Value As String)
            sDivision2 = Value
        End Set
    End Property
    'XNET 03.09.2012 added TelepayDebitNO
    Public Property TelepayDebitNO() As Boolean
        Get
            TelepayDebitNO = bTelepayDebitNO
        End Get
        Set(ByVal Value As Boolean)
            bTelepayDebitNO = Value
        End Set
    End Property
    Public Property StandardPurposeCode() As String
        Get
            StandardPurposeCode = sStandardPurposeCode
        End Get
        Set(ByVal Value As String)
            sStandardPurposeCode = Value
        End Set
    End Property
    Public Property SplitFiles() As Boolean
        Get
            SplitFiles = bSplitFiles
        End Get
        Set(ByVal Value As Boolean)
            bSplitFiles = Value
        End Set
    End Property
    Public Property ISO20022() As Boolean
        Get
            ISO20022 = bISO20022
        End Get
        Set(ByVal Value As Boolean)
            bISO20022 = Value
        End Set
    End Property
    
    Public Property secFileNamePublicKey() As String
        Get
            secFileNamePublicKey = sFileNamePublicKey
        End Get
        Set(ByVal Value As String)
            sFileNamePublicKey = Value
        End Set
    End Property
    Public Property secHash() As String
        Get
            secHash = sHash
        End Get
        Set(ByVal Value As String)
            sHash = Value
        End Set
    End Property
    Public Property secCypher() As String
        Get
            secCypher = sCypher
        End Get
        Set(ByVal Value As String)
            sCypher = Value
        End Set
    End Property

    Public Property secEncrypt() As Boolean
        Get
            secEncrypt = bEncrypt
        End Get
        Set(ByVal Value As Boolean)
            bEncrypt = Value
        End Set
    End Property
    Public Property secSign() As Boolean
        Get
            secSign = bSign
        End Get
        Set(ByVal Value As Boolean)
            bSign = Value
        End Set
    End Property
    Public Property secConfidential() As Boolean
        Get
            secConfidential = bConfidential
        End Get
        Set(ByVal Value As Boolean)
            bConfidential = Value
        End Set
    End Property
    Public Property secSecureEnvelope() As Boolean
        Get
            secSecureEnvelope = bSecureEnvelope
        End Get
        Set(ByVal Value As Boolean)
            bSecureEnvelope = Value
        End Set
    End Property
    Public Property secFileNameBankPublicKey() As String
        Get
            secFileNameBankPublicKey = sFileNameBankPublicKey
        End Get
        Set(ByVal Value As String)
            sFileNameBankPublicKey = Value
        End Set
    End Property
    Public Property secCompression() As String
        Get
            secCompression = sCompression
        End Get
        Set(ByVal Value As String)
            sCompression = Value
        End Set
    End Property
    Public Property secOurPublicKey() As String
        Get
            secOurPublicKey = sOurPublicKey
        End Get
        Set(ByVal Value As String)
            sOurPublicKey = Value
        End Set
    End Property
    Public Property secBANKPublicKey() As String
        Get
            secBANKPublicKey = sBANKPublicKey
        End Get
        Set(ByVal Value As String)
            sBANKPublicKey = Value
        End Set
    End Property
    Public Property secOurPrivateKey() As String
        Get
            secOurPrivateKey = sOurPrivateKey
        End Get
        Set(ByVal Value As String)
            sOurPrivateKey = Value
        End Set
    End Property
    Public Property secKeyCreationDate() As String
        Get
            secKeyCreationDate = sKeyCreationDate
        End Get
        Set(ByVal Value As String)
            sKeyCreationDate = Value
        End Set
    End Property
    Public Property secUserID() As String
        Get
            secUserID = sSecUserID
        End Get
        Set(ByVal Value As String)
            sSecUserID = Value
        End Set
    End Property
    Public Property secPassword() As String
        Get
            secPassword = sSecPassword
        End Get
        Set(ByVal Value As String)
            sSecPassword = Value
        End Set
    End Property
    Public Property FileEncoding() As BabelFiles.Encoding
        Get
            FileEncoding = eFileEncoding
        End Get
        Set(ByVal Value As BabelFiles.Encoding)
            eFileEncoding = Value
        End Set
    End Property


    '********* END PROPERTY SETTINGS ***********************

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		nFileSetup_ID = 0
		iFormat_ID = 0
		iEnum_ID = -1
		lBank_ID = 0
		lEnumBank_ID = 0
        sShortName = ""
        sDescription = ""
        bSilent = False
        bLog = False
        nLogType = 1 ' Windows log
        sLogFilename = "BabelBank.log"
        bLogOverwrite = False
        bLogShowStartStop = True
        nLogMaxLines = 500
        iLogDetails = 4
        iFilesetupOut = -1
        sFileNameIn1 = ""
        sFileNameIn2 = ""
        sFileNameIn3 = ""
        bBackupIn = False
        bManual = False
        bOverWriteIn = False
        bWarningIn = False
        sFileNameOut1 = ""
        sFileNameOut2 = ""
        sFileNameOut3 = ""
        bBackupOut = False
        bOverWriteOut = False
        bWarningOut = False
        bRemoveOldOut = False
        bNotSplitOnAccount = False
        eSeqnoType = 1
        nSeqnoTotal = 0
        nSeqNoDay = 0
        sDivision = ""
        sCompanyNo = ""
        sAdditionalNo = ""
        bFromAccountingSystem = False
        bInfoNewClient = False
        bKeepBatch = False
        bCtrlNegative = False
        bReturnMass = True
        bCtrlZip = False
        bCtrlAccount = False
        bCtrlKID = False
        bChangeDateDue = False
        bChangeDateAll = False
        bRejectWrong = False
        bRedoWrong = False

        bAccumulation = False
        bClientToScreen = False
        bClientToPrint = False
        bClientToFile = False
        bClientToMail = False
        bReportToPrint1 = False
        bReportToPrint2 = False
        bReportToPrint3 = False
        bReportToScreen1 = False
        bReportToScreen2 = False
        bReportToScreen3 = False
        eStatus = 0
        sMessage = ""
        sVersion = ""
        bEDIFormat = False
        bSetup_Send = False
        bSetup_Return = False
        bSetup_NewSend = False
        bSetup_NewReturn = False
        sPreJCL = ""
        sPostJCL = ""
        sTreatSpecial = ""
        bAutoMatch = False
        bMatchOCR = False
        iShowCorrections = 0
        sPaymentType = ""
        bCrypt = False
        bSplitAfter450 = False
        sFilenameOut4 = ""
        iFormatOutID1 = 0
        iFormatOutID2 = 0
        iFormatOutID3 = 0
        iFormatOutID4 = 0
        iMATCHOutGL = 0
        iMATCHOutOCR = 0
        iMATCHOutAutogiro = 0
        iMATCHOutRest = 0
        iDelDays = 90
        bReportAllPayments = False
        iMergePayments = 0
        bNotificationMail = False
        bNotificationPrint = False
        bChargeMeDomestic = True
        bChargeMeAbroad = False
        bChargesFromSetup = False
        lCodeNB = -1
        bCodeNBCurrencyOnly = False
        sExplanationNB = " "
        sStructText = ""
        iStructInvoicePos = 0
        sStructTextNegative = ""
        iStructInvoicePosNegative = 0
        bSplitOCRandFBO = False
        bExtractInfofromKID = False
        sBackUpPath = ""
        iAddDays = 0
        sMappingFilename = ""
        bUseSignalFile = False
        bValidateFormatAtImport = False
        sFilenameSignalFile = ""
        sFilenameSignalFileError = ""
        iSorting = 0
        ' XNET 13.12.2010 added sSpecialMarker
        sSpecialMarker = ""
        'XNET 03.07.2012 added CurrencyToUse
        sCurrencyToUse = vbNullString
        ' XNET 03.09.2012 added 2 fields;
        sDivision2 = ""
        bTelepayDebitNO = False
        ' XokNET 23.06.2014
        sStandardPurposeCode = ""
        bISO20022 = False
        bSplitFiles = False

        ' 05.01.2017 Security
        sFileNamePublicKey = ""
        sHash = ""
        sCypher = ""
        bEncrypt = False
        bSign = False
        bSecureEnvelope = False
        sFileNameBankPublicKey = ""
        sCompression = ""
        bConfidential = False

        sOurPublicKey = ""
        sOurPrivateKey = ""
        sKeyCreationDate = ""
        sSecUserID = ""
        sSecPassword = ""
        sBANKPublicKey = ""
        eFileEncoding = BabelFiles.Encoding.No_Specific_Encoding

        ' When vbbabel.dll is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'UPGRADE_NOTE: Object oClients may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		oClients = Nothing
		'UPGRADE_NOTE: Object oReports may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		oReports = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
    Public Function Load(ByRef iCompanyNo As Short, ByRef myBBDB_AccessFileSetupReader As System.Data.OleDb.OleDbDataReader, ByRef myBBDB_AccessConnection As System.Data.OleDb.OleDbConnection) As Boolean
        Dim sMySQL As String
        'Dim oClients As Clients
        Dim oClient As Client
        Dim bClient As Boolean
        Dim sName, sClient_ID As String
        ' New 15.03.03 JanP
        Dim oReport As Report
        Dim iReport_ID As Short = 0
        Dim sSelectPart As String
        Dim myBBDB_AccessClientReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessClientCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessReportsReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessReportsCommand As System.Data.OleDb.OleDbCommand = Nothing

        nFileSetup_ID = myBBDB_AccessFileSetupReader.GetInt32(0)
        iFormat_ID = myBBDB_AccessFileSetupReader.GetInt32(1)
        iEnum_ID = myBBDB_AccessFileSetupReader.GetInt16(2)
        lBank_ID = myBBDB_AccessFileSetupReader.GetInt16(3)

        If Not myBBDB_AccessFileSetupReader.IsDBNull(4) Then
            sShortName = Trim(myBBDB_AccessFileSetupReader.GetString(4))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(5) Then
            sDescription = myBBDB_AccessFileSetupReader.GetString(5)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(6) Then
            bSilent = Trim(myBBDB_AccessFileSetupReader.GetBoolean(6))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(7) Then
            bLog = myBBDB_AccessFileSetupReader.GetBoolean(7)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(8) Then
            nLogType = myBBDB_AccessFileSetupReader.GetByte(8)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(9) Then
            sLogFilename = Trim(myBBDB_AccessFileSetupReader.GetString(9))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(10) Then
            bLogOverwrite = Trim(myBBDB_AccessFileSetupReader.GetBoolean(10))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(11) Then
            bLogShowStartStop = myBBDB_AccessFileSetupReader.GetBoolean(11)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(12) Then
            nLogMaxLines = myBBDB_AccessFileSetupReader.GetInt32(12)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(13) Then
            iLogDetails = myBBDB_AccessFileSetupReader.GetInt32(13)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(14) Then
            iFilesetupOut = myBBDB_AccessFileSetupReader.GetInt16(14)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(15) Then
            sFileNameIn1 = Trim(myBBDB_AccessFileSetupReader.GetString(15))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(16) Then
            sFileNameIn2 = Trim(myBBDB_AccessFileSetupReader.GetString(16))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(17) Then
            sFileNameIn3 = Trim(myBBDB_AccessFileSetupReader.GetString(17))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(18) Then '20.12.2017 - Added IsDBNull-test
            bBackupIn = myBBDB_AccessFileSetupReader.GetBoolean(18)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(19) Then '20.12.2017 - Added IsDBNull-test
            bManual = myBBDB_AccessFileSetupReader.GetBoolean(19)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(20) Then '20.12.2017 - Added IsDBNull-test
            bOverWriteIn = myBBDB_AccessFileSetupReader.GetBoolean(20)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(21) Then '20.12.2017 - Added IsDBNull-test
            bWarningIn = myBBDB_AccessFileSetupReader.GetBoolean(21)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(22) Then
            sFileNameOut1 = Trim(myBBDB_AccessFileSetupReader.GetString(22))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(23) Then
            sFileNameOut2 = Trim(myBBDB_AccessFileSetupReader.GetString(23))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(24) Then
            sFileNameOut3 = Trim(myBBDB_AccessFileSetupReader.GetString(24))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(25) Then '20.12.2017 - Added IsDBNull-test
            bBackupOut = myBBDB_AccessFileSetupReader.GetBoolean(25)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(26) Then '20.12.2017 - Added IsDBNull-test
            bOverWriteOut = myBBDB_AccessFileSetupReader.GetBoolean(26)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(27) Then '20.12.2017 - Added IsDBNull-test
            bWarningOut = myBBDB_AccessFileSetupReader.GetBoolean(27)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(28) Then '20.12.2017 - Added IsDBNull-test
            bRemoveOldOut = myBBDB_AccessFileSetupReader.GetBoolean(28)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(29) Then '20.12.2017 - Added IsDBNull-test
            bNotSplitOnAccount = myBBDB_AccessFileSetupReader.GetBoolean(29)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(30) Then '20.12.2017 - Added IsDBNull-test
            eSeqnoType = myBBDB_AccessFileSetupReader.GetInt16(30)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(31) Then
            nSeqnoTotal = myBBDB_AccessFileSetupReader.GetInt32(31)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(32) Then
            nSeqNoDay = myBBDB_AccessFileSetupReader.GetInt32(32)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(33) Then
            sDivision = Trim(myBBDB_AccessFileSetupReader.GetString(33))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(34) Then
            sCompanyNo = myBBDB_AccessFileSetupReader.GetString(34)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(35) Then
            sAdditionalNo = myBBDB_AccessFileSetupReader.GetString(35)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(36) Then '20.12.2017 - Added IsDBNull-test
            bFromAccountingSystem = myBBDB_AccessFileSetupReader.GetBoolean(36)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(37) Then '20.12.2017 - Added IsDBNull-test
            bInfoNewClient = myBBDB_AccessFileSetupReader.GetBoolean(37)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(38) Then '20.12.2017 - Added IsDBNull-test
            bKeepBatch = myBBDB_AccessFileSetupReader.GetBoolean(38)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(39) Then '20.12.2017 - Added IsDBNull-test
            bCtrlNegative = myBBDB_AccessFileSetupReader.GetBoolean(39)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(40) Then '20.12.2017 - Added IsDBNull-test
            bCtrlZip = myBBDB_AccessFileSetupReader.GetBoolean(40)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(41) Then '20.12.2017 - Added IsDBNull-test
            bCtrlAccount = myBBDB_AccessFileSetupReader.GetBoolean(41)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(42) Then '20.12.2017 - Added IsDBNull-test
            bCtrlKID = myBBDB_AccessFileSetupReader.GetBoolean(42)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(43) Then '20.12.2017 - Added IsDBNull-test
            bChangeDateDue = myBBDB_AccessFileSetupReader.GetBoolean(43)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(44) Then '20.12.2017 - Added IsDBNull-test
            bChangeDateAll = myBBDB_AccessFileSetupReader.GetBoolean(44)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(45) Then '20.12.2017 - Added IsDBNull-test
            bRejectWrong = myBBDB_AccessFileSetupReader.GetBoolean(45)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(46) Then '20.12.2017 - Added IsDBNull-test
            bRedoWrong = myBBDB_AccessFileSetupReader.GetBoolean(46)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(47) Then '20.12.2017 - Added IsDBNull-test
            bAccumulation = myBBDB_AccessFileSetupReader.GetBoolean(47)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(48) Then '20.12.2017 - Added IsDBNull-test
            bClientToScreen = myBBDB_AccessFileSetupReader.GetBoolean(48)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(49) Then '20.12.2017 - Added IsDBNull-test
            bClientToPrint = myBBDB_AccessFileSetupReader.GetBoolean(49)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(50) Then '20.12.2017 - Added IsDBNull-test
            bClientToFile = myBBDB_AccessFileSetupReader.GetBoolean(50)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(51) Then '20.12.2017 - Added IsDBNull-test
            bClientToMail = myBBDB_AccessFileSetupReader.GetBoolean(51)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(52) Then '20.12.2017 - Added IsDBNull-test
            bReportToPrint1 = myBBDB_AccessFileSetupReader.GetBoolean(52)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(53) Then '20.12.2017 - Added IsDBNull-test
            bReportToPrint2 = myBBDB_AccessFileSetupReader.GetBoolean(53)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(54) Then '20.12.2017 - Added IsDBNull-test
            bReportToPrint3 = myBBDB_AccessFileSetupReader.GetBoolean(54)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(55) Then '20.12.2017 - Added IsDBNull-test
            bReportToScreen1 = myBBDB_AccessFileSetupReader.GetBoolean(55)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(56) Then '20.12.2017 - Added IsDBNull-test
            bReportToScreen2 = myBBDB_AccessFileSetupReader.GetBoolean(56)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(57) Then '20.12.2017 - Added IsDBNull-test
            bReportToScreen3 = myBBDB_AccessFileSetupReader.GetBoolean(57)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(58) Then
            sMessage = Trim(myBBDB_AccessFileSetupReader.GetString(58))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(59) Then
            sVersion = Trim(myBBDB_AccessFileSetupReader.GetString(59))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(60) Then '20.12.2017 - Added IsDBNull-test
            bEDIFormat = myBBDB_AccessFileSetupReader.GetBoolean(60)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(61) Then
            bReturnMass = myBBDB_AccessFileSetupReader.GetBoolean(61)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(62) Then
            sPreJCL = Replace(Trim(myBBDB_AccessFileSetupReader.GetString(62)), "@$@", "'")
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(63) Then
            sPostJCL = Replace(Trim(myBBDB_AccessFileSetupReader.GetString(63)), "@$@", "'")
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(64) Then
            sTreatSpecial = myBBDB_AccessFileSetupReader.GetString(64)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(65) Then '20.12.2017 - Added IsDBNull-test
            bAutoMatch = myBBDB_AccessFileSetupReader.GetBoolean(65)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(66) Then '20.12.2017 - Added IsDBNull-test
            bMatchOCR = myBBDB_AccessFileSetupReader.GetBoolean(66)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(67) Then
            iShowCorrections = myBBDB_AccessFileSetupReader.GetInt32(67)
        Else
            iShowCorrections = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(68) Then
            sPaymentType = myBBDB_AccessFileSetupReader.GetString(68)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(69) Then '20.12.2017 - Added IsDBNull-test
            bCrypt = myBBDB_AccessFileSetupReader.GetBoolean(69)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(70) Then '20.12.2017 - Added IsDBNull-test
            bSplitAfter450 = myBBDB_AccessFileSetupReader.GetBoolean(70)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(71) Then
            sFilenameOut4 = Trim(myBBDB_AccessFileSetupReader.GetString(71))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(72) Then
            iFormatOutID1 = myBBDB_AccessFileSetupReader.GetInt32(72)
        Else
            iFormatOutID1 = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(73) Then
            iFormatOutID2 = myBBDB_AccessFileSetupReader.GetInt32(73)
        Else
            iFormatOutID2 = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(74) Then
            iFormatOutID3 = myBBDB_AccessFileSetupReader.GetInt32(74)
        Else
            iFormatOutID3 = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(75) Then
            iFormatOutID4 = myBBDB_AccessFileSetupReader.GetInt32(75)
        Else
            iFormatOutID4 = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(76) Then
            iMATCHOutGL = myBBDB_AccessFileSetupReader.GetInt32(76)
        Else
            iMATCHOutGL = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(77) Then
            iMATCHOutOCR = myBBDB_AccessFileSetupReader.GetInt32(77)
        Else
            iMATCHOutOCR = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(78) Then
            iMATCHOutAutogiro = myBBDB_AccessFileSetupReader.GetInt32(78)
        Else
            iMATCHOutAutogiro = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(79) Then
            iMATCHOutRest = myBBDB_AccessFileSetupReader.GetInt32(79)
        Else
            iMATCHOutRest = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(80) Then
            iDelDays = myBBDB_AccessFileSetupReader.GetInt32(80)
        Else
            iDelDays = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(81) Then '20.12.2017 - Added IsDBNull-test
            bReportAllPayments = myBBDB_AccessFileSetupReader.GetBoolean(81)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(82) Then
            iMergePayments = myBBDB_AccessFileSetupReader.GetInt32(82)
        Else
            iMergePayments = 0
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(83) Then
            bNotificationMail = myBBDB_AccessFileSetupReader.GetBoolean(83)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(84) Then
            bNotificationPrint = myBBDB_AccessFileSetupReader.GetBoolean(84)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(85) Then
            bChargeMeDomestic = myBBDB_AccessFileSetupReader.GetBoolean(85)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(86) Then
            bChargeMeAbroad = myBBDB_AccessFileSetupReader.GetBoolean(86)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(87) Then
            bChargesFromSetup = myBBDB_AccessFileSetupReader.GetBoolean(87)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(88) Then '20.12.2017 - Added IsDBNull-test
            lCodeNB = myBBDB_AccessFileSetupReader.GetInt32(88)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(89) Then
            sExplanationNB = myBBDB_AccessFileSetupReader.GetString(89)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(90) Then
            sStructText = myBBDB_AccessFileSetupReader.GetString(90)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(91) Then
            iStructInvoicePos = myBBDB_AccessFileSetupReader.GetInt32(91)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(92) Then
            iAddDays = myBBDB_AccessFileSetupReader.GetInt32(92)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(93) Then
            sMappingFilename = myBBDB_AccessFileSetupReader.GetString(93)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(94) Then
            sStructTextNegative = myBBDB_AccessFileSetupReader.GetString(94)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(95) Then
            iStructInvoicePosNegative = myBBDB_AccessFileSetupReader.GetInt32(95)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(96) Then
            bSplitOCRandFBO = myBBDB_AccessFileSetupReader.GetBoolean(96)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(97) Then
            bExtractInfofromKID = myBBDB_AccessFileSetupReader.GetBoolean(97)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(98) Then
            sBackUpPath = Trim(myBBDB_AccessFileSetupReader.GetString(98))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(99) Then '20.12.2017 - Added IsDBNull-test
            bUseSignalFile = myBBDB_AccessFileSetupReader.GetBoolean(99)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(100) Then
            sFilenameSignalFile = Trim(myBBDB_AccessFileSetupReader.GetString(100))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(101) Then '20.12.2017 - Added IsDBNull-test
            bValidateFormatAtImport = Trim(myBBDB_AccessFileSetupReader.GetBoolean(101))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(102) Then
            iSorting = myBBDB_AccessFileSetupReader.GetInt32(102)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(103) Then
            sSpecialMarker = Trim(myBBDB_AccessFileSetupReader.GetString(103))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(104) Then
            lEnumBank_ID = CInt(myBBDB_AccessFileSetupReader.GetDouble(104))
        Else
            lEnumBank_ID = vbBabel.BabelFiles.Bank.No_Bank_Specified
        End If
        'XNET 03.07.2012 added CurrencyToUse
        If Not myBBDB_AccessFileSetupReader.IsDBNull(105) Then
            sCurrencyToUse = Trim(myBBDB_AccessFileSetupReader.GetString(105))
        End If
        'XNET 03.09.2012 added Division2
        If Not myBBDB_AccessFileSetupReader.IsDBNull(106) Then
            sDivision2 = Trim(myBBDB_AccessFileSetupReader.GetString(106))
        End If
        'XNET 03.09.2012 added bTelepayDebitNO
        bTelepayDebitNO = Trim(myBBDB_AccessFileSetupReader.GetBoolean(107))
        ' XokNET 23.06.2014
        If Not myBBDB_AccessFileSetupReader.IsDBNull(108) Then
            sStandardPurposeCode = Trim(myBBDB_AccessFileSetupReader.GetString(108))
        End If
        ' added 03.02.2016
        If Not myBBDB_AccessFileSetupReader.IsDBNull(109) Then
            bSplitFiles = Trim(myBBDB_AccessFileSetupReader.GetBoolean(109))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(110) Then
            bISO20022 = Trim(myBBDB_AccessFileSetupReader.GetBoolean(110))
        End If

        ' 05.01.2017 Security
        If Not myBBDB_AccessFileSetupReader.IsDBNull(111) Then
            sFileNamePublicKey = Trim(myBBDB_AccessFileSetupReader.GetString(111))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(112) Then
            sHash = Trim(myBBDB_AccessFileSetupReader.GetString(112))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(113) Then
            sCypher = Trim(myBBDB_AccessFileSetupReader.GetString(113))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(114) Then
            bEncrypt = Trim(myBBDB_AccessFileSetupReader.GetBoolean(114))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(115) Then
            bSign = Trim(myBBDB_AccessFileSetupReader.GetBoolean(115))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(116) Then
            bSecureEnvelope = Trim(myBBDB_AccessFileSetupReader.GetBoolean(116))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(117) Then
            sFileNameBankPublicKey = Trim(myBBDB_AccessFileSetupReader.GetString(117))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(118) Then
            sCompression = Trim(myBBDB_AccessFileSetupReader.GetString(118))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(119) Then
            sOurPublicKey = Trim(myBBDB_AccessFileSetupReader.GetString(119))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(120) Then
            sOurPrivateKey = Trim(myBBDB_AccessFileSetupReader.GetString(120))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(121) Then
            sKeyCreationDate = Trim(myBBDB_AccessFileSetupReader.GetString(121))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(122) Then
            sSecUserID = Trim(myBBDB_AccessFileSetupReader.GetString(122))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(123) Then
            sSecPassword = Trim(myBBDB_AccessFileSetupReader.GetString(123))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(124) Then
            sBANKPublicKey = Trim(myBBDB_AccessFileSetupReader.GetString(124))
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(125) Then
            eFileEncoding = myBBDB_AccessFileSetupReader.GetInt32(125)
        End If
        If Not myBBDB_AccessFileSetupReader.IsDBNull(126) Then
            bConfidential = Trim(myBBDB_AccessFileSetupReader.GetBoolean(126))
        End If
        '16.05.2022 - Added
        If Not myBBDB_AccessFileSetupReader.IsDBNull(127) Then
            sFilenameSignalFileError = Trim(myBBDB_AccessFileSetupReader.GetString(127))
        End If


        'We only need to add clientinfo on formats that we are receiving from the ERP-system
        'FIX: Is this true? Have put a comment on IF
        'If bFromAccountingSystem = True Then

        sSelectPart = ""
        sSelectPart = "SELECT ClientFormat_ID, CF.Client_ID, ClientNO, FormatIn, FormatOut, SeqnoTotal, OwnRefText, Outgoing, Name, City, "
        sSelectPart = sSelectPart & "CountryCode, CompNo, Division, Notification1, Notification2, Notification3, ImportKID1, ImportKID2, "
        sSelectPart = sSelectPart & "ImportKID3, ExportKID, NoMatchAccount, NoMatchAccountType, RoundingsAccount, RoundingsAccountType, "
        sSelectPart = sSelectPart & "ChargesAccount, ChargesAccountType, AkontoPattern, AkontoPatternType, MatchPattern, MatchPatternType, "
        sSelectPart = sSelectPart & "GLPattern, GLPatternType, VoucherNo, VoucherType, VoucherNo2, VoucherType2, HowToAddVoucherNo, "
        sSelectPart = sSelectPart & "AddVoucherNoBeforeOCR, AddCounterToVoucherNo, DBProfile_ID, KIDStart, KIDLength, KIDValue, "
        sSelectPart = sSelectPart & "BB_Year, BB_Period, "
        ' added 09.07.2015
        sSelectPart = sSelectPart & "HistoryDelDays, SEPASingle, EndToEndRefFromBabelBank, MsgID_Variable, ValidationLevel, SavePaymentData, UseStateBankText, "
        sSelectPart = sSelectPart & "AdjustForSchemaValidation, AdjustToBBANNorway, AdjustToBBANSweden, AdjustToBBANDenmark, UseDifferentInvoiceAndPaymentCurrency, "
        sSelectPart = sSelectPart & "ISOSplitOCR, ISOSplitOCRWithCredits, ISOSRedoFreetextToStruct, ISODoNotGroup, ISORemoveBICInSEPA, ISOUseTransferCurrency, SocialSecurityNo"

        sMySQL = sSelectPart & " FROM ClientFormat CF, Client C where CF.Company_ID = " & iCompanyNo.ToString & " And cf.Client_ID = c.Client_ID And (cf.FormatIn = " & nFileSetup_ID.ToString & " Or cf.FormatOut = " & nFileSetup_ID.ToString & ") ORDER BY c.Client_ID"

        '        If bFromAccountingSystem Then
        '
        '            sMySQL = "Select * from ClientFormat CF, Client C where CF.Company_ID=" & Str(iCompanyNo) _
        ''                & " and CF.FormatIn=" & Str(nFileSetup_ID) _
        ''                & " and CF.client_id = C.client_id"
        '                '& " and CF.FormatIn=" & Str(nFileName_ID) _
        ''            'Select * from Clientformat cf, client c where  cf.Company_ID=1 and cf.formatin=2 and cf.client_id = c.client_id
        '        Else
        '            sMySQL = "Select * from ClientFormat CF, Client C where CF.Company_ID=" & Str(iCompanyNo) _
        ''                & " and CF.FormatOut=" & Str(nFileSetup_ID) _
        ''                & " and CF.client_id = C.client_id"
        '        End If

        If myBBDB_AccessClientCommand Is Nothing Then
            myBBDB_AccessClientCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessClientCommand.CommandType = CommandType.Text
            myBBDB_AccessClientCommand.Connection = myBBDB_AccessConnection
        End If

        If Not myBBDB_AccessClientReader Is Nothing Then
            If Not myBBDB_AccessClientReader.IsClosed Then
                myBBDB_AccessClientReader.Close()
            End If
        End If

        myBBDB_AccessClientCommand.CommandText = sMySQL

        myBBDB_AccessClientReader = myBBDB_AccessClientCommand.ExecuteReader

        oClients = New Clients
        If myBBDB_AccessClientReader.HasRows Then
            Do While myBBDB_AccessClientReader.Read()
                'Make a Client object
                oClient = oClients.Add(myBBDB_AccessClientReader.GetInt32(1).ToString) 'CF.Client_ID
                bClient = oClient.Load(iCompanyNo, myBBDB_AccessClientReader, myBBDB_AccessConnection)
            Loop
        End If

        '------------------------
        ' Load from Report-table
        '------------------------

        sSelectPart = ""
        sSelectPart = "SELECT FileSetup_ID, ReportNo, Preview, Printer, AskForPrinter, EMail, ExportType, ExportFilename, BreakLevel, TotalLevel, "
        sSelectPart = sSelectPart & "DetailLevel, Heading, BitmapFilename, PrinterName, LongDate, ReportName, ReportSQL, IncludeOCR, "
        sSelectPart = sSelectPart & "ReportSort, ReportWhen, ReportOnDatabase, SaveBeforeReport, SpecialReport, ClientReport, "
        sSelectPart = sSelectPart & "eMailAddresses, ActivateSpecialSQL "

        sMySQL = sSelectPart & "FROM Reports where Company_ID = " & iCompanyNo.ToString & " And Filesetup_ID = " & nFileSetup_ID.ToString

        If myBBDB_AccessReportsCommand Is Nothing Then
            myBBDB_AccessReportsCommand = myBBDB_AccessConnection.CreateCommand()
            myBBDB_AccessReportsCommand.CommandType = CommandType.Text
            myBBDB_AccessReportsCommand.Connection = myBBDB_AccessConnection
        End If

        If Not myBBDB_AccessReportsReader Is Nothing Then
            If Not myBBDB_AccessReportsReader.IsClosed Then
                myBBDB_AccessReportsReader.Close()
            End If
        End If

        myBBDB_AccessReportsCommand.CommandText = sMySQL

        myBBDB_AccessReportsReader = myBBDB_AccessReportsCommand.ExecuteReader

        oReports = New Reports
        If myBBDB_AccessReportsReader.HasRows Then
            Do While myBBDB_AccessReportsReader.Read()
                If myBBDB_AccessReportsReader.GetInt32(0) = nFileSetup_ID Then 'FileSetup_ID
                    'Make a Report-object
                    oReport = oReports.Add(myBBDB_AccessReportsReader.GetInt32(1).ToString) 'CF.Client_ID
                    oReport.Load(iCompanyNo, myBBDB_AccessReportsReader)
                End If
            Loop
        End If

        If Not myBBDB_AccessClientReader Is Nothing Then
            If Not myBBDB_AccessClientReader.IsClosed Then
                myBBDB_AccessClientReader.Close()
            End If
            myBBDB_AccessClientReader = Nothing
        End If
        If Not myBBDB_AccessClientCommand Is Nothing Then
            myBBDB_AccessClientCommand.Dispose()
            myBBDB_AccessClientCommand = Nothing
        End If
        If Not myBBDB_AccessReportsReader Is Nothing Then
            If Not myBBDB_AccessReportsReader.IsClosed Then
                myBBDB_AccessReportsReader.Close()
            End If
            myBBDB_AccessReportsReader = Nothing
        End If
        If Not myBBDB_AccessReportsCommand Is Nothing Then
            myBBDB_AccessReportsCommand.Dispose()
            myBBDB_AccessReportsCommand = Nothing
        End If

    End Function
End Class
