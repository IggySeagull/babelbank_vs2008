<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDatabases
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lstDatabases As System.Windows.Forms.ListBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDatabases))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdOK = New System.Windows.Forms.Button
		Me.lstDatabases = New System.Windows.Forms.ListBox
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Velg database"
		Me.ClientSize = New System.Drawing.Size(241, 219)
		Me.Location = New System.Drawing.Point(4, 30)
		Me.ControlBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDatabases"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdCancel.Text = "&Avbryt"
		Me.cmdCancel.Size = New System.Drawing.Size(80, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(139, 181)
		Me.cmdCancel.TabIndex = 2
		Me.cmdCancel.TabStop = False
		Me.cmdCancel.Visible = False
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.cmdOK.Size = New System.Drawing.Size(80, 25)
		Me.cmdOK.Location = New System.Drawing.Point(56, 181)
		Me.cmdOK.TabIndex = 1
		Me.cmdOK.TabStop = False
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.Name = "cmdOK"
		Me.lstDatabases.Size = New System.Drawing.Size(199, 150)
		Me.lstDatabases.Location = New System.Drawing.Point(18, 20)
		Me.lstDatabases.TabIndex = 0
		Me.lstDatabases.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstDatabases.BackColor = System.Drawing.SystemColors.Window
		Me.lstDatabases.CausesValidation = True
		Me.lstDatabases.Enabled = True
		Me.lstDatabases.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstDatabases.IntegralHeight = True
		Me.lstDatabases.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstDatabases.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstDatabases.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstDatabases.Sorted = False
		Me.lstDatabases.TabStop = True
		Me.lstDatabases.Visible = True
		Me.lstDatabases.MultiColumn = False
		Me.lstDatabases.Name = "lstDatabases"
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(lstDatabases)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
