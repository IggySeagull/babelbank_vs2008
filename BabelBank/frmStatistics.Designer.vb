<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmStatistics
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtPW As System.Windows.Forms.TextBox
	Public WithEvents txtToDate As System.Windows.Forms.TextBox
	Public WithEvents txtFromDate As System.Windows.Forms.TextBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblPassword As System.Windows.Forms.Label
	Public WithEvents lblToDate As System.Windows.Forms.Label
	Public WithEvents lblFromDate As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtPW = New System.Windows.Forms.TextBox
        Me.txtToDate = New System.Windows.Forms.TextBox
        Me.txtFromDate = New System.Windows.Forms.TextBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblToDate = New System.Windows.Forms.Label
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtPW
        '
        Me.txtPW.AcceptsReturn = True
        Me.txtPW.BackColor = System.Drawing.SystemColors.Window
        Me.txtPW.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPW.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPW.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPW.Location = New System.Drawing.Point(320, 128)
        Me.txtPW.MaxLength = 0
        Me.txtPW.Name = "txtPW"
        Me.txtPW.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPW.Size = New System.Drawing.Size(161, 20)
        Me.txtPW.TabIndex = 2
        '
        'txtToDate
        '
        Me.txtToDate.AcceptsReturn = True
        Me.txtToDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtToDate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtToDate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtToDate.Location = New System.Drawing.Point(320, 96)
        Me.txtToDate.MaxLength = 8
        Me.txtToDate.Name = "txtToDate"
        Me.txtToDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtToDate.Size = New System.Drawing.Size(73, 20)
        Me.txtToDate.TabIndex = 1
        Me.txtToDate.Text = "YYYYMMDD"
        '
        'txtFromDate
        '
        Me.txtFromDate.AcceptsReturn = True
        Me.txtFromDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtFromDate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFromDate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFromDate.Location = New System.Drawing.Point(320, 64)
        Me.txtFromDate.MaxLength = 8
        Me.txtFromDate.Name = "txtFromDate"
        Me.txtFromDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFromDate.Size = New System.Drawing.Size(73, 20)
        Me.txtFromDate.TabIndex = 0
        Me.txtFromDate.Text = "YYYYMMDD"
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(320, 184)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "55001 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(408, 184)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 3
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.SystemColors.Control
        Me.lblPassword.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPassword.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPassword.Location = New System.Drawing.Point(200, 128)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPassword.Size = New System.Drawing.Size(113, 17)
        Me.lblPassword.TabIndex = 7
        Me.lblPassword.Text = "60165 - Password"
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblToDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToDate.Location = New System.Drawing.Point(200, 96)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToDate.Size = New System.Drawing.Size(113, 17)
        Me.lblToDate.TabIndex = 6
        Me.lblToDate.Text = "60162 - Todate"
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblFromDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFromDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFromDate.Location = New System.Drawing.Point(200, 64)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFromDate.Size = New System.Drawing.Size(113, 17)
        Me.lblFromDate.TabIndex = 5
        Me.lblFromDate.Text = "60161 - Fromdate"
        '
        'frmStatistics
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(498, 217)
        Me.Controls.Add(Me.txtPW)
        Me.Controls.Add(Me.txtToDate)
        Me.Controls.Add(Me.txtFromDate)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblToDate)
        Me.Controls.Add(Me.lblFromDate)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmStatistics"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60163 - Statistikk"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region 
End Class
