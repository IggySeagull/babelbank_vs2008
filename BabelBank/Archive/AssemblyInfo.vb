Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

<Assembly: AssemblyTitle("vbArchive")> 
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Visual Banking AS")>
<Assembly: AssemblyProduct("BabelBank")> 
<Assembly: AssemblyCopyright("Copyright � Visual Banking 2012-2021")> 
<Assembly: AssemblyTrademark("Visual Banking AS")> 
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Build Number
'	Revision

' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.6.0.72")> 



<Assembly: AssemblyFileVersionAttribute("2.6.0.72")> 
