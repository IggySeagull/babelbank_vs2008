Option Strict Off
Option Explicit On
'<System.Runtime.InteropServices.ProgId("Archive_NET.Archive")> Public Class Archive
Public Class Archive
    ' Johannes# 
    Private iDatabase As Short
    Private oMyArchiveDAL As vbBabel.DAL = Nothing
    Private oMyArchiveSelectDAL As vbBabel.DAL = Nothing
    Private Const MSAccess As Short = 0
    Private Const Oracle10 As Short = 1
    Private Const SQLServer As Short = 2
    Private Const OracleOdin As Short = 3
    'New code 11.04.2007 by Kjell - added the next variable and the use of it.
    Private bUpdateDone As Boolean
    Private sOwner As String
    Private sSpecial As String
    Private bBeginTransRun As Boolean

    Public Function CopyDataToArchiveDB(ByRef oBabelFiles As vbBabel.BabelFiles, Optional ByRef bOnlyOCR As Boolean = False, Optional ByRef bOnlyAutogiro As Boolean = False, Optional ByRef bOnlyNoStructured As Boolean = False, Optional ByVal sCalledFrom As String = "") As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim oProfile As vbBabel.Profile
        Dim bx As Boolean
        Dim sMySQL As String
        Dim bBabelFileRecordUpdated As Boolean
        Dim bBatchRecordUpdated As Boolean
        Dim bAddNewPayment As Boolean
        Dim bExportPayment As Boolean
        Dim sErrorString As String
        Dim sCompanyID As String
        Dim bRecordExist As Boolean = False

        Dim bAlreadyShown As Boolean = False

        On Error GoTo ERR_CopyDataToArchiveDB

        bBabelFileRecordUpdated = False
        bBatchRecordUpdated = False
        oMyArchiveDAL.TEST_UseCommitFromOutside = True
        oMyArchiveDAL.TEST_BeginTrans()
        bBeginTransRun = True


        For Each oBabelFile In oBabelFiles
            oProfile = oBabelFile.VB_Profile
            sCompanyID = Trim(Str(oProfile.Company_ID))
            bBabelFileRecordUpdated = False
            For Each oBatch In oBabelFile.Batches
                bBatchRecordUpdated = False
                For Each oPayment In oBatch.Payments
                    bExportPayment = False
                    bAddNewPayment = True
                    If bOnlyOCR Then
                        'If Not bAlreadyShown Then
                        '    MsgBox("Dette er oppdatering av OCR")
                        '    bAlreadyShown = True
                        'End If
                        If IsOCR(oPayment.PayCode) Then
                            bExportPayment = True
                        Else
                            bExportPayment = False
                        End If
                    ElseIf bOnlyAutogiro Then
                        'If Not bAlreadyShown Then
                        '    MsgBox("Dette er oppdatering av Autogiro.")
                        '    bAlreadyShown = True
                        'End If
                        If IsAutogiro(oPayment.PayCode) Then
                            bExportPayment = True
                        Else
                            bExportPayment = False
                        End If
                    ElseIf bOnlyNoStructured Then
                        'If Not bAlreadyShown Then
                        '    MsgBox("Dette er oppdatering av andre betalinger")
                        '    bAlreadyShown = True
                        'End If
                        If IsOCR(oPayment.PayCode) Then
                            bExportPayment = False
                        ElseIf IsAutogiro(oPayment.PayCode) Then
                            bExportPayment = False
                        Else
                            bExportPayment = True
                        End If
                    Else
                        bExportPayment = True
                    End If

                    If bExportPayment Then
                        'Check if payment already exists in the archive database
                        If Not RunTime() Then
                            sOwner = ""  ' test SLETT
                        End If
                        '02.05.2016 - Changed test because Appearence always returned Blank for SISMO
                        'sMySQL = "SELECT COUNT(*) AS Appearence FROM " & sOwner & "Payment WHERE Company_ID = " & sCompanyID
                        'sMySQL = sMySQL & " AND Babelfile_ID = " & oBabelFile.Index.ToString
                        'sMySQL = sMySQL & " AND Batch_ID = " & oBatch.Index.ToString
                        'sMySQL = sMySQL & " AND Payment_ID = " & oPayment.Index.ToString
                        sMySQL = "SELECT Babelfile_ID FROM " & sOwner & "Payment WHERE Company_ID = " & sCompanyID
                        sMySQL = sMySQL & " AND Babelfile_ID = " & oBabelFile.Index.ToString
                        sMySQL = sMySQL & " AND Batch_ID = " & oBatch.Index.ToString
                        sMySQL = sMySQL & " AND Payment_ID = " & oPayment.Index.ToString

                        bRecordExist = False
                        oMyArchiveSelectDAL.SQL = sMySQL
                        If oMyArchiveSelectDAL.Reader_Execute() Then
                            If oMyArchiveSelectDAL.Reader_HasRows Then
                                bRecordExist = True

                                '02.05.2016 - Changed test because Appearence always returned Blank for SISMO
                                'Do While oMyArchiveSelectDAL.Reader_ReadRecord
                                '    If EmptyString(oMyArchiveSelectDAL.Reader_GetString("Appearence")) Then
                                '        bRecordExist = False
                                '    ElseIf CDbl(oMyArchiveSelectDAL.Reader_GetString("Appearence")) > 0 Then
                                '        bRecordExist = True
                                '    Else
                                '        bRecordExist = False
                                '    End If
                                'Loop
                            Else
                                bRecordExist = False
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & oMyArchiveSelectDAL.ErrorMessage)
                        End If

                        If bRecordExist Then
                            'MsgBox("Betalingen ligger allerede inne.")
                            bBabelFileRecordUpdated = True
                            bBatchRecordUpdated = True
                            bAddNewPayment = False
                        Else
                            If Not bBabelFileRecordUpdated Then
                                'MsgBox("Skal sjekke om Filen allerede er oppdatert.")
                                'Check if babelfile already exists in the archive database
                                ' i.e. the OCR transactions are stored and thus the BabelFile is stored, and we want to save the rest
                                ' Not necessary to do the same check for Batch since all payments are either OCR or Not within a Batch

                                '02.05.2016 Changed the test below. Appearence always returned empty for SISMO
                                'sMySQL = "SELECT COUNT(*) AS Appearence FROM " & sOwner & "Babelfile WHERE Company_ID = " & sCompanyID
                                'sMySQL = sMySQL & " AND Babelfile_ID = " & oBabelFile.Index.ToString
                                sMySQL = "SELECT Babelfile_ID FROM " & sOwner & "Babelfile WHERE Company_ID = " & sCompanyID
                                sMySQL = sMySQL & " AND Babelfile_ID = " & oBabelFile.Index.ToString

                                oMyArchiveSelectDAL.SQL = sMySQL
                                If oMyArchiveSelectDAL.Reader_Execute() Then

                                    '02.05.2016 Changed the test below. Appearence always returned empty for SISMO
                                    If oMyArchiveSelectDAL.Reader_HasRows Then
                                        'MsgBox("Filen finnes fra f�r.")
                                        bBabelFileRecordUpdated = True
                                    Else
                                        bBabelFileRecordUpdated = False
                                        'MsgBox("Finner ikke filen fra f�r")
                                    End If
                                    Do While oMyArchiveSelectDAL.Reader_ReadRecord

                                        'MsgBox("Sql returnerer f�lgende verdi: " & oMyArchiveSelectDAL.Reader_GetString("Appearence"))

                                        ''09.03.2016 - Changed code below for SISMO the SQL probaly returned NULL, which will be translated to "" in the DAL.
                                        'If EmptyString(oMyArchiveSelectDAL.Reader_GetString("Appearence")) Then
                                        '    bBabelFileRecordUpdated = False
                                        '    MsgBox("Finner ikke filen fra f�r")
                                        'ElseIf CDbl(oMyArchiveSelectDAL.Reader_GetString("Appearence")) > 0 Then
                                        '    MsgBox("Filen finnes fra f�r.")
                                        '    bBabelFileRecordUpdated = True
                                        'End If

                                        'Old code
                                        'If CDbl(oMyArchiveSelectDAL.Reader_GetString("Appearence")) > 0 Then
                                        'bBabelFileRecordUpdated = True
                                        'End If
                                    Loop
                                Else
                                    Throw New Exception(LRSCommon(45002) & oMyArchiveSelectDAL.ErrorMessage)
                                End If
                            End If
                        End If
                    End If

                    If bExportPayment Then
                        If Not bBabelFileRecordUpdated Then

                            With oBabelFile

                                'MsgBox("Skal legge inn BabelFile.")

                                'Mangler ExportDate
                                sMySQL = "INSERT INTO " & sOwner & "Babelfile(Company_ID, Babelfile_ID, FilenameIn, File_ID, "
                                sMySQL = sMySQL & "Ident_Sender, Ident_Receiver, MON_TransferredAmount, MON_InvoiceAmount, "
                                sMySQL = sMySQL & "DATE_Production, ImportFormat, "
                                sMySQL = sMySQL & "EDI_MessageNo, "
                                sMySQL = sMySQL & "Bank "
                                'TEMPCODE SISMO
                                sMySQL = sMySQL & ", Exported"

                                sMySQL = sMySQL & ") "

                                sMySQL = sMySQL & "VALUES(" & sCompanyID & ", " & .Index.ToString
                                '29.01.2018 - Added right(.. below
                                sMySQL = sMySQL & ", '" & Right(.FilenameIn, 100) & "', '" & .File_id & "', '" & .IDENT_Sender
                                sMySQL = sMySQL & "', '" & .IDENT_Receiver & "', " & .MON_TransferredAmount.ToString
                                sMySQL = sMySQL & ", " & .MON_InvoiceAmount.ToString
                                sMySQL = sMySQL & ", '" & .DATE_Production & "', " & Trim(Str(.ImportFormat))
                                sMySQL = sMySQL & ", '" & .EDI_MessageNo
                                sMySQL = sMySQL & "', " & Trim(Str(.BankByCode))
                                'sMySQL = sMySQL & ", " & Str(.FileExportedMark)
                                'sMySQL = sMySQL & ", '" & .Special & "')"

                                'TEMPCODE SISMO - m� fjernes ffra SISMO DB fordi den ikke finnes i kolleksjonen
                                sMySQL = sMySQL & ", 1"

                                sMySQL = sMySQL & ")"
                            End With

                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                Else
                                    Err.Raise(22225, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If

                            bBabelFileRecordUpdated = True
                        End If
                        If Not bBatchRecordUpdated Then

                            With oBatch
                                sMySQL = "INSERT INTO " & sOwner & "Batch(Company_ID, Babelfile_ID, Batch_ID, "
                                sMySQL = sMySQL & "I_EnterpriseNo, "
                                sMySQL = sMySQL & "I_Branch, DATE_Production, REF_Own, REF_Bank, "
                                sMySQL = sMySQL & "MON_TransferredAmount, MON_InvoiceAmount) "

                                sMySQL = sMySQL & "VALUES(" & sCompanyID & ", " & oBabelFile.Index.ToString
                                sMySQL = sMySQL & ", " & .Index.ToString
                                sMySQL = sMySQL & ", '" & .I_EnterpriseNo
                                sMySQL = sMySQL & "', '" & .I_Branch & "', '" & .DATE_Production
                                sMySQL = sMySQL & "', '" & .REF_Own & "', '" & .REF_Bank
                                sMySQL = sMySQL & "', " & .MON_TransferredAmount.ToString & ", " & .MON_InvoiceAmount.ToString
                                sMySQL = sMySQL & ")"
                            End With

                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                Else
                                    Err.Raise(22225, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If

                            bBatchRecordUpdated = True
                        End If

                        'PAYMENT-object

                        If bAddNewPayment Then
                            With oPayment
                                sMySQL = "INSERT INTO " & sOwner & "Payment(Company_ID, Babelfile_ID, Batch_ID, Payment_ID, "
                                sMySQL = sMySQL & "REF_Own, REF_Bank1, REF_Bank2, "
                                sMySQL = sMySQL & "E_Account, E_Name, "
                                sMySQL = sMySQL & "E_Adr1, E_Adr2, E_Adr3, E_Zip, "
                                sMySQL = sMySQL & "E_City, E_CountryCode, "
                                sMySQL = sMySQL & "DATE_Payment, DATE_Value, "
                                sMySQL = sMySQL & "PayCode, PayType, "
                                sMySQL = sMySQL & "I_Account, I_Client, "
                                sMySQL = sMySQL & "I_Name, I_Adr1, I_Adr2, I_Adr3, "
                                sMySQL = sMySQL & "I_Zip, I_City, I_CountryCode, "
                                sMySQL = sMySQL & "MON_TransferredAmount, MON_TransferCurrency, "
                                sMySQL = sMySQL & "MON_LocalAmount, MON_LocalCurrency, MON_LocalExchRate, "
                                sMySQL = sMySQL & "MON_AccountAmount, MON_AccountCurrency, MON_AccountExchRate, "
                                sMySQL = sMySQL & "MON_InvoiceAmount, MON_InvoiceCurrency, "
                                sMySQL = sMySQL & "MON_EuroAmount, MON_EuroExchRate, "
                                sMySQL = sMySQL & "MON_OriginallyPaidAmount, MON_OriginallyPaidCurrency, "
                                sMySQL = sMySQL & "MON_ChargesAmount, MON_ChargesCurrency, MON_ChargeMeDomestic, MON_ChargeMeAbroad, "
                                sMySQL = sMySQL & "BANK_SWIFTCode, BANK_BranchNo, BANK_Name, "
                                sMySQL = sMySQL & "BANK_Adr1, BANK_Adr2, BANK_Adr3, "
                                sMySQL = sMySQL & "BANK_CountryCode, BANK_SWIFTCodeCorrBank, "
                                sMySQL = sMySQL & "FilenameOut_ID, Exported, "
                                sMySQL = sMySQL & "MATCH_Matched, Unique_PaymentID, "
                                sMySQL = sMySQL & "Text_E_Statement, Text_I_Statement, "
                                sMySQL = sMySQL & "Unique_ERPID, UserID, "
                                sMySQL = sMySQL & "VoucherNo, VoucherType, HowMatched, "
                                sMySQL = sMySQL & "ExtraD1, ExtraD2, ExtraI1"
                                sMySQL = sMySQL & ") "

                                sMySQL = sMySQL & "VALUES(" & sCompanyID & ", " & oBabelFile.Index.ToString
                                sMySQL = sMySQL & ", " & oBatch.Index.ToString & ", " & .Index.ToString
                                sMySQL = sMySQL & ", '" & ReplaceIllegalCharactersInString(.REF_Own) & "', '" & ReplaceIllegalCharactersInString(.REF_Bank1)
                                sMySQL = sMySQL & "', '" & ReplaceIllegalCharactersInString(.REF_Bank2)
                                sMySQL = sMySQL & "', '" & .E_Account
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.E_Name), 35)
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.E_Adr1), 35) & "', '" & Left(ReplaceIllegalCharactersInString(.E_Adr2), 35)
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.E_Adr3), 35) & "', '" & .E_Zip
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.E_City), 35) & "', '" & .E_CountryCode
                                sMySQL = sMySQL & "', '" & .DATE_Payment
                                sMySQL = sMySQL & "', '" & .DATE_Value
                                sMySQL = sMySQL & "', '" & .PayCode & "', '" & .PayType
                                sMySQL = sMySQL & "', '" & .I_Account
                                sMySQL = sMySQL & "', '" & .I_Client
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.I_Name), 35) & "', '" & Left(ReplaceIllegalCharactersInString(.I_Adr1), 35)
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.I_Adr2), 35) & "', '" & Left(ReplaceIllegalCharactersInString(.I_Adr3), 35)
                                sMySQL = sMySQL & "', '" & .I_Zip & "', '" & Left(ReplaceIllegalCharactersInString(.I_City), 35)
                                sMySQL = sMySQL & "', '" & .I_CountryCode
                                sMySQL = sMySQL & "', " & .MON_TransferredAmount.ToString & ", '" & .MON_TransferCurrency
                                sMySQL = sMySQL & "', " & .MON_LocalAmount.ToString & ", '" & .MON_LocalCurrency & "', " & .MON_LocalExchRate.ToString.Replace(",", ".")
                                sMySQL = sMySQL & ", " & .MON_AccountAmount.ToString & ", '" & .MON_AccountCurrency & "', " & .MON_AccountExchRate.ToString.Replace(",", ".")
                                sMySQL = sMySQL & ", " & .MON_InvoiceAmount.ToString & ", '" & .MON_InvoiceCurrency
                                sMySQL = sMySQL & "', " & .MON_EuroAmount.ToString & ", " & .MON_EuroExchRate.ToString.Replace(",", ".")
                                sMySQL = sMySQL & ", " & .MON_OriginallyPaidAmount.ToString & ", '" & .MON_OriginallyPaidCurrency
                                sMySQL = sMySQL & "', " & .MON_ChargesAmount.ToString & ", '" & .MON_ChargesCurrency
                                sMySQL = sMySQL & "', " & TransformBoolean(.MON_ChargeMeDomestic) & ", " & TransformBoolean(.MON_ChargeMeAbroad)
                                sMySQL = sMySQL & ", '" & .BANK_SWIFTCode
                                sMySQL = sMySQL & "', '" & .BANK_BranchNo & "', '" & ReplaceIllegalCharactersInString(.BANK_Name)
                                sMySQL = sMySQL & "', '" & ReplaceIllegalCharactersInString(.BANK_Adr1) & "', '" & ReplaceIllegalCharactersInString(.BANK_Adr2)
                                sMySQL = sMySQL & "', '" & ReplaceIllegalCharactersInString(.BANK_Adr3) & "', '" & .BANK_CountryCode
                                sMySQL = sMySQL & "', '" & .BANK_SWIFTCodeCorrBank
                                'New code 11.04.2007 by Kjell
                                'Added the if sentence and splitted the smysql-sentences code to ensure correct updating of the archive db
                                sMySQL = sMySQL & "', " & .VB_FilenameOut_ID.ToString

                                If bOnlyOCR Or bOnlyAutogiro Then
                                    sMySQL = sMySQL & ", " & TransformBoolean(True)
                                    ' removed next line 02.08.2010 at ODIN - 27.08.2010 - Changed back
                                    sMySQL = sMySQL & ", " & Trim(Str(vbBabel.BabelFiles.MatchStatus.Matched))
                                Else
                                    sMySQL = sMySQL & ", " & TransformBoolean(.Exported)
                                    ' removed next line 02.08.2010 at ODIN - 27.08.2010 - Changed back
                                    sMySQL = sMySQL & ", " & Trim(Str(.MATCH_Matched))
                                End If
                                ' 02.08.2010 - copied next line here (at ODIN) - 27.08.2010 - Changed back
                                'sMySQL = sMySQL & ", " & Trim$(Str(.MATCH_Matched))
                                sMySQL = sMySQL & ", " & Trim(Str(.Unique_PaymentID))
                                'Old code
                                'sMySQL = sMySQL & "', " & Trim$(Str(.VB_FilenameOut_ID)) & ", " & TransformBoolean(.Exported)
                                'sMySQL = sMySQL & ", " & Trim$(Str(.MATCH_Matched)) & ", " & Trim$(Str(.Unique_PaymentID))

                                sMySQL = sMySQL & ", '" & ReplaceIllegalCharactersInString(.Text_E_Statement) & "', '" & ReplaceIllegalCharactersInString(.Text_I_Statement)
                                sMySQL = sMySQL & "', '" & .Unique_ERPID & "', '" & ReplaceIllegalCharactersInString(.UserID)
                                sMySQL = sMySQL & "', '" & .VoucherNo & "', '" & .VoucherType
                                sMySQL = sMySQL & "', '" & Right(Trim(ReplaceIllegalCharactersInString(.MATCH_HowMatched)), 250)
                                sMySQL = sMySQL & "', '" & .ExtraD1 & "', '" & .ExtraD2 & "', '" & .ExtraI1
                                sMySQL = sMySQL & "')"
                                'sMySQL = sMySQL & "', '" & .ExtraI1 & "')"
                            End With

                            'sMySQL = "INSERT INTO Payment(Company_ID, Babelfile_ID, Batch_ID, Payment_ID, REF_Own, REF_Bank1, REF_Bank2, E_Account, E_Name, E_Adr1, E_Adr2, E_Adr3, E_Zip, E_City, E_CountryCode, DATE_Payment, DATE_Value, PayCode, PayType, I_Account, I_Client, I_Name, I_Adr1, I_Adr2, I_Adr3, I_Zip, I_City, I_CountryCode, MON_TransferredAmount, MON_TransferCurrency, MON_LocalAmount, MON_LocalCurrency, MON_LocalExchRate, MON_AccountAmount, MON_AccountCurrency, MON_AccountExchRate, MON_InvoiceAmount, MON_InvoiceCurrency, MON_EuroAmount, MON_EuroExchRate, MON_OriginallyPaidAmount, MON_OriginallyPaidCurrency, MON_ChargesAmount, MON_ChargesCurrency, MON_ChargeMeDomestic, MON_ChargeMeAbroad, BANK_SWIFTCode, BANK_BranchNo, BANK_Name, BANK_Adr1, BANK_Adr2, BANK_Adr3, BANK_CountryCode, BANK_SWIFTCodeCorrBank, FilenameOut_ID, Exported, MATCH_Matched, Unique_PaymentID, Text_E_Statement, Text_I_Statement, Unique_ERPID, UserID, VoucherNo, VoucherType, HowMatched, ExtraD1, ExtraD2, ExtraI1) "
                            'sMySQL = sMySQL & "VALUES(1, 8, 1, 1, '', '362539343', '62761023921', '3637165983 ', 'Kjell Arve Samn', '', '', '', '', '', '', '20061003', '20061003', '510', 'D', '76940512057', '', '', '', '', '', '', '', '', 160000, 'NOK', 0, 'NOK', 0, 160000, 'NOK', 0, 160000, 'NOK', 0, 0, 160000, 'NOK', 0, '', True, False, '', '', '', '', '', '', '', '', 1, 1, 0, -1, '', '', '', '', '', '', '', '', '', '')"
                            'sMySQL = "INSERT INTO Payment(Company_ID, Babelfile_ID, Batch_ID, Payment_ID, REF_Own, REF_Bank1, REF_Bank2, E_Account, E_Name, E_Adr1, E_Adr2, E_Adr3, E_Zip, E_City, E_CountryCode, DATE_Payment, DATE_Value, PayCode, PayType, I_Account, I_Client, I_Name, I_Adr1, I_Adr2, I_Adr3, I_Zip, I_City, I_CountryCode, MON_TransferredAmount, MON_TransferCurrency, MON_LocalAmount, MON_LocalCurrency, MON_LocalExchRate, MON_AccountAmount, MON_AccountCurrency, MON_AccountExchRate, MON_InvoiceAmount, MON_InvoiceCurrency, MON_EuroAmount, MON_EuroExchRate, MON_OriginallyPaidAmount, MON_OriginallyPaidCurrency, MON_ChargesAmount, MON_ChargesCurrency, MON_ChargeMeDomestic, MON_ChargeMeAbroad, BANK_SWIFTCode, BANK_BranchNo, BANK_Name, BANK_Adr1, BANK_Adr2, BANK_Adr3, BANK_CountryCode, BANK_SWIFTCodeCorrBank, FilenameOut_ID, Exported) " ', MATCH_Matched, Unique_PaymentID, Text_E_Statement, Text_I_Statement, Unique_ERPID, UserID, VoucherNo, VoucherType, HowMatched, ExtraD1, ExtraD2, ExtraI1) "
                            'sMySQL = sMySQL & "VALUES(1, 8, 1, 1, '', '362539343', '62761023921', '3637165983 ', 'Kjell Arve Samn', '', '', '', '', '', '', '20061003', '20061003', '510', 'D', '76940512057', '', '', '', '', '', '', '', '', 160000, 'NOK', 0, 'NOK', 0, 160000, 'NOK', 0, 160000, 'NOK', 0, 0, 160000, 'NOK', 0, '', True, False, '', '', '', '', '', '', '', '', 1, 1)" '0, -1, '', '', '', '', '', '', '', '', '', '')"

                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                Else
                                    Err.Raise(22225, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If

                        Else

                            With oPayment
                                'The payment already exists in the archiveDB, just update necessary info
                                'Update only values which can be changed during matching
                                sMySQL = "UPDATE " & sOwner & "Payment SET Match_Matched = " & Trim(Str(.MATCH_Matched))
                                'sMySQL = sMySQL & ", MATCH_MatchingIDAgainstObservationAccount = '" & Trim$(.MATCH_MatchingIDAgainstObservationAccount)
                                sMySQL = sMySQL & ", MON_LocalExchRate = " & .MON_LocalExchRate.ToString.Replace(",", ".")
                                'sMySQL = sMySQL & ", MON_LocalExchRateSet = " & Str(.MON_LocalExchRateSet)
                                sMySQL = sMySQL & ", E_Name = '" & Left(ReplaceIllegalCharactersInString(.E_Name), 35)
                                sMySQL = sMySQL & "', ExtraD1 = '" & ReplaceIllegalCharactersInString(.ExtraD1)
                                'sMySQL = sMySQL & "', ExtraD3 = '" & ReplaceIllegalCharactersInString(.ExtraD3)
                                'New code 11.04.2007 by Kjell
                                'Added this code to ensure correct updating of the archive db
                                sMySQL = sMySQL & "', Exported = " & TransformBoolean(.Exported)
                                sMySQL = sMySQL & ", HowMatched = '" & Replace(Right(Trim(ReplaceIllegalCharactersInString(.MATCH_HowMatched)), 250), vbCrLf, "")
                                'sMySQL = sMySQL & "', StatusCode = '" & .StatusCode & "' WHERE Company_ID = " & sCompanyID
                                sMySQL = sMySQL & "' WHERE Company_ID = " & sCompanyID
                                sMySQL = sMySQL & " AND Babelfile_ID = " & oBabelFile.Index.ToString
                                sMySQL = sMySQL & " AND Batch_ID = " & oBatch.Index.ToString
                                sMySQL = sMySQL & " AND Payment_ID = " & .Index.ToString
                            End With
                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                Else
                                    Err.Raise(22225, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If

                            'Delete the data in the Freetext-table and the Invoice-table for this payment
                            sMySQL = "DELETE FROM " & sOwner & TransformTableName("Freetext") & " WHERE Company_ID = "
                            sMySQL = sMySQL & oProfile.Company_ID.ToString & " AND BabelFile_ID = "
                            sMySQL = sMySQL & oBabelFile.Index.ToString & " AND Batch_ID = "
                            sMySQL = sMySQL & oBatch.Index.ToString & " AND Payment_ID = "
                            sMySQL = sMySQL & oPayment.Index.ToString
                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If
                            sMySQL = "DELETE FROM " & sOwner & "Invoice WHERE Company_ID = "
                            sMySQL = sMySQL & oProfile.Company_ID.ToString & " AND BabelFile_ID = "
                            sMySQL = sMySQL & oBabelFile.Index.ToString & " AND Batch_ID = "
                            sMySQL = sMySQL & oBatch.Index.ToString & " AND Payment_ID = "
                            sMySQL = sMySQL & oPayment.Index.ToString
                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If

                        End If 'If bAddNewPayment

                        For Each oInvoice In oPayment.Invoices

                            'INVOICE-object

                            With oInvoice
                                sMySQL = "INSERT INTO " & sOwner & "Invoice(Company_ID, Babelfile_ID, Batch_ID, Payment_ID, Invoice_ID, "
                                sMySQL = sMySQL & "MON_InvoiceAmount, MON_TransferredAmount, MON_AccountAmount, MON_LocalAmount, "
                                sMySQL = sMySQL & "REF_Own, REF_Bank, "
                                sMySQL = sMySQL & "InvoiceNo, CustomerNo, Unique_Id, STATEBANK_Code, "
                                sMySQL = sMySQL & "STATEBANK_Text, STATEBANK_DATE, MATCH_Original, "
                                sMySQL = sMySQL & "MATCH_Final, MATCH_Matched, MyField, "
                                sMySQL = sMySQL & "MATCH_Type, MATCH_ID, MATCH_PartlyPaid, "
                                sMySQL = sMySQL & "MATCH_ClientNo, Extra1) "


                                sMySQL = sMySQL & "VALUES(" & sCompanyID & ", " & oBabelFile.Index.ToString
                                sMySQL = sMySQL & ", " & oBatch.Index.ToString & ", " & oPayment.Index.ToString & ", " & .Index.ToString
                                sMySQL = sMySQL & ", " & .MON_InvoiceAmount.ToString & ", " & .MON_TransferredAmount.ToString
                                sMySQL = sMySQL & ", " & .MON_AccountAmount.ToString & ", " & .MON_LocalAmount.ToString
                                sMySQL = sMySQL & ", '" & ReplaceIllegalCharactersInString(.REF_Own) & "', '" & ReplaceIllegalCharactersInString(.REF_Bank)
                                sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(.InvoiceNo), 30) & "', '" & ReplaceIllegalCharactersInString(.CustomerNo)
                                sMySQL = sMySQL & "', '" & .Unique_Id & "', '" & .STATEBANK_Code
                                sMySQL = sMySQL & "', '" & .STATEBANK_Text & "', '" & .STATEBANK_DATE
                                sMySQL = sMySQL & "', " & TransformBoolean(.MATCH_Original)

                                'New code 11.04.2007 by Kjell
                                'Added the if sentence and splitted the smysql-sentence code to ensure correct updating of the archive db
                                sMySQL = sMySQL & ", " & TransformBoolean(.MATCH_Final)
                                If bOnlyOCR Or bOnlyAutogiro Then
                                    sMySQL = sMySQL & ", " & TransformBoolean(True)
                                Else
                                    sMySQL = sMySQL & ", " & TransformBoolean(.MATCH_Matched)
                                End If
                                'Old code
                                'sMySQL = sMySQL & ", " & TransformBoolean(.MATCH_Final) & ", " & TransformBoolean(.MATCH_Matched)
                                sMySQL = sMySQL & ", '" & .MyField & "', " & Trim(Str(.MATCH_MatchType))
                                sMySQL = sMySQL & ", '" & .MATCH_ID & "', " & TransformBoolean(.MATCH_PartlyPaid) & ", '" & .MATCH_ClientNo & "', '" & .Extra1 & "'"
                                sMySQL = sMySQL & ")"
                            End With

                            oMyArchiveDAL.SQL = sMySQL
                            If oMyArchiveDAL.ExecuteNonQuery Then
                                If oMyArchiveDAL.RecordsAffected > 0 Then
                                    bUpdateDone = True
                                Else
                                    Err.Raise(22225, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                End If
                            Else
                                Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                            End If

                            For Each oFreeText In oInvoice.Freetexts

                                With oFreeText
                                    'Freetext is a reserved word in SQL Server
                                    sMySQL = "INSERT INTO " & sOwner & TransformTableName("Freetext") & "(Company_ID, Babelfile_ID, Batch_ID, Payment_ID, "
                                    sMySQL = sMySQL & "Invoice_ID, Freetext_ID, Qualifier, XText) "

                                    sMySQL = sMySQL & "VALUES(" & sCompanyID & ", " & Trim(Str(oBabelFile.Index))
                                    sMySQL = sMySQL & ", " & Trim(Str(oBatch.Index)) & ", " & Trim(Str(oPayment.Index))
                                    sMySQL = sMySQL & ", " & Trim(Str(oInvoice.Index)) & ", " & Trim(Str(.Index))
                                    sMySQL = sMySQL & ", " & Trim(Str(.Qualifier))
                                    sMySQL = sMySQL & ", '" & ReplaceIllegalCharactersInString(.Text)
                                    sMySQL = sMySQL & "')"
                                End With

                                oMyArchiveDAL.SQL = sMySQL
                                If oMyArchiveDAL.ExecuteNonQuery Then
                                    If oMyArchiveDAL.RecordsAffected > 0 Then
                                        bUpdateDone = True
                                    Else
                                        Err.Raise(22225, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                    End If
                                Else
                                    Err.Raise(22224, "CopyDataToArchiveDB", oMyArchiveDAL.ErrorMessage & vbCrLf & vbCrLf & "SQL: " & sMySQL)
                                End If

                            Next oFreeText

                        Next oInvoice
                    End If 'if bexportpayment then
                Next oPayment
                bBatchRecordUpdated = False
            Next oBatch
            bBabelFileRecordUpdated = False
        Next oBabelFile

        If sSpecial = "NORDEA LIV" Then
            bx = UpdateBalancesForNordea(oBabelFiles)
        End If

        CopyDataToArchiveDB = True

        On Error GoTo 0
        Exit Function

ERR_CopyDataToArchiveDB:

        CopyDataToArchiveDB = False

        RollbackCopiedDataToArchiveDB()

        If Not oMyArchiveDAL Is Nothing Then
            oMyArchiveDAL.Close()
            oMyArchiveDAL = Nothing
        End If

        If Not oMyArchiveSelectDAL Is Nothing Then
            oMyArchiveSelectDAL.Close()
            oMyArchiveSelectDAL = Nothing
        End If

        '09.03.2016 - KI - Added next IF
        If Not EmptyString(sMySQL) Then
            Err.Raise(22224, "CopyDataToArchiveDB", Err.Description & vbCrLf & "SQL: " & sMySQL)
        Else
            Err.Raise(22224, "CopyDataToArchiveDB", Err.Description)
        End If

    End Function


    Public Function CommitCopiedDataToArchiveDB(Optional ByVal bCloseConnection As Boolean = True) As Boolean
        '28.09.2017 - HURRA!!! Added bCloseConnection as parameter

        If bBeginTransRun Then
            oMyArchiveDAL.TEST_CommitTrans()
        End If

        'MsgBox("Commiter dataene.")

        bBeginTransRun = False
        bUpdateDone = False

        If bCloseConnection Then
            If Not oMyArchiveDAL Is Nothing Then
                oMyArchiveDAL.Close()
                oMyArchiveDAL = Nothing
            End If
            If Not oMyArchiveSelectDAL Is Nothing Then
                oMyArchiveSelectDAL.Close()
                oMyArchiveSelectDAL = Nothing
            End If
        End If

    End Function
    Public Function RollbackCopiedDataToArchiveDB() As Boolean

        If bBeginTransRun Then
            oMyArchiveDAL.TEST_Rollback()
        End If

        bBeginTransRun = False
        bUpdateDone = False

        If Not oMyArchiveDAL Is Nothing Then
            oMyArchiveDAL.Close()
            oMyArchiveDAL = Nothing
        End If
        If Not oMyArchiveSelectDAL Is Nothing Then
            oMyArchiveSelectDAL.Close()
            oMyArchiveSelectDAL = Nothing
        End If

    End Function
    Public WriteOnly Property ConnectionString() As String
        Set(ByVal Value As String)
            'sConnectionString = Value
        End Set
    End Property
    Public WriteOnly Property TableOwner() As String
        Set(ByVal Value As String)
            sOwner = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public Function InitiateAndConnect(ByRef sConnectionString As String, ByRef iDatabaseType As Short) As Boolean

        'Set databasetype
        iDatabase = iDatabaseType

        'Connect to Archive-database
        oMyArchiveDAL = New vbBabel.DAL
        oMyArchiveDAL.TypeOfDB = vbBabel.DAL.DatabaseType.ArchiveDB
        oMyArchiveDAL.Company_ID = 1 'TODO: Hardcoded Company_ID
        If Not oMyArchiveDAL.ConnectToDB() Then
            Throw New System.Exception(oMyArchiveDAL.ErrorMessage)
        End If
        oMyArchiveDAL.TEST_UseCommitFromOutside = True

        'Connection used for SELECT-statements
        oMyArchiveSelectDAL = New vbBabel.DAL
        oMyArchiveSelectDAL.TypeOfDB = vbBabel.DAL.DatabaseType.ArchiveDB
        oMyArchiveSelectDAL.Company_ID = 1 'TODO: Hardcoded Company_ID
        oMyArchiveSelectDAL.TEST_UseCommitFromOutside = True
        If Not oMyArchiveSelectDAL.ConnectToDB() Then
            Throw New System.Exception(oMyArchiveSelectDAL.ErrorMessage)
        End If

    End Function
    Public Function RetrievelastUsedBabelFile_ID(ByRef sCompanyID As String) As Integer
        Dim lReturnValue As Integer
        Dim sMySQL As String

        'BABELREINDEX
        sMySQL = "SELECT MAX(BabelFile_ID) AS BabelFile_ID FROM " & sOwner & "BabelFile WHERE Company_ID = " & sCompanyID
        oMyArchiveSelectDAL.SQL = sMySQL
        If oMyArchiveSelectDAL.Reader_Execute() Then
            If oMyArchiveSelectDAL.Reader_HasRows Then
                Do While oMyArchiveSelectDAL.Reader_ReadRecord
                    lReturnValue = CLng(oMyArchiveSelectDAL.Reader_GetString("BabelFile_ID"))
                Loop
            Else
                lReturnValue = 0
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyArchiveSelectDAL.ErrorMessage)
        End If

        RetrievelastUsedBabelFile_ID = lReturnValue

    End Function


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        bUpdateDone = False
        bBeginTransRun = False
        sOwner = ""
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()

        If Not oMyArchiveDAL Is Nothing Then
            'oMyArchiveDAL.Close()
            oMyArchiveDAL = Nothing
        End If
        If Not oMyArchiveSelectDAL Is Nothing Then
            'oMyArchiveSelectDAL.Close()
            oMyArchiveSelectDAL = Nothing
        End If
        iDatabase = -1

    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
    Private Function TransformBoolean(ByRef bValue As Boolean) As String

        Select Case iDatabase

            Case MSAccess
                If bValue = True Then
                    TransformBoolean = "True"
                Else
                    TransformBoolean = "False"
                End If
            Case Oracle10
                If bValue = True Then
                    TransformBoolean = "-1"
                Else
                    TransformBoolean = "0"
                End If
            Case OracleOdin
                If bValue = True Then
                    TransformBoolean = "1"
                Else
                    TransformBoolean = "0"
                End If

            Case SQLServer
                If bValue = True Then
                    TransformBoolean = "-1"
                Else
                    TransformBoolean = "0"
                End If
            Case Else
                If bValue = True Then
                    TransformBoolean = "True"
                Else
                    TransformBoolean = "False"
                End If

        End Select

    End Function
    Private Function TransformTableName(ByRef sTableName As String) As String

        Select Case UCase(sTableName)

            Case "FREETEXT"

                Select Case iDatabase

                    Case MSAccess
                        TransformTableName = sTableName
                    Case Oracle10
                        TransformTableName = sTableName
                    Case SQLServer
                        TransformTableName = "Ftext"
                    Case Else
                        TransformTableName = sTableName

                End Select

            Case Else
                TransformTableName = sTableName


        End Select

    End Function
    Private Function UpdateBalancesForNordea(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim sFirstDate As String
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim bReturnValue As Boolean

        On Error GoTo ErrUpdateBalancesForNordea

        bReturnValue = True

        sFirstDate = "29990101"

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.DATE_Payment < sFirstDate Then
                        sFirstDate = oPayment.DATE_Payment
                    End If
                Next oPayment
            Next oBatch
        Next oBabelFile

        If sFirstDate < "29990101" Then
            bReturnValue = NordeaLivUpdateBalance(sFirstDate, "1")
        End If

        UpdateBalancesForNordea = bReturnValue

        On Error GoTo 0
        Exit Function

ErrUpdateBalancesForNordea:

        UpdateBalancesForNordea = False

        RollbackCopiedDataToArchiveDB()

        Err.Raise(22224, "UpdateBalancesForNordea", Err.Description)


    End Function

    Public Function NordeaLivUpdateBalance(ByRef sStartDate As String, ByRef sCompanyID As String, Optional ByRef sErrorstringToReturn As String = "", Optional ByRef bCalledFromOutside As Boolean = False) As Boolean
        'sStartDate is defined as the first date You want to Calculate a new balance

        Dim sIBDate As String = ""
        Dim nIBSaldo, nAmount As Double
        Dim dStartDate As Date
        Dim sEndDate As String
        Dim dEndDate As Date
        Dim sMySQL As String
        Dim dActiveDate As Date
        Dim sErrorString As String

        On Error GoTo ErrNordeaLivUpdateBalance

        sErrorString = ""

        'Set the last date that exists in the DB
        sMySQL = "SELECT MAX(DATE_Payment) AS EndDate FROM " & sOwner & "Payment WHERE Company_ID = " & sCompanyID

        oMyArchiveSelectDAL.SQL = sMySQL
        If oMyArchiveSelectDAL.Reader_Execute() Then
            If oMyArchiveSelectDAL.Reader_HasRows Then
                Do While oMyArchiveSelectDAL.Reader_ReadRecord
                    sEndDate = oMyArchiveSelectDAL.Reader_GetString("EndDate")
                Loop
            Else
                sEndDate = sIBDate
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyArchiveSelectDAL.ErrorMessage)
        End If

        dStartDate = StringToDate(sStartDate)
        dEndDate = StringToDate(sEndDate)

        'Retrieve the UB from the day before the start date
        'Beware that the day before may not exist (f.ex. weekend)
        sMySQL = "SELECT UB, Date FROM " & sOwner & "Saldo WHERE Date = (SELECT MAX(Date) FROM BankAvstKoll.Saldo WHERE Date < '" & DateToString(System.DateTime.FromOADate(dStartDate.ToOADate - 1)) & "' AND Company_ID =  " & sCompanyID & ") AND Company_ID = " & sCompanyID

        oMyArchiveSelectDAL.SQL = sMySQL
        If oMyArchiveSelectDAL.Reader_Execute() Then
            If oMyArchiveSelectDAL.Reader_HasRows Then
                Do While oMyArchiveSelectDAL.Reader_ReadRecord
                    sEndDate = oMyArchiveSelectDAL.Reader_GetString("EndDate")
                    'Add 1 to the date so it starts with the first date to update
                    dStartDate = System.DateTime.FromOADate(StringToDate(oMyArchiveSelectDAL.Reader_GetString("Date")).ToOADate + 1)
                    sStartDate = DateToString(dStartDate)
                    nIBSaldo = CDbl(oMyArchiveSelectDAL.Reader_GetString("UB"))
                Loop
            Else
                'Retrieve the initial IB and date
                sMySQL = "SELECT Date, UB FROM " & sOwner & "IBSaldo WHERE Company_ID = " & sCompanyID

                oMyArchiveSelectDAL.SQL = sMySQL
                If oMyArchiveSelectDAL.Reader_Execute() Then
                    If oMyArchiveSelectDAL.Reader_HasRows Then
                        Do While oMyArchiveSelectDAL.Reader_ReadRecord
                            If oMyArchiveSelectDAL.Reader_GetString("Date") = DateToString(System.DateTime.FromOADate(dStartDate.ToOADate - 1)) Then
                                sStartDate = oMyArchiveSelectDAL.Reader_GetString("Date")
                                nIBSaldo = CDbl(oMyArchiveSelectDAL.Reader_GetString("UB"))
                            Else
                                Err.Raise(7655, "NordeaLivUpdateBalance", "BabelBank is not able to retrieve a correct balance , because the initial balance set in the table IBSaldo doesn't correspond to the imported data." & vbCrLf & "The date stated in IBSaldo must be one day earlier than the first date on the imported file." & vbCrLf & "Date in IBSaldo: " & oMyArchiveSelectDAL.Reader_GetString("Date") & vbCrLf & "First date on importfile: " & DateToString(dStartDate))
                            End If
                        Loop
                    Else
                        Err.Raise(7654, "NordeaLivUpdateBalance", "BabelBank is not able to retrieve a correct balance , because no initial balance is set in the table IBSaldo!")
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & oMyArchiveSelectDAL.ErrorMessage)
                End If

            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyArchiveSelectDAL.ErrorMessage)
        End If

        'dStartDate is no storing information about the day before

        'TODO - Can't use FOR - NEXT on a Datevariable - Nordea Liv
        'For dActiveDate = dStartDate.ToOADate To dEndDate.ToOADate Step 1
        '    sMySQL = "SELECT SUM(MON_InvoiceAmount) AS Totalbelop FROM " & sOwner & "Payment WHERE DATE_Payment = '" & DateToString(dActiveDate) & "' AND MATCH_Matched <> " & Trim(Str(vbBabel.MatchStatus.ProposedMatched)) & " AND Company_ID = " & sCompanyID
        '    rsDB.Open(sMySQL, conArchiveDB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)
        '    If rsDB.RecordCount = 0 Then
        '        nAmount = 0
        '        'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
        '    ElseIf IsDBNull(rsDB.Fields("Totalbelop").Value) Then
        '        nAmount = 0
        '    Else
        '        nAmount = rsDB.Fields("Totalbelop").Value
        '    End If
        '    rsDB.Close()

        '    'Calculate the new balance
        '    nIBSaldo = nIBSaldo + nAmount

        '    'Store the new balance
        '    sMySQL = "IF (SELECT count(*) FROM BankAvstKoll.Saldo WHERE Date = '" & DateToString(dActiveDate) & "') = 1" & " BEGIN " & "UPDATE BankAvstKoll.Saldo SET UB = " & nIBSaldo & " WHERE Date = '" & DateToString(dActiveDate) & "'" & " END " & "ELSE " & "INSERT INTO BankAvstKoll.Saldo(Company_ID, Date, UB) VALUES(1, '" & DateToString(dActiveDate) & "', " & nIBSaldo & ")"

        '    If Not ExecuteTheSQL(conArchiveDB, sMySQL, sErrorString, False) Then
        '        'New code 11.04.2007 by Kjell - changed from msgbox to err.raise several places in this function.
        '        Err.Raise(22225, "NordeaLivUpdateBalance", sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        '        'MsgBox sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL
        '    End If

        'Next dActiveDate

        NordeaLivUpdateBalance = True

        On Error GoTo 0
        Exit Function

ErrNordeaLivUpdateBalance:

        NordeaLivUpdateBalance = False

        RollbackCopiedDataToArchiveDB()

        If bCalledFromOutside Then
            sErrorstringToReturn = Err.Description
        Else
            Err.Raise(22224, "NordeaLivUpdateBalance", Err.Description)
        End If


    End Function

End Class