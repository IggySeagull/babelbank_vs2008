Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
<System.Runtime.InteropServices.ProgId("Invoice_NET.Invoice")> Public Class Invoice
	
	Private oFile As Scripting.TextStream 'Physical file
    Private oXLWSheet As Microsoft.Office.Interop.Excel.Worksheet
	Private oProfile As vbBabel.Profile
	Private oCargo As vbBabel.Cargo
	Private oMapping As vbBabel.Mapping

    ' XNET 14.06.2013, added oErrorObject
    Private oErrorObjects As vbBabel.ErrorObjects

	Private nIndex As Double
    Private sStatusCode As String
    Private sVisma_StatusCode As String
	Private bCancel As Boolean
	Private bExported As Boolean
	Private nMON_InvoiceAmount As Double
	Private nMON_AccountAmount, nMON_TransferredAmount, nMON_LocalAmount As Double
	Private nMON_DiscountAmount As Double
	Private bAmountSetTransferred, bAmountSetInvoice As Boolean
	Private sREF_Bank, sREF_Own, sInvoice_ID As String
	Private sInvoiceNo, sCustomerNo As String
	Private sUnique_Id As String 'Used for KID
    'XNET - 13.12.2010 - Added next variable
    Private sInvoiceDate As String 'Can contain different dateformats
    Private sSTATEBANK_Text, sSTATEBANK_Code, sSTATEBANK_DATE As String
	Private sExtra1 As String
	Private iImportFormat As Short
	Private bMATCH_Final, bMATCH_Original, bMATCH_Matched As Boolean
    Private eMATCH_Type As vbBabel.BabelFiles.MatchType
    Private sMATCH_ID As String
    Private bMATCH_PartlyPaid As Boolean
    Private sMATCH_ClientNo As String
    Private sMATCH_VATCode As String ' added 03.10.06
    Private sCargoFTX, sMyField As String
    Private sMATCH_ERPName As String
    Private eQualifier As vbBabel.BabelFiles.InvoiceQualifier
    'New 30.01.2009
    Private eTypeOfStructuredInfo As vbBabel.BabelFiles.TypeOfStructuredInfo 'Not stored yet

    Dim sImportLineBuffer As String
    'local variable(s) to hold property value(s)
    Private oFreetexts As vbBabel.Freetexts 'local copy
    ' Added 09.08.06 - Used for special reporting purposes in report_990
    Private bToSpecialReport As Boolean
    ' XNET 07.02.2013 added SupplierNo, to take care of Swedish formats for outgoing payments
    Dim sSupplierNo As String
    Private sVisma_FrmNo As String          ' Firm number
    ' new 11.12.2014
    Private nMON_CurrencyAmount As Double
    Private sMON_Currency As String
    Private sDim1 As String
    Private sDim2 As String
    Private sDim3 As String
    Private sDim4 As String
    Private sDim5 As String
    Private sDim6 As String
    Private sDim7 As String
    Private sDim8 As String
    Private sDim9 As String
    Private sDim10 As String
    Private sMyField2 As String
    Private sMyField3 As String
    Private bBackPayment As Boolean
    Private sBackPaymentAccount As String

    '********* START PROPERTY SETTINGS ***********************
    Public ReadOnly Property Freetexts() As vbBabel.Freetexts
        Get
            Freetexts = oFreetexts
        End Get
    End Property
    Public Property Index() As Double
        Get
            Index = nIndex
        End Get
        Set(ByVal Value As Double)
            nIndex = Value
        End Set
    End Property
    Public Property StatusCode() As String
        Get
            StatusCode = sStatusCode
        End Get
        Set(ByVal Value As String)
            sStatusCode = Value
        End Set
    End Property
    Public Property Visma_StatusCode() As String
        Get
            Visma_StatusCode = sVisma_StatusCode
        End Get
        Set(ByVal Value As String)
            sVisma_StatusCode = Value
        End Set
    End Property
    Public Property Cancel() As Boolean
        Get
            Cancel = bCancel
        End Get
        Set(ByVal Value As Boolean)
            bCancel = Value
        End Set
    End Property
    Public Property Exported() As Boolean
        Get
            Exported = bExported
        End Get
        Set(ByVal Value As Boolean)
            bExported = Value
        End Set
    End Property
    Public Property Extra1() As String
        Get
            Extra1 = sExtra1
        End Get
        Set(ByVal Value As String)
            sExtra1 = Value
        End Set
    End Property
    Public Property MON_DiscountAmount() As Object
        Get
            MON_DiscountAmount = CObj(nMON_DiscountAmount)
        End Get
        Set(ByVal Value As Object)
            If EmptyString(Str(Value)) Then
                nMON_DiscountAmount = 0
            Else
                nMON_DiscountAmount = Val(Str(Value))
            End If
        End Set
    End Property
    Public Property MON_InvoiceAmount() As Double
        Get
            MON_InvoiceAmount = nMON_InvoiceAmount
        End Get
        Set(ByVal Value As Double)
            nMON_InvoiceAmount = Value
            bAmountSetInvoice = True
        End Set
    End Property
    Public Property MON_CurrencyAmount() As Object
        Get
            MON_CurrencyAmount = CObj(nMON_CurrencyAmount)
        End Get
        Set(ByVal Value As Object)
            If EmptyString(Str(Value)) Then
                nMON_CurrencyAmount = 0
            Else
                nMON_CurrencyAmount = Val(Str(Value))
            End If
        End Set
    End Property
    Public Property AmountSetTransferred() As Boolean
        Get
            AmountSetTransferred = bAmountSetTransferred
        End Get
        Set(ByVal Value As Boolean)
            bAmountSetTransferred = Value
        End Set
    End Property
    Friend ReadOnly Property AmountSetInvoice() As Boolean
        Get
            AmountSetInvoice = bAmountSetInvoice
        End Get
    End Property
    Public Property MON_TransferredAmount() As Double
        Get
            MON_TransferredAmount = nMON_TransferredAmount
        End Get
        Set(ByVal Value As Double)
            nMON_TransferredAmount = Value
            bAmountSetTransferred = True
        End Set
    End Property
    Public Property MON_AccountAmount() As Double
        Get
            MON_AccountAmount = nMON_AccountAmount
        End Get
        Set(ByVal Value As Double)
            nMON_AccountAmount = Value
        End Set
    End Property
    Public Property MON_LocalAmount() As Double
        Get
            MON_LocalAmount = nMON_LocalAmount
        End Get
        Set(ByVal Value As Double)
            nMON_LocalAmount = Value
        End Set
    End Property
    Public Property REF_Own() As String
        Get
            REF_Own = sREF_Own
        End Get
        Set(ByVal Value As String)
            sREF_Own = Value
        End Set
    End Property
    Public Property REF_Bank() As String
        Get
            REF_Bank = sREF_Bank
        End Get
        Set(ByVal Value As String)
            sREF_Bank = Value
        End Set
    End Property
    Public Property Invoice_ID() As String
        Get
            Invoice_ID = sInvoice_ID
        End Get
        Set(ByVal Value As String)
            sInvoice_ID = Value
        End Set
    End Property
    Public Property InvoiceNo() As String
        Get
            InvoiceNo = sInvoiceNo
        End Get
        Set(ByVal Value As String)
            sInvoiceNo = Value
        End Set
    End Property
    Public Property CustomerNo() As String
        Get
            CustomerNo = sCustomerNo
        End Get
        Set(ByVal Value As String)
            sCustomerNo = Value
        End Set
    End Property
    Public Property SupplierNo() As String
        Get
            SupplierNo = sSupplierNo
        End Get
        Set(ByVal Value As String)
            sSupplierNo = Value
        End Set
    End Property
    Public Property Unique_Id() As String
        Get
            Unique_Id = sUnique_Id
        End Get
        Set(ByVal Value As String)
            sUnique_Id = Value
        End Set
    End Property
    'XNET - 13.12.2010 - Added next 2 properties - Can contain different dateformats
    Public Property InvoiceDate() As String
        Get
            InvoiceDate = sInvoiceDate
        End Get
        Set(ByVal Value As String)
            sInvoiceDate = Value
        End Set
    End Property
    Public Property STATEBANK_Code() As String
        Get
            STATEBANK_Code = sSTATEBANK_Code
        End Get
        Set(ByVal Value As String)
            sSTATEBANK_Code = Value
        End Set
    End Property
    Public Property STATEBANK_Text() As String
        Get
            STATEBANK_Text = sSTATEBANK_Text
        End Get
        Set(ByVal Value As String)
            sSTATEBANK_Text = Value
        End Set
    End Property
    Public Property STATEBANK_DATE() As String
        Get
            'Format MMYYYY
            STATEBANK_DATE = sSTATEBANK_DATE
        End Get
        Set(ByVal Value As String)
            'Format MMYYYY
            sSTATEBANK_DATE = Value
        End Set
    End Property
    Public Property ImportFormat() As Short
        Get
            ImportFormat = iImportFormat
        End Get
        Set(ByVal Value As Short)
            iImportFormat = Value
        End Set
    End Property
    Public Property MATCH_Original() As Boolean
        Get
            MATCH_Original = bMATCH_Original
        End Get
        Set(ByVal Value As Boolean)
            bMATCH_Original = Value
        End Set
    End Property
    Public Property MATCH_Final() As Boolean
        Get
            MATCH_Final = bMATCH_Final
        End Get
        Set(ByVal Value As Boolean)
            bMATCH_Final = Value
        End Set
    End Property
    Public Property MATCH_Matched() As Boolean
        Get
            MATCH_Matched = bMATCH_Matched
        End Get
        Set(ByVal Value As Boolean)
            bMATCH_Matched = Value
        End Set
    End Property
    Public Property MATCH_PartlyPaid() As Boolean
        Get
            MATCH_PartlyPaid = bMATCH_PartlyPaid
        End Get
        Set(ByVal Value As Boolean)
            bMATCH_PartlyPaid = Value
        End Set
    End Property
    Public Property MATCH_MatchType() As vbBabel.BabelFiles.MatchType
        Get
            MATCH_MatchType = eMATCH_Type
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.MatchType)
            eMATCH_Type = Value
        End Set
    End Property
    Public Property TypeOfStructuredInfo() As vbBabel.BabelFiles.TypeOfStructuredInfo
        Get
            TypeOfStructuredInfo = eTypeOfStructuredInfo
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.TypeOfStructuredInfo)
            eTypeOfStructuredInfo = Value
        End Set
    End Property
	Public Property MATCH_ID() As String
		Get
			MATCH_ID = sMATCH_ID
		End Get
		Set(ByVal Value As String)
			sMATCH_ID = Value
		End Set
	End Property
	Public Property MATCH_ClientNo() As String
		Get
			MATCH_ClientNo = sMATCH_ClientNo
		End Get
		Set(ByVal Value As String)
			sMATCH_ClientNo = Value
		End Set
	End Property
	Public Property MATCH_VATCode() As String
		Get
			MATCH_VATCode = sMATCH_VATCode
		End Get
		Set(ByVal Value As String)
			sMATCH_VATCode = Value
		End Set
	End Property
	Friend WriteOnly Property CargoFTX() As String
		Set(ByVal Value As String)
			sCargoFTX = Value
		End Set
	End Property
	Public Property MyField() As String
		Get
			MyField = sMyField
		End Get
		Set(ByVal Value As String)
			sMyField = Value
		End Set
	End Property
	Public Property MATCH_ERPName() As String
		Get
			MATCH_ERPName = sMATCH_ERPName
		End Get
		Set(ByVal Value As String)
			sMATCH_ERPName = Value
		End Set
	End Property
    Public Property Qualifier() As vbBabel.BabelFiles.InvoiceQualifier
        Get
            Qualifier = eQualifier
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.InvoiceQualifier)
            eQualifier = Value
        End Set
    End Property
	Friend WriteOnly Property objFile() As Scripting.TextStream
		Set(ByVal Value As Scripting.TextStream)
            If Not IsReference(Value) Then 'Or NewVal Is Nothing Then
                'Error # 10001
                Err.Raise(10001, , LRS(10001))
            End If
			
			oFile = Value
		End Set
	End Property
    Friend WriteOnly Property objXLSheet() As Microsoft.Office.Interop.Excel.Worksheet
        Set(ByVal Value As Microsoft.Office.Interop.Excel.Worksheet)
            'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Not IsReference(Value) Then ' Or NewVal Is Nothing Then
                'Error # 10001
                Err.Raise(10001, , LRS(10001))
            End If

            oXLWSheet = Value
        End Set
    End Property
	Public Property VB_Profile() As vbBabel.Profile
		Get
			VB_Profile = oProfile
		End Get
		Set(ByVal Value As vbBabel.Profile)
			'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Not IsReference(Value) Then
				RaiseInvalidObject()
			End If
            'Store reference
			oProfile = Value
		End Set
	End Property
	Friend Property Cargo() As vbBabel.Cargo
		Get
			Cargo = oCargo
		End Get
		Set(ByVal Value As vbBabel.Cargo)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If
			
			'Store reference
			oCargo = Value
		End Set
	End Property
    Public Property ErrorObjects() As vbBabel.ErrorObjects
        Get
            ErrorObjects = oErrorObjects
        End Get
        Set(ByVal Value As vbBabel.ErrorObjects)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oErrorObjects = Value
        End Set
    End Property
    Friend WriteOnly Property Mapping() As vbBabel.Mapping
        Set(ByVal Value As vbBabel.Mapping)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oMapping = Value
        End Set
    End Property
    Public Property ToSpecialReport() As Boolean
        Get
            ToSpecialReport = bToSpecialReport
        End Get
        Set(ByVal Value As Boolean)
            bToSpecialReport = Value
        End Set
    End Property
    Public Property Visma_FrmNo() As String
        Get
            Visma_FrmNo = sVisma_FrmNo
        End Get
        Set(ByVal Value As String)
            sVisma_FrmNo = Value
        End Set
    End Property
    Public Property MON_Currency() As String
        Get
            MON_Currency = sMON_Currency
        End Get
        Set(ByVal Value As String)
            sMON_Currency = Value
        End Set
    End Property
    Public Property Dim1() As String
        Get
            Dim1 = sDim1
        End Get
        Set(ByVal Value As String)
            sDim1 = Value
        End Set
    End Property
    Public Property Dim2() As String
        Get
            Dim2 = sDim2
        End Get
        Set(ByVal Value As String)
            sDim2 = Value
        End Set
    End Property
    Public Property Dim3() As String
        Get
            Dim3 = sDim3
        End Get
        Set(ByVal Value As String)
            sDim3 = Value
        End Set
    End Property
    Public Property Dim4() As String
        Get
            Dim4 = sDim4
        End Get
        Set(ByVal Value As String)
            sDim4 = Value
        End Set
    End Property
    Public Property Dim5() As String
        Get
            Dim5 = sDim5
        End Get
        Set(ByVal Value As String)
            sDim5 = Value
        End Set
    End Property
    Public Property Dim6() As String
        Get
            Dim6 = sDim6
        End Get
        Set(ByVal Value As String)
            sDim6 = Value
        End Set
    End Property
    Public Property Dim7() As String
        Get
            Dim7 = sDim7
        End Get
        Set(ByVal Value As String)
            sDim7 = Value
        End Set
    End Property
    Public Property Dim8() As String
        Get
            Dim8 = sDim8
        End Get
        Set(ByVal Value As String)
            sDim8 = Value
        End Set
    End Property
    Public Property Dim9() As String
        Get
            Dim9 = sDim9
        End Get
        Set(ByVal Value As String)
            sDim9 = Value
        End Set
    End Property
    Public Property Dim10() As String
        Get
            Dim10 = sDim10
        End Get
        Set(ByVal Value As String)
            sDim10 = Value
        End Set
    End Property
    Public Property MyField2() As String
        Get
            MyField2 = sMyField2
        End Get
        Set(ByVal Value As String)
            sMyField2 = Value
        End Set
    End Property
    Public Property MyField3() As String
        Get
            MyField3 = sMyField3
        End Get
        Set(ByVal Value As String)
            sMyField3 = Value
        End Set
    End Property
    Public Property BackPayment() As Boolean
        Get
            BackPayment = bBackPayment
        End Get
        Set(ByVal Value As Boolean)
            bBackPayment = Value
        End Set
    End Property
    Public Property BackPaymentAccount() As String
        Get
            BackPaymentAccount = sBackPaymentAccount
        End Get
        Set(ByVal Value As String)
            sBackPaymentAccount = Value
        End Set
    End Property
    '********* END PROPERTY SETTINGS ***********************

    Friend Function Import(ByRef sData As Object) As String

        'Assume failure
        Import = ""

        'FIX: Babelbank_Error

        'UPGRADE_WARNING: Couldn't resolve default property of object sData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = sData

        Import = ReadFile()
        'If Not ReadFile() Then
        'FIX:Add error info
        'End If

        'Import = True

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing


    End Function
    Friend Function ImportEDI(ByRef oPRC As MSXML2.IXMLDOMNode, ByRef oEDIMapping As MSXML2.IXMLDOMElement, ByRef sTypeOfPayment As String) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportEDI = ReadEDIFile(oPRC, oEDIMapping, sTypeOfPayment)

        'Exit Function
    End Function
    'Removed 11.09.2019
    'Friend Function ImportTBI(ByRef nodPaymentDetails As MSXML2.IXMLDOMNode, ByRef sTBIType As String) As Boolean
    '    Dim bReturnValue As Boolean

    '    'Assume failure
    '    bReturnValue = False

    '    'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    oFile = Nothing

    '    ImportTBI = ReadTBIFile(nodPaymentDetails, sTBIType)

    '    'Exit Function
    'End Function
    Friend Function ImportINS2000(ByRef nodPaymentDetails As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportINS2000 = ReadINS2000(nodPaymentDetails)

        'Exit Function
    End Function
    'Removed 11.09.2019
    'Friend Function ImportNHC_Philippines(ByRef nodPaymentDetails As System.Xml.XmlNode) As Boolean
    '    Dim bReturnValue As Boolean

    '    'Assume failure
    '    bReturnValue = False

    '    'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    oFile = Nothing

    '    ImportNHC_Philippines = ReadNHC_Philippines(nodPaymentDetails)

    '    'Exit Function
    'End Function
    Friend Function ImportBambora_XML(ByRef nodPaymentDetails As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportBambora_XML = ReadBambora_XML(nodPaymentDetails)

        'Exit Function
    End Function
    Friend Function ImporteGlobal(ByVal nodPaymentDetails As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        oFile = Nothing

        ImporteGlobal = ReadeGlobal(nodPaymentDetails)

        'Exit Function
    End Function
    Friend Function ImportTellerXML(ByRef nodTrans_Detail As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportTellerXML = ReadTellerXML(nodTrans_Detail)

        'Exit Function
    End Function
    Friend Function ImportTellerXMLNew(ByRef nodTransaction As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing

        ImportTellerXMLNew = ReadTellerXMLNew(nodTransaction)

        'Exit Function
    End Function
    ' XokNET 21.08.2015 added function
    Friend Function ImportTellerXMLNew_2015(ByVal nodTransaction As MSXML2.IXMLDOMNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        oFile = Nothing

        ImportTellerXMLNew_2015 = ReadTellerXMLNew_2015(nodTransaction)

        'Exit Function
    End Function
    Friend Function ImportTellerCardXML(ByVal nodTransaction As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        oFile = Nothing

        ImportTellerCardXML = ReadTellerCardXML(nodTransaction)

        'Exit Function
    End Function
    Friend Function ImportHelseFonna_XML_Nets(ByVal nodTransaction As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        oFile = Nothing

        ImportHelseFonna_XML_Nets = ReadHelseFonna_XML_Nets(nodTransaction)

        'Exit Function
    End Function
    'Friend Function ImportAdyen_XML(ByVal nodInvoice As System.Xml.XmlNode) As Boolean
    '    Dim bReturnValue As Boolean

    '    'Assume failure
    '    bReturnValue = False

    '    oFile = Nothing

    '    ImportAdyen_XML = ReadAdyen_XML(nodInvoice)

    '    'Exit Function
    'End Function
    Friend Function ImportVippsXML(ByVal nodTransaction As System.Xml.XmlNode) As Boolean
        Dim bReturnValue As Boolean

        'Assume failure
        bReturnValue = False

        oFile = Nothing

        ImportVippsXML = ReadVippsXML(nodTransaction)

        'Exit Function
    End Function
    Private Function ReadFile() As String

        ReadFile = ""

        Select Case iImportFormat
            Case BabelFiles.FileType.Telepay
                ReadFile = ReadTelepayFile()
            Case BabelFiles.FileType.Telepay2
                ReadFile = ReadTelepay2File()
            Case BabelFiles.FileType.TelepayPlus
                ReadFile = ReadTelepay2File("+")
            Case BabelFiles.FileType.AutoGiro
                'ReadAutoGiro
            Case BabelFiles.FileType.OCR
                'FIX: Babelerror ReadOCR
            Case BabelFiles.FileType.Dirrem
                ReadFile = ReadDirremFile()
            Case BabelFiles.FileType.coda
                ReadFile = ReadCodaFile()
            Case BabelFiles.FileType.TelepayTBIO
                ReadFile = ReadTelepayFile()
            Case BabelFiles.FileType.OneTimeFormat
                ReadFile = ReadOneTimeFormat()
            Case BabelFiles.FileType.DnB_TBIW_Standard
                ReadFile = ReadDnB_TBIW_Standard()
            Case BabelFiles.FileType.DTAUS
                ReadFile = ReadDTAUS()
            Case BabelFiles.FileType.NordeaUnitel
                ReadFile = ReadNordeaUnitel()
            Case BabelFiles.FileType.ShipNet
                ReadFile = ReadShipNet()
            Case BabelFiles.FileType.CitiDirect_US_Flat_File
                ReadFile = ReadCitiDirectUSFlatFile()
            Case BabelFiles.FileType.BACS_For_TBI
                ReadFile = ReadCitiDirectUSFlatFile()
            Case BabelFiles.FileType.First_Union_ACH
                ReadFile = ReadFirst_Union_ACH()
            Case BabelFiles.FileType.DnBNattsafe
                ReadFile = ReadDnBNattsafe()
            Case BabelFiles.FileType.SecuritasNattsafe
                ReadFile = ReadSecuritasNattsafe()
            Case BabelFiles.FileType.Nordea_Innbet_Ktoinf_utl
                ReadFile = ReadNordea_Innbet_Ktoinf_utl()
            Case BabelFiles.FileType.Handelsbanken_DK_Domestic
                ReadFile = ReadHandelsbanken_DK_Domestic()
            Case BabelFiles.FileType.LM_Domestic_Finland
                ReadFile = ReadLM_Domestic_Finland()
            Case BabelFiles.FileType.LUM_International_Finland
                ReadFile = ReadLUM_International_Finland()
            Case BabelFiles.FileType.Excel
                ReadFile = ReadExcelFile()
            Case BabelFiles.FileType.EXCEL_Innbetaling
                ReadFile = ReadExcelInnbetalingFile()
            Case BabelFiles.FileType.Modhi_Internal
                ReadFile = ReadExcelInnbetalingFile()
            Case BabelFiles.FileType.ErhvervsGiro_Udbetaling
                ReadFile = ReadErhvervsGiro()
            Case BabelFiles.FileType.NordeaDK_EDI
                ReadFile = ReadNordeaDK_EDI()
            Case BabelFiles.FileType.Excel_XLS
                ReadFile = ReadExcelXLS()
            Case BabelFiles.FileType.Avprickning
                ReadFile = ReadAvprickning()
            Case BabelFiles.FileType.BgMax
                ReadFile = ReadBgMax()
            Case BabelFiles.FileType.OCR_Bankgirot
                ReadFile = ReadOCR_Bankgirot()
            Case BabelFiles.FileType.OCR_Bankgirot_As_Message
                ReadFile = ReadOCR_Bankgirot()
            Case BabelFiles.FileType.PTG
                ReadFile = ReadErhvervsGiro()
            Case BabelFiles.FileType.Periodiska_Betalningar
                ReadFile = ReadPeriodiskaBetalningarFinland()
            Case BabelFiles.FileType.KLINK_ExcelUtbetaling, BabelFiles.FileType.Nordea_Liv_Registrert
                ReadFile = ReadKLINK_ExcelUtbetaling()
            Case BabelFiles.FileType.DnBNOR_UK_Standard
                ReadFile = ReadDnBNOR_UK_TBI_Standard()
            Case BabelFiles.FileType.Erhvervsformat_BEC_DK
                ReadFile = ReadHandelsbanken_DK_Domestic()
            Case BabelFiles.FileType.ABNAmro_BTL91
                ReadFile = ReadABNAmroBTL91()
                ' XNET 02.11.2010
            Case BabelFiles.FileType.GiroDirekt_Plusgiro
                ReadFile = ReadGirodirekt_Plusgiro()
                ' XNET 09.10.2012
            Case BabelFiles.FileType.SEB_CI_Incoming
                ReadFile = ReadSEB_CI_Incoming()
                ' XNET 30.10.2012
            Case BabelFiles.FileType.DanskeBankCollection
                ReadFile = ReadDanskeBankCollection()
                ' XNET 23.02.2013
            Case BabelFiles.FileType.Total_IN
                ReadFile = ReadTotal_IN()
            Case BabelFiles.FileType.Swedbank_CardReconciliation
                ReadFile = ReadSwedbank_CardReconciliation()

                'Specialformats
                'Case FileType.CapsyAG
                '    ReadFile = ReadCapsyAGFile()
            Case BabelFiles.FileType.OrklaMediaExcel
                ReadFile = ReadOrklaMediaExcelFile()
            Case BabelFiles.FileType.ISS_SDV
                ReadFile = ReadISS_SDVFile()
            Case BabelFiles.FileType.ISS_LonnsTrekk
                ReadFile = ReadISS_LonnsTrekk()
            Case BabelFiles.FileType.SRI_Access
                ReadFile = ReadSRI_Access()
            Case BabelFiles.FileType.Compuware
                ReadFile = ReadCompuware()
            Case BabelFiles.FileType.SeaTruck
                ReadFile = ReadSeaTruck()
            Case BabelFiles.FileType.Elkjop
                ReadFile = ReadElkjopFile()
            Case BabelFiles.FileType.DanskeBankLokaleDanske, BabelFiles.FileType.DnBNOR_TBIW_Denmark
                ReadFile = ReadDanskeBankLokaleDanske()
            Case BabelFiles.FileType.DnBTBUKIncoming
                ReadFile = ReadDnBTBUKIncoming()
            Case BabelFiles.FileType.ControlConsultExcel
                ReadFile = ReadControlConsultExcelFile()
            Case BabelFiles.FileType.DnBStockholmExcel
                ReadFile = ReadDnBStockholmExcelFile()
            Case BabelFiles.FileType.FirstNordic
                ReadFile = ReadFirstNordicFile()
            Case BabelFiles.FileType.pgs
                ReadFile = ReadPGS()
            Case BabelFiles.FileType.Sydbank
                ReadFile = ReadSydbank()
            Case BabelFiles.FileType.Sandvik_Tamrock
                ReadFile = ReadExcelFile()
            Case BabelFiles.FileType.NCL
                ReadFile = ReadNCLFile()
            Case BabelFiles.FileType.DnBTBWK
                ReadFile = ReadDnBTBWK()
            Case BabelFiles.FileType.Autocare_Norge
                ReadFile = ReadAutocare_Norge()
            Case BabelFiles.FileType.StrommeSingapore
                ReadFile = ReadStrommeSingapore()
            Case BabelFiles.FileType.StrommeSingaporeG3
                ReadFile = ReadStrommeSingapore_G3()

            Case BabelFiles.FileType.TeekaySingapore
                ReadFile = ReadTeekaySingapore()
            Case BabelFiles.FileType.Sudjaca
                ReadFile = ReadSudjaca()
            Case BabelFiles.FileType.Tandberg_ACH
                ReadFile = ReadExcelFile() ' Set oCargoSpecial in BabelFile.ReadFile
            Case BabelFiles.FileType.Bankdata_DK_4_45 ' Bankdata from SEB DK (SEBdec)
                ReadFile = ReadSydbank() ' Nearly the same format as Sydbank
            Case BabelFiles.FileType.CyberCity
                ReadFile = ReadCyberCity()
            Case BabelFiles.FileType.NSA
                ReadFile = ReadNSA()
            Case BabelFiles.FileType.AquariusOutgoing
                ReadFile = ReadAquariusOutgoing()
            Case BabelFiles.FileType.PGS_Sweden
                ReadFile = ReadPGS()
            Case BabelFiles.FileType.Gjensidige_S2000_PAYMUL
                ReadFile = ReadGjensidige_S2000_PAYMULFile()
            Case BabelFiles.FileType.AquariusIncoming
                'ReadFile = ReadAquariusIncoming
            Case BabelFiles.FileType.DnBNOR_US_Penta
                ReadFile = ReadDnBNOR_US_Penta()
            Case BabelFiles.FileType.RECSingapore
                ReadFile = ReadRECSingapore()
            Case BabelFiles.FileType.DnV_UK_Sage
                ReadFile = Read_DnV_UK_Sage()
            Case BabelFiles.FileType.Seajack_Wages
                ReadFile = ReadSeajackWages()
                ' XNET 14.10.2010 Added function ReadSAP_PAYEXT_PEXR2002
            Case BabelFiles.FileType.SAP_PAYEXT_PEXR2002
                ReadFile = ReadSAP_PAYEXT_PEXR2002()
                ' XNET 29.10.2010 added format
            Case BabelFiles.FileType.DnBNOR_Connect_Test
                ReadFile = ReadDnBNOR_Connect_Test()
                'XokNET 12.04.2012
            Case BabelFiles.FileType.DTAZV
                ReadFile = ReadDTAZV()
            Case BabelFiles.FileType.Lindorff_Gebyr
                ReadFile = ReadLindorff_Gebyr()
            Case BabelFiles.FileType.FlyWire
                ReadFile = ReadFlyWire_Incoming()

        End Select


    End Function
    Private Function ReadTelepayFile() As String
        Dim oFreeText As Freetext
        Dim bReadLine As Boolean
        'Dim bCreateObject As Boolean
        'Dim bFromLevelUnder As Boolean

        Select Case Mid(sImportLineBuffer, 41, 8)


            'BETFOR04 Invoice international
            Case "BETFOR04"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 151, 15))
                If Mid(sImportLineBuffer, 166, 1) <> "D" Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
                bAmountSetInvoice = True
                'nMON_InvoiceAmount = nMON_InvoiceAmount / 100
                sREF_Own = Mid(sImportLineBuffer, 116, 35)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                'sInvoiceNo - Will be added later. Have to be extracted form freetext.
                'sCustomerNo - Will be added later. Have to be extracted form freetext.
                'sUnique_Id = Not used in international payments
                sSTATEBANK_Code = Trim(Mid(sImportLineBuffer, 167, 6))
                sSTATEBANK_Text = Mid(sImportLineBuffer, 173, 60)
                'sExtra1 - Not used in this format
                'Add freetext, one line.
                '29.01.2015 - Added next IF to use the KID-mark in the format
                If sImportLineBuffer.Substring(292, 1).ToUpper = "K" Then
                    sUnique_Id = Trim(Mid(sImportLineBuffer, 81, 35))
                    ' 16.09.2015 - read freetext also even if KID set in BETFOR04
                End If
                'Else
                If Trim(Mid(sImportLineBuffer, 81, 35)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 81, 35) & Space(5)
                End If
                'End If
                bReadLine = True

            Case "BETFOR21"
                If Mid(sImportLineBuffer, 267, 1) = "E" Then
                    sStatusCode = Mid(sImportLineBuffer, 4, 2)
                    If Val(sStatusCode) > 2 Then
                        ' Rejected, move up in the ladder ...
                        oCargo.RejectsExists = True
                    End If

                    nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 249, 15))
                    bAmountSetInvoice = True
                    bReadLine = True
                Else
                    'Something wrong in the format
                End If

                'BETFOR22 Mass payment
            Case "BETFOR22"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                If Mid(sImportLineBuffer, 137, 1) = "S" Then
                    bCancel = True
                End If
                sREF_Own = Mid(sImportLineBuffer, 138, 35)
                ' Two ownrefs in BETFOR22!
                ' Use Extra1 to store second ownref (10 characters)
                sExtra1 = Mid(sImportLineBuffer, 283, 10)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 122, 15))
                bAmountSetInvoice = True
                'nMON_InvoiceAmount = nMON_InvoiceAmount / 100
                bReadLine = True


                'BETFOR23 Invoice domestic
            Case "BETFOR23"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 258, 15))
                If Mid(sImportLineBuffer, 273, 1) <> "D" Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
                bAmountSetInvoice = True
                'nMON_InvoiceAmount = nMON_InvoiceAmount / 100
                sREF_Own = Mid(sImportLineBuffer, 228, 30)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                'sInvoiceNo - Will be added later. Have to be extracted form freetext.
                'sCustomerNo - Will be added later. Have to be extracted form freetext.
                sUnique_Id = Mid(sImportLineBuffer, 201, 27)
                'sSTATEBANK_Code - Not used in domestic payments
                'sSTATEBANK_Text - Not used in domestic payments
                'sExtra1 - Not used in this format
                'Add freetext, up to three lines in the format
                If Trim(Mid(sImportLineBuffer, 81, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 81, 40)
                End If
                If Trim(Mid(sImportLineBuffer, 121, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 121, 40)
                End If
                If Trim(Mid(sImportLineBuffer, 161, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 161, 40)
                End If
                bReadLine = True

                'BETFOR23 Invoice domestic
            Case "BETFOR99"
                'New code 10.07.2002
                'Specialcase from DnB when the last transaction is deleted (I think online)
                'We have to test if the transaction is deleted, if so we accept it
                'FIX: To be 100% correct we should also test that it is from DnB
                If bCancel = True Then
                    bReadLine = False
                Else
                    'ERROR
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-004")
                End If

                'All other content would indicate error
            Case Else
                'All other BETFOR-types gives error
                'Error # 10010-004
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-004")


        End Select

        'New code 10.07.2002
        If bReadLine Then
            'Read new record
            ' XNET 06.07.2012 extra columns in file from XELLIA
            If oCargo.Special = "XELLIA" Then
                sImportLineBuffer = ReadTelepayXelliaRecord(oFile)
            Else
                sImportLineBuffer = ReadTelepayRecord(oFile) ', oProfile)
            End If
        End If

        ReadTelepayFile = sImportLineBuffer

    End Function
    Private Function ReadTelepay2File(Optional ByRef TelepayType As String = "") As String
        '-----------------------------------------------------------------------
        ' 30.12.2008: Added parameter TelepayType.
        '             Used so far for DnBNOR Telepay+, then the parameter = "+"
        '-----------------------------------------------------------------------
        Dim oFreeText As Freetext
        Dim bReadLine As Boolean

        Select Case Mid(sImportLineBuffer, 41, 8)
            'BETFOR04 Invoice international
            Case "BETFOR04"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 151, 15))
                If Mid(sImportLineBuffer, 166, 1) <> "D" Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
                bAmountSetInvoice = True
                sREF_Own = Mid(sImportLineBuffer, 116, 35)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                sSTATEBANK_Code = Trim(Mid(sImportLineBuffer, 167, 6))
                ' added 03.11.2008 - for those using 000000 we have unneccesary warnings in TBI
                If sSTATEBANK_Code = "000000" Then
                    sSTATEBANK_Code = ""
                End If
                sSTATEBANK_Text = Mid(sImportLineBuffer, 173, 60)

                ' 22.03.2021 JPS
                ' DETTE UNDER ER FEIL - DET SKAL V�RE POS 293.
                ' MEN VI LAR DET ST� IGJEN OGS� 292, i tilfelle noe har brukt dette fra f�r
                '29.01.2015 - Added next IF to use the KID-mark in the format
                If sImportLineBuffer.Substring(293, 1).ToUpper = "K" Then
                    sUnique_Id = Trim(Mid(sImportLineBuffer, 81, 35))
                End If
                If sImportLineBuffer.Substring(292, 1).ToUpper = "K" Then
                    sUnique_Id = Trim(Mid(sImportLineBuffer, 81, 35))
                End If
                'Else
                'Add freetext, one line.
                ' 13.11.2015 - do not import freetext if marked as KID 
                ' 22.03.2021 - RIktig skal v�re pos 293
                If sImportLineBuffer.Substring(292, 1).ToUpper <> "K" And sImportLineBuffer.Substring(293, 1).ToUpper <> "K" Then
                    If Trim(Mid(sImportLineBuffer, 81, 35)) <> "" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 81, 35) & Space(5)
                    End If
                End If

                bReadLine = True

            Case "BETFOR21"
                If Mid(sImportLineBuffer, 267, 1) = "E" Then
                    sStatusCode = Mid(sImportLineBuffer, 4, 2)
                    If Val(sStatusCode) > 2 Then
                        ' Rejected, move up in the ladder ...
                        oCargo.RejectsExists = True
                    End If

                    nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 249, 15))
                    bAmountSetInvoice = True
                    bReadLine = True
                Else
                    'Something wrong in the format
                End If

                'BETFOR22 Mass payment
            Case "BETFOR22"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                If Mid(sImportLineBuffer, 137, 1) = "S" Then
                    bCancel = True
                End If
                sREF_Own = Mid(sImportLineBuffer, 138, 35)
                ' Two ownrefs in BETFOR22!
                ' Use Extra1 to store second ownref (10 characters)
                sExtra1 = Mid(sImportLineBuffer, 283, 10)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 122, 15))
                bAmountSetInvoice = True
                bReadLine = True

                ' XNET 15.11.2010 freetext
                If TelepayType = "+" Then
                    ' Remittance information  409;548 140 ALPHA
                    If Trim$(Mid$(sImportLineBuffer, 409, 35)) <> "" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 409, 35)
                    End If
                    If Trim$(Mid$(sImportLineBuffer, 444, 35)) <> "" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 444, 35)
                    End If
                    If Trim$(Mid$(sImportLineBuffer, 479, 35)) <> "" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 479, 35)
                    End If
                    If Trim$(Mid$(sImportLineBuffer, 514, 35)) <> "" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 514, 35)
                    End If
                End If
                ' END XNET

                'BETFOR23 Invoice domestic
            Case "BETFOR23"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 258, 15))
                If Mid(sImportLineBuffer, 273, 1) <> "D" And Mid(sImportLineBuffer, 273, 1) <> "-" Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
                ' added 25.03.2015
                ' - in pos 273 means that the invoice is deleted
                If Mid(sImportLineBuffer, 273, 1) = "-" Then
                    bCancel = True
                    bToSpecialReport = True
                End If
                bAmountSetInvoice = True
                sREF_Own = Mid(sImportLineBuffer, 228, 30)
                sInvoice_ID = Trim(Mid(sImportLineBuffer, 75, 6))
                sUnique_Id = Trim(Mid(sImportLineBuffer, 201, 27))
                sInvoiceNo = Trim(Mid(sImportLineBuffer, 274, 20))
                sCustomerNo = Trim(Mid(sImportLineBuffer, 298, 13))
                'XNET - 13.12.2010 - Added next line
                sInvoiceDate = Mid$(sImportLineBuffer, 313, 8)

                'Add freetext, up to three lines in the format
                If Trim(Mid(sImportLineBuffer, 81, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 81, 40)
                End If
                If Trim(Mid(sImportLineBuffer, 121, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 121, 40)
                End If
                If Trim(Mid(sImportLineBuffer, 161, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 161, 40)
                End If
                bReadLine = True

                'BETFOR23 Invoice domestic
            Case "BETFOR99"
                'New code 10.07.2002
                'Specialcase from DnB when the last transaction is deleted (I think online)
                'We have to test if the transaction is deleted, if so we accept it
                'FIX: To be 100% correct we should also test that it is from DnB
                If bCancel = True Then
                    bReadLine = False
                Else
                    'ERROR
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-004")
                End If

                ' XNET 17.11.2010 Added BETFOR35 (not in use)
            Case "BETFOR35"
                ' not in use (ultimate creditor)
                bReadLine = True

                ' XokNET 19.09.2011 Added BETFOR36 (not in use)
            Case "BETFOR36"
                ' not in use (extended bankinfo)
                bReadLine = True

                ' XokNET 11.06.2015 Added BETFOR37 (not in use)
            Case "BETFOR37"
                ' not in use (extended bankinfo)
                bReadLine = True

                ' XokNET 11.06.2015 Added BETFOR11 (not in use)
            Case "BETFOR11"
                ' not in use (extended bankinfo)
                bReadLine = True

                'All other content would indicate error
            Case Else
                'All other BETFOR-types gives error
                'Error # 10010-004
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-004")


        End Select

        If bReadLine Then
            'Read new record
            ' New 27.03.06
            If oCargo.Special = "LINE80" Then
                sImportLineBuffer = ReadTelepayRecord(oFile, True) ', oProfile)
            Else
                ' added TelepayPlus 30.12.2008
                If TelepayType = "+" Then
                    sImportLineBuffer = ReadTelepayPlusRecord(oFile)
                Else
                    sImportLineBuffer = ReadTelepayRecord(oFile) ', oProfile)
                End If
            End If
        End If

        ReadTelepay2File = sImportLineBuffer

    End Function
    Private Function ReadDirremFile() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext

        bReadLine = False

        Do While True

            Select Case Mid(sImportLineBuffer, 7, 2)

                Case "30"
                    Exit Do

                    'Informationrecord
                Case "49"
                    oFreeText = oFreetexts.Add()
                    ' Added 19.9.05
                    ' Must save line and col-pos for text on voucher;
                    oFreeText.Row = Val(Mid(sImportLineBuffer, 16, 3))
                    oFreeText.Col = Val(Mid(sImportLineBuffer, 19, 1))

                    oFreeText.Text = Mid(sImportLineBuffer, 20, 40)
                    bReadLine = True

                    'Special KID-record
                Case "50"
                    If Mid(sImportLineBuffer, 5, 2) = "16" Then
                        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 41, 17))
                        bAmountSetInvoice = True
                        sUnique_Id = Mid(sImportLineBuffer, 16, 25)
                    ElseIf Mid(sImportLineBuffer, 5, 2) = "17" Then
                        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 41, 17)) * -1
                        bAmountSetInvoice = True
                        sUnique_Id = Mid(sImportLineBuffer, 16, 25)
                        'Set oFreeText = oFreetexts.Add()
                        'oFreeText.Text = Mid(sImportLineBuffer, 16, 25)
                    Else
                        'BB-error
                    End If
                    bReadLine = True

                Case "88"
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'Error # 10010-008
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-008")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If


            If bReadLine Then
                'Read new record
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadDirremRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadDirremRecord(oFile) ', oProfile)
            End If

            If Mid(sImportLineBuffer, 5, 2) = "16" Or Mid(sImportLineBuffer, 5, 2) = "17" Then
                ' we have to move up to create another Invoice-object!
                Exit Do
            End If

        Loop

        ReadDirremFile = sImportLineBuffer

    End Function
    Private Function ReadLM_Domestic_Finland() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sTypeOfMessage As String

        bReadLine = True

        Do While True

            Select Case Mid(sImportLineBuffer, 5, 1)

                Case "1", "2" ' Transaction record or Specification record

                    If Mid(sImportLineBuffer, 6, 1) <> "9" Then
                        ' Payment, 0/4, or Credit Note 2
                        oFreeText = oFreetexts.Add()

                        sTypeOfMessage = Mid(sImportLineBuffer, 108, 1)
                        Select Case sTypeOfMessage
                            Case "1" ' Reference.
                                sUnique_Id = Mid(sImportLineBuffer, 109, 20)
                                'oFreeText.Text = Mid(sImportLineBuffer, 109, 20)
                            Case "2" ' Payment data
                                oFreeText.Text = PadRight(Trim(Mid(sImportLineBuffer, 109, 31)), 40, " ")
                                sCustomerNo = Trim(Mid(sImportLineBuffer, 109, 10))
                                sInvoiceNo = Trim(Mid(sImportLineBuffer, 119, 15))
                            Case "3", "5", "6"
                                ' 04.02.05; Special for MI Chemicals
                                If sTypeOfMessage = "5" And oCargo.Special = "MICHEMICALS" Then
                                    ' Reference.
                                    sUnique_Id = Mid(sImportLineBuffer, 109, 20)
                                Else
                                    ' Normal file
                                    oFreeText.Text = Mid(sImportLineBuffer, 109, 35) & Space(5)
                                    oFreeText = oFreetexts.Add()
                                    oFreeText.Text = Mid(sImportLineBuffer, 144, 35) & Space(5)
                                End If
                            Case "7" ' Tax message
                                oFreeText.Text = "Ref: " & Mid(sImportLineBuffer, 109, 20)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = Mid(sImportLineBuffer, 144, 35) & Space(5)
                        End Select

                        ' Amount
                        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 187, 12))
                        ' Transactiontype 2 is Credit Note
                        If Mid(sImportLineBuffer, 6, 1) = "2" Then
                            nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                        End If

                    Else
                        '9 - Messagerecord
                        ' Additonal freetexts
                        If Len(Mid(sImportLineBuffer, 104, 35) & Space(5)) > 0 Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 104, 35) & Space(5)
                        End If
                        If Len(Mid(sImportLineBuffer, 139, 35) & Space(5)) > 0 Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 139, 35) & Space(5)
                        End If
                        If Len(Mid(sImportLineBuffer, 174, 35) & Space(5)) > 0 Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 174, 35) & Space(5)
                        End If
                        If Len(Mid(sImportLineBuffer, 209, 35) & Space(5)) > 0 Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 209, 35) & Space(5)
                        End If
                        If Len(Mid(sImportLineBuffer, 244, 35) & Space(5)) > 0 Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 244, 35) & Space(5)
                        End If

                    End If
                Case "9" ' End record - added 11.12.2009
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'Error # 10010-008
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-008")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadLM_Domestic_FinlandRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadLM_Domestic_FinlandRecord(oFile, oCargo.Special) '04.02.05; Added Special for MI Chemicals, Finland
            End If

            ' Jump to payment for all other than messagerecords;
            If Mid(sImportLineBuffer, 6, 1) <> "9" Then
                Exit Do
            End If
        Loop

        ReadLM_Domestic_Finland = sImportLineBuffer

    End Function
    'XNET - 10.02.2012 - Whole function
    Private Function ReadLUM_International_Finland() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim bFromLevelUnder As Boolean

        bReadLine = True

        Do While True

            Select Case Mid$(sImportLineBuffer, 5, 1)

                Case "1"  ' Betalningspost
                    If bFromLevelUnder Then
                        Exit Do
                    End If
                    If Len(Trim$(Mid(sImportLineBuffer, 334, 20))) > 0 Then
                        oFreeText = Freetexts.Add
                        oFreeText.Text = PadRight(Mid(sImportLineBuffer, 334, 20), 40, " ")
                    End If
                    If Len(Trim$(Mid(sImportLineBuffer, 369, 20))) > 0 Then
                        oFreeText = Freetexts.Add
                        oFreeText.Text = PadRight(Mid(sImportLineBuffer, 369, 20), 40, " ")
                    End If
                    If Len(Trim$(Mid(sImportLineBuffer, 404, 20))) > 0 Then
                        oFreeText = Freetexts.Add
                        oFreeText.Text = PadRight(Mid(sImportLineBuffer, 404, 20), 40, " ")
                    End If
                    If Len(Trim$(Mid(sImportLineBuffer, 439, 20))) > 0 Then
                        oFreeText = Freetexts.Add
                        oFreeText.Text = PadRight(Mid(sImportLineBuffer, 439, 20), 40, " ")
                    End If

                    ' Amount
                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 474, 15))

                Case "L"
                    bReadLine = True


                Case "2"
                    ' Programmed for one invoice only pr payment!

                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 12, 15))
                    If Mid$(sImportLineBuffer, 11, 1) = "2" Then
                        ' Creditnote
                        nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                    End If
                    sREF_Own = Trim$(Mid$(sImportLineBuffer, 81, 50))
                    bReadLine = True

                Case "9"
                    Exit Do

                    'All other content would indicate error
                Case Else
                    'Error # 10010-008
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-008")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadLUM_International_FinlandRecord(oFile) ', oProfile)
            End If
            'XNET 10.02.2012
            'Exit Do ' Jumpt to payment
            bFromLevelUnder = True
        Loop

        ReadLUM_International_Finland = sImportLineBuffer

    End Function
    Private Function ReadPeriodiskaBetalningarFinland() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sTypeOfMessage As String

        bReadLine = True

        Do
            Select Case Mid(sImportLineBuffer, 1, 1)

                Case "1" ' Transaction record
                    sREF_Own = Mid(sImportLineBuffer, 24, 9) '5) Betalarens kod

                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = PadRight(Trim(Mid(sImportLineBuffer, 65, 11)), 35, " ") '9) Mottagarens kod

                    nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 35, 11)) '7) Penningbelopp


                    'All other content would indicate error
                Case Else
                    'Error # 10010-008
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-008")

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadPeriodiskaBetalningarFinlandRecord(oFile)
            End If
            Exit Do

        Loop

        ReadPeriodiskaBetalningarFinland = sImportLineBuffer

    End Function
    Private Function ReadCodaFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim iOffset As Short

        ' 25.09.03 Find offset due to accountno shorter than 11 digits (re Navion)
        ' For iOffset = 0 To 10
        ' Changed 07.10.03, might be Letters in ToAccount!!!!
        For iOffset = 10 To 0 Step -1
            If InStr("FKU", Mid(sImportLineBuffer, 20 - iOffset, 1)) > 0 Then
                Exit For
            End If
        Next iOffset



        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 56 - iOffset, 16))

        bAmountSetInvoice = True
        sREF_Own = Mid(sImportLineBuffer, 72 - iOffset, 25)
        sSTATEBANK_Code = Mid(sImportLineBuffer, 432 - iOffset, 2)
        ' 05.12.2007 added for TravelRetailGroup, special = "TRN"
        If oCargo.Special = "TRN" Then
            ' standartekst to NorgesBank
            sSTATEBANK_Text = "Varekj�p i henhold til mottatte fakturaer "
        End If

        If Mid(sImportLineBuffer, 20 - iOffset, 1) = "K" Then
            sUnique_Id = Mid(sImportLineBuffer, 226 - iOffset, 25)
            If sUnique_Id = "" Then
                'FIX: Babelerror, No KID entered. Not critical
            End If
        ElseIf Mid(sImportLineBuffer, 20 - iOffset, 1) = "F" And Mid(sImportLineBuffer, 226 - iOffset, 1) = "*" Then
            ' KID given as * in first pos of freetext
            ' for IKEA, * (KID) comes as first sign in Kundenummerfield
            ' same for Reitan_Danmark, as FIK

            ' Added 28.09.06
            ' Problems for IKEA, in new version of Coda. Kundenummerfield has *XPINVMGR
            ' import Invoiceno as structured info Fakturanummer:374980, i pos 261
            'If Mid$(sImportLineBuffer, 261, 14) = "Fakturanummer:" Then
            '    sInvoiceNO = Trim$(Mid$(sImportLineBuffer, 275, 10))
            If vbIsNumeric(Trim(Mid(sImportLineBuffer, 227 - iOffset, 25)), "0123456789-") Then
                sUnique_Id = Mid(sImportLineBuffer, 227 - iOffset, 25) ' as pre 28.09.06

            Else
                ' New 17.11.06 Structured InvoiceNo;
                ' Put text after Fakturanummer: into Inoiceno !
                If Mid(sImportLineBuffer, 261 - iOffset, 14) = "Fakturanummer:" Then
                    sInvoiceNo = Trim(Mid(sImportLineBuffer, 275 - iOffset, 21))
                Else
                    ' create freetext
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Trim(Mid(sImportLineBuffer, 261, 30))
                End If
            End If

        ElseIf oCargo.Special = "TEEKAY" And Left(Trim(Replace(Mid(sImportLineBuffer, 226 - iOffset, 27), "Invno.:", "")), 1) = "+" Then
            ' New 19.01.05 -  TEEKAY sets + in first pos of freetext (after Invno.:) to mark KID
            sUnique_Id = Mid(Trim(Replace(Mid(sImportLineBuffer, 226 - iOffset, 35), "Invno.:", "")), 2)
        Else
            If oCargo.Special = "REITAN_SVERIGE" Then
                ' Special for Reitan SE to make it equal to old payX stuff
                ' Er vanligvis fra 230, men for OCR er fra 228 !!!!
                sREF_Own = Trim(Mid(sImportLineBuffer, 228, 25))
                oFreeText = oFreetexts.Add()
                If Mid(sImportLineBuffer, 228, 1) = "*" Then
                    ' OCR no given as * in first pos of freetext
                    ' In Sweden, treat as ordinary payment
                    oFreeText.Text = Trim(Mid(sImportLineBuffer, 229, 30))
                Else
                    ' 17.11.05 - Need only invoiceno to receiver
                    oFreeText.Text = Mid(sImportLineBuffer, 263, 30)
                    'oFreeText.Text = Left$(Trim$(Mid$(sImportLineBuffer, 263, 30)) & "/" & Trim$(Mid$(sImportLineBuffer, 230, 30)), 25)
                End If
            Else
                ' New 17.11.06 Structured InvoiceNo;
                ' Put text after Fakturanummer: into Inoiceno !
                If Mid(sImportLineBuffer, 261 - iOffset, 14) = "Fakturanummer:" Then
                    sInvoiceNo = Trim(Mid(sImportLineBuffer, 275 - iOffset, 21))
                    ' Shit - this format is also used for Reitan DK, which then need freetext!
                    ' 05.06.2007 Has then changed next line from else to endif
                End If
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sImportLineBuffer, 226 - iOffset, 35) & Space(5)
                If Trim(Mid(sImportLineBuffer, 261 - iOffset, 35)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 261 - iOffset, 35) & Space(5)
                End If
                ''''''''''End If
            End If
        End If


        'UPGRADE_WARNING: Couldn't resolve default property of object ReadCodaRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = ReadCodaRecord(oFile) ', oProfile)


        ReadCodaFile = sImportLineBuffer

    End Function
    Private Function ReadPGS() As String
        ' Special format for PGS - originally special payX-format

        Dim oFreeText As Freetext

        If oCargo.PayCode = "D" Then

            ' Domestic
            ' --------
            If Mid(sImportLineBuffer, 3, 1) = "*" And Not EmptyString(Mid(sImportLineBuffer, 4, 5)) Then
                ' KID when * in start of Melding
                sUnique_Id = Mid(sImportLineBuffer, 4, 29)
            Else
                ' added 21.11.06 Structured invoiceno;
                If Mid(sImportLineBuffer, 3, 8) = "FAKT.NR:" And oCargo.Special <> "PGS_SWEDEN" And oCargo.Special <> "PGSUK" And oCargo.Special <> "TECNL" And oCargo.Special <> "PGS_ARROW" And oCargo.Special <> "PGS_TBI_UK" Then
                    sInvoiceNo = Trim(Mid(sImportLineBuffer, 12, 20))
                ElseIf Mid(sImportLineBuffer, 33, 8) = "FAKT.NR:" And oCargo.Special <> "PGS_SWEDEN" And oCargo.Special <> "PGSUK" And oCargo.Special <> "PGS_TBI_UK" And oCargo.Special <> "TECNL" Then  ' XNET 07.10.2010 added PGS_TBI_UK
                    sInvoiceNo = Trim(Mid(sImportLineBuffer, 42, 20))
                Else
                    ' Melding til mottaker
                    If Trim(Mid(sImportLineBuffer, 3, 30)) <> "*" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 3, 30) & Space(10)
                    End If
                    If Len(Trim(Mid(sImportLineBuffer, 33, 30))) > 0 Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 33, 30) & Space(10)
                    End If
                    If Len(Trim(Mid(sImportLineBuffer, 63, 30))) > 0 Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 63, 30) & Space(10)
                    End If

                End If
            End If
            nMON_InvoiceAmount = Val(Replace(Mid(sImportLineBuffer, 98, 17), ".", ""))

        Else
            ' International
            ' -------------

            ' 05.08.2010
            ' PGS Houston has added 50 more chars to the notification. Don't now if it is used other places, so test!
            If Mid(sImportLineBuffer, 157, 1) = "." Then

                ' amount moved, pick up more text !
                If Trim(Mid(sImportLineBuffer, 3, 35)) <> "*" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 3, 35) & Space(10)
                End If
                If Len(Trim(Mid(sImportLineBuffer, 38, 35))) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 38, 35) & Space(10)
                End If
                If Len(Trim(Mid(sImportLineBuffer, 73, 35))) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 73, 35) & Space(10)
                End If
                If Len(Trim(Mid(sImportLineBuffer, 108, 35))) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 108, 35) & Space(10)
                End If

                nMON_InvoiceAmount = Val(Replace(Mid(sImportLineBuffer, 143, 17), ".", ""))

            Else
                ' as pre 05.08.2010

                ' Melding til mottaker
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sImportLineBuffer, 3, 30) & Space(10)

                nMON_InvoiceAmount = Val(Replace(Mid(sImportLineBuffer, 38, 17), ".", ""))
            End If
        End If


        bAmountSetInvoice = True

        ReadPGS = sImportLineBuffer

    End Function
    Private Function ReadISS_LonnsTrekk() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext

        ' 17.12.04 Changed postions for all fields
        nMON_InvoiceAmount = Val(Replace(Mid(sImportLineBuffer, 83, 9), "-", "")) * 100
        bAmountSetInvoice = True
        oFreeText = oFreetexts.Add()
        oFreeText.Text = Mid(sImportLineBuffer, 61, 20) & Space(20)
        oFreeText = oFreetexts.Add()
        oFreeText.Text = "F.nr. " & Mid(sImportLineBuffer, 38, 11) & Space(29)

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadISS_LonnsTrekkRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = ReadISS_LonnsTrekkRecord(oFile) ', oProfile)


        ReadISS_LonnsTrekk = sImportLineBuffer

    End Function
    Private Function ReadDnBNattsafe() As String
        'Dim oFreeText As FreeText
        'Dim bReadLine As Boolean
        'Dim bCreateObject As Boolean
        'Dim bFromLevelUnder As Boolean
        '
        '' Analyziz only
        'Dim sBranch As String
        'Dim sKK As String
        'Dim sTempDate As String
        'Dim dTempDate As Date
        'Dim dDATE_Value As Date
        'Dim bDateOK As Boolean
        ''----------
        'Do While True
        '
        '    If xDelim(sImportLineBuffer, ",", 1) = "TOT" Then
        '        'Delete this?  Never enters here
        '        If bFromLevelUnder Then
        '            ' Add to payment-amount, TOT only here!
        '            nMON_TransferredAmount = nMON_TransferredAmount + CDbl(Replace(xDelim(sImportLineBuffer, ",", 6), ".", ""))           'Registrert bel�p (st�r p� posen)
        '            nMON_AccountAmount = nMON_AccountAmount + CDbl(Replace(xDelim(sImportLineBuffer, ",", 7), ".", ""))          'Opptalt bel�p
        '            nMON_InvoiceAmount = nMON_InvoiceAmount + CDbl(Replace(xDelim(sImportLineBuffer, ",", 7), ".", ""))         'Opptalt bel�p
        '        End If
        '        ' Jump to payment
        '        bCreateObject = False
        '        bReadLine = False
        '        Exit Do
        '
        '
        '    Else
        '
        '        sREF_Bank = xDelim(sImportLineBuffer, ",", 2) ' Posenr
        '        sREF_Own = xDelim(sImportLineBuffer, ",", 10)  'KK_Felt
        '
        '
        '        bCreateObject = True  ' Create one Freetext for TP
        '        bReadLine = False
        '
        '
        '    End If
        '
        '    If bCreateObject Then
        '
        '        '----------------------------------------
        '        ' TESTING, Analyzing DnB KK-field;
        '        '----------------------------------------
        '
        '        sBranch = xDelim(sImportLineBuffer, ",", 4) ' Avdelingsnummer
        '        sKK = xDelim(sImportLineBuffer, ",", 10)
        '        sTempDate = xDelim(sImportLineBuffer, ",", 9) ' Valuteringsdato YYYYMMDD
        '        dDATE_Value = DateSerial(Mid$(sTempDate, 1, 4), Mid$(sTempDate, 5, 2), Mid$(sTempDate, 7, 2))
        '
        '        bDateOK = False
        '        ' Try to extract date from KK-field
        '        ' Date should be pos 6,4 as DDMM
        '        ' AvdNr in 4,2 ?
        '        If sBranch = LStrip(Mid$(sKK, 4, 2), "0") Then
        '            ' Check if Veksel:
        '            If Left$(sREF_Own, 2) = "99" Then
        '                sTempDate = Mid$(sREF_Own, 6, 2)
        '                ' Date as day in pos 6,2
        '                dTempDate = DateSerial(Year(Now), Month(Now), sTempDate)
        '                ' Check if we have jumped into a new year;
        '                If dTempDate > Now + 10 Then
        '                    ' Deduct 1 form year-part;
        '                    dTempDate = DateSerial(Year(Now) - 1, Month(Now), sTempDate)
        '                End If
        '                ' Check if we have jumped into a new month
        '                If dTempDate > Now + 10 Then
        '                    ' Deduct 1 form month-part;
        '                    If Month(Now) > 1 Then
        '                        dTempDate = DateSerial(Year(Now), Month(Now) - 1, Left$(sTempDate, 2))
        '                    End If
        '                End If
        '                ' Validate against ValueDate;
        '                If Not (dTempDate > dDATE_Value Or dTempDate < dDATE_Value - 5) Then
        '                    ' Date OK, use this one;
        '                    dDATE_Value = dTempDate
        '                    bDateOK = True
        '                End If
        '
        '            Else
        '                '------------------
        '                ' Ordinary Nattsafe
        '                '------------------
        '
        '                sTempDate = Mid$(sREF_Own, 6, 4)
        '                dTempDate = DateSerial(Year(Now), Right$(sTempDate, 2), Left$(sTempDate, 2))
        '                ' Check if we have jumped to next year (re first part of january
        '                If dTempDate > Now + 10 Then
        '                    ' Deduct 1 form year-part;
        '                    dTempDate = DateSerial(Year(Now) - 1, Right$(sTempDate, 2), Left$(sTempDate, 2))
        '                End If
        '                ' Validate against ValueDate;
        '                If Not (dTempDate > dDATE_Value Or dTempDate < dDATE_Value - 15) Then
        '                    ' Date OK, use this one;
        '                    dDATE_Value = dTempDate
        '                    bDateOK = True
        '                End If
        '
        '            End If
        '        Else
        '            bDateOK = False
        '        End If
        '
        '        '----------------------------------------
        '
        '        If CDbl(Replace(xDelim(sImportLineBuffer, ",", 7), ".", "")) <> 0 Then ' Not Amount = 0
        '
        '            If Not bDateOK Then
        '            '-------------------
        '            ' ANALYZIS ONLY!!!
        '            '-------------------
        '            Set oFreeText = oFreetexts.Add()
        '            oFreeText.Text = "Feil i KK-felt: " & sKK
        '            Set oFreeText = oFreetexts.Add()
        '            oFreeText.Text = "-----------------------------'"
        '            End If
        '       '-----------------------------------------------------
        '
        '
        '
        '            Set oFreeText = oFreetexts.Add()
        '            ' Input into text
        '            If xDelim(sImportLineBuffer, ",", 5) = "TP" Then
        '                oFreeText.Text = "--------" & _
        ''                xDelim(sImportLineBuffer, ",", 10) & "-" & _
        ''                xDelim(sImportLineBuffer, ",", 5) & "-" & _
        ''                xDelim(sImportLineBuffer, ",", 4) & "-" & _
        ''                xDelim(sImportLineBuffer, ",", 2) & "-" & _
        ''                PadLeft(VB6.Format(xDelim(sImportLineBuffer, ",", 7), "##,##0.00"), 12, " ")
        '                If xDelim(sImportLineBuffer, ",", 7) <> xDelim(sImportLineBuffer, ",", 6) Then
        '                    Set oFreeText = oFreetexts.Add
        '                    oFreeText.Text = "AVVIK i POSE: " &VB6.Format(Val(xDelim(sImportLineBuffer, ",", 7)) - Val(xDelim(sImportLineBuffer, ",", 6)), "##,##0.00")
        '                End If
        '            Else
        '                oFreeText.Text = _
        ''                xDelim(sImportLineBuffer, ",", 10) & "-" & _
        ''                xDelim(sImportLineBuffer, ",", 5) & "-" & _
        ''                xDelim(sImportLineBuffer, ",", 4) & "-" & _
        ''                xDelim(sImportLineBuffer, ",", 2) & "-" & _
        ''                PadLeft(VB6.Format(xDelim(sImportLineBuffer, ",", 7), "##,##0.00"), 12, " ")
        '            End If
        '        End If
        '        bReadLine = True
        '        bFromLevelUnder = True
        '
        '    End If
        '
        '
        '    If oFile.AtEndOfStream = True Then
        '        If sImportLineBuffer = "" Then
        '            Exit Do
        '        End If
        '    End If
        '
        '    If bReadLine Then
        '        sImportLineBuffer = ReadDnBNattsafeRecord(oFile)
        '        If xDelim(sImportLineBuffer, ",", 5) = "BK" Then
        '            Exit Do
        '        End If
        '    End If
        '    'Exit Do
        'Loop
        '
        'ReadDnBNattsafe = sImportLineBuffer

    End Function
    Private Function ReadSecuritasNattsafe() As String
        Dim oFreeText As Freetext

        ''''    nMON_TransferredAmount = nMON_TransferredAmount + ((CDbl(xDelim(sImportLineBuffer, ";", 8)) + CDbl(xDelim(sImportLineBuffer, ";", 9))) * 100)          '9 shows diffamount
        ''''    'nMON_TransferredAmount = nMON_TransferredAmount + CDbl(xDelim(sImportLineBuffer, ";", 9)) * 100          '9 shows diffamount
        ''''    nMON_InvoiceAmount = nMON_InvoiceAmount + CDbl(xDelim(sImportLineBuffer, ";", 8)) * 100          'Opptalt bel�p
        ''''    nMON_AccountAmount = nMON_AccountAmount + CDbl(xDelim(sImportLineBuffer, ";", 8)) * 100          'Opptalt bel�p


        ''''''        ' Detect if old Securitasformat or not:
        ''''''        ' Old: 12 times ;
        ''''''        ' New 13 times ;
        ''''''        If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) = 12 Then
        ''''''            ' Old format
        ''''''            Set oFreeText = oFreetexts.Add()
        ''''''            oFreeText.Text = "Gammelt Securitas filformat - intet avdelingsfelt!"
        ''''''            Set oFreeText = oFreetexts.Add()
        ''''''            oFreeText.Text = Right(xDelim(sImportLineBuffer, ";", 3), 4)
        ''''''            Set oFreeText = oFreetexts.Add()
        ''''''            oFreeText.Text = "--------------------------------------------------"
        ''''''        End If

        If CDbl(Replace(xDelim(sImportLineBuffer, ";", 9), ".", "")) <> 0 Then ' Not Amount = 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 3) & "-" & xDelim(sImportLineBuffer, ";", 6) & "-" & "Totalt pose " & ":" & PadLeft(VB6.Format(xDelim(sImportLineBuffer, ";", 9), "##,##0.00"), 12, " ")
        End If
        If CDbl(Replace(xDelim(sImportLineBuffer, ";", 11), ".", "")) <> 0 Then ' Not Amount = 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 3) & "-" & xDelim(sImportLineBuffer, ";", 6) & "-" & "Mynt            " & ":" & PadLeft(VB6.Format(xDelim(sImportLineBuffer, ";", 11), "##,##0.00"), 12, " ")
        End If
        If CDbl(Replace(xDelim(sImportLineBuffer, ";", 12), ".", "")) <> 0 Then ' Not Amount = 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 3) & "-" & xDelim(sImportLineBuffer, ";", 6) & "-" & "Sedler        " & ":" & PadLeft(VB6.Format(xDelim(sImportLineBuffer, ";", 12), "##,##0.00"), 12, " ")
        End If
        If CDbl(Replace(xDelim(sImportLineBuffer, ";", 13), ".", "")) <> 0 Then ' Not Amount = 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 3) & "-" & xDelim(sImportLineBuffer, ";", 6) & "-" & "Sjekker       " & ":" & PadLeft(VB6.Format(xDelim(sImportLineBuffer, ";", 13), "##,##0.00"), 12, " ")
        End If
        If CDbl(Replace(xDelim(sImportLineBuffer, ";", 14), ".", "")) <> 0 Then ' Not Amount = 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 3) & "-" & xDelim(sImportLineBuffer, ";", 6) & "-" & "Annet       " & ":" & PadLeft(VB6.Format(xDelim(sImportLineBuffer, ";", 14), "##,##0.00"), 12, " ")
        End If
        If CDbl(Replace(xDelim(sImportLineBuffer, ";", 10), ".", "")) <> 0 Then ' Not Amount = 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 3) & "-" & xDelim(sImportLineBuffer, ";", 6) & "-" & "Differanse  " & ":" & PadLeft(VB6.Format(xDelim(sImportLineBuffer, ";", 10), "##,##0.00"), 12, " ")
        End If

        ReadSecuritasNattsafe = sImportLineBuffer

    End Function

    Private Function ReadSRI_Access() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String

        nMON_InvoiceAmount = Val(Replace(xDelim(sImportLineBuffer, ";", 11), ",", ""))
        bAmountSetInvoice = True
        sMessage = xDelim(sImportLineBuffer, ";", 12, Chr(34))

        ' Notification to Norges Bank
        ' CHANGED 19.09.05
        ' 00 if amount <
        ' 70 if amount >=
        'If nMON_InvoiceAmount < 1250000 Then  ' usualy USD, aprox. 100 000 NOK
        '    sSTATEBANK_Code = "00"
        '    sSTATEBANK_Text = ""
        'Else
        '    sSTATEBANK_Code = "70"
        '    sSTATEBANK_Text = xDelim(sImportLineBuffer, ";", 12, Chr(34))
        'End If
        ' 19.09.05 - Now finds code and text at end of each line in importfile
        sSTATEBANK_Code = xDelim(sImportLineBuffer, ";", 20)
        sSTATEBANK_Text = Left(xDelim(sImportLineBuffer, ";", 21, Chr(34)), 60)

        oFreeText = oFreetexts.Add()
        oFreeText.Text = Left(PadRight(sMessage, 40, " "), 40)
        'UPGRADE_WARNING: Couldn't resolve default property of object ReadSRI_AccessRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = ReadSRI_AccessRecord(oFile) ', oProfile)


        ReadSRI_Access = sImportLineBuffer

    End Function

    Private Function ReadCompuware() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim sAmount As String

        'sAmount = Val(Replace(xDelim(sImportLineBuffer, ",", 2), ".", "")) / 100
        ' fix: bruk kjells namount=ConvertToAmount(samount)
        sAmount = ConvertToAmount(xDelim(sImportLineBuffer, ",", 2), "", ".,")
        If sAmount = "ERROR" Then
            sImportLineBuffer = "ERROR"
        Else
            nMON_InvoiceAmount = Val(sAmount)
            bAmountSetInvoice = True
            sMessage = xDelim(sImportLineBuffer, ",", 1)

            oFreeText = oFreetexts.Add()
            oFreeText.Text = Left(PadRight(sMessage, 40, " "), 40)
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadCompuwareRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadCompuwareRecord(oFile) ', oProfile)
        End If

        ReadCompuware = sImportLineBuffer

    End Function
    Private Function ReadSeaTruck() As String
        Dim oFreeText As Freetext
        Dim sMessage1 As String
        Dim sMessage2 As String
        Dim sAmount As String

        sAmount = ConvertToAmount(Replace(xDelim(sImportLineBuffer, ",", 27, Chr(34)), "$", ""), ",", ".")
        If sAmount = "ERROR" Then
            sImportLineBuffer = "ERROR"
        Else
            nMON_InvoiceAmount = Val(sAmount)
            bAmountSetInvoice = True
            sMessage1 = xDelim(sImportLineBuffer, ",", 2, Chr(34))
            sMessage2 = xDelim(sImportLineBuffer, ",", 24, Chr(34)) 'Invoiceno

            ' added 13.10.06, gives Structured invoiceNo!!
            sInvoiceNo = xDelim(sImportLineBuffer, ",", 24, Chr(34))

            If Len(sMessage1) > 0 Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(PadRight(sMessage1, 40, " "), 40)
            End If
            If Len(sMessage2) > 0 Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(PadRight(sMessage2, 40, " "), 40)
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object ReadSeaTruckRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadSeaTruckRecord(oFile)
        End If

        ReadSeaTruck = sImportLineBuffer

    End Function
    Private Function ReadNordeaUnitel() As String
        Dim oFreeText As Freetext
        Dim sAmount As String
        Dim lLine As Short

        sAmount = ConvertToAmount(Replace(xDelim(sImportLineBuffer, ",", 33, Chr(34)), "$", ""), ",", ".")
        If sAmount = "ERROR" Then
            sImportLineBuffer = "ERROR"
        Else
            nMON_InvoiceAmount = Val(sAmount)
            bAmountSetInvoice = True
            ' Meddeleseslinjer in 23-26,53,54-90
            For lLine = 23 To 26
                If Len(Trim(xDelim(sImportLineBuffer, ",", lLine, Chr(34)))) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = PadRight(Trim(xDelim(sImportLineBuffer, ",", lLine, Chr(34))), 40, " ")
                End If
            Next lLine

            For lLine = 53 To 90
                If Len(Trim(xDelim(sImportLineBuffer, ",", lLine, Chr(34)))) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = PadRight(Trim(xDelim(sImportLineBuffer, ",", lLine, Chr(34))), 40, " ")
                End If
            Next lLine

            sREF_Own = xDelim(sImportLineBuffer, ",", 38, Chr(34))
            ' New for salary;
            If EmptyString(sREF_Own) And oCargo.PayCode = "203" Then
                sREF_Own = "-"
            End If

            ' Check if FIK-payment
            'If Trim$(xDelim(sImportLineBuffer, ",", 45, Chr(34))) <> "" Then
            ' Changed 02.12.03, due to problems at MiniCrosser (DnB TBI)
            If Trim(xDelim(sImportLineBuffer, ",", 2, Chr(34))) = "5" Or Trim(xDelim(sImportLineBuffer, ",", 2, Chr(34))) = "44" Or Trim(xDelim(sImportLineBuffer, ",", 2, Chr(34))) = "46" Then
                ' FIK, put toghether 45 and 46
                sUnique_Id = Trim(xDelim(sImportLineBuffer, ",", 45, Chr(34))) & Trim(xDelim(sImportLineBuffer, ",", 46, Chr(34)))
            End If

            ' To statebank;
            sSTATEBANK_Code = Trim(xDelim(sImportLineBuffer, ",", 41, Chr(34)))
            sSTATEBANK_Text = Trim(xDelim(sImportLineBuffer, ",", 42, Chr(34)))
            sSTATEBANK_DATE = Trim(xDelim(sImportLineBuffer, ",", 35, Chr(34)))

            sImportLineBuffer = ReadNordeaUnitelRecord(oFile)
        End If

        ReadNordeaUnitel = sImportLineBuffer

    End Function
    Private Function ReadShipNet() As String
        Dim oFreeText As Freetext
        Dim sAmount As String
        Dim sText As String

        sAmount = ConvertToAmount(Replace(xDelim(sImportLineBuffer, ";", 47), "$", ""), "", ",")
        If sAmount = "ERROR" Then
            sImportLineBuffer = "ERROR"
        Else
            nMON_InvoiceAmount = Val(sAmount)
            bAmountSetInvoice = True
            If xDelim(sImportLineBuffer, ";", 49) <> "" Then
                sAmount = ConvertToAmount(Replace(xDelim(sImportLineBuffer, ";", 49), "$", ""), "", ",")
                If sAmount = "ERROR" Then
                    nMON_TransferredAmount = CDbl(sAmount)
                End If
            End If

            ' Melding til mottaker inntil 255 tegn, del opp i 40 og 40
            sText = xDelim(sImportLineBuffer, ";", 66)
            Do While Len(sText) > 0
                If Len(sText) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Left(sText, 40)
                End If
                sText = Mid(sText, 41)
            Loop

            sREF_Own = xDelim(sImportLineBuffer, ";", 7)
            sInvoiceNo = xDelim(sImportLineBuffer, ";", 63)
            sCustomerNo = xDelim(sImportLineBuffer, ";", 65)

            ' Check if KID-payment
            sUnique_Id = Trim(xDelim(sImportLineBuffer, ";", 36))

            ' To statebank;
            sSTATEBANK_Code = Trim(xDelim(sImportLineBuffer, ";", 41))
            sSTATEBANK_Text = Trim(xDelim(sImportLineBuffer, ";", 42))
            sStatusCode = "00" ' Innsending av betalinger

            sImportLineBuffer = ReadShipNetRecord(oFile)
        End If

        ReadShipNet = sImportLineBuffer

    End Function

    Private Function ReadCitiDirectUSFlatFile() As String
        Dim oFreeText As Freetext
        Dim sText As String


        ' Melding til mottaker inntil 4 x 35
        sText = xDelim(sImportLineBuffer, "#", 72)
        If Len(sText) > 0 Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = sText
        End If
        sText = xDelim(sImportLineBuffer, "#", 73)
        If Len(sText) > 0 Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = sText
            sText = xDelim(sImportLineBuffer, "#", 74)
            If Len(sText) > 0 Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = sText
                sText = xDelim(sImportLineBuffer, "#", 75)
                If Len(sText) > 0 Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = sText
                End If
            End If
        End If

        ReadCitiDirectUSFlatFile = sImportLineBuffer

    End Function

    'Private Function ReadBACSforTBI() As String
    'Dim oFreeText As Freetext
    'Dim sMessage As String
    '
    'sMessage = xDelim(sImportLineBuffer, ",", 4, Chr(34))
    '
    'If Len(sMessage) > 0 Then
    '    Set oFreeText = oFreetexts.Add()
    '    oFreeText.Text = Left$(PadRight(sMessage, 40, " "), 40)
    'End If
    '
    'ReadBACSforTBI = sImportLineBuffer
    '
    'End Function

    Private Function ReadEDIFile(ByRef oPRC As MSXML2.IXMLDOMNode, ByRef oEDIMapping As MSXML2.IXMLDOMElement, ByRef sTypeOfPayment As String) As Boolean
        Dim oFreeText As Freetext
        'Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        'Variables used to set properties in the payment-object
        'Dim sI_Account As String
        Dim oFTX As MSXML2.IXMLDOMNode
        Dim oFTXList As MSXML2.IXMLDOMNodeList
        Dim l As Integer
        Dim bReturnValue As Boolean
        Dim sText, sTextCopy As String
        Dim nTempAmount As Double
        Dim iTypeofSplitting As Short
        Dim sSign As String
        Dim sStringAmount As String
        ' 1 = 1 line in the EDI-file equals 1 line in Freetext
        ' 2 = 1 line in the EDI-file is splitted like this
        '     1. Freetext = First 40 posistions in element 1
        '     2. Freetext = Last 30 in element 1 + first 10 in element 2
        Dim bBankBBSButCreatedByFellesdata As Boolean
        Dim sAdditionalStructuredInfo, sAdditionalStructuredInfoQualifier As String

        Dim iStringLength As Short
        Dim iPosDebetNota, iPosKreditNota As Short
        Dim iPosNextInvoice As Short

        Dim bStructured As Boolean

        bBankBBSButCreatedByFellesdata = False
        iTypeofSplitting = 1
        bReturnValue = False
        'sStatusCode = "02"



        Dim oTemp As MSXML2.IXMLDOMNode
        Select Case iImportFormat

            Case BabelFiles.FileType.CREMUL

                'If 1 = 2 Then

                'Else
                Select Case oCargo.Bank

                    Case BabelFiles.Bank.DnBNOR_INPS



                        bStructured = False
                        'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)

                        If EmptyString(sSign) Then
                            oTemp = oPRC.selectSingleNode("GROUP[@group = ' 21']")
                            If Not oTemp Is Nothing Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object oTemp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sSign = GetPropValue("invoice/mon_signinvoiceamount", oTemp, oEDIMapping)
                                If Not EmptyString(sSign) Then
                                    'Change oPRC
                                    'UPGRADE_NOTE: Object oTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                    oTemp = Nothing
                                    oPRC = oPRC.selectSingleNode("GROUP[@group = ' 21']")
                                Else
                                    'UPGRADE_NOTE: Object oTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                    oTemp = Nothing
                                End If
                            End If
                        End If

                        'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nTempAmount = Val(Replace(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping), ".", ","))
                        Select Case sSign
                            Case "380"
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                bStructured = True

                            Case "381"
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                                nTempAmount = nTempAmount * -1
                                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                bStructured = True

                            Case "998"
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                nTempAmount = nTempAmount * -1
                                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                bStructured = True

                            Case "999"
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                bStructured = True

                            Case "YW3"
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                bStructured = True

                            Case Else
                                sUnique_Id = sUnique_Id

                        End Select

                        If bStructured Then
                            oFTXList = oPRC.selectNodes("FTX")
                            If oFTXList.length = 0 Then
                                'Try to find a text at the level above
                                oFTXList = oPRC.parentNode.selectNodes("FTX")
                            End If
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                            'Set oFTXList = oPRC.selectNodes("GROUP/FTX") 'Removed for DKK 08.05.2009
                            'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                            oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX") 'Added 08.05.2009
                        End If

                    Case BabelFiles.Bank.DnB, BabelFiles.Bank.Nordea_NO, BabelFiles.Bank.Fellesdata, BabelFiles.Bank.BBS, BabelFiles.Bank.Danske_Bank_NO, BabelFiles.Bank.SEB_NO, BabelFiles.Bank.Nordea_eGateway, BabelFiles.Bank.Svenska_Handelsbanken, BabelFiles.Bank.Danske_Bank_dk
                        If sTypeOfPayment = "STRUCTURED" Then

                            'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                            'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            nTempAmount = Val(Replace(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping), ".", ","))
                            Select Case sSign
                                Case "380"
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                                    sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sInvoiceNo = Nothing Then
                                        sInvoiceNo = ""
                                    End If

                                Case "381"
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                                    nTempAmount = nTempAmount * -1
                                    sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sInvoiceNo = Nothing Then
                                        sInvoiceNo = ""
                                    End If

                                Case "998"
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                    nTempAmount = nTempAmount * -1
                                    sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sUnique_Id = Nothing Then
                                        sUnique_Id = ""
                                    End If

                                Case "999"
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                    '14.10.2009 - Added trim
                                    sUnique_Id = Trim(GetPropValue("invoice/unique_id", oPRC, oEDIMapping))
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sUnique_Id = Nothing Then
                                        sUnique_Id = ""
                                    End If

                                Case "YW3"
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                    sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sUnique_Id = Nothing Then
                                        sUnique_Id = ""
                                    End If

                                Case Else
                                    sUnique_Id = sUnique_Id

                            End Select
                            oFTXList = oPRC.selectNodes("FTX")

                        ElseIf sTypeOfPayment = "UNSTRUCTURED" Then
                            nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                            oFTXList = oPRC.selectNodes("GROUP/FTX")
                            sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                            '24.08.2015 - Added next IF to avoid nothing as a value
                            If sUnique_Id = Nothing Then
                                sUnique_Id = ""
                            End If

                        Else 'sTypeOfPayment = "UNSTRUCTURED20" Then
                            nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                            sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                            '24.08.2015 - Added next IF to avoid nothing as a value
                            If sUnique_Id = Nothing Then
                                sUnique_Id = ""
                            End If
                            oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX")


                        End If


                    Case BabelFiles.Bank.DnBNOR_SE
                        sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                        nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))

                        'NYCREMUL
                        Select Case sSign
                            Case "380"
                                If oCargo.PayCode = "510" Then
                                    sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sUnique_Id = Nothing Then
                                        sUnique_Id = ""
                                    End If
                                    If Not EmptyString(sUnique_Id) Then
                                        sSign = "999"
                                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                    Else
                                        oFTXList = oPRC.selectNodes("FTX")
                                    End If
                                Else
                                    oFTXList = oPRC.selectNodes("FTX")
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                                    sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sInvoiceNo = Nothing Then
                                        sInvoiceNo = ""
                                    End If
                                End If

                            Case "381"
                                nTempAmount = nTempAmount * -1
                                If oCargo.PayCode = "510" Then
                                    sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sUnique_Id = Nothing Then
                                        sUnique_Id = ""
                                    End If
                                    If Not EmptyString(sUnique_Id) Then
                                        sSign = "998"
                                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                    Else
                                        oFTXList = oPRC.selectNodes("FTX")
                                    End If
                                Else
                                    oFTXList = oPRC.selectNodes("FTX")
                                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                                    sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                    '24.08.2015 - Added next IF to avoid nothing as a value
                                    If sInvoiceNo = Nothing Then
                                        sInvoiceNo = ""
                                    End If
                                End If

                            Case "998"
                                nTempAmount = nTempAmount * -1
                                'Set oFTXList = oPRC.selectNodes("FTX")
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                '24.08.2015 - Added next IF to avoid nothing as a value
                                If sUnique_Id = Nothing Then
                                    sUnique_Id = ""
                                End If

                            Case "999"
                                'Set oFTXList = oPRC.selectNodes("FTX")
                                eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                '24.08.2015 - Added next IF to avoid nothing as a value
                                If sUnique_Id = Nothing Then
                                    sUnique_Id = ""
                                End If

                            Case Else
                                'Normal situation, as before the new CREMUL-format version 2.11
                                oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX")
                                'NOR-Cargo!!!!
                                If oFTXList.length = 0 Then
                                    oFTXList = oPRC.selectNodes("GROUP/GROUP/FTX")
                                    If oFTXList.length = 0 Then 'New situation, if the file is originally created by Fellesdata.
                                        oFTXList = oPRC.selectNodes("GROUP/FTX")
                                        bBankBBSButCreatedByFellesdata = True
                                    End If
                                End If
                                'Set oFTXList = oPRC.selectSingleNode("GROUP[@group = ' 20']/FTX")
                                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                                '24.08.2015 - Added next IF to avoid nothing as a value
                                If sUnique_Id = Nothing Then
                                    sUnique_Id = ""
                                End If

                        End Select

                        '    Case Nordea_NO
                        '        sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                        '        nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                        '        Select Case sSign
                        '            Case "380"
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "381"
                        '                nTempAmount = nTempAmount * -1
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "998"
                        '                nTempAmount = nTempAmount * -1
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "999"
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case Else
                        '
                        '        End Select
                        '
                        '        Set oFTXList = oPRC.selectNodes("FTX")
                        '

                        '    Case Fellesdata
                        '        If oCargo.PayCode <> "629" Then
                        '            nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                        '            Set oFTXList = oPRC.selectNodes("GROUP/FTX")
                        '            sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '        Else
                        '            sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                        '            nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                        '            Select Case sSign
                        '                Case "380"
                        '                    'Set oFTXList = oPRC.selectNodes("FTX")
                        '                    sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '                Case "381"
                        '                    nTempAmount = nTempAmount * -1
                        '                    'Set oFTXList = oPRC.selectNodes("FTX")
                        '                    sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '                Case "998"
                        '                    nTempAmount = nTempAmount * -1
                        '                    'Set oFTXList = oPRC.selectNodes("FTX")
                        '                    sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '                Case "999"
                        '                    'Set oFTXList = oPRC.selectNodes("FTX")
                        '                    sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '                Case Else
                        '
                        '            End Select
                        '        End If

                        '    Case Danske_Bank_NO
                        '        nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                        '        sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                        '        Select Case sSign
                        '            Case "380"
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "381"
                        '                nTempAmount = nTempAmount * -1
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "998"
                        '                nTempAmount = nTempAmount * -1
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "999"
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case Else
                        '
                        '        End Select
                        '        sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '        Set oFTXList = oPRC.selectNodes("FTX")
                        '        Set oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX")

                    Case Else 'BBS
                        'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))
                        '
                        '        'NYCREMUL
                        '        sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                        '        Select Case sSign
                        '            Case "380"
                        '                Set oFTXList = oPRC.selectNodes("FTX")
                        '                sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "381"
                        '                Set oFTXList = oPRC.selectNodes("FTX")
                        '                nTempAmount = nTempAmount * -1
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sInvoiceNO = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case "998"
                        '                nTempAmount = nTempAmount * -1
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '    '            If Len(sUnique_Id) > 0 Then
                        '    '                Set oFreeText = oFreetexts.Add()
                        '    '                oFreeText.Text = "KID: " & sUnique_Id
                        '    '            End If
                        '
                        '            Case "999"
                        '                'Set oFTXList = oPRC.selectNodes("FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '            Case Else
                        '                'Normal situation, as before the new CREMUL-format version 2.11
                        '                Set oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX")
                        '                'NOR-Cargo!!!!
                        '                If oFTXList.length = 0 Then
                        '                    Set oFTXList = oPRC.selectNodes("GROUP/GROUP/FTX")
                        '                    If oFTXList.length = 0 Then 'New situation, if the file is originally created by Fellesdata.
                        '                        Set oFTXList = oPRC.selectNodes("GROUP/FTX")
                        '                        bBankBBSButCreatedByFellesdata = True
                        '                    End If
                        '                End If
                        '                'Set oFTXList = oPRC.selectSingleNode("GROUP[@group = ' 20']/FTX")
                        '                sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '
                        '        End Select

                        'End If

                        'Changed by Kjell 12/2-2004
                        ''''''Set oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX")
                        '''''''NOR-Cargo!!!!
                        ''''''If oFTXList.length = 0 Then
                        ''''''    Set oFTXList = oPRC.selectNodes("GROUP/GROUP/FTX")
                        ''''''End If
                        '''''''Set oFTXList = oPRC.selectSingleNode("GROUP[@group = ' 20']/FTX")
                        ''''''sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        '''''''    If Not oFTXTempList Is Nothing Then
                        '''''''        Set oFTXList = oFTXTempList.Item(1)
                        '''''''    End If

                End Select

                If Len(sInvoiceNo) > 40 Then
                    sInvoiceNo = Left(sInvoiceNo, 40)
                End If


            Case BabelFiles.FileType.PAYMUL
                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                nTempAmount = Val(GetPropValue("invoice/mon_invoiceamount", oPRC, oEDIMapping))

                'NYPAYMUL
                'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sSign = GetPropValue("invoice/mon_signinvoiceamount", oPRC, oEDIMapping)
                Select Case sSign
                    Case "380"
                        oFTXList = oPRC.selectNodes("FTX")
                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                        sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)

                    Case "381"
                        oFTXList = oPRC.selectNodes("FTX")
                        nTempAmount = nTempAmount * -1
                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                        sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)

                    Case "998"
                        nTempAmount = nTempAmount * -1
                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                        sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)

                    Case "999"
                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                        sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)

                    Case "YW3"
                        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                        sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)

                    Case Else
                        'Normal situation, as before the new CREMUL-format version 2.11
                        oFTXList = oPRC.selectNodes("GROUP[@group = ' 16']/FTX")
                        If oFTXList.length = 0 Then
                            oFTXList = oPRC.selectNodes("GROUP/GROUP/FTX")
                        End If
                        sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)

                End Select


        End Select

        If nTempAmount <> 0 Then
            nMON_InvoiceAmount = nTempAmount
            bAmountSetInvoice = True
        End If
        'FIX: The next two are not tested, and they should indeed be!
        'sSTATEBANK_Code = GetPropValue("invoice/statebankcode", oPRC, oEDIMapping)
        'sSTATEBANK_Text = GetPropValue("invoice/statebanktext", oPRC, oEDIMapping)


        'New code to add freetext to the structured payment

        'Next IF added 06.08.2009 - may influence users who import structured info other than in the CREMUL-format
        'KI can't think of any doing that, therefore .....
        If iImportFormat = BabelFiles.FileType.CREMUL Then
            Select Case sSign
                Case "380"
                    sStringAmount = ""
                    '02.04.2009 Set above when retrieving structured info
                    If oCargo.Bank <> BabelFiles.Bank.DnBNOR_INPS Then
                        oFTXList = oPRC.selectNodes("FTX")
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                    '26.05.2009 - Changed for all structured payments 380, 381, 998 etc. Added a thousandsep.
                    'When we didn't use a thousandsep the invoicesearch sometimes will find the amount as a possible invoice
                    sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                    'Old code
                    '    sStringAmount = Trim$(Str(nMON_InvoiceAmount))
                    '    If Len(sStringAmount) = 1 Then
                    '        sStringAmount = "00" & sStringAmount
                    '    ElseIf Len(sStringAmount) = 2 Then
                    '        sStringAmount = "0" & sStringAmount
                    '    End If
                    '    sStringAmount = PadLeft(Left$(sStringAmount, Len(sStringAmount) - 2), 15, " ") & "." & Right$(sStringAmount, 2)
                    'End old code
                    If Len(sInvoiceNo) > 0 Then
                        oFreeText = oFreetexts.Add()
                        If Len(sInvoiceNo) < 33 Then
                            oFreeText.Text = "Faktnr: " & sInvoiceNo
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                        Else
                            oFreeText.Text = "Faktnr: " & Left(sInvoiceNo, 32)
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sInvoiceNo, 33)
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                        End If
                    Else
                        'No invoiceinfo, add amount as text
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sAdditionalStructuredInfo = GetPropValue("invoice/rff_invoiceref", oPRC, oEDIMapping)
                    If Not EmptyString(sAdditionalStructuredInfo) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sAdditionalStructuredInfoQualifier = GetPropValue("invoice/rff_invoicerefqualifier", oPRC, oEDIMapping)
                        Select Case sAdditionalStructuredInfoQualifier
                            '
                            Case "AAK"
                                'AAK - Pakkseddelnummer (Despatch Advice Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60153) & sAdditionalStructuredInfo

                            Case "AEL"
                                'AEL - Levereringsnummer (Delivery Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60154) & sAdditionalStructuredInfo

                            Case "CO"
                                'CO - Kj�pers ordrenummer (Buyer's Order Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60155) & sAdditionalStructuredInfo

                            Case "CT"
                                'CT - Kontraksnummer (Contract Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60156) & sAdditionalStructuredInfo

                            Case "IT"
                                'IT - Kundenummer (Internal Customer Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60157) & sAdditionalStructuredInfo

                            Case "IV"
                                'IV - Fakturanummer (Invoice Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60158) & sAdditionalStructuredInfo

                        End Select
                    End If

                Case "381"
                    sStringAmount = ""
                    'Changed 22.09.2004 The amount is already multiplied with -1. Why do it again?
                    'nTempAmount = nTempAmount * -1
                    'Set oFTXList = oPRC.selectNodes("FTX")
                    'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sInvoiceNo = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                    sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                    '    sStringAmount = Trim$(Str(nMON_InvoiceAmount))
                    '    If Len(sStringAmount) = 1 Then
                    '        sStringAmount = "00" & sStringAmount
                    '    ElseIf Len(sStringAmount) = 2 Then
                    '        sStringAmount = "0" & sStringAmount
                    '    End If
                    '    sStringAmount = PadLeft(Left$(sStringAmount, Len(sStringAmount) - 2), 15, " ") & "." & Right$(sStringAmount, 2)
                    If Len(sInvoiceNo) > 0 Then
                        oFreeText = oFreetexts.Add()
                        If Len(sInvoiceNo) < 33 Then
                            oFreeText.Text = "Faktnr: " & sInvoiceNo
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                        Else
                            oFreeText.Text = "Faktnr: " & Left(sInvoiceNo, 32)
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sInvoiceNo, 33)
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                        End If
                    Else
                        'No invoiceinfo, add amount as text
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sAdditionalStructuredInfo = GetPropValue("invoice/rff_invoiceref", oPRC, oEDIMapping)
                    If Not EmptyString(sAdditionalStructuredInfo) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sAdditionalStructuredInfoQualifier = GetPropValue("invoice/rff_invoicerefqualifier", oPRC, oEDIMapping)
                        Select Case sAdditionalStructuredInfoQualifier
                            '
                            Case "AAK"
                                'AAK - Pakkseddelnummer (Despatch Advice Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60153) & sAdditionalStructuredInfo

                            Case "AEL"
                                'AEL - Levereringsnummer (Delivery Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60154) & sAdditionalStructuredInfo

                            Case "CO"
                                'CO - Kj�pers ordrenummer (Buyer's Order Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60155) & sAdditionalStructuredInfo

                            Case "CT"
                                'CT - Kontraksnummer (Contract Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60156) & sAdditionalStructuredInfo

                            Case "IT"
                                'IT - Kundenummer (Internal Customer Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60157) & sAdditionalStructuredInfo

                            Case "IV"
                                'IV - Fakturanummer (Invoice Number)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = LRS(60158) & sAdditionalStructuredInfo

                        End Select
                    End If

                Case "998", "999", "YW3"
                    If oCargo.PayCode = "629" Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object oPRC. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sUnique_Id = GetPropValue("invoice/unique_id", oPRC, oEDIMapping)
                        sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                        '        sStringAmount = Trim$(Str(nMON_InvoiceAmount))
                        '        If Len(sStringAmount) = 1 Then
                        '            sStringAmount = "00" & sStringAmount
                        '        ElseIf Len(sStringAmount) = 2 Then
                        '            sStringAmount = "0" & sStringAmount
                        '        End If
                        '        sStringAmount = PadLeft(Left$(sStringAmount, Len(sStringAmount) - 2), 15, " ") & "." & Right$(sStringAmount, 2)
                        If Not EmptyString(sUnique_Id) Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = "KID: " & sUnique_Id
                        End If
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = LRS(60060) & ":  " & sStringAmount & " / "
                    End If

            End Select
        End If
        'Set oFTXList = oPRC.selectNodes("FTX")

        'If oFTXList.length = 0 Then
        '    'Set oFTXList = oPRC.selectNodes("GROUP[@group = ' 20']/FTX")
        '    Set oFTXList = oPRC.selectNodes("GROUP/FTX")
        '    'Set oFTX = oPRC.selectSingleNode("GROUP/FTX")
        'End If


        If Not oFTXList Is Nothing Then
            For l = 0 To oFTXList.length - 1
                'If BBS just traverse throug the first object in the DOM-list

                'New 15.12.2005 added Nordea_No in the following IF-statement
                '  due to double texts for Nor-Cargo

                '09.02.2006 - Changed next IF-statement
                'FROM:
                'If (oCargo.Bank = BBS And Not bBankBBSButCreatedByFellesdata) Or oCargo.Bank = Danske_Bank_NO Or oCargo.Bank = Nordea_NO Then
                '    l = oFTXList.length - 1
                'End If

                'TO:
                'If oCargo.Special = "OK" Or oCargo.PayCode = "629" Then

                'Else
                'l = oFTXList.length - 1
                'Else

                'End If
                oFTX = oFTXList.item(l)
                If Not oFTX Is Nothing Then
                    'sText = GetPropValue("freetext/text", oFTXList.Item(l).parentNode, oEDIMapping)
                    'UPGRADE_WARNING: Couldn't resolve default property of object oFTX.parentNode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sText = GetPropValue("freetext/text", oFTX.parentNode, oEDIMapping)
                    sTextCopy = sText
                    'Chec first if we have a specific pattern for Nordea (and maybe DnB)
                    '            If Left$(sText, 9) = "DEBETNOTA" Then
                    '                If Mid$(sText, 24, 1) = "," Then
                    '                    If Mid$(sText, 27, 1) = " " Then
                    '                        iTypeofSplitting = 3
                    '                    End If
                    '                End If
                    '            ElseIf Left$(sText, 10) = "KREDITNOTA" Then
                    '                If Mid$(sText, 24, 1) = "," Then
                    '                    If Mid$(sText, 27, 1) = "-" Then
                    '                        iTypeofSplitting = 3
                    '                    End If
                    '                End If
                    '            End If
                    If Trim(Mid(sText, 41, 30)) <> "" And iTypeofSplitting <> 3 Then
                        iTypeofSplitting = 2
                    End If
                    If iTypeofSplitting = 1 Then
                        Do While Len(sText) > 0
                            oFreeText = oFreetexts.Add()
                            If Len(sText) > 40 Then
                                oFreeText.Text = Left(sText, 40)
                                If Trim(Mid(sText, 41, 30)) <> "" Then
                                    iTypeofSplitting = 2
                                    'l = 0
                                    sText = sTextCopy
                                    'UPGRADE_NOTE: Object oFreetexts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                    oFreetexts = Nothing
                                    oFreetexts = New Freetexts
                                    Exit Do
                                End If
                                sText = Right(sText, Len(sText) - 70)
                            Else
                                oFreeText.Text = Mid(sText, 1, Len(sText))
                                sText = ""
                            End If
                        Loop

                        'New 15.12.2005
                        'Old code
                        ''End If
                        'ElseIf iTypeofSplitting = 2 Then

                        'New code
                    End If
                    If iTypeofSplitting = 2 Then

                        Do While Len(sText) > 0
                            oFreeText = oFreetexts.Add()
                            If Len(sText) > 40 Then
                                oFreeText.Text = Left(sText, 40)
                                sText = Right(sText, Len(sText) - 40)
                                oFreeText = oFreetexts.Add()
                                oFreeText.Text = Left(sText, 30)
                                sText = Right(sText, Len(sText) - 30)
                            Else
                                oFreeText.Text = Mid(sText, 1, Len(sText))
                                sText = ""
                            End If
                        Loop
                        '            ElseIf iTypeofSplitting = 3 Then
                        '                Do While Len(Trim$(sText)) > 0
                        '                    Set oFreeText = oFreetexts.Add()
                        '                    If Left$(sText, 9) = "DEBETNOTA" Then
                        '                        iStringLength = 29
                        '                    ElseIf Left$(sText, 10) = "KREDITNOTA" Then
                        '                        iStringLength = 29
                        '                    Else
                        '                        iPosDebetNota = InStr(1, sText, "DEBETNOTA", vbTextCompare)
                        '                        iPosKreditNota = InStr(1, sText, "KREDITNOTA", vbTextCompare)
                        '                        If iPosDebetNota > 0 Then
                        '                            If iPosKreditNota > 0 Then
                        '                                If iPosDebetNota > iPosKreditNota Then
                        '                                    iPosNextInvoice = iPosDebetNota
                        '                                Else
                        '                                    iPosNextInvoice = iPosKreditNota
                        '                                End If
                        '                            Else
                        '                                iPosNextInvoice = iPosDebetNota
                        '                            End If
                        '                        Else
                        '                            If iPosKreditNota > 0 Then
                        '                                iPosNextInvoice = iPosKreditNota
                        '                            Else
                        '                                iPosNextInvoice = 0
                        '                            End If
                        '                        End If
                        '                        If iPosNextInvoice > 40 Then
                        '                            iStringLength = 40
                        '                        Else
                        '                            If iPosNextInvoice = 0 Then
                        '                                If Len(sText) > 40 Then
                        '                                    iStringLength = 40
                        '                                Else
                        '                                    iStringLength = Len(sText)
                        '                                End If
                        '                            Else
                        '                                iStringLength = iPosNextInvoice - 1
                        '                            End If
                        '                        End If
                        '                    End If
                        '                    If Len(sText) > iStringLength Then
                        '                        oFreeText.Text = Left$(sText, iStringLength)
                        '                        sText = Mid$(sText, iStringLength + 1)
                        '                    Else
                        '                        oFreeText.Text = Mid(sText, 1, Len(sText))
                        '                        sText = ""
                        '                    End If
                        '                Loop
                    End If
                End If
                If sTypeOfPayment = "UNSTRUCTURED20" Then
                    Exit For 'Just traverse through once
                ElseIf oCargo.Special = "VISMA_FOKUS" Then
                    Exit For
                End If
            Next l
        End If

        'Special case where we have freetext to the bank statement. We add it to the
        ' regular freetext with a spesific qualificator.
        Do While Len(sCargoFTX) > 0
            oFreeText = oFreetexts.Add()
            oFreeText.Qualifier = CShort("2")
            If Len(sCargoFTX) > 40 Then
                oFreeText.Text = Left(sCargoFTX, 40)
                sCargoFTX = Right(sCargoFTX, Len(sCargoFTX) - 40)
            Else
                oFreeText.Text = Mid(sCargoFTX, 1, Len(sCargoFTX))
                sCargoFTX = ""
            End If
        Loop

        'If Len(Trim$(sUnique_Id)) = 25 Then
        '    sCargoFTX = "KOKO"
        'End If

        ReadEDIFile = bReturnValue

    End Function
    Friend Function ReadPain001(ByRef nodInvoice As System.Xml.XmlNode, ByRef nsMgr As System.Xml.XmlNamespaceManager, ByRef sFileNameIn As String, ByVal sBatchCounter As String, ByVal sPaymentInfo As String) As Boolean
        Dim nodeList As System.Xml.XmlNodeList
        Dim nodeList2 As System.Xml.XmlNodeList
        Dim nodeTemp As System.Xml.XmlNode
        Dim node As System.Xml.XmlNode
        Dim oFreetext As vbBabel.Freetext = Nothing
        Dim sTmp As String = ""
        Dim sTmp2 As String = ""
        Dim nTmp As Double = 0
        Dim bReturnValue As Boolean = True
        Dim iStartIndex As Integer
        Dim iStartPos As Integer
        Dim iCount As Integer
        Dim iPosCounter As Integer
        Dim iLength As Integer


        Try

            nodeList = nodInvoice.SelectNodes("ISO:Ustrd", nsMgr)
            If Not nodInvoice Is Nothing Then
                ' Forel�pig KUN Ustrd
                For Each nodeTemp In nodInvoice
                    sTmp = nodeTemp.InnerText
                    Do While Len(sTmp) > 0
                        oFreetext = oFreetexts.Add()
                        If Len(sTmp) > 40 Then
                            oFreetext.Text = Left(sTmp, 40)
                            sTmp = Mid(sTmp, 41)
                        Else
                            oFreetext.Text = Mid(sTmp, 1, Len(sTmp))
                            sTmp = ""
                        End If
                    Loop
                Next nodeTemp
            End If


            ''Search the string for SCOR, signifies reference-payment (my guess)
            'iStartIndex = 0
            'iCount = 0
            'iPosCounter = 0
            'iLength = sTmp.Length
            'iStartPos = sTmp.IndexOf("SCOR", iStartIndex, StringComparison.CurrentCulture)
            'If iStartPos > 0 Then
            '    If sTmp.IndexOf("SCOR", iStartPos + 5, StringComparison.CurrentCulture) = -1 Then
            '        iStartPos = iStartPos + 5 'This should be the first position of the reference, after SCOR <Space>
            '        Do While True
            '            'Check if we have reached the end of the text
            '            If iLength > iStartPos + iPosCounter Then
            '                If vbIsNumeric(sTmp.Substring(iStartPos + iPosCounter, 1), "0123456789") Then
            '                    iPosCounter = iPosCounter + 1
            '                Else
            '                    Exit Do
            '                End If
            '            Else
            '                Exit Do
            '            End If
            '        Loop
            '        'Remove old freetext
            '        oFreetexts = Nothing
            '        oFreetexts = New Freetexts

            '        'Create new freetext and be sure to store the reference in a separate line
            '        sTmp2 = String.Empty
            '        sTmp2 = sTmp.Substring(0, iStartPos - 5)
            '        Do While Len(sTmp2) > 0
            '            oFreetext = oFreetexts.Add()
            '            If Len(sTmp2) > 40 Then
            '                oFreetext.Text = Left(sTmp2, 40)
            '                sTmp2 = Mid(sTmp2, 41)
            '            Else
            '                oFreetext.Text = Mid(sTmp2, 1, sTmp2.Length)
            '                sTmp2 = ""
            '            End If
            '        Loop

            '        'Store the reference and remove leading zeros (maybe this should not be a general solution, but it is essential for Visma Collectors)
            '        oFreetext = oFreetexts.Add()
            '        oFreetext.Text = RemoveLeadingCharacters(sTmp.Substring(iStartPos, iPosCounter), "0")

            '        'Store the rest of the freetext
            '        sTmp2 = sTmp.Substring(iStartPos + iPosCounter)
            '        Do While Len(sTmp2) > 0
            '            oFreetext = oFreetexts.Add()
            '            If Len(sTmp2) > 40 Then
            '                oFreetext.Text = Left(sTmp2, 40)
            '                sTmp2 = Mid(sTmp2, 41)
            '            Else
            '                oFreetext.Text = Mid(sTmp2, 1, sTmp2.Length)
            '                sTmp2 = ""
            '            End If
            '        Loop

            '    End If
            'End If
            ''24.05.2016  b�r ha en returverdi!
            ReadPain001 = True


        Catch ex As Exception
            ReadPain001 = False
            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadPain001_Invoice" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sBatchCounter, sTmp))
            End If

        Finally
            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not nodeList2 Is Nothing Then
                nodeList2 = Nothing
            End If
            If Not nodeTemp Is Nothing Then
                nodeTemp = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If

        End Try

    End Function
    Friend Function ReadPain001_Structured(ByRef nodInvoice As System.Xml.XmlNode, ByRef nsMgr As System.Xml.XmlNamespaceManager, ByRef sFileNameIn As String, ByVal sBatchCounter As String, ByVal sPaymentInfo As String, ByRef nodeListUnstructured As System.Xml.XmlNodeList, ByRef sPayCode As String) As Boolean
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim oFreetext As vbBabel.Freetext = Nothing
        Dim sTmp As String = ""
        Dim sTmp2 As String = ""
        Dim nTmp As Double = 0
        Dim bReturnValue As Boolean = True
        Dim sStringAmount As String = ""

        Try

            nodeList = nodInvoice.SelectNodes("ISO:RfrdDocInf", nsMgr)
            For Each node In nodeList
                sTmp = GetInnerText(node, "ISO:Tp/ISO:CdOrPrtry/ISO:Cd", nsMgr)
                sTmp2 = GetInnerText(node, "ISO:Tp/ISO:CdOrPrtry/ISO:Prtry", nsMgr)
                If sTmp = "CREN" Or sTmp = "CINV" Then
                    If IsAutogiro(sPayCode) Then
                        sUnique_Id = GetInnerText(node, "ISO:Nb", nsMgr)
                        '17.03.2015 - PUTIN Commented this line, created problems for Visma Collectors and is illogical
                        'But this change may create problems for the customer this solution was created for (Gjensdige??)
                        'bMATCH_Matched = True
                    Else
                        sInvoiceNo = GetInnerText(node, "ISO:Nb", nsMgr)
                    End If
                ElseIf sTmp2 = "AGJ" Then
                    sREF_Bank = GetInnerText(node, "ISO:Nb", nsMgr)
                ElseIf sTmp2 = "IT" Then
                    sREF_Bank = GetInnerText(node, "ISO:Nb", nsMgr)
                End If
                sInvoiceDate = Replace(GetInnerText(node, "ISO:RltdDt", nsMgr), "-", "")
            Next node

            ' 22.10.2018 - test also Ref
            ' 16.08.2019 - Must first test if this Tag holds info, otherwise we can delete info already set for sInvoiceNo
            If Not EmptyString(GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Ref", nsMgr)) Then
                sInvoiceNo = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Ref", nsMgr)
            End If

            'Amounts
            bAmountSetInvoice = True
            bAmountSetTransferred = True
            sTmp = GetInnerText(nodInvoice, "ISO:RfrdDocAmt/ISO:RmtdAmt", nsMgr)
            If Not EmptyString(sTmp) Then
                nTmp = ConvertToAmount(sTmp, "", ".")
            Else
                sTmp = GetInnerText(nodInvoice, "ISO:RfrdDocAmt/ISO:CdtNoteAmt", nsMgr)
                If Not EmptyString(sTmp) Then
                    nTmp = ConvertToAmount(sTmp, "", ".") * -1
                Else
                    sTmp = GetInnerText(nodInvoice, "ISO:RfrdDocAmt/ISO:DuePyblAmt", nsMgr)
                    If Not EmptyString(sTmp) Then
                        nTmp = ConvertToAmount(sTmp, "", ".")
                    Else
                        nTmp = 0
                        bAmountSetInvoice = False
                        bAmountSetTransferred = False
                    End If
                End If
            End If
            nMON_InvoiceAmount = nTmp
            nMON_TransferredAmount = nMON_InvoiceAmount

            'OCR
            sTmp = ""
            sTmp = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Tp/ISO:CdOrPrtry/ISO:Cd", nsMgr)
            If sTmp = "SCOR" Then
                sUnique_Id = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Ref", nsMgr)
                '17.03.2015 - PUTIN Commented this line, created problems for Visma Collectors and is illogical
                'But this change may create problems for the customer this solution was created for (Gjensdige??)
                'bMATCH_Matched = True
            End If

            If Len(sInvoiceNo) > 0 Then
                sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                oFreetext = oFreetexts.Add()
                If Len(sInvoiceNo) < 33 Then
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = LRS(60215) & sInvoiceNo
                    Else
                        oFreetext.Text = LRS(60214) & sInvoiceNo
                    End If
                    ' 22.10.2018 - added next If. We do not always get structured invoiceamount;
                    If bAmountSetInvoice Then
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                    End If
                Else
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = LRS(60215) & Left(sInvoiceNo, 32)
                    Else
                        oFreetext.Text = LRS(60214) & Left(sInvoiceNo, 32)
                    End If
                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = Mid(sInvoiceNo, 33)
                    ' 22.10.2018 - added next If. We do not always get structured invoiceamount;
                    If bAmountSetInvoice Then
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                    End If
                End If
                If Not nodeListUnstructured Is Nothing Then
                    If Not nodeListUnstructured.Count = 0 Then
                        For Each node In nodeListUnstructured
                            sTmp = Trim(node.InnerText)
                            Do While Len(sTmp) > 0
                                If Len(sTmp) > 40 Then
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = Mid(sTmp, 41)
                                Else
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = ""
                                End If
                            Loop
                        Next node
                    End If
                End If
            ElseIf Len(sUnique_Id) > 0 Then 'KID
                sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                oFreetext = oFreetexts.Add()
                If Len(sUnique_Id) < 33 Then
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = "KID: " & sUnique_Id
                    Else
                        oFreetext.Text = "KID: " & sUnique_Id
                    End If
                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                Else
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = "KID: " & Left(sUnique_Id, 32)
                    Else
                        oFreetext.Text = "KID: " & Left(sUnique_Id, 32)
                    End If
                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = Mid(sInvoiceNo, 33)
                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                End If
                If Not nodeListUnstructured Is Nothing Then
                    If Not nodeListUnstructured.Count = 0 Then
                        For Each node In nodeListUnstructured
                            sTmp = Trim(node.InnerText)
                            Do While Len(sTmp) > 0
                                If Len(sTmp) > 40 Then
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = Mid(sTmp, 41)
                                Else
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = ""
                                End If
                            Loop
                        Next node
                    End If
                End If
            Else
                'No invoiceinfo, add amount as text
                oFreetext = oFreetexts.Add()
                oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                If Not nodeListUnstructured Is Nothing Then
                    If Not nodeListUnstructured.Count = 0 Then
                        For Each node In nodeListUnstructured
                            sTmp = Trim(node.InnerText)
                            Do While Len(sTmp) > 0
                                If Len(sTmp) > 40 Then
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = Mid(sTmp, 41)
                                Else
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = ""
                                End If
                            Loop
                        Next node
                    End If
                End If
            End If



            Return bReturnValue

        Catch ex As Exception

            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadPain001_InvoiceStructured" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sBatchCounter, sTmp))
            End If

        Finally
            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If

        End Try
    End Function
    Friend Function ReadPain001_D365_SemiStructured(ByRef nodInvoice As System.Xml.XmlNode, ByRef nsMgr As System.Xml.XmlNamespaceManager, ByRef sFileNameIn As String, ByVal sBatchCounter As String, ByVal sPaymentInfo As String, ByRef nodeListUnstructured As System.Xml.XmlNodeList, ByRef sPayCode As String) As Boolean
        ' 07.01.2020
        ' Added this function for Veidekke Industri and Dynamics365.
        ' They are producing a Pain.001 file with an illegal structure
        ' There is one, and only one <Strd>, followed by several <RfrdDocInf>, and no <RfrdDocAmt>
        ' Redo to Ustrd, with all invoices listed as freetext 
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim oFreetext As vbBabel.Freetext = Nothing
        Dim sTmp As String = ""
        'Dim sTmp2 As String = ""
        'Dim nTmp As Double = 0
        Dim bReturnValue As Boolean = True
        Dim sStringAmount As String = ""
        Dim sFreetext As String = ""

        Try
            sFreetext = ""
            nodeList = nodInvoice.SelectNodes("ISO:RfrdDocInf", nsMgr)
            For Each node In nodeList
                sTmp = GetInnerText(node, "ISO:Tp/ISO:CdOrPrtry/ISO:Cd", nsMgr)
                'sTmp2 = GetInnerText(node, "ISO:Tp/ISO:CdOrPrtry/ISO:Prtry", nsMgr)
                If sTmp = "CREN" Or sTmp = "CINV" Then
                    If EmptyString(sFreetext) Then
                        sFreetext = GetInnerText(node, "ISO:Nb", nsMgr)
                    Else
                        sFreetext = sFreetext & "," & GetInnerText(node, "ISO:Nb", nsMgr)
                    End If
                End If
            Next node

            'Amounts
            bAmountSetInvoice = False
            bAmountSetTransferred = False

            oFreetext = oFreetexts.Add()
            oFreetext.Text = sFreetext

            Return bReturnValue

        Catch ex As Exception

            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadPain001_D365SemiStructured" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sBatchCounter, sTmp))
            End If

        Finally
            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If

        End Try
    End Function
    Friend Function ReadCamt054(ByRef nodInvoice As System.Xml.XmlNode, ByRef nsMgr As System.Xml.XmlNamespaceManager, ByRef sFileNameIn As String, ByVal sBatchCounter As String, ByVal sPaymentInfo As String) As Boolean
        Dim nodeList As System.Xml.XmlNodeList
        Dim nodeList2 As System.Xml.XmlNodeList
        Dim nodeTemp As System.Xml.XmlNode
        Dim node As System.Xml.XmlNode
        Dim oFreetext As vbBabel.Freetext = Nothing
        Dim sTmp As String = ""
        Dim sTmp2 As String = ""
        Dim nTmp As Double = 0
        Dim bReturnValue As Boolean = True
        Dim iStartIndex As Integer
        Dim iStartPos As Integer
        Dim iCount As Integer
        Dim iPosCounter As Integer
        Dim iLength As Integer

        '*RmtInf - 0 .. 1 
        '   *Ustrd - 0 .. unbounded - Max140Text
        '       Sweden D: Information on international payments. 
        '       Denmark Denmark: Information on direct debit transactions.
        '   *Strd - 0 .. unbounded - Information supplied to enable the matching/reconciliation of an entry with the items that the payment is intended to settle, 
        '           such as commercial invoices in an accounts' receivable system, in a structured form.
        '       *RfrdDocInf - 0 .. unbounded - Set of elements used to identify the documents referred to in the remittance information.
        '           *Tp - 0 .. 1 - Specifies the type of referred document.
        '               *CdOrPrtry - 1 .. 1 - Provides the type details of the referred document.
        '                   *Cd - 1 .. 1 - Document type in a coded form.
        '                       Allowed Codes: 
        '                       CINV CommercialInvoice 
        '                       CREN CreditNote
        '                   *Prtry - 1 .. 1 - Max35Text
        '                       'NOT FULLY IMPLEMENTED
        '                       Allowed Codes
        '                       ABO Journal number (Total in SE) 
        '                       ACD Other Reference 
        '                       AGJ Transaction Sequence Number 
        '                       IT Internal Customer Number
        '           *Nb - 0 .. 1 - Max35Text - Unique and unambiguous identification of the referred document.
        '               Sweden: For incoming Bankgiro payments (except for direct debit transactions), �AGJ� will always be sent. 
        '                       For payments through Bankgiro Inbetalningar, it will contain the transaction sequence number of the payment (bank reference). 
        '                           If archive/image information is stored at the clearinghouse, the very same reference (AGJ) will also be forwarded in ACD. 
        '                       For incoming payments through TotalIN �AGJ� will contain the PlusGiro transaction sequence number. 
        '                       For structured remitting information with extra references, which belong to the same transaction, this sequence number will be the same as for the main transaction. 
        '                           When archive/image information exists for this transaction, it will be identified by this reference.
        '               Finland: If payer has given a customer number in the incoming payment it will be stated with qualifier IT. 
        '                       If payer has given a customer number in the incoming payment Service code (KTL)
        '           *RltdDt - 0 .. 1 - Date associated with the referred document.
        '       *RfrdDocAmt -  0 .. 1 - 
        '           *DuePyblAmt -  0 .. 1 - Amount specified is the exact amount due and payable to the creditor.
        '               Delivered when available. This is the invoice amount before any deduction by the payer.
        '               *Ccy - Attribute
        '           *CdtNoteAmt -  0 .. 1 - Amount specified for the referred document is the amount of a credit note.
        '               *Ccy - Attribute 
        '           *RmtdAmt -  0 .. 1 - Amount of money remitted for the referred document.
        '               Sweden: Zero amount is possible i.e. digit �0� may occur. 
        '               Poland D: The revoked amount from Poland will be original amount plus interest for the period from payment date to date when the Direct Debit transaction was revoked. 
        '                   Revoked amount will consequently in most cases be higher than the original direct debit amount.
        '               *Ccy - Attribute
        '       *CdtrRefInf - 0 .. 1 - Reference information provided by the creditor to allow the identification of the underlying documents
        '           *Tp - 0 .. 1 - Specifies the type of creditor reference.
        '               *CdOrPrtry - 1 .. 1 - Coded or proprietary format creditor reference type.
        '                   *Cd - 1 .. 1 - 
        '                       Allowed Codes: SCOR StructuredCommunicationReference
        '               *Issr - 0 .. 1 - Max35Text
        '                   Issuer should be specified with the text 'ISO' when CreditorReference is structured in accordance with ISO 11649.
        '           *Ref - 0 .. 1 - Max35Text
        '               Denmark: Transfer Form Code + Reference
        '   *AddtlRmtInf - 0 .. 3 - Max140Text
        '       Additional information, in free text form, to complement the structured remittance information. 
        '       Use: /ERR/ - Error in Document Number, /ARI/ - Additional reference information

        Try

            nodeList = nodInvoice.SelectNodes("ISO:Ustrd", nsMgr)
            If Not nodeList Is Nothing Then
                For Each nodeTemp In nodeList
                    sTmp = nodeTemp.InnerText
                    Do While Len(sTmp) > 0
                        oFreetext = oFreetexts.Add()
                        If oCargo.Special = "VEIDEKKE_SPLITTE" Then
                            oFreetext.Text = sTmp
                            Exit Do
                        Else
                            If Len(sTmp) > 40 Then
                                oFreetext.Text = Left(sTmp, 40)
                                sTmp = Mid(sTmp, 41)
                            Else
                                oFreetext.Text = Mid(sTmp, 1, Len(sTmp))
                                sTmp = ""
                            End If
                        End If
                    Loop
                Next nodeTemp
            End If

            'nodeList = nodInvoice.SelectNodes("ISO:Ustrd", nsMgr)



            '19.05.2015 - Added the next IF
            If oCargo.Bank = BabelFiles.Bank.DnBNOR_INPS Then
                'Reference-payments are reported as strustured payments. We got a problem that the reference may
                'be split on two lines.
                'Try to find the reference and store it in the unique-ID

                'First store all freetext in a string (with no seperator between the lines
                sTmp = String.Empty
                For Each oFreetext In Freetexts
                    sTmp = sTmp & oFreetext.Text.Trim
                Next oFreetext

                'Search the string for SCOR, signifies reference-payment (my guess)
                iStartIndex = 0
                iCount = 0
                iPosCounter = 0
                iLength = sTmp.Length
                iStartPos = sTmp.IndexOf("SCOR", iStartIndex, StringComparison.CurrentCulture)
                If iStartPos > 0 Then
                    If sTmp.IndexOf("SCOR", iStartPos + 5, StringComparison.CurrentCulture) = -1 Then
                        iStartPos = iStartPos + 5 'This should be the first position of the reference, after SCOR <Space>
                        Do While True
                            'Check if we have reached the end of the text
                            If iLength > iStartPos + iPosCounter Then
                                If vbIsNumeric(sTmp.Substring(iStartPos + iPosCounter, 1), "0123456789") Then
                                    iPosCounter = iPosCounter + 1
                                Else
                                    Exit Do
                                End If
                            Else
                                Exit Do
                            End If
                        Loop
                        'Remove old freetext
                        oFreetexts = Nothing
                        oFreetexts = New Freetexts

                        'Create new freetext and be sure to store the reference in a separate line
                        sTmp2 = String.Empty
                        sTmp2 = sTmp.Substring(0, iStartPos - 5)
                        Do While Len(sTmp2) > 0
                            oFreetext = oFreetexts.Add()
                            If Len(sTmp2) > 40 Then
                                oFreetext.Text = Left(sTmp2, 40)
                                sTmp2 = Mid(sTmp2, 41)
                            Else
                                oFreetext.Text = Mid(sTmp2, 1, sTmp2.Length)
                                sTmp2 = ""
                            End If
                        Loop

                        'Store the reference and remove leading zeros (maybe this should not be a general solution, but it is essential for Visma Collectors)
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = RemoveLeadingCharacters(sTmp.Substring(iStartPos, iPosCounter), "0")

                        'Store the rest of the freetext
                        sTmp2 = sTmp.Substring(iStartPos + iPosCounter)
                        Do While Len(sTmp2) > 0
                            oFreetext = oFreetexts.Add()
                            If Len(sTmp2) > 40 Then
                                oFreetext.Text = Left(sTmp2, 40)
                                sTmp2 = Mid(sTmp2, 41)
                            Else
                                oFreetext.Text = Mid(sTmp2, 1, sTmp2.Length)
                                sTmp2 = ""
                            End If
                        Loop

                    End If
                End If
            End If
            '24.05.2016  b�r ha en returverdi!
            ReadCamt054 = True


        Catch ex As Exception
            ReadCamt054 = False
            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadCamt054_Invoice" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sBatchCounter, sTmp))
            End If

        Finally
            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not nodeList2 Is Nothing Then
                nodeList2 = Nothing
            End If
            If Not nodeTemp Is Nothing Then
                nodeTemp = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If

        End Try

    End Function
    Friend Function ReadCamt054_Structured(ByRef nodInvoice As System.Xml.XmlNode, ByRef nsMgr As System.Xml.XmlNamespaceManager, ByRef sFileNameIn As String, ByVal sBatchCounter As String, ByVal sPaymentInfo As String, ByRef nodeListUnstructured As System.Xml.XmlNodeList, Optional ByRef sPayCode As String = "", Optional ByRef bUseRefAsFreetext As Boolean = False, Optional ByRef bAddAmountToFreetext As Boolean = True) As Boolean
        '25.01.2022 - Added the use of bAddAmountToFreetext in the function
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim oFreetext As vbBabel.Freetext = Nothing
        Dim sTmp As String = ""
        Dim sTmp2 As String = ""
        Dim nTmp As Double = 0
        Dim bReturnValue As Boolean = True
        Dim sStringAmount As String = ""
        Dim bAddStructuredAsFreetext As Boolean = False
        Dim bCheckForStructured As Boolean
        Dim sFreetextToAdd As String = ""

        Try

            bAddStructuredAsFreetext = False
            sFreetextToAdd = ""
            nodeList = nodInvoice.SelectNodes("ISO:RfrdDocInf", nsMgr)
            For Each node In nodeList
                sTmp = GetInnerText(node, "ISO:Tp/ISO:CdOrPrtry/ISO:Cd", nsMgr)
                sTmp2 = GetInnerText(node, "ISO:Tp/ISO:CdOrPrtry/ISO:Prtry", nsMgr)
                If sTmp = "CREN" Or sTmp = "CINV" Then
                    If IsAutogiro(sPayCode) Then
                        sUnique_Id = GetInnerText(node, "ISO:Nb", nsMgr)
                        '17.03.2015 - PUTIN Commented this line, created problems for Visma Collectors and is illogical
                        'But this change may create problems for the customer this solution was created for (Gjensdige??)
                        'bMATCH_Matched = True
                    Else
                        sInvoiceNo = GetInnerText(node, "ISO:Nb", nsMgr)

                        '17.10.2022
                        'In som formats f.ex. Nordea Corporate Finance  
                        'for Sweden the invoiceno is stated in AddtlRmtInf
                        '
                        'Do it for all formats
                        If EmptyString(sInvoiceNo) Then
                            sInvoiceNo = GetInnerText(nodInvoice, "ISO:AddtlRmtInf", nsMgr)
                        End If

                    End If
                ElseIf sTmp2 = "AGJ" Then
                        sREF_Bank = GetInnerText(nodInvoice, "ISO:Nb", nsMgr)
                ElseIf sTmp2 = "IT" Then
                        sREF_Bank = GetInnerText(node, "ISO:Nb", nsMgr)
                ElseIf sTmp2 = "DEBI" Then '20.11.2019 - Added for Danske Bank DK for SG Finans. This info is not useful for SG
                        bAddStructuredAsFreetext = True
                        sFreetextToAdd = "DEBI: " & GetInnerText(node, "ISO:Nb", nsMgr)
                        sPayCode = "601" 'This however is imortant for the automatic matching
                End If
            Next node
            'Amounts
            bAmountSetInvoice = True
            bAmountSetTransferred = True
            sTmp = GetInnerText(nodInvoice, "ISO:RfrdDocAmt/ISO:RmtdAmt", nsMgr)
            If Not EmptyString(sTmp) Then
                nTmp = ConvertToAmount(sTmp, "", ".", , True)
            Else
                sTmp = GetInnerText(nodInvoice, "ISO:RfrdDocAmt/ISO:CdtNoteAmt", nsMgr)
                If Not EmptyString(sTmp) Then
                    nTmp = ConvertToAmount(sTmp, "", ".", , True) * -1
                Else
                    sTmp = GetInnerText(nodInvoice, "ISO:RfrdDocAmt/ISO:DuePyblAmt", nsMgr)
                    If Not EmptyString(sTmp) Then
                        nTmp = ConvertToAmount(sTmp, "", ".", , True)
                    Else
                        nTmp = 0
                        bAmountSetInvoice = False
                        bAmountSetTransferred = False
                    End If
                End If
            End If
            nMON_InvoiceAmount = nTmp
            nMON_TransferredAmount = nMON_InvoiceAmount

            'OCR
            sTmp = ""
            sTmp = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Tp/ISO:CdOrPrtry/ISO:Cd", nsMgr)
            If sTmp = "SCOR" Then
                ' 06.01.2020 - added next If for XBCT (crossborder) set as SCOR
                If bUseRefAsFreetext Then
                    bAddStructuredAsFreetext = True
                    sFreetextToAdd = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Ref", nsMgr)
                Else
                    sUnique_Id = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Ref", nsMgr)
                    '17.03.2015 - PUTIN Commented this line, created problems for Visma Collectors and is illogical
                    'But this change may create problems for the customer this solution was created for (Gjensdige??)
                    'bMATCH_Matched = True

                    '17.10.2022 - Nordea Finance Eq
                    'Might change the code here and always store the info in .InvoiceNo  when SCOR is stated
                    'Should be OK if the invoice matchingrule also includes KID, without validation of the KID
                    'In SE it is not necesary a KID when SCOR.

                End If
            End If

            ' 07.02.2020 - KJELLLLLLLL
            ' Her m� vi se p� hva vi gj�r for Visma Collectors DK
            ' 11.02.2020 - re nye filer for Visma Collectors.
            ' I CdtrRefInf/Ref f�r vi med OCR-en, som skal brukes videre i avstemming for Visma
            ' Det er med 4 ledende 0-er, men vi leser inn de ogs�, og tar hensyn til dette ved oppsett av avstemmingsregler
            If IsDnB(oCargo.Bank) And oCargo.CountryName = BabelFiles.CountryName.Denmark Then
                If IsOCR(sPayCode) Then
                    sUnique_Id = GetInnerText(nodInvoice, "ISO:CdtrRefInf/ISO:Ref", nsMgr)
                End If
            End If

            If Len(sInvoiceNo) > 0 Then
                sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                oFreetext = oFreetexts.Add()
                If Len(sInvoiceNo) < 33 Then
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = LRS(60215) & sInvoiceNo
                    Else
                        oFreetext.Text = LRS(60214) & sInvoiceNo
                    End If
                    If bAddAmountToFreetext Then
                        If Not IsEqualAmount(nMON_InvoiceAmount, 0) Then
                            oFreetext = oFreetexts.Add()
                            oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                        End If
                    End If
                Else
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = LRS(60215) & Left(sInvoiceNo, 32)
                    Else
                        oFreetext.Text = LRS(60214) & Left(sInvoiceNo, 32)
                    End If
                    'oFreetext = oFreetexts.Add()
                    'oFreetext.Text = Mid(sInvoiceNo, 33)
                    If bAddAmountToFreetext Then
                        If Not IsEqualAmount(nMON_InvoiceAmount, 0) Then
                            oFreetext = oFreetexts.Add()
                            oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                        End If
                    End If
                End If
                If Not nodeListUnstructured Is Nothing Then
                    If Not nodeListUnstructured.Count = 0 Then
                        For Each node In nodeListUnstructured
                            sTmp = Trim(node.InnerText)
                            Do While Len(sTmp) > 0
                                If Len(sTmp) > 40 Then
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = Mid(sTmp, 41)
                                Else
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = ""
                                End If
                            Loop
                        Next node
                    End If
                End If

                ' 12.10.2020 - Keep oInvoice.InvoiceNo. This info is cut to 6, which is real invoiceno, in TreatSpecial
                'If oCargo.Special = "ABAX_SWEDEN_INCOMING" Then
                '    ' for abax, treat as "normal" freetext
                '    ' otherwise, the matching will not be good - as there are freetext inside sInvoiceNo
                '    sInvoiceNo = ""
                'End If
                ' 30.09.2019
                If oCargo.Special = "DNBNORFINANS_CREMUL" Then
                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                End If
            ElseIf Len(sUnique_Id) > 0 Then 'KID
                ' 21.11.2018 - override Paycode to OCR-payment, first for Norway ( so far )
                If oCargo.CountryName = BabelFiles.CountryName.Norway Then
                    sPayCode = 510
                End If
                ' 24.05.2019 - do the same for Denmark, from SEB for ABAX first time
                If oCargo.CountryName = BabelFiles.CountryName.Denmark And IsSEB(oCargo.Bank) Then
                    sPayCode = 510
                End If

                '23.12.2019 - Be careful to do this for Sweden, because of Gjensidige which imports Autogiro in Sweden (payCode 611

                sStringAmount = PadLeft(ConvertFromAmountToString(nMON_InvoiceAmount, ".", ","), 18, " ")
                oFreetext = oFreetexts.Add()
                If Len(sUnique_Id) < 33 Then
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = "KID: " & sUnique_Id
                    Else
                        oFreetext.Text = "KID: " & sUnique_Id
                    End If
                    If bAddAmountToFreetext Then
                        If Not IsEqualAmount(nMON_InvoiceAmount, 0) Then
                            oFreetext = oFreetexts.Add()
                            oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                        End If
                    End If
                Else
                    If nMON_InvoiceAmount < 0 Then
                        oFreetext.Text = "KID: " & Left(sUnique_Id, 32)
                    Else
                        oFreetext.Text = "KID: " & Left(sUnique_Id, 32)
                    End If
                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = Mid(sUnique_Id, 33)
                    If bAddAmountToFreetext Then
                        If Not IsEqualAmount(nMON_InvoiceAmount, 0) Then
                            oFreetext = oFreetexts.Add()
                            oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                        End If
                    End If
                End If
                If Not nodeListUnstructured Is Nothing Then
                    If Not nodeListUnstructured.Count = 0 Then
                        For Each node In nodeListUnstructured
                            sTmp = Trim(node.InnerText)
                            Do While Len(sTmp) > 0
                                If Len(sTmp) > 40 Then
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = Mid(sTmp, 41)
                                Else
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = ""
                                End If
                            Loop
                        Next node
                    End If
                End If
                ' 30.09.2019
                If oCargo.Special = "DNBNORFINANS_CREMUL" Then
                    eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                End If

            Else
                bCheckForStructured = True

                If bAddAmountToFreetext Then
                    'No invoiceinfo, add amount as text
                    If Not IsEqualAmount(nMON_InvoiceAmount, 0) Then
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = LRS(60060) & ":  " & ConvertFromAmountToString(nMON_InvoiceAmount, " ", ",") & " / "
                    End If
                End If
                If Not nodeListUnstructured Is Nothing Then
                    If Not nodeListUnstructured.Count = 0 Then
                        bCheckForStructured = False
                        For Each node In nodeListUnstructured
                            sTmp = Trim(node.InnerText)
                            Do While Len(sTmp) > 0
                                If Len(sTmp) > 40 Then
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = Mid(sTmp, 41)
                                Else
                                    oFreetext = oFreetexts.Add
                                    oFreetext.Text = sTmp
                                    sTmp = ""
                                End If
                            Loop
                        Next node
                    End If
                End If

                If bCheckForStructured Then
                    sTmp = GetInnerText(nodInvoice, "ISO:AddtlRmtInf", nsMgr)
                    Do While Len(sTmp) > 0
                        If Len(sTmp) > 40 Then
                            oFreetext = oFreetexts.Add
                            oFreetext.Text = sTmp
                            sTmp = Mid(sTmp, 41)
                        Else
                            oFreetext = oFreetexts.Add
                            oFreetext.Text = sTmp
                            sTmp = ""
                        End If
                    Loop
                End If

                '20.11.2019 - Added next IF
                If bAddStructuredAsFreetext = True Then
                    sTmp = sFreetextToAdd
                    Do While Len(sTmp) > 0
                        If Len(sTmp) > 40 Then
                            oFreetext = oFreetexts.Add
                            oFreetext.Text = sTmp
                            sTmp = Mid(sTmp, 41)
                        Else
                            oFreetext = oFreetexts.Add
                            oFreetext.Text = sTmp
                            sTmp = ""
                        End If
                    Loop
                End If
                End If



                Return bReturnValue

        Catch ex As Exception

            If Left(ex.Message, 5) = "BBBBB" Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception("BBBBBFunction: ReadCamt054_InvoiceStructured" & vbCrLf & ex.Message & vbCrLf & LRS(11044, sFileNameIn, sBatchCounter, sTmp))
            End If

        Finally
            If Not nodeList Is Nothing Then
                nodeList = Nothing
            End If
            If Not node Is Nothing Then
                node = Nothing
            End If

        End Try
    End Function
    'Removed 11.09.2019
    'Private Function ReadTBIFile(ByRef nodPaymentDetails As MSXML2.IXMLDOMNode, ByRef sFormatType As String) As Boolean
    '    Dim oFreeText As Freetext
    '    Dim bReturnValue As Boolean
    '    Dim sFreetext As String

    '    bReturnValue = True


    '    If sFormatType = "TBI_ACH" Or sFormatType = "TBI_Salary" Then
    '        'UPGRADE_WARNING: Couldn't resolve default property of object nodPaymentDetails.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        sREF_Own = nodPaymentDetails.selectSingleNode("child::*[name()='ACHPayersRef']").nodeTypedValue
    '        'UPGRADE_WARNING: Couldn't resolve default property of object nodPaymentDetails.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        sFreetext = nodPaymentDetails.selectSingleNode("child::*[name()='ACHBeneficiaryRef']").nodeTypedValue
    '        If Len(sFreetext) > 0 Then
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = sFreetext
    '        End If
    '    Else
    '        'Could be 140 pos. long
    '        'UPGRADE_WARNING: Couldn't resolve default property of object nodPaymentDetails.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        sFreetext = nodPaymentDetails.selectSingleNode("child::*[name()='InfoToBeneficiary']").nodeTypedValue
    '        Select Case Len(sFreetext)
    '            Case Is < 41
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = sFreetext

    '            Case Is < 81
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Left(sFreetext, 40)
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Mid(sFreetext, 41)

    '            Case Is < 121
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Left(sFreetext, 40)
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Mid(sFreetext, 41, 40)
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Mid(sFreetext, 81)

    '            Case Else
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Left(sFreetext, 40)
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Mid(sFreetext, 41, 40)
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Mid(sFreetext, 81, 40)
    '                oFreeText = oFreetexts.Add()
    '                oFreeText.Text = Mid(sFreetext, 121)

    '        End Select
    '    End If

    '    'If oPayment.PayType = "I" Then
    '    '    bx = AddInvoiceAmountOnBatch()
    '    'Else
    '    '    nMON_InvoiceAmount = nMON_TransferredAmount
    '    '    bAmountSetInvoice = True
    '    'End If

    '    ReadTBIFile = bReturnValue

    'End Function
    Private Function ReadTellerXML(ByRef nodTrans_Detail As MSXML2.IXMLDOMNode) As Boolean
        Dim oFreeText As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String

        bReturnValue = True

        'Used by Lindorff

        '    trans_detail        Innpakking av enkelt transaksjoner
        'X signifies not implemented
        'X�   pos_no  �   Brukerstedsnummer       x(7)
        'description �   Brukerstedetsnavn       x(130)
        'X.   delivery_date   �   Mottaksdato (nota_dato)     date(8)
        'your_ref    �   Referanse       x(15)
        'slip_no �   Arkivreferanse      X(25)
        'X�   purchase_date   �   Kj�psdato       date(8)
        'card_no �   Kortnummer (strippet)       X(12)
        'X   brand   �   Brand
        'X�   amount  �   Bel�p       M(14)
        'currency_amount �   Bel�p i valuta      M(14)
        'X�   currency    �   Valuta kode (f.eks 'NOK')       X(3)
        'X�   our_ref �   Teller referanse (notasammendragsnummer)        X(15)
        'X�   air_customer_ref    �   Brukerstedets fakturanummer     X(20)
        'X�   authorisation_ref   �   Autorisasjonsnummer X(6)
        'transaction_ref �   Transaksjonsreferanse
        'X�   transaction_ref2    �   Transaksjonsreferanse 2

        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sREF_Own = nodTrans_Detail.selectSingleNode("child::*[name()='your_ref']").nodeTypedValue 'Referanse
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sREF_Bank = nodTrans_Detail.selectSingleNode("child::*[name()='slip_no']").nodeTypedValue 'Arkivreferanse
        nMON_InvoiceAmount = CDbl(ConvertToAmount((nodTrans_Detail.selectSingleNode("child::*[name()='currency_amount']").nodeTypedValue), "", ".", True)) 'Bruttobel�p i valuta
        'The use of the following fields will differ from company to company
        'air_customer_ref    �   Brukerstedets fakturanummer     X(20)
        'authorisation_ref   �   Autorisasjonsnummer X(6)
        'transaction_ref �   Transaksjonsreferanse
        'transaction_ref2    �   Transaksjonsreferanse 2

        'For Lindorff
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sUnique_Id = nodTrans_Detail.selectSingleNode("child::*[name()='transaction_ref2']").nodeTypedValue 'KID

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='card_no']").nodeTypedValue 'Kortnummer (strippet)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = LRS(11018) & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='air_customer_ref']").nodeTypedValue 'Kortnummer (strippet)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Air_customer_ref: " & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='authorisation_ref']").nodeTypedValue 'Kortnummer (strippet)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Authorisation_ref: " & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='transaction_ref']").nodeTypedValue 'Kortnummer (strippet)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Transaction_ref: " & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='transaction_ref2']").nodeTypedValue 'Kortnummer (strippet)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Transaction_ref2: " & sFreetext
        End If

        sFreetext = ""
        sFreetext = ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Amount: " & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='purchase_date']").nodeTypedValue 'Kortnummer (strippet)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Purchase date: " & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTrans_Detail.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTrans_Detail.selectSingleNode("child::*[name()='description']").nodeTypedValue 'Brukerstedetsnavn
        If Not EmptyString(sFreetext) Then
            Do While True
                oFreeText = oFreetexts.Add()
                If Len(sFreetext) > 40 Then
                    oFreeText.Text = Left(sFreetext, 40)
                    sFreetext = Mid(sFreetext, 41)
                Else
                    oFreeText.Text = sFreetext
                    Exit Do
                End If
            Loop
        End If

        '''''''*****************************************************************************
        '''''''Testcode for Lindorff
        ''''''Dim sLineToWrite As String
        ''''''Dim sTmp As String
        ''''''Dim oTellerInvoice As vbBabel.Invoice
        ''''''Dim oTellerFreetext As vbBabel.FreeText
        ''''''Dim sTellerValuedate As String
        ''''''Dim bTellerValuedateFound As Boolean
        ''''''Dim bSaksnrFound As Boolean
        ''''''Dim sMATCH_ID As String
        ''''''
        ''''''sMATCH_ID = "4767644"
        ''''''
        ''''''    sTellerValuedate = ""
        ''''''    bTellerValuedateFound = False
        ''''''    bSaksnrFound = False
        ''''''    'For Each oTellerInvoice In oPayment
        ''''''        For Each oTellerFreetext In Freetexts
        ''''''            If bSaksnrFound Then
        ''''''                If Left$(oTellerFreetext.Text, 14) = "Purchase date:" Then
        ''''''                    sTellerValuedate = Mid$(oTellerFreetext.Text, 16, 10)
        ''''''                    bTellerValuedateFound = True
        ''''''                    Exit For
        ''''''                End If
        ''''''            Else
        ''''''                If Left$(oTellerFreetext.Text, 17) = "Transaction_ref2:" Then
        ''''''                    If PadLeft(sMATCH_ID, 8, "0") = Mid$(oTellerFreetext.Text, 21, 8) Then
        ''''''                        bSaksnrFound = True
        ''''''                    End If
        ''''''                End If
        ''''''            End If
        ''''''        Next oTellerFreetext
        ''''''        If bSaksnrFound Then
        ''''''            sMATCH_ID = ""
        ''''''        End If
        ''''''    'Next oTellerInvoice
        ''''''    If bTellerValuedateFound Then
        ''''''        sLineToWrite = sLineToWrite & Replace(Replace(sTellerValuedate, "-", ""), ".", "")              ' Valuteringsdato, 8
        ''''''    Else
        ''''''        'sLineToWrite = sLineToWrite & oPayment.DATE_Value                  ' Valuteringsdato, 8
        ''''''    End If
        '''''''***************************************************************************

        ReadTellerXML = bReturnValue

    End Function
    Private Function ReadTellerXMLNew(ByRef nodTransaction As MSXML2.IXMLDOMNode) As Boolean
        Dim oFreeText As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim nodTemp As MSXML2.IXMLDOMNode

        bReturnValue = True

        'Used by Lindorff

        '************************************************************************
        'Transactions                Innpakking av transaksjon
        '************************************************************************
        'TransactionNo           Transaksjonsnummer                             int
        'SettlementMerchantNo    Brukerstedsnummer til overliggende brukersted  int
        'BatchNo                 Buntnummer                                     int
        'ReceivalNo              Buntforsendelsesnummer                         int
        'MerchantNo              Brukerstedsnummer                              int
        'Brand                   Brand p� transaksjon                           string(256)
        'CreDebCardType          Credit eller debet kort (C eller D)            string(1)
        'NatCardType             Nasjonalt kort eller internasjonalt kort (N eller W)    string(256)
        'TransactionCode         Beskrivelse at type transaksjon                string(256)
        'SubmissionNo            Forsendelsesnummer til oppgj�r                 int
        'BatchDate               Buntdato                                       date(yyyy-mm-dd)
        'SettledCode             Oppgjortkode (teller intern) - alle transaksjoner i et oppgj�r er oppgjort. string(256)
        'BatchRef                Buntreferanse                                  string(256)
        'PurchaseDate            Kj�psdato                                      date(yyyy-mm-dd)
        'CardNo                  Kortnummer (maskert)                           string(256)
        'TransactionCurrency     Kj�psvaluta kode                               string(3)
        'GrossAmount             Brutto kj�psbel�p                              decimal(11,2)
        'NetAmount               Netto kj�psbel�p                               decimal(11,2)
        'AcquirerRefNo           Microreferanse                                 string(256)
        'TellerRef               Teller referanse for bunt                      string(256)
        'TransRef                Transaksjonsreferanse                          string(256)
        'TransRef2               Transaksjonsreferanse 2                        string(256)
        'AuthorizationRef        Autorisasjonsnummer                            string(256)
        'CustomerRef             Kundereferanse                                 string(256)
        '************************************************************************

        'UPGRADE_WARNING: Couldn't resolve default property of object nodTransaction.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sREF_Own = nodTransaction.selectSingleNode("child::*[name()='CustomerRef']").nodeTypedValue 'CustomerRef - Kundereferanse
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTransaction.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sREF_Bank = nodTransaction.selectSingleNode("child::*[name()='TellerRef']").nodeTypedValue 'TellerRef - Teller referanse for bunt

        If oCargo.Special = "JAPANPHOTO BUTIKKHANDEL" Then
            ' 01.07.2014
            ' TellerRef holds BAX-number for Japan Photo, butikkhandel (kortkj�p)
            ' BAX er felles referanse mellom Tellerfilene og oppslag i NAV
            ' TellerRef has BAX, but last three digits is not part of BAX, 00000265973 297
            ' Use BAX as "invoiceNo"
            sInvoiceNo = Replace(sREF_Bank, " ", "")
            sInvoiceNo = Mid(sInvoiceNo, 6, 6)  '00000265973 297
        End If
        nMON_InvoiceAmount = CDbl(ConvertToAmount((nodTransaction.selectSingleNode("child::*[name()='GrossAmount']").nodeTypedValue), "", ".", True)) 'GrossAmount - Brutto kj�psbel�p
        nMON_TransferredAmount = CDbl(ConvertToAmount((nodTransaction.selectSingleNode("child::*[name()='NetAmount']").nodeTypedValue), "", ".", True)) 'NetAmount - Netto kj�psbel�p
        'The use of the following fields will differ from company to company
        'AcquirerRefNo           Microreferanse
        'TellerRef               Teller referanse for bunt
        'TransRef                Transaksjonsreferanse
        'TransRef2               Transaksjonsreferanse 2
        'AuthorizationRef        Autorisasjonsnummer
        'CustomerRef             Kundereferanse
        'transaction_ref2    �   Transaksjonsreferanse 2

        'For Lindorff
        '*�*�*�*�*�*�*�
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTransaction.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sUnique_Id = nodTransaction.selectSingleNode("child::*[name()='CustomerRef']").nodeTypedValue '????????

        ' added 04.06.2014
        sFreetext = "Avdeling: " & oCargo.I_Name
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = sFreetext
        End If

        sFreetext = nodTransaction.selectSingleNode("child::*[name()='CardNo']").nodeTypedValue 'CardNo - Kortnummer (maskert)
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = LRS(11018) & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTransaction.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTransaction.selectSingleNode("child::*[name()='TransRef']").nodeTypedValue 'TransRef - Transaksjonsreferanse
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "TransRef: " & sFreetext
        End If

        sFreetext = ""
        nodTemp = nodTransaction.selectSingleNode("child::*[name()='TransRef2']")
        If Not nodTemp Is Nothing Then
            'UPGRADE_WARNING: Couldn't resolve default property of object nodTemp.nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sFreetext = nodTemp.nodeTypedValue 'TransRef - Transaksjonsreferanse
            If Not EmptyString(sFreetext) Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = "TransRef2: " & sFreetext
            End If
            'UPGRADE_NOTE: Object nodTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            nodTemp = Nothing
        End If

        sFreetext = ""
        nodTemp = nodTransaction.selectSingleNode("child::*[name()='AuthorizationRef']")
        If Not nodTemp Is Nothing Then
            'UPGRADE_WARNING: Couldn't resolve default property of object nodTemp.nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sFreetext = nodTemp.nodeTypedValue 'AuthorizationRef - Autorisasjonsnummer
            If Not EmptyString(sFreetext) Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = "AuthorizationRef: " & sFreetext
            End If
            'UPGRADE_NOTE: Object nodTemp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            nodTemp = Nothing
        End If

        sFreetext = ""
        sFreetext = ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "Amount: " & sFreetext
        End If

        sFreetext = ""
        'UPGRADE_WARNING: Couldn't resolve default property of object nodTransaction.selectSingleNode().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sFreetext = nodTransaction.selectSingleNode("child::*[name()='PurchaseDate']").nodeTypedValue 'PurchaseDate - Kj�psdato
        If Not EmptyString(sFreetext) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "PurchaseDate: " & sFreetext
        End If

        ' 04.06.2014
        ' To be able to match on "structured" info, pick up this here
        ' This is case-dependant
        Select Case oCargo.Special
            Case "JAPANPHOTO NETTHANDEL"
                sInvoiceNo = nodTransaction.selectSingleNode("child::*[name()='TransRef']").nodeTypedValue 'TransRef - Transaksjonsreferanse
        End Select

        'sFreetext = ""
        'sFreetext = nodTransaction.selectSingleNode("child::*[name()='description']").nodeTypedValue 'Brukerstedetsnavn
        'If Not EmptyString(sFreetext) Then
        '    Do While True
        '        Set oFreeText = oFreetexts.Add()
        '        If Len(sFreetext) > 40 Then
        '            oFreeText.Text = Left$(sFreetext, 40)
        '            sFreetext = Mid$(sFreetext, 41)
        '        Else
        '            oFreeText.Text = sFreetext
        '            Exit Do
        '        End If
        '    Loop
        'End If


        ReadTellerXMLNew = bReturnValue

    End Function
    Private Function ReadTellerCardXML(ByVal nodTransaction As System.Xml.XmlNode) As Boolean
        Dim oFreetext As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim nodTemp As System.Xml.XmlNode

        bReturnValue = True

        sREF_Own = nodTransaction.Attributes.GetNamedItem("TransactionNum").Value
        sREF_Bank = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum2']")
        nMON_InvoiceAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionOriginalAmount']"), "", ".", True)
        'nMON_TransferredAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='NetAmount']").nodeTypedValue, "", ".", True) 'NetAmount - Netto kj�psbel�p
        ' Vi har ingen netto bel�p p� TransactionLevel i nytt format
        nMON_TransferredAmount = nMON_InvoiceAmount


        'The next fields may vary depending on company
        '----------------------------------------------
        If oCargo.Special = "SISMO" Then
            sUnique_Id = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum2']")
        Else
            sUnique_Id = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum1']")
        End If

        ' 01.11.2017 - put date/timepart of PurchaseDate into oInvoice.Extra1 (2017-09-28T16:13:34)
        sExtra1 = GetInnerText(nodTransaction, "child::*[name()='PurchaseDate']") 'PurchaseDate - Kj�psdato

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='CardNumMasked']") ' - Kortnummer (maskert)
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = LRS(11018) & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum1']") ' TransactionRefNum1 - OLD:TransRef - Transaksjonsreferanse
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "TransRef: " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum2']")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "TransRef2: " & sFreetext
        End If

        ' 'AuthorizationRefNum' finnes ikke alltid !!!
        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='AuthorizationRefNum']")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "AuthorizationRef: " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            'oFreetext.Text = "Amount: " & sFreetext
            oFreetext.Text = LRS(40021) & " " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='PurchaseDate']") 'PurchaseDate - Kj�psdato
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "PurchaseDate: " & sFreetext
        End If

        ' added 25.10.2017
        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='CardScheme']") 'VI, MS, etc
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "CardType: " & sFreetext
        End If

        ' 04.06.2014
        ' To be able to match on "structured" info, pick up this here
        ' This is case-dependant
        Select Case oCargo.Special
            Case "JAPANPHOTO NETTHANDEL"
                sInvoiceNo = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum1']") 'TransRef - Transaksjonsreferanse
        End Select

        ReadTellerCardXML = bReturnValue

    End Function
    Private Function ReadHelseFonna_XML_Nets(ByVal nodTransaction As System.Xml.XmlNode) As Boolean
        Dim oFreetext As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim nodTemp As System.Xml.XmlNode

        bReturnValue = True

        sREF_Own = nodTransaction.Attributes.GetNamedItem("TransactionNum").Value
        sREF_Bank = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum2']")
        nMON_InvoiceAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionOriginalAmount']"), "", ".", True)
        'nMON_TransferredAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='NetAmount']").nodeTypedValue, "", ".", True) 'NetAmount - Netto kj�psbel�p
        ' Vi har ingen netto bel�p p� TransactionLevel i nytt format
        nMON_TransferredAmount = nMON_InvoiceAmount


        'The next fields may vary depending on company
        '----------------------------------------------
        If oCargo.Special = "SISMO" Then
            sUnique_Id = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum2']")
        Else
            sUnique_Id = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum1']")
        End If

        ' 01.11.2017 - put date/timepart of PurchaseDate into oInvoice.Extra1 (2017-09-28T16:13:34)
        sExtra1 = GetInnerText(nodTransaction, "child::*[name()='PurchaseDate']") 'PurchaseDate - Kj�psdato

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='CardNumMasked']") ' - Kortnummer (maskert)
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = LRS(11018) & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum1']") ' TransactionRefNum1 - OLD:TransRef - Transaksjonsreferanse
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "TransRef: " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum2']")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "TransRef2: " & sFreetext
        End If

        ' 'AuthorizationRefNum' finnes ikke alltid !!!
        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='AuthorizationRefNum']")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "AuthorizationRef: " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            'oFreetext.Text = "Amount: " & sFreetext
            oFreetext.Text = LRS(40021) & " " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='PurchaseDate']") 'PurchaseDate - Kj�psdato
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "PurchaseDate: " & sFreetext
        End If

        ' added 25.10.2017
        sFreetext = vbNullString
        sFreetext = GetInnerText(nodTransaction, "child::*[name()='CardScheme']") 'VI, MS, etc
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "CardType: " & sFreetext
        End If

        ' 04.06.2014
        ' To be able to match on "structured" info, pick up this here
        ' This is case-dependant
        Select Case oCargo.Special
            Case "JAPANPHOTO NETTHANDEL"
                sInvoiceNo = GetInnerText(nodTransaction, "child::*[name()='TransactionRefNum1']") 'TransRef - Transaksjonsreferanse
        End Select

        ReadHelseFonna_XML_Nets = bReturnValue

    End Function
    ' XokNET 21.08.2015 added function
    Private Function ReadTellerXMLNew_2015(ByVal nodTransaction As MSXML2.IXMLDOMNode) As Boolean
        Dim oFreetext As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim nodTemp As MSXML2.IXMLDOMNode

        On Error GoTo ERR_ReadTellerXMLNew

        bReturnValue = True

        sREF_Own = nodTransaction.attributes.getNamedItem("TransactionNum").nodeTypedValue
        nodTemp = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum2']")
        'XOKNET - 07.12.2015 - Added this test several places
        If Not nodTemp Is Nothing Then
            sREF_Bank = nodTemp.nodeTypedValue
        End If
        nMON_InvoiceAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='TransactionOriginalAmount']").nodeTypedValue, "", ".", True)
        'nMON_TransferredAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='NetAmount']").nodeTypedValue, "", ".", True) 'NetAmount - Netto kj�psbel�p
        ' Vi har ingen netto bel�p p� TransactionLevel i nytt format
        nMON_TransferredAmount = nMON_InvoiceAmount


        'The next fields may vary depending on company
        '----------------------------------------------
        If oCargo.Special = "SISMO" Then
            sUnique_Id = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum2']").nodeTypedValue
        ElseIf oCargo.Special = "SERGELNORGE" Then
            sTemp = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum2']").nodeTypedValue
            If Not EmptyString(sTemp) Then
                If sTemp.Length = 16 Then
                    sUnique_Id = sTemp
                Else
                    sInvoiceNo = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum1']").nodeTypedValue
                End If
            Else
                sInvoiceNo = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum1']").nodeTypedValue
            End If
        Else
            sUnique_Id = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum1']").nodeTypedValue
        End If

        ' 01.11.2017 - put date/timepart of PurchaseDate into oInvoice.Extra1 (2017-09-28T16:13:34)
        sExtra1 = nodTransaction.selectSingleNode("child::*[name()='PurchaseDate']").nodeTypedValue 'PurchaseDate - Kj�psdato

        sFreetext = vbNullString
        sFreetext = nodTransaction.selectSingleNode("child::*[name()='CardNumMasked']").nodeTypedValue ' - Kortnummer (maskert)
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = LRS(11018) & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum1']").nodeTypedValue ' TransactionRefNum1 - OLD:TransRef - Transaksjonsreferanse
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "TransRef: " & sFreetext
        End If

        sFreetext = vbNullString
        nodTemp = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum2']")
        If Not nodTemp Is Nothing Then
            'sFreetext = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum2']").nodeTypedValue
            sFreetext = nodTemp.nodeTypedValue
            If Not EmptyString(sFreetext) Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = "TransRef2: " & sFreetext
            End If
        End If

        ' 'AuthorizationRefNum' finnes ikke alltid !!!
        nodTemp = nodTransaction.selectSingleNode("child::*[name()='AuthorizationRefNum']")
        If Not nodTemp Is Nothing Then
            sFreetext = vbNullString
            'sFreetext = nodTransaction.selectSingleNode("child::*[name()='AuthorizationRefNum']").nodeTypedValue
            sFreetext = nodTemp.nodeTypedValue
            If Not EmptyString(sFreetext) Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = "AuthorizationRef: " & sFreetext
            End If
        End If

        sFreetext = vbNullString
        sFreetext = ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            'oFreetext.Text = "Amount: " & sFreetext
            oFreetext.Text = LRS(40021) & " " & sFreetext
        End If

        sFreetext = vbNullString
        sFreetext = nodTransaction.selectSingleNode("child::*[name()='PurchaseDate']").nodeTypedValue 'PurchaseDate - Kj�psdato
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "PurchaseDate: " & sFreetext
        End If

        ' added 25.10.2017
        sFreetext = vbNullString
        sFreetext = nodTransaction.selectSingleNode("child::*[name()='CardScheme']").nodeTypedValue 'VI, MS, etc
        If Not EmptyString(sFreetext) Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "CardType: " & sFreetext
        End If

        ' 04.06.2014
        ' To be able to match on "structured" info, pick up this here
        ' This is case-dependant
        Select Case oCargo.Special
            Case "JAPANPHOTO NETTHANDEL"
                sInvoiceNo = nodTransaction.selectSingleNode("child::*[name()='TransactionRefNum1']").nodeTypedValue 'TransRef - Transaksjonsreferanse
        End Select

        ReadTellerXMLNew_2015 = bReturnValue

        On Error GoTo 0
        Exit Function

ERR_ReadTellerXMLNew:

        If Not nodTransaction Is Nothing Then
            nodTransaction = Nothing
        End If

        Err.Raise(Err.Number, "Invoice.ReadTellerXMLNEW_2015", Err.Description)

    End Function
    '    Private Function ReadAdyen_XML(ByVal nodInvoice As System.Xml.XmlNode) As Boolean
    '        Dim oFreeText As Freetext
    '        Dim bReturnValue As Boolean
    '        Dim nodeFieldList As System.Xml.XmlNodeList
    '        Dim nodeField As System.Xml.XmlNode
    '        Dim lNodeFieldCounter As Long
    '        Dim sOuterXML As String = ""
    '        Dim sTmp As String = ""

    '        On Error GoTo ERR_ReadAdyen


    '        If nodInvoice.OuterXml.IndexOf("Balancetransfer") > -1 Then
    '            ' do not use this one!
    '        Else
    '            ' all elements are named Field, with <Title> <Type> <Text>
    '            nodeFieldList = nodInvoice.selectNodes("field")

    '            For lNodeFieldCounter = 0 To nodeFieldList.Count - 1
    '                nodeField = nodeFieldList.Item(lNodeFieldCounter)
    '                sOuterXML = nodeField.OuterXml()

    '                If sOuterXML.IndexOf("Company Account") > -1 Then
    '                    sExtra1 = nodeField.InnerText
    '                End If

    '                If sOuterXML.IndexOf("Psp Reference") > -1 Then
    '                    ' dette er en intern Adyen referanse
    '                    sREF_Own = nodeField.InnerText
    '                End If

    '                If sOuterXML.IndexOf("Merchant Reference") > -1 Then
    '                    If sOuterXML.IndexOf("Modification Merchant Reference") = -1 Then ' Finnes b�de Field Modification Merch .. + kun Merchant  Re...
    '                        ' dette er sannsynligvis gjennoml�pende referanse
    '                        sInvoiceNo = nodeField.InnerText
    '                    End If
    '                End If

    '                If sOuterXML.IndexOf("Payment Method") > -1 Then
    '                    sREF_Bank = nodeField.InnerText
    '                End If

    '                If sOuterXML.IndexOf("Creation Date") > -1 Then
    '                    sTmp = nodeField.InnerText
    '                    If sTmp <> "" Then
    '                        oCargo.DATE_Payment = Left(sTmp, 4) & Mid(sTmp, 6, 2) & Mid(sTmp, 9, 2)
    '                    Else
    '                        oCargo.DATE_Payment = Year(Now).ToString & Month(Now).ToString & VB.Day(Now).ToString
    '                    End If
    '                End If

    '                'If sOuterXML.IndexOf("Type") > -1 Then
    '                '    ' avvent denne - men her kommer kanskje Fee som egen?
    '                '    sE_AccountPrefix = nodeField.InnerText
    '                'End If

    '                ' Trenger vi disse?
    '                'Modification Reference

    '                If sOuterXML.IndexOf("Gross Currency") > -1 Then
    '                    oCargo.MONCurrency = nodeField.InnerText
    '                End If

    '                If sOuterXML.IndexOf("Gross Debit (GC)") > -1 Then
    '                    If Not EmptyString(nodeField.InnerText) Then
    '                        nMON_InvoiceAmount = ConvertToAmount(nodeField.InnerText, "", ".") * -1
    '                        nMON_TransferredAmount = nMON_InvoiceAmount
    '                    End If
    '                End If
    '                If sOuterXML.IndexOf("Gross Credit (GC)") > -1 Then
    '                    If Not EmptyString(nodeField.InnerText) Then
    '                        nMON_InvoiceAmount = ConvertToAmount(nodeField.InnerText, "", ".")
    '                        nMON_TransferredAmount = nMON_InvoiceAmount
    '                    End If
    '                End If
    '                'If sOuterXML.IndexOf("Net Debit (NC)") > -1 Then
    '                '    ' bel�p minus provisjon
    '                '    If Not EmptyString(nodeField.InnerText) Then
    '                '        nMON_TransferredAmount = ConvertToAmount(nodeField.InnerText, "", ".") * -1
    '                '    End If
    '                'End If
    '                'If sOuterXML.IndexOf("Net Credit (NC)") > -1 Then
    '                '    ' bel�p minus provisjon
    '                '    If Not EmptyString(nodeField.InnerText) Then
    '                '        nMON_TransferredAmount = ConvertToAmount(nodeField.InnerText, "", ".")
    '                '    End If
    '                'End If

    '                If sOuterXML.IndexOf("Commission (NC)") > -1 Then
    '                    ' provisjon
    '                    If Not EmptyString(nodeField.InnerText) Then
    '                        nMON_DiscountAmount = ConvertToAmount(nodeField.InnerText, "", ".")
    '                    End If
    '                End If
    '            Next lNodeFieldCounter

    '            ' for � ikke f� negative bel�p, sett balanseposten til 0
    '            'If sE_AccountPrefix = "Balancetransfer" Then
    '            ' ---> SKAL VI LAGE NY PAYMENT HER?????
    '            'nMON_InvoiceAmount = 0
    '            'nMON_TransferredAmount = 0
    '            'End If


    '            ' 29.08.2017 add freetext 
    '            oFreeText = oFreetexts.Add
    '            oFreeText.Text = sInvoiceNo & "  -  " & PadLeft(Format(nMON_InvoiceAmount / 100, "#,##0.00"), 12, " ") & "  " & LRS(40043) & " " & Format(nMON_DiscountAmount / 100, "#,##0.00") & " - " & sREF_Bank

    '        End If

    '        Exit Function


    'ERR_ReadAdyen:

    '        If Not nodInvoice Is Nothing Then
    '            nodInvoice = Nothing
    '        End If

    '        Err.Raise(Err.Number, "Invoice.ReadAdyen_XML", Err.Description)

    '    End Function


    Private Function ReadVippsXML(ByVal nodTransaction As System.Xml.XmlNode) As Boolean
        Dim oFreetext As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim nodTemp As System.Xml.XmlNode

        Try

            bReturnValue = True

            sREF_Own = GetInnerText(nodTransaction, "child::*[name()='TransactionID']", Nothing)
            ' 14.05.2019 - byttet om Gross og Net amount
            ' 21.12.2020 - byttet tilbake i standard, men beholde nette for Haraldsplass
            '           tror det kun er Helse Vest som benytter Vipps p.t.
            If oCargo.Special = "HELSEVEST_VIPPS" Then
                nMON_InvoiceAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionNetAmount']", Nothing), "", ".", True)
                nMON_TransferredAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionGrossAmount']", Nothing), "", ".", True) 'NetAmount - Netto kj�psbel�p
            Else
                nMON_TransferredAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionNetAmount']", Nothing), "", ".", True)
                nMON_InvoiceAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionGrossAmount']", Nothing), "", ".", True) 'NetAmount - Netto kj�psbel�p
            End If
            nMON_DiscountAmount = ConvertToAmount(GetInnerText(nodTransaction, "child::*[name()='TransactionFeeAmount']", Nothing), "", ".", True)  ' Gebyr

            ' Unique EndTOEnd ref pr transaction - use for matching
            ' 14.05.2019
            nodTemp = nodTransaction.SelectSingleNode("child::*[name()='OrderID']")
            If Not nodTemp Is Nothing Then
                'sUnique_Id = nodTransaction.selectSingleNode("child::*[name()='OrderID']").nodeTypedValue
                sInvoiceNo = GetInnerText(nodTransaction, "child::*[name()='OrderID']", Nothing)
            Else
                'sUnique_Id = sREF_Own
                sInvoiceNo = sREF_Own
            End If
            ' 03.11.2020 - HelseVest, Haraldsplass, konverterer fra Vipps til OCR. Trenger KID
            If oCargo.Special = "HELSEVEST_VIPPS" Then
                sUnique_Id = sInvoiceNo
            End If

            If oCargo.Special = "GJENSIDIGE_VIPPS" Then
                sUnique_Id = sInvoiceNo
            End If

            oFreetext = oFreetexts.Add()
            oFreetext.Text = "SettlementID: " & oCargo.REF

            ' 14.05.2019
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "OrderID: " & sInvoiceNo

            'sFreetext = nodTransaction.selectSingleNode("child::*[name()='OrderID']").nodeTypedValue
            sFreetext = sREF_Own
            If Not EmptyString(sFreetext) Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = "TransactionID: " & sFreetext
            End If

            sFreetext = vbNullString
            sFreetext = ConvertFromAmountToString(nMON_TransferredAmount, , ",")
            If Not EmptyString(sFreetext) Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = LRS(40143) & " " & sFreetext & " " & LRS(40144) & " " & ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
            End If

            ReadVippsXML = bReturnValue

        Catch ex As Exception

            If Not nodTransaction Is Nothing Then
                nodTransaction = Nothing
            End If

            Err.Raise(Err.Number, "Invoice.ReadVippsXML", Err.Description)
        End Try

    End Function
    Private Function SLETT_ReadVippsXML(ByVal nodTransaction As MSXML2.IXMLDOMNode) As Boolean
        Dim oFreetext As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim nodTemp As MSXML2.IXMLDOMNode

        Try

            bReturnValue = True

            sREF_Own = nodTransaction.selectSingleNode("child::*[name()='TransactionID']").nodeTypedValue
            ' 14.05.2019 - byttet om Gross og Net amount
            nMON_TransferredAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='TransactionNetAmount']").nodeTypedValue, "", ".", True)
            nMON_InvoiceAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='TransactionGrossAmount']").nodeTypedValue, "", ".", True) 'NetAmount - Netto kj�psbel�p

            nMON_DiscountAmount = ConvertToAmount(nodTransaction.selectSingleNode("child::*[name()='TransactionFeeAmount']").nodeTypedValue, "", ".", True)  ' Gebyr

            ' Unique EndTOEnd ref pr transaction - use for matching
            ' 14.05.2019
            nodTemp = nodTransaction.selectSingleNode("child::*[name()='OrderID']")
            If Not nodTemp Is Nothing Then
                'sUnique_Id = nodTransaction.selectSingleNode("child::*[name()='OrderID']").nodeTypedValue
                sInvoiceNo = nodTransaction.selectSingleNode("child::*[name()='OrderID']").nodeTypedValue
            Else
                'sUnique_Id = sREF_Own
                sInvoiceNo = sREF_Own
            End If
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "SettlementID: " & oCargo.REF

            ' 14.05.2019
            oFreetext = oFreetexts.Add()
            oFreetext.Text = "OrderID: " & sInvoiceNo

            'sFreetext = nodTransaction.selectSingleNode("child::*[name()='OrderID']").nodeTypedValue
            sFreetext = sREF_Own
            If Not EmptyString(sFreetext) Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = "TransactionID: " & sFreetext
            End If

            sFreetext = vbNullString
            sFreetext = ConvertFromAmountToString(nMON_TransferredAmount, , ",")
            If Not EmptyString(sFreetext) Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = LRS(40143) & " " & sFreetext & " " & LRS(40144) & " " & ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
            End If

            SLETT_ReadVippsXML = bReturnValue

        Catch ex As Exception

            If Not nodTransaction Is Nothing Then
                nodTransaction = Nothing
            End If

            Err.Raise(Err.Number, "Invoice.ReadVippsXML", Err.Description)
        End Try

    End Function
    Private Function ReadFirst_Union_ACH() As String
        Dim oFreeText As Freetext
        Dim bReadLine As Boolean
        'Dim bCreateObject As Boolean
        'Dim bFromLevelUnder As Boolean

        Select Case Mid(sImportLineBuffer, 41, 8)


            'BETFOR04 Invoice international
            Case "BETFOR04"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 151, 15))
                If Mid(sImportLineBuffer, 166, 1) <> "D" Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
                bAmountSetInvoice = True
                'nMON_InvoiceAmount = nMON_InvoiceAmount / 100
                sREF_Own = Mid(sImportLineBuffer, 116, 35)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                'sInvoiceNo - Will be added later. Have to be extracted form freetext.
                'sCustomerNo - Will be added later. Have to be extracted form freetext.
                'sUnique_Id = Not used in international payments
                sSTATEBANK_Code = Trim(Mid(sImportLineBuffer, 167, 6))
                sSTATEBANK_Text = Mid(sImportLineBuffer, 173, 60)
                'sExtra1 - Not used in this format
                'Add freetext, one line.
                If Trim(Mid(sImportLineBuffer, 81, 35)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 81, 35) & Space(5)
                End If
                bReadLine = True

            Case "BETFOR21"
                If Mid(sImportLineBuffer, 267, 1) = "E" Then
                    sStatusCode = Mid(sImportLineBuffer, 4, 2)
                    If Val(sStatusCode) > 2 Then
                        ' Rejected, move up in the ladder ...
                        oCargo.RejectsExists = True
                    End If

                    nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 249, 15))
                    bAmountSetInvoice = True
                    bReadLine = True
                Else
                    'Something wrong in the format
                End If

                'BETFOR22 Mass payment
            Case "BETFOR22"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                If Mid(sImportLineBuffer, 137, 1) = "S" Then
                    bCancel = True
                End If
                sREF_Own = Mid(sImportLineBuffer, 138, 35)
                ' Two ownrefs in BETFOR22!
                ' Use Extra1 to store second ownref (10 characters)
                sExtra1 = Mid(sImportLineBuffer, 283, 10)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 122, 15))
                bAmountSetInvoice = True
                'nMON_InvoiceAmount = nMON_InvoiceAmount / 100
                bReadLine = True


                'BETFOR23 Invoice domestic
            Case "BETFOR23"
                sStatusCode = Mid(sImportLineBuffer, 4, 2)
                If Val(sStatusCode) > 2 Then
                    ' Rejected, move up in the ladder ...
                    oCargo.RejectsExists = True
                End If

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 258, 15))
                If Mid(sImportLineBuffer, 273, 1) <> "D" Then
                    nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                End If
                bAmountSetInvoice = True
                'nMON_InvoiceAmount = nMON_InvoiceAmount / 100
                sREF_Own = Mid(sImportLineBuffer, 228, 30)
                sInvoice_ID = Mid(sImportLineBuffer, 75, 6)
                'sInvoiceNo - Will be added later. Have to be extracted form freetext.
                'sCustomerNo - Will be added later. Have to be extracted form freetext.
                sUnique_Id = Mid(sImportLineBuffer, 201, 27)
                'sSTATEBANK_Code - Not used in domestic payments
                'sSTATEBANK_Text - Not used in domestic payments
                'sExtra1 - Not used in this format
                'Add freetext, up to three lines in the format
                If Trim(Mid(sImportLineBuffer, 81, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 81, 40)
                End If
                If Trim(Mid(sImportLineBuffer, 121, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 121, 40)
                End If
                If Trim(Mid(sImportLineBuffer, 161, 40)) <> "" Then
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 161, 40)
                End If
                bReadLine = True

                'BETFOR23 Invoice domestic
            Case "BETFOR99"
                'New code 10.07.2002
                'Specialcase from DnB when the last transaction is deleted (I think online)
                'We have to test if the transaction is deleted, if so we accept it
                'FIX: To be 100% correct we should also test that it is from DnB.
                If bCancel = True Then
                    bReadLine = False
                Else
                    'ERROR
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-004")
                End If

                'All other content would indicate error
            Case Else
                'All other BETFOR-types gives error
                'Error # 10010-004
                Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-004")


        End Select

        'New code 10.07.2002
        If bReadLine Then
            'Read new record
            sImportLineBuffer = ReadFirst_Union_ACHRecord(oFile) ', oProfile)
        End If

        ReadFirst_Union_ACH = sImportLineBuffer

    End Function
    Private Function ReadOrklaMediaExcelFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String


        nMON_InvoiceAmount = Val(Trim(xDelim(sImportLineBuffer, ";", 8))) * 100
        bAmountSetInvoice = True
        'sREF_Own = Mid$(sImportLineBuffer, 72, 25)

        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups().Message. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sMessage = oProfile.FileSetups(FileSetup_ID()).Message

        If InStr(1, sMessage, "#aksjeantall#") > 0 Then
            sMessage = Replace(sMessage, "#aksjeantall#", xDelim(sImportLineBuffer, ";", 9))
        End If
        oFreeText = oFreetexts.Add()
        oFreeText.Text = Left(PadRight(sMessage, 40, " "), 40)
        oFreeText = oFreetexts.Add()
        oFreeText.Text = Mid(PadRight(sMessage, 80, " "), 41, 40)


        'UPGRADE_WARNING: Couldn't resolve default property of object ReadOrklaMediaExcelRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = ReadOrklaMediaExcelRecord(oFile) ', oProfile)


        ReadOrklaMediaExcelFile = sImportLineBuffer

    End Function
    Private Function ReadExcelFile() As String
        ' Common importroutine for several Excel-files
        ' Special in setup decides format (oBabelfile.Special)
        Dim oFreeText As Freetext
        Dim sText As String

        Select Case UCase(oCargo.Special)

            Case "SGFINANS"
                ' 10. Amount
                nMON_InvoiceAmount = CDbl(Trim(xDelim(sImportLineBuffer, ";", 10)))
                bAmountSetInvoice = True
                ' 9. Notification
                sText = PadRight(xDelim(sImportLineBuffer, ";", 9), 40, " ")
                oFreeText = oFreetexts.Add()
                oFreeText.Text = sText
                '--- End SGFINANS ---

            Case "LITRA", "JOKER"

                nMON_InvoiceAmount = CDbl(Trim(xDelim(sImportLineBuffer, ";", 5))) * 100
                bAmountSetInvoice = True


                'oFreeText.Text = "Utbytte for " & xDelim(sImportLineBuffer, ";", 3) & " aksjer � kr. " & xDelim(sImportLineBuffer, ";", 4)
                ' Changed 20.05.05 - problems if long freetext
                sText = "Utbytte for " & xDelim(sImportLineBuffer, ";", 3) & " aksjer � kr. " & xDelim(sImportLineBuffer, ";", 4)
                ' Remove whitespace
                sText = Replace(sText, "     ", "")
                If Len(sText) > 35 Then
                    Do
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = PadRight(Left(sText, 35), 40, " ")
                        sText = Mid(sText, 36)
                        If Len(sText) = 0 Then
                            Exit Do
                        End If
                    Loop

                Else
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = sText
                End If
                '--- End LITRA, JOKER ---

            Case "PLANBO"

                nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 7), "", ".,"))
                bAmountSetInvoice = True

                oFreeText = oFreetexts.Add()
                ' If col 10 filled in, set in first freetext:
                If Len(Trim(xDelim(sImportLineBuffer, ";", 10))) > 0 Then
                    If xDelim(sImportLineBuffer, ";", 10) <> "" Then
                        oFreeText.Text = Trim(xDelim(sImportLineBuffer, ";", 10))
                        oFreeText = oFreetexts.Add()
                    End If
                End If
                oFreeText.Text = "Leil.nr: " & xDelim(sImportLineBuffer, ";", 2) & " Utleieperiode " & xDelim(sImportLineBuffer, ";", 9)
                '--- End PLANBO ---

            Case "SANDVIK TAMROCK"
                nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 2, "'"), "", ".,"))
                bAmountSetInvoice = True
                ' Changed 14.03.06
                ' Has added new column, 12, with KID (preceeded by *
                If Left(xDelim(sImportLineBuffer, ",", 12, "'"), 1) = "*" Then
                    sUnique_Id = Mid(xDelim(sImportLineBuffer, ",", 12, "'"), 2)
                Else
                    ' as pre 14.03.06
                    sInvoiceNo = xDelim(sImportLineBuffer, ",", 8, "'")
                End If
                '--- End SANDVIK TAMROCK ---

            Case "�STLENDINGEN"

                nMON_InvoiceAmount = CDbl(Trim(xDelim(sImportLineBuffer, ";", 2))) * 100
                bAmountSetInvoice = True

                oFreeText = oFreetexts.Add()
                oFreeText.Text = xDelim(sImportLineBuffer, ";", 8)
                '--- End �stlendingen ---

            Case "IST" ' IST Norway, J�rgen Hellerstedt

                nMON_InvoiceAmount = CDbl(Trim(xDelim(sImportLineBuffer, ";", 11)))
                bAmountSetInvoice = True

                ' Use KID or structured invoiceNo
                If Not EmptyString(Trim(xDelim(sImportLineBuffer, ";", 9))) Then 'KID
                    ' KID
                    sUnique_Id = Trim(xDelim(sImportLineBuffer, ";", 9))
                Else
                    ' Structured invoiceno
                    If Not EmptyString(Trim(xDelim(sImportLineBuffer, ";", 8))) Then
                        sInvoiceNo = Trim(xDelim(sImportLineBuffer, ";", 8))
                    Else
                        ' In case there is no KID and no InvNo
                        sInvoiceNo = "0"
                    End If
                End If
                '--- IST ---


            Case "TANDBERG_ACH"
                nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 3), "", ".,"))
                bAmountSetInvoice = True


                ' Must decide if Domestic (in Singapore) or International payment.
                ' Domestic has 12 columns, Intl har 13
                'If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) = 11 Then
                If EmptyString(xDelim(sImportLineBuffer, ";", 13)) Then
                    ' Domestic Sinagpore payment;
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = xDelim(sImportLineBuffer, ";", 10) '''& " " & xDelim(sImportLineBuffer, ";", 12)
                    sREF_Own = xDelim(sImportLineBuffer, ";", 11)
                End If
                If EmptyString((oFreeText.Text)) Then
                    oFreeText.Text = "-"
                End If
                '- End Tandbeg_ACH

            Case "NESPRESSO"
                sText = xDelim(sImportLineBuffer, ";", 4)
                sText = Left(sText, Len(sText) - 4)
                nMON_InvoiceAmount = CDbl(ConvertToAmount(sText, ".", ",")) ' Amount

                bAmountSetInvoice = True
                sUnique_Id = Trim(xDelim(sImportLineBuffer, ";", 9))

                ' Structured invoiceno?
                If oCargo.PayType <> "I" And Len(Trim(xDelim(sImportLineBuffer, ";", 10))) < 21 Then
                    sInvoiceNo = Trim(xDelim(sImportLineBuffer, ";", 10))
                Else
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Trim(xDelim(sImportLineBuffer, ";", 10))
                End If
                '--- End NESPRESSO ---
            Case "FYSIO"

                nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 12), "", ".,"))
                bAmountSetInvoice = True

                '    Set oFreeText = oFreetexts.Add()
                '    ' If col 10 filled in, set in first freetext:
                '    If Len(Trim$(xDelim(sImportLineBuffer, ";", 10))) > 0 Then
                '        If xDelim(sImportLineBuffer, ";", 10) <> "" Then
                '            oFreeText.Text = Trim$(xDelim(sImportLineBuffer, ";", 10))
                '            Set oFreeText = oFreetexts.Add()
                '        End If
                '    End If
                '    oFreeText.Text = "Leil.nr: " & xDelim(sImportLineBuffer, ";", 2) & " Utleieperiode " & xDelim(sImportLineBuffer, ";", 9)
                '--- FYSIO ---

                'XNET - 20.05.2011 - Added next case
            Case "ODIN_UTBETALING"
                ' 3. Amount
                nMON_InvoiceAmount = ConvertToAmount(Trim$(xDelim(sImportLineBuffer, ";", 3)), "", ",")
                bAmountSetInvoice = True
                ' 6. Notification
                sText = PadRight(xDelim(sImportLineBuffer, ";", 6, Chr(34)), 40, " ")
                oFreeText = oFreetexts.Add()
                oFreeText.Text = sText
                ' 8. EGENREFERANSE
                sREF_Own = xDelim(sImportLineBuffer, ";", 8, Chr(34)) & "-" & Trim(Str(nIndex))

                '--- End ODIN_UTBETALING ---

            Case "ODIN_UTBETALING_UTLAND"
                ' 3. Amount
                nMON_InvoiceAmount = ConvertToAmount(Trim$(xDelim(sImportLineBuffer, ";", 3)), "", ",")
                bAmountSetInvoice = True
                ' 12. MELDINGTILMOTTAKER
                sText = PadRight(xDelim(sImportLineBuffer, ";", 12, Chr(34)), 40, " ")
                oFreeText = oFreetexts.Add()
                oFreeText.Text = sText
                ' 13. BETALINGSARTKODENORGESBANK
                sSTATEBANK_Code = xDelim(sImportLineBuffer, ";", 13, Chr(34))
                ' 14. BEL�PETGJELDER_NORGESBANK
                sSTATEBANK_Text = xDelim(sImportLineBuffer, ";", 14, Chr(34))
                'XNET 14.09.2011 - Added next line
                ' 32. EGENREFERANSE
                sREF_Own = xDelim(sImportLineBuffer, ";", 32, Chr(34)) & "-" & Trim(Str(nIndex))
                '--- End ODIN_UTBETALING_UTLAND ---

                'XNET - 09.05.2012 - Added next case
            Case "ENERGIKONTO_UTBET"
                ' 1. Amount
                nMON_InvoiceAmount = ConvertToAmount(Trim$(xDelim(sImportLineBuffer, ";", 1)), "", ".")
                bAmountSetInvoice = True

                '12.04.2021 Added the use of KID
                '  
                sText = xDelim(sImportLineBuffer, ";", 16).Trim

                If EmptyString(sText) Then
                    ' 9. MELDINGTILMOTTAKER
                    sText = xDelim(sImportLineBuffer, ";", 9)
                    sText = sText & xDelim(sImportLineBuffer, ";", 15)
                    If Not EmptyString(sText) Then
                        oFreeText = oFreetexts.Add()
                        '23.11.2015 - Changed according to instructions from Odd Narve
                        oFreeText.Text = "Gjelder fakturanummer " & sText & ", "
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = xDelim(sImportLineBuffer, ";", 1).Trim & " kroner."
                        'oFreeText.Text = "KID: " & sText
                    End If
                    sText = xDelim(sImportLineBuffer, ";", 12) 'Referanse
                    If Not EmptyString(sText) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left("Referanse: " & sText, 40)
                    End If
                    sText = xDelim(sImportLineBuffer, ";", 13) 'Beskrivelse
                    If Not EmptyString(sText) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left("Beskrivelse: " & sText, 40)
                    End If
                Else
                    sUnique_Id = sText
                End If
                '--- End ENERGIKONTO_UTBET ---

            Case "PROFUNDO"
                ' 3. Amount
                nMON_InvoiceAmount = ConvertToAmount(Trim$(xDelim(sImportLineBuffer, ",", 3)), "", ",")
                bAmountSetInvoice = True
                ' 4. Notification
                sText = PadRight(xDelim(sImportLineBuffer, ",", 4, Chr(34)), 100, " ").Trim
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(sText, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sText, 41)

                ' 1. EGENREFERANSE
                sREF_Own = xDelim(sImportLineBuffer, ",", 1)

                '--- End PROFUNDO ---

            Case "FREUDENBERG"
                ' 2. Self reference invoice
                sREF_Own = xDelim(sImportLineBuffer, ";", 2, Chr(34))
                ' 19. Declaration Code
                sSTATEBANK_Code = xDelim(sImportLineBuffer, ";", 19, Chr(34))
                ' 20. Information to National Bank
                sSTATEBANK_Text = xDelim(sImportLineBuffer, ";", 20, Chr(34))
                ' 22. Amount
                nMON_InvoiceAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 22), "", ",")
                bAmountSetInvoice = True
                ' 26. Invoice number
                sInvoiceNo = xDelim(sImportLineBuffer, ";", 26, Chr(34))
                ' 27. Supplier number
                sCustomerNo = xDelim(sImportLineBuffer, ";", 27, Chr(34))
                ' 28. Message to receiver
                sText = xDelim(sImportLineBuffer, ";", 28, Chr(34))
                If Not EmptyString(sText) Then
                    Do While True
                        If sText.Length > 40 Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = sText.Substring(0, 40)
                            sText = sText.Substring(40)
                        Else
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = sText
                            sText = ""
                        End If
                        If EmptyString(sText) Then
                            Exit Do
                        End If
                    Loop
                End If
                ' 29. KID
                sUnique_Id = xDelim(sImportLineBuffer, ";", 29, Chr(34))

                '--- End FREUDENBERG ---

            Case "JDE_GJENSIDIGE_USERS"
                Dim sTmp As String = ""
                Dim i As Integer = 0
                ' import freetextlines, 
                ' 13	Basware_Grupper	GROUP	ExternalGroupCode	Kan v�re flere verdier separert av komma
                ' ------------> TROR DETTE SKAL V�RE 12
                sText = xDelim(sImportLineBuffer, ";", 12, Chr(34))
                i = 0
                Do
                    i = i + 1
                    sTmp = xDelim(sText, ",", i)
                    If sTmp <> "" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = sTmp
                        sTmp = ""
                    Else
                        Exit Do
                    End If
                Loop
                ' --- End "JDE_GJENSIDIGE_USERS" ---

            Case "JDE_GJENSIDIGE_USERS_NYDRM"
                ' 15.12.2020 - comma as delimiter
                Do
                    ' import freetextlines, 
                    ' 13	Basware_Grupper	GROUP	ExternalGroupCode	Kan v�re flere verdier separert av komma
                    ' ------------> TROR DETTE SKAL V�RE 12
                    sText = xDelim(sImportLineBuffer, ",", 12, Chr(34))
                    ' 10.06.2021 - problems with ������
                    sText = ConvertCharactersFromUTF8ToANSI_NO(sText)

                    oCargo.Temp = xDelim(sImportLineBuffer, ",", 1, Chr(34))  ' added 02.11.2020
                    If Not EmptyString(sText) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = sText
                    End If

                    sImportLineBuffer = ReadGenericRecord(oFile)
                    ' 02.11.2020 - added test for first column
                    ' do not create new invoice if same LoginId (column 1)
                    If UCase(oCargo.Special) = "JDE_GJENSIDIGE_USERS_NYDRM" Then
                        If oCargo.Temp <> xDelim(sImportLineBuffer, ",", 1, Chr(34)) Then
                            ' jump out to create new payment for new login id
                            Exit Do
                        End If
                    End If

                Loop
                ' --- End "JDE_GJENSIDIGE_USERS_NYDRM" ---

        End Select

        If UCase(oCargo.Special) <> "JDE_GJENSIDIGE_USERS_NYDRM" Then   ' added 02.11.2020
            sImportLineBuffer = ReadGenericRecord(oFile)
        End If


        ReadExcelFile = sImportLineBuffer

    End Function
    Friend Function ReadDatabase(ByRef oMyDal As vbBabel.DAL, ByRef sMySQL As String) As Boolean
        Dim oFreeText As vbBabel.Freetext
        Dim sText As String
        Dim bReturnValue As Boolean = True
        Dim iPos As Integer

        'Try  - 02.02.2021 - Removed ErrorHandling because it destoys the info created in payment

        If InStr(1, sMySQL, "BBRET_Amount", CompareMethod.Text) > 0 Then
            If oCargo.Special = "SG_REVISJON_SALDO" Or oCargo.Special = "SG_REVISJON_OPENITEMS" Then
                sText = oMyDal.Reader_GetString("BBRET_Amount")
                'MsgBox("Verdien er: " & sText)
                sText = Trim(Replace(oMyDal.Reader_GetString("BBRET_Amount"), ",", "."))
                'MsgBox("Verdien er: " & sText)
                If Not EmptyString(sText) Then
                    If sText.Contains(".") Then  ' (".") then
                        iPos = InStr(sText, ".")
                        sText = sText.Substring(0, iPos - 1)
                    End If
                End If

                nMON_InvoiceAmount = CDbl(sText)
                'MsgBox("Detta gikk bra?")

                'MsgBox("Ny verdi er: " & nMON_InvoiceAmount.ToString)
            Else
                nMON_InvoiceAmount = CDbl(Trim(Replace(oMyDal.Reader_GetString("BBRET_Amount"), ",", ".")))
            End If
            bAmountSetInvoice = True
        End If
        If InStr(1, sMySQL, "BBRET_UniqueID", CompareMethod.Text) > 0 Then
            sUnique_Id = oMyDal.Reader_GetString("BBRET_UniqueID")
        End If
        If InStr(1, sMySQL, "BBRET_InvoiceNo", CompareMethod.Text) > 0 Then
            sInvoiceNo = oMyDal.Reader_GetString("BBRET_InvoiceNo")
        End If
        If InStr(1, sMySQL, "BBRET_CustomerNo", CompareMethod.Text) > 0 Then
            sCustomerNo = oMyDal.Reader_GetString("BBRET_CustomerNo")
        End If

        If InStr(1, sMySQL, "BBRET_REFOwn", CompareMethod.Text) > 0 Then
            sREF_Own = oMyDal.Reader_GetString("BBRET_REFOwn")
        End If

        If EmptyString(sUnique_Id) Or Left(UCase(oCargo.Special), 19) = "NORDEA LIV HENTDATA" Then
            If InStr(1, sMySQL, "BBRET_Freetext", CompareMethod.Text) > 0 Then

                sText = RTrim(oMyDal.Reader_GetString("BBRET_Freetext"))

                Do While Len(sText) > 0
                    If Len(sText) > 0 Then

                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left(sText, 40)
                    End If
                    sText = Mid(sText, 41)
                Loop
            End If
        End If

        sSTATEBANK_Code = oMyDal.Reader_GetString("BBRET_StateBankCode")
        sSTATEBANK_Text = oMyDal.Reader_GetString("BBRET_StateBankText")

        If oCargo.Special.ToUpper = "AEROGULF" Then
            ' 02.03.2015 - "Juksel�sning" for � legge p� NorgesBank deklareringskode dersom ikke finnes fra f�r
            If sSTATEBANK_Code = "" Then
                sSTATEBANK_Code = "14"
                sSTATEBANK_Text = "Kj�p og salg av varer"
            End If

        End If

        'Catch ex As Exception

        '    bReturnValue = False
        '    Throw New Exception("Function: ReadDatabase_Invoice" & vbCrLf & ex.Message)

        'End Try

        ReadDatabase = True

    End Function
    Private Function ReadExcelInnbetalingFile() As String
        ' Common importroutine for several Excel-files
        ' Special in setup decides format (oBabelfile.Special)
        Dim oFreeText As Freetext
        Dim sText As String
        Dim sTemp1 As String
        ' 03.06.2008 by JanP
        ' Added sDelimiter, beacuse it may be TAB for some customers and ; for other
        Dim sDelimiter As String

        sDelimiter = sDelimiter
        ' May also be " as surround
        Dim sSurround As String

        sDelimiter = ";"
        sSurround = Chr(34)
        If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, sDelimiter, "")) < 10 Then
            ' not "enough" ; to be delimiter, try TAB instead!
            sDelimiter = Chr(9)
        End If
        If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, sSurround, "")) < 10 Then
            ' not "enough" " to be surround
            sSurround = "" ' nothing
        End If

        ' skj�nner ikke en dritt av hvorfor, men det er = foran neste alle felt hos KN
        ' ="0000.00.00000"    ="TANUM MENIGHET"   ="NEDRE �SVEI 36"           ="1341 SLEPENDEN"   ="1594.22.87248"    ="KIRKENS N�DHJELP/ NORWEGIAN CH"   ="POSTBOKS 7100, ST.OLAVS PLASS"            ="0130 OSLO"    30.04.2008  3.851,00    ="NOK"  1,0000  3.851,00    ="NOK"  ="636423"   30.04.2008  ="0"    ="offer 23/3-08                                          3.851,00"
        sImportLineBuffer = Replace(sImportLineBuffer, "=" & sSurround, sSurround)

        Select Case oCargo.Bank

            Case BabelFiles.Bank.DnB
                ' 21.08.2008 Moved bel�p from 14 to 17
                '14. Bel�p7
                sTemp1 = xDelim(sImportLineBuffer, sDelimiter, 17, sSurround)
                nMON_InvoiceAmount = CDbl(ConvertToAmount(sTemp1, ".", ","))
                nMON_TransferredAmount = nMON_InvoiceAmount
                ' 21.08.2008 moved Melding from 22 to 31
                ' 31. Notification
                sText = xDelim(sImportLineBuffer, sDelimiter, 31, sSurround)
                ' Melding til mottaker inntil 255 tegn, del opp i 40 og 40
                Do While Len(sText) > 0
                    If Len(sText) > 0 Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left(sText, 40)
                    End If
                    sText = Mid(sText, 41)
                Loop
                '--- End DNBNOR INNBETALING ---

        End Select

        sImportLineBuffer = ReadGenericRecord(oFile)
        ReadExcelInnbetalingFile = sImportLineBuffer

    End Function
    Private Function ReadExcelXLS() As String
        ' Common importroutine for several ExcelXLS-files
        ' Special in setup decides format (oBabelfile.Special)
        Dim oFreeText As Freetext
        Dim sText As String
        Dim lNoOfColumns As Integer

        Select Case UCase(oCargo.Special)

            Case "DNBNOR_NIBS"
                lNoOfColumns = 34
                'Col 13 Amount
                nMON_InvoiceAmount = CDbl(Trim(xDelim(sImportLineBuffer, ";", 13))) * 100
                bAmountSetInvoice = True

                oFreeText = oFreetexts.Add()
                oFreeText.Text = Trim(xDelim(sImportLineBuffer, ";", 5))

                ' Varsling Norges Bank
                sSTATEBANK_Code = Trim(xDelim(sImportLineBuffer, ";", 30))
                sSTATEBANK_Text = Trim(xDelim(sImportLineBuffer, ";", 31))

                '--- DNBNOR NIBS -----


        End Select

        sImportLineBuffer = ReadLineFromExcel(oXLWSheet, lNoOfColumns)
        ReadExcelXLS = sImportLineBuffer

    End Function

    Private Function ReadControlConsultExcelFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim sAmount As String
        Dim nAmount As Decimal

        If xDelim(sImportLineBuffer, ";", 1) = "T" Then
            ' new format, 13.03.06
            nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 22), "", ".,"))
            bAmountSetInvoice = True

            ' Set ref_own to Posenr og ref-column
            ' In payment, paycode is set to 222 (Telepay=622), which tells the bank
            ' to put Onw-ref as text on receivers statement
            sREF_Own = "Ref: " & Trim(xDelim(sImportLineBuffer, ";", 12))

            ' read another line
            If Not oFile.AtEndOfStream Then
                sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
            End If

        ElseIf xDelim(sImportLineBuffer, ";", 1) = "D" Then
            ' Detail line, not in use here
            ' read another line
            If Not oFile.AtEndOfStream Then
                sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
            End If
        Else
            ' old way, pre 13.03.06
            nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 24), "", ".,"))
            bAmountSetInvoice = True

            ' New 17.06.04:
            ' Set ref_own to Posenr og ref-column
            ' In payment, paycode is set to 222 (Telepay=622), which tells the bank
            ' to put Onw-ref as text on receivers statement
            sREF_Own = "Ref: " & Trim(xDelim(sImportLineBuffer, ";", 4))

            ' Construct info to ben from several columns;
            ' Dato : 13.02.04 Pose: 2421557 Avd: 1102
            ' Mynt : 901,50      Sedler : 14050,00
            ' Avvik: 0,50        Total  : 14951,00
            ' Line 1
            sMessage = "Dato : "
            sMessage = sMessage & xDelim(sImportLineBuffer, ";", 5) ' Date, col 5
            sMessage = sMessage & " Ref.: " ' Endret 16.06.04 "Pose: "
            sMessage = sMessage & xDelim(sImportLineBuffer, ";", 4) ' Bag.no, col 4
            sMessage = sMessage & " Avd: "
            sMessage = sMessage & xDelim(sImportLineBuffer, ";", 3) ' Avd, col 3
            oFreeText = oFreetexts.Add()
            oFreeText.Text = PadRight(sMessage, 40, " ")

            ' Line 2

            sMessage = "Mynt : "
            sAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 20), "", ".,")
            nAmount = Val(sAmount) / 100
            sAmount = VB6.Format(nAmount, "##,##0.00")
            sAmount = PadLeft(sAmount, 12, " ")
            sMessage = sMessage & sAmount

            sMessage = sMessage & " Sedler: "
            sAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 21), "", ".,")
            nAmount = Val(sAmount) / 100
            sAmount = VB6.Format(nAmount, "##,##0.00")
            sAmount = PadLeft(sAmount, 12, " ")
            sMessage = sMessage & sAmount

            oFreeText = oFreetexts.Add()
            oFreeText.Text = PadRight(sMessage, 40, " ")


            ' Line 3
            sMessage = "Avvik: "
            sAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 23), "", ".,")
            nAmount = Val(sAmount) / 100
            sAmount = VB6.Format(nAmount, "##,##0.00")
            sAmount = PadLeft(sAmount, 12, " ")
            sMessage = sMessage & sAmount


            sMessage = sMessage & " Total : "
            sAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 24), "", ".,")
            nAmount = Val(sAmount) / 100
            sAmount = VB6.Format(nAmount, "##,##0.00")
            sAmount = PadLeft(sAmount, 12, " ")
            sMessage = sMessage & sAmount
            oFreeText = oFreetexts.Add()
            oFreeText.Text = PadRight(sMessage, 40, " ")


            ' New 17.06.04, do not import empty lines
            Do Until oFile.AtEndOfStream = True
                sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                ' only import lines with an amount>0 (Field 24)
                sAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 24), "", ".,")
                nAmount = Val(sAmount) / 100
                If nAmount > 0 Then
                    Exit Do
                End If
                If Left(sImportLineBuffer, 14) = "Kontroll;;;;;;" Then
                    Exit Do
                End If
            Loop

            If Left(sImportLineBuffer, 14) = "Kontroll;;;;;;" Then
                Do Until oFile.AtEndOfStream = True
                    oFile.SkipLine()
                Loop
                sImportLineBuffer = ""
            End If

        End If

        ReadControlConsultExcelFile = sImportLineBuffer

    End Function
    Private Function ReadDnBStockholmExcelFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext


        nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 8), "", ".,"))
        bAmountSetInvoice = True

        oFreeText = oFreetexts.Add()
        oFreeText.Text = xDelim(sImportLineBuffer, ";", 11)

        sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
        If Left(sImportLineBuffer, 12) = ";;;;;;;;;;;;" Or Len(Trim(sImportLineBuffer)) = 0 Then
            Do Until oFile.AtEndOfStream = True
                oFile.SkipLine()
            Loop
            sImportLineBuffer = ""
        End If

        ReadDnBStockholmExcelFile = sImportLineBuffer

    End Function
    Private Function ReadStrommeSingapore() As String
        Dim oFreetext As Freetext
        ' XokNET 05.03.2015 - take whole function
        Dim sDelimChar As String
        Dim i As Integer

        ' XOKNET 27.04.2012
        Dim sTmp As String
        On Error GoTo ERR_ReadStromme

        nMON_InvoiceAmount = ConvertToAmount(xDelim(sImportLineBuffer, ";", 3, Chr(34)), vbNullString, ".,")
        bAmountSetInvoice = True


        ' Must decide if Domestic (in Singapore) or International payment.
        ' Domestic has 12 columns, Intl har 13
        'If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) = 11 Then
        If EmptyString(xDelim(sImportLineBuffer, ";", 13)) Then
            ' Domestic Sinagpore payment;
            oFreetext = oFreetexts.Add()
            oFreetext.Text = xDelim(sImportLineBuffer, ";", 10, Chr(34)) '''& " " & xDelim(sImportLineBuffer, ";", 12)
            sREF_Own = xDelim(sImportLineBuffer, ";", 11, Chr(34))
        Else
            'International
            oFreetext = oFreetexts.Add()
            oFreetext.Text = xDelim(sImportLineBuffer, ";", 11, Chr(34)) '''& " " & xDelim(sImportLineBuffer, ";", 13)
            sREF_Own = xDelim(sImportLineBuffer, ";", 12, Chr(34))
        End If
        If EmptyString(oFreetext.Text) Then
            'oFreeText.Text = "-"
            ' XOKNET 21.01.2013
            ' Due to problems with "-" in DNB, we change to "."
            oFreetext.Text = "."
        End If

        'XokNET - 06.07.2011 - Added next IF
        If oCargo.Special = "TAB_SEPARATED" Then
            sImportLineBuffer = ReadGenericRecord(oFile)
            sImportLineBuffer = Replace(sImportLineBuffer, Chr(9), ";")
        Else
            sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 13)
            ' XNET 04.03.2015 - next lines

            ' We are accepting both TAB, ; , as separators in new solution
            ' TAB is already accepted OK in ReadLineFromExcel
            If Not EmptyString(sImportLineBuffer) Then
                ' remove trailing ;
                For i = Len(sImportLineBuffer) To 0 Step -1
                    If Mid(sImportLineBuffer, i, 1) = ";" Then
                        sImportLineBuffer = Left(sImportLineBuffer, i - 1)
                    Else
                        Exit For
                    End If
                Next i

                ' Semicolon not - will be seen as comma in ReadLineFromExcel
                ' Same as comma sep
                ' Space sep?
                ' Mens ren Excel og TAB gir oss ; som skilletegn, og da lar vi det vare v�re som det er
                If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) > 10 Then
                    sDelimChar = ";"
                ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ",", "")) > 10 Then
                    sDelimChar = ","
                ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, " ", "")) > 10 Then
                    'sImportLineBuffer = Replace(sImportLineBuffer, " ", ";")
                    sDelimChar = " "
                End If

                ' Because we may have spaces or ;  inside an element, we cant just replace space with ;
                sImportLineBuffer = xDelim(sImportLineBuffer, sDelimChar, 1, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 2, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 3, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 4, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 5, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 6, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 7, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 8, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 9, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 10, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 11, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 12, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 13, Chr(34))
            End If
        End If

        ReadStrommeSingapore = sImportLineBuffer

        ' XOKNET 27.04.2012 added next lines
        Exit Function

ERR_ReadStromme:

        ' XNET 06.05.2014
        Err.Raise(Err.Number, "ReadStrommeSingapore - Invoice", Err.Description)

    End Function
    Private Function ReadStrommeSingapore_G3() As String
        ' XNET 06.05.2014 added function
        Dim oFreetext As Freetext
        Dim sDelimChar As String
        Dim i As Integer

        Dim sTmp As String
        On Error GoTo ERR_ReadStromme

        ' XOKNET 27.04.2012 - try to catch amount error
        sTmp = Trim(xDelim(sImportLineBuffer, ";", 3, Chr(34)))
        nMON_InvoiceAmount = ConvertToAmount(Trim(xDelim(sImportLineBuffer, ";", 3, Chr(34))), vbNullString, ".,")
        bAmountSetInvoice = True

        oFreetext = oFreetexts.Add()
        oFreetext.Text = Trim(xDelim(sImportLineBuffer, ";", 11, Chr(34))) '''& " " & xDelim(sImportLineBuffer, ";", 13)
        sREF_Own = Trim(xDelim(sImportLineBuffer, ";", 10, Chr(34)))
        If EmptyString(oFreetext.Text) Then
            oFreetext.Text = "."
        End If

        If oCargo.Special = "TAB_SEPARATED" Then
            sImportLineBuffer = ReadGenericRecord(oFile)
            sImportLineBuffer = Replace(sImportLineBuffer, Chr(9), ";")
        Else
            sImportLineBuffer = ReadLineFromStromme(oXLWSheet, 14)
            ' We are accepting both TAB, ; , as separators in new solution
            ' TAB is already accepted OK in ReadLineFromExcel
            If Not EmptyString(sImportLineBuffer) Then
                ' remove trailing ;
                For i = Len(sImportLineBuffer) To 0 Step -1
                    If Mid(sImportLineBuffer, i, 1) = ";" Then
                        sImportLineBuffer = Left(sImportLineBuffer, i - 1)
                    Else
                        Exit For
                    End If
                Next i

                ' Semicolon not - will be seen as comma in ReadLineFromExcel
                ' Same as comma sep
                ' Space sep?
                ' Mens ren Excel og TAB gir oss ; som skilletegn, og da lar vi det vare v�re som det er
                If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) > 10 Then
                    sDelimChar = ";"
                ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ",", "")) > 10 Then
                    sDelimChar = ","
                ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, " ", "")) > 10 Then
                    'sImportLineBuffer = Replace(sImportLineBuffer, " ", ";")
                    sDelimChar = " "
                ElseIf Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, Chr(9), "")) > 10 Then
                    'sImportLineBuffer = Replace(sImportLineBuffer, " ", ";")
                    sDelimChar = Chr(9)
                End If

                ' Because we may have spaces or ;  inside an element, we cant just replace space with ;
                sImportLineBuffer = xDelim(sImportLineBuffer, sDelimChar, 1, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 2, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 3, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 4, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 5, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 6, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 7, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 8, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 9, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 10, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 11, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 12, Chr(34)) & ";" & xDelim(sImportLineBuffer, sDelimChar, 13, Chr(34))
            End If

        End If

        ReadStrommeSingapore_G3 = sImportLineBuffer

        Exit Function

ERR_ReadStromme:

        Err.Raise(Err.Number, "ReadStrommeSingapore_G3 - Invoice", Err.Description)

    End Function

    Private Function ReadRECSingapore() As String
        Dim oFreeText As Freetext
        Dim sDelimiter As String
        sDelimiter = Chr(9) ' TAB

        nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, sDelimiter, 3), "", ".,"))
        bAmountSetInvoice = True

        ' Must decide if Domestic (in Singapore) or International payment.
        ' 10.05.2019 - Added 4 address fields, which are needed when Rec's accounts are moved to Norway
        '              thus - we must test for 18 or 19 columns
        If Not EmptyString(xDelim(sImportLineBuffer, sDelimiter, 19)) Then
            ' Domestic Sinagpore payment;
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, sDelimiter, 10) '''& " " & xDelim(sImportLineBuffer, sdelimiter, 12)
            sREF_Own = xDelim(sImportLineBuffer, sDelimiter, 11)
        Else
            'International
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, sDelimiter, 11)
            ' to be used i freetext, via Special = REC_SINGAPORE
            sMyField = xDelim(sImportLineBuffer, sDelimiter, 13)
            sREF_Own = xDelim(sImportLineBuffer, sDelimiter, 12)

            ' State bank reporting
            sSTATEBANK_Code = "14"
            sSTATEBANK_Text = "Purchase of goods"
        End If
        If EmptyString((oFreeText.Text)) Then
            oFreeText.Text = "-"
        End If

        ' XOKNET 24.06.2015 added trim to catch empty lines
        sImportLineBuffer = Trim$(ReadGenericRecord(oFile))

        If Len(sImportLineBuffer) < 20 Then
            sImportLineBuffer = ""
        End If

        ReadRECSingapore = sImportLineBuffer

    End Function
    Private Function ReadTeekaySingapore() As String
        Dim oFreeText As Freetext

        nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 3), "", ".,"))
        bAmountSetInvoice = True

        ' Must decide if Domestic (in Singapore) or International payment.
        ' Domestic has 12 columns, Intl har 13
        'If Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ",", "")) = 11 Then
        If EmptyString(xDelim(sImportLineBuffer, ",", 13)) Then
            ' Domestic Sinagpore payment;
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ",", 10)
            sREF_Own = xDelim(sImportLineBuffer, ",", 11)
        Else
            'International
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ",", 11)
            sREF_Own = xDelim(sImportLineBuffer, ",", 12)
        End If
        If EmptyString((oFreeText.Text)) Then
            oFreeText.Text = "-"
        End If

        sImportLineBuffer = ReadGenericRecord(oFile)

        ReadTeekaySingapore = sImportLineBuffer

    End Function
    Private Function ReadSudjaca() As String
        Dim oFreeText As Freetext

        nMON_InvoiceAmount = CDbl(ConvertToAmount(Replace(xDelim(sImportLineBuffer, ",", 4, Chr(34)), "$", ""), ",", "."))
        bAmountSetInvoice = True

        'International
        oFreeText = oFreetexts.Add()
        oFreeText.Text = "Salary"
        sREF_Own = xDelim(sImportLineBuffer, ",", 3, Chr(34))

        If Not oFile.AtEndOfStream = True Then
            sImportLineBuffer = ReadGenericRecord(oFile)
            'sImportLineBuffer = sImportLineBuffer & Replace(ReadGenericRecord(oFile), Chr(34), "") ' "Real" line 2
            ' XNET 17.08.2012 Do not replace chr(34)
            sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "Real" line 2
            If InStr(sImportLineBuffer, "Home Allotment Listing For") > 0 Then
                ' pagebreak
                ' reread
                ' 22/07/2004 4:11:05 PM valerie-13772-13771,Page 1 ,,
                ' Home Allotment Listing For The Month Of Jul 2004,,,
                ' Vessel code :,RGD,RED GOLD,
                sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "pagefooter 3" Vessel code :,RGD,RED GOLD,

                ' XNET 17.08.2012 - read 5 new lines as headers
                sImportLineBuffer = ReadGenericRecord(oFile)   ' "Real" line 1
                sImportLineBuffer = ReadGenericRecord(oFile)   ' "Real" line 1
                sImportLineBuffer = ReadGenericRecord(oFile)   ' "Real" line 1
                sImportLineBuffer = ReadGenericRecord(oFile)   ' "Real" line 1
                sImportLineBuffer = ReadGenericRecord(oFile)   ' "Real" line 1

                ' read to first lines for new record on new page;
                sImportLineBuffer = ReadGenericRecord(oFile) ' "Real" line 1
                sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "Real" line 2
            End If
            sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "Real" line 3
            sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "Real" line 4
            sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "Real" line 5
            sImportLineBuffer = sImportLineBuffer & ReadGenericRecord(oFile) ' "Real" line 6
            ' New 02.11.05
            ' remove "
            ' XNET 17.08.2012 - DO NOT REMOVE " - causes trouble for bankadresses with comma !!
            'sImportLineBuffer = Replace(sImportLineBuffer, Chr(34), "")

            ' test for last "real" payment
            '---------------------------------
            '17,C30602,Uum Tompo,$467.00
            ',Madjid Abdul,A/c No 0166-01-005661-50-6,
            ', ,BRI,
            '  , ,Cabang Labuan Banten,
            '  , ,Indonesia,
            '  , ,,
            '  , ,Vessel Total Amount,"$15,053.00 "
            '  , ,Allotment Type Total Amount,"$15,053.00 "
            '22/07/2004 4:11:05 PM valerie-13772-13771,Page 2 ,,
            'Home Allotment Listing For The Month Of Jul 2004,,,
            'Vessel code :,RGD,RED GOLD,
            '  , ,,
            '--- End of Report ---,,,
            '22/07/2004 4:11:05 PM valerie-13772-13771,Page 3 ,,
            '</CSV>,,,
            '----------------------------------------
            ' XNET 17.08.2012 added "--- End of Report ---" test
            If InStr(sImportLineBuffer, "Vessel Total Amount") > 0 Or InStr(sImportLineBuffer, "--- End of Report ---") > 0 Then
                ' read rest of file;
                sImportLineBuffer = ""
                Do Until oFile.AtEndOfStream = True
                    oFile.SkipLine()
                Loop
                sImportLineBuffer = ""
            End If
        Else
            sImportLineBuffer = ""
        End If

        ReadSudjaca = sImportLineBuffer

    End Function
    Private Function ReadAutocare_Norge() As String
        Dim oFreeText As Freetext

        nMON_InvoiceAmount = CDbl(xDelim(sImportLineBuffer, ";", 11))
        bAmountSetInvoice = True

        If EmptyString(xDelim(sImportLineBuffer, ";", 9)) Then
            ' No KID
            oFreeText = oFreetexts.Add()
            oFreeText.Text = xDelim(sImportLineBuffer, ";", 8)
        Else
            ' KID
            sUnique_Id = xDelim(sImportLineBuffer, ";", 9)
        End If

        sREF_Own = xDelim(sImportLineBuffer, ";", 10)

        sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
        ReadAutocare_Norge = sImportLineBuffer

    End Function
    Private Function ReadFirstNordicFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext

        nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 16), "", ","))
        bAmountSetInvoice = True

        oFreeText = oFreetexts.Add()
        oFreeText.Text = xDelim(sImportLineBuffer, ";", 18)

        sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)

        ReadFirstNordicFile = sImportLineBuffer

    End Function
    Private Function ReadNCLFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext

        'The , in amount is in position 61, start reading from pos 50
        nMON_InvoiceAmount = CDbl(ConvertToAmount(Trim(Mid(sImportLineBuffer, 50)), ".", ","))
        bAmountSetInvoice = True
        'The invoiceno starts in position 13
        sInvoiceNo = Trim(Mid(sImportLineBuffer, 5, 23))

        'Set oFreeText = oFreetexts.Add()
        'oFreeText.Text = xDelim(sImportLineBuffer, ";", 18)

        ReadNCLFile = sImportLineBuffer

    End Function

    Private Function ReadISS_SDVFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String


        nMON_InvoiceAmount = Val(Replace(Trim(xDelim(sImportLineBuffer, ";", 7, Chr(34))), ",", ".")) * 100
        bAmountSetInvoice = True
        sMessage = xDelim(sImportLineBuffer, ";", 10, Chr(34))

        oFreeText = oFreetexts.Add()
        oFreeText.Text = Left(PadRight(sMessage, 40, " "), 40)

        'UPGRADE_WARNING: Couldn't resolve default property of object ReadExcelRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sImportLineBuffer = ReadExcelRecord(oFile) ', oProfile)

        ReadISS_SDVFile = sImportLineBuffer

    End Function
    Private Function ReadElkjopFile() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim nTmp As Double
        Dim iCrLfPos As Short

        nMON_InvoiceAmount = Val(Trim(xDelim(sImportLineBuffer, ";", 27, Chr(34)))) 'NetAmount
        bAmountSetInvoice = True

        sCustomerNo = Trim(xDelim(sImportLineBuffer, ";", 19, Chr(34), oCargo.FilenameIn, "CreditorNo")) 'CreditorNo
        sInvoiceNo = Trim(xDelim(sImportLineBuffer, ";", 18, Chr(34), oCargo.FilenameIn, "InvoiceNo")) 'InvoiceNo
        'CreditorNo + / + InvoiceNo
        sREF_Own = sCustomerNo & "/" & sInvoiceNo
        'Test if KID payment
        If Len(Trim(xDelim(sImportLineBuffer, ";", 17, Chr(34), oCargo.FilenameIn, "KID"))) = 0 Then 'KID
            'Not KID
            'Fakturanr.: #InvoiceNo# #CrLf#Total amount: #Total# - Cash Discount: #Discount# = Net amount: #Net#
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups().Message. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sMessage = Trim(oProfile.FileSetups(FileSetup_ID()).Message)
            If InStr(1, sMessage, "#InvoiceNo#") Then
                sMessage = Replace(sMessage, "#InvoiceNo#", sInvoiceNo)
            End If
            If InStr(1, sMessage, "#Total#") Then
                nTmp = Val(Trim(xDelim(sImportLineBuffer, ";", 25, Chr(34), oCargo.FilenameIn, "TotalAmount"))) 'TotalAmount
                sMessage = Replace(sMessage, "#Total#", VB6.Format(nTmp / 100, "Standard")) '"##,##0.00"))
            End If
            If InStr(1, sMessage, "#Discount#") Then
                nTmp = Val(Trim(xDelim(sImportLineBuffer, ";", 26, Chr(34), oCargo.FilenameIn, "CashDiscount"))) 'CashDiscount
                sMessage = Replace(sMessage, "#Discount#", VB6.Format(nTmp / 100, "Standard"))
            End If
            If InStr(1, sMessage, "#Net#") Then
                sMessage = Replace(sMessage, "#Net#", VB6.Format(nMON_InvoiceAmount / 100, "Standard"))
            End If

            Do While Len(sMessage) > 0
                If Len(sMessage) > 40 Then
                    iCrLfPos = InStr(1, Left(sMessage, 40), "#CrLf#")
                    If iCrLfPos > 0 And iCrLfPos < 41 Then
                        oFreeText = oFreetexts.Add
                        sMessage = Replace(sMessage, "#CrLf#", "", 1, 1)
                        oFreeText.Text = PadRight(Left(sMessage, iCrLfPos - 1), 40, " ")
                        sMessage = Mid(sMessage, iCrLfPos)
                    Else
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = Left(sMessage, 40)
                        sMessage = Mid(sMessage, 41)
                    End If
                Else
                    iCrLfPos = InStr(1, Left(sMessage, 40), "#CrLf#")
                    If iCrLfPos > 0 And iCrLfPos < 41 Then
                        oFreeText = oFreetexts.Add
                        sMessage = Replace(sMessage, "#CrLF#", "", 1, 1)
                        oFreeText.Text = PadRight(Left(sMessage, iCrLfPos - 1), 40, " ")
                        sMessage = Mid(sMessage, iCrLfPos)
                    Else
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(sMessage, 40, " ")
                        sMessage = ""
                    End If
                End If
            Loop
        Else
            sUnique_Id = Trim(xDelim(sImportLineBuffer, ";", 17, Chr(34))) 'KID
        End If
        sSTATEBANK_Code = Trim(xDelim(sImportLineBuffer, ";", 29, Chr(34), oCargo.FilenameIn, "National Bank Code")) 'National Bank Code
        sSTATEBANK_Text = Trim(xDelim(sImportLineBuffer, ";", 30, Chr(34), oCargo.FilenameIn, "National Bank Text")) 'National Bank Text

        ReadElkjopFile = sImportLineBuffer

    End Function
    Private Function ReadDanskeBankLokaleDanske() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim i As Short
        Dim sTmp As String
        sREF_Own = "X"

        If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMBO" Then
            ' Domestic Danish
            nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 4, Chr(34)), "", ".,"))
            bAmountSetInvoice = True

            'Test if KID payment
            If Left(xDelim(sImportLineBuffer, ",", 3, Chr(34)), 2) = "IK" Then
                ' FIK
                ' Kortart + Betalingsid
                sUnique_Id = xDelim(sImportLineBuffer, ",", 23, Chr(34)) & xDelim(sImportLineBuffer, ",", 24, Chr(34))
            End If

            '02.11.2015 - Added next if
            sTmp = xDelim(sImportLineBuffer, ",", 8, Chr(34))
            If sTmp = "J" Or sTmp = "S" Or sTmp = "U" Then
                sMessage = xDelim(sImportLineBuffer, ",", 25, Chr(34)) 'Debitorid.
                If Len(sMessage) > 0 Then
                    sCustomerNo = sMessage.Trim
                End If

                sMessage = xDelim(sImportLineBuffer, ",", 26, Chr(34)) 'Dokumentreference
                If Len(sMessage) > 0 Then
                    'Maybe it should be set in the InvoiceNo. Seems so in the documention, but not used so by Gjensidige.
                    'If the code is changed we must use the following IF around the code beneath.
                    'If oCargo.Special = "GJENSIDIGE_FORSIKRING_DANMARK" Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = sMessage.Trim
                End If
            End If

            ' Advice in field 27 - 67
            For i = 27 To 67
                sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = sMessage
                End If
            Next i

        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMNI" Then
            ' Norwegian payments, new 09.07.2010
            nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 4, Chr(34)), "", ".,"))
            bAmountSetInvoice = True

            'Test if KID payment
            If xDelim(sImportLineBuffer, ",", 8, Chr(34)) = "R" Then
                sUnique_Id = xDelim(sImportLineBuffer, ",", 24, Chr(34))
            Else
                ' NB Does not support stuctured payments (so far)
                ' Advice in field 27 - 67
                For i = 27 To 67
                    sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = sMessage
                    End If
                Next i
            End If

            ' 68. Betalingsform�lskode
            '-------------------------
            sSTATEBANK_Code = xDelim(sImportLineBuffer, ",", 25, Chr(34))

            ' 70./71. Tekst til Norges bank
            '------------------------
            sSTATEBANK_Text = xDelim(sImportLineBuffer, ",", 70, Chr(34)) & " " & xDelim(sImportLineBuffer, ",", 71, Chr(34))

        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMSI" Then
            ' Swedish payments, new 09.07.2010
            nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 4, Chr(34)), "", ".,"))
            bAmountSetInvoice = True

            'Test if KID payment
            If Left(xDelim(sImportLineBuffer, ",", 8, Chr(34)), 2) = "R" Then
                sUnique_Id = xDelim(sImportLineBuffer, ",", 24, Chr(34))
            Else
                ' Advice in field 27 - 56
                For i = 27 To 56
                    sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = sMessage
                    End If
                Next i

                If EmptyString(sMessage) Then
                    ' try to find info in Tekstreference (Modtager)
                    If Not EmptyString(xDelim(sImportLineBuffer, ",", 22, Chr(34))) Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = Trim(xDelim(sImportLineBuffer, ",", 22, Chr(34)))
                    End If
                End If
            End If
            ' 68. Betalingsform�lskode
            '-------------------------
            sSTATEBANK_Code = xDelim(sImportLineBuffer, ",", 25, Chr(34))

            ' 70./71. Tekst til Norges bank
            '------------------------
            sSTATEBANK_Text = xDelim(sImportLineBuffer, ",", 70, Chr(34)) & " " & xDelim(sImportLineBuffer, ",", 71, Chr(34))



            ' 09.07.2010 changed from
            'Else
            ' to
        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMUO" Or xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMUONO" Or xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMUOSE" Then
            ' International, CMUO
            ' Foreign transfers from Norway (CMUONO)

            ' 5. Bel�b
            '---------
            'nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 5, Chr(34)), ",", ""), ".", ""))
            ' Changed 09.12.05, Because to how Jotun handles figures
            nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 5, Chr(34)), "", ".,"))
            bAmountSetInvoice = True

            ' 18-21. Tekst til modtager
            '--------------------------
            For i = 18 To 21
                sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = sMessage
                End If
            Next i

            ' XNET 03.10.2012 - added next If / Else
            If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMUONO" Then
                ' 55. Betalingsform�lskode
                '-------------------------
                sSTATEBANK_Code = xDelim(sImportLineBuffer, ",", 55, Chr(34))

                ' XNET 14.12.2012 - changed from Else to ... CMUO (25,26,29 gjelder ikke for CMUOSE)
                'Else
            ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "CMUO" Then
                ' 25. Betalingsform�lskode
                '-------------------------
                sSTATEBANK_Code = xDelim(sImportLineBuffer, ",", 25, Chr(34))

                ' 26. Indf�rselstidspunkt
                '------------------------
                sSTATEBANK_Text = xDelim(sImportLineBuffer, ",", 26, Chr(34))

                ' 29. Debitor SE-nummer
                '------------------------
                sSTATEBANK_Text = sSTATEBANK_Text & " " & Trim$(xDelim(sImportLineBuffer, ",", 29, Chr(34)))
            End If  ' XNET 03.10.2012 Addes End If

            ' 32-35 Beskrivelse av betalingsform�l
            '-------------------------------------
            sTmp = ""
            For i = 32 To 35
                sTmp = sTmp & " " & Trim(xDelim(sImportLineBuffer, ",", i, Chr(34)))
            Next i
            sTmp = Trim(sTmp)
            sSTATEBANK_Text = sSTATEBANK_Text & " " & sTmp
            sSTATEBANK_Text = Trim(sSTATEBANK_Text)
        End If

        ' Read another line;
        sImportLineBuffer = ReadDanskeBankLokaleDanskeRecord(oFile)

        ReadDanskeBankLokaleDanske = sImportLineBuffer

    End Function
    Private Function ReadSydbank() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim i As Short
        Dim sTmp As String
        Dim sIndex As String

        If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "IB030201000002" Then
            ' Domestic, to own account

            ' 4) Trans-bel�p
            nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 4, Chr(34)), ",", ""), ".", ""))
            bAmountSetInvoice = True

        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "IB030202000005" Or xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "IB030202000004" Then
            ' 18.09.2007 Added IB030202000004
            sIndex = Right(xDelim(sImportLineBuffer, ",", 2, Chr(34)), 1) ' 2) Index

            If sIndex = "1" Then

                ' Domestic
                ' 4) Trans-bel�p
                nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 4, Chr(34)), ",", ""), ".", ""))
                bAmountSetInvoice = True

                ' Advice in field 19 - 27
                For i = 19 To 27
                    sMessage = Trim(xDelim(sImportLineBuffer, ",", i, Chr(34)))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(sMessage, 40, " ")
                    End If
                Next i

            ElseIf sIndex = "2" Then
                ' Advice in field 8 - 9
                'For i = 8 To 9
                '    sMessage = Trim$(xDelim(sImportLineBuffer, ",", i, Chr(34)))
                '    If Len(sMessage) > 0 Then
                '        Set oFreeText = oFreetexts.Add
                '        oFreeText.Text = PadRight(sMessage, 40, " ")
                '    End If
                'Next i
                sREF_Own = xDelim(sImportLineBuffer, ",", 10, Chr(34))

            ElseIf sIndex = "3" Or sIndex = "4" Then
                ' Advice in field 3 - 24
                For i = 3 To 24
                    sMessage = Trim(xDelim(sImportLineBuffer, ",", i, Chr(34)))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(sMessage, 40, " ")
                    End If
                Next i

            End If

        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "IB030204000003" Then
            ' International
            sIndex = Right(xDelim(sImportLineBuffer, ",", 2, Chr(34)), 1) ' 2) Index

            If sIndex = "1" Then

                ' 4) Trans-bel�p
                nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 4, Chr(34)), ",", ""), ".", ""))
                bAmountSetInvoice = True

                ' Advice in field 10 - 13
                For i = 10 To 13
                    sMessage = Trim(xDelim(sImportLineBuffer, ",", i, Chr(34)))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(sMessage, 40, " ")
                    End If
                Next i

                ' To statebank;
                sSTATEBANK_Code = Trim(xDelim(sImportLineBuffer, ",", 18, Chr(34)))
                sSTATEBANK_Text = Trim(xDelim(sImportLineBuffer, ",", 21, Chr(34))) & Trim(xDelim(sImportLineBuffer, ",", 22, Chr(34))) & Trim(xDelim(sImportLineBuffer, ",", 23, Chr(34)))
                sSTATEBANK_DATE = Trim(xDelim(sImportLineBuffer, ",", 19, Chr(34)))


            End If

        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "IB030207000002" Then

            sIndex = Right(xDelim(sImportLineBuffer, ",", 2, Chr(34)), 1) ' 2) Index

            If sIndex = "1" Then

                ' Domestic
                ' 4) Trans-bel�p
                nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 4, Chr(34)), ",", ""), ".", ""))
                bAmountSetInvoice = True

                ' FIK
                ' Kortart (7) + Betalingsid (8)
                sUnique_Id = Trim(xDelim(sImportLineBuffer, ",", 7, Chr(34))) & Trim(xDelim(sImportLineBuffer, ",", 8, Chr(34)))



                ' Advice in field 20 - 25
                For i = 20 To 25
                    sMessage = Trim(xDelim(sImportLineBuffer, ",", i, Chr(34)))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(sMessage, 40, " ")
                    End If
                Next i

            ElseIf sIndex = "2" Or CDbl(sIndex) = 3 Then
                ' Advice in field 3 - 24
                For i = 3 To 24
                    sMessage = Trim(xDelim(sImportLineBuffer, ",", i, Chr(34)))
                    If Len(sMessage) > 0 Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(sMessage, 40, " ")
                    End If
                Next i

            End If

        End If

        ' Read another line;
        sImportLineBuffer = ReadSydbankDanskeRecord(oFile)

        ReadSydbank = sImportLineBuffer

    End Function
    Private Function ReadCyberCity() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim i As Short
        Dim sTmp As String
        Dim sIndex As String

        If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "008" Then
            ' Betalingstype Bank Danmark

            ' 6) Trans-bel�p
            nMON_InvoiceAmount = Val(Replace(xDelim(sImportLineBuffer, ",", 6, Chr(34)), ".", "")) / 100 ' 4 decimals
            bAmountSetInvoice = True

            sMessage = Trim(xDelim(sImportLineBuffer, ",", 12, Chr(34))) ' Upto 1435 chars
            Do
                If Len(sMessage) = 0 Then
                    Exit Do
                End If
                oFreeText = oFreetexts.Add
                ' Chop into 40 and 40 chars
                oFreeText.Text = PadRight(sMessage, 40, " ")
                sMessage = Mid(sMessage, 41)
            Loop


        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "009" Then
            ' FIK
            ' 6) Trans-bel�p
            nMON_InvoiceAmount = Val(Replace(xDelim(sImportLineBuffer, ",", 6, Chr(34)), ".", "")) / 100 ' 4 decimals
            bAmountSetInvoice = True

            ' FIK
            ' Kortart (10) + Betalingsid (11)
            sUnique_Id = Trim(xDelim(sImportLineBuffer, ",", 10, Chr(34))) & Trim(xDelim(sImportLineBuffer, ",", 11, Chr(34)))

            ' Advice in field 11
            sMessage = Trim(xDelim(sImportLineBuffer, ",", 12, Chr(34))) ' Upto 1435 chars
            Do
                If Len(sMessage) = 0 Then
                    Exit Do
                End If
                oFreeText = oFreetexts.Add
                ' Chop into 40 and 40 chars
                oFreeText.Text = PadRight(sMessage, 40, " ")
                sMessage = Mid(sMessage, 41)
            Loop
        ElseIf Left(sImportLineBuffer, 3) = "003" Then
            ' L�n
            nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 22, 15)) / 100
            bAmountSetInvoice = True
            sREF_Own = Mid(sImportLineBuffer, 4, 9)

        End If

        ' Read another line;
        sImportLineBuffer = ReadCyberCityRecord(oFile)

        ReadCyberCity = sImportLineBuffer

    End Function
    Private Function ReadNSA() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim i As Short
        Dim sTmp As String
        Dim sIndex As String

        If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "010" Then
            ' Betalingstype Bank Udland

            ' 8) Trans-bel�p
            nMON_InvoiceAmount = Val(Replace(xDelim(sImportLineBuffer, ",", 10, Chr(34)), ",", "")) ' 2 decimals
            bAmountSetInvoice = True

            oFreeText = oFreetexts.Add
            'oFreeText.Text = PadRight("NSA Scandinavia", 40, " ")
            ' changed 30.10.2008
            oFreeText.Text = xDelim(sImportLineBuffer, ",", 13, Chr(34))

        End If

        ' Read another line;
        sImportLineBuffer = ReadNSARecord(oFile)

        ReadNSA = sImportLineBuffer

    End Function
    Private Function ReadAquariusOutgoing() As String
        ' File from Aquarius for SG FinansDim oPayment As Payment
        Dim oFreeText As Freetext
        Dim sTmp As String

        ' 24.05.2007
        ' Added KID (prfixed with *)
        'sTmp = PadRight(Trim$(xDelim(sImportLineBuffer, ";", 9, Chr(34))), 40, " ")
        ' 05.03.2008, there may now be up to 120 chars freetext
        sTmp = PadRight(Trim(xDelim(sImportLineBuffer, ";", 9, Chr(34))), 120, " ")
        If Left(sTmp, 1) = "*" Then
            sUnique_Id = Trim(Mid(sTmp, 2))
        Else
            ' Freetext notification in field 9
            oFreeText = oFreetexts.Add
            oFreeText.Text = Left(sTmp, 40)
            If Len(sTmp) > 40 Then
                oFreeText = oFreetexts.Add
                oFreeText.Text = Mid(sTmp, 41, 40)
            End If
            If Len(sTmp) > 80 Then
                oFreeText = oFreetexts.Add
                oFreeText.Text = Mid(sTmp, 81, 40)
            End If

        End If
        nMON_InvoiceAmount = Val(Replace(xDelim(sImportLineBuffer, ";", 10, Chr(34)), ",", "")) ' 2 decimals
        bAmountSetInvoice = True

        ' added 11.12.2007 for International Payments
        STATEBANK_Code = "14" ' Always!
        sSTATEBANK_Text = "Factoring" ' Always

        If oFile.AtEndOfStream Then
            sImportLineBuffer = ""
        Else
            ' Read another line;
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadExcelRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadExcelRecord(oFile)
            ' 05.03.2008 - do a test here to ensure correct lines;
            ' Starts with 8101 and minimum 12 ;
            ' 81010788293;20080213;000000000000000000000000004309;33611286622;Haugaland Asfalt As;B� Vest;4262;Avaldsnes;Forskudd;000000100000000;NOK;;
            If Left(sImportLineBuffer, 4) <> "8101" Or (Len(sImportLineBuffer) - Len(Replace(sImportLineBuffer, ";", "")) < 12) Then
                Call BabelImportError((BabelFiles.FileType.AquariusOutgoing), FilenameImported(), sImportLineBuffer, "10010")
            End If
        End If

        ReadAquariusOutgoing = sImportLineBuffer

    End Function


    Private Function ReadNordeaDK_EDI() As String
        Dim oFreeText As Freetext
        Dim i As Short
        Dim iNoOfMessageLines As Short = 0
        Dim iNoOfAddressLines As Short = 0
        Dim iNoOfBankLines As Short = 0

        Select Case Left(sImportLineBuffer, 6)
            Case "UBT033"
                ' Domestic, old
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 46, 15))
                bAmountSetInvoice = True

                iNoOfMessageLines = Val(Mid(sImportLineBuffer, 321, 2)) 'Tells how many cols of 35 characters for info to ben
                For i = 1 To iNoOfMessageLines
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(Mid(sImportLineBuffer, 323 + ((i - 1) * 35), 35), 40, " ")
                Next i

            Case "UBT043"
                ' Request for transfer
                ' "Domestic payment" in other countries
                ' NB! Denne kan v�re spesiell for Georg Jensen, DK !!!

                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 45, 15))
                bAmountSetInvoice = True

                iNoOfBankLines = Val(Mid(sImportLineBuffer, 125, 2))
                iNoOfAddressLines = Val(Mid(sImportLineBuffer, 138 + iNoOfBankLines * 35, 2))

                oFreeText = oFreetexts.Add
                oFreeText.Text = Mid(sImportLineBuffer, 142 + iNoOfBankLines * 35 + iNoOfAddressLines * 35, 35)

            Case "UBT045"
                ' Domestic
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 46, 15))
                bAmountSetInvoice = True

                sInvoiceNo = Mid(sImportLineBuffer, 187, 35).Trim  'Ref-prim�rt dokument

                iNoOfAddressLines = Val(Mid(sImportLineBuffer, 225, 2))
                iNoOfMessageLines = Val(Mid(sImportLineBuffer, 227 + iNoOfAddressLines * 35, 2)) 'Tells how many cols of 35 characters for info to ben
                For i = 1 To iNoOfMessageLines
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = Mid(sImportLineBuffer, 229 + (iNoOfAddressLines * 35) + ((i - 1) * 35), 35).Trim
                Next i

            Case "UBT046"
                ' Transfer form / Giro payment
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 46, 15))
                bAmountSetInvoice = True

                sUnique_Id = Mid(sImportLineBuffer, 103, 21).Trim  'kortartkode + betalingsreferanse

                iNoOfAddressLines = Val(Mid(sImportLineBuffer, 127, 2))
                iNoOfMessageLines = Val(Mid(sImportLineBuffer, 129 + iNoOfAddressLines * 35, 2)) 'Tells how many cols of 35 characters for info to ben
                For i = 1 To iNoOfMessageLines
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = Mid(sImportLineBuffer, 129 + iNoOfAddressLines + ((i - 1) * 35), 35).Trim
                Next i
                ' Customers may pad with leading zeros. Try to cut for the most commonly used codes:
                Select Case Left(sUnique_Id, 2)
                    Case "71"
                        ' 71 + 15 betalerident
                        If Len(sUnique_Id) > 17 Then
                            sUnique_Id = Left(sUnique_Id, 2) & Right(sUnique_Id, 15)
                        End If
                    Case "04", "75"
                        If Len(sUnique_Id) > 18 Then
                            sUnique_Id = Left(sUnique_Id, 2) & Right(sUnique_Id, 16)
                        End If
                End Select

            Case "UBT049"
                ' International
                nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 46, 15))
                bAmountSetInvoice = True
                iNoOfAddressLines = Val(Mid(sImportLineBuffer, 475, 2))
                iNoOfMessageLines = Val(Mid(sImportLineBuffer, 477 + (iNoOfAddressLines * 35), 2)) 'Tells how many cols of 35 characters for info to ben
                For i = 1 To iNoOfMessageLines
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(Mid(sImportLineBuffer, 479 + (iNoOfAddressLines * 35) + (i - 1) * 35, 35), 40, " ")
                Next i
            Case Else
                iNoOfAddressLines = Val(Mid(sImportLineBuffer, 475, 2))

        End Select

        ' Read another line;
        sImportLineBuffer = ReadNordeaDK_EDIRecord(oFile)

        ReadNordeaDK_EDI = sImportLineBuffer

    End Function
    Private Function ReadNordea_Innbet_Ktoinf_utl() As String
        Dim bReadLine As Boolean
        Dim sLeftOverText As String
        Dim oFreeText As Freetext

        Do While True

            Select Case Left(sImportLineBuffer, 2)

                Case "25"
                    Exit Do

                Case "26"
                    sLeftOverText = sLeftOverText & Mid(sImportLineBuffer, 5, 65)
                    Do While Len(sLeftOverText) > 40
                        oFreeText = Freetexts.Add
                        oFreeText.Text = Left(sLeftOverText, 40)
                        sLeftOverText = Mid(sLeftOverText, 41)
                    Loop
                    If Len(sLeftOverText) > 0 Then
                        oFreeText = Freetexts.Add
                        oFreeText.Text = sLeftOverText
                        sLeftOverText = ""
                    End If

                    bReadLine = True

                Case "27" 'Utg�ende saldo
                    Exit Do

                Case Else
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-001")

                    Exit Do

            End Select

            If bReadLine Then
                'Read new record
                sImportLineBuffer = oFile.ReadLine()
            End If

            If oFile.AtEndOfStream = True Then
                'If sImportLineBuffer = "" Then
                Exit Do
                'End If
            End If
        Loop

        If Not EmptyString(sLeftOverText) Then
            oFreeText = Freetexts.Add
            oFreeText.Text = Left(sLeftOverText, 40)
        End If

        ReadNordea_Innbet_Ktoinf_utl = sImportLineBuffer

    End Function

    Private Function ReadHandelsbanken_DK_Domestic() As String
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim nTempAmount As Double
        Dim i As Short

        '9. Bel�p
        '--------
        ' 06.09.2007 - also replaces "." with blank
        ' also checking for amounts with no decimal comma or point
        If InStr(xDelim(sImportLineBuffer, ",", 9, Chr(34)), ".") = 0 And InStr(xDelim(sImportLineBuffer, ",", 9, Chr(34)), ",") = 0 Then
            ' no decimal
            nMON_InvoiceAmount = Val(xDelim(sImportLineBuffer, ",", 9, Chr(34))) * 100
        Else
            nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 9, Chr(34)), ",", ""), ".", ""))
        End If

        '14. Udenlandsk Bel�p
        '--------------------
        ' If Udenlandsk Bel�p set, use it:
        nTempAmount = Val(Replace(xDelim(sImportLineBuffer, ",", 14, Chr(34)), ",", ""))
        If nTempAmount <> 0 Then
            nMON_InvoiceAmount = nTempAmount
        End If
        bAmountSetInvoice = True


        'Test if FIK payment
        If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH351" Then
            ' FIK, kortart 71
            ' Kortart + Betalingsid
            '11. Kort reference
            '------------------
            sUnique_Id = xDelim(sImportLineBuffer, ",", 11, Chr(34))
        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH352" Then
            ' GIRO-kort uten advisering, kortart 04 eller 15
            ' Kortart + Betalingsid
            '11. Kort reference
            '------------------
            sUnique_Id = xDelim(sImportLineBuffer, ",", 11, Chr(34))
        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH354" Then
            ' GIRO-kort med advisering, kortart 01 eller 41
            ' Kortart + Betalingsid
            '11. Kort reference
            '------------------
            sUnique_Id = xDelim(sImportLineBuffer, ",", 11, Chr(34))
        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH357" Then
            ' FIK, kortart 73
            ' Kortart + Betalingsid
            '11. Kort reference
            '------------------
            sUnique_Id = xDelim(sImportLineBuffer, ",", 11, Chr(34))
        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH358" Then
            ' FIK, kortart 75
            ' Kortart + Betalingsid
            '11. Kort reference
            '------------------
            sUnique_Id = xDelim(sImportLineBuffer, ",", 11, Chr(34))
        ElseIf xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH400" Or xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH500" Then
            For i = 33 To 36
                sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(sMessage, 40, " ")
                End If
            Next i
        Else
            ' NOtification/Advice to receiver
            For i = 33 To 73
                sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(sMessage, 40, " ")
                End If
            Next i
        End If

        If xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH400" Or xDelim(sImportLineBuffer, ",", 1, Chr(34)) = "ERH500" Then
            ' 15 Nasjonalbankkode
            sSTATEBANK_Code = xDelim(sImportLineBuffer, ",", 15, Chr(34))
        End If

        ' Read another line;
        sImportLineBuffer = ReadHandelsbanken_DK_DomesticRecord(oFile)

        ReadHandelsbanken_DK_Domestic = sImportLineBuffer

    End Function
    Private Function ReadErhvervsGiro() As String
        ' 11.09.06: Also for PTG (SEB, Torben)
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim nTempAmount As Double
        Dim i As Short



        ' Still has a 30-record (Paymentrecord) in sImportLineBuffer

        '13. Bel�p
        '--------
        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 59, 12))

        'Test if FIK payment
        If oCargo.PayCode = "301" Then
            ' 6. Kortartkode + 5. Identifikation
            '-----------------------------------
            '    sUnique_Id = Mid$(sImportLineBuffer, 30, 2) & Mid$(sImportLineBuffer, 16, 14)
            ' Changed 02.09.05 - Problems knowing correct FI-start ?
            sUnique_Id = Mid(sImportLineBuffer, 30, 2) & Mid(sImportLineBuffer, 15, 15)
            sMessage = Mid(sImportLineBuffer, 104, 20)
            If Not EmptyString(sMessage) Then
                ' for som Kortarts/Giros, there may be a notification
                oFreeText = oFreetexts.Add
                oFreeText.Text = PadRight(sMessage, 40, " ")
            End If
            ' Read next 30-record (payment) (or 99)
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadErhvervsgiroRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadErhvervsgiroRecord(oFile, oCargo.Special, 125)
        Else
            ' Set 18. Tekstlinje1 as first text to receiver
            oFreeText = oFreetexts.Add
            If Len(Trim(Mid(sImportLineBuffer, 104, 20))) > 0 Then
                oFreeText.Text = PadRight(Mid(sImportLineBuffer, 104, 20), 40, " ")
            End If
            ' Notification/Advice to receiver
            ' Upto 14 lines with info to receiver
            For i = 1 To 15
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadErhvervsgiroRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadErhvervsgiroRecord(oFile, oCargo.Special, 125)

                'If Mid$(sImportLineBuffer, 8, 2) = "30" Or Mid$(sImportLineBuffer, 8, 2) = "99" Then
                ' Changed 25.05.05
                If Mid(sImportLineBuffer, 8, 2) = "20" Or Mid(sImportLineBuffer, 8, 2) = "30" Or Mid(sImportLineBuffer, 8, 2) = "99" Then
                    ' New paymentrecord - get out of here
                    Exit For
                End If
                ' Upto three messages pr record
                sMessage = Mid(sImportLineBuffer, 11, 35)
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(sMessage, 40, " ")
                End If

                sMessage = Mid(sImportLineBuffer, 49, 35)
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(sMessage, 40, " ")
                End If

                sMessage = Mid(sImportLineBuffer, 87, 35)
                If Len(sMessage) > 0 Then
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = PadRight(sMessage, 40, " ")
                End If

            Next i
        End If

        ReadErhvervsGiro = sImportLineBuffer

    End Function
    Private Function enitelReadOneTimeFormat() As String
        ' Enitel konkursbo

        Dim oFreeText As Freetext
        Dim sMessage As String


        nMON_InvoiceAmount = Val(Replace(Trim(xDelim(sImportLineBuffer, ";", 5)), ",", ".")) * 100

        bAmountSetInvoice = True
        sMessage = xDelim(sImportLineBuffer, ";", 6)
        ' Several lines, split;
        Do
            If Len(sMessage) > 0 Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(sMessage, 40)
                sMessage = Mid(sMessage, 41)
            Else
                Exit Do
            End If
        Loop

        enitelReadOneTimeFormat = sImportLineBuffer

    End Function

    Private Function ReadOneTimeFormat() As String
        Dim oFreeText As Freetext
        Dim sMessage As String


        nMON_InvoiceAmount = Val(Replace(Trim(xDelim(sImportLineBuffer, ";", 9)), ",", ".")) * 100
        ' New run 20.03.03 with different layout
        'nMON_InvoiceAmount = Val(Replace(Trim$(xDelim(sImportLineBuffer, ";", 7)), ",", ".")) * 100

        bAmountSetInvoice = True
        sMessage = "Forikringstaker " & xDelim(sImportLineBuffer, ";", 1)

        '    Set oFreetext = oFreetexts.Add()
        '    sMessage = xDelim(sImportLineBuffer, ";", 13)
        '    oFreetext.Text = sMessage


        '    If bMATCH_Matched = True Then
        oFreeText = oFreetexts.Add()
        oFreeText.Text = "Utbetaling av eierverdier ifm omdanning "
        oFreeText = oFreetexts.Add()
        oFreeText.Text = "av Gjensidige NOR Spareforsikring til "
        oFreeText = oFreetexts.Add()
        oFreeText.Text = "aksjeselskap. Skatten er fratrukket."


        '    Else
        '        ' uten kontonr.
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 1."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 2."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 3."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 4."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 5."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 6."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 7."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 8."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 9."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 10."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Visual Banking calling BBS 11."
        '        Set oFreeText = oFreeTexts.Add()
        '        oFreeText.Text = "Kan du lese dette er alt OK."
        '
        '    End If

        sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)

        ReadOneTimeFormat = sImportLineBuffer

    End Function
    Private Function ReadDnB_TBIW_Standard() As String
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim sPaymentType As String

        sSTATEBANK_Code = xDelim(sImportLineBuffer, ";", 11, Chr(34))
        sSTATEBANK_Text = xDelim(sImportLineBuffer, ";", 12, Chr(34))

        ' AmountDetails
        ' Remove spaces, change from decimal , to .
        nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ";", 22, Chr(34)), ",", "."), " ", "")) * 100
        bAmountSetInvoice = True

        'Check paymenttype, read from sImportlinebuffer;
        sPaymentType = xDelim(sImportLineBuffer, ";", 1, Chr(34))


        If sPaymentType = "ACH" Or sPaymentType = "Salary" Then
            sMessage = xDelim(sImportLineBuffer, ";", 44, Chr(34))
            sREF_Own = xDelim(sImportLineBuffer, ";", 43, Chr(34))
        Else
            sMessage = xDelim(sImportLineBuffer, ";", 40, Chr(34))
        End If

        ' Cut message in chuncks of 40 and 40
        Do
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Left(sMessage, 40)
            sMessage = Mid(sMessage, 41)
            If Len(sMessage) = 0 Then
                Exit Do
            End If
        Loop

        ' Special treatment of FI-payments (Danish FI-cards)
        If sPaymentType = "FI" Then
            ' All FI-card info in one string, split it;
            ' Creditorno into E_Account;
            sUnique_Id = SplitFI(xDelim(sImportLineBuffer, ";", 33, Chr(34)), 2) '2=Kortart+BetalingsID
        End If

        If Not oFile.AtEndOfStream = True Then
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadExcelRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadExcelRecord(oFile) ', oProfile)
        End If

        ReadDnB_TBIW_Standard = sImportLineBuffer

    End Function
    Private Function ReadDnBNOR_UK_TBI_Standard() As String
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim sPaymentType As String


        ' AmountDetails
        ' Remove spaces, change from decimal , to .
        nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ",", 5, Chr(34)), ",", "."), " ", "")) * 100
        bAmountSetInvoice = True

        sMessage = xDelim(sImportLineBuffer, ",", 6, Chr(34))
        ' Cut message in chuncks of 40 and 40
        Do
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Left(sMessage, 40)
            sMessage = Mid(sMessage, 41)
            If Len(sMessage) = 0 Then
                Exit Do
            End If
        Loop

        ' added 21.12.2009
        ' XNET 24.01.2013 - changed from column 9 to 8
        sREF_Own = xDelim(sImportLineBuffer, ",", 8, Chr(34))

        '-------------------
        ' XNET 10.05.2012 - rest of function
        '-------------------
        ' There are two versions of this format -
        ' One version which I beleave is the "current" one with doc being spread to customers
        ' have Date in field 9, an other have Date in field 10
        ' To use the one with Date in field 10, then Special must be set to "OLD_STANDARD"
        ' The rest of the code in this function is changed to reflect this, by using
        ' the variable iAddColumn
        '------------------
        Dim iAddColumn As Integer
        If oCargo.Special = "OLD_STANDARD" Then
            iAddColumn = 1
        Else
            iAddColumn = 0
        End If

        sSTATEBANK_Code = xDelim(sImportLineBuffer, ",", 34 + iAddColumn, Chr(34))
        sSTATEBANK_Text = xDelim(sImportLineBuffer, ",", 35 + iAddColumn, Chr(34))

        If Not oFile.AtEndOfStream = True Then
            sImportLineBuffer = ReadExcelRecord(oFile) ', oProfile)
        Else
            sImportLineBuffer = ""
        End If

        ReadDnBNOR_UK_TBI_Standard = sImportLineBuffer

    End Function
    Private Function ReadDTAUS() As String
        Dim oFreeText As Freetext
        Dim iNoOfExtensionParts, iFreetextCounter As Short
        Dim iCounter, iFillerCount As Short

        iNoOfExtensionParts = Val(Mid(sImportLineBuffer, 186, 2))

        ' 1 signifies Euro, else DEM
        If Mid(sImportLineBuffer, 183, 1) = "1" Then
            nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 80, 11))
            bAmountSetInvoice = True
        Else
            nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 51, 11))
            bAmountSetInvoice = True
        End If
        sCustomerNo = Mid(sImportLineBuffer, 32, 13)

        iFreetextCounter = 0
        iFillerCount = 0
        'Retrieve the freetext
        If Len(Trim(Mid(sImportLineBuffer, 156, 27))) > 0 Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid(sImportLineBuffer, 156, 27)
        End If
        'Go through the Extension Parts to see if we have any additional freetext info
        ' Maximum 13 extension parts.
        For iCounter = 1 To iNoOfExtensionParts
            If Mid(sImportLineBuffer, 188 + (iCounter - 1) * 29 + iFillerCount, 2) = "02" Then
                iFreetextCounter = iFreetextCounter + 1
                If iFreetextCounter > 13 Then
                    'Error # 10028-020
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10028-020")
                Else
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 188 + (iCounter - 1) * 29 + 2 + iFillerCount, 27) & Space(13)
                End If
            End If
            'Add up the filler, used after some of the extension parts.
            Select Case iCounter
                Case 2
                    iFillerCount = iFillerCount + 11
                Case 6
                    iFillerCount = iFillerCount + 12
                Case 10
                    iFillerCount = iFillerCount + 12
                Case 14
                    iFillerCount = iFillerCount + 12
                Case Else
                    'No fillers
            End Select
        Next iCounter


        ReadDTAUS = sImportLineBuffer

    End Function
    Private Function ReadAvprickning() As String
        Dim oFreeText As Freetext
        Dim iFreetextCounter As Short
        Dim bReadLine As Boolean
        Dim bFirstTK01Imported As Boolean

        bFirstTK01Imported = False

        Do While True

            bReadLine = False
            Select Case Mid(sImportLineBuffer, 1, 2)

                'Domestic payment
                Case "01" 'Betalningspost
                    If bFirstTK01Imported Then
                        bReadLine = False
                        Exit Do
                    Else
                        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 54, 12))
                        If Mid(sImportLineBuffer, 66, 1) = "-" Then 'Markering f�r negativt belopp (minustecken) eller blank.
                            nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                        End If
                        nMON_TransferredAmount = nMON_InvoiceAmount
                        bAmountSetInvoice = True
                        bAmountSetTransferred = True
                        If Not EmptyString(Mid(sImportLineBuffer, 29, 25)) Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 29, 25)
                        End If
                        bReadLine = True
                        bFirstTK01Imported = True
                    End If

                Case "25" 'Informationspost
                    If Not EmptyString(Mid(sImportLineBuffer, 23, 40)) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 23, 40)
                        If Not EmptyString(Mid(sImportLineBuffer, 63)) Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = Mid(sImportLineBuffer, 63)
                        End If
                    End If
                    bReadLine = True

                Case "26", "27" 'Namnpost, Adresspost
                    Exit Do

                Case "29" 'Avst�mningspost - End batch
                    Exit Do

            End Select

            If bReadLine Then
                'Read a new record
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadLeverantorsbetalningarRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadLeverantorsbetalningarRecord(oFile)
            End If

        Loop

        ReadAvprickning = sImportLineBuffer

    End Function
    'XNET 27.08.2013 - Whole function
    Private Function ReadBgMax() As String
        Dim oFreeText As Freetext
        Dim iFreetextCounter As Integer
        Dim bReadLine As Boolean
        Dim bFirstTK01Imported As Boolean
        Dim bPaymentrecordFound As Boolean
        Dim sTmpAmount As String
        Dim sFreetextToAdd As String
        Dim bAddFreetext As Boolean
        Dim bExtraReferenceFound As Double

        bPaymentrecordFound = False
        bAddFreetext = False
        bExtraReferenceFound = False

        bFirstTK01Imported = False

        Do While True

            bReadLine = False
            Select Case Mid$(sImportLineBuffer, 1, 2)

                Case "15" 'Deposit record
                    bReadLine = False
                    Exit Do

                    'Domestic payment
                Case "20" '20 = Payment record
                    If bPaymentrecordFound Or bExtraReferenceFound Then

                        bReadLine = False
                        Exit Do
                    Else
                        bPaymentrecordFound = True
                    End If

                    '03.06.2010 - Use the reference indicator (temporary stored as a reference) to interpret the value in the reference field.
                    'I understand fuck all of this shit
                    'Added the IF-test around this, the Else is new
                    If sREF_Bank = "2" Or sREF_Bank = "3" Or sREF_Bank = "4" Then
                        sUnique_Id = Trim$(Mid$(sImportLineBuffer, 13, 25)) 'Payee�s reference, see Table 5.
                        'XokNET - 22.04.2014 - Next 2 lines
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sImportLineBuffer, 13, 25)

                    Else
                        If Not EmptyString(Mid$(sImportLineBuffer, 13, 25)) Then
                            sFreetextToAdd = Trim(Mid$(sImportLineBuffer, 13, 25))
                            bAddFreetext = True
                            '                Set oFreeText = oFreetexts.Add()
                            '                oFreeText.Text = Mid$(sImportLineBuffer, 13, 25)
                        End If
                    End If

                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 38, 18))
                    nMON_TransferredAmount = nMON_InvoiceAmount

                    bReadLine = True

                Case "21" '21 = Deduction record
                    If bPaymentrecordFound Or bExtraReferenceFound Then
                        bReadLine = False
                        Exit Do
                    Else
                        bPaymentrecordFound = True
                    End If
                    bAddFreetext = False

                    '03.06.2010 - Use the reference indicator (temporary stored as a reference) to interpret the value in the reference field.
                    'I understand fuck all of this shit
                    'Added the IF-test around this, the Else is new
                    If sREF_Bank = "2" Or sREF_Bank = "3" Or sREF_Bank = "4" Then
                        sUnique_Id = Trim$(Mid$(sImportLineBuffer, 13, 25)) 'Payee�s reference, see Table 5.
                        'XokNET - 22.04.2014 - Next 2 lines
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sImportLineBuffer, 13, 25)
                    Else
                        If Not EmptyString(Mid$(sImportLineBuffer, 13, 25)) Then
                            sFreetextToAdd = Trim(Mid$(sImportLineBuffer, 13, 25))
                            bAddFreetext = True
                            '                Set oFreeText = oFreetexts.Add()
                            '                oFreeText.Text = Mid$(sImportLineBuffer, 13, 25)
                        End If
                    End If

                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 38, 18)) * -1
                    nMON_TransferredAmount = nMON_InvoiceAmount

                    bReadLine = True

                Case "22" '22 = Extra reference, more than 1 reference is stated
                    If bExtraReferenceFound Then
                        'An extra reference is already found.
                        'Jump up to the payment to create a new invoice
                        bReadLine = False
                        Exit Do
                    Else
                        'Remove the Unique_Id (more than 1 is stated, and add a freetext with reference and amount
                        sUnique_Id = vbNullString
                        If Not EmptyString(Mid$(sImportLineBuffer, 13, 25)) Then
                            sUnique_Id = Trim$(Mid$(sImportLineBuffer, 13, 25)) 'Payee�s reference, see Table 5.
                            oFreeText = oFreetexts.Add()
                            'XokNET - 22.04.2014 - Changed next line
                            oFreeText.Text = Mid$(sImportLineBuffer, 13, 25)

                        End If
                        'It seens that the amount is either stated in the 20-record (then the first 22-record should not alter the amountof the invoice, and
                        '  if there is more 22 or 23 the amount will be set to 0,-
                        'If the amount is set on the first 22, it seems that it is also set in the preceeding 22/23.
                        'I've tested about 25 BG Files, and the import is now fine regarding to amount.
                        If Not Mid$(sImportLineBuffer, 38, 18) = "000000000000000000" Then
                            nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 38, 18))
                            nMON_TransferredAmount = nMON_InvoiceAmount

                            sTmpAmount = RemoveLeadingCharacters(Mid$(sImportLineBuffer, 38, 18), "0")
                            sTmpAmount = Left(sTmpAmount, Len(sTmpAmount) - 2) & "," & Right$(sTmpAmount, 2)
                            ' XOKNET 16.12.2015 - if no text in 13,25 we have no oFreetext
                            If oFreeText Is Nothing Then
                                oFreeText = oFreetexts.Add()
                            End If
                            oFreeText.Text = LRS(60060) & ":  " & sTmpAmount & " / "
                        Else
                            'nMON_InvoiceAmount = 0
                            'nMON_TransferredAmount = nMON_InvoiceAmount
                        End If
                        bAddFreetext = False
                        bExtraReferenceFound = True

                        bReadLine = True
                    End If

                Case "23" '23 = Extra reference, deductionrecord
                    If bExtraReferenceFound Then
                        'An extra reference is already found.
                        'Jump up to the payment to create a new invoice
                        bReadLine = False
                        Exit Do
                    Else
                        'Remove the Unique_Id (more than 1 is stated, and add a freetext with reference and amount
                        sUnique_Id = vbNullString
                        If Not EmptyString(Mid$(sImportLineBuffer, 13, 25)) Then
                            sUnique_Id = Trim$(Mid$(sImportLineBuffer, 13, 25)) 'Payee�s reference, see Table 5.
                            oFreeText = oFreetexts.Add()
                            'XokNET - 22.04.2014 - Changed next line
                            oFreeText.Text = Mid$(sImportLineBuffer, 13, 25)

                        End If
                        If Not Mid$(sImportLineBuffer, 38, 18) = "000000000000000000" Then
                            nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 38, 18)) * -1
                            nMON_TransferredAmount = nMON_InvoiceAmount

                            sTmpAmount = RemoveLeadingCharacters(Mid$(sImportLineBuffer, 38, 18), "0")
                            sTmpAmount = "-" & Left(sTmpAmount, Len(sTmpAmount) - 2) & "," & Right$(sTmpAmount, 2)
                            ' XOKNET 16.12.2015 - if no text in 13,25 we have no oFreetext
                            If oFreeText Is Nothing Then
                                oFreeText = oFreetexts.Add()
                            End If
                            oFreeText.Text = LRS(60060) & ":  " & sTmpAmount & " / "
                        Else
                            nMON_InvoiceAmount = 0
                            nMON_TransferredAmount = nMON_InvoiceAmount
                        End If
                        bAddFreetext = False
                        bExtraReferenceFound = True

                        bReadLine = True
                    End If

                Case "25" 'Informationspost
                    If Not EmptyString(Mid$(sImportLineBuffer, 3, 50)) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sImportLineBuffer, 3, 40)
                        If Not EmptyString(Mid$(sImportLineBuffer, 43, 10)) Then
                            oFreeText = oFreetexts.Add()
                            oFreeText.Text = RTrim$(Mid$(sImportLineBuffer, 43, 10))
                        End If
                    End If
                    bReadLine = True

                Case "26", "27" 'Namnpost, Adresspost
                    Exit Do

                Case "29" 'Avst�mningspost - End batch
                    Exit Do

            End Select

            If bReadLine Then
                'Read a new record
                sImportLineBuffer = ReadLeverantorsbetalningarRecord(oFile)
            End If

        Loop

        If bAddFreetext Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = sFreetextToAdd
            bAddFreetext = False
        End If

        ReadBgMax = sImportLineBuffer

    End Function
    Private Function ReadOCR_Bankgirot() As String
        Dim oFreeText As Freetext
        Dim bReadLine As Boolean
        Dim sAmount As String

        'Transaktionspost
        sAmount = Mid(sImportLineBuffer, 28, 13)
        If Not vbIsNumeric(Right(sAmount, 1), "0123456789") Then
            nMON_InvoiceAmount = TranslateSignedAmount(sAmount, , (BabelFiles.FileType.OCR_Bankgirot))
        Else
            nMON_InvoiceAmount = Val(sAmount)
        End If

        nMON_TransferredAmount = nMON_InvoiceAmount
        bAmountSetInvoice = True
        bAmountSetTransferred = True

        'New 13.02.2008
        'The KID should be imported as Freetext, ref SG Finans
        If oCargo.PayCode = "601" Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = "KID: " & Trim(Mid(sImportLineBuffer, 3, 25))
        Else
            sUnique_Id = Trim(Mid(sImportLineBuffer, 3, 25))
        End If

        ReadOCR_Bankgirot = sImportLineBuffer

    End Function
    Private Function ReadDnBTBUKIncoming() As String
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim bFromLevelUnder As Boolean

        Do
            Select Case Left(sImportLineBuffer, 8)
                Case "940SWI02"
                    If bFromLevelUnder Then
                        ' New SWI02, jump up the ladder, all the way to BabelFile,
                        ' to create another batch
                        Exit Do
                    Else
                        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 104, 15))
                        nMON_TransferredAmount = nMON_InvoiceAmount
                        bAmountSetInvoice = True

                        ' Add Info-part to freetext
                        '18.06.2005 - Commented by Kjell
                        'Set oFreeText = oFreetexts.Add
                        'oFreeText.Text = Mid$(sImportLineBuffer, 157, 34)

                    End If

                Case "940SWI04"
                    ' Payers reference
                    sMessage = Mid(sImportLineBuffer, 91, 65)
                    If Len(sMessage) > 0 Then
                        ' Info seems to be divided, first part=32
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = PadRight(Left(sMessage, 32), 40, " ")
                        If Len(Trim(Mid(sMessage, 33))) > 0 Then
                            oFreeText = oFreetexts.Add
                            oFreeText.Text = PadRight(Mid(sMessage, 33), 40, " ")
                        End If
                    End If

                Case Else
                    Exit Do

            End Select

            bFromLevelUnder = True
            ' Read another record;
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBTBUKIncomingRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadDnBTBUKIncomingRecord(oFile)

        Loop

        ReadDnBTBUKIncoming = sImportLineBuffer

    End Function
    Private Function ReadDnBTBWK() As String
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim bFromLevelUnder As Boolean

        Do
            Select Case Left(sImportLineBuffer, 8)
                Case "940SWI02"
                    If bFromLevelUnder Then
                        ' New SWI02, jump up the ladder, all the way to Batch,
                        ' to create another payment
                        Exit Do

                    End If

                Case "940SWI07"
                    ' Payers reference, read first 40 chars
                    sMessage = Mid(sImportLineBuffer, 91, 40)
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = sMessage

                Case Else
                    Exit Do

            End Select

            bFromLevelUnder = True
            ' Read another record;
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBTBWKRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadDnBTBWKRecord(oFile)

        Loop

        ReadDnBTBWK = sImportLineBuffer

    End Function
    Public Function SaveData(ByVal bSQLServer As Boolean, ByRef myBBDBConnectionAccess As System.Data.OleDb.OleDbConnection, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByRef bMatchedItemsOnly As Boolean, ByRef lBabelFile_ID As Integer, ByRef lBatch_ID As Integer, ByRef lPayment_ID As Integer, ByRef lMyID As Integer, ByRef bUseCommitAgainstBBDB As Boolean) As Boolean
        Dim sMySQL, sErrorString As String
        Dim bReturnValue As Boolean
        Dim lCounter As Integer
        Dim oFreeText As Freetext

        On Error GoTo ERRSaveData

        sMySQL = "INSERT INTO Invoice(Company_ID, Babelfile_ID, Batch_ID, Payment_ID, "
        'GUNNRITA
        'sMySQL = sMySQL & "Invoice_ID, XIndex, StatusCode, Cancel, "
        sMySQL = sMySQL & "Invoice_ID, StatusCode, Cancel, "
        sMySQL = sMySQL & "MON_DiscountAmount, MON_InvoiceAmount, MON_TransferredAmount, MON_AccountAmount, MON_LocalAmount, "
        sMySQL = sMySQL & "AmountSetTransferred, AmountSetInvoice, REF_Own, REF_Bank, "
        sMySQL = sMySQL & "InvoiceNo, CustomerNo, Unique_Id, STATEBANK_Code, "
        sMySQL = sMySQL & "STATEBANK_Text, STATEBANK_DATE, Extra1, MATCH_Original, "
        sMySQL = sMySQL & "MATCH_Final, MATCH_Matched, MyField, "
        sMySQL = sMySQL & "MATCH_Type, MATCH_ID, MATCH_ClientNo, MATCH_VATCode, MATCH_PartlyPaid, MATCH_ERPName, "
        ' 11.12.2014 added several fields
        sMySQL = sMySQL & "MON_CurrencyAmount, MON_Currency, Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10, MyField2, MyField3, "
        sMySQL = sMySQL & "BackPayment, BackPaymentAccount) "
        If oProfile Is Nothing Then
            sMySQL = sMySQL & "VALUES(" & "1" & ", " & Trim(Str(lBabelFile_ID))
        Else
            sMySQL = sMySQL & "VALUES(" & Trim(Str(oProfile.Company_ID)) & ", " & Trim(Str(lBabelFile_ID))
        End If

        sMySQL = sMySQL & ", " & Trim(Str(lBatch_ID)) & ", " & Trim(Str(lPayment_ID)) & ", " & Trim(Str(lMyID))
        'GUNNRITA
        'sMySQL = sMySQL & ", " & Trim$(Str(nIndex)) & ", '" & sStatusCode & "', " & Str(bCancel)
        If bSQLServer Then
            sMySQL = sMySQL & ", '" & sStatusCode & "', " & CInt(bCancel)
        Else
            sMySQL = sMySQL & ", '" & sStatusCode & "', " & Str(bCancel)
        End If
        sMySQL = sMySQL & ", " & Trim(Str(nMON_DiscountAmount)) & ", " & Trim(Str(nMON_InvoiceAmount)) & ", " & Trim(Str(nMON_TransferredAmount))
        sMySQL = sMySQL & ", " & Trim(Str(nMON_AccountAmount)) & ", " & Trim(Str(nMON_LocalAmount))
        If bSQLServer Then
            sMySQL = sMySQL & ", " & CInt(bAmountSetTransferred) & ", " & CInt(bAmountSetInvoice)
        Else
            sMySQL = sMySQL & ", " & Str(bAmountSetTransferred) & ", " & Str(bAmountSetInvoice)
        End If
        ' 26.01.2010, lagt in left$( p� tre ReplaceIllegalChar..
        sMySQL = sMySQL & ", '" & Left(ReplaceIllegalCharactersInString(sREF_Own), 50) & "', '" & Left(ReplaceIllegalCharactersInString(sREF_Bank), 50)
        sMySQL = sMySQL & "', '" & Left(ReplaceIllegalCharactersInString(sInvoiceNo), 30) & "', '" & Left(ReplaceIllegalCharactersInString(sCustomerNo), 60)
        sMySQL = sMySQL & "', '" & Left(sUnique_Id, 30) & "', '" & Left(sSTATEBANK_Code, 5)  '13.08.2018 added left,5 on sStatebank_Code, 28.11.2016 - Added Left 30 on Unique_ID
        sMySQL = sMySQL & "', '" & sSTATEBANK_Text & "', '" & sSTATEBANK_DATE
        If bSQLServer Then
            sMySQL = sMySQL & "', '" & sExtra1 & "', " & CInt(bMATCH_Original)
            sMySQL = sMySQL & ", " & CInt(bMATCH_Final) & ", " & CInt(bMATCH_Matched)
        Else
            sMySQL = sMySQL & "', '" & sExtra1 & "', " & Str(bMATCH_Original)
            sMySQL = sMySQL & ", " & Str(bMATCH_Final) & ", " & Str(bMATCH_Matched)
        End If
        sMySQL = sMySQL & ", '" & sMyField & "', " & Trim(Str(CDbl(eMATCH_Type)))
        If bSQLServer Then
            sMySQL = sMySQL & ", '" & sMATCH_ID & "', '" & sMATCH_ClientNo & "', '" & sMATCH_VATCode & "', " & CInt(bMATCH_PartlyPaid)
        Else
            sMySQL = sMySQL & ", '" & sMATCH_ID & "', '" & sMATCH_ClientNo & "', '" & sMATCH_VATCode & "', " & Str(bMATCH_PartlyPaid)
        End If
        ' 26.01.2010, lagt in left$( p� ReplaceIllegalChar..
        sMySQL = sMySQL & ", '" & Left(ReplaceIllegalCharactersInString(sMATCH_ERPName), 255) & "'"
        sMySQL = sMySQL & ", " & Trim(Str(nMON_CurrencyAmount))
        sMySQL = sMySQL & ", '" & sMON_Currency
        sMySQL = sMySQL & "', '" & Left(sDim1, 20)
        sMySQL = sMySQL & "', '" & Left(sDim2, 20)
        sMySQL = sMySQL & "', '" & Left(sDim3, 20)
        sMySQL = sMySQL & "', '" & Left(sDim4, 20)
        sMySQL = sMySQL & "', '" & Left(sDim5, 20)
        sMySQL = sMySQL & "', '" & Left(sDim6, 20)
        sMySQL = sMySQL & "', '" & Left(sDim7, 20)
        sMySQL = sMySQL & "', '" & Left(sDim8, 20)
        sMySQL = sMySQL & "', '" & Left(sDim9, 20)
        sMySQL = sMySQL & "', '" & Left(sDim10, 20)
        sMySQL = sMySQL & "', '" & Left(sMyField2, 50)
        sMySQL = sMySQL & "', '" & Left(sMyField3, 50)
        If bSQLServer Then
            sMySQL = sMySQL & "', " & CInt(bBackPayment) & ", '" & Left(sBackPaymentAccount, 35)
        Else
            sMySQL = sMySQL & "', " & Str(bBackPayment) & ", '" & Left(sBackPaymentAccount, 35)
        End If
        sMySQL = sMySQL & "')"
        'Not saved:
        '    sMySQL = "ALTER TABLE Invoice ADD CargoFTX Text(100) NULL"
        'XNET - 13.12.2010 - Next comment
        'InvoiceDate

        myBBDB_AccessCommand.CommandText = sMySQL
        If Not myBBDB_AccessCommand.ExecuteNonQuery() = 1 Then
            Err.Raise(14000, "Invoice", sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        Else
            'MsgBox "Babelfile saving OK"
        End If
        'If Not ExecuteTheSQL(cnProfile, sMySQL, sErrorString, bUseCommitAgainstBBDB) Then
        '    MsgBox(sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        'Else
        '    'MsgBox "Invoice saved OK"
        'End If

        lCounter = 0
        If Freetexts.Count > 0 Then
            For Each oFreeText In Freetexts
                lCounter = lCounter + 1
                If oProfile Is Nothing Then
                    bReturnValue = oFreeText.SaveData(bSQLServer, myBBDBConnectionAccess, myBBDB_AccessCommand, bMatchedItemsOnly, lBabelFile_ID, lBatch_ID, lPayment_ID, lMyID, lCounter, "1", bUseCommitAgainstBBDB)
                Else
                    bReturnValue = oFreeText.SaveData(bSQLServer, myBBDBConnectionAccess, myBBDB_AccessCommand, bMatchedItemsOnly, lBabelFile_ID, lBatch_ID, lPayment_ID, lMyID, lCounter, oProfile.Company_ID, bUseCommitAgainstBBDB)
                End If
                If bReturnValue = False Then
                    Exit For
                End If
            Next oFreeText
        Else
            'No freetext
            bReturnValue = True
        End If
       

        SaveData = bReturnValue

        Exit Function

ERRSaveData:
        If Err.Number = 999 Then
            Err.Raise(Err.Number, Err.Source, Err.Description)
        Else
            Err.Raise(999, "Invoice", Err.Description & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        End If

    End Function
    'Public Function RetrieveData(ByRef cnProfile As String, ByRef rsInvoice As String, ByRef bOpenItemsOnly As Boolean, ByRef iCompanyNo As Short, ByRef lBabelFile_ID As Integer, ByRef lBatch_ID As Integer, ByRef lPayment_ID As Integer, ByRef bUseProfile As Boolean) As Boolean
    '        Dim lCounter As Integer
    '        Dim oFreeText As Freetext
    '        Dim sMySQL As String
    '        Dim rsFreetext As New ADODB.Recordset
    '        Dim lDBInvoiceIndex As Integer
    '        Dim bReturnValue As Boolean

    '        On Error GoTo errorRetrieveData

    '        'Retrieve and save the data to the local variables
    '        lDBInvoiceIndex = rsInvoice.Fields("Invoice_ID").Value
    '        'GUNNRITA
    '        'nIndex = rsInvoice!Xindex
    '        nIndex = rsInvoice.Fields("Invoice_ID").Value
    '        sStatusCode = rsInvoice.Fields("StatusCode").Value
    '        bCancel = rsInvoice.Fields("Cancel").Value
    '        If IsDbNull(rsInvoice.Fields("MON_DiscountAmount").Value) Then
    '            nMON_DiscountAmount = 0
    '        Else
    '            nMON_DiscountAmount = rsInvoice.Fields("MON_DiscountAmount").Value
    '        End If
    '        nMON_InvoiceAmount = rsInvoice.Fields("MON_InvoiceAmount").Value
    '        nMON_TransferredAmount = rsInvoice.Fields("MON_TransferredAmount").Value
    '        nMON_AccountAmount = rsInvoice.Fields("MON_AccountAmount").Value
    '        nMON_LocalAmount = rsInvoice.Fields("MON_LocalAmount").Value
    '        bAmountSetTransferred = rsInvoice.Fields("AmountSetTransferred").Value
    '        bAmountSetInvoice = rsInvoice.Fields("AmountSetInvoice").Value
    '        sREF_Own = RetrieveIllegalCharactersInString(rsInvoice.Fields("REF_Own").Value)
    '        sREF_Bank = RetrieveIllegalCharactersInString(rsInvoice.Fields("REF_Bank").Value)
    '        sInvoiceNo = RetrieveIllegalCharactersInString(rsInvoice.Fields("InvoiceNo").Value)
    '        sCustomerNo = RetrieveIllegalCharactersInString(rsInvoice.Fields("CustomerNo").Value)
    '        sUnique_Id = rsInvoice.Fields("Unique_Id").Value
    '        sSTATEBANK_Code = rsInvoice.Fields("STATEBANK_Code").Value
    '        sSTATEBANK_Text = rsInvoice.Fields("STATEBANK_Text").Value
    '        sSTATEBANK_DATE = rsInvoice.Fields("STATEBANK_DATE").Value
    '        If Not IsDbNull(rsInvoice.Fields("Extra1").Value) Then
    '            sExtra1 = rsInvoice.Fields("Extra1").Value
    '        End If
    '        bMATCH_Original = rsInvoice.Fields("MATCH_Original").Value
    '        bMATCH_Final = rsInvoice.Fields("MATCH_Final").Value
    '        bMATCH_Matched = rsInvoice.Fields("MATCH_Matched").Value
    '        If Not IsDbNull(rsInvoice.Fields("MyField").Value) Then
    '            sMyField = rsInvoice.Fields("MyField").Value
    '        End If
    '        eMATCH_Type = rsInvoice.Fields("match_type").Value
    '        sMATCH_ID = rsInvoice.Fields("MATCH_ID").Value
    '        If Not IsDbNull(rsInvoice.Fields("MATCH_ClientNo").Value) Then
    '            sMATCH_ClientNo = rsInvoice.Fields("MATCH_ClientNo").Value
    '        Else
    '            sMATCH_ClientNo = ""
    '        End If
    '        If Not IsDbNull(rsInvoice.Fields("MATCH_VATCode").Value) Then
    '            sMATCH_VATCode = rsInvoice.Fields("MATCH_VATCode").Value
    '        Else
    '            sMATCH_VATCode = ""
    '        End If
    '        If Not IsDbNull(rsInvoice.Fields("MATCH_ERPName").Value) Then
    '            sMATCH_ERPName = RetrieveIllegalCharactersInString(rsInvoice.Fields("MATCH_ERPName").Value)
    '        Else
    '            sMATCH_ERPName = ""
    '        End If
    '        bMATCH_PartlyPaid = rsInvoice.Fields("MATCH_PartlyPaid").Value

    '        sMySQL = "SELECT * FROM Freetext WHERE Company_ID = " & Trim(Str(iCompanyNo)) & " AND BabelFile_ID = " & Trim(Str(lBabelFile_ID)) & " AND Batch_ID = " & Trim(Str(lBatch_ID)) & " AND Payment_ID = " & Trim(Str(lPayment_ID)) & " AND Invoice_ID = " & Trim(Str(lDBInvoiceIndex))
    '        rsFreetext.Open(sMySQL, cnProfile, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

    '        'Create the collection
    '        If rsFreetext.RecordCount > 0 Then
    '            oFreetexts = New Freetexts
    '            rsFreetext.MoveFirst()
    '            For lCounter = 1 To rsFreetext.RecordCount
    '                oFreeText = oFreetexts.Add(Trim(Str(rsFreetext.Fields("Freetext_ID").Value)))
    '                If oFreeText.RetrieveData(rsFreetext) Then
    '                    bReturnValue = True
    '                Else
    '                    bReturnValue = False
    '                    Exit For
    '                End If
    '                rsFreetext.MoveNext()
    '            Next lCounter
    '        Else
    '            'No freetext. That's OK
    '            bReturnValue = True
    '        End If

    '        'UPGRADE_NOTE: Object rsFreetext may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        rsFreetext = Nothing

    '        RetrieveData = bReturnValue

    '        Exit Function

    'errorRetrieveData:
    '        If Not rsFreetext Is Nothing Then
    '            'UPGRADE_NOTE: Object rsFreetext may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            rsFreetext = Nothing
    '        End If
    '        If Err.Number = 999 Then
    '            Err.Raise(Err.Number, Err.Source, Err.Description)
    '        Else
    '            Err.Raise(999, "Invoice", Err.Description)
    '        End If

    'End Function
    Public Function RetrieveData(ByVal bSQLServer As Boolean, ByRef myBBDB_AccessInvoiceReader As System.Data.OleDb.OleDbDataReader, ByRef myBBDB_AccessConnection As System.Data.OleDb.OleDbConnection, ByRef bOpenItemsOnly As Boolean, ByRef iCompanyNo As Short, ByRef lBabelFile_ID As Integer, ByRef lBatch_ID As Integer, ByRef lPayment_ID As Integer, ByRef bUseProfile As Boolean) As Boolean
        Dim lCounter As Integer
        Dim oFreeText As Freetext
        Dim sMySQL As String
        Dim lDBInvoiceIndex As Integer
        Dim bReturnValue As Boolean
        Dim sSelectPart As String
        Dim myBBDB_AccessFreetextReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessFreetextCommand As System.Data.OleDb.OleDbCommand = Nothing

        'On Error GoTo errorRetrieveData
        Try

            'Retrieve and save the data to the local variables
            lDBInvoiceIndex = myBBDB_AccessInvoiceReader.GetInt32(0) 'Invoice_ID
            nIndex = myBBDB_AccessInvoiceReader.GetInt32(0) 'Index
            sStatusCode = myBBDB_AccessInvoiceReader.GetString(1) 'StatusCode
            bCancel = myBBDB_AccessInvoiceReader.GetBoolean(2) 'Cancel
            If Not myBBDB_AccessInvoiceReader.IsDBNull(3) Then
                nMON_DiscountAmount = myBBDB_AccessInvoiceReader.GetDouble(3) 'MON_DiscountAmount
            Else
                nMON_DiscountAmount = 0
            End If
            nMON_InvoiceAmount = myBBDB_AccessInvoiceReader.GetDouble(4) 'MON_InvoiceAmount
            nMON_TransferredAmount = myBBDB_AccessInvoiceReader.GetDouble(5) 'MON_TransferredAmount
            nMON_AccountAmount = myBBDB_AccessInvoiceReader.GetDouble(6) 'MON_AccountAmount
            nMON_LocalAmount = myBBDB_AccessInvoiceReader.GetDouble(7) 'MON_LocalAmount
            bAmountSetTransferred = myBBDB_AccessInvoiceReader.GetBoolean(8) 'AmountSetTransferred
            bAmountSetInvoice = myBBDB_AccessInvoiceReader.GetBoolean(9) 'AmountSetInvoice
            sREF_Own = RetrieveIllegalCharactersInString(myBBDB_AccessInvoiceReader.GetString(10)) 'REF_Own
            If sREF_Own Is Nothing Then
                sREF_Own = ""
            End If
            sREF_Bank = RetrieveIllegalCharactersInString(myBBDB_AccessInvoiceReader.GetString(11)) 'REF_Bank
            If sREF_Bank Is Nothing Then
                sREF_Bank = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(12) Then
                sInvoiceNo = RetrieveIllegalCharactersInString(myBBDB_AccessInvoiceReader.GetString(12)) 'InvoiceNo
            End If
            If sInvoiceNo Is Nothing Then
                sInvoiceNo = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(13) Then
                sCustomerNo = RetrieveIllegalCharactersInString(myBBDB_AccessInvoiceReader.GetString(13)) 'CustomerNo
            End If
            If sCustomerNo Is Nothing Then
                sCustomerNo = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(14) Then
                sUnique_Id = myBBDB_AccessInvoiceReader.GetString(14) 'Unique_Id
            End If
            If sUnique_Id Is Nothing Then
                sUnique_Id = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(15) Then
                sSTATEBANK_Code = myBBDB_AccessInvoiceReader.GetString(15) 'STATEBANK_Code
            End If
            sSTATEBANK_Text = myBBDB_AccessInvoiceReader.GetString(16) 'STATEBANK_Text
            If Not myBBDB_AccessInvoiceReader.IsDBNull(17) Then
                sSTATEBANK_DATE = myBBDB_AccessInvoiceReader.GetString(17) 'STATEBANK_DATE
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(18) Then
                sExtra1 = myBBDB_AccessInvoiceReader.GetString(18) 'Extra1
            End If
            bMATCH_Original = myBBDB_AccessInvoiceReader.GetBoolean(19) 'MATCH_Original
            bMATCH_Final = myBBDB_AccessInvoiceReader.GetBoolean(20) 'MATCH_Final
            bMATCH_Matched = myBBDB_AccessInvoiceReader.GetBoolean(21) 'MATCH_Matched
            If Not myBBDB_AccessInvoiceReader.IsDBNull(22) Then
                sMyField = myBBDB_AccessInvoiceReader.GetString(22) 'MyField
            End If
            eMATCH_Type = myBBDB_AccessInvoiceReader.GetInt32(23) 'MATCH_Type
            If Not myBBDB_AccessInvoiceReader.IsDBNull(24) Then
                sMATCH_ID = myBBDB_AccessInvoiceReader.GetString(24) 'MATCH_ID
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(25) Then
                sMATCH_ClientNo = myBBDB_AccessInvoiceReader.GetString(25) 'MATCH_ClientNo
            Else
                sMATCH_ClientNo = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(26) Then
                sMATCH_VATCode = myBBDB_AccessInvoiceReader.GetString(26) 'MATCH_VATCode
            Else
                sMATCH_VATCode = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(27) Then
                sMATCH_ERPName = myBBDB_AccessInvoiceReader.GetString(27) 'MATCH_ERPName
            Else
                sMATCH_ERPName = ""
            End If
            bMATCH_PartlyPaid = myBBDB_AccessInvoiceReader.GetBoolean(28) 'MATCH_PartlyPaid

            ' 11.12.2014 added several fields
            If Not myBBDB_AccessInvoiceReader.IsDBNull(29) Then
                nMON_CurrencyAmount = myBBDB_AccessInvoiceReader.GetDouble(29) 'MON_CurrencyAmount
            Else
                nMON_CurrencyAmount = 0
            End If

            If Not myBBDB_AccessInvoiceReader.IsDBNull(30) Then
                sMON_Currency = myBBDB_AccessInvoiceReader.GetString(30).Trim 'MON_Currency
            Else
                sMON_Currency = ""
            End If

            If Not myBBDB_AccessInvoiceReader.IsDBNull(31) Then
                sDim1 = myBBDB_AccessInvoiceReader.GetString(31).Trim 'dim1
            Else
                sDim1 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(32) Then
                sDim2 = myBBDB_AccessInvoiceReader.GetString(32).Trim 'dim2
            Else
                sDim2 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(33) Then
                sDim3 = myBBDB_AccessInvoiceReader.GetString(33).Trim 'dim3
            Else
                sDim3 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(34) Then
                sDim4 = myBBDB_AccessInvoiceReader.GetString(34).Trim 'dim4
            Else
                sDim4 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(35) Then
                sDim5 = myBBDB_AccessInvoiceReader.GetString(35).Trim 'dim1
            Else
                sDim5 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(36) Then
                sDim6 = myBBDB_AccessInvoiceReader.GetString(36).Trim 'dim6
            Else
                sDim6 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(37) Then
                sDim7 = myBBDB_AccessInvoiceReader.GetString(37).Trim 'dim7
            Else
                sDim7 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(38) Then
                sDim8 = myBBDB_AccessInvoiceReader.GetString(38).Trim 'dim8
            Else
                sDim8 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(39) Then
                sDim9 = myBBDB_AccessInvoiceReader.GetString(39).Trim 'dim9
            Else
                sDim9 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(40) Then
                sDim10 = myBBDB_AccessInvoiceReader.GetString(40).Trim 'dim10   ' NB 01.03.2015 rettet fra (10) til (40)
            Else
                sDim10 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(41) Then
                sMyField2 = myBBDB_AccessInvoiceReader.GetString(41).Trim 'MyField2
            Else
                sMyField2 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(42) Then
                sMyField3 = myBBDB_AccessInvoiceReader.GetString(42).Trim 'MyField3
            Else
                sMyField3 = ""
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(43) Then
                bBackPayment = myBBDB_AccessInvoiceReader.GetBoolean(43) 'BackPayment
            Else
                bBackPayment = False
            End If
            If Not myBBDB_AccessInvoiceReader.IsDBNull(44) Then
                sBackPaymentAccount = myBBDB_AccessInvoiceReader.GetString(44).Trim 'BackPaymentAccount
            Else
                sBackPaymentAccount = ""
            End If

            sSelectPart = "SELECT Freetext_ID, Qualifier, XText "

            '07.02.2017 added   & "ORDER BY Freetext_ID"
            sMySQL = sSelectPart & "FROM [Freetext] WHERE Company_ID = " & Trim(Str(iCompanyNo)) & " AND BabelFile_ID = " & Trim(Str(lBabelFile_ID)) & " AND Batch_ID = " & Trim(Str(lBatch_ID)) & " AND Payment_ID = " & Trim(Str(lPayment_ID)) & " AND Invoice_ID = " & Trim(Str(lDBInvoiceIndex)) & " ORDER BY Freetext_ID"

            If myBBDB_AccessFreetextCommand Is Nothing Then
                myBBDB_AccessFreetextCommand = myBBDB_AccessConnection.CreateCommand()
                myBBDB_AccessFreetextCommand.CommandType = CommandType.Text
                'Default er CommandTimeout = 30
                myBBDB_AccessFreetextCommand.CommandTimeout = 120
                myBBDB_AccessFreetextCommand.Connection = myBBDB_AccessConnection
            End If

            If Not myBBDB_AccessFreetextReader Is Nothing Then
                If Not myBBDB_AccessFreetextReader.IsClosed Then
                    myBBDB_AccessFreetextReader.Close()
                End If
            End If

            myBBDB_AccessFreetextCommand.CommandText = sMySQL

            myBBDB_AccessFreetextReader = myBBDB_AccessFreetextCommand.ExecuteReader

            'Create the collection
            If myBBDB_AccessFreetextReader.HasRows Then
                oFreetexts = New Freetexts
                Do While myBBDB_AccessFreetextReader.Read()
                    oFreeText = oFreetexts.Add()
                    'Old code 17.10.2011
                    'oFreeText = oFreetexts.Add(Trim(Str(rsFreetext.Fields("Freetext_ID").Value)))
                    If oFreeText.RetrieveData(bSQLServer, myBBDB_AccessFreetextReader) Then
                        bReturnValue = True
                    Else
                        bReturnValue = False
                        Exit Do
                    End If
                Loop
            Else
                'No freetext. That's OK
                bReturnValue = True
            End If

            'If Not myBBDB_AccessFreetextReader Is Nothing Then
            '    If Not myBBDB_AccessFreetextReader.IsClosed Then
            '        myBBDB_AccessFreetextReader.Close()
            '    End If
            '    myBBDB_AccessFreetextReader = Nothing
            'End If
            'If Not myBBDB_AccessFreetextCommand Is Nothing Then
            '    myBBDB_AccessFreetextCommand.Dispose()
            '    myBBDB_AccessFreetextCommand = Nothing
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)

        Finally
            If Not myBBDB_AccessFreetextReader Is Nothing Then
                If Not myBBDB_AccessFreetextReader.IsClosed Then
                    myBBDB_AccessFreetextReader.Close()
                End If
                myBBDB_AccessFreetextReader = Nothing
            End If
            If Not myBBDB_AccessFreetextCommand Is Nothing Then
                myBBDB_AccessFreetextCommand.Dispose()
                myBBDB_AccessFreetextCommand = Nothing
            End If

        End Try

        '            RetrieveData = bReturnValue

        '            Exit Function

        'errorRetrieveData:
        '            If Not myBBDB_AccessFreetextReader Is Nothing Then
        '                If Not myBBDB_AccessFreetextReader.IsClosed Then
        '                    myBBDB_AccessFreetextReader.Close()
        '                End If
        '                myBBDB_AccessFreetextReader = Nothing
        '            End If
        '            If Not myBBDB_AccessFreetextCommand Is Nothing Then
        '                myBBDB_AccessFreetextCommand.Dispose()
        '                myBBDB_AccessFreetextCommand = Nothing
        '            End If

        '            If Err.Number = 999 Then
        '                Err.Raise(Err.Number, Err.Source, Err.Description)
        '            Else
        '                Err.Raise(999, "Invoice", Err.Description)
        '            End If

        Return bReturnValue

    End Function
    Public Function CopyObject() As vbBabel.Invoice
        Dim oCopyInvoice As vbBabel.Invoice
        'Dim oFreetextOrig As vbBabel.FreeText
        'Dim oCopyFreetext As vbBabel.FreeText

        oCopyInvoice = New vbBabel.Invoice
        oCopyInvoice.VB_Profile = oProfile
        oCopyInvoice.Cargo = oCargo
        'For Each oFreetextOrig In Freetexts
        '    Set oCopyFreetext = oCopyInvoice.Freetexts.Add
        '    oCopyFreetext.Qualifier = oFreetextOrig.Qualifier
        '    oCopyFreetext.Col = oFreetextOrig.Col
        '    oCopyFreetext.Row = oFreetextOrig.Row
        '    oCopyFreetext.Text = oFreetextOrig.Text
        'Next oFreetextOrig

        oCopyInvoice.StatusCode = sStatusCode
        oCopyInvoice.Visma_StatusCode = sVisma_StatusCode
        oCopyInvoice.Cancel = bCancel
        oCopyInvoice.MON_DiscountAmount = nMON_DiscountAmount
        oCopyInvoice.MON_InvoiceAmount = nMON_InvoiceAmount
        oCopyInvoice.MON_TransferredAmount = nMON_TransferredAmount
        oCopyInvoice.MON_AccountAmount = nMON_AccountAmount
        oCopyInvoice.MON_LocalAmount = nMON_LocalAmount
        oCopyInvoice.REF_Own = sREF_Own
        oCopyInvoice.REF_Bank = sREF_Bank
        oCopyInvoice.Invoice_ID = sInvoice_ID
        oCopyInvoice.InvoiceNo = sInvoiceNo
        oCopyInvoice.CustomerNo = sCustomerNo
        'XNET 07.02.2013
        oCopyInvoice.SupplierNo = sSupplierNo
        oCopyInvoice.Unique_Id = sUnique_Id
        oCopyInvoice.STATEBANK_Code = sSTATEBANK_Code
        oCopyInvoice.STATEBANK_Text = sSTATEBANK_Text
        oCopyInvoice.STATEBANK_DATE = sSTATEBANK_DATE
        oCopyInvoice.Extra1 = sExtra1
        oCopyInvoice.ImportFormat = iImportFormat
        oCopyInvoice.MATCH_Original = bMATCH_Original
        oCopyInvoice.MATCH_Final = bMATCH_Final
        oCopyInvoice.MATCH_Matched = bMATCH_Matched
        oCopyInvoice.MATCH_MatchType = eMATCH_Type
        oCopyInvoice.MATCH_ID = sMATCH_ID
        oCopyInvoice.MATCH_ClientNo = sMATCH_ClientNo
        oCopyInvoice.MATCH_VATCode = sMATCH_VATCode
        oCopyInvoice.MATCH_PartlyPaid = bMATCH_PartlyPaid
        oCopyInvoice.MATCH_ERPName = sMATCH_ERPName
        oCopyInvoice.CargoFTX = sCargoFTX
        oCopyInvoice.MyField = sMyField
        oCopyInvoice.ToSpecialReport = bToSpecialReport

        ' added new fields 11.12.2014
        'oCopyInvoice.MON_CurrencyAmount = sMON_Currency
        ' 15.10.2019 - Denne over m� jo v�re riv ruskende gal
        oCopyInvoice.MON_CurrencyAmount = nMON_CurrencyAmount
        oCopyInvoice.MON_Currency = sMON_Currency
        oCopyInvoice.Dim1 = sDim1
        oCopyInvoice.Dim2 = sDim2
        oCopyInvoice.Dim3 = sDim3
        oCopyInvoice.Dim4 = sDim4
        oCopyInvoice.Dim5 = sDim5
        oCopyInvoice.Dim6 = sDim6
        oCopyInvoice.Dim7 = sDim7
        oCopyInvoice.Dim8 = sDim8
        oCopyInvoice.Dim9 = sDim9
        oCopyInvoice.Dim10 = sDim10
        oCopyInvoice.MyField2 = sMyField2
        oCopyInvoice.MyField3 = sMyField3

        'Set from inside the class
        'ProfileInUse

        'Just local variables
        'sFormatType
        'sImportLineBuffer

        'Not included
        'oFile
        'nIndex
        'oXLWSheet

        'Private bAmountSetTransferred As Boolean, bAmountSetInvoice As Boolean

        CopyObject = oCopyInvoice

    End Function


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        ' XNET 14.06.2013
        oErrorObjects = New ErrorObjects

        oFreetexts = New Freetexts
        sStatusCode = "00"
        sVisma_StatusCode = "00"
        bCancel = False
        bExported = False
        nMON_DiscountAmount = 0
        nMON_InvoiceAmount = 0
        nMON_TransferredAmount = 0
        nMON_AccountAmount = 0
        nMON_LocalAmount = 0
        bAmountSetInvoice = False
        bAmountSetTransferred = False
        sREF_Own = ""
        sREF_Bank = ""
        sInvoice_ID = ""
        sInvoiceNo = ""
        sCustomerNo = ""
        ' XNET 07.02.2013
        sSupplierNo = ""
        sUnique_Id = ""
        'XNET - 13.12.2010 - Added next variable - Can contain different dateformats
        sInvoiceDate = ""
        sSTATEBANK_Code = ""  '""
        sSTATEBANK_Text = ""
        sExtra1 = ""
        iImportFormat = BabelFiles.FileType.Unknown
        bMATCH_Original = True
        bMATCH_Final = True
        bMATCH_Matched = False
        sMyField = ""
        eMATCH_Type = BabelFiles.MatchType.OpenInvoice
        eTypeOfStructuredInfo = BabelFiles.TypeOfStructuredInfo.NotStructured '= TypeOfStructuredInfo.NotStructured
        sMATCH_ID = ""
        sMATCH_ClientNo = ""
        sMATCH_VATCode = ""
        bMATCH_PartlyPaid = False
        sMATCH_ERPName = ""
        bToSpecialReport = False

        ' XNET 27.03.2013 added property
        sVisma_FrmNo = vbNullString         ' Firm number

        nMON_CurrencyAmount = 0
        sMON_Currency = ""
        sDim1 = ""
        sDim2 = ""
        sDim3 = ""
        sDim4 = ""
        sDim5 = ""
        sDim6 = ""
        sDim7 = ""
        sDim8 = ""
        sDim9 = ""
        sDim10 = ""
        sMyField2 = ""
        sMyField3 = ""
        bBackPayment = False
        sBackPaymentAccount = ""

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub


    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object oFreetexts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFreetexts = Nothing
    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
    Private Function ReadGjensidige_S2000_PAYMULFile() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sProgress As String

        Do While True
            bReadLine = True ' Default

            'Check linetype
            Select Case Mid(sImportLineBuffer, 1, 3)

                Case "000" ' Start of file. Can be more than one "File" inside a physical file
                    sProgress = "Leser 000-record, BabelFile / ReadGjensidige_S2000_PAYMULFile()"
                    'If the sender is BBS identified by "00008080" then the file is send from
                    ' BBS and not from the internal system.
                    Exit Do

                Case "020" ' Ser ut som en "batch" header record, for poster gruppert etter valuta, dato og konto
                    '020200708240708240101                         ZZZ000000019580313.00NOKNOK347NOK
                    sProgress = "Leser 020-record, bFromLevelUnder = True, Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Exit Do

                Case "040" ' F�rste record i en betaling
                    ' 040000000000000403.0007082401010001
                    sProgress = "Leser 040-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Exit Do

                Case "070" ' Norges Bank kode/tekst
                    ' 07029 FORSIKRINGSERSTATNING
                    sProgress = "Tolker 070-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    sSTATEBANK_Code = Mid(sImportLineBuffer, 4, 3)
                    sSTATEBANK_Text = Mid(sImportLineBuffer, 7, 70)
                    oCargo.FreeText = sSTATEBANK_Code & sSTATEBANK_Text

                Case "071" ' Notification record
                    ' 071SK.NR. 577910630002
                    sProgress = "Tolker 071-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    oFreeText = oFreetexts.Add()
                    ' 04.01.2008
                    ' PGA SEBS problemer med Telepay, m� vi fjerne komma fra fritekst!!
                    ' NBNBNB **** Denne test m� fjernes n�r vi skal ta ibruk PAYMUL
                    oFreeText.Text = Mid(sImportLineBuffer, 4, 34) ' Det passer � dele i 34, pga tekstens utforming
                    ' 04.01.2008
                    ' PGA SEBS problemer med Telepay, m� vi fjerne komma fra fritekst!!
                    ' NBNBNB **** Denne test m� fjernes n�r vi skal ta ibruk PAYMUL
                    'oFreeText.Text = Replace(oFreeText.Text, ",", " ")
                    If Not EmptyString(Mid(sImportLineBuffer, 38, 35)) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 38, 35)
                        ' 04.01.2008
                        ' PGA SEBS problemer med Telepay, m� vi fjerne komma fra fritekst!!
                        ' NBNBNB **** Denne test m� fjernes n�r vi skal ta ibruk PAYMUL
                        'oFreeText.Text = Replace(oFreeText.Text, ",", " ")
                    End If

                    ' 13.12.2007
                    ' SEB can not use Invoices with 0-amount, then we must put as much info into
                    ' one BETFOR04/23 as possible

                    ' For International, we must create another Invoice if more than 35 chars text,
                    ' due to the way we have constructed WriteTelepay2 !
                    'If oCargo.PayType = "I" Then
                    '    ' Must read next line here!
                    '    sImportLineBuffer = ReadGjensidige_S2000_PAYMULRecord(oFile)
                    '    Exit Do
                    'End If


                Case "072" ' Struktur
                    ' Benyttes i Segment 13, NAD, kvalifikator 3035, dataelement 3207.
                    ' 07211 21
                    sProgress = "Leser 072-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"

                Case "078" ' Also Notification record
                    ' 078GJENSIDIGE SKADE 778477050495
                    sProgress = "Tolker 078-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"

                    'New code 06.03.2008
                    oCargo.Temp = oCargo.Temp & Mid(sImportLineBuffer, 4, 70)

                    'Old code
                    'Set oFreeText = oFreetexts.Add()
                    'oFreeText.Text = Mid$(sImportLineBuffer, 4, 70)

                    ' 04.01.2008
                    ' PGA SEBS problemer med Telepay, m� vi fjerne komma fra fritekst!!
                    ' NBNBNB **** Denne test m� fjernes n�r vi skal ta ibruk PAYMUL
                    'oFreeText.Text = Replace(oFreeText.Text, ",", " ")

                Case "080" ' End of payment if not KID
                    ' Empty
                    ' 080
                    sProgress = "Leser 080-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"

                Case "081" ' ???????? Not in use ???????????
                    sProgress = "Leser 081-record, DENNE ER ENN� IKKE PROGRAMMERT. Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-005", , , , (oFile.Line))

                Case "082" ' ???????? Not in use ???????????
                    sProgress = "Leser 082-record, DENNE ER ENN� IKKE PROGRAMMERT. Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-005", , , , (oFile.Line))

                Case "083" ' ???????? Not in use ???????????
                    sProgress = "Leser 083-record, DENNE ER ENN� IKKE PROGRAMMERT. Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-005", , , , (oFile.Line))

                Case "084" ' KID, and end of payment
                    ' 08422088521577983953254
                    sProgress = "Tolker 084-record, Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    sUnique_Id = Mid(sImportLineBuffer, 4, 35)
                    oCargo.PayCode = "301" 'OCR Outgoing

                Case "089" ' ???????? Not in use ???????????
                    sProgress = "Leser 089-record, DENNE ER ENN� IKKE PROGRAMMERT. Invoice / ReadGjensidige_S2000_PAYMULFile()"
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-005", , , , (oFile.Line))


                    'All other content would indicate error
                Case Else
                    'Error # 10010-008
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sProgress & " " & sImportLineBuffer, "10010-008", , , , (oFile.Line))

            End Select

            'WARNING! Is this correct
            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                'UPGRADE_WARNING: Couldn't resolve default property of object ReadGjensidige_S2000_PAYMULRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImportLineBuffer = ReadGjensidige_S2000_PAYMULRecord(oFile)
            End If

        Loop

        ReadGjensidige_S2000_PAYMULFile = sImportLineBuffer

    End Function
    Private Function ReadKLINK_ExcelUtbetaling() As String
        'Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim sMessage As String


        nMON_InvoiceAmount = Val(Replace(Trim(xDelim(sImportLineBuffer, Chr(9), 5)), ",", ".")) * 100
        nMON_InvoiceAmount = nMON_InvoiceAmount * -1
        nMON_TransferredAmount = nMON_InvoiceAmount
        bAmountSetInvoice = True
        sMessage = Trim(xDelim(sImportLineBuffer, Chr(9), 20))

        Do While Len(sMessage) > 0
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Left(PadRight(sMessage, 40, " "), 40)
            sMessage = Mid(sMessage, 41)
        Loop

        sUnique_Id = Trim(xDelim(sImportLineBuffer, Chr(9), 15))
        If EmptyString(sUnique_Id) Then
            sUnique_Id = "UTBET"
        End If

        ReadKLINK_ExcelUtbetaling = sImportLineBuffer

    End Function
    Private Function ReadDnBNOR_US_Penta() As String
        Dim oFreeText As Freetext
        Dim sTmp As String

        nMON_InvoiceAmount = CDbl(ConvertToAmount(Mid(sImportLineBuffer, 5, 15), "", "."))
        bAmountSetInvoice = True

        If Left(sImportLineBuffer, 1) = "A" Or Left(sImportLineBuffer, 1) = "L" Then
            ' for ACH, read one line with Remittance info, 80 chars, split into two freetexts
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid(sImportLineBuffer, 557, 40)
            If Not EmptyString(Mid(sImportLineBuffer, 597, 40)) Then
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sImportLineBuffer, 597, 40)
            End If
        Else
            ' other paymenttypes, read upto four lines a 35 chars
            '24. Remittance Information  557 636 80  35  o
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid(sImportLineBuffer, 557, 35)
            If Not EmptyString(Mid(sImportLineBuffer, 637, 35)) Then
                '25. Remittance Information  637 671 35  35  o
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sImportLineBuffer, 637, 35)
            End If
            If Not EmptyString(Mid(sImportLineBuffer, 672, 35)) Then
                '26. Remittance Information  672 706 35  35  o
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sImportLineBuffer, 672, 35)
            End If
            If Not EmptyString(Mid(sImportLineBuffer, 707, 35)) Then
                '27. Remittance Information  707 741 35  35  o
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sImportLineBuffer, 707, 35)
            End If
        End If

        If Not oFile.AtEndOfStream Then
            'UPGRADE_WARNING: Couldn't resolve default property of object ReadDnBNOR_US_PentaRecord(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sImportLineBuffer = ReadDnBNOR_US_PentaRecord(oFile) ', oProfile)
        Else
            sImportLineBuffer = ""
        End If

        ReadDnBNOR_US_Penta = sImportLineBuffer

    End Function

    Private Function Read_DnV_UK_Sage() As String
        ' Det norske Veritas (DnV in UK - from Sage)
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim nTempAmount As Double
        Dim i As Short

        nMON_InvoiceAmount = Val(Mid(sImportLineBuffer, 32, 15))
        sREF_Own = Trim(Mid(sImportLineBuffer, 65, 18))

        ' notification of receiver
        oFreeText = oFreetexts.Add
        oFreeText.Text = PadRight(Mid(sImportLineBuffer, 65, 18), 40, " ")

        Read_DnV_UK_Sage = sImportLineBuffer

    End Function
    Private Function ReadSeajackWages() As String
        Dim oFreeText As Freetext

        nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ";", 7), "", ".,")) 'G forel�pig
        bAmountSetInvoice = True

        'International
        oFreeText = oFreetexts.Add()
        oFreeText.Text = "Wages"
        sREF_Own = xDelim(sImportLineBuffer, ";", 1) ' A Name

        sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 7)

        ReadSeajackWages = sImportLineBuffer

    End Function
    Private Function ReadABNAmroBTL91() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext

        bReadLine = False

        Do While True

            Select Case Mid(sImportLineBuffer, 1, 2)

                Case "31" ' 31 = Totals record
                    Exit Do

                Case "21" ' 21 = Sub record 1
                    Exit Do

                    'Informationrecord
                Case "24" ' 24 = Sub record 4
                    oFreeText = oFreetexts.Add()
                    oFreeText.Text = Mid(sImportLineBuffer, 7, 35)
                    If Not EmptyString(Mid(sImportLineBuffer, 42, 35)) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 42, 35)
                    End If
                    If Not EmptyString(Mid(sImportLineBuffer, 77, 35)) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 77, 35)
                    End If
                    If Not EmptyString(Mid(sImportLineBuffer, 112, 35)) Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 112, 35)
                    End If

                    bReadLine = True

                    'All other content would indicate error
                Case Else
                    'Error # 10010-008
                    Call BabelImportError(iImportFormat, oCargo.FilenameIn, sImportLineBuffer, "10010-008")

            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadABNAmroBTL91Record(oFile)
            End If

        Loop

        ReadABNAmroBTL91 = sImportLineBuffer

    End Function
    Private Function ReadINS2000(ByRef nodPaymentDetails As System.Xml.XmlNode) As Boolean
        Dim oFreeText As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim nTestNodes As System.Xml.XmlNodeList

        bReturnValue = True


        'sLocalSTATEBANK_Code & Text Added during export from setup in BB
        sREF_Own = nodPaymentDetails.SelectSingleNode("child::*[name()='TRANSACTIONID']").InnerText
        'sREF_Own = sREF_Own & "-" & nodPaymentDetails.selectSingleNode("child::*[name()='DISREF']").nodeTypedValue
        sREF_Own = sREF_Own & "-" & nodPaymentDetails.SelectSingleNode("child::*[name()='OURREF']").InnerText

        'nMON_InvoiceAmount = Val(nodPaymentDetails.selectSingleNode("child::*[name()='NETPRE']").nodeTypedValue) * 100
        nMON_InvoiceAmount = Val(nodPaymentDetails.SelectSingleNode("child::*[name()='AMOUNT']").InnerText) * 100

        nTestNodes = nodPaymentDetails.selectNodes("child::*[name()='TEXTMESSAGE']")     'XOKNET 20.09.2012 - Changed from MESSAGE to TEXTMESSAGE after receiving a mail INS2000
        If nTestNodes.Count > 0 Then

            sFreetext = nodPaymentDetails.SelectSingleNode("child::*[name()='TEXTMESSAGE']").InnerText 'XNET 20.09.2012

            ' 04.11.2020 - added posibilty for KID
            ' If freetext starts with *, then it is KID
            ' Used first time by Wilhelmsen
            If Left(sFreetext, 1) = "*" Then
                sUnique_Id = Mid(sFreetext, 2)
            Else
                Select Case Len(sFreetext)
                    Case 0
                        'Nothing to do

                    Case Is < 41
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = sFreetext

                    Case Is < 81
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left(sFreetext, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sFreetext, 41)

                    Case Is < 121
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left(sFreetext, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sFreetext, 41, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sFreetext, 81)

                    Case Else
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left(sFreetext, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sFreetext, 41, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sFreetext, 81, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sFreetext, 121)

                End Select
            End If
        End If

        ' added 16.05.2014
        sSTATEBANK_Code = "29"
        sSTATEBANK_Text = "Forsikring"

        ReadINS2000 = bReturnValue

    End Function
    'Removed 11.09.2019
    'Private Function ReadNHC_Philippines(ByRef nodPaymentDetails As System.Xml.XmlNode) As Boolean
    '    Dim oFreeText As Freetext
    '    Dim bReturnValue As Boolean
    '    Dim sFreetext As String
    '    Dim sTmp As String

    '    bReturnValue = True


    '    'sLocalSTATEBANK_Code & Text Added during export from setup in BB
    '    'sREF_Own = GetInnerText(nodPaymentDetails, "child::*[name()='TRANSACTIONID']", Nothing)
    '    sREF_Own = GetInnerText(nodPaymentDetails, "PAYMENT/OURREF", Nothing).Trim

    '    sTmp = GetInnerText(nodPaymentDetails, "PAYMENT/AMOUNT", Nothing).Trim
    '    nMON_InvoiceAmount = Math.Round(Val(sTmp) * 100, 0)

    '    sFreetext = GetInnerText(nodPaymentDetails, "PAYMENT/MESSAGE", Nothing)

    '    Select Case Len(sFreetext)
    '        Case 0
    '            'Nothing to do

    '        Case Is < 41
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = sFreetext

    '        Case Is < 81
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Left(sFreetext, 40)
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Mid(sFreetext, 41)

    '        Case Is < 121
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Left(sFreetext, 40)
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Mid(sFreetext, 41, 40)
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Mid(sFreetext, 81)

    '        Case Else
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Left(sFreetext, 40)
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Mid(sFreetext, 41, 40)
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Mid(sFreetext, 81, 40)
    '            oFreeText = oFreetexts.Add()
    '            oFreeText.Text = Mid(sFreetext, 121)

    '    End Select

    '    '' added 16.05.2014
    '    'sSTATEBANK_Code = "29"
    '    'sSTATEBANK_Text = "Forsikring"

    '    ReadNHC_Philippines = bReturnValue

    'End Function
    Private Function ReadBambora_XML(ByRef nodPaymentDetails As System.Xml.XmlNode) As Boolean
        Dim oFreeText As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTmp As String

        On Error GoTo ERR_ReadBambora_XML

        bReturnValue = True

        sTmp = ""
        sTmp = GetInnerText(nodPaymentDetails, "TransactionAmountPresented", Nothing)
        If sTmp = "" Then
            sTmp = GetInnerText(nodPaymentDetails, "EstimatedSettlementValue", Nothing)
        End If
        If sTmp = "" Then
            nMON_InvoiceAmount = 0
        Else
            nMON_InvoiceAmount = Val(sTmp)
        End If
        nMON_TransferredAmount = nMON_InvoiceAmount

        sFreetext = GetInnerText(nodPaymentDetails, "AdditionalReferenceNumber", Nothing) & " " & ConvertFromAmountToString(nMON_InvoiceAmount, ".", ",")
        sUnique_Id = GetInnerText(nodPaymentDetails, "AdditionalReferenceNumber", Nothing)

        '14.11.2016 - Special for Kredinor
        If oCargo.Special = "KREDINOR_K90" Then
            sMATCH_ID = sUnique_Id
            sInvoiceNo = sUnique_Id
        End If

        Select Case sFreetext.Length
            Case 0
                'Nothing to do

            Case Is < 41
                oFreeText = oFreetexts.Add()
                oFreeText.Text = sFreetext

            Case Is < 81
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(sFreetext, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sFreetext, 41)

            Case Is < 121
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(sFreetext, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sFreetext, 41, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sFreetext, 81)

            Case Else
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left(sFreetext, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sFreetext, 41, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sFreetext, 81, 40)
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Mid(sFreetext, 121)

        End Select

        ReadBambora_XML = bReturnValue
        On Error GoTo 0
        Exit Function

ERR_ReadBambora_XML:

        If Not nodPaymentDetails Is Nothing Then
            nodPaymentDetails = Nothing
        End If

        Err.Raise(Err.Number, "ReadBambora_XML - Payment", Err.Description)

    End Function
    Private Function ReadeGlobal(ByVal nodPaymentDetails As MSXML2.IXMLDOMNode) As Boolean
        Dim oFreeText As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim nTestNodes As MSXML2.IXMLDOMNodeList

        bReturnValue = True


        'sLocalSTATEBANK_Code & Text Added during export from setup in BB
        'XNET - 14.02.2012 - Changed the next line and added three more
        'XNET - 02.03.2012 - Changed the next few lines
        sREF_Own = oCargo.REF
        'sREF_Own = PadRight(nodPaymentDetails.selectSingleNode("child::*[name()='OURTRANSACTIONID']").nodeTypedValue, 27, " ")
        nTestNodes = nodPaymentDetails.selectNodes("child::*[name()='STATEMENTNO']")
        If nTestNodes.length > 0 Then
            sREF_Own = sREF_Own & "&" & nodPaymentDetails.selectSingleNode("child::*[name()='STATEMENTNO']").nodeTypedValue
        End If
        nMON_InvoiceAmount = Val(nodPaymentDetails.selectSingleNode("child::*[name()='NETPRE']").nodeTypedValue) * 100
        nTestNodes = nodPaymentDetails.selectNodes("child::*[name()='KIDREF']")
        If nTestNodes.length = 1 Then
            sUnique_Id = nodPaymentDetails.selectSingleNode("child::*[name()='KIDREF']").nodeTypedValue
        End If

        If EmptyString(sUnique_Id) Then
            nTestNodes = nodPaymentDetails.selectNodes("child::*[name()='MESSAGE']")
            If nTestNodes.length > 0 Then

                sFreetext = nodPaymentDetails.selectSingleNode("child::*[name()='MESSAGE']").nodeTypedValue

                Select Case Len(sFreetext)
                    Case 0
                        'Nothing to do

                    Case Is < 41
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = sFreetext

                    Case Is < 81
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left$(sFreetext, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sFreetext, 41)

                    Case Is < 121
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left$(sFreetext, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sFreetext, 41, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sFreetext, 81)

                    Case Else
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Left$(sFreetext, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sFreetext, 41, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sFreetext, 81, 40)
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid$(sFreetext, 121)

                End Select
            End If
        End If

        ReadeGlobal = bReturnValue

    End Function
    Friend Function ReadConnect_WCFRemit(ByVal wcfPayment As Conecto.BabelTransactionDto) As Boolean
        Dim oFreetext As vbBabel.Freetext
        Dim sText As String = ""
        Dim bReturnValue As Boolean = False

        On Error GoTo ERR_ReadConnect_WCFRemit

        sREF_Own = wcfPayment.Trans_OpId
        nMON_InvoiceAmount = wcfPayment.RemitAmount * -100
        'sFreetext = wcfPayment

        ' Melding til mottaker inntil 255 tegn, del opp i 40 og 40
        sText = wcfPayment.RemitText
        Do While Len(sText) > 0
            If Len(sText) > 0 Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = Left(sText, 40)
            End If
            sText = Mid(sText, 41)
        Loop

        Return True

ERR_ReadConnect_WCFRemit:

        Err.Raise(Err.Number, "ReadConnect_WCFRemit - Invoice", Err.Description)


    End Function
    ' XNET 14.10.2010 Added function ReadSAP_PAYEXT_PEXR2002
    Private Function ReadSAP_PAYEXT_PEXR2002() As String
        Dim bReadLine As Boolean
        Dim oFreeText As Freetext
        Dim bFromLevelUnder As Boolean

        bFromLevelUnder = False

        Do While True

            bReadLine = True
            'Check linetype
            ' Read recordtype from first 10
            Select Case Trim$(Mid$(sImportLineBuffer, 1, 10))
                Case "EDI_DC40"
                    bReadLine = False
                    bFromLevelUnder = False
                    ' new payment, exit
                    Exit Do
                Case "E2IDB02002"
                Case "E2IDPU1"
                    ' PAYORD/REMADV (DOC)
                    If bFromLevelUnder Then
                        ' if line imported here, then jump up to payment to create an other invoice
                        Exit Do
                    End If

                    ' if from payment, nothing to do here, just import new line
                    bReadLine = True

                Case "E2IDKU3002"
                Case "E2IDKU5"
                Case "E2IDPU5", "E2IDPU5001" 'XNET 08.02.2011 added "E2IDPU5001"
                    'If Mid$(sImportLineBuffer, 64, 3) = "006" Then
                    ' XNET 23.02.2011 - IMPORTANT change, covering negative amounts (credits)
                    ' Pacific reported problems with credits, and after seeing examples, they have
                    ' qualifer = 016, and debits (normal payments) have 004
                    ' thus, changed the If
                    If Mid$(sImportLineBuffer, 64, 3) = "004" Or Mid$(sImportLineBuffer, 64, 3) = "016" Then
                        ' find amount here
                        ' XOKNET 27.10.2010 - problems with some amounts having decimals. some without.
                        If InStr(Mid$(sImportLineBuffer, 67, 18), ".") > 0 Then
                            ' as pre 27.10.2010
                            nMON_InvoiceAmount = Val(Replace(Mid$(sImportLineBuffer, 67, 18), ".", ""))
                        Else
                            nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 67, 18)) * 100
                        End If
                        ' XNET 23.02.2011 - IMPORTANT change, covering negative amounts (credits)
                        ' Pacific reported problems with credits, and after seeing examples, they have
                        ' qualifer = 016, and debits (normal payments) have 004
                        ' thus, added next If
                        If Mid$(sImportLineBuffer, 64, 3) = "016" Then
                            nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                        End If

                        nMON_TransferredAmount = nMON_InvoiceAmount
                    End If
                    bReadLine = True

                Case "E2EDKA1003"
                    ' XNET 24.11.2010 Case "E2IDB02002"

                Case "E2EDP02001"
                    ' XNET 23.02.2011, Pacific has credits, with 028 here, thus added 028
                    If Mid$(sImportLineBuffer, 64, 3) = "009" Or Mid$(sImportLineBuffer, 64, 3) = "028" Then
                        oFreeText = oFreetexts.Add()
                        oFreeText.Text = Mid(sImportLineBuffer, 67, 35)
                    End If
                    If Mid$(sImportLineBuffer, 64, 3) = "010" Then
                        sREF_Own = Mid(sImportLineBuffer, 67, 35)
                    End If
                    bReadLine = True

                Case Else
                    ' many other legal recordtypes, which we do not use
            End Select

            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadGenericRecord(oFile)
                bFromLevelUnder = True
            End If

        Loop

        ReadSAP_PAYEXT_PEXR2002 = sImportLineBuffer

    End Function
    Private Function ReadDnBNOR_Connect_Test() As String
        Dim oFreeText As Freetext
        Dim sMessage As String
        Dim sPaymentType As String
        Dim i As Long

        ' AmountDetails
        ' Remove spaces, change from decimal , to .
        nMON_InvoiceAmount = Val(Replace(Replace(xDelim(sImportLineBuffer, ";", 4, Chr(34)), ",", "."), " ", "")) * 100    'D
        bAmountSetInvoice = True

        If EmptyString(Trim(xDelim(sImportLineBuffer, ";", 1, Chr(34)))) And _
            EmptyString(Trim(xDelim(sImportLineBuffer, ";", 2, Chr(34)))) And _
            EmptyString(Trim(xDelim(sImportLineBuffer, ";", 3, Chr(34)))) And _
            EmptyString(Trim(xDelim(sImportLineBuffer, ";", 6, Chr(34)))) And _
            EmptyString(Trim(xDelim(sImportLineBuffer, ";", 18, Chr(34)))) And _
            oCargo.Temp <> "" Then

            ' When used with structured info, user puts into only for Amount (D) and Ref/KID/InvoiceNo, etc. Then find info in previous row
            sSTATEBANK_Code = xDelim(oCargo.Temp, ";", 10, Chr(34))    'J
            sSTATEBANK_Text = xDelim(oCargo.Temp, ";", 11, Chr(34))   'K

            sREF_Own = xDelim(oCargo.Temp, ";", 3, Chr(34))   'C
            sMessage = xDelim(oCargo.Temp, ";", 34, Chr(34))  'AH

            ' can be invoiceNo, or KID/FIK/REF or RF Sepa
            If UCase(xDelim(oCargo.Temp, ";", 37, Chr(34))) = "INVOICENO" Then
                sInvoiceNo = xDelim(sImportLineBuffer, ";", 36, Chr(34)) 'AJ
            ElseIf UCase(xDelim(oCargo.Temp, ";", 37, Chr(34))) = "RF" Then
                sUnique_Id = "RF" & xDelim(sImportLineBuffer, ";", 36, Chr(34))  'AJ
            ElseIf UCase(xDelim(oCargo.Temp, ";", 37, Chr(34))) = "FIK" Then
                ' Special treatment of FI-payments (Danish FI-cards)
                sUnique_Id = xDelim(sImportLineBuffer, ";", 36, Chr(34))  'AJ
                sUnique_Id = SplitFI(sUnique_Id, 2)  '2=Kortart+BetalingsID
            Else
                sUnique_Id = xDelim(sImportLineBuffer, ";", 36, Chr(34))  'AJ
            End If

        Else
            sSTATEBANK_Code = xDelim(sImportLineBuffer, ";", 10, Chr(34))    'J
            sSTATEBANK_Text = xDelim(sImportLineBuffer, ";", 11, Chr(34))   'K

            sREF_Own = xDelim(sImportLineBuffer, ";", 3, Chr(34))   'C
            sMessage = xDelim(sImportLineBuffer, ";", 34, Chr(34))  'AH

            ' can be invoiceNo, or KID/FIK/REF or RF Sepa
            If UCase(xDelim(sImportLineBuffer, ";", 37, Chr(34))) = "INVOICENO" Then
                sInvoiceNo = xDelim(sImportLineBuffer, ";", 36, Chr(34)) 'AJ
            ElseIf UCase(xDelim(sImportLineBuffer, ";", 37, Chr(34))) = "RF" Then
                sUnique_Id = "RF" & xDelim(sImportLineBuffer, ";", 36, Chr(34))  'AJ
            ElseIf UCase(xDelim(sImportLineBuffer, ";", 37, Chr(34))) = "FIK" Then
                ' Special treatment of FI-payments (Danish FI-cards)
                sUnique_Id = xDelim(sImportLineBuffer, ";", 36, Chr(34))  'AJ
                sUnique_Id = SplitFI(sUnique_Id, 2)  '2=Kortart+BetalingsID
            Else
                sUnique_Id = xDelim(sImportLineBuffer, ";", 36, Chr(34))  'AJ
            End If

            ' When used with structured info, user puts into only for Amount (D) and Ref/KID/InvoiceNo, etc. Then find info in previous row
            oCargo.Temp = sImportLineBuffer

        End If
        ' Cut message in chuncks of 40 and 40
        If Len(sMessage) > 0 Then
            Do
                oFreeText = oFreetexts.Add()
                oFreeText.Text = Left$(sMessage, 40)
                sMessage = Mid$(sMessage, 41)
                If Len(sMessage) = 0 Then
                    Exit Do
                End If
            Loop
        End If

        sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 42)
        Do While EmptyString(xDelim(sImportLineBuffer, ";", 4))
            ' Must have amount ("D")
            ' Count number of empty lines
            i = i + 1
            sImportLineBuffer = ReadLineFromExcel(oXLWSheet, 42)
            If i > 100 Then
                ' assume end when we have more than 100 empty lines
                Exit Do
            End If
        Loop

        ReadDnBNOR_Connect_Test = sImportLineBuffer

    End Function

    ' XNET 02.11.2010 New format Girodirekt
    Private Function ReadGirodirekt_Plusgiro() As String
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bFromLevelUnder As Boolean, bReadLine As Boolean, bCreateObject As Boolean
        Dim bCheckAccountNo As Boolean
        Dim bKontoInsetting As Boolean

        bFromLevelUnder = False

        Do While True
            bReadLine = True
            bCreateObject = False
            bCheckAccountNo = False

            '---------------------------
            ' Domestic payments
            '---------------------------
            Select Case Left$(sImportLineBuffer, 4)

                Case "PI00"  ' Betalning/Girering till PlusGirokonto
                    If bFromLevelUnder Then
                        ' to payment
                        Exit Do
                    End If

                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 33, 13))

                    If Not EmptyString(Mid$(sImportLineBuffer, 46, 25)) Then
                        Select Case Mid$(sImportLineBuffer, 5, 2)
                            ' paymenttype/accounttype
                            Case "00", "04"
                                ' OCR
                                sUnique_Id = Trim$(Mid$(sImportLineBuffer, 46, 25))
                            Case "02"
                                ' do nothing !!!!
                            Case Else
                                oFreeText = oFreetexts.Add
                                oFreeText.Text = Trim$(Mid$(sImportLineBuffer, 46, 25))
                        End Select
                    End If

                Case "BM99"  ' Meddelande
                    oFreeText = oFreetexts.Add
                    oFreeText.Text = Mid$(sImportLineBuffer, 5, 35)
                    If Not EmptyString(Mid$(sImportLineBuffer, 40, 35)) Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = Mid$(sImportLineBuffer, 40, 35)
                    End If

                Case "BA00"  ' Avs�ndarens egna noteringar
                    oCargo.Temp = Trim$(Mid$(sImportLineBuffer, 5, 18))
                    sREF_Own = Trim$(Mid$(sImportLineBuffer, 32, 35))
                    oCargo.REF = sREF_Own

                Case "CNDB"  ' Debetpost
                    If bFromLevelUnder Then
                        ' jump to payment to create another invoice, for the second "CN" and following
                        Exit Do
                    End If

                    sREF_Own = Mid$(sImportLineBuffer, 5, 35)
                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 40, 13))
                    If Not EmptyString(Mid$(sImportLineBuffer, 5, 35)) Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = Mid$(sImportLineBuffer, 5, 35)
                    End If

                    bFromLevelUnder = True

                Case "CNKR"  ' Kreditpost
                    If bFromLevelUnder Then
                        ' jump to payment to create another invoice, for the second "CN" and following
                        Exit Do
                    End If

                    sREF_Own = Mid$(sImportLineBuffer, 5, 35)
                    nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 40, 13)) * -1
                    If Not EmptyString(Mid$(sImportLineBuffer, 5, 35)) Then
                        oFreeText = oFreetexts.Add
                        oFreeText.Text = Mid$(sImportLineBuffer, 5, 35)
                    End If

                    bFromLevelUnder = True

                Case "MT00"  ' Avslutningspost
                    Exit Do

                Case Else
                    Exit Do

            End Select


            If oFile.AtEndOfStream = True Then
                Exit Do
            End If

            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadGiroDirektRecord(oFile)
                If Left$(sImportLineBuffer, 4) = "PI00" Then
                    bFromLevelUnder = True
                End If
            End If

        Loop

        ReadGirodirekt_Plusgiro = sImportLineBuffer

    End Function
    ' XokNET 16.04.2012 New function
    Private Function ReadDTAZV() As String
        Dim oFreeText As Freetext
        Dim iNoOfExtensionParts As Integer, iFreetextCounter As Integer
        Dim iCounter As Integer, iFillerCount As Integer

        ' We are at a T-record
        ' --------------------
        nMON_InvoiceAmount = Val(Mid$(sImportLineBuffer, 459, 14) & Mid$(sImportLineBuffer, 473, 2))
        nMON_TransferredAmount = nMON_InvoiceAmount
        bAmountSetInvoice = True

        If Not EmptyString(Mid$(sImportLineBuffer, 476, 35)) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid$(sImportLineBuffer, 476, 35)
        End If
        If Not EmptyString(Mid$(sImportLineBuffer, 511, 35)) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid$(sImportLineBuffer, 511, 35)
        End If
        If Not EmptyString(Mid$(sImportLineBuffer, 546, 35)) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid$(sImportLineBuffer, 546, 35)
        End If
        If Not EmptyString(Mid$(sImportLineBuffer, 581, 35)) Then
            oFreeText = oFreetexts.Add()
            oFreeText.Text = Mid$(sImportLineBuffer, 581, 35)
        End If

        ' At this stage we have "used" the T-record, 768 chars
        ' Delete from importlinebuffer;
        sImportLineBuffer = Mid$(sImportLineBuffer, 769)
        ' fill up with more chars
        sImportLineBuffer = ReadDTAZVRecord(oFile, sImportLineBuffer)


        ReadDTAZV = sImportLineBuffer

    End Function
    ' XNET 09.10.2012 - added function
    Private Function ReadSEB_CI_Incoming() As String
        Dim oFreetext As Freetext
        Dim sText As String
        Dim sTemp1 As String
        Dim sDelimiter As String
        Dim sSurround As String
        Dim bStructured As Boolean
        Dim sErrString As String

        ' NB! IKKE FERDIG - m� kj�res gjennom n�ye for structurerte !!!
        ' Sjekk ogs� om hvordan vi skal behandle strukturert info i felt 9
        ' ----------------------------------------------------------------------
        Try

            sDelimiter = ";"
            sSurround = Chr(34)

            '5. Bel�p
            sErrString = "MON_InvoiceAmount"
            sTemp1 = Replace(xDelim(sImportLineBuffer, sDelimiter, 5, sSurround), " ", "")
            nMON_InvoiceAmount = ConvertToAmount(sTemp1, ",", ".")
            nMON_TransferredAmount = nMON_InvoiceAmount

            sTemp1 = xDelim(sImportLineBuffer, sDelimiter, 13, sSurround)
            If sTemp1 = "230" Or sTemp1 = "240" Or sTemp1 = "YT7" Then
                bStructured = True
            Else
                bStructured = False
            End If

            ' Structured in 8
            If Not EmptyString(xDelim(sImportLineBuffer, sDelimiter, 8, sSurround)) And bStructured Then
                sErrString = "Structured"
                ' Split structured text
                ' KID;61046687150;NOK;331.00
                ' Invoice;6104661628;NOK;4,900.00;Invoice;6104661628.;NOK;16,633.00


                ' can be more than one structured pr line
                ' we must go to payment for each new invoice, except the first one, to create a new oInvoice
                ' store rest of structured text in oCargo, except the first one.
                If EmptyString(oCargo.Temp) Then
                    oCargo.Temp = xDelim(sImportLineBuffer, sDelimiter, 8, sSurround)
                End If

                ' XNET 20.09.2013 - added next IF, and Else/Endif further down because structured from Sweden may be formatted wrongly
                ' treat as structured if 4 columns (= 3 ; in textline)
                If Len(oCargo.Temp) - Len(Replace(oCargo.Temp, ";", "")) = 3 Then

                    sTemp1 = xDelim(oCargo.Temp, ";", 1)
                    If sTemp1 = "KID" Or sTemp1 = "KID Credit Note" Then
                        sUnique_Id = xDelim(oCargo.Temp, ";", 2)
                    ElseIf sTemp1 = "Invoice" Or sTemp1 = "Credit Note" Then
                        sInvoiceNo = xDelim(oCargo.Temp, ";", 2)
                    End If
                    ' must update invoiceamount for each structured
                    sTemp1 = xDelim(oCargo.Temp, ";", 4)
                    nMON_InvoiceAmount = ConvertToAmount(sTemp1, ",", ".")
                    nMON_TransferredAmount = nMON_InvoiceAmount

                    ' take next structured if present, and pass into oCargo;
                    ' next structured will always be 5th "column" from here;
                    ' this, remove ther first 4 "columns"
                    oCargo.Temp = Replace(oCargo.Temp, xDelim(oCargo.Temp, ";", 1) & ";" & xDelim(oCargo.Temp, ";", 2) & ";" & xDelim(oCargo.Temp, ";", 3) & ";" & xDelim(oCargo.Temp, ";", 4), "")
                    ' remove leading ; if present
                    If Left$(oCargo.Temp, 1) = ";" Then
                        oCargo.Temp = Mid$(oCargo.Temp, 2)
                    End If

                    ' If structured, then add 2 lines of freetext
                    If Not EmptyString(sUnique_Id) Then
                        eTypeOfStructuredInfo = vbBabel.BabelFiles.TypeOfStructuredInfo.StructureTypeKID
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = "KID: " & sUnique_Id
                    End If
                    If Not EmptyString(sInvoiceNo) Then
                        eTypeOfStructuredInfo = vbBabel.BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = "FakturaNr.: " & sInvoiceNo
                    End If

                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = LRS(60060) & ":  " & Str(nMON_InvoiceAmount) & " / "

                Else
                    ' XNET 20.09.2013 treat as unstructured, because there are not exactly 4 columns
                    oFreetext = oFreetexts.Add()
                    oFreetext.Text = oCargo.Temp
                    oCargo.Temp = ""
                End If
            Else
                ' unstructured - Additonal info i col 9
                sErrString = "Unstructured"
                sText = xDelim(sImportLineBuffer, sDelimiter, 9, sSurround)
                ' If no text in col 9 (I), then find text in col 5 (F)
                If EmptyString(sText) Then
                    sText = xDelim(sImportLineBuffer, sDelimiter, 6, sSurround)
                End If
                ' del opp i 40 og 40
                Do While Len(sText) > 0
                    If Len(sText) > 0 Then
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = Left$(sText, 40)
                    End If
                    sText = Mid$(sText, 41)
                Loop
            End If


            If EmptyString(oCargo.Temp) Then
                sErrString = "EmptyString(oCargo.Temp)"
                ' read new line only when we have "eaten" all KIDs og Invoices from structured text
                If Mid$(oCargo.BANK_I_SWIFTCode, 5, 2) = "NO" And _
                    xDelim(sImportLineBuffer, sDelimiter, 13, sSurround) = "" And _
                    Not (xDelim(sImportLineBuffer, sDelimiter, 10, sSurround) = "00000000000" Or xDelim(sImportLineBuffer, sDelimiter, 10, sSurround) = "0") And _
                    Not xDelim(sImportLineBuffer, sDelimiter, 11, sSurround) = "" Then

                    ' Swift payments not in batch with others, must then jump up to create new batch for each single payment. do not read new line for these ones!
                Else
                    sImportLineBuffer = ReadGenericRecord(oFile)
                End If

                ' there may also be uncomplete lines, must have info in col 3,4 and 5
                If EmptyString(xDelim(sImportLineBuffer, sDelimiter, 3, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 4, sSurround)) And EmptyString(xDelim(sImportLineBuffer, sDelimiter, 5, sSurround)) Then
                    ' read another line, forget this one
                    sImportLineBuffer = ReadGenericRecord(oFile) ', oProfile)
                End If

            End If
            ReadSEB_CI_Incoming = sImportLineBuffer

        Catch ex As Exception
            '------------------------------------
            ' Standard BB Errorexception handling
            '------------------------------------

            ' pass the error on, "up the ladder" to be handled in ImportExport
            Dim sLocalErr As String
            sLocalErr = ""
            sLocalErr = sLocalErr & "(BB) Source: oInvoice.ReadSEB_CI_Incoming" & vbCrLf
            sLocalErr = sLocalErr & "ErrorNo: " & Err.Number & vbCrLf
            sLocalErr = sLocalErr & "ErrorInfo: " & sErrString & vbCrLf

            sLocalErr = sLocalErr & "ErrorMsgEx: " & ex.Message & vbCrLf

            sLocalErr = sLocalErr & "Callstack:" & Environment.StackTrace
            Throw New System.Exception(sLocalErr)
        End Try


    End Function
    Friend Function ReadVisma_Database(ByVal oMyExternalDal As vbBabel.DAL, ByVal sMySQL As String) As vbBabel.DAL
        Dim oFreetext As vbBabel.Freetext
        Dim sText As String
        Dim sErrorString As String

        '******************************************************************
        ' Diverse:
        ' - vi m� ha en n�ye gjennomgang av hvilke bel�psfelt som er hvilke
        ' - finnes det annen melding til mottaker enn RecipReference ?
        ' - hvordan sette sammen Ref_Own p� oInvoice?
        ' - hvordan setter vi sClientNo ?
        '******************************************************************

        On Error GoTo ERR_ReadVisma_Database

        ' Invoiceamount
        sErrorString = "AmountCurrency"
        If Not EmptyString(oMyExternalDal.Reader_GetString("AmountCurrency")) Then
            nMON_InvoiceAmount = ConvertToAmountNet(oMyExternalDal.Reader_GetString("AmountCurrency"), "", ".,") * -1
            bAmountSetInvoice = True
        End If

        ' Transferredamount
        sErrorString = "CurrAmount"
        If Not EmptyString(oMyExternalDal.Reader_GetString("CurrAmount")) Then
            ' 27.05.2014
            ' For Creditnotes, CurrCashAmount is 0
            If CDbl(oMyExternalDal.Reader_GetString("CurrCashAmount")) = 0 Then
                nMON_TransferredAmount = nMON_InvoiceAmount
            Else
                nMON_TransferredAmount = ConvertToAmountNet(oMyExternalDal.Reader_GetString("CurrCashAmount"), "", ".,") * -1
            End If

            bAmountSetInvoice = True
        End If

        ' Localamount (Bel�p i regnskapets valuta?)
        sErrorString = "AmountDomestic"
        If Not EmptyString(oMyExternalDal.Reader_GetString("AmountDomestic")) Then
            nMON_LocalAmount = ConvertToAmountNet(oMyExternalDal.Reader_GetString("AmountDomestic"), "", ".,") * -1
            bAmountSetInvoice = True

            ' 12.06.2013 - There may be cases where rsPayments!AmountCurrency = 0
            ' After discussions with Eddie, we have then decided that we will use rsPayments!AmountDomestic in these cases
            If nMON_InvoiceAmount = 0 Then
                nMON_InvoiceAmount = ConvertToAmountNet(oMyExternalDal.Reader_GetString("AmountDomestic"), "", ".,") * -1
                bAmountSetInvoice = True
            End If
        End If

        ' KID
        sErrorString = "CidCode"
        'XokNET - 24.01.2014 - Added next IF
        If Not EmptyString(oMyExternalDal.Reader_GetString("CidCode")) Then
            ' XokNET 25.04.2014 - for Danmark, og FI, s� ligger FI eller Giro-koden (71, 73, 75 etc) i PmtMethod
            ' XokNET 02.07.2014
            ' VI skal hente FIK-kode fra EFTFormType
            'If StripSpecialChars(Trim$(rsPayments!FirmCountry)) = "DK" And rsPayments!PmtMethod <> 0 Then
            '   sUnique_Id = Trim$(rsPayments!PmtMethod) & StripSpecialChars(Trim$(rsPayments!CidCode))
            If StripSpecialChars(oMyExternalDal.Reader_GetString("FirmCountry")) = "DK" And oMyExternalDal.Reader_GetString("EftFormType") <> "0" Then
                ' changed 07.01.2016, for Girokort 01/04, which can be set as 1 or 4 from Business
                'sUnique_Id = Trim$(rsPayments!EftFormType) & StripSpecialChars(Trim$(rsPayments!CidCode))
                sUnique_Id = PadLeft(oMyExternalDal.Reader_GetString("EftFormType"), 2, "0") & StripSpecialChars(oMyExternalDal.Reader_GetString("CidCode"))

                ' TESTING 17.09.2015 - usikker p� om dette kanskje ogs� skal ligge generelt, avvent svar fra PJ/Eddie
            ElseIf StripSpecialChars(oMyExternalDal.Reader_GetString("FirmCountry")) = "DK" And oMyExternalDal.Reader_GetString("PmtMethod") <> "0" Then
                ' changed 07.01.2016, for Girokort 01/04, which can be set as 1 or 4 from Business
                'sUnique_Id = Trim$(CStr(rsPayments!PmtMethod)) & StripSpecialChars(Trim$(rsPayments!CidCode))
                sUnique_Id = PadLeft(oMyExternalDal.Reader_GetString("PmtMethod"), 2, "0") & StripSpecialChars(oMyExternalDal.Reader_GetString("CidCode"))

            Else
                sUnique_Id = StripSpecialChars(oMyExternalDal.Reader_GetString("CidCode"))
            End If
        End If

        ' InvoiceNo
        sErrorString = "InvoiceNo"
        sInvoiceNo = StripSpecialChars(oMyExternalDal.Reader_GetString("InvoiceNo"))

        ' Information to receiver
        ' Is there any info other than InvoiceNo ?
        sErrorString = "RecipReference"
        'If IsNull(rsPayments!CidCode) And IsNull(rsPayments!InvoiceNo) Then
        ' due to foreign payments, and non structured formats, always import freetext
        sText = StripSpecialChars(oMyExternalDal.Reader_GetString("RecipReference"))
        Do While Len(sText) > 0
            If Len(sText) > 0 Then
                oFreetext = oFreetexts.Add()
                oFreetext.Text = Left$(sText, 40)
            End If
            sText = Mid$(sText, 41)
        Loop

        ' For Domestic, structured payments, if no KID and no InvoiceNo,
        ' fill InvoiceNo with RecipRef, to avoid problems with mix of structured/unstructured;
        ' Dette er avgjort 12.07.2013 av Eddie, Hege og Jan-Petter
        If oCargo.PayType = "D" Then
            ' domestic;
            If EmptyString(sInvoiceNo) And EmptyString(sUnique_Id) Then
                sErrorString = "RecipReference"
                sInvoiceNo = StripSpecialChars(oMyExternalDal.Reader_GetString("RecipReference"))
            End If
        End If
        ' set SupplierNo (in wrong property, but OK)
        sErrorString = "SupNo"
        If Not EmptyString(oMyExternalDal.Reader_GetString("SupNo")) Then
            sCustomerNo = StripSpecialChars(oMyExternalDal.Reader_GetString("SupNo"))

            If sCustomerNo = "0" Then
                ' 07.12.2016 - in some cases there are Customerpayments, not Supplier
                sCustomerNo = StripSpecialChars(oMyExternalDal.Reader_GetString("CustomerNo"))
            End If
            ' also put into SupplierNo (used for Swedish formats)
            sSupplierNo = sCustomerNo   ' Changed 07.12.2016 StripSpecialChars(oMyExternalDal.Reader_GetString("SupNo"))
        End If

        ' If no freetext imported, fetch from InvoiceNo
        If oFreetexts.Count = 0 Then
            oFreetext = oFreetexts.Add()
            oFreetext.Text = StripSpecialChars(oMyExternalDal.Reader_GetString("InvoiceNo"))
        End If
        'End If

        ' Special handling of creditnotes to have better success with creditnotes, treat at structured if Telepay!
        If nMON_InvoiceAmount <= 0 Then
            sInvoiceNo = Left$(StripSpecialChars(oMyExternalDal.Reader_GetString("RecipReference")), 25)
        End If

        ' Ownref -
        '*****************************************************************
        ' Must be discussed. Important to use for matching of returnfiles
        ' As a start, use ClientNo+Bankpartner+Paymentline+Lineno
        '*****************************************************************
        sErrorString = "sRef_Own"
        'sREF_Own = sVisma_FrmNo & ";" & sVisma_BPartNo & ";" & Trim$(rsPayments!PmtLineNo) & ";" & Trim$(rsPayments!LineNo)
        sREF_Own = sVisma_FrmNo & ";" & oMyExternalDal.Reader_GetString("BPartNo") & ";" & oMyExternalDal.Reader_GetString("PmtLineNo") & ";" & oMyExternalDal.Reader_GetString("LineNo")


        sErrorString = "DeclCode"
        sSTATEBANK_Code = StripSpecialChars(oMyExternalDal.Reader_GetString("DeclCode"))

        sErrorString = "DeclText"
        sSTATEBANK_Text = StripSpecialChars(oMyExternalDal.Reader_GetString("DeclText"))

        sErrorString = "VoucherDate"
        sInvoiceDate = StripSpecialChars(oMyExternalDal.Reader_GetString("VoucherDate"))

        ReadVisma_Database = oMyExternalDal

        Exit Function

ERR_ReadVisma_Database:

        Err.Raise(Err.Number, "ReadVisma_Database - Invoice, Errorstring: " & sErrorString, Err.Description)

    End Function
    ' XNET 30.10.2012 new function
    Private Function ReadDanskeBankCollection() As String
        Dim oFreetext As Freetext
        Dim sMessage As String
        Dim i As Integer
        Dim sTmp As String
        sREF_Own = "X"

        'nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, ",", 23, Chr(34)), "", ".,")) '23. Amount
        nMON_InvoiceAmount = CDbl(xDelim(sImportLineBuffer, ",", 23, Chr(34))) '23. Amount
        bAmountSetInvoice = True

        sUnique_Id = xDelim(sImportLineBuffer, ",", 25, Chr(34))    '25. KID
        sCustomerNo = xDelim(sImportLineBuffer, ",", 26, Chr(34))   '26. Debtor ID
        sInvoiceNo = xDelim(sImportLineBuffer, ",", 28, Chr(34))    '28. InvoiceNo
        sExtra1 = xDelim(sImportLineBuffer, ",", 30, Chr(34))       '30. OrderNo

        ' Advice in field 65 - 10064
        ' We can't take that much info, restrict to 65-200
        For i = 65 To 200
            sMessage = xDelim(sImportLineBuffer, ",", i, Chr(34))
            If Len(sMessage) > 0 Then
                ' message is upto 80 chars, split in 2
                oFreetext = oFreetexts.Add
                oFreetext.Text = Left$(sMessage, 40)
                If Len(sMessage) > 40 Then
                    oFreetext = oFreetexts.Add
                    oFreetext.Text = Mid$(sMessage, 41)
                End If
            End If
        Next i

        ' Read another line;
        sImportLineBuffer = ReadDanskeBankCollectionRecord(oFile)

        ReadDanskeBankCollection = sImportLineBuffer

    End Function

    Private Function ReadTotal_IN() As String
        Dim oFreetext As Freetext
        Dim sFreetext As String
        Dim bReadLine As Boolean

        Do While True
            Select Case Left$(sImportLineBuffer, 2)

                Case "40" 'Meddelandepost
                    '1�2 Posttyp = 40 Numeriskt
                    '3�37 Meddelande 1, fr�n avs�ndaren. Alfanumeriskt, v�nsterst�llt, blankutfyllt
                    '38-72 Meddelande 2, fr�n avs�ndaren. Alfanumeriskt, v�nsterst�llt, blankutfyllt
                    '73�80 Reserv. Blankutfyllt.

                    sFreetext = Trim(Mid$(sImportLineBuffer, 3, 35))
                    If Not EmptyString(sFreetext) Then
                        oFreetext = oFreetexts.Add
                        oFreetext.Text = sFreetext
                        sFreetext = ""
                    End If
                    sFreetext = sFreetext & Trim(Mid$(sImportLineBuffer, 38, 35))
                    If Not EmptyString(sFreetext) Then
                        oFreetext = oFreetexts.Add
                        oFreetext.Text = sFreetext
                        sFreetext = ""
                    End If
                    bReadLine = True

                Case Else
                    bReadLine = False
                    Exit Do

            End Select

            '    If oFile.AtEndOfStream = True Then
            '        Exit Do
            '    End If
            '
            If bReadLine Then
                'Read new record
                sImportLineBuffer = ReadGenericRecord(oFile)
            End If

        Loop

        ReadTotal_IN = sImportLineBuffer

    End Function
    Private Function ReadSwedbank_CardReconciliation() As String
        Dim oFreetext As Freetext
        Dim bReturnValue As Boolean
        Dim sFreetext As String
        Dim sTemp As String
        Dim bReadLine As Boolean = False
        Dim bCreateObject As Boolean = False
        Dim bFromLevelUnder As Boolean = False
        On Error GoTo ERR_ReadSwedbank_CardReconciliation

        Do While True
            bReadLine = False
            bCreateObject = False

            Select Case Left(sImportLineBuffer, 4).Trim
                Case "F400" ' F400 - Transaction details

                    If bFromLevelUnder Then
                        bReadLine = False
                        Exit Do
                    End If

                    sREF_Own = Mid(sImportLineBuffer, 114, 12)    'Your reference
                    sREF_Bank = Mid(sImportLineBuffer, 127, 11)    'Our reference

                    nMON_InvoiceAmount = ConvertToAmount(Mid(sImportLineBuffer, 95, 14), "", ".", True) * 100 'Amount purchase
                    ' Normalt + fortegn, men ta h�yde for credits
                    If Mid(sImportLineBuffer, 94, 1) = "-" Then
                        nMON_InvoiceAmount = nMON_InvoiceAmount * -1
                    End If
                    nMON_TransferredAmount = nMON_InvoiceAmount


                    'The next fields may vary depending on company
                    '----------------------------------------------
                    sUnique_Id = Mid(sImportLineBuffer, 127, 11).Trim  ' Our ref
                    sInvoiceNo = Mid(sImportLineBuffer, 196, 23).Trim

                    '02.06.2016 - Removed for Patentstyret
                    'sFreetext = vbNullString
                    'sFreetext = Mid(sImportLineBuffer, 54, 21).Trim
                    'If Not EmptyString(sFreetext) Then
                    '    oFreetext = oFreetexts.Add()
                    '    oFreetext.Text = LRS(11018) & sFreetext     ' Kortnummer
                    'End If

                    '02.06.2016 - Removed for Patentstyret
                    'sFreetext = vbNullString
                    'sFreetext = Mid(sImportLineBuffer, 127, 11).Trim  ' Our ref
                    'If Not EmptyString(sFreetext) Then
                    '    oFreetext = oFreetexts.Add()
                    '    oFreetext.Text = "Our reference: " & sFreetext
                    'End If

                    '02.06.2016 - Removed for Patentstyret
                    'sFreetext = vbNullString
                    'sFreetext = Mid(sImportLineBuffer, 114, 12).Trim  ' Your ref
                    'If Not EmptyString(sFreetext) Then
                    '    oFreetext = oFreetexts.Add()
                    '    oFreetext.Text = "Your reference: " & sFreetext
                    'End If

                    '02.06.2016 - Removed for Patentstyret
                    'sFreetext = Mid(sImportLineBuffer, 156, 6).Trim  ' Approval code
                    'If Not EmptyString(sFreetext) Then
                    '    oFreetext = oFreetexts.Add()
                    '    oFreetext.Text = "Approval code: " & sFreetext
                    'End If

                    '02.06.2016 - Added for Patentstyret
                    sFreetext = Mid(sImportLineBuffer, 196, 23).Trim  ' Issuer Merchandise Info
                    If Not EmptyString(sFreetext) Then
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = "Merchandise info: " & sFreetext
                    End If

                    sFreetext = vbNullString
                    sFreetext = ConvertFromAmountToString(nMON_InvoiceAmount, , ",")
                    If Not EmptyString(sFreetext) Then
                        oFreetext = oFreetexts.Add()
                        oFreetext.Text = LRS(40021) & " " & sFreetext  ' Amount
                    End If

                    '02.06.2016 - Removed for Patentstyret
                    'sFreetext = Mid(sImportLineBuffer, 76, 10)
                    'If Not EmptyString(sFreetext) Then
                    '    oFreetext = oFreetexts.Add()
                    '    oFreetext.Text = "PurchaseDate: " & sFreetext
                    'End If
                    bReadLine = True
                    bFromLevelUnder = True

                Case Else
                    Exit Do
                    bReadLine = False

            End Select


            'Read next line
            If bReadLine Then
                sImportLineBuffer = ReadSwedbank_CardReconciliationRecord(oFile)
            End If

        Loop
        ReadSwedbank_CardReconciliation = sImportLineBuffer

        On Error GoTo 0
        Exit Function

ERR_ReadSwedbank_CardReconciliation:

        Err.Raise(Err.Number, "Invoice.ReadSwedbank_CardReconciliation", Err.Description)

    End Function
    Private Function ReadLindorff_Gebyr() As String

        Dim oFreetext As Freetext
        Dim bCheckAccountNo As Boolean, bx As Boolean
        Dim sRecordType As String = ""

        On Error GoTo ERR_Read


        sImportLineBuffer = Trim$(sImportLineBuffer)

        sRecordType = Left(sImportLineBuffer, 1)  'I=Innbetaling or F=Innbet. p� fakturaniv�

        oFreetext = Freetexts.Add()
        oFreetext.Text = Trim(Mid(sImportLineBuffer, 34, 8).Trim & " " & Mid(sImportLineBuffer, 92, 20).Trim)    '4. SAKSNR + 12. LEDIG

        ReadLindorff_Gebyr = sImportLineBuffer

        Exit Function

ERR_Read:
        Err.Raise(Err.Number, "ReadLindorff_Gebyr - Invoice", Err.Description)

    End Function
    Private Function ReadFlyWire_Incoming() As String
        ' FlyWire incoming payments, for Keystone Academic  

        Dim oFreeText As Freetext
        Dim sDelimiter As String = ","
        Dim sSurround As String = Chr(34)

        ' "7. ccy_to_amount" - invoice amount (received amount from FlyWire)
        nMON_InvoiceAmount = CDbl(ConvertToAmount(xDelim(sImportLineBuffer, sDelimiter, 7, sSurround), "", "."))
        nMON_TransferredAmount = nMON_InvoiceAmount

        ' "2. invoice_reference"
        sInvoiceNo = xDelim(sImportLineBuffer, sDelimiter, 2, sSurround)
        ' also set this one as freetext
        oFreeText = oFreetexts.Add()
        oFreeText.Text = sInvoiceNo

        ' "1. customer_reference"
        oFreeText = oFreetexts.Add()
        oFreeText.Text = xDelim(sImportLineBuffer, sDelimiter, 1, sSurround)

        If Not oFile.AtEndOfStream Then
            sImportLineBuffer = ReadGenericRecord(oFile)
            ReadFlyWire_Incoming = sImportLineBuffer
        Else
            sImportLineBuffer = ""
            ReadFlyWire_Incoming = sImportLineBuffer
        End If

    End Function

End Class
