Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmUnknownAccount
	Inherits System.Windows.Forms.Form
	Private oProfile As Profile
	'Private Sub CboFormatOut_LostFocus()
	'Dim oFormatUsed As FormatUsed
	'Dim oFilename As Filename
	'
	'For Each oFormatUsed In oProfile.FormatsUsed
	'    If oFormatUsed.Description = CboFormatOut.Text Then
	'        For Each oFilename In oFormatUsed.Filenames
	'            cboFilenameOut.AddItem oFilename.FileNameIn1
	'        Next
	'    End If
	'Next
	'
	'End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Dim bOK As Boolean
		
        If txtFileNameOut1.Text = "" Then
            'UPGRADE_WARNING: Couldn't resolve default property of object CreateFilename(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bOK = CreateFilename()
        Else
            Me.Hide()
        End If
	End Sub
	
	Public Function SendProfile(ByRef op As Profile) As Boolean
		
		oProfile = op
		
		SendProfile = True
		
		
	End Function
	
	
	Private Sub frmUnknownAccount_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		lblClientNo.Text = LRS(20004)
		lblAccountNo.Text = LRS(20005)
		lblFormatOut.Text = LRS(20006)
		lblFilenameOut1.Text = LRS(20007)
		Me.Text = LRS(20008)
		cmdCancel.Text = LRS(55001)
		cmdOK.Text = LRS(55002)
		
	End Sub
	
	Private Sub txtClientNo_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtClientNo.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Dim bOK As Boolean
		
		If KeyCode = (13 Or 9) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object CreateFilename(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			bOK = CreateFilename()
		End If
		
	End Sub
	
	Function CreateFilename() As Object
		Dim sFilenameOld, sFilenameNew As String
		Dim iPosition, iNoOfCharacters As Short
		Dim bSpecialCharcter As Boolean
		Dim iAnswer As Short
		
		sFilenameOld = txtFilename1Hidden.Text
		
        iPosition = InStr(sFilenameOld, "�")
		iNoOfCharacters = 1
		
		bSpecialCharcter = True
		
		Do While bSpecialCharcter
            If Mid(sFilenameOld, iPosition + iNoOfCharacters, 1) = "�" Then
                iNoOfCharacters = iNoOfCharacters + 1
            Else
                bSpecialCharcter = False
            End If
		Loop 
		
		If Len(txtClientNo.Text) = iNoOfCharacters Then
			sFilenameNew = VB.Left(sFilenameOld, iPosition - 1)
			sFilenameNew = sFilenameNew & txtClientNo.Text
			sFilenameNew = sFilenameNew & VB.Right(sFilenameOld, Len(sFilenameOld) - iPosition - iNoOfCharacters + 1)
			txtFileNameOut1.Text = sFilenameNew
			txtFileNameOut1.Visible = True
			txtFormatOut.Visible = True
		Else
			iAnswer = MsgBox(LRS(20002), MsgBoxStyle.OKOnly, LRS(20003))
			txtFileNameOut1.Visible = False
			txtFormatOut.Visible = False
		End If
		
		
	End Function
End Class
