Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_9001_Documentation
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim iBabelFile_ID As Integer
    Dim iBatch_ID As Integer
    Dim sBreakField As String

    Dim sReportName As String
    Dim sSpecial As String
    Dim bSQLServer As Boolean = False
    Dim iDetailLevel As Integer
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean

    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_9001_Documentation_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize

        Fields.Add("fldProdDate")

        ' grFreetext
        Fields.Add("fldFreetextBreak")
        Fields.Add("fldFreetextBody")

        'Detail
        Fields.Add("Body")
        Fields.Add("Label")

        ' PageFooter
        Fields.Add("fldRunDate")

        ' Breakfields
        Fields.Add("fldBreakMain")
        Fields.Add("fldBreakSub")
        Fields.Add("BreakField")

    End Sub

    Private Sub rp_9001_Documentation_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_9001_Documentation_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        ''Long or ShortDate
        'If bUseLongDate Then
        '    Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
        '    Me.txtDATE_Production.OutputFormat = "D"
        '    Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        'Else
        '    Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
        '    Me.txtDATE_Production.OutputFormat = "d"
        '    Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        'End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40009)   ' "BabelBank dokumentasjon" 
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

    End Sub

    Private Sub rp_9001_Documentation_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        Static iArrayCounter As Integer

        eArgs.EOF = False

        If Not eArgs.EOF Then

            iArrayCounter = iArrayCounter + 1

            Me.txtLabel.Text = "Brighton"
            Me.txtBody.Text = "Wins again"
            'START WORKING HERE

            If iArrayCounter > 2 Then
                eArgs.EOF = True
            End If

        End If

    End Sub
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
